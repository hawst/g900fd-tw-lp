.class public Lorg/apache/lucene/index/IndexWriter;
.super Ljava/lang/Object;
.source "IndexWriter.java"

# interfaces
.implements Ljava/io/Closeable;
.implements Lorg/apache/lucene/index/TwoPhaseCommit;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/IndexWriter$IndexReaderWarmer;,
        Lorg/apache/lucene/index/IndexWriter$ReaderPool;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final MAX_TERM_LENGTH:I = 0x7ffe

.field public static final SOURCE:Ljava/lang/String; = "source"

.field public static final SOURCE_ADDINDEXES_READERS:Ljava/lang/String; = "addIndexes(IndexReader...)"

.field public static final SOURCE_FLUSH:Ljava/lang/String; = "flush"

.field public static final SOURCE_MERGE:Ljava/lang/String; = "merge"

.field private static final UNBOUNDED_MAX_MERGE_SEGMENTS:I = -0x1

.field public static final WRITE_LOCK_NAME:Ljava/lang/String; = "write.lock"


# instance fields
.field private final analyzer:Lorg/apache/lucene/analysis/Analyzer;

.field final bufferedDeletesStream:Lorg/apache/lucene/index/BufferedDeletesStream;

.field private volatile changeCount:J

.field private volatile closed:Z

.field private volatile closing:Z

.field final codec:Lorg/apache/lucene/codecs/Codec;

.field private final commitLock:Ljava/lang/Object;

.field private final config:Lorg/apache/lucene/index/LiveIndexWriterConfig;

.field final deleter:Lorg/apache/lucene/index/IndexFileDeleter;

.field private final directory:Lorg/apache/lucene/store/Directory;

.field private docWriter:Lorg/apache/lucene/index/DocumentsWriter;

.field private filesToCommit:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final flushCount:Ljava/util/concurrent/atomic/AtomicInteger;

.field final flushDeletesCount:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final fullFlushLock:Ljava/lang/Object;

.field final globalFieldNumberMap:Lorg/apache/lucene/index/FieldInfos$FieldNumbers;

.field private volatile hitOOM:Z

.field final infoStream:Lorg/apache/lucene/util/InfoStream;

.field private keepFullyDeletedSegments:Z

.field private lastCommitChangeCount:J

.field private mergeExceptions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/MergePolicy$OneMerge;",
            ">;"
        }
    .end annotation
.end field

.field private mergeGen:J

.field private mergeMaxNumSegments:I

.field private mergePolicy:Lorg/apache/lucene/index/MergePolicy;

.field private final mergeScheduler:Lorg/apache/lucene/index/MergeScheduler;

.field private mergingSegments:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lorg/apache/lucene/index/SegmentInfoPerCommit;",
            ">;"
        }
    .end annotation
.end field

.field volatile pendingCommit:Lorg/apache/lucene/index/SegmentInfos;

.field volatile pendingCommitChangeCount:J

.field private pendingMerges:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lorg/apache/lucene/index/MergePolicy$OneMerge;",
            ">;"
        }
    .end annotation
.end field

.field private volatile poolReaders:Z

.field final readerPool:Lorg/apache/lucene/index/IndexWriter$ReaderPool;

.field private rollbackSegments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/SegmentInfoPerCommit;",
            ">;"
        }
    .end annotation
.end field

.field private runningMerges:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lorg/apache/lucene/index/MergePolicy$OneMerge;",
            ">;"
        }
    .end annotation
.end field

.field final segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

.field private segmentsToMerge:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lorg/apache/lucene/index/SegmentInfoPerCommit;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private stopMerges:Z

.field private writeLock:Lorg/apache/lucene/store/Lock;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 187
    const-class v0, Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    .line 212
    return-void

    .line 187
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexWriterConfig;)V
    .locals 14
    .param p1, "d"    # Lorg/apache/lucene/store/Directory;
    .param p2, "conf"    # Lorg/apache/lucene/index/IndexWriterConfig;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 629
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 235
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->segmentsToMerge:Ljava/util/Map;

    .line 245
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->mergingSegments:Ljava/util/HashSet;

    .line 249
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->pendingMerges:Ljava/util/LinkedList;

    .line 250
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->runningMerges:Ljava/util/Set;

    .line 251
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->mergeExceptions:Ljava/util/List;

    .line 255
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->flushCount:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 256
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->flushDeletesCount:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 258
    new-instance v0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;

    invoke-direct {v0, p0}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;-><init>(Lorg/apache/lucene/index/IndexWriter;)V

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->readerPool:Lorg/apache/lucene/index/IndexWriter$ReaderPool;

    .line 2785
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->commitLock:Ljava/lang/Object;

    .line 2886
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->fullFlushLock:Ljava/lang/Object;

    .line 630
    new-instance v0, Lorg/apache/lucene/index/LiveIndexWriterConfig;

    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/index/IndexWriterConfig;->clone()Lorg/apache/lucene/index/IndexWriterConfig;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/LiveIndexWriterConfig;-><init>(Lorg/apache/lucene/index/IndexWriterConfig;)V

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->config:Lorg/apache/lucene/index/LiveIndexWriterConfig;

    .line 631
    iput-object p1, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    .line 632
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->config:Lorg/apache/lucene/index/LiveIndexWriterConfig;

    invoke-virtual {v0}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getAnalyzer()Lorg/apache/lucene/analysis/Analyzer;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    .line 633
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->config:Lorg/apache/lucene/index/LiveIndexWriterConfig;

    invoke-virtual {v0}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getInfoStream()Lorg/apache/lucene/util/InfoStream;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    .line 634
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->config:Lorg/apache/lucene/index/LiveIndexWriterConfig;

    invoke-virtual {v0}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getMergePolicy()Lorg/apache/lucene/index/MergePolicy;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->mergePolicy:Lorg/apache/lucene/index/MergePolicy;

    .line 635
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->mergePolicy:Lorg/apache/lucene/index/MergePolicy;

    invoke-virtual {v0, p0}, Lorg/apache/lucene/index/MergePolicy;->setIndexWriter(Lorg/apache/lucene/index/IndexWriter;)V

    .line 636
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->config:Lorg/apache/lucene/index/LiveIndexWriterConfig;

    invoke-virtual {v0}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getMergeScheduler()Lorg/apache/lucene/index/MergeScheduler;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->mergeScheduler:Lorg/apache/lucene/index/MergeScheduler;

    .line 637
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->config:Lorg/apache/lucene/index/LiveIndexWriterConfig;

    invoke-virtual {v0}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getCodec()Lorg/apache/lucene/codecs/Codec;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->codec:Lorg/apache/lucene/codecs/Codec;

    .line 639
    new-instance v0, Lorg/apache/lucene/index/BufferedDeletesStream;

    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/BufferedDeletesStream;-><init>(Lorg/apache/lucene/util/InfoStream;)V

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->bufferedDeletesStream:Lorg/apache/lucene/index/BufferedDeletesStream;

    .line 640
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->config:Lorg/apache/lucene/index/LiveIndexWriterConfig;

    invoke-virtual {v0}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getReaderPooling()Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/index/IndexWriter;->poolReaders:Z

    .line 642
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    const-string/jumbo v1, "write.lock"

    invoke-virtual {v0, v1}, Lorg/apache/lucene/store/Directory;->makeLock(Ljava/lang/String;)Lorg/apache/lucene/store/Lock;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->writeLock:Lorg/apache/lucene/store/Lock;

    .line 644
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->writeLock:Lorg/apache/lucene/store/Lock;

    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->config:Lorg/apache/lucene/index/LiveIndexWriterConfig;

    invoke-virtual {v1}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getWriteLockTimeout()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/store/Lock;->obtain(J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 645
    new-instance v0, Lorg/apache/lucene/store/LockObtainFailedException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Index locked for write: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->writeLock:Lorg/apache/lucene/store/Lock;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/lucene/store/LockObtainFailedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 647
    :cond_0
    const/4 v13, 0x0

    .line 649
    .local v13, "success":Z
    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->config:Lorg/apache/lucene/index/LiveIndexWriterConfig;

    invoke-virtual {v0}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getOpenMode()Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;

    move-result-object v11

    .line 651
    .local v11, "mode":Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;
    sget-object v0, Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;->CREATE:Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;

    if-ne v11, v0, :cond_6

    .line 652
    const/4 v8, 0x1

    .line 662
    .local v8, "create":Z
    :goto_0
    new-instance v0, Lorg/apache/lucene/index/SegmentInfos;

    invoke-direct {v0}, Lorg/apache/lucene/index/SegmentInfos;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 664
    const/4 v10, 0x1

    .line 666
    .local v10, "initialIndexExists":Z
    if-eqz v8, :cond_9

    .line 672
    :try_start_1
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/SegmentInfos;->read(Lorg/apache/lucene/store/Directory;)V

    .line 673
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentInfos;->clear()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 681
    :goto_1
    :try_start_2
    iget-wide v0, p0, Lorg/apache/lucene/index/IndexWriter;->changeCount:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/lucene/index/IndexWriter;->changeCount:J

    .line 682
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentInfos;->changed()V

    .line 706
    :cond_1
    :goto_2
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentInfos;->createBackupSegmentInfos()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->rollbackSegments:Ljava/util/List;

    .line 709
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexWriter;->getFieldNumberMap()Lorg/apache/lucene/index/FieldInfos$FieldNumbers;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->globalFieldNumberMap:Lorg/apache/lucene/index/FieldInfos$FieldNumbers;

    .line 710
    new-instance v0, Lorg/apache/lucene/index/DocumentsWriter;

    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->codec:Lorg/apache/lucene/codecs/Codec;

    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->config:Lorg/apache/lucene/index/LiveIndexWriterConfig;

    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->globalFieldNumberMap:Lorg/apache/lucene/index/FieldInfos$FieldNumbers;

    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->bufferedDeletesStream:Lorg/apache/lucene/index/BufferedDeletesStream;

    move-object v4, p0

    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/index/DocumentsWriter;-><init>(Lorg/apache/lucene/codecs/Codec;Lorg/apache/lucene/index/LiveIndexWriterConfig;Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexWriter;Lorg/apache/lucene/index/FieldInfos$FieldNumbers;Lorg/apache/lucene/index/BufferedDeletesStream;)V

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    .line 714
    monitor-enter p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 715
    :try_start_3
    new-instance v0, Lorg/apache/lucene/index/IndexFileDeleter;

    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    .line 716
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->config:Lorg/apache/lucene/index/LiveIndexWriterConfig;

    invoke-virtual {v2}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getIndexDeletionPolicy()Lorg/apache/lucene/index/IndexDeletionPolicy;

    move-result-object v2

    .line 717
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    move-object v5, p0

    move v6, v10

    .line 718
    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/index/IndexFileDeleter;-><init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexDeletionPolicy;Lorg/apache/lucene/index/SegmentInfos;Lorg/apache/lucene/util/InfoStream;Lorg/apache/lucene/index/IndexWriter;Z)V

    .line 715
    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    .line 714
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 721
    :try_start_4
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    iget-boolean v0, v0, Lorg/apache/lucene/index/IndexFileDeleter;->startingCommitDeleted:Z

    if-eqz v0, :cond_2

    .line 726
    iget-wide v0, p0, Lorg/apache/lucene/index/IndexWriter;->changeCount:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/lucene/index/IndexWriter;->changeCount:J

    .line 727
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentInfos;->changed()V

    .line 730
    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v1, "IW"

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 731
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v1, "IW"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "init: create="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 732
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexWriter;->messageState()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 735
    :cond_3
    const/4 v13, 0x1

    .line 738
    if-nez v13, :cond_5

    .line 739
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v1, "IW"

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 740
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v1, "IW"

    const-string v2, "init: hit exception on init; releasing write lock"

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 743
    :cond_4
    :try_start_5
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->writeLock:Lorg/apache/lucene/store/Lock;

    invoke-virtual {v0}, Lorg/apache/lucene/store/Lock;->release()V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1

    .line 747
    :goto_3
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->writeLock:Lorg/apache/lucene/store/Lock;

    .line 750
    :cond_5
    return-void

    .line 653
    .end local v8    # "create":Z
    .end local v10    # "initialIndexExists":Z
    :cond_6
    :try_start_6
    sget-object v0, Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;->APPEND:Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;

    if-ne v11, v0, :cond_7

    .line 654
    const/4 v8, 0x0

    .line 655
    .restart local v8    # "create":Z
    goto/16 :goto_0

    .line 657
    .end local v8    # "create":Z
    :cond_7
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-static {v0}, Lorg/apache/lucene/index/DirectoryReader;->indexExists(Lorg/apache/lucene/store/Directory;)Z

    move-result v0

    if-eqz v0, :cond_8

    const/4 v8, 0x0

    .restart local v8    # "create":Z
    :goto_4
    goto/16 :goto_0

    .end local v8    # "create":Z
    :cond_8
    const/4 v8, 0x1

    goto :goto_4

    .line 674
    .restart local v8    # "create":Z
    .restart local v10    # "initialIndexExists":Z
    :catch_0
    move-exception v9

    .line 676
    .local v9, "e":Ljava/io/IOException;
    const/4 v10, 0x0

    goto/16 :goto_1

    .line 684
    .end local v9    # "e":Ljava/io/IOException;
    :cond_9
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/SegmentInfos;->read(Lorg/apache/lucene/store/Directory;)V

    .line 686
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->config:Lorg/apache/lucene/index/LiveIndexWriterConfig;

    invoke-virtual {v0}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getIndexCommit()Lorg/apache/lucene/index/IndexCommit;

    move-result-object v7

    .line 687
    .local v7, "commit":Lorg/apache/lucene/index/IndexCommit;
    if-eqz v7, :cond_1

    .line 693
    invoke-virtual {v7}, Lorg/apache/lucene/index/IndexCommit;->getDirectory()Lorg/apache/lucene/store/Directory;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    if-eq v0, v1, :cond_c

    .line 694
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "IndexCommit\'s directory doesn\'t match my directory"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 737
    .end local v7    # "commit":Lorg/apache/lucene/index/IndexCommit;
    .end local v8    # "create":Z
    .end local v10    # "initialIndexExists":Z
    .end local v11    # "mode":Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;
    :catchall_0
    move-exception v0

    .line 738
    if-nez v13, :cond_b

    .line 739
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v2, "IW"

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 740
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v2, "IW"

    const-string v3, "init: hit exception on init; releasing write lock"

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 743
    :cond_a
    :try_start_7
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->writeLock:Lorg/apache/lucene/store/Lock;

    invoke-virtual {v1}, Lorg/apache/lucene/store/Lock;->release()V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_2

    .line 747
    :goto_5
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->writeLock:Lorg/apache/lucene/store/Lock;

    .line 749
    :cond_b
    throw v0

    .line 695
    .restart local v7    # "commit":Lorg/apache/lucene/index/IndexCommit;
    .restart local v8    # "create":Z
    .restart local v10    # "initialIndexExists":Z
    .restart local v11    # "mode":Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;
    :cond_c
    :try_start_8
    new-instance v12, Lorg/apache/lucene/index/SegmentInfos;

    invoke-direct {v12}, Lorg/apache/lucene/index/SegmentInfos;-><init>()V

    .line 696
    .local v12, "oldInfos":Lorg/apache/lucene/index/SegmentInfos;
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v7}, Lorg/apache/lucene/index/IndexCommit;->getSegmentsFileName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v12, v0, v1}, Lorg/apache/lucene/index/SegmentInfos;->read(Lorg/apache/lucene/store/Directory;Ljava/lang/String;)V

    .line 697
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v0, v12}, Lorg/apache/lucene/index/SegmentInfos;->replace(Lorg/apache/lucene/index/SegmentInfos;)V

    .line 698
    iget-wide v0, p0, Lorg/apache/lucene/index/IndexWriter;->changeCount:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/lucene/index/IndexWriter;->changeCount:J

    .line 699
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentInfos;->changed()V

    .line 700
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v1, "IW"

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 701
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v1, "IW"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "init: loaded commit \""

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Lorg/apache/lucene/index/IndexCommit;->getSegmentsFileName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_2

    .line 714
    .end local v7    # "commit":Lorg/apache/lucene/index/IndexCommit;
    .end local v12    # "oldInfos":Lorg/apache/lucene/index/SegmentInfos;
    :catchall_1
    move-exception v0

    :try_start_9
    monitor-exit p0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    :try_start_a
    throw v0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 744
    :catch_1
    move-exception v0

    goto/16 :goto_3

    .end local v8    # "create":Z
    .end local v10    # "initialIndexExists":Z
    .end local v11    # "mode":Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;
    :catch_2
    move-exception v1

    goto :goto_5
.end method

.method private declared-synchronized _mergeInit(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V
    .locals 12
    .param p1, "merge"    # Lorg/apache/lucene/index/MergePolicy$OneMerge;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, -0x1

    .line 3465
    monitor-enter p0

    :try_start_0
    sget-boolean v1, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    const-string v1, "startMergeInit"

    invoke-virtual {p0, v1}, Lorg/apache/lucene/index/IndexWriter;->testPoint(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 3467
    :cond_0
    :try_start_1
    sget-boolean v1, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    iget-boolean v1, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->registerDone:Z

    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 3468
    :cond_1
    sget-boolean v1, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v1, :cond_2

    iget v1, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->maxNumSegments:I

    if-eq v1, v2, :cond_2

    iget v1, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->maxNumSegments:I

    if-gtz v1, :cond_2

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 3470
    :cond_2
    iget-boolean v1, p0, Lorg/apache/lucene/index/IndexWriter;->hitOOM:Z

    if-eqz v1, :cond_3

    .line 3471
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "this writer hit an OutOfMemoryError; cannot merge"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 3474
    :cond_3
    iget-object v1, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v1, :cond_5

    .line 3527
    :cond_4
    :goto_0
    monitor-exit p0

    return-void

    .line 3479
    :cond_5
    :try_start_2
    invoke-virtual {p1}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->isAborted()Z

    move-result v1

    if-nez v1, :cond_4

    .line 3489
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->bufferedDeletesStream:Lorg/apache/lucene/index/BufferedDeletesStream;

    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->readerPool:Lorg/apache/lucene/index/IndexWriter$ReaderPool;

    iget-object v4, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    invoke-virtual {v1, v2, v4}, Lorg/apache/lucene/index/BufferedDeletesStream;->applyDeletes(Lorg/apache/lucene/index/IndexWriter$ReaderPool;Ljava/util/List;)Lorg/apache/lucene/index/BufferedDeletesStream$ApplyDeletesResult;

    move-result-object v11

    .line 3491
    .local v11, "result":Lorg/apache/lucene/index/BufferedDeletesStream$ApplyDeletesResult;
    iget-boolean v1, v11, Lorg/apache/lucene/index/BufferedDeletesStream$ApplyDeletesResult;->anyDeletes:Z

    if-eqz v1, :cond_6

    .line 3492
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->checkpoint()V

    .line 3495
    :cond_6
    iget-boolean v1, p0, Lorg/apache/lucene/index/IndexWriter;->keepFullyDeletedSegments:Z

    if-nez v1, :cond_8

    iget-object v1, v11, Lorg/apache/lucene/index/BufferedDeletesStream$ApplyDeletesResult;->allDeleted:Ljava/util/List;

    if-eqz v1, :cond_8

    .line 3496
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v2, "IW"

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 3497
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v2, "IW"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "drop 100% deleted segments: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v11, Lorg/apache/lucene/index/BufferedDeletesStream$ApplyDeletesResult;->allDeleted:Ljava/util/List;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 3499
    :cond_7
    iget-object v1, v11, Lorg/apache/lucene/index/BufferedDeletesStream$ApplyDeletesResult;->allDeleted:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_9

    .line 3507
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->checkpoint()V

    .line 3513
    :cond_8
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->newSegmentName()Ljava/lang/String;

    move-result-object v3

    .line 3514
    .local v3, "mergeSegmentName":Ljava/lang/String;
    new-instance v0, Lorg/apache/lucene/index/SegmentInfo;

    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    sget-object v2, Lorg/apache/lucene/util/Constants;->LUCENE_MAIN_VERSION:Ljava/lang/String;

    const/4 v4, -0x1

    const/4 v5, 0x0

    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->codec:Lorg/apache/lucene/codecs/Codec;

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Lorg/apache/lucene/index/SegmentInfo;-><init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Ljava/lang/String;IZLorg/apache/lucene/codecs/Codec;Ljava/util/Map;Ljava/util/Map;)V

    .line 3515
    .local v0, "si":Lorg/apache/lucene/index/SegmentInfo;
    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    .line 3516
    .local v9, "details":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v1, "mergeMaxNumSegments"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v4, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->maxNumSegments:I

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v9, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3517
    const-string v1, "mergeFactor"

    iget-object v2, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v9, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3518
    const-string v1, "merge"

    invoke-static {v0, v1, v9}, Lorg/apache/lucene/index/IndexWriter;->setDiagnostics(Lorg/apache/lucene/index/SegmentInfo;Ljava/lang/String;Ljava/util/Map;)V

    .line 3519
    new-instance v1, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    const/4 v2, 0x0

    const-wide/16 v4, -0x1

    invoke-direct {v1, v0, v2, v4, v5}, Lorg/apache/lucene/index/SegmentInfoPerCommit;-><init>(Lorg/apache/lucene/index/SegmentInfo;IJ)V

    invoke-virtual {p1, v1}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->setInfo(Lorg/apache/lucene/index/SegmentInfoPerCommit;)V

    .line 3522
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->bufferedDeletesStream:Lorg/apache/lucene/index/BufferedDeletesStream;

    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/index/BufferedDeletesStream;->prune(Lorg/apache/lucene/index/SegmentInfos;)V

    .line 3524
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v2, "IW"

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 3525
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v2, "IW"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "merge seg="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    iget-object v5, v5, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v5, v5, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    invoke-virtual {p0, v5}, Lorg/apache/lucene/index/IndexWriter;->segString(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 3499
    .end local v0    # "si":Lorg/apache/lucene/index/SegmentInfo;
    .end local v3    # "mergeSegmentName":Ljava/lang/String;
    .end local v9    # "details":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_9
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .line 3500
    .local v10, "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v2, v10}, Lorg/apache/lucene/index/SegmentInfos;->remove(Lorg/apache/lucene/index/SegmentInfoPerCommit;)V

    .line 3501
    iget-object v2, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    invoke-interface {v2, v10}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 3502
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->mergingSegments:Ljava/util/HashSet;

    invoke-virtual {v2, v10}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 3503
    iget-object v2, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    invoke-interface {v2, v10}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 3505
    :cond_a
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->readerPool:Lorg/apache/lucene/index/IndexWriter$ReaderPool;

    invoke-virtual {v2, v10}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->drop(Lorg/apache/lucene/index/SegmentInfoPerCommit;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1
.end method

.method static synthetic access$0(Lorg/apache/lucene/index/IndexWriter;)Z
    .locals 1

    .prologue
    .line 270
    iget-boolean v0, p0, Lorg/apache/lucene/index/IndexWriter;->poolReaders:Z

    return v0
.end method

.method static synthetic access$1(Lorg/apache/lucene/index/IndexWriter;)Lorg/apache/lucene/store/Directory;
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    return-object v0
.end method

.method private closeInternal(ZZ)V
    .locals 10
    .param p1, "waitForMerges"    # Z
    .param p2, "doFlush"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 923
    const/4 v0, 0x0

    .line 926
    .local v0, "interrupted":Z
    :try_start_0
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->pendingCommit:Lorg/apache/lucene/index/SegmentInfos;

    if-eqz v4, :cond_2

    .line 927
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "cannot close: prepareCommit was already called with no corresponding call to commit"

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1015
    :catch_0
    move-exception v2

    .line 1016
    .local v2, "oom":Ljava/lang/OutOfMemoryError;
    :try_start_1
    const-string v4, "closeInternal"

    invoke-direct {p0, v2, v4}, Lorg/apache/lucene/index/IndexWriter;->handleOOM(Ljava/lang/OutOfMemoryError;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1018
    monitor-enter p0

    .line 1019
    const/4 v4, 0x0

    :try_start_2
    iput-boolean v4, p0, Lorg/apache/lucene/index/IndexWriter;->closing:Z

    .line 1020
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 1021
    iget-boolean v4, p0, Lorg/apache/lucene/index/IndexWriter;->closed:Z

    if-nez v4, :cond_0

    .line 1022
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v5, "IW"

    invoke-virtual {v4, v5}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1023
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v5, "IW"

    const-string v6, "hit exception while closing"

    invoke-virtual {v4, v5, v6}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 1018
    :cond_0
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_8

    .line 1028
    if-eqz v0, :cond_1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Thread;->interrupt()V

    .line 1030
    .end local v2    # "oom":Ljava/lang/OutOfMemoryError;
    :cond_1
    :goto_0
    return-void

    .line 930
    :cond_2
    :try_start_3
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v7, "IW"

    invoke-virtual {v4, v7}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 931
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v7, "IW"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "now flush at close waitForMerges="

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 934
    :cond_3
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v4}, Lorg/apache/lucene/index/DocumentsWriter;->close()V
    :try_end_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 939
    if-eqz p2, :cond_c

    .line 940
    const/4 v4, 0x1

    :try_start_4
    invoke-virtual {p0, p1, v4}, Lorg/apache/lucene/index/IndexWriter;->flush(ZZ)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 948
    :goto_1
    :try_start_5
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    move-result v0

    .line 950
    if-eqz p1, :cond_4

    .line 954
    :try_start_6
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->mergeScheduler:Lorg/apache/lucene/index/MergeScheduler;

    invoke-virtual {v4, p0}, Lorg/apache/lucene/index/MergeScheduler;->merge(Lorg/apache/lucene/index/IndexWriter;)V
    :try_end_6
    .catch Lorg/apache/lucene/util/ThreadInterruptedException; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    .line 964
    :cond_4
    :goto_2
    :try_start_7
    monitor-enter p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    .line 967
    :cond_5
    :goto_3
    if-eqz p1, :cond_10

    if-nez v0, :cond_10

    move v4, v5

    :goto_4
    :try_start_8
    invoke-direct {p0, v4}, Lorg/apache/lucene/index/IndexWriter;->finishMerges(Z)V
    :try_end_8
    .catch Lorg/apache/lucene/util/ThreadInterruptedException; {:try_start_8 .. :try_end_8} :catch_4
    .catchall {:try_start_8 .. :try_end_8} :catchall_5

    .line 979
    const/4 v4, 0x1

    :try_start_9
    iput-boolean v4, p0, Lorg/apache/lucene/index/IndexWriter;->stopMerges:Z

    .line 964
    monitor-exit p0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_5

    .line 982
    const/4 v4, 0x2

    :try_start_a
    new-array v4, v4, [Ljava/io/Closeable;

    const/4 v5, 0x0

    .line 984
    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->mergePolicy:Lorg/apache/lucene/index/MergePolicy;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->mergeScheduler:Lorg/apache/lucene/index/MergeScheduler;

    aput-object v6, v4, v5

    invoke-static {v4}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 988
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v5, "IW"

    invoke-virtual {v4, v5}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 989
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v5, "IW"

    const-string v6, "now call final commit()"

    invoke-virtual {v4, v5, v6}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 992
    :cond_6
    if-eqz p2, :cond_7

    .line 993
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexWriter;->commitInternal()V

    .line 996
    :cond_7
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v5, "IW"

    invoke-virtual {v4, v5}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 997
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v5, "IW"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "at close: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->segString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 1000
    :cond_8
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    .line 1001
    .local v1, "oldWriter":Lorg/apache/lucene/index/DocumentsWriter;
    monitor-enter p0
    :try_end_a
    .catch Ljava/lang/OutOfMemoryError; {:try_start_a .. :try_end_a} :catch_0
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 1002
    :try_start_b
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->readerPool:Lorg/apache/lucene/index/IndexWriter$ReaderPool;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->dropAll(Z)V

    .line 1003
    const/4 v4, 0x0

    iput-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    .line 1004
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    invoke-virtual {v4}, Lorg/apache/lucene/index/IndexFileDeleter;->close()V

    .line 1001
    monitor-exit p0
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_6

    .line 1007
    :try_start_c
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->writeLock:Lorg/apache/lucene/store/Lock;

    if-eqz v4, :cond_9

    .line 1008
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->writeLock:Lorg/apache/lucene/store/Lock;

    invoke-virtual {v4}, Lorg/apache/lucene/store/Lock;->release()V

    .line 1009
    const/4 v4, 0x0

    iput-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->writeLock:Lorg/apache/lucene/store/Lock;

    .line 1011
    :cond_9
    monitor-enter p0
    :try_end_c
    .catch Ljava/lang/OutOfMemoryError; {:try_start_c .. :try_end_c} :catch_0
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    .line 1012
    const/4 v4, 0x1

    :try_start_d
    iput-boolean v4, p0, Lorg/apache/lucene/index/IndexWriter;->closed:Z

    .line 1011
    monitor-exit p0
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_7

    .line 1014
    :try_start_e
    sget-boolean v4, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v4, :cond_11

    iget-object v4, v1, Lorg/apache/lucene/index/DocumentsWriter;->perThreadPool:Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;

    invoke-virtual {v4}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->numDeactivatedThreadStates()I

    move-result v4

    iget-object v5, v1, Lorg/apache/lucene/index/DocumentsWriter;->perThreadPool:Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;

    invoke-virtual {v5}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->getMaxThreadStates()I

    move-result v5

    if-eq v4, v5, :cond_11

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4
    :try_end_e
    .catch Ljava/lang/OutOfMemoryError; {:try_start_e .. :try_end_e} :catch_0
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    .line 1017
    .end local v1    # "oldWriter":Lorg/apache/lucene/index/DocumentsWriter;
    :catchall_0
    move-exception v4

    .line 1018
    monitor-enter p0

    .line 1019
    const/4 v5, 0x0

    :try_start_f
    iput-boolean v5, p0, Lorg/apache/lucene/index/IndexWriter;->closing:Z

    .line 1020
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 1021
    iget-boolean v5, p0, Lorg/apache/lucene/index/IndexWriter;->closed:Z

    if-nez v5, :cond_a

    .line 1022
    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v6, "IW"

    invoke-virtual {v5, v6}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 1023
    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v6, "IW"

    const-string v7, "hit exception while closing"

    invoke-virtual {v5, v6, v7}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 1018
    :cond_a
    monitor-exit p0
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_9

    .line 1028
    if-eqz v0, :cond_b

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Thread;->interrupt()V

    .line 1029
    :cond_b
    throw v4

    .line 942
    :cond_c
    :try_start_10
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v4}, Lorg/apache/lucene/index/DocumentsWriter;->abort()V
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    goto/16 :goto_1

    .line 945
    :catchall_1
    move-exception v4

    .line 948
    :try_start_11
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_2

    move-result v0

    .line 950
    if-eqz p1, :cond_d

    .line 954
    :try_start_12
    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->mergeScheduler:Lorg/apache/lucene/index/MergeScheduler;

    invoke-virtual {v7, p0}, Lorg/apache/lucene/index/MergeScheduler;->merge(Lorg/apache/lucene/index/IndexWriter;)V
    :try_end_12
    .catch Lorg/apache/lucene/util/ThreadInterruptedException; {:try_start_12 .. :try_end_12} :catch_1
    .catchall {:try_start_12 .. :try_end_12} :catchall_2

    .line 964
    :cond_d
    :goto_5
    :try_start_13
    monitor-enter p0
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_2

    .line 967
    :cond_e
    :goto_6
    if-eqz p1, :cond_f

    if-nez v0, :cond_f

    move v7, v5

    :goto_7
    :try_start_14
    invoke-direct {p0, v7}, Lorg/apache/lucene/index/IndexWriter;->finishMerges(Z)V
    :try_end_14
    .catch Lorg/apache/lucene/util/ThreadInterruptedException; {:try_start_14 .. :try_end_14} :catch_2
    .catchall {:try_start_14 .. :try_end_14} :catchall_3

    .line 979
    const/4 v5, 0x1

    :try_start_15
    iput-boolean v5, p0, Lorg/apache/lucene/index/IndexWriter;->stopMerges:Z

    .line 964
    monitor-exit p0
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_3

    .line 982
    const/4 v5, 0x2

    :try_start_16
    new-array v5, v5, [Ljava/io/Closeable;

    const/4 v6, 0x0

    .line 984
    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->mergePolicy:Lorg/apache/lucene/index/MergePolicy;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->mergeScheduler:Lorg/apache/lucene/index/MergeScheduler;

    aput-object v7, v5, v6

    invoke-static {v5}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 986
    throw v4
    :try_end_16
    .catch Ljava/lang/OutOfMemoryError; {:try_start_16 .. :try_end_16} :catch_0
    .catchall {:try_start_16 .. :try_end_16} :catchall_0

    .line 955
    :catch_1
    move-exception v3

    .line 957
    .local v3, "tie":Lorg/apache/lucene/util/ThreadInterruptedException;
    const/4 v0, 0x1

    .line 958
    :try_start_17
    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v8, "IW"

    invoke-virtual {v7, v8}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_d

    .line 959
    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v8, "IW"

    const-string v9, "interrupted while waiting for final merges"

    invoke-virtual {v7, v8, v9}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_2

    goto :goto_5

    .line 982
    .end local v3    # "tie":Lorg/apache/lucene/util/ThreadInterruptedException;
    :catchall_2
    move-exception v4

    const/4 v5, 0x2

    :try_start_18
    new-array v5, v5, [Ljava/io/Closeable;

    const/4 v6, 0x0

    .line 984
    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->mergePolicy:Lorg/apache/lucene/index/MergePolicy;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->mergeScheduler:Lorg/apache/lucene/index/MergeScheduler;

    aput-object v7, v5, v6

    invoke-static {v5}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 985
    throw v4
    :try_end_18
    .catch Ljava/lang/OutOfMemoryError; {:try_start_18 .. :try_end_18} :catch_0
    .catchall {:try_start_18 .. :try_end_18} :catchall_0

    :cond_f
    move v7, v6

    .line 967
    goto :goto_7

    .line 969
    :catch_2
    move-exception v3

    .line 973
    .restart local v3    # "tie":Lorg/apache/lucene/util/ThreadInterruptedException;
    const/4 v0, 0x1

    .line 974
    :try_start_19
    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v8, "IW"

    invoke-virtual {v7, v8}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_e

    .line 975
    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v8, "IW"

    const-string v9, "interrupted while waiting for merges to finish"

    invoke-virtual {v7, v8, v9}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    .line 964
    .end local v3    # "tie":Lorg/apache/lucene/util/ThreadInterruptedException;
    :catchall_3
    move-exception v4

    monitor-exit p0
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_3

    :try_start_1a
    throw v4
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_2

    .line 955
    :catch_3
    move-exception v3

    .line 957
    .restart local v3    # "tie":Lorg/apache/lucene/util/ThreadInterruptedException;
    const/4 v0, 0x1

    .line 958
    :try_start_1b
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v7, "IW"

    invoke-virtual {v4, v7}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 959
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v7, "IW"

    const-string v8, "interrupted while waiting for final merges"

    invoke-virtual {v4, v7, v8}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1b
    .catchall {:try_start_1b .. :try_end_1b} :catchall_4

    goto/16 :goto_2

    .line 982
    .end local v3    # "tie":Lorg/apache/lucene/util/ThreadInterruptedException;
    :catchall_4
    move-exception v4

    const/4 v5, 0x2

    :try_start_1c
    new-array v5, v5, [Ljava/io/Closeable;

    const/4 v6, 0x0

    .line 984
    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->mergePolicy:Lorg/apache/lucene/index/MergePolicy;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->mergeScheduler:Lorg/apache/lucene/index/MergeScheduler;

    aput-object v7, v5, v6

    invoke-static {v5}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 985
    throw v4
    :try_end_1c
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1c .. :try_end_1c} :catch_0
    .catchall {:try_start_1c .. :try_end_1c} :catchall_0

    :cond_10
    move v4, v6

    .line 967
    goto/16 :goto_4

    .line 969
    :catch_4
    move-exception v3

    .line 973
    .restart local v3    # "tie":Lorg/apache/lucene/util/ThreadInterruptedException;
    const/4 v0, 0x1

    .line 974
    :try_start_1d
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v7, "IW"

    invoke-virtual {v4, v7}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 975
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v7, "IW"

    const-string v8, "interrupted while waiting for merges to finish"

    invoke-virtual {v4, v7, v8}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 964
    .end local v3    # "tie":Lorg/apache/lucene/util/ThreadInterruptedException;
    :catchall_5
    move-exception v4

    monitor-exit p0
    :try_end_1d
    .catchall {:try_start_1d .. :try_end_1d} :catchall_5

    :try_start_1e
    throw v4
    :try_end_1e
    .catchall {:try_start_1e .. :try_end_1e} :catchall_4

    .line 1001
    .restart local v1    # "oldWriter":Lorg/apache/lucene/index/DocumentsWriter;
    :catchall_6
    move-exception v4

    :try_start_1f
    monitor-exit p0
    :try_end_1f
    .catchall {:try_start_1f .. :try_end_1f} :catchall_6

    :try_start_20
    throw v4
    :try_end_20
    .catch Ljava/lang/OutOfMemoryError; {:try_start_20 .. :try_end_20} :catch_0
    .catchall {:try_start_20 .. :try_end_20} :catchall_0

    .line 1011
    :catchall_7
    move-exception v4

    :try_start_21
    monitor-exit p0
    :try_end_21
    .catchall {:try_start_21 .. :try_end_21} :catchall_7

    :try_start_22
    throw v4
    :try_end_22
    .catch Ljava/lang/OutOfMemoryError; {:try_start_22 .. :try_end_22} :catch_0
    .catchall {:try_start_22 .. :try_end_22} :catchall_0

    .line 1018
    .end local v1    # "oldWriter":Lorg/apache/lucene/index/DocumentsWriter;
    .restart local v2    # "oom":Ljava/lang/OutOfMemoryError;
    :catchall_8
    move-exception v4

    :try_start_23
    monitor-exit p0
    :try_end_23
    .catchall {:try_start_23 .. :try_end_23} :catchall_8

    throw v4

    .end local v2    # "oom":Ljava/lang/OutOfMemoryError;
    :catchall_9
    move-exception v4

    :try_start_24
    monitor-exit p0
    :try_end_24
    .catchall {:try_start_24 .. :try_end_24} :catchall_9

    throw v4

    .restart local v1    # "oldWriter":Lorg/apache/lucene/index/DocumentsWriter;
    :cond_11
    monitor-enter p0

    .line 1019
    const/4 v4, 0x0

    :try_start_25
    iput-boolean v4, p0, Lorg/apache/lucene/index/IndexWriter;->closing:Z

    .line 1020
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 1021
    iget-boolean v4, p0, Lorg/apache/lucene/index/IndexWriter;->closed:Z

    if-nez v4, :cond_12

    .line 1022
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v5, "IW"

    invoke-virtual {v4, v5}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_12

    .line 1023
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v5, "IW"

    const-string v6, "hit exception while closing"

    invoke-virtual {v4, v5, v6}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 1018
    :cond_12
    monitor-exit p0
    :try_end_25
    .catchall {:try_start_25 .. :try_end_25} :catchall_a

    .line 1028
    if-eqz v0, :cond_1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Thread;->interrupt()V

    goto/16 :goto_0

    .line 1018
    :catchall_a
    move-exception v4

    :try_start_26
    monitor-exit p0
    :try_end_26
    .catchall {:try_start_26 .. :try_end_26} :catchall_a

    throw v4
.end method

.method private final declared-synchronized closeMergeReaders(Lorg/apache/lucene/index/MergePolicy$OneMerge;Z)V
    .locals 10
    .param p1, "merge"    # Lorg/apache/lucene/index/MergePolicy$OneMerge;
    .param p2, "suppressExceptions"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 3571
    monitor-enter p0

    :try_start_0
    iget-object v7, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->readers:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v2

    .line 3572
    .local v2, "numSegments":I
    const/4 v6, 0x0

    .line 3574
    .local v6, "th":Ljava/lang/Throwable;
    if-eqz p2, :cond_0

    .line 3576
    .local v0, "drop":Z
    :goto_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-lt v1, v2, :cond_1

    .line 3601
    if-nez p2, :cond_9

    if-eqz v6, :cond_9

    .line 3602
    instance-of v7, v6, Ljava/io/IOException;

    if-eqz v7, :cond_6

    check-cast v6, Ljava/io/IOException;

    .end local v6    # "th":Ljava/lang/Throwable;
    throw v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3571
    .end local v0    # "drop":Z
    .end local v1    # "i":I
    .end local v2    # "numSegments":I
    :catchall_0
    move-exception v7

    monitor-exit p0

    throw v7

    .line 3574
    .restart local v2    # "numSegments":I
    .restart local v6    # "th":Ljava/lang/Throwable;
    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 3577
    .restart local v0    # "drop":Z
    .restart local v1    # "i":I
    :cond_1
    :try_start_1
    iget-object v7, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->readers:Ljava/util/List;

    invoke-interface {v7, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/index/SegmentReader;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3578
    .local v4, "sr":Lorg/apache/lucene/index/SegmentReader;
    if-eqz v4, :cond_3

    .line 3580
    :try_start_2
    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->readerPool:Lorg/apache/lucene/index/IndexWriter$ReaderPool;

    invoke-virtual {v4}, Lorg/apache/lucene/index/SegmentReader;->getSegmentInfo()Lorg/apache/lucene/index/SegmentInfoPerCommit;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->get(Lorg/apache/lucene/index/SegmentInfoPerCommit;Z)Lorg/apache/lucene/index/ReadersAndLiveDocs;

    move-result-object v3

    .line 3582
    .local v3, "rld":Lorg/apache/lucene/index/ReadersAndLiveDocs;
    sget-boolean v7, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v7, :cond_4

    if-nez v3, :cond_4

    new-instance v7, Ljava/lang/AssertionError;

    invoke-direct {v7}, Ljava/lang/AssertionError;-><init>()V

    throw v7
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 3591
    .end local v3    # "rld":Lorg/apache/lucene/index/ReadersAndLiveDocs;
    :catch_0
    move-exception v5

    .line 3592
    .local v5, "t":Ljava/lang/Throwable;
    if-nez v6, :cond_2

    .line 3593
    move-object v6, v5

    .line 3596
    .end local v5    # "t":Ljava/lang/Throwable;
    :cond_2
    :goto_2
    :try_start_3
    iget-object v7, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->readers:Ljava/util/List;

    const/4 v8, 0x0

    invoke-interface {v7, v1, v8}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 3576
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 3583
    .restart local v3    # "rld":Lorg/apache/lucene/index/ReadersAndLiveDocs;
    :cond_4
    if-eqz v0, :cond_5

    .line 3584
    :try_start_4
    invoke-virtual {v3}, Lorg/apache/lucene/index/ReadersAndLiveDocs;->dropChanges()V

    .line 3586
    :cond_5
    invoke-virtual {v3, v4}, Lorg/apache/lucene/index/ReadersAndLiveDocs;->release(Lorg/apache/lucene/index/SegmentReader;)V

    .line 3587
    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->readerPool:Lorg/apache/lucene/index/IndexWriter$ReaderPool;

    invoke-virtual {v7, v3}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->release(Lorg/apache/lucene/index/ReadersAndLiveDocs;)V

    .line 3588
    if-eqz v0, :cond_2

    .line 3589
    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->readerPool:Lorg/apache/lucene/index/IndexWriter$ReaderPool;

    iget-object v8, v3, Lorg/apache/lucene/index/ReadersAndLiveDocs;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    invoke-virtual {v7, v8}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->drop(Lorg/apache/lucene/index/SegmentInfoPerCommit;)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    .line 3603
    .end local v3    # "rld":Lorg/apache/lucene/index/ReadersAndLiveDocs;
    .end local v4    # "sr":Lorg/apache/lucene/index/SegmentReader;
    :cond_6
    :try_start_5
    instance-of v7, v6, Ljava/lang/RuntimeException;

    if-eqz v7, :cond_7

    check-cast v6, Ljava/lang/RuntimeException;

    .end local v6    # "th":Ljava/lang/Throwable;
    throw v6

    .line 3604
    .restart local v6    # "th":Ljava/lang/Throwable;
    :cond_7
    instance-of v7, v6, Ljava/lang/Error;

    if-eqz v7, :cond_8

    check-cast v6, Ljava/lang/Error;

    .end local v6    # "th":Ljava/lang/Throwable;
    throw v6

    .line 3605
    .restart local v6    # "th":Ljava/lang/Throwable;
    :cond_8
    new-instance v7, Ljava/lang/RuntimeException;

    invoke-direct {v7, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v7
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 3607
    :cond_9
    monitor-exit p0

    return-void
.end method

.method private final commitInternal()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2824
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v1, "IW"

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2825
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v1, "IW"

    const-string v2, "commit: start"

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 2828
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->commitLock:Ljava/lang/Object;

    monitor-enter v1

    .line 2829
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen(Z)V

    .line 2831
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v2, "IW"

    invoke-virtual {v0, v2}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2832
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v2, "IW"

    const-string v3, "commit: enter lock"

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 2835
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->pendingCommit:Lorg/apache/lucene/index/SegmentInfos;

    if-nez v0, :cond_4

    .line 2836
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v2, "IW"

    invoke-virtual {v0, v2}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2837
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v2, "IW"

    const-string v3, "commit: now prepare"

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 2839
    :cond_2
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexWriter;->prepareCommitInternal()V

    .line 2846
    :cond_3
    :goto_0
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexWriter;->finishCommit()V

    .line 2828
    monitor-exit v1

    .line 2848
    return-void

    .line 2841
    :cond_4
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v2, "IW"

    invoke-virtual {v0, v2}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2842
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v2, "IW"

    const-string v3, "commit: already prepared"

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2828
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private declared-synchronized commitMerge(Lorg/apache/lucene/index/MergePolicy$OneMerge;Lorg/apache/lucene/index/MergeState;)Z
    .locals 10
    .param p1, "merge"    # Lorg/apache/lucene/index/MergePolicy$OneMerge;
    .param p2, "mergeState"    # Lorg/apache/lucene/index/MergeState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 3148
    monitor-enter p0

    :try_start_0
    sget-boolean v6, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v6, :cond_0

    const-string v6, "startCommitMerge"

    invoke-virtual {p0, v6}, Lorg/apache/lucene/index/IndexWriter;->testPoint(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    .line 3150
    :cond_0
    :try_start_1
    iget-boolean v6, p0, Lorg/apache/lucene/index/IndexWriter;->hitOOM:Z

    if-eqz v6, :cond_1

    .line 3151
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "this writer hit an OutOfMemoryError; cannot complete merge"

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 3154
    :cond_1
    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v7, "IW"

    invoke-virtual {v6, v7}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 3155
    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v7, "IW"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "commitMerge: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v9, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    invoke-virtual {p0, v9}, Lorg/apache/lucene/index/IndexWriter;->segString(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " index="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->segString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 3158
    :cond_2
    sget-boolean v6, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v6, :cond_3

    iget-boolean v6, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->registerDone:Z

    if-nez v6, :cond_3

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 3166
    :cond_3
    invoke-virtual {p1}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->isAborted()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 3167
    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v6, "IW"

    invoke-virtual {v5, v6}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 3168
    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v6, "IW"

    const-string v7, "commitMerge: skip: it was aborted"

    invoke-virtual {v5, v6, v7}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 3170
    :cond_4
    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    iget-object v6, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    invoke-virtual {v6}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->files()Ljava/util/Collection;

    move-result-object v6

    invoke-virtual {v5, v6}, Lorg/apache/lucene/index/IndexFileDeleter;->deleteNewFiles(Ljava/util/Collection;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3254
    :goto_0
    monitor-exit p0

    return v4

    .line 3174
    :cond_5
    :try_start_2
    iget-object v6, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    iget-object v6, v6, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v6}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v6

    if-nez v6, :cond_6

    const/4 v2, 0x0

    .line 3176
    .local v2, "mergedDeletes":Lorg/apache/lucene/index/ReadersAndLiveDocs;
    :goto_1
    sget-boolean v6, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v6, :cond_7

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Lorg/apache/lucene/index/ReadersAndLiveDocs;->getPendingDeleteCount()I

    move-result v6

    if-nez v6, :cond_7

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 3174
    .end local v2    # "mergedDeletes":Lorg/apache/lucene/index/ReadersAndLiveDocs;
    :cond_6
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/index/IndexWriter;->commitMergedDeletes(Lorg/apache/lucene/index/MergePolicy$OneMerge;Lorg/apache/lucene/index/MergeState;)Lorg/apache/lucene/index/ReadersAndLiveDocs;

    move-result-object v2

    goto :goto_1

    .line 3183
    .restart local v2    # "mergedDeletes":Lorg/apache/lucene/index/ReadersAndLiveDocs;
    :cond_7
    sget-boolean v6, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v6, :cond_8

    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    iget-object v7, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    invoke-virtual {v6, v7}, Lorg/apache/lucene/index/SegmentInfos;->contains(Lorg/apache/lucene/index/SegmentInfoPerCommit;)Z

    move-result v6

    if-eqz v6, :cond_8

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 3185
    :cond_8
    iget-object v6, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-eqz v6, :cond_b

    .line 3186
    iget-object v6, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    iget-object v6, v6, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v6}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v6

    if-eqz v6, :cond_b

    .line 3187
    if-eqz v2, :cond_9

    .line 3188
    invoke-virtual {v2}, Lorg/apache/lucene/index/ReadersAndLiveDocs;->getPendingDeleteCount()I

    move-result v6

    iget-object v7, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    iget-object v7, v7, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v7}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v7

    if-eq v6, v7, :cond_b

    :cond_9
    move v0, v4

    .line 3190
    .local v0, "allDeleted":Z
    :goto_2
    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v7, "IW"

    invoke-virtual {v6, v7}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 3191
    if-eqz v0, :cond_a

    .line 3192
    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v8, "IW"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v9, "merged segment "

    invoke-direct {v6, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v9, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, " is 100% deleted"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-boolean v6, p0, Lorg/apache/lucene/index/IndexWriter;->keepFullyDeletedSegments:Z

    if-eqz v6, :cond_c

    const-string v6, ""

    :goto_3
    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v8, v6}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 3196
    :cond_a
    if-eqz v0, :cond_d

    iget-boolean v6, p0, Lorg/apache/lucene/index/IndexWriter;->keepFullyDeletedSegments:Z

    if-nez v6, :cond_d

    move v1, v5

    .line 3200
    .local v1, "dropSegment":Z
    :goto_4
    sget-boolean v4, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v4, :cond_e

    iget-object v4, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-gtz v4, :cond_e

    if-nez v1, :cond_e

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .end local v0    # "allDeleted":Z
    .end local v1    # "dropSegment":Z
    :cond_b
    move v0, v5

    .line 3185
    goto :goto_2

    .line 3192
    .restart local v0    # "allDeleted":Z
    :cond_c
    const-string v6, "; skipping insert"

    goto :goto_3

    :cond_d
    move v1, v4

    .line 3196
    goto :goto_4

    .line 3202
    .restart local v1    # "dropSegment":Z
    :cond_e
    sget-boolean v4, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v4, :cond_f

    iget-object v4, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    iget-object v4, v4, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v4}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v4

    if-nez v4, :cond_f

    iget-boolean v4, p0, Lorg/apache/lucene/index/IndexWriter;->keepFullyDeletedSegments:Z

    if-nez v4, :cond_f

    if-nez v1, :cond_f

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 3204
    :cond_f
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v4, p1, v1}, Lorg/apache/lucene/index/SegmentInfos;->applyMergeChanges(Lorg/apache/lucene/index/MergePolicy$OneMerge;Z)V

    .line 3206
    if-eqz v2, :cond_11

    .line 3207
    if-eqz v1, :cond_10

    .line 3208
    invoke-virtual {v2}, Lorg/apache/lucene/index/ReadersAndLiveDocs;->dropChanges()V

    .line 3210
    :cond_10
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->readerPool:Lorg/apache/lucene/index/IndexWriter$ReaderPool;

    invoke-virtual {v4, v2}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->release(Lorg/apache/lucene/index/ReadersAndLiveDocs;)V

    .line 3213
    :cond_11
    if-eqz v1, :cond_13

    .line 3214
    sget-boolean v4, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v4, :cond_12

    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    iget-object v6, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    invoke-virtual {v4, v6}, Lorg/apache/lucene/index/SegmentInfos;->contains(Lorg/apache/lucene/index/SegmentInfoPerCommit;)Z

    move-result v4

    if-eqz v4, :cond_12

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 3215
    :cond_12
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->readerPool:Lorg/apache/lucene/index/IndexWriter$ReaderPool;

    iget-object v6, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    invoke-virtual {v4, v6}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->drop(Lorg/apache/lucene/index/SegmentInfoPerCommit;)V

    .line 3216
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    iget-object v6, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    invoke-virtual {v6}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->files()Ljava/util/Collection;

    move-result-object v6

    invoke-virtual {v4, v6}, Lorg/apache/lucene/index/IndexFileDeleter;->deleteNewFiles(Ljava/util/Collection;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 3219
    :cond_13
    const/4 v3, 0x0

    .line 3224
    .local v3, "success":Z
    const/4 v4, 0x0

    :try_start_3
    invoke-direct {p0, p1, v4}, Lorg/apache/lucene/index/IndexWriter;->closeMergeReaders(Lorg/apache/lucene/index/MergePolicy$OneMerge;Z)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 3225
    const/4 v3, 0x1

    .line 3230
    if-eqz v3, :cond_17

    .line 3231
    :try_start_4
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->checkpoint()V

    .line 3241
    :goto_5
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    invoke-virtual {v4}, Lorg/apache/lucene/index/IndexFileDeleter;->deletePendingFiles()V

    .line 3243
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v6, "IW"

    invoke-virtual {v4, v6}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_14

    .line 3244
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v6, "IW"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "after commitMerge: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->segString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 3247
    :cond_14
    iget v4, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->maxNumSegments:I

    const/4 v6, -0x1

    if-eq v4, v6, :cond_15

    if-nez v1, :cond_15

    .line 3249
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->segmentsToMerge:Ljava/util/Map;

    iget-object v6, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    invoke-interface {v4, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_15

    .line 3250
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->segmentsToMerge:Ljava/util/Map;

    iget-object v6, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    sget-object v7, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-interface {v4, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_15
    move v4, v5

    .line 3254
    goto/16 :goto_0

    .line 3226
    :catchall_1
    move-exception v4

    .line 3230
    if-eqz v3, :cond_16

    .line 3231
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->checkpoint()V

    .line 3239
    :goto_6
    throw v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 3234
    :cond_16
    :try_start_5
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->checkpoint()V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_6

    .line 3235
    :catch_0
    move-exception v5

    goto :goto_6

    .line 3234
    :cond_17
    :try_start_6
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->checkpoint()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_5

    .line 3235
    :catch_1
    move-exception v4

    goto :goto_5
.end method

.method private declared-synchronized commitMergedDeletes(Lorg/apache/lucene/index/MergePolicy$OneMerge;Lorg/apache/lucene/index/MergeState;)Lorg/apache/lucene/index/ReadersAndLiveDocs;
    .locals 19
    .param p1, "merge"    # Lorg/apache/lucene/index/MergePolicy$OneMerge;
    .param p2, "mergeState"    # Lorg/apache/lucene/index/MergeState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3037
    monitor-enter p0

    :try_start_0
    sget-boolean v15, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v15, :cond_0

    const-string v15, "startCommitMergeDeletes"

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lorg/apache/lucene/index/IndexWriter;->testPoint(Ljava/lang/String;)Z

    move-result v15

    if-nez v15, :cond_0

    new-instance v15, Ljava/lang/AssertionError;

    invoke-direct {v15}, Ljava/lang/AssertionError;-><init>()V

    throw v15
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v15

    monitor-exit p0

    throw v15

    .line 3039
    :cond_0
    :try_start_1
    move-object/from16 v0, p1

    iget-object v14, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    .line 3041
    .local v14, "sourceSegments":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfoPerCommit;>;"
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v16, "IW"

    invoke-virtual/range {v15 .. v16}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_1

    .line 3042
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v16, "IW"

    new-instance v17, Ljava/lang/StringBuilder;

    const-string v18, "commitMergeDeletes "

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    iget-object v0, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/IndexWriter;->segString(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v15 .. v17}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 3047
    :cond_1
    const/4 v5, 0x0

    .line 3048
    .local v5, "docUpto":I
    const-wide v10, 0x7fffffffffffffffL

    .line 3051
    .local v10, "minGen":J
    const/4 v9, 0x0

    .line 3052
    .local v9, "mergedDeletes":Lorg/apache/lucene/index/ReadersAndLiveDocs;
    const/4 v4, 0x0

    .line 3054
    .local v4, "docMap":Lorg/apache/lucene/index/MergePolicy$DocMap;
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v15

    if-lt v6, v15, :cond_2

    .line 3131
    sget-boolean v15, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v15, :cond_13

    move-object/from16 v0, p1

    iget-object v15, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    iget-object v15, v15, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v15}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v15

    if-eq v5, v15, :cond_13

    new-instance v15, Ljava/lang/AssertionError;

    invoke-direct {v15}, Ljava/lang/AssertionError;-><init>()V

    throw v15

    .line 3055
    :cond_2
    invoke-interface {v14, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .line 3056
    .local v7, "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    invoke-virtual {v7}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->getBufferedDeletesGen()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-static {v0, v1, v10, v11}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v10

    .line 3057
    iget-object v15, v7, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v15}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v3

    .line 3058
    .local v3, "docCount":I
    move-object/from16 v0, p1

    iget-object v15, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->readers:Ljava/util/List;

    invoke-interface {v15, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lorg/apache/lucene/index/SegmentReader;

    invoke-virtual {v15}, Lorg/apache/lucene/index/SegmentReader;->getLiveDocs()Lorg/apache/lucene/util/Bits;

    move-result-object v12

    .line 3060
    .local v12, "prevLiveDocs":Lorg/apache/lucene/util/Bits;
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/index/IndexWriter;->readerPool:Lorg/apache/lucene/index/IndexWriter$ReaderPool;

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v15, v7, v0}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->get(Lorg/apache/lucene/index/SegmentInfoPerCommit;Z)Lorg/apache/lucene/index/ReadersAndLiveDocs;

    move-result-object v13

    .line 3062
    .local v13, "rld":Lorg/apache/lucene/index/ReadersAndLiveDocs;
    sget-boolean v15, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v15, :cond_3

    if-nez v13, :cond_3

    new-instance v15, Ljava/lang/AssertionError;

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "seg="

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v7, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-direct/range {v15 .. v16}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v15

    .line 3063
    :cond_3
    invoke-virtual {v13}, Lorg/apache/lucene/index/ReadersAndLiveDocs;->getLiveDocs()Lorg/apache/lucene/util/Bits;

    move-result-object v2

    .line 3065
    .local v2, "currentLiveDocs":Lorg/apache/lucene/util/Bits;
    if-eqz v12, :cond_e

    .line 3069
    sget-boolean v15, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v15, :cond_4

    if-nez v2, :cond_4

    new-instance v15, Ljava/lang/AssertionError;

    invoke-direct {v15}, Ljava/lang/AssertionError;-><init>()V

    throw v15

    .line 3070
    :cond_4
    sget-boolean v15, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v15, :cond_5

    invoke-interface {v12}, Lorg/apache/lucene/util/Bits;->length()I

    move-result v15

    if-eq v15, v3, :cond_5

    new-instance v15, Ljava/lang/AssertionError;

    invoke-direct {v15}, Ljava/lang/AssertionError;-><init>()V

    throw v15

    .line 3071
    :cond_5
    sget-boolean v15, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v15, :cond_6

    invoke-interface {v2}, Lorg/apache/lucene/util/Bits;->length()I

    move-result v15

    if-eq v15, v3, :cond_6

    new-instance v15, Ljava/lang/AssertionError;

    invoke-direct {v15}, Ljava/lang/AssertionError;-><init>()V

    throw v15

    .line 3085
    :cond_6
    if-eq v2, v12, :cond_d

    .line 3090
    const/4 v8, 0x0

    .local v8, "j":I
    :goto_1
    if-lt v8, v3, :cond_8

    .line 3054
    .end local v8    # "j":I
    :cond_7
    :goto_2
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_0

    .line 3091
    .restart local v8    # "j":I
    :cond_8
    invoke-interface {v12, v8}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v15

    if-nez v15, :cond_9

    .line 3092
    sget-boolean v15, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v15, :cond_c

    invoke-interface {v2, v8}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v15

    if-eqz v15, :cond_c

    new-instance v15, Ljava/lang/AssertionError;

    invoke-direct {v15}, Ljava/lang/AssertionError;-><init>()V

    throw v15

    .line 3094
    :cond_9
    invoke-interface {v2, v8}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v15

    if-nez v15, :cond_b

    .line 3095
    if-nez v9, :cond_a

    .line 3096
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/index/IndexWriter;->readerPool:Lorg/apache/lucene/index/IndexWriter$ReaderPool;

    move-object/from16 v0, p1

    iget-object v0, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    move-object/from16 v16, v0

    const/16 v17, 0x1

    invoke-virtual/range {v15 .. v17}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->get(Lorg/apache/lucene/index/SegmentInfoPerCommit;Z)Lorg/apache/lucene/index/ReadersAndLiveDocs;

    move-result-object v9

    .line 3097
    invoke-virtual {v9}, Lorg/apache/lucene/index/ReadersAndLiveDocs;->initWritableLiveDocs()V

    .line 3098
    invoke-virtual/range {p1 .. p2}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->getDocMap(Lorg/apache/lucene/index/MergeState;)Lorg/apache/lucene/index/MergePolicy$DocMap;

    move-result-object v4

    .line 3099
    sget-boolean v15, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v15, :cond_a

    move-object/from16 v0, p1

    iget-object v15, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    iget-object v15, v15, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v15}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v15

    invoke-virtual {v4, v15}, Lorg/apache/lucene/index/MergePolicy$DocMap;->isConsistent(I)Z

    move-result v15

    if-nez v15, :cond_a

    new-instance v15, Ljava/lang/AssertionError;

    invoke-direct {v15}, Ljava/lang/AssertionError;-><init>()V

    throw v15

    .line 3101
    :cond_a
    invoke-virtual {v4, v5}, Lorg/apache/lucene/index/MergePolicy$DocMap;->map(I)I

    move-result v15

    invoke-virtual {v9, v15}, Lorg/apache/lucene/index/ReadersAndLiveDocs;->delete(I)Z

    .line 3103
    :cond_b
    add-int/lit8 v5, v5, 0x1

    .line 3090
    :cond_c
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 3107
    .end local v8    # "j":I
    :cond_d
    iget-object v15, v7, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v15}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v15

    invoke-virtual {v7}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->getDelCount()I

    move-result v16

    sub-int v15, v15, v16

    invoke-virtual {v13}, Lorg/apache/lucene/index/ReadersAndLiveDocs;->getPendingDeleteCount()I

    move-result v16

    sub-int v15, v15, v16

    add-int/2addr v5, v15

    .line 3109
    goto :goto_2

    :cond_e
    if-eqz v2, :cond_12

    .line 3110
    sget-boolean v15, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v15, :cond_f

    invoke-interface {v2}, Lorg/apache/lucene/util/Bits;->length()I

    move-result v15

    if-eq v15, v3, :cond_f

    new-instance v15, Ljava/lang/AssertionError;

    invoke-direct {v15}, Ljava/lang/AssertionError;-><init>()V

    throw v15

    .line 3113
    :cond_f
    const/4 v8, 0x0

    .restart local v8    # "j":I
    :goto_3
    if-ge v8, v3, :cond_7

    .line 3114
    invoke-interface {v2, v8}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v15

    if-nez v15, :cond_11

    .line 3115
    if-nez v9, :cond_10

    .line 3116
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/index/IndexWriter;->readerPool:Lorg/apache/lucene/index/IndexWriter$ReaderPool;

    move-object/from16 v0, p1

    iget-object v0, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    move-object/from16 v16, v0

    const/16 v17, 0x1

    invoke-virtual/range {v15 .. v17}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->get(Lorg/apache/lucene/index/SegmentInfoPerCommit;Z)Lorg/apache/lucene/index/ReadersAndLiveDocs;

    move-result-object v9

    .line 3117
    invoke-virtual {v9}, Lorg/apache/lucene/index/ReadersAndLiveDocs;->initWritableLiveDocs()V

    .line 3118
    invoke-virtual/range {p1 .. p2}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->getDocMap(Lorg/apache/lucene/index/MergeState;)Lorg/apache/lucene/index/MergePolicy$DocMap;

    move-result-object v4

    .line 3119
    sget-boolean v15, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v15, :cond_10

    move-object/from16 v0, p1

    iget-object v15, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    iget-object v15, v15, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v15}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v15

    invoke-virtual {v4, v15}, Lorg/apache/lucene/index/MergePolicy$DocMap;->isConsistent(I)Z

    move-result v15

    if-nez v15, :cond_10

    new-instance v15, Ljava/lang/AssertionError;

    invoke-direct {v15}, Ljava/lang/AssertionError;-><init>()V

    throw v15

    .line 3121
    :cond_10
    invoke-virtual {v4, v5}, Lorg/apache/lucene/index/MergePolicy$DocMap;->map(I)I

    move-result v15

    invoke-virtual {v9, v15}, Lorg/apache/lucene/index/ReadersAndLiveDocs;->delete(I)Z

    .line 3123
    :cond_11
    add-int/lit8 v5, v5, 0x1

    .line 3113
    add-int/lit8 v8, v8, 0x1

    goto :goto_3

    .line 3127
    .end local v8    # "j":I
    :cond_12
    iget-object v15, v7, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v15}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v15

    add-int/2addr v5, v15

    goto/16 :goto_2

    .line 3133
    .end local v2    # "currentLiveDocs":Lorg/apache/lucene/util/Bits;
    .end local v3    # "docCount":I
    .end local v7    # "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .end local v12    # "prevLiveDocs":Lorg/apache/lucene/util/Bits;
    .end local v13    # "rld":Lorg/apache/lucene/index/ReadersAndLiveDocs;
    :cond_13
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v16, "IW"

    invoke-virtual/range {v15 .. v16}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_14

    .line 3134
    if-nez v9, :cond_15

    .line 3135
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v16, "IW"

    const-string v17, "no new deletes since merge started"

    invoke-virtual/range {v15 .. v17}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 3141
    :cond_14
    :goto_4
    move-object/from16 v0, p1

    iget-object v15, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    invoke-virtual {v15, v10, v11}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->setBufferedDeletesGen(J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3143
    monitor-exit p0

    return-object v9

    .line 3137
    :cond_15
    :try_start_2
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v16, "IW"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Lorg/apache/lucene/index/ReadersAndLiveDocs;->getPendingDeleteCount()I

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v18, " new deletes since merge started"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v15 .. v17}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_4
.end method

.method private copySegmentAsIs(Lorg/apache/lucene/index/SegmentInfoPerCommit;Ljava/lang/String;Ljava/util/Map;Ljava/util/Set;Lorg/apache/lucene/store/IOContext;Ljava/util/Set;)Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .locals 22
    .param p1, "info"    # Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .param p2, "segName"    # Ljava/lang/String;
    .param p5, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/SegmentInfoPerCommit;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lorg/apache/lucene/store/IOContext;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lorg/apache/lucene/index/SegmentInfoPerCommit;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2519
    .local p3, "dsNames":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .local p4, "dsFilesCopied":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .local p6, "copiedFiles":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    move-object/from16 v0, p1

    iget-object v3, v0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-static {v3}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoFormat;->getDocStoreSegment(Lorg/apache/lucene/index/SegmentInfo;)Ljava/lang/String;

    move-result-object v12

    .line 2520
    .local v12, "dsName":Ljava/lang/String;
    sget-boolean v3, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    if-nez v12, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 2522
    :cond_0
    move-object/from16 v0, p3

    invoke-interface {v0, v12}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2523
    move-object/from16 v0, p3

    invoke-interface {v0, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    .line 2531
    .local v15, "newDsName":Ljava/lang/String;
    :goto_0
    move-object/from16 v0, p1

    iget-object v3, v0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/index/IndexWriter;->getFieldInfos(Lorg/apache/lucene/index/SegmentInfo;)Lorg/apache/lucene/index/FieldInfos;

    move-result-object v14

    .line 2533
    .local v14, "fis":Lorg/apache/lucene/index/FieldInfos;
    move-object/from16 v0, p1

    iget-object v3, v0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-static {v3}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xCodec;->getDocStoreFiles(Lorg/apache/lucene/index/SegmentInfo;)Ljava/util/Set;

    move-result-object v11

    .line 2538
    .local v11, "docStoreFiles3xOnly":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    move-object/from16 v0, p1

    iget-object v3, v0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v3}, Lorg/apache/lucene/index/SegmentInfo;->attributes()Ljava/util/Map;

    move-result-object v3

    if-nez v3, :cond_5

    .line 2539
    new-instance v10, Ljava/util/HashMap;

    invoke-direct {v10}, Ljava/util/HashMap;-><init>()V

    .line 2543
    .local v10, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :goto_1
    if-eqz v11, :cond_1

    .line 2547
    sget-object v3, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoFormat;->DS_NAME_KEY:Ljava/lang/String;

    invoke-interface {v10, v3, v15}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2552
    :cond_1
    new-instance v2, Lorg/apache/lucene/index/SegmentInfo;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    move-object/from16 v0, p1

    iget-object v4, v0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v4}, Lorg/apache/lucene/index/SegmentInfo;->getVersion()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    iget-object v5, v0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v5}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v6

    .line 2553
    move-object/from16 v0, p1

    iget-object v5, v0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v5}, Lorg/apache/lucene/index/SegmentInfo;->getUseCompoundFile()Z

    move-result v7

    .line 2554
    move-object/from16 v0, p1

    iget-object v5, v0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v5}, Lorg/apache/lucene/index/SegmentInfo;->getCodec()Lorg/apache/lucene/codecs/Codec;

    move-result-object v8

    move-object/from16 v0, p1

    iget-object v5, v0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v5}, Lorg/apache/lucene/index/SegmentInfo;->getDiagnostics()Ljava/util/Map;

    move-result-object v9

    move-object/from16 v5, p2

    .line 2552
    invoke-direct/range {v2 .. v10}, Lorg/apache/lucene/index/SegmentInfo;-><init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Ljava/lang/String;IZLorg/apache/lucene/codecs/Codec;Ljava/util/Map;Ljava/util/Map;)V

    .line 2555
    .local v2, "newInfo":Lorg/apache/lucene/index/SegmentInfo;
    new-instance v17, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->getDelCount()I

    move-result v3

    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->getDelGen()J

    move-result-wide v4

    move-object/from16 v0, v17

    invoke-direct {v0, v2, v3, v4, v5}, Lorg/apache/lucene/index/SegmentInfoPerCommit;-><init>(Lorg/apache/lucene/index/SegmentInfo;IJ)V

    .line 2557
    .local v17, "newInfoPerCommit":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    new-instance v18, Ljava/util/HashSet;

    invoke-direct/range {v18 .. v18}, Ljava/util/HashSet;-><init>()V

    .line 2561
    .local v18, "segFiles":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->files()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_6

    .line 2570
    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Lorg/apache/lucene/index/SegmentInfo;->setFiles(Ljava/util/Set;)V

    .line 2575
    new-instance v21, Lorg/apache/lucene/store/TrackingDirectoryWrapper;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    move-object/from16 v0, v21

    invoke-direct {v0, v3}, Lorg/apache/lucene/store/TrackingDirectoryWrapper;-><init>(Lorg/apache/lucene/store/Directory;)V

    .line 2577
    .local v21, "trackingDir":Lorg/apache/lucene/store/TrackingDirectoryWrapper;
    :try_start_0
    invoke-virtual {v2}, Lorg/apache/lucene/index/SegmentInfo;->getCodec()Lorg/apache/lucene/codecs/Codec;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/lucene/codecs/Codec;->segmentInfoFormat()Lorg/apache/lucene/codecs/SegmentInfoFormat;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/lucene/codecs/SegmentInfoFormat;->getSegmentInfoWriter()Lorg/apache/lucene/codecs/SegmentInfoWriter;

    move-result-object v3

    move-object/from16 v0, v21

    move-object/from16 v1, p5

    invoke-virtual {v3, v0, v2, v14, v1}, Lorg/apache/lucene/codecs/SegmentInfoWriter;->write(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/store/IOContext;)V
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_2

    .line 2583
    :goto_3
    invoke-virtual/range {v21 .. v21}, Lorg/apache/lucene/store/TrackingDirectoryWrapper;->getCreatedFiles()Ljava/util/Set;

    move-result-object v19

    .line 2585
    .local v19, "siFiles":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    const/16 v20, 0x0

    .line 2589
    .local v20, "success":Z
    :try_start_1
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->files()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v4

    if-nez v4, :cond_8

    .line 2612
    const/16 v20, 0x1

    .line 2614
    if-nez v20, :cond_3

    .line 2615
    invoke-virtual {v2}, Lorg/apache/lucene/index/SegmentInfo;->files()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_e

    .line 2624
    :cond_3
    return-object v17

    .line 2525
    .end local v2    # "newInfo":Lorg/apache/lucene/index/SegmentInfo;
    .end local v10    # "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v11    # "docStoreFiles3xOnly":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v14    # "fis":Lorg/apache/lucene/index/FieldInfos;
    .end local v15    # "newDsName":Ljava/lang/String;
    .end local v17    # "newInfoPerCommit":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .end local v18    # "segFiles":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v19    # "siFiles":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    .end local v20    # "success":Z
    .end local v21    # "trackingDir":Lorg/apache/lucene/store/TrackingDirectoryWrapper;
    :cond_4
    move-object/from16 v0, p3

    move-object/from16 v1, p2

    invoke-interface {v0, v12, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2526
    move-object/from16 v15, p2

    .restart local v15    # "newDsName":Ljava/lang/String;
    goto/16 :goto_0

    .line 2541
    .restart local v11    # "docStoreFiles3xOnly":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .restart local v14    # "fis":Lorg/apache/lucene/index/FieldInfos;
    :cond_5
    new-instance v10, Ljava/util/HashMap;

    move-object/from16 v0, p1

    iget-object v3, v0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v3}, Lorg/apache/lucene/index/SegmentInfo;->attributes()Ljava/util/Map;

    move-result-object v3

    invoke-direct {v10, v3}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .restart local v10    # "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    goto/16 :goto_1

    .line 2561
    .restart local v2    # "newInfo":Lorg/apache/lucene/index/SegmentInfo;
    .restart local v17    # "newInfoPerCommit":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .restart local v18    # "segFiles":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_6
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    .line 2563
    .local v13, "file":Ljava/lang/String;
    if-eqz v11, :cond_7

    invoke-interface {v11, v13}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 2564
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v15}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v13}, Lorg/apache/lucene/index/IndexFileNames;->stripSegmentName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 2568
    .local v16, "newFileName":Ljava/lang/String;
    :goto_6
    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 2566
    .end local v16    # "newFileName":Ljava/lang/String;
    :cond_7
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static/range {p2 .. p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v13}, Lorg/apache/lucene/index/IndexFileNames;->stripSegmentName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .restart local v16    # "newFileName":Ljava/lang/String;
    goto :goto_6

    .line 2589
    .end local v13    # "file":Ljava/lang/String;
    .end local v16    # "newFileName":Ljava/lang/String;
    .restart local v19    # "siFiles":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    .restart local v20    # "success":Z
    .restart local v21    # "trackingDir":Lorg/apache/lucene/store/TrackingDirectoryWrapper;
    :cond_8
    :try_start_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    .line 2592
    .restart local v13    # "file":Ljava/lang/String;
    if-eqz v11, :cond_a

    invoke-interface {v11, v13}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 2593
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v15}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v13}, Lorg/apache/lucene/index/IndexFileNames;->stripSegmentName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 2594
    .restart local v16    # "newFileName":Ljava/lang/String;
    move-object/from16 v0, p4

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 2597
    move-object/from16 v0, p4

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2602
    :goto_7
    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 2607
    sget-boolean v4, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v4, :cond_b

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Lorg/apache/lucene/store/Directory;->fileExists(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_b

    new-instance v3, Ljava/lang/AssertionError;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "file \""

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\" already exists; siFiles="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2613
    .end local v13    # "file":Ljava/lang/String;
    .end local v16    # "newFileName":Ljava/lang/String;
    :catchall_0
    move-exception v3

    .line 2614
    if-nez v20, :cond_9

    .line 2615
    invoke-virtual {v2}, Lorg/apache/lucene/index/SegmentInfo;->files()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_8
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_d

    .line 2622
    :cond_9
    throw v3

    .line 2599
    .restart local v13    # "file":Ljava/lang/String;
    :cond_a
    :try_start_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static/range {p2 .. p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v13}, Lorg/apache/lucene/index/IndexFileNames;->stripSegmentName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .restart local v16    # "newFileName":Ljava/lang/String;
    goto :goto_7

    .line 2608
    :cond_b
    sget-boolean v4, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v4, :cond_c

    move-object/from16 v0, p6

    invoke-interface {v0, v13}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    new-instance v3, Ljava/lang/AssertionError;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "file \""

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\" is being copied more than once"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 2609
    :cond_c
    move-object/from16 v0, p6

    invoke-interface {v0, v13}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2610
    move-object/from16 v0, p1

    iget-object v4, v0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v4, v4, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    move-object/from16 v0, v16

    move-object/from16 v1, p5

    invoke-virtual {v4, v5, v13, v0, v1}, Lorg/apache/lucene/store/Directory;->copy(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_4

    .line 2615
    .end local v13    # "file":Ljava/lang/String;
    .end local v16    # "newFileName":Ljava/lang/String;
    :cond_d
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    .line 2617
    .restart local v13    # "file":Ljava/lang/String;
    :try_start_4
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v5, v13}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_8

    .line 2618
    :catch_0
    move-exception v5

    goto :goto_8

    .line 2615
    .end local v13    # "file":Ljava/lang/String;
    :cond_e
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    .line 2617
    .restart local v13    # "file":Ljava/lang/String;
    :try_start_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v4, v13}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1

    goto/16 :goto_5

    .line 2618
    :catch_1
    move-exception v4

    goto/16 :goto_5

    .line 2578
    .end local v13    # "file":Ljava/lang/String;
    .end local v19    # "siFiles":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    .end local v20    # "success":Z
    :catch_2
    move-exception v3

    goto/16 :goto_3
.end method

.method static final createCompoundFile(Lorg/apache/lucene/util/InfoStream;Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/MergeState$CheckAbort;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/store/IOContext;)Ljava/util/Collection;
    .locals 14
    .param p0, "infoStream"    # Lorg/apache/lucene/util/InfoStream;
    .param p1, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p2, "checkAbort"    # Lorg/apache/lucene/index/MergeState$CheckAbort;
    .param p3, "info"    # Lorg/apache/lucene/index/SegmentInfo;
    .param p4, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/InfoStream;",
            "Lorg/apache/lucene/store/Directory;",
            "Lorg/apache/lucene/index/MergeState$CheckAbort;",
            "Lorg/apache/lucene/index/SegmentInfo;",
            "Lorg/apache/lucene/store/IOContext;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4216
    move-object/from16 v0, p3

    iget-object v10, v0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    const-string v11, ""

    const-string v12, "cfs"

    invoke-static {v10, v11, v12}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 4217
    .local v5, "fileName":Ljava/lang/String;
    const-string v10, "IW"

    invoke-virtual {p0, v10}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 4218
    const-string v10, "IW"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "create compound file "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p0, v10, v11}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 4220
    :cond_0
    sget-boolean v10, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v10, :cond_1

    invoke-static/range {p3 .. p3}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoFormat;->getDocStoreOffset(Lorg/apache/lucene/index/SegmentInfo;)I

    move-result v10

    const/4 v11, -0x1

    if-eq v10, v11, :cond_1

    new-instance v10, Ljava/lang/AssertionError;

    invoke-direct {v10}, Ljava/lang/AssertionError;-><init>()V

    throw v10

    .line 4222
    :cond_1
    invoke-virtual/range {p3 .. p3}, Lorg/apache/lucene/index/SegmentInfo;->files()Ljava/util/Set;

    move-result-object v6

    .line 4223
    .local v6, "files":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    new-instance v2, Lorg/apache/lucene/store/CompoundFileDirectory;

    const/4 v10, 0x1

    move-object/from16 v0, p4

    invoke-direct {v2, p1, v5, v0, v10}, Lorg/apache/lucene/store/CompoundFileDirectory;-><init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/store/IOContext;Z)V

    .line 4224
    .local v2, "cfsDir":Lorg/apache/lucene/store/CompoundFileDirectory;
    const/4 v7, 0x0

    .line 4226
    .local v7, "prior":Ljava/io/IOException;
    :try_start_0
    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v11

    if-nez v11, :cond_3

    .line 4233
    const/4 v9, 0x0

    .line 4235
    .local v9, "success":Z
    const/4 v10, 0x1

    :try_start_1
    new-array v10, v10, [Ljava/io/Closeable;

    const/4 v11, 0x0

    aput-object v2, v10, v11

    invoke-static {v7, v10}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException(Ljava/lang/Exception;[Ljava/io/Closeable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    .line 4236
    const/4 v9, 0x1

    .line 4238
    if-nez v9, :cond_2

    .line 4240
    :try_start_2
    invoke-virtual {p1, v5}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_7

    .line 4244
    :goto_1
    :try_start_3
    move-object/from16 v0, p3

    iget-object v10, v0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    const-string v11, ""

    const-string v12, "cfe"

    invoke-static {v10, v11, v12}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_8

    .line 4252
    :cond_2
    :goto_2
    new-instance v8, Ljava/util/HashSet;

    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    .line 4253
    .local v8, "siFiles":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v8, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 4254
    move-object/from16 v0, p3

    iget-object v10, v0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    const-string v11, ""

    const-string v12, "cfe"

    invoke-static {v10, v11, v12}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v8, v10}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 4255
    move-object/from16 v0, p3

    invoke-virtual {v0, v8}, Lorg/apache/lucene/index/SegmentInfo;->setFiles(Ljava/util/Set;)V

    .line 4257
    return-object v6

    .line 4226
    .end local v8    # "siFiles":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v9    # "success":Z
    :cond_3
    :try_start_4
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 4227
    .local v4, "file":Ljava/lang/String;
    move-object/from16 v0, p4

    invoke-virtual {p1, v2, v4, v4, v0}, Lorg/apache/lucene/store/Directory;->copy(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)V

    .line 4228
    invoke-virtual {p1, v4}, Lorg/apache/lucene/store/Directory;->fileLength(Ljava/lang/String;)J

    move-result-wide v12

    long-to-double v12, v12

    move-object/from16 v0, p2

    invoke-virtual {v0, v12, v13}, Lorg/apache/lucene/index/MergeState$CheckAbort;->work(D)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0

    .line 4230
    .end local v4    # "file":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 4231
    .local v3, "ex":Ljava/io/IOException;
    move-object v7, v3

    .line 4233
    const/4 v9, 0x0

    .line 4235
    .restart local v9    # "success":Z
    const/4 v10, 0x1

    :try_start_5
    new-array v10, v10, [Ljava/io/Closeable;

    const/4 v11, 0x0

    aput-object v2, v10, v11

    invoke-static {v7, v10}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException(Ljava/lang/Exception;[Ljava/io/Closeable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 4236
    const/4 v9, 0x1

    .line 4238
    if-nez v9, :cond_2

    .line 4240
    :try_start_6
    invoke-virtual {p1, v5}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_3

    .line 4244
    :goto_3
    :try_start_7
    move-object/from16 v0, p3

    iget-object v10, v0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    const-string v11, ""

    const-string v12, "cfe"

    invoke-static {v10, v11, v12}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_1

    goto :goto_2

    .line 4245
    :catch_1
    move-exception v10

    goto :goto_2

    .line 4237
    :catchall_0
    move-exception v10

    .line 4238
    if-nez v9, :cond_4

    .line 4240
    :try_start_8
    invoke-virtual {p1, v5}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_2

    .line 4244
    :goto_4
    :try_start_9
    move-object/from16 v0, p3

    iget-object v11, v0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    const-string v12, ""

    const-string v13, "cfe"

    invoke-static {v11, v12, v13}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p1, v11}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_c

    .line 4248
    :cond_4
    :goto_5
    throw v10

    .line 4232
    .end local v3    # "ex":Ljava/io/IOException;
    .end local v9    # "success":Z
    :catchall_1
    move-exception v10

    .line 4233
    const/4 v9, 0x0

    .line 4235
    .restart local v9    # "success":Z
    const/4 v11, 0x1

    :try_start_a
    new-array v11, v11, [Ljava/io/Closeable;

    const/4 v12, 0x0

    aput-object v2, v11, v12

    invoke-static {v7, v11}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException(Ljava/lang/Exception;[Ljava/io/Closeable;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    .line 4236
    const/4 v9, 0x1

    .line 4238
    if-nez v9, :cond_5

    .line 4240
    :try_start_b
    invoke-virtual {p1, v5}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_5

    .line 4244
    :goto_6
    :try_start_c
    move-object/from16 v0, p3

    iget-object v11, v0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    const-string v12, ""

    const-string v13, "cfe"

    invoke-static {v11, v12, v13}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p1, v11}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_c
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_c} :catch_a

    .line 4249
    :cond_5
    :goto_7
    throw v10

    .line 4237
    :catchall_2
    move-exception v10

    .line 4238
    if-nez v9, :cond_6

    .line 4240
    :try_start_d
    invoke-virtual {p1, v5}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_d
    .catch Ljava/lang/Throwable; {:try_start_d .. :try_end_d} :catch_4

    .line 4244
    :goto_8
    :try_start_e
    move-object/from16 v0, p3

    iget-object v11, v0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    const-string v12, ""

    const-string v13, "cfe"

    invoke-static {v11, v12, v13}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p1, v11}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_e
    .catch Ljava/lang/Throwable; {:try_start_e .. :try_end_e} :catch_b

    .line 4248
    :cond_6
    :goto_9
    throw v10

    .line 4237
    :catchall_3
    move-exception v10

    .line 4238
    if-nez v9, :cond_7

    .line 4240
    :try_start_f
    invoke-virtual {p1, v5}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_f
    .catch Ljava/lang/Throwable; {:try_start_f .. :try_end_f} :catch_6

    .line 4244
    :goto_a
    :try_start_10
    move-object/from16 v0, p3

    iget-object v11, v0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    const-string v12, ""

    const-string v13, "cfe"

    invoke-static {v11, v12, v13}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p1, v11}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_10
    .catch Ljava/lang/Throwable; {:try_start_10 .. :try_end_10} :catch_9

    .line 4248
    :cond_7
    :goto_b
    throw v10

    .line 4241
    .restart local v3    # "ex":Ljava/io/IOException;
    :catch_2
    move-exception v11

    goto :goto_4

    :catch_3
    move-exception v10

    goto :goto_3

    .end local v3    # "ex":Ljava/io/IOException;
    :catch_4
    move-exception v11

    goto :goto_8

    :catch_5
    move-exception v11

    goto :goto_6

    :catch_6
    move-exception v11

    goto :goto_a

    :catch_7
    move-exception v10

    goto/16 :goto_1

    .line 4245
    :catch_8
    move-exception v10

    goto/16 :goto_2

    :catch_9
    move-exception v11

    goto :goto_b

    :catch_a
    move-exception v11

    goto :goto_7

    :catch_b
    move-exception v11

    goto :goto_9

    .restart local v3    # "ex":Ljava/io/IOException;
    :catch_c
    move-exception v11

    goto :goto_5
.end method

.method private doFlush(Z)Z
    .locals 8
    .param p1, "applyAllDeletes"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2912
    iget-boolean v4, p0, Lorg/apache/lucene/index/IndexWriter;->hitOOM:Z

    if-eqz v4, :cond_0

    .line 2913
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "this writer hit an OutOfMemoryError; cannot flush"

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 2916
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->doBeforeFlush()V

    .line 2917
    sget-boolean v4, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    const-string v4, "startDoFlush"

    invoke-virtual {p0, v4}, Lorg/apache/lucene/index/IndexWriter;->testPoint(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 2918
    :cond_1
    const/4 v3, 0x0

    .line 2921
    .local v3, "success":Z
    :try_start_0
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v5, "IW"

    invoke-virtual {v4, v5}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2922
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v5, "IW"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "  start flush: applyAllDeletes="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 2923
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v5, "IW"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "  index before flush "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->segString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 2927
    :cond_2
    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->fullFlushLock:Ljava/lang/Object;

    monitor-enter v5
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    .line 2928
    const/4 v1, 0x0

    .line 2930
    .local v1, "flushSuccess":Z
    :try_start_1
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v4}, Lorg/apache/lucene/index/DocumentsWriter;->flushAllThreads()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 2931
    .local v0, "anySegmentFlushed":Z
    const/4 v1, 0x1

    .line 2933
    :try_start_2
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v4, v1}, Lorg/apache/lucene/index/DocumentsWriter;->finishFullFlush(Z)V

    .line 2927
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2936
    :try_start_3
    monitor-enter p0
    :try_end_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 2937
    :try_start_4
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/IndexWriter;->maybeApplyDeletes(Z)V

    .line 2938
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->doAfterFlush()V

    .line 2939
    if-nez v0, :cond_3

    .line 2941
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->flushCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 2943
    :cond_3
    const/4 v3, 0x1

    .line 2944
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 2951
    if-nez v3, :cond_4

    .line 2952
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v5, "IW"

    invoke-virtual {v4, v5}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 2953
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v5, "IW"

    const-string v6, "hit exception during flush"

    invoke-virtual {v4, v5, v6}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 2949
    .end local v0    # "anySegmentFlushed":Z
    .end local v1    # "flushSuccess":Z
    :cond_4
    :goto_0
    return v0

    .line 2932
    .restart local v1    # "flushSuccess":Z
    :catchall_0
    move-exception v4

    .line 2933
    :try_start_5
    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v6, v1}, Lorg/apache/lucene/index/DocumentsWriter;->finishFullFlush(Z)V

    .line 2934
    throw v4

    .line 2927
    :catchall_1
    move-exception v4

    monitor-exit v5
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    throw v4
    :try_end_6
    .catch Ljava/lang/OutOfMemoryError; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 2946
    .end local v1    # "flushSuccess":Z
    :catch_0
    move-exception v2

    .line 2947
    .local v2, "oom":Ljava/lang/OutOfMemoryError;
    :try_start_7
    const-string v4, "doFlush"

    invoke-direct {p0, v2, v4}, Lorg/apache/lucene/index/IndexWriter;->handleOOM(Ljava/lang/OutOfMemoryError;Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    .line 2951
    if-nez v3, :cond_5

    .line 2952
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v5, "IW"

    invoke-virtual {v4, v5}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2953
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v5, "IW"

    const-string v6, "hit exception during flush"

    invoke-virtual {v4, v5, v6}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 2949
    :cond_5
    const/4 v0, 0x0

    goto :goto_0

    .line 2936
    .end local v2    # "oom":Ljava/lang/OutOfMemoryError;
    .restart local v0    # "anySegmentFlushed":Z
    .restart local v1    # "flushSuccess":Z
    :catchall_2
    move-exception v4

    :try_start_8
    monitor-exit p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    :try_start_9
    throw v4
    :try_end_9
    .catch Ljava/lang/OutOfMemoryError; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    .line 2950
    .end local v0    # "anySegmentFlushed":Z
    .end local v1    # "flushSuccess":Z
    :catchall_3
    move-exception v4

    .line 2951
    if-nez v3, :cond_6

    .line 2952
    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v6, "IW"

    invoke-virtual {v5, v6}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 2953
    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v6, "IW"

    const-string v7, "hit exception during flush"

    invoke-virtual {v5, v6, v7}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 2956
    :cond_6
    throw v4
.end method

.method private declared-synchronized doWait()V
    .locals 4

    .prologue
    .line 3923
    monitor-enter p0

    const-wide/16 v2, 0x3e8

    :try_start_0
    invoke-virtual {p0, v2, v3}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3927
    monitor-exit p0

    return-void

    .line 3924
    :catch_0
    move-exception v0

    .line 3925
    .local v0, "ie":Ljava/lang/InterruptedException;
    :try_start_1
    new-instance v1, Lorg/apache/lucene/util/ThreadInterruptedException;

    invoke-direct {v1, v0}, Lorg/apache/lucene/util/ThreadInterruptedException;-><init>(Ljava/lang/InterruptedException;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3923
    .end local v0    # "ie":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private declared-synchronized ensureValidMerge(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V
    .locals 4
    .param p1, "merge"    # Lorg/apache/lucene/index/MergePolicy$OneMerge;

    .prologue
    .line 3019
    monitor-enter p0

    :try_start_0
    iget-object v1, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_1

    .line 3024
    monitor-exit p0

    return-void

    .line 3019
    :cond_1
    :try_start_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .line 3020
    .local v0, "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v2, v0}, Lorg/apache/lucene/index/SegmentInfos;->contains(Lorg/apache/lucene/index/SegmentInfoPerCommit;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 3021
    new-instance v1, Lorg/apache/lucene/index/MergePolicy$MergeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "MergePolicy selected a segment ("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v3, v3, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") that is not in the current index "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->segString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-direct {v1, v2, v3}, Lorg/apache/lucene/index/MergePolicy$MergeException;-><init>(Ljava/lang/String;Lorg/apache/lucene/store/Directory;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3019
    .end local v0    # "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private filesExist(Lorg/apache/lucene/index/SegmentInfos;)Z
    .locals 5
    .param p1, "toSync"    # Lorg/apache/lucene/index/SegmentInfos;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3945
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Lorg/apache/lucene/index/SegmentInfos;->files(Lorg/apache/lucene/store/Directory;Z)Ljava/util/Collection;

    move-result-object v1

    .line 3946
    .local v1, "files":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 3955
    const/4 v2, 0x1

    return v2

    .line 3946
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 3947
    .local v0, "fileName":Ljava/lang/String;
    sget-boolean v3, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v3, :cond_2

    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v3, v0}, Lorg/apache/lucene/store/Directory;->fileExists(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    new-instance v2, Ljava/lang/AssertionError;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "file "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " does not exist"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 3953
    :cond_2
    sget-boolean v3, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    invoke-virtual {v3, v0}, Lorg/apache/lucene/index/IndexFileDeleter;->exists(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "IndexFileDeleter doesn\'t know about file "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method private final declared-synchronized finishCommit()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2852
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->pendingCommit:Lorg/apache/lucene/index/SegmentInfos;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz v0, :cond_4

    .line 2854
    :try_start_1
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v1, "IW"

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2855
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v1, "IW"

    const-string v2, "commit: pendingCommit != null"

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 2857
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->pendingCommit:Lorg/apache/lucene/index/SegmentInfos;

    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/SegmentInfos;->finishCommit(Lorg/apache/lucene/store/Directory;)V

    .line 2858
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v1, "IW"

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2859
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v1, "IW"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "commit: wrote segments file \""

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->pendingCommit:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v3}, Lorg/apache/lucene/index/SegmentInfos;->getSegmentsFileName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 2861
    :cond_1
    iget-wide v0, p0, Lorg/apache/lucene/index/IndexWriter;->pendingCommitChangeCount:J

    iput-wide v0, p0, Lorg/apache/lucene/index/IndexWriter;->lastCommitChangeCount:J

    .line 2862
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->pendingCommit:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/SegmentInfos;->updateGeneration(Lorg/apache/lucene/index/SegmentInfos;)V

    .line 2863
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->pendingCommit:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentInfos;->createBackupSegmentInfos()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->rollbackSegments:Ljava/util/List;

    .line 2864
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->pendingCommit:Lorg/apache/lucene/index/SegmentInfos;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/index/IndexFileDeleter;->checkpoint(Lorg/apache/lucene/index/SegmentInfos;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2867
    :try_start_2
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->filesToCommit:Ljava/util/Collection;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/IndexFileDeleter;->decRef(Ljava/util/Collection;)V

    .line 2868
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->filesToCommit:Ljava/util/Collection;

    .line 2869
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->pendingCommit:Lorg/apache/lucene/index/SegmentInfos;

    .line 2870
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 2879
    :cond_2
    :goto_0
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v1, "IW"

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2880
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v1, "IW"

    const-string v2, "commit: done"

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2882
    :cond_3
    monitor-exit p0

    return-void

    .line 2865
    :catchall_0
    move-exception v0

    .line 2867
    :try_start_3
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->filesToCommit:Ljava/util/Collection;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/index/IndexFileDeleter;->decRef(Ljava/util/Collection;)V

    .line 2868
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->filesToCommit:Ljava/util/Collection;

    .line 2869
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->pendingCommit:Lorg/apache/lucene/index/SegmentInfos;

    .line 2870
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 2871
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2852
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2874
    :cond_4
    :try_start_4
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v1, "IW"

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2875
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v1, "IW"

    const-string v2, "commit: pendingCommit == null; skip"

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0
.end method

.method private declared-synchronized finishMerges(Z)V
    .locals 6
    .param p1, "waitForMerges"    # Z

    .prologue
    .line 2095
    monitor-enter p0

    if-nez p1, :cond_8

    .line 2097
    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lorg/apache/lucene/index/IndexWriter;->stopMerges:Z

    .line 2100
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->pendingMerges:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2107
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->pendingMerges:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->clear()V

    .line 2109
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->runningMerges:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_2

    .line 2121
    :goto_2
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->runningMerges:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    if-gtz v1, :cond_4

    .line 2128
    const/4 v1, 0x0

    iput-boolean v1, p0, Lorg/apache/lucene/index/IndexWriter;->stopMerges:Z

    .line 2129
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 2131
    sget-boolean v1, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v1, :cond_6

    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->mergingSegments:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->size()I

    move-result v1

    if-eqz v1, :cond_6

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2095
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 2100
    :cond_0
    :try_start_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;

    .line 2101
    .local v0, "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v3, "IW"

    invoke-virtual {v2, v3}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2102
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v3, "IW"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "now abort pending merge "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    invoke-virtual {p0, v5}, Lorg/apache/lucene/index/IndexWriter;->segString(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 2104
    :cond_1
    invoke-virtual {v0}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->abort()V

    .line 2105
    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexWriter;->mergeFinish(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    goto :goto_0

    .line 2109
    .end local v0    # "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;

    .line 2110
    .restart local v0    # "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v3, "IW"

    invoke-virtual {v2, v3}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2111
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v3, "IW"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "now abort running merge "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    invoke-virtual {p0, v5}, Lorg/apache/lucene/index/IndexWriter;->segString(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 2113
    :cond_3
    invoke-virtual {v0}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->abort()V

    goto/16 :goto_1

    .line 2122
    .end local v0    # "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    :cond_4
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v2, "IW"

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2123
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v2, "IW"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "now wait for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->runningMerges:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " running merge/s to abort"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 2125
    :cond_5
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexWriter;->doWait()V

    goto/16 :goto_2

    .line 2133
    :cond_6
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v2, "IW"

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 2134
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v2, "IW"

    const-string v3, "all running merges have aborted"

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2145
    :cond_7
    :goto_3
    monitor-exit p0

    return-void

    .line 2143
    :cond_8
    :try_start_2
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->waitForMerges()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3
.end method

.method private getFieldInfos(Lorg/apache/lucene/index/SegmentInfo;)Lorg/apache/lucene/index/FieldInfos;
    .locals 6
    .param p1, "info"    # Lorg/apache/lucene/index/SegmentInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 753
    const/4 v0, 0x0

    .line 755
    .local v0, "cfsDir":Lorg/apache/lucene/store/Directory;
    :try_start_0
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfo;->getUseCompoundFile()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 756
    new-instance v1, Lorg/apache/lucene/store/CompoundFileDirectory;

    iget-object v2, p1, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    .line 757
    iget-object v3, p1, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    const-string v4, ""

    const-string v5, "cfs"

    invoke-static {v3, v4, v5}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 758
    sget-object v4, Lorg/apache/lucene/store/IOContext;->READONCE:Lorg/apache/lucene/store/IOContext;

    .line 759
    const/4 v5, 0x0

    .line 756
    invoke-direct {v1, v2, v3, v4, v5}, Lorg/apache/lucene/store/CompoundFileDirectory;-><init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/store/IOContext;Z)V

    .end local v0    # "cfsDir":Lorg/apache/lucene/store/Directory;
    .local v1, "cfsDir":Lorg/apache/lucene/store/Directory;
    move-object v0, v1

    .line 763
    .end local v1    # "cfsDir":Lorg/apache/lucene/store/Directory;
    .restart local v0    # "cfsDir":Lorg/apache/lucene/store/Directory;
    :goto_0
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfo;->getCodec()Lorg/apache/lucene/codecs/Codec;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/lucene/codecs/Codec;->fieldInfosFormat()Lorg/apache/lucene/codecs/FieldInfosFormat;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/lucene/codecs/FieldInfosFormat;->getFieldInfosReader()Lorg/apache/lucene/codecs/FieldInfosReader;

    move-result-object v2

    .line 764
    iget-object v3, p1, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    .line 765
    sget-object v4, Lorg/apache/lucene/store/IOContext;->READONCE:Lorg/apache/lucene/store/IOContext;

    .line 763
    invoke-virtual {v2, v0, v3, v4}, Lorg/apache/lucene/codecs/FieldInfosReader;->read(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/index/FieldInfos;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 767
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfo;->getUseCompoundFile()Z

    move-result v3

    if-eqz v3, :cond_0

    if-eqz v0, :cond_0

    .line 768
    invoke-virtual {v0}, Lorg/apache/lucene/store/Directory;->close()V

    .line 763
    :cond_0
    return-object v2

    .line 761
    :cond_1
    :try_start_1
    iget-object v0, p1, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 766
    :catchall_0
    move-exception v2

    .line 767
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfo;->getUseCompoundFile()Z

    move-result v3

    if-eqz v3, :cond_2

    if-eqz v0, :cond_2

    .line 768
    invoke-virtual {v0}, Lorg/apache/lucene/store/Directory;->close()V

    .line 770
    :cond_2
    throw v2
.end method

.method private getFieldNumberMap()Lorg/apache/lucene/index/FieldInfos$FieldNumbers;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 778
    new-instance v2, Lorg/apache/lucene/index/FieldInfos$FieldNumbers;

    invoke-direct {v2}, Lorg/apache/lucene/index/FieldInfos$FieldNumbers;-><init>()V

    .line 780
    .local v2, "map":Lorg/apache/lucene/index/FieldInfos$FieldNumbers;
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v3}, Lorg/apache/lucene/index/SegmentInfos;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 786
    return-object v2

    .line 780
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .line 781
    .local v1, "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    iget-object v4, v1, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-direct {p0, v4}, Lorg/apache/lucene/index/IndexWriter;->getFieldInfos(Lorg/apache/lucene/index/SegmentInfo;)Lorg/apache/lucene/index/FieldInfos;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/lucene/index/FieldInfos;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/FieldInfo;

    .line 782
    .local v0, "fi":Lorg/apache/lucene/index/FieldInfo;
    iget-object v5, v0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    iget v6, v0, Lorg/apache/lucene/index/FieldInfo;->number:I

    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfo;->getDocValuesType()Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-result-object v7

    invoke-virtual {v2, v5, v6, v7}, Lorg/apache/lucene/index/FieldInfos$FieldNumbers;->addOrGet(Ljava/lang/String;ILorg/apache/lucene/index/FieldInfo$DocValuesType;)I

    goto :goto_0
.end method

.method private final handleMergeException(Ljava/lang/Throwable;Lorg/apache/lucene/index/MergePolicy$OneMerge;)V
    .locals 4
    .param p1, "t"    # Ljava/lang/Throwable;
    .param p2, "merge"    # Lorg/apache/lucene/index/MergePolicy$OneMerge;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3259
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v1, "IW"

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3260
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v1, "IW"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "handleMergeException: merge="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p2, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    invoke-virtual {p0, v3}, Lorg/apache/lucene/index/IndexWriter;->segString(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " exc="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 3266
    :cond_0
    invoke-virtual {p2, p1}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->setException(Ljava/lang/Throwable;)V

    .line 3267
    invoke-virtual {p0, p2}, Lorg/apache/lucene/index/IndexWriter;->addMergeException(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    .line 3269
    instance-of v0, p1, Lorg/apache/lucene/index/MergePolicy$MergeAbortedException;

    if-eqz v0, :cond_1

    .line 3276
    iget-boolean v0, p2, Lorg/apache/lucene/index/MergePolicy$OneMerge;->isExternal:Z

    if-eqz v0, :cond_5

    .line 3277
    check-cast p1, Lorg/apache/lucene/index/MergePolicy$MergeAbortedException;

    .end local p1    # "t":Ljava/lang/Throwable;
    throw p1

    .line 3278
    .restart local p1    # "t":Ljava/lang/Throwable;
    :cond_1
    instance-of v0, p1, Ljava/io/IOException;

    if-eqz v0, :cond_2

    .line 3279
    check-cast p1, Ljava/io/IOException;

    .end local p1    # "t":Ljava/lang/Throwable;
    throw p1

    .line 3280
    .restart local p1    # "t":Ljava/lang/Throwable;
    :cond_2
    instance-of v0, p1, Ljava/lang/RuntimeException;

    if-eqz v0, :cond_3

    .line 3281
    check-cast p1, Ljava/lang/RuntimeException;

    .end local p1    # "t":Ljava/lang/Throwable;
    throw p1

    .line 3282
    .restart local p1    # "t":Ljava/lang/Throwable;
    :cond_3
    instance-of v0, p1, Ljava/lang/Error;

    if-eqz v0, :cond_4

    .line 3283
    check-cast p1, Ljava/lang/Error;

    .end local p1    # "t":Ljava/lang/Throwable;
    throw p1

    .line 3286
    .restart local p1    # "t":Ljava/lang/Throwable;
    :cond_4
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    .line 3287
    :cond_5
    return-void
.end method

.method private handleOOM(Ljava/lang/OutOfMemoryError;Ljava/lang/String;)V
    .locals 4
    .param p1, "oom"    # Ljava/lang/OutOfMemoryError;
    .param p2, "location"    # Ljava/lang/String;

    .prologue
    .line 4135
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v1, "IW"

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4136
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v1, "IW"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "hit OutOfMemoryError inside "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 4138
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/index/IndexWriter;->hitOOM:Z

    .line 4139
    throw p1
.end method

.method public static isLocked(Lorg/apache/lucene/store/Directory;)Z
    .locals 1
    .param p0, "directory"    # Lorg/apache/lucene/store/Directory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4094
    const-string/jumbo v0, "write.lock"

    invoke-virtual {p0, v0}, Lorg/apache/lucene/store/Directory;->makeLock(Ljava/lang/String;)Lorg/apache/lucene/store/Lock;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/store/Lock;->isLocked()Z

    move-result v0

    return v0
.end method

.method private declared-synchronized maxNumSegmentsMergesPending()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v4, -0x1

    .line 1717
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->pendingMerges:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 1722
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->runningMerges:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_3

    .line 1727
    const/4 v1, 0x0

    :goto_0
    monitor-exit p0

    return v1

    .line 1717
    :cond_2
    :try_start_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;

    .line 1718
    .local v0, "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    iget v3, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->maxNumSegments:I

    if-eq v3, v4, :cond_0

    goto :goto_0

    .line 1722
    .end local v0    # "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;

    .line 1723
    .restart local v0    # "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    iget v3, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->maxNumSegments:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eq v3, v4, :cond_1

    goto :goto_0

    .line 1717
    .end local v0    # "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private final maybeMerge(Lorg/apache/lucene/index/MergePolicy$MergeTrigger;I)V
    .locals 1
    .param p1, "trigger"    # Lorg/apache/lucene/index/MergePolicy$MergeTrigger;
    .param p2, "maxNumSegments"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1857
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen(Z)V

    .line 1858
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/index/IndexWriter;->updatePendingMerges(Lorg/apache/lucene/index/MergePolicy$MergeTrigger;I)V

    .line 1859
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->mergeScheduler:Lorg/apache/lucene/index/MergeScheduler;

    invoke-virtual {v0, p0}, Lorg/apache/lucene/index/MergeScheduler;->merge(Lorg/apache/lucene/index/IndexWriter;)V

    .line 1860
    return-void
.end method

.method private mergeMiddle(Lorg/apache/lucene/index/MergePolicy$OneMerge;)I
    .locals 38
    .param p1, "merge"    # Lorg/apache/lucene/index/MergePolicy$OneMerge;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3614
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->checkAborted(Lorg/apache/lucene/store/Directory;)V

    .line 3616
    move-object/from16 v0, p1

    iget-object v5, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    iget-object v5, v5, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v0, v5, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    move-object/from16 v19, v0

    .line 3618
    .local v19, "mergedName":Ljava/lang/String;
    move-object/from16 v0, p1

    iget-object v0, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    move-object/from16 v26, v0

    .line 3620
    .local v26, "sourceSegments":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfoPerCommit;>;"
    new-instance v12, Lorg/apache/lucene/store/IOContext;

    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->getMergeInfo()Lorg/apache/lucene/store/MergeInfo;

    move-result-object v5

    invoke-direct {v12, v5}, Lorg/apache/lucene/store/IOContext;-><init>(Lorg/apache/lucene/store/MergeInfo;)V

    .line 3622
    .local v12, "context":Lorg/apache/lucene/store/IOContext;
    new-instance v10, Lorg/apache/lucene/index/MergeState$CheckAbort;

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    move-object/from16 v0, p1

    invoke-direct {v10, v0, v5}, Lorg/apache/lucene/index/MergeState$CheckAbort;-><init>(Lorg/apache/lucene/index/MergePolicy$OneMerge;Lorg/apache/lucene/store/Directory;)V

    .line 3623
    .local v10, "checkAbort":Lorg/apache/lucene/index/MergeState$CheckAbort;
    new-instance v8, Lorg/apache/lucene/store/TrackingDirectoryWrapper;

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-direct {v8, v5}, Lorg/apache/lucene/store/TrackingDirectoryWrapper;-><init>(Lorg/apache/lucene/store/Directory;)V

    .line 3625
    .local v8, "dirWrapper":Lorg/apache/lucene/store/TrackingDirectoryWrapper;
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v6, "IW"

    invoke-virtual {v5, v6}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 3626
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v6, "IW"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v9, "merging "

    invoke-direct {v7, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    iget-object v9, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lorg/apache/lucene/index/IndexWriter;->segString(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 3629
    :cond_0
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p1

    iput-object v5, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->readers:Ljava/util/List;

    .line 3633
    const/16 v28, 0x0

    .line 3635
    .local v28, "success":Z
    const/16 v25, 0x0

    .line 3636
    .local v25, "segUpto":I
    :goto_0
    :try_start_0
    invoke-interface/range {v26 .. v26}, Ljava/util/List;->size()I

    move-result v5

    move/from16 v0, v25

    if-lt v0, v5, :cond_3

    .line 3699
    new-instance v4, Lorg/apache/lucene/index/SegmentMerger;

    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->getMergeReaders()Ljava/util/List;

    move-result-object v5

    .line 3700
    move-object/from16 v0, p1

    iget-object v6, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    iget-object v6, v6, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/lucene/index/IndexWriter;->config:Lorg/apache/lucene/index/LiveIndexWriterConfig;

    invoke-virtual {v9}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getTermIndexInterval()I

    move-result v9

    .line 3701
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/index/IndexWriter;->globalFieldNumberMap:Lorg/apache/lucene/index/FieldInfos$FieldNumbers;

    .line 3699
    invoke-direct/range {v4 .. v12}, Lorg/apache/lucene/index/SegmentMerger;-><init>(Ljava/util/List;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/util/InfoStream;Lorg/apache/lucene/store/Directory;ILorg/apache/lucene/index/MergeState$CheckAbort;Lorg/apache/lucene/index/FieldInfos$FieldNumbers;Lorg/apache/lucene/store/IOContext;)V

    .line 3703
    .local v4, "merger":Lorg/apache/lucene/index/SegmentMerger;
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->checkAborted(Lorg/apache/lucene/store/Directory;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3707
    const/16 v30, 0x0

    .line 3709
    .local v30, "success3":Z
    :try_start_1
    invoke-virtual {v4}, Lorg/apache/lucene/index/SegmentMerger;->merge()Lorg/apache/lucene/index/MergeState;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    move-result-object v18

    .line 3710
    .local v18, "mergeState":Lorg/apache/lucene/index/MergeState;
    const/16 v30, 0x1

    .line 3712
    if-nez v30, :cond_1

    .line 3713
    :try_start_2
    monitor-enter p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 3714
    :try_start_3
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    move-object/from16 v0, p1

    iget-object v6, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    iget-object v6, v6, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v6, v6, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lorg/apache/lucene/index/IndexFileDeleter;->refresh(Ljava/lang/String;)V

    .line 3713
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_5

    .line 3718
    :cond_1
    :try_start_4
    sget-boolean v5, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v5, :cond_f

    move-object/from16 v0, v18

    iget-object v5, v0, Lorg/apache/lucene/index/MergeState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    move-object/from16 v0, p1

    iget-object v6, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    iget-object v6, v6, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    if-eq v5, v6, :cond_f

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 3851
    .end local v4    # "merger":Lorg/apache/lucene/index/SegmentMerger;
    .end local v18    # "mergeState":Lorg/apache/lucene/index/MergeState;
    .end local v30    # "success3":Z
    :catchall_0
    move-exception v5

    .line 3854
    if-nez v28, :cond_2

    .line 3855
    const/4 v6, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v6}, Lorg/apache/lucene/index/IndexWriter;->closeMergeReaders(Lorg/apache/lucene/index/MergePolicy$OneMerge;Z)V

    .line 3857
    :cond_2
    throw v5

    .line 3638
    :cond_3
    :try_start_5
    move-object/from16 v0, v26

    move/from16 v1, v25

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .line 3642
    .local v15, "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->readerPool:Lorg/apache/lucene/index/IndexWriter$ReaderPool;

    const/4 v6, 0x1

    invoke-virtual {v5, v15, v6}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->get(Lorg/apache/lucene/index/SegmentInfoPerCommit;Z)Lorg/apache/lucene/index/ReadersAndLiveDocs;

    move-result-object v24

    .line 3643
    .local v24, "rld":Lorg/apache/lucene/index/ReadersAndLiveDocs;
    move-object/from16 v0, v24

    invoke-virtual {v0, v12}, Lorg/apache/lucene/index/ReadersAndLiveDocs;->getMergeReader(Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/index/SegmentReader;

    move-result-object v22

    .line 3644
    .local v22, "reader":Lorg/apache/lucene/index/SegmentReader;
    sget-boolean v5, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v5, :cond_4

    if-nez v22, :cond_4

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 3650
    :cond_4
    monitor-enter p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 3654
    :try_start_6
    invoke-virtual/range {v24 .. v24}, Lorg/apache/lucene/index/ReadersAndLiveDocs;->getReadOnlyLiveDocs()Lorg/apache/lucene/util/Bits;

    move-result-object v17

    .line 3655
    .local v17, "liveDocs":Lorg/apache/lucene/util/Bits;
    invoke-virtual/range {v24 .. v24}, Lorg/apache/lucene/index/ReadersAndLiveDocs;->getPendingDeleteCount()I

    move-result v5

    invoke-virtual {v15}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->getDelCount()I

    move-result v6

    add-int v13, v5, v6

    .line 3657
    .local v13, "delCount":I
    sget-boolean v5, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v5, :cond_5

    invoke-virtual/range {v24 .. v24}, Lorg/apache/lucene/index/ReadersAndLiveDocs;->verifyDocCounts()Z

    move-result v5

    if-nez v5, :cond_5

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 3650
    .end local v13    # "delCount":I
    .end local v17    # "liveDocs":Lorg/apache/lucene/util/Bits;
    :catchall_1
    move-exception v5

    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :try_start_7
    throw v5
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 3659
    .restart local v13    # "delCount":I
    .restart local v17    # "liveDocs":Lorg/apache/lucene/util/Bits;
    :cond_5
    :try_start_8
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v6, "IW"

    invoke-virtual {v5, v6}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 3660
    invoke-virtual/range {v24 .. v24}, Lorg/apache/lucene/index/ReadersAndLiveDocs;->getPendingDeleteCount()I

    move-result v5

    if-eqz v5, :cond_7

    .line 3661
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v6, "IW"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v9, "seg="

    invoke-direct {v7, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lorg/apache/lucene/index/IndexWriter;->segString(Lorg/apache/lucene/index/SegmentInfoPerCommit;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, " delCount="

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v15}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->getDelCount()I

    move-result v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, " pendingDelCount="

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual/range {v24 .. v24}, Lorg/apache/lucene/index/ReadersAndLiveDocs;->getPendingDeleteCount()I

    move-result v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 3650
    :cond_6
    :goto_1
    monitor-exit p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 3674
    :try_start_9
    invoke-virtual/range {v22 .. v22}, Lorg/apache/lucene/index/SegmentReader;->numDeletedDocs()I

    move-result v5

    if-eq v5, v13, :cond_b

    .line 3676
    sget-boolean v5, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v5, :cond_9

    invoke-virtual/range {v22 .. v22}, Lorg/apache/lucene/index/SegmentReader;->numDeletedDocs()I

    move-result v5

    if-gt v13, v5, :cond_9

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 3662
    :cond_7
    :try_start_a
    invoke-virtual {v15}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->getDelCount()I

    move-result v5

    if-eqz v5, :cond_8

    .line 3663
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v6, "IW"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v9, "seg="

    invoke-direct {v7, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lorg/apache/lucene/index/IndexWriter;->segString(Lorg/apache/lucene/index/SegmentInfoPerCommit;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, " delCount="

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v15}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->getDelCount()I

    move-result v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 3665
    :cond_8
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v6, "IW"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v9, "seg="

    invoke-direct {v7, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lorg/apache/lucene/index/IndexWriter;->segString(Lorg/apache/lucene/index/SegmentInfoPerCommit;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, " no deletes"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    goto :goto_1

    .line 3678
    :cond_9
    :try_start_b
    new-instance v21, Lorg/apache/lucene/index/SegmentReader;

    move-object/from16 v0, v22

    iget-object v5, v0, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    iget-object v6, v15, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v6}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v6

    sub-int/2addr v6, v13

    move-object/from16 v0, v21

    move-object/from16 v1, v17

    invoke-direct {v0, v15, v5, v1, v6}, Lorg/apache/lucene/index/SegmentReader;-><init>(Lorg/apache/lucene/index/SegmentInfoPerCommit;Lorg/apache/lucene/index/SegmentCoreReaders;Lorg/apache/lucene/util/Bits;I)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 3679
    .local v21, "newReader":Lorg/apache/lucene/index/SegmentReader;
    const/16 v23, 0x0

    .line 3681
    .local v23, "released":Z
    :try_start_c
    move-object/from16 v0, v24

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/ReadersAndLiveDocs;->release(Lorg/apache/lucene/index/SegmentReader;)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    .line 3682
    const/16 v23, 0x1

    .line 3684
    if-nez v23, :cond_a

    .line 3685
    :try_start_d
    invoke-virtual/range {v21 .. v21}, Lorg/apache/lucene/index/SegmentReader;->decRef()V

    .line 3689
    :cond_a
    move-object/from16 v22, v21

    .line 3692
    .end local v21    # "newReader":Lorg/apache/lucene/index/SegmentReader;
    .end local v23    # "released":Z
    :cond_b
    move-object/from16 v0, p1

    iget-object v5, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->readers:Ljava/util/List;

    move-object/from16 v0, v22

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3693
    sget-boolean v5, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v5, :cond_d

    iget-object v5, v15, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v5}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v5

    if-le v13, v5, :cond_d

    new-instance v5, Ljava/lang/AssertionError;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "delCount="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " info.docCount="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v15, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v7}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " rld.pendingDeleteCount="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {v24 .. v24}, Lorg/apache/lucene/index/ReadersAndLiveDocs;->getPendingDeleteCount()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " info.getDelCount()="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v15}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->getDelCount()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v5

    .line 3683
    .restart local v21    # "newReader":Lorg/apache/lucene/index/SegmentReader;
    .restart local v23    # "released":Z
    :catchall_2
    move-exception v5

    .line 3684
    if-nez v23, :cond_c

    .line 3685
    invoke-virtual/range {v21 .. v21}, Lorg/apache/lucene/index/SegmentReader;->decRef()V

    .line 3687
    :cond_c
    throw v5

    .line 3694
    .end local v21    # "newReader":Lorg/apache/lucene/index/SegmentReader;
    .end local v23    # "released":Z
    :cond_d
    add-int/lit8 v25, v25, 0x1

    goto/16 :goto_0

    .line 3711
    .end local v13    # "delCount":I
    .end local v15    # "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .end local v17    # "liveDocs":Lorg/apache/lucene/util/Bits;
    .end local v22    # "reader":Lorg/apache/lucene/index/SegmentReader;
    .end local v24    # "rld":Lorg/apache/lucene/index/ReadersAndLiveDocs;
    .restart local v4    # "merger":Lorg/apache/lucene/index/SegmentMerger;
    .restart local v30    # "success3":Z
    :catchall_3
    move-exception v5

    .line 3712
    if-nez v30, :cond_e

    .line 3713
    monitor-enter p0
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 3714
    :try_start_e
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    move-object/from16 v0, p1

    iget-object v7, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    iget-object v7, v7, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v7, v7, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lorg/apache/lucene/index/IndexFileDeleter;->refresh(Ljava/lang/String;)V

    .line 3713
    monitor-exit p0
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_4

    .line 3717
    :cond_e
    :try_start_f
    throw v5
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    .line 3713
    :catchall_4
    move-exception v5

    :try_start_10
    monitor-exit p0
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_4

    :try_start_11
    throw v5
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    .restart local v18    # "mergeState":Lorg/apache/lucene/index/MergeState;
    :catchall_5
    move-exception v5

    :try_start_12
    monitor-exit p0
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_5

    :try_start_13
    throw v5

    .line 3719
    :cond_f
    move-object/from16 v0, p1

    iget-object v5, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    iget-object v5, v5, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    new-instance v6, Ljava/util/HashSet;

    invoke-virtual {v8}, Lorg/apache/lucene/store/TrackingDirectoryWrapper;->getCreatedFiles()Ljava/util/Set;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v5, v6}, Lorg/apache/lucene/index/SegmentInfo;->setFiles(Ljava/util/Set;)V

    .line 3723
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v6, "IW"

    invoke-virtual {v5, v6}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_10

    .line 3724
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v7, "IW"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v9, "merge codec="

    invoke-direct {v5, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/lucene/index/IndexWriter;->codec:Lorg/apache/lucene/codecs/Codec;

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, " docCount="

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    iget-object v9, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    iget-object v9, v9, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v9}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v9

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, "; merged segment has "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 3725
    move-object/from16 v0, v18

    iget-object v5, v0, Lorg/apache/lucene/index/MergeState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v5}, Lorg/apache/lucene/index/FieldInfos;->hasVectors()Z

    move-result v5

    if-eqz v5, :cond_15

    const-string v5, "vectors"

    :goto_2
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, "; "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 3726
    move-object/from16 v0, v18

    iget-object v5, v0, Lorg/apache/lucene/index/MergeState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v5}, Lorg/apache/lucene/index/FieldInfos;->hasNorms()Z

    move-result v5

    if-eqz v5, :cond_16

    const-string v5, "norms"

    :goto_3
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, "; "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 3727
    move-object/from16 v0, v18

    iget-object v5, v0, Lorg/apache/lucene/index/MergeState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v5}, Lorg/apache/lucene/index/FieldInfos;->hasDocValues()Z

    move-result v5

    if-eqz v5, :cond_17

    const-string v5, "docValues"

    :goto_4
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, "; "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 3728
    move-object/from16 v0, v18

    iget-object v5, v0, Lorg/apache/lucene/index/MergeState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v5}, Lorg/apache/lucene/index/FieldInfos;->hasProx()Z

    move-result v5

    if-eqz v5, :cond_18

    const-string v5, "prox"

    :goto_5
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, "; "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 3729
    move-object/from16 v0, v18

    iget-object v5, v0, Lorg/apache/lucene/index/MergeState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v5}, Lorg/apache/lucene/index/FieldInfos;->hasProx()Z

    move-result v5

    if-eqz v5, :cond_19

    const-string v5, "freqs"

    :goto_6
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 3724
    invoke-virtual {v6, v7, v5}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 3737
    :cond_10
    monitor-enter p0
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_0

    .line 3738
    :try_start_14
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->mergePolicy:Lorg/apache/lucene/index/MergePolicy;

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    move-object/from16 v0, p1

    iget-object v7, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    invoke-virtual {v5, v6, v7}, Lorg/apache/lucene/index/MergePolicy;->useCompoundFile(Lorg/apache/lucene/index/SegmentInfos;Lorg/apache/lucene/index/SegmentInfoPerCommit;)Z

    move-result v32

    .line 3737
    .local v32, "useCompoundFile":Z
    monitor-exit p0
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_6

    .line 3741
    if-eqz v32, :cond_24

    .line 3742
    const/16 v28, 0x0

    .line 3744
    :try_start_15
    move-object/from16 v0, p1

    iget-object v5, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    invoke-virtual {v5}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->files()Ljava/util/Collection;
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_0

    move-result-object v14

    .line 3747
    .local v14, "filesToRemove":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    :try_start_16
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    move-object/from16 v0, p1

    iget-object v7, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    iget-object v7, v7, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-static {v5, v6, v10, v7, v12}, Lorg/apache/lucene/index/IndexWriter;->createCompoundFile(Lorg/apache/lucene/util/InfoStream;Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/MergeState$CheckAbort;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/store/IOContext;)Ljava/util/Collection;
    :try_end_16
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_16} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_16 .. :try_end_16} :catch_1
    .catchall {:try_start_16 .. :try_end_16} :catchall_9

    move-result-object v14

    .line 3748
    const/16 v28, 0x1

    .line 3762
    if-nez v28, :cond_12

    .line 3763
    :try_start_17
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v6, "IW"

    invoke-virtual {v5, v6}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_11

    .line 3764
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v6, "IW"

    const-string v7, "hit exception creating compound file during merge"

    invoke-virtual {v5, v6, v7}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 3767
    :cond_11
    monitor-enter p0
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_0

    .line 3768
    :try_start_18
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    const-string v6, ""

    const-string v7, "cfs"

    move-object/from16 v0, v19

    invoke-static {v0, v6, v7}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lorg/apache/lucene/index/IndexFileDeleter;->deleteFile(Ljava/lang/String;)V

    .line 3769
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    const-string v6, ""

    const-string v7, "cfe"

    move-object/from16 v0, v19

    invoke-static {v0, v6, v7}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lorg/apache/lucene/index/IndexFileDeleter;->deleteFile(Ljava/lang/String;)V

    .line 3770
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    move-object/from16 v0, p1

    iget-object v6, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    invoke-virtual {v6}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->files()Ljava/util/Collection;

    move-result-object v6

    invoke-virtual {v5, v6}, Lorg/apache/lucene/index/IndexFileDeleter;->deleteNewFiles(Ljava/util/Collection;)V

    .line 3767
    monitor-exit p0
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_c

    .line 3778
    :cond_12
    :goto_7
    const/16 v28, 0x0

    .line 3780
    :try_start_19
    monitor-enter p0
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_0

    .line 3784
    :try_start_1a
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    invoke-virtual {v5, v14}, Lorg/apache/lucene/index/IndexFileDeleter;->deleteNewFiles(Ljava/util/Collection;)V

    .line 3786
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->isAborted()Z

    move-result v5

    if-eqz v5, :cond_1f

    .line 3787
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v6, "IW"

    invoke-virtual {v5, v6}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_13

    .line 3788
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v6, "IW"

    const-string v7, "abort merge after building CFS"

    invoke-virtual {v5, v6, v7}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 3790
    :cond_13
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    const-string v6, ""

    const-string v7, "cfs"

    move-object/from16 v0, v19

    invoke-static {v0, v6, v7}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lorg/apache/lucene/index/IndexFileDeleter;->deleteFile(Ljava/lang/String;)V

    .line 3791
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    const-string v6, ""

    const-string v7, "cfe"

    move-object/from16 v0, v19

    invoke-static {v0, v6, v7}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lorg/apache/lucene/index/IndexFileDeleter;->deleteFile(Ljava/lang/String;)V

    .line 3792
    monitor-exit p0
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_d

    .line 3854
    if-nez v28, :cond_14

    .line 3855
    const/4 v5, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v5}, Lorg/apache/lucene/index/IndexWriter;->closeMergeReaders(Lorg/apache/lucene/index/MergePolicy$OneMerge;Z)V

    :cond_14
    const/4 v5, 0x0

    .line 3859
    .end local v14    # "filesToRemove":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    :goto_8
    return v5

    .line 3725
    .end local v32    # "useCompoundFile":Z
    :cond_15
    :try_start_1b
    const-string v5, "no vectors"

    goto/16 :goto_2

    .line 3726
    :cond_16
    const-string v5, "no norms"

    goto/16 :goto_3

    .line 3727
    :cond_17
    const-string v5, "no docValues"

    goto/16 :goto_4

    .line 3728
    :cond_18
    const-string v5, "no prox"

    goto/16 :goto_5

    .line 3729
    :cond_19
    const-string v5, "no freqs"
    :try_end_1b
    .catchall {:try_start_1b .. :try_end_1b} :catchall_0

    goto/16 :goto_6

    .line 3737
    :catchall_6
    move-exception v5

    :try_start_1c
    monitor-exit p0
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_6

    :try_start_1d
    throw v5
    :try_end_1d
    .catchall {:try_start_1d .. :try_end_1d} :catchall_0

    .line 3749
    .restart local v14    # "filesToRemove":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    .restart local v32    # "useCompoundFile":Z
    :catch_0
    move-exception v16

    .line 3750
    .local v16, "ioe":Ljava/io/IOException;
    :try_start_1e
    monitor-enter p0
    :try_end_1e
    .catchall {:try_start_1e .. :try_end_1e} :catchall_9

    .line 3751
    :try_start_1f
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->isAborted()Z

    move-result v5

    if-nez v5, :cond_1a

    .line 3756
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/index/IndexWriter;->handleMergeException(Ljava/lang/Throwable;Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    .line 3750
    :cond_1a
    monitor-exit p0
    :try_end_1f
    .catchall {:try_start_1f .. :try_end_1f} :catchall_8

    .line 3762
    if-nez v28, :cond_12

    .line 3763
    :try_start_20
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v6, "IW"

    invoke-virtual {v5, v6}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1b

    .line 3764
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v6, "IW"

    const-string v7, "hit exception creating compound file during merge"

    invoke-virtual {v5, v6, v7}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 3767
    :cond_1b
    monitor-enter p0
    :try_end_20
    .catchall {:try_start_20 .. :try_end_20} :catchall_0

    .line 3768
    :try_start_21
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    const-string v6, ""

    const-string v7, "cfs"

    move-object/from16 v0, v19

    invoke-static {v0, v6, v7}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lorg/apache/lucene/index/IndexFileDeleter;->deleteFile(Ljava/lang/String;)V

    .line 3769
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    const-string v6, ""

    const-string v7, "cfe"

    move-object/from16 v0, v19

    invoke-static {v0, v6, v7}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lorg/apache/lucene/index/IndexFileDeleter;->deleteFile(Ljava/lang/String;)V

    .line 3770
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    move-object/from16 v0, p1

    iget-object v6, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    invoke-virtual {v6}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->files()Ljava/util/Collection;

    move-result-object v6

    invoke-virtual {v5, v6}, Lorg/apache/lucene/index/IndexFileDeleter;->deleteNewFiles(Ljava/util/Collection;)V

    .line 3767
    monitor-exit p0

    goto/16 :goto_7

    :catchall_7
    move-exception v5

    monitor-exit p0
    :try_end_21
    .catchall {:try_start_21 .. :try_end_21} :catchall_7

    :try_start_22
    throw v5
    :try_end_22
    .catchall {:try_start_22 .. :try_end_22} :catchall_0

    .line 3750
    :catchall_8
    move-exception v5

    :try_start_23
    monitor-exit p0
    :try_end_23
    .catchall {:try_start_23 .. :try_end_23} :catchall_8

    :try_start_24
    throw v5
    :try_end_24
    .catchall {:try_start_24 .. :try_end_24} :catchall_9

    .line 3761
    .end local v16    # "ioe":Ljava/io/IOException;
    :catchall_9
    move-exception v5

    .line 3762
    if-nez v28, :cond_1d

    .line 3763
    :try_start_25
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v7, "IW"

    invoke-virtual {v6, v7}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1c

    .line 3764
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v7, "IW"

    const-string v9, "hit exception creating compound file during merge"

    invoke-virtual {v6, v7, v9}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 3767
    :cond_1c
    monitor-enter p0
    :try_end_25
    .catchall {:try_start_25 .. :try_end_25} :catchall_0

    .line 3768
    :try_start_26
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    const-string v7, ""

    const-string v9, "cfs"

    move-object/from16 v0, v19

    invoke-static {v0, v7, v9}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lorg/apache/lucene/index/IndexFileDeleter;->deleteFile(Ljava/lang/String;)V

    .line 3769
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    const-string v7, ""

    const-string v9, "cfe"

    move-object/from16 v0, v19

    invoke-static {v0, v7, v9}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lorg/apache/lucene/index/IndexFileDeleter;->deleteFile(Ljava/lang/String;)V

    .line 3770
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    move-object/from16 v0, p1

    iget-object v7, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    invoke-virtual {v7}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->files()Ljava/util/Collection;

    move-result-object v7

    invoke-virtual {v6, v7}, Lorg/apache/lucene/index/IndexFileDeleter;->deleteNewFiles(Ljava/util/Collection;)V

    .line 3767
    monitor-exit p0
    :try_end_26
    .catchall {:try_start_26 .. :try_end_26} :catchall_b

    .line 3773
    :cond_1d
    :try_start_27
    throw v5
    :try_end_27
    .catchall {:try_start_27 .. :try_end_27} :catchall_0

    .line 3759
    :catch_1
    move-exception v31

    .line 3760
    .local v31, "t":Ljava/lang/Throwable;
    :try_start_28
    move-object/from16 v0, p0

    move-object/from16 v1, v31

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/index/IndexWriter;->handleMergeException(Ljava/lang/Throwable;Lorg/apache/lucene/index/MergePolicy$OneMerge;)V
    :try_end_28
    .catchall {:try_start_28 .. :try_end_28} :catchall_9

    .line 3762
    if-nez v28, :cond_12

    .line 3763
    :try_start_29
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v6, "IW"

    invoke-virtual {v5, v6}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1e

    .line 3764
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v6, "IW"

    const-string v7, "hit exception creating compound file during merge"

    invoke-virtual {v5, v6, v7}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 3767
    :cond_1e
    monitor-enter p0
    :try_end_29
    .catchall {:try_start_29 .. :try_end_29} :catchall_0

    .line 3768
    :try_start_2a
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    const-string v6, ""

    const-string v7, "cfs"

    move-object/from16 v0, v19

    invoke-static {v0, v6, v7}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lorg/apache/lucene/index/IndexFileDeleter;->deleteFile(Ljava/lang/String;)V

    .line 3769
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    const-string v6, ""

    const-string v7, "cfe"

    move-object/from16 v0, v19

    invoke-static {v0, v6, v7}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lorg/apache/lucene/index/IndexFileDeleter;->deleteFile(Ljava/lang/String;)V

    .line 3770
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    move-object/from16 v0, p1

    iget-object v6, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    invoke-virtual {v6}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->files()Ljava/util/Collection;

    move-result-object v6

    invoke-virtual {v5, v6}, Lorg/apache/lucene/index/IndexFileDeleter;->deleteNewFiles(Ljava/util/Collection;)V

    .line 3767
    monitor-exit p0

    goto/16 :goto_7

    :catchall_a
    move-exception v5

    monitor-exit p0
    :try_end_2a
    .catchall {:try_start_2a .. :try_end_2a} :catchall_a

    :try_start_2b
    throw v5
    :try_end_2b
    .catchall {:try_start_2b .. :try_end_2b} :catchall_0

    .end local v31    # "t":Ljava/lang/Throwable;
    :catchall_b
    move-exception v5

    :try_start_2c
    monitor-exit p0
    :try_end_2c
    .catchall {:try_start_2c .. :try_end_2c} :catchall_b

    :try_start_2d
    throw v5
    :try_end_2d
    .catchall {:try_start_2d .. :try_end_2d} :catchall_0

    :catchall_c
    move-exception v5

    :try_start_2e
    monitor-exit p0
    :try_end_2e
    .catchall {:try_start_2e .. :try_end_2e} :catchall_c

    :try_start_2f
    throw v5
    :try_end_2f
    .catchall {:try_start_2f .. :try_end_2f} :catchall_0

    .line 3780
    :cond_1f
    :try_start_30
    monitor-exit p0
    :try_end_30
    .catchall {:try_start_30 .. :try_end_30} :catchall_d

    .line 3796
    :try_start_31
    move-object/from16 v0, p1

    iget-object v5, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    iget-object v5, v5, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lorg/apache/lucene/index/SegmentInfo;->setUseCompoundFile(Z)V
    :try_end_31
    .catchall {:try_start_31 .. :try_end_31} :catchall_0

    .line 3808
    .end local v14    # "filesToRemove":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    :goto_9
    const/16 v29, 0x0

    .line 3810
    .local v29, "success2":Z
    :try_start_32
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->codec:Lorg/apache/lucene/codecs/Codec;

    invoke-virtual {v5}, Lorg/apache/lucene/codecs/Codec;->segmentInfoFormat()Lorg/apache/lucene/codecs/SegmentInfoFormat;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/lucene/codecs/SegmentInfoFormat;->getSegmentInfoWriter()Lorg/apache/lucene/codecs/SegmentInfoWriter;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    move-object/from16 v0, p1

    iget-object v7, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    iget-object v7, v7, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    move-object/from16 v0, v18

    iget-object v9, v0, Lorg/apache/lucene/index/MergeState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v5, v6, v7, v9, v12}, Lorg/apache/lucene/codecs/SegmentInfoWriter;->write(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/store/IOContext;)V
    :try_end_32
    .catchall {:try_start_32 .. :try_end_32} :catchall_e

    .line 3811
    const/16 v29, 0x1

    .line 3813
    if-nez v29, :cond_20

    .line 3814
    :try_start_33
    monitor-enter p0
    :try_end_33
    .catchall {:try_start_33 .. :try_end_33} :catchall_0

    .line 3815
    :try_start_34
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    move-object/from16 v0, p1

    iget-object v6, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    invoke-virtual {v6}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->files()Ljava/util/Collection;

    move-result-object v6

    invoke-virtual {v5, v6}, Lorg/apache/lucene/index/IndexFileDeleter;->deleteNewFiles(Ljava/util/Collection;)V

    .line 3814
    monitor-exit p0
    :try_end_34
    .catchall {:try_start_34 .. :try_end_34} :catchall_10

    .line 3824
    :cond_20
    :try_start_35
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v6, "IW"

    invoke-virtual {v5, v6}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_21

    .line 3825
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v6, "IW"

    sget-object v7, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    const-string v9, "merged segment size=%.3f MB vs estimate=%.3f MB"

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/16 v33, 0x0

    move-object/from16 v0, p1

    iget-object v0, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->sizeInBytes()J

    move-result-wide v34

    move-wide/from16 v0, v34

    long-to-double v0, v0

    move-wide/from16 v34, v0

    const-wide/high16 v36, 0x4090000000000000L    # 1024.0

    div-double v34, v34, v36

    const-wide/high16 v36, 0x4090000000000000L    # 1024.0

    div-double v34, v34, v36

    invoke-static/range {v34 .. v35}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v34

    aput-object v34, v11, v33

    const/16 v33, 0x1

    move-object/from16 v0, p1

    iget-wide v0, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->estimatedMergeBytes:J

    move-wide/from16 v34, v0

    const-wide/16 v36, 0x400

    div-long v34, v34, v36

    move-wide/from16 v0, v34

    long-to-double v0, v0

    move-wide/from16 v34, v0

    const-wide/high16 v36, 0x4090000000000000L    # 1024.0

    div-double v34, v34, v36

    invoke-static/range {v34 .. v35}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v34

    aput-object v34, v11, v33

    invoke-static {v7, v9, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 3828
    :cond_21
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->config:Lorg/apache/lucene/index/LiveIndexWriterConfig;

    invoke-virtual {v5}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getMergedSegmentWarmer()Lorg/apache/lucene/index/IndexWriter$IndexReaderWarmer;

    move-result-object v20

    .line 3829
    .local v20, "mergedSegmentWarmer":Lorg/apache/lucene/index/IndexWriter$IndexReaderWarmer;
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lorg/apache/lucene/index/IndexWriter;->poolReaders:Z

    if-eqz v5, :cond_22

    if-eqz v20, :cond_22

    move-object/from16 v0, p1

    iget-object v5, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    iget-object v5, v5, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v5}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v5

    if-eqz v5, :cond_22

    .line 3830
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->readerPool:Lorg/apache/lucene/index/IndexWriter$ReaderPool;

    move-object/from16 v0, p1

    iget-object v6, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    const/4 v7, 0x1

    invoke-virtual {v5, v6, v7}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->get(Lorg/apache/lucene/index/SegmentInfoPerCommit;Z)Lorg/apache/lucene/index/ReadersAndLiveDocs;

    move-result-object v24

    .line 3831
    .restart local v24    # "rld":Lorg/apache/lucene/index/ReadersAndLiveDocs;
    sget-object v5, Lorg/apache/lucene/store/IOContext;->READ:Lorg/apache/lucene/store/IOContext;

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Lorg/apache/lucene/index/ReadersAndLiveDocs;->getReader(Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/index/SegmentReader;
    :try_end_35
    .catchall {:try_start_35 .. :try_end_35} :catchall_0

    move-result-object v27

    .line 3833
    .local v27, "sr":Lorg/apache/lucene/index/SegmentReader;
    :try_start_36
    move-object/from16 v0, v20

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/IndexWriter$IndexReaderWarmer;->warm(Lorg/apache/lucene/index/AtomicReader;)V
    :try_end_36
    .catchall {:try_start_36 .. :try_end_36} :catchall_11

    .line 3835
    :try_start_37
    monitor-enter p0
    :try_end_37
    .catchall {:try_start_37 .. :try_end_37} :catchall_0

    .line 3836
    :try_start_38
    move-object/from16 v0, v24

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/ReadersAndLiveDocs;->release(Lorg/apache/lucene/index/SegmentReader;)V

    .line 3837
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->readerPool:Lorg/apache/lucene/index/IndexWriter$ReaderPool;

    move-object/from16 v0, v24

    invoke-virtual {v5, v0}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->release(Lorg/apache/lucene/index/ReadersAndLiveDocs;)V

    .line 3835
    monitor-exit p0
    :try_end_38
    .catchall {:try_start_38 .. :try_end_38} :catchall_13

    .line 3844
    .end local v24    # "rld":Lorg/apache/lucene/index/ReadersAndLiveDocs;
    .end local v27    # "sr":Lorg/apache/lucene/index/SegmentReader;
    :cond_22
    :try_start_39
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/index/IndexWriter;->commitMerge(Lorg/apache/lucene/index/MergePolicy$OneMerge;Lorg/apache/lucene/index/MergeState;)Z
    :try_end_39
    .catchall {:try_start_39 .. :try_end_39} :catchall_0

    move-result v5

    if-nez v5, :cond_26

    .line 3854
    if-nez v28, :cond_23

    .line 3855
    const/4 v5, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v5}, Lorg/apache/lucene/index/IndexWriter;->closeMergeReaders(Lorg/apache/lucene/index/MergePolicy$OneMerge;Z)V

    .line 3846
    :cond_23
    const/4 v5, 0x0

    goto/16 :goto_8

    .line 3780
    .end local v20    # "mergedSegmentWarmer":Lorg/apache/lucene/index/IndexWriter$IndexReaderWarmer;
    .end local v29    # "success2":Z
    .restart local v14    # "filesToRemove":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    :catchall_d
    move-exception v5

    :try_start_3a
    monitor-exit p0
    :try_end_3a
    .catchall {:try_start_3a .. :try_end_3a} :catchall_d

    :try_start_3b
    throw v5

    .line 3801
    .end local v14    # "filesToRemove":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    :cond_24
    const/16 v28, 0x0

    goto/16 :goto_9

    .line 3812
    .restart local v29    # "success2":Z
    :catchall_e
    move-exception v5

    .line 3813
    if-nez v29, :cond_25

    .line 3814
    monitor-enter p0
    :try_end_3b
    .catchall {:try_start_3b .. :try_end_3b} :catchall_0

    .line 3815
    :try_start_3c
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    move-object/from16 v0, p1

    iget-object v7, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    invoke-virtual {v7}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->files()Ljava/util/Collection;

    move-result-object v7

    invoke-virtual {v6, v7}, Lorg/apache/lucene/index/IndexFileDeleter;->deleteNewFiles(Ljava/util/Collection;)V

    .line 3814
    monitor-exit p0
    :try_end_3c
    .catchall {:try_start_3c .. :try_end_3c} :catchall_f

    .line 3818
    :cond_25
    :try_start_3d
    throw v5
    :try_end_3d
    .catchall {:try_start_3d .. :try_end_3d} :catchall_0

    .line 3814
    :catchall_f
    move-exception v5

    :try_start_3e
    monitor-exit p0
    :try_end_3e
    .catchall {:try_start_3e .. :try_end_3e} :catchall_f

    :try_start_3f
    throw v5
    :try_end_3f
    .catchall {:try_start_3f .. :try_end_3f} :catchall_0

    :catchall_10
    move-exception v5

    :try_start_40
    monitor-exit p0
    :try_end_40
    .catchall {:try_start_40 .. :try_end_40} :catchall_10

    :try_start_41
    throw v5

    .line 3834
    .restart local v20    # "mergedSegmentWarmer":Lorg/apache/lucene/index/IndexWriter$IndexReaderWarmer;
    .restart local v24    # "rld":Lorg/apache/lucene/index/ReadersAndLiveDocs;
    .restart local v27    # "sr":Lorg/apache/lucene/index/SegmentReader;
    :catchall_11
    move-exception v5

    .line 3835
    monitor-enter p0
    :try_end_41
    .catchall {:try_start_41 .. :try_end_41} :catchall_0

    .line 3836
    :try_start_42
    move-object/from16 v0, v24

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/ReadersAndLiveDocs;->release(Lorg/apache/lucene/index/SegmentReader;)V

    .line 3837
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/index/IndexWriter;->readerPool:Lorg/apache/lucene/index/IndexWriter$ReaderPool;

    move-object/from16 v0, v24

    invoke-virtual {v6, v0}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->release(Lorg/apache/lucene/index/ReadersAndLiveDocs;)V

    .line 3835
    monitor-exit p0
    :try_end_42
    .catchall {:try_start_42 .. :try_end_42} :catchall_12

    .line 3839
    :try_start_43
    throw v5
    :try_end_43
    .catchall {:try_start_43 .. :try_end_43} :catchall_0

    .line 3835
    :catchall_12
    move-exception v5

    :try_start_44
    monitor-exit p0
    :try_end_44
    .catchall {:try_start_44 .. :try_end_44} :catchall_12

    :try_start_45
    throw v5
    :try_end_45
    .catchall {:try_start_45 .. :try_end_45} :catchall_0

    :catchall_13
    move-exception v5

    :try_start_46
    monitor-exit p0
    :try_end_46
    .catchall {:try_start_46 .. :try_end_46} :catchall_13

    :try_start_47
    throw v5
    :try_end_47
    .catchall {:try_start_47 .. :try_end_47} :catchall_0

    .line 3849
    .end local v24    # "rld":Lorg/apache/lucene/index/ReadersAndLiveDocs;
    .end local v27    # "sr":Lorg/apache/lucene/index/SegmentReader;
    :cond_26
    const/16 v28, 0x1

    .line 3854
    if-nez v28, :cond_27

    .line 3855
    const/4 v5, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v5}, Lorg/apache/lucene/index/IndexWriter;->closeMergeReaders(Lorg/apache/lucene/index/MergePolicy$OneMerge;Z)V

    .line 3859
    :cond_27
    move-object/from16 v0, p1

    iget-object v5, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    iget-object v5, v5, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v5}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v5

    goto/16 :goto_8
.end method

.method private messageState()V
    .locals 4

    .prologue
    .line 799
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v1, "IW"

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 800
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v1, "IW"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "\ndir="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 801
    const-string v3, "index="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->segString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 802
    const-string/jumbo v3, "version="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lorg/apache/lucene/util/Constants;->LUCENE_VERSION:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 803
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->config:Lorg/apache/lucene/index/LiveIndexWriterConfig;

    invoke-virtual {v3}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 800
    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 805
    :cond_0
    return-void
.end method

.method private varargs noDupDirs([Lorg/apache/lucene/store/Directory;)V
    .locals 5
    .param p1, "dirs"    # [Lorg/apache/lucene/store/Directory;

    .prologue
    .line 2232
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 2233
    .local v0, "dups":Ljava/util/HashSet;, "Ljava/util/HashSet<Lorg/apache/lucene/store/Directory;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, p1

    if-lt v1, v2, :cond_0

    .line 2240
    return-void

    .line 2234
    :cond_0
    aget-object v2, p1, v1

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2235
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Directory "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v4, p1, v1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " appears more than once"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 2236
    :cond_1
    aget-object v2, p1, v1

    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    if-ne v2, v3, :cond_2

    .line 2237
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Cannot add directory to itself"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 2238
    :cond_2
    aget-object v2, p1, v1

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 2233
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private prepareCommitInternal()V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2665
    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->commitLock:Ljava/lang/Object;

    monitor-enter v6

    .line 2666
    const/4 v5, 0x0

    :try_start_0
    invoke-virtual {p0, v5}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen(Z)V

    .line 2667
    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v7, "IW"

    invoke-virtual {v5, v7}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2668
    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v7, "IW"

    const-string v8, "prepareCommit: flush"

    invoke-virtual {v5, v7, v8}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 2669
    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v7, "IW"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "  index before flush "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->segString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 2672
    :cond_0
    iget-boolean v5, p0, Lorg/apache/lucene/index/IndexWriter;->hitOOM:Z

    if-eqz v5, :cond_1

    .line 2673
    new-instance v5, Ljava/lang/IllegalStateException;

    const-string v7, "this writer hit an OutOfMemoryError; cannot commit"

    invoke-direct {v5, v7}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 2665
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5

    .line 2676
    :cond_1
    :try_start_1
    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->pendingCommit:Lorg/apache/lucene/index/SegmentInfos;

    if-eqz v5, :cond_2

    .line 2677
    new-instance v5, Ljava/lang/IllegalStateException;

    const-string v7, "prepareCommit was already called with no corresponding call to commit"

    invoke-direct {v5, v7}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 2680
    :cond_2
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->doBeforeFlush()V

    .line 2681
    sget-boolean v5, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v5, :cond_3

    const-string v5, "startDoFlush"

    invoke-virtual {p0, v5}, Lorg/apache/lucene/index/IndexWriter;->testPoint(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_3

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2682
    :cond_3
    const/4 v4, 0x0

    .line 2683
    .local v4, "toCommit":Lorg/apache/lucene/index/SegmentInfos;
    const/4 v0, 0x0

    .line 2691
    .local v0, "anySegmentsFlushed":Z
    :try_start_2
    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->fullFlushLock:Ljava/lang/Object;

    monitor-enter v7
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2692
    const/4 v1, 0x0

    .line 2693
    .local v1, "flushSuccess":Z
    const/4 v3, 0x0

    .line 2695
    .local v3, "success":Z
    :try_start_3
    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v5}, Lorg/apache/lucene/index/DocumentsWriter;->flushAllThreads()Z

    move-result v0

    .line 2696
    if-nez v0, :cond_4

    .line 2699
    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->flushCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 2701
    :cond_4
    const/4 v1, 0x1

    .line 2703
    monitor-enter p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 2704
    const/4 v5, 0x1

    :try_start_4
    invoke-virtual {p0, v5}, Lorg/apache/lucene/index/IndexWriter;->maybeApplyDeletes(Z)V

    .line 2706
    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->readerPool:Lorg/apache/lucene/index/IndexWriter$ReaderPool;

    iget-object v8, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v5, v8}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->commit(Lorg/apache/lucene/index/SegmentInfos;)V

    .line 2713
    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v5}, Lorg/apache/lucene/index/SegmentInfos;->clone()Lorg/apache/lucene/index/SegmentInfos;

    move-result-object v4

    .line 2715
    iget-wide v8, p0, Lorg/apache/lucene/index/IndexWriter;->changeCount:J

    iput-wide v8, p0, Lorg/apache/lucene/index/IndexWriter;->pendingCommitChangeCount:J

    .line 2722
    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    const/4 v8, 0x0

    invoke-virtual {v4, v5, v8}, Lorg/apache/lucene/index/SegmentInfos;->files(Lorg/apache/lucene/store/Directory;Z)Ljava/util/Collection;

    move-result-object v5

    iput-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->filesToCommit:Ljava/util/Collection;

    .line 2723
    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    iget-object v8, p0, Lorg/apache/lucene/index/IndexWriter;->filesToCommit:Ljava/util/Collection;

    invoke-virtual {v5, v8}, Lorg/apache/lucene/index/IndexFileDeleter;->incRef(Ljava/util/Collection;)V

    .line 2703
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2725
    const/4 v3, 0x1

    .line 2727
    if-nez v3, :cond_5

    .line 2728
    :try_start_5
    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v8, "IW"

    invoke-virtual {v5, v8}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 2729
    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v8, "IW"

    const-string v9, "hit exception during prepareCommit"

    invoke-virtual {v5, v8, v9}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 2733
    :cond_5
    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v5, v1}, Lorg/apache/lucene/index/DocumentsWriter;->finishFullFlush(Z)V

    .line 2734
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->doAfterFlush()V

    .line 2691
    monitor-exit v7
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    .line 2741
    .end local v1    # "flushSuccess":Z
    .end local v3    # "success":Z
    :goto_0
    const/4 v3, 0x0

    .line 2743
    .restart local v3    # "success":Z
    if-eqz v0, :cond_6

    .line 2744
    :try_start_6
    sget-object v5, Lorg/apache/lucene/index/MergePolicy$MergeTrigger;->FULL_FLUSH:Lorg/apache/lucene/index/MergePolicy$MergeTrigger;

    const/4 v7, -0x1

    invoke-direct {p0, v5, v7}, Lorg/apache/lucene/index/IndexWriter;->maybeMerge(Lorg/apache/lucene/index/MergePolicy$MergeTrigger;I)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    .line 2746
    :cond_6
    const/4 v3, 0x1

    .line 2748
    if-nez v3, :cond_7

    .line 2749
    :try_start_7
    monitor-enter p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 2750
    :try_start_8
    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->filesToCommit:Ljava/util/Collection;

    invoke-virtual {v5, v7}, Lorg/apache/lucene/index/IndexFileDeleter;->decRef(Ljava/util/Collection;)V

    .line 2751
    const/4 v5, 0x0

    iput-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->filesToCommit:Ljava/util/Collection;

    .line 2749
    monitor-exit p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_6

    .line 2756
    :cond_7
    :try_start_9
    invoke-direct {p0, v4}, Lorg/apache/lucene/index/IndexWriter;->startCommit(Lorg/apache/lucene/index/SegmentInfos;)V

    .line 2665
    monitor-exit v6
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 2758
    return-void

    .line 2703
    .restart local v1    # "flushSuccess":Z
    :catchall_1
    move-exception v5

    :try_start_a
    monitor-exit p0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    :try_start_b
    throw v5
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    .line 2726
    :catchall_2
    move-exception v5

    .line 2727
    if-nez v3, :cond_8

    .line 2728
    :try_start_c
    iget-object v8, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v9, "IW"

    invoke-virtual {v8, v9}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 2729
    iget-object v8, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v9, "IW"

    const-string v10, "hit exception during prepareCommit"

    invoke-virtual {v8, v9, v10}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 2733
    :cond_8
    iget-object v8, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v8, v1}, Lorg/apache/lucene/index/DocumentsWriter;->finishFullFlush(Z)V

    .line 2734
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->doAfterFlush()V

    .line 2735
    throw v5

    .line 2691
    :catchall_3
    move-exception v5

    monitor-exit v7
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    :try_start_d
    throw v5
    :try_end_d
    .catch Ljava/lang/OutOfMemoryError; {:try_start_d .. :try_end_d} :catch_0
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 2737
    .end local v1    # "flushSuccess":Z
    .end local v3    # "success":Z
    :catch_0
    move-exception v2

    .line 2738
    .local v2, "oom":Ljava/lang/OutOfMemoryError;
    :try_start_e
    const-string v5, "prepareCommit"

    invoke-direct {p0, v2, v5}, Lorg/apache/lucene/index/IndexWriter;->handleOOM(Ljava/lang/OutOfMemoryError;Ljava/lang/String;)V

    goto :goto_0

    .line 2747
    .end local v2    # "oom":Ljava/lang/OutOfMemoryError;
    .restart local v3    # "success":Z
    :catchall_4
    move-exception v5

    .line 2748
    if-nez v3, :cond_9

    .line 2749
    monitor-enter p0
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    .line 2750
    :try_start_f
    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    iget-object v8, p0, Lorg/apache/lucene/index/IndexWriter;->filesToCommit:Ljava/util/Collection;

    invoke-virtual {v7, v8}, Lorg/apache/lucene/index/IndexFileDeleter;->decRef(Ljava/util/Collection;)V

    .line 2751
    const/4 v7, 0x0

    iput-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->filesToCommit:Ljava/util/Collection;

    .line 2749
    monitor-exit p0
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_5

    .line 2754
    :cond_9
    :try_start_10
    throw v5
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    .line 2749
    :catchall_5
    move-exception v5

    :try_start_11
    monitor-exit p0
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_5

    :try_start_12
    throw v5
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    :catchall_6
    move-exception v5

    :try_start_13
    monitor-exit p0
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_6

    :try_start_14
    throw v5
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_0
.end method

.method private declared-synchronized resetMergeExceptions()V
    .locals 4

    .prologue
    .line 2227
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->mergeExceptions:Ljava/util/List;

    .line 2228
    iget-wide v0, p0, Lorg/apache/lucene/index/IndexWriter;->mergeGen:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/lucene/index/IndexWriter;->mergeGen:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2229
    monitor-exit p0

    return-void

    .line 2227
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private rollbackInternal()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 1964
    const/4 v1, 0x0

    .line 1966
    .local v1, "success":Z
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v3, "IW"

    invoke-virtual {v2, v3}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1967
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v3, "IW"

    const-string v4, "rollback"

    invoke-virtual {v2, v3, v4}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 1971
    :cond_0
    :try_start_0
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 1972
    const/4 v2, 0x0

    :try_start_1
    invoke-direct {p0, v2}, Lorg/apache/lucene/index/IndexWriter;->finishMerges(Z)V

    .line 1973
    const/4 v2, 0x1

    iput-boolean v2, p0, Lorg/apache/lucene/index/IndexWriter;->stopMerges:Z

    .line 1971
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1976
    :try_start_2
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v3, "IW"

    invoke-virtual {v2, v3}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1977
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v3, "IW"

    const-string v4, "rollback: done finish merges"

    invoke-virtual {v2, v3, v4}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 1983
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->mergePolicy:Lorg/apache/lucene/index/MergePolicy;

    invoke-virtual {v2}, Lorg/apache/lucene/index/MergePolicy;->close()V

    .line 1984
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->mergeScheduler:Lorg/apache/lucene/index/MergeScheduler;

    invoke-virtual {v2}, Lorg/apache/lucene/index/MergeScheduler;->close()V

    .line 1986
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->bufferedDeletesStream:Lorg/apache/lucene/index/BufferedDeletesStream;

    invoke-virtual {v2}, Lorg/apache/lucene/index/BufferedDeletesStream;->clear()V

    .line 1987
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v2}, Lorg/apache/lucene/index/DocumentsWriter;->close()V

    .line 1988
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v2}, Lorg/apache/lucene/index/DocumentsWriter;->abort()V

    .line 1989
    monitor-enter p0
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 1991
    :try_start_3
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->pendingCommit:Lorg/apache/lucene/index/SegmentInfos;

    if-eqz v2, :cond_2

    .line 1992
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->pendingCommit:Lorg/apache/lucene/index/SegmentInfos;

    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/index/SegmentInfos;->rollbackCommit(Lorg/apache/lucene/store/Directory;)V

    .line 1993
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->pendingCommit:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/index/IndexFileDeleter;->decRef(Lorg/apache/lucene/index/SegmentInfos;)V

    .line 1994
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->pendingCommit:Lorg/apache/lucene/index/SegmentInfos;

    .line 1995
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 1999
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->readerPool:Lorg/apache/lucene/index/IndexWriter$ReaderPool;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->dropAll(Z)V

    .line 2006
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->rollbackSegments:Ljava/util/List;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/index/SegmentInfos;->rollbackSegmentInfos(Ljava/util/List;)V

    .line 2007
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v3, "IW"

    invoke-virtual {v2, v3}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2008
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v3, "IW"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "rollback: infos="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {p0, v5}, Lorg/apache/lucene/index/IndexWriter;->segString(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 2012
    :cond_3
    sget-boolean v2, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v2, :cond_6

    const-string v2, "rollback before checkpoint"

    invoke-virtual {p0, v2}, Lorg/apache/lucene/index/IndexWriter;->testPoint(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_6

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 1989
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v2
    :try_end_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 2023
    :catch_0
    move-exception v0

    .line 2024
    .local v0, "oom":Ljava/lang/OutOfMemoryError;
    :try_start_5
    const-string v2, "rollbackInternal"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/index/IndexWriter;->handleOOM(Ljava/lang/OutOfMemoryError;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2026
    monitor-enter p0

    .line 2027
    if-nez v1, :cond_4

    .line 2028
    const/4 v2, 0x0

    :try_start_6
    iput-boolean v2, p0, Lorg/apache/lucene/index/IndexWriter;->closing:Z

    .line 2029
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 2030
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v3, "IW"

    invoke-virtual {v2, v3}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2031
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v3, "IW"

    const-string v4, "hit exception during rollback"

    invoke-virtual {v2, v3, v4}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 2026
    :cond_4
    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    .line 2037
    .end local v0    # "oom":Ljava/lang/OutOfMemoryError;
    :goto_0
    invoke-direct {p0, v6, v6}, Lorg/apache/lucene/index/IndexWriter;->closeInternal(ZZ)V

    .line 2038
    return-void

    .line 1971
    :catchall_1
    move-exception v2

    :try_start_7
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    throw v2
    :try_end_8
    .catch Ljava/lang/OutOfMemoryError; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 2025
    :catchall_2
    move-exception v2

    .line 2026
    monitor-enter p0

    .line 2027
    if-nez v1, :cond_5

    .line 2028
    const/4 v3, 0x0

    :try_start_9
    iput-boolean v3, p0, Lorg/apache/lucene/index/IndexWriter;->closing:Z

    .line 2029
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 2030
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v4, "IW"

    invoke-virtual {v3, v4}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 2031
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v4, "IW"

    const-string v5, "hit exception during rollback"

    invoke-virtual {v3, v4, v5}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 2026
    :cond_5
    monitor-exit p0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_5

    .line 2035
    throw v2

    .line 2016
    :cond_6
    :try_start_a
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lorg/apache/lucene/index/IndexFileDeleter;->checkpoint(Lorg/apache/lucene/index/SegmentInfos;Z)V

    .line 2017
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    invoke-virtual {v2}, Lorg/apache/lucene/index/IndexFileDeleter;->refresh()V

    .line 2019
    iget-wide v2, p0, Lorg/apache/lucene/index/IndexWriter;->changeCount:J

    iput-wide v2, p0, Lorg/apache/lucene/index/IndexWriter;->lastCommitChangeCount:J

    .line 1989
    monitor-exit p0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 2022
    const/4 v1, 0x1

    .line 2026
    monitor-enter p0

    .line 2027
    if-nez v1, :cond_7

    .line 2028
    const/4 v2, 0x0

    :try_start_b
    iput-boolean v2, p0, Lorg/apache/lucene/index/IndexWriter;->closing:Z

    .line 2029
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 2030
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v3, "IW"

    invoke-virtual {v2, v3}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 2031
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v3, "IW"

    const-string v4, "hit exception during rollback"

    invoke-virtual {v2, v3, v4}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 2026
    :cond_7
    monitor-exit p0

    goto :goto_0

    :catchall_3
    move-exception v2

    monitor-exit p0
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    throw v2

    .restart local v0    # "oom":Ljava/lang/OutOfMemoryError;
    :catchall_4
    move-exception v2

    :try_start_c
    monitor-exit p0
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_4

    throw v2

    .end local v0    # "oom":Ljava/lang/OutOfMemoryError;
    :catchall_5
    move-exception v2

    :try_start_d
    monitor-exit p0
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_5

    throw v2
.end method

.method static setDiagnostics(Lorg/apache/lucene/index/SegmentInfo;Ljava/lang/String;)V
    .locals 1
    .param p0, "info"    # Lorg/apache/lucene/index/SegmentInfo;
    .param p1, "source"    # Ljava/lang/String;

    .prologue
    .line 3530
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lorg/apache/lucene/index/IndexWriter;->setDiagnostics(Lorg/apache/lucene/index/SegmentInfo;Ljava/lang/String;Ljava/util/Map;)V

    .line 3531
    return-void
.end method

.method private static setDiagnostics(Lorg/apache/lucene/index/SegmentInfo;Ljava/lang/String;Ljava/util/Map;)V
    .locals 4
    .param p0, "info"    # Lorg/apache/lucene/index/SegmentInfo;
    .param p1, "source"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/SegmentInfo;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 3534
    .local p2, "details":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 3535
    .local v0, "diagnostics":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v1, "source"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3536
    const-string v1, "lucene.version"

    sget-object v2, Lorg/apache/lucene/util/Constants;->LUCENE_VERSION:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3537
    const-string v1, "os"

    sget-object v2, Lorg/apache/lucene/util/Constants;->OS_NAME:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3538
    const-string v1, "os.arch"

    sget-object v2, Lorg/apache/lucene/util/Constants;->OS_ARCH:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3539
    const-string v1, "os.version"

    sget-object v2, Lorg/apache/lucene/util/Constants;->OS_VERSION:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3540
    const-string v1, "java.version"

    sget-object v2, Lorg/apache/lucene/util/Constants;->JAVA_VERSION:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3541
    const-string v1, "java.vendor"

    sget-object v2, Lorg/apache/lucene/util/Constants;->JAVA_VENDOR:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3542
    const-string v1, "timestamp"

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3543
    if-eqz p2, :cond_0

    .line 3544
    invoke-interface {v0, p2}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 3546
    :cond_0
    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/SegmentInfo;->setDiagnostics(Ljava/util/Map;)V

    .line 3547
    return-void
.end method

.method private declared-synchronized shouldClose()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 906
    monitor-enter p0

    :goto_0
    :try_start_0
    iget-boolean v1, p0, Lorg/apache/lucene/index/IndexWriter;->closed:Z

    if-nez v1, :cond_1

    .line 907
    iget-boolean v1, p0, Lorg/apache/lucene/index/IndexWriter;->closing:Z

    if-nez v1, :cond_0

    .line 908
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/apache/lucene/index/IndexWriter;->closing:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 917
    :goto_1
    monitor-exit p0

    return v0

    .line 914
    :cond_0
    :try_start_1
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexWriter;->doWait()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 906
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 917
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private startCommit(Lorg/apache/lucene/index/SegmentInfos;)V
    .locals 10
    .param p1, "toSync"    # Lorg/apache/lucene/index/SegmentInfos;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3983
    sget-boolean v4, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    const-string v4, "startStartCommit"

    invoke-virtual {p0, v4}, Lorg/apache/lucene/index/IndexWriter;->testPoint(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 3984
    :cond_0
    sget-boolean v4, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->pendingCommit:Lorg/apache/lucene/index/SegmentInfos;

    if-eqz v4, :cond_1

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 3986
    :cond_1
    iget-boolean v4, p0, Lorg/apache/lucene/index/IndexWriter;->hitOOM:Z

    if-eqz v4, :cond_2

    .line 3987
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "this writer hit an OutOfMemoryError; cannot commit"

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 3992
    :cond_2
    :try_start_0
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v5, "IW"

    invoke-virtual {v4, v5}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 3993
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v5, "IW"

    const-string v6, "startCommit(): start"

    invoke-virtual {v4, v5, v6}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 3996
    :cond_3
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 3998
    :try_start_1
    sget-boolean v4, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v4, :cond_4

    iget-wide v4, p0, Lorg/apache/lucene/index/IndexWriter;->lastCommitChangeCount:J

    iget-wide v6, p0, Lorg/apache/lucene/index/IndexWriter;->changeCount:J

    cmp-long v4, v4, v6

    if-lez v4, :cond_4

    new-instance v4, Ljava/lang/AssertionError;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "lastCommitChangeCount="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v6, p0, Lorg/apache/lucene/index/IndexWriter;->lastCommitChangeCount:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " changeCount="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, p0, Lorg/apache/lucene/index/IndexWriter;->changeCount:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    .line 3996
    :catchall_0
    move-exception v4

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v4
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_0

    .line 4081
    :catch_0
    move-exception v1

    .line 4082
    .local v1, "oom":Ljava/lang/OutOfMemoryError;
    const-string v4, "startCommit"

    invoke-direct {p0, v1, v4}, Lorg/apache/lucene/index/IndexWriter;->handleOOM(Ljava/lang/OutOfMemoryError;Ljava/lang/String;)V

    .line 4084
    .end local v1    # "oom":Ljava/lang/OutOfMemoryError;
    :goto_0
    sget-boolean v4, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v4, :cond_6

    const-string v4, "finishStartCommit"

    invoke-virtual {p0, v4}, Lorg/apache/lucene/index/IndexWriter;->testPoint(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_6

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 4000
    :cond_4
    :try_start_3
    iget-wide v4, p0, Lorg/apache/lucene/index/IndexWriter;->pendingCommitChangeCount:J

    iget-wide v6, p0, Lorg/apache/lucene/index/IndexWriter;->lastCommitChangeCount:J

    cmp-long v4, v4, v6

    if-nez v4, :cond_7

    .line 4001
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v5, "IW"

    invoke-virtual {v4, v5}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 4002
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v5, "IW"

    const-string v6, "  skip startCommit(): no changes pending"

    invoke-virtual {v4, v5, v6}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 4004
    :cond_5
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->filesToCommit:Ljava/util/Collection;

    invoke-virtual {v4, v5}, Lorg/apache/lucene/index/IndexFileDeleter;->decRef(Ljava/util/Collection;)V

    .line 4005
    const/4 v4, 0x0

    iput-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->filesToCommit:Ljava/util/Collection;

    .line 4006
    monitor-exit p0

    .line 4085
    :cond_6
    return-void

    .line 4009
    :cond_7
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v5, "IW"

    invoke-virtual {v4, v5}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 4010
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v5, "IW"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "startCommit index="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/IndexWriter;->toLiveInfos(Lorg/apache/lucene/index/SegmentInfos;)Lorg/apache/lucene/index/SegmentInfos;

    move-result-object v7

    invoke-virtual {p0, v7}, Lorg/apache/lucene/index/IndexWriter;->segString(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " changeCount="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-wide v8, p0, Lorg/apache/lucene/index/IndexWriter;->changeCount:J

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 4013
    :cond_8
    sget-boolean v4, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v4, :cond_9

    invoke-direct {p0, p1}, Lorg/apache/lucene/index/IndexWriter;->filesExist(Lorg/apache/lucene/index/SegmentInfos;)Z

    move-result v4

    if-nez v4, :cond_9

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 3996
    :cond_9
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 4016
    :try_start_4
    sget-boolean v4, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v4, :cond_a

    const-string v4, "midStartCommit"

    invoke-virtual {p0, v4}, Lorg/apache/lucene/index/IndexWriter;->testPoint(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_a

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4
    :try_end_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_0

    .line 4018
    :cond_a
    const/4 v2, 0x0

    .line 4022
    .local v2, "pendingCommitSet":Z
    :try_start_5
    sget-boolean v4, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v4, :cond_d

    const-string v4, "midStartCommit2"

    invoke-virtual {p0, v4}, Lorg/apache/lucene/index/IndexWriter;->testPoint(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_d

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 4062
    :catchall_1
    move-exception v4

    .line 4063
    :try_start_6
    monitor-enter p0
    :try_end_6
    .catch Ljava/lang/OutOfMemoryError; {:try_start_6 .. :try_end_6} :catch_0

    .line 4068
    :try_start_7
    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v5, p1}, Lorg/apache/lucene/index/SegmentInfos;->updateGeneration(Lorg/apache/lucene/index/SegmentInfos;)V

    .line 4070
    if-nez v2, :cond_c

    .line 4071
    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v6, "IW"

    invoke-virtual {v5, v6}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 4072
    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v6, "IW"

    const-string v7, "hit exception committing segments file"

    invoke-virtual {v5, v6, v7}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 4076
    :cond_b
    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->filesToCommit:Ljava/util/Collection;

    invoke-virtual {v5, v6}, Lorg/apache/lucene/index/IndexFileDeleter;->decRef(Ljava/util/Collection;)V

    .line 4077
    const/4 v5, 0x0

    iput-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->filesToCommit:Ljava/util/Collection;

    .line 4063
    :cond_c
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    .line 4080
    :try_start_8
    throw v4
    :try_end_8
    .catch Ljava/lang/OutOfMemoryError; {:try_start_8 .. :try_end_8} :catch_0

    .line 4024
    :cond_d
    :try_start_9
    monitor-enter p0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 4026
    :try_start_a
    sget-boolean v4, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v4, :cond_e

    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->pendingCommit:Lorg/apache/lucene/index/SegmentInfos;

    if-eqz v4, :cond_e

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 4024
    :catchall_2
    move-exception v4

    monitor-exit p0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    :try_start_b
    throw v4
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 4028
    :cond_e
    :try_start_c
    sget-boolean v4, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v4, :cond_f

    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v4}, Lorg/apache/lucene/index/SegmentInfos;->getGeneration()J

    move-result-wide v4

    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfos;->getGeneration()J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-eqz v4, :cond_f

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 4033
    :cond_f
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {p1, v4}, Lorg/apache/lucene/index/SegmentInfos;->prepareCommit(Lorg/apache/lucene/store/Directory;)V

    .line 4036
    const/4 v2, 0x1

    .line 4037
    iput-object p1, p0, Lorg/apache/lucene/index/IndexWriter;->pendingCommit:Lorg/apache/lucene/index/SegmentInfos;

    .line 4024
    monitor-exit p0
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    .line 4042
    const/4 v3, 0x0

    .line 4045
    .local v3, "success":Z
    :try_start_d
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    const/4 v5, 0x0

    invoke-virtual {p1, v4, v5}, Lorg/apache/lucene/index/SegmentInfos;->files(Lorg/apache/lucene/store/Directory;Z)Ljava/util/Collection;

    move-result-object v0

    .line 4046
    .local v0, "filesToSync":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v4, v0}, Lorg/apache/lucene/store/Directory;->sync(Ljava/util/Collection;)V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_3

    .line 4047
    const/4 v3, 0x1

    .line 4049
    if-nez v3, :cond_10

    .line 4050
    const/4 v2, 0x0

    .line 4051
    const/4 v4, 0x0

    :try_start_e
    iput-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->pendingCommit:Lorg/apache/lucene/index/SegmentInfos;

    .line 4052
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {p1, v4}, Lorg/apache/lucene/index/SegmentInfos;->rollbackCommit(Lorg/apache/lucene/store/Directory;)V

    .line 4056
    :cond_10
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v5, "IW"

    invoke-virtual {v4, v5}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_11

    .line 4057
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v5, "IW"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "done all syncs: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 4060
    :cond_11
    sget-boolean v4, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v4, :cond_13

    const-string v4, "midStartCommitSuccess"

    invoke-virtual {p0, v4}, Lorg/apache/lucene/index/IndexWriter;->testPoint(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_13

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 4048
    .end local v0    # "filesToSync":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    :catchall_3
    move-exception v4

    .line 4049
    if-nez v3, :cond_12

    .line 4050
    const/4 v2, 0x0

    .line 4051
    const/4 v5, 0x0

    iput-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->pendingCommit:Lorg/apache/lucene/index/SegmentInfos;

    .line 4052
    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {p1, v5}, Lorg/apache/lucene/index/SegmentInfos;->rollbackCommit(Lorg/apache/lucene/store/Directory;)V

    .line 4054
    :cond_12
    throw v4
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    .line 4063
    .end local v3    # "success":Z
    :catchall_4
    move-exception v4

    :try_start_f
    monitor-exit p0
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_4

    :try_start_10
    throw v4

    .restart local v0    # "filesToSync":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    .restart local v3    # "success":Z
    :cond_13
    monitor-enter p0
    :try_end_10
    .catch Ljava/lang/OutOfMemoryError; {:try_start_10 .. :try_end_10} :catch_0

    .line 4068
    :try_start_11
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v4, p1}, Lorg/apache/lucene/index/SegmentInfos;->updateGeneration(Lorg/apache/lucene/index/SegmentInfos;)V

    .line 4070
    if-nez v2, :cond_15

    .line 4071
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v5, "IW"

    invoke-virtual {v4, v5}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_14

    .line 4072
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v5, "IW"

    const-string v6, "hit exception committing segments file"

    invoke-virtual {v4, v5, v6}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 4076
    :cond_14
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->filesToCommit:Ljava/util/Collection;

    invoke-virtual {v4, v5}, Lorg/apache/lucene/index/IndexFileDeleter;->decRef(Ljava/util/Collection;)V

    .line 4077
    const/4 v4, 0x0

    iput-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->filesToCommit:Ljava/util/Collection;

    .line 4063
    :cond_15
    monitor-exit p0

    goto/16 :goto_0

    :catchall_5
    move-exception v4

    monitor-exit p0
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_5

    :try_start_12
    throw v4
    :try_end_12
    .catch Ljava/lang/OutOfMemoryError; {:try_start_12 .. :try_end_12} :catch_0
.end method

.method public static unlock(Lorg/apache/lucene/store/Directory;)V
    .locals 1
    .param p0, "directory"    # Lorg/apache/lucene/store/Directory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4105
    const-string/jumbo v0, "write.lock"

    invoke-virtual {p0, v0}, Lorg/apache/lucene/store/Directory;->makeLock(Ljava/lang/String;)Lorg/apache/lucene/store/Lock;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/store/Lock;->release()V

    .line 4106
    return-void
.end method

.method private declared-synchronized updatePendingMerges(Lorg/apache/lucene/index/MergePolicy$MergeTrigger;I)V
    .locals 7
    .param p1, "trigger"    # Lorg/apache/lucene/index/MergePolicy$MergeTrigger;
    .param p2, "maxNumSegments"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, -0x1

    .line 1864
    monitor-enter p0

    :try_start_0
    sget-boolean v4, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    if-eq p2, v5, :cond_0

    if-gtz p2, :cond_0

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    .line 1865
    :cond_0
    :try_start_1
    sget-boolean v4, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    if-nez p1, :cond_1

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 1866
    :cond_1
    iget-boolean v4, p0, Lorg/apache/lucene/index/IndexWriter;->stopMerges:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v4, :cond_3

    .line 1898
    :cond_2
    monitor-exit p0

    return-void

    .line 1871
    :cond_3
    :try_start_2
    iget-boolean v4, p0, Lorg/apache/lucene/index/IndexWriter;->hitOOM:Z

    if-nez v4, :cond_2

    .line 1876
    if-eq p2, v5, :cond_7

    .line 1877
    sget-boolean v4, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v4, :cond_4

    sget-object v4, Lorg/apache/lucene/index/MergePolicy$MergeTrigger;->EXPLICIT:Lorg/apache/lucene/index/MergePolicy$MergeTrigger;

    if-eq p1, v4, :cond_4

    sget-object v4, Lorg/apache/lucene/index/MergePolicy$MergeTrigger;->MERGE_FINISHED:Lorg/apache/lucene/index/MergePolicy$MergeTrigger;

    if-eq p1, v4, :cond_4

    new-instance v4, Ljava/lang/AssertionError;

    .line 1878
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Expected EXPLICT or MERGE_FINISHED as trigger even with maxNumSegments set but was: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/lucene/index/MergePolicy$MergeTrigger;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    .line 1879
    :cond_4
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->mergePolicy:Lorg/apache/lucene/index/MergePolicy;

    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->segmentsToMerge:Ljava/util/Map;

    invoke-static {v6}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v6

    invoke-virtual {v4, v5, p2, v6}, Lorg/apache/lucene/index/MergePolicy;->findForcedMerges(Lorg/apache/lucene/index/SegmentInfos;ILjava/util/Map;)Lorg/apache/lucene/index/MergePolicy$MergeSpecification;

    move-result-object v3

    .line 1880
    .local v3, "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    if-eqz v3, :cond_5

    .line 1881
    iget-object v4, v3, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;->merges:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v2

    .line 1882
    .local v2, "numMerges":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v2, :cond_6

    .line 1892
    .end local v0    # "i":I
    .end local v2    # "numMerges":I
    :cond_5
    :goto_1
    if-eqz v3, :cond_2

    .line 1893
    iget-object v4, v3, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;->merges:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v2

    .line 1894
    .restart local v2    # "numMerges":I
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_2
    if-ge v0, v2, :cond_2

    .line 1895
    iget-object v4, v3, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;->merges:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/index/MergePolicy$OneMerge;

    invoke-virtual {p0, v4}, Lorg/apache/lucene/index/IndexWriter;->registerMerge(Lorg/apache/lucene/index/MergePolicy$OneMerge;)Z

    .line 1894
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1883
    :cond_6
    iget-object v4, v3, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;->merges:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/MergePolicy$OneMerge;

    .line 1884
    .local v1, "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    iput p2, v1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->maxNumSegments:I

    .line 1882
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1889
    .end local v0    # "i":I
    .end local v1    # "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    .end local v2    # "numMerges":I
    .end local v3    # "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    :cond_7
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->mergePolicy:Lorg/apache/lucene/index/MergePolicy;

    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v4, p1, v5}, Lorg/apache/lucene/index/MergePolicy;->findMerges(Lorg/apache/lucene/index/MergePolicy$MergeTrigger;Lorg/apache/lucene/index/SegmentInfos;)Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v3

    .restart local v3    # "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    goto :goto_1
.end method


# virtual methods
.method public addDocument(Ljava/lang/Iterable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lorg/apache/lucene/index/IndexableField;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1141
    .local p1, "doc":Ljava/lang/Iterable;, "Ljava/lang/Iterable<+Lorg/apache/lucene/index/IndexableField;>;"
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/index/IndexWriter;->addDocument(Ljava/lang/Iterable;Lorg/apache/lucene/analysis/Analyzer;)V

    .line 1142
    return-void
.end method

.method public addDocument(Ljava/lang/Iterable;Lorg/apache/lucene/analysis/Analyzer;)V
    .locals 1
    .param p2, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lorg/apache/lucene/index/IndexableField;",
            ">;",
            "Lorg/apache/lucene/analysis/Analyzer;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1160
    .local p1, "doc":Ljava/lang/Iterable;, "Ljava/lang/Iterable<+Lorg/apache/lucene/index/IndexableField;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1, p2}, Lorg/apache/lucene/index/IndexWriter;->updateDocument(Lorg/apache/lucene/index/Term;Ljava/lang/Iterable;Lorg/apache/lucene/analysis/Analyzer;)V

    .line 1161
    return-void
.end method

.method public addDocuments(Ljava/lang/Iterable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Ljava/lang/Iterable",
            "<+",
            "Lorg/apache/lucene/index/IndexableField;",
            ">;>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1205
    .local p1, "docs":Ljava/lang/Iterable;, "Ljava/lang/Iterable<+Ljava/lang/Iterable<+Lorg/apache/lucene/index/IndexableField;>;>;"
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/index/IndexWriter;->addDocuments(Ljava/lang/Iterable;Lorg/apache/lucene/analysis/Analyzer;)V

    .line 1206
    return-void
.end method

.method public addDocuments(Ljava/lang/Iterable;Lorg/apache/lucene/analysis/Analyzer;)V
    .locals 1
    .param p2, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Ljava/lang/Iterable",
            "<+",
            "Lorg/apache/lucene/index/IndexableField;",
            ">;>;",
            "Lorg/apache/lucene/analysis/Analyzer;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1220
    .local p1, "docs":Ljava/lang/Iterable;, "Ljava/lang/Iterable<+Ljava/lang/Iterable<+Lorg/apache/lucene/index/IndexableField;>;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1, p2}, Lorg/apache/lucene/index/IndexWriter;->updateDocuments(Lorg/apache/lucene/index/Term;Ljava/lang/Iterable;Lorg/apache/lucene/analysis/Analyzer;)V

    .line 1221
    return-void
.end method

.method public varargs addIndexes([Lorg/apache/lucene/index/IndexReader;)V
    .locals 25
    .param p1, "readers"    # [Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2403
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 2404
    const/4 v3, 0x0

    .line 2407
    .local v3, "numDocs":I
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v6, "IW"

    invoke-virtual {v2, v6}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2408
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v6, "IW"

    const-string v7, "flush at addIndexes(IndexReader...)"

    invoke-virtual {v2, v6, v7}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 2410
    :cond_0
    const/4 v2, 0x0

    const/4 v6, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v6}, Lorg/apache/lucene/index/IndexWriter;->flush(ZZ)V

    .line 2412
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/IndexWriter;->newSegmentName()Ljava/lang/String;

    move-result-object v20

    .line 2413
    .local v20, "mergedName":Ljava/lang/String;
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 2414
    .local v18, "mergeReaders":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/AtomicReader;>;"
    move-object/from16 v0, p1

    array-length v6, v0

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v6, :cond_2

    .line 2420
    new-instance v13, Lorg/apache/lucene/store/IOContext;

    new-instance v2, Lorg/apache/lucene/store/MergeInfo;

    const-wide/16 v4, -0x1

    const/4 v6, 0x1

    const/4 v7, -0x1

    invoke-direct/range {v2 .. v7}, Lorg/apache/lucene/store/MergeInfo;-><init>(IJZI)V

    invoke-direct {v13, v2}, Lorg/apache/lucene/store/IOContext;-><init>(Lorg/apache/lucene/store/MergeInfo;)V

    .line 2424
    .local v13, "context":Lorg/apache/lucene/store/IOContext;
    new-instance v23, Lorg/apache/lucene/store/TrackingDirectoryWrapper;

    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    move-object/from16 v0, v23

    invoke-direct {v0, v2}, Lorg/apache/lucene/store/TrackingDirectoryWrapper;-><init>(Lorg/apache/lucene/store/Directory;)V

    .line 2426
    .local v23, "trackingDir":Lorg/apache/lucene/store/TrackingDirectoryWrapper;
    new-instance v4, Lorg/apache/lucene/index/SegmentInfo;

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    sget-object v6, Lorg/apache/lucene/util/Constants;->LUCENE_MAIN_VERSION:Ljava/lang/String;

    const/4 v8, -0x1

    .line 2427
    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/lucene/index/IndexWriter;->codec:Lorg/apache/lucene/codecs/Codec;

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object/from16 v7, v20

    .line 2426
    invoke-direct/range {v4 .. v12}, Lorg/apache/lucene/index/SegmentInfo;-><init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Ljava/lang/String;IZLorg/apache/lucene/codecs/Codec;Ljava/util/Map;Ljava/util/Map;)V

    .line 2429
    .local v4, "info":Lorg/apache/lucene/index/SegmentInfo;
    new-instance v5, Lorg/apache/lucene/index/SegmentMerger;

    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/IndexWriter;->config:Lorg/apache/lucene/index/LiveIndexWriterConfig;

    invoke-virtual {v2}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getTermIndexInterval()I

    move-result v10

    .line 2430
    sget-object v11, Lorg/apache/lucene/index/MergeState$CheckAbort;->NONE:Lorg/apache/lucene/index/MergeState$CheckAbort;

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/index/IndexWriter;->globalFieldNumberMap:Lorg/apache/lucene/index/FieldInfos$FieldNumbers;

    move-object/from16 v6, v18

    move-object v7, v4

    move-object/from16 v9, v23

    .line 2429
    invoke-direct/range {v5 .. v13}, Lorg/apache/lucene/index/SegmentMerger;-><init>(Ljava/util/List;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/util/InfoStream;Lorg/apache/lucene/store/Directory;ILorg/apache/lucene/index/MergeState$CheckAbort;Lorg/apache/lucene/index/FieldInfos$FieldNumbers;Lorg/apache/lucene/store/IOContext;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 2433
    .local v5, "merger":Lorg/apache/lucene/index/SegmentMerger;
    const/16 v22, 0x0

    .line 2435
    .local v22, "success":Z
    :try_start_1
    invoke-virtual {v5}, Lorg/apache/lucene/index/SegmentMerger;->merge()Lorg/apache/lucene/index/MergeState;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v19

    .line 2436
    .local v19, "mergeState":Lorg/apache/lucene/index/MergeState;
    const/16 v22, 0x1

    .line 2438
    if-nez v22, :cond_1

    .line 2439
    :try_start_2
    monitor-enter p0
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_0

    .line 2440
    :try_start_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    iget-object v6, v4, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    invoke-virtual {v2, v6}, Lorg/apache/lucene/index/IndexFileDeleter;->refresh(Ljava/lang/String;)V

    .line 2439
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 2445
    :cond_1
    :try_start_4
    new-instance v17, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    const/4 v2, 0x0

    const-wide/16 v6, -0x1

    move-object/from16 v0, v17

    invoke-direct {v0, v4, v2, v6, v7}, Lorg/apache/lucene/index/SegmentInfoPerCommit;-><init>(Lorg/apache/lucene/index/SegmentInfo;IJ)V

    .line 2447
    .local v17, "infoPerCommit":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    new-instance v2, Ljava/util/HashSet;

    invoke-virtual/range {v23 .. v23}, Lorg/apache/lucene/store/TrackingDirectoryWrapper;->getCreatedFiles()Ljava/util/Set;

    move-result-object v6

    invoke-direct {v2, v6}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v4, v2}, Lorg/apache/lucene/index/SegmentInfo;->setFiles(Ljava/util/Set;)V

    .line 2448
    invoke-virtual/range {v23 .. v23}, Lorg/apache/lucene/store/TrackingDirectoryWrapper;->getCreatedFiles()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->clear()V

    .line 2450
    const-string v2, "addIndexes(IndexReader...)"

    invoke-static {v4, v2}, Lorg/apache/lucene/index/IndexWriter;->setDiagnostics(Lorg/apache/lucene/index/SegmentInfo;Ljava/lang/String;)V

    .line 2453
    monitor-enter p0
    :try_end_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_0

    .line 2454
    :try_start_5
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lorg/apache/lucene/index/IndexWriter;->stopMerges:Z

    if-eqz v2, :cond_5

    .line 2455
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    invoke-virtual/range {v17 .. v17}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->files()Ljava/util/Collection;

    move-result-object v6

    invoke-virtual {v2, v6}, Lorg/apache/lucene/index/IndexFileDeleter;->deleteNewFiles(Ljava/util/Collection;)V

    .line 2456
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    .line 2508
    .end local v4    # "info":Lorg/apache/lucene/index/SegmentInfo;
    .end local v5    # "merger":Lorg/apache/lucene/index/SegmentMerger;
    .end local v13    # "context":Lorg/apache/lucene/store/IOContext;
    .end local v17    # "infoPerCommit":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .end local v18    # "mergeReaders":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/AtomicReader;>;"
    .end local v19    # "mergeState":Lorg/apache/lucene/index/MergeState;
    .end local v20    # "mergedName":Ljava/lang/String;
    .end local v22    # "success":Z
    .end local v23    # "trackingDir":Lorg/apache/lucene/store/TrackingDirectoryWrapper;
    :goto_1
    return-void

    .line 2414
    .restart local v18    # "mergeReaders":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/AtomicReader;>;"
    .restart local v20    # "mergedName":Ljava/lang/String;
    :cond_2
    :try_start_6
    aget-object v16, p1, v2

    .line 2415
    .local v16, "indexReader":Lorg/apache/lucene/index/IndexReader;
    invoke-virtual/range {v16 .. v16}, Lorg/apache/lucene/index/IndexReader;->numDocs()I

    move-result v7

    add-int/2addr v3, v7

    .line 2416
    invoke-virtual/range {v16 .. v16}, Lorg/apache/lucene/index/IndexReader;->leaves()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_3

    .line 2414
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .line 2416
    :cond_3
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lorg/apache/lucene/index/AtomicReaderContext;

    .line 2417
    .local v14, "ctx":Lorg/apache/lucene/index/AtomicReaderContext;
    invoke-virtual {v14}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v8

    move-object/from16 v0, v18

    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_6
    .catch Ljava/lang/OutOfMemoryError; {:try_start_6 .. :try_end_6} :catch_0

    goto :goto_2

    .line 2505
    .end local v14    # "ctx":Lorg/apache/lucene/index/AtomicReaderContext;
    .end local v16    # "indexReader":Lorg/apache/lucene/index/IndexReader;
    .end local v18    # "mergeReaders":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/AtomicReader;>;"
    .end local v20    # "mergedName":Ljava/lang/String;
    :catch_0
    move-exception v21

    .line 2506
    .local v21, "oom":Ljava/lang/OutOfMemoryError;
    const-string v2, "addIndexes(IndexReader...)"

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/index/IndexWriter;->handleOOM(Ljava/lang/OutOfMemoryError;Ljava/lang/String;)V

    goto :goto_1

    .line 2437
    .end local v21    # "oom":Ljava/lang/OutOfMemoryError;
    .restart local v4    # "info":Lorg/apache/lucene/index/SegmentInfo;
    .restart local v5    # "merger":Lorg/apache/lucene/index/SegmentMerger;
    .restart local v13    # "context":Lorg/apache/lucene/store/IOContext;
    .restart local v18    # "mergeReaders":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/AtomicReader;>;"
    .restart local v20    # "mergedName":Ljava/lang/String;
    .restart local v22    # "success":Z
    .restart local v23    # "trackingDir":Lorg/apache/lucene/store/TrackingDirectoryWrapper;
    :catchall_0
    move-exception v2

    .line 2438
    if-nez v22, :cond_4

    .line 2439
    :try_start_7
    monitor-enter p0
    :try_end_7
    .catch Ljava/lang/OutOfMemoryError; {:try_start_7 .. :try_end_7} :catch_0

    .line 2440
    :try_start_8
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    iget-object v7, v4, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lorg/apache/lucene/index/IndexFileDeleter;->refresh(Ljava/lang/String;)V

    .line 2439
    monitor-exit p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 2443
    :cond_4
    :try_start_9
    throw v2
    :try_end_9
    .catch Ljava/lang/OutOfMemoryError; {:try_start_9 .. :try_end_9} :catch_0

    .line 2439
    :catchall_1
    move-exception v2

    :try_start_a
    monitor-exit p0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    :try_start_b
    throw v2
    :try_end_b
    .catch Ljava/lang/OutOfMemoryError; {:try_start_b .. :try_end_b} :catch_0

    .restart local v19    # "mergeState":Lorg/apache/lucene/index/MergeState;
    :catchall_2
    move-exception v2

    :try_start_c
    monitor-exit p0
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    :try_start_d
    throw v2
    :try_end_d
    .catch Ljava/lang/OutOfMemoryError; {:try_start_d .. :try_end_d} :catch_0

    .line 2458
    .restart local v17    # "infoPerCommit":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    :cond_5
    :try_start_e
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 2459
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/IndexWriter;->mergePolicy:Lorg/apache/lucene/index/MergePolicy;

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    move-object/from16 v0, v17

    invoke-virtual {v2, v6, v0}, Lorg/apache/lucene/index/MergePolicy;->useCompoundFile(Lorg/apache/lucene/index/SegmentInfos;Lorg/apache/lucene/index/SegmentInfoPerCommit;)Z

    move-result v24

    .line 2453
    .local v24, "useCompoundFile":Z
    monitor-exit p0
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_4

    .line 2463
    if-eqz v24, :cond_6

    .line 2464
    :try_start_f
    invoke-virtual/range {v17 .. v17}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->files()Ljava/util/Collection;
    :try_end_f
    .catch Ljava/lang/OutOfMemoryError; {:try_start_f .. :try_end_f} :catch_0

    move-result-object v15

    .line 2466
    .local v15, "filesToDelete":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    :try_start_10
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    sget-object v7, Lorg/apache/lucene/index/MergeState$CheckAbort;->NONE:Lorg/apache/lucene/index/MergeState$CheckAbort;

    invoke-static {v2, v6, v7, v4, v13}, Lorg/apache/lucene/index/IndexWriter;->createCompoundFile(Lorg/apache/lucene/util/InfoStream;Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/MergeState$CheckAbort;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/store/IOContext;)Ljava/util/Collection;
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_5

    .line 2470
    :try_start_11
    monitor-enter p0
    :try_end_11
    .catch Ljava/lang/OutOfMemoryError; {:try_start_11 .. :try_end_11} :catch_0

    .line 2471
    :try_start_12
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    invoke-virtual {v2, v15}, Lorg/apache/lucene/index/IndexFileDeleter;->deleteNewFiles(Ljava/util/Collection;)V

    .line 2470
    monitor-exit p0
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_7

    .line 2474
    const/4 v2, 0x1

    :try_start_13
    invoke-virtual {v4, v2}, Lorg/apache/lucene/index/SegmentInfo;->setUseCompoundFile(Z)V
    :try_end_13
    .catch Ljava/lang/OutOfMemoryError; {:try_start_13 .. :try_end_13} :catch_0

    .line 2481
    .end local v15    # "filesToDelete":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    :cond_6
    const/16 v22, 0x0

    .line 2483
    :try_start_14
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/IndexWriter;->codec:Lorg/apache/lucene/codecs/Codec;

    invoke-virtual {v2}, Lorg/apache/lucene/codecs/Codec;->segmentInfoFormat()Lorg/apache/lucene/codecs/SegmentInfoFormat;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/lucene/codecs/SegmentInfoFormat;->getSegmentInfoWriter()Lorg/apache/lucene/codecs/SegmentInfoWriter;

    move-result-object v2

    move-object/from16 v0, v19

    iget-object v6, v0, Lorg/apache/lucene/index/MergeState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    move-object/from16 v0, v23

    invoke-virtual {v2, v0, v4, v6, v13}, Lorg/apache/lucene/codecs/SegmentInfoWriter;->write(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/store/IOContext;)V
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_8

    .line 2484
    const/16 v22, 0x1

    .line 2486
    if-nez v22, :cond_7

    .line 2487
    :try_start_15
    monitor-enter p0
    :try_end_15
    .catch Ljava/lang/OutOfMemoryError; {:try_start_15 .. :try_end_15} :catch_0

    .line 2488
    :try_start_16
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    iget-object v6, v4, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    invoke-virtual {v2, v6}, Lorg/apache/lucene/index/IndexFileDeleter;->refresh(Ljava/lang/String;)V

    .line 2487
    monitor-exit p0
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_a

    .line 2493
    :cond_7
    :try_start_17
    invoke-virtual/range {v23 .. v23}, Lorg/apache/lucene/store/TrackingDirectoryWrapper;->getCreatedFiles()Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v4, v2}, Lorg/apache/lucene/index/SegmentInfo;->addFiles(Ljava/util/Collection;)V

    .line 2496
    monitor-enter p0
    :try_end_17
    .catch Ljava/lang/OutOfMemoryError; {:try_start_17 .. :try_end_17} :catch_0

    .line 2497
    :try_start_18
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lorg/apache/lucene/index/IndexWriter;->stopMerges:Z

    if-eqz v2, :cond_9

    .line 2498
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    invoke-virtual {v4}, Lorg/apache/lucene/index/SegmentInfo;->files()Ljava/util/Set;

    move-result-object v6

    invoke-virtual {v2, v6}, Lorg/apache/lucene/index/IndexFileDeleter;->deleteNewFiles(Ljava/util/Collection;)V

    .line 2499
    monitor-exit p0

    goto/16 :goto_1

    .line 2496
    :catchall_3
    move-exception v2

    monitor-exit p0
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_3

    :try_start_19
    throw v2
    :try_end_19
    .catch Ljava/lang/OutOfMemoryError; {:try_start_19 .. :try_end_19} :catch_0

    .line 2453
    .end local v24    # "useCompoundFile":Z
    :catchall_4
    move-exception v2

    :try_start_1a
    monitor-exit p0
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_4

    :try_start_1b
    throw v2

    .line 2467
    .restart local v15    # "filesToDelete":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    .restart local v24    # "useCompoundFile":Z
    :catchall_5
    move-exception v2

    .line 2470
    monitor-enter p0
    :try_end_1b
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1b .. :try_end_1b} :catch_0

    .line 2471
    :try_start_1c
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    invoke-virtual {v6, v15}, Lorg/apache/lucene/index/IndexFileDeleter;->deleteNewFiles(Ljava/util/Collection;)V

    .line 2470
    monitor-exit p0
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_6

    .line 2473
    :try_start_1d
    throw v2
    :try_end_1d
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1d .. :try_end_1d} :catch_0

    .line 2470
    :catchall_6
    move-exception v2

    :try_start_1e
    monitor-exit p0
    :try_end_1e
    .catchall {:try_start_1e .. :try_end_1e} :catchall_6

    :try_start_1f
    throw v2
    :try_end_1f
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1f .. :try_end_1f} :catch_0

    :catchall_7
    move-exception v2

    :try_start_20
    monitor-exit p0
    :try_end_20
    .catchall {:try_start_20 .. :try_end_20} :catchall_7

    :try_start_21
    throw v2

    .line 2485
    .end local v15    # "filesToDelete":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    :catchall_8
    move-exception v2

    .line 2486
    if-nez v22, :cond_8

    .line 2487
    monitor-enter p0
    :try_end_21
    .catch Ljava/lang/OutOfMemoryError; {:try_start_21 .. :try_end_21} :catch_0

    .line 2488
    :try_start_22
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    iget-object v7, v4, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lorg/apache/lucene/index/IndexFileDeleter;->refresh(Ljava/lang/String;)V

    .line 2487
    monitor-exit p0
    :try_end_22
    .catchall {:try_start_22 .. :try_end_22} :catchall_9

    .line 2491
    :cond_8
    :try_start_23
    throw v2
    :try_end_23
    .catch Ljava/lang/OutOfMemoryError; {:try_start_23 .. :try_end_23} :catch_0

    .line 2487
    :catchall_9
    move-exception v2

    :try_start_24
    monitor-exit p0
    :try_end_24
    .catchall {:try_start_24 .. :try_end_24} :catchall_9

    :try_start_25
    throw v2
    :try_end_25
    .catch Ljava/lang/OutOfMemoryError; {:try_start_25 .. :try_end_25} :catch_0

    :catchall_a
    move-exception v2

    :try_start_26
    monitor-exit p0
    :try_end_26
    .catchall {:try_start_26 .. :try_end_26} :catchall_a

    :try_start_27
    throw v2
    :try_end_27
    .catch Ljava/lang/OutOfMemoryError; {:try_start_27 .. :try_end_27} :catch_0

    .line 2501
    :cond_9
    :try_start_28
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 2502
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Lorg/apache/lucene/index/SegmentInfos;->add(Lorg/apache/lucene/index/SegmentInfoPerCommit;)V

    .line 2503
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/IndexWriter;->checkpoint()V

    .line 2496
    monitor-exit p0
    :try_end_28
    .catchall {:try_start_28 .. :try_end_28} :catchall_3

    goto/16 :goto_1
.end method

.method public varargs addIndexes([Lorg/apache/lucene/store/Directory;)V
    .locals 25
    .param p1, "dirs"    # [Lorg/apache/lucene/store/Directory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2287
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 2289
    invoke-direct/range {p0 .. p1}, Lorg/apache/lucene/index/IndexWriter;->noDupDirs([Lorg/apache/lucene/store/Directory;)V

    .line 2292
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v3, "IW"

    invoke-virtual {v2, v3}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2293
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v3, "IW"

    const-string v4, "flush at addIndexes(Directory...)"

    invoke-virtual {v2, v3, v4}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 2296
    :cond_0
    const/4 v2, 0x0

    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/index/IndexWriter;->flush(ZZ)V

    .line 2298
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 2299
    .local v16, "infos":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfoPerCommit;>;"
    const/16 v21, 0x0

    .line 2301
    .local v21, "success":Z
    :try_start_1
    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v23, v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v2, 0x0

    move/from16 v22, v2

    :goto_0
    move/from16 v0, v22

    move/from16 v1, v23

    if-lt v0, v1, :cond_5

    .line 2327
    const/16 v21, 0x1

    .line 2329
    if-nez v21, :cond_2

    .line 2330
    :try_start_2
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_e

    .line 2341
    :cond_2
    monitor-enter p0
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_0

    .line 2342
    const/16 v21, 0x0

    .line 2344
    :try_start_3
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2345
    const/16 v21, 0x1

    .line 2347
    if-nez v21, :cond_4

    .line 2348
    :try_start_4
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_12

    .line 2358
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Lorg/apache/lucene/index/SegmentInfos;->addAll(Ljava/lang/Iterable;)V

    .line 2359
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/IndexWriter;->checkpoint()V

    .line 2341
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 2365
    .end local v16    # "infos":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfoPerCommit;>;"
    .end local v21    # "success":Z
    :goto_1
    return-void

    .line 2301
    .restart local v16    # "infos":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfoPerCommit;>;"
    .restart local v21    # "success":Z
    :cond_5
    :try_start_5
    aget-object v10, p1, v22

    .line 2302
    .local v10, "dir":Lorg/apache/lucene/store/Directory;
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v3, "IW"

    invoke-virtual {v2, v3}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 2303
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v3, "IW"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "addIndexes: process directory "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 2305
    :cond_6
    new-instance v20, Lorg/apache/lucene/index/SegmentInfos;

    invoke-direct/range {v20 .. v20}, Lorg/apache/lucene/index/SegmentInfos;-><init>()V

    .line 2306
    .local v20, "sis":Lorg/apache/lucene/index/SegmentInfos;
    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Lorg/apache/lucene/index/SegmentInfos;->read(Lorg/apache/lucene/store/Directory;)V

    .line 2307
    new-instance v11, Ljava/util/HashSet;

    invoke-direct {v11}, Ljava/util/HashSet;-><init>()V

    .line 2308
    .local v11, "dsFilesCopied":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v12, Ljava/util/HashMap;

    invoke-direct {v12}, Ljava/util/HashMap;-><init>()V

    .line 2309
    .local v12, "dsNames":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v8, Ljava/util/HashSet;

    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    .line 2310
    .local v8, "copiedFiles":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-virtual/range {v20 .. v20}, Lorg/apache/lucene/index/SegmentInfos;->iterator()Ljava/util/Iterator;

    move-result-object v24

    :goto_2
    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_7

    .line 2301
    add-int/lit8 v2, v22, 0x1

    move/from16 v22, v2

    goto/16 :goto_0

    .line 2310
    :cond_7
    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .line 2311
    .local v15, "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    sget-boolean v2, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v2, :cond_a

    move-object/from16 v0, v16

    invoke-interface {v0, v15}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    new-instance v2, Ljava/lang/AssertionError;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "dup info dir="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v15, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v4, v4, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " name="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v15, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v4, v4, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 2328
    .end local v8    # "copiedFiles":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v10    # "dir":Lorg/apache/lucene/store/Directory;
    .end local v11    # "dsFilesCopied":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v12    # "dsNames":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v15    # "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .end local v20    # "sis":Lorg/apache/lucene/index/SegmentInfos;
    :catchall_0
    move-exception v2

    .line 2329
    if-nez v21, :cond_9

    .line 2330
    :try_start_6
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_8
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_d

    .line 2339
    :cond_9
    throw v2
    :try_end_6
    .catch Ljava/lang/OutOfMemoryError; {:try_start_6 .. :try_end_6} :catch_0

    .line 2362
    .end local v16    # "infos":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfoPerCommit;>;"
    .end local v21    # "success":Z
    :catch_0
    move-exception v18

    .line 2363
    .local v18, "oom":Ljava/lang/OutOfMemoryError;
    const-string v2, "addIndexes(Directory...)"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/index/IndexWriter;->handleOOM(Ljava/lang/OutOfMemoryError;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2313
    .end local v18    # "oom":Ljava/lang/OutOfMemoryError;
    .restart local v8    # "copiedFiles":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .restart local v10    # "dir":Lorg/apache/lucene/store/Directory;
    .restart local v11    # "dsFilesCopied":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .restart local v12    # "dsNames":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v15    # "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .restart local v16    # "infos":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfoPerCommit;>;"
    .restart local v20    # "sis":Lorg/apache/lucene/index/SegmentInfos;
    .restart local v21    # "success":Z
    :cond_a
    :try_start_7
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/IndexWriter;->newSegmentName()Ljava/lang/String;

    move-result-object v17

    .line 2315
    .local v17, "newSegName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v3, "IW"

    invoke-virtual {v2, v3}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 2316
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v3, "IW"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "addIndexes: process segment origName="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v15, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v5, v5, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " newName="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " info="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 2319
    :cond_b
    new-instance v9, Lorg/apache/lucene/store/IOContext;

    new-instance v2, Lorg/apache/lucene/store/MergeInfo;

    iget-object v3, v15, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v3}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v3

    invoke-virtual {v15}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->sizeInBytes()J

    move-result-wide v4

    const/4 v6, 0x1

    const/4 v7, -0x1

    invoke-direct/range {v2 .. v7}, Lorg/apache/lucene/store/MergeInfo;-><init>(IJZI)V

    invoke-direct {v9, v2}, Lorg/apache/lucene/store/IOContext;-><init>(Lorg/apache/lucene/store/MergeInfo;)V

    .line 2321
    .local v9, "context":Lorg/apache/lucene/store/IOContext;
    iget-object v2, v15, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lorg/apache/lucene/index/IndexWriter;->getFieldInfos(Lorg/apache/lucene/index/SegmentInfo;)Lorg/apache/lucene/index/FieldInfos;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/lucene/index/FieldInfos;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_c

    move-object/from16 v2, p0

    move-object v3, v15

    move-object/from16 v4, v17

    move-object v5, v12

    move-object v6, v11

    move-object v7, v9

    .line 2324
    invoke-direct/range {v2 .. v8}, Lorg/apache/lucene/index/IndexWriter;->copySegmentAsIs(Lorg/apache/lucene/index/SegmentInfoPerCommit;Ljava/lang/String;Ljava/util/Map;Ljava/util/Set;Lorg/apache/lucene/store/IOContext;Ljava/util/Set;)Lorg/apache/lucene/index/SegmentInfoPerCommit;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 2321
    :cond_c
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lorg/apache/lucene/index/FieldInfo;

    .line 2322
    .local v13, "fi":Lorg/apache/lucene/index/FieldInfo;
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/index/IndexWriter;->globalFieldNumberMap:Lorg/apache/lucene/index/FieldInfos$FieldNumbers;

    iget-object v4, v13, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    iget v5, v13, Lorg/apache/lucene/index/FieldInfo;->number:I

    invoke-virtual {v13}, Lorg/apache/lucene/index/FieldInfo;->getDocValuesType()Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, Lorg/apache/lucene/index/FieldInfos$FieldNumbers;->addOrGet(Ljava/lang/String;ILorg/apache/lucene/index/FieldInfo$DocValuesType;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_3

    .line 2330
    .end local v8    # "copiedFiles":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v9    # "context":Lorg/apache/lucene/store/IOContext;
    .end local v10    # "dir":Lorg/apache/lucene/store/Directory;
    .end local v11    # "dsFilesCopied":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v12    # "dsNames":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v13    # "fi":Lorg/apache/lucene/index/FieldInfo;
    .end local v15    # "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .end local v17    # "newSegName":Ljava/lang/String;
    .end local v20    # "sis":Lorg/apache/lucene/index/SegmentInfos;
    :cond_d
    :try_start_8
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .line 2331
    .local v19, "sipc":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->files()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_8

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;
    :try_end_8
    .catch Ljava/lang/OutOfMemoryError; {:try_start_8 .. :try_end_8} :catch_0

    .line 2333
    .local v14, "file":Ljava/lang/String;
    :try_start_9
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v5, v14}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_9 .. :try_end_9} :catch_0

    goto :goto_4

    .line 2334
    :catch_1
    move-exception v5

    goto :goto_4

    .line 2330
    .end local v14    # "file":Ljava/lang/String;
    .end local v19    # "sipc":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    :cond_e
    :try_start_a
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .line 2331
    .restart local v19    # "sipc":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->files()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;
    :try_end_a
    .catch Ljava/lang/OutOfMemoryError; {:try_start_a .. :try_end_a} :catch_0

    .line 2333
    .restart local v14    # "file":Ljava/lang/String;
    :try_start_b
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v4, v14}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_b .. :try_end_b} :catch_0

    goto :goto_5

    .line 2334
    :catch_2
    move-exception v4

    goto :goto_5

    .line 2346
    .end local v14    # "file":Ljava/lang/String;
    .end local v19    # "sipc":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    :catchall_1
    move-exception v2

    .line 2347
    if-nez v21, :cond_10

    .line 2348
    :try_start_c
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_f
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_11

    .line 2357
    :cond_10
    throw v2

    .line 2341
    :catchall_2
    move-exception v2

    monitor-exit p0
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    :try_start_d
    throw v2
    :try_end_d
    .catch Ljava/lang/OutOfMemoryError; {:try_start_d .. :try_end_d} :catch_0

    .line 2348
    :cond_11
    :try_start_e
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .line 2349
    .restart local v19    # "sipc":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->files()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_6
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_f

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    .line 2351
    .restart local v14    # "file":Ljava/lang/String;
    :try_start_f
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v5, v14}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_f
    .catch Ljava/lang/Throwable; {:try_start_f .. :try_end_f} :catch_3
    .catchall {:try_start_f .. :try_end_f} :catchall_2

    goto :goto_6

    .line 2352
    :catch_3
    move-exception v5

    goto :goto_6

    .line 2348
    .end local v14    # "file":Ljava/lang/String;
    .end local v19    # "sipc":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    :cond_12
    :try_start_10
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .line 2349
    .restart local v19    # "sipc":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->files()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_7
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_2

    .line 2351
    .restart local v14    # "file":Ljava/lang/String;
    :try_start_11
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v4, v14}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_11
    .catch Ljava/lang/Throwable; {:try_start_11 .. :try_end_11} :catch_4
    .catchall {:try_start_11 .. :try_end_11} :catchall_2

    goto :goto_7

    .line 2352
    :catch_4
    move-exception v4

    goto :goto_7
.end method

.method declared-synchronized addMergeException(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V
    .locals 4
    .param p1, "merge"    # Lorg/apache/lucene/index/MergePolicy$OneMerge;

    .prologue
    .line 3863
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->getException()Ljava/lang/Throwable;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 3864
    :cond_0
    :try_start_1
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->mergeExceptions:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-wide v0, p0, Lorg/apache/lucene/index/IndexWriter;->mergeGen:J

    iget-wide v2, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->mergeGen:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 3865
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->mergeExceptions:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3867
    :cond_1
    monitor-exit p0

    return-void
.end method

.method final declared-synchronized applyAllDeletes()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2971
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->flushDeletesCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 2973
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->bufferedDeletesStream:Lorg/apache/lucene/index/BufferedDeletesStream;

    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->readerPool:Lorg/apache/lucene/index/IndexWriter$ReaderPool;

    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v4}, Lorg/apache/lucene/index/SegmentInfos;->asList()Ljava/util/List;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/apache/lucene/index/BufferedDeletesStream;->applyDeletes(Lorg/apache/lucene/index/IndexWriter$ReaderPool;Ljava/util/List;)Lorg/apache/lucene/index/BufferedDeletesStream$ApplyDeletesResult;

    move-result-object v1

    .line 2974
    .local v1, "result":Lorg/apache/lucene/index/BufferedDeletesStream$ApplyDeletesResult;
    iget-boolean v2, v1, Lorg/apache/lucene/index/BufferedDeletesStream$ApplyDeletesResult;->anyDeletes:Z

    if-eqz v2, :cond_0

    .line 2975
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->checkpoint()V

    .line 2977
    :cond_0
    iget-boolean v2, p0, Lorg/apache/lucene/index/IndexWriter;->keepFullyDeletedSegments:Z

    if-nez v2, :cond_3

    iget-object v2, v1, Lorg/apache/lucene/index/BufferedDeletesStream$ApplyDeletesResult;->allDeleted:Ljava/util/List;

    if-eqz v2, :cond_3

    .line 2978
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v3, "IW"

    invoke-virtual {v2, v3}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2979
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v3, "IW"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "drop 100% deleted segments: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v1, Lorg/apache/lucene/index/BufferedDeletesStream$ApplyDeletesResult;->allDeleted:Ljava/util/List;

    invoke-virtual {p0, v5}, Lorg/apache/lucene/index/IndexWriter;->segString(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 2981
    :cond_1
    iget-object v2, v1, Lorg/apache/lucene/index/BufferedDeletesStream$ApplyDeletesResult;->allDeleted:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_4

    .line 2991
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->checkpoint()V

    .line 2993
    :cond_3
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->bufferedDeletesStream:Lorg/apache/lucene/index/BufferedDeletesStream;

    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/index/BufferedDeletesStream;->prune(Lorg/apache/lucene/index/SegmentInfos;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2994
    monitor-exit p0

    return-void

    .line 2981
    :cond_4
    :try_start_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .line 2986
    .local v0, "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->mergingSegments:Ljava/util/HashSet;

    invoke-virtual {v3, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 2987
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v3, v0}, Lorg/apache/lucene/index/SegmentInfos;->remove(Lorg/apache/lucene/index/SegmentInfoPerCommit;)V

    .line 2988
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->readerPool:Lorg/apache/lucene/index/IndexWriter$ReaderPool;

    invoke-virtual {v3, v0}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->drop(Lorg/apache/lucene/index/SegmentInfoPerCommit;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2971
    .end local v0    # "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .end local v1    # "result":Lorg/apache/lucene/index/BufferedDeletesStream$ApplyDeletesResult;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method declared-synchronized checkpoint()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2176
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lorg/apache/lucene/index/IndexWriter;->changeCount:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/lucene/index/IndexWriter;->changeCount:J

    .line 2177
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentInfos;->changed()V

    .line 2178
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/index/IndexFileDeleter;->checkpoint(Lorg/apache/lucene/index/SegmentInfos;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2179
    monitor-exit p0

    return-void

    .line 2176
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 857
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexWriter;->close(Z)V

    .line 858
    return-void
.end method

.method public close(Z)V
    .locals 2
    .param p1, "waitForMerges"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 887
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->commitLock:Ljava/lang/Object;

    monitor-enter v1

    .line 888
    :try_start_0
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexWriter;->shouldClose()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 892
    iget-boolean v0, p0, Lorg/apache/lucene/index/IndexWriter;->hitOOM:Z

    if-eqz v0, :cond_1

    .line 893
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexWriter;->rollbackInternal()V

    .line 887
    :cond_0
    :goto_0
    monitor-exit v1

    .line 899
    return-void

    .line 895
    :cond_1
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/index/IndexWriter;->closeInternal(ZZ)V

    goto :goto_0

    .line 887
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final commit()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2818
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 2819
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexWriter;->commitInternal()V

    .line 2820
    return-void
.end method

.method public declared-synchronized deleteAll()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2057
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2058
    const/4 v1, 0x0

    .line 2062
    .local v1, "success":Z
    const/4 v2, 0x0

    :try_start_1
    invoke-direct {p0, v2}, Lorg/apache/lucene/index/IndexWriter;->finishMerges(Z)V

    .line 2065
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v2}, Lorg/apache/lucene/index/DocumentsWriter;->abort()V

    .line 2068
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v2}, Lorg/apache/lucene/index/SegmentInfos;->clear()V

    .line 2071
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lorg/apache/lucene/index/IndexFileDeleter;->checkpoint(Lorg/apache/lucene/index/SegmentInfos;Z)V

    .line 2072
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    invoke-virtual {v2}, Lorg/apache/lucene/index/IndexFileDeleter;->refresh()V

    .line 2074
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->globalFieldNumberMap:Lorg/apache/lucene/index/FieldInfos$FieldNumbers;

    invoke-virtual {v2}, Lorg/apache/lucene/index/FieldInfos$FieldNumbers;->clear()V

    .line 2077
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->readerPool:Lorg/apache/lucene/index/IndexWriter$ReaderPool;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->dropAll(Z)V

    .line 2080
    iget-wide v2, p0, Lorg/apache/lucene/index/IndexWriter;->changeCount:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lorg/apache/lucene/index/IndexWriter;->changeCount:J

    .line 2081
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v2}, Lorg/apache/lucene/index/SegmentInfos;->changed()V
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2082
    const/4 v1, 0x1

    .line 2086
    if-nez v1, :cond_0

    .line 2087
    :try_start_2
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v3, "IW"

    invoke-virtual {v2, v3}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2088
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v3, "IW"

    const-string v4, "hit exception during deleteAll"

    invoke-virtual {v2, v3, v4}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2092
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 2083
    :catch_0
    move-exception v0

    .line 2084
    .local v0, "oom":Ljava/lang/OutOfMemoryError;
    :try_start_3
    const-string v2, "deleteAll"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/index/IndexWriter;->handleOOM(Ljava/lang/OutOfMemoryError;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2086
    if-nez v1, :cond_0

    .line 2087
    :try_start_4
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v3, "IW"

    invoke-virtual {v2, v3}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2088
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v3, "IW"

    const-string v4, "hit exception during deleteAll"

    invoke-virtual {v2, v3, v4}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 2057
    .end local v0    # "oom":Ljava/lang/OutOfMemoryError;
    .end local v1    # "success":Z
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 2085
    .restart local v1    # "success":Z
    :catchall_1
    move-exception v2

    .line 2086
    if-nez v1, :cond_1

    .line 2087
    :try_start_5
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v4, "IW"

    invoke-virtual {v3, v4}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2088
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v4, "IW"

    const-string v5, "hit exception during deleteAll"

    invoke-virtual {v3, v4, v5}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 2091
    :cond_1
    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method

.method public deleteDocuments(Lorg/apache/lucene/index/Term;)V
    .locals 4
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1289
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 1291
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    const/4 v2, 0x1

    new-array v2, v2, [Lorg/apache/lucene/index/Term;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v1, v2}, Lorg/apache/lucene/index/DocumentsWriter;->deleteTerms([Lorg/apache/lucene/index/Term;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 1295
    :goto_0
    return-void

    .line 1292
    :catch_0
    move-exception v0

    .line 1293
    .local v0, "oom":Ljava/lang/OutOfMemoryError;
    const-string v1, "deleteDocuments(Term)"

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/index/IndexWriter;->handleOOM(Ljava/lang/OutOfMemoryError;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public deleteDocuments(Lorg/apache/lucene/search/Query;)V
    .locals 4
    .param p1, "query"    # Lorg/apache/lucene/search/Query;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1408
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 1410
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    const/4 v2, 0x1

    new-array v2, v2, [Lorg/apache/lucene/search/Query;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v1, v2}, Lorg/apache/lucene/index/DocumentsWriter;->deleteQueries([Lorg/apache/lucene/search/Query;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 1414
    :goto_0
    return-void

    .line 1411
    :catch_0
    move-exception v0

    .line 1412
    .local v0, "oom":Ljava/lang/OutOfMemoryError;
    const-string v1, "deleteDocuments(Query)"

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/index/IndexWriter;->handleOOM(Ljava/lang/OutOfMemoryError;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public varargs deleteDocuments([Lorg/apache/lucene/index/Term;)V
    .locals 2
    .param p1, "terms"    # [Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1388
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 1390
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/index/DocumentsWriter;->deleteTerms([Lorg/apache/lucene/index/Term;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 1394
    :goto_0
    return-void

    .line 1391
    :catch_0
    move-exception v0

    .line 1392
    .local v0, "oom":Ljava/lang/OutOfMemoryError;
    const-string v1, "deleteDocuments(Term..)"

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/index/IndexWriter;->handleOOM(Ljava/lang/OutOfMemoryError;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public varargs deleteDocuments([Lorg/apache/lucene/search/Query;)V
    .locals 2
    .param p1, "queries"    # [Lorg/apache/lucene/search/Query;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1430
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 1432
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/index/DocumentsWriter;->deleteQueries([Lorg/apache/lucene/search/Query;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 1436
    :goto_0
    return-void

    .line 1433
    :catch_0
    move-exception v0

    .line 1434
    .local v0, "oom":Ljava/lang/OutOfMemoryError;
    const-string v1, "deleteDocuments(Query..)"

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/index/IndexWriter;->handleOOM(Ljava/lang/OutOfMemoryError;Ljava/lang/String;)V

    goto :goto_0
.end method

.method final declared-synchronized deleteNewFiles(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4267
    .local p1, "files":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/IndexFileDeleter;->deleteNewFiles(Ljava/util/Collection;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4268
    monitor-exit p0

    return-void

    .line 4267
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized deletePendingFiles()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4204
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexFileDeleter;->deletePendingFiles()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4205
    monitor-exit p0

    return-void

    .line 4204
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized deleteUnusedFiles()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4197
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen(Z)V

    .line 4198
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexFileDeleter;->deletePendingFiles()V

    .line 4199
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexFileDeleter;->revisitPolicy()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4200
    monitor-exit p0

    return-void

    .line 4197
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected doAfterFlush()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2632
    return-void
.end method

.method protected doBeforeFlush()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2638
    return-void
.end method

.method protected final ensureOpen()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/store/AlreadyClosedException;
        }
    .end annotation

    .prologue
    .line 605
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen(Z)V

    .line 606
    return-void
.end method

.method protected final ensureOpen(Z)V
    .locals 2
    .param p1, "failIfClosing"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/store/AlreadyClosedException;
        }
    .end annotation

    .prologue
    .line 590
    iget-boolean v0, p0, Lorg/apache/lucene/index/IndexWriter;->closed:Z

    if-nez v0, :cond_0

    if-eqz p1, :cond_1

    iget-boolean v0, p0, Lorg/apache/lucene/index/IndexWriter;->closing:Z

    if-eqz v0, :cond_1

    .line 591
    :cond_0
    new-instance v0, Lorg/apache/lucene/store/AlreadyClosedException;

    const-string v1, "this IndexWriter is closed"

    invoke-direct {v0, v1}, Lorg/apache/lucene/store/AlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 593
    :cond_1
    return-void
.end method

.method protected final flush(ZZ)V
    .locals 2
    .param p1, "triggerMerge"    # Z
    .param p2, "applyAllDeletes"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2905
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen(Z)V

    .line 2906
    invoke-direct {p0, p2}, Lorg/apache/lucene/index/IndexWriter;->doFlush(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 2907
    sget-object v0, Lorg/apache/lucene/index/MergePolicy$MergeTrigger;->FULL_FLUSH:Lorg/apache/lucene/index/MergePolicy$MergeTrigger;

    const/4 v1, -0x1

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/index/IndexWriter;->maybeMerge(Lorg/apache/lucene/index/MergePolicy$MergeTrigger;I)V

    .line 2909
    :cond_0
    return-void
.end method

.method final declared-synchronized flushFailed(Lorg/apache/lucene/index/SegmentInfo;)V
    .locals 2
    .param p1, "info"    # Lorg/apache/lucene/index/SegmentInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4275
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    iget-object v1, p1, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/IndexFileDeleter;->refresh(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4276
    monitor-exit p0

    return-void

    .line 4275
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public forceMerge(I)V
    .locals 1
    .param p1, "maxNumSegments"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1622
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/index/IndexWriter;->forceMerge(IZ)V

    .line 1623
    return-void
.end method

.method public forceMerge(IZ)V
    .locals 11
    .param p1, "maxNumSegments"    # I
    .param p2, "doWait"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x1

    .line 1636
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 1638
    if-ge p1, v10, :cond_0

    .line 1639
    new-instance v6, Ljava/lang/IllegalArgumentException;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "maxNumSegments must be >= 1; got "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 1641
    :cond_0
    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v7, "IW"

    invoke-virtual {v6, v7}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1642
    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v7, "IW"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "forceMerge: index now "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->segString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 1643
    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v7, "IW"

    const-string v8, "now flush at forceMerge"

    invoke-virtual {v6, v7, v8}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 1646
    :cond_1
    invoke-virtual {p0, v10, v10}, Lorg/apache/lucene/index/IndexWriter;->flush(ZZ)V

    .line 1648
    monitor-enter p0

    .line 1649
    :try_start_0
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexWriter;->resetMergeExceptions()V

    .line 1650
    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->segmentsToMerge:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->clear()V

    .line 1651
    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v6}, Lorg/apache/lucene/index/SegmentInfos;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_2

    .line 1654
    iput p1, p0, Lorg/apache/lucene/index/IndexWriter;->mergeMaxNumSegments:I

    .line 1658
    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->pendingMerges:Ljava/util/LinkedList;

    invoke-virtual {v6}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_3

    .line 1663
    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->runningMerges:Ljava/util/Set;

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_4

    .line 1648
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1669
    sget-object v6, Lorg/apache/lucene/index/MergePolicy$MergeTrigger;->EXPLICIT:Lorg/apache/lucene/index/MergePolicy$MergeTrigger;

    invoke-direct {p0, v6, p1}, Lorg/apache/lucene/index/IndexWriter;->maybeMerge(Lorg/apache/lucene/index/MergePolicy$MergeTrigger;I)V

    .line 1671
    if-eqz p2, :cond_b

    .line 1672
    monitor-enter p0

    .line 1675
    :goto_3
    :try_start_1
    iget-boolean v6, p0, Lorg/apache/lucene/index/IndexWriter;->hitOOM:Z

    if-eqz v6, :cond_5

    .line 1676
    new-instance v6, Ljava/lang/IllegalStateException;

    const-string v7, "this writer hit an OutOfMemoryError; cannot complete forceMerge"

    invoke-direct {v6, v7}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 1672
    :catchall_0
    move-exception v6

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v6

    .line 1651
    :cond_2
    :try_start_2
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .line 1652
    .local v2, "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->segmentsToMerge:Ljava/util/Map;

    sget-object v8, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-interface {v7, v2, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1648
    .end local v2    # "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    :catchall_1
    move-exception v6

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v6

    .line 1658
    :cond_3
    :try_start_3
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/index/MergePolicy$OneMerge;

    .line 1659
    .local v3, "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    iput p1, v3, Lorg/apache/lucene/index/MergePolicy$OneMerge;->maxNumSegments:I

    .line 1660
    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->segmentsToMerge:Ljava/util/Map;

    iget-object v8, v3, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    sget-object v9, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-interface {v7, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 1663
    .end local v3    # "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    :cond_4
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/index/MergePolicy$OneMerge;

    .line 1664
    .restart local v3    # "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    iput p1, v3, Lorg/apache/lucene/index/MergePolicy$OneMerge;->maxNumSegments:I

    .line 1665
    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->segmentsToMerge:Ljava/util/Map;

    iget-object v8, v3, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    sget-object v9, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-interface {v7, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_2

    .line 1679
    .end local v3    # "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    :cond_5
    :try_start_4
    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->mergeExceptions:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-lez v6, :cond_6

    .line 1682
    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->mergeExceptions:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v4

    .line 1683
    .local v4, "size":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_4
    if-lt v1, v4, :cond_7

    .line 1695
    .end local v1    # "i":I
    .end local v4    # "size":I
    :cond_6
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexWriter;->maxNumSegmentsMergesPending()Z

    move-result v6

    if-eqz v6, :cond_a

    .line 1696
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexWriter;->doWait()V

    goto :goto_3

    .line 1684
    .restart local v1    # "i":I
    .restart local v4    # "size":I
    :cond_7
    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->mergeExceptions:Ljava/util/List;

    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/index/MergePolicy$OneMerge;

    .line 1685
    .restart local v3    # "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    iget v6, v3, Lorg/apache/lucene/index/MergePolicy$OneMerge;->maxNumSegments:I

    const/4 v7, -0x1

    if-eq v6, v7, :cond_9

    .line 1686
    new-instance v0, Ljava/io/IOException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "background merge hit exception: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v3, v7}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segString(Lorg/apache/lucene/store/Directory;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v0, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 1687
    .local v0, "err":Ljava/io/IOException;
    invoke-virtual {v3}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->getException()Ljava/lang/Throwable;

    move-result-object v5

    .line 1688
    .local v5, "t":Ljava/lang/Throwable;
    if-eqz v5, :cond_8

    .line 1689
    invoke-virtual {v0, v5}, Ljava/io/IOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 1690
    :cond_8
    throw v0

    .line 1683
    .end local v0    # "err":Ljava/io/IOException;
    .end local v5    # "t":Ljava/lang/Throwable;
    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 1672
    .end local v1    # "i":I
    .end local v3    # "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    .end local v4    # "size":I
    :cond_a
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1706
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 1712
    :cond_b
    return-void
.end method

.method public forceMergeDeletes()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1832
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexWriter;->forceMergeDeletes(Z)V

    .line 1833
    return-void
.end method

.method public forceMergeDeletes(Z)V
    .locals 11
    .param p1, "doWait"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    .line 1747
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 1749
    invoke-virtual {p0, v7, v7}, Lorg/apache/lucene/index/IndexWriter;->flush(ZZ)V

    .line 1751
    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v8, "IW"

    invoke-virtual {v7, v8}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1752
    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v8, "IW"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "forceMergeDeletes: index now "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->segString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 1757
    :cond_0
    monitor-enter p0

    .line 1758
    :try_start_0
    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->mergePolicy:Lorg/apache/lucene/index/MergePolicy;

    iget-object v8, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v7, v8}, Lorg/apache/lucene/index/MergePolicy;->findForcedDeletesMerges(Lorg/apache/lucene/index/SegmentInfos;)Lorg/apache/lucene/index/MergePolicy$MergeSpecification;

    move-result-object v5

    .line 1759
    .local v5, "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    if-eqz v5, :cond_1

    .line 1760
    iget-object v7, v5, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;->merges:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v3

    .line 1761
    .local v3, "numMerges":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v3, :cond_4

    .line 1757
    .end local v0    # "i":I
    .end local v3    # "numMerges":I
    :cond_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1766
    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->mergeScheduler:Lorg/apache/lucene/index/MergeScheduler;

    invoke-virtual {v7, p0}, Lorg/apache/lucene/index/MergeScheduler;->merge(Lorg/apache/lucene/index/IndexWriter;)V

    .line 1768
    if-eqz v5, :cond_3

    if-eqz p1, :cond_3

    .line 1769
    iget-object v7, v5, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;->merges:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v3

    .line 1770
    .restart local v3    # "numMerges":I
    monitor-enter p0

    .line 1771
    const/4 v4, 0x1

    .line 1772
    .local v4, "running":Z
    :cond_2
    :goto_1
    if-nez v4, :cond_5

    .line 1770
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1805
    .end local v3    # "numMerges":I
    .end local v4    # "running":Z
    :cond_3
    return-void

    .line 1762
    .restart local v0    # "i":I
    .restart local v3    # "numMerges":I
    :cond_4
    :try_start_2
    iget-object v7, v5, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;->merges:Ljava/util/List;

    invoke-interface {v7, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/lucene/index/MergePolicy$OneMerge;

    invoke-virtual {p0, v7}, Lorg/apache/lucene/index/IndexWriter;->registerMerge(Lorg/apache/lucene/index/MergePolicy$OneMerge;)Z

    .line 1761
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1757
    .end local v0    # "i":I
    .end local v3    # "numMerges":I
    .end local v5    # "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    :catchall_0
    move-exception v7

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v7

    .line 1774
    .restart local v3    # "numMerges":I
    .restart local v4    # "running":Z
    .restart local v5    # "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    :cond_5
    :try_start_3
    iget-boolean v7, p0, Lorg/apache/lucene/index/IndexWriter;->hitOOM:Z

    if-eqz v7, :cond_6

    .line 1775
    new-instance v7, Ljava/lang/IllegalStateException;

    const-string v8, "this writer hit an OutOfMemoryError; cannot complete forceMergeDeletes"

    invoke-direct {v7, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 1770
    :catchall_1
    move-exception v7

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v7

    .line 1781
    :cond_6
    const/4 v4, 0x0

    .line 1782
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_2
    if-lt v0, v3, :cond_7

    .line 1796
    if-eqz v4, :cond_2

    .line 1797
    :try_start_4
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexWriter;->doWait()V

    goto :goto_1

    .line 1783
    :cond_7
    iget-object v7, v5, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;->merges:Ljava/util/List;

    invoke-interface {v7, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/index/MergePolicy$OneMerge;

    .line 1784
    .local v2, "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->pendingMerges:Ljava/util/LinkedList;

    invoke-virtual {v7, v2}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_8

    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->runningMerges:Ljava/util/Set;

    invoke-interface {v7, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 1785
    :cond_8
    const/4 v4, 0x1

    .line 1787
    :cond_9
    invoke-virtual {v2}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->getException()Ljava/lang/Throwable;

    move-result-object v6

    .line 1788
    .local v6, "t":Ljava/lang/Throwable;
    if-eqz v6, :cond_a

    .line 1789
    new-instance v1, Ljava/io/IOException;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "background merge hit exception: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v2, v8}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segString(Lorg/apache/lucene/store/Directory;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v1, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 1790
    .local v1, "ioe":Ljava/io/IOException;
    invoke-virtual {v1, v6}, Ljava/io/IOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 1791
    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1782
    .end local v1    # "ioe":Ljava/io/IOException;
    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method public getAnalyzer()Lorg/apache/lucene/analysis/Analyzer;
    .locals 1

    .prologue
    .line 1039
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 1040
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    return-object v0
.end method

.method final getBufferedDeleteTermsSize()I
    .locals 1

    .prologue
    .line 3871
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocumentsWriter;->getBufferedDeleteTermsSize()I

    move-result v0

    return v0
.end method

.method public final declared-synchronized getCommitData()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2780
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentInfos;->getUserData()Ljava/util/Map;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getConfig()Lorg/apache/lucene/index/LiveIndexWriterConfig;
    .locals 1

    .prologue
    .line 794
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen(Z)V

    .line 795
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->config:Lorg/apache/lucene/index/LiveIndexWriterConfig;

    return-object v0
.end method

.method public getDirectory()Lorg/apache/lucene/store/Directory;
    .locals 1

    .prologue
    .line 1034
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    return-object v0
.end method

.method final declared-synchronized getDocCount(I)I
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 1520
    monitor-enter p0

    if-ltz p1, :cond_0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentInfos;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 1521
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/SegmentInfos;->info(I)Lorg/apache/lucene/index/SegmentInfoPerCommit;

    move-result-object v0

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 1523
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0

    .line 1520
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method getDocsWriter()Lorg/apache/lucene/index/DocumentsWriter;
    .locals 2

    .prologue
    .line 3006
    const/4 v0, 0x0

    .line 3007
    .local v0, "test":Z
    sget-boolean v1, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    const/4 v0, 0x1

    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 3008
    :cond_0
    if-eqz v0, :cond_1

    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method final getFlushCount()I
    .locals 1

    .prologue
    .line 1529
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->flushCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    return v0
.end method

.method final getFlushDeletesCount()I
    .locals 1

    .prologue
    .line 1534
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->flushDeletesCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    return v0
.end method

.method final declared-synchronized getIndexFileNames()Ljava/util/Collection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1515
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/index/SegmentInfos;->files(Lorg/apache/lucene/store/Directory;Z)Ljava/util/Collection;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method getKeepFullyDeletedSegments()Z
    .locals 1

    .prologue
    .line 3939
    iget-boolean v0, p0, Lorg/apache/lucene/index/IndexWriter;->keepFullyDeletedSegments:Z

    return v0
.end method

.method public declared-synchronized getMergingSegments()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/index/SegmentInfoPerCommit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1909
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->mergingSegments:Ljava/util/HashSet;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getNextMerge()Lorg/apache/lucene/index/MergePolicy$OneMerge;
    .locals 2

    .prologue
    .line 1919
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->pendingMerges:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_0

    .line 1920
    const/4 v0, 0x0

    .line 1925
    :goto_0
    monitor-exit p0

    return-object v0

    .line 1923
    :cond_0
    :try_start_1
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->pendingMerges:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;

    .line 1924
    .local v0, "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->runningMerges:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1919
    .end local v0    # "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method final getNumBufferedDeleteTerms()I
    .locals 1

    .prologue
    .line 3876
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocumentsWriter;->getNumBufferedDeleteTerms()I

    move-result v0

    return v0
.end method

.method final declared-synchronized getNumBufferedDocuments()I
    .locals 1

    .prologue
    .line 1510
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocumentsWriter;->getNumDocs()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method getReader()Lorg/apache/lucene/index/DirectoryReader;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 277
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexWriter;->getReader(Z)Lorg/apache/lucene/index/DirectoryReader;

    move-result-object v0

    return-object v0
.end method

.method getReader(Z)Lorg/apache/lucene/index/DirectoryReader;
    .locals 14
    .param p1, "applyAllDeletes"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 339
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 341
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 343
    .local v6, "tStart":J
    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v8, "IW"

    invoke-virtual {v5, v8}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 344
    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v8, "IW"

    const-string v9, "flush at getReader"

    invoke-virtual {v5, v8, v9}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 349
    :cond_0
    const/4 v5, 0x1

    iput-boolean v5, p0, Lorg/apache/lucene/index/IndexWriter;->poolReaders:Z

    .line 350
    const/4 v2, 0x0

    .line 351
    .local v2, "r":Lorg/apache/lucene/index/DirectoryReader;
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->doBeforeFlush()V

    .line 352
    const/4 v0, 0x0

    .line 360
    .local v0, "anySegmentFlushed":Z
    const/4 v4, 0x0

    .line 362
    .local v4, "success2":Z
    :try_start_0
    iget-object v8, p0, Lorg/apache/lucene/index/IndexWriter;->fullFlushLock:Ljava/lang/Object;

    monitor-enter v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    .line 363
    const/4 v3, 0x0

    .line 365
    .local v3, "success":Z
    :try_start_1
    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v5}, Lorg/apache/lucene/index/DocumentsWriter;->flushAllThreads()Z

    move-result v0

    .line 366
    if-nez v0, :cond_1

    .line 369
    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->flushCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 371
    :cond_1
    const/4 v3, 0x1

    .line 375
    monitor-enter p0
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 376
    :try_start_2
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/IndexWriter;->maybeApplyDeletes(Z)V

    .line 377
    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-static {p0, v5, p1}, Lorg/apache/lucene/index/StandardDirectoryReader;->open(Lorg/apache/lucene/index/IndexWriter;Lorg/apache/lucene/index/SegmentInfos;Z)Lorg/apache/lucene/index/DirectoryReader;

    move-result-object v2

    .line 378
    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v9, "IW"

    invoke-virtual {v5, v9}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 379
    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v9, "IW"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "return reader version="

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lorg/apache/lucene/index/DirectoryReader;->getVersion()J

    move-result-wide v12

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " reader="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5, v9, v10}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 375
    :cond_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 387
    if-nez v3, :cond_3

    .line 388
    :try_start_3
    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v9, "IW"

    invoke-virtual {v5, v9}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 389
    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v9, "IW"

    const-string v10, "hit exception during NRT reader"

    invoke-virtual {v5, v9, v10}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 393
    :cond_3
    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v5, v3}, Lorg/apache/lucene/index/DocumentsWriter;->finishFullFlush(Z)V

    .line 394
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->doAfterFlush()V

    .line 362
    monitor-exit v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 397
    if-eqz v0, :cond_4

    .line 398
    :try_start_4
    sget-object v5, Lorg/apache/lucene/index/MergePolicy$MergeTrigger;->FULL_FLUSH:Lorg/apache/lucene/index/MergePolicy$MergeTrigger;

    const/4 v8, -0x1

    invoke-direct {p0, v5, v8}, Lorg/apache/lucene/index/IndexWriter;->maybeMerge(Lorg/apache/lucene/index/MergePolicy$MergeTrigger;I)V

    .line 400
    :cond_4
    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v8, "IW"

    invoke-virtual {v5, v8}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 401
    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v8, "IW"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "getReader took "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    sub-long/2addr v10, v6

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " msec"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    .line 403
    :cond_5
    const/4 v4, 0x1

    .line 405
    if-nez v4, :cond_6

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/io/Closeable;

    const/4 v8, 0x0

    .line 406
    aput-object v2, v5, v8

    invoke-static {v5}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    :cond_6
    move-object v5, v2

    .line 409
    :goto_0
    return-object v5

    .line 375
    :catchall_0
    move-exception v5

    :try_start_5
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v5
    :try_end_6
    .catch Ljava/lang/OutOfMemoryError; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 382
    :catch_0
    move-exception v1

    .line 383
    .local v1, "oom":Ljava/lang/OutOfMemoryError;
    :try_start_7
    const-string v5, "getReader"

    invoke-direct {p0, v1, v5}, Lorg/apache/lucene/index/IndexWriter;->handleOOM(Ljava/lang/OutOfMemoryError;Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 387
    if-nez v3, :cond_7

    .line 388
    :try_start_8
    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v9, "IW"

    invoke-virtual {v5, v9}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 389
    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v9, "IW"

    const-string v10, "hit exception during NRT reader"

    invoke-virtual {v5, v9, v10}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 393
    :cond_7
    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v5, v3}, Lorg/apache/lucene/index/DocumentsWriter;->finishFullFlush(Z)V

    .line 394
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->doAfterFlush()V

    monitor-exit v8
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 405
    if-nez v4, :cond_8

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/io/Closeable;

    const/4 v8, 0x0

    .line 406
    aput-object v2, v5, v8

    invoke-static {v5}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 385
    :cond_8
    const/4 v5, 0x0

    goto :goto_0

    .line 386
    .end local v1    # "oom":Ljava/lang/OutOfMemoryError;
    :catchall_1
    move-exception v5

    .line 387
    if-nez v3, :cond_9

    .line 388
    :try_start_9
    iget-object v9, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v10, "IW"

    invoke-virtual {v9, v10}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 389
    iget-object v9, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v10, "IW"

    const-string v11, "hit exception during NRT reader"

    invoke-virtual {v9, v10, v11}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 393
    :cond_9
    iget-object v9, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v9, v3}, Lorg/apache/lucene/index/DocumentsWriter;->finishFullFlush(Z)V

    .line 394
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->doAfterFlush()V

    .line 395
    throw v5

    .line 362
    :catchall_2
    move-exception v5

    monitor-exit v8
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    :try_start_a
    throw v5
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    .line 404
    .end local v3    # "success":Z
    :catchall_3
    move-exception v5

    .line 405
    if-nez v4, :cond_a

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/io/Closeable;

    const/4 v9, 0x0

    .line 406
    aput-object v2, v8, v9

    invoke-static {v8}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 408
    :cond_a
    throw v5
.end method

.method final declared-synchronized getSegmentCount()I
    .locals 1

    .prologue
    .line 1505
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentInfos;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized hasDeletions()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1083
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 1084
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->bufferedDeletesStream:Lorg/apache/lucene/index/BufferedDeletesStream;

    invoke-virtual {v2}, Lorg/apache/lucene/index/BufferedDeletesStream;->any()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_1

    .line 1095
    :cond_0
    :goto_0
    monitor-exit p0

    return v1

    .line 1087
    :cond_1
    :try_start_1
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v2}, Lorg/apache/lucene/index/DocumentsWriter;->anyDeletions()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1090
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v2}, Lorg/apache/lucene/index/SegmentInfos;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_3

    .line 1095
    const/4 v1, 0x0

    goto :goto_0

    .line 1090
    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .line 1091
    .local v0, "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->hasDeletions()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_0

    .line 1083
    .end local v0    # "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized hasPendingMerges()Z
    .locals 1

    .prologue
    .line 1935
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->pendingMerges:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized isClosed()Z
    .locals 1

    .prologue
    .line 4168
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lorg/apache/lucene/index/IndexWriter;->closed:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method keepFullyDeletedSegments()V
    .locals 1

    .prologue
    .line 3935
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/index/IndexWriter;->keepFullyDeletedSegments:Z

    .line 3936
    return-void
.end method

.method public declared-synchronized maxDoc()I
    .locals 2

    .prologue
    .line 1048
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 1050
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    if-eqz v1, :cond_0

    .line 1051
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v1}, Lorg/apache/lucene/index/DocumentsWriter;->getNumDocs()I

    move-result v0

    .line 1055
    .local v0, "count":I
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentInfos;->totalDocCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    add-int/2addr v0, v1

    .line 1056
    monitor-exit p0

    return v0

    .line 1053
    .end local v0    # "count":I
    :cond_0
    const/4 v0, 0x0

    .restart local v0    # "count":I
    goto :goto_0

    .line 1048
    .end local v0    # "count":I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method final declared-synchronized maybeApplyDeletes(Z)V
    .locals 6
    .param p1, "applyAllDeletes"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2960
    monitor-enter p0

    if-eqz p1, :cond_2

    .line 2961
    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v1, "IW"

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2962
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v1, "IW"

    const-string v2, "apply all deletes during flush"

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 2964
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->applyAllDeletes()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2968
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 2965
    :cond_2
    :try_start_1
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v1, "IW"

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2966
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v1, "IW"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "don\'t apply deletes now delTermCount="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->bufferedDeletesStream:Lorg/apache/lucene/index/BufferedDeletesStream;

    invoke-virtual {v3}, Lorg/apache/lucene/index/BufferedDeletesStream;->numTerms()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " bytesUsed="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->bufferedDeletesStream:Lorg/apache/lucene/index/BufferedDeletesStream;

    invoke-virtual {v3}, Lorg/apache/lucene/index/BufferedDeletesStream;->bytesUsed()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2960
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final maybeMerge()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1853
    sget-object v0, Lorg/apache/lucene/index/MergePolicy$MergeTrigger;->EXPLICIT:Lorg/apache/lucene/index/MergePolicy$MergeTrigger;

    const/4 v1, -0x1

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/index/IndexWriter;->maybeMerge(Lorg/apache/lucene/index/MergePolicy$MergeTrigger;I)V

    .line 1854
    return-void
.end method

.method public merge(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V
    .locals 10
    .param p1, "merge"    # Lorg/apache/lucene/index/MergePolicy$OneMerge;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v9, -0x1

    .line 3297
    const/4 v1, 0x0

    .line 3299
    .local v1, "success":Z
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 3304
    .local v4, "t0":J
    :try_start_0
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/IndexWriter;->mergeInit(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    .line 3309
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v6, "IW"

    invoke-virtual {v3, v6}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 3310
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v6, "IW"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "now merge\n  merge="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    invoke-virtual {p0, v8}, Lorg/apache/lucene/index/IndexWriter;->segString(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\n  index="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->segString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 3313
    :cond_0
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/IndexWriter;->mergeMiddle(Lorg/apache/lucene/index/MergePolicy$OneMerge;)I

    .line 3314
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/IndexWriter;->mergeSuccess(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3315
    const/4 v1, 0x1

    .line 3320
    :goto_0
    :try_start_1
    monitor-enter p0
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_1

    .line 3321
    :try_start_2
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/IndexWriter;->mergeFinish(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    .line 3323
    if-nez v1, :cond_2

    .line 3324
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v6, "IW"

    invoke-virtual {v3, v6}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 3325
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v6, "IW"

    const-string v7, "hit exception during merge"

    invoke-virtual {v3, v6, v7}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 3327
    :cond_1
    iget-object v3, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    iget-object v6, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    invoke-virtual {v3, v6}, Lorg/apache/lucene/index/SegmentInfos;->contains(Lorg/apache/lucene/index/SegmentInfoPerCommit;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 3328
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    iget-object v6, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    iget-object v6, v6, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v6, v6, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    invoke-virtual {v3, v6}, Lorg/apache/lucene/index/IndexFileDeleter;->refresh(Ljava/lang/String;)V

    .line 3335
    :cond_2
    if-eqz v1, :cond_4

    invoke-virtual {p1}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->isAborted()Z

    move-result v3

    if-nez v3, :cond_4

    iget v3, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->maxNumSegments:I

    if-ne v3, v9, :cond_3

    iget-boolean v3, p0, Lorg/apache/lucene/index/IndexWriter;->closed:Z

    if-nez v3, :cond_4

    iget-boolean v3, p0, Lorg/apache/lucene/index/IndexWriter;->closing:Z

    if-nez v3, :cond_4

    .line 3336
    :cond_3
    sget-object v3, Lorg/apache/lucene/index/MergePolicy$MergeTrigger;->MERGE_FINISHED:Lorg/apache/lucene/index/MergePolicy$MergeTrigger;

    iget v6, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->maxNumSegments:I

    invoke-direct {p0, v3, v6}, Lorg/apache/lucene/index/IndexWriter;->updatePendingMerges(Lorg/apache/lucene/index/MergePolicy$MergeTrigger;I)V

    .line 3320
    :cond_4
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 3343
    :goto_1
    iget-object v3, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    if-eqz v3, :cond_5

    invoke-virtual {p1}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->isAborted()Z

    move-result v3

    if-nez v3, :cond_5

    .line 3344
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v6, "IW"

    invoke-virtual {v3, v6}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 3345
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v6, "IW"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "merge time "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long/2addr v8, v4

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " msec for "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    iget-object v8, v8, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v8}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " docs"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 3348
    :cond_5
    return-void

    .line 3316
    :catch_0
    move-exception v2

    .line 3317
    .local v2, "t":Ljava/lang/Throwable;
    :try_start_3
    invoke-direct {p0, v2, p1}, Lorg/apache/lucene/index/IndexWriter;->handleMergeException(Ljava/lang/Throwable;Lorg/apache/lucene/index/MergePolicy$OneMerge;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 3319
    .end local v2    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v3

    .line 3320
    :try_start_4
    monitor-enter p0
    :try_end_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_1

    .line 3321
    :try_start_5
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/IndexWriter;->mergeFinish(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    .line 3323
    if-nez v1, :cond_7

    .line 3324
    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v7, "IW"

    invoke-virtual {v6, v7}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 3325
    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v7, "IW"

    const-string v8, "hit exception during merge"

    invoke-virtual {v6, v7, v8}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 3327
    :cond_6
    iget-object v6, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    if-eqz v6, :cond_7

    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    iget-object v7, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    invoke-virtual {v6, v7}, Lorg/apache/lucene/index/SegmentInfos;->contains(Lorg/apache/lucene/index/SegmentInfoPerCommit;)Z

    move-result v6

    if-nez v6, :cond_7

    .line 3328
    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    iget-object v7, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    iget-object v7, v7, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v7, v7, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lorg/apache/lucene/index/IndexFileDeleter;->refresh(Ljava/lang/String;)V

    .line 3335
    :cond_7
    if-eqz v1, :cond_9

    invoke-virtual {p1}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->isAborted()Z

    move-result v6

    if-nez v6, :cond_9

    iget v6, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->maxNumSegments:I

    if-ne v6, v9, :cond_8

    iget-boolean v6, p0, Lorg/apache/lucene/index/IndexWriter;->closed:Z

    if-nez v6, :cond_9

    iget-boolean v6, p0, Lorg/apache/lucene/index/IndexWriter;->closing:Z

    if-nez v6, :cond_9

    .line 3336
    :cond_8
    sget-object v6, Lorg/apache/lucene/index/MergePolicy$MergeTrigger;->MERGE_FINISHED:Lorg/apache/lucene/index/MergePolicy$MergeTrigger;

    iget v7, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->maxNumSegments:I

    invoke-direct {p0, v6, v7}, Lorg/apache/lucene/index/IndexWriter;->updatePendingMerges(Lorg/apache/lucene/index/MergePolicy$MergeTrigger;I)V

    .line 3320
    :cond_9
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 3339
    :try_start_6
    throw v3
    :try_end_6
    .catch Ljava/lang/OutOfMemoryError; {:try_start_6 .. :try_end_6} :catch_1

    .line 3340
    :catch_1
    move-exception v0

    .line 3341
    .local v0, "oom":Ljava/lang/OutOfMemoryError;
    const-string v3, "merge"

    invoke-direct {p0, v0, v3}, Lorg/apache/lucene/index/IndexWriter;->handleOOM(Ljava/lang/OutOfMemoryError;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 3320
    .end local v0    # "oom":Ljava/lang/OutOfMemoryError;
    :catchall_1
    move-exception v3

    :try_start_7
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    throw v3
    :try_end_8
    .catch Ljava/lang/OutOfMemoryError; {:try_start_8 .. :try_end_8} :catch_1

    :catchall_2
    move-exception v3

    :try_start_9
    monitor-exit p0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    :try_start_a
    throw v3
    :try_end_a
    .catch Ljava/lang/OutOfMemoryError; {:try_start_a .. :try_end_a} :catch_1
.end method

.method final declared-synchronized mergeFinish(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V
    .locals 4
    .param p1, "merge"    # Lorg/apache/lucene/index/MergePolicy$OneMerge;

    .prologue
    .line 3555
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 3559
    iget-boolean v2, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->registerDone:Z

    if-eqz v2, :cond_0

    .line 3560
    iget-object v1, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    .line 3561
    .local v1, "sourceSegments":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfoPerCommit;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 3564
    const/4 v2, 0x0

    iput-boolean v2, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->registerDone:Z

    .line 3567
    .end local v1    # "sourceSegments":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfoPerCommit;>;"
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->runningMerges:Ljava/util/Set;

    invoke-interface {v2, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3568
    monitor-exit p0

    return-void

    .line 3561
    .restart local v1    # "sourceSegments":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfoPerCommit;>;"
    :cond_1
    :try_start_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .line 3562
    .local v0, "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->mergingSegments:Ljava/util/HashSet;

    invoke-virtual {v3, v0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 3555
    .end local v0    # "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .end local v1    # "sourceSegments":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfoPerCommit;>;"
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method final declared-synchronized mergeInit(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V
    .locals 5
    .param p1, "merge"    # Lorg/apache/lucene/index/MergePolicy$OneMerge;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3449
    monitor-enter p0

    const/4 v0, 0x0

    .line 3451
    .local v0, "success":Z
    :try_start_0
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/IndexWriter;->_mergeInit(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3452
    const/4 v0, 0x1

    .line 3454
    if-nez v0, :cond_1

    .line 3455
    :try_start_1
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v2, "IW"

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3456
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v2, "IW"

    const-string v3, "hit exception in mergeInit"

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 3458
    :cond_0
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/IndexWriter;->mergeFinish(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 3461
    :cond_1
    monitor-exit p0

    return-void

    .line 3453
    :catchall_0
    move-exception v1

    .line 3454
    if-nez v0, :cond_3

    .line 3455
    :try_start_2
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v3, "IW"

    invoke-virtual {v2, v3}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 3456
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v3, "IW"

    const-string v4, "hit exception in mergeInit"

    invoke-virtual {v2, v3, v4}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 3458
    :cond_2
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/IndexWriter;->mergeFinish(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    .line 3460
    :cond_3
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 3449
    :catchall_1
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method mergeSuccess(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V
    .locals 0
    .param p1, "merge"    # Lorg/apache/lucene/index/MergePolicy$OneMerge;

    .prologue
    .line 3352
    return-void
.end method

.method final newSegmentName()Ljava/lang/String;
    .locals 6

    .prologue
    .line 1540
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    monitor-enter v1

    .line 1546
    :try_start_0
    iget-wide v2, p0, Lorg/apache/lucene/index/IndexWriter;->changeCount:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lorg/apache/lucene/index/IndexWriter;->changeCount:J

    .line 1547
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentInfos;->changed()V

    .line 1548
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "_"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    iget v3, v2, Lorg/apache/lucene/index/SegmentInfos;->counter:I

    add-int/lit8 v4, v3, 0x1

    iput v4, v2, Lorg/apache/lucene/index/SegmentInfos;->counter:I

    const/16 v2, 0x24

    invoke-static {v3, v2}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 1540
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method declared-synchronized newestSegment()Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .locals 2

    .prologue
    .line 3881
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentInfos;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentInfos;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/SegmentInfos;->info(I)Lorg/apache/lucene/index/SegmentInfoPerCommit;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized nrtIsCurrent(Lorg/apache/lucene/index/SegmentInfos;)Z
    .locals 10
    .param p1, "infos"    # Lorg/apache/lucene/index/SegmentInfos;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 4159
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 4160
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v3, "IW"

    invoke-virtual {v2, v3}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 4161
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v4, "IW"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v2, "nrtIsCurrent: infoVersion matches: "

    invoke-direct {v5, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v6, p1, Lorg/apache/lucene/index/SegmentInfos;->version:J

    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    iget-wide v8, v2, Lorg/apache/lucene/index/SegmentInfos;->version:J

    cmp-long v2, v6, v8

    if-nez v2, :cond_1

    move v2, v0

    :goto_0
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " DW changes: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v5}, Lorg/apache/lucene/index/DocumentsWriter;->anyChanges()Z

    move-result v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " BD changes: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->bufferedDeletesStream:Lorg/apache/lucene/index/BufferedDeletesStream;

    invoke-virtual {v5}, Lorg/apache/lucene/index/BufferedDeletesStream;->any()Z

    move-result v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 4164
    :cond_0
    iget-wide v2, p1, Lorg/apache/lucene/index/SegmentInfos;->version:J

    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    iget-wide v4, v4, Lorg/apache/lucene/index/SegmentInfos;->version:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v2}, Lorg/apache/lucene/index/DocumentsWriter;->anyChanges()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->bufferedDeletesStream:Lorg/apache/lucene/index/BufferedDeletesStream;

    invoke-virtual {v2}, Lorg/apache/lucene/index/BufferedDeletesStream;->any()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_2

    :goto_1
    monitor-exit p0

    return v0

    :cond_1
    move v2, v1

    .line 4161
    goto :goto_0

    :cond_2
    move v0, v1

    .line 4164
    goto :goto_1

    .line 4159
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public numDeletedDocs(Lorg/apache/lucene/index/SegmentInfoPerCommit;)I
    .locals 4
    .param p1, "info"    # Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .prologue
    const/4 v3, 0x0

    .line 568
    invoke-virtual {p0, v3}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen(Z)V

    .line 569
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->getDelCount()I

    move-result v0

    .line 571
    .local v0, "delCount":I
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->readerPool:Lorg/apache/lucene/index/IndexWriter$ReaderPool;

    invoke-virtual {v2, p1, v3}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->get(Lorg/apache/lucene/index/SegmentInfoPerCommit;Z)Lorg/apache/lucene/index/ReadersAndLiveDocs;

    move-result-object v1

    .line 572
    .local v1, "rld":Lorg/apache/lucene/index/ReadersAndLiveDocs;
    if-eqz v1, :cond_0

    .line 573
    invoke-virtual {v1}, Lorg/apache/lucene/index/ReadersAndLiveDocs;->getPendingDeleteCount()I

    move-result v2

    add-int/2addr v0, v2

    .line 575
    :cond_0
    return v0
.end method

.method public declared-synchronized numDocs()I
    .locals 5

    .prologue
    .line 1066
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 1068
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    if-eqz v2, :cond_0

    .line 1069
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v2}, Lorg/apache/lucene/index/DocumentsWriter;->getNumDocs()I

    move-result v0

    .line 1073
    .local v0, "count":I
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v2}, Lorg/apache/lucene/index/SegmentInfos;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_1

    .line 1076
    monitor-exit p0

    return v0

    .line 1071
    .end local v0    # "count":I
    :cond_0
    const/4 v0, 0x0

    .restart local v0    # "count":I
    goto :goto_0

    .line 1073
    :cond_1
    :try_start_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .line 1074
    .local v1, "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    iget-object v3, v1, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v3}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v3

    invoke-virtual {p0, v1}, Lorg/apache/lucene/index/IndexWriter;->numDeletedDocs(Lorg/apache/lucene/index/SegmentInfoPerCommit;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v4

    sub-int/2addr v3, v4

    add-int/2addr v0, v3

    goto :goto_1

    .line 1066
    .end local v0    # "count":I
    .end local v1    # "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public final declared-synchronized numRamDocs()I
    .locals 1

    .prologue
    .line 3014
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 3015
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocumentsWriter;->getNumDocs()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 3014
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final prepareCommit()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2660
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 2661
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexWriter;->prepareCommitInternal()V

    .line 2662
    return-void
.end method

.method declared-synchronized publishFlushedSegment(Lorg/apache/lucene/index/SegmentInfoPerCommit;Lorg/apache/lucene/index/FrozenBufferedDeletes;Lorg/apache/lucene/index/FrozenBufferedDeletes;)V
    .locals 7
    .param p1, "newSegment"    # Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .param p2, "packet"    # Lorg/apache/lucene/index/FrozenBufferedDeletes;
    .param p3, "globalPacket"    # Lorg/apache/lucene/index/FrozenBufferedDeletes;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2195
    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->bufferedDeletesStream:Lorg/apache/lucene/index/BufferedDeletesStream;

    monitor-enter v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2196
    :try_start_1
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v4, "IW"

    invoke-virtual {v2, v4}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2197
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v4, "IW"

    const-string v5, "publishFlushedSegment"

    invoke-virtual {v2, v4, v5}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 2200
    :cond_0
    if-eqz p3, :cond_1

    invoke-virtual {p3}, Lorg/apache/lucene/index/FrozenBufferedDeletes;->any()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2201
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->bufferedDeletesStream:Lorg/apache/lucene/index/BufferedDeletesStream;

    invoke-virtual {v2, p3}, Lorg/apache/lucene/index/BufferedDeletesStream;->push(Lorg/apache/lucene/index/FrozenBufferedDeletes;)J

    .line 2206
    :cond_1
    if-eqz p2, :cond_3

    invoke-virtual {p2}, Lorg/apache/lucene/index/FrozenBufferedDeletes;->any()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2207
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->bufferedDeletesStream:Lorg/apache/lucene/index/BufferedDeletesStream;

    invoke-virtual {v2, p2}, Lorg/apache/lucene/index/BufferedDeletesStream;->push(Lorg/apache/lucene/index/FrozenBufferedDeletes;)J

    move-result-wide v0

    .line 2213
    .local v0, "nextGen":J
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v4, "IW"

    invoke-virtual {v2, v4}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2214
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v4, "IW"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "publish sets newSegment delGen="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " seg="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/IndexWriter;->segString(Lorg/apache/lucene/index/SegmentInfoPerCommit;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 2216
    :cond_2
    invoke-virtual {p1, v0, v1}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->setBufferedDeletesGen(J)V

    .line 2217
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/index/SegmentInfos;->add(Lorg/apache/lucene/index/SegmentInfoPerCommit;)V

    .line 2218
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->checkpoint()V

    .line 2195
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2220
    monitor-exit p0

    return-void

    .line 2211
    .end local v0    # "nextGen":J
    :cond_3
    :try_start_2
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->bufferedDeletesStream:Lorg/apache/lucene/index/BufferedDeletesStream;

    invoke-virtual {v2}, Lorg/apache/lucene/index/BufferedDeletesStream;->getNextGen()J

    move-result-wide v0

    .restart local v0    # "nextGen":J
    goto :goto_0

    .line 2195
    .end local v0    # "nextGen":J
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method declared-synchronized publishFrozenDeletes(Lorg/apache/lucene/index/FrozenBufferedDeletes;)V
    .locals 2
    .param p1, "packet"    # Lorg/apache/lucene/index/FrozenBufferedDeletes;

    .prologue
    .line 2182
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lorg/apache/lucene/index/FrozenBufferedDeletes;->any()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2183
    :cond_1
    :try_start_1
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->bufferedDeletesStream:Lorg/apache/lucene/index/BufferedDeletesStream;

    monitor-enter v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2184
    :try_start_2
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->bufferedDeletesStream:Lorg/apache/lucene/index/BufferedDeletesStream;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/BufferedDeletesStream;->push(Lorg/apache/lucene/index/FrozenBufferedDeletes;)J

    .line 2183
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2186
    monitor-exit p0

    return-void

    .line 2183
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method

.method public final ramSizeInBytes()J
    .locals 4

    .prologue
    .line 3000
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 3001
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter;->flushControl:Lorg/apache/lucene/index/DocumentsWriterFlushControl;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->netBytes()J

    move-result-wide v0

    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->bufferedDeletesStream:Lorg/apache/lucene/index/BufferedDeletesStream;

    invoke-virtual {v2}, Lorg/apache/lucene/index/BufferedDeletesStream;->bytesUsed()J

    move-result-wide v2

    add-long/2addr v0, v2

    return-wide v0
.end method

.method final declared-synchronized registerMerge(Lorg/apache/lucene/index/MergePolicy$OneMerge;)Z
    .locals 14
    .param p1, "merge"    # Lorg/apache/lucene/index/MergePolicy$OneMerge;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v12, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 3362
    monitor-enter p0

    :try_start_0
    iget-boolean v8, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->registerDone:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v8, :cond_0

    .line 3443
    :goto_0
    monitor-exit p0

    return v6

    .line 3365
    :cond_0
    :try_start_1
    sget-boolean v8, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v8, :cond_1

    iget-object v8, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    if-gtz v8, :cond_1

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3362
    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6

    .line 3367
    :cond_1
    :try_start_2
    iget-boolean v8, p0, Lorg/apache/lucene/index/IndexWriter;->stopMerges:Z

    if-eqz v8, :cond_2

    .line 3368
    invoke-virtual {p1}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->abort()V

    .line 3369
    new-instance v6, Lorg/apache/lucene/index/MergePolicy$MergeAbortedException;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "merge is aborted: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    invoke-virtual {p0, v8}, Lorg/apache/lucene/index/IndexWriter;->segString(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Lorg/apache/lucene/index/MergePolicy$MergeAbortedException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 3372
    :cond_2
    const/4 v5, 0x0

    .line 3373
    .local v5, "isExternal":Z
    iget-object v8, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_3
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-nez v9, :cond_6

    .line 3394
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/IndexWriter;->ensureValidMerge(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    .line 3396
    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->pendingMerges:Ljava/util/LinkedList;

    invoke-virtual {v7, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 3398
    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v8, "IW"

    invoke-virtual {v7, v8}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 3399
    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v8, "IW"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "add merge to pendingMerges: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v10, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    invoke-virtual {p0, v10}, Lorg/apache/lucene/index/IndexWriter;->segString(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " [total "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lorg/apache/lucene/index/IndexWriter;->pendingMerges:Ljava/util/LinkedList;

    invoke-virtual {v10}, Ljava/util/LinkedList;->size()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " pending]"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 3402
    :cond_4
    iget-wide v8, p0, Lorg/apache/lucene/index/IndexWriter;->mergeGen:J

    iput-wide v8, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->mergeGen:J

    .line 3403
    iput-boolean v5, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->isExternal:Z

    .line 3409
    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v8, "IW"

    invoke-virtual {v7, v8}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 3410
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v7, "registerMerge merging= ["

    invoke-direct {v0, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 3411
    .local v0, "builder":Ljava/lang/StringBuilder;
    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->mergingSegments:Ljava/util/HashSet;

    invoke-virtual {v7}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_c

    .line 3414
    const-string v7, "]"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3417
    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v8, "IW"

    invoke-virtual {v7, v8}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 3418
    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v8, "IW"

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 3421
    .end local v0    # "builder":Ljava/lang/StringBuilder;
    :cond_5
    iget-object v7, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_d

    .line 3428
    sget-boolean v7, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v7, :cond_f

    iget-wide v8, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->estimatedMergeBytes:J

    cmp-long v7, v8, v12

    if-eqz v7, :cond_f

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 3373
    :cond_6
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .line 3374
    .local v4, "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    iget-object v9, p0, Lorg/apache/lucene/index/IndexWriter;->mergingSegments:Ljava/util/HashSet;

    invoke-virtual {v9, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_8

    .line 3375
    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v8, "IW"

    invoke-virtual {v6, v8}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 3376
    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v8, "IW"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "reject merge "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v10, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    invoke-virtual {p0, v10}, Lorg/apache/lucene/index/IndexWriter;->segString(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ": segment "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p0, v4}, Lorg/apache/lucene/index/IndexWriter;->segString(Lorg/apache/lucene/index/SegmentInfoPerCommit;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " is already marked for merge"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v8, v9}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    move v6, v7

    .line 3378
    goto/16 :goto_0

    .line 3380
    :cond_8
    iget-object v9, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v9, v4}, Lorg/apache/lucene/index/SegmentInfos;->contains(Lorg/apache/lucene/index/SegmentInfoPerCommit;)Z

    move-result v9

    if-nez v9, :cond_a

    .line 3381
    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v8, "IW"

    invoke-virtual {v6, v8}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 3382
    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v8, "IW"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "reject merge "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v10, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    invoke-virtual {p0, v10}, Lorg/apache/lucene/index/IndexWriter;->segString(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ": segment "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p0, v4}, Lorg/apache/lucene/index/IndexWriter;->segString(Lorg/apache/lucene/index/SegmentInfoPerCommit;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " does not exist in live infos"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v8, v9}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    move v6, v7

    .line 3384
    goto/16 :goto_0

    .line 3386
    :cond_a
    iget-object v9, v4, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v9, v9, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    iget-object v10, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    if-eq v9, v10, :cond_b

    .line 3387
    const/4 v5, 0x1

    .line 3389
    :cond_b
    iget-object v9, p0, Lorg/apache/lucene/index/IndexWriter;->segmentsToMerge:Ljava/util/Map;

    invoke-interface {v9, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 3390
    iget v9, p0, Lorg/apache/lucene/index/IndexWriter;->mergeMaxNumSegments:I

    iput v9, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->maxNumSegments:I

    goto/16 :goto_1

    .line 3411
    .end local v4    # "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .restart local v0    # "builder":Ljava/lang/StringBuilder;
    :cond_c
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .line 3412
    .restart local v4    # "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    iget-object v8, v4, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v8, v8, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    .line 3421
    .end local v0    # "builder":Ljava/lang/StringBuilder;
    .end local v4    # "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    :cond_d
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .line 3422
    .restart local v4    # "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    iget-object v8, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v9, "IW"

    invoke-virtual {v8, v9}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_e

    .line 3423
    iget-object v8, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v9, "IW"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "registerMerge info="

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v4}, Lorg/apache/lucene/index/IndexWriter;->segString(Lorg/apache/lucene/index/SegmentInfoPerCommit;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 3425
    :cond_e
    iget-object v8, p0, Lorg/apache/lucene/index/IndexWriter;->mergingSegments:Ljava/util/HashSet;

    invoke-virtual {v8, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 3429
    .end local v4    # "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    :cond_f
    sget-boolean v7, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v7, :cond_10

    iget-wide v8, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->totalMergeBytes:J

    cmp-long v7, v8, v12

    if-eqz v7, :cond_10

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 3430
    :cond_10
    iget-object v7, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_11
    :goto_4
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_12

    .line 3441
    const/4 v7, 0x1

    iput-boolean v7, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->registerDone:Z

    goto/16 :goto_0

    .line 3430
    :cond_12
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .line 3431
    .restart local v4    # "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    iget-object v8, v4, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v8}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v8

    if-lez v8, :cond_11

    .line 3432
    invoke-virtual {p0, v4}, Lorg/apache/lucene/index/IndexWriter;->numDeletedDocs(Lorg/apache/lucene/index/SegmentInfoPerCommit;)I

    move-result v1

    .line 3433
    .local v1, "delCount":I
    sget-boolean v8, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v8, :cond_13

    iget-object v8, v4, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v8}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v8

    if-le v1, v8, :cond_13

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 3434
    :cond_13
    int-to-double v8, v1

    iget-object v10, v4, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v10}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v10

    int-to-double v10, v10

    div-double v2, v8, v10

    .line 3435
    .local v2, "delRatio":D
    iget-wide v8, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->estimatedMergeBytes:J

    long-to-double v8, v8

    invoke-virtual {v4}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->sizeInBytes()J

    move-result-wide v10

    long-to-double v10, v10

    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v12, v2

    mul-double/2addr v10, v12

    add-double/2addr v8, v10

    double-to-long v8, v8

    iput-wide v8, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->estimatedMergeBytes:J

    .line 3436
    iget-wide v8, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->totalMergeBytes:J

    invoke-virtual {v4}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->sizeInBytes()J

    move-result-wide v10

    add-long/2addr v8, v10

    iput-wide v8, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->totalMergeBytes:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_4
.end method

.method public rollback()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1951
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 1955
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->commitLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1956
    :try_start_0
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexWriter;->shouldClose()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1957
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexWriter;->rollbackInternal()V

    .line 1955
    :cond_0
    monitor-exit v1

    .line 1960
    return-void

    .line 1955
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public declared-synchronized segString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3889
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexWriter;->segString(Ljava/lang/Iterable;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized segString(Ljava/lang/Iterable;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lorg/apache/lucene/index/SegmentInfoPerCommit;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 3897
    .local p1, "infos":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lorg/apache/lucene/index/SegmentInfoPerCommit;>;"
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 3898
    .local v0, "buffer":Ljava/lang/StringBuilder;
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 3904
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    monitor-exit p0

    return-object v2

    .line 3898
    :cond_0
    :try_start_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .line 3899
    .local v1, "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-lez v3, :cond_1

    .line 3900
    const/16 v3, 0x20

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 3902
    :cond_1
    invoke-virtual {p0, v1}, Lorg/apache/lucene/index/IndexWriter;->segString(Lorg/apache/lucene/index/SegmentInfoPerCommit;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 3897
    .end local v0    # "buffer":Ljava/lang/StringBuilder;
    .end local v1    # "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized segString(Lorg/apache/lucene/index/SegmentInfoPerCommit;)Ljava/lang/String;
    .locals 3
    .param p1, "info"    # Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .prologue
    .line 3912
    monitor-enter p0

    :try_start_0
    iget-object v0, p1, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/IndexWriter;->numDeletedDocs(Lorg/apache/lucene/index/SegmentInfoPerCommit;)I

    move-result v1

    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->getDelCount()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->toString(Lorg/apache/lucene/store/Directory;I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized setCommitData(Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2771
    .local p1, "commitUserData":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1, p1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/SegmentInfos;->setUserData(Ljava/util/Map;)V

    .line 2772
    iget-wide v0, p0, Lorg/apache/lucene/index/IndexWriter;->changeCount:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/lucene/index/IndexWriter;->changeCount:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2773
    monitor-exit p0

    return-void

    .line 2771
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method testPoint(Ljava/lang/String;)Z
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 4154
    const/4 v0, 0x1

    return v0
.end method

.method declared-synchronized toLiveInfos(Lorg/apache/lucene/index/SegmentInfos;)Lorg/apache/lucene/index/SegmentInfos;
    .locals 6
    .param p1, "sis"    # Lorg/apache/lucene/index/SegmentInfos;

    .prologue
    .line 3960
    monitor-enter p0

    :try_start_0
    new-instance v3, Lorg/apache/lucene/index/SegmentInfos;

    invoke-direct {v3}, Lorg/apache/lucene/index/SegmentInfos;-><init>()V

    .line 3961
    .local v3, "newSIS":Lorg/apache/lucene/index/SegmentInfos;
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 3962
    .local v2, "liveSIS":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/index/SegmentInfoPerCommit;Lorg/apache/lucene/index/SegmentInfoPerCommit;>;"
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v4}, Lorg/apache/lucene/index/SegmentInfos;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_0

    .line 3965
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfos;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v5

    if-nez v5, :cond_1

    .line 3973
    monitor-exit p0

    return-object v3

    .line 3962
    :cond_0
    :try_start_1
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .line 3963
    .local v0, "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    invoke-interface {v2, v0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 3960
    .end local v0    # "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .end local v2    # "liveSIS":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/index/SegmentInfoPerCommit;Lorg/apache/lucene/index/SegmentInfoPerCommit;>;"
    .end local v3    # "newSIS":Lorg/apache/lucene/index/SegmentInfos;
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    .line 3965
    .restart local v2    # "liveSIS":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/index/SegmentInfoPerCommit;Lorg/apache/lucene/index/SegmentInfoPerCommit;>;"
    .restart local v3    # "newSIS":Lorg/apache/lucene/index/SegmentInfos;
    :cond_1
    :try_start_2
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .line 3966
    .restart local v0    # "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .line 3967
    .local v1, "liveInfo":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    if-eqz v1, :cond_2

    .line 3968
    move-object v0, v1

    .line 3970
    :cond_2
    invoke-virtual {v3, v0}, Lorg/apache/lucene/index/SegmentInfos;->add(Lorg/apache/lucene/index/SegmentInfoPerCommit;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public declared-synchronized tryDeleteDocument(Lorg/apache/lucene/index/IndexReader;I)Z
    .locals 14
    .param p1, "readerIn"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "docID"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 1314
    monitor-enter p0

    :try_start_0
    instance-of v8, p1, Lorg/apache/lucene/index/AtomicReader;

    if-eqz v8, :cond_1

    .line 1316
    move-object v0, p1

    check-cast v0, Lorg/apache/lucene/index/AtomicReader;

    move-object v5, v0

    .line 1327
    .local v5, "reader":Lorg/apache/lucene/index/AtomicReader;
    :cond_0
    instance-of v8, v5, Lorg/apache/lucene/index/SegmentReader;

    if-nez v8, :cond_3

    .line 1328
    new-instance v8, Ljava/lang/IllegalArgumentException;

    const-string v9, "the reader must be a SegmentReader or composite reader containing only SegmentReaders"

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1314
    .end local v5    # "reader":Lorg/apache/lucene/index/AtomicReader;
    :catchall_0
    move-exception v8

    monitor-exit p0

    throw v8

    .line 1319
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReader;->leaves()Ljava/util/List;

    move-result-object v4

    .line 1320
    .local v4, "leaves":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/AtomicReaderContext;>;"
    move/from16 v0, p2

    invoke-static {v0, v4}, Lorg/apache/lucene/index/ReaderUtil;->subIndex(ILjava/util/List;)I

    move-result v7

    .line 1321
    .local v7, "subIndex":I
    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/apache/lucene/index/AtomicReaderContext;

    invoke-virtual {v8}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v5

    .line 1322
    .restart local v5    # "reader":Lorg/apache/lucene/index/AtomicReader;
    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/apache/lucene/index/AtomicReaderContext;

    iget v8, v8, Lorg/apache/lucene/index/AtomicReaderContext;->docBase:I

    sub-int p2, p2, v8

    .line 1323
    sget-boolean v8, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v8, :cond_2

    if-gez p2, :cond_2

    new-instance v8, Ljava/lang/AssertionError;

    invoke-direct {v8}, Ljava/lang/AssertionError;-><init>()V

    throw v8

    .line 1324
    :cond_2
    sget-boolean v8, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v8, :cond_0

    invoke-virtual {v5}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v8

    move/from16 v0, p2

    if-lt v0, v8, :cond_0

    new-instance v8, Ljava/lang/AssertionError;

    invoke-direct {v8}, Ljava/lang/AssertionError;-><init>()V

    throw v8

    .line 1331
    .end local v4    # "leaves":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/AtomicReaderContext;>;"
    .end local v7    # "subIndex":I
    :cond_3
    check-cast v5, Lorg/apache/lucene/index/SegmentReader;

    .end local v5    # "reader":Lorg/apache/lucene/index/AtomicReader;
    invoke-virtual {v5}, Lorg/apache/lucene/index/SegmentReader;->getSegmentInfo()Lorg/apache/lucene/index/SegmentInfoPerCommit;

    move-result-object v3

    .line 1338
    .local v3, "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    iget-object v8, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v8, v3}, Lorg/apache/lucene/index/SegmentInfos;->indexOf(Lorg/apache/lucene/index/SegmentInfoPerCommit;)I

    move-result v8

    const/4 v10, -0x1

    if-eq v8, v10, :cond_6

    .line 1339
    iget-object v8, p0, Lorg/apache/lucene/index/IndexWriter;->readerPool:Lorg/apache/lucene/index/IndexWriter$ReaderPool;

    const/4 v10, 0x0

    invoke-virtual {v8, v3, v10}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->get(Lorg/apache/lucene/index/SegmentInfoPerCommit;Z)Lorg/apache/lucene/index/ReadersAndLiveDocs;

    move-result-object v6

    .line 1340
    .local v6, "rld":Lorg/apache/lucene/index/ReadersAndLiveDocs;
    if-eqz v6, :cond_6

    .line 1341
    iget-object v9, p0, Lorg/apache/lucene/index/IndexWriter;->bufferedDeletesStream:Lorg/apache/lucene/index/BufferedDeletesStream;

    monitor-enter v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1342
    :try_start_2
    invoke-virtual {v6}, Lorg/apache/lucene/index/ReadersAndLiveDocs;->initWritableLiveDocs()V

    .line 1343
    move/from16 v0, p2

    invoke-virtual {v6, v0}, Lorg/apache/lucene/index/ReadersAndLiveDocs;->delete(I)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 1344
    iget-object v8, v6, Lorg/apache/lucene/index/ReadersAndLiveDocs;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    invoke-virtual {v8}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->getDelCount()I

    move-result v8

    invoke-virtual {v6}, Lorg/apache/lucene/index/ReadersAndLiveDocs;->getPendingDeleteCount()I

    move-result v10

    add-int v2, v8, v10

    .line 1345
    .local v2, "fullDelCount":I
    iget-object v8, v6, Lorg/apache/lucene/index/ReadersAndLiveDocs;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    iget-object v8, v8, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v8}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v8

    if-ne v2, v8, :cond_4

    .line 1350
    iget-object v8, p0, Lorg/apache/lucene/index/IndexWriter;->mergingSegments:Ljava/util/HashSet;

    iget-object v10, v6, Lorg/apache/lucene/index/ReadersAndLiveDocs;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    invoke-virtual {v8, v10}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_4

    .line 1351
    iget-object v8, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    iget-object v10, v6, Lorg/apache/lucene/index/ReadersAndLiveDocs;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    invoke-virtual {v8, v10}, Lorg/apache/lucene/index/SegmentInfos;->remove(Lorg/apache/lucene/index/SegmentInfoPerCommit;)V

    .line 1352
    iget-object v8, p0, Lorg/apache/lucene/index/IndexWriter;->readerPool:Lorg/apache/lucene/index/IndexWriter$ReaderPool;

    iget-object v10, v6, Lorg/apache/lucene/index/ReadersAndLiveDocs;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    invoke-virtual {v8, v10}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->drop(Lorg/apache/lucene/index/SegmentInfoPerCommit;)V

    .line 1353
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->checkpoint()V

    .line 1359
    :cond_4
    iget-wide v10, p0, Lorg/apache/lucene/index/IndexWriter;->changeCount:J

    const-wide/16 v12, 0x1

    add-long/2addr v10, v12

    iput-wide v10, p0, Lorg/apache/lucene/index/IndexWriter;->changeCount:J

    .line 1362
    .end local v2    # "fullDelCount":I
    :cond_5
    monitor-exit v9
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    const/4 v8, 0x1

    .line 1370
    .end local v6    # "rld":Lorg/apache/lucene/index/ReadersAndLiveDocs;
    :goto_0
    monitor-exit p0

    return v8

    .line 1341
    .restart local v6    # "rld":Lorg/apache/lucene/index/ReadersAndLiveDocs;
    :catchall_1
    move-exception v8

    :try_start_3
    monitor-exit v9
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    throw v8
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .end local v6    # "rld":Lorg/apache/lucene/index/ReadersAndLiveDocs;
    :cond_6
    move v8, v9

    .line 1370
    goto :goto_0
.end method

.method public updateDocument(Lorg/apache/lucene/index/Term;Ljava/lang/Iterable;)V
    .locals 1
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/Term;",
            "Ljava/lang/Iterable",
            "<+",
            "Lorg/apache/lucene/index/IndexableField;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1456
    .local p2, "doc":Ljava/lang/Iterable;, "Ljava/lang/Iterable<+Lorg/apache/lucene/index/IndexableField;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 1457
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    invoke-virtual {p0, p1, p2, v0}, Lorg/apache/lucene/index/IndexWriter;->updateDocument(Lorg/apache/lucene/index/Term;Ljava/lang/Iterable;Lorg/apache/lucene/analysis/Analyzer;)V

    .line 1458
    return-void
.end method

.method public updateDocument(Lorg/apache/lucene/index/Term;Ljava/lang/Iterable;Lorg/apache/lucene/analysis/Analyzer;)V
    .locals 7
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .param p3, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/Term;",
            "Ljava/lang/Iterable",
            "<+",
            "Lorg/apache/lucene/index/IndexableField;",
            ">;",
            "Lorg/apache/lucene/analysis/Analyzer;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1480
    .local p2, "doc":Ljava/lang/Iterable;, "Ljava/lang/Iterable<+Lorg/apache/lucene/index/IndexableField;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 1482
    const/4 v2, 0x0

    .line 1483
    .local v2, "success":Z
    const/4 v0, 0x0

    .line 1485
    .local v0, "anySegmentFlushed":Z
    :try_start_0
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v3, p2, p3, p1}, Lorg/apache/lucene/index/DocumentsWriter;->updateDocument(Ljava/lang/Iterable;Lorg/apache/lucene/analysis/Analyzer;Lorg/apache/lucene/index/Term;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 1486
    const/4 v2, 0x1

    .line 1488
    if-nez v2, :cond_0

    .line 1489
    :try_start_1
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v4, "IW"

    invoke-virtual {v3, v4}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1490
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v4, "IW"

    const-string v5, "hit exception updating document"

    invoke-virtual {v3, v4, v5}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 1495
    :cond_0
    if-eqz v0, :cond_1

    .line 1496
    sget-object v3, Lorg/apache/lucene/index/MergePolicy$MergeTrigger;->SEGMENT_FLUSH:Lorg/apache/lucene/index/MergePolicy$MergeTrigger;

    const/4 v4, -0x1

    invoke-direct {p0, v3, v4}, Lorg/apache/lucene/index/IndexWriter;->maybeMerge(Lorg/apache/lucene/index/MergePolicy$MergeTrigger;I)V

    .line 1501
    :cond_1
    :goto_0
    return-void

    .line 1487
    :catchall_0
    move-exception v3

    .line 1488
    if-nez v2, :cond_2

    .line 1489
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v5, "IW"

    invoke-virtual {v4, v5}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1490
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v5, "IW"

    const-string v6, "hit exception updating document"

    invoke-virtual {v4, v5, v6}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 1493
    :cond_2
    throw v3
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0

    .line 1498
    :catch_0
    move-exception v1

    .line 1499
    .local v1, "oom":Ljava/lang/OutOfMemoryError;
    const-string v3, "updateDocument"

    invoke-direct {p0, v1, v3}, Lorg/apache/lucene/index/IndexWriter;->handleOOM(Ljava/lang/OutOfMemoryError;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public updateDocuments(Lorg/apache/lucene/index/Term;Ljava/lang/Iterable;)V
    .locals 1
    .param p1, "delTerm"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/Term;",
            "Ljava/lang/Iterable",
            "<+",
            "Ljava/lang/Iterable",
            "<+",
            "Lorg/apache/lucene/index/IndexableField;",
            ">;>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1237
    .local p2, "docs":Ljava/lang/Iterable;, "Ljava/lang/Iterable<+Ljava/lang/Iterable<+Lorg/apache/lucene/index/IndexableField;>;>;"
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    invoke-virtual {p0, p1, p2, v0}, Lorg/apache/lucene/index/IndexWriter;->updateDocuments(Lorg/apache/lucene/index/Term;Ljava/lang/Iterable;Lorg/apache/lucene/analysis/Analyzer;)V

    .line 1238
    return-void
.end method

.method public updateDocuments(Lorg/apache/lucene/index/Term;Ljava/lang/Iterable;Lorg/apache/lucene/analysis/Analyzer;)V
    .locals 7
    .param p1, "delTerm"    # Lorg/apache/lucene/index/Term;
    .param p3, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/Term;",
            "Ljava/lang/Iterable",
            "<+",
            "Ljava/lang/Iterable",
            "<+",
            "Lorg/apache/lucene/index/IndexableField;",
            ">;>;",
            "Lorg/apache/lucene/analysis/Analyzer;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1255
    .local p2, "docs":Ljava/lang/Iterable;, "Ljava/lang/Iterable<+Ljava/lang/Iterable<+Lorg/apache/lucene/index/IndexableField;>;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 1257
    const/4 v2, 0x0

    .line 1258
    .local v2, "success":Z
    const/4 v0, 0x0

    .line 1260
    .local v0, "anySegmentFlushed":Z
    :try_start_0
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v3, p2, p3, p1}, Lorg/apache/lucene/index/DocumentsWriter;->updateDocuments(Ljava/lang/Iterable;Lorg/apache/lucene/analysis/Analyzer;Lorg/apache/lucene/index/Term;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 1261
    const/4 v2, 0x1

    .line 1263
    if-nez v2, :cond_0

    .line 1264
    :try_start_1
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v4, "IW"

    invoke-virtual {v3, v4}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1265
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v4, "IW"

    const-string v5, "hit exception updating document"

    invoke-virtual {v3, v4, v5}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 1269
    :cond_0
    if-eqz v0, :cond_1

    .line 1270
    sget-object v3, Lorg/apache/lucene/index/MergePolicy$MergeTrigger;->SEGMENT_FLUSH:Lorg/apache/lucene/index/MergePolicy$MergeTrigger;

    const/4 v4, -0x1

    invoke-direct {p0, v3, v4}, Lorg/apache/lucene/index/IndexWriter;->maybeMerge(Lorg/apache/lucene/index/MergePolicy$MergeTrigger;I)V

    .line 1275
    :cond_1
    :goto_0
    return-void

    .line 1262
    :catchall_0
    move-exception v3

    .line 1263
    if-nez v2, :cond_2

    .line 1264
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v5, "IW"

    invoke-virtual {v4, v5}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1265
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v5, "IW"

    const-string v6, "hit exception updating document"

    invoke-virtual {v4, v5, v6}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 1268
    :cond_2
    throw v3
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0

    .line 1272
    :catch_0
    move-exception v1

    .line 1273
    .local v1, "oom":Ljava/lang/OutOfMemoryError;
    const-string v3, "updateDocuments"

    invoke-direct {p0, v1, v3}, Lorg/apache/lucene/index/IndexWriter;->handleOOM(Ljava/lang/OutOfMemoryError;Ljava/lang/String;)V

    goto :goto_0
.end method

.method declared-synchronized useCompoundFile(Lorg/apache/lucene/index/SegmentInfoPerCommit;)Z
    .locals 2
    .param p1, "segmentInfo"    # Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2223
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->mergePolicy:Lorg/apache/lucene/index/MergePolicy;

    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v0, v1, p1}, Lorg/apache/lucene/index/MergePolicy;->useCompoundFile(Lorg/apache/lucene/index/SegmentInfos;Lorg/apache/lucene/index/SegmentInfoPerCommit;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized waitForMerges()V
    .locals 3

    .prologue
    .line 2154
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen(Z)V

    .line 2155
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v1, "IW"

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2156
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v1, "IW"

    const-string/jumbo v2, "waitForMerges"

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 2158
    :cond_0
    :goto_0
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->pendingMerges:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-gtz v0, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->runningMerges:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-gtz v0, :cond_1

    .line 2163
    sget-boolean v0, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->mergingSegments:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2154
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2159
    :cond_1
    :try_start_1
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexWriter;->doWait()V

    goto :goto_0

    .line 2165
    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v1, "IW"

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2166
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v1, "IW"

    const-string/jumbo v2, "waitForMerges done"

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2168
    :cond_3
    monitor-exit p0

    return-void
.end method
