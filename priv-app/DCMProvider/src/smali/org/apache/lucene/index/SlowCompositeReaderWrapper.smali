.class public final Lorg/apache/lucene/index/SlowCompositeReaderWrapper;
.super Lorg/apache/lucene/index/AtomicReader;
.source "SlowCompositeReaderWrapper.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final cachedOrdMaps:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;",
            ">;"
        }
    .end annotation
.end field

.field private final fields:Lorg/apache/lucene/index/Fields;

.field private final in:Lorg/apache/lucene/index/CompositeReader;

.field private final liveDocs:Lorg/apache/lucene/util/Bits;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const-class v0, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/index/CompositeReader;)V
    .locals 1
    .param p1, "reader"    # Lorg/apache/lucene/index/CompositeReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 70
    invoke-direct {p0}, Lorg/apache/lucene/index/AtomicReader;-><init>()V

    .line 179
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;->cachedOrdMaps:Ljava/util/Map;

    .line 71
    iput-object p1, p0, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;->in:Lorg/apache/lucene/index/CompositeReader;

    .line 72
    iget-object v0, p0, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;->in:Lorg/apache/lucene/index/CompositeReader;

    invoke-static {v0}, Lorg/apache/lucene/index/MultiFields;->getFields(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/index/Fields;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;->fields:Lorg/apache/lucene/index/Fields;

    .line 73
    iget-object v0, p0, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;->in:Lorg/apache/lucene/index/CompositeReader;

    invoke-static {v0}, Lorg/apache/lucene/index/MultiFields;->getLiveDocs(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/util/Bits;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;->liveDocs:Lorg/apache/lucene/util/Bits;

    .line 74
    iget-object v0, p0, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;->in:Lorg/apache/lucene/index/CompositeReader;

    invoke-virtual {v0, p0}, Lorg/apache/lucene/index/CompositeReader;->registerParentReader(Lorg/apache/lucene/index/IndexReader;)V

    .line 75
    return-void
.end method

.method public static wrap(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/index/AtomicReader;
    .locals 1
    .param p0, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 59
    instance-of v0, p0, Lorg/apache/lucene/index/CompositeReader;

    if-eqz v0, :cond_0

    .line 60
    new-instance v0, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;

    check-cast p0, Lorg/apache/lucene/index/CompositeReader;

    .end local p0    # "reader":Lorg/apache/lucene/index/IndexReader;
    invoke-direct {v0, p0}, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;-><init>(Lorg/apache/lucene/index/CompositeReader;)V

    move-object p0, v0

    .line 63
    :goto_0
    return-object p0

    .line 62
    .restart local p0    # "reader":Lorg/apache/lucene/index/IndexReader;
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    instance-of v0, p0, Lorg/apache/lucene/index/AtomicReader;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 63
    :cond_1
    check-cast p0, Lorg/apache/lucene/index/AtomicReader;

    goto :goto_0
.end method


# virtual methods
.method protected doClose()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 236
    iget-object v0, p0, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;->in:Lorg/apache/lucene/index/CompositeReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/CompositeReader;->close()V

    .line 237
    return-void
.end method

.method public document(ILorg/apache/lucene/index/StoredFieldVisitor;)V
    .locals 1
    .param p1, "docID"    # I
    .param p2, "visitor"    # Lorg/apache/lucene/index/StoredFieldVisitor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 207
    invoke-virtual {p0}, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;->ensureOpen()V

    .line 208
    iget-object v0, p0, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;->in:Lorg/apache/lucene/index/CompositeReader;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/index/CompositeReader;->document(ILorg/apache/lucene/index/StoredFieldVisitor;)V

    .line 209
    return-void
.end method

.method public fields()Lorg/apache/lucene/index/Fields;
    .locals 1

    .prologue
    .line 84
    invoke-virtual {p0}, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;->ensureOpen()V

    .line 85
    iget-object v0, p0, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;->fields:Lorg/apache/lucene/index/Fields;

    return-object v0
.end method

.method public getBinaryDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/BinaryDocValues;
    .locals 1
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 96
    invoke-virtual {p0}, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;->ensureOpen()V

    .line 97
    iget-object v0, p0, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;->in:Lorg/apache/lucene/index/CompositeReader;

    invoke-static {v0, p1}, Lorg/apache/lucene/index/MultiDocValues;->getBinaryValues(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;)Lorg/apache/lucene/index/BinaryDocValues;

    move-result-object v0

    return-object v0
.end method

.method public getCombinedCoreAndDeletesKey()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;->in:Lorg/apache/lucene/index/CompositeReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/CompositeReader;->getCombinedCoreAndDeletesKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getCoreCacheKey()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;->in:Lorg/apache/lucene/index/CompositeReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/CompositeReader;->getCoreCacheKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getFieldInfos()Lorg/apache/lucene/index/FieldInfos;
    .locals 1

    .prologue
    .line 219
    invoke-virtual {p0}, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;->ensureOpen()V

    .line 220
    iget-object v0, p0, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;->in:Lorg/apache/lucene/index/CompositeReader;

    invoke-static {v0}, Lorg/apache/lucene/index/MultiFields;->getMergedFieldInfos(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/index/FieldInfos;

    move-result-object v0

    return-object v0
.end method

.method public getLiveDocs()Lorg/apache/lucene/util/Bits;
    .locals 1

    .prologue
    .line 213
    invoke-virtual {p0}, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;->ensureOpen()V

    .line 214
    iget-object v0, p0, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;->liveDocs:Lorg/apache/lucene/util/Bits;

    return-object v0
.end method

.method public getNormValues(Ljava/lang/String;)Lorg/apache/lucene/index/NumericDocValues;
    .locals 1
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 183
    invoke-virtual {p0}, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;->ensureOpen()V

    .line 184
    iget-object v0, p0, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;->in:Lorg/apache/lucene/index/CompositeReader;

    invoke-static {v0, p1}, Lorg/apache/lucene/index/MultiDocValues;->getNormValues(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;)Lorg/apache/lucene/index/NumericDocValues;

    move-result-object v0

    return-object v0
.end method

.method public getNumericDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/NumericDocValues;
    .locals 1
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 90
    invoke-virtual {p0}, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;->ensureOpen()V

    .line 91
    iget-object v0, p0, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;->in:Lorg/apache/lucene/index/CompositeReader;

    invoke-static {v0, p1}, Lorg/apache/lucene/index/MultiDocValues;->getNumericValues(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;)Lorg/apache/lucene/index/NumericDocValues;

    move-result-object v0

    return-object v0
.end method

.method public getSortedDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/SortedDocValues;
    .locals 12
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 102
    invoke-virtual {p0}, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;->ensureOpen()V

    .line 103
    const/4 v4, 0x0

    .line 104
    .local v4, "map":Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;
    iget-object v10, p0, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;->cachedOrdMaps:Ljava/util/Map;

    monitor-enter v10

    .line 105
    :try_start_0
    iget-object v9, p0, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;->cachedOrdMaps:Ljava/util/Map;

    invoke-interface {v9, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    move-object v0, v9

    check-cast v0, Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;

    move-object v4, v0

    .line 106
    if-nez v4, :cond_1

    .line 108
    iget-object v9, p0, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;->in:Lorg/apache/lucene/index/CompositeReader;

    invoke-static {v9, p1}, Lorg/apache/lucene/index/MultiDocValues;->getSortedValues(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;)Lorg/apache/lucene/index/SortedDocValues;

    move-result-object v2

    .line 109
    .local v2, "dv":Lorg/apache/lucene/index/SortedDocValues;
    instance-of v9, v2, Lorg/apache/lucene/index/MultiDocValues$MultiSortedDocValues;

    if-eqz v9, :cond_0

    .line 110
    move-object v0, v2

    check-cast v0, Lorg/apache/lucene/index/MultiDocValues$MultiSortedDocValues;

    move-object v9, v0

    iget-object v4, v9, Lorg/apache/lucene/index/MultiDocValues$MultiSortedDocValues;->mapping:Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;

    .line 111
    iget-object v9, v4, Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;->owner:Ljava/lang/Object;

    invoke-virtual {p0}, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;->getCoreCacheKey()Ljava/lang/Object;

    move-result-object v11

    if-ne v9, v11, :cond_0

    .line 112
    iget-object v9, p0, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;->cachedOrdMaps:Ljava/util/Map;

    invoke-interface {v9, p1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    :cond_0
    monitor-exit v10

    .line 135
    .end local v2    # "dv":Lorg/apache/lucene/index/SortedDocValues;
    :goto_0
    return-object v2

    .line 104
    :cond_1
    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 119
    invoke-virtual {p0}, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;->getFieldInfos()Lorg/apache/lucene/index/FieldInfos;

    move-result-object v9

    invoke-virtual {v9, p1}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(Ljava/lang/String;)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v9

    invoke-virtual {v9}, Lorg/apache/lucene/index/FieldInfo;->getDocValuesType()Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-result-object v9

    sget-object v10, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->SORTED:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    if-eq v9, v10, :cond_2

    .line 120
    const/4 v2, 0x0

    goto :goto_0

    .line 104
    :catchall_0
    move-exception v9

    :try_start_1
    monitor-exit v10
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v9

    .line 122
    :cond_2
    iget-object v9, p0, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;->in:Lorg/apache/lucene/index/CompositeReader;

    invoke-virtual {v9}, Lorg/apache/lucene/index/CompositeReader;->leaves()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v5

    .line 123
    .local v5, "size":I
    new-array v8, v5, [Lorg/apache/lucene/index/SortedDocValues;

    .line 124
    .local v8, "values":[Lorg/apache/lucene/index/SortedDocValues;
    add-int/lit8 v9, v5, 0x1

    new-array v6, v9, [I

    .line 125
    .local v6, "starts":[I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-lt v3, v5, :cond_3

    .line 134
    invoke-virtual {p0}, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;->maxDoc()I

    move-result v9

    aput v9, v6, v5

    .line 135
    new-instance v2, Lorg/apache/lucene/index/MultiDocValues$MultiSortedDocValues;

    invoke-direct {v2, v8, v6, v4}, Lorg/apache/lucene/index/MultiDocValues$MultiSortedDocValues;-><init>([Lorg/apache/lucene/index/SortedDocValues;[ILorg/apache/lucene/index/MultiDocValues$OrdinalMap;)V

    goto :goto_0

    .line 126
    :cond_3
    iget-object v9, p0, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;->in:Lorg/apache/lucene/index/CompositeReader;

    invoke-virtual {v9}, Lorg/apache/lucene/index/CompositeReader;->leaves()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/AtomicReaderContext;

    .line 127
    .local v1, "context":Lorg/apache/lucene/index/AtomicReaderContext;
    invoke-virtual {v1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v9

    invoke-virtual {v9, p1}, Lorg/apache/lucene/index/AtomicReader;->getSortedDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/SortedDocValues;

    move-result-object v7

    .line 128
    .local v7, "v":Lorg/apache/lucene/index/SortedDocValues;
    if-nez v7, :cond_4

    .line 129
    sget-object v7, Lorg/apache/lucene/index/SortedDocValues;->EMPTY:Lorg/apache/lucene/index/SortedDocValues;

    .line 131
    :cond_4
    aput-object v7, v8, v3

    .line 132
    iget v9, v1, Lorg/apache/lucene/index/AtomicReaderContext;->docBase:I

    aput v9, v6, v3

    .line 125
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method public getSortedSetDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/SortedSetDocValues;
    .locals 12
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 140
    invoke-virtual {p0}, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;->ensureOpen()V

    .line 141
    const/4 v4, 0x0

    .line 142
    .local v4, "map":Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;
    iget-object v10, p0, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;->cachedOrdMaps:Ljava/util/Map;

    monitor-enter v10

    .line 143
    :try_start_0
    iget-object v9, p0, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;->cachedOrdMaps:Ljava/util/Map;

    invoke-interface {v9, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    move-object v0, v9

    check-cast v0, Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;

    move-object v4, v0

    .line 144
    if-nez v4, :cond_1

    .line 146
    iget-object v9, p0, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;->in:Lorg/apache/lucene/index/CompositeReader;

    invoke-static {v9, p1}, Lorg/apache/lucene/index/MultiDocValues;->getSortedSetValues(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;)Lorg/apache/lucene/index/SortedSetDocValues;

    move-result-object v2

    .line 147
    .local v2, "dv":Lorg/apache/lucene/index/SortedSetDocValues;
    instance-of v9, v2, Lorg/apache/lucene/index/MultiDocValues$MultiSortedSetDocValues;

    if-eqz v9, :cond_0

    .line 148
    move-object v0, v2

    check-cast v0, Lorg/apache/lucene/index/MultiDocValues$MultiSortedSetDocValues;

    move-object v9, v0

    iget-object v4, v9, Lorg/apache/lucene/index/MultiDocValues$MultiSortedSetDocValues;->mapping:Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;

    .line 149
    iget-object v9, v4, Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;->owner:Ljava/lang/Object;

    invoke-virtual {p0}, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;->getCoreCacheKey()Ljava/lang/Object;

    move-result-object v11

    if-ne v9, v11, :cond_0

    .line 150
    iget-object v9, p0, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;->cachedOrdMaps:Ljava/util/Map;

    invoke-interface {v9, p1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 153
    :cond_0
    monitor-exit v10

    .line 174
    .end local v2    # "dv":Lorg/apache/lucene/index/SortedSetDocValues;
    :goto_0
    return-object v2

    .line 142
    :cond_1
    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 157
    invoke-virtual {p0}, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;->getFieldInfos()Lorg/apache/lucene/index/FieldInfos;

    move-result-object v9

    invoke-virtual {v9, p1}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(Ljava/lang/String;)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v9

    invoke-virtual {v9}, Lorg/apache/lucene/index/FieldInfo;->getDocValuesType()Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-result-object v9

    sget-object v10, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->SORTED_SET:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    if-eq v9, v10, :cond_2

    .line 158
    const/4 v2, 0x0

    goto :goto_0

    .line 142
    :catchall_0
    move-exception v9

    :try_start_1
    monitor-exit v10
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v9

    .line 160
    :cond_2
    sget-boolean v9, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;->$assertionsDisabled:Z

    if-nez v9, :cond_3

    if-nez v4, :cond_3

    new-instance v9, Ljava/lang/AssertionError;

    invoke-direct {v9}, Ljava/lang/AssertionError;-><init>()V

    throw v9

    .line 161
    :cond_3
    iget-object v9, p0, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;->in:Lorg/apache/lucene/index/CompositeReader;

    invoke-virtual {v9}, Lorg/apache/lucene/index/CompositeReader;->leaves()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v5

    .line 162
    .local v5, "size":I
    new-array v8, v5, [Lorg/apache/lucene/index/SortedSetDocValues;

    .line 163
    .local v8, "values":[Lorg/apache/lucene/index/SortedSetDocValues;
    add-int/lit8 v9, v5, 0x1

    new-array v6, v9, [I

    .line 164
    .local v6, "starts":[I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-lt v3, v5, :cond_4

    .line 173
    invoke-virtual {p0}, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;->maxDoc()I

    move-result v9

    aput v9, v6, v5

    .line 174
    new-instance v2, Lorg/apache/lucene/index/MultiDocValues$MultiSortedSetDocValues;

    invoke-direct {v2, v8, v6, v4}, Lorg/apache/lucene/index/MultiDocValues$MultiSortedSetDocValues;-><init>([Lorg/apache/lucene/index/SortedSetDocValues;[ILorg/apache/lucene/index/MultiDocValues$OrdinalMap;)V

    goto :goto_0

    .line 165
    :cond_4
    iget-object v9, p0, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;->in:Lorg/apache/lucene/index/CompositeReader;

    invoke-virtual {v9}, Lorg/apache/lucene/index/CompositeReader;->leaves()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/AtomicReaderContext;

    .line 166
    .local v1, "context":Lorg/apache/lucene/index/AtomicReaderContext;
    invoke-virtual {v1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v9

    invoke-virtual {v9, p1}, Lorg/apache/lucene/index/AtomicReader;->getSortedSetDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/SortedSetDocValues;

    move-result-object v7

    .line 167
    .local v7, "v":Lorg/apache/lucene/index/SortedSetDocValues;
    if-nez v7, :cond_5

    .line 168
    sget-object v7, Lorg/apache/lucene/index/SortedSetDocValues;->EMPTY:Lorg/apache/lucene/index/SortedSetDocValues;

    .line 170
    :cond_5
    aput-object v7, v8, v3

    .line 171
    iget v9, v1, Lorg/apache/lucene/index/AtomicReaderContext;->docBase:I

    aput v9, v6, v3

    .line 164
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method public getTermVectors(I)Lorg/apache/lucene/index/Fields;
    .locals 1
    .param p1, "docID"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 189
    invoke-virtual {p0}, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;->ensureOpen()V

    .line 190
    iget-object v0, p0, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;->in:Lorg/apache/lucene/index/CompositeReader;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/CompositeReader;->getTermVectors(I)Lorg/apache/lucene/index/Fields;

    move-result-object v0

    return-object v0
.end method

.method public maxDoc()I
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;->in:Lorg/apache/lucene/index/CompositeReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/CompositeReader;->maxDoc()I

    move-result v0

    return v0
.end method

.method public numDocs()I
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;->in:Lorg/apache/lucene/index/CompositeReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/CompositeReader;->numDocs()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 79
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SlowCompositeReaderWrapper("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;->in:Lorg/apache/lucene/index/CompositeReader;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
