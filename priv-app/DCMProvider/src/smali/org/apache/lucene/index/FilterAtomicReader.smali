.class public Lorg/apache/lucene/index/FilterAtomicReader;
.super Lorg/apache/lucene/index/AtomicReader;
.source "FilterAtomicReader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/FilterAtomicReader$FilterDocsAndPositionsEnum;,
        Lorg/apache/lucene/index/FilterAtomicReader$FilterDocsEnum;,
        Lorg/apache/lucene/index/FilterAtomicReader$FilterFields;,
        Lorg/apache/lucene/index/FilterAtomicReader$FilterTerms;,
        Lorg/apache/lucene/index/FilterAtomicReader$FilterTermsEnum;
    }
.end annotation


# instance fields
.field protected final in:Lorg/apache/lucene/index/AtomicReader;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/AtomicReader;)V
    .locals 0
    .param p1, "in"    # Lorg/apache/lucene/index/AtomicReader;

    .prologue
    .line 327
    invoke-direct {p0}, Lorg/apache/lucene/index/AtomicReader;-><init>()V

    .line 328
    iput-object p1, p0, Lorg/apache/lucene/index/FilterAtomicReader;->in:Lorg/apache/lucene/index/AtomicReader;

    .line 329
    invoke-virtual {p1, p0}, Lorg/apache/lucene/index/AtomicReader;->registerParentReader(Lorg/apache/lucene/index/IndexReader;)V

    .line 330
    return-void
.end method


# virtual methods
.method protected doClose()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 370
    iget-object v0, p0, Lorg/apache/lucene/index/FilterAtomicReader;->in:Lorg/apache/lucene/index/AtomicReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/AtomicReader;->close()V

    .line 371
    return-void
.end method

.method public document(ILorg/apache/lucene/index/StoredFieldVisitor;)V
    .locals 1
    .param p1, "docID"    # I
    .param p2, "visitor"    # Lorg/apache/lucene/index/StoredFieldVisitor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 364
    invoke-virtual {p0}, Lorg/apache/lucene/index/FilterAtomicReader;->ensureOpen()V

    .line 365
    iget-object v0, p0, Lorg/apache/lucene/index/FilterAtomicReader;->in:Lorg/apache/lucene/index/AtomicReader;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/index/AtomicReader;->document(ILorg/apache/lucene/index/StoredFieldVisitor;)V

    .line 366
    return-void
.end method

.method public fields()Lorg/apache/lucene/index/Fields;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 375
    invoke-virtual {p0}, Lorg/apache/lucene/index/FilterAtomicReader;->ensureOpen()V

    .line 376
    iget-object v0, p0, Lorg/apache/lucene/index/FilterAtomicReader;->in:Lorg/apache/lucene/index/AtomicReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/AtomicReader;->fields()Lorg/apache/lucene/index/Fields;

    move-result-object v0

    return-object v0
.end method

.method public getBinaryDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/BinaryDocValues;
    .locals 1
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 395
    invoke-virtual {p0}, Lorg/apache/lucene/index/FilterAtomicReader;->ensureOpen()V

    .line 396
    iget-object v0, p0, Lorg/apache/lucene/index/FilterAtomicReader;->in:Lorg/apache/lucene/index/AtomicReader;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/AtomicReader;->getBinaryDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/BinaryDocValues;

    move-result-object v0

    return-object v0
.end method

.method public getFieldInfos()Lorg/apache/lucene/index/FieldInfos;
    .locals 1

    .prologue
    .line 340
    iget-object v0, p0, Lorg/apache/lucene/index/FilterAtomicReader;->in:Lorg/apache/lucene/index/AtomicReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/AtomicReader;->getFieldInfos()Lorg/apache/lucene/index/FieldInfos;

    move-result-object v0

    return-object v0
.end method

.method public getLiveDocs()Lorg/apache/lucene/util/Bits;
    .locals 1

    .prologue
    .line 334
    invoke-virtual {p0}, Lorg/apache/lucene/index/FilterAtomicReader;->ensureOpen()V

    .line 335
    iget-object v0, p0, Lorg/apache/lucene/index/FilterAtomicReader;->in:Lorg/apache/lucene/index/AtomicReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/AtomicReader;->getLiveDocs()Lorg/apache/lucene/util/Bits;

    move-result-object v0

    return-object v0
.end method

.method public getNormValues(Ljava/lang/String;)Lorg/apache/lucene/index/NumericDocValues;
    .locals 1
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 413
    invoke-virtual {p0}, Lorg/apache/lucene/index/FilterAtomicReader;->ensureOpen()V

    .line 414
    iget-object v0, p0, Lorg/apache/lucene/index/FilterAtomicReader;->in:Lorg/apache/lucene/index/AtomicReader;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/AtomicReader;->getNormValues(Ljava/lang/String;)Lorg/apache/lucene/index/NumericDocValues;

    move-result-object v0

    return-object v0
.end method

.method public getNumericDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/NumericDocValues;
    .locals 1
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 389
    invoke-virtual {p0}, Lorg/apache/lucene/index/FilterAtomicReader;->ensureOpen()V

    .line 390
    iget-object v0, p0, Lorg/apache/lucene/index/FilterAtomicReader;->in:Lorg/apache/lucene/index/AtomicReader;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/AtomicReader;->getNumericDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/NumericDocValues;

    move-result-object v0

    return-object v0
.end method

.method public getSortedDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/SortedDocValues;
    .locals 1
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 401
    invoke-virtual {p0}, Lorg/apache/lucene/index/FilterAtomicReader;->ensureOpen()V

    .line 402
    iget-object v0, p0, Lorg/apache/lucene/index/FilterAtomicReader;->in:Lorg/apache/lucene/index/AtomicReader;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/AtomicReader;->getSortedDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/SortedDocValues;

    move-result-object v0

    return-object v0
.end method

.method public getSortedSetDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/SortedSetDocValues;
    .locals 1
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 407
    invoke-virtual {p0}, Lorg/apache/lucene/index/FilterAtomicReader;->ensureOpen()V

    .line 408
    iget-object v0, p0, Lorg/apache/lucene/index/FilterAtomicReader;->in:Lorg/apache/lucene/index/AtomicReader;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/AtomicReader;->getSortedSetDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/SortedSetDocValues;

    move-result-object v0

    return-object v0
.end method

.method public getTermVectors(I)Lorg/apache/lucene/index/Fields;
    .locals 1
    .param p1, "docID"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 346
    invoke-virtual {p0}, Lorg/apache/lucene/index/FilterAtomicReader;->ensureOpen()V

    .line 347
    iget-object v0, p0, Lorg/apache/lucene/index/FilterAtomicReader;->in:Lorg/apache/lucene/index/AtomicReader;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/AtomicReader;->getTermVectors(I)Lorg/apache/lucene/index/Fields;

    move-result-object v0

    return-object v0
.end method

.method public maxDoc()I
    .locals 1

    .prologue
    .line 359
    iget-object v0, p0, Lorg/apache/lucene/index/FilterAtomicReader;->in:Lorg/apache/lucene/index/AtomicReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v0

    return v0
.end method

.method public numDocs()I
    .locals 1

    .prologue
    .line 353
    iget-object v0, p0, Lorg/apache/lucene/index/FilterAtomicReader;->in:Lorg/apache/lucene/index/AtomicReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/AtomicReader;->numDocs()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 381
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "FilterAtomicReader("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 382
    .local v0, "buffer":Ljava/lang/StringBuilder;
    iget-object v1, p0, Lorg/apache/lucene/index/FilterAtomicReader;->in:Lorg/apache/lucene/index/AtomicReader;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 383
    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 384
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
