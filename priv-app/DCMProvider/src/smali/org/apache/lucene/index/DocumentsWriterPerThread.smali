.class Lorg/apache/lucene/index/DocumentsWriterPerThread;
.super Ljava/lang/Object;
.source "DocumentsWriterPerThread.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;,
        Lorg/apache/lucene/index/DocumentsWriterPerThread$FlushedSegment;,
        Lorg/apache/lucene/index/DocumentsWriterPerThread$IndexingChain;,
        Lorg/apache/lucene/index/DocumentsWriterPerThread$IntBlockAllocator;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field static final BYTE_BLOCK_NOT_MASK:I = -0x8000

.field private static final INFO_VERBOSE:Z = false

.field static final MAX_TERM_LENGTH_UTF8:I = 0x7ffe

.field static final defaultIndexingChain:Lorg/apache/lucene/index/DocumentsWriterPerThread$IndexingChain;


# instance fields
.field aborting:Z

.field final byteBlockAllocator:Lorg/apache/lucene/util/ByteBlockPool$Allocator;

.field final bytesUsed:Lorg/apache/lucene/util/Counter;

.field final codec:Lorg/apache/lucene/codecs/Codec;

.field final consumer:Lorg/apache/lucene/index/DocConsumer;

.field deleteQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

.field deleteSlice:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;

.field final directory:Lorg/apache/lucene/store/TrackingDirectoryWrapper;

.field final directoryOrig:Lorg/apache/lucene/store/Directory;

.field final docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

.field private fieldInfos:Lorg/apache/lucene/index/FieldInfos$Builder;

.field flushState:Lorg/apache/lucene/index/SegmentWriteState;

.field private flushedDocCount:I

.field hasAborted:Z

.field private final infoStream:Lorg/apache/lucene/util/InfoStream;

.field final intBlockAllocator:Lorg/apache/lucene/util/IntBlockPool$Allocator;

.field private final nf:Ljava/text/NumberFormat;

.field private numDocsInRAM:I

.field final parent:Lorg/apache/lucene/index/DocumentsWriter;

.field pendingDeletes:Lorg/apache/lucene/index/BufferedDeletes;

.field segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

.field final writer:Lorg/apache/lucene/index/IndexWriter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-class v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->$assertionsDisabled:Z

    .line 58
    new-instance v0, Lorg/apache/lucene/index/DocumentsWriterPerThread$1;

    invoke-direct {v0}, Lorg/apache/lucene/index/DocumentsWriterPerThread$1;-><init>()V

    sput-object v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->defaultIndexingChain:Lorg/apache/lucene/index/DocumentsWriterPerThread$IndexingChain;

    .line 633
    return-void

    .line 46
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/index/DocumentsWriterPerThread;Lorg/apache/lucene/index/FieldInfos$Builder;)V
    .locals 3
    .param p1, "other"    # Lorg/apache/lucene/index/DocumentsWriterPerThread;
    .param p2, "fieldInfos"    # Lorg/apache/lucene/index/FieldInfos$Builder;

    .prologue
    .line 221
    iget-object v0, p1, Lorg/apache/lucene/index/DocumentsWriterPerThread;->directoryOrig:Lorg/apache/lucene/store/Directory;

    iget-object v1, p1, Lorg/apache/lucene/index/DocumentsWriterPerThread;->parent:Lorg/apache/lucene/index/DocumentsWriter;

    iget-object v2, p1, Lorg/apache/lucene/index/DocumentsWriterPerThread;->parent:Lorg/apache/lucene/index/DocumentsWriter;

    iget-object v2, v2, Lorg/apache/lucene/index/DocumentsWriter;->chain:Lorg/apache/lucene/index/DocumentsWriterPerThread$IndexingChain;

    invoke-direct {p0, v0, v1, p2, v2}, Lorg/apache/lucene/index/DocumentsWriterPerThread;-><init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/DocumentsWriter;Lorg/apache/lucene/index/FieldInfos$Builder;Lorg/apache/lucene/index/DocumentsWriterPerThread$IndexingChain;)V

    .line 222
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/DocumentsWriter;Lorg/apache/lucene/index/FieldInfos$Builder;Lorg/apache/lucene/index/DocumentsWriterPerThread$IndexingChain;)V
    .locals 2
    .param p1, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p2, "parent"    # Lorg/apache/lucene/index/DocumentsWriter;
    .param p3, "fieldInfos"    # Lorg/apache/lucene/index/FieldInfos$Builder;
    .param p4, "indexingChain"    # Lorg/apache/lucene/index/DocumentsWriterPerThread$IndexingChain;

    .prologue
    const/4 v0, 0x0

    .line 199
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 185
    iput-boolean v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->aborting:Z

    .line 186
    iput-boolean v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->hasAborted:Z

    .line 194
    sget-object v0, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-static {v0}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->nf:Ljava/text/NumberFormat;

    .line 201
    iput-object p1, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->directoryOrig:Lorg/apache/lucene/store/Directory;

    .line 202
    new-instance v0, Lorg/apache/lucene/store/TrackingDirectoryWrapper;

    invoke-direct {v0, p1}, Lorg/apache/lucene/store/TrackingDirectoryWrapper;-><init>(Lorg/apache/lucene/store/Directory;)V

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->directory:Lorg/apache/lucene/store/TrackingDirectoryWrapper;

    .line 203
    iput-object p2, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->parent:Lorg/apache/lucene/index/DocumentsWriter;

    .line 204
    iput-object p3, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->fieldInfos:Lorg/apache/lucene/index/FieldInfos$Builder;

    .line 205
    iget-object v0, p2, Lorg/apache/lucene/index/DocumentsWriter;->indexWriter:Lorg/apache/lucene/index/IndexWriter;

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->writer:Lorg/apache/lucene/index/IndexWriter;

    .line 206
    iget-object v0, p2, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->infoStream:Lorg/apache/lucene/util/InfoStream;

    .line 207
    iget-object v0, p2, Lorg/apache/lucene/index/DocumentsWriter;->codec:Lorg/apache/lucene/codecs/Codec;

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->codec:Lorg/apache/lucene/codecs/Codec;

    .line 208
    new-instance v0, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->infoStream:Lorg/apache/lucene/util/InfoStream;

    invoke-direct {v0, p0, v1}, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;-><init>(Lorg/apache/lucene/index/DocumentsWriterPerThread;Lorg/apache/lucene/util/InfoStream;)V

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    .line 209
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    iget-object v1, p2, Lorg/apache/lucene/index/DocumentsWriter;->indexWriter:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v1}, Lorg/apache/lucene/index/IndexWriter;->getConfig()Lorg/apache/lucene/index/LiveIndexWriterConfig;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getSimilarity()Lorg/apache/lucene/search/similarities/Similarity;

    move-result-object v1

    iput-object v1, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;->similarity:Lorg/apache/lucene/search/similarities/Similarity;

    .line 210
    invoke-static {}, Lorg/apache/lucene/util/Counter;->newCounter()Lorg/apache/lucene/util/Counter;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->bytesUsed:Lorg/apache/lucene/util/Counter;

    .line 211
    new-instance v0, Lorg/apache/lucene/util/ByteBlockPool$DirectTrackingAllocator;

    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->bytesUsed:Lorg/apache/lucene/util/Counter;

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/ByteBlockPool$DirectTrackingAllocator;-><init>(Lorg/apache/lucene/util/Counter;)V

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->byteBlockAllocator:Lorg/apache/lucene/util/ByteBlockPool$Allocator;

    .line 212
    new-instance v0, Lorg/apache/lucene/index/BufferedDeletes;

    invoke-direct {v0}, Lorg/apache/lucene/index/BufferedDeletes;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->pendingDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    .line 213
    new-instance v0, Lorg/apache/lucene/index/DocumentsWriterPerThread$IntBlockAllocator;

    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->bytesUsed:Lorg/apache/lucene/util/Counter;

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/DocumentsWriterPerThread$IntBlockAllocator;-><init>(Lorg/apache/lucene/util/Counter;)V

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->intBlockAllocator:Lorg/apache/lucene/util/IntBlockPool$Allocator;

    .line 214
    invoke-virtual {p0}, Lorg/apache/lucene/index/DocumentsWriterPerThread;->initialize()V

    .line 217
    invoke-virtual {p4, p0}, Lorg/apache/lucene/index/DocumentsWriterPerThread$IndexingChain;->getChain(Lorg/apache/lucene/index/DocumentsWriterPerThread;)Lorg/apache/lucene/index/DocConsumer;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->consumer:Lorg/apache/lucene/index/DocConsumer;

    .line 218
    return-void
.end method

.method private doAfterFlush()V
    .locals 2

    .prologue
    .line 429
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    .line 430
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->consumer:Lorg/apache/lucene/index/DocConsumer;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocConsumer;->doAfterFlush()V

    .line 431
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->directory:Lorg/apache/lucene/store/TrackingDirectoryWrapper;

    invoke-virtual {v0}, Lorg/apache/lucene/store/TrackingDirectoryWrapper;->getCreatedFiles()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 432
    new-instance v0, Lorg/apache/lucene/index/FieldInfos$Builder;

    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->fieldInfos:Lorg/apache/lucene/index/FieldInfos$Builder;

    iget-object v1, v1, Lorg/apache/lucene/index/FieldInfos$Builder;->globalFieldNumbers:Lorg/apache/lucene/index/FieldInfos$FieldNumbers;

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/FieldInfos$Builder;-><init>(Lorg/apache/lucene/index/FieldInfos$FieldNumbers;)V

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->fieldInfos:Lorg/apache/lucene/index/FieldInfos$Builder;

    .line 433
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->parent:Lorg/apache/lucene/index/DocumentsWriter;

    iget v1, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->numDocsInRAM:I

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/DocumentsWriter;->subtractFlushedNumDocs(I)V

    .line 434
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->numDocsInRAM:I

    .line 435
    return-void
.end method

.method private finishDocument(Lorg/apache/lucene/index/Term;)V
    .locals 3
    .param p1, "delTerm"    # Lorg/apache/lucene/index/Term;

    .prologue
    .line 377
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->deleteSlice:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;

    if-nez v0, :cond_1

    .line 378
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->deleteQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->newSlice()Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->deleteSlice:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;

    .line 379
    if-eqz p1, :cond_0

    .line 380
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->deleteQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->deleteSlice:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;

    invoke-virtual {v0, p1, v1}, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->add(Lorg/apache/lucene/index/Term;Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;)V

    .line 381
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->deleteSlice:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;->reset()V

    .line 393
    :cond_0
    :goto_0
    iget v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->numDocsInRAM:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->numDocsInRAM:I

    .line 394
    return-void

    .line 385
    :cond_1
    if-eqz p1, :cond_3

    .line 386
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->deleteQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->deleteSlice:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;

    invoke-virtual {v0, p1, v1}, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->add(Lorg/apache/lucene/index/Term;Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;)V

    .line 387
    sget-boolean v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->deleteSlice:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;->isTailItem(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "expected the delete term as the tail item"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 388
    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->deleteSlice:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;

    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->pendingDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    iget v2, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->numDocsInRAM:I

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;->apply(Lorg/apache/lucene/index/BufferedDeletes;I)V

    goto :goto_0

    .line 389
    :cond_3
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->deleteQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->deleteSlice:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->updateSlice(Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 390
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->deleteSlice:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;

    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->pendingDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    iget v2, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->numDocsInRAM:I

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;->apply(Lorg/apache/lucene/index/BufferedDeletes;I)V

    goto :goto_0
.end method

.method private initSegmentInfo()V
    .locals 9

    .prologue
    const/4 v7, 0x0

    .line 285
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexWriter;->newSegmentName()Ljava/lang/String;

    move-result-object v3

    .line 286
    .local v3, "segment":Ljava/lang/String;
    new-instance v0, Lorg/apache/lucene/index/SegmentInfo;

    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->directoryOrig:Lorg/apache/lucene/store/Directory;

    sget-object v2, Lorg/apache/lucene/util/Constants;->LUCENE_MAIN_VERSION:Ljava/lang/String;

    const/4 v4, -0x1

    .line 287
    const/4 v5, 0x0

    iget-object v6, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->codec:Lorg/apache/lucene/codecs/Codec;

    move-object v8, v7

    invoke-direct/range {v0 .. v8}, Lorg/apache/lucene/index/SegmentInfo;-><init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Ljava/lang/String;IZLorg/apache/lucene/codecs/Codec;Ljava/util/Map;Ljava/util/Map;)V

    .line 286
    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    .line 288
    sget-boolean v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->numDocsInRAM:I

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 292
    :cond_0
    return-void
.end method


# virtual methods
.method abort()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 149
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->aborting:Z

    iput-boolean v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->hasAborted:Z

    .line 151
    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v1, "DWPT"

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v1, "DWPT"

    const-string v2, "now abort"

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 155
    :cond_0
    :try_start_1
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->consumer:Lorg/apache/lucene/index/DocConsumer;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocConsumer;->abort()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 159
    :goto_0
    :try_start_2
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->pendingDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    invoke-virtual {v0}, Lorg/apache/lucene/index/BufferedDeletes;->clear()V

    .line 160
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->deleteQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->newSlice()Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->deleteSlice:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;

    .line 162
    invoke-direct {p0}, Lorg/apache/lucene/index/DocumentsWriterPerThread;->doAfterFlush()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 165
    iput-boolean v3, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->aborting:Z

    .line 166
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v1, "DWPT"

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 167
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v1, "DWPT"

    const-string v2, "done abort"

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    :cond_1
    return-void

    .line 164
    :catchall_0
    move-exception v0

    .line 165
    iput-boolean v3, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->aborting:Z

    .line 166
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v2, "DWPT"

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 167
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v2, "DWPT"

    const-string v3, "done abort"

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    :cond_2
    throw v0

    .line 156
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method bytesUsed()J
    .locals 4

    .prologue
    .line 624
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->bytesUsed:Lorg/apache/lucene/util/Counter;

    invoke-virtual {v0}, Lorg/apache/lucene/util/Counter;->get()J

    move-result-wide v0

    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->pendingDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    iget-object v2, v2, Lorg/apache/lucene/index/BufferedDeletes;->bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v2

    add-long/2addr v0, v2

    return-wide v0
.end method

.method checkAndResetHasAborted()Z
    .locals 2

    .prologue
    .line 236
    iget-boolean v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->hasAborted:Z

    .line 237
    .local v0, "retval":Z
    const/4 v1, 0x0

    iput-boolean v1, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->hasAborted:Z

    .line 238
    return v0
.end method

.method deleteDocID(I)V
    .locals 1
    .param p1, "docIDUpto"    # I

    .prologue
    .line 399
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->pendingDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/BufferedDeletes;->addDocID(I)V

    .line 409
    return-void
.end method

.method flush()Lorg/apache/lucene/index/DocumentsWriterPerThread$FlushedSegment;
    .locals 20
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 458
    sget-boolean v4, Lorg/apache/lucene/index/DocumentsWriterPerThread;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->numDocsInRAM:I

    if-gtz v4, :cond_0

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 459
    :cond_0
    sget-boolean v4, Lorg/apache/lucene/index/DocumentsWriterPerThread;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->deleteSlice:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;

    if-eqz v4, :cond_1

    new-instance v4, Ljava/lang/AssertionError;

    const-string v6, "all deletes must be applied in prepareFlush"

    invoke-direct {v4, v6}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    .line 460
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    move-object/from16 v0, p0

    iget v6, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->numDocsInRAM:I

    invoke-virtual {v4, v6}, Lorg/apache/lucene/index/SegmentInfo;->setDocCount(I)V

    .line 461
    new-instance v2, Lorg/apache/lucene/index/SegmentWriteState;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->infoStream:Lorg/apache/lucene/util/InfoStream;

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->directory:Lorg/apache/lucene/store/TrackingDirectoryWrapper;

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->fieldInfos:Lorg/apache/lucene/index/FieldInfos$Builder;

    invoke-virtual {v6}, Lorg/apache/lucene/index/FieldInfos$Builder;->finish()Lorg/apache/lucene/index/FieldInfos;

    move-result-object v6

    .line 462
    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v7}, Lorg/apache/lucene/index/IndexWriter;->getConfig()Lorg/apache/lucene/index/LiveIndexWriterConfig;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getTermIndexInterval()I

    move-result v7

    .line 463
    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->pendingDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    new-instance v9, Lorg/apache/lucene/store/IOContext;

    new-instance v16, Lorg/apache/lucene/store/FlushInfo;

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->numDocsInRAM:I

    move/from16 v17, v0

    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/DocumentsWriterPerThread;->bytesUsed()J

    move-result-wide v18

    invoke-direct/range {v16 .. v19}, Lorg/apache/lucene/store/FlushInfo;-><init>(IJ)V

    move-object/from16 v0, v16

    invoke-direct {v9, v0}, Lorg/apache/lucene/store/IOContext;-><init>(Lorg/apache/lucene/store/FlushInfo;)V

    invoke-direct/range {v2 .. v9}, Lorg/apache/lucene/index/SegmentWriteState;-><init>(Lorg/apache/lucene/util/InfoStream;Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/index/FieldInfos;ILorg/apache/lucene/index/BufferedDeletes;Lorg/apache/lucene/store/IOContext;)V

    .line 461
    move-object/from16 v0, p0

    iput-object v2, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->flushState:Lorg/apache/lucene/index/SegmentWriteState;

    .line 464
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->parent:Lorg/apache/lucene/index/DocumentsWriter;

    iget-object v4, v4, Lorg/apache/lucene/index/DocumentsWriter;->flushControl:Lorg/apache/lucene/index/DocumentsWriterFlushControl;

    invoke-virtual {v4}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->netBytes()J

    move-result-wide v6

    long-to-double v6, v6

    const-wide/high16 v8, 0x4090000000000000L    # 1024.0

    div-double/2addr v6, v8

    const-wide/high16 v8, 0x4090000000000000L    # 1024.0

    div-double v14, v6, v8

    .line 469
    .local v14, "startMBUsed":D
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->pendingDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    iget-object v4, v4, Lorg/apache/lucene/index/BufferedDeletes;->docIDs:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_2

    .line 470
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->flushState:Lorg/apache/lucene/index/SegmentWriteState;

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->codec:Lorg/apache/lucene/codecs/Codec;

    invoke-virtual {v6}, Lorg/apache/lucene/codecs/Codec;->liveDocsFormat()Lorg/apache/lucene/codecs/LiveDocsFormat;

    move-result-object v6

    move-object/from16 v0, p0

    iget v7, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->numDocsInRAM:I

    invoke-virtual {v6, v7}, Lorg/apache/lucene/codecs/LiveDocsFormat;->newLiveDocs(I)Lorg/apache/lucene/util/MutableBits;

    move-result-object v6

    iput-object v6, v4, Lorg/apache/lucene/index/SegmentWriteState;->liveDocs:Lorg/apache/lucene/util/MutableBits;

    .line 471
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->pendingDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    iget-object v4, v4, Lorg/apache/lucene/index/BufferedDeletes;->docIDs:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_5

    .line 474
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->flushState:Lorg/apache/lucene/index/SegmentWriteState;

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->pendingDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    iget-object v6, v6, Lorg/apache/lucene/index/BufferedDeletes;->docIDs:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    iput v6, v4, Lorg/apache/lucene/index/SegmentWriteState;->delCountOnFlush:I

    .line 475
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->pendingDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    iget-object v4, v4, Lorg/apache/lucene/index/BufferedDeletes;->bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->pendingDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    iget-object v6, v6, Lorg/apache/lucene/index/BufferedDeletes;->docIDs:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    neg-int v6, v6

    sget v7, Lorg/apache/lucene/index/BufferedDeletes;->BYTES_PER_DEL_DOCID:I

    mul-int/2addr v6, v7

    int-to-long v6, v6

    invoke-virtual {v4, v6, v7}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    .line 476
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->pendingDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    iget-object v4, v4, Lorg/apache/lucene/index/BufferedDeletes;->docIDs:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 479
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->aborting:Z

    if-eqz v4, :cond_6

    .line 480
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v6, "DWPT"

    invoke-virtual {v4, v6}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 481
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v6, "DWPT"

    const-string v7, "flush: skip because aborting is set"

    invoke-virtual {v4, v6, v7}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 483
    :cond_3
    const/4 v2, 0x0

    .line 537
    :cond_4
    :goto_1
    return-object v2

    .line 471
    :cond_5
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v10

    .line 472
    .local v10, "delDocID":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->flushState:Lorg/apache/lucene/index/SegmentWriteState;

    iget-object v4, v4, Lorg/apache/lucene/index/SegmentWriteState;->liveDocs:Lorg/apache/lucene/util/MutableBits;

    invoke-interface {v4, v10}, Lorg/apache/lucene/util/MutableBits;->clear(I)V

    goto :goto_0

    .line 486
    .end local v10    # "delDocID":I
    :cond_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v6, "DWPT"

    invoke-virtual {v4, v6}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 487
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v6, "DWPT"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "flush postings as segment "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->flushState:Lorg/apache/lucene/index/SegmentWriteState;

    iget-object v8, v8, Lorg/apache/lucene/index/SegmentWriteState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v8, v8, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " numDocs="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p0

    iget v8, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->numDocsInRAM:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 490
    :cond_7
    const/4 v11, 0x0

    .line 493
    .local v11, "success":Z
    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->consumer:Lorg/apache/lucene/index/DocConsumer;

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->flushState:Lorg/apache/lucene/index/SegmentWriteState;

    invoke-virtual {v4, v6}, Lorg/apache/lucene/index/DocConsumer;->flush(Lorg/apache/lucene/index/SegmentWriteState;)V

    .line 494
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->pendingDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    iget-object v4, v4, Lorg/apache/lucene/index/BufferedDeletes;->terms:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->clear()V

    .line 495
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    new-instance v6, Ljava/util/HashSet;

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->directory:Lorg/apache/lucene/store/TrackingDirectoryWrapper;

    invoke-virtual {v7}, Lorg/apache/lucene/store/TrackingDirectoryWrapper;->getCreatedFiles()Ljava/util/Set;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v4, v6}, Lorg/apache/lucene/index/SegmentInfo;->setFiles(Ljava/util/Set;)V

    .line 497
    new-instance v3, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    const/4 v6, 0x0

    const-wide/16 v8, -0x1

    invoke-direct {v3, v4, v6, v8, v9}, Lorg/apache/lucene/index/SegmentInfoPerCommit;-><init>(Lorg/apache/lucene/index/SegmentInfo;IJ)V

    .line 498
    .local v3, "segmentInfoPerCommit":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v6, "DWPT"

    invoke-virtual {v4, v6}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 499
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v7, "DWPT"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v4, "new segment has "

    invoke-direct {v8, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->flushState:Lorg/apache/lucene/index/SegmentWriteState;

    iget-object v4, v4, Lorg/apache/lucene/index/SegmentWriteState;->liveDocs:Lorg/apache/lucene/util/MutableBits;

    if-nez v4, :cond_c

    const/4 v4, 0x0

    :goto_2
    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, " deleted docs"

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v7, v4}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 500
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v7, "DWPT"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v4, "new segment has "

    invoke-direct {v8, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 501
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->flushState:Lorg/apache/lucene/index/SegmentWriteState;

    iget-object v4, v4, Lorg/apache/lucene/index/SegmentWriteState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v4}, Lorg/apache/lucene/index/FieldInfos;->hasVectors()Z

    move-result v4

    if-eqz v4, :cond_d

    const-string v4, "vectors"

    :goto_3
    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, "; "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 502
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->flushState:Lorg/apache/lucene/index/SegmentWriteState;

    iget-object v4, v4, Lorg/apache/lucene/index/SegmentWriteState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v4}, Lorg/apache/lucene/index/FieldInfos;->hasNorms()Z

    move-result v4

    if-eqz v4, :cond_e

    const-string v4, "norms"

    :goto_4
    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, "; "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 503
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->flushState:Lorg/apache/lucene/index/SegmentWriteState;

    iget-object v4, v4, Lorg/apache/lucene/index/SegmentWriteState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v4}, Lorg/apache/lucene/index/FieldInfos;->hasDocValues()Z

    move-result v4

    if-eqz v4, :cond_f

    const-string v4, "docValues"

    :goto_5
    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, "; "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 504
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->flushState:Lorg/apache/lucene/index/SegmentWriteState;

    iget-object v4, v4, Lorg/apache/lucene/index/SegmentWriteState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v4}, Lorg/apache/lucene/index/FieldInfos;->hasProx()Z

    move-result v4

    if-eqz v4, :cond_10

    const-string v4, "prox"

    :goto_6
    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, "; "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 505
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->flushState:Lorg/apache/lucene/index/SegmentWriteState;

    iget-object v4, v4, Lorg/apache/lucene/index/SegmentWriteState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v4}, Lorg/apache/lucene/index/FieldInfos;->hasFreq()Z

    move-result v4

    if-eqz v4, :cond_11

    const-string v4, "freqs"

    :goto_7
    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 500
    invoke-virtual {v6, v7, v4}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 506
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v6, "DWPT"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "flushedFiles="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->files()Ljava/util/Collection;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 507
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v6, "DWPT"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "flushed codec="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->codec:Lorg/apache/lucene/codecs/Codec;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 510
    :cond_8
    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->flushedDocCount:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->flushState:Lorg/apache/lucene/index/SegmentWriteState;

    iget-object v6, v6, Lorg/apache/lucene/index/SegmentWriteState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v6}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v6

    add-int/2addr v4, v6

    move-object/from16 v0, p0

    iput v4, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->flushedDocCount:I

    .line 513
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->pendingDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    iget-object v4, v4, Lorg/apache/lucene/index/BufferedDeletes;->queries:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_12

    .line 514
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->pendingDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    invoke-virtual {v4}, Lorg/apache/lucene/index/BufferedDeletes;->clear()V

    .line 515
    const/4 v5, 0x0

    .line 521
    .local v5, "segmentDeletes":Lorg/apache/lucene/index/BufferedDeletes;
    :goto_8
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v6, "DWPT"

    invoke-virtual {v4, v6}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 522
    invoke-virtual {v3}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->sizeInBytes()J

    move-result-wide v6

    long-to-double v6, v6

    const-wide/high16 v8, 0x4090000000000000L    # 1024.0

    div-double/2addr v6, v8

    const-wide/high16 v8, 0x4090000000000000L    # 1024.0

    div-double v12, v6, v8

    .line 523
    .local v12, "newSegmentSize":D
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v6, "DWPT"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "flushed: segment="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v8, v8, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 524
    const-string v8, " ramUsed="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->nf:Ljava/text/NumberFormat;

    invoke-virtual {v8, v14, v15}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " MB"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 525
    const-string v8, " newFlushedSize(includes docstores)="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->nf:Ljava/text/NumberFormat;

    invoke-virtual {v8, v12, v13}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " MB"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 526
    const-string v8, " docs/MB="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->nf:Ljava/text/NumberFormat;

    move-object/from16 v0, p0

    iget v9, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->flushedDocCount:I

    int-to-double v0, v9

    move-wide/from16 v16, v0

    div-double v16, v16, v12

    move-wide/from16 v0, v16

    invoke-virtual {v8, v0, v1}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 523
    invoke-virtual {v4, v6, v7}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 529
    .end local v12    # "newSegmentSize":D
    :cond_9
    sget-boolean v4, Lorg/apache/lucene/index/DocumentsWriterPerThread;->$assertionsDisabled:Z

    if-nez v4, :cond_13

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    if-nez v4, :cond_13

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 538
    .end local v3    # "segmentInfoPerCommit":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .end local v5    # "segmentDeletes":Lorg/apache/lucene/index/BufferedDeletes;
    :catchall_0
    move-exception v4

    .line 539
    if-nez v11, :cond_b

    .line 540
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    if-eqz v6, :cond_a

    .line 541
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->writer:Lorg/apache/lucene/index/IndexWriter;

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v6, v7}, Lorg/apache/lucene/index/IndexWriter;->flushFailed(Lorg/apache/lucene/index/SegmentInfo;)V

    .line 543
    :cond_a
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/DocumentsWriterPerThread;->abort()V

    .line 545
    :cond_b
    throw v4

    .line 499
    .restart local v3    # "segmentInfoPerCommit":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    :cond_c
    :try_start_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->flushState:Lorg/apache/lucene/index/SegmentWriteState;

    iget-object v4, v4, Lorg/apache/lucene/index/SegmentWriteState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v4}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->flushState:Lorg/apache/lucene/index/SegmentWriteState;

    iget v9, v9, Lorg/apache/lucene/index/SegmentWriteState;->delCountOnFlush:I

    sub-int/2addr v4, v9

    goto/16 :goto_2

    .line 501
    :cond_d
    const-string v4, "no vectors"

    goto/16 :goto_3

    .line 502
    :cond_e
    const-string v4, "no norms"

    goto/16 :goto_4

    .line 503
    :cond_f
    const-string v4, "no docValues"

    goto/16 :goto_5

    .line 504
    :cond_10
    const-string v4, "no prox"

    goto/16 :goto_6

    .line 505
    :cond_11
    const-string v4, "no freqs"

    goto/16 :goto_7

    .line 517
    :cond_12
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->pendingDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    .line 518
    .restart local v5    # "segmentDeletes":Lorg/apache/lucene/index/BufferedDeletes;
    new-instance v4, Lorg/apache/lucene/index/BufferedDeletes;

    invoke-direct {v4}, Lorg/apache/lucene/index/BufferedDeletes;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->pendingDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    goto/16 :goto_8

    .line 531
    :cond_13
    new-instance v2, Lorg/apache/lucene/index/DocumentsWriterPerThread$FlushedSegment;

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->flushState:Lorg/apache/lucene/index/SegmentWriteState;

    iget-object v4, v4, Lorg/apache/lucene/index/SegmentWriteState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    .line 532
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->flushState:Lorg/apache/lucene/index/SegmentWriteState;

    iget-object v6, v6, Lorg/apache/lucene/index/SegmentWriteState;->liveDocs:Lorg/apache/lucene/util/MutableBits;

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->flushState:Lorg/apache/lucene/index/SegmentWriteState;

    iget v7, v7, Lorg/apache/lucene/index/SegmentWriteState;->delCountOnFlush:I

    .line 531
    const/4 v8, 0x0

    invoke-direct/range {v2 .. v8}, Lorg/apache/lucene/index/DocumentsWriterPerThread$FlushedSegment;-><init>(Lorg/apache/lucene/index/SegmentInfoPerCommit;Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/index/BufferedDeletes;Lorg/apache/lucene/util/MutableBits;ILorg/apache/lucene/index/DocumentsWriterPerThread$FlushedSegment;)V

    .line 533
    .local v2, "fs":Lorg/apache/lucene/index/DocumentsWriterPerThread$FlushedSegment;
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/apache/lucene/index/DocumentsWriterPerThread;->sealFlushedSegment(Lorg/apache/lucene/index/DocumentsWriterPerThread$FlushedSegment;)V

    .line 534
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/index/DocumentsWriterPerThread;->doAfterFlush()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 535
    const/4 v11, 0x1

    .line 539
    if-nez v11, :cond_4

    .line 540
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    if-eqz v4, :cond_14

    .line 541
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->writer:Lorg/apache/lucene/index/IndexWriter;

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v4, v6}, Lorg/apache/lucene/index/IndexWriter;->flushFailed(Lorg/apache/lucene/index/SegmentInfo;)V

    .line 543
    :cond_14
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/DocumentsWriterPerThread;->abort()V

    goto/16 :goto_1
.end method

.method public getNumDocsInRAM()I
    .locals 1

    .prologue
    .line 424
    iget v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->numDocsInRAM:I

    return v0
.end method

.method getSegmentInfo()Lorg/apache/lucene/index/SegmentInfo;
    .locals 1

    .prologue
    .line 620
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    return-object v0
.end method

.method initialize()V
    .locals 3

    .prologue
    .line 225
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->parent:Lorg/apache/lucene/index/DocumentsWriter;

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter;->deleteQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->deleteQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    .line 226
    sget-boolean v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->numDocsInRAM:I

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "num docs "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->numDocsInRAM:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 227
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->pendingDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    invoke-virtual {v0}, Lorg/apache/lucene/index/BufferedDeletes;->clear()V

    .line 228
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->deleteSlice:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;

    .line 229
    return-void
.end method

.method public numDeleteTerms()I
    .locals 1

    .prologue
    .line 416
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->pendingDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    iget-object v0, v0, Lorg/apache/lucene/index/BufferedDeletes;->numTermDeletes:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    return v0
.end method

.method prepareFlush()Lorg/apache/lucene/index/FrozenBufferedDeletes;
    .locals 4

    .prologue
    .line 443
    sget-boolean v1, Lorg/apache/lucene/index/DocumentsWriterPerThread;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget v1, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->numDocsInRAM:I

    if-gtz v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 444
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->deleteQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->deleteSlice:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->freezeGlobalBuffer(Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;)Lorg/apache/lucene/index/FrozenBufferedDeletes;

    move-result-object v0

    .line 447
    .local v0, "globalDeletes":Lorg/apache/lucene/index/FrozenBufferedDeletes;
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->deleteSlice:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;

    if-eqz v1, :cond_2

    .line 449
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->deleteSlice:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;

    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->pendingDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    iget v3, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->numDocsInRAM:I

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;->apply(Lorg/apache/lucene/index/BufferedDeletes;I)V

    .line 450
    sget-boolean v1, Lorg/apache/lucene/index/DocumentsWriterPerThread;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->deleteSlice:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;

    invoke-virtual {v1}, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 451
    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->deleteSlice:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;

    .line 453
    :cond_2
    return-object v0
.end method

.method sealFlushedSegment(Lorg/apache/lucene/index/DocumentsWriterPerThread$FlushedSegment;)V
    .locals 12
    .param p1, "flushedSegment"    # Lorg/apache/lucene/index/DocumentsWriterPerThread$FlushedSegment;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 553
    sget-boolean v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 555
    :cond_0
    iget-object v7, p1, Lorg/apache/lucene/index/DocumentsWriterPerThread$FlushedSegment;->segmentInfo:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .line 557
    .local v7, "newSegment":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    iget-object v0, v7, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    const-string v1, "flush"

    invoke-static {v0, v1}, Lorg/apache/lucene/index/IndexWriter;->setDiagnostics(Lorg/apache/lucene/index/SegmentInfo;Ljava/lang/String;)V

    .line 559
    new-instance v5, Lorg/apache/lucene/store/IOContext;

    new-instance v0, Lorg/apache/lucene/store/FlushInfo;

    iget-object v1, v7, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v1

    invoke-virtual {v7}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->sizeInBytes()J

    move-result-wide v10

    invoke-direct {v0, v1, v10, v11}, Lorg/apache/lucene/store/FlushInfo;-><init>(IJ)V

    invoke-direct {v5, v0}, Lorg/apache/lucene/store/IOContext;-><init>(Lorg/apache/lucene/store/FlushInfo;)V

    .line 561
    .local v5, "context":Lorg/apache/lucene/store/IOContext;
    const/4 v9, 0x0

    .line 563
    .local v9, "success":Z
    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0, v7}, Lorg/apache/lucene/index/IndexWriter;->useCompoundFile(Lorg/apache/lucene/index/SegmentInfoPerCommit;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 566
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->infoStream:Lorg/apache/lucene/util/InfoStream;

    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->directory:Lorg/apache/lucene/store/TrackingDirectoryWrapper;

    sget-object v2, Lorg/apache/lucene/index/MergeState$CheckAbort;->NONE:Lorg/apache/lucene/index/MergeState$CheckAbort;

    iget-object v10, v7, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-static {v0, v1, v2, v10, v5}, Lorg/apache/lucene/index/IndexWriter;->createCompoundFile(Lorg/apache/lucene/util/InfoStream;Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/MergeState$CheckAbort;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/store/IOContext;)Ljava/util/Collection;

    move-result-object v8

    .line 567
    .local v8, "oldFiles":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    iget-object v0, v7, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/SegmentInfo;->setUseCompoundFile(Z)V

    .line 568
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0, v8}, Lorg/apache/lucene/index/IndexWriter;->deleteNewFiles(Ljava/util/Collection;)V

    .line 575
    .end local v8    # "oldFiles":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->codec:Lorg/apache/lucene/codecs/Codec;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/Codec;->segmentInfoFormat()Lorg/apache/lucene/codecs/SegmentInfoFormat;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/SegmentInfoFormat;->getSegmentInfoWriter()Lorg/apache/lucene/codecs/SegmentInfoWriter;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->directory:Lorg/apache/lucene/store/TrackingDirectoryWrapper;

    iget-object v2, v7, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v10, p1, Lorg/apache/lucene/index/DocumentsWriterPerThread$FlushedSegment;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v0, v1, v2, v10, v5}, Lorg/apache/lucene/codecs/SegmentInfoWriter;->write(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/store/IOContext;)V

    .line 583
    iget-object v0, p1, Lorg/apache/lucene/index/DocumentsWriterPerThread$FlushedSegment;->liveDocs:Lorg/apache/lucene/util/MutableBits;

    if-eqz v0, :cond_6

    .line 584
    iget v4, p1, Lorg/apache/lucene/index/DocumentsWriterPerThread$FlushedSegment;->delCount:I

    .line 585
    .local v4, "delCount":I
    sget-boolean v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->$assertionsDisabled:Z

    if-nez v0, :cond_4

    if-gtz v4, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 607
    .end local v4    # "delCount":I
    :catchall_0
    move-exception v0

    .line 608
    if-nez v9, :cond_3

    .line 609
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v2, "DWPT"

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 610
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v2, "DWPT"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "hit exception reating compound file for newly flushed segment "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 611
    iget-object v11, v7, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v11, v11, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 610
    invoke-virtual {v1, v2, v10}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 613
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->writer:Lorg/apache/lucene/index/IndexWriter;

    iget-object v2, v7, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/index/IndexWriter;->flushFailed(Lorg/apache/lucene/index/SegmentInfo;)V

    .line 615
    :cond_3
    throw v0

    .line 586
    .restart local v4    # "delCount":I
    :cond_4
    :try_start_1
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v1, "DWPT"

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 587
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v1, "DWPT"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v10, "flush: write "

    invoke-direct {v2, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v10, " deletes gen="

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v10, p1, Lorg/apache/lucene/index/DocumentsWriterPerThread$FlushedSegment;->segmentInfo:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    invoke-virtual {v10}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->getDelGen()J

    move-result-wide v10

    invoke-virtual {v2, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 599
    :cond_5
    iget-object v3, p1, Lorg/apache/lucene/index/DocumentsWriterPerThread$FlushedSegment;->segmentInfo:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .line 600
    .local v3, "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    iget-object v0, v3, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentInfo;->getCodec()Lorg/apache/lucene/codecs/Codec;

    move-result-object v6

    .line 601
    .local v6, "codec":Lorg/apache/lucene/codecs/Codec;
    invoke-virtual {v6}, Lorg/apache/lucene/codecs/Codec;->liveDocsFormat()Lorg/apache/lucene/codecs/LiveDocsFormat;

    move-result-object v0

    iget-object v1, p1, Lorg/apache/lucene/index/DocumentsWriterPerThread$FlushedSegment;->liveDocs:Lorg/apache/lucene/util/MutableBits;

    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->directory:Lorg/apache/lucene/store/TrackingDirectoryWrapper;

    invoke-virtual/range {v0 .. v5}, Lorg/apache/lucene/codecs/LiveDocsFormat;->writeLiveDocs(Lorg/apache/lucene/util/MutableBits;Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfoPerCommit;ILorg/apache/lucene/store/IOContext;)V

    .line 602
    invoke-virtual {v7, v4}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->setDelCount(I)V

    .line 603
    invoke-virtual {v7}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->advanceDelGen()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 606
    .end local v3    # "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .end local v4    # "delCount":I
    .end local v6    # "codec":Lorg/apache/lucene/codecs/Codec;
    :cond_6
    const/4 v9, 0x1

    .line 608
    if-nez v9, :cond_8

    .line 609
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v1, "DWPT"

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 610
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v1, "DWPT"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v10, "hit exception reating compound file for newly flushed segment "

    invoke-direct {v2, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 611
    iget-object v10, v7, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v10, v10, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 610
    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 613
    :cond_7
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->writer:Lorg/apache/lucene/index/IndexWriter;

    iget-object v1, v7, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/IndexWriter;->flushFailed(Lorg/apache/lucene/index/SegmentInfo;)V

    .line 616
    :cond_8
    return-void
.end method

.method setAborting()V
    .locals 1

    .prologue
    .line 232
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->aborting:Z

    .line 233
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 662
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "DocumentsWriterPerThread [pendingDeletes="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->pendingDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 663
    const-string v1, ", segment="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", aborting="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->aborting:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", numDocsInRAM="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 664
    iget v1, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->numDocsInRAM:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", deleteQueue="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->deleteQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 662
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 663
    :cond_0
    const-string v0, "null"

    goto :goto_0
.end method

.method public updateDocument(Ljava/lang/Iterable;Lorg/apache/lucene/analysis/Analyzer;Lorg/apache/lucene/index/Term;)V
    .locals 3
    .param p2, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;
    .param p3, "delTerm"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lorg/apache/lucene/index/IndexableField;",
            ">;",
            "Lorg/apache/lucene/analysis/Analyzer;",
            "Lorg/apache/lucene/index/Term;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 242
    .local p1, "doc":Ljava/lang/Iterable;, "Ljava/lang/Iterable<+Lorg/apache/lucene/index/IndexableField;>;"
    sget-boolean v1, Lorg/apache/lucene/index/DocumentsWriterPerThread;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->writer:Lorg/apache/lucene/index/IndexWriter;

    const-string v2, "DocumentsWriterPerThread addDocument start"

    invoke-virtual {v1, v2}, Lorg/apache/lucene/index/IndexWriter;->testPoint(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 243
    :cond_0
    sget-boolean v1, Lorg/apache/lucene/index/DocumentsWriterPerThread;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->deleteQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 244
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    iput-object p1, v1, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;->doc:Ljava/lang/Iterable;

    .line 245
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    iput-object p2, v1, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    .line 246
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    iget v2, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->numDocsInRAM:I

    iput v2, v1, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;->docID:I

    .line 247
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    if-nez v1, :cond_2

    .line 248
    invoke-direct {p0}, Lorg/apache/lucene/index/DocumentsWriterPerThread;->initSegmentInfo()V

    .line 253
    :cond_2
    const/4 v0, 0x0

    .line 256
    .local v0, "success":Z
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->consumer:Lorg/apache/lucene/index/DocConsumer;

    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->fieldInfos:Lorg/apache/lucene/index/FieldInfos$Builder;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/index/DocConsumer;->processDocument(Lorg/apache/lucene/index/FieldInfos$Builder;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 258
    :try_start_1
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    invoke-virtual {v1}, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 260
    const/4 v0, 0x1

    .line 262
    if-nez v0, :cond_3

    .line 263
    iget-boolean v1, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->aborting:Z

    if-nez v1, :cond_7

    .line 265
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    iget v1, v1, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;->docID:I

    invoke-virtual {p0, v1}, Lorg/apache/lucene/index/DocumentsWriterPerThread;->deleteDocID(I)V

    .line 266
    iget v1, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->numDocsInRAM:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->numDocsInRAM:I

    .line 272
    :cond_3
    :goto_0
    const/4 v0, 0x0

    .line 274
    :try_start_2
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->consumer:Lorg/apache/lucene/index/DocConsumer;

    invoke-virtual {v1}, Lorg/apache/lucene/index/DocConsumer;->finishDocument()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 275
    const/4 v0, 0x1

    .line 277
    if-nez v0, :cond_4

    .line 278
    invoke-virtual {p0}, Lorg/apache/lucene/index/DocumentsWriterPerThread;->abort()V

    .line 281
    :cond_4
    invoke-direct {p0, p3}, Lorg/apache/lucene/index/DocumentsWriterPerThread;->finishDocument(Lorg/apache/lucene/index/Term;)V

    .line 282
    return-void

    .line 257
    :catchall_0
    move-exception v1

    .line 258
    :try_start_3
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    invoke-virtual {v2}, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;->clear()V

    .line 259
    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 261
    :catchall_1
    move-exception v1

    .line 262
    if-nez v0, :cond_5

    .line 263
    iget-boolean v2, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->aborting:Z

    if-nez v2, :cond_6

    .line 265
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    iget v2, v2, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;->docID:I

    invoke-virtual {p0, v2}, Lorg/apache/lucene/index/DocumentsWriterPerThread;->deleteDocID(I)V

    .line 266
    iget v2, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->numDocsInRAM:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->numDocsInRAM:I

    .line 271
    :cond_5
    :goto_1
    throw v1

    .line 268
    :cond_6
    invoke-virtual {p0}, Lorg/apache/lucene/index/DocumentsWriterPerThread;->abort()V

    goto :goto_1

    :cond_7
    invoke-virtual {p0}, Lorg/apache/lucene/index/DocumentsWriterPerThread;->abort()V

    goto :goto_0

    .line 276
    :catchall_2
    move-exception v1

    .line 277
    if-nez v0, :cond_8

    .line 278
    invoke-virtual {p0}, Lorg/apache/lucene/index/DocumentsWriterPerThread;->abort()V

    .line 280
    :cond_8
    throw v1
.end method

.method public updateDocuments(Ljava/lang/Iterable;Lorg/apache/lucene/analysis/Analyzer;Lorg/apache/lucene/index/Term;)I
    .locals 9
    .param p2, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;
    .param p3, "delTerm"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Ljava/lang/Iterable",
            "<+",
            "Lorg/apache/lucene/index/IndexableField;",
            ">;>;",
            "Lorg/apache/lucene/analysis/Analyzer;",
            "Lorg/apache/lucene/index/Term;",
            ")I"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 295
    .local p1, "docs":Ljava/lang/Iterable;, "Ljava/lang/Iterable<+Ljava/lang/Iterable<+Lorg/apache/lucene/index/IndexableField;>;>;"
    sget-boolean v6, Lorg/apache/lucene/index/DocumentsWriterPerThread;->$assertionsDisabled:Z

    if-nez v6, :cond_0

    iget-object v6, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->writer:Lorg/apache/lucene/index/IndexWriter;

    const-string v7, "DocumentsWriterPerThread addDocuments start"

    invoke-virtual {v6, v7}, Lorg/apache/lucene/index/IndexWriter;->testPoint(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 296
    :cond_0
    sget-boolean v6, Lorg/apache/lucene/index/DocumentsWriterPerThread;->$assertionsDisabled:Z

    if-nez v6, :cond_1

    iget-object v6, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->deleteQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    if-nez v6, :cond_1

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 297
    :cond_1
    iget-object v6, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    iput-object p2, v6, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    .line 298
    iget-object v6, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    if-nez v6, :cond_2

    .line 299
    invoke-direct {p0}, Lorg/apache/lucene/index/DocumentsWriterPerThread;->initSegmentInfo()V

    .line 304
    :cond_2
    const/4 v2, 0x0

    .line 305
    .local v2, "docCount":I
    const/4 v0, 0x0

    .line 307
    .local v0, "allDocsIndexed":Z
    :try_start_0
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_4

    .line 340
    const/4 v0, 0x1

    .line 345
    if-eqz p3, :cond_c

    .line 346
    iget-object v6, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->deleteQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    iget-object v7, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->deleteSlice:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;

    invoke-virtual {v6, p3, v7}, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->add(Lorg/apache/lucene/index/Term;Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;)V

    .line 347
    sget-boolean v6, Lorg/apache/lucene/index/DocumentsWriterPerThread;->$assertionsDisabled:Z

    if-nez v6, :cond_b

    iget-object v6, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->deleteSlice:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;

    invoke-virtual {v6, p3}, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;->isTailItem(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_b

    new-instance v6, Ljava/lang/AssertionError;

    const-string v7, "expected the delete term as the tail item"

    invoke-direct {v6, v7}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 351
    :catchall_0
    move-exception v6

    .line 352
    if-nez v0, :cond_3

    iget-boolean v7, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->aborting:Z

    if-nez v7, :cond_3

    .line 355
    iget v7, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->numDocsInRAM:I

    add-int/lit8 v3, v7, -0x1

    .line 356
    .local v3, "docID":I
    sub-int v4, v3, v2

    .line 357
    .local v4, "endDocID":I
    :goto_1
    if-gt v3, v4, :cond_e

    .line 362
    .end local v3    # "docID":I
    .end local v4    # "endDocID":I
    :cond_3
    iget-object v7, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    invoke-virtual {v7}, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;->clear()V

    .line 363
    throw v6

    .line 307
    :cond_4
    :try_start_1
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 308
    .local v1, "doc":Ljava/lang/Iterable;, "Ljava/lang/Iterable<+Lorg/apache/lucene/index/IndexableField;>;"
    iget-object v7, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    iput-object v1, v7, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;->doc:Ljava/lang/Iterable;

    .line 309
    iget-object v7, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    iget v8, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->numDocsInRAM:I

    iput v8, v7, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;->docID:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 310
    add-int/lit8 v2, v2, 0x1

    .line 312
    const/4 v5, 0x0

    .line 314
    .local v5, "success":Z
    :try_start_2
    iget-object v7, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->consumer:Lorg/apache/lucene/index/DocConsumer;

    iget-object v8, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->fieldInfos:Lorg/apache/lucene/index/FieldInfos$Builder;

    invoke-virtual {v7, v8}, Lorg/apache/lucene/index/DocConsumer;->processDocument(Lorg/apache/lucene/index/FieldInfos$Builder;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 315
    const/4 v5, 0x1

    .line 317
    if-nez v5, :cond_5

    .line 319
    :try_start_3
    iget-boolean v7, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->aborting:Z

    if-nez v7, :cond_9

    .line 322
    iget v7, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->numDocsInRAM:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->numDocsInRAM:I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 328
    :cond_5
    :goto_2
    const/4 v5, 0x0

    .line 330
    :try_start_4
    iget-object v7, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->consumer:Lorg/apache/lucene/index/DocConsumer;

    invoke-virtual {v7}, Lorg/apache/lucene/index/DocConsumer;->finishDocument()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 331
    const/4 v5, 0x1

    .line 333
    if-nez v5, :cond_6

    .line 334
    :try_start_5
    invoke-virtual {p0}, Lorg/apache/lucene/index/DocumentsWriterPerThread;->abort()V

    .line 338
    :cond_6
    const/4 v7, 0x0

    invoke-direct {p0, v7}, Lorg/apache/lucene/index/DocumentsWriterPerThread;->finishDocument(Lorg/apache/lucene/index/Term;)V

    goto :goto_0

    .line 316
    :catchall_1
    move-exception v6

    .line 317
    if-nez v5, :cond_7

    .line 319
    iget-boolean v7, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->aborting:Z

    if-nez v7, :cond_8

    .line 322
    iget v7, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->numDocsInRAM:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->numDocsInRAM:I

    .line 327
    :cond_7
    :goto_3
    throw v6

    .line 324
    :cond_8
    invoke-virtual {p0}, Lorg/apache/lucene/index/DocumentsWriterPerThread;->abort()V

    goto :goto_3

    :cond_9
    invoke-virtual {p0}, Lorg/apache/lucene/index/DocumentsWriterPerThread;->abort()V

    goto :goto_2

    .line 332
    :catchall_2
    move-exception v6

    .line 333
    if-nez v5, :cond_a

    .line 334
    invoke-virtual {p0}, Lorg/apache/lucene/index/DocumentsWriterPerThread;->abort()V

    .line 336
    :cond_a
    throw v6

    .line 348
    .end local v1    # "doc":Ljava/lang/Iterable;, "Ljava/lang/Iterable<+Lorg/apache/lucene/index/IndexableField;>;"
    .end local v5    # "success":Z
    :cond_b
    iget-object v6, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->deleteSlice:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;

    iget-object v7, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->pendingDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    iget v8, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->numDocsInRAM:I

    sub-int/2addr v8, v2

    invoke-virtual {v6, v7, v8}, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;->apply(Lorg/apache/lucene/index/BufferedDeletes;I)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 352
    :cond_c
    if-nez v0, :cond_d

    iget-boolean v6, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->aborting:Z

    if-nez v6, :cond_d

    .line 355
    iget v6, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->numDocsInRAM:I

    add-int/lit8 v3, v6, -0x1

    .line 356
    .restart local v3    # "docID":I
    sub-int v4, v3, v2

    .line 357
    .restart local v4    # "endDocID":I
    :goto_4
    if-gt v3, v4, :cond_f

    .line 362
    .end local v3    # "docID":I
    .end local v4    # "endDocID":I
    :cond_d
    iget-object v6, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    invoke-virtual {v6}, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;->clear()V

    .line 365
    return v2

    .line 358
    .restart local v3    # "docID":I
    .restart local v4    # "endDocID":I
    :cond_e
    invoke-virtual {p0, v3}, Lorg/apache/lucene/index/DocumentsWriterPerThread;->deleteDocID(I)V

    .line 359
    add-int/lit8 v3, v3, -0x1

    goto :goto_1

    .line 358
    :cond_f
    invoke-virtual {p0, v3}, Lorg/apache/lucene/index/DocumentsWriterPerThread;->deleteDocID(I)V

    .line 359
    add-int/lit8 v3, v3, -0x1

    goto :goto_4
.end method
