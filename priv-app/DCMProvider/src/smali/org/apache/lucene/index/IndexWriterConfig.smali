.class public final Lorg/apache/lucene/index/IndexWriterConfig;
.super Lorg/apache/lucene/index/LiveIndexWriterConfig;
.source "IndexWriterConfig.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;
    }
.end annotation


# static fields
.field public static final DEFAULT_MAX_BUFFERED_DELETE_TERMS:I = -0x1

.field public static final DEFAULT_MAX_BUFFERED_DOCS:I = -0x1

.field public static final DEFAULT_MAX_THREAD_STATES:I = 0x8

.field public static final DEFAULT_RAM_BUFFER_SIZE_MB:D = 16.0

.field public static final DEFAULT_RAM_PER_THREAD_HARD_LIMIT_MB:I = 0x799

.field public static final DEFAULT_READER_POOLING:Z = false

.field public static final DEFAULT_READER_TERMS_INDEX_DIVISOR:I = 0x1

.field public static final DEFAULT_TERM_INDEX_INTERVAL:I = 0x20

.field public static final DISABLE_AUTO_FLUSH:I = -0x1

.field public static WRITE_LOCK_TIMEOUT:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 96
    const-wide/16 v0, 0x3e8

    sput-wide v0, Lorg/apache/lucene/index/IndexWriterConfig;->WRITE_LOCK_TIMEOUT:J

    .line 111
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/Analyzer;)V
    .locals 0
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;

    .prologue
    .line 144
    invoke-direct {p0, p2, p1}, Lorg/apache/lucene/index/LiveIndexWriterConfig;-><init>(Lorg/apache/lucene/analysis/Analyzer;Lorg/apache/lucene/util/Version;)V

    .line 145
    return-void
.end method

.method public static getDefaultWriteLockTimeout()J
    .locals 2

    .prologue
    .line 128
    sget-wide v0, Lorg/apache/lucene/index/IndexWriterConfig;->WRITE_LOCK_TIMEOUT:J

    return-wide v0
.end method

.method public static setDefaultWriteLockTimeout(J)V
    .locals 0
    .param p0, "writeLockTimeout"    # J

    .prologue
    .line 118
    sput-wide p0, Lorg/apache/lucene/index/IndexWriterConfig;->WRITE_LOCK_TIMEOUT:J

    .line 119
    return-void
.end method


# virtual methods
.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriterConfig;->clone()Lorg/apache/lucene/index/IndexWriterConfig;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lorg/apache/lucene/index/IndexWriterConfig;
    .locals 3

    .prologue
    .line 150
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/IndexWriterConfig;

    .line 155
    .local v0, "clone":Lorg/apache/lucene/index/IndexWriterConfig;
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriterConfig;->delPolicy:Lorg/apache/lucene/index/IndexDeletionPolicy;

    invoke-virtual {v2}, Lorg/apache/lucene/index/IndexDeletionPolicy;->clone()Lorg/apache/lucene/index/IndexDeletionPolicy;

    move-result-object v2

    iput-object v2, v0, Lorg/apache/lucene/index/IndexWriterConfig;->delPolicy:Lorg/apache/lucene/index/IndexDeletionPolicy;

    .line 156
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriterConfig;->flushPolicy:Lorg/apache/lucene/index/FlushPolicy;

    invoke-virtual {v2}, Lorg/apache/lucene/index/FlushPolicy;->clone()Lorg/apache/lucene/index/FlushPolicy;

    move-result-object v2

    iput-object v2, v0, Lorg/apache/lucene/index/IndexWriterConfig;->flushPolicy:Lorg/apache/lucene/index/FlushPolicy;

    .line 157
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriterConfig;->indexerThreadPool:Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;

    invoke-virtual {v2}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->clone()Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;

    move-result-object v2

    iput-object v2, v0, Lorg/apache/lucene/index/IndexWriterConfig;->indexerThreadPool:Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;

    .line 160
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriterConfig;->infoStream:Lorg/apache/lucene/util/InfoStream;

    invoke-virtual {v2}, Lorg/apache/lucene/util/InfoStream;->clone()Lorg/apache/lucene/util/InfoStream;

    move-result-object v2

    iput-object v2, v0, Lorg/apache/lucene/index/IndexWriterConfig;->infoStream:Lorg/apache/lucene/util/InfoStream;

    .line 161
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriterConfig;->mergePolicy:Lorg/apache/lucene/index/MergePolicy;

    invoke-virtual {v2}, Lorg/apache/lucene/index/MergePolicy;->clone()Lorg/apache/lucene/index/MergePolicy;

    move-result-object v2

    iput-object v2, v0, Lorg/apache/lucene/index/IndexWriterConfig;->mergePolicy:Lorg/apache/lucene/index/MergePolicy;

    .line 162
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriterConfig;->mergeScheduler:Lorg/apache/lucene/index/MergeScheduler;

    invoke-virtual {v2}, Lorg/apache/lucene/index/MergeScheduler;->clone()Lorg/apache/lucene/index/MergeScheduler;

    move-result-object v2

    iput-object v2, v0, Lorg/apache/lucene/index/IndexWriterConfig;->mergeScheduler:Lorg/apache/lucene/index/MergeScheduler;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 164
    return-object v0

    .line 165
    .end local v0    # "clone":Lorg/apache/lucene/index/IndexWriterConfig;
    :catch_0
    move-exception v1

    .line 166
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method public getAnalyzer()Lorg/apache/lucene/analysis/Analyzer;
    .locals 1

    .prologue
    .line 461
    invoke-super {p0}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getAnalyzer()Lorg/apache/lucene/analysis/Analyzer;

    move-result-object v0

    return-object v0
.end method

.method public getCodec()Lorg/apache/lucene/codecs/Codec;
    .locals 1

    .prologue
    .line 316
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriterConfig;->codec:Lorg/apache/lucene/codecs/Codec;

    return-object v0
.end method

.method getFlushPolicy()Lorg/apache/lucene/index/FlushPolicy;
    .locals 1

    .prologue
    .line 451
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriterConfig;->flushPolicy:Lorg/apache/lucene/index/FlushPolicy;

    return-object v0
.end method

.method public getIndexCommit()Lorg/apache/lucene/index/IndexCommit;
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriterConfig;->commit:Lorg/apache/lucene/index/IndexCommit;

    return-object v0
.end method

.method public getIndexDeletionPolicy()Lorg/apache/lucene/index/IndexDeletionPolicy;
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriterConfig;->delPolicy:Lorg/apache/lucene/index/IndexDeletionPolicy;

    return-object v0
.end method

.method getIndexerThreadPool()Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;
    .locals 1

    .prologue
    .line 349
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriterConfig;->indexerThreadPool:Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;

    return-object v0
.end method

.method getIndexingChain()Lorg/apache/lucene/index/DocumentsWriterPerThread$IndexingChain;
    .locals 1

    .prologue
    .line 406
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriterConfig;->indexingChain:Lorg/apache/lucene/index/DocumentsWriterPerThread$IndexingChain;

    return-object v0
.end method

.method public getInfoStream()Lorg/apache/lucene/util/InfoStream;
    .locals 1

    .prologue
    .line 456
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriterConfig;->infoStream:Lorg/apache/lucene/util/InfoStream;

    return-object v0
.end method

.method public getMaxBufferedDeleteTerms()I
    .locals 1

    .prologue
    .line 466
    invoke-super {p0}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getMaxBufferedDeleteTerms()I

    move-result v0

    return v0
.end method

.method public getMaxBufferedDocs()I
    .locals 1

    .prologue
    .line 471
    invoke-super {p0}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getMaxBufferedDocs()I

    move-result v0

    return v0
.end method

.method public getMaxThreadStates()I
    .locals 2

    .prologue
    .line 367
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriterConfig;->indexerThreadPool:Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;

    check-cast v1, Lorg/apache/lucene/index/ThreadAffinityDocumentsWriterThreadPool;

    invoke-virtual {v1}, Lorg/apache/lucene/index/ThreadAffinityDocumentsWriterThreadPool;->getMaxThreadStates()I
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    return v1

    .line 368
    :catch_0
    move-exception v0

    .line 369
    .local v0, "cce":Ljava/lang/ClassCastException;
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public getMergePolicy()Lorg/apache/lucene/index/MergePolicy;
    .locals 1

    .prologue
    .line 322
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriterConfig;->mergePolicy:Lorg/apache/lucene/index/MergePolicy;

    return-object v0
.end method

.method public getMergeScheduler()Lorg/apache/lucene/index/MergeScheduler;
    .locals 1

    .prologue
    .line 266
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriterConfig;->mergeScheduler:Lorg/apache/lucene/index/MergeScheduler;

    return-object v0
.end method

.method public getMergedSegmentWarmer()Lorg/apache/lucene/index/IndexWriter$IndexReaderWarmer;
    .locals 1

    .prologue
    .line 476
    invoke-super {p0}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getMergedSegmentWarmer()Lorg/apache/lucene/index/IndexWriter$IndexReaderWarmer;

    move-result-object v0

    return-object v0
.end method

.method public getOpenMode()Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriterConfig;->openMode:Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;

    return-object v0
.end method

.method public getRAMBufferSizeMB()D
    .locals 2

    .prologue
    .line 481
    invoke-super {p0}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getRAMBufferSizeMB()D

    move-result-wide v0

    return-wide v0
.end method

.method public getRAMPerThreadHardLimitMB()I
    .locals 1

    .prologue
    .line 446
    iget v0, p0, Lorg/apache/lucene/index/IndexWriterConfig;->perThreadHardLimitMB:I

    return v0
.end method

.method public getReaderPooling()Z
    .locals 1

    .prologue
    .line 390
    iget-boolean v0, p0, Lorg/apache/lucene/index/IndexWriterConfig;->readerPooling:Z

    return v0
.end method

.method public getReaderTermsIndexDivisor()I
    .locals 1

    .prologue
    .line 486
    invoke-super {p0}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getReaderTermsIndexDivisor()I

    move-result v0

    return v0
.end method

.method public getSimilarity()Lorg/apache/lucene/search/similarities/Similarity;
    .locals 1

    .prologue
    .line 246
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriterConfig;->similarity:Lorg/apache/lucene/search/similarities/Similarity;

    return-object v0
.end method

.method public getTermIndexInterval()I
    .locals 1

    .prologue
    .line 491
    invoke-super {p0}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getTermIndexInterval()I

    move-result v0

    return v0
.end method

.method public getWriteLockTimeout()J
    .locals 2

    .prologue
    .line 282
    iget-wide v0, p0, Lorg/apache/lucene/index/IndexWriterConfig;->writeLockTimeout:J

    return-wide v0
.end method

.method public setCodec(Lorg/apache/lucene/codecs/Codec;)Lorg/apache/lucene/index/IndexWriterConfig;
    .locals 2
    .param p1, "codec"    # Lorg/apache/lucene/codecs/Codec;

    .prologue
    .line 307
    if-nez p1, :cond_0

    .line 308
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "codec must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 310
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/index/IndexWriterConfig;->codec:Lorg/apache/lucene/codecs/Codec;

    .line 311
    return-object p0
.end method

.method setFlushPolicy(Lorg/apache/lucene/index/FlushPolicy;)Lorg/apache/lucene/index/IndexWriterConfig;
    .locals 2
    .param p1, "flushPolicy"    # Lorg/apache/lucene/index/FlushPolicy;

    .prologue
    .line 418
    if-nez p1, :cond_0

    .line 419
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "flushPolicy must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 421
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/index/IndexWriterConfig;->flushPolicy:Lorg/apache/lucene/index/FlushPolicy;

    .line 422
    return-object p0
.end method

.method public setIndexCommit(Lorg/apache/lucene/index/IndexCommit;)Lorg/apache/lucene/index/IndexWriterConfig;
    .locals 0
    .param p1, "commit"    # Lorg/apache/lucene/index/IndexCommit;

    .prologue
    .line 221
    iput-object p1, p0, Lorg/apache/lucene/index/IndexWriterConfig;->commit:Lorg/apache/lucene/index/IndexCommit;

    .line 222
    return-object p0
.end method

.method public setIndexDeletionPolicy(Lorg/apache/lucene/index/IndexDeletionPolicy;)Lorg/apache/lucene/index/IndexWriterConfig;
    .locals 2
    .param p1, "delPolicy"    # Lorg/apache/lucene/index/IndexDeletionPolicy;

    .prologue
    .line 203
    if-nez p1, :cond_0

    .line 204
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "indexDeletionPolicy must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 206
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/index/IndexWriterConfig;->delPolicy:Lorg/apache/lucene/index/IndexDeletionPolicy;

    .line 207
    return-object p0
.end method

.method setIndexerThreadPool(Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;)Lorg/apache/lucene/index/IndexWriterConfig;
    .locals 2
    .param p1, "threadPool"    # Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;

    .prologue
    .line 340
    if-nez p1, :cond_0

    .line 341
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "threadPool must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 343
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/index/IndexWriterConfig;->indexerThreadPool:Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;

    .line 344
    return-object p0
.end method

.method setIndexingChain(Lorg/apache/lucene/index/DocumentsWriterPerThread$IndexingChain;)Lorg/apache/lucene/index/IndexWriterConfig;
    .locals 2
    .param p1, "indexingChain"    # Lorg/apache/lucene/index/DocumentsWriterPerThread$IndexingChain;

    .prologue
    .line 397
    if-nez p1, :cond_0

    .line 398
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "indexingChain must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 400
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/index/IndexWriterConfig;->indexingChain:Lorg/apache/lucene/index/DocumentsWriterPerThread$IndexingChain;

    .line 401
    return-object p0
.end method

.method public setInfoStream(Ljava/io/PrintStream;)Lorg/apache/lucene/index/IndexWriterConfig;
    .locals 2
    .param p1, "printStream"    # Ljava/io/PrintStream;

    .prologue
    .line 509
    if-nez p1, :cond_0

    .line 510
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "printStream must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 512
    :cond_0
    new-instance v0, Lorg/apache/lucene/util/PrintStreamInfoStream;

    invoke-direct {v0, p1}, Lorg/apache/lucene/util/PrintStreamInfoStream;-><init>(Ljava/io/PrintStream;)V

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexWriterConfig;->setInfoStream(Lorg/apache/lucene/util/InfoStream;)Lorg/apache/lucene/index/IndexWriterConfig;

    move-result-object v0

    return-object v0
.end method

.method public setInfoStream(Lorg/apache/lucene/util/InfoStream;)Lorg/apache/lucene/index/IndexWriterConfig;
    .locals 2
    .param p1, "infoStream"    # Lorg/apache/lucene/util/InfoStream;

    .prologue
    .line 499
    if-nez p1, :cond_0

    .line 500
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot set InfoStream implementation to null. To disable logging use InfoStream.NO_OUTPUT"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 503
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/index/IndexWriterConfig;->infoStream:Lorg/apache/lucene/util/InfoStream;

    .line 504
    return-object p0
.end method

.method public setMaxBufferedDeleteTerms(I)Lorg/apache/lucene/index/IndexWriterConfig;
    .locals 1
    .param p1, "maxBufferedDeleteTerms"    # I

    .prologue
    .line 517
    invoke-super {p0, p1}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->setMaxBufferedDeleteTerms(I)Lorg/apache/lucene/index/LiveIndexWriterConfig;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/IndexWriterConfig;

    return-object v0
.end method

.method public bridge synthetic setMaxBufferedDeleteTerms(I)Lorg/apache/lucene/index/LiveIndexWriterConfig;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/IndexWriterConfig;->setMaxBufferedDeleteTerms(I)Lorg/apache/lucene/index/IndexWriterConfig;

    move-result-object v0

    return-object v0
.end method

.method public setMaxBufferedDocs(I)Lorg/apache/lucene/index/IndexWriterConfig;
    .locals 1
    .param p1, "maxBufferedDocs"    # I

    .prologue
    .line 522
    invoke-super {p0, p1}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->setMaxBufferedDocs(I)Lorg/apache/lucene/index/LiveIndexWriterConfig;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/IndexWriterConfig;

    return-object v0
.end method

.method public bridge synthetic setMaxBufferedDocs(I)Lorg/apache/lucene/index/LiveIndexWriterConfig;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/IndexWriterConfig;->setMaxBufferedDocs(I)Lorg/apache/lucene/index/IndexWriterConfig;

    move-result-object v0

    return-object v0
.end method

.method public setMaxThreadStates(I)Lorg/apache/lucene/index/IndexWriterConfig;
    .locals 1
    .param p1, "maxThreadStates"    # I

    .prologue
    .line 360
    new-instance v0, Lorg/apache/lucene/index/ThreadAffinityDocumentsWriterThreadPool;

    invoke-direct {v0, p1}, Lorg/apache/lucene/index/ThreadAffinityDocumentsWriterThreadPool;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriterConfig;->indexerThreadPool:Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;

    .line 361
    return-object p0
.end method

.method public setMergePolicy(Lorg/apache/lucene/index/MergePolicy;)Lorg/apache/lucene/index/IndexWriterConfig;
    .locals 2
    .param p1, "mergePolicy"    # Lorg/apache/lucene/index/MergePolicy;

    .prologue
    .line 293
    if-nez p1, :cond_0

    .line 294
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "mergePolicy must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 296
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/index/IndexWriterConfig;->mergePolicy:Lorg/apache/lucene/index/MergePolicy;

    .line 297
    return-object p0
.end method

.method public setMergeScheduler(Lorg/apache/lucene/index/MergeScheduler;)Lorg/apache/lucene/index/IndexWriterConfig;
    .locals 2
    .param p1, "mergeScheduler"    # Lorg/apache/lucene/index/MergeScheduler;

    .prologue
    .line 257
    if-nez p1, :cond_0

    .line 258
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "mergeScheduler must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 260
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/index/IndexWriterConfig;->mergeScheduler:Lorg/apache/lucene/index/MergeScheduler;

    .line 261
    return-object p0
.end method

.method public setMergedSegmentWarmer(Lorg/apache/lucene/index/IndexWriter$IndexReaderWarmer;)Lorg/apache/lucene/index/IndexWriterConfig;
    .locals 1
    .param p1, "mergeSegmentWarmer"    # Lorg/apache/lucene/index/IndexWriter$IndexReaderWarmer;

    .prologue
    .line 527
    invoke-super {p0, p1}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->setMergedSegmentWarmer(Lorg/apache/lucene/index/IndexWriter$IndexReaderWarmer;)Lorg/apache/lucene/index/LiveIndexWriterConfig;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/IndexWriterConfig;

    return-object v0
.end method

.method public bridge synthetic setMergedSegmentWarmer(Lorg/apache/lucene/index/IndexWriter$IndexReaderWarmer;)Lorg/apache/lucene/index/LiveIndexWriterConfig;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/IndexWriterConfig;->setMergedSegmentWarmer(Lorg/apache/lucene/index/IndexWriter$IndexReaderWarmer;)Lorg/apache/lucene/index/IndexWriterConfig;

    move-result-object v0

    return-object v0
.end method

.method public setOpenMode(Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;)Lorg/apache/lucene/index/IndexWriterConfig;
    .locals 2
    .param p1, "openMode"    # Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;

    .prologue
    .line 174
    if-nez p1, :cond_0

    .line 175
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "openMode must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 177
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/index/IndexWriterConfig;->openMode:Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;

    .line 178
    return-object p0
.end method

.method public setRAMBufferSizeMB(D)Lorg/apache/lucene/index/IndexWriterConfig;
    .locals 1
    .param p1, "ramBufferSizeMB"    # D

    .prologue
    .line 532
    invoke-super {p0, p1, p2}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->setRAMBufferSizeMB(D)Lorg/apache/lucene/index/LiveIndexWriterConfig;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/IndexWriterConfig;

    return-object v0
.end method

.method public bridge synthetic setRAMBufferSizeMB(D)Lorg/apache/lucene/index/LiveIndexWriterConfig;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/index/IndexWriterConfig;->setRAMBufferSizeMB(D)Lorg/apache/lucene/index/IndexWriterConfig;

    move-result-object v0

    return-object v0
.end method

.method public setRAMPerThreadHardLimitMB(I)Lorg/apache/lucene/index/IndexWriterConfig;
    .locals 2
    .param p1, "perThreadHardLimitMB"    # I

    .prologue
    .line 437
    if-lez p1, :cond_0

    const/16 v0, 0x800

    if-lt p1, v0, :cond_1

    .line 438
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "PerThreadHardLimit must be greater than 0 and less than 2048MB"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 440
    :cond_1
    iput p1, p0, Lorg/apache/lucene/index/IndexWriterConfig;->perThreadHardLimitMB:I

    .line 441
    return-object p0
.end method

.method public setReaderPooling(Z)Lorg/apache/lucene/index/IndexWriterConfig;
    .locals 0
    .param p1, "readerPooling"    # Z

    .prologue
    .line 384
    iput-boolean p1, p0, Lorg/apache/lucene/index/IndexWriterConfig;->readerPooling:Z

    .line 385
    return-object p0
.end method

.method public setReaderTermsIndexDivisor(I)Lorg/apache/lucene/index/IndexWriterConfig;
    .locals 1
    .param p1, "divisor"    # I

    .prologue
    .line 537
    invoke-super {p0, p1}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->setReaderTermsIndexDivisor(I)Lorg/apache/lucene/index/LiveIndexWriterConfig;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/IndexWriterConfig;

    return-object v0
.end method

.method public bridge synthetic setReaderTermsIndexDivisor(I)Lorg/apache/lucene/index/LiveIndexWriterConfig;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/IndexWriterConfig;->setReaderTermsIndexDivisor(I)Lorg/apache/lucene/index/IndexWriterConfig;

    move-result-object v0

    return-object v0
.end method

.method public setSimilarity(Lorg/apache/lucene/search/similarities/Similarity;)Lorg/apache/lucene/index/IndexWriterConfig;
    .locals 2
    .param p1, "similarity"    # Lorg/apache/lucene/search/similarities/Similarity;

    .prologue
    .line 237
    if-nez p1, :cond_0

    .line 238
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "similarity must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 240
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/index/IndexWriterConfig;->similarity:Lorg/apache/lucene/search/similarities/Similarity;

    .line 241
    return-object p0
.end method

.method public setTermIndexInterval(I)Lorg/apache/lucene/index/IndexWriterConfig;
    .locals 1
    .param p1, "interval"    # I

    .prologue
    .line 542
    invoke-super {p0, p1}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->setTermIndexInterval(I)Lorg/apache/lucene/index/LiveIndexWriterConfig;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/IndexWriterConfig;

    return-object v0
.end method

.method public bridge synthetic setTermIndexInterval(I)Lorg/apache/lucene/index/LiveIndexWriterConfig;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/IndexWriterConfig;->setTermIndexInterval(I)Lorg/apache/lucene/index/IndexWriterConfig;

    move-result-object v0

    return-object v0
.end method

.method public setWriteLockTimeout(J)Lorg/apache/lucene/index/IndexWriterConfig;
    .locals 1
    .param p1, "writeLockTimeout"    # J

    .prologue
    .line 276
    iput-wide p1, p0, Lorg/apache/lucene/index/IndexWriterConfig;->writeLockTimeout:J

    .line 277
    return-object p0
.end method
