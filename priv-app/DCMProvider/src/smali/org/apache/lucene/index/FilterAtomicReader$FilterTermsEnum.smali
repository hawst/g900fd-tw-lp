.class public Lorg/apache/lucene/index/FilterAtomicReader$FilterTermsEnum;
.super Lorg/apache/lucene/index/TermsEnum;
.source "FilterAtomicReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/FilterAtomicReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FilterTermsEnum"
.end annotation


# instance fields
.field protected final in:Lorg/apache/lucene/index/TermsEnum;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/TermsEnum;)V
    .locals 0
    .param p1, "in"    # Lorg/apache/lucene/index/TermsEnum;

    .prologue
    .line 152
    invoke-direct {p0}, Lorg/apache/lucene/index/TermsEnum;-><init>()V

    iput-object p1, p0, Lorg/apache/lucene/index/FilterAtomicReader$FilterTermsEnum;->in:Lorg/apache/lucene/index/TermsEnum;

    return-void
.end method


# virtual methods
.method public attributes()Lorg/apache/lucene/util/AttributeSource;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lorg/apache/lucene/index/FilterAtomicReader$FilterTermsEnum;->in:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermsEnum;->attributes()Lorg/apache/lucene/util/AttributeSource;

    move-result-object v0

    return-object v0
.end method

.method public docFreq()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 186
    iget-object v0, p0, Lorg/apache/lucene/index/FilterAtomicReader$FilterTermsEnum;->in:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermsEnum;->docFreq()I

    move-result v0

    return v0
.end method

.method public docs(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;I)Lorg/apache/lucene/index/DocsEnum;
    .locals 1
    .param p1, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p2, "reuse"    # Lorg/apache/lucene/index/DocsEnum;
    .param p3, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 196
    iget-object v0, p0, Lorg/apache/lucene/index/FilterAtomicReader$FilterTermsEnum;->in:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/lucene/index/TermsEnum;->docs(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;I)Lorg/apache/lucene/index/DocsEnum;

    move-result-object v0

    return-object v0
.end method

.method public docsAndPositions(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsAndPositionsEnum;I)Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .locals 1
    .param p1, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p2, "reuse"    # Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .param p3, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 201
    iget-object v0, p0, Lorg/apache/lucene/index/FilterAtomicReader$FilterTermsEnum;->in:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/lucene/index/TermsEnum;->docsAndPositions(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsAndPositionsEnum;I)Lorg/apache/lucene/index/DocsAndPositionsEnum;

    move-result-object v0

    return-object v0
.end method

.method public getComparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation

    .prologue
    .line 206
    iget-object v0, p0, Lorg/apache/lucene/index/FilterAtomicReader$FilterTermsEnum;->in:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermsEnum;->getComparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public next()Lorg/apache/lucene/util/BytesRef;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 171
    iget-object v0, p0, Lorg/apache/lucene/index/FilterAtomicReader$FilterTermsEnum;->in:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermsEnum;->next()Lorg/apache/lucene/util/BytesRef;

    move-result-object v0

    return-object v0
.end method

.method public ord()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 181
    iget-object v0, p0, Lorg/apache/lucene/index/FilterAtomicReader$FilterTermsEnum;->in:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermsEnum;->ord()J

    move-result-wide v0

    return-wide v0
.end method

.method public seekCeil(Lorg/apache/lucene/util/BytesRef;Z)Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    .locals 1
    .param p1, "text"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "useCache"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 161
    iget-object v0, p0, Lorg/apache/lucene/index/FilterAtomicReader$FilterTermsEnum;->in:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/index/TermsEnum;->seekCeil(Lorg/apache/lucene/util/BytesRef;Z)Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    move-result-object v0

    return-object v0
.end method

.method public seekExact(J)V
    .locals 1
    .param p1, "ord"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 166
    iget-object v0, p0, Lorg/apache/lucene/index/FilterAtomicReader$FilterTermsEnum;->in:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/index/TermsEnum;->seekExact(J)V

    .line 167
    return-void
.end method

.method public term()Lorg/apache/lucene/util/BytesRef;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 176
    iget-object v0, p0, Lorg/apache/lucene/index/FilterAtomicReader$FilterTermsEnum;->in:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermsEnum;->term()Lorg/apache/lucene/util/BytesRef;

    move-result-object v0

    return-object v0
.end method

.method public totalTermFreq()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 191
    iget-object v0, p0, Lorg/apache/lucene/index/FilterAtomicReader$FilterTermsEnum;->in:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermsEnum;->totalTermFreq()J

    move-result-wide v0

    return-wide v0
.end method
