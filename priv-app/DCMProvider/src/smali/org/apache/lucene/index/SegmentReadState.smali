.class public Lorg/apache/lucene/index/SegmentReadState;
.super Ljava/lang/Object;
.source "SegmentReadState.java"


# instance fields
.field public final context:Lorg/apache/lucene/store/IOContext;

.field public final directory:Lorg/apache/lucene/store/Directory;

.field public final fieldInfos:Lorg/apache/lucene/index/FieldInfos;

.field public final segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

.field public final segmentSuffix:Ljava/lang/String;

.field public termsIndexDivisor:I


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/SegmentReadState;Ljava/lang/String;)V
    .locals 1
    .param p1, "other"    # Lorg/apache/lucene/index/SegmentReadState;
    .param p2, "newSegmentSuffix"    # Ljava/lang/String;

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    iget-object v0, p1, Lorg/apache/lucene/index/SegmentReadState;->directory:Lorg/apache/lucene/store/Directory;

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentReadState;->directory:Lorg/apache/lucene/store/Directory;

    .line 88
    iget-object v0, p1, Lorg/apache/lucene/index/SegmentReadState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentReadState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    .line 89
    iget-object v0, p1, Lorg/apache/lucene/index/SegmentReadState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentReadState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    .line 90
    iget-object v0, p1, Lorg/apache/lucene/index/SegmentReadState;->context:Lorg/apache/lucene/store/IOContext;

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentReadState;->context:Lorg/apache/lucene/store/IOContext;

    .line 91
    iget v0, p1, Lorg/apache/lucene/index/SegmentReadState;->termsIndexDivisor:I

    iput v0, p0, Lorg/apache/lucene/index/SegmentReadState;->termsIndexDivisor:I

    .line 92
    iput-object p2, p0, Lorg/apache/lucene/index/SegmentReadState;->segmentSuffix:Ljava/lang/String;

    .line 93
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/store/IOContext;I)V
    .locals 7
    .param p1, "dir"    # Lorg/apache/lucene/store/Directory;
    .param p2, "info"    # Lorg/apache/lucene/index/SegmentInfo;
    .param p3, "fieldInfos"    # Lorg/apache/lucene/index/FieldInfos;
    .param p4, "context"    # Lorg/apache/lucene/store/IOContext;
    .param p5, "termsIndexDivisor"    # I

    .prologue
    .line 66
    const-string v6, ""

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/index/SegmentReadState;-><init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/store/IOContext;ILjava/lang/String;)V

    .line 67
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/store/IOContext;ILjava/lang/String;)V
    .locals 0
    .param p1, "dir"    # Lorg/apache/lucene/store/Directory;
    .param p2, "info"    # Lorg/apache/lucene/index/SegmentInfo;
    .param p3, "fieldInfos"    # Lorg/apache/lucene/index/FieldInfos;
    .param p4, "context"    # Lorg/apache/lucene/store/IOContext;
    .param p5, "termsIndexDivisor"    # I
    .param p6, "segmentSuffix"    # Ljava/lang/String;

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    iput-object p1, p0, Lorg/apache/lucene/index/SegmentReadState;->directory:Lorg/apache/lucene/store/Directory;

    .line 77
    iput-object p2, p0, Lorg/apache/lucene/index/SegmentReadState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    .line 78
    iput-object p3, p0, Lorg/apache/lucene/index/SegmentReadState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    .line 79
    iput-object p4, p0, Lorg/apache/lucene/index/SegmentReadState;->context:Lorg/apache/lucene/store/IOContext;

    .line 80
    iput p5, p0, Lorg/apache/lucene/index/SegmentReadState;->termsIndexDivisor:I

    .line 81
    iput-object p6, p0, Lorg/apache/lucene/index/SegmentReadState;->segmentSuffix:Ljava/lang/String;

    .line 82
    return-void
.end method
