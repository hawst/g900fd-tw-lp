.class abstract Lorg/apache/lucene/index/DocumentsWriterFlushQueue$FlushTicket;
.super Ljava/lang/Object;
.source "DocumentsWriterFlushQueue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/DocumentsWriterFlushQueue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x408
    name = "FlushTicket"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field protected frozenDeletes:Lorg/apache/lucene/index/FrozenBufferedDeletes;

.field protected published:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 163
    const-class v0, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/DocumentsWriterFlushQueue$FlushTicket;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected constructor <init>(Lorg/apache/lucene/index/FrozenBufferedDeletes;)V
    .locals 1
    .param p1, "frozenDeletes"    # Lorg/apache/lucene/index/FrozenBufferedDeletes;

    .prologue
    .line 167
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 165
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushQueue$FlushTicket;->published:Z

    .line 168
    sget-boolean v0, Lorg/apache/lucene/index/DocumentsWriterFlushQueue$FlushTicket;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 169
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/index/DocumentsWriterFlushQueue$FlushTicket;->frozenDeletes:Lorg/apache/lucene/index/FrozenBufferedDeletes;

    .line 170
    return-void
.end method


# virtual methods
.method protected abstract canPublish()Z
.end method

.method protected abstract publish(Lorg/apache/lucene/index/DocumentsWriter;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
