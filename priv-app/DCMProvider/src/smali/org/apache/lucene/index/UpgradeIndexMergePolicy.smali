.class public Lorg/apache/lucene/index/UpgradeIndexMergePolicy;
.super Lorg/apache/lucene/index/MergePolicy;
.source "UpgradeIndexMergePolicy.java"


# instance fields
.field protected final base:Lorg/apache/lucene/index/MergePolicy;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/MergePolicy;)V
    .locals 0
    .param p1, "base"    # Lorg/apache/lucene/index/MergePolicy;

    .prologue
    .line 59
    invoke-direct {p0}, Lorg/apache/lucene/index/MergePolicy;-><init>()V

    .line 60
    iput-object p1, p0, Lorg/apache/lucene/index/UpgradeIndexMergePolicy;->base:Lorg/apache/lucene/index/MergePolicy;

    .line 61
    return-void
.end method

.method private message(Ljava/lang/String;)V
    .locals 2
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 159
    iget-object v0, p0, Lorg/apache/lucene/index/UpgradeIndexMergePolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    invoke-virtual {v0}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/IndexWriter;

    iget-object v0, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v1, "UPGMP"

    invoke-virtual {v0, v1, p1}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    return-void
.end method

.method private verbose()Z
    .locals 3

    .prologue
    .line 154
    iget-object v1, p0, Lorg/apache/lucene/index/UpgradeIndexMergePolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    invoke-virtual {v1}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/IndexWriter;

    .line 155
    .local v0, "w":Lorg/apache/lucene/index/IndexWriter;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v2, "UPGMP"

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lorg/apache/lucene/index/UpgradeIndexMergePolicy;->base:Lorg/apache/lucene/index/MergePolicy;

    invoke-virtual {v0}, Lorg/apache/lucene/index/MergePolicy;->close()V

    .line 146
    return-void
.end method

.method public findForcedDeletesMerges(Lorg/apache/lucene/index/SegmentInfos;)Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    .locals 1
    .param p1, "segmentInfos"    # Lorg/apache/lucene/index/SegmentInfos;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 135
    iget-object v0, p0, Lorg/apache/lucene/index/UpgradeIndexMergePolicy;->base:Lorg/apache/lucene/index/MergePolicy;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/MergePolicy;->findForcedDeletesMerges(Lorg/apache/lucene/index/SegmentInfos;)Lorg/apache/lucene/index/MergePolicy$MergeSpecification;

    move-result-object v0

    return-object v0
.end method

.method public findForcedMerges(Lorg/apache/lucene/index/SegmentInfos;ILjava/util/Map;)Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    .locals 9
    .param p1, "segmentInfos"    # Lorg/apache/lucene/index/SegmentInfos;
    .param p2, "maxSegmentCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/SegmentInfos;",
            "I",
            "Ljava/util/Map",
            "<",
            "Lorg/apache/lucene/index/SegmentInfoPerCommit;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lorg/apache/lucene/index/MergePolicy$MergeSpecification;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 86
    .local p3, "segmentsToMerge":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/index/SegmentInfoPerCommit;Ljava/lang/Boolean;>;"
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 87
    .local v1, "oldSegments":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/index/SegmentInfoPerCommit;Ljava/lang/Boolean;>;"
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfos;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_3

    .line 94
    invoke-direct {p0}, Lorg/apache/lucene/index/UpgradeIndexMergePolicy;->verbose()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 95
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "findForcedMerges: segmentsToUpgrade="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lorg/apache/lucene/index/UpgradeIndexMergePolicy;->message(Ljava/lang/String;)V

    .line 98
    :cond_1
    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 99
    const/4 v4, 0x0

    .line 130
    :cond_2
    :goto_1
    return-object v4

    .line 87
    :cond_3
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .line 88
    .local v3, "si":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    invoke-interface {p3, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    .line 89
    .local v5, "v":Ljava/lang/Boolean;
    if-eqz v5, :cond_0

    invoke-virtual {p0, v3}, Lorg/apache/lucene/index/UpgradeIndexMergePolicy;->shouldUpgradeSegment(Lorg/apache/lucene/index/SegmentInfoPerCommit;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 90
    invoke-interface {v1, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 101
    .end local v3    # "si":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .end local v5    # "v":Ljava/lang/Boolean;
    :cond_4
    iget-object v6, p0, Lorg/apache/lucene/index/UpgradeIndexMergePolicy;->base:Lorg/apache/lucene/index/MergePolicy;

    invoke-virtual {v6, p1, p2, v1}, Lorg/apache/lucene/index/MergePolicy;->findForcedMerges(Lorg/apache/lucene/index/SegmentInfos;ILjava/util/Map;)Lorg/apache/lucene/index/MergePolicy$MergeSpecification;

    move-result-object v4

    .line 103
    .local v4, "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    if-eqz v4, :cond_5

    .line 107
    iget-object v6, v4, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;->merges:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_9

    .line 112
    :cond_5
    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_2

    .line 113
    invoke-direct {p0}, Lorg/apache/lucene/index/UpgradeIndexMergePolicy;->verbose()Z

    move-result v6

    if-eqz v6, :cond_6

    .line 114
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "findForcedMerges: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lorg/apache/lucene/index/UpgradeIndexMergePolicy;->base:Lorg/apache/lucene/index/MergePolicy;

    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 115
    const-string v7, " does not want to merge all old segments, merge remaining ones into new segment: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 114
    invoke-direct {p0, v6}, Lorg/apache/lucene/index/UpgradeIndexMergePolicy;->message(Ljava/lang/String;)V

    .line 117
    :cond_6
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 118
    .local v0, "newInfos":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfoPerCommit;>;"
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfos;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_7
    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_a

    .line 124
    if-nez v4, :cond_8

    .line 125
    new-instance v4, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;

    .end local v4    # "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    invoke-direct {v4}, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;-><init>()V

    .line 127
    .restart local v4    # "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    :cond_8
    new-instance v6, Lorg/apache/lucene/index/MergePolicy$OneMerge;

    invoke-direct {v6, v0}, Lorg/apache/lucene/index/MergePolicy$OneMerge;-><init>(Ljava/util/List;)V

    invoke-virtual {v4, v6}, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;->add(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    goto :goto_1

    .line 107
    .end local v0    # "newInfos":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfoPerCommit;>;"
    :cond_9
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/index/MergePolicy$OneMerge;

    .line 108
    .local v2, "om":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v7

    iget-object v8, v2, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    invoke-interface {v7, v8}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    goto :goto_2

    .line 118
    .end local v2    # "om":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    .restart local v0    # "newInfos":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfoPerCommit;>;"
    :cond_a
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .line 119
    .restart local v3    # "si":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    invoke-interface {v1, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 120
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3
.end method

.method public findMerges(Lorg/apache/lucene/index/MergePolicy$MergeTrigger;Lorg/apache/lucene/index/SegmentInfos;)Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    .locals 2
    .param p1, "mergeTrigger"    # Lorg/apache/lucene/index/MergePolicy$MergeTrigger;
    .param p2, "segmentInfos"    # Lorg/apache/lucene/index/SegmentInfos;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 80
    iget-object v0, p0, Lorg/apache/lucene/index/UpgradeIndexMergePolicy;->base:Lorg/apache/lucene/index/MergePolicy;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p2}, Lorg/apache/lucene/index/MergePolicy;->findMerges(Lorg/apache/lucene/index/MergePolicy$MergeTrigger;Lorg/apache/lucene/index/SegmentInfos;)Lorg/apache/lucene/index/MergePolicy$MergeSpecification;

    move-result-object v0

    return-object v0
.end method

.method public setIndexWriter(Lorg/apache/lucene/index/IndexWriter;)V
    .locals 1
    .param p1, "writer"    # Lorg/apache/lucene/index/IndexWriter;

    .prologue
    .line 74
    invoke-super {p0, p1}, Lorg/apache/lucene/index/MergePolicy;->setIndexWriter(Lorg/apache/lucene/index/IndexWriter;)V

    .line 75
    iget-object v0, p0, Lorg/apache/lucene/index/UpgradeIndexMergePolicy;->base:Lorg/apache/lucene/index/MergePolicy;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/MergePolicy;->setIndexWriter(Lorg/apache/lucene/index/IndexWriter;)V

    .line 76
    return-void
.end method

.method protected shouldUpgradeSegment(Lorg/apache/lucene/index/SegmentInfoPerCommit;)Z
    .locals 2
    .param p1, "si"    # Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .prologue
    .line 69
    sget-object v0, Lorg/apache/lucene/util/Constants;->LUCENE_MAIN_VERSION:Ljava/lang/String;

    iget-object v1, p1, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentInfo;->getVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 150
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "->"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/index/UpgradeIndexMergePolicy;->base:Lorg/apache/lucene/index/MergePolicy;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public useCompoundFile(Lorg/apache/lucene/index/SegmentInfos;Lorg/apache/lucene/index/SegmentInfoPerCommit;)Z
    .locals 1
    .param p1, "segments"    # Lorg/apache/lucene/index/SegmentInfos;
    .param p2, "newSegment"    # Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 140
    iget-object v0, p0, Lorg/apache/lucene/index/UpgradeIndexMergePolicy;->base:Lorg/apache/lucene/index/MergePolicy;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/index/MergePolicy;->useCompoundFile(Lorg/apache/lucene/index/SegmentInfos;Lorg/apache/lucene/index/SegmentInfoPerCommit;)Z

    move-result v0

    return v0
.end method
