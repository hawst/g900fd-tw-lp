.class final Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;
.super Ljava/lang/Object;
.source "MultiTermsEnum.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/MultiTermsEnum;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "TermsEnumWithSlice"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field public current:Lorg/apache/lucene/util/BytesRef;

.field final index:I

.field private final subSlice:Lorg/apache/lucene/index/ReaderSlice;

.field terms:Lorg/apache/lucene/index/TermsEnum;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 498
    const-class v0, Lorg/apache/lucene/index/MultiTermsEnum;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(ILorg/apache/lucene/index/ReaderSlice;)V
    .locals 3
    .param p1, "index"    # I
    .param p2, "subSlice"    # Lorg/apache/lucene/index/ReaderSlice;

    .prologue
    .line 504
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 505
    iput-object p2, p0, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->subSlice:Lorg/apache/lucene/index/ReaderSlice;

    .line 506
    iput p1, p0, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->index:I

    .line 507
    sget-boolean v0, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p2, Lorg/apache/lucene/index/ReaderSlice;->length:I

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "length="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p2, Lorg/apache/lucene/index/ReaderSlice;->length:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 508
    :cond_0
    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;)Lorg/apache/lucene/index/ReaderSlice;
    .locals 1

    .prologue
    .line 499
    iget-object v0, p0, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->subSlice:Lorg/apache/lucene/index/ReaderSlice;

    return-object v0
.end method


# virtual methods
.method public reset(Lorg/apache/lucene/index/TermsEnum;Lorg/apache/lucene/util/BytesRef;)V
    .locals 0
    .param p1, "terms"    # Lorg/apache/lucene/index/TermsEnum;
    .param p2, "term"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 511
    iput-object p1, p0, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->terms:Lorg/apache/lucene/index/TermsEnum;

    .line 512
    iput-object p2, p0, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->current:Lorg/apache/lucene/util/BytesRef;

    .line 513
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 517
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->subSlice:Lorg/apache/lucene/index/ReaderSlice;

    invoke-virtual {v1}, Lorg/apache/lucene/index/ReaderSlice;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->terms:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
