.class Lorg/apache/lucene/index/DocumentsWriterPerThread$FlushedSegment;
.super Ljava/lang/Object;
.source "DocumentsWriterPerThread.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/DocumentsWriterPerThread;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "FlushedSegment"
.end annotation


# instance fields
.field final delCount:I

.field final fieldInfos:Lorg/apache/lucene/index/FieldInfos;

.field final liveDocs:Lorg/apache/lucene/util/MutableBits;

.field final segmentDeletes:Lorg/apache/lucene/index/FrozenBufferedDeletes;

.field final segmentInfo:Lorg/apache/lucene/index/SegmentInfoPerCommit;


# direct methods
.method private constructor <init>(Lorg/apache/lucene/index/SegmentInfoPerCommit;Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/index/BufferedDeletes;Lorg/apache/lucene/util/MutableBits;I)V
    .locals 2
    .param p1, "segmentInfo"    # Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .param p2, "fieldInfos"    # Lorg/apache/lucene/index/FieldInfos;
    .param p3, "segmentDeletes"    # Lorg/apache/lucene/index/BufferedDeletes;
    .param p4, "liveDocs"    # Lorg/apache/lucene/util/MutableBits;
    .param p5, "delCount"    # I

    .prologue
    .line 133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 135
    iput-object p1, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread$FlushedSegment;->segmentInfo:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .line 136
    iput-object p2, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread$FlushedSegment;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    .line 137
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Lorg/apache/lucene/index/BufferedDeletes;->any()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/lucene/index/FrozenBufferedDeletes;

    const/4 v1, 0x1

    invoke-direct {v0, p3, v1}, Lorg/apache/lucene/index/FrozenBufferedDeletes;-><init>(Lorg/apache/lucene/index/BufferedDeletes;Z)V

    :goto_0
    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread$FlushedSegment;->segmentDeletes:Lorg/apache/lucene/index/FrozenBufferedDeletes;

    .line 138
    iput-object p4, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread$FlushedSegment;->liveDocs:Lorg/apache/lucene/util/MutableBits;

    .line 139
    iput p5, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread$FlushedSegment;->delCount:I

    .line 140
    return-void

    .line 137
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/index/SegmentInfoPerCommit;Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/index/BufferedDeletes;Lorg/apache/lucene/util/MutableBits;ILorg/apache/lucene/index/DocumentsWriterPerThread$FlushedSegment;)V
    .locals 0

    .prologue
    .line 133
    invoke-direct/range {p0 .. p5}, Lorg/apache/lucene/index/DocumentsWriterPerThread$FlushedSegment;-><init>(Lorg/apache/lucene/index/SegmentInfoPerCommit;Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/index/BufferedDeletes;Lorg/apache/lucene/util/MutableBits;I)V

    return-void
.end method
