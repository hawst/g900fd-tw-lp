.class public Lorg/apache/lucene/index/OrdTermState;
.super Lorg/apache/lucene/index/TermState;
.source "OrdTermState.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field public ord:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lorg/apache/lucene/index/OrdTermState;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/OrdTermState;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lorg/apache/lucene/index/TermState;-><init>()V

    .line 32
    return-void
.end method


# virtual methods
.method public copyFrom(Lorg/apache/lucene/index/TermState;)V
    .locals 3
    .param p1, "other"    # Lorg/apache/lucene/index/TermState;

    .prologue
    .line 36
    sget-boolean v0, Lorg/apache/lucene/index/OrdTermState;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    instance-of v0, p1, Lorg/apache/lucene/index/OrdTermState;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "can not copy from "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 37
    :cond_0
    check-cast p1, Lorg/apache/lucene/index/OrdTermState;

    .end local p1    # "other":Lorg/apache/lucene/index/TermState;
    iget-wide v0, p1, Lorg/apache/lucene/index/OrdTermState;->ord:J

    iput-wide v0, p0, Lorg/apache/lucene/index/OrdTermState;->ord:J

    .line 38
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 42
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "OrdTermState ord="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lorg/apache/lucene/index/OrdTermState;->ord:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
