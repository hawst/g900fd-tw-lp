.class public final enum Lorg/apache/lucene/index/TermsEnum$SeekStatus;
.super Ljava/lang/Enum;
.source "TermsEnum.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/TermsEnum;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SeekStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/lucene/index/TermsEnum$SeekStatus;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum END:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

.field private static final synthetic ENUM$VALUES:[Lorg/apache/lucene/index/TermsEnum$SeekStatus;

.field public static final enum FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

.field public static final enum NOT_FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 61
    new-instance v0, Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    const-string v1, "END"

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/index/TermsEnum$SeekStatus;-><init>(Ljava/lang/String;I)V

    .line 62
    sput-object v0, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->END:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    .line 63
    new-instance v0, Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    const-string v1, "FOUND"

    invoke-direct {v0, v1, v3}, Lorg/apache/lucene/index/TermsEnum$SeekStatus;-><init>(Ljava/lang/String;I)V

    .line 64
    sput-object v0, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    .line 65
    new-instance v0, Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    const-string v1, "NOT_FOUND"

    invoke-direct {v0, v1, v4}, Lorg/apache/lucene/index/TermsEnum$SeekStatus;-><init>(Ljava/lang/String;I)V

    .line 66
    sput-object v0, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->NOT_FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    .line 60
    const/4 v0, 0x3

    new-array v0, v0, [Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    sget-object v1, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->END:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    aput-object v1, v0, v2

    sget-object v1, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    aput-object v1, v0, v3

    sget-object v1, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->NOT_FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    aput-object v1, v0, v4

    sput-object v0, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->ENUM$VALUES:[Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    return-object v0
.end method

.method public static values()[Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->ENUM$VALUES:[Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    array-length v1, v0

    new-array v2, v1, [Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
