.class Lorg/apache/lucene/index/DocumentsWriterFlushControl$BlockedFlush;
.super Ljava/lang/Object;
.source "DocumentsWriterFlushControl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/DocumentsWriterFlushControl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "BlockedFlush"
.end annotation


# instance fields
.field final bytes:J

.field final dwpt:Lorg/apache/lucene/index/DocumentsWriterPerThread;


# direct methods
.method constructor <init>(Lorg/apache/lucene/index/DocumentsWriterPerThread;J)V
    .locals 0
    .param p1, "dwpt"    # Lorg/apache/lucene/index/DocumentsWriterPerThread;
    .param p2, "bytes"    # J

    .prologue
    .line 659
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 660
    iput-object p1, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl$BlockedFlush;->dwpt:Lorg/apache/lucene/index/DocumentsWriterPerThread;

    .line 661
    iput-wide p2, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl$BlockedFlush;->bytes:J

    .line 662
    return-void
.end method
