.class Lorg/apache/lucene/index/DocumentsWriterPerThread$IntBlockAllocator;
.super Lorg/apache/lucene/util/IntBlockPool$Allocator;
.source "DocumentsWriterPerThread.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/DocumentsWriterPerThread;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "IntBlockAllocator"
.end annotation


# instance fields
.field private final bytesUsed:Lorg/apache/lucene/util/Counter;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/util/Counter;)V
    .locals 1
    .param p1, "bytesUsed"    # Lorg/apache/lucene/util/Counter;

    .prologue
    .line 640
    const/16 v0, 0x2000

    invoke-direct {p0, v0}, Lorg/apache/lucene/util/IntBlockPool$Allocator;-><init>(I)V

    .line 641
    iput-object p1, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread$IntBlockAllocator;->bytesUsed:Lorg/apache/lucene/util/Counter;

    .line 642
    return-void
.end method


# virtual methods
.method public getIntBlock()[I
    .locals 4

    .prologue
    .line 647
    const/16 v1, 0x2000

    new-array v0, v1, [I

    .line 648
    .local v0, "b":[I
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread$IntBlockAllocator;->bytesUsed:Lorg/apache/lucene/util/Counter;

    const-wide/32 v2, 0x8000

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/util/Counter;->addAndGet(J)J

    .line 650
    return-object v0
.end method

.method public recycleIntBlocks([[III)V
    .locals 4
    .param p1, "blocks"    # [[I
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 655
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread$IntBlockAllocator;->bytesUsed:Lorg/apache/lucene/util/Counter;

    const v1, 0x8000

    mul-int/2addr v1, p3

    neg-int v1, v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/util/Counter;->addAndGet(J)J

    .line 656
    return-void
.end method
