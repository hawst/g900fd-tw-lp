.class public Lorg/apache/lucene/index/LogByteSizeMergePolicy;
.super Lorg/apache/lucene/index/LogMergePolicy;
.source "LogByteSizeMergePolicy.java"


# static fields
.field public static final DEFAULT_MAX_MERGE_MB:D = 2048.0

.field public static final DEFAULT_MAX_MERGE_MB_FOR_FORCED_MERGE:D = 9.223372036854776E18

.field public static final DEFAULT_MIN_MERGE_MB:D = 1.6


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 39
    invoke-direct {p0}, Lorg/apache/lucene/index/LogMergePolicy;-><init>()V

    .line 40
    const-wide/32 v0, 0x199999

    iput-wide v0, p0, Lorg/apache/lucene/index/LogByteSizeMergePolicy;->minMergeSize:J

    .line 41
    const-wide v0, 0x80000000L

    iput-wide v0, p0, Lorg/apache/lucene/index/LogByteSizeMergePolicy;->maxMergeSize:J

    .line 42
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lorg/apache/lucene/index/LogByteSizeMergePolicy;->maxMergeSizeForForcedMerge:J

    .line 43
    return-void
.end method


# virtual methods
.method public getMaxMergeMB()D
    .locals 4

    .prologue
    const-wide/high16 v2, 0x4090000000000000L    # 1024.0

    .line 70
    iget-wide v0, p0, Lorg/apache/lucene/index/LogByteSizeMergePolicy;->maxMergeSize:J

    long-to-double v0, v0

    div-double/2addr v0, v2

    div-double/2addr v0, v2

    return-wide v0
.end method

.method public getMaxMergeMBForForcedMerge()D
    .locals 4

    .prologue
    const-wide/high16 v2, 0x4090000000000000L    # 1024.0

    .line 87
    iget-wide v0, p0, Lorg/apache/lucene/index/LogByteSizeMergePolicy;->maxMergeSizeForForcedMerge:J

    long-to-double v0, v0

    div-double/2addr v0, v2

    div-double/2addr v0, v2

    return-wide v0
.end method

.method public getMinMergeMB()D
    .locals 4

    .prologue
    const-wide/high16 v2, 0x4090000000000000L    # 1024.0

    .line 107
    iget-wide v0, p0, Lorg/apache/lucene/index/LogByteSizeMergePolicy;->minMergeSize:J

    long-to-double v0, v0

    div-double/2addr v0, v2

    div-double/2addr v0, v2

    return-wide v0
.end method

.method public setMaxMergeMB(D)V
    .locals 5
    .param p1, "mb"    # D

    .prologue
    const-wide/high16 v2, 0x4090000000000000L    # 1024.0

    .line 62
    mul-double v0, p1, v2

    mul-double/2addr v0, v2

    double-to-long v0, v0

    iput-wide v0, p0, Lorg/apache/lucene/index/LogByteSizeMergePolicy;->maxMergeSize:J

    .line 63
    return-void
.end method

.method public setMaxMergeMBForForcedMerge(D)V
    .locals 5
    .param p1, "mb"    # D

    .prologue
    const-wide/high16 v2, 0x4090000000000000L    # 1024.0

    .line 79
    mul-double v0, p1, v2

    mul-double/2addr v0, v2

    double-to-long v0, v0

    iput-wide v0, p0, Lorg/apache/lucene/index/LogByteSizeMergePolicy;->maxMergeSizeForForcedMerge:J

    .line 80
    return-void
.end method

.method public setMinMergeMB(D)V
    .locals 5
    .param p1, "mb"    # D

    .prologue
    const-wide/high16 v2, 0x4090000000000000L    # 1024.0

    .line 100
    mul-double v0, p1, v2

    mul-double/2addr v0, v2

    double-to-long v0, v0

    iput-wide v0, p0, Lorg/apache/lucene/index/LogByteSizeMergePolicy;->minMergeSize:J

    .line 101
    return-void
.end method

.method protected size(Lorg/apache/lucene/index/SegmentInfoPerCommit;)J
    .locals 2
    .param p1, "info"    # Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 47
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/LogByteSizeMergePolicy;->sizeBytes(Lorg/apache/lucene/index/SegmentInfoPerCommit;)J

    move-result-wide v0

    return-wide v0
.end method
