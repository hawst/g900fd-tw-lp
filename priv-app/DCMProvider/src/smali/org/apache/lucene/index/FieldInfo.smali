.class public final Lorg/apache/lucene/index/FieldInfo;
.super Ljava/lang/Object;
.source "FieldInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/FieldInfo$DocValuesType;,
        Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private attributes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private docValueType:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

.field private indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

.field private indexed:Z

.field public final name:Ljava/lang/String;

.field private normType:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

.field public final number:I

.field private omitNorms:Z

.field private storePayloads:Z

.field private storeTermVector:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lorg/apache/lucene/index/FieldInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/FieldInfo;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;ZIZZZLorg/apache/lucene/index/FieldInfo$IndexOptions;Lorg/apache/lucene/index/FieldInfo$DocValuesType;Lorg/apache/lucene/index/FieldInfo$DocValuesType;Ljava/util/Map;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "indexed"    # Z
    .param p3, "number"    # I
    .param p4, "storeTermVector"    # Z
    .param p5, "omitNorms"    # Z
    .param p6, "storePayloads"    # Z
    .param p7, "indexOptions"    # Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    .param p8, "docValues"    # Lorg/apache/lucene/index/FieldInfo$DocValuesType;
    .param p9, "normsType"    # Lorg/apache/lucene/index/FieldInfo$DocValuesType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "ZIZZZ",
            "Lorg/apache/lucene/index/FieldInfo$IndexOptions;",
            "Lorg/apache/lucene/index/FieldInfo$DocValuesType;",
            "Lorg/apache/lucene/index/FieldInfo$DocValuesType;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p10, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 121
    iput-object p1, p0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    .line 122
    iput-boolean p2, p0, Lorg/apache/lucene/index/FieldInfo;->indexed:Z

    .line 123
    iput p3, p0, Lorg/apache/lucene/index/FieldInfo;->number:I

    .line 124
    iput-object p8, p0, Lorg/apache/lucene/index/FieldInfo;->docValueType:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    .line 125
    if-eqz p2, :cond_1

    .line 126
    iput-boolean p4, p0, Lorg/apache/lucene/index/FieldInfo;->storeTermVector:Z

    .line 127
    iput-boolean p6, p0, Lorg/apache/lucene/index/FieldInfo;->storePayloads:Z

    .line 128
    iput-boolean p5, p0, Lorg/apache/lucene/index/FieldInfo;->omitNorms:Z

    .line 129
    iput-object p7, p0, Lorg/apache/lucene/index/FieldInfo;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    .line 130
    if-nez p5, :cond_0

    .end local p9    # "normsType":Lorg/apache/lucene/index/FieldInfo$DocValuesType;
    :goto_0
    iput-object p9, p0, Lorg/apache/lucene/index/FieldInfo;->normType:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    .line 138
    :goto_1
    iput-object p10, p0, Lorg/apache/lucene/index/FieldInfo;->attributes:Ljava/util/Map;

    .line 139
    sget-boolean v0, Lorg/apache/lucene/index/FieldInfo;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    invoke-direct {p0}, Lorg/apache/lucene/index/FieldInfo;->checkConsistency()Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .restart local p9    # "normsType":Lorg/apache/lucene/index/FieldInfo$DocValuesType;
    :cond_0
    move-object p9, v0

    .line 130
    goto :goto_0

    .line 132
    :cond_1
    iput-boolean v1, p0, Lorg/apache/lucene/index/FieldInfo;->storeTermVector:Z

    .line 133
    iput-boolean v1, p0, Lorg/apache/lucene/index/FieldInfo;->storePayloads:Z

    .line 134
    iput-boolean v1, p0, Lorg/apache/lucene/index/FieldInfo;->omitNorms:Z

    .line 135
    iput-object v0, p0, Lorg/apache/lucene/index/FieldInfo;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    .line 136
    iput-object v0, p0, Lorg/apache/lucene/index/FieldInfo;->normType:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    goto :goto_1

    .line 140
    .end local p9    # "normsType":Lorg/apache/lucene/index/FieldInfo$DocValuesType;
    :cond_2
    return-void
.end method

.method private checkConsistency()Z
    .locals 2

    .prologue
    .line 143
    iget-boolean v0, p0, Lorg/apache/lucene/index/FieldInfo;->indexed:Z

    if-nez v0, :cond_4

    .line 144
    sget-boolean v0, Lorg/apache/lucene/index/FieldInfo;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/apache/lucene/index/FieldInfo;->storeTermVector:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 145
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/index/FieldInfo;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lorg/apache/lucene/index/FieldInfo;->storePayloads:Z

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 146
    :cond_1
    sget-boolean v0, Lorg/apache/lucene/index/FieldInfo;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lorg/apache/lucene/index/FieldInfo;->omitNorms:Z

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 147
    :cond_2
    sget-boolean v0, Lorg/apache/lucene/index/FieldInfo;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfo;->normType:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 148
    :cond_3
    sget-boolean v0, Lorg/apache/lucene/index/FieldInfo;->$assertionsDisabled:Z

    if-nez v0, :cond_7

    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfo;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    if-eqz v0, :cond_7

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 150
    :cond_4
    sget-boolean v0, Lorg/apache/lucene/index/FieldInfo;->$assertionsDisabled:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfo;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 151
    :cond_5
    iget-boolean v0, p0, Lorg/apache/lucene/index/FieldInfo;->omitNorms:Z

    if-eqz v0, :cond_6

    .line 152
    sget-boolean v0, Lorg/apache/lucene/index/FieldInfo;->$assertionsDisabled:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfo;->normType:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    if-eqz v0, :cond_6

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 155
    :cond_6
    sget-boolean v0, Lorg/apache/lucene/index/FieldInfo;->$assertionsDisabled:Z

    if-nez v0, :cond_7

    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfo;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    sget-object v1, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-gez v0, :cond_7

    iget-boolean v0, p0, Lorg/apache/lucene/index/FieldInfo;->storePayloads:Z

    if-eqz v0, :cond_7

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 158
    :cond_7
    const/4 v0, 0x1

    return v0
.end method


# virtual methods
.method public attributes()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 319
    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfo;->attributes:Ljava/util/Map;

    return-object v0
.end method

.method public getAttribute(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 291
    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfo;->attributes:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 292
    const/4 v0, 0x0

    .line 294
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfo;->attributes:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getDocValuesType()Lorg/apache/lucene/index/FieldInfo$DocValuesType;
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfo;->docValueType:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    return-object v0
.end method

.method public getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfo;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    return-object v0
.end method

.method public getNormType()Lorg/apache/lucene/index/FieldInfo$DocValuesType;
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfo;->normType:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    return-object v0
.end method

.method public hasDocValues()Z
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfo;->docValueType:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNorms()Z
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfo;->normType:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPayloads()Z
    .locals 1

    .prologue
    .line 277
    iget-boolean v0, p0, Lorg/apache/lucene/index/FieldInfo;->storePayloads:Z

    return v0
.end method

.method public hasVectors()Z
    .locals 1

    .prologue
    .line 284
    iget-boolean v0, p0, Lorg/apache/lucene/index/FieldInfo;->storeTermVector:Z

    return v0
.end method

.method public isIndexed()Z
    .locals 1

    .prologue
    .line 270
    iget-boolean v0, p0, Lorg/apache/lucene/index/FieldInfo;->indexed:Z

    return v0
.end method

.method public omitsNorms()Z
    .locals 1

    .prologue
    .line 256
    iget-boolean v0, p0, Lorg/apache/lucene/index/FieldInfo;->omitNorms:Z

    return v0
.end method

.method public putAttribute(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 309
    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfo;->attributes:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 310
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/FieldInfo;->attributes:Ljava/util/Map;

    .line 312
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfo;->attributes:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method setDocValuesType(Lorg/apache/lucene/index/FieldInfo$DocValuesType;)V
    .locals 3
    .param p1, "type"    # Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    .prologue
    .line 199
    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfo;->docValueType:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfo;->docValueType:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    if-eq v0, p1, :cond_0

    .line 200
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "cannot change DocValues type from "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/apache/lucene/index/FieldInfo;->docValueType:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for field \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 202
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/index/FieldInfo;->docValueType:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    .line 203
    sget-boolean v0, Lorg/apache/lucene/index/FieldInfo;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    invoke-direct {p0}, Lorg/apache/lucene/index/FieldInfo;->checkConsistency()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 204
    :cond_1
    return-void
.end method

.method setNormValueType(Lorg/apache/lucene/index/FieldInfo$DocValuesType;)V
    .locals 3
    .param p1, "type"    # Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    .prologue
    .line 245
    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfo;->normType:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfo;->normType:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    if-eq v0, p1, :cond_0

    .line 246
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "cannot change Norm type from "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/apache/lucene/index/FieldInfo;->normType:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for field \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 248
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/index/FieldInfo;->normType:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    .line 249
    sget-boolean v0, Lorg/apache/lucene/index/FieldInfo;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    invoke-direct {p0}, Lorg/apache/lucene/index/FieldInfo;->checkConsistency()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 250
    :cond_1
    return-void
.end method

.method setStorePayloads()V
    .locals 2

    .prologue
    .line 238
    iget-boolean v0, p0, Lorg/apache/lucene/index/FieldInfo;->indexed:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfo;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    sget-object v1, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-ltz v0, :cond_0

    .line 239
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/index/FieldInfo;->storePayloads:Z

    .line 241
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/index/FieldInfo;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    invoke-direct {p0}, Lorg/apache/lucene/index/FieldInfo;->checkConsistency()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 242
    :cond_1
    return-void
.end method

.method setStoreTermVectors()V
    .locals 1

    .prologue
    .line 233
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/index/FieldInfo;->storeTermVector:Z

    .line 234
    sget-boolean v0, Lorg/apache/lucene/index/FieldInfo;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/index/FieldInfo;->checkConsistency()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 235
    :cond_0
    return-void
.end method

.method update(Lorg/apache/lucene/index/IndexableFieldType;)V
    .locals 6
    .param p1, "ft"    # Lorg/apache/lucene/index/IndexableFieldType;

    .prologue
    const/4 v2, 0x0

    .line 162
    invoke-interface {p1}, Lorg/apache/lucene/index/IndexableFieldType;->indexed()Z

    move-result v1

    invoke-interface {p1}, Lorg/apache/lucene/index/IndexableFieldType;->omitNorms()Z

    move-result v3

    invoke-interface {p1}, Lorg/apache/lucene/index/IndexableFieldType;->indexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v5

    move-object v0, p0

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Lorg/apache/lucene/index/FieldInfo;->update(ZZZZLorg/apache/lucene/index/FieldInfo$IndexOptions;)V

    .line 163
    return-void
.end method

.method update(ZZZZLorg/apache/lucene/index/FieldInfo$IndexOptions;)V
    .locals 2
    .param p1, "indexed"    # Z
    .param p2, "storeTermVector"    # Z
    .param p3, "omitNorms"    # Z
    .param p4, "storePayloads"    # Z
    .param p5, "indexOptions"    # Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    .prologue
    const/4 v1, 0x1

    .line 168
    iget-boolean v0, p0, Lorg/apache/lucene/index/FieldInfo;->indexed:Z

    if-eq v0, p1, :cond_0

    .line 169
    iput-boolean v1, p0, Lorg/apache/lucene/index/FieldInfo;->indexed:Z

    .line 171
    :cond_0
    if-eqz p1, :cond_4

    .line 172
    iget-boolean v0, p0, Lorg/apache/lucene/index/FieldInfo;->storeTermVector:Z

    if-eq v0, p2, :cond_1

    .line 173
    iput-boolean v1, p0, Lorg/apache/lucene/index/FieldInfo;->storeTermVector:Z

    .line 175
    :cond_1
    iget-boolean v0, p0, Lorg/apache/lucene/index/FieldInfo;->storePayloads:Z

    if-eq v0, p4, :cond_2

    .line 176
    iput-boolean v1, p0, Lorg/apache/lucene/index/FieldInfo;->storePayloads:Z

    .line 178
    :cond_2
    iget-boolean v0, p0, Lorg/apache/lucene/index/FieldInfo;->omitNorms:Z

    if-eq v0, p3, :cond_3

    .line 179
    iput-boolean v1, p0, Lorg/apache/lucene/index/FieldInfo;->omitNorms:Z

    .line 180
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/index/FieldInfo;->normType:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    .line 182
    :cond_3
    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfo;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    if-eq v0, p5, :cond_4

    .line 183
    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfo;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    if-nez v0, :cond_5

    .line 184
    iput-object p5, p0, Lorg/apache/lucene/index/FieldInfo;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    .line 189
    .end local p5    # "indexOptions":Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    :goto_0
    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfo;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    sget-object v1, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-gez v0, :cond_4

    .line 191
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/index/FieldInfo;->storePayloads:Z

    .line 195
    :cond_4
    sget-boolean v0, Lorg/apache/lucene/index/FieldInfo;->$assertionsDisabled:Z

    if-nez v0, :cond_7

    invoke-direct {p0}, Lorg/apache/lucene/index/FieldInfo;->checkConsistency()Z

    move-result v0

    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 187
    .restart local p5    # "indexOptions":Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    :cond_5
    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfo;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {v0, p5}, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-gez v0, :cond_6

    iget-object p5, p0, Lorg/apache/lucene/index/FieldInfo;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    .end local p5    # "indexOptions":Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    :cond_6
    iput-object p5, p0, Lorg/apache/lucene/index/FieldInfo;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    goto :goto_0

    .line 196
    :cond_7
    return-void
.end method
