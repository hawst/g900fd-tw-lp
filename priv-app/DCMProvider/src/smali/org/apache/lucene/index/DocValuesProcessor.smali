.class final Lorg/apache/lucene/index/DocValuesProcessor;
.super Lorg/apache/lucene/index/StoredFieldsConsumer;
.source "DocValuesProcessor.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final bytesUsed:Lorg/apache/lucene/util/Counter;

.field private final writers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/index/DocValuesWriter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lorg/apache/lucene/index/DocValuesProcessor;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/DocValuesProcessor;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Counter;)V
    .locals 1
    .param p1, "bytesUsed"    # Lorg/apache/lucene/util/Counter;

    .prologue
    .line 39
    invoke-direct {p0}, Lorg/apache/lucene/index/StoredFieldsConsumer;-><init>()V

    .line 36
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/DocValuesProcessor;->writers:Ljava/util/Map;

    .line 40
    iput-object p1, p0, Lorg/apache/lucene/index/DocValuesProcessor;->bytesUsed:Lorg/apache/lucene/util/Counter;

    .line 41
    return-void
.end method

.method private getTypeDesc(Lorg/apache/lucene/index/DocValuesWriter;)Ljava/lang/String;
    .locals 1
    .param p1, "obj"    # Lorg/apache/lucene/index/DocValuesWriter;

    .prologue
    .line 157
    instance-of v0, p1, Lorg/apache/lucene/index/BinaryDocValuesWriter;

    if-eqz v0, :cond_0

    .line 158
    const-string v0, "binary"

    .line 163
    :goto_0
    return-object v0

    .line 159
    :cond_0
    instance-of v0, p1, Lorg/apache/lucene/index/NumericDocValuesWriter;

    if-eqz v0, :cond_1

    .line 160
    const-string v0, "numeric"

    goto :goto_0

    .line 162
    :cond_1
    sget-boolean v0, Lorg/apache/lucene/index/DocValuesProcessor;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    instance-of v0, p1, Lorg/apache/lucene/index/SortedDocValuesWriter;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 163
    :cond_2
    const-string v0, "sorted"

    goto :goto_0
.end method


# virtual methods
.method public abort()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 169
    iget-object v1, p0, Lorg/apache/lucene/index/DocValuesProcessor;->writers:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 175
    iget-object v1, p0, Lorg/apache/lucene/index/DocValuesProcessor;->writers:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 176
    return-void

    .line 169
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/DocValuesWriter;

    .line 171
    .local v0, "writer":Lorg/apache/lucene/index/DocValuesWriter;
    :try_start_0
    invoke-virtual {v0}, Lorg/apache/lucene/index/DocValuesWriter;->abort()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 172
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method addBinaryField(Lorg/apache/lucene/index/FieldInfo;ILorg/apache/lucene/util/BytesRef;)V
    .locals 5
    .param p1, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;
    .param p2, "docID"    # I
    .param p3, "value"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 101
    iget-object v2, p0, Lorg/apache/lucene/index/DocValuesProcessor;->writers:Ljava/util/Map;

    iget-object v3, p1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/DocValuesWriter;

    .line 103
    .local v1, "writer":Lorg/apache/lucene/index/DocValuesWriter;
    if-nez v1, :cond_0

    .line 104
    new-instance v0, Lorg/apache/lucene/index/BinaryDocValuesWriter;

    iget-object v2, p0, Lorg/apache/lucene/index/DocValuesProcessor;->bytesUsed:Lorg/apache/lucene/util/Counter;

    invoke-direct {v0, p1, v2}, Lorg/apache/lucene/index/BinaryDocValuesWriter;-><init>(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/util/Counter;)V

    .line 105
    .local v0, "binaryWriter":Lorg/apache/lucene/index/BinaryDocValuesWriter;
    iget-object v2, p0, Lorg/apache/lucene/index/DocValuesProcessor;->writers:Ljava/util/Map;

    iget-object v3, p1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    :goto_0
    invoke-virtual {v0, p2, p3}, Lorg/apache/lucene/index/BinaryDocValuesWriter;->addValue(ILorg/apache/lucene/util/BytesRef;)V

    .line 112
    return-void

    .line 106
    .end local v0    # "binaryWriter":Lorg/apache/lucene/index/BinaryDocValuesWriter;
    :cond_0
    instance-of v2, v1, Lorg/apache/lucene/index/BinaryDocValuesWriter;

    if-nez v2, :cond_1

    .line 107
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Incompatible DocValues type: field \""

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\" changed from "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-direct {p0, v1}, Lorg/apache/lucene/index/DocValuesProcessor;->getTypeDesc(Lorg/apache/lucene/index/DocValuesWriter;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " to binary"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1
    move-object v0, v1

    .line 109
    check-cast v0, Lorg/apache/lucene/index/BinaryDocValuesWriter;

    .restart local v0    # "binaryWriter":Lorg/apache/lucene/index/BinaryDocValuesWriter;
    goto :goto_0
.end method

.method public addField(ILorg/apache/lucene/index/IndexableField;Lorg/apache/lucene/index/FieldInfo;)V
    .locals 4
    .param p1, "docID"    # I
    .param p2, "field"    # Lorg/apache/lucene/index/IndexableField;
    .param p3, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;

    .prologue
    .line 53
    invoke-interface {p2}, Lorg/apache/lucene/index/IndexableField;->fieldType()Lorg/apache/lucene/index/IndexableFieldType;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/lucene/index/IndexableFieldType;->docValueType()Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-result-object v0

    .line 54
    .local v0, "dvType":Lorg/apache/lucene/index/FieldInfo$DocValuesType;
    if-eqz v0, :cond_0

    .line 55
    invoke-virtual {p3, v0}, Lorg/apache/lucene/index/FieldInfo;->setDocValuesType(Lorg/apache/lucene/index/FieldInfo$DocValuesType;)V

    .line 56
    sget-object v1, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->BINARY:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    if-ne v0, v1, :cond_1

    .line 57
    invoke-interface {p2}, Lorg/apache/lucene/index/IndexableField;->binaryValue()Lorg/apache/lucene/util/BytesRef;

    move-result-object v1

    invoke-virtual {p0, p3, p1, v1}, Lorg/apache/lucene/index/DocValuesProcessor;->addBinaryField(Lorg/apache/lucene/index/FieldInfo;ILorg/apache/lucene/util/BytesRef;)V

    .line 71
    :cond_0
    :goto_0
    return-void

    .line 58
    :cond_1
    sget-object v1, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->SORTED:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    if-ne v0, v1, :cond_2

    .line 59
    invoke-interface {p2}, Lorg/apache/lucene/index/IndexableField;->binaryValue()Lorg/apache/lucene/util/BytesRef;

    move-result-object v1

    invoke-virtual {p0, p3, p1, v1}, Lorg/apache/lucene/index/DocValuesProcessor;->addSortedField(Lorg/apache/lucene/index/FieldInfo;ILorg/apache/lucene/util/BytesRef;)V

    goto :goto_0

    .line 60
    :cond_2
    sget-object v1, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->SORTED_SET:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    if-ne v0, v1, :cond_3

    .line 61
    invoke-interface {p2}, Lorg/apache/lucene/index/IndexableField;->binaryValue()Lorg/apache/lucene/util/BytesRef;

    move-result-object v1

    invoke-virtual {p0, p3, p1, v1}, Lorg/apache/lucene/index/DocValuesProcessor;->addSortedSetField(Lorg/apache/lucene/index/FieldInfo;ILorg/apache/lucene/util/BytesRef;)V

    goto :goto_0

    .line 62
    :cond_3
    sget-object v1, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->NUMERIC:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    if-ne v0, v1, :cond_5

    .line 63
    invoke-interface {p2}, Lorg/apache/lucene/index/IndexableField;->numericValue()Ljava/lang/Number;

    move-result-object v1

    instance-of v1, v1, Ljava/lang/Long;

    if-nez v1, :cond_4

    .line 64
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "illegal type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p2}, Lorg/apache/lucene/index/IndexableField;->numericValue()Ljava/lang/Number;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": DocValues types must be Long"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 66
    :cond_4
    invoke-interface {p2}, Lorg/apache/lucene/index/IndexableField;->numericValue()Ljava/lang/Number;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Number;->longValue()J

    move-result-wide v2

    invoke-virtual {p0, p3, p1, v2, v3}, Lorg/apache/lucene/index/DocValuesProcessor;->addNumericField(Lorg/apache/lucene/index/FieldInfo;IJ)V

    goto :goto_0

    .line 68
    :cond_5
    sget-boolean v1, Lorg/apache/lucene/index/DocValuesProcessor;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unrecognized DocValues.Type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method addNumericField(Lorg/apache/lucene/index/FieldInfo;IJ)V
    .locals 5
    .param p1, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;
    .param p2, "docID"    # I
    .param p3, "value"    # J

    .prologue
    .line 143
    iget-object v2, p0, Lorg/apache/lucene/index/DocValuesProcessor;->writers:Ljava/util/Map;

    iget-object v3, p1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/DocValuesWriter;

    .line 145
    .local v1, "writer":Lorg/apache/lucene/index/DocValuesWriter;
    if-nez v1, :cond_0

    .line 146
    new-instance v0, Lorg/apache/lucene/index/NumericDocValuesWriter;

    iget-object v2, p0, Lorg/apache/lucene/index/DocValuesProcessor;->bytesUsed:Lorg/apache/lucene/util/Counter;

    invoke-direct {v0, p1, v2}, Lorg/apache/lucene/index/NumericDocValuesWriter;-><init>(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/util/Counter;)V

    .line 147
    .local v0, "numericWriter":Lorg/apache/lucene/index/NumericDocValuesWriter;
    iget-object v2, p0, Lorg/apache/lucene/index/DocValuesProcessor;->writers:Ljava/util/Map;

    iget-object v3, p1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 153
    :goto_0
    invoke-virtual {v0, p2, p3, p4}, Lorg/apache/lucene/index/NumericDocValuesWriter;->addValue(IJ)V

    .line 154
    return-void

    .line 148
    .end local v0    # "numericWriter":Lorg/apache/lucene/index/NumericDocValuesWriter;
    :cond_0
    instance-of v2, v1, Lorg/apache/lucene/index/NumericDocValuesWriter;

    if-nez v2, :cond_1

    .line 149
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Incompatible DocValues type: field \""

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\" changed from "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-direct {p0, v1}, Lorg/apache/lucene/index/DocValuesProcessor;->getTypeDesc(Lorg/apache/lucene/index/DocValuesWriter;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " to numeric"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1
    move-object v0, v1

    .line 151
    check-cast v0, Lorg/apache/lucene/index/NumericDocValuesWriter;

    .restart local v0    # "numericWriter":Lorg/apache/lucene/index/NumericDocValuesWriter;
    goto :goto_0
.end method

.method addSortedField(Lorg/apache/lucene/index/FieldInfo;ILorg/apache/lucene/util/BytesRef;)V
    .locals 5
    .param p1, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;
    .param p2, "docID"    # I
    .param p3, "value"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 115
    iget-object v2, p0, Lorg/apache/lucene/index/DocValuesProcessor;->writers:Ljava/util/Map;

    iget-object v3, p1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/DocValuesWriter;

    .line 117
    .local v1, "writer":Lorg/apache/lucene/index/DocValuesWriter;
    if-nez v1, :cond_0

    .line 118
    new-instance v0, Lorg/apache/lucene/index/SortedDocValuesWriter;

    iget-object v2, p0, Lorg/apache/lucene/index/DocValuesProcessor;->bytesUsed:Lorg/apache/lucene/util/Counter;

    invoke-direct {v0, p1, v2}, Lorg/apache/lucene/index/SortedDocValuesWriter;-><init>(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/util/Counter;)V

    .line 119
    .local v0, "sortedWriter":Lorg/apache/lucene/index/SortedDocValuesWriter;
    iget-object v2, p0, Lorg/apache/lucene/index/DocValuesProcessor;->writers:Ljava/util/Map;

    iget-object v3, p1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    :goto_0
    invoke-virtual {v0, p2, p3}, Lorg/apache/lucene/index/SortedDocValuesWriter;->addValue(ILorg/apache/lucene/util/BytesRef;)V

    .line 126
    return-void

    .line 120
    .end local v0    # "sortedWriter":Lorg/apache/lucene/index/SortedDocValuesWriter;
    :cond_0
    instance-of v2, v1, Lorg/apache/lucene/index/SortedDocValuesWriter;

    if-nez v2, :cond_1

    .line 121
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Incompatible DocValues type: field \""

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\" changed from "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-direct {p0, v1}, Lorg/apache/lucene/index/DocValuesProcessor;->getTypeDesc(Lorg/apache/lucene/index/DocValuesWriter;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " to sorted"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1
    move-object v0, v1

    .line 123
    check-cast v0, Lorg/apache/lucene/index/SortedDocValuesWriter;

    .restart local v0    # "sortedWriter":Lorg/apache/lucene/index/SortedDocValuesWriter;
    goto :goto_0
.end method

.method addSortedSetField(Lorg/apache/lucene/index/FieldInfo;ILorg/apache/lucene/util/BytesRef;)V
    .locals 5
    .param p1, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;
    .param p2, "docID"    # I
    .param p3, "value"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 129
    iget-object v2, p0, Lorg/apache/lucene/index/DocValuesProcessor;->writers:Ljava/util/Map;

    iget-object v3, p1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/DocValuesWriter;

    .line 131
    .local v1, "writer":Lorg/apache/lucene/index/DocValuesWriter;
    if-nez v1, :cond_0

    .line 132
    new-instance v0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;

    iget-object v2, p0, Lorg/apache/lucene/index/DocValuesProcessor;->bytesUsed:Lorg/apache/lucene/util/Counter;

    invoke-direct {v0, p1, v2}, Lorg/apache/lucene/index/SortedSetDocValuesWriter;-><init>(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/util/Counter;)V

    .line 133
    .local v0, "sortedSetWriter":Lorg/apache/lucene/index/SortedSetDocValuesWriter;
    iget-object v2, p0, Lorg/apache/lucene/index/DocValuesProcessor;->writers:Ljava/util/Map;

    iget-object v3, p1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    :goto_0
    invoke-virtual {v0, p2, p3}, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->addValue(ILorg/apache/lucene/util/BytesRef;)V

    .line 140
    return-void

    .line 134
    .end local v0    # "sortedSetWriter":Lorg/apache/lucene/index/SortedSetDocValuesWriter;
    :cond_0
    instance-of v2, v1, Lorg/apache/lucene/index/SortedSetDocValuesWriter;

    if-nez v2, :cond_1

    .line 135
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Incompatible DocValues type: field \""

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\" changed from "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-direct {p0, v1}, Lorg/apache/lucene/index/DocValuesProcessor;->getTypeDesc(Lorg/apache/lucene/index/DocValuesWriter;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " to sorted"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1
    move-object v0, v1

    .line 137
    check-cast v0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;

    .restart local v0    # "sortedSetWriter":Lorg/apache/lucene/index/SortedSetDocValuesWriter;
    goto :goto_0
.end method

.method finishDocument()V
    .locals 0

    .prologue
    .line 49
    return-void
.end method

.method flush(Lorg/apache/lucene/index/SegmentWriteState;)V
    .locals 8
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentWriteState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 75
    iget-object v4, p0, Lorg/apache/lucene/index/DocValuesProcessor;->writers:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 76
    iget-object v4, p1, Lorg/apache/lucene/index/SegmentWriteState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v4}, Lorg/apache/lucene/index/SegmentInfo;->getCodec()Lorg/apache/lucene/codecs/Codec;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/lucene/codecs/Codec;->docValuesFormat()Lorg/apache/lucene/codecs/DocValuesFormat;

    move-result-object v1

    .line 77
    .local v1, "fmt":Lorg/apache/lucene/codecs/DocValuesFormat;
    invoke-virtual {v1, p1}, Lorg/apache/lucene/codecs/DocValuesFormat;->fieldsConsumer(Lorg/apache/lucene/index/SegmentWriteState;)Lorg/apache/lucene/codecs/DocValuesConsumer;

    move-result-object v0

    .line 78
    .local v0, "dvConsumer":Lorg/apache/lucene/codecs/DocValuesConsumer;
    const/4 v2, 0x0

    .line 80
    .local v2, "success":Z
    :try_start_0
    iget-object v4, p0, Lorg/apache/lucene/index/DocValuesProcessor;->writers:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_1

    .line 88
    iget-object v4, p0, Lorg/apache/lucene/index/DocValuesProcessor;->writers:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 89
    const/4 v2, 0x1

    .line 91
    if-eqz v2, :cond_3

    new-array v4, v7, [Ljava/io/Closeable;

    .line 92
    aput-object v0, v4, v6

    invoke-static {v4}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 98
    .end local v0    # "dvConsumer":Lorg/apache/lucene/codecs/DocValuesConsumer;
    .end local v1    # "fmt":Lorg/apache/lucene/codecs/DocValuesFormat;
    .end local v2    # "success":Z
    :cond_0
    :goto_1
    return-void

    .line 80
    .restart local v0    # "dvConsumer":Lorg/apache/lucene/codecs/DocValuesConsumer;
    .restart local v1    # "fmt":Lorg/apache/lucene/codecs/DocValuesFormat;
    .restart local v2    # "success":Z
    :cond_1
    :try_start_1
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/index/DocValuesWriter;

    .line 81
    .local v3, "writer":Lorg/apache/lucene/index/DocValuesWriter;
    iget-object v5, p1, Lorg/apache/lucene/index/SegmentWriteState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v5}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v5

    invoke-virtual {v3, v5}, Lorg/apache/lucene/index/DocValuesWriter;->finish(I)V

    .line 82
    invoke-virtual {v3, p1, v0}, Lorg/apache/lucene/index/DocValuesWriter;->flush(Lorg/apache/lucene/index/SegmentWriteState;Lorg/apache/lucene/codecs/DocValuesConsumer;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 90
    .end local v3    # "writer":Lorg/apache/lucene/index/DocValuesWriter;
    :catchall_0
    move-exception v4

    .line 91
    if-eqz v2, :cond_2

    new-array v5, v7, [Ljava/io/Closeable;

    .line 92
    aput-object v0, v5, v6

    invoke-static {v5}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 96
    :goto_2
    throw v4

    .line 93
    :cond_2
    new-array v5, v7, [Ljava/io/Closeable;

    .line 94
    aput-object v0, v5, v6

    invoke-static {v5}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto :goto_2

    .line 93
    :cond_3
    new-array v4, v7, [Ljava/io/Closeable;

    .line 94
    aput-object v0, v4, v6

    invoke-static {v4}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto :goto_1
.end method

.method startDocument()V
    .locals 0

    .prologue
    .line 45
    return-void
.end method
