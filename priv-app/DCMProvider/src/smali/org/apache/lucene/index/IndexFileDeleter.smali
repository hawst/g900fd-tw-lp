.class final Lorg/apache/lucene/index/IndexFileDeleter;
.super Ljava/lang/Object;
.source "IndexFileDeleter.java"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;,
        Lorg/apache/lucene/index/IndexFileDeleter$RefCount;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static VERBOSE_REF_COUNTS:Z


# instance fields
.field private commits:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;",
            ">;"
        }
    .end annotation
.end field

.field private commitsToDelete:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;",
            ">;"
        }
    .end annotation
.end field

.field private deletable:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private directory:Lorg/apache/lucene/store/Directory;

.field private final infoStream:Lorg/apache/lucene/util/InfoStream;

.field private lastFiles:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private lastSegmentInfos:Lorg/apache/lucene/index/SegmentInfos;

.field private policy:Lorg/apache/lucene/index/IndexDeletionPolicy;

.field private refCounts:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/index/IndexFileDeleter$RefCount;",
            ">;"
        }
    .end annotation
.end field

.field final startingCommitDeleted:Z

.field private final writer:Lorg/apache/lucene/index/IndexWriter;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 73
    const-class v0, Lorg/apache/lucene/index/IndexFileDeleter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/IndexFileDeleter;->$assertionsDisabled:Z

    .line 108
    sput-boolean v1, Lorg/apache/lucene/index/IndexFileDeleter;->VERBOSE_REF_COUNTS:Z

    return-void

    :cond_0
    move v0, v1

    .line 73
    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexDeletionPolicy;Lorg/apache/lucene/index/SegmentInfos;Lorg/apache/lucene/util/InfoStream;Lorg/apache/lucene/index/IndexWriter;Z)V
    .locals 22
    .param p1, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p2, "policy"    # Lorg/apache/lucene/index/IndexDeletionPolicy;
    .param p3, "segmentInfos"    # Lorg/apache/lucene/index/SegmentInfos;
    .param p4, "infoStream"    # Lorg/apache/lucene/util/InfoStream;
    .param p5, "writer"    # Lorg/apache/lucene/index/IndexWriter;
    .param p6, "initialIndexExists"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 125
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 83
    new-instance v16, Ljava/util/HashMap;

    invoke-direct/range {v16 .. v16}, Ljava/util/HashMap;-><init>()V

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/index/IndexFileDeleter;->refCounts:Ljava/util/Map;

    .line 90
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/index/IndexFileDeleter;->commits:Ljava/util/List;

    .line 94
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/index/IndexFileDeleter;->lastFiles:Ljava/util/List;

    .line 97
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/index/IndexFileDeleter;->commitsToDelete:Ljava/util/List;

    .line 127
    move-object/from16 v0, p4

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/index/IndexFileDeleter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    .line 128
    move-object/from16 v0, p5

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/index/IndexFileDeleter;->writer:Lorg/apache/lucene/index/IndexWriter;

    .line 130
    invoke-virtual/range {p3 .. p3}, Lorg/apache/lucene/index/SegmentInfos;->getSegmentsFileName()Ljava/lang/String;

    move-result-object v8

    .line 132
    .local v8, "currentSegmentsFile":Ljava/lang/String;
    const-string v16, "IFD"

    move-object/from16 v0, p4

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_0

    .line 133
    const-string v16, "IFD"

    new-instance v17, Ljava/lang/StringBuilder;

    const-string v18, "init: current segments file is \""

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "\"; deletionPolicy="

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p4

    move-object/from16 v1, v16

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    :cond_0
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/index/IndexFileDeleter;->policy:Lorg/apache/lucene/index/IndexDeletionPolicy;

    .line 137
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/index/IndexFileDeleter;->directory:Lorg/apache/lucene/store/Directory;

    .line 141
    invoke-virtual/range {p3 .. p3}, Lorg/apache/lucene/index/SegmentInfos;->getGeneration()J

    move-result-wide v6

    .line 143
    .local v6, "currentGen":J
    const/4 v5, 0x0

    .line 144
    .local v5, "currentCommitPoint":Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;
    const/4 v12, 0x0

    .line 146
    .local v12, "files":[Ljava/lang/String;
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/store/Directory;->listAll()[Ljava/lang/String;
    :try_end_0
    .catch Lorg/apache/lucene/store/NoSuchDirectoryException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v12

    .line 152
    :goto_0
    if-eqz v8, :cond_1

    .line 153
    sget-object v16, Lorg/apache/lucene/index/IndexFileNames;->CODEC_FILE_PATTERN:Ljava/util/regex/Pattern;

    const-string v17, ""

    invoke-virtual/range {v16 .. v17}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v13

    .line 154
    .local v13, "m":Ljava/util/regex/Matcher;
    array-length v0, v12

    move/from16 v17, v0

    const/16 v16, 0x0

    :goto_1
    move/from16 v0, v16

    move/from16 v1, v17

    if-lt v0, v1, :cond_6

    .line 212
    .end local v13    # "m":Ljava/util/regex/Matcher;
    :cond_1
    if-nez v5, :cond_3

    if-eqz v8, :cond_3

    if-eqz p6, :cond_3

    .line 220
    new-instance v15, Lorg/apache/lucene/index/SegmentInfos;

    invoke-direct {v15}, Lorg/apache/lucene/index/SegmentInfos;-><init>()V

    .line 222
    .local v15, "sis":Lorg/apache/lucene/index/SegmentInfos;
    :try_start_1
    move-object/from16 v0, p1

    invoke-virtual {v15, v0, v8}, Lorg/apache/lucene/index/SegmentInfos;->read(Lorg/apache/lucene/store/Directory;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    .line 226
    const-string v16, "IFD"

    move-object/from16 v0, p4

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_2

    .line 227
    const-string v16, "IFD"

    new-instance v17, Ljava/lang/StringBuilder;

    const-string v18, "forced open of current segments file "

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p3 .. p3}, Lorg/apache/lucene/index/SegmentInfos;->getSegmentsFileName()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p4

    move-object/from16 v1, v16

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    :cond_2
    new-instance v5, Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;

    .end local v5    # "currentCommitPoint":Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/IndexFileDeleter;->commitsToDelete:Ljava/util/List;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    move-object/from16 v1, p1

    invoke-direct {v5, v0, v1, v15}, Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;-><init>(Ljava/util/Collection;Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfos;)V

    .line 230
    .restart local v5    # "currentCommitPoint":Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/IndexFileDeleter;->commits:Ljava/util/List;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 231
    const/16 v16, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v15, v1}, Lorg/apache/lucene/index/IndexFileDeleter;->incRef(Lorg/apache/lucene/index/SegmentInfos;Z)V

    .line 235
    .end local v15    # "sis":Lorg/apache/lucene/index/SegmentInfos;
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/IndexFileDeleter;->commits:Ljava/util/List;

    move-object/from16 v16, v0

    invoke-static/range {v16 .. v16}, Lorg/apache/lucene/util/CollectionUtil;->mergeSort(Ljava/util/List;)V

    .line 240
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/IndexFileDeleter;->refCounts:Ljava/util/Map;

    move-object/from16 v16, v0

    invoke-interface/range {v16 .. v16}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v16

    invoke-interface/range {v16 .. v16}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :cond_4
    :goto_2
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-nez v17, :cond_e

    .line 253
    if-eqz v8, :cond_5

    .line 254
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/IndexFileDeleter;->commits:Ljava/util/List;

    move-object/from16 v16, v0

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/IndexDeletionPolicy;->onInit(Ljava/util/List;)V

    .line 259
    :cond_5
    const/16 v16, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/index/IndexFileDeleter;->checkpoint(Lorg/apache/lucene/index/SegmentInfos;Z)V

    .line 261
    if-nez v5, :cond_10

    const/16 v16, 0x0

    :goto_3
    move/from16 v0, v16

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lorg/apache/lucene/index/IndexFileDeleter;->startingCommitDeleted:Z

    .line 263
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/index/IndexFileDeleter;->deleteCommits()V

    .line 264
    return-void

    .line 147
    :catch_0
    move-exception v9

    .line 149
    .local v9, "e":Lorg/apache/lucene/store/NoSuchDirectoryException;
    const/16 v16, 0x0

    move/from16 v0, v16

    new-array v12, v0, [Ljava/lang/String;

    goto/16 :goto_0

    .line 154
    .end local v9    # "e":Lorg/apache/lucene/store/NoSuchDirectoryException;
    .restart local v13    # "m":Ljava/util/regex/Matcher;
    :cond_6
    aget-object v11, v12, v16

    .line 155
    .local v11, "fileName":Ljava/lang/String;
    invoke-virtual {v13, v11}, Ljava/util/regex/Matcher;->reset(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    .line 156
    const-string/jumbo v18, "write.lock"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v18

    if-nez v18, :cond_b

    const-string v18, "segments.gen"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_b

    .line 157
    invoke-virtual {v13}, Ljava/util/regex/Matcher;->matches()Z

    move-result v18

    if-nez v18, :cond_7

    const-string v18, "segments"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_b

    .line 160
    :cond_7
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lorg/apache/lucene/index/IndexFileDeleter;->getRefCount(Ljava/lang/String;)Lorg/apache/lucene/index/IndexFileDeleter$RefCount;

    .line 162
    const-string v18, "segments"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_b

    .line 167
    const-string v18, "IFD"

    move-object/from16 v0, p4

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_8

    .line 168
    const-string v18, "IFD"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "init: load commit \""

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\""

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p4

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    :cond_8
    new-instance v15, Lorg/apache/lucene/index/SegmentInfos;

    invoke-direct {v15}, Lorg/apache/lucene/index/SegmentInfos;-><init>()V

    .line 172
    .restart local v15    # "sis":Lorg/apache/lucene/index/SegmentInfos;
    :try_start_2
    move-object/from16 v0, p1

    invoke-virtual {v15, v0, v11}, Lorg/apache/lucene/index/SegmentInfos;->read(Lorg/apache/lucene/store/Directory;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 195
    :goto_4
    if-eqz v15, :cond_b

    .line 196
    new-instance v4, Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/IndexFileDeleter;->commitsToDelete:Ljava/util/List;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-direct {v4, v0, v1, v15}, Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;-><init>(Ljava/util/Collection;Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfos;)V

    .line 197
    .local v4, "commitPoint":Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;
    invoke-virtual {v15}, Lorg/apache/lucene/index/SegmentInfos;->getGeneration()J

    move-result-wide v18

    invoke-virtual/range {p3 .. p3}, Lorg/apache/lucene/index/SegmentInfos;->getGeneration()J

    move-result-wide v20

    cmp-long v18, v18, v20

    if-nez v18, :cond_9

    .line 198
    move-object v5, v4

    .line 200
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/IndexFileDeleter;->commits:Ljava/util/List;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 201
    const/16 v18, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v15, v1}, Lorg/apache/lucene/index/IndexFileDeleter;->incRef(Lorg/apache/lucene/index/SegmentInfos;Z)V

    .line 203
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/IndexFileDeleter;->lastSegmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    move-object/from16 v18, v0

    if-eqz v18, :cond_a

    invoke-virtual {v15}, Lorg/apache/lucene/index/SegmentInfos;->getGeneration()J

    move-result-wide v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/IndexFileDeleter;->lastSegmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lorg/apache/lucene/index/SegmentInfos;->getGeneration()J

    move-result-wide v20

    cmp-long v18, v18, v20

    if-lez v18, :cond_b

    .line 204
    :cond_a
    move-object/from16 v0, p0

    iput-object v15, v0, Lorg/apache/lucene/index/IndexFileDeleter;->lastSegmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    .line 154
    .end local v4    # "commitPoint":Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;
    .end local v15    # "sis":Lorg/apache/lucene/index/SegmentInfos;
    :cond_b
    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_1

    .line 173
    .restart local v15    # "sis":Lorg/apache/lucene/index/SegmentInfos;
    :catch_1
    move-exception v9

    .line 181
    .local v9, "e":Ljava/io/FileNotFoundException;
    const-string v18, "IFD"

    move-object/from16 v0, p4

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_c

    .line 182
    const-string v18, "IFD"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "init: hit FileNotFoundException when loading commit \""

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\"; skipping this commit point"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p4

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    :cond_c
    const/4 v15, 0x0

    goto/16 :goto_4

    .line 185
    .end local v9    # "e":Ljava/io/FileNotFoundException;
    :catch_2
    move-exception v9

    .line 186
    .local v9, "e":Ljava/io/IOException;
    invoke-static {v11}, Lorg/apache/lucene/index/SegmentInfos;->generationFromSegmentsFileName(Ljava/lang/String;)J

    move-result-wide v18

    cmp-long v18, v18, v6

    if-gtz v18, :cond_d

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Lorg/apache/lucene/store/Directory;->fileLength(Ljava/lang/String;)J

    move-result-wide v18

    const-wide/16 v20, 0x0

    cmp-long v18, v18, v20

    if-lez v18, :cond_d

    .line 187
    throw v9

    .line 192
    :cond_d
    const/4 v15, 0x0

    goto/16 :goto_4

    .line 223
    .end local v9    # "e":Ljava/io/IOException;
    .end local v11    # "fileName":Ljava/lang/String;
    .end local v13    # "m":Ljava/util/regex/Matcher;
    :catch_3
    move-exception v9

    .line 224
    .restart local v9    # "e":Ljava/io/IOException;
    new-instance v16, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v17, Ljava/lang/StringBuilder;

    const-string v18, "failed to locate current segments_N file \""

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "\""

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-direct/range {v16 .. v17}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v16

    .line 240
    .end local v9    # "e":Ljava/io/IOException;
    .end local v15    # "sis":Lorg/apache/lucene/index/SegmentInfos;
    :cond_e
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/util/Map$Entry;

    .line 241
    .local v10, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lorg/apache/lucene/index/IndexFileDeleter$RefCount;>;"
    invoke-interface {v10}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lorg/apache/lucene/index/IndexFileDeleter$RefCount;

    .line 242
    .local v14, "rc":Lorg/apache/lucene/index/IndexFileDeleter$RefCount;
    invoke-interface {v10}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    .line 243
    .restart local v11    # "fileName":Ljava/lang/String;
    iget v0, v14, Lorg/apache/lucene/index/IndexFileDeleter$RefCount;->count:I

    move/from16 v17, v0

    if-nez v17, :cond_4

    .line 244
    const-string v17, "IFD"

    move-object/from16 v0, p4

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_f

    .line 245
    const-string v17, "IFD"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "init: removing unreferenced file \""

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "\""

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p4

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    :cond_f
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lorg/apache/lucene/index/IndexFileDeleter;->deleteFile(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 261
    .end local v10    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lorg/apache/lucene/index/IndexFileDeleter$RefCount;>;"
    .end local v11    # "fileName":Ljava/lang/String;
    .end local v14    # "rc":Lorg/apache/lucene/index/IndexFileDeleter$RefCount;
    :cond_10
    invoke-virtual {v5}, Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;->isDeleted()Z

    move-result v16

    goto/16 :goto_3
.end method

.method private deleteCommits()V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 276
    iget-object v6, p0, Lorg/apache/lucene/index/IndexFileDeleter;->commitsToDelete:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v4

    .line 278
    .local v4, "size":I
    if-lez v4, :cond_0

    .line 282
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-lt v2, v4, :cond_1

    .line 291
    iget-object v6, p0, Lorg/apache/lucene/index/IndexFileDeleter;->commitsToDelete:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->clear()V

    .line 294
    iget-object v6, p0, Lorg/apache/lucene/index/IndexFileDeleter;->commits:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v4

    .line 295
    const/4 v3, 0x0

    .line 296
    .local v3, "readFrom":I
    const/4 v5, 0x0

    .line 297
    .local v5, "writeTo":I
    :goto_1
    if-lt v3, v4, :cond_4

    .line 308
    :goto_2
    if-gt v4, v5, :cond_7

    .line 313
    .end local v2    # "i":I
    .end local v3    # "readFrom":I
    .end local v5    # "writeTo":I
    :cond_0
    return-void

    .line 283
    .restart local v2    # "i":I
    :cond_1
    iget-object v6, p0, Lorg/apache/lucene/index/IndexFileDeleter;->commitsToDelete:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;

    .line 284
    .local v0, "commit":Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;
    iget-object v6, p0, Lorg/apache/lucene/index/IndexFileDeleter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v7, "IFD"

    invoke-virtual {v6, v7}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 285
    iget-object v6, p0, Lorg/apache/lucene/index/IndexFileDeleter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v7, "IFD"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "deleteCommits: now decRef commit \""

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;->getSegmentsFileName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    :cond_2
    iget-object v6, v0, Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;->files:Ljava/util/Collection;

    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_3

    .line 282
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 287
    :cond_3
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 288
    .local v1, "file":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lorg/apache/lucene/index/IndexFileDeleter;->decRef(Ljava/lang/String;)V

    goto :goto_3

    .line 298
    .end local v0    # "commit":Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;
    .end local v1    # "file":Ljava/lang/String;
    .restart local v3    # "readFrom":I
    .restart local v5    # "writeTo":I
    :cond_4
    iget-object v6, p0, Lorg/apache/lucene/index/IndexFileDeleter;->commits:Ljava/util/List;

    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;

    .line 299
    .restart local v0    # "commit":Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;
    iget-boolean v6, v0, Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;->deleted:Z

    if-nez v6, :cond_6

    .line 300
    if-eq v5, v3, :cond_5

    .line 301
    iget-object v7, p0, Lorg/apache/lucene/index/IndexFileDeleter;->commits:Ljava/util/List;

    iget-object v6, p0, Lorg/apache/lucene/index/IndexFileDeleter;->commits:Ljava/util/List;

    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;

    invoke-interface {v7, v5, v6}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 303
    :cond_5
    add-int/lit8 v5, v5, 0x1

    .line 305
    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 309
    .end local v0    # "commit":Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;
    :cond_7
    iget-object v6, p0, Lorg/apache/lucene/index/IndexFileDeleter;->commits:Ljava/util/List;

    add-int/lit8 v7, v4, -0x1

    invoke-interface {v6, v7}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 310
    add-int/lit8 v4, v4, -0x1

    goto :goto_2
.end method

.method private getRefCount(Ljava/lang/String;)Lorg/apache/lucene/index/IndexFileDeleter$RefCount;
    .locals 2
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 544
    sget-boolean v1, Lorg/apache/lucene/index/IndexFileDeleter;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/index/IndexFileDeleter;->locked()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 546
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/IndexFileDeleter;->refCounts:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 547
    new-instance v0, Lorg/apache/lucene/index/IndexFileDeleter$RefCount;

    invoke-direct {v0, p1}, Lorg/apache/lucene/index/IndexFileDeleter$RefCount;-><init>(Ljava/lang/String;)V

    .line 548
    .local v0, "rc":Lorg/apache/lucene/index/IndexFileDeleter$RefCount;
    iget-object v1, p0, Lorg/apache/lucene/index/IndexFileDeleter;->refCounts:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 552
    :goto_0
    return-object v0

    .line 550
    .end local v0    # "rc":Lorg/apache/lucene/index/IndexFileDeleter$RefCount;
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/index/IndexFileDeleter;->refCounts:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/IndexFileDeleter$RefCount;

    .restart local v0    # "rc":Lorg/apache/lucene/index/IndexFileDeleter$RefCount;
    goto :goto_0
.end method

.method private locked()Z
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lorg/apache/lucene/index/IndexFileDeleter;->writer:Lorg/apache/lucene/index/IndexWriter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/IndexFileDeleter;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public checkpoint(Lorg/apache/lucene/index/SegmentInfos;Z)V
    .locals 12
    .param p1, "segmentInfos"    # Lorg/apache/lucene/index/SegmentInfos;
    .param p2, "isCommit"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 436
    sget-boolean v1, Lorg/apache/lucene/index/IndexFileDeleter;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/index/IndexFileDeleter;->locked()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 438
    :cond_0
    sget-boolean v1, Lorg/apache/lucene/index/IndexFileDeleter;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lorg/apache/lucene/index/IndexFileDeleter;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-static {v1}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 439
    :cond_1
    const-wide/16 v2, 0x0

    .line 440
    .local v2, "t0":J
    iget-object v1, p0, Lorg/apache/lucene/index/IndexFileDeleter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v6, "IFD"

    invoke-virtual {v1, v6}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 441
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    .line 442
    iget-object v1, p0, Lorg/apache/lucene/index/IndexFileDeleter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v6, "IFD"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "now checkpoint \""

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lorg/apache/lucene/index/IndexFileDeleter;->writer:Lorg/apache/lucene/index/IndexWriter;

    iget-object v9, p0, Lorg/apache/lucene/index/IndexFileDeleter;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v9, p1}, Lorg/apache/lucene/index/IndexWriter;->toLiveInfos(Lorg/apache/lucene/index/SegmentInfos;)Lorg/apache/lucene/index/SegmentInfos;

    move-result-object v9

    invoke-virtual {v8, v9}, Lorg/apache/lucene/index/IndexWriter;->segString(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\" ["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfos;->size()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " segments "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "; isCommit = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 447
    :cond_2
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexFileDeleter;->deletePendingFiles()V

    .line 450
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/index/IndexFileDeleter;->incRef(Lorg/apache/lucene/index/SegmentInfos;Z)V

    .line 452
    if-eqz p2, :cond_4

    .line 454
    iget-object v1, p0, Lorg/apache/lucene/index/IndexFileDeleter;->commits:Ljava/util/List;

    new-instance v6, Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;

    iget-object v7, p0, Lorg/apache/lucene/index/IndexFileDeleter;->commitsToDelete:Ljava/util/List;

    iget-object v8, p0, Lorg/apache/lucene/index/IndexFileDeleter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-direct {v6, v7, v8, p1}, Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;-><init>(Ljava/util/Collection;Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfos;)V

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 457
    iget-object v1, p0, Lorg/apache/lucene/index/IndexFileDeleter;->policy:Lorg/apache/lucene/index/IndexDeletionPolicy;

    iget-object v6, p0, Lorg/apache/lucene/index/IndexFileDeleter;->commits:Ljava/util/List;

    invoke-virtual {v1, v6}, Lorg/apache/lucene/index/IndexDeletionPolicy;->onCommit(Ljava/util/List;)V

    .line 460
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexFileDeleter;->deleteCommits()V

    .line 471
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/index/IndexFileDeleter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v6, "IFD"

    invoke-virtual {v1, v6}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 472
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    .line 473
    .local v4, "t1":J
    iget-object v1, p0, Lorg/apache/lucene/index/IndexFileDeleter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v6, "IFD"

    new-instance v7, Ljava/lang/StringBuilder;

    sub-long v8, v4, v2

    const-wide/32 v10, 0xf4240

    div-long/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, " msec to checkpoint"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 475
    .end local v4    # "t1":J
    :cond_3
    return-void

    .line 463
    :cond_4
    iget-object v1, p0, Lorg/apache/lucene/index/IndexFileDeleter;->lastFiles:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_5

    .line 466
    iget-object v1, p0, Lorg/apache/lucene/index/IndexFileDeleter;->lastFiles:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 469
    iget-object v1, p0, Lorg/apache/lucene/index/IndexFileDeleter;->lastFiles:Ljava/util/List;

    iget-object v6, p0, Lorg/apache/lucene/index/IndexFileDeleter;->directory:Lorg/apache/lucene/store/Directory;

    const/4 v7, 0x0

    invoke-virtual {p1, v6, v7}, Lorg/apache/lucene/index/SegmentInfos;->files(Lorg/apache/lucene/store/Directory;Z)Ljava/util/Collection;

    move-result-object v6

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 463
    :cond_5
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 464
    .local v0, "lastFile":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexFileDeleter;->decRef(Ljava/util/Collection;)V

    goto :goto_1
.end method

.method public close()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 367
    sget-boolean v2, Lorg/apache/lucene/index/IndexFileDeleter;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/index/IndexFileDeleter;->locked()Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 368
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/index/IndexFileDeleter;->lastFiles:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    .line 369
    .local v1, "size":I
    if-lez v1, :cond_1

    .line 370
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v1, :cond_2

    .line 373
    iget-object v2, p0, Lorg/apache/lucene/index/IndexFileDeleter;->lastFiles:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 376
    .end local v0    # "i":I
    :cond_1
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexFileDeleter;->deletePendingFiles()V

    .line 377
    return-void

    .line 371
    .restart local v0    # "i":I
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/index/IndexFileDeleter;->lastFiles:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Collection;

    invoke-virtual {p0, v2}, Lorg/apache/lucene/index/IndexFileDeleter;->decRef(Ljava/util/Collection;)V

    .line 370
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method decRef(Ljava/lang/String;)V
    .locals 5
    .param p1, "fileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 512
    sget-boolean v1, Lorg/apache/lucene/index/IndexFileDeleter;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/index/IndexFileDeleter;->locked()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 513
    :cond_0
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/IndexFileDeleter;->getRefCount(Ljava/lang/String;)Lorg/apache/lucene/index/IndexFileDeleter$RefCount;

    move-result-object v0

    .line 514
    .local v0, "rc":Lorg/apache/lucene/index/IndexFileDeleter$RefCount;
    iget-object v1, p0, Lorg/apache/lucene/index/IndexFileDeleter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v2, "IFD"

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 515
    sget-boolean v1, Lorg/apache/lucene/index/IndexFileDeleter;->VERBOSE_REF_COUNTS:Z

    if-eqz v1, :cond_1

    .line 516
    iget-object v1, p0, Lorg/apache/lucene/index/IndexFileDeleter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v2, "IFD"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "  DecRef \""

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\": pre-decr count is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v0, Lorg/apache/lucene/index/IndexFileDeleter$RefCount;->count:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 519
    :cond_1
    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexFileDeleter$RefCount;->DecRef()I

    move-result v1

    if-nez v1, :cond_2

    .line 522
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/IndexFileDeleter;->deleteFile(Ljava/lang/String;)V

    .line 523
    iget-object v1, p0, Lorg/apache/lucene/index/IndexFileDeleter;->refCounts:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 525
    :cond_2
    return-void
.end method

.method decRef(Ljava/util/Collection;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 505
    .local p1, "files":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    sget-boolean v1, Lorg/apache/lucene/index/IndexFileDeleter;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/index/IndexFileDeleter;->locked()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 506
    :cond_0
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 509
    return-void

    .line 506
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 507
    .local v0, "file":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexFileDeleter;->decRef(Ljava/lang/String;)V

    goto :goto_0
.end method

.method decRef(Lorg/apache/lucene/index/SegmentInfos;)V
    .locals 3
    .param p1, "segmentInfos"    # Lorg/apache/lucene/index/SegmentInfos;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 528
    sget-boolean v1, Lorg/apache/lucene/index/IndexFileDeleter;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/index/IndexFileDeleter;->locked()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 529
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/IndexFileDeleter;->directory:Lorg/apache/lucene/store/Directory;

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Lorg/apache/lucene/index/SegmentInfos;->files(Lorg/apache/lucene/store/Directory;Z)Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 532
    return-void

    .line 529
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 530
    .local v0, "file":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexFileDeleter;->decRef(Ljava/lang/String;)V

    goto :goto_0
.end method

.method deleteFile(Ljava/lang/String;)V
    .locals 5
    .param p1, "fileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 584
    sget-boolean v1, Lorg/apache/lucene/index/IndexFileDeleter;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/index/IndexFileDeleter;->locked()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 586
    :cond_0
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/index/IndexFileDeleter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v2, "IFD"

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 587
    iget-object v1, p0, Lorg/apache/lucene/index/IndexFileDeleter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v2, "IFD"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "delete \""

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 589
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/index/IndexFileDeleter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 609
    :cond_2
    :goto_0
    return-void

    .line 590
    :catch_0
    move-exception v0

    .line 591
    .local v0, "e":Ljava/io/IOException;
    iget-object v1, p0, Lorg/apache/lucene/index/IndexFileDeleter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/store/Directory;->fileExists(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 600
    iget-object v1, p0, Lorg/apache/lucene/index/IndexFileDeleter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v2, "IFD"

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 601
    iget-object v1, p0, Lorg/apache/lucene/index/IndexFileDeleter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v2, "IFD"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "unable to remove file \""

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "; Will re-try later."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 603
    :cond_3
    iget-object v1, p0, Lorg/apache/lucene/index/IndexFileDeleter;->deletable:Ljava/util/List;

    if-nez v1, :cond_4

    .line 604
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/index/IndexFileDeleter;->deletable:Ljava/util/List;

    .line 606
    :cond_4
    iget-object v1, p0, Lorg/apache/lucene/index/IndexFileDeleter;->deletable:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method deleteFiles(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 556
    .local p1, "files":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sget-boolean v1, Lorg/apache/lucene/index/IndexFileDeleter;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/index/IndexFileDeleter;->locked()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 557
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 560
    return-void

    .line 557
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 558
    .local v0, "file":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexFileDeleter;->deleteFile(Ljava/lang/String;)V

    goto :goto_0
.end method

.method deleteNewFiles(Ljava/util/Collection;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 565
    .local p1, "files":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    sget-boolean v1, Lorg/apache/lucene/index/IndexFileDeleter;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/index/IndexFileDeleter;->locked()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 566
    :cond_0
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_2

    .line 580
    return-void

    .line 566
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 573
    .local v0, "fileName":Ljava/lang/String;
    iget-object v1, p0, Lorg/apache/lucene/index/IndexFileDeleter;->refCounts:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lorg/apache/lucene/index/IndexFileDeleter;->refCounts:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/IndexFileDeleter$RefCount;

    iget v1, v1, Lorg/apache/lucene/index/IndexFileDeleter$RefCount;->count:I

    if-nez v1, :cond_1

    .line 574
    :cond_3
    iget-object v1, p0, Lorg/apache/lucene/index/IndexFileDeleter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v3, "IFD"

    invoke-virtual {v1, v3}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 575
    iget-object v1, p0, Lorg/apache/lucene/index/IndexFileDeleter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v3, "IFD"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "delete new file \""

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 577
    :cond_4
    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexFileDeleter;->deleteFile(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public deletePendingFiles()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 401
    sget-boolean v3, Lorg/apache/lucene/index/IndexFileDeleter;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/index/IndexFileDeleter;->locked()Z

    move-result v3

    if-nez v3, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 402
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/index/IndexFileDeleter;->deletable:Ljava/util/List;

    if-eqz v3, :cond_1

    .line 403
    iget-object v1, p0, Lorg/apache/lucene/index/IndexFileDeleter;->deletable:Ljava/util/List;

    .line 404
    .local v1, "oldDeletable":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v3, 0x0

    iput-object v3, p0, Lorg/apache/lucene/index/IndexFileDeleter;->deletable:Ljava/util/List;

    .line 405
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    .line 406
    .local v2, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v2, :cond_2

    .line 413
    .end local v0    # "i":I
    .end local v1    # "oldDeletable":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v2    # "size":I
    :cond_1
    return-void

    .line 407
    .restart local v0    # "i":I
    .restart local v1    # "oldDeletable":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v2    # "size":I
    :cond_2
    iget-object v3, p0, Lorg/apache/lucene/index/IndexFileDeleter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v4, "IFD"

    invoke-virtual {v3, v4}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 408
    iget-object v4, p0, Lorg/apache/lucene/index/IndexFileDeleter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v5, "IFD"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v3, "delete pending file "

    invoke-direct {v6, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v5, v3}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 410
    :cond_3
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {p0, v3}, Lorg/apache/lucene/index/IndexFileDeleter;->deleteFile(Ljava/lang/String;)V

    .line 406
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public exists(Ljava/lang/String;)Z
    .locals 2
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 535
    sget-boolean v1, Lorg/apache/lucene/index/IndexFileDeleter;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/index/IndexFileDeleter;->locked()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 536
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/IndexFileDeleter;->refCounts:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 539
    :cond_1
    :goto_0
    return v0

    :cond_2
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/IndexFileDeleter;->getRefCount(Ljava/lang/String;)Lorg/apache/lucene/index/IndexFileDeleter$RefCount;

    move-result-object v1

    iget v1, v1, Lorg/apache/lucene/index/IndexFileDeleter$RefCount;->count:I

    if-lez v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getLastSegmentInfos()Lorg/apache/lucene/index/SegmentInfos;
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Lorg/apache/lucene/index/IndexFileDeleter;->lastSegmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    return-object v0
.end method

.method incRef(Ljava/lang/String;)V
    .locals 5
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 494
    sget-boolean v1, Lorg/apache/lucene/index/IndexFileDeleter;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/index/IndexFileDeleter;->locked()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 495
    :cond_0
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/IndexFileDeleter;->getRefCount(Ljava/lang/String;)Lorg/apache/lucene/index/IndexFileDeleter$RefCount;

    move-result-object v0

    .line 496
    .local v0, "rc":Lorg/apache/lucene/index/IndexFileDeleter$RefCount;
    iget-object v1, p0, Lorg/apache/lucene/index/IndexFileDeleter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v2, "IFD"

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 497
    sget-boolean v1, Lorg/apache/lucene/index/IndexFileDeleter;->VERBOSE_REF_COUNTS:Z

    if-eqz v1, :cond_1

    .line 498
    iget-object v1, p0, Lorg/apache/lucene/index/IndexFileDeleter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v2, "IFD"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "  IncRef \""

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\": pre-incr count is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v0, Lorg/apache/lucene/index/IndexFileDeleter$RefCount;->count:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 501
    :cond_1
    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexFileDeleter$RefCount;->IncRef()I

    .line 502
    return-void
.end method

.method incRef(Ljava/util/Collection;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 487
    .local p1, "files":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    sget-boolean v1, Lorg/apache/lucene/index/IndexFileDeleter;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/index/IndexFileDeleter;->locked()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 488
    :cond_0
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 491
    return-void

    .line 488
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 489
    .local v0, "file":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexFileDeleter;->incRef(Ljava/lang/String;)V

    goto :goto_0
.end method

.method incRef(Lorg/apache/lucene/index/SegmentInfos;Z)V
    .locals 3
    .param p1, "segmentInfos"    # Lorg/apache/lucene/index/SegmentInfos;
    .param p2, "isCommit"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 478
    sget-boolean v1, Lorg/apache/lucene/index/IndexFileDeleter;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/index/IndexFileDeleter;->locked()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 481
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/IndexFileDeleter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {p1, v1, p2}, Lorg/apache/lucene/index/SegmentInfos;->files(Lorg/apache/lucene/store/Directory;Z)Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 484
    return-void

    .line 481
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 482
    .local v0, "fileName":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexFileDeleter;->incRef(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public refresh()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 360
    sget-boolean v0, Lorg/apache/lucene/index/IndexFileDeleter;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/index/IndexFileDeleter;->locked()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 361
    :cond_0
    iput-object v1, p0, Lorg/apache/lucene/index/IndexFileDeleter;->deletable:Ljava/util/List;

    .line 362
    invoke-virtual {p0, v1}, Lorg/apache/lucene/index/IndexFileDeleter;->refresh(Ljava/lang/String;)V

    .line 363
    return-void
.end method

.method public refresh(Ljava/lang/String;)V
    .locals 10
    .param p1, "segmentName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 324
    sget-boolean v6, Lorg/apache/lucene/index/IndexFileDeleter;->$assertionsDisabled:Z

    if-nez v6, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/index/IndexFileDeleter;->locked()Z

    move-result v6

    if-nez v6, :cond_0

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 326
    :cond_0
    iget-object v6, p0, Lorg/apache/lucene/index/IndexFileDeleter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v6}, Lorg/apache/lucene/store/Directory;->listAll()[Ljava/lang/String;

    move-result-object v1

    .line 329
    .local v1, "files":[Ljava/lang/String;
    if-eqz p1, :cond_1

    .line 330
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 331
    .local v4, "segmentPrefix1":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 337
    .local v5, "segmentPrefix2":Ljava/lang/String;
    :goto_0
    sget-object v6, Lorg/apache/lucene/index/IndexFileNames;->CODEC_FILE_PATTERN:Ljava/util/regex/Pattern;

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    .line 339
    .local v3, "m":Ljava/util/regex/Matcher;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    array-length v6, v1

    if-lt v2, v6, :cond_2

    .line 354
    return-void

    .line 333
    .end local v2    # "i":I
    .end local v3    # "m":Ljava/util/regex/Matcher;
    .end local v4    # "segmentPrefix1":Ljava/lang/String;
    .end local v5    # "segmentPrefix2":Ljava/lang/String;
    :cond_1
    const/4 v4, 0x0

    .line 334
    .restart local v4    # "segmentPrefix1":Ljava/lang/String;
    const/4 v5, 0x0

    .restart local v5    # "segmentPrefix2":Ljava/lang/String;
    goto :goto_0

    .line 340
    .restart local v2    # "i":I
    .restart local v3    # "m":Ljava/util/regex/Matcher;
    :cond_2
    aget-object v0, v1, v2

    .line 341
    .local v0, "fileName":Ljava/lang/String;
    invoke-virtual {v3, v0}, Ljava/util/regex/Matcher;->reset(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    .line 342
    if-eqz p1, :cond_3

    invoke-virtual {v0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_3

    invoke-virtual {v0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 343
    :cond_3
    const-string/jumbo v6, "write.lock"

    invoke-virtual {v0, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_6

    .line 344
    iget-object v6, p0, Lorg/apache/lucene/index/IndexFileDeleter;->refCounts:Ljava/util/Map;

    invoke-interface {v6, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_6

    .line 345
    const-string v6, "segments.gen"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_6

    .line 346
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->matches()Z

    move-result v6

    if-nez v6, :cond_4

    const-string v6, "segments"

    invoke-virtual {v0, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 348
    :cond_4
    iget-object v6, p0, Lorg/apache/lucene/index/IndexFileDeleter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v7, "IFD"

    invoke-virtual {v6, v7}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 349
    iget-object v6, p0, Lorg/apache/lucene/index/IndexFileDeleter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v7, "IFD"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "refresh [prefix="

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]: removing newly created unreferenced file \""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    :cond_5
    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexFileDeleter;->deleteFile(Ljava/lang/String;)V

    .line 339
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method revisitPolicy()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 389
    sget-boolean v0, Lorg/apache/lucene/index/IndexFileDeleter;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/index/IndexFileDeleter;->locked()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 390
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/IndexFileDeleter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v1, "IFD"

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 391
    iget-object v0, p0, Lorg/apache/lucene/index/IndexFileDeleter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v1, "IFD"

    const-string v2, "now revisitPolicy"

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 394
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/index/IndexFileDeleter;->commits:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 395
    iget-object v0, p0, Lorg/apache/lucene/index/IndexFileDeleter;->policy:Lorg/apache/lucene/index/IndexDeletionPolicy;

    iget-object v1, p0, Lorg/apache/lucene/index/IndexFileDeleter;->commits:Ljava/util/List;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/IndexDeletionPolicy;->onCommit(Ljava/util/List;)V

    .line 396
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexFileDeleter;->deleteCommits()V

    .line 398
    :cond_2
    return-void
.end method
