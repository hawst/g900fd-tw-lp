.class final Lorg/apache/lucene/index/DocumentsWriterFlushQueue$GlobalDeletesTicket;
.super Lorg/apache/lucene/index/DocumentsWriterFlushQueue$FlushTicket;
.source "DocumentsWriterFlushQueue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/DocumentsWriterFlushQueue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "GlobalDeletesTicket"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 176
    const-class v0, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/DocumentsWriterFlushQueue$GlobalDeletesTicket;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected constructor <init>(Lorg/apache/lucene/index/FrozenBufferedDeletes;)V
    .locals 0
    .param p1, "frozenDeletes"    # Lorg/apache/lucene/index/FrozenBufferedDeletes;

    .prologue
    .line 179
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/DocumentsWriterFlushQueue$FlushTicket;-><init>(Lorg/apache/lucene/index/FrozenBufferedDeletes;)V

    .line 180
    return-void
.end method


# virtual methods
.method protected canPublish()Z
    .locals 1

    .prologue
    .line 191
    const/4 v0, 0x1

    return v0
.end method

.method protected publish(Lorg/apache/lucene/index/DocumentsWriter;)V
    .locals 2
    .param p1, "writer"    # Lorg/apache/lucene/index/DocumentsWriter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 183
    sget-boolean v0, Lorg/apache/lucene/index/DocumentsWriterFlushQueue$GlobalDeletesTicket;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushQueue$GlobalDeletesTicket;->published:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "ticket was already publised - can not publish twice"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 184
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushQueue$GlobalDeletesTicket;->published:Z

    .line 186
    const/4 v0, 0x0

    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterFlushQueue$GlobalDeletesTicket;->frozenDeletes:Lorg/apache/lucene/index/FrozenBufferedDeletes;

    invoke-virtual {p1, v0, v1}, Lorg/apache/lucene/index/DocumentsWriter;->finishFlush(Lorg/apache/lucene/index/DocumentsWriterPerThread$FlushedSegment;Lorg/apache/lucene/index/FrozenBufferedDeletes;)V

    .line 187
    return-void
.end method
