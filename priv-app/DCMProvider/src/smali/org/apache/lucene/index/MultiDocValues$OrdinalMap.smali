.class public Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;
.super Ljava/lang/Object;
.source "MultiDocValues.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/MultiDocValues;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "OrdinalMap"
.end annotation


# instance fields
.field final globalOrdDeltas:Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;

.field final ordDeltas:[Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;

.field final owner:Ljava/lang/Object;

.field final subIndexes:Lorg/apache/lucene/util/packed/AppendingLongBuffer;


# direct methods
.method public constructor <init>(Ljava/lang/Object;[Lorg/apache/lucene/index/TermsEnum;)V
    .locals 20
    .param p1, "owner"    # Ljava/lang/Object;
    .param p2, "subs"    # [Lorg/apache/lucene/index/TermsEnum;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 292
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 295
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;->owner:Ljava/lang/Object;

    .line 296
    new-instance v15, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;

    invoke-direct {v15}, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;-><init>()V

    move-object/from16 v0, p0

    iput-object v15, v0, Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;->globalOrdDeltas:Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;

    .line 297
    new-instance v15, Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    invoke-direct {v15}, Lorg/apache/lucene/util/packed/AppendingLongBuffer;-><init>()V

    move-object/from16 v0, p0

    iput-object v15, v0, Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;->subIndexes:Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    .line 298
    move-object/from16 v0, p2

    array-length v15, v0

    new-array v15, v15, [Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;

    move-object/from16 v0, p0

    iput-object v15, v0, Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;->ordDeltas:[Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;

    .line 299
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;->ordDeltas:[Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;

    array-length v15, v15

    if-lt v6, v15, :cond_0

    .line 302
    move-object/from16 v0, p2

    array-length v15, v0

    new-array v12, v15, [J

    .line 303
    .local v12, "segmentOrds":[J
    move-object/from16 v0, p2

    array-length v15, v0

    new-array v13, v15, [Lorg/apache/lucene/index/ReaderSlice;

    .line 304
    .local v13, "slices":[Lorg/apache/lucene/index/ReaderSlice;
    array-length v15, v13

    new-array v7, v15, [Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumIndex;

    .line 305
    .local v7, "indexes":[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumIndex;
    const/4 v6, 0x0

    :goto_1
    array-length v15, v13

    if-lt v6, v15, :cond_1

    .line 309
    new-instance v9, Lorg/apache/lucene/index/MultiTermsEnum;

    invoke-direct {v9, v13}, Lorg/apache/lucene/index/MultiTermsEnum;-><init>([Lorg/apache/lucene/index/ReaderSlice;)V

    .line 310
    .local v9, "mte":Lorg/apache/lucene/index/MultiTermsEnum;
    invoke-virtual {v9, v7}, Lorg/apache/lucene/index/MultiTermsEnum;->reset([Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumIndex;)Lorg/apache/lucene/index/TermsEnum;

    .line 311
    const-wide/16 v4, 0x0

    .line 312
    .local v4, "globalOrd":J
    :goto_2
    invoke-virtual {v9}, Lorg/apache/lucene/index/MultiTermsEnum;->next()Lorg/apache/lucene/util/BytesRef;

    move-result-object v15

    if-nez v15, :cond_2

    .line 331
    return-void

    .line 300
    .end local v4    # "globalOrd":J
    .end local v7    # "indexes":[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumIndex;
    .end local v9    # "mte":Lorg/apache/lucene/index/MultiTermsEnum;
    .end local v12    # "segmentOrds":[J
    .end local v13    # "slices":[Lorg/apache/lucene/index/ReaderSlice;
    :cond_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;->ordDeltas:[Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;

    new-instance v16, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;

    invoke-direct/range {v16 .. v16}, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;-><init>()V

    aput-object v16, v15, v6

    .line 299
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 306
    .restart local v7    # "indexes":[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumIndex;
    .restart local v12    # "segmentOrds":[J
    .restart local v13    # "slices":[Lorg/apache/lucene/index/ReaderSlice;
    :cond_1
    new-instance v15, Lorg/apache/lucene/index/ReaderSlice;

    const/16 v16, 0x0

    const/16 v17, 0x0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-direct {v15, v0, v1, v6}, Lorg/apache/lucene/index/ReaderSlice;-><init>(III)V

    aput-object v15, v13, v6

    .line 307
    new-instance v15, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumIndex;

    aget-object v16, p2, v6

    move-object/from16 v0, v16

    invoke-direct {v15, v0, v6}, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumIndex;-><init>(Lorg/apache/lucene/index/TermsEnum;I)V

    aput-object v15, v7, v6

    .line 305
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 313
    .restart local v4    # "globalOrd":J
    .restart local v9    # "mte":Lorg/apache/lucene/index/MultiTermsEnum;
    :cond_2
    invoke-virtual {v9}, Lorg/apache/lucene/index/MultiTermsEnum;->getMatchArray()[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

    move-result-object v8

    .line 314
    .local v8, "matches":[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;
    const/4 v6, 0x0

    :goto_3
    invoke-virtual {v9}, Lorg/apache/lucene/index/MultiTermsEnum;->getMatchCount()I

    move-result v15

    if-lt v6, v15, :cond_3

    .line 329
    const-wide/16 v16, 0x1

    add-long v4, v4, v16

    goto :goto_2

    .line 315
    :cond_3
    aget-object v15, v8, v6

    iget v14, v15, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->index:I

    .line 316
    .local v14, "subIndex":I
    aget-object v15, v8, v6

    iget-object v15, v15, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->terms:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v15}, Lorg/apache/lucene/index/TermsEnum;->ord()J

    move-result-wide v10

    .line 317
    .local v10, "segmentOrd":J
    sub-long v2, v4, v10

    .line 319
    .local v2, "delta":J
    if-nez v6, :cond_4

    .line 320
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;->subIndexes:Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    int-to-long v0, v14

    move-wide/from16 v16, v0

    invoke-virtual/range {v15 .. v17}, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->add(J)V

    .line 321
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;->globalOrdDeltas:Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;

    invoke-virtual {v15, v2, v3}, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;->add(J)V

    .line 324
    :cond_4
    :goto_4
    aget-wide v16, v12, v14

    cmp-long v15, v16, v10

    if-lez v15, :cond_5

    .line 314
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 325
    :cond_5
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;->ordDeltas:[Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;

    aget-object v15, v15, v14

    invoke-virtual {v15, v2, v3}, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;->add(J)V

    .line 326
    aget-wide v16, v12, v14

    const-wide/16 v18, 0x1

    add-long v16, v16, v18

    aput-wide v16, v12, v14

    goto :goto_4
.end method


# virtual methods
.method public getGlobalOrd(IJ)J
    .locals 2
    .param p1, "subIndex"    # I
    .param p2, "segmentOrd"    # J

    .prologue
    .line 338
    iget-object v0, p0, Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;->ordDeltas:[Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;

    aget-object v0, v0, p1

    invoke-virtual {v0, p2, p3}, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;->get(J)J

    move-result-wide v0

    add-long/2addr v0, p2

    return-wide v0
.end method

.method public getSegmentNumber(J)I
    .locals 3
    .param p1, "globalOrd"    # J

    .prologue
    .line 354
    iget-object v0, p0, Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;->subIndexes:Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->get(J)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public getSegmentOrd(IJ)J
    .locals 2
    .param p1, "subIndex"    # I
    .param p2, "globalOrd"    # J

    .prologue
    .line 346
    iget-object v0, p0, Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;->globalOrdDeltas:Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;

    invoke-virtual {v0, p2, p3}, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;->get(J)J

    move-result-wide v0

    sub-long v0, p2, v0

    return-wide v0
.end method

.method public getValueCount()J
    .locals 2

    .prologue
    .line 361
    iget-object v0, p0, Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;->globalOrdDeltas:Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;

    invoke-virtual {v0}, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;->size()J

    move-result-wide v0

    return-wide v0
.end method
