.class Lorg/apache/lucene/index/CoalescedDeletes$2$1;
.super Ljava/lang/Object;
.source "CoalescedDeletes.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/index/CoalescedDeletes$2;->iterator()Ljava/util/Iterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Lorg/apache/lucene/index/BufferedDeletesStream$QueryAndLimit;",
        ">;"
    }
.end annotation


# instance fields
.field private final iter:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<",
            "Ljava/util/Map$Entry",
            "<",
            "Lorg/apache/lucene/search/Query;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field final synthetic this$1:Lorg/apache/lucene/index/CoalescedDeletes$2;


# direct methods
.method constructor <init>(Lorg/apache/lucene/index/CoalescedDeletes$2;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/index/CoalescedDeletes$2$1;->this$1:Lorg/apache/lucene/index/CoalescedDeletes$2;

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    # getter for: Lorg/apache/lucene/index/CoalescedDeletes$2;->this$0:Lorg/apache/lucene/index/CoalescedDeletes;
    invoke-static {p1}, Lorg/apache/lucene/index/CoalescedDeletes$2;->access$0(Lorg/apache/lucene/index/CoalescedDeletes$2;)Lorg/apache/lucene/index/CoalescedDeletes;

    move-result-object v0

    iget-object v0, v0, Lorg/apache/lucene/index/CoalescedDeletes;->queries:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/CoalescedDeletes$2$1;->iter:Ljava/util/Iterator;

    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lorg/apache/lucene/index/CoalescedDeletes$2$1;->iter:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/index/CoalescedDeletes$2$1;->next()Lorg/apache/lucene/index/BufferedDeletesStream$QueryAndLimit;

    move-result-object v0

    return-object v0
.end method

.method public next()Lorg/apache/lucene/index/BufferedDeletesStream$QueryAndLimit;
    .locals 4

    .prologue
    .line 77
    iget-object v1, p0, Lorg/apache/lucene/index/CoalescedDeletes$2$1;->iter:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 78
    .local v0, "ent":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lorg/apache/lucene/search/Query;Ljava/lang/Integer;>;"
    new-instance v3, Lorg/apache/lucene/index/BufferedDeletesStream$QueryAndLimit;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/search/Query;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {v3, v1, v2}, Lorg/apache/lucene/index/BufferedDeletesStream$QueryAndLimit;-><init>(Lorg/apache/lucene/search/Query;I)V

    return-object v3
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 83
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
