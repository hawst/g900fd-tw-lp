.class public final Lorg/apache/lucene/index/CheckIndex$Status$TermVectorStatus;
.super Ljava/lang/Object;
.source "CheckIndex.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/CheckIndex$Status;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TermVectorStatus"
.end annotation


# instance fields
.field public docCount:I

.field public error:Ljava/lang/Throwable;

.field public totVectors:J


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    .line 281
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 285
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/index/CheckIndex$Status$TermVectorStatus;->docCount:I

    .line 288
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/apache/lucene/index/CheckIndex$Status$TermVectorStatus;->totVectors:J

    .line 291
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/index/CheckIndex$Status$TermVectorStatus;->error:Ljava/lang/Throwable;

    .line 282
    return-void
.end method
