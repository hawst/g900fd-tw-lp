.class public abstract Lorg/apache/lucene/index/DocsEnum;
.super Lorg/apache/lucene/search/DocIdSetIterator;
.source "DocsEnum.java"


# static fields
.field public static final FLAG_FREQS:I = 0x1

.field public static final FLAG_NONE:I


# instance fields
.field private atts:Lorg/apache/lucene/util/AttributeSource;


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Lorg/apache/lucene/search/DocIdSetIterator;-><init>()V

    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/index/DocsEnum;->atts:Lorg/apache/lucene/util/AttributeSource;

    .line 49
    return-void
.end method


# virtual methods
.method public attributes()Lorg/apache/lucene/util/AttributeSource;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lorg/apache/lucene/index/DocsEnum;->atts:Lorg/apache/lucene/util/AttributeSource;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/lucene/util/AttributeSource;

    invoke-direct {v0}, Lorg/apache/lucene/util/AttributeSource;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/DocsEnum;->atts:Lorg/apache/lucene/util/AttributeSource;

    .line 66
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/DocsEnum;->atts:Lorg/apache/lucene/util/AttributeSource;

    return-object v0
.end method

.method public abstract freq()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
