.class public Lorg/apache/lucene/index/FilterAtomicReader$FilterDocsEnum;
.super Lorg/apache/lucene/index/DocsEnum;
.source "FilterAtomicReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/FilterAtomicReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FilterDocsEnum"
.end annotation


# instance fields
.field protected final in:Lorg/apache/lucene/index/DocsEnum;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/DocsEnum;)V
    .locals 0
    .param p1, "in"    # Lorg/apache/lucene/index/DocsEnum;

    .prologue
    .line 219
    invoke-direct {p0}, Lorg/apache/lucene/index/DocsEnum;-><init>()V

    .line 220
    iput-object p1, p0, Lorg/apache/lucene/index/FilterAtomicReader$FilterDocsEnum;->in:Lorg/apache/lucene/index/DocsEnum;

    .line 221
    return-void
.end method


# virtual methods
.method public advance(I)I
    .locals 1
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 245
    iget-object v0, p0, Lorg/apache/lucene/index/FilterAtomicReader$FilterDocsEnum;->in:Lorg/apache/lucene/index/DocsEnum;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/DocsEnum;->advance(I)I

    move-result v0

    return v0
.end method

.method public attributes()Lorg/apache/lucene/util/AttributeSource;
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lorg/apache/lucene/index/FilterAtomicReader$FilterDocsEnum;->in:Lorg/apache/lucene/index/DocsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocsEnum;->attributes()Lorg/apache/lucene/util/AttributeSource;

    move-result-object v0

    return-object v0
.end method

.method public cost()J
    .locals 2

    .prologue
    .line 250
    iget-object v0, p0, Lorg/apache/lucene/index/FilterAtomicReader$FilterDocsEnum;->in:Lorg/apache/lucene/index/DocsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocsEnum;->cost()J

    move-result-wide v0

    return-wide v0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, Lorg/apache/lucene/index/FilterAtomicReader$FilterDocsEnum;->in:Lorg/apache/lucene/index/DocsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocsEnum;->docID()I

    move-result v0

    return v0
.end method

.method public freq()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 235
    iget-object v0, p0, Lorg/apache/lucene/index/FilterAtomicReader$FilterDocsEnum;->in:Lorg/apache/lucene/index/DocsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocsEnum;->freq()I

    move-result v0

    return v0
.end method

.method public nextDoc()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 240
    iget-object v0, p0, Lorg/apache/lucene/index/FilterAtomicReader$FilterDocsEnum;->in:Lorg/apache/lucene/index/DocsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocsEnum;->nextDoc()I

    move-result v0

    return v0
.end method
