.class public Lorg/apache/lucene/index/MultiDocValues;
.super Ljava/lang/Object;
.source "MultiDocValues.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/MultiDocValues$MultiSortedDocValues;,
        Lorg/apache/lucene/index/MultiDocValues$MultiSortedSetDocValues;,
        Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-class v0, Lorg/apache/lucene/index/MultiDocValues;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/MultiDocValues;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getBinaryValues(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;)Lorg/apache/lucene/index/BinaryDocValues;
    .locals 10
    .param p0, "r"    # Lorg/apache/lucene/index/IndexReader;
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 145
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->leaves()Ljava/util/List;

    move-result-object v3

    .line 146
    .local v3, "leaves":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/AtomicReaderContext;>;"
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    .line 148
    .local v4, "size":I
    if-nez v4, :cond_1

    .line 173
    :cond_0
    :goto_0
    return-object v8

    .line 150
    :cond_1
    const/4 v9, 0x1

    if-ne v4, v9, :cond_2

    .line 151
    const/4 v8, 0x0

    invoke-interface {v3, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/apache/lucene/index/AtomicReaderContext;

    invoke-virtual {v8}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v8

    invoke-virtual {v8, p1}, Lorg/apache/lucene/index/AtomicReader;->getBinaryDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/BinaryDocValues;

    move-result-object v8

    goto :goto_0

    .line 154
    :cond_2
    const/4 v0, 0x0

    .line 155
    .local v0, "anyReal":Z
    new-array v7, v4, [Lorg/apache/lucene/index/BinaryDocValues;

    .line 156
    .local v7, "values":[Lorg/apache/lucene/index/BinaryDocValues;
    add-int/lit8 v9, v4, 0x1

    new-array v5, v9, [I

    .line 157
    .local v5, "starts":[I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-lt v2, v4, :cond_3

    .line 168
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v9

    aput v9, v5, v4

    .line 170
    if-eqz v0, :cond_0

    .line 173
    new-instance v8, Lorg/apache/lucene/index/MultiDocValues$3;

    invoke-direct {v8, v5, v7}, Lorg/apache/lucene/index/MultiDocValues$3;-><init>([I[Lorg/apache/lucene/index/BinaryDocValues;)V

    goto :goto_0

    .line 158
    :cond_3
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/AtomicReaderContext;

    .line 159
    .local v1, "context":Lorg/apache/lucene/index/AtomicReaderContext;
    invoke-virtual {v1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v9

    invoke-virtual {v9, p1}, Lorg/apache/lucene/index/AtomicReader;->getBinaryDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/BinaryDocValues;

    move-result-object v6

    .line 160
    .local v6, "v":Lorg/apache/lucene/index/BinaryDocValues;
    if-nez v6, :cond_4

    .line 161
    sget-object v6, Lorg/apache/lucene/index/BinaryDocValues;->EMPTY:Lorg/apache/lucene/index/BinaryDocValues;

    .line 165
    :goto_2
    aput-object v6, v7, v2

    .line 166
    iget v9, v1, Lorg/apache/lucene/index/AtomicReaderContext;->docBase:I

    aput v9, v5, v2

    .line 157
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 163
    :cond_4
    const/4 v0, 0x1

    goto :goto_2
.end method

.method public static getNormValues(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;)Lorg/apache/lucene/index/NumericDocValues;
    .locals 11
    .param p0, "r"    # Lorg/apache/lucene/index/IndexReader;
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 55
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->leaves()Ljava/util/List;

    move-result-object v4

    .line 56
    .local v4, "leaves":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/AtomicReaderContext;>;"
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    .line 57
    .local v5, "size":I
    if-nez v5, :cond_1

    .line 85
    :cond_0
    :goto_0
    return-object v9

    .line 59
    :cond_1
    const/4 v10, 0x1

    if-ne v5, v10, :cond_2

    .line 60
    const/4 v9, 0x0

    invoke-interface {v4, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/apache/lucene/index/AtomicReaderContext;

    invoke-virtual {v9}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v9

    invoke-virtual {v9, p1}, Lorg/apache/lucene/index/AtomicReader;->getNormValues(Ljava/lang/String;)Lorg/apache/lucene/index/NumericDocValues;

    move-result-object v9

    goto :goto_0

    .line 62
    :cond_2
    invoke-static {p0}, Lorg/apache/lucene/index/MultiFields;->getMergedFieldInfos(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/index/FieldInfos;

    move-result-object v10

    invoke-virtual {v10, p1}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(Ljava/lang/String;)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v2

    .line 63
    .local v2, "fi":Lorg/apache/lucene/index/FieldInfo;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lorg/apache/lucene/index/FieldInfo;->hasNorms()Z

    move-result v10

    if-eqz v10, :cond_0

    .line 67
    const/4 v0, 0x0

    .line 68
    .local v0, "anyReal":Z
    new-array v8, v5, [Lorg/apache/lucene/index/NumericDocValues;

    .line 69
    .local v8, "values":[Lorg/apache/lucene/index/NumericDocValues;
    add-int/lit8 v9, v5, 0x1

    new-array v6, v9, [I

    .line 70
    .local v6, "starts":[I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-lt v3, v5, :cond_3

    .line 81
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v9

    aput v9, v6, v5

    .line 83
    sget-boolean v9, Lorg/apache/lucene/index/MultiDocValues;->$assertionsDisabled:Z

    if-nez v9, :cond_5

    if-nez v0, :cond_5

    new-instance v9, Ljava/lang/AssertionError;

    invoke-direct {v9}, Ljava/lang/AssertionError;-><init>()V

    throw v9

    .line 71
    :cond_3
    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/AtomicReaderContext;

    .line 72
    .local v1, "context":Lorg/apache/lucene/index/AtomicReaderContext;
    invoke-virtual {v1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v9

    invoke-virtual {v9, p1}, Lorg/apache/lucene/index/AtomicReader;->getNormValues(Ljava/lang/String;)Lorg/apache/lucene/index/NumericDocValues;

    move-result-object v7

    .line 73
    .local v7, "v":Lorg/apache/lucene/index/NumericDocValues;
    if-nez v7, :cond_4

    .line 74
    sget-object v7, Lorg/apache/lucene/index/NumericDocValues;->EMPTY:Lorg/apache/lucene/index/NumericDocValues;

    .line 78
    :goto_2
    aput-object v7, v8, v3

    .line 79
    iget v9, v1, Lorg/apache/lucene/index/AtomicReaderContext;->docBase:I

    aput v9, v6, v3

    .line 70
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 76
    :cond_4
    const/4 v0, 0x1

    goto :goto_2

    .line 85
    .end local v1    # "context":Lorg/apache/lucene/index/AtomicReaderContext;
    .end local v7    # "v":Lorg/apache/lucene/index/NumericDocValues;
    :cond_5
    new-instance v9, Lorg/apache/lucene/index/MultiDocValues$1;

    invoke-direct {v9, v6, v8}, Lorg/apache/lucene/index/MultiDocValues$1;-><init>([I[Lorg/apache/lucene/index/NumericDocValues;)V

    goto :goto_0
.end method

.method public static getNumericValues(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;)Lorg/apache/lucene/index/NumericDocValues;
    .locals 10
    .param p0, "r"    # Lorg/apache/lucene/index/IndexReader;
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 101
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->leaves()Ljava/util/List;

    move-result-object v3

    .line 102
    .local v3, "leaves":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/AtomicReaderContext;>;"
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    .line 103
    .local v4, "size":I
    if-nez v4, :cond_1

    .line 128
    :cond_0
    :goto_0
    return-object v8

    .line 105
    :cond_1
    const/4 v9, 0x1

    if-ne v4, v9, :cond_2

    .line 106
    const/4 v8, 0x0

    invoke-interface {v3, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/apache/lucene/index/AtomicReaderContext;

    invoke-virtual {v8}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v8

    invoke-virtual {v8, p1}, Lorg/apache/lucene/index/AtomicReader;->getNumericDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/NumericDocValues;

    move-result-object v8

    goto :goto_0

    .line 109
    :cond_2
    const/4 v0, 0x0

    .line 110
    .local v0, "anyReal":Z
    new-array v7, v4, [Lorg/apache/lucene/index/NumericDocValues;

    .line 111
    .local v7, "values":[Lorg/apache/lucene/index/NumericDocValues;
    add-int/lit8 v9, v4, 0x1

    new-array v5, v9, [I

    .line 112
    .local v5, "starts":[I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-lt v2, v4, :cond_3

    .line 123
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v9

    aput v9, v5, v4

    .line 125
    if-eqz v0, :cond_0

    .line 128
    new-instance v8, Lorg/apache/lucene/index/MultiDocValues$2;

    invoke-direct {v8, v5, v7}, Lorg/apache/lucene/index/MultiDocValues$2;-><init>([I[Lorg/apache/lucene/index/NumericDocValues;)V

    goto :goto_0

    .line 113
    :cond_3
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/AtomicReaderContext;

    .line 114
    .local v1, "context":Lorg/apache/lucene/index/AtomicReaderContext;
    invoke-virtual {v1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v9

    invoke-virtual {v9, p1}, Lorg/apache/lucene/index/AtomicReader;->getNumericDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/NumericDocValues;

    move-result-object v6

    .line 115
    .local v6, "v":Lorg/apache/lucene/index/NumericDocValues;
    if-nez v6, :cond_4

    .line 116
    sget-object v6, Lorg/apache/lucene/index/NumericDocValues;->EMPTY:Lorg/apache/lucene/index/NumericDocValues;

    .line 120
    :goto_2
    aput-object v6, v7, v2

    .line 121
    iget v9, v1, Lorg/apache/lucene/index/AtomicReaderContext;->docBase:I

    aput v9, v5, v2

    .line 112
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 118
    :cond_4
    const/4 v0, 0x1

    goto :goto_2
.end method

.method public static getSortedSetValues(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;)Lorg/apache/lucene/index/SortedSetDocValues;
    .locals 12
    .param p0, "r"    # Lorg/apache/lucene/index/IndexReader;
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    .line 234
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->leaves()Ljava/util/List;

    move-result-object v4

    .line 235
    .local v4, "leaves":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/AtomicReaderContext;>;"
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    .line 237
    .local v6, "size":I
    if-nez v6, :cond_1

    .line 267
    :cond_0
    :goto_0
    return-object v10

    .line 239
    :cond_1
    const/4 v11, 0x1

    if-ne v6, v11, :cond_2

    .line 240
    const/4 v10, 0x0

    invoke-interface {v4, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lorg/apache/lucene/index/AtomicReaderContext;

    invoke-virtual {v10}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v10

    invoke-virtual {v10, p1}, Lorg/apache/lucene/index/AtomicReader;->getSortedSetDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/SortedSetDocValues;

    move-result-object v10

    goto :goto_0

    .line 243
    :cond_2
    const/4 v0, 0x0

    .line 244
    .local v0, "anyReal":Z
    new-array v9, v6, [Lorg/apache/lucene/index/SortedSetDocValues;

    .line 245
    .local v9, "values":[Lorg/apache/lucene/index/SortedSetDocValues;
    add-int/lit8 v11, v6, 0x1

    new-array v7, v11, [I

    .line 246
    .local v7, "starts":[I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-lt v3, v6, :cond_3

    .line 257
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v11

    aput v11, v7, v6

    .line 259
    if-eqz v0, :cond_0

    .line 262
    array-length v10, v9

    new-array v2, v10, [Lorg/apache/lucene/index/TermsEnum;

    .line 263
    .local v2, "enums":[Lorg/apache/lucene/index/TermsEnum;
    const/4 v3, 0x0

    :goto_2
    array-length v10, v9

    if-lt v3, v10, :cond_5

    .line 266
    new-instance v5, Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;

    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->getCoreCacheKey()Ljava/lang/Object;

    move-result-object v10

    invoke-direct {v5, v10, v2}, Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;-><init>(Ljava/lang/Object;[Lorg/apache/lucene/index/TermsEnum;)V

    .line 267
    .local v5, "mapping":Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;
    new-instance v10, Lorg/apache/lucene/index/MultiDocValues$MultiSortedSetDocValues;

    invoke-direct {v10, v9, v7, v5}, Lorg/apache/lucene/index/MultiDocValues$MultiSortedSetDocValues;-><init>([Lorg/apache/lucene/index/SortedSetDocValues;[ILorg/apache/lucene/index/MultiDocValues$OrdinalMap;)V

    goto :goto_0

    .line 247
    .end local v2    # "enums":[Lorg/apache/lucene/index/TermsEnum;
    .end local v5    # "mapping":Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;
    :cond_3
    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/AtomicReaderContext;

    .line 248
    .local v1, "context":Lorg/apache/lucene/index/AtomicReaderContext;
    invoke-virtual {v1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v11

    invoke-virtual {v11, p1}, Lorg/apache/lucene/index/AtomicReader;->getSortedSetDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/SortedSetDocValues;

    move-result-object v8

    .line 249
    .local v8, "v":Lorg/apache/lucene/index/SortedSetDocValues;
    if-nez v8, :cond_4

    .line 250
    sget-object v8, Lorg/apache/lucene/index/SortedSetDocValues;->EMPTY:Lorg/apache/lucene/index/SortedSetDocValues;

    .line 254
    :goto_3
    aput-object v8, v9, v3

    .line 255
    iget v11, v1, Lorg/apache/lucene/index/AtomicReaderContext;->docBase:I

    aput v11, v7, v3

    .line 246
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 252
    :cond_4
    const/4 v0, 0x1

    goto :goto_3

    .line 264
    .end local v1    # "context":Lorg/apache/lucene/index/AtomicReaderContext;
    .end local v8    # "v":Lorg/apache/lucene/index/SortedSetDocValues;
    .restart local v2    # "enums":[Lorg/apache/lucene/index/TermsEnum;
    :cond_5
    aget-object v10, v9, v3

    invoke-virtual {v10}, Lorg/apache/lucene/index/SortedSetDocValues;->termsEnum()Lorg/apache/lucene/index/TermsEnum;

    move-result-object v10

    aput-object v10, v2, v3

    .line 263
    add-int/lit8 v3, v3, 0x1

    goto :goto_2
.end method

.method public static getSortedValues(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;)Lorg/apache/lucene/index/SortedDocValues;
    .locals 12
    .param p0, "r"    # Lorg/apache/lucene/index/IndexReader;
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    .line 190
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->leaves()Ljava/util/List;

    move-result-object v4

    .line 191
    .local v4, "leaves":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/AtomicReaderContext;>;"
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    .line 193
    .local v6, "size":I
    if-nez v6, :cond_1

    .line 223
    :cond_0
    :goto_0
    return-object v10

    .line 195
    :cond_1
    const/4 v11, 0x1

    if-ne v6, v11, :cond_2

    .line 196
    const/4 v10, 0x0

    invoke-interface {v4, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lorg/apache/lucene/index/AtomicReaderContext;

    invoke-virtual {v10}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v10

    invoke-virtual {v10, p1}, Lorg/apache/lucene/index/AtomicReader;->getSortedDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/SortedDocValues;

    move-result-object v10

    goto :goto_0

    .line 199
    :cond_2
    const/4 v0, 0x0

    .line 200
    .local v0, "anyReal":Z
    new-array v9, v6, [Lorg/apache/lucene/index/SortedDocValues;

    .line 201
    .local v9, "values":[Lorg/apache/lucene/index/SortedDocValues;
    add-int/lit8 v11, v6, 0x1

    new-array v7, v11, [I

    .line 202
    .local v7, "starts":[I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-lt v3, v6, :cond_3

    .line 213
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v11

    aput v11, v7, v6

    .line 215
    if-eqz v0, :cond_0

    .line 218
    array-length v10, v9

    new-array v2, v10, [Lorg/apache/lucene/index/TermsEnum;

    .line 219
    .local v2, "enums":[Lorg/apache/lucene/index/TermsEnum;
    const/4 v3, 0x0

    :goto_2
    array-length v10, v9

    if-lt v3, v10, :cond_5

    .line 222
    new-instance v5, Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;

    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->getCoreCacheKey()Ljava/lang/Object;

    move-result-object v10

    invoke-direct {v5, v10, v2}, Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;-><init>(Ljava/lang/Object;[Lorg/apache/lucene/index/TermsEnum;)V

    .line 223
    .local v5, "mapping":Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;
    new-instance v10, Lorg/apache/lucene/index/MultiDocValues$MultiSortedDocValues;

    invoke-direct {v10, v9, v7, v5}, Lorg/apache/lucene/index/MultiDocValues$MultiSortedDocValues;-><init>([Lorg/apache/lucene/index/SortedDocValues;[ILorg/apache/lucene/index/MultiDocValues$OrdinalMap;)V

    goto :goto_0

    .line 203
    .end local v2    # "enums":[Lorg/apache/lucene/index/TermsEnum;
    .end local v5    # "mapping":Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;
    :cond_3
    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/AtomicReaderContext;

    .line 204
    .local v1, "context":Lorg/apache/lucene/index/AtomicReaderContext;
    invoke-virtual {v1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v11

    invoke-virtual {v11, p1}, Lorg/apache/lucene/index/AtomicReader;->getSortedDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/SortedDocValues;

    move-result-object v8

    .line 205
    .local v8, "v":Lorg/apache/lucene/index/SortedDocValues;
    if-nez v8, :cond_4

    .line 206
    sget-object v8, Lorg/apache/lucene/index/SortedDocValues;->EMPTY:Lorg/apache/lucene/index/SortedDocValues;

    .line 210
    :goto_3
    aput-object v8, v9, v3

    .line 211
    iget v11, v1, Lorg/apache/lucene/index/AtomicReaderContext;->docBase:I

    aput v11, v7, v3

    .line 202
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 208
    :cond_4
    const/4 v0, 0x1

    goto :goto_3

    .line 220
    .end local v1    # "context":Lorg/apache/lucene/index/AtomicReaderContext;
    .end local v8    # "v":Lorg/apache/lucene/index/SortedDocValues;
    .restart local v2    # "enums":[Lorg/apache/lucene/index/TermsEnum;
    :cond_5
    aget-object v10, v9, v3

    invoke-virtual {v10}, Lorg/apache/lucene/index/SortedDocValues;->termsEnum()Lorg/apache/lucene/index/TermsEnum;

    move-result-object v10

    aput-object v10, v2, v3

    .line 219
    add-int/lit8 v3, v3, 0x1

    goto :goto_2
.end method
