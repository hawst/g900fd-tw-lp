.class public Lorg/apache/lucene/index/MergeState;
.super Ljava/lang/Object;
.source "MergeState.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/MergeState$CheckAbort;,
        Lorg/apache/lucene/index/MergeState$DocMap;,
        Lorg/apache/lucene/index/MergeState$NoDelDocMap;
    }
.end annotation


# instance fields
.field public final checkAbort:Lorg/apache/lucene/index/MergeState$CheckAbort;

.field public docBase:[I

.field public docMaps:[Lorg/apache/lucene/index/MergeState$DocMap;

.field public fieldInfos:Lorg/apache/lucene/index/FieldInfos;

.field public final infoStream:Lorg/apache/lucene/util/InfoStream;

.field public matchedCount:I

.field public matchingSegmentReaders:[Lorg/apache/lucene/index/SegmentReader;

.field public final readers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/AtomicReader;",
            ">;"
        }
    .end annotation
.end field

.field public final segmentInfo:Lorg/apache/lucene/index/SegmentInfo;


# direct methods
.method constructor <init>(Ljava/util/List;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/util/InfoStream;Lorg/apache/lucene/index/MergeState$CheckAbort;)V
    .locals 0
    .param p2, "segmentInfo"    # Lorg/apache/lucene/index/SegmentInfo;
    .param p3, "infoStream"    # Lorg/apache/lucene/util/InfoStream;
    .param p4, "checkAbort"    # Lorg/apache/lucene/index/MergeState$CheckAbort;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/AtomicReader;",
            ">;",
            "Lorg/apache/lucene/index/SegmentInfo;",
            "Lorg/apache/lucene/util/InfoStream;",
            "Lorg/apache/lucene/index/MergeState$CheckAbort;",
            ")V"
        }
    .end annotation

    .prologue
    .line 165
    .local p1, "readers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/AtomicReader;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 166
    iput-object p1, p0, Lorg/apache/lucene/index/MergeState;->readers:Ljava/util/List;

    .line 167
    iput-object p2, p0, Lorg/apache/lucene/index/MergeState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    .line 168
    iput-object p3, p0, Lorg/apache/lucene/index/MergeState;->infoStream:Lorg/apache/lucene/util/InfoStream;

    .line 169
    iput-object p4, p0, Lorg/apache/lucene/index/MergeState;->checkAbort:Lorg/apache/lucene/index/MergeState$CheckAbort;

    .line 170
    return-void
.end method
