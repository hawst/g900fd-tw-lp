.class public Lorg/apache/lucene/index/MultiReader;
.super Lorg/apache/lucene/index/BaseCompositeReader;
.source "MultiReader.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/index/BaseCompositeReader",
        "<",
        "Lorg/apache/lucene/index/IndexReader;",
        ">;"
    }
.end annotation


# instance fields
.field private final closeSubReaders:Z


# direct methods
.method public varargs constructor <init>([Lorg/apache/lucene/index/IndexReader;)V
    .locals 1
    .param p1, "subReaders"    # [Lorg/apache/lucene/index/IndexReader;

    .prologue
    .line 49
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/index/MultiReader;-><init>([Lorg/apache/lucene/index/IndexReader;Z)V

    .line 50
    return-void
.end method

.method public constructor <init>([Lorg/apache/lucene/index/IndexReader;Z)V
    .locals 2
    .param p1, "subReaders"    # [Lorg/apache/lucene/index/IndexReader;
    .param p2, "closeSubReaders"    # Z

    .prologue
    .line 59
    invoke-virtual {p1}, [Lorg/apache/lucene/index/IndexReader;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lorg/apache/lucene/index/IndexReader;

    invoke-direct {p0, v1}, Lorg/apache/lucene/index/BaseCompositeReader;-><init>([Lorg/apache/lucene/index/IndexReader;)V

    .line 60
    iput-boolean p2, p0, Lorg/apache/lucene/index/MultiReader;->closeSubReaders:Z

    .line 61
    if-nez p2, :cond_0

    .line 62
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-lt v0, v1, :cond_1

    .line 66
    .end local v0    # "i":I
    :cond_0
    return-void

    .line 63
    .restart local v0    # "i":I
    :cond_1
    aget-object v1, p1, v0

    invoke-virtual {v1}, Lorg/apache/lucene/index/IndexReader;->incRef()V

    .line 62
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method protected declared-synchronized doClose()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 70
    monitor-enter p0

    const/4 v1, 0x0

    .line 71
    .local v1, "ioe":Ljava/io/IOException;
    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/index/MultiReader;->getSequentialSubReaders()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 83
    if-eqz v1, :cond_3

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 70
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 71
    :cond_1
    :try_start_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/index/IndexReader;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 73
    .local v2, "r":Lorg/apache/lucene/index/IndexReader;
    :try_start_2
    iget-boolean v4, p0, Lorg/apache/lucene/index/MultiReader;->closeSubReaders:Z

    if-eqz v4, :cond_2

    .line 74
    invoke-virtual {v2}, Lorg/apache/lucene/index/IndexReader;->close()V

    goto :goto_0

    .line 78
    :catch_0
    move-exception v0

    .line 79
    .local v0, "e":Ljava/io/IOException;
    if-nez v1, :cond_0

    move-object v1, v0

    goto :goto_0

    .line 76
    .end local v0    # "e":Ljava/io/IOException;
    :cond_2
    invoke-virtual {v2}, Lorg/apache/lucene/index/IndexReader;->decRef()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 84
    .end local v2    # "r":Lorg/apache/lucene/index/IndexReader;
    :cond_3
    monitor-exit p0

    return-void
.end method
