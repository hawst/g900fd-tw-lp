.class public abstract Lorg/apache/lucene/index/AtomicReader;
.super Lorg/apache/lucene/index/IndexReader;
.source "AtomicReader.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final readerContext:Lorg/apache/lucene/index/AtomicReaderContext;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    const-class v0, Lorg/apache/lucene/index/AtomicReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/AtomicReader;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexReader;-><init>()V

    .line 49
    new-instance v0, Lorg/apache/lucene/index/AtomicReaderContext;

    invoke-direct {v0, p0}, Lorg/apache/lucene/index/AtomicReaderContext;-><init>(Lorg/apache/lucene/index/AtomicReader;)V

    iput-object v0, p0, Lorg/apache/lucene/index/AtomicReader;->readerContext:Lorg/apache/lucene/index/AtomicReaderContext;

    .line 55
    return-void
.end method


# virtual methods
.method public final docFreq(Lorg/apache/lucene/index/Term;)I
    .locals 6
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 85
    invoke-virtual {p0}, Lorg/apache/lucene/index/AtomicReader;->fields()Lorg/apache/lucene/index/Fields;

    move-result-object v0

    .line 86
    .local v0, "fields":Lorg/apache/lucene/index/Fields;
    if-nez v0, :cond_1

    .line 97
    :cond_0
    :goto_0
    return v3

    .line 89
    :cond_1
    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lorg/apache/lucene/index/Fields;->terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v1

    .line 90
    .local v1, "terms":Lorg/apache/lucene/index/Terms;
    if-eqz v1, :cond_0

    .line 93
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v2

    .line 94
    .local v2, "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->bytes()Lorg/apache/lucene/util/BytesRef;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v2, v4, v5}, Lorg/apache/lucene/index/TermsEnum;->seekExact(Lorg/apache/lucene/util/BytesRef;Z)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 95
    invoke-virtual {v2}, Lorg/apache/lucene/index/TermsEnum;->docFreq()I

    move-result v3

    goto :goto_0
.end method

.method public abstract fields()Lorg/apache/lucene/index/Fields;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getBinaryDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/BinaryDocValues;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public final getContext()Lorg/apache/lucene/index/AtomicReaderContext;
    .locals 1

    .prologue
    .line 59
    invoke-virtual {p0}, Lorg/apache/lucene/index/AtomicReader;->ensureOpen()V

    .line 60
    iget-object v0, p0, Lorg/apache/lucene/index/AtomicReader;->readerContext:Lorg/apache/lucene/index/AtomicReaderContext;

    return-object v0
.end method

.method public bridge synthetic getContext()Lorg/apache/lucene/index/IndexReaderContext;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/index/AtomicReader;->getContext()Lorg/apache/lucene/index/AtomicReaderContext;

    move-result-object v0

    return-object v0
.end method

.method public final getDocCount(Ljava/lang/String;)I
    .locals 2
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 135
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/AtomicReader;->terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v0

    .line 136
    .local v0, "terms":Lorg/apache/lucene/index/Terms;
    if-nez v0, :cond_0

    .line 137
    const/4 v1, 0x0

    .line 139
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Lorg/apache/lucene/index/Terms;->getDocCount()I

    move-result v1

    goto :goto_0
.end method

.method public abstract getFieldInfos()Lorg/apache/lucene/index/FieldInfos;
.end method

.method public abstract getLiveDocs()Lorg/apache/lucene/util/Bits;
.end method

.method public abstract getNormValues(Ljava/lang/String;)Lorg/apache/lucene/index/NumericDocValues;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getNumericDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/NumericDocValues;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getSortedDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/SortedDocValues;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getSortedSetDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/SortedSetDocValues;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public final getSumDocFreq(Ljava/lang/String;)J
    .locals 4
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 126
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/AtomicReader;->terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v0

    .line 127
    .local v0, "terms":Lorg/apache/lucene/index/Terms;
    if-nez v0, :cond_0

    .line 128
    const-wide/16 v2, 0x0

    .line 130
    :goto_0
    return-wide v2

    :cond_0
    invoke-virtual {v0}, Lorg/apache/lucene/index/Terms;->getSumDocFreq()J

    move-result-wide v2

    goto :goto_0
.end method

.method public final getSumTotalTermFreq(Ljava/lang/String;)J
    .locals 4
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 144
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/AtomicReader;->terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v0

    .line 145
    .local v0, "terms":Lorg/apache/lucene/index/Terms;
    if-nez v0, :cond_0

    .line 146
    const-wide/16 v2, 0x0

    .line 148
    :goto_0
    return-wide v2

    :cond_0
    invoke-virtual {v0}, Lorg/apache/lucene/index/Terms;->getSumTotalTermFreq()J

    move-result-wide v2

    goto :goto_0
.end method

.method public final hasNorms(Ljava/lang/String;)Z
    .locals 2
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 70
    invoke-virtual {p0}, Lorg/apache/lucene/index/AtomicReader;->ensureOpen()V

    .line 72
    invoke-virtual {p0}, Lorg/apache/lucene/index/AtomicReader;->getFieldInfos()Lorg/apache/lucene/index/FieldInfos;

    move-result-object v1

    invoke-virtual {v1, p1}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(Ljava/lang/String;)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v0

    .line 73
    .local v0, "fi":Lorg/apache/lucene/index/FieldInfo;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfo;->hasNorms()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final termDocsEnum(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/DocsEnum;
    .locals 6
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 165
    sget-boolean v4, Lorg/apache/lucene/index/AtomicReader;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 166
    :cond_0
    sget-boolean v4, Lorg/apache/lucene/index/AtomicReader;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->bytes()Lorg/apache/lucene/util/BytesRef;

    move-result-object v4

    if-nez v4, :cond_1

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 167
    :cond_1
    invoke-virtual {p0}, Lorg/apache/lucene/index/AtomicReader;->fields()Lorg/apache/lucene/index/Fields;

    move-result-object v0

    .line 168
    .local v0, "fields":Lorg/apache/lucene/index/Fields;
    if-eqz v0, :cond_2

    .line 169
    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lorg/apache/lucene/index/Fields;->terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v1

    .line 170
    .local v1, "terms":Lorg/apache/lucene/index/Terms;
    if-eqz v1, :cond_2

    .line 171
    invoke-virtual {v1, v3}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v2

    .line 172
    .local v2, "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->bytes()Lorg/apache/lucene/util/BytesRef;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v2, v4, v5}, Lorg/apache/lucene/index/TermsEnum;->seekExact(Lorg/apache/lucene/util/BytesRef;Z)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 173
    invoke-virtual {p0}, Lorg/apache/lucene/index/AtomicReader;->getLiveDocs()Lorg/apache/lucene/util/Bits;

    move-result-object v4

    invoke-virtual {v2, v4, v3}, Lorg/apache/lucene/index/TermsEnum;->docs(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;)Lorg/apache/lucene/index/DocsEnum;

    move-result-object v3

    .line 177
    .end local v1    # "terms":Lorg/apache/lucene/index/Terms;
    .end local v2    # "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    :cond_2
    return-object v3
.end method

.method public final termPositionsEnum(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .locals 6
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 185
    sget-boolean v4, Lorg/apache/lucene/index/AtomicReader;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 186
    :cond_0
    sget-boolean v4, Lorg/apache/lucene/index/AtomicReader;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->bytes()Lorg/apache/lucene/util/BytesRef;

    move-result-object v4

    if-nez v4, :cond_1

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 187
    :cond_1
    invoke-virtual {p0}, Lorg/apache/lucene/index/AtomicReader;->fields()Lorg/apache/lucene/index/Fields;

    move-result-object v0

    .line 188
    .local v0, "fields":Lorg/apache/lucene/index/Fields;
    if-eqz v0, :cond_2

    .line 189
    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lorg/apache/lucene/index/Fields;->terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v1

    .line 190
    .local v1, "terms":Lorg/apache/lucene/index/Terms;
    if-eqz v1, :cond_2

    .line 191
    invoke-virtual {v1, v3}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v2

    .line 192
    .local v2, "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->bytes()Lorg/apache/lucene/util/BytesRef;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v2, v4, v5}, Lorg/apache/lucene/index/TermsEnum;->seekExact(Lorg/apache/lucene/util/BytesRef;Z)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 193
    invoke-virtual {p0}, Lorg/apache/lucene/index/AtomicReader;->getLiveDocs()Lorg/apache/lucene/util/Bits;

    move-result-object v4

    invoke-virtual {v2, v4, v3}, Lorg/apache/lucene/index/TermsEnum;->docsAndPositions(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsAndPositionsEnum;)Lorg/apache/lucene/index/DocsAndPositionsEnum;

    move-result-object v3

    .line 197
    .end local v1    # "terms":Lorg/apache/lucene/index/Terms;
    .end local v2    # "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    :cond_2
    return-object v3
.end method

.method public final terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;
    .locals 2
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 153
    invoke-virtual {p0}, Lorg/apache/lucene/index/AtomicReader;->fields()Lorg/apache/lucene/index/Fields;

    move-result-object v0

    .line 154
    .local v0, "fields":Lorg/apache/lucene/index/Fields;
    if-nez v0, :cond_0

    .line 155
    const/4 v1, 0x0

    .line 157
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/Fields;->terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v1

    goto :goto_0
.end method

.method public final totalTermFreq(Lorg/apache/lucene/index/Term;)J
    .locals 7
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    .line 108
    invoke-virtual {p0}, Lorg/apache/lucene/index/AtomicReader;->fields()Lorg/apache/lucene/index/Fields;

    move-result-object v0

    .line 109
    .local v0, "fields":Lorg/apache/lucene/index/Fields;
    if-nez v0, :cond_1

    .line 120
    :cond_0
    :goto_0
    return-wide v4

    .line 112
    :cond_1
    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lorg/apache/lucene/index/Fields;->terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v1

    .line 113
    .local v1, "terms":Lorg/apache/lucene/index/Terms;
    if-eqz v1, :cond_0

    .line 116
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v2

    .line 117
    .local v2, "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->bytes()Lorg/apache/lucene/util/BytesRef;

    move-result-object v3

    const/4 v6, 0x1

    invoke-virtual {v2, v3, v6}, Lorg/apache/lucene/index/TermsEnum;->seekExact(Lorg/apache/lucene/util/BytesRef;Z)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 118
    invoke-virtual {v2}, Lorg/apache/lucene/index/TermsEnum;->totalTermFreq()J

    move-result-wide v4

    goto :goto_0
.end method
