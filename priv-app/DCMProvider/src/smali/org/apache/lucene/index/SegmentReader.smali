.class public final Lorg/apache/lucene/index/SegmentReader;
.super Lorg/apache/lucene/index/AtomicReader;
.source "SegmentReader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/SegmentReader$CoreClosedListener;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final core:Lorg/apache/lucene/index/SegmentCoreReaders;

.field private final liveDocs:Lorg/apache/lucene/util/Bits;

.field private final numDocs:I

.field private final si:Lorg/apache/lucene/index/SegmentInfoPerCommit;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lorg/apache/lucene/index/SegmentReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/SegmentReader;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/index/SegmentInfoPerCommit;ILorg/apache/lucene/store/IOContext;)V
    .locals 7
    .param p1, "si"    # Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .param p2, "termInfosIndexDivisor"    # I
    .param p3, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 54
    invoke-direct {p0}, Lorg/apache/lucene/index/AtomicReader;-><init>()V

    .line 55
    iput-object p1, p0, Lorg/apache/lucene/index/SegmentReader;->si:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .line 56
    new-instance v0, Lorg/apache/lucene/index/SegmentCoreReaders;

    iget-object v1, p1, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v2, v1, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p3

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/index/SegmentCoreReaders;-><init>(Lorg/apache/lucene/index/SegmentReader;Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfoPerCommit;Lorg/apache/lucene/store/IOContext;I)V

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    .line 57
    const/4 v6, 0x0

    .line 59
    .local v6, "success":Z
    :try_start_0
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->hasDeletions()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 61
    iget-object v0, p1, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentInfo;->getCodec()Lorg/apache/lucene/codecs/Codec;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/Codec;->liveDocsFormat()Lorg/apache/lucene/codecs/LiveDocsFormat;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentReader;->directory()Lorg/apache/lucene/store/Directory;

    move-result-object v1

    new-instance v2, Lorg/apache/lucene/store/IOContext;

    sget-object v3, Lorg/apache/lucene/store/IOContext;->READ:Lorg/apache/lucene/store/IOContext;

    const/4 v4, 0x1

    invoke-direct {v2, v3, v4}, Lorg/apache/lucene/store/IOContext;-><init>(Lorg/apache/lucene/store/IOContext;Z)V

    invoke-virtual {v0, v1, p1, v2}, Lorg/apache/lucene/codecs/LiveDocsFormat;->readLiveDocs(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfoPerCommit;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/util/Bits;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->liveDocs:Lorg/apache/lucene/util/Bits;

    .line 66
    :goto_0
    iget-object v0, p1, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v0

    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->getDelCount()I

    move-result v1

    sub-int/2addr v0, v1

    iput v0, p0, Lorg/apache/lucene/index/SegmentReader;->numDocs:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 67
    const/4 v6, 0x1

    .line 74
    if-nez v6, :cond_0

    .line 75
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentCoreReaders;->decRef()V

    .line 78
    :cond_0
    return-void

    .line 63
    :cond_1
    :try_start_1
    sget-boolean v0, Lorg/apache/lucene/index/SegmentReader;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->getDelCount()I

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 68
    :catchall_0
    move-exception v0

    .line 74
    if-nez v6, :cond_2

    .line 75
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentCoreReaders;->decRef()V

    .line 77
    :cond_2
    throw v0

    .line 64
    :cond_3
    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->liveDocs:Lorg/apache/lucene/util/Bits;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/index/SegmentInfoPerCommit;Lorg/apache/lucene/index/SegmentCoreReaders;Lorg/apache/lucene/store/IOContext;)V
    .locals 3
    .param p1, "si"    # Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .param p2, "core"    # Lorg/apache/lucene/index/SegmentCoreReaders;
    .param p3, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 84
    .line 85
    iget-object v0, p1, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentInfo;->getCodec()Lorg/apache/lucene/codecs/Codec;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/Codec;->liveDocsFormat()Lorg/apache/lucene/codecs/LiveDocsFormat;

    move-result-object v0

    iget-object v1, p1, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v1, v1, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, v1, p1, p3}, Lorg/apache/lucene/codecs/LiveDocsFormat;->readLiveDocs(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfoPerCommit;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/util/Bits;

    move-result-object v0

    .line 86
    iget-object v1, p1, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v1

    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->getDelCount()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-direct {p0, p1, p2, v0, v1}, Lorg/apache/lucene/index/SegmentReader;-><init>(Lorg/apache/lucene/index/SegmentInfoPerCommit;Lorg/apache/lucene/index/SegmentCoreReaders;Lorg/apache/lucene/util/Bits;I)V

    .line 87
    return-void
.end method

.method constructor <init>(Lorg/apache/lucene/index/SegmentInfoPerCommit;Lorg/apache/lucene/index/SegmentCoreReaders;Lorg/apache/lucene/util/Bits;I)V
    .locals 1
    .param p1, "si"    # Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .param p2, "core"    # Lorg/apache/lucene/index/SegmentCoreReaders;
    .param p3, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p4, "numDocs"    # I

    .prologue
    .line 93
    invoke-direct {p0}, Lorg/apache/lucene/index/AtomicReader;-><init>()V

    .line 94
    iput-object p1, p0, Lorg/apache/lucene/index/SegmentReader;->si:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .line 95
    iput-object p2, p0, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    .line 96
    invoke-virtual {p2}, Lorg/apache/lucene/index/SegmentCoreReaders;->incRef()V

    .line 98
    sget-boolean v0, Lorg/apache/lucene/index/SegmentReader;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p3, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 99
    :cond_0
    iput-object p3, p0, Lorg/apache/lucene/index/SegmentReader;->liveDocs:Lorg/apache/lucene/util/Bits;

    .line 101
    iput p4, p0, Lorg/apache/lucene/index/SegmentReader;->numDocs:I

    .line 102
    return-void
.end method

.method private checkBounds(I)V
    .locals 3
    .param p1, "docID"    # I

    .prologue
    .line 173
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentReader;->maxDoc()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 174
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "docID must be >= 0 and < maxDoc="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentReader;->maxDoc()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " (got docID="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 176
    :cond_1
    return-void
.end method


# virtual methods
.method public addCoreClosedListener(Lorg/apache/lucene/index/SegmentReader$CoreClosedListener;)V
    .locals 1
    .param p1, "listener"    # Lorg/apache/lucene/index/SegmentReader$CoreClosedListener;

    .prologue
    .line 277
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentReader;->ensureOpen()V

    .line 278
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/SegmentCoreReaders;->addCoreClosedListener(Lorg/apache/lucene/index/SegmentReader$CoreClosedListener;)V

    .line 279
    return-void
.end method

.method public directory()Lorg/apache/lucene/store/Directory;
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->si:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    return-object v0
.end method

.method protected doClose()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 113
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentCoreReaders;->decRef()V

    .line 114
    return-void
.end method

.method public document(ILorg/apache/lucene/index/StoredFieldVisitor;)V
    .locals 1
    .param p1, "docID"    # I
    .param p2, "visitor"    # Lorg/apache/lucene/index/StoredFieldVisitor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 132
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/SegmentReader;->checkBounds(I)V

    .line 133
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentReader;->getFieldsReader()Lorg/apache/lucene/codecs/StoredFieldsReader;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/codecs/StoredFieldsReader;->visitDocument(ILorg/apache/lucene/index/StoredFieldVisitor;)V

    .line 134
    return-void
.end method

.method public fields()Lorg/apache/lucene/index/Fields;
    .locals 1

    .prologue
    .line 138
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentReader;->ensureOpen()V

    .line 139
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentCoreReaders;->fields:Lorg/apache/lucene/codecs/FieldsProducer;

    return-object v0
.end method

.method public getBinaryDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/BinaryDocValues;
    .locals 1
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 234
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentReader;->ensureOpen()V

    .line 235
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/SegmentCoreReaders;->getBinaryDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/BinaryDocValues;

    move-result-object v0

    return-object v0
.end method

.method public getCombinedCoreAndDeletesKey()Ljava/lang/Object;
    .locals 0

    .prologue
    .line 217
    return-object p0
.end method

.method public getCoreCacheKey()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    return-object v0
.end method

.method public getFieldInfos()Lorg/apache/lucene/index/FieldInfos;
    .locals 1

    .prologue
    .line 118
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentReader;->ensureOpen()V

    .line 119
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentCoreReaders;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    return-object v0
.end method

.method public getFieldsReader()Lorg/apache/lucene/codecs/StoredFieldsReader;
    .locals 1

    .prologue
    .line 126
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentReader;->ensureOpen()V

    .line 127
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentCoreReaders;->fieldsReaderLocal:Lorg/apache/lucene/util/CloseableThreadLocal;

    invoke-virtual {v0}, Lorg/apache/lucene/util/CloseableThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/codecs/StoredFieldsReader;

    return-object v0
.end method

.method public getLiveDocs()Lorg/apache/lucene/util/Bits;
    .locals 1

    .prologue
    .line 106
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentReader;->ensureOpen()V

    .line 107
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->liveDocs:Lorg/apache/lucene/util/Bits;

    return-object v0
.end method

.method public getNormValues(Ljava/lang/String;)Lorg/apache/lucene/index/NumericDocValues;
    .locals 1
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 252
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentReader;->ensureOpen()V

    .line 253
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/SegmentCoreReaders;->getNormValues(Ljava/lang/String;)Lorg/apache/lucene/index/NumericDocValues;

    move-result-object v0

    return-object v0
.end method

.method public getNumericDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/NumericDocValues;
    .locals 1
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 228
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentReader;->ensureOpen()V

    .line 229
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/SegmentCoreReaders;->getNumericDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/NumericDocValues;

    move-result-object v0

    return-object v0
.end method

.method public getSegmentInfo()Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->si:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    return-object v0
.end method

.method public getSegmentName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->si:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getSortedDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/SortedDocValues;
    .locals 1
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 240
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentReader;->ensureOpen()V

    .line 241
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/SegmentCoreReaders;->getSortedDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/SortedDocValues;

    move-result-object v0

    return-object v0
.end method

.method public getSortedSetDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/SortedSetDocValues;
    .locals 1
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 246
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentReader;->ensureOpen()V

    .line 247
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/SegmentCoreReaders;->getSortedSetDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/SortedSetDocValues;

    move-result-object v0

    return-object v0
.end method

.method public getTermInfosIndexDivisor()I
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    iget v0, v0, Lorg/apache/lucene/index/SegmentCoreReaders;->termsIndexDivisor:I

    return v0
.end method

.method public getTermVectors(I)Lorg/apache/lucene/index/Fields;
    .locals 2
    .param p1, "docID"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 164
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentReader;->getTermVectorsReader()Lorg/apache/lucene/codecs/TermVectorsReader;

    move-result-object v0

    .line 165
    .local v0, "termVectorsReader":Lorg/apache/lucene/codecs/TermVectorsReader;
    if-nez v0, :cond_0

    .line 166
    const/4 v1, 0x0

    .line 169
    :goto_0
    return-object v1

    .line 168
    :cond_0
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/SegmentReader;->checkBounds(I)V

    .line 169
    invoke-virtual {v0, p1}, Lorg/apache/lucene/codecs/TermVectorsReader;->get(I)Lorg/apache/lucene/index/Fields;

    move-result-object v1

    goto :goto_0
.end method

.method public getTermVectorsReader()Lorg/apache/lucene/codecs/TermVectorsReader;
    .locals 1

    .prologue
    .line 158
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentReader;->ensureOpen()V

    .line 159
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentCoreReaders;->termVectorsLocal:Lorg/apache/lucene/util/CloseableThreadLocal;

    invoke-virtual {v0}, Lorg/apache/lucene/util/CloseableThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/codecs/TermVectorsReader;

    return-object v0
.end method

.method public maxDoc()I
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->si:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v0

    return v0
.end method

.method public numDocs()I
    .locals 1

    .prologue
    .line 145
    iget v0, p0, Lorg/apache/lucene/index/SegmentReader;->numDocs:I

    return v0
.end method

.method public removeCoreClosedListener(Lorg/apache/lucene/index/SegmentReader$CoreClosedListener;)V
    .locals 1
    .param p1, "listener"    # Lorg/apache/lucene/index/SegmentReader$CoreClosedListener;

    .prologue
    .line 283
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentReader;->ensureOpen()V

    .line 284
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/SegmentCoreReaders;->removeCoreClosedListener(Lorg/apache/lucene/index/SegmentReader$CoreClosedListener;)V

    .line 285
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 182
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->si:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    iget-object v1, p0, Lorg/apache/lucene/index/SegmentReader;->si:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    iget-object v1, v1, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v1, v1, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    iget-object v2, p0, Lorg/apache/lucene/index/SegmentReader;->si:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    iget-object v2, v2, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v2}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v2

    iget v3, p0, Lorg/apache/lucene/index/SegmentReader;->numDocs:I

    sub-int/2addr v2, v3

    iget-object v3, p0, Lorg/apache/lucene/index/SegmentReader;->si:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    invoke-virtual {v3}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->getDelCount()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->toString(Lorg/apache/lucene/store/Directory;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
