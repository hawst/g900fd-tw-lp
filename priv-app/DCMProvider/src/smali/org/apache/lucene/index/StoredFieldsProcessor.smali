.class final Lorg/apache/lucene/index/StoredFieldsProcessor;
.super Lorg/apache/lucene/index/StoredFieldsConsumer;
.source "StoredFieldsProcessor.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field allocCount:I

.field final codec:Lorg/apache/lucene/codecs/Codec;

.field final docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

.field final docWriter:Lorg/apache/lucene/index/DocumentsWriterPerThread;

.field private fieldInfos:[Lorg/apache/lucene/index/FieldInfo;

.field fieldsWriter:Lorg/apache/lucene/codecs/StoredFieldsWriter;

.field freeCount:I

.field lastDocID:I

.field private numStoredFields:I

.field private storedFields:[Lorg/apache/lucene/index/IndexableField;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lorg/apache/lucene/index/StoredFieldsProcessor;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/StoredFieldsProcessor;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/index/DocumentsWriterPerThread;)V
    .locals 1
    .param p1, "docWriter"    # Lorg/apache/lucene/index/DocumentsWriterPerThread;

    .prologue
    .line 40
    invoke-direct {p0}, Lorg/apache/lucene/index/StoredFieldsConsumer;-><init>()V

    .line 41
    iput-object p1, p0, Lorg/apache/lucene/index/StoredFieldsProcessor;->docWriter:Lorg/apache/lucene/index/DocumentsWriterPerThread;

    .line 42
    iget-object v0, p1, Lorg/apache/lucene/index/DocumentsWriterPerThread;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    iput-object v0, p0, Lorg/apache/lucene/index/StoredFieldsProcessor;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    .line 43
    iget-object v0, p1, Lorg/apache/lucene/index/DocumentsWriterPerThread;->codec:Lorg/apache/lucene/codecs/Codec;

    iput-object v0, p0, Lorg/apache/lucene/index/StoredFieldsProcessor;->codec:Lorg/apache/lucene/codecs/Codec;

    .line 44
    return-void
.end method

.method private declared-synchronized initFieldsWriter(Lorg/apache/lucene/store/IOContext;)V
    .locals 3
    .param p1, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 85
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/StoredFieldsProcessor;->fieldsWriter:Lorg/apache/lucene/codecs/StoredFieldsWriter;

    if-nez v0, :cond_0

    .line 86
    iget-object v0, p0, Lorg/apache/lucene/index/StoredFieldsProcessor;->codec:Lorg/apache/lucene/codecs/Codec;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/Codec;->storedFieldsFormat()Lorg/apache/lucene/codecs/StoredFieldsFormat;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/index/StoredFieldsProcessor;->docWriter:Lorg/apache/lucene/index/DocumentsWriterPerThread;

    iget-object v1, v1, Lorg/apache/lucene/index/DocumentsWriterPerThread;->directory:Lorg/apache/lucene/store/TrackingDirectoryWrapper;

    iget-object v2, p0, Lorg/apache/lucene/index/StoredFieldsProcessor;->docWriter:Lorg/apache/lucene/index/DocumentsWriterPerThread;

    invoke-virtual {v2}, Lorg/apache/lucene/index/DocumentsWriterPerThread;->getSegmentInfo()Lorg/apache/lucene/index/SegmentInfo;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, Lorg/apache/lucene/codecs/StoredFieldsFormat;->fieldsWriter(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/codecs/StoredFieldsWriter;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/StoredFieldsProcessor;->fieldsWriter:Lorg/apache/lucene/codecs/StoredFieldsWriter;

    .line 87
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/index/StoredFieldsProcessor;->lastDocID:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 89
    :cond_0
    monitor-exit p0

    return-void

    .line 85
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method abort()V
    .locals 1

    .prologue
    .line 95
    invoke-virtual {p0}, Lorg/apache/lucene/index/StoredFieldsProcessor;->reset()V

    .line 97
    iget-object v0, p0, Lorg/apache/lucene/index/StoredFieldsProcessor;->fieldsWriter:Lorg/apache/lucene/codecs/StoredFieldsWriter;

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lorg/apache/lucene/index/StoredFieldsProcessor;->fieldsWriter:Lorg/apache/lucene/codecs/StoredFieldsWriter;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/StoredFieldsWriter;->abort()V

    .line 99
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/index/StoredFieldsProcessor;->fieldsWriter:Lorg/apache/lucene/codecs/StoredFieldsWriter;

    .line 100
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/index/StoredFieldsProcessor;->lastDocID:I

    .line 102
    :cond_0
    return-void
.end method

.method public addField(ILorg/apache/lucene/index/IndexableField;Lorg/apache/lucene/index/FieldInfo;)V
    .locals 6
    .param p1, "docID"    # I
    .param p2, "field"    # Lorg/apache/lucene/index/IndexableField;
    .param p3, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;

    .prologue
    const/4 v5, 0x0

    .line 137
    invoke-interface {p2}, Lorg/apache/lucene/index/IndexableField;->fieldType()Lorg/apache/lucene/index/IndexableFieldType;

    move-result-object v3

    invoke-interface {v3}, Lorg/apache/lucene/index/IndexableFieldType;->stored()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 138
    iget v3, p0, Lorg/apache/lucene/index/StoredFieldsProcessor;->numStoredFields:I

    iget-object v4, p0, Lorg/apache/lucene/index/StoredFieldsProcessor;->storedFields:[Lorg/apache/lucene/index/IndexableField;

    array-length v4, v4

    if-ne v3, v4, :cond_0

    .line 139
    iget v3, p0, Lorg/apache/lucene/index/StoredFieldsProcessor;->numStoredFields:I

    add-int/lit8 v3, v3, 0x1

    sget v4, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_REF:I

    invoke-static {v3, v4}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v2

    .line 140
    .local v2, "newSize":I
    new-array v0, v2, [Lorg/apache/lucene/index/IndexableField;

    .line 141
    .local v0, "newArray":[Lorg/apache/lucene/index/IndexableField;
    iget-object v3, p0, Lorg/apache/lucene/index/StoredFieldsProcessor;->storedFields:[Lorg/apache/lucene/index/IndexableField;

    iget v4, p0, Lorg/apache/lucene/index/StoredFieldsProcessor;->numStoredFields:I

    invoke-static {v3, v5, v0, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 142
    iput-object v0, p0, Lorg/apache/lucene/index/StoredFieldsProcessor;->storedFields:[Lorg/apache/lucene/index/IndexableField;

    .line 144
    new-array v1, v2, [Lorg/apache/lucene/index/FieldInfo;

    .line 145
    .local v1, "newInfoArray":[Lorg/apache/lucene/index/FieldInfo;
    iget-object v3, p0, Lorg/apache/lucene/index/StoredFieldsProcessor;->fieldInfos:[Lorg/apache/lucene/index/FieldInfo;

    iget v4, p0, Lorg/apache/lucene/index/StoredFieldsProcessor;->numStoredFields:I

    invoke-static {v3, v5, v1, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 146
    iput-object v1, p0, Lorg/apache/lucene/index/StoredFieldsProcessor;->fieldInfos:[Lorg/apache/lucene/index/FieldInfo;

    .line 149
    .end local v0    # "newArray":[Lorg/apache/lucene/index/IndexableField;
    .end local v1    # "newInfoArray":[Lorg/apache/lucene/index/FieldInfo;
    .end local v2    # "newSize":I
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/index/StoredFieldsProcessor;->storedFields:[Lorg/apache/lucene/index/IndexableField;

    iget v4, p0, Lorg/apache/lucene/index/StoredFieldsProcessor;->numStoredFields:I

    aput-object p2, v3, v4

    .line 150
    iget-object v3, p0, Lorg/apache/lucene/index/StoredFieldsProcessor;->fieldInfos:[Lorg/apache/lucene/index/FieldInfo;

    iget v4, p0, Lorg/apache/lucene/index/StoredFieldsProcessor;->numStoredFields:I

    aput-object p3, v3, v4

    .line 151
    iget v3, p0, Lorg/apache/lucene/index/StoredFieldsProcessor;->numStoredFields:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/lucene/index/StoredFieldsProcessor;->numStoredFields:I

    .line 153
    sget-boolean v3, Lorg/apache/lucene/index/StoredFieldsProcessor;->$assertionsDisabled:Z

    if-nez v3, :cond_1

    iget-object v3, p0, Lorg/apache/lucene/index/StoredFieldsProcessor;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    const-string v4, "StoredFieldsWriterPerThread.processFields.writeField"

    invoke-virtual {v3, v4}, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;->testPoint(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 155
    :cond_1
    return-void
.end method

.method fill(I)V
    .locals 2
    .param p1, "docID"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 108
    :goto_0
    iget v0, p0, Lorg/apache/lucene/index/StoredFieldsProcessor;->lastDocID:I

    if-lt v0, p1, :cond_0

    .line 113
    return-void

    .line 109
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/StoredFieldsProcessor;->fieldsWriter:Lorg/apache/lucene/codecs/StoredFieldsWriter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/apache/lucene/codecs/StoredFieldsWriter;->startDocument(I)V

    .line 110
    iget v0, p0, Lorg/apache/lucene/index/StoredFieldsProcessor;->lastDocID:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/index/StoredFieldsProcessor;->lastDocID:I

    .line 111
    iget-object v0, p0, Lorg/apache/lucene/index/StoredFieldsProcessor;->fieldsWriter:Lorg/apache/lucene/codecs/StoredFieldsWriter;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/StoredFieldsWriter;->finishDocument()V

    goto :goto_0
.end method

.method finishDocument()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 117
    sget-boolean v1, Lorg/apache/lucene/index/StoredFieldsProcessor;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/index/StoredFieldsProcessor;->docWriter:Lorg/apache/lucene/index/DocumentsWriterPerThread;

    iget-object v1, v1, Lorg/apache/lucene/index/DocumentsWriterPerThread;->writer:Lorg/apache/lucene/index/IndexWriter;

    const-string v2, "StoredFieldsWriter.finishDocument start"

    invoke-virtual {v1, v2}, Lorg/apache/lucene/index/IndexWriter;->testPoint(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 119
    :cond_0
    sget-object v1, Lorg/apache/lucene/store/IOContext;->DEFAULT:Lorg/apache/lucene/store/IOContext;

    invoke-direct {p0, v1}, Lorg/apache/lucene/index/StoredFieldsProcessor;->initFieldsWriter(Lorg/apache/lucene/store/IOContext;)V

    .line 120
    iget-object v1, p0, Lorg/apache/lucene/index/StoredFieldsProcessor;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    iget v1, v1, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;->docID:I

    invoke-virtual {p0, v1}, Lorg/apache/lucene/index/StoredFieldsProcessor;->fill(I)V

    .line 122
    iget-object v1, p0, Lorg/apache/lucene/index/StoredFieldsProcessor;->fieldsWriter:Lorg/apache/lucene/codecs/StoredFieldsWriter;

    if-eqz v1, :cond_1

    iget v1, p0, Lorg/apache/lucene/index/StoredFieldsProcessor;->numStoredFields:I

    if-lez v1, :cond_1

    .line 123
    iget-object v1, p0, Lorg/apache/lucene/index/StoredFieldsProcessor;->fieldsWriter:Lorg/apache/lucene/codecs/StoredFieldsWriter;

    iget v2, p0, Lorg/apache/lucene/index/StoredFieldsProcessor;->numStoredFields:I

    invoke-virtual {v1, v2}, Lorg/apache/lucene/codecs/StoredFieldsWriter;->startDocument(I)V

    .line 124
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lorg/apache/lucene/index/StoredFieldsProcessor;->numStoredFields:I

    if-lt v0, v1, :cond_2

    .line 127
    iget-object v1, p0, Lorg/apache/lucene/index/StoredFieldsProcessor;->fieldsWriter:Lorg/apache/lucene/codecs/StoredFieldsWriter;

    invoke-virtual {v1}, Lorg/apache/lucene/codecs/StoredFieldsWriter;->finishDocument()V

    .line 128
    iget v1, p0, Lorg/apache/lucene/index/StoredFieldsProcessor;->lastDocID:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/index/StoredFieldsProcessor;->lastDocID:I

    .line 131
    .end local v0    # "i":I
    :cond_1
    invoke-virtual {p0}, Lorg/apache/lucene/index/StoredFieldsProcessor;->reset()V

    .line 132
    sget-boolean v1, Lorg/apache/lucene/index/StoredFieldsProcessor;->$assertionsDisabled:Z

    if-nez v1, :cond_3

    iget-object v1, p0, Lorg/apache/lucene/index/StoredFieldsProcessor;->docWriter:Lorg/apache/lucene/index/DocumentsWriterPerThread;

    iget-object v1, v1, Lorg/apache/lucene/index/DocumentsWriterPerThread;->writer:Lorg/apache/lucene/index/IndexWriter;

    const-string v2, "StoredFieldsWriter.finishDocument end"

    invoke-virtual {v1, v2}, Lorg/apache/lucene/index/IndexWriter;->testPoint(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 125
    .restart local v0    # "i":I
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/index/StoredFieldsProcessor;->fieldsWriter:Lorg/apache/lucene/codecs/StoredFieldsWriter;

    iget-object v2, p0, Lorg/apache/lucene/index/StoredFieldsProcessor;->fieldInfos:[Lorg/apache/lucene/index/FieldInfo;

    aget-object v2, v2, v0

    iget-object v3, p0, Lorg/apache/lucene/index/StoredFieldsProcessor;->storedFields:[Lorg/apache/lucene/index/IndexableField;

    aget-object v3, v3, v0

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/codecs/StoredFieldsWriter;->writeField(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/index/IndexableField;)V

    .line 124
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 133
    .end local v0    # "i":I
    :cond_3
    return-void
.end method

.method public flush(Lorg/apache/lucene/index/SegmentWriteState;)V
    .locals 5
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentWriteState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 63
    iget-object v1, p1, Lorg/apache/lucene/index/SegmentWriteState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v0

    .line 65
    .local v0, "numDocs":I
    if-lez v0, :cond_0

    .line 69
    iget-object v1, p1, Lorg/apache/lucene/index/SegmentWriteState;->context:Lorg/apache/lucene/store/IOContext;

    invoke-direct {p0, v1}, Lorg/apache/lucene/index/StoredFieldsProcessor;->initFieldsWriter(Lorg/apache/lucene/store/IOContext;)V

    .line 70
    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/StoredFieldsProcessor;->fill(I)V

    .line 73
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/StoredFieldsProcessor;->fieldsWriter:Lorg/apache/lucene/codecs/StoredFieldsWriter;

    if-eqz v1, :cond_1

    .line 75
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/index/StoredFieldsProcessor;->fieldsWriter:Lorg/apache/lucene/codecs/StoredFieldsWriter;

    iget-object v2, p1, Lorg/apache/lucene/index/SegmentWriteState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v1, v2, v0}, Lorg/apache/lucene/codecs/StoredFieldsWriter;->finish(Lorg/apache/lucene/index/FieldInfos;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 77
    iget-object v1, p0, Lorg/apache/lucene/index/StoredFieldsProcessor;->fieldsWriter:Lorg/apache/lucene/codecs/StoredFieldsWriter;

    invoke-virtual {v1}, Lorg/apache/lucene/codecs/StoredFieldsWriter;->close()V

    .line 78
    iput-object v4, p0, Lorg/apache/lucene/index/StoredFieldsProcessor;->fieldsWriter:Lorg/apache/lucene/codecs/StoredFieldsWriter;

    .line 79
    iput v3, p0, Lorg/apache/lucene/index/StoredFieldsProcessor;->lastDocID:I

    .line 82
    :cond_1
    return-void

    .line 76
    :catchall_0
    move-exception v1

    .line 77
    iget-object v2, p0, Lorg/apache/lucene/index/StoredFieldsProcessor;->fieldsWriter:Lorg/apache/lucene/codecs/StoredFieldsWriter;

    invoke-virtual {v2}, Lorg/apache/lucene/codecs/StoredFieldsWriter;->close()V

    .line 78
    iput-object v4, p0, Lorg/apache/lucene/index/StoredFieldsProcessor;->fieldsWriter:Lorg/apache/lucene/codecs/StoredFieldsWriter;

    .line 79
    iput v3, p0, Lorg/apache/lucene/index/StoredFieldsProcessor;->lastDocID:I

    .line 80
    throw v1
.end method

.method public reset()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 51
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/index/StoredFieldsProcessor;->numStoredFields:I

    .line 52
    new-array v0, v1, [Lorg/apache/lucene/index/IndexableField;

    iput-object v0, p0, Lorg/apache/lucene/index/StoredFieldsProcessor;->storedFields:[Lorg/apache/lucene/index/IndexableField;

    .line 53
    new-array v0, v1, [Lorg/apache/lucene/index/FieldInfo;

    iput-object v0, p0, Lorg/apache/lucene/index/StoredFieldsProcessor;->fieldInfos:[Lorg/apache/lucene/index/FieldInfo;

    .line 54
    return-void
.end method

.method public startDocument()V
    .locals 0

    .prologue
    .line 58
    invoke-virtual {p0}, Lorg/apache/lucene/index/StoredFieldsProcessor;->reset()V

    .line 59
    return-void
.end method
