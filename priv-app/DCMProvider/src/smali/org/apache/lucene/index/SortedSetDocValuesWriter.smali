.class Lorg/apache/lucene/index/SortedSetDocValuesWriter;
.super Lorg/apache/lucene/index/DocValuesWriter;
.source "SortedSetDocValuesWriter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/SortedSetDocValuesWriter$OrdCountIterator;,
        Lorg/apache/lucene/index/SortedSetDocValuesWriter$OrdsIterator;,
        Lorg/apache/lucene/index/SortedSetDocValuesWriter$ValuesIterator;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private bytesUsed:J

.field private currentDoc:I

.field private currentUpto:I

.field private currentValues:[I

.field private final fieldInfo:Lorg/apache/lucene/index/FieldInfo;

.field final hash:Lorg/apache/lucene/util/BytesRefHash;

.field private final iwBytesUsed:Lorg/apache/lucene/util/Counter;

.field private maxCount:I

.field private pending:Lorg/apache/lucene/util/packed/AppendingLongBuffer;

.field private pendingCounts:Lorg/apache/lucene/util/packed/AppendingLongBuffer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/util/Counter;)V
    .locals 4
    .param p1, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;
    .param p2, "iwBytesUsed"    # Lorg/apache/lucene/util/Counter;

    .prologue
    const/16 v3, 0x10

    const/4 v1, 0x0

    .line 51
    invoke-direct {p0}, Lorg/apache/lucene/index/DocValuesWriter;-><init>()V

    .line 47
    const/16 v0, 0x8

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->currentValues:[I

    .line 48
    iput v1, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->currentUpto:I

    .line 49
    iput v1, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->maxCount:I

    .line 52
    iput-object p1, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    .line 53
    iput-object p2, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->iwBytesUsed:Lorg/apache/lucene/util/Counter;

    .line 54
    new-instance v0, Lorg/apache/lucene/util/BytesRefHash;

    .line 55
    new-instance v1, Lorg/apache/lucene/util/ByteBlockPool;

    .line 56
    new-instance v2, Lorg/apache/lucene/util/ByteBlockPool$DirectTrackingAllocator;

    invoke-direct {v2, p2}, Lorg/apache/lucene/util/ByteBlockPool$DirectTrackingAllocator;-><init>(Lorg/apache/lucene/util/Counter;)V

    .line 55
    invoke-direct {v1, v2}, Lorg/apache/lucene/util/ByteBlockPool;-><init>(Lorg/apache/lucene/util/ByteBlockPool$Allocator;)V

    .line 58
    new-instance v2, Lorg/apache/lucene/util/BytesRefHash$DirectBytesStartArray;

    invoke-direct {v2, v3, p2}, Lorg/apache/lucene/util/BytesRefHash$DirectBytesStartArray;-><init>(ILorg/apache/lucene/util/Counter;)V

    invoke-direct {v0, v1, v3, v2}, Lorg/apache/lucene/util/BytesRefHash;-><init>(Lorg/apache/lucene/util/ByteBlockPool;ILorg/apache/lucene/util/BytesRefHash$BytesStartArray;)V

    .line 54
    iput-object v0, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->hash:Lorg/apache/lucene/util/BytesRefHash;

    .line 59
    new-instance v0, Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    invoke-direct {v0}, Lorg/apache/lucene/util/packed/AppendingLongBuffer;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->pending:Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    .line 60
    new-instance v0, Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    invoke-direct {v0}, Lorg/apache/lucene/util/packed/AppendingLongBuffer;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->pendingCounts:Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    .line 61
    iget-object v0, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->pending:Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    invoke-virtual {v0}, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->ramBytesUsed()J

    move-result-wide v0

    iget-object v2, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->pendingCounts:Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    invoke-virtual {v2}, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->ramBytesUsed()J

    move-result-wide v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->bytesUsed:J

    .line 62
    iget-wide v0, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->bytesUsed:J

    invoke-virtual {p2, v0, v1}, Lorg/apache/lucene/util/Counter;->addAndGet(J)J

    .line 63
    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/index/SortedSetDocValuesWriter;)Lorg/apache/lucene/util/packed/AppendingLongBuffer;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->pending:Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    return-object v0
.end method

.method static synthetic access$1(Lorg/apache/lucene/index/SortedSetDocValuesWriter;)Lorg/apache/lucene/util/packed/AppendingLongBuffer;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->pendingCounts:Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    return-object v0
.end method

.method private addOneValue(Lorg/apache/lucene/util/BytesRef;)V
    .locals 4
    .param p1, "value"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 119
    iget-object v1, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->hash:Lorg/apache/lucene/util/BytesRefHash;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/util/BytesRefHash;->add(Lorg/apache/lucene/util/BytesRef;)I

    move-result v0

    .line 120
    .local v0, "termID":I
    if-gez v0, :cond_1

    .line 121
    neg-int v1, v0

    add-int/lit8 v0, v1, -0x1

    .line 130
    :goto_0
    iget v1, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->currentUpto:I

    iget-object v2, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->currentValues:[I

    array-length v2, v2

    if-ne v1, v2, :cond_0

    .line 131
    iget-object v1, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->currentValues:[I

    iget-object v2, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->currentValues:[I

    array-length v2, v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v1, v2}, Lorg/apache/lucene/util/ArrayUtil;->grow([II)[I

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->currentValues:[I

    .line 134
    iget-object v1, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->iwBytesUsed:Lorg/apache/lucene/util/Counter;

    iget-object v2, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->currentValues:[I

    array-length v2, v2

    iget v3, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->currentUpto:I

    sub-int/2addr v2, v3

    mul-int/lit8 v2, v2, 0x2

    mul-int/lit8 v2, v2, 0x4

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/util/Counter;->addAndGet(J)J

    .line 137
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->currentValues:[I

    iget v2, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->currentUpto:I

    aput v0, v1, v2

    .line 138
    iget v1, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->currentUpto:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->currentUpto:I

    .line 139
    return-void

    .line 127
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->iwBytesUsed:Lorg/apache/lucene/util/Counter;

    const-wide/16 v2, 0x8

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/util/Counter;->addAndGet(J)J

    goto :goto_0
.end method

.method private finishCurrentDoc()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 89
    iget-object v4, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->currentValues:[I

    iget v5, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->currentUpto:I

    invoke-static {v4, v8, v5}, Ljava/util/Arrays;->sort([III)V

    .line 90
    const/4 v2, -0x1

    .line 91
    .local v2, "lastValue":I
    const/4 v0, 0x0

    .line 92
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v4, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->currentUpto:I

    if-lt v1, v4, :cond_0

    .line 102
    iget-object v4, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->pendingCounts:Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    int-to-long v6, v0

    invoke-virtual {v4, v6, v7}, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->add(J)V

    .line 103
    iget v4, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->maxCount:I

    invoke-static {v4, v0}, Ljava/lang/Math;->max(II)I

    move-result v4

    iput v4, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->maxCount:I

    .line 104
    iput v8, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->currentUpto:I

    .line 105
    iget v4, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->currentDoc:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->currentDoc:I

    .line 106
    return-void

    .line 93
    :cond_0
    iget-object v4, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->currentValues:[I

    aget v3, v4, v1

    .line 95
    .local v3, "termID":I
    if-eq v3, v2, :cond_1

    .line 96
    iget-object v4, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->pending:Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    int-to-long v6, v3

    invoke-virtual {v4, v6, v7}, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->add(J)V

    .line 97
    add-int/lit8 v0, v0, 0x1

    .line 99
    :cond_1
    move v2, v3

    .line 92
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private updateBytesUsed()V
    .locals 6

    .prologue
    .line 142
    iget-object v2, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->pending:Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    invoke-virtual {v2}, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->ramBytesUsed()J

    move-result-wide v2

    iget-object v4, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->pendingCounts:Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    invoke-virtual {v4}, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->ramBytesUsed()J

    move-result-wide v4

    add-long v0, v2, v4

    .line 143
    .local v0, "newBytesUsed":J
    iget-object v2, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->iwBytesUsed:Lorg/apache/lucene/util/Counter;

    iget-wide v4, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->bytesUsed:J

    sub-long v4, v0, v4

    invoke-virtual {v2, v4, v5}, Lorg/apache/lucene/util/Counter;->addAndGet(J)J

    .line 144
    iput-wide v0, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->bytesUsed:J

    .line 145
    return-void
.end method


# virtual methods
.method public abort()V
    .locals 0

    .prologue
    .line 190
    return-void
.end method

.method public addValue(ILorg/apache/lucene/util/BytesRef;)V
    .locals 4
    .param p1, "docID"    # I
    .param p2, "value"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    const/16 v3, 0x7ffe

    .line 66
    if-nez p2, :cond_0

    .line 67
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "field \""

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    iget-object v2, v2, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\": null value not allowed"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 69
    :cond_0
    iget v0, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    if-le v0, v3, :cond_1

    .line 70
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DocValuesField \""

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    iget-object v2, v2, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\" is too large, must be <= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 73
    :cond_1
    iget v0, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->currentDoc:I

    if-eq p1, v0, :cond_2

    .line 74
    invoke-direct {p0}, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->finishCurrentDoc()V

    .line 78
    :cond_2
    :goto_0
    iget v0, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->currentDoc:I

    if-lt v0, p1, :cond_3

    .line 83
    invoke-direct {p0, p2}, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->addOneValue(Lorg/apache/lucene/util/BytesRef;)V

    .line 84
    invoke-direct {p0}, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->updateBytesUsed()V

    .line 85
    return-void

    .line 79
    :cond_3
    iget-object v0, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->pendingCounts:Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->add(J)V

    .line 80
    iget v0, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->currentDoc:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->currentDoc:I

    goto :goto_0
.end method

.method public finish(I)V
    .locals 4
    .param p1, "maxDoc"    # I

    .prologue
    .line 110
    invoke-direct {p0}, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->finishCurrentDoc()V

    .line 113
    iget v0, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->currentDoc:I

    .local v0, "i":I
    :goto_0
    if-lt v0, p1, :cond_0

    .line 116
    return-void

    .line 114
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->pendingCounts:Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->add(J)V

    .line 113
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public flush(Lorg/apache/lucene/index/SegmentWriteState;Lorg/apache/lucene/codecs/DocValuesConsumer;)V
    .locals 10
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentWriteState;
    .param p2, "dvConsumer"    # Lorg/apache/lucene/codecs/DocValuesConsumer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 149
    iget-object v6, p1, Lorg/apache/lucene/index/SegmentWriteState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v6}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v1

    .line 150
    .local v1, "maxDoc":I
    iget v0, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->maxCount:I

    .line 151
    .local v0, "maxCountPerDoc":I
    sget-boolean v6, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->$assertionsDisabled:Z

    if-nez v6, :cond_0

    iget-object v6, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->pendingCounts:Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    invoke-virtual {v6}, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->size()J

    move-result-wide v6

    int-to-long v8, v1

    cmp-long v6, v6, v8

    if-eqz v6, :cond_0

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 152
    :cond_0
    iget-object v6, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->hash:Lorg/apache/lucene/util/BytesRefHash;

    invoke-virtual {v6}, Lorg/apache/lucene/util/BytesRefHash;->size()I

    move-result v5

    .line 154
    .local v5, "valueCount":I
    iget-object v6, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->hash:Lorg/apache/lucene/util/BytesRefHash;

    invoke-static {}, Lorg/apache/lucene/util/BytesRef;->getUTF8SortedAsUnicodeComparator()Ljava/util/Comparator;

    move-result-object v7

    invoke-virtual {v6, v7}, Lorg/apache/lucene/util/BytesRefHash;->sort(Ljava/util/Comparator;)[I

    move-result-object v4

    .line 155
    .local v4, "sortedValues":[I
    new-array v3, v5, [I

    .line 157
    .local v3, "ordMap":[I
    const/4 v2, 0x0

    .local v2, "ord":I
    :goto_0
    if-lt v2, v5, :cond_1

    .line 161
    iget-object v6, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    .line 164
    new-instance v7, Lorg/apache/lucene/index/SortedSetDocValuesWriter$1;

    invoke-direct {v7, p0, v4, v5}, Lorg/apache/lucene/index/SortedSetDocValuesWriter$1;-><init>(Lorg/apache/lucene/index/SortedSetDocValuesWriter;[II)V

    .line 172
    new-instance v8, Lorg/apache/lucene/index/SortedSetDocValuesWriter$2;

    invoke-direct {v8, p0, v1}, Lorg/apache/lucene/index/SortedSetDocValuesWriter$2;-><init>(Lorg/apache/lucene/index/SortedSetDocValuesWriter;I)V

    .line 180
    new-instance v9, Lorg/apache/lucene/index/SortedSetDocValuesWriter$3;

    invoke-direct {v9, p0, v3, v0}, Lorg/apache/lucene/index/SortedSetDocValuesWriter$3;-><init>(Lorg/apache/lucene/index/SortedSetDocValuesWriter;[II)V

    .line 161
    invoke-virtual {p2, v6, v7, v8, v9}, Lorg/apache/lucene/codecs/DocValuesConsumer;->addSortedSetField(Lorg/apache/lucene/index/FieldInfo;Ljava/lang/Iterable;Ljava/lang/Iterable;Ljava/lang/Iterable;)V

    .line 186
    return-void

    .line 158
    :cond_1
    aget v6, v4, v2

    aput v2, v3, v6

    .line 157
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method
