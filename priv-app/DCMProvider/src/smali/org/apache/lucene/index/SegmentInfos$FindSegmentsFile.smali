.class public abstract Lorg/apache/lucene/index/SegmentInfos$FindSegmentsFile;
.super Ljava/lang/Object;
.source "SegmentInfos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/SegmentInfos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "FindSegmentsFile"
.end annotation


# instance fields
.field final directory:Lorg/apache/lucene/store/Directory;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/store/Directory;)V
    .locals 0
    .param p1, "directory"    # Lorg/apache/lucene/store/Directory;

    .prologue
    .line 623
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 624
    iput-object p1, p0, Lorg/apache/lucene/index/SegmentInfos$FindSegmentsFile;->directory:Lorg/apache/lucene/store/Directory;

    .line 625
    return-void
.end method


# virtual methods
.method protected abstract doBody(Ljava/lang/String;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public run()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 630
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/SegmentInfos$FindSegmentsFile;->run(Lorg/apache/lucene/index/IndexCommit;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public run(Lorg/apache/lucene/index/IndexCommit;)Ljava/lang/Object;
    .locals 34
    .param p1, "commit"    # Lorg/apache/lucene/index/IndexCommit;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 635
    if-eqz p1, :cond_2

    .line 636
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentInfos$FindSegmentsFile;->directory:Lorg/apache/lucene/store/Directory;

    move-object/from16 v30, v0

    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/IndexCommit;->getDirectory()Lorg/apache/lucene/store/Directory;

    move-result-object v31

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    if-eq v0, v1, :cond_0

    .line 637
    new-instance v30, Ljava/io/IOException;

    const-string v31, "the specified commit does not match the specified Directory"

    invoke-direct/range {v30 .. v31}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v30

    .line 638
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/IndexCommit;->getSegmentsFileName()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/SegmentInfos$FindSegmentsFile;->doBody(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v28

    .line 822
    :cond_1
    :goto_0
    return-object v28

    .line 641
    :cond_2
    const/16 v26, 0x0

    .line 642
    .local v26, "segmentFileName":Ljava/lang/String;
    const-wide/16 v22, -0x1

    .line 643
    .local v22, "lastGen":J
    const-wide/16 v10, 0x0

    .line 644
    .local v10, "gen":J
    const/16 v20, 0x0

    .line 645
    .local v20, "genLookaheadCount":I
    const/4 v7, 0x0

    .line 646
    .local v7, "exc":Ljava/io/IOException;
    const/16 v25, 0x0

    .line 648
    .local v25, "retryCount":I
    const/16 v27, 0x1

    .line 668
    .local v27, "useFirstMethod":Z
    :cond_3
    :goto_1
    if-eqz v27, :cond_d

    .line 675
    const/4 v8, 0x0

    .line 677
    .local v8, "files":[Ljava/lang/String;
    const-wide/16 v16, -0x1

    .line 679
    .local v16, "genA":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentInfos$FindSegmentsFile;->directory:Lorg/apache/lucene/store/Directory;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lorg/apache/lucene/store/Directory;->listAll()[Ljava/lang/String;

    move-result-object v8

    .line 681
    if-eqz v8, :cond_4

    .line 682
    invoke-static {v8}, Lorg/apache/lucene/index/SegmentInfos;->getLastCommitGeneration([Ljava/lang/String;)J

    move-result-wide v16

    .line 685
    :cond_4
    # getter for: Lorg/apache/lucene/index/SegmentInfos;->infoStream:Ljava/io/PrintStream;
    invoke-static {}, Lorg/apache/lucene/index/SegmentInfos;->access$1()Ljava/io/PrintStream;

    move-result-object v30

    if-eqz v30, :cond_5

    .line 686
    new-instance v30, Ljava/lang/StringBuilder;

    const-string v31, "directory listing genA="

    invoke-direct/range {v30 .. v31}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v30

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    # invokes: Lorg/apache/lucene/index/SegmentInfos;->message(Ljava/lang/String;)V
    invoke-static/range {v30 .. v30}, Lorg/apache/lucene/index/SegmentInfos;->access$2(Ljava/lang/String;)V

    .line 694
    :cond_5
    const-wide/16 v18, -0x1

    .line 695
    .local v18, "genB":J
    const/4 v9, 0x0

    .line 697
    .local v9, "genInput":Lorg/apache/lucene/store/IndexInput;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentInfos$FindSegmentsFile;->directory:Lorg/apache/lucene/store/Directory;

    move-object/from16 v30, v0

    const-string v31, "segments.gen"

    sget-object v32, Lorg/apache/lucene/store/IOContext;->READONCE:Lorg/apache/lucene/store/IOContext;

    invoke-virtual/range {v30 .. v32}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v9

    .line 708
    :cond_6
    :goto_2
    if-eqz v9, :cond_9

    .line 710
    :try_start_1
    invoke-virtual {v9}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v29

    .line 711
    .local v29, "version":I
    const/16 v30, -0x2

    move/from16 v0, v29

    move/from16 v1, v30

    if-ne v0, v1, :cond_b

    .line 712
    invoke-virtual {v9}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v12

    .line 713
    .local v12, "gen0":J
    invoke-virtual {v9}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v14

    .line 714
    .local v14, "gen1":J
    # getter for: Lorg/apache/lucene/index/SegmentInfos;->infoStream:Ljava/io/PrintStream;
    invoke-static {}, Lorg/apache/lucene/index/SegmentInfos;->access$1()Ljava/io/PrintStream;

    move-result-object v30

    if-eqz v30, :cond_7

    .line 715
    new-instance v30, Ljava/lang/StringBuilder;

    const-string v31, "fallback check: "

    invoke-direct/range {v30 .. v31}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v30

    invoke-virtual {v0, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, "; "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v0, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    # invokes: Lorg/apache/lucene/index/SegmentInfos;->message(Ljava/lang/String;)V
    invoke-static/range {v30 .. v30}, Lorg/apache/lucene/index/SegmentInfos;->access$2(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 717
    :cond_7
    cmp-long v30, v12, v14

    if-nez v30, :cond_8

    .line 719
    move-wide/from16 v18, v12

    .line 728
    :cond_8
    invoke-virtual {v9}, Lorg/apache/lucene/store/IndexInput;->close()V

    .line 732
    .end local v12    # "gen0":J
    .end local v14    # "gen1":J
    .end local v29    # "version":I
    :cond_9
    :goto_3
    # getter for: Lorg/apache/lucene/index/SegmentInfos;->infoStream:Ljava/io/PrintStream;
    invoke-static {}, Lorg/apache/lucene/index/SegmentInfos;->access$1()Ljava/io/PrintStream;

    move-result-object v30

    if-eqz v30, :cond_a

    .line 733
    new-instance v30, Ljava/lang/StringBuilder;

    const-string v31, "segments.gen check: genB="

    invoke-direct/range {v30 .. v31}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v30

    move-wide/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    # invokes: Lorg/apache/lucene/index/SegmentInfos;->message(Ljava/lang/String;)V
    invoke-static/range {v30 .. v30}, Lorg/apache/lucene/index/SegmentInfos;->access$2(Ljava/lang/String;)V

    .line 737
    :cond_a
    invoke-static/range {v16 .. v19}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v10

    .line 739
    const-wide/16 v30, -0x1

    cmp-long v30, v10, v30

    if-nez v30, :cond_d

    .line 741
    new-instance v30, Lorg/apache/lucene/index/IndexNotFoundException;

    new-instance v31, Ljava/lang/StringBuilder;

    const-string v32, "no segments* file found in "

    invoke-direct/range {v31 .. v32}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentInfos$FindSegmentsFile;->directory:Lorg/apache/lucene/store/Directory;

    move-object/from16 v32, v0

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, ": files: "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-static {v8}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v32

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-direct/range {v30 .. v31}, Lorg/apache/lucene/index/IndexNotFoundException;-><init>(Ljava/lang/String;)V

    throw v30

    .line 698
    :catch_0
    move-exception v4

    .line 699
    .local v4, "e":Ljava/io/FileNotFoundException;
    # getter for: Lorg/apache/lucene/index/SegmentInfos;->infoStream:Ljava/io/PrintStream;
    invoke-static {}, Lorg/apache/lucene/index/SegmentInfos;->access$1()Ljava/io/PrintStream;

    move-result-object v30

    if-eqz v30, :cond_6

    .line 700
    new-instance v30, Ljava/lang/StringBuilder;

    const-string v31, "segments.gen open: FileNotFoundException "

    invoke-direct/range {v30 .. v31}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    # invokes: Lorg/apache/lucene/index/SegmentInfos;->message(Ljava/lang/String;)V
    invoke-static/range {v30 .. v30}, Lorg/apache/lucene/index/SegmentInfos;->access$2(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 702
    .end local v4    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v4

    .line 703
    .local v4, "e":Ljava/io/IOException;
    # getter for: Lorg/apache/lucene/index/SegmentInfos;->infoStream:Ljava/io/PrintStream;
    invoke-static {}, Lorg/apache/lucene/index/SegmentInfos;->access$1()Ljava/io/PrintStream;

    move-result-object v30

    if-eqz v30, :cond_6

    .line 704
    new-instance v30, Ljava/lang/StringBuilder;

    const-string v31, "segments.gen open: IOException "

    invoke-direct/range {v30 .. v31}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    # invokes: Lorg/apache/lucene/index/SegmentInfos;->message(Ljava/lang/String;)V
    invoke-static/range {v30 .. v30}, Lorg/apache/lucene/index/SegmentInfos;->access$2(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 722
    .end local v4    # "e":Ljava/io/IOException;
    .restart local v29    # "version":I
    :cond_b
    :try_start_2
    new-instance v30, Lorg/apache/lucene/index/IndexFormatTooNewException;

    const/16 v31, -0x2

    const/16 v32, -0x2

    move-object/from16 v0, v30

    move/from16 v1, v29

    move/from16 v2, v31

    move/from16 v3, v32

    invoke-direct {v0, v9, v1, v2, v3}, Lorg/apache/lucene/index/IndexFormatTooNewException;-><init>(Lorg/apache/lucene/store/DataInput;III)V

    throw v30
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 724
    .end local v29    # "version":I
    :catch_2
    move-exception v6

    .line 726
    .local v6, "err2":Ljava/io/IOException;
    :try_start_3
    instance-of v0, v6, Lorg/apache/lucene/index/CorruptIndexException;

    move/from16 v30, v0

    if-eqz v30, :cond_c

    throw v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 727
    .end local v6    # "err2":Ljava/io/IOException;
    :catchall_0
    move-exception v30

    .line 728
    invoke-virtual {v9}, Lorg/apache/lucene/store/IndexInput;->close()V

    .line 729
    throw v30

    .line 728
    .restart local v6    # "err2":Ljava/io/IOException;
    :cond_c
    invoke-virtual {v9}, Lorg/apache/lucene/store/IndexInput;->close()V

    goto/16 :goto_3

    .line 745
    .end local v6    # "err2":Ljava/io/IOException;
    .end local v8    # "files":[Ljava/lang/String;
    .end local v9    # "genInput":Lorg/apache/lucene/store/IndexInput;
    .end local v16    # "genA":J
    .end local v18    # "genB":J
    :cond_d
    if-eqz v27, :cond_e

    cmp-long v30, v22, v10

    if-nez v30, :cond_e

    const/16 v30, 0x2

    move/from16 v0, v25

    move/from16 v1, v30

    if-lt v0, v1, :cond_e

    .line 749
    const/16 v27, 0x0

    .line 755
    :cond_e
    if-nez v27, :cond_14

    .line 756
    # getter for: Lorg/apache/lucene/index/SegmentInfos;->defaultGenLookaheadCount:I
    invoke-static {}, Lorg/apache/lucene/index/SegmentInfos;->access$3()I

    move-result v30

    move/from16 v0, v20

    move/from16 v1, v30

    if-ge v0, v1, :cond_13

    .line 757
    const-wide/16 v30, 0x1

    add-long v10, v10, v30

    .line 758
    add-int/lit8 v20, v20, 0x1

    .line 759
    # getter for: Lorg/apache/lucene/index/SegmentInfos;->infoStream:Ljava/io/PrintStream;
    invoke-static {}, Lorg/apache/lucene/index/SegmentInfos;->access$1()Ljava/io/PrintStream;

    move-result-object v30

    if-eqz v30, :cond_f

    .line 760
    new-instance v30, Ljava/lang/StringBuilder;

    const-string v31, "look ahead increment gen to "

    invoke-direct/range {v30 .. v31}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v30

    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    # invokes: Lorg/apache/lucene/index/SegmentInfos;->message(Ljava/lang/String;)V
    invoke-static/range {v30 .. v30}, Lorg/apache/lucene/index/SegmentInfos;->access$2(Ljava/lang/String;)V

    .line 776
    :cond_f
    :goto_4
    move-wide/from16 v22, v10

    .line 778
    const-string v30, "segments"

    .line 779
    const-string v31, ""

    .line 778
    move-object/from16 v0, v30

    move-object/from16 v1, v31

    invoke-static {v0, v1, v10, v11}, Lorg/apache/lucene/index/IndexFileNames;->fileNameFromGeneration(Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v26

    .line 783
    :try_start_4
    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/SegmentInfos$FindSegmentsFile;->doBody(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v28

    .line 784
    .local v28, "v":Ljava/lang/Object;
    # getter for: Lorg/apache/lucene/index/SegmentInfos;->infoStream:Ljava/io/PrintStream;
    invoke-static {}, Lorg/apache/lucene/index/SegmentInfos;->access$1()Ljava/io/PrintStream;

    move-result-object v30

    if-eqz v30, :cond_1

    .line 785
    new-instance v30, Ljava/lang/StringBuilder;

    const-string v31, "success on "

    invoke-direct/range {v30 .. v31}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v30

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    # invokes: Lorg/apache/lucene/index/SegmentInfos;->message(Ljava/lang/String;)V
    invoke-static/range {v30 .. v30}, Lorg/apache/lucene/index/SegmentInfos;->access$2(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    goto/16 :goto_0

    .line 788
    .end local v28    # "v":Ljava/lang/Object;
    :catch_3
    move-exception v5

    .line 791
    .local v5, "err":Ljava/io/IOException;
    if-nez v7, :cond_10

    .line 792
    move-object v7, v5

    .line 795
    :cond_10
    # getter for: Lorg/apache/lucene/index/SegmentInfos;->infoStream:Ljava/io/PrintStream;
    invoke-static {}, Lorg/apache/lucene/index/SegmentInfos;->access$1()Ljava/io/PrintStream;

    move-result-object v30

    if-eqz v30, :cond_11

    .line 796
    new-instance v30, Ljava/lang/StringBuilder;

    const-string v31, "primary Exception on \'"

    invoke-direct/range {v30 .. v31}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v30

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, "\': "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, "\'; will retry: retryCount="

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, "; gen = "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    # invokes: Lorg/apache/lucene/index/SegmentInfos;->message(Ljava/lang/String;)V
    invoke-static/range {v30 .. v30}, Lorg/apache/lucene/index/SegmentInfos;->access$2(Ljava/lang/String;)V

    .line 799
    :cond_11
    const-wide/16 v30, 0x1

    cmp-long v30, v10, v30

    if-lez v30, :cond_3

    if-eqz v27, :cond_3

    const/16 v30, 0x1

    move/from16 v0, v25

    move/from16 v1, v30

    if-ne v0, v1, :cond_3

    .line 806
    const-string v30, "segments"

    .line 807
    const-string v31, ""

    .line 808
    const-wide/16 v32, 0x1

    sub-long v32, v10, v32

    .line 806
    invoke-static/range {v30 .. v33}, Lorg/apache/lucene/index/IndexFileNames;->fileNameFromGeneration(Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v24

    .line 811
    .local v24, "prevSegmentFileName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentInfos$FindSegmentsFile;->directory:Lorg/apache/lucene/store/Directory;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/apache/lucene/store/Directory;->fileExists(Ljava/lang/String;)Z

    move-result v21

    .line 813
    .local v21, "prevExists":Z
    if-eqz v21, :cond_3

    .line 814
    # getter for: Lorg/apache/lucene/index/SegmentInfos;->infoStream:Ljava/io/PrintStream;
    invoke-static {}, Lorg/apache/lucene/index/SegmentInfos;->access$1()Ljava/io/PrintStream;

    move-result-object v30

    if-eqz v30, :cond_12

    .line 815
    new-instance v30, Ljava/lang/StringBuilder;

    const-string v31, "fallback to prior segment file \'"

    invoke-direct/range {v30 .. v31}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v30

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, "\'"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    # invokes: Lorg/apache/lucene/index/SegmentInfos;->message(Ljava/lang/String;)V
    invoke-static/range {v30 .. v30}, Lorg/apache/lucene/index/SegmentInfos;->access$2(Ljava/lang/String;)V

    .line 818
    :cond_12
    :try_start_5
    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/SegmentInfos$FindSegmentsFile;->doBody(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v28

    .line 819
    .restart local v28    # "v":Ljava/lang/Object;
    # getter for: Lorg/apache/lucene/index/SegmentInfos;->infoStream:Ljava/io/PrintStream;
    invoke-static {}, Lorg/apache/lucene/index/SegmentInfos;->access$1()Ljava/io/PrintStream;

    move-result-object v30

    if-eqz v30, :cond_1

    .line 820
    new-instance v30, Ljava/lang/StringBuilder;

    const-string v31, "success on fallback "

    invoke-direct/range {v30 .. v31}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v30

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    # invokes: Lorg/apache/lucene/index/SegmentInfos;->message(Ljava/lang/String;)V
    invoke-static/range {v30 .. v30}, Lorg/apache/lucene/index/SegmentInfos;->access$2(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    goto/16 :goto_0

    .line 823
    .end local v28    # "v":Ljava/lang/Object;
    :catch_4
    move-exception v6

    .line 824
    .restart local v6    # "err2":Ljava/io/IOException;
    # getter for: Lorg/apache/lucene/index/SegmentInfos;->infoStream:Ljava/io/PrintStream;
    invoke-static {}, Lorg/apache/lucene/index/SegmentInfos;->access$1()Ljava/io/PrintStream;

    move-result-object v30

    if-eqz v30, :cond_3

    .line 825
    new-instance v30, Ljava/lang/StringBuilder;

    const-string v31, "secondary Exception on \'"

    invoke-direct/range {v30 .. v31}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v30

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, "\': "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, "\'; will retry"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    # invokes: Lorg/apache/lucene/index/SegmentInfos;->message(Ljava/lang/String;)V
    invoke-static/range {v30 .. v30}, Lorg/apache/lucene/index/SegmentInfos;->access$2(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 764
    .end local v5    # "err":Ljava/io/IOException;
    .end local v6    # "err2":Ljava/io/IOException;
    .end local v21    # "prevExists":Z
    .end local v24    # "prevSegmentFileName":Ljava/lang/String;
    :cond_13
    throw v7

    .line 766
    :cond_14
    cmp-long v30, v22, v10

    if-nez v30, :cond_15

    .line 769
    add-int/lit8 v25, v25, 0x1

    .line 770
    goto/16 :goto_4

    .line 773
    :cond_15
    const/16 v25, 0x0

    goto/16 :goto_4
.end method
