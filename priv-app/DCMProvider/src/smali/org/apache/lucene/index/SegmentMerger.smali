.class final Lorg/apache/lucene/index/SegmentMerger;
.super Ljava/lang/Object;
.source "SegmentMerger.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final codec:Lorg/apache/lucene/codecs/Codec;

.field private final context:Lorg/apache/lucene/store/IOContext;

.field private final directory:Lorg/apache/lucene/store/Directory;

.field private final fieldInfosBuilder:Lorg/apache/lucene/index/FieldInfos$Builder;

.field private final mergeState:Lorg/apache/lucene/index/MergeState;

.field private final termIndexInterval:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-class v0, Lorg/apache/lucene/index/SegmentMerger;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/SegmentMerger;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Ljava/util/List;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/util/InfoStream;Lorg/apache/lucene/store/Directory;ILorg/apache/lucene/index/MergeState$CheckAbort;Lorg/apache/lucene/index/FieldInfos$FieldNumbers;Lorg/apache/lucene/store/IOContext;)V
    .locals 1
    .param p2, "segmentInfo"    # Lorg/apache/lucene/index/SegmentInfo;
    .param p3, "infoStream"    # Lorg/apache/lucene/util/InfoStream;
    .param p4, "dir"    # Lorg/apache/lucene/store/Directory;
    .param p5, "termIndexInterval"    # I
    .param p6, "checkAbort"    # Lorg/apache/lucene/index/MergeState$CheckAbort;
    .param p7, "fieldNumbers"    # Lorg/apache/lucene/index/FieldInfos$FieldNumbers;
    .param p8, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/AtomicReader;",
            ">;",
            "Lorg/apache/lucene/index/SegmentInfo;",
            "Lorg/apache/lucene/util/InfoStream;",
            "Lorg/apache/lucene/store/Directory;",
            "I",
            "Lorg/apache/lucene/index/MergeState$CheckAbort;",
            "Lorg/apache/lucene/index/FieldInfos$FieldNumbers;",
            "Lorg/apache/lucene/store/IOContext;",
            ")V"
        }
    .end annotation

    .prologue
    .line 55
    .local p1, "readers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/AtomicReader;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    new-instance v0, Lorg/apache/lucene/index/MergeState;

    invoke-direct {v0, p1, p2, p3, p6}, Lorg/apache/lucene/index/MergeState;-><init>(Ljava/util/List;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/util/InfoStream;Lorg/apache/lucene/index/MergeState$CheckAbort;)V

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    .line 58
    iput-object p4, p0, Lorg/apache/lucene/index/SegmentMerger;->directory:Lorg/apache/lucene/store/Directory;

    .line 59
    iput p5, p0, Lorg/apache/lucene/index/SegmentMerger;->termIndexInterval:I

    .line 60
    invoke-virtual {p2}, Lorg/apache/lucene/index/SegmentInfo;->getCodec()Lorg/apache/lucene/codecs/Codec;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentMerger;->codec:Lorg/apache/lucene/codecs/Codec;

    .line 61
    iput-object p8, p0, Lorg/apache/lucene/index/SegmentMerger;->context:Lorg/apache/lucene/store/IOContext;

    .line 62
    new-instance v0, Lorg/apache/lucene/index/FieldInfos$Builder;

    invoke-direct {v0, p7}, Lorg/apache/lucene/index/FieldInfos$Builder;-><init>(Lorg/apache/lucene/index/FieldInfos$FieldNumbers;)V

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentMerger;->fieldInfosBuilder:Lorg/apache/lucene/index/FieldInfos$Builder;

    .line 63
    return-void
.end method

.method private mergeDocValues(Lorg/apache/lucene/index/SegmentWriteState;)V
    .locals 13
    .param p1, "segmentWriteState"    # Lorg/apache/lucene/index/SegmentWriteState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 146
    iget-object v10, p0, Lorg/apache/lucene/index/SegmentMerger;->codec:Lorg/apache/lucene/codecs/Codec;

    invoke-virtual {v10}, Lorg/apache/lucene/codecs/Codec;->docValuesFormat()Lorg/apache/lucene/codecs/DocValuesFormat;

    move-result-object v10

    invoke-virtual {v10, p1}, Lorg/apache/lucene/codecs/DocValuesFormat;->fieldsConsumer(Lorg/apache/lucene/index/SegmentWriteState;)Lorg/apache/lucene/codecs/DocValuesConsumer;

    move-result-object v0

    .line 147
    .local v0, "consumer":Lorg/apache/lucene/codecs/DocValuesConsumer;
    const/4 v3, 0x0

    .line 149
    .local v3, "success":Z
    :try_start_0
    iget-object v10, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v10, v10, Lorg/apache/lucene/index/MergeState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v10}, Lorg/apache/lucene/index/FieldInfos;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_0
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v11

    if-nez v11, :cond_1

    .line 197
    const/4 v3, 0x1

    .line 199
    if-eqz v3, :cond_f

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/io/Closeable;

    const/4 v11, 0x0

    .line 200
    aput-object v0, v10, v11

    invoke-static {v10}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 205
    :goto_1
    return-void

    .line 149
    :cond_1
    :try_start_1
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/FieldInfo;

    .line 150
    .local v1, "field":Lorg/apache/lucene/index/FieldInfo;
    invoke-virtual {v1}, Lorg/apache/lucene/index/FieldInfo;->getDocValuesType()Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-result-object v8

    .line 151
    .local v8, "type":Lorg/apache/lucene/index/FieldInfo$DocValuesType;
    if-eqz v8, :cond_0

    .line 152
    sget-object v11, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->NUMERIC:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    if-ne v8, v11, :cond_4

    .line 153
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 154
    .local v5, "toMerge":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/NumericDocValues;>;"
    iget-object v11, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v11, v11, Lorg/apache/lucene/index/MergeState;->readers:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_2

    .line 161
    iget-object v11, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    invoke-virtual {v0, v1, v11, v5}, Lorg/apache/lucene/codecs/DocValuesConsumer;->mergeNumericField(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/index/MergeState;Ljava/util/List;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 198
    .end local v1    # "field":Lorg/apache/lucene/index/FieldInfo;
    .end local v5    # "toMerge":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/NumericDocValues;>;"
    .end local v8    # "type":Lorg/apache/lucene/index/FieldInfo$DocValuesType;
    :catchall_0
    move-exception v10

    .line 199
    if-eqz v3, :cond_e

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/io/Closeable;

    const/4 v12, 0x0

    .line 200
    aput-object v0, v11, v12

    invoke-static {v11}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 204
    :goto_3
    throw v10

    .line 154
    .restart local v1    # "field":Lorg/apache/lucene/index/FieldInfo;
    .restart local v5    # "toMerge":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/NumericDocValues;>;"
    .restart local v8    # "type":Lorg/apache/lucene/index/FieldInfo$DocValuesType;
    :cond_2
    :try_start_2
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/index/AtomicReader;

    .line 155
    .local v2, "reader":Lorg/apache/lucene/index/AtomicReader;
    iget-object v12, v1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {v2, v12}, Lorg/apache/lucene/index/AtomicReader;->getNumericDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/NumericDocValues;

    move-result-object v9

    .line 156
    .local v9, "values":Lorg/apache/lucene/index/NumericDocValues;
    if-nez v9, :cond_3

    .line 157
    sget-object v9, Lorg/apache/lucene/index/NumericDocValues;->EMPTY:Lorg/apache/lucene/index/NumericDocValues;

    .line 159
    :cond_3
    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 162
    .end local v2    # "reader":Lorg/apache/lucene/index/AtomicReader;
    .end local v5    # "toMerge":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/NumericDocValues;>;"
    .end local v9    # "values":Lorg/apache/lucene/index/NumericDocValues;
    :cond_4
    sget-object v11, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->BINARY:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    if-ne v8, v11, :cond_7

    .line 163
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 164
    .local v4, "toMerge":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/BinaryDocValues;>;"
    iget-object v11, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v11, v11, Lorg/apache/lucene/index/MergeState;->readers:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_4
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_5

    .line 171
    iget-object v11, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    invoke-virtual {v0, v1, v11, v4}, Lorg/apache/lucene/codecs/DocValuesConsumer;->mergeBinaryField(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/index/MergeState;Ljava/util/List;)V

    goto :goto_0

    .line 164
    :cond_5
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/index/AtomicReader;

    .line 165
    .restart local v2    # "reader":Lorg/apache/lucene/index/AtomicReader;
    iget-object v12, v1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {v2, v12}, Lorg/apache/lucene/index/AtomicReader;->getBinaryDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/BinaryDocValues;

    move-result-object v9

    .line 166
    .local v9, "values":Lorg/apache/lucene/index/BinaryDocValues;
    if-nez v9, :cond_6

    .line 167
    sget-object v9, Lorg/apache/lucene/index/BinaryDocValues;->EMPTY:Lorg/apache/lucene/index/BinaryDocValues;

    .line 169
    :cond_6
    invoke-interface {v4, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 172
    .end local v2    # "reader":Lorg/apache/lucene/index/AtomicReader;
    .end local v4    # "toMerge":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/BinaryDocValues;>;"
    .end local v9    # "values":Lorg/apache/lucene/index/BinaryDocValues;
    :cond_7
    sget-object v11, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->SORTED:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    if-ne v8, v11, :cond_a

    .line 173
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 174
    .local v6, "toMerge":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SortedDocValues;>;"
    iget-object v11, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v11, v11, Lorg/apache/lucene/index/MergeState;->readers:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_5
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_8

    .line 181
    iget-object v11, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    invoke-virtual {v0, v1, v11, v6}, Lorg/apache/lucene/codecs/DocValuesConsumer;->mergeSortedField(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/index/MergeState;Ljava/util/List;)V

    goto/16 :goto_0

    .line 174
    :cond_8
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/index/AtomicReader;

    .line 175
    .restart local v2    # "reader":Lorg/apache/lucene/index/AtomicReader;
    iget-object v12, v1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {v2, v12}, Lorg/apache/lucene/index/AtomicReader;->getSortedDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/SortedDocValues;

    move-result-object v9

    .line 176
    .local v9, "values":Lorg/apache/lucene/index/SortedDocValues;
    if-nez v9, :cond_9

    .line 177
    sget-object v9, Lorg/apache/lucene/index/SortedDocValues;->EMPTY:Lorg/apache/lucene/index/SortedDocValues;

    .line 179
    :cond_9
    invoke-interface {v6, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 182
    .end local v2    # "reader":Lorg/apache/lucene/index/AtomicReader;
    .end local v6    # "toMerge":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SortedDocValues;>;"
    .end local v9    # "values":Lorg/apache/lucene/index/SortedDocValues;
    :cond_a
    sget-object v11, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->SORTED_SET:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    if-ne v8, v11, :cond_d

    .line 183
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 184
    .local v7, "toMerge":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SortedSetDocValues;>;"
    iget-object v11, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v11, v11, Lorg/apache/lucene/index/MergeState;->readers:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_6
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_b

    .line 191
    iget-object v11, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    invoke-virtual {v0, v1, v11, v7}, Lorg/apache/lucene/codecs/DocValuesConsumer;->mergeSortedSetField(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/index/MergeState;Ljava/util/List;)V

    goto/16 :goto_0

    .line 184
    :cond_b
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/index/AtomicReader;

    .line 185
    .restart local v2    # "reader":Lorg/apache/lucene/index/AtomicReader;
    iget-object v12, v1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {v2, v12}, Lorg/apache/lucene/index/AtomicReader;->getSortedSetDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/SortedSetDocValues;

    move-result-object v9

    .line 186
    .local v9, "values":Lorg/apache/lucene/index/SortedSetDocValues;
    if-nez v9, :cond_c

    .line 187
    sget-object v9, Lorg/apache/lucene/index/SortedSetDocValues;->EMPTY:Lorg/apache/lucene/index/SortedSetDocValues;

    .line 189
    :cond_c
    invoke-interface {v7, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 193
    .end local v2    # "reader":Lorg/apache/lucene/index/AtomicReader;
    .end local v7    # "toMerge":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SortedSetDocValues;>;"
    .end local v9    # "values":Lorg/apache/lucene/index/SortedSetDocValues;
    :cond_d
    new-instance v10, Ljava/lang/AssertionError;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "type="

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v10
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 201
    .end local v1    # "field":Lorg/apache/lucene/index/FieldInfo;
    .end local v8    # "type":Lorg/apache/lucene/index/FieldInfo$DocValuesType;
    :cond_e
    const/4 v11, 0x1

    new-array v11, v11, [Ljava/io/Closeable;

    const/4 v12, 0x0

    .line 202
    aput-object v0, v11, v12

    invoke-static {v11}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto/16 :goto_3

    .line 201
    :cond_f
    const/4 v10, 0x1

    new-array v10, v10, [Ljava/io/Closeable;

    const/4 v11, 0x0

    .line 202
    aput-object v0, v10, v11

    invoke-static {v10}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto/16 :goto_1
.end method

.method private mergeFields()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 295
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentMerger;->codec:Lorg/apache/lucene/codecs/Codec;

    invoke-virtual {v1}, Lorg/apache/lucene/codecs/Codec;->storedFieldsFormat()Lorg/apache/lucene/codecs/StoredFieldsFormat;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/index/SegmentMerger;->directory:Lorg/apache/lucene/store/Directory;

    iget-object v3, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v3, v3, Lorg/apache/lucene/index/MergeState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v4, p0, Lorg/apache/lucene/index/SegmentMerger;->context:Lorg/apache/lucene/store/IOContext;

    invoke-virtual {v1, v2, v3, v4}, Lorg/apache/lucene/codecs/StoredFieldsFormat;->fieldsWriter(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/codecs/StoredFieldsWriter;

    move-result-object v0

    .line 298
    .local v0, "fieldsWriter":Lorg/apache/lucene/codecs/StoredFieldsWriter;
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/codecs/StoredFieldsWriter;->merge(Lorg/apache/lucene/index/MergeState;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 300
    invoke-virtual {v0}, Lorg/apache/lucene/codecs/StoredFieldsWriter;->close()V

    .line 298
    return v1

    .line 299
    :catchall_0
    move-exception v1

    .line 300
    invoke-virtual {v0}, Lorg/apache/lucene/codecs/StoredFieldsWriter;->close()V

    .line 301
    throw v1
.end method

.method private mergeNorms(Lorg/apache/lucene/index/SegmentWriteState;)V
    .locals 11
    .param p1, "segmentWriteState"    # Lorg/apache/lucene/index/SegmentWriteState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 208
    iget-object v6, p0, Lorg/apache/lucene/index/SegmentMerger;->codec:Lorg/apache/lucene/codecs/Codec;

    invoke-virtual {v6}, Lorg/apache/lucene/codecs/Codec;->normsFormat()Lorg/apache/lucene/codecs/NormsFormat;

    move-result-object v6

    invoke-virtual {v6, p1}, Lorg/apache/lucene/codecs/NormsFormat;->normsConsumer(Lorg/apache/lucene/index/SegmentWriteState;)Lorg/apache/lucene/codecs/DocValuesConsumer;

    move-result-object v0

    .line 209
    .local v0, "consumer":Lorg/apache/lucene/codecs/DocValuesConsumer;
    const/4 v4, 0x0

    .line 211
    .local v4, "success":Z
    :try_start_0
    iget-object v6, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v6, v6, Lorg/apache/lucene/index/MergeState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v6}, Lorg/apache/lucene/index/FieldInfos;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    if-nez v7, :cond_1

    .line 224
    const/4 v4, 0x1

    .line 226
    if-eqz v4, :cond_5

    new-array v6, v10, [Ljava/io/Closeable;

    .line 227
    aput-object v0, v6, v9

    invoke-static {v6}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 232
    :goto_1
    return-void

    .line 211
    :cond_1
    :try_start_1
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/FieldInfo;

    .line 212
    .local v1, "field":Lorg/apache/lucene/index/FieldInfo;
    invoke-virtual {v1}, Lorg/apache/lucene/index/FieldInfo;->hasNorms()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 213
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 214
    .local v5, "toMerge":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/NumericDocValues;>;"
    iget-object v7, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v7, v7, Lorg/apache/lucene/index/MergeState;->readers:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_2

    .line 221
    iget-object v7, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    invoke-virtual {v0, v1, v7, v5}, Lorg/apache/lucene/codecs/DocValuesConsumer;->mergeNumericField(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/index/MergeState;Ljava/util/List;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 225
    .end local v1    # "field":Lorg/apache/lucene/index/FieldInfo;
    .end local v5    # "toMerge":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/NumericDocValues;>;"
    :catchall_0
    move-exception v6

    .line 226
    if-eqz v4, :cond_4

    new-array v7, v10, [Ljava/io/Closeable;

    .line 227
    aput-object v0, v7, v9

    invoke-static {v7}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 231
    :goto_3
    throw v6

    .line 214
    .restart local v1    # "field":Lorg/apache/lucene/index/FieldInfo;
    .restart local v5    # "toMerge":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/NumericDocValues;>;"
    :cond_2
    :try_start_2
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/index/AtomicReader;

    .line 215
    .local v3, "reader":Lorg/apache/lucene/index/AtomicReader;
    iget-object v8, v1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {v3, v8}, Lorg/apache/lucene/index/AtomicReader;->getNormValues(Ljava/lang/String;)Lorg/apache/lucene/index/NumericDocValues;

    move-result-object v2

    .line 216
    .local v2, "norms":Lorg/apache/lucene/index/NumericDocValues;
    if-nez v2, :cond_3

    .line 217
    sget-object v2, Lorg/apache/lucene/index/NumericDocValues;->EMPTY:Lorg/apache/lucene/index/NumericDocValues;

    .line 219
    :cond_3
    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 228
    .end local v1    # "field":Lorg/apache/lucene/index/FieldInfo;
    .end local v2    # "norms":Lorg/apache/lucene/index/NumericDocValues;
    .end local v3    # "reader":Lorg/apache/lucene/index/AtomicReader;
    .end local v5    # "toMerge":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/NumericDocValues;>;"
    :cond_4
    new-array v7, v10, [Ljava/io/Closeable;

    .line 229
    aput-object v0, v7, v9

    invoke-static {v7}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto :goto_3

    .line 228
    :cond_5
    new-array v6, v10, [Ljava/io/Closeable;

    .line 229
    aput-object v0, v6, v9

    invoke-static {v6}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto :goto_1
.end method

.method private mergeTerms(Lorg/apache/lucene/index/SegmentWriteState;)V
    .locals 13
    .param p1, "segmentWriteState"    # Lorg/apache/lucene/index/SegmentWriteState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 346
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 347
    .local v3, "fields":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/Fields;>;"
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 349
    .local v7, "slices":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/ReaderSlice;>;"
    const/4 v1, 0x0

    .line 351
    .local v1, "docBase":I
    const/4 v6, 0x0

    .local v6, "readerIndex":I
    :goto_0
    iget-object v9, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v9, v9, Lorg/apache/lucene/index/MergeState;->readers:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    if-lt v6, v9, :cond_0

    .line 362
    iget-object v9, p0, Lorg/apache/lucene/index/SegmentMerger;->codec:Lorg/apache/lucene/codecs/Codec;

    invoke-virtual {v9}, Lorg/apache/lucene/codecs/Codec;->postingsFormat()Lorg/apache/lucene/codecs/PostingsFormat;

    move-result-object v9

    invoke-virtual {v9, p1}, Lorg/apache/lucene/codecs/PostingsFormat;->fieldsConsumer(Lorg/apache/lucene/index/SegmentWriteState;)Lorg/apache/lucene/codecs/FieldsConsumer;

    move-result-object v0

    .line 363
    .local v0, "consumer":Lorg/apache/lucene/codecs/FieldsConsumer;
    const/4 v8, 0x0

    .line 365
    .local v8, "success":Z
    :try_start_0
    iget-object v11, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    .line 366
    new-instance v12, Lorg/apache/lucene/index/MultiFields;

    sget-object v9, Lorg/apache/lucene/index/Fields;->EMPTY_ARRAY:[Lorg/apache/lucene/index/Fields;

    invoke-interface {v3, v9}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Lorg/apache/lucene/index/Fields;

    .line 367
    sget-object v10, Lorg/apache/lucene/index/ReaderSlice;->EMPTY_ARRAY:[Lorg/apache/lucene/index/ReaderSlice;

    invoke-interface {v7, v10}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v10

    check-cast v10, [Lorg/apache/lucene/index/ReaderSlice;

    .line 366
    invoke-direct {v12, v9, v10}, Lorg/apache/lucene/index/MultiFields;-><init>([Lorg/apache/lucene/index/Fields;[Lorg/apache/lucene/index/ReaderSlice;)V

    .line 365
    invoke-virtual {v0, v11, v12}, Lorg/apache/lucene/codecs/FieldsConsumer;->merge(Lorg/apache/lucene/index/MergeState;Lorg/apache/lucene/index/Fields;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 368
    const/4 v8, 0x1

    .line 370
    if-eqz v8, :cond_3

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/io/Closeable;

    const/4 v10, 0x0

    .line 371
    aput-object v0, v9, v10

    invoke-static {v9}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 376
    :goto_1
    return-void

    .line 352
    .end local v0    # "consumer":Lorg/apache/lucene/codecs/FieldsConsumer;
    .end local v8    # "success":Z
    :cond_0
    iget-object v9, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v9, v9, Lorg/apache/lucene/index/MergeState;->readers:Ljava/util/List;

    invoke-interface {v9, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/index/AtomicReader;

    .line 353
    .local v5, "reader":Lorg/apache/lucene/index/AtomicReader;
    invoke-virtual {v5}, Lorg/apache/lucene/index/AtomicReader;->fields()Lorg/apache/lucene/index/Fields;

    move-result-object v2

    .line 354
    .local v2, "f":Lorg/apache/lucene/index/Fields;
    invoke-virtual {v5}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v4

    .line 355
    .local v4, "maxDoc":I
    if-eqz v2, :cond_1

    .line 356
    new-instance v9, Lorg/apache/lucene/index/ReaderSlice;

    invoke-direct {v9, v1, v4, v6}, Lorg/apache/lucene/index/ReaderSlice;-><init>(III)V

    invoke-interface {v7, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 357
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 359
    :cond_1
    add-int/2addr v1, v4

    .line 351
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 369
    .end local v2    # "f":Lorg/apache/lucene/index/Fields;
    .end local v4    # "maxDoc":I
    .end local v5    # "reader":Lorg/apache/lucene/index/AtomicReader;
    .restart local v0    # "consumer":Lorg/apache/lucene/codecs/FieldsConsumer;
    .restart local v8    # "success":Z
    :catchall_0
    move-exception v9

    .line 370
    if-eqz v8, :cond_2

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/io/Closeable;

    const/4 v11, 0x0

    .line 371
    aput-object v0, v10, v11

    invoke-static {v10}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 375
    :goto_2
    throw v9

    .line 372
    :cond_2
    const/4 v10, 0x1

    new-array v10, v10, [Ljava/io/Closeable;

    const/4 v11, 0x0

    .line 373
    aput-object v0, v10, v11

    invoke-static {v10}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto :goto_2

    .line 372
    :cond_3
    const/4 v9, 0x1

    new-array v9, v9, [Ljava/io/Closeable;

    const/4 v10, 0x0

    .line 373
    aput-object v0, v9, v10

    invoke-static {v9}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto :goto_1
.end method

.method private mergeVectors()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 309
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentMerger;->codec:Lorg/apache/lucene/codecs/Codec;

    invoke-virtual {v1}, Lorg/apache/lucene/codecs/Codec;->termVectorsFormat()Lorg/apache/lucene/codecs/TermVectorsFormat;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/index/SegmentMerger;->directory:Lorg/apache/lucene/store/Directory;

    iget-object v3, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v3, v3, Lorg/apache/lucene/index/MergeState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v4, p0, Lorg/apache/lucene/index/SegmentMerger;->context:Lorg/apache/lucene/store/IOContext;

    invoke-virtual {v1, v2, v3, v4}, Lorg/apache/lucene/codecs/TermVectorsFormat;->vectorsWriter(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/codecs/TermVectorsWriter;

    move-result-object v0

    .line 312
    .local v0, "termVectorsWriter":Lorg/apache/lucene/codecs/TermVectorsWriter;
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/codecs/TermVectorsWriter;->merge(Lorg/apache/lucene/index/MergeState;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 314
    invoke-virtual {v0}, Lorg/apache/lucene/codecs/TermVectorsWriter;->close()V

    .line 312
    return v1

    .line 313
    :catchall_0
    move-exception v1

    .line 314
    invoke-virtual {v0}, Lorg/apache/lucene/codecs/TermVectorsWriter;->close()V

    .line 315
    throw v1
.end method

.method private setDocMaps()I
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 320
    iget-object v5, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v5, v5, Lorg/apache/lucene/index/MergeState;->readers:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v3

    .line 323
    .local v3, "numReaders":I
    iget-object v5, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    new-array v6, v3, [Lorg/apache/lucene/index/MergeState$DocMap;

    iput-object v6, v5, Lorg/apache/lucene/index/MergeState;->docMaps:[Lorg/apache/lucene/index/MergeState$DocMap;

    .line 324
    iget-object v5, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    new-array v6, v3, [I

    iput-object v6, v5, Lorg/apache/lucene/index/MergeState;->docBase:[I

    .line 326
    const/4 v0, 0x0

    .line 328
    .local v0, "docBase":I
    const/4 v2, 0x0

    .line 329
    .local v2, "i":I
    :goto_0
    iget-object v5, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v5, v5, Lorg/apache/lucene/index/MergeState;->readers:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-lt v2, v5, :cond_0

    .line 341
    return v0

    .line 331
    :cond_0
    iget-object v5, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v5, v5, Lorg/apache/lucene/index/MergeState;->readers:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/index/AtomicReader;

    .line 333
    .local v4, "reader":Lorg/apache/lucene/index/AtomicReader;
    iget-object v5, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v5, v5, Lorg/apache/lucene/index/MergeState;->docBase:[I

    aput v0, v5, v2

    .line 334
    invoke-static {v4}, Lorg/apache/lucene/index/MergeState$DocMap;->build(Lorg/apache/lucene/index/AtomicReader;)Lorg/apache/lucene/index/MergeState$DocMap;

    move-result-object v1

    .line 335
    .local v1, "docMap":Lorg/apache/lucene/index/MergeState$DocMap;
    iget-object v5, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v5, v5, Lorg/apache/lucene/index/MergeState;->docMaps:[Lorg/apache/lucene/index/MergeState$DocMap;

    aput-object v1, v5, v2

    .line 336
    invoke-virtual {v1}, Lorg/apache/lucene/index/MergeState$DocMap;->numDocs()I

    move-result v5

    add-int/2addr v0, v5

    .line 338
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private setMatchingSegmentReaders()V
    .locals 13

    .prologue
    .line 238
    iget-object v8, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v8, v8, Lorg/apache/lucene/index/MergeState;->readers:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v2

    .line 239
    .local v2, "numReaders":I
    iget-object v8, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    new-array v9, v2, [Lorg/apache/lucene/index/SegmentReader;

    iput-object v9, v8, Lorg/apache/lucene/index/MergeState;->matchingSegmentReaders:[Lorg/apache/lucene/index/SegmentReader;

    .line 245
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v2, :cond_1

    .line 270
    iget-object v8, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v8, v8, Lorg/apache/lucene/index/MergeState;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v9, "SM"

    invoke-virtual {v8, v9}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 271
    iget-object v8, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v8, v8, Lorg/apache/lucene/index/MergeState;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v9, "SM"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "merge store matchedCount="

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v11, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget v11, v11, Lorg/apache/lucene/index/MergeState;->matchedCount:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " vs "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v11, v11, Lorg/apache/lucene/index/MergeState;->readers:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    iget-object v8, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget v8, v8, Lorg/apache/lucene/index/MergeState;->matchedCount:I

    iget-object v9, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v9, v9, Lorg/apache/lucene/index/MergeState;->readers:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    if-eq v8, v9, :cond_0

    .line 273
    iget-object v8, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v8, v8, Lorg/apache/lucene/index/MergeState;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v9, "SM"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v11, v11, Lorg/apache/lucene/index/MergeState;->readers:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v11

    iget-object v12, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget v12, v12, Lorg/apache/lucene/index/MergeState;->matchedCount:I

    sub-int/2addr v11, v12

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " non-bulk merges"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    :cond_0
    return-void

    .line 246
    :cond_1
    iget-object v8, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v8, v8, Lorg/apache/lucene/index/MergeState;->readers:Ljava/util/List;

    invoke-interface {v8, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/index/AtomicReader;

    .line 252
    .local v4, "reader":Lorg/apache/lucene/index/AtomicReader;
    instance-of v8, v4, Lorg/apache/lucene/index/SegmentReader;

    if-eqz v8, :cond_3

    move-object v7, v4

    .line 253
    check-cast v7, Lorg/apache/lucene/index/SegmentReader;

    .line 254
    .local v7, "segmentReader":Lorg/apache/lucene/index/SegmentReader;
    const/4 v5, 0x1

    .line 255
    .local v5, "same":Z
    invoke-virtual {v7}, Lorg/apache/lucene/index/SegmentReader;->getFieldInfos()Lorg/apache/lucene/index/FieldInfos;

    move-result-object v6

    .line 256
    .local v6, "segmentFieldInfos":Lorg/apache/lucene/index/FieldInfos;
    invoke-virtual {v6}, Lorg/apache/lucene/index/FieldInfos;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-nez v9, :cond_4

    .line 263
    :goto_1
    if-eqz v5, :cond_3

    .line 264
    iget-object v8, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v8, v8, Lorg/apache/lucene/index/MergeState;->matchingSegmentReaders:[Lorg/apache/lucene/index/SegmentReader;

    aput-object v7, v8, v1

    .line 265
    iget-object v8, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget v9, v8, Lorg/apache/lucene/index/MergeState;->matchedCount:I

    add-int/lit8 v9, v9, 0x1

    iput v9, v8, Lorg/apache/lucene/index/MergeState;->matchedCount:I

    .line 245
    .end local v5    # "same":Z
    .end local v6    # "segmentFieldInfos":Lorg/apache/lucene/index/FieldInfos;
    .end local v7    # "segmentReader":Lorg/apache/lucene/index/SegmentReader;
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 256
    .restart local v5    # "same":Z
    .restart local v6    # "segmentFieldInfos":Lorg/apache/lucene/index/FieldInfos;
    .restart local v7    # "segmentReader":Lorg/apache/lucene/index/SegmentReader;
    :cond_4
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/FieldInfo;

    .line 257
    .local v0, "fi":Lorg/apache/lucene/index/FieldInfo;
    iget-object v9, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v9, v9, Lorg/apache/lucene/index/MergeState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    iget v10, v0, Lorg/apache/lucene/index/FieldInfo;->number:I

    invoke-virtual {v9, v10}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(I)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v3

    .line 258
    .local v3, "other":Lorg/apache/lucene/index/FieldInfo;
    if-eqz v3, :cond_5

    iget-object v9, v3, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    iget-object v10, v0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_2

    .line 259
    :cond_5
    const/4 v5, 0x0

    .line 260
    goto :goto_1
.end method


# virtual methods
.method merge()Lorg/apache/lucene/index/MergeState;
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 79
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v1, v1, Lorg/apache/lucene/index/MergeState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    invoke-direct {p0}, Lorg/apache/lucene/index/SegmentMerger;->setDocMaps()I

    move-result v2

    invoke-virtual {v1, v2}, Lorg/apache/lucene/index/SegmentInfo;->setDocCount(I)V

    .line 80
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentMerger;->mergeFieldInfos()V

    .line 81
    invoke-direct {p0}, Lorg/apache/lucene/index/SegmentMerger;->setMatchingSegmentReaders()V

    .line 82
    const-wide/16 v10, 0x0

    .line 83
    .local v10, "t0":J
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v1, v1, Lorg/apache/lucene/index/MergeState;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v2, "SM"

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 84
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v10

    .line 86
    :cond_0
    invoke-direct {p0}, Lorg/apache/lucene/index/SegmentMerger;->mergeFields()I

    move-result v9

    .line 87
    .local v9, "numMerged":I
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v1, v1, Lorg/apache/lucene/index/MergeState;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v2, "SM"

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 88
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v12

    .line 89
    .local v12, "t1":J
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v1, v1, Lorg/apache/lucene/index/MergeState;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v2, "SM"

    new-instance v3, Ljava/lang/StringBuilder;

    sub-long v4, v12, v10

    const-wide/32 v6, 0xf4240

    div-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " msec to merge stored fields ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " docs]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    .end local v12    # "t1":J
    :cond_1
    sget-boolean v1, Lorg/apache/lucene/index/SegmentMerger;->$assertionsDisabled:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v1, v1, Lorg/apache/lucene/index/MergeState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v1

    if-eq v9, v1, :cond_2

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 93
    :cond_2
    new-instance v0, Lorg/apache/lucene/index/SegmentWriteState;

    iget-object v1, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v1, v1, Lorg/apache/lucene/index/MergeState;->infoStream:Lorg/apache/lucene/util/InfoStream;

    iget-object v2, p0, Lorg/apache/lucene/index/SegmentMerger;->directory:Lorg/apache/lucene/store/Directory;

    iget-object v3, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v3, v3, Lorg/apache/lucene/index/MergeState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    .line 94
    iget-object v4, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v4, v4, Lorg/apache/lucene/index/MergeState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    iget v5, p0, Lorg/apache/lucene/index/SegmentMerger;->termIndexInterval:I

    const/4 v6, 0x0

    iget-object v7, p0, Lorg/apache/lucene/index/SegmentMerger;->context:Lorg/apache/lucene/store/IOContext;

    .line 93
    invoke-direct/range {v0 .. v7}, Lorg/apache/lucene/index/SegmentWriteState;-><init>(Lorg/apache/lucene/util/InfoStream;Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/index/FieldInfos;ILorg/apache/lucene/index/BufferedDeletes;Lorg/apache/lucene/store/IOContext;)V

    .line 95
    .local v0, "segmentWriteState":Lorg/apache/lucene/index/SegmentWriteState;
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v1, v1, Lorg/apache/lucene/index/MergeState;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v2, "SM"

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 96
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v10

    .line 98
    :cond_3
    invoke-direct {p0, v0}, Lorg/apache/lucene/index/SegmentMerger;->mergeTerms(Lorg/apache/lucene/index/SegmentWriteState;)V

    .line 99
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v1, v1, Lorg/apache/lucene/index/MergeState;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v2, "SM"

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 100
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v12

    .line 101
    .restart local v12    # "t1":J
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v1, v1, Lorg/apache/lucene/index/MergeState;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v2, "SM"

    new-instance v3, Ljava/lang/StringBuilder;

    sub-long v4, v12, v10

    const-wide/32 v6, 0xf4240

    div-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " msec to merge postings ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " docs]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    .end local v12    # "t1":J
    :cond_4
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v1, v1, Lorg/apache/lucene/index/MergeState;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v2, "SM"

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 105
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v10

    .line 107
    :cond_5
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v1, v1, Lorg/apache/lucene/index/MergeState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v1}, Lorg/apache/lucene/index/FieldInfos;->hasDocValues()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 108
    invoke-direct {p0, v0}, Lorg/apache/lucene/index/SegmentMerger;->mergeDocValues(Lorg/apache/lucene/index/SegmentWriteState;)V

    .line 110
    :cond_6
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v1, v1, Lorg/apache/lucene/index/MergeState;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v2, "SM"

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 111
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v12

    .line 112
    .restart local v12    # "t1":J
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v1, v1, Lorg/apache/lucene/index/MergeState;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v2, "SM"

    new-instance v3, Ljava/lang/StringBuilder;

    sub-long v4, v12, v10

    const-wide/32 v6, 0xf4240

    div-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " msec to merge doc values ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " docs]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    .end local v12    # "t1":J
    :cond_7
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v1, v1, Lorg/apache/lucene/index/MergeState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v1}, Lorg/apache/lucene/index/FieldInfos;->hasNorms()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 116
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v1, v1, Lorg/apache/lucene/index/MergeState;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v2, "SM"

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 117
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v10

    .line 119
    :cond_8
    invoke-direct {p0, v0}, Lorg/apache/lucene/index/SegmentMerger;->mergeNorms(Lorg/apache/lucene/index/SegmentWriteState;)V

    .line 120
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v1, v1, Lorg/apache/lucene/index/MergeState;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v2, "SM"

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 121
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v12

    .line 122
    .restart local v12    # "t1":J
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v1, v1, Lorg/apache/lucene/index/MergeState;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v2, "SM"

    new-instance v3, Ljava/lang/StringBuilder;

    sub-long v4, v12, v10

    const-wide/32 v6, 0xf4240

    div-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " msec to merge norms ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " docs]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    .end local v12    # "t1":J
    :cond_9
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v1, v1, Lorg/apache/lucene/index/MergeState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v1}, Lorg/apache/lucene/index/FieldInfos;->hasVectors()Z

    move-result v1

    if-eqz v1, :cond_c

    .line 127
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v1, v1, Lorg/apache/lucene/index/MergeState;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v2, "SM"

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 128
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v10

    .line 130
    :cond_a
    invoke-direct {p0}, Lorg/apache/lucene/index/SegmentMerger;->mergeVectors()I

    move-result v9

    .line 131
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v1, v1, Lorg/apache/lucene/index/MergeState;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v2, "SM"

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 132
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v12

    .line 133
    .restart local v12    # "t1":J
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v1, v1, Lorg/apache/lucene/index/MergeState;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v2, "SM"

    new-instance v3, Ljava/lang/StringBuilder;

    sub-long v4, v12, v10

    const-wide/32 v6, 0xf4240

    div-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " msec to merge vectors ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " docs]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    .end local v12    # "t1":J
    :cond_b
    sget-boolean v1, Lorg/apache/lucene/index/SegmentMerger;->$assertionsDisabled:Z

    if-nez v1, :cond_c

    iget-object v1, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v1, v1, Lorg/apache/lucene/index/MergeState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v1

    if-eq v9, v1, :cond_c

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 139
    :cond_c
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentMerger;->codec:Lorg/apache/lucene/codecs/Codec;

    invoke-virtual {v1}, Lorg/apache/lucene/codecs/Codec;->fieldInfosFormat()Lorg/apache/lucene/codecs/FieldInfosFormat;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/lucene/codecs/FieldInfosFormat;->getFieldInfosWriter()Lorg/apache/lucene/codecs/FieldInfosWriter;

    move-result-object v8

    .line 140
    .local v8, "fieldInfosWriter":Lorg/apache/lucene/codecs/FieldInfosWriter;
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentMerger;->directory:Lorg/apache/lucene/store/Directory;

    iget-object v2, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v2, v2, Lorg/apache/lucene/index/MergeState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v2, v2, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    iget-object v3, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v3, v3, Lorg/apache/lucene/index/MergeState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    iget-object v4, p0, Lorg/apache/lucene/index/SegmentMerger;->context:Lorg/apache/lucene/store/IOContext;

    invoke-virtual {v8, v1, v2, v3, v4}, Lorg/apache/lucene/codecs/FieldInfosWriter;->write(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/store/IOContext;)V

    .line 142
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    return-object v1
.end method

.method public mergeFieldInfos()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 279
    iget-object v3, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v3, v3, Lorg/apache/lucene/index/MergeState;->readers:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 285
    iget-object v3, p0, Lorg/apache/lucene/index/SegmentMerger;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v4, p0, Lorg/apache/lucene/index/SegmentMerger;->fieldInfosBuilder:Lorg/apache/lucene/index/FieldInfos$Builder;

    invoke-virtual {v4}, Lorg/apache/lucene/index/FieldInfos$Builder;->finish()Lorg/apache/lucene/index/FieldInfos;

    move-result-object v4

    iput-object v4, v3, Lorg/apache/lucene/index/MergeState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    .line 286
    return-void

    .line 279
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/AtomicReader;

    .line 280
    .local v1, "reader":Lorg/apache/lucene/index/AtomicReader;
    invoke-virtual {v1}, Lorg/apache/lucene/index/AtomicReader;->getFieldInfos()Lorg/apache/lucene/index/FieldInfos;

    move-result-object v2

    .line 281
    .local v2, "readerFieldInfos":Lorg/apache/lucene/index/FieldInfos;
    invoke-virtual {v2}, Lorg/apache/lucene/index/FieldInfos;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/FieldInfo;

    .line 282
    .local v0, "fi":Lorg/apache/lucene/index/FieldInfo;
    iget-object v5, p0, Lorg/apache/lucene/index/SegmentMerger;->fieldInfosBuilder:Lorg/apache/lucene/index/FieldInfos$Builder;

    invoke-virtual {v5, v0}, Lorg/apache/lucene/index/FieldInfos$Builder;->add(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/FieldInfo;

    goto :goto_0
.end method
