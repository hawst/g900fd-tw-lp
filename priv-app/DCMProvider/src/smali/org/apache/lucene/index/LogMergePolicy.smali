.class public abstract Lorg/apache/lucene/index/LogMergePolicy;
.super Lorg/apache/lucene/index/MergePolicy;
.source "LogMergePolicy.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final DEFAULT_MAX_CFS_SEGMENT_SIZE:J = 0x7fffffffffffffffL

.field public static final DEFAULT_MAX_MERGE_DOCS:I = 0x7fffffff

.field public static final DEFAULT_MERGE_FACTOR:I = 0xa

.field public static final DEFAULT_NO_CFS_RATIO:D = 0.1

.field public static final LEVEL_LOG_SPAN:D = 0.75


# instance fields
.field protected calibrateSizeByDeletes:Z

.field protected maxCFSSegmentSize:J

.field protected maxMergeDocs:I

.field protected maxMergeSize:J

.field protected maxMergeSizeForForcedMerge:J

.field protected mergeFactor:I

.field protected minMergeSize:J

.field protected noCFSRatio:D

.field protected useCompoundFile:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const-class v0, Lorg/apache/lucene/index/LogMergePolicy;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/LogMergePolicy;->$assertionsDisabled:Z

    .line 75
    return-void

    .line 49
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 6

    .prologue
    const-wide v4, 0x7fffffffffffffffL

    const/4 v2, 0x1

    .line 122
    invoke-direct {p0}, Lorg/apache/lucene/index/MergePolicy;-><init>()V

    .line 78
    const/16 v0, 0xa

    iput v0, p0, Lorg/apache/lucene/index/LogMergePolicy;->mergeFactor:I

    .line 93
    iput-wide v4, p0, Lorg/apache/lucene/index/LogMergePolicy;->maxMergeSizeForForcedMerge:J

    .line 97
    const v0, 0x7fffffff

    iput v0, p0, Lorg/apache/lucene/index/LogMergePolicy;->maxMergeDocs:I

    .line 103
    const-wide v0, 0x3fb999999999999aL    # 0.1

    iput-wide v0, p0, Lorg/apache/lucene/index/LogMergePolicy;->noCFSRatio:D

    .line 107
    iput-wide v4, p0, Lorg/apache/lucene/index/LogMergePolicy;->maxCFSSegmentSize:J

    .line 111
    iput-boolean v2, p0, Lorg/apache/lucene/index/LogMergePolicy;->calibrateSizeByDeletes:Z

    .line 117
    iput-boolean v2, p0, Lorg/apache/lucene/index/LogMergePolicy;->useCompoundFile:Z

    .line 123
    return-void
.end method

.method private findForcedMergesMaxNumSegments(Lorg/apache/lucene/index/SegmentInfos;II)Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    .locals 16
    .param p1, "infos"    # Lorg/apache/lucene/index/SegmentInfos;
    .param p2, "maxNumSegments"    # I
    .param p3, "last"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 350
    new-instance v9, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;

    invoke-direct {v9}, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;-><init>()V

    .line 351
    .local v9, "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/SegmentInfos;->asList()Ljava/util/List;

    move-result-object v8

    .line 355
    .local v8, "segments":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfoPerCommit;>;"
    :goto_0
    sub-int v12, p3, p2

    add-int/lit8 v12, v12, 0x1

    move-object/from16 v0, p0

    iget v13, v0, Lorg/apache/lucene/index/LogMergePolicy;->mergeFactor:I

    if-ge v12, v13, :cond_3

    .line 362
    iget-object v12, v9, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;->merges:Ljava/util/List;

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v12

    if-nez v12, :cond_1

    .line 363
    const/4 v12, 0x1

    move/from16 v0, p2

    if-ne v0, v12, :cond_4

    .line 367
    const/4 v12, 0x1

    move/from16 v0, p3

    if-gt v0, v12, :cond_0

    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lorg/apache/lucene/index/SegmentInfos;->info(I)Lorg/apache/lucene/index/SegmentInfoPerCommit;

    move-result-object v12

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lorg/apache/lucene/index/LogMergePolicy;->isMerged(Lorg/apache/lucene/index/SegmentInfoPerCommit;)Z

    move-result v12

    if-nez v12, :cond_1

    .line 368
    :cond_0
    new-instance v12, Lorg/apache/lucene/index/MergePolicy$OneMerge;

    const/4 v13, 0x0

    move/from16 v0, p3

    invoke-interface {v8, v13, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v13

    invoke-direct {v12, v13}, Lorg/apache/lucene/index/MergePolicy$OneMerge;-><init>(Ljava/util/List;)V

    invoke-virtual {v9, v12}, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;->add(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    .line 401
    :cond_1
    :goto_1
    iget-object v12, v9, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;->merges:Ljava/util/List;

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v12

    if-nez v12, :cond_2

    const/4 v9, 0x0

    .end local v9    # "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    :cond_2
    return-object v9

    .line 356
    .restart local v9    # "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    :cond_3
    new-instance v12, Lorg/apache/lucene/index/MergePolicy$OneMerge;

    move-object/from16 v0, p0

    iget v13, v0, Lorg/apache/lucene/index/LogMergePolicy;->mergeFactor:I

    sub-int v13, p3, v13

    move/from16 v0, p3

    invoke-interface {v8, v13, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v13

    invoke-direct {v12, v13}, Lorg/apache/lucene/index/MergePolicy$OneMerge;-><init>(Ljava/util/List;)V

    invoke-virtual {v9, v12}, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;->add(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    .line 357
    move-object/from16 v0, p0

    iget v12, v0, Lorg/apache/lucene/index/LogMergePolicy;->mergeFactor:I

    sub-int p3, p3, v12

    goto :goto_0

    .line 370
    :cond_4
    move/from16 v0, p3

    move/from16 v1, p2

    if-le v0, v1, :cond_1

    .line 381
    sub-int v12, p3, p2

    add-int/lit8 v5, v12, 0x1

    .line 384
    .local v5, "finalMergeSize":I
    const-wide/16 v2, 0x0

    .line 385
    .local v2, "bestSize":J
    const/4 v4, 0x0

    .line 387
    .local v4, "bestStart":I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_2
    sub-int v12, p3, v5

    add-int/lit8 v12, v12, 0x1

    if-lt v6, v12, :cond_5

    .line 398
    new-instance v12, Lorg/apache/lucene/index/MergePolicy$OneMerge;

    add-int v13, v4, v5

    invoke-interface {v8, v4, v13}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v13

    invoke-direct {v12, v13}, Lorg/apache/lucene/index/MergePolicy$OneMerge;-><init>(Ljava/util/List;)V

    invoke-virtual {v9, v12}, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;->add(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    goto :goto_1

    .line 388
    :cond_5
    const-wide/16 v10, 0x0

    .line 389
    .local v10, "sumSize":J
    const/4 v7, 0x0

    .local v7, "j":I
    :goto_3
    if-lt v7, v5, :cond_8

    .line 392
    if-eqz v6, :cond_6

    const-wide/16 v12, 0x2

    add-int/lit8 v14, v6, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lorg/apache/lucene/index/SegmentInfos;->info(I)Lorg/apache/lucene/index/SegmentInfoPerCommit;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lorg/apache/lucene/index/LogMergePolicy;->size(Lorg/apache/lucene/index/SegmentInfoPerCommit;)J

    move-result-wide v14

    mul-long/2addr v12, v14

    cmp-long v12, v10, v12

    if-gez v12, :cond_7

    cmp-long v12, v10, v2

    if-gez v12, :cond_7

    .line 393
    :cond_6
    move v4, v6

    .line 394
    move-wide v2, v10

    .line 387
    :cond_7
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 390
    :cond_8
    add-int v12, v7, v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lorg/apache/lucene/index/SegmentInfos;->info(I)Lorg/apache/lucene/index/SegmentInfoPerCommit;

    move-result-object v12

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lorg/apache/lucene/index/LogMergePolicy;->size(Lorg/apache/lucene/index/SegmentInfoPerCommit;)J

    move-result-wide v12

    add-long/2addr v10, v12

    .line 389
    add-int/lit8 v7, v7, 0x1

    goto :goto_3
.end method

.method private findForcedMergesSizeLimit(Lorg/apache/lucene/index/SegmentInfos;II)Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    .locals 8
    .param p1, "infos"    # Lorg/apache/lucene/index/SegmentInfos;
    .param p2, "maxNumSegments"    # I
    .param p3, "last"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 309
    new-instance v2, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;

    invoke-direct {v2}, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;-><init>()V

    .line 310
    .local v2, "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfos;->asList()Ljava/util/List;

    move-result-object v1

    .line 312
    .local v1, "segments":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfoPerCommit;>;"
    add-int/lit8 v3, p3, -0x1

    .line 313
    .local v3, "start":I
    :goto_0
    if-gez v3, :cond_3

    .line 337
    if-lez p3, :cond_1

    add-int/lit8 v3, v3, 0x1

    add-int/lit8 v4, v3, 0x1

    if-lt v4, p3, :cond_0

    invoke-virtual {p1, v3}, Lorg/apache/lucene/index/SegmentInfos;->info(I)Lorg/apache/lucene/index/SegmentInfoPerCommit;

    move-result-object v4

    invoke-virtual {p0, v4}, Lorg/apache/lucene/index/LogMergePolicy;->isMerged(Lorg/apache/lucene/index/SegmentInfoPerCommit;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 338
    :cond_0
    new-instance v4, Lorg/apache/lucene/index/MergePolicy$OneMerge;

    invoke-interface {v1, v3, p3}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/apache/lucene/index/MergePolicy$OneMerge;-><init>(Ljava/util/List;)V

    invoke-virtual {v2, v4}, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;->add(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    .line 341
    :cond_1
    iget-object v4, v2, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;->merges:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_2

    const/4 v2, 0x0

    .end local v2    # "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    :cond_2
    return-object v2

    .line 314
    .restart local v2    # "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    :cond_3
    invoke-virtual {p1, v3}, Lorg/apache/lucene/index/SegmentInfos;->info(I)Lorg/apache/lucene/index/SegmentInfoPerCommit;

    move-result-object v0

    .line 315
    .local v0, "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/LogMergePolicy;->size(Lorg/apache/lucene/index/SegmentInfoPerCommit;)J

    move-result-wide v4

    iget-wide v6, p0, Lorg/apache/lucene/index/LogMergePolicy;->maxMergeSizeForForcedMerge:J

    cmp-long v4, v4, v6

    if-gtz v4, :cond_4

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/LogMergePolicy;->sizeDocs(Lorg/apache/lucene/index/SegmentInfoPerCommit;)J

    move-result-wide v4

    iget v6, p0, Lorg/apache/lucene/index/LogMergePolicy;->maxMergeDocs:I

    int-to-long v6, v6

    cmp-long v4, v4, v6

    if-lez v4, :cond_9

    .line 316
    :cond_4
    invoke-virtual {p0}, Lorg/apache/lucene/index/LogMergePolicy;->verbose()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 317
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "findForcedMergesSizeLimit: skip segment="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": size is > maxMergeSize ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v6, p0, Lorg/apache/lucene/index/LogMergePolicy;->maxMergeSizeForForcedMerge:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") or sizeDocs is > maxMergeDocs ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lorg/apache/lucene/index/LogMergePolicy;->maxMergeDocs:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lorg/apache/lucene/index/LogMergePolicy;->message(Ljava/lang/String;)V

    .line 321
    :cond_5
    sub-int v4, p3, v3

    add-int/lit8 v4, v4, -0x1

    const/4 v5, 0x1

    if-gt v4, v5, :cond_6

    add-int/lit8 v4, p3, -0x1

    if-eq v3, v4, :cond_7

    add-int/lit8 v4, v3, 0x1

    invoke-virtual {p1, v4}, Lorg/apache/lucene/index/SegmentInfos;->info(I)Lorg/apache/lucene/index/SegmentInfoPerCommit;

    move-result-object v4

    invoke-virtual {p0, v4}, Lorg/apache/lucene/index/LogMergePolicy;->isMerged(Lorg/apache/lucene/index/SegmentInfoPerCommit;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 324
    :cond_6
    new-instance v4, Lorg/apache/lucene/index/MergePolicy$OneMerge;

    add-int/lit8 v5, v3, 0x1

    invoke-interface {v1, v5, p3}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/apache/lucene/index/MergePolicy$OneMerge;-><init>(Ljava/util/List;)V

    invoke-virtual {v2, v4}, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;->add(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    .line 326
    :cond_7
    move p3, v3

    .line 332
    :cond_8
    :goto_1
    add-int/lit8 v3, v3, -0x1

    goto/16 :goto_0

    .line 327
    :cond_9
    sub-int v4, p3, v3

    iget v5, p0, Lorg/apache/lucene/index/LogMergePolicy;->mergeFactor:I

    if-ne v4, v5, :cond_8

    .line 329
    new-instance v4, Lorg/apache/lucene/index/MergePolicy$OneMerge;

    invoke-interface {v1, v3, p3}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/apache/lucene/index/MergePolicy$OneMerge;-><init>(Ljava/util/List;)V

    invoke-virtual {v2, v4}, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;->add(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    .line 330
    move p3, v3

    goto :goto_1
.end method


# virtual methods
.method public close()V
    .locals 0

    .prologue
    .line 227
    return-void
.end method

.method public findForcedDeletesMerges(Lorg/apache/lucene/index/SegmentInfos;)Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    .locals 11
    .param p1, "segmentInfos"    # Lorg/apache/lucene/index/SegmentInfos;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v10, -0x1

    .line 484
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfos;->asList()Ljava/util/List;

    move-result-object v5

    .line 485
    .local v5, "segments":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfoPerCommit;>;"
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v4

    .line 487
    .local v4, "numSegments":I
    invoke-virtual {p0}, Lorg/apache/lucene/index/LogMergePolicy;->verbose()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 488
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "findForcedDeleteMerges: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " segments"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lorg/apache/lucene/index/LogMergePolicy;->message(Ljava/lang/String;)V

    .line 491
    :cond_0
    new-instance v6, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;

    invoke-direct {v6}, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;-><init>()V

    .line 492
    .local v6, "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    const/4 v1, -0x1

    .line 493
    .local v1, "firstSegmentWithDeletions":I
    iget-object v8, p0, Lorg/apache/lucene/index/LogMergePolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    invoke-virtual {v8}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/lucene/index/IndexWriter;

    .line 494
    .local v7, "w":Lorg/apache/lucene/index/IndexWriter;
    sget-boolean v8, Lorg/apache/lucene/index/LogMergePolicy;->$assertionsDisabled:Z

    if-nez v8, :cond_1

    if-nez v7, :cond_1

    new-instance v8, Ljava/lang/AssertionError;

    invoke-direct {v8}, Ljava/lang/AssertionError;-><init>()V

    throw v8

    .line 495
    :cond_1
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-lt v2, v4, :cond_4

    .line 525
    if-eq v1, v10, :cond_3

    .line 526
    invoke-virtual {p0}, Lorg/apache/lucene/index/LogMergePolicy;->verbose()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 527
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "  add merge "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " to "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    add-int/lit8 v9, v4, -0x1

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " inclusive"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lorg/apache/lucene/index/LogMergePolicy;->message(Ljava/lang/String;)V

    .line 529
    :cond_2
    new-instance v8, Lorg/apache/lucene/index/MergePolicy$OneMerge;

    invoke-interface {v5, v1, v4}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v9

    invoke-direct {v8, v9}, Lorg/apache/lucene/index/MergePolicy$OneMerge;-><init>(Ljava/util/List;)V

    invoke-virtual {v6, v8}, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;->add(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    .line 532
    :cond_3
    return-object v6

    .line 496
    :cond_4
    invoke-virtual {p1, v2}, Lorg/apache/lucene/index/SegmentInfos;->info(I)Lorg/apache/lucene/index/SegmentInfoPerCommit;

    move-result-object v3

    .line 497
    .local v3, "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    invoke-virtual {v7, v3}, Lorg/apache/lucene/index/IndexWriter;->numDeletedDocs(Lorg/apache/lucene/index/SegmentInfoPerCommit;)I

    move-result v0

    .line 498
    .local v0, "delCount":I
    if-lez v0, :cond_9

    .line 499
    invoke-virtual {p0}, Lorg/apache/lucene/index/LogMergePolicy;->verbose()Z

    move-result v8

    if-eqz v8, :cond_5

    .line 500
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "  segment "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v9, v3, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v9, v9, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " has deletions"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lorg/apache/lucene/index/LogMergePolicy;->message(Ljava/lang/String;)V

    .line 502
    :cond_5
    if-ne v1, v10, :cond_7

    .line 503
    move v1, v2

    .line 495
    :cond_6
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 504
    :cond_7
    sub-int v8, v2, v1

    iget v9, p0, Lorg/apache/lucene/index/LogMergePolicy;->mergeFactor:I

    if-ne v8, v9, :cond_6

    .line 507
    invoke-virtual {p0}, Lorg/apache/lucene/index/LogMergePolicy;->verbose()Z

    move-result v8

    if-eqz v8, :cond_8

    .line 508
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "  add merge "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " to "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    add-int/lit8 v9, v2, -0x1

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " inclusive"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lorg/apache/lucene/index/LogMergePolicy;->message(Ljava/lang/String;)V

    .line 510
    :cond_8
    new-instance v8, Lorg/apache/lucene/index/MergePolicy$OneMerge;

    invoke-interface {v5, v1, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v9

    invoke-direct {v8, v9}, Lorg/apache/lucene/index/MergePolicy$OneMerge;-><init>(Ljava/util/List;)V

    invoke-virtual {v6, v8}, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;->add(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    .line 511
    move v1, v2

    .line 513
    goto :goto_1

    :cond_9
    if-eq v1, v10, :cond_6

    .line 517
    invoke-virtual {p0}, Lorg/apache/lucene/index/LogMergePolicy;->verbose()Z

    move-result v8

    if-eqz v8, :cond_a

    .line 518
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "  add merge "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " to "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    add-int/lit8 v9, v2, -0x1

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " inclusive"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lorg/apache/lucene/index/LogMergePolicy;->message(Ljava/lang/String;)V

    .line 520
    :cond_a
    new-instance v8, Lorg/apache/lucene/index/MergePolicy$OneMerge;

    invoke-interface {v5, v1, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v9

    invoke-direct {v8, v9}, Lorg/apache/lucene/index/MergePolicy$OneMerge;-><init>(Ljava/util/List;)V

    invoke-virtual {v6, v8}, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;->add(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    .line 521
    const/4 v1, -0x1

    goto :goto_1
.end method

.method public findForcedMerges(Lorg/apache/lucene/index/SegmentInfos;ILjava/util/Map;)Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    .locals 8
    .param p1, "infos"    # Lorg/apache/lucene/index/SegmentInfos;
    .param p2, "maxNumSegments"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/SegmentInfos;",
            "I",
            "Ljava/util/Map",
            "<",
            "Lorg/apache/lucene/index/SegmentInfoPerCommit;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lorg/apache/lucene/index/MergePolicy$MergeSpecification;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p3, "segmentsToMerge":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/index/SegmentInfoPerCommit;Ljava/lang/Boolean;>;"
    const/4 v7, 0x1

    const/4 v4, 0x0

    .line 418
    sget-boolean v5, Lorg/apache/lucene/index/LogMergePolicy;->$assertionsDisabled:Z

    if-nez v5, :cond_0

    if-gtz p2, :cond_0

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 419
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/index/LogMergePolicy;->verbose()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 420
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "findForcedMerges: maxNumSegs="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " segsToMerge="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lorg/apache/lucene/index/LogMergePolicy;->message(Ljava/lang/String;)V

    .line 425
    :cond_1
    invoke-virtual {p0, p1, p2, p3}, Lorg/apache/lucene/index/LogMergePolicy;->isMerged(Lorg/apache/lucene/index/SegmentInfos;ILjava/util/Map;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 426
    invoke-virtual {p0}, Lorg/apache/lucene/index/LogMergePolicy;->verbose()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 427
    const-string v5, "already merged; skip"

    invoke-virtual {p0, v5}, Lorg/apache/lucene/index/LogMergePolicy;->message(Ljava/lang/String;)V

    .line 472
    :cond_2
    :goto_0
    return-object v4

    .line 435
    :cond_3
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfos;->size()I

    move-result v3

    .line 436
    .local v3, "last":I
    :cond_4
    if-gtz v3, :cond_5

    .line 444
    :goto_1
    if-nez v3, :cond_6

    .line 445
    invoke-virtual {p0}, Lorg/apache/lucene/index/LogMergePolicy;->verbose()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 446
    const-string v5, "last == 0; skip"

    invoke-virtual {p0, v5}, Lorg/apache/lucene/index/LogMergePolicy;->message(Ljava/lang/String;)V

    goto :goto_0

    .line 437
    :cond_5
    add-int/lit8 v3, v3, -0x1

    invoke-virtual {p1, v3}, Lorg/apache/lucene/index/SegmentInfos;->info(I)Lorg/apache/lucene/index/SegmentInfoPerCommit;

    move-result-object v2

    .line 438
    .local v2, "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    invoke-interface {p3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_4

    .line 439
    add-int/lit8 v3, v3, 0x1

    .line 440
    goto :goto_1

    .line 452
    .end local v2    # "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    :cond_6
    if-ne p2, v7, :cond_7

    if-ne v3, v7, :cond_7

    const/4 v5, 0x0

    invoke-virtual {p1, v5}, Lorg/apache/lucene/index/SegmentInfos;->info(I)Lorg/apache/lucene/index/SegmentInfoPerCommit;

    move-result-object v5

    invoke-virtual {p0, v5}, Lorg/apache/lucene/index/LogMergePolicy;->isMerged(Lorg/apache/lucene/index/SegmentInfoPerCommit;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 453
    invoke-virtual {p0}, Lorg/apache/lucene/index/LogMergePolicy;->verbose()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 454
    const-string v5, "already 1 seg; skip"

    invoke-virtual {p0, v5}, Lorg/apache/lucene/index/LogMergePolicy;->message(Ljava/lang/String;)V

    goto :goto_0

    .line 460
    :cond_7
    const/4 v0, 0x0

    .line 461
    .local v0, "anyTooLarge":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    if-lt v1, v3, :cond_8

    .line 469
    :goto_3
    if-eqz v0, :cond_b

    .line 470
    invoke-direct {p0, p1, p2, v3}, Lorg/apache/lucene/index/LogMergePolicy;->findForcedMergesSizeLimit(Lorg/apache/lucene/index/SegmentInfos;II)Lorg/apache/lucene/index/MergePolicy$MergeSpecification;

    move-result-object v4

    goto :goto_0

    .line 462
    :cond_8
    invoke-virtual {p1, v1}, Lorg/apache/lucene/index/SegmentInfos;->info(I)Lorg/apache/lucene/index/SegmentInfoPerCommit;

    move-result-object v2

    .line 463
    .restart local v2    # "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    invoke-virtual {p0, v2}, Lorg/apache/lucene/index/LogMergePolicy;->size(Lorg/apache/lucene/index/SegmentInfoPerCommit;)J

    move-result-wide v4

    iget-wide v6, p0, Lorg/apache/lucene/index/LogMergePolicy;->maxMergeSizeForForcedMerge:J

    cmp-long v4, v4, v6

    if-gtz v4, :cond_9

    invoke-virtual {p0, v2}, Lorg/apache/lucene/index/LogMergePolicy;->sizeDocs(Lorg/apache/lucene/index/SegmentInfoPerCommit;)J

    move-result-wide v4

    iget v6, p0, Lorg/apache/lucene/index/LogMergePolicy;->maxMergeDocs:I

    int-to-long v6, v6

    cmp-long v4, v4, v6

    if-lez v4, :cond_a

    .line 464
    :cond_9
    const/4 v0, 0x1

    .line 465
    goto :goto_3

    .line 461
    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 472
    .end local v2    # "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    :cond_b
    invoke-direct {p0, p1, p2, v3}, Lorg/apache/lucene/index/LogMergePolicy;->findForcedMergesMaxNumSegments(Lorg/apache/lucene/index/SegmentInfos;II)Lorg/apache/lucene/index/MergePolicy$MergeSpecification;

    move-result-object v4

    goto :goto_0
.end method

.method public findMerges(Lorg/apache/lucene/index/MergePolicy$MergeTrigger;Lorg/apache/lucene/index/SegmentInfos;)Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    .locals 36
    .param p1, "mergeTrigger"    # Lorg/apache/lucene/index/MergePolicy$MergeTrigger;
    .param p2, "infos"    # Lorg/apache/lucene/index/SegmentInfos;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 563
    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/index/SegmentInfos;->size()I

    move-result v18

    .line 564
    .local v18, "numSegments":I
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/LogMergePolicy;->verbose()Z

    move-result v26

    if-eqz v26, :cond_0

    .line 565
    new-instance v26, Ljava/lang/StringBuilder;

    const-string v27, "findMerges: "

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v26

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, " segments"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/LogMergePolicy;->message(Ljava/lang/String;)V

    .line 570
    :cond_0
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 571
    .local v12, "levels":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;>;"
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/index/LogMergePolicy;->mergeFactor:I

    move/from16 v26, v0

    move/from16 v0, v26

    int-to-double v0, v0

    move-wide/from16 v26, v0

    invoke-static/range {v26 .. v27}, Ljava/lang/Math;->log(D)D

    move-result-wide v26

    move-wide/from16 v0, v26

    double-to-float v0, v0

    move/from16 v16, v0

    .line 573
    .local v16, "norm":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/LogMergePolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual/range {v26 .. v26}, Lorg/apache/lucene/index/IndexWriter;->getMergingSegments()Ljava/util/Collection;

    move-result-object v15

    .line 575
    .local v15, "mergingSegments":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/index/SegmentInfoPerCommit;>;"
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    move/from16 v0, v18

    if-lt v6, v0, :cond_1

    .line 598
    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/index/LogMergePolicy;->minMergeSize:J

    move-wide/from16 v26, v0

    const-wide/16 v28, 0x0

    cmp-long v26, v26, v28

    if-gtz v26, :cond_6

    .line 599
    const/4 v11, 0x0

    .line 610
    .local v11, "levelFloor":F
    :goto_1
    const/16 v19, 0x0

    .line 612
    .local v19, "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v17

    .line 614
    .local v17, "numMergeableSegments":I
    const/16 v24, 0x0

    .line 615
    .local v24, "start":I
    :goto_2
    move/from16 v0, v24

    move/from16 v1, v17

    if-lt v0, v1, :cond_7

    .line 692
    return-object v19

    .line 576
    .end local v11    # "levelFloor":F
    .end local v17    # "numMergeableSegments":I
    .end local v19    # "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    .end local v24    # "start":I
    :cond_1
    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Lorg/apache/lucene/index/SegmentInfos;->info(I)Lorg/apache/lucene/index/SegmentInfoPerCommit;

    move-result-object v7

    .line 577
    .local v7, "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lorg/apache/lucene/index/LogMergePolicy;->size(Lorg/apache/lucene/index/SegmentInfoPerCommit;)J

    move-result-wide v22

    .line 580
    .local v22, "size":J
    const-wide/16 v26, 0x1

    cmp-long v26, v22, v26

    if-gez v26, :cond_2

    .line 581
    const-wide/16 v22, 0x1

    .line 584
    :cond_2
    new-instance v8, Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;

    move-wide/from16 v0, v22

    long-to-double v0, v0

    move-wide/from16 v26, v0

    invoke-static/range {v26 .. v27}, Ljava/lang/Math;->log(D)D

    move-result-wide v26

    move-wide/from16 v0, v26

    double-to-float v0, v0

    move/from16 v26, v0

    div-float v26, v26, v16

    move/from16 v0, v26

    invoke-direct {v8, v7, v0, v6}, Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;-><init>(Lorg/apache/lucene/index/SegmentInfoPerCommit;FI)V

    .line 585
    .local v8, "infoLevel":Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;
    invoke-interface {v12, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 587
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/LogMergePolicy;->verbose()Z

    move-result v26

    if-eqz v26, :cond_4

    .line 588
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lorg/apache/lucene/index/LogMergePolicy;->sizeBytes(Lorg/apache/lucene/index/SegmentInfoPerCommit;)J

    move-result-wide v20

    .line 589
    .local v20, "segBytes":J
    invoke-interface {v15, v7}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_5

    const-string v5, " [merging]"

    .line 590
    .local v5, "extra":Ljava/lang/String;
    :goto_3
    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/index/LogMergePolicy;->maxMergeSize:J

    move-wide/from16 v26, v0

    cmp-long v26, v22, v26

    if-ltz v26, :cond_3

    .line 591
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v27

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v27, " [skip: too large]"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 593
    :cond_3
    new-instance v27, Ljava/lang/StringBuilder;

    const-string v26, "seg="

    move-object/from16 v0, v27

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/LogMergePolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lorg/apache/lucene/index/IndexWriter;

    move-object/from16 v0, v26

    invoke-virtual {v0, v7}, Lorg/apache/lucene/index/IndexWriter;->segString(Lorg/apache/lucene/index/SegmentInfoPerCommit;)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v27

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, " level="

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    iget v0, v8, Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;->level:F

    move/from16 v27, v0

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, " size="

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    sget-object v27, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    const-string v28, "%.3f MB"

    const/16 v29, 0x1

    move/from16 v0, v29

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v29, v0

    const/16 v30, 0x0

    const-wide/16 v32, 0x400

    div-long v32, v20, v32

    move-wide/from16 v0, v32

    long-to-double v0, v0

    move-wide/from16 v32, v0

    const-wide/high16 v34, 0x4090000000000000L    # 1024.0

    div-double v32, v32, v34

    invoke-static/range {v32 .. v33}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v31

    aput-object v31, v29, v30

    invoke-static/range {v27 .. v29}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/LogMergePolicy;->message(Ljava/lang/String;)V

    .line 575
    .end local v5    # "extra":Ljava/lang/String;
    .end local v20    # "segBytes":J
    :cond_4
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_0

    .line 589
    .restart local v20    # "segBytes":J
    :cond_5
    const-string v5, ""

    goto/16 :goto_3

    .line 601
    .end local v7    # "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .end local v8    # "infoLevel":Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;
    .end local v20    # "segBytes":J
    .end local v22    # "size":J
    :cond_6
    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/index/LogMergePolicy;->minMergeSize:J

    move-wide/from16 v26, v0

    move-wide/from16 v0, v26

    long-to-double v0, v0

    move-wide/from16 v26, v0

    invoke-static/range {v26 .. v27}, Ljava/lang/Math;->log(D)D

    move-result-wide v26

    move/from16 v0, v16

    float-to-double v0, v0

    move-wide/from16 v28, v0

    div-double v26, v26, v28

    move-wide/from16 v0, v26

    double-to-float v11, v0

    .restart local v11    # "levelFloor":F
    goto/16 :goto_1

    .line 619
    .restart local v17    # "numMergeableSegments":I
    .restart local v19    # "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    .restart local v24    # "start":I
    :cond_7
    move/from16 v0, v24

    invoke-interface {v12, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;

    move-object/from16 v0, v26

    iget v13, v0, Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;->level:F

    .line 620
    .local v13, "maxLevel":F
    add-int/lit8 v6, v24, 0x1

    :goto_4
    move/from16 v0, v17

    if-lt v6, v0, :cond_b

    .line 630
    cmpg-float v26, v13, v11

    if-gtz v26, :cond_d

    .line 632
    const/high16 v10, -0x40800000    # -1.0f

    .line 642
    .local v10, "levelBottom":F
    :cond_8
    :goto_5
    add-int/lit8 v25, v17, -0x1

    .line 643
    .local v25, "upto":I
    :goto_6
    move/from16 v0, v25

    move/from16 v1, v24

    if-ge v0, v1, :cond_e

    .line 649
    :cond_9
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/LogMergePolicy;->verbose()Z

    move-result v26

    if-eqz v26, :cond_a

    .line 650
    new-instance v26, Ljava/lang/StringBuilder;

    const-string v27, "  level "

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v26

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, " to "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, ": "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    add-int/lit8 v27, v25, 0x1

    sub-int v27, v27, v24

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, " segments"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/LogMergePolicy;->message(Ljava/lang/String;)V

    .line 654
    :cond_a
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/index/LogMergePolicy;->mergeFactor:I

    move/from16 v26, v0

    add-int v4, v24, v26

    .line 655
    .local v4, "end":I
    :goto_7
    add-int/lit8 v26, v25, 0x1

    move/from16 v0, v26

    if-le v4, v0, :cond_f

    .line 689
    add-int/lit8 v24, v25, 0x1

    goto/16 :goto_2

    .line 621
    .end local v4    # "end":I
    .end local v10    # "levelBottom":F
    .end local v25    # "upto":I
    :cond_b
    invoke-interface {v12, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;

    move-object/from16 v0, v26

    iget v9, v0, Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;->level:F

    .line 622
    .local v9, "level":F
    cmpl-float v26, v9, v13

    if-lez v26, :cond_c

    .line 623
    move v13, v9

    .line 620
    :cond_c
    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    .line 634
    .end local v9    # "level":F
    :cond_d
    float-to-double v0, v13

    move-wide/from16 v26, v0

    const-wide/high16 v28, 0x3fe8000000000000L    # 0.75

    sub-double v26, v26, v28

    move-wide/from16 v0, v26

    double-to-float v10, v0

    .line 637
    .restart local v10    # "levelBottom":F
    cmpg-float v26, v10, v11

    if-gez v26, :cond_8

    cmpl-float v26, v13, v11

    if-ltz v26, :cond_8

    .line 638
    move v10, v11

    goto :goto_5

    .line 644
    .restart local v25    # "upto":I
    :cond_e
    move/from16 v0, v25

    invoke-interface {v12, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;

    move-object/from16 v0, v26

    iget v0, v0, Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;->level:F

    move/from16 v26, v0

    cmpl-float v26, v26, v10

    if-gez v26, :cond_9

    .line 647
    add-int/lit8 v25, v25, -0x1

    goto/16 :goto_6

    .line 656
    .restart local v4    # "end":I
    :cond_f
    const/4 v3, 0x0

    .line 657
    .local v3, "anyTooLarge":Z
    const/4 v2, 0x0

    .line 658
    .local v2, "anyMerging":Z
    move/from16 v6, v24

    :goto_8
    if-lt v6, v4, :cond_13

    .line 667
    :goto_9
    if-nez v2, :cond_12

    .line 669
    if-nez v3, :cond_18

    .line 670
    if-nez v19, :cond_10

    .line 671
    new-instance v19, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;

    .end local v19    # "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    invoke-direct/range {v19 .. v19}, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;-><init>()V

    .line 672
    .restart local v19    # "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    :cond_10
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 673
    .local v14, "mergeInfos":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfoPerCommit;>;"
    move/from16 v6, v24

    :goto_a
    if-lt v6, v4, :cond_16

    .line 677
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/LogMergePolicy;->verbose()Z

    move-result v26

    if-eqz v26, :cond_11

    .line 678
    new-instance v27, Ljava/lang/StringBuilder;

    const-string v26, "  add merge="

    move-object/from16 v0, v27

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/LogMergePolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lorg/apache/lucene/index/IndexWriter;

    move-object/from16 v0, v26

    invoke-virtual {v0, v14}, Lorg/apache/lucene/index/IndexWriter;->segString(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v27

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, " start="

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, " end="

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/LogMergePolicy;->message(Ljava/lang/String;)V

    .line 680
    :cond_11
    new-instance v26, Lorg/apache/lucene/index/MergePolicy$OneMerge;

    move-object/from16 v0, v26

    invoke-direct {v0, v14}, Lorg/apache/lucene/index/MergePolicy$OneMerge;-><init>(Ljava/util/List;)V

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;->add(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    .line 685
    .end local v14    # "mergeInfos":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfoPerCommit;>;"
    :cond_12
    :goto_b
    move/from16 v24, v4

    .line 686
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/index/LogMergePolicy;->mergeFactor:I

    move/from16 v26, v0

    add-int v4, v24, v26

    goto/16 :goto_7

    .line 659
    :cond_13
    invoke-interface {v12, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;

    move-object/from16 v0, v26

    iget-object v7, v0, Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .line 660
    .restart local v7    # "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lorg/apache/lucene/index/LogMergePolicy;->size(Lorg/apache/lucene/index/SegmentInfoPerCommit;)J

    move-result-wide v26

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/index/LogMergePolicy;->maxMergeSize:J

    move-wide/from16 v28, v0

    cmp-long v26, v26, v28

    if-gez v26, :cond_14

    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lorg/apache/lucene/index/LogMergePolicy;->sizeDocs(Lorg/apache/lucene/index/SegmentInfoPerCommit;)J

    move-result-wide v26

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/index/LogMergePolicy;->maxMergeDocs:I

    move/from16 v28, v0

    move/from16 v0, v28

    int-to-long v0, v0

    move-wide/from16 v28, v0

    cmp-long v26, v26, v28

    if-gez v26, :cond_14

    const/16 v26, 0x0

    :goto_c
    or-int v3, v3, v26

    .line 661
    invoke-interface {v15, v7}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_15

    .line 662
    const/4 v2, 0x1

    .line 663
    goto/16 :goto_9

    .line 660
    :cond_14
    const/16 v26, 0x1

    goto :goto_c

    .line 658
    :cond_15
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_8

    .line 674
    .end local v7    # "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .restart local v14    # "mergeInfos":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfoPerCommit;>;"
    :cond_16
    invoke-interface {v12, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;

    move-object/from16 v0, v26

    iget-object v0, v0, Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-interface {v14, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 675
    sget-boolean v26, Lorg/apache/lucene/index/LogMergePolicy;->$assertionsDisabled:Z

    if-nez v26, :cond_17

    invoke-interface {v12, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;

    move-object/from16 v0, v26

    iget-object v0, v0, Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    move-object/from16 v26, v0

    move-object/from16 v0, p2

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/SegmentInfos;->contains(Lorg/apache/lucene/index/SegmentInfoPerCommit;)Z

    move-result v26

    if-nez v26, :cond_17

    new-instance v26, Ljava/lang/AssertionError;

    invoke-direct/range {v26 .. v26}, Ljava/lang/AssertionError;-><init>()V

    throw v26

    .line 673
    :cond_17
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_a

    .line 681
    .end local v14    # "mergeInfos":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfoPerCommit;>;"
    :cond_18
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/LogMergePolicy;->verbose()Z

    move-result v26

    if-eqz v26, :cond_12

    .line 682
    new-instance v26, Ljava/lang/StringBuilder;

    const-string v27, "    "

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v26

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, " to "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, ": contains segment over maxMergeSize or maxMergeDocs; skipping"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/LogMergePolicy;->message(Ljava/lang/String;)V

    goto/16 :goto_b
.end method

.method public getCalibrateSizeByDeletes()Z
    .locals 1

    .prologue
    .line 223
    iget-boolean v0, p0, Lorg/apache/lucene/index/LogMergePolicy;->calibrateSizeByDeletes:Z

    return v0
.end method

.method public final getMaxCFSSegmentSizeMB()D
    .locals 4

    .prologue
    .line 739
    iget-wide v0, p0, Lorg/apache/lucene/index/LogMergePolicy;->maxCFSSegmentSize:J

    const-wide/16 v2, 0x400

    div-long/2addr v0, v2

    long-to-double v0, v0

    const-wide/high16 v2, 0x4090000000000000L    # 1024.0

    div-double/2addr v0, v2

    return-wide v0
.end method

.method public getMaxMergeDocs()I
    .locals 1

    .prologue
    .line 718
    iget v0, p0, Lorg/apache/lucene/index/LogMergePolicy;->maxMergeDocs:I

    return v0
.end method

.method public getMergeFactor()I
    .locals 1

    .prologue
    .line 163
    iget v0, p0, Lorg/apache/lucene/index/LogMergePolicy;->mergeFactor:I

    return v0
.end method

.method public getNoCFSRatio()D
    .locals 2

    .prologue
    .line 136
    iget-wide v0, p0, Lorg/apache/lucene/index/LogMergePolicy;->noCFSRatio:D

    return-wide v0
.end method

.method public getUseCompoundFile()Z
    .locals 1

    .prologue
    .line 211
    iget-boolean v0, p0, Lorg/apache/lucene/index/LogMergePolicy;->useCompoundFile:Z

    return v0
.end method

.method protected isMerged(Lorg/apache/lucene/index/SegmentInfoPerCommit;)Z
    .locals 8
    .param p1, "info"    # Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 290
    iget-object v4, p0, Lorg/apache/lucene/index/LogMergePolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    invoke-virtual {v4}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/IndexWriter;

    .line 291
    .local v1, "w":Lorg/apache/lucene/index/IndexWriter;
    sget-boolean v4, Lorg/apache/lucene/index/LogMergePolicy;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    if-nez v1, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 292
    :cond_0
    invoke-virtual {v1, p1}, Lorg/apache/lucene/index/IndexWriter;->numDeletedDocs(Lorg/apache/lucene/index/SegmentInfoPerCommit;)I

    move-result v4

    if-lez v4, :cond_2

    move v0, v2

    .line 293
    .local v0, "hasDeletions":Z
    :goto_0
    if-nez v0, :cond_3

    .line 294
    iget-object v4, p1, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v4}, Lorg/apache/lucene/index/SegmentInfo;->hasSeparateNorms()Z

    move-result v4

    if-nez v4, :cond_3

    .line 295
    iget-object v4, p1, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v4, v4, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v1}, Lorg/apache/lucene/index/IndexWriter;->getDirectory()Lorg/apache/lucene/store/Directory;

    move-result-object v5

    if-ne v4, v5, :cond_3

    .line 296
    iget-object v4, p1, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v4}, Lorg/apache/lucene/index/SegmentInfo;->getUseCompoundFile()Z

    move-result v4

    iget-boolean v5, p0, Lorg/apache/lucene/index/LogMergePolicy;->useCompoundFile:Z

    if-eq v4, v5, :cond_1

    iget-wide v4, p0, Lorg/apache/lucene/index/LogMergePolicy;->noCFSRatio:D

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    .line 293
    cmpg-double v4, v4, v6

    if-gez v4, :cond_3

    :cond_1
    :goto_1
    return v2

    .end local v0    # "hasDeletions":Z
    :cond_2
    move v0, v3

    .line 292
    goto :goto_0

    .restart local v0    # "hasDeletions":Z
    :cond_3
    move v2, v3

    .line 293
    goto :goto_1
.end method

.method protected isMerged(Lorg/apache/lucene/index/SegmentInfos;ILjava/util/Map;)Z
    .locals 9
    .param p1, "infos"    # Lorg/apache/lucene/index/SegmentInfos;
    .param p2, "maxNumSegments"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/SegmentInfos;",
            "I",
            "Ljava/util/Map",
            "<",
            "Lorg/apache/lucene/index/SegmentInfoPerCommit;",
            "Ljava/lang/Boolean;",
            ">;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p3, "segmentsToMerge":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/index/SegmentInfoPerCommit;Ljava/lang/Boolean;>;"
    const/4 v7, 0x1

    .line 267
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfos;->size()I

    move-result v4

    .line 268
    .local v4, "numSegments":I
    const/4 v5, 0x0

    .line 269
    .local v5, "numToMerge":I
    const/4 v3, 0x0

    .line 270
    .local v3, "mergeInfo":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    const/4 v6, 0x0

    .line 271
    .local v6, "segmentIsOriginal":Z
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v4, :cond_0

    if-le v5, p2, :cond_2

    .line 281
    :cond_0
    if-gt v5, p2, :cond_4

    .line 282
    if-ne v5, v7, :cond_1

    if-eqz v6, :cond_1

    invoke-virtual {p0, v3}, Lorg/apache/lucene/index/LogMergePolicy;->isMerged(Lorg/apache/lucene/index/SegmentInfoPerCommit;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 281
    :cond_1
    :goto_1
    return v7

    .line 272
    :cond_2
    invoke-virtual {p1, v0}, Lorg/apache/lucene/index/SegmentInfos;->info(I)Lorg/apache/lucene/index/SegmentInfoPerCommit;

    move-result-object v1

    .line 273
    .local v1, "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    invoke-interface {p3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    .line 274
    .local v2, "isOriginal":Ljava/lang/Boolean;
    if-eqz v2, :cond_3

    .line 275
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    .line 276
    add-int/lit8 v5, v5, 0x1

    .line 277
    move-object v3, v1

    .line 271
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 281
    .end local v1    # "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .end local v2    # "isOriginal":Ljava/lang/Boolean;
    :cond_4
    const/4 v7, 0x0

    goto :goto_1
.end method

.method protected message(Ljava/lang/String;)V
    .locals 2
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 154
    invoke-virtual {p0}, Lorg/apache/lucene/index/LogMergePolicy;->verbose()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lorg/apache/lucene/index/LogMergePolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    invoke-virtual {v0}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/IndexWriter;

    iget-object v0, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v1, "LMP"

    invoke-virtual {v0, v1, p1}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    :cond_0
    return-void
.end method

.method public setCalibrateSizeByDeletes(Z)V
    .locals 0
    .param p1, "calibrateSizeByDeletes"    # Z

    .prologue
    .line 217
    iput-boolean p1, p0, Lorg/apache/lucene/index/LogMergePolicy;->calibrateSizeByDeletes:Z

    .line 218
    return-void
.end method

.method public final setMaxCFSSegmentSizeMB(D)V
    .locals 3
    .param p1, "v"    # D

    .prologue
    .line 748
    const-wide/16 v0, 0x0

    cmpg-double v0, p1, v0

    if-gez v0, :cond_0

    .line 749
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "maxCFSSegmentSizeMB must be >=0 (got "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 751
    :cond_0
    const-wide/high16 v0, 0x4130000000000000L    # 1048576.0

    mul-double/2addr p1, v0

    .line 752
    const-wide/high16 v0, 0x43e0000000000000L    # 9.223372036854776E18

    cmpl-double v0, p1, v0

    if-lez v0, :cond_1

    const-wide v0, 0x7fffffffffffffffL

    :goto_0
    iput-wide v0, p0, Lorg/apache/lucene/index/LogMergePolicy;->maxCFSSegmentSize:J

    .line 753
    return-void

    .line 752
    :cond_1
    double-to-long v0, p1

    goto :goto_0
.end method

.method public setMaxMergeDocs(I)V
    .locals 0
    .param p1, "maxMergeDocs"    # I

    .prologue
    .line 711
    iput p1, p0, Lorg/apache/lucene/index/LogMergePolicy;->maxMergeDocs:I

    .line 712
    return-void
.end method

.method public setMergeFactor(I)V
    .locals 2
    .param p1, "mergeFactor"    # I

    .prologue
    .line 176
    const/4 v0, 0x2

    if-ge p1, v0, :cond_0

    .line 177
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "mergeFactor cannot be less than 2"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 178
    :cond_0
    iput p1, p0, Lorg/apache/lucene/index/LogMergePolicy;->mergeFactor:I

    .line 179
    return-void
.end method

.method public setNoCFSRatio(D)V
    .locals 3
    .param p1, "noCFSRatio"    # D

    .prologue
    .line 145
    const-wide/16 v0, 0x0

    cmpg-double v0, p1, v0

    if-ltz v0, :cond_0

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    cmpl-double v0, p1, v0

    if-lez v0, :cond_1

    .line 146
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "noCFSRatio must be 0.0 to 1.0 inclusive; got "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 148
    :cond_1
    iput-wide p1, p0, Lorg/apache/lucene/index/LogMergePolicy;->noCFSRatio:D

    .line 149
    return-void
.end method

.method public setUseCompoundFile(Z)V
    .locals 0
    .param p1, "useCompoundFile"    # Z

    .prologue
    .line 204
    iput-boolean p1, p0, Lorg/apache/lucene/index/LogMergePolicy;->useCompoundFile:Z

    .line 205
    return-void
.end method

.method protected abstract size(Lorg/apache/lucene/index/SegmentInfoPerCommit;)J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method protected sizeBytes(Lorg/apache/lucene/index/SegmentInfoPerCommit;)J
    .locals 10
    .param p1, "info"    # Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    .line 252
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->sizeInBytes()J

    move-result-wide v0

    .line 253
    .local v0, "byteSize":J
    iget-boolean v3, p0, Lorg/apache/lucene/index/LogMergePolicy;->calibrateSizeByDeletes:Z

    if-eqz v3, :cond_2

    .line 254
    iget-object v3, p0, Lorg/apache/lucene/index/LogMergePolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    invoke-virtual {v3}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v3, p1}, Lorg/apache/lucene/index/IndexWriter;->numDeletedDocs(Lorg/apache/lucene/index/SegmentInfoPerCommit;)I

    move-result v2

    .line 255
    .local v2, "delCount":I
    iget-object v3, p1, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v3}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v3

    if-gtz v3, :cond_0

    const/4 v3, 0x0

    :goto_0
    float-to-double v4, v3

    .line 256
    .local v4, "delRatio":D
    sget-boolean v3, Lorg/apache/lucene/index/LogMergePolicy;->$assertionsDisabled:Z

    if-nez v3, :cond_1

    cmpg-double v3, v4, v8

    if-lez v3, :cond_1

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 255
    .end local v4    # "delRatio":D
    :cond_0
    int-to-float v3, v2

    iget-object v6, p1, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v6}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v3, v6

    goto :goto_0

    .line 257
    .restart local v4    # "delRatio":D
    :cond_1
    iget-object v3, p1, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v3}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v3

    if-gtz v3, :cond_3

    .line 259
    .end local v0    # "byteSize":J
    .end local v2    # "delCount":I
    .end local v4    # "delRatio":D
    :cond_2
    :goto_1
    return-wide v0

    .line 257
    .restart local v0    # "byteSize":J
    .restart local v2    # "delCount":I
    .restart local v4    # "delRatio":D
    :cond_3
    long-to-double v6, v0

    sub-double/2addr v8, v4

    mul-double/2addr v6, v8

    double-to-long v0, v6

    goto :goto_1
.end method

.method protected sizeDocs(Lorg/apache/lucene/index/SegmentInfoPerCommit;)J
    .locals 6
    .param p1, "info"    # Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 238
    iget-boolean v1, p0, Lorg/apache/lucene/index/LogMergePolicy;->calibrateSizeByDeletes:Z

    if-eqz v1, :cond_1

    .line 239
    iget-object v1, p0, Lorg/apache/lucene/index/LogMergePolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    invoke-virtual {v1}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/index/IndexWriter;->numDeletedDocs(Lorg/apache/lucene/index/SegmentInfoPerCommit;)I

    move-result v0

    .line 240
    .local v0, "delCount":I
    sget-boolean v1, Lorg/apache/lucene/index/LogMergePolicy;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-object v1, p1, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v1

    if-le v0, v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 241
    :cond_0
    iget-object v1, p1, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v1

    int-to-long v2, v1

    int-to-long v4, v0

    sub-long/2addr v2, v4

    .line 243
    .end local v0    # "delCount":I
    :goto_0
    return-wide v2

    :cond_1
    iget-object v1, p1, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v1

    int-to-long v2, v1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 723
    new-instance v0, Ljava/lang/StringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 724
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v1, "minMergeSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lorg/apache/lucene/index/LogMergePolicy;->minMergeSize:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 725
    const-string v1, "mergeFactor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/index/LogMergePolicy;->mergeFactor:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 726
    const-string v1, "maxMergeSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lorg/apache/lucene/index/LogMergePolicy;->maxMergeSize:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 727
    const-string v1, "maxMergeSizeForForcedMerge="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lorg/apache/lucene/index/LogMergePolicy;->maxMergeSizeForForcedMerge:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 728
    const-string v1, "calibrateSizeByDeletes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lorg/apache/lucene/index/LogMergePolicy;->calibrateSizeByDeletes:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 729
    const-string v1, "maxMergeDocs="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/index/LogMergePolicy;->maxMergeDocs:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 730
    const-string v1, "useCompoundFile="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lorg/apache/lucene/index/LogMergePolicy;->useCompoundFile:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 731
    const-string v1, "maxCFSSegmentSizeMB="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/lucene/index/LogMergePolicy;->getMaxCFSSegmentSizeMB()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 732
    const-string v1, "noCFSRatio="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lorg/apache/lucene/index/LogMergePolicy;->noCFSRatio:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 733
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 734
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public useCompoundFile(Lorg/apache/lucene/index/SegmentInfos;Lorg/apache/lucene/index/SegmentInfoPerCommit;)Z
    .locals 12
    .param p1, "infos"    # Lorg/apache/lucene/index/SegmentInfos;
    .param p2, "mergedInfo"    # Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 184
    invoke-virtual {p0}, Lorg/apache/lucene/index/LogMergePolicy;->getUseCompoundFile()Z

    move-result v1

    if-nez v1, :cond_0

    .line 185
    const/4 v1, 0x0

    .line 198
    :goto_0
    return v1

    .line 187
    :cond_0
    invoke-virtual {p0, p2}, Lorg/apache/lucene/index/LogMergePolicy;->size(Lorg/apache/lucene/index/SegmentInfoPerCommit;)J

    move-result-wide v2

    .line 188
    .local v2, "mergedInfoSize":J
    iget-wide v6, p0, Lorg/apache/lucene/index/LogMergePolicy;->maxCFSSegmentSize:J

    cmp-long v1, v2, v6

    if-lez v1, :cond_1

    .line 189
    const/4 v1, 0x0

    goto :goto_0

    .line 191
    :cond_1
    invoke-virtual {p0}, Lorg/apache/lucene/index/LogMergePolicy;->getNoCFSRatio()D

    move-result-wide v6

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    cmpl-double v1, v6, v8

    if-ltz v1, :cond_2

    .line 192
    const/4 v1, 0x1

    goto :goto_0

    .line 194
    :cond_2
    const-wide/16 v4, 0x0

    .line 195
    .local v4, "totalSize":J
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfos;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_3

    .line 198
    long-to-double v6, v2

    invoke-virtual {p0}, Lorg/apache/lucene/index/LogMergePolicy;->getNoCFSRatio()D

    move-result-wide v8

    long-to-double v10, v4

    mul-double/2addr v8, v10

    cmpg-double v1, v6, v8

    if-gtz v1, :cond_4

    const/4 v1, 0x1

    goto :goto_0

    .line 195
    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .line 196
    .local v0, "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/LogMergePolicy;->size(Lorg/apache/lucene/index/SegmentInfoPerCommit;)J

    move-result-wide v6

    add-long/2addr v4, v6

    goto :goto_1

    .line 198
    .end local v0    # "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    :cond_4
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected verbose()Z
    .locals 3

    .prologue
    .line 128
    iget-object v1, p0, Lorg/apache/lucene/index/LogMergePolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    invoke-virtual {v1}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/IndexWriter;

    .line 129
    .local v0, "w":Lorg/apache/lucene/index/IndexWriter;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v2, "LMP"

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
