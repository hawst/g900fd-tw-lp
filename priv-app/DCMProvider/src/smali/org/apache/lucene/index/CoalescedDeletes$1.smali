.class Lorg/apache/lucene/index/CoalescedDeletes$1;
.super Ljava/lang/Object;
.source "CoalescedDeletes.java"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/index/CoalescedDeletes;->termsIterable()Ljava/lang/Iterable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable",
        "<",
        "Lorg/apache/lucene/index/Term;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/index/CoalescedDeletes;


# direct methods
.method constructor <init>(Lorg/apache/lucene/index/CoalescedDeletes;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/index/CoalescedDeletes$1;->this$0:Lorg/apache/lucene/index/CoalescedDeletes;

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public iterator()Ljava/util/Iterator;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lorg/apache/lucene/index/Term;",
            ">;"
        }
    .end annotation

    .prologue
    .line 53
    iget-object v2, p0, Lorg/apache/lucene/index/CoalescedDeletes$1;->this$0:Lorg/apache/lucene/index/CoalescedDeletes;

    iget-object v2, v2, Lorg/apache/lucene/index/CoalescedDeletes;->iterables:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    new-array v1, v2, [Ljava/util/Iterator;

    .line 54
    .local v1, "subs":[Ljava/util/Iterator;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/index/CoalescedDeletes$1;->this$0:Lorg/apache/lucene/index/CoalescedDeletes;

    iget-object v2, v2, Lorg/apache/lucene/index/CoalescedDeletes;->iterables:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lt v0, v2, :cond_0

    .line 57
    new-instance v2, Lorg/apache/lucene/index/MergedIterator;

    invoke-direct {v2, v1}, Lorg/apache/lucene/index/MergedIterator;-><init>([Ljava/util/Iterator;)V

    return-object v2

    .line 55
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/index/CoalescedDeletes$1;->this$0:Lorg/apache/lucene/index/CoalescedDeletes;

    iget-object v2, v2, Lorg/apache/lucene/index/CoalescedDeletes;->iterables:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    aput-object v2, v1, v0

    .line 54
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
