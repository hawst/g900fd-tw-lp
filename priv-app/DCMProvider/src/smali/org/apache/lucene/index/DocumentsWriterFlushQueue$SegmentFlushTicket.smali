.class final Lorg/apache/lucene/index/DocumentsWriterFlushQueue$SegmentFlushTicket;
.super Lorg/apache/lucene/index/DocumentsWriterFlushQueue$FlushTicket;
.source "DocumentsWriterFlushQueue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/DocumentsWriterFlushQueue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "SegmentFlushTicket"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private failed:Z

.field private segment:Lorg/apache/lucene/index/DocumentsWriterPerThread$FlushedSegment;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 195
    const-class v0, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/DocumentsWriterFlushQueue$SegmentFlushTicket;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected constructor <init>(Lorg/apache/lucene/index/FrozenBufferedDeletes;)V
    .locals 1
    .param p1, "frozenDeletes"    # Lorg/apache/lucene/index/FrozenBufferedDeletes;

    .prologue
    .line 200
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/DocumentsWriterFlushQueue$FlushTicket;-><init>(Lorg/apache/lucene/index/FrozenBufferedDeletes;)V

    .line 197
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushQueue$SegmentFlushTicket;->failed:Z

    .line 201
    return-void
.end method


# virtual methods
.method protected canPublish()Z
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushQueue$SegmentFlushTicket;->segment:Lorg/apache/lucene/index/DocumentsWriterPerThread$FlushedSegment;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushQueue$SegmentFlushTicket;->failed:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected publish(Lorg/apache/lucene/index/DocumentsWriter;)V
    .locals 2
    .param p1, "writer"    # Lorg/apache/lucene/index/DocumentsWriter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 205
    sget-boolean v0, Lorg/apache/lucene/index/DocumentsWriterFlushQueue$SegmentFlushTicket;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushQueue$SegmentFlushTicket;->published:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "ticket was already publised - can not publish twice"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 206
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushQueue$SegmentFlushTicket;->published:Z

    .line 207
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushQueue$SegmentFlushTicket;->segment:Lorg/apache/lucene/index/DocumentsWriterPerThread$FlushedSegment;

    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterFlushQueue$SegmentFlushTicket;->frozenDeletes:Lorg/apache/lucene/index/FrozenBufferedDeletes;

    invoke-virtual {p1, v0, v1}, Lorg/apache/lucene/index/DocumentsWriter;->finishFlush(Lorg/apache/lucene/index/DocumentsWriterPerThread$FlushedSegment;Lorg/apache/lucene/index/FrozenBufferedDeletes;)V

    .line 208
    return-void
.end method

.method protected setFailed()V
    .locals 1

    .prologue
    .line 216
    sget-boolean v0, Lorg/apache/lucene/index/DocumentsWriterFlushQueue$SegmentFlushTicket;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushQueue$SegmentFlushTicket;->segment:Lorg/apache/lucene/index/DocumentsWriterPerThread$FlushedSegment;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 217
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushQueue$SegmentFlushTicket;->failed:Z

    .line 218
    return-void
.end method

.method protected setSegment(Lorg/apache/lucene/index/DocumentsWriterPerThread$FlushedSegment;)V
    .locals 1
    .param p1, "segment"    # Lorg/apache/lucene/index/DocumentsWriterPerThread$FlushedSegment;

    .prologue
    .line 211
    sget-boolean v0, Lorg/apache/lucene/index/DocumentsWriterFlushQueue$SegmentFlushTicket;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushQueue$SegmentFlushTicket;->failed:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 212
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/index/DocumentsWriterFlushQueue$SegmentFlushTicket;->segment:Lorg/apache/lucene/index/DocumentsWriterPerThread$FlushedSegment;

    .line 213
    return-void
.end method
