.class public abstract Lorg/apache/lucene/index/IndexReaderContext;
.super Ljava/lang/Object;
.source "IndexReaderContext.java"


# instance fields
.field public final docBaseInParent:I

.field public final isTopLevel:Z

.field public final ordInParent:I

.field public final parent:Lorg/apache/lucene/index/CompositeReaderContext;


# direct methods
.method constructor <init>(Lorg/apache/lucene/index/CompositeReaderContext;II)V
    .locals 2
    .param p1, "parent"    # Lorg/apache/lucene/index/CompositeReaderContext;
    .param p2, "ordInParent"    # I
    .param p3, "docBaseInParent"    # I

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    instance-of v0, p0, Lorg/apache/lucene/index/CompositeReaderContext;

    if-nez v0, :cond_0

    instance-of v0, p0, Lorg/apache/lucene/index/AtomicReaderContext;

    if-nez v0, :cond_0

    .line 38
    new-instance v0, Ljava/lang/Error;

    const-string v1, "This class should never be extended by custom code!"

    invoke-direct {v0, v1}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    throw v0

    .line 39
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/index/IndexReaderContext;->parent:Lorg/apache/lucene/index/CompositeReaderContext;

    .line 40
    iput p3, p0, Lorg/apache/lucene/index/IndexReaderContext;->docBaseInParent:I

    .line 41
    iput p2, p0, Lorg/apache/lucene/index/IndexReaderContext;->ordInParent:I

    .line 42
    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lorg/apache/lucene/index/IndexReaderContext;->isTopLevel:Z

    .line 43
    return-void

    .line 42
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public abstract children()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/IndexReaderContext;",
            ">;"
        }
    .end annotation
.end method

.method public abstract leaves()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/AtomicReaderContext;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation
.end method

.method public abstract reader()Lorg/apache/lucene/index/IndexReader;
.end method
