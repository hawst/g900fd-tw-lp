.class Lorg/apache/lucene/index/TieredMergePolicy$1;
.super Lorg/apache/lucene/index/TieredMergePolicy$MergeScore;
.source "TieredMergePolicy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/index/TieredMergePolicy;->score(Ljava/util/List;ZJ)Lorg/apache/lucene/index/TieredMergePolicy$MergeScore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/index/TieredMergePolicy;

.field private final synthetic val$finalMergeScore:D

.field private final synthetic val$nonDelRatio:D

.field private final synthetic val$skew:D


# direct methods
.method constructor <init>(Lorg/apache/lucene/index/TieredMergePolicy;DDD)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/index/TieredMergePolicy$1;->this$0:Lorg/apache/lucene/index/TieredMergePolicy;

    iput-wide p2, p0, Lorg/apache/lucene/index/TieredMergePolicy$1;->val$finalMergeScore:D

    iput-wide p4, p0, Lorg/apache/lucene/index/TieredMergePolicy$1;->val$skew:D

    iput-wide p6, p0, Lorg/apache/lucene/index/TieredMergePolicy$1;->val$nonDelRatio:D

    .line 506
    invoke-direct {p0}, Lorg/apache/lucene/index/TieredMergePolicy$MergeScore;-><init>()V

    return-void
.end method


# virtual methods
.method public getExplanation()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 515
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "skew="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    const-string v2, "%.3f"

    new-array v3, v7, [Ljava/lang/Object;

    iget-wide v4, p0, Lorg/apache/lucene/index/TieredMergePolicy$1;->val$skew:D

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " nonDelRatio="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    const-string v2, "%.3f"

    new-array v3, v7, [Ljava/lang/Object;

    iget-wide v4, p0, Lorg/apache/lucene/index/TieredMergePolicy$1;->val$nonDelRatio:D

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getScore()D
    .locals 2

    .prologue
    .line 510
    iget-wide v0, p0, Lorg/apache/lucene/index/TieredMergePolicy$1;->val$finalMergeScore:D

    return-wide v0
.end method
