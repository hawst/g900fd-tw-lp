.class public abstract Lorg/apache/lucene/index/CompositeReader;
.super Lorg/apache/lucene/index/IndexReader;
.source "CompositeReader.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private volatile readerContext:Lorg/apache/lucene/index/CompositeReaderContext;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    const-class v0, Lorg/apache/lucene/index/CompositeReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/CompositeReader;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexReader;-><init>()V

    .line 58
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/index/CompositeReader;->readerContext:Lorg/apache/lucene/index/CompositeReaderContext;

    .line 64
    return-void
.end method


# virtual methods
.method public final getContext()Lorg/apache/lucene/index/CompositeReaderContext;
    .locals 1

    .prologue
    .line 96
    invoke-virtual {p0}, Lorg/apache/lucene/index/CompositeReader;->ensureOpen()V

    .line 98
    iget-object v0, p0, Lorg/apache/lucene/index/CompositeReader;->readerContext:Lorg/apache/lucene/index/CompositeReaderContext;

    if-nez v0, :cond_1

    .line 99
    sget-boolean v0, Lorg/apache/lucene/index/CompositeReader;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/index/CompositeReader;->getSequentialSubReaders()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 100
    :cond_0
    invoke-static {p0}, Lorg/apache/lucene/index/CompositeReaderContext;->create(Lorg/apache/lucene/index/CompositeReader;)Lorg/apache/lucene/index/CompositeReaderContext;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/CompositeReader;->readerContext:Lorg/apache/lucene/index/CompositeReaderContext;

    .line 102
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/index/CompositeReader;->readerContext:Lorg/apache/lucene/index/CompositeReaderContext;

    return-object v0
.end method

.method public bridge synthetic getContext()Lorg/apache/lucene/index/IndexReaderContext;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/index/CompositeReader;->getContext()Lorg/apache/lucene/index/CompositeReaderContext;

    move-result-object v0

    return-object v0
.end method

.method protected abstract getSequentialSubReaders()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lorg/apache/lucene/index/IndexReader;",
            ">;"
        }
    .end annotation
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 68
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 69
    .local v0, "buffer":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 70
    const/16 v4, 0x28

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 71
    invoke-virtual {p0}, Lorg/apache/lucene/index/CompositeReader;->getSequentialSubReaders()Ljava/util/List;

    move-result-object v3

    .line 72
    .local v3, "subReaders":Ljava/util/List;, "Ljava/util/List<+Lorg/apache/lucene/index/IndexReader;>;"
    sget-boolean v4, Lorg/apache/lucene/index/CompositeReader;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    if-nez v3, :cond_0

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 73
    :cond_0
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1

    .line 74
    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 75
    const/4 v2, 0x1

    .local v2, "i":I
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    .local v1, "c":I
    :goto_0
    if-lt v2, v1, :cond_2

    .line 79
    .end local v1    # "c":I
    .end local v2    # "i":I
    :cond_1
    const/16 v4, 0x29

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 80
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4

    .line 76
    .restart local v1    # "c":I
    .restart local v2    # "i":I
    :cond_2
    const-string v4, " "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 75
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method
