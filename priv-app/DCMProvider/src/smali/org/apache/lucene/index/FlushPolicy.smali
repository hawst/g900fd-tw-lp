.class abstract Lorg/apache/lucene/index/FlushPolicy;
.super Ljava/lang/Object;
.source "FlushPolicy.java"

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field protected indexWriterConfig:Lorg/apache/lucene/index/LiveIndexWriterConfig;

.field protected writer:Lorg/apache/lucene/util/SetOnce;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/SetOnce",
            "<",
            "Lorg/apache/lucene/index/DocumentsWriter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const-class v0, Lorg/apache/lucene/index/FlushPolicy;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/FlushPolicy;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-instance v0, Lorg/apache/lucene/util/SetOnce;

    invoke-direct {v0}, Lorg/apache/lucene/util/SetOnce;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/FlushPolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    .line 53
    return-void
.end method

.method private assertMessage(Ljava/lang/String;)Z
    .locals 2
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 129
    iget-object v0, p0, Lorg/apache/lucene/index/FlushPolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    invoke-virtual {v0}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/DocumentsWriter;

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v1, "FP"

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 130
    iget-object v0, p0, Lorg/apache/lucene/index/FlushPolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    invoke-virtual {v0}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/DocumentsWriter;

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v1, "FP"

    invoke-virtual {v0, v1, p1}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    :cond_0
    const/4 v0, 0x1

    return v0
.end method


# virtual methods
.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/index/FlushPolicy;->clone()Lorg/apache/lucene/index/FlushPolicy;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lorg/apache/lucene/index/FlushPolicy;
    .locals 3

    .prologue
    .line 139
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/FlushPolicy;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 144
    .local v0, "clone":Lorg/apache/lucene/index/FlushPolicy;
    new-instance v2, Lorg/apache/lucene/util/SetOnce;

    invoke-direct {v2}, Lorg/apache/lucene/util/SetOnce;-><init>()V

    iput-object v2, v0, Lorg/apache/lucene/index/FlushPolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    .line 145
    const/4 v2, 0x0

    iput-object v2, v0, Lorg/apache/lucene/index/FlushPolicy;->indexWriterConfig:Lorg/apache/lucene/index/LiveIndexWriterConfig;

    .line 146
    return-object v0

    .line 140
    .end local v0    # "clone":Lorg/apache/lucene/index/FlushPolicy;
    :catch_0
    move-exception v1

    .line 142
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method protected findLargestNonPendingWriter(Lorg/apache/lucene/index/DocumentsWriterFlushControl;Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;)Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;
    .locals 9
    .param p1, "control"    # Lorg/apache/lucene/index/DocumentsWriterFlushControl;
    .param p2, "perThreadState"    # Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;

    .prologue
    .line 108
    sget-boolean v5, Lorg/apache/lucene/index/FlushPolicy;->$assertionsDisabled:Z

    if-nez v5, :cond_0

    iget-object v5, p2, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->dwpt:Lorg/apache/lucene/index/DocumentsWriterPerThread;

    invoke-virtual {v5}, Lorg/apache/lucene/index/DocumentsWriterPerThread;->getNumDocsInRAM()I

    move-result v5

    if-gtz v5, :cond_0

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 109
    :cond_0
    iget-wide v2, p2, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->bytesUsed:J

    .line 111
    .local v2, "maxRamSoFar":J
    move-object v1, p2

    .line 112
    .local v1, "maxRamUsingThreadState":Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;
    sget-boolean v5, Lorg/apache/lucene/index/FlushPolicy;->$assertionsDisabled:Z

    if-nez v5, :cond_1

    iget-boolean v5, p2, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->flushPending:Z

    if-eqz v5, :cond_1

    new-instance v5, Ljava/lang/AssertionError;

    const-string v8, "DWPT should have flushed"

    invoke-direct {v5, v8}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v5

    .line 113
    :cond_1
    invoke-virtual {p1}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->allActiveThreadStates()Ljava/util/Iterator;

    move-result-object v0

    .line 114
    .local v0, "activePerThreadsIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;>;"
    :cond_2
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_3

    .line 124
    sget-boolean v5, Lorg/apache/lucene/index/FlushPolicy;->$assertionsDisabled:Z

    if-nez v5, :cond_4

    const-string v5, "set largest ram consuming thread pending on lower watermark"

    invoke-direct {p0, v5}, Lorg/apache/lucene/index/FlushPolicy;->assertMessage(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_4

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 115
    :cond_3
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;

    .line 116
    .local v4, "next":Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;
    iget-boolean v5, v4, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->flushPending:Z

    if-nez v5, :cond_2

    .line 117
    iget-wide v6, v4, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->bytesUsed:J

    .line 118
    .local v6, "nextRam":J
    cmp-long v5, v6, v2

    if-lez v5, :cond_2

    iget-object v5, v4, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->dwpt:Lorg/apache/lucene/index/DocumentsWriterPerThread;

    invoke-virtual {v5}, Lorg/apache/lucene/index/DocumentsWriterPerThread;->getNumDocsInRAM()I

    move-result v5

    if-lez v5, :cond_2

    .line 119
    move-wide v2, v6

    .line 120
    move-object v1, v4

    goto :goto_0

    .line 125
    .end local v4    # "next":Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;
    .end local v6    # "nextRam":J
    :cond_4
    return-object v1
.end method

.method protected declared-synchronized init(Lorg/apache/lucene/index/DocumentsWriter;)V
    .locals 1
    .param p1, "docsWriter"    # Lorg/apache/lucene/index/DocumentsWriter;

    .prologue
    .line 96
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/FlushPolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/util/SetOnce;->set(Ljava/lang/Object;)V

    .line 97
    iget-object v0, p1, Lorg/apache/lucene/index/DocumentsWriter;->indexWriter:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexWriter;->getConfig()Lorg/apache/lucene/index/LiveIndexWriterConfig;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/FlushPolicy;->indexWriterConfig:Lorg/apache/lucene/index/LiveIndexWriterConfig;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 98
    monitor-exit p0

    return-void

    .line 96
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public abstract onDelete(Lorg/apache/lucene/index/DocumentsWriterFlushControl;Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;)V
.end method

.method public abstract onInsert(Lorg/apache/lucene/index/DocumentsWriterFlushControl;Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;)V
.end method

.method public onUpdate(Lorg/apache/lucene/index/DocumentsWriterFlushControl;Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;)V
    .locals 0
    .param p1, "control"    # Lorg/apache/lucene/index/DocumentsWriterFlushControl;
    .param p2, "state"    # Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;

    .prologue
    .line 77
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/index/FlushPolicy;->onInsert(Lorg/apache/lucene/index/DocumentsWriterFlushControl;Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;)V

    .line 78
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/index/FlushPolicy;->onDelete(Lorg/apache/lucene/index/DocumentsWriterFlushControl;Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;)V

    .line 79
    return-void
.end method
