.class Lorg/apache/lucene/index/SortedDocValuesWriter$ValuesIterator;
.super Ljava/lang/Object;
.source "SortedDocValuesWriter.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/SortedDocValuesWriter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ValuesIterator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Lorg/apache/lucene/util/BytesRef;",
        ">;"
    }
.end annotation


# instance fields
.field ordUpto:I

.field final scratch:Lorg/apache/lucene/util/BytesRef;

.field final sortedValues:[I

.field final synthetic this$0:Lorg/apache/lucene/index/SortedDocValuesWriter;

.field final valueCount:I


# direct methods
.method constructor <init>(Lorg/apache/lucene/index/SortedDocValuesWriter;[II)V
    .locals 1
    .param p2, "sortedValues"    # [I
    .param p3, "valueCount"    # I

    .prologue
    .line 151
    iput-object p1, p0, Lorg/apache/lucene/index/SortedDocValuesWriter$ValuesIterator;->this$0:Lorg/apache/lucene/index/SortedDocValuesWriter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 147
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/SortedDocValuesWriter$ValuesIterator;->scratch:Lorg/apache/lucene/util/BytesRef;

    .line 152
    iput-object p2, p0, Lorg/apache/lucene/index/SortedDocValuesWriter$ValuesIterator;->sortedValues:[I

    .line 153
    iput p3, p0, Lorg/apache/lucene/index/SortedDocValuesWriter$ValuesIterator;->valueCount:I

    .line 154
    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 2

    .prologue
    .line 158
    iget v0, p0, Lorg/apache/lucene/index/SortedDocValuesWriter$ValuesIterator;->ordUpto:I

    iget v1, p0, Lorg/apache/lucene/index/SortedDocValuesWriter$ValuesIterator;->valueCount:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/index/SortedDocValuesWriter$ValuesIterator;->next()Lorg/apache/lucene/util/BytesRef;

    move-result-object v0

    return-object v0
.end method

.method public next()Lorg/apache/lucene/util/BytesRef;
    .locals 3

    .prologue
    .line 163
    invoke-virtual {p0}, Lorg/apache/lucene/index/SortedDocValuesWriter$ValuesIterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 164
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 166
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/SortedDocValuesWriter$ValuesIterator;->this$0:Lorg/apache/lucene/index/SortedDocValuesWriter;

    iget-object v0, v0, Lorg/apache/lucene/index/SortedDocValuesWriter;->hash:Lorg/apache/lucene/util/BytesRefHash;

    iget-object v1, p0, Lorg/apache/lucene/index/SortedDocValuesWriter$ValuesIterator;->sortedValues:[I

    iget v2, p0, Lorg/apache/lucene/index/SortedDocValuesWriter$ValuesIterator;->ordUpto:I

    aget v1, v1, v2

    iget-object v2, p0, Lorg/apache/lucene/index/SortedDocValuesWriter$ValuesIterator;->scratch:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/BytesRefHash;->get(ILorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/BytesRef;

    .line 167
    iget v0, p0, Lorg/apache/lucene/index/SortedDocValuesWriter$ValuesIterator;->ordUpto:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/index/SortedDocValuesWriter$ValuesIterator;->ordUpto:I

    .line 168
    iget-object v0, p0, Lorg/apache/lucene/index/SortedDocValuesWriter$ValuesIterator;->scratch:Lorg/apache/lucene/util/BytesRef;

    return-object v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 173
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
