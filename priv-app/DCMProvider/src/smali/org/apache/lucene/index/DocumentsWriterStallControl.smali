.class final Lorg/apache/lucene/index/DocumentsWriterStallControl;
.super Ljava/lang/Object;
.source "DocumentsWriterStallControl.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private numWaiting:I

.field private volatile stalled:Z

.field private final waiting:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Thread;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private wasStalled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lorg/apache/lucene/index/DocumentsWriterStallControl;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/DocumentsWriterStallControl;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    new-instance v0, Ljava/util/IdentityHashMap;

    invoke-direct {v0}, Ljava/util/IdentityHashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterStallControl;->waiting:Ljava/util/Map;

    .line 40
    return-void
.end method

.method private decrWaiters()Z
    .locals 2

    .prologue
    .line 97
    iget v0, p0, Lorg/apache/lucene/index/DocumentsWriterStallControl;->numWaiting:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/index/DocumentsWriterStallControl;->numWaiting:I

    .line 98
    sget-boolean v0, Lorg/apache/lucene/index/DocumentsWriterStallControl;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterStallControl;->waiting:Ljava/util/Map;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 99
    :cond_0
    iget v0, p0, Lorg/apache/lucene/index/DocumentsWriterStallControl;->numWaiting:I

    if-ltz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private incWaiters()Z
    .locals 3

    .prologue
    .line 90
    iget v0, p0, Lorg/apache/lucene/index/DocumentsWriterStallControl;->numWaiting:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/index/DocumentsWriterStallControl;->numWaiting:I

    .line 91
    sget-boolean v0, Lorg/apache/lucene/index/DocumentsWriterStallControl;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterStallControl;->waiting:Ljava/util/Map;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 93
    :cond_0
    iget v0, p0, Lorg/apache/lucene/index/DocumentsWriterStallControl;->numWaiting:I

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method anyStalledThreads()Z
    .locals 1

    .prologue
    .line 85
    iget-boolean v0, p0, Lorg/apache/lucene/index/DocumentsWriterStallControl;->stalled:Z

    return v0
.end method

.method declared-synchronized hasBlocked()Z
    .locals 1

    .prologue
    .line 103
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lorg/apache/lucene/index/DocumentsWriterStallControl;->numWaiting:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method isHealthy()Z
    .locals 1

    .prologue
    .line 107
    iget-boolean v0, p0, Lorg/apache/lucene/index/DocumentsWriterStallControl;->stalled:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method declared-synchronized isThreadQueued(Ljava/lang/Thread;)Z
    .locals 1
    .param p1, "t"    # Ljava/lang/Thread;

    .prologue
    .line 111
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterStallControl;->waiting:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized updateStalled(Z)V
    .locals 1
    .param p1, "stalled"    # Z

    .prologue
    .line 56
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lorg/apache/lucene/index/DocumentsWriterStallControl;->stalled:Z

    .line 57
    if-eqz p1, :cond_0

    .line 58
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/index/DocumentsWriterStallControl;->wasStalled:Z

    .line 60
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 61
    monitor-exit p0

    return-void

    .line 56
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method waitIfStalled()V
    .locals 2

    .prologue
    .line 68
    iget-boolean v1, p0, Lorg/apache/lucene/index/DocumentsWriterStallControl;->stalled:Z

    if-eqz v1, :cond_2

    .line 69
    monitor-enter p0

    .line 70
    :try_start_0
    iget-boolean v1, p0, Lorg/apache/lucene/index/DocumentsWriterStallControl;->stalled:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_1

    .line 73
    :try_start_1
    sget-boolean v1, Lorg/apache/lucene/index/DocumentsWriterStallControl;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/index/DocumentsWriterStallControl;->incWaiters()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 76
    :catch_0
    move-exception v0

    .line 77
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_2
    new-instance v1, Lorg/apache/lucene/util/ThreadInterruptedException;

    invoke-direct {v1, v0}, Lorg/apache/lucene/util/ThreadInterruptedException;-><init>(Ljava/lang/InterruptedException;)V

    throw v1

    .line 69
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 74
    :cond_0
    :try_start_3
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V

    .line 75
    sget-boolean v1, Lorg/apache/lucene/index/DocumentsWriterStallControl;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    invoke-direct {p0}, Lorg/apache/lucene/index/DocumentsWriterStallControl;->decrWaiters()Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 69
    :cond_1
    :try_start_4
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 82
    :cond_2
    return-void
.end method

.method declared-synchronized wasStalled()Z
    .locals 1

    .prologue
    .line 115
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lorg/apache/lucene/index/DocumentsWriterStallControl;->wasStalled:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
