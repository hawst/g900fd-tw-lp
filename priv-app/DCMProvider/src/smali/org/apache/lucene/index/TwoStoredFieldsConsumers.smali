.class Lorg/apache/lucene/index/TwoStoredFieldsConsumers;
.super Lorg/apache/lucene/index/StoredFieldsConsumer;
.source "TwoStoredFieldsConsumers.java"


# instance fields
.field private final first:Lorg/apache/lucene/index/StoredFieldsConsumer;

.field private final second:Lorg/apache/lucene/index/StoredFieldsConsumer;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/StoredFieldsConsumer;Lorg/apache/lucene/index/StoredFieldsConsumer;)V
    .locals 0
    .param p1, "first"    # Lorg/apache/lucene/index/StoredFieldsConsumer;
    .param p2, "second"    # Lorg/apache/lucene/index/StoredFieldsConsumer;

    .prologue
    .line 28
    invoke-direct {p0}, Lorg/apache/lucene/index/StoredFieldsConsumer;-><init>()V

    .line 29
    iput-object p1, p0, Lorg/apache/lucene/index/TwoStoredFieldsConsumers;->first:Lorg/apache/lucene/index/StoredFieldsConsumer;

    .line 30
    iput-object p2, p0, Lorg/apache/lucene/index/TwoStoredFieldsConsumers;->second:Lorg/apache/lucene/index/StoredFieldsConsumer;

    .line 31
    return-void
.end method


# virtual methods
.method abort()V
    .locals 1

    .prologue
    .line 48
    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/TwoStoredFieldsConsumers;->first:Lorg/apache/lucene/index/StoredFieldsConsumer;

    invoke-virtual {v0}, Lorg/apache/lucene/index/StoredFieldsConsumer;->abort()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 52
    :goto_0
    :try_start_1
    iget-object v0, p0, Lorg/apache/lucene/index/TwoStoredFieldsConsumers;->second:Lorg/apache/lucene/index/StoredFieldsConsumer;

    invoke-virtual {v0}, Lorg/apache/lucene/index/StoredFieldsConsumer;->abort()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    .line 55
    :goto_1
    return-void

    .line 53
    :catch_0
    move-exception v0

    goto :goto_1

    .line 49
    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method public addField(ILorg/apache/lucene/index/IndexableField;Lorg/apache/lucene/index/FieldInfo;)V
    .locals 1
    .param p1, "docID"    # I
    .param p2, "field"    # Lorg/apache/lucene/index/IndexableField;
    .param p3, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 35
    iget-object v0, p0, Lorg/apache/lucene/index/TwoStoredFieldsConsumers;->first:Lorg/apache/lucene/index/StoredFieldsConsumer;

    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/lucene/index/StoredFieldsConsumer;->addField(ILorg/apache/lucene/index/IndexableField;Lorg/apache/lucene/index/FieldInfo;)V

    .line 36
    iget-object v0, p0, Lorg/apache/lucene/index/TwoStoredFieldsConsumers;->second:Lorg/apache/lucene/index/StoredFieldsConsumer;

    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/lucene/index/StoredFieldsConsumer;->addField(ILorg/apache/lucene/index/IndexableField;Lorg/apache/lucene/index/FieldInfo;)V

    .line 37
    return-void
.end method

.method finishDocument()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 65
    iget-object v0, p0, Lorg/apache/lucene/index/TwoStoredFieldsConsumers;->first:Lorg/apache/lucene/index/StoredFieldsConsumer;

    invoke-virtual {v0}, Lorg/apache/lucene/index/StoredFieldsConsumer;->finishDocument()V

    .line 66
    iget-object v0, p0, Lorg/apache/lucene/index/TwoStoredFieldsConsumers;->second:Lorg/apache/lucene/index/StoredFieldsConsumer;

    invoke-virtual {v0}, Lorg/apache/lucene/index/StoredFieldsConsumer;->finishDocument()V

    .line 67
    return-void
.end method

.method flush(Lorg/apache/lucene/index/SegmentWriteState;)V
    .locals 1
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentWriteState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 41
    iget-object v0, p0, Lorg/apache/lucene/index/TwoStoredFieldsConsumers;->first:Lorg/apache/lucene/index/StoredFieldsConsumer;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/StoredFieldsConsumer;->flush(Lorg/apache/lucene/index/SegmentWriteState;)V

    .line 42
    iget-object v0, p0, Lorg/apache/lucene/index/TwoStoredFieldsConsumers;->second:Lorg/apache/lucene/index/StoredFieldsConsumer;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/StoredFieldsConsumer;->flush(Lorg/apache/lucene/index/SegmentWriteState;)V

    .line 43
    return-void
.end method

.method startDocument()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 59
    iget-object v0, p0, Lorg/apache/lucene/index/TwoStoredFieldsConsumers;->first:Lorg/apache/lucene/index/StoredFieldsConsumer;

    invoke-virtual {v0}, Lorg/apache/lucene/index/StoredFieldsConsumer;->startDocument()V

    .line 60
    iget-object v0, p0, Lorg/apache/lucene/index/TwoStoredFieldsConsumers;->second:Lorg/apache/lucene/index/StoredFieldsConsumer;

    invoke-virtual {v0}, Lorg/apache/lucene/index/StoredFieldsConsumer;->startDocument()V

    .line 61
    return-void
.end method
