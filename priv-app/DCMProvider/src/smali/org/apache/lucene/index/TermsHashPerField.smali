.class final Lorg/apache/lucene/index/TermsHashPerField;
.super Lorg/apache/lucene/index/InvertedDocConsumerPerField;
.source "TermsHashPerField.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/TermsHashPerField$PostingsBytesStartArray;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final HASH_INIT_SIZE:I = 0x4


# instance fields
.field final bytePool:Lorg/apache/lucene/util/ByteBlockPool;

.field final bytesHash:Lorg/apache/lucene/util/BytesRefHash;

.field private final bytesUsed:Lorg/apache/lucene/util/Counter;

.field final consumer:Lorg/apache/lucene/index/TermsHashConsumerPerField;

.field private doCall:Z

.field private doNextCall:Z

.field final docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

.field final fieldInfo:Lorg/apache/lucene/index/FieldInfo;

.field final fieldState:Lorg/apache/lucene/index/FieldInvertState;

.field final intPool:Lorg/apache/lucene/util/IntBlockPool;

.field intUptoStart:I

.field intUptos:[I

.field final nextPerField:Lorg/apache/lucene/index/TermsHashPerField;

.field final numPostingInt:I

.field postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

.field final streamCount:I

.field termAtt:Lorg/apache/lucene/analysis/tokenattributes/TermToBytesRefAttribute;

.field final termBytePool:Lorg/apache/lucene/util/ByteBlockPool;

.field termBytesRef:Lorg/apache/lucene/util/BytesRef;

.field final termsHash:Lorg/apache/lucene/index/TermsHash;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lorg/apache/lucene/index/TermsHashPerField;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/TermsHashPerField;->$assertionsDisabled:Z

    .line 33
    return-void

    .line 32
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/index/DocInverterPerField;Lorg/apache/lucene/index/TermsHash;Lorg/apache/lucene/index/TermsHash;Lorg/apache/lucene/index/FieldInfo;)V
    .locals 5
    .param p1, "docInverterPerField"    # Lorg/apache/lucene/index/DocInverterPerField;
    .param p2, "termsHash"    # Lorg/apache/lucene/index/TermsHash;
    .param p3, "nextTermsHash"    # Lorg/apache/lucene/index/TermsHash;
    .param p4, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;

    .prologue
    const/4 v4, 0x0

    .line 60
    invoke-direct {p0}, Lorg/apache/lucene/index/InvertedDocConsumerPerField;-><init>()V

    .line 61
    iget-object v1, p2, Lorg/apache/lucene/index/TermsHash;->intPool:Lorg/apache/lucene/util/IntBlockPool;

    iput-object v1, p0, Lorg/apache/lucene/index/TermsHashPerField;->intPool:Lorg/apache/lucene/util/IntBlockPool;

    .line 62
    iget-object v1, p2, Lorg/apache/lucene/index/TermsHash;->bytePool:Lorg/apache/lucene/util/ByteBlockPool;

    iput-object v1, p0, Lorg/apache/lucene/index/TermsHashPerField;->bytePool:Lorg/apache/lucene/util/ByteBlockPool;

    .line 63
    iget-object v1, p2, Lorg/apache/lucene/index/TermsHash;->termBytePool:Lorg/apache/lucene/util/ByteBlockPool;

    iput-object v1, p0, Lorg/apache/lucene/index/TermsHashPerField;->termBytePool:Lorg/apache/lucene/util/ByteBlockPool;

    .line 64
    iget-object v1, p2, Lorg/apache/lucene/index/TermsHash;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    iput-object v1, p0, Lorg/apache/lucene/index/TermsHashPerField;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    .line 65
    iput-object p2, p0, Lorg/apache/lucene/index/TermsHashPerField;->termsHash:Lorg/apache/lucene/index/TermsHash;

    .line 66
    iget-object v1, p2, Lorg/apache/lucene/index/TermsHash;->bytesUsed:Lorg/apache/lucene/util/Counter;

    iput-object v1, p0, Lorg/apache/lucene/index/TermsHashPerField;->bytesUsed:Lorg/apache/lucene/util/Counter;

    .line 67
    iget-object v1, p1, Lorg/apache/lucene/index/DocInverterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iput-object v1, p0, Lorg/apache/lucene/index/TermsHashPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    .line 68
    iget-object v1, p2, Lorg/apache/lucene/index/TermsHash;->consumer:Lorg/apache/lucene/index/TermsHashConsumer;

    invoke-virtual {v1, p0, p4}, Lorg/apache/lucene/index/TermsHashConsumer;->addField(Lorg/apache/lucene/index/TermsHashPerField;Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/TermsHashConsumerPerField;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/index/TermsHashPerField;->consumer:Lorg/apache/lucene/index/TermsHashConsumerPerField;

    .line 69
    new-instance v0, Lorg/apache/lucene/index/TermsHashPerField$PostingsBytesStartArray;

    iget-object v1, p0, Lorg/apache/lucene/index/TermsHashPerField;->bytesUsed:Lorg/apache/lucene/util/Counter;

    invoke-direct {v0, p0, v1, v4}, Lorg/apache/lucene/index/TermsHashPerField$PostingsBytesStartArray;-><init>(Lorg/apache/lucene/index/TermsHashPerField;Lorg/apache/lucene/util/Counter;Lorg/apache/lucene/index/TermsHashPerField$PostingsBytesStartArray;)V

    .line 70
    .local v0, "byteStarts":Lorg/apache/lucene/index/TermsHashPerField$PostingsBytesStartArray;
    new-instance v1, Lorg/apache/lucene/util/BytesRefHash;

    iget-object v2, p0, Lorg/apache/lucene/index/TermsHashPerField;->termBytePool:Lorg/apache/lucene/util/ByteBlockPool;

    const/4 v3, 0x4

    invoke-direct {v1, v2, v3, v0}, Lorg/apache/lucene/util/BytesRefHash;-><init>(Lorg/apache/lucene/util/ByteBlockPool;ILorg/apache/lucene/util/BytesRefHash$BytesStartArray;)V

    iput-object v1, p0, Lorg/apache/lucene/index/TermsHashPerField;->bytesHash:Lorg/apache/lucene/util/BytesRefHash;

    .line 71
    iget-object v1, p0, Lorg/apache/lucene/index/TermsHashPerField;->consumer:Lorg/apache/lucene/index/TermsHashConsumerPerField;

    invoke-virtual {v1}, Lorg/apache/lucene/index/TermsHashConsumerPerField;->getStreamCount()I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/index/TermsHashPerField;->streamCount:I

    .line 72
    iget v1, p0, Lorg/apache/lucene/index/TermsHashPerField;->streamCount:I

    mul-int/lit8 v1, v1, 0x2

    iput v1, p0, Lorg/apache/lucene/index/TermsHashPerField;->numPostingInt:I

    .line 73
    iput-object p4, p0, Lorg/apache/lucene/index/TermsHashPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    .line 74
    if-eqz p3, :cond_0

    .line 75
    invoke-virtual {p3, p1, p4}, Lorg/apache/lucene/index/TermsHash;->addField(Lorg/apache/lucene/index/DocInverterPerField;Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/InvertedDocConsumerPerField;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/TermsHashPerField;

    iput-object v1, p0, Lorg/apache/lucene/index/TermsHashPerField;->nextPerField:Lorg/apache/lucene/index/TermsHashPerField;

    .line 78
    :goto_0
    return-void

    .line 77
    :cond_0
    iput-object v4, p0, Lorg/apache/lucene/index/TermsHashPerField;->nextPerField:Lorg/apache/lucene/index/TermsHashPerField;

    goto :goto_0
.end method


# virtual methods
.method public abort()V
    .locals 1

    .prologue
    .line 94
    invoke-virtual {p0}, Lorg/apache/lucene/index/TermsHashPerField;->reset()V

    .line 95
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->nextPerField:Lorg/apache/lucene/index/TermsHashPerField;

    if-eqz v0, :cond_0

    .line 96
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->nextPerField:Lorg/apache/lucene/index/TermsHashPerField;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermsHashPerField;->abort()V

    .line 97
    :cond_0
    return-void
.end method

.method add()V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 185
    :try_start_0
    iget-object v6, p0, Lorg/apache/lucene/index/TermsHashPerField;->bytesHash:Lorg/apache/lucene/util/BytesRefHash;

    iget-object v7, p0, Lorg/apache/lucene/index/TermsHashPerField;->termBytesRef:Lorg/apache/lucene/util/BytesRef;

    iget-object v8, p0, Lorg/apache/lucene/index/TermsHashPerField;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/TermToBytesRefAttribute;

    invoke-interface {v8}, Lorg/apache/lucene/analysis/tokenattributes/TermToBytesRefAttribute;->fillBytesRef()I

    move-result v8

    invoke-virtual {v6, v7, v8}, Lorg/apache/lucene/util/BytesRefHash;->add(Lorg/apache/lucene/util/BytesRef;I)I
    :try_end_0
    .catch Lorg/apache/lucene/util/BytesRefHash$MaxBytesLengthExceededException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    .line 205
    .local v4, "termID":I
    if-ltz v4, :cond_5

    .line 206
    iget-object v6, p0, Lorg/apache/lucene/index/TermsHashPerField;->bytesHash:Lorg/apache/lucene/util/BytesRefHash;

    invoke-virtual {v6, v4}, Lorg/apache/lucene/util/BytesRefHash;->byteStart(I)I

    .line 208
    iget v6, p0, Lorg/apache/lucene/index/TermsHashPerField;->numPostingInt:I

    iget-object v7, p0, Lorg/apache/lucene/index/TermsHashPerField;->intPool:Lorg/apache/lucene/util/IntBlockPool;

    iget v7, v7, Lorg/apache/lucene/util/IntBlockPool;->intUpto:I

    add-int/2addr v6, v7

    const/16 v7, 0x2000

    if-le v6, v7, :cond_0

    .line 209
    iget-object v6, p0, Lorg/apache/lucene/index/TermsHashPerField;->intPool:Lorg/apache/lucene/util/IntBlockPool;

    invoke-virtual {v6}, Lorg/apache/lucene/util/IntBlockPool;->nextBuffer()V

    .line 212
    :cond_0
    const v6, 0x8000

    iget-object v7, p0, Lorg/apache/lucene/index/TermsHashPerField;->bytePool:Lorg/apache/lucene/util/ByteBlockPool;

    iget v7, v7, Lorg/apache/lucene/util/ByteBlockPool;->byteUpto:I

    sub-int/2addr v6, v7

    iget v7, p0, Lorg/apache/lucene/index/TermsHashPerField;->numPostingInt:I

    sget v8, Lorg/apache/lucene/util/ByteBlockPool;->FIRST_LEVEL_SIZE:I

    mul-int/2addr v7, v8

    if-ge v6, v7, :cond_1

    .line 213
    iget-object v6, p0, Lorg/apache/lucene/index/TermsHashPerField;->bytePool:Lorg/apache/lucene/util/ByteBlockPool;

    invoke-virtual {v6}, Lorg/apache/lucene/util/ByteBlockPool;->nextBuffer()V

    .line 216
    :cond_1
    iget-object v6, p0, Lorg/apache/lucene/index/TermsHashPerField;->intPool:Lorg/apache/lucene/util/IntBlockPool;

    iget-object v6, v6, Lorg/apache/lucene/util/IntBlockPool;->buffer:[I

    iput-object v6, p0, Lorg/apache/lucene/index/TermsHashPerField;->intUptos:[I

    .line 217
    iget-object v6, p0, Lorg/apache/lucene/index/TermsHashPerField;->intPool:Lorg/apache/lucene/util/IntBlockPool;

    iget v6, v6, Lorg/apache/lucene/util/IntBlockPool;->intUpto:I

    iput v6, p0, Lorg/apache/lucene/index/TermsHashPerField;->intUptoStart:I

    .line 218
    iget-object v6, p0, Lorg/apache/lucene/index/TermsHashPerField;->intPool:Lorg/apache/lucene/util/IntBlockPool;

    iget v7, v6, Lorg/apache/lucene/util/IntBlockPool;->intUpto:I

    iget v8, p0, Lorg/apache/lucene/index/TermsHashPerField;->streamCount:I

    add-int/2addr v7, v8

    iput v7, v6, Lorg/apache/lucene/util/IntBlockPool;->intUpto:I

    .line 220
    iget-object v6, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    iget-object v6, v6, Lorg/apache/lucene/index/ParallelPostingsArray;->intStarts:[I

    iget v7, p0, Lorg/apache/lucene/index/TermsHashPerField;->intUptoStart:I

    iget-object v8, p0, Lorg/apache/lucene/index/TermsHashPerField;->intPool:Lorg/apache/lucene/util/IntBlockPool;

    iget v8, v8, Lorg/apache/lucene/util/IntBlockPool;->intOffset:I

    add-int/2addr v7, v8

    aput v7, v6, v4

    .line 222
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v6, p0, Lorg/apache/lucene/index/TermsHashPerField;->streamCount:I

    if-lt v1, v6, :cond_4

    .line 226
    iget-object v6, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    iget-object v6, v6, Lorg/apache/lucene/index/ParallelPostingsArray;->byteStarts:[I

    iget-object v7, p0, Lorg/apache/lucene/index/TermsHashPerField;->intUptos:[I

    iget v8, p0, Lorg/apache/lucene/index/TermsHashPerField;->intUptoStart:I

    aget v7, v7, v8

    aput v7, v6, v4

    .line 228
    iget-object v6, p0, Lorg/apache/lucene/index/TermsHashPerField;->consumer:Lorg/apache/lucene/index/TermsHashConsumerPerField;

    invoke-virtual {v6, v4}, Lorg/apache/lucene/index/TermsHashConsumerPerField;->newTerm(I)V

    .line 238
    .end local v1    # "i":I
    :goto_1
    iget-boolean v6, p0, Lorg/apache/lucene/index/TermsHashPerField;->doNextCall:Z

    if-eqz v6, :cond_2

    .line 239
    iget-object v6, p0, Lorg/apache/lucene/index/TermsHashPerField;->nextPerField:Lorg/apache/lucene/index/TermsHashPerField;

    iget-object v7, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    iget-object v7, v7, Lorg/apache/lucene/index/ParallelPostingsArray;->textStarts:[I

    aget v7, v7, v4

    invoke-virtual {v6, v7}, Lorg/apache/lucene/index/TermsHashPerField;->add(I)V

    .line 240
    .end local v4    # "termID":I
    :cond_2
    :goto_2
    return-void

    .line 186
    :catch_0
    move-exception v0

    .line 193
    .local v0, "e":Lorg/apache/lucene/util/BytesRefHash$MaxBytesLengthExceededException;
    iget-object v6, p0, Lorg/apache/lucene/index/TermsHashPerField;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    iget-object v6, v6, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;->maxTermPrefix:Ljava/lang/String;

    if-nez v6, :cond_3

    .line 194
    iget-object v6, p0, Lorg/apache/lucene/index/TermsHashPerField;->termBytesRef:Lorg/apache/lucene/util/BytesRef;

    iget v3, v6, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 196
    .local v3, "saved":I
    :try_start_1
    iget-object v6, p0, Lorg/apache/lucene/index/TermsHashPerField;->termBytesRef:Lorg/apache/lucene/util/BytesRef;

    const/16 v7, 0x1e

    const/16 v8, 0x7ffe

    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    move-result v7

    iput v7, v6, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 197
    iget-object v6, p0, Lorg/apache/lucene/index/TermsHashPerField;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    iget-object v7, p0, Lorg/apache/lucene/index/TermsHashPerField;->termBytesRef:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v7}, Lorg/apache/lucene/util/BytesRef;->toString()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;->maxTermPrefix:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 199
    iget-object v6, p0, Lorg/apache/lucene/index/TermsHashPerField;->termBytesRef:Lorg/apache/lucene/util/BytesRef;

    iput v3, v6, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 202
    .end local v3    # "saved":I
    :cond_3
    iget-object v6, p0, Lorg/apache/lucene/index/TermsHashPerField;->consumer:Lorg/apache/lucene/index/TermsHashConsumerPerField;

    invoke-virtual {v6}, Lorg/apache/lucene/index/TermsHashConsumerPerField;->skippingLongTerm()V

    goto :goto_2

    .line 198
    .restart local v3    # "saved":I
    :catchall_0
    move-exception v6

    .line 199
    iget-object v7, p0, Lorg/apache/lucene/index/TermsHashPerField;->termBytesRef:Lorg/apache/lucene/util/BytesRef;

    iput v3, v7, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 200
    throw v6

    .line 223
    .end local v0    # "e":Lorg/apache/lucene/util/BytesRefHash$MaxBytesLengthExceededException;
    .end local v3    # "saved":I
    .restart local v1    # "i":I
    .restart local v4    # "termID":I
    :cond_4
    iget-object v6, p0, Lorg/apache/lucene/index/TermsHashPerField;->bytePool:Lorg/apache/lucene/util/ByteBlockPool;

    sget v7, Lorg/apache/lucene/util/ByteBlockPool;->FIRST_LEVEL_SIZE:I

    invoke-virtual {v6, v7}, Lorg/apache/lucene/util/ByteBlockPool;->newSlice(I)I

    move-result v5

    .line 224
    .local v5, "upto":I
    iget-object v6, p0, Lorg/apache/lucene/index/TermsHashPerField;->intUptos:[I

    iget v7, p0, Lorg/apache/lucene/index/TermsHashPerField;->intUptoStart:I

    add-int/2addr v7, v1

    iget-object v8, p0, Lorg/apache/lucene/index/TermsHashPerField;->bytePool:Lorg/apache/lucene/util/ByteBlockPool;

    iget v8, v8, Lorg/apache/lucene/util/ByteBlockPool;->byteOffset:I

    add-int/2addr v8, v5

    aput v8, v6, v7

    .line 222
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 231
    .end local v1    # "i":I
    .end local v5    # "upto":I
    :cond_5
    neg-int v6, v4

    add-int/lit8 v4, v6, -0x1

    .line 232
    iget-object v6, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    iget-object v6, v6, Lorg/apache/lucene/index/ParallelPostingsArray;->intStarts:[I

    aget v2, v6, v4

    .line 233
    .local v2, "intStart":I
    iget-object v6, p0, Lorg/apache/lucene/index/TermsHashPerField;->intPool:Lorg/apache/lucene/util/IntBlockPool;

    iget-object v6, v6, Lorg/apache/lucene/util/IntBlockPool;->buffers:[[I

    shr-int/lit8 v7, v2, 0xd

    aget-object v6, v6, v7

    iput-object v6, p0, Lorg/apache/lucene/index/TermsHashPerField;->intUptos:[I

    .line 234
    and-int/lit16 v6, v2, 0x1fff

    iput v6, p0, Lorg/apache/lucene/index/TermsHashPerField;->intUptoStart:I

    .line 235
    iget-object v6, p0, Lorg/apache/lucene/index/TermsHashPerField;->consumer:Lorg/apache/lucene/index/TermsHashConsumerPerField;

    invoke-virtual {v6, v4}, Lorg/apache/lucene/index/TermsHashConsumerPerField;->addTerm(I)V

    goto :goto_1
.end method

.method public add(I)V
    .locals 7
    .param p1, "textStart"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 141
    iget-object v4, p0, Lorg/apache/lucene/index/TermsHashPerField;->bytesHash:Lorg/apache/lucene/util/BytesRefHash;

    invoke-virtual {v4, p1}, Lorg/apache/lucene/util/BytesRefHash;->addByPoolOffset(I)I

    move-result v2

    .line 142
    .local v2, "termID":I
    if-ltz v2, :cond_3

    .line 146
    iget v4, p0, Lorg/apache/lucene/index/TermsHashPerField;->numPostingInt:I

    iget-object v5, p0, Lorg/apache/lucene/index/TermsHashPerField;->intPool:Lorg/apache/lucene/util/IntBlockPool;

    iget v5, v5, Lorg/apache/lucene/util/IntBlockPool;->intUpto:I

    add-int/2addr v4, v5

    const/16 v5, 0x2000

    if-le v4, v5, :cond_0

    .line 147
    iget-object v4, p0, Lorg/apache/lucene/index/TermsHashPerField;->intPool:Lorg/apache/lucene/util/IntBlockPool;

    invoke-virtual {v4}, Lorg/apache/lucene/util/IntBlockPool;->nextBuffer()V

    .line 149
    :cond_0
    const v4, 0x8000

    iget-object v5, p0, Lorg/apache/lucene/index/TermsHashPerField;->bytePool:Lorg/apache/lucene/util/ByteBlockPool;

    iget v5, v5, Lorg/apache/lucene/util/ByteBlockPool;->byteUpto:I

    sub-int/2addr v4, v5

    iget v5, p0, Lorg/apache/lucene/index/TermsHashPerField;->numPostingInt:I

    sget v6, Lorg/apache/lucene/util/ByteBlockPool;->FIRST_LEVEL_SIZE:I

    mul-int/2addr v5, v6

    if-ge v4, v5, :cond_1

    .line 150
    iget-object v4, p0, Lorg/apache/lucene/index/TermsHashPerField;->bytePool:Lorg/apache/lucene/util/ByteBlockPool;

    invoke-virtual {v4}, Lorg/apache/lucene/util/ByteBlockPool;->nextBuffer()V

    .line 153
    :cond_1
    iget-object v4, p0, Lorg/apache/lucene/index/TermsHashPerField;->intPool:Lorg/apache/lucene/util/IntBlockPool;

    iget-object v4, v4, Lorg/apache/lucene/util/IntBlockPool;->buffer:[I

    iput-object v4, p0, Lorg/apache/lucene/index/TermsHashPerField;->intUptos:[I

    .line 154
    iget-object v4, p0, Lorg/apache/lucene/index/TermsHashPerField;->intPool:Lorg/apache/lucene/util/IntBlockPool;

    iget v4, v4, Lorg/apache/lucene/util/IntBlockPool;->intUpto:I

    iput v4, p0, Lorg/apache/lucene/index/TermsHashPerField;->intUptoStart:I

    .line 155
    iget-object v4, p0, Lorg/apache/lucene/index/TermsHashPerField;->intPool:Lorg/apache/lucene/util/IntBlockPool;

    iget v5, v4, Lorg/apache/lucene/util/IntBlockPool;->intUpto:I

    iget v6, p0, Lorg/apache/lucene/index/TermsHashPerField;->streamCount:I

    add-int/2addr v5, v6

    iput v5, v4, Lorg/apache/lucene/util/IntBlockPool;->intUpto:I

    .line 157
    iget-object v4, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    iget-object v4, v4, Lorg/apache/lucene/index/ParallelPostingsArray;->intStarts:[I

    iget v5, p0, Lorg/apache/lucene/index/TermsHashPerField;->intUptoStart:I

    iget-object v6, p0, Lorg/apache/lucene/index/TermsHashPerField;->intPool:Lorg/apache/lucene/util/IntBlockPool;

    iget v6, v6, Lorg/apache/lucene/util/IntBlockPool;->intOffset:I

    add-int/2addr v5, v6

    aput v5, v4, v2

    .line 159
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v4, p0, Lorg/apache/lucene/index/TermsHashPerField;->streamCount:I

    if-lt v0, v4, :cond_2

    .line 163
    iget-object v4, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    iget-object v4, v4, Lorg/apache/lucene/index/ParallelPostingsArray;->byteStarts:[I

    iget-object v5, p0, Lorg/apache/lucene/index/TermsHashPerField;->intUptos:[I

    iget v6, p0, Lorg/apache/lucene/index/TermsHashPerField;->intUptoStart:I

    aget v5, v5, v6

    aput v5, v4, v2

    .line 165
    iget-object v4, p0, Lorg/apache/lucene/index/TermsHashPerField;->consumer:Lorg/apache/lucene/index/TermsHashConsumerPerField;

    invoke-virtual {v4, v2}, Lorg/apache/lucene/index/TermsHashConsumerPerField;->newTerm(I)V

    .line 174
    .end local v0    # "i":I
    :goto_1
    return-void

    .line 160
    .restart local v0    # "i":I
    :cond_2
    iget-object v4, p0, Lorg/apache/lucene/index/TermsHashPerField;->bytePool:Lorg/apache/lucene/util/ByteBlockPool;

    sget v5, Lorg/apache/lucene/util/ByteBlockPool;->FIRST_LEVEL_SIZE:I

    invoke-virtual {v4, v5}, Lorg/apache/lucene/util/ByteBlockPool;->newSlice(I)I

    move-result v3

    .line 161
    .local v3, "upto":I
    iget-object v4, p0, Lorg/apache/lucene/index/TermsHashPerField;->intUptos:[I

    iget v5, p0, Lorg/apache/lucene/index/TermsHashPerField;->intUptoStart:I

    add-int/2addr v5, v0

    iget-object v6, p0, Lorg/apache/lucene/index/TermsHashPerField;->bytePool:Lorg/apache/lucene/util/ByteBlockPool;

    iget v6, v6, Lorg/apache/lucene/util/ByteBlockPool;->byteOffset:I

    add-int/2addr v6, v3

    aput v6, v4, v5

    .line 159
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 168
    .end local v0    # "i":I
    .end local v3    # "upto":I
    :cond_3
    neg-int v4, v2

    add-int/lit8 v2, v4, -0x1

    .line 169
    iget-object v4, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    iget-object v4, v4, Lorg/apache/lucene/index/ParallelPostingsArray;->intStarts:[I

    aget v1, v4, v2

    .line 170
    .local v1, "intStart":I
    iget-object v4, p0, Lorg/apache/lucene/index/TermsHashPerField;->intPool:Lorg/apache/lucene/util/IntBlockPool;

    iget-object v4, v4, Lorg/apache/lucene/util/IntBlockPool;->buffers:[[I

    shr-int/lit8 v5, v1, 0xd

    aget-object v4, v4, v5

    iput-object v4, p0, Lorg/apache/lucene/index/TermsHashPerField;->intUptos:[I

    .line 171
    and-int/lit16 v4, v1, 0x1fff

    iput v4, p0, Lorg/apache/lucene/index/TermsHashPerField;->intUptoStart:I

    .line 172
    iget-object v4, p0, Lorg/apache/lucene/index/TermsHashPerField;->consumer:Lorg/apache/lucene/index/TermsHashConsumerPerField;

    invoke-virtual {v4, v2}, Lorg/apache/lucene/index/TermsHashConsumerPerField;->addTerm(I)V

    goto :goto_1
.end method

.method finish()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 278
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->consumer:Lorg/apache/lucene/index/TermsHashConsumerPerField;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermsHashConsumerPerField;->finish()V

    .line 279
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->nextPerField:Lorg/apache/lucene/index/TermsHashPerField;

    if-eqz v0, :cond_0

    .line 280
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->nextPerField:Lorg/apache/lucene/index/TermsHashPerField;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermsHashPerField;->finish()V

    .line 281
    :cond_0
    return-void
.end method

.method public initReader(Lorg/apache/lucene/index/ByteSliceReader;II)V
    .locals 6
    .param p1, "reader"    # Lorg/apache/lucene/index/ByteSliceReader;
    .param p2, "termID"    # I
    .param p3, "stream"    # I

    .prologue
    .line 100
    sget-boolean v3, Lorg/apache/lucene/index/TermsHashPerField;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    iget v3, p0, Lorg/apache/lucene/index/TermsHashPerField;->streamCount:I

    if-lt p3, v3, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 101
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    iget-object v3, v3, Lorg/apache/lucene/index/ParallelPostingsArray;->intStarts:[I

    aget v0, v3, p2

    .line 102
    .local v0, "intStart":I
    iget-object v3, p0, Lorg/apache/lucene/index/TermsHashPerField;->intPool:Lorg/apache/lucene/util/IntBlockPool;

    iget-object v3, v3, Lorg/apache/lucene/util/IntBlockPool;->buffers:[[I

    shr-int/lit8 v4, v0, 0xd

    aget-object v1, v3, v4

    .line 103
    .local v1, "ints":[I
    and-int/lit16 v2, v0, 0x1fff

    .line 104
    .local v2, "upto":I
    iget-object v3, p0, Lorg/apache/lucene/index/TermsHashPerField;->bytePool:Lorg/apache/lucene/util/ByteBlockPool;

    .line 105
    iget-object v4, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    iget-object v4, v4, Lorg/apache/lucene/index/ParallelPostingsArray;->byteStarts:[I

    aget v4, v4, p2

    sget v5, Lorg/apache/lucene/util/ByteBlockPool;->FIRST_LEVEL_SIZE:I

    mul-int/2addr v5, p3

    add-int/2addr v4, v5

    .line 106
    add-int v5, v2, p3

    aget v5, v1, v5

    .line 104
    invoke-virtual {p1, v3, v4, v5}, Lorg/apache/lucene/index/ByteSliceReader;->init(Lorg/apache/lucene/util/ByteBlockPool;II)V

    .line 107
    return-void
.end method

.method public reset()V
    .locals 2

    .prologue
    .line 87
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->bytesHash:Lorg/apache/lucene/util/BytesRefHash;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/BytesRefHash;->clear(Z)V

    .line 88
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->nextPerField:Lorg/apache/lucene/index/TermsHashPerField;

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->nextPerField:Lorg/apache/lucene/index/TermsHashPerField;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermsHashPerField;->reset()V

    .line 90
    :cond_0
    return-void
.end method

.method shrinkHash(I)V
    .locals 2
    .param p1, "targetSize"    # I

    .prologue
    .line 83
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->bytesHash:Lorg/apache/lucene/util/BytesRefHash;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/BytesRefHash;->clear(Z)V

    .line 84
    return-void
.end method

.method public sortPostings(Ljava/util/Comparator;)[I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;)[I"
        }
    .end annotation

    .prologue
    .line 111
    .local p1, "termComp":Ljava/util/Comparator;, "Ljava/util/Comparator<Lorg/apache/lucene/util/BytesRef;>;"
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->bytesHash:Lorg/apache/lucene/util/BytesRefHash;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/util/BytesRefHash;->sort(Ljava/util/Comparator;)[I

    move-result-object v0

    return-object v0
.end method

.method start(Lorg/apache/lucene/index/IndexableField;)V
    .locals 2
    .param p1, "f"    # Lorg/apache/lucene/index/IndexableField;

    .prologue
    .line 119
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iget-object v0, v0, Lorg/apache/lucene/index/FieldInvertState;->attributeSource:Lorg/apache/lucene/util/AttributeSource;

    const-class v1, Lorg/apache/lucene/analysis/tokenattributes/TermToBytesRefAttribute;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/AttributeSource;->getAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/TermToBytesRefAttribute;

    iput-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/TermToBytesRefAttribute;

    .line 120
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/TermToBytesRefAttribute;

    invoke-interface {v0}, Lorg/apache/lucene/analysis/tokenattributes/TermToBytesRefAttribute;->getBytesRef()Lorg/apache/lucene/util/BytesRef;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->termBytesRef:Lorg/apache/lucene/util/BytesRef;

    .line 121
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->consumer:Lorg/apache/lucene/index/TermsHashConsumerPerField;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/TermsHashConsumerPerField;->start(Lorg/apache/lucene/index/IndexableField;)V

    .line 122
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->nextPerField:Lorg/apache/lucene/index/TermsHashPerField;

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->nextPerField:Lorg/apache/lucene/index/TermsHashPerField;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/TermsHashPerField;->start(Lorg/apache/lucene/index/IndexableField;)V

    .line 125
    :cond_0
    return-void
.end method

.method start([Lorg/apache/lucene/index/IndexableField;I)Z
    .locals 1
    .param p1, "fields"    # [Lorg/apache/lucene/index/IndexableField;
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 129
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->consumer:Lorg/apache/lucene/index/TermsHashConsumerPerField;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/index/TermsHashConsumerPerField;->start([Lorg/apache/lucene/index/IndexableField;I)Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->doCall:Z

    .line 130
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->bytesHash:Lorg/apache/lucene/util/BytesRefHash;

    invoke-virtual {v0}, Lorg/apache/lucene/util/BytesRefHash;->reinit()V

    .line 131
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->nextPerField:Lorg/apache/lucene/index/TermsHashPerField;

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->nextPerField:Lorg/apache/lucene/index/TermsHashPerField;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/index/TermsHashPerField;->start([Lorg/apache/lucene/index/IndexableField;I)Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->doNextCall:Z

    .line 134
    :cond_0
    iget-boolean v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->doCall:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->doNextCall:Z

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method writeByte(IB)V
    .locals 6
    .param p1, "stream"    # I
    .param p2, "b"    # B

    .prologue
    .line 246
    iget-object v3, p0, Lorg/apache/lucene/index/TermsHashPerField;->intUptos:[I

    iget v4, p0, Lorg/apache/lucene/index/TermsHashPerField;->intUptoStart:I

    add-int/2addr v4, p1

    aget v2, v3, v4

    .line 247
    .local v2, "upto":I
    iget-object v3, p0, Lorg/apache/lucene/index/TermsHashPerField;->bytePool:Lorg/apache/lucene/util/ByteBlockPool;

    iget-object v3, v3, Lorg/apache/lucene/util/ByteBlockPool;->buffers:[[B

    shr-int/lit8 v4, v2, 0xf

    aget-object v0, v3, v4

    .line 248
    .local v0, "bytes":[B
    sget-boolean v3, Lorg/apache/lucene/index/TermsHashPerField;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    if-nez v0, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 249
    :cond_0
    and-int/lit16 v1, v2, 0x7fff

    .line 250
    .local v1, "offset":I
    aget-byte v3, v0, v1

    if-eqz v3, :cond_1

    .line 252
    iget-object v3, p0, Lorg/apache/lucene/index/TermsHashPerField;->bytePool:Lorg/apache/lucene/util/ByteBlockPool;

    invoke-virtual {v3, v0, v1}, Lorg/apache/lucene/util/ByteBlockPool;->allocSlice([BI)I

    move-result v1

    .line 253
    iget-object v3, p0, Lorg/apache/lucene/index/TermsHashPerField;->bytePool:Lorg/apache/lucene/util/ByteBlockPool;

    iget-object v0, v3, Lorg/apache/lucene/util/ByteBlockPool;->buffer:[B

    .line 254
    iget-object v3, p0, Lorg/apache/lucene/index/TermsHashPerField;->intUptos:[I

    iget v4, p0, Lorg/apache/lucene/index/TermsHashPerField;->intUptoStart:I

    add-int/2addr v4, p1

    iget-object v5, p0, Lorg/apache/lucene/index/TermsHashPerField;->bytePool:Lorg/apache/lucene/util/ByteBlockPool;

    iget v5, v5, Lorg/apache/lucene/util/ByteBlockPool;->byteOffset:I

    add-int/2addr v5, v1

    aput v5, v3, v4

    .line 256
    :cond_1
    aput-byte p2, v0, v1

    .line 257
    iget-object v3, p0, Lorg/apache/lucene/index/TermsHashPerField;->intUptos:[I

    iget v4, p0, Lorg/apache/lucene/index/TermsHashPerField;->intUptoStart:I

    add-int/2addr v4, p1

    aget v5, v3, v4

    add-int/lit8 v5, v5, 0x1

    aput v5, v3, v4

    .line 258
    return-void
.end method

.method public writeBytes(I[BII)V
    .locals 3
    .param p1, "stream"    # I
    .param p2, "b"    # [B
    .param p3, "offset"    # I
    .param p4, "len"    # I

    .prologue
    .line 262
    add-int v0, p3, p4

    .line 263
    .local v0, "end":I
    move v1, p3

    .local v1, "i":I
    :goto_0
    if-lt v1, v0, :cond_0

    .line 265
    return-void

    .line 264
    :cond_0
    aget-byte v2, p2, v1

    invoke-virtual {p0, p1, v2}, Lorg/apache/lucene/index/TermsHashPerField;->writeByte(IB)V

    .line 263
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method writeVInt(II)V
    .locals 1
    .param p1, "stream"    # I
    .param p2, "i"    # I

    .prologue
    .line 268
    sget-boolean v0, Lorg/apache/lucene/index/TermsHashPerField;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->streamCount:I

    if-lt p1, v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 270
    :cond_0
    and-int/lit8 v0, p2, 0x7f

    or-int/lit16 v0, v0, 0x80

    int-to-byte v0, v0

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/index/TermsHashPerField;->writeByte(IB)V

    .line 271
    ushr-int/lit8 p2, p2, 0x7

    .line 269
    :cond_1
    and-int/lit8 v0, p2, -0x80

    if-nez v0, :cond_0

    .line 273
    int-to-byte v0, p2

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/index/TermsHashPerField;->writeByte(IB)V

    .line 274
    return-void
.end method
