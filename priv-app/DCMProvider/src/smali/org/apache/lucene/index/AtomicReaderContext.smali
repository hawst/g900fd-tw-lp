.class public final Lorg/apache/lucene/index/AtomicReaderContext;
.super Lorg/apache/lucene/index/IndexReaderContext;
.source "AtomicReaderContext.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field public final docBase:I

.field private final leaves:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/AtomicReaderContext;",
            ">;"
        }
    .end annotation
.end field

.field public final ord:I

.field private final reader:Lorg/apache/lucene/index/AtomicReader;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lorg/apache/lucene/index/AtomicReaderContext;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/AtomicReaderContext;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/index/AtomicReader;)V
    .locals 7
    .param p1, "atomicReader"    # Lorg/apache/lucene/index/AtomicReader;

    .prologue
    const/4 v3, 0x0

    .line 48
    const/4 v1, 0x0

    move-object v0, p0

    move-object v2, p1

    move v4, v3

    move v5, v3

    move v6, v3

    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/index/AtomicReaderContext;-><init>(Lorg/apache/lucene/index/CompositeReaderContext;Lorg/apache/lucene/index/AtomicReader;IIII)V

    .line 49
    return-void
.end method

.method constructor <init>(Lorg/apache/lucene/index/CompositeReaderContext;Lorg/apache/lucene/index/AtomicReader;IIII)V
    .locals 1
    .param p1, "parent"    # Lorg/apache/lucene/index/CompositeReaderContext;
    .param p2, "reader"    # Lorg/apache/lucene/index/AtomicReader;
    .param p3, "ord"    # I
    .param p4, "docBase"    # I
    .param p5, "leafOrd"    # I
    .param p6, "leafDocBase"    # I

    .prologue
    .line 40
    invoke-direct {p0, p1, p3, p4}, Lorg/apache/lucene/index/IndexReaderContext;-><init>(Lorg/apache/lucene/index/CompositeReaderContext;II)V

    .line 41
    iput p5, p0, Lorg/apache/lucene/index/AtomicReaderContext;->ord:I

    .line 42
    iput p6, p0, Lorg/apache/lucene/index/AtomicReaderContext;->docBase:I

    .line 43
    iput-object p2, p0, Lorg/apache/lucene/index/AtomicReaderContext;->reader:Lorg/apache/lucene/index/AtomicReader;

    .line 44
    iget-boolean v0, p0, Lorg/apache/lucene/index/AtomicReaderContext;->isTopLevel:Z

    if-eqz v0, :cond_0

    invoke-static {p0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lorg/apache/lucene/index/AtomicReaderContext;->leaves:Ljava/util/List;

    .line 45
    return-void

    .line 44
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public children()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/IndexReaderContext;",
            ">;"
        }
    .end annotation

    .prologue
    .line 62
    const/4 v0, 0x0

    return-object v0
.end method

.method public leaves()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/AtomicReaderContext;",
            ">;"
        }
    .end annotation

    .prologue
    .line 53
    iget-boolean v0, p0, Lorg/apache/lucene/index/AtomicReaderContext;->isTopLevel:Z

    if-nez v0, :cond_0

    .line 54
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This is not a top-level context."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 56
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/index/AtomicReaderContext;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/index/AtomicReaderContext;->leaves:Ljava/util/List;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 57
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/index/AtomicReaderContext;->leaves:Ljava/util/List;

    return-object v0
.end method

.method public reader()Lorg/apache/lucene/index/AtomicReader;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lorg/apache/lucene/index/AtomicReaderContext;->reader:Lorg/apache/lucene/index/AtomicReader;

    return-object v0
.end method

.method public bridge synthetic reader()Lorg/apache/lucene/index/IndexReader;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v0

    return-object v0
.end method
