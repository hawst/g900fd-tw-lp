.class Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;
.super Ljava/lang/Object;
.source "DocumentsWriterDeleteQueue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "DeleteSlice"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field sliceHead:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node",
            "<*>;"
        }
    .end annotation
.end field

.field sliceTail:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 245
    const-class v0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 250
    .local p1, "currentTail":Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;, "Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node<*>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 251
    sget-boolean v0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 257
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;->sliceTail:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;

    iput-object p1, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;->sliceHead:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;

    .line 258
    return-void
.end method


# virtual methods
.method apply(Lorg/apache/lucene/index/BufferedDeletes;I)V
    .locals 3
    .param p1, "del"    # Lorg/apache/lucene/index/BufferedDeletes;
    .param p2, "docIDUpto"    # I

    .prologue
    .line 261
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;->sliceHead:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;

    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;->sliceTail:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;

    if-ne v1, v2, :cond_0

    .line 279
    :goto_0
    return-void

    .line 271
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;->sliceHead:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;

    .line 273
    .local v0, "current":Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;, "Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node<*>;"
    :cond_1
    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;->next:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;

    .line 274
    sget-boolean v1, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;->$assertionsDisabled:Z

    if-nez v1, :cond_2

    if-nez v0, :cond_2

    new-instance v1, Ljava/lang/AssertionError;

    const-string v2, "slice property violated between the head on the tail must not be a null node"

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 275
    :cond_2
    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;->apply(Lorg/apache/lucene/index/BufferedDeletes;I)V

    .line 277
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;->sliceTail:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;

    if-ne v0, v1, :cond_1

    .line 278
    invoke-virtual {p0}, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;->reset()V

    goto :goto_0
.end method

.method isEmpty()Z
    .locals 2

    .prologue
    .line 295
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;->sliceHead:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;

    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;->sliceTail:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isTailItem(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "item"    # Ljava/lang/Object;

    .prologue
    .line 291
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;->sliceTail:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;->item:Ljava/lang/Object;

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method reset()V
    .locals 1

    .prologue
    .line 283
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;->sliceTail:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;->sliceHead:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;

    .line 284
    return-void
.end method
