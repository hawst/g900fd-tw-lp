.class public Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotCommitPoint;
.super Lorg/apache/lucene/index/IndexCommit;
.source "SnapshotDeletionPolicy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/SnapshotDeletionPolicy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "SnapshotCommitPoint"
.end annotation


# instance fields
.field protected cp:Lorg/apache/lucene/index/IndexCommit;

.field final synthetic this$0:Lorg/apache/lucene/index/SnapshotDeletionPolicy;


# direct methods
.method protected constructor <init>(Lorg/apache/lucene/index/SnapshotDeletionPolicy;Lorg/apache/lucene/index/IndexCommit;)V
    .locals 0
    .param p2, "cp"    # Lorg/apache/lucene/index/IndexCommit;

    .prologue
    .line 81
    iput-object p1, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotCommitPoint;->this$0:Lorg/apache/lucene/index/SnapshotDeletionPolicy;

    invoke-direct {p0}, Lorg/apache/lucene/index/IndexCommit;-><init>()V

    .line 82
    iput-object p2, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotCommitPoint;->cp:Lorg/apache/lucene/index/IndexCommit;

    .line 83
    return-void
.end method


# virtual methods
.method public delete()V
    .locals 2

    .prologue
    .line 100
    iget-object v1, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotCommitPoint;->this$0:Lorg/apache/lucene/index/SnapshotDeletionPolicy;

    monitor-enter v1

    .line 103
    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotCommitPoint;->getSegmentsFileName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotCommitPoint;->shouldDelete(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotCommitPoint;->cp:Lorg/apache/lucene/index/IndexCommit;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexCommit;->delete()V

    .line 100
    :cond_0
    monitor-exit v1

    .line 107
    return-void

    .line 100
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getDirectory()Lorg/apache/lucene/store/Directory;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotCommitPoint;->cp:Lorg/apache/lucene/index/IndexCommit;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexCommit;->getDirectory()Lorg/apache/lucene/store/Directory;

    move-result-object v0

    return-object v0
.end method

.method public getFileNames()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 116
    iget-object v0, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotCommitPoint;->cp:Lorg/apache/lucene/index/IndexCommit;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexCommit;->getFileNames()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public getGeneration()J
    .locals 2

    .prologue
    .line 121
    iget-object v0, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotCommitPoint;->cp:Lorg/apache/lucene/index/IndexCommit;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexCommit;->getGeneration()J

    move-result-wide v0

    return-wide v0
.end method

.method public getSegmentCount()I
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotCommitPoint;->cp:Lorg/apache/lucene/index/IndexCommit;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexCommit;->getSegmentCount()I

    move-result v0

    return v0
.end method

.method public getSegmentsFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotCommitPoint;->cp:Lorg/apache/lucene/index/IndexCommit;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexCommit;->getSegmentsFileName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUserData()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 131
    iget-object v0, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotCommitPoint;->cp:Lorg/apache/lucene/index/IndexCommit;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexCommit;->getUserData()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public isDeleted()Z
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotCommitPoint;->cp:Lorg/apache/lucene/index/IndexCommit;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexCommit;->isDeleted()Z

    move-result v0

    return v0
.end method

.method protected shouldDelete(Ljava/lang/String;)Z
    .locals 1
    .param p1, "segmentsFileName"    # Ljava/lang/String;

    .prologue
    .line 95
    iget-object v0, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotCommitPoint;->this$0:Lorg/apache/lucene/index/SnapshotDeletionPolicy;

    # getter for: Lorg/apache/lucene/index/SnapshotDeletionPolicy;->segmentsFileToIDs:Ljava/util/Map;
    invoke-static {v0}, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->access$0(Lorg/apache/lucene/index/SnapshotDeletionPolicy;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 87
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SnapshotDeletionPolicy.SnapshotCommitPoint("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotCommitPoint;->cp:Lorg/apache/lucene/index/IndexCommit;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
