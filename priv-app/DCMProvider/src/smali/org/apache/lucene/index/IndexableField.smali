.class public interface abstract Lorg/apache/lucene/index/IndexableField;
.super Ljava/lang/Object;
.source "IndexableField.java"


# virtual methods
.method public abstract binaryValue()Lorg/apache/lucene/util/BytesRef;
.end method

.method public abstract boost()F
.end method

.method public abstract fieldType()Lorg/apache/lucene/index/IndexableFieldType;
.end method

.method public abstract name()Ljava/lang/String;
.end method

.method public abstract numericValue()Ljava/lang/Number;
.end method

.method public abstract readerValue()Ljava/io/Reader;
.end method

.method public abstract stringValue()Ljava/lang/String;
.end method

.method public abstract tokenStream(Lorg/apache/lucene/analysis/Analyzer;)Lorg/apache/lucene/analysis/TokenStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
