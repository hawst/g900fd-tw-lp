.class public Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;
.super Ljava/lang/Thread;
.source "ConcurrentMergeScheduler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/ConcurrentMergeScheduler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "MergeThread"
.end annotation


# instance fields
.field private volatile done:Z

.field runningMerge:Lorg/apache/lucene/index/MergePolicy$OneMerge;

.field startMerge:Lorg/apache/lucene/index/MergePolicy$OneMerge;

.field tWriter:Lorg/apache/lucene/index/IndexWriter;

.field final synthetic this$0:Lorg/apache/lucene/index/ConcurrentMergeScheduler;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/ConcurrentMergeScheduler;Lorg/apache/lucene/index/IndexWriter;Lorg/apache/lucene/index/MergePolicy$OneMerge;)V
    .locals 0
    .param p2, "writer"    # Lorg/apache/lucene/index/IndexWriter;
    .param p3, "startMerge"    # Lorg/apache/lucene/index/MergePolicy$OneMerge;

    .prologue
    .line 423
    iput-object p1, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->this$0:Lorg/apache/lucene/index/ConcurrentMergeScheduler;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 424
    iput-object p2, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->tWriter:Lorg/apache/lucene/index/IndexWriter;

    .line 425
    iput-object p3, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->startMerge:Lorg/apache/lucene/index/MergePolicy$OneMerge;

    .line 426
    return-void
.end method


# virtual methods
.method public declared-synchronized getCurrentMerge()Lorg/apache/lucene/index/MergePolicy$OneMerge;
    .locals 1

    .prologue
    .line 441
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->done:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 442
    const/4 v0, 0x0

    .line 446
    :goto_0
    monitor-exit p0

    return-object v0

    .line 443
    :cond_0
    :try_start_1
    iget-object v0, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->runningMerge:Lorg/apache/lucene/index/MergePolicy$OneMerge;

    if-eqz v0, :cond_1

    .line 444
    iget-object v0, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->runningMerge:Lorg/apache/lucene/index/MergePolicy$OneMerge;

    goto :goto_0

    .line 446
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->startMerge:Lorg/apache/lucene/index/MergePolicy$OneMerge;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 441
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getRunningMerge()Lorg/apache/lucene/index/MergePolicy$OneMerge;
    .locals 1

    .prologue
    .line 435
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->runningMerge:Lorg/apache/lucene/index/MergePolicy$OneMerge;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 468
    iget-object v1, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->startMerge:Lorg/apache/lucene/index/MergePolicy$OneMerge;

    .line 472
    .local v1, "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    :try_start_0
    iget-object v2, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->this$0:Lorg/apache/lucene/index/ConcurrentMergeScheduler;

    invoke-virtual {v2}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->verbose()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 473
    iget-object v2, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->this$0:Lorg/apache/lucene/index/ConcurrentMergeScheduler;

    const-string v3, "  merge thread: start"

    invoke-virtual {v2, v3}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->message(Ljava/lang/String;)V

    .line 477
    :cond_0
    :goto_0
    invoke-virtual {p0, v1}, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->setRunningMerge(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    .line 478
    iget-object v2, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->this$0:Lorg/apache/lucene/index/ConcurrentMergeScheduler;

    invoke-virtual {v2, v1}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->doMerge(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    .line 482
    iget-object v2, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->tWriter:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v2}, Lorg/apache/lucene/index/IndexWriter;->getNextMerge()Lorg/apache/lucene/index/MergePolicy$OneMerge;

    move-result-object v1

    .line 487
    iget-object v3, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->this$0:Lorg/apache/lucene/index/ConcurrentMergeScheduler;

    monitor-enter v3
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 488
    :try_start_1
    iget-object v2, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->this$0:Lorg/apache/lucene/index/ConcurrentMergeScheduler;

    invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V

    .line 487
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 491
    if-eqz v1, :cond_2

    .line 492
    :try_start_2
    iget-object v2, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->this$0:Lorg/apache/lucene/index/ConcurrentMergeScheduler;

    invoke-virtual {v2}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->updateMergeThreads()V

    .line 493
    iget-object v2, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->this$0:Lorg/apache/lucene/index/ConcurrentMergeScheduler;

    invoke-virtual {v2}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->verbose()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 494
    iget-object v2, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->this$0:Lorg/apache/lucene/index/ConcurrentMergeScheduler;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "  merge thread: do another merge "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->tWriter:Lorg/apache/lucene/index/IndexWriter;

    iget-object v5, v1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    invoke-virtual {v4, v5}, Lorg/apache/lucene/index/IndexWriter;->segString(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->message(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 505
    :catch_0
    move-exception v0

    .line 508
    .local v0, "exc":Ljava/lang/Throwable;
    :try_start_3
    instance-of v2, v0, Lorg/apache/lucene/index/MergePolicy$MergeAbortedException;

    if-nez v2, :cond_1

    .line 511
    iget-object v2, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->this$0:Lorg/apache/lucene/index/ConcurrentMergeScheduler;

    # getter for: Lorg/apache/lucene/index/ConcurrentMergeScheduler;->suppressExceptions:Z
    invoke-static {v2}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->access$0(Lorg/apache/lucene/index/ConcurrentMergeScheduler;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 514
    iget-object v2, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->this$0:Lorg/apache/lucene/index/ConcurrentMergeScheduler;

    invoke-virtual {v2, v0}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->handleMergeException(Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 518
    :cond_1
    iput-boolean v6, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->done:Z

    .line 519
    iget-object v3, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->this$0:Lorg/apache/lucene/index/ConcurrentMergeScheduler;

    monitor-enter v3

    .line 520
    :try_start_4
    iget-object v2, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->this$0:Lorg/apache/lucene/index/ConcurrentMergeScheduler;

    invoke-virtual {v2}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->updateMergeThreads()V

    .line 521
    iget-object v2, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->this$0:Lorg/apache/lucene/index/ConcurrentMergeScheduler;

    invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V

    .line 519
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    .line 524
    .end local v0    # "exc":Ljava/lang/Throwable;
    :goto_1
    return-void

    .line 487
    :catchall_0
    move-exception v2

    :try_start_5
    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v2
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 517
    :catchall_1
    move-exception v2

    .line 518
    iput-boolean v6, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->done:Z

    .line 519
    iget-object v3, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->this$0:Lorg/apache/lucene/index/ConcurrentMergeScheduler;

    monitor-enter v3

    .line 520
    :try_start_7
    iget-object v4, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->this$0:Lorg/apache/lucene/index/ConcurrentMergeScheduler;

    invoke-virtual {v4}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->updateMergeThreads()V

    .line 521
    iget-object v4, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->this$0:Lorg/apache/lucene/index/ConcurrentMergeScheduler;

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    .line 519
    monitor-exit v3
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    .line 523
    throw v2

    .line 501
    :cond_2
    :try_start_8
    iget-object v2, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->this$0:Lorg/apache/lucene/index/ConcurrentMergeScheduler;

    invoke-virtual {v2}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->verbose()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 502
    iget-object v2, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->this$0:Lorg/apache/lucene/index/ConcurrentMergeScheduler;

    const-string v3, "  merge thread: done"

    invoke-virtual {v2, v3}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->message(Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 518
    :cond_3
    iput-boolean v6, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->done:Z

    .line 519
    iget-object v3, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->this$0:Lorg/apache/lucene/index/ConcurrentMergeScheduler;

    monitor-enter v3

    .line 520
    :try_start_9
    iget-object v2, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->this$0:Lorg/apache/lucene/index/ConcurrentMergeScheduler;

    invoke-virtual {v2}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->updateMergeThreads()V

    .line 521
    iget-object v2, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->this$0:Lorg/apache/lucene/index/ConcurrentMergeScheduler;

    invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V

    .line 519
    monitor-exit v3

    goto :goto_1

    :catchall_2
    move-exception v2

    monitor-exit v3
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    throw v2

    .restart local v0    # "exc":Ljava/lang/Throwable;
    :catchall_3
    move-exception v2

    :try_start_a
    monitor-exit v3
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    throw v2

    .end local v0    # "exc":Ljava/lang/Throwable;
    :catchall_4
    move-exception v2

    :try_start_b
    monitor-exit v3
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_4

    throw v2
.end method

.method public declared-synchronized setRunningMerge(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V
    .locals 1
    .param p1, "merge"    # Lorg/apache/lucene/index/MergePolicy$OneMerge;

    .prologue
    .line 430
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->runningMerge:Lorg/apache/lucene/index/MergePolicy$OneMerge;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 431
    monitor-exit p0

    return-void

    .line 430
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setThreadPriority(I)V
    .locals 1
    .param p1, "pri"    # I

    .prologue
    .line 453
    :try_start_0
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->setPriority(I)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    .line 461
    :goto_0
    return-void

    .line 454
    :catch_0
    move-exception v0

    goto :goto_0

    .line 457
    :catch_1
    move-exception v0

    goto :goto_0
.end method
