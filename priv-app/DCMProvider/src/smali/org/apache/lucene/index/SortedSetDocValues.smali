.class public abstract Lorg/apache/lucene/index/SortedSetDocValues;
.super Ljava/lang/Object;
.source "SortedSetDocValues.java"


# static fields
.field public static final EMPTY:Lorg/apache/lucene/index/SortedSetDocValues;

.field public static final NO_MORE_ORDS:J = -0x1L


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 72
    new-instance v0, Lorg/apache/lucene/index/SortedSetDocValues$1;

    invoke-direct {v0}, Lorg/apache/lucene/index/SortedSetDocValues$1;-><init>()V

    sput-object v0, Lorg/apache/lucene/index/SortedSetDocValues;->EMPTY:Lorg/apache/lucene/index/SortedSetDocValues;

    .line 91
    return-void
.end method

.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getValueCount()J
.end method

.method public abstract lookupOrd(JLorg/apache/lucene/util/BytesRef;)V
.end method

.method public lookupTerm(Lorg/apache/lucene/util/BytesRef;)J
    .locals 14
    .param p1, "key"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    const-wide/16 v12, 0x1

    .line 100
    new-instance v1, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v1}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    .line 101
    .local v1, "spare":Lorg/apache/lucene/util/BytesRef;
    const-wide/16 v4, 0x0

    .line 102
    .local v4, "low":J
    invoke-virtual {p0}, Lorg/apache/lucene/index/SortedSetDocValues;->getValueCount()J

    move-result-wide v8

    sub-long v2, v8, v12

    .line 104
    .local v2, "high":J
    :goto_0
    cmp-long v8, v4, v2

    if-lez v8, :cond_1

    .line 118
    add-long v8, v4, v12

    neg-long v6, v8

    :cond_0
    return-wide v6

    .line 105
    :cond_1
    add-long v8, v4, v2

    const/4 v10, 0x1

    ushr-long v6, v8, v10

    .line 106
    .local v6, "mid":J
    invoke-virtual {p0, v6, v7, v1}, Lorg/apache/lucene/index/SortedSetDocValues;->lookupOrd(JLorg/apache/lucene/util/BytesRef;)V

    .line 107
    invoke-virtual {v1, p1}, Lorg/apache/lucene/util/BytesRef;->compareTo(Lorg/apache/lucene/util/BytesRef;)I

    move-result v0

    .line 109
    .local v0, "cmp":I
    if-gez v0, :cond_2

    .line 110
    add-long v4, v6, v12

    .line 111
    goto :goto_0

    :cond_2
    if-lez v0, :cond_0

    .line 112
    sub-long v2, v6, v12

    .line 113
    goto :goto_0
.end method

.method public abstract nextOrd()J
.end method

.method public abstract setDocument(I)V
.end method

.method public termsEnum()Lorg/apache/lucene/index/TermsEnum;
    .locals 1

    .prologue
    .line 126
    new-instance v0, Lorg/apache/lucene/index/SortedSetDocValuesTermsEnum;

    invoke-direct {v0, p0}, Lorg/apache/lucene/index/SortedSetDocValuesTermsEnum;-><init>(Lorg/apache/lucene/index/SortedSetDocValues;)V

    return-object v0
.end method
