.class final Lorg/apache/lucene/index/BitsSlice;
.super Ljava/lang/Object;
.source "BitsSlice.java"

# interfaces
.implements Lorg/apache/lucene/util/Bits;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final length:I

.field private final parent:Lorg/apache/lucene/util/Bits;

.field private final start:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lorg/apache/lucene/index/BitsSlice;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/BitsSlice;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/ReaderSlice;)V
    .locals 3
    .param p1, "parent"    # Lorg/apache/lucene/util/Bits;
    .param p2, "slice"    # Lorg/apache/lucene/index/ReaderSlice;

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lorg/apache/lucene/index/BitsSlice;->parent:Lorg/apache/lucene/util/Bits;

    .line 35
    iget v0, p2, Lorg/apache/lucene/index/ReaderSlice;->start:I

    iput v0, p0, Lorg/apache/lucene/index/BitsSlice;->start:I

    .line 36
    iget v0, p2, Lorg/apache/lucene/index/ReaderSlice;->length:I

    iput v0, p0, Lorg/apache/lucene/index/BitsSlice;->length:I

    .line 37
    sget-boolean v0, Lorg/apache/lucene/index/BitsSlice;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/index/BitsSlice;->length:I

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "length="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/lucene/index/BitsSlice;->length:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 38
    :cond_0
    return-void
.end method


# virtual methods
.method public get(I)Z
    .locals 3
    .param p1, "doc"    # I

    .prologue
    .line 42
    iget v0, p0, Lorg/apache/lucene/index/BitsSlice;->length:I

    if-lt p1, v0, :cond_0

    .line 43
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "doc "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is out of bounds 0 .. "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/index/BitsSlice;->length:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 45
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/index/BitsSlice;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget v0, p0, Lorg/apache/lucene/index/BitsSlice;->length:I

    if-lt p1, v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "doc="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " length="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/index/BitsSlice;->length:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 46
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/index/BitsSlice;->parent:Lorg/apache/lucene/util/Bits;

    iget v1, p0, Lorg/apache/lucene/index/BitsSlice;->start:I

    add-int/2addr v1, p1

    invoke-interface {v0, v1}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v0

    return v0
.end method

.method public length()I
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lorg/apache/lucene/index/BitsSlice;->length:I

    return v0
.end method
