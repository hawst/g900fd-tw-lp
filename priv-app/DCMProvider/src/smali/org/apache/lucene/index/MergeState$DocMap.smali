.class public abstract Lorg/apache/lucene/index/MergeState$DocMap;
.super Ljava/lang/Object;
.source "MergeState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/MergeState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "DocMap"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lorg/apache/lucene/index/MergeState;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/MergeState$DocMap;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static build(ILorg/apache/lucene/util/Bits;)Lorg/apache/lucene/index/MergeState$DocMap;
    .locals 8
    .param p0, "maxDoc"    # I
    .param p1, "liveDocs"    # Lorg/apache/lucene/util/Bits;

    .prologue
    .line 71
    sget-boolean v4, Lorg/apache/lucene/index/MergeState$DocMap;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    if-nez p1, :cond_0

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 72
    :cond_0
    new-instance v1, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;

    invoke-direct {v1}, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;-><init>()V

    .line 73
    .local v1, "docMap":Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;
    const/4 v0, 0x0

    .line 74
    .local v0, "del":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-lt v2, p0, :cond_1

    .line 80
    move v3, v0

    .line 81
    .local v3, "numDeletedDocs":I
    sget-boolean v4, Lorg/apache/lucene/index/MergeState$DocMap;->$assertionsDisabled:Z

    if-nez v4, :cond_3

    invoke-virtual {v1}, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;->size()J

    move-result-wide v4

    int-to-long v6, p0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_3

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 75
    .end local v3    # "numDeletedDocs":I
    :cond_1
    sub-int v4, v2, v0

    int-to-long v4, v4

    invoke-virtual {v1, v4, v5}, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;->add(J)V

    .line 76
    invoke-interface {p1, v2}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v4

    if-nez v4, :cond_2

    .line 77
    add-int/lit8 v0, v0, 0x1

    .line 74
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 82
    .restart local v3    # "numDeletedDocs":I
    :cond_3
    new-instance v4, Lorg/apache/lucene/index/MergeState$DocMap$1;

    invoke-direct {v4, p1, v1, p0, v3}, Lorg/apache/lucene/index/MergeState$DocMap$1;-><init>(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;II)V

    return-object v4
.end method

.method public static build(Lorg/apache/lucene/index/AtomicReader;)Lorg/apache/lucene/index/MergeState$DocMap;
    .locals 4
    .param p0, "reader"    # Lorg/apache/lucene/index/AtomicReader;

    .prologue
    .line 62
    invoke-virtual {p0}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v1

    .line 63
    .local v1, "maxDoc":I
    invoke-virtual {p0}, Lorg/apache/lucene/index/AtomicReader;->hasDeletions()Z

    move-result v2

    if-nez v2, :cond_0

    .line 64
    new-instance v2, Lorg/apache/lucene/index/MergeState$NoDelDocMap;

    const/4 v3, 0x0

    invoke-direct {v2, v1, v3}, Lorg/apache/lucene/index/MergeState$NoDelDocMap;-><init>(ILorg/apache/lucene/index/MergeState$NoDelDocMap;)V

    .line 67
    :goto_0
    return-object v2

    .line 66
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/index/AtomicReader;->getLiveDocs()Lorg/apache/lucene/util/Bits;

    move-result-object v0

    .line 67
    .local v0, "liveDocs":Lorg/apache/lucene/util/Bits;
    invoke-static {v1, v0}, Lorg/apache/lucene/index/MergeState$DocMap;->build(ILorg/apache/lucene/util/Bits;)Lorg/apache/lucene/index/MergeState$DocMap;

    move-result-object v2

    goto :goto_0
.end method


# virtual methods
.method public abstract get(I)I
.end method

.method public hasDeletions()Z
    .locals 1

    .prologue
    .line 56
    invoke-virtual {p0}, Lorg/apache/lucene/index/MergeState$DocMap;->numDeletedDocs()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract maxDoc()I
.end method

.method public abstract numDeletedDocs()I
.end method

.method public final numDocs()I
    .locals 2

    .prologue
    .line 48
    invoke-virtual {p0}, Lorg/apache/lucene/index/MergeState$DocMap;->maxDoc()I

    move-result v0

    invoke-virtual {p0}, Lorg/apache/lucene/index/MergeState$DocMap;->numDeletedDocs()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method
