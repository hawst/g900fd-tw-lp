.class public final Lorg/apache/lucene/index/CompositeReaderContext;
.super Lorg/apache/lucene/index/IndexReaderContext;
.source "CompositeReaderContext.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/CompositeReaderContext$Builder;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final children:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/IndexReaderContext;",
            ">;"
        }
    .end annotation
.end field

.field private final leaves:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/AtomicReaderContext;",
            ">;"
        }
    .end annotation
.end field

.field private final reader:Lorg/apache/lucene/index/CompositeReader;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lorg/apache/lucene/index/CompositeReaderContext;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/CompositeReaderContext;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/index/CompositeReader;Ljava/util/List;Ljava/util/List;)V
    .locals 7
    .param p1, "reader"    # Lorg/apache/lucene/index/CompositeReader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/CompositeReader;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/IndexReaderContext;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/AtomicReaderContext;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/IndexReaderContext;>;"
    .local p3, "leaves":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/AtomicReaderContext;>;"
    const/4 v3, 0x0

    .line 50
    const/4 v1, 0x0

    move-object v0, p0

    move-object v2, p1

    move v4, v3

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/index/CompositeReaderContext;-><init>(Lorg/apache/lucene/index/CompositeReaderContext;Lorg/apache/lucene/index/CompositeReader;IILjava/util/List;Ljava/util/List;)V

    .line 51
    return-void
.end method

.method constructor <init>(Lorg/apache/lucene/index/CompositeReaderContext;Lorg/apache/lucene/index/CompositeReader;IILjava/util/List;)V
    .locals 7
    .param p1, "parent"    # Lorg/apache/lucene/index/CompositeReaderContext;
    .param p2, "reader"    # Lorg/apache/lucene/index/CompositeReader;
    .param p3, "ordInParent"    # I
    .param p4, "docbaseInParent"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/CompositeReaderContext;",
            "Lorg/apache/lucene/index/CompositeReader;",
            "II",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/IndexReaderContext;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 43
    .local p5, "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/IndexReaderContext;>;"
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/index/CompositeReaderContext;-><init>(Lorg/apache/lucene/index/CompositeReaderContext;Lorg/apache/lucene/index/CompositeReader;IILjava/util/List;Ljava/util/List;)V

    .line 44
    return-void
.end method

.method private constructor <init>(Lorg/apache/lucene/index/CompositeReaderContext;Lorg/apache/lucene/index/CompositeReader;IILjava/util/List;Ljava/util/List;)V
    .locals 1
    .param p1, "parent"    # Lorg/apache/lucene/index/CompositeReaderContext;
    .param p2, "reader"    # Lorg/apache/lucene/index/CompositeReader;
    .param p3, "ordInParent"    # I
    .param p4, "docbaseInParent"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/CompositeReaderContext;",
            "Lorg/apache/lucene/index/CompositeReader;",
            "II",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/IndexReaderContext;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/AtomicReaderContext;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 56
    .local p5, "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/IndexReaderContext;>;"
    .local p6, "leaves":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/AtomicReaderContext;>;"
    invoke-direct {p0, p1, p3, p4}, Lorg/apache/lucene/index/IndexReaderContext;-><init>(Lorg/apache/lucene/index/CompositeReaderContext;II)V

    .line 57
    invoke-static {p5}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/CompositeReaderContext;->children:Ljava/util/List;

    .line 58
    if-nez p6, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lorg/apache/lucene/index/CompositeReaderContext;->leaves:Ljava/util/List;

    .line 59
    iput-object p2, p0, Lorg/apache/lucene/index/CompositeReaderContext;->reader:Lorg/apache/lucene/index/CompositeReader;

    .line 60
    return-void

    .line 58
    :cond_0
    invoke-static {p6}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method static create(Lorg/apache/lucene/index/CompositeReader;)Lorg/apache/lucene/index/CompositeReaderContext;
    .locals 1
    .param p0, "reader"    # Lorg/apache/lucene/index/CompositeReader;

    .prologue
    .line 34
    new-instance v0, Lorg/apache/lucene/index/CompositeReaderContext$Builder;

    invoke-direct {v0, p0}, Lorg/apache/lucene/index/CompositeReaderContext$Builder;-><init>(Lorg/apache/lucene/index/CompositeReader;)V

    invoke-virtual {v0}, Lorg/apache/lucene/index/CompositeReaderContext$Builder;->build()Lorg/apache/lucene/index/CompositeReaderContext;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public children()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/IndexReaderContext;",
            ">;"
        }
    .end annotation

    .prologue
    .line 73
    iget-object v0, p0, Lorg/apache/lucene/index/CompositeReaderContext;->children:Ljava/util/List;

    return-object v0
.end method

.method public leaves()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/AtomicReaderContext;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 64
    iget-boolean v0, p0, Lorg/apache/lucene/index/CompositeReaderContext;->isTopLevel:Z

    if-nez v0, :cond_0

    .line 65
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This is not a top-level context."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 66
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/index/CompositeReaderContext;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/index/CompositeReaderContext;->leaves:Ljava/util/List;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 67
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/index/CompositeReaderContext;->leaves:Ljava/util/List;

    return-object v0
.end method

.method public reader()Lorg/apache/lucene/index/CompositeReader;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lorg/apache/lucene/index/CompositeReaderContext;->reader:Lorg/apache/lucene/index/CompositeReader;

    return-object v0
.end method

.method public bridge synthetic reader()Lorg/apache/lucene/index/IndexReader;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/index/CompositeReaderContext;->reader()Lorg/apache/lucene/index/CompositeReader;

    move-result-object v0

    return-object v0
.end method
