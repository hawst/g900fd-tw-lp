.class public final Lorg/apache/lucene/index/TermContext;
.super Ljava/lang/Object;
.source "TermContext.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private docFreq:I

.field private final states:[Lorg/apache/lucene/index/TermState;

.field public final topReaderContext:Lorg/apache/lucene/index/IndexReaderContext;

.field private totalTermFreq:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lorg/apache/lucene/index/TermContext;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/TermContext;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/index/IndexReaderContext;)V
    .locals 2
    .param p1, "context"    # Lorg/apache/lucene/index/IndexReaderContext;

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    sget-boolean v1, Lorg/apache/lucene/index/TermContext;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    if-eqz p1, :cond_0

    iget-boolean v1, p1, Lorg/apache/lucene/index/IndexReaderContext;->isTopLevel:Z

    if-nez v1, :cond_1

    :cond_0
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 53
    :cond_1
    iput-object p1, p0, Lorg/apache/lucene/index/TermContext;->topReaderContext:Lorg/apache/lucene/index/IndexReaderContext;

    .line 54
    const/4 v1, 0x0

    iput v1, p0, Lorg/apache/lucene/index/TermContext;->docFreq:I

    .line 56
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReaderContext;->leaves()Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_2

    .line 57
    const/4 v0, 0x1

    .line 61
    .local v0, "len":I
    :goto_0
    new-array v1, v0, [Lorg/apache/lucene/index/TermState;

    iput-object v1, p0, Lorg/apache/lucene/index/TermContext;->states:[Lorg/apache/lucene/index/TermState;

    .line 62
    return-void

    .line 59
    .end local v0    # "len":I
    :cond_2
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReaderContext;->leaves()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    .restart local v0    # "len":I
    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/index/IndexReaderContext;Lorg/apache/lucene/index/TermState;IIJ)V
    .locals 7
    .param p1, "context"    # Lorg/apache/lucene/index/IndexReaderContext;
    .param p2, "state"    # Lorg/apache/lucene/index/TermState;
    .param p3, "ord"    # I
    .param p4, "docFreq"    # I
    .param p5, "totalTermFreq"    # J

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/TermContext;-><init>(Lorg/apache/lucene/index/IndexReaderContext;)V

    move-object v0, p0

    move-object v1, p2

    move v2, p3

    move v3, p4

    move-wide v4, p5

    .line 70
    invoke-virtual/range {v0 .. v5}, Lorg/apache/lucene/index/TermContext;->register(Lorg/apache/lucene/index/TermState;IIJ)V

    .line 71
    return-void
.end method

.method public static build(Lorg/apache/lucene/index/IndexReaderContext;Lorg/apache/lucene/index/Term;Z)Lorg/apache/lucene/index/TermContext;
    .locals 13
    .param p0, "context"    # Lorg/apache/lucene/index/IndexReaderContext;
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .param p2, "cache"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 83
    sget-boolean v2, Lorg/apache/lucene/index/TermContext;->$assertionsDisabled:Z

    if-nez v2, :cond_1

    if-eqz p0, :cond_0

    iget-boolean v2, p0, Lorg/apache/lucene/index/IndexReaderContext;->isTopLevel:Z

    if-nez v2, :cond_1

    :cond_0
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 84
    :cond_1
    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v8

    .line 85
    .local v8, "field":Ljava/lang/String;
    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->bytes()Lorg/apache/lucene/util/BytesRef;

    move-result-object v6

    .line 86
    .local v6, "bytes":Lorg/apache/lucene/util/BytesRef;
    new-instance v0, Lorg/apache/lucene/index/TermContext;

    invoke-direct {v0, p0}, Lorg/apache/lucene/index/TermContext;-><init>(Lorg/apache/lucene/index/IndexReaderContext;)V

    .line 88
    .local v0, "perReaderTermState":Lorg/apache/lucene/index/TermContext;
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReaderContext;->leaves()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_2
    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_3

    .line 103
    return-object v0

    .line 88
    :cond_3
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/lucene/index/AtomicReaderContext;

    .line 90
    .local v7, "ctx":Lorg/apache/lucene/index/AtomicReaderContext;
    invoke-virtual {v7}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/lucene/index/AtomicReader;->fields()Lorg/apache/lucene/index/Fields;

    move-result-object v9

    .line 91
    .local v9, "fields":Lorg/apache/lucene/index/Fields;
    if-eqz v9, :cond_2

    .line 92
    invoke-virtual {v9, v8}, Lorg/apache/lucene/index/Fields;->terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v10

    .line 93
    .local v10, "terms":Lorg/apache/lucene/index/Terms;
    if-eqz v10, :cond_2

    .line 94
    const/4 v2, 0x0

    invoke-virtual {v10, v2}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v11

    .line 95
    .local v11, "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    invoke-virtual {v11, v6, p2}, Lorg/apache/lucene/index/TermsEnum;->seekExact(Lorg/apache/lucene/util/BytesRef;Z)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 96
    invoke-virtual {v11}, Lorg/apache/lucene/index/TermsEnum;->termState()Lorg/apache/lucene/index/TermState;

    move-result-object v1

    .line 98
    .local v1, "termState":Lorg/apache/lucene/index/TermState;
    iget v2, v7, Lorg/apache/lucene/index/AtomicReaderContext;->ord:I

    invoke-virtual {v11}, Lorg/apache/lucene/index/TermsEnum;->docFreq()I

    move-result v3

    invoke-virtual {v11}, Lorg/apache/lucene/index/TermsEnum;->totalTermFreq()J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, Lorg/apache/lucene/index/TermContext;->register(Lorg/apache/lucene/index/TermState;IIJ)V

    goto :goto_0
.end method


# virtual methods
.method public clear()V
    .locals 2

    .prologue
    .line 111
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/index/TermContext;->docFreq:I

    .line 112
    iget-object v0, p0, Lorg/apache/lucene/index/TermContext;->states:[Lorg/apache/lucene/index/TermState;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 113
    return-void
.end method

.method public docFreq()I
    .locals 1

    .prologue
    .line 153
    iget v0, p0, Lorg/apache/lucene/index/TermContext;->docFreq:I

    return v0
.end method

.method public get(I)Lorg/apache/lucene/index/TermState;
    .locals 1
    .param p1, "ord"    # I

    .prologue
    .line 142
    sget-boolean v0, Lorg/apache/lucene/index/TermContext;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-ltz p1, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/TermContext;->states:[Lorg/apache/lucene/index/TermState;

    array-length v0, v0

    if-lt p1, v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 143
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/index/TermContext;->states:[Lorg/apache/lucene/index/TermState;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public register(Lorg/apache/lucene/index/TermState;IIJ)V
    .locals 4
    .param p1, "state"    # Lorg/apache/lucene/index/TermState;
    .param p2, "ord"    # I
    .param p3, "docFreq"    # I
    .param p4, "totalTermFreq"    # J

    .prologue
    const-wide/16 v2, 0x0

    .line 120
    sget-boolean v0, Lorg/apache/lucene/index/TermContext;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "state must not be null"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 121
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/index/TermContext;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    if-ltz p2, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/index/TermContext;->states:[Lorg/apache/lucene/index/TermState;

    array-length v0, v0

    if-lt p2, v0, :cond_2

    :cond_1
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 122
    :cond_2
    sget-boolean v0, Lorg/apache/lucene/index/TermContext;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lorg/apache/lucene/index/TermContext;->states:[Lorg/apache/lucene/index/TermState;

    aget-object v0, v0, p2

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "state for ord: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 123
    const-string v2, " already registered"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 124
    :cond_3
    iget v0, p0, Lorg/apache/lucene/index/TermContext;->docFreq:I

    add-int/2addr v0, p3

    iput v0, p0, Lorg/apache/lucene/index/TermContext;->docFreq:I

    .line 125
    iget-wide v0, p0, Lorg/apache/lucene/index/TermContext;->totalTermFreq:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_4

    cmp-long v0, p4, v2

    if-ltz v0, :cond_4

    .line 126
    iget-wide v0, p0, Lorg/apache/lucene/index/TermContext;->totalTermFreq:J

    add-long/2addr v0, p4

    iput-wide v0, p0, Lorg/apache/lucene/index/TermContext;->totalTermFreq:J

    .line 129
    :goto_0
    iget-object v0, p0, Lorg/apache/lucene/index/TermContext;->states:[Lorg/apache/lucene/index/TermState;

    aput-object p1, v0, p2

    .line 130
    return-void

    .line 128
    :cond_4
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lorg/apache/lucene/index/TermContext;->totalTermFreq:J

    goto :goto_0
.end method

.method public setDocFreq(I)V
    .locals 0
    .param p1, "docFreq"    # I

    .prologue
    .line 169
    iput p1, p0, Lorg/apache/lucene/index/TermContext;->docFreq:I

    .line 170
    return-void
.end method

.method public totalTermFreq()J
    .locals 2

    .prologue
    .line 163
    iget-wide v0, p0, Lorg/apache/lucene/index/TermContext;->totalTermFreq:J

    return-wide v0
.end method
