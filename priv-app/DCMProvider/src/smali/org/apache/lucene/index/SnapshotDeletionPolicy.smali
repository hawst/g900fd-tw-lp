.class public Lorg/apache/lucene/index/SnapshotDeletionPolicy;
.super Lorg/apache/lucene/index/IndexDeletionPolicy;
.source "SnapshotDeletionPolicy.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotCommitPoint;,
        Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;
    }
.end annotation


# instance fields
.field private idToSnapshot:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;",
            ">;"
        }
    .end annotation
.end field

.field protected lastCommit:Lorg/apache/lucene/index/IndexCommit;

.field private primary:Lorg/apache/lucene/index/IndexDeletionPolicy;

.field private segmentsFileToIDs:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/IndexDeletionPolicy;)V
    .locals 1
    .param p1, "primary"    # Lorg/apache/lucene/index/IndexDeletionPolicy;

    .prologue
    .line 158
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexDeletionPolicy;-><init>()V

    .line 146
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->idToSnapshot:Ljava/util/Map;

    .line 149
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->segmentsFileToIDs:Ljava/util/Map;

    .line 159
    iput-object p1, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->primary:Lorg/apache/lucene/index/IndexDeletionPolicy;

    .line 160
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/index/IndexDeletionPolicy;Ljava/util/Map;)V
    .locals 5
    .param p1, "primary"    # Lorg/apache/lucene/index/IndexDeletionPolicy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/IndexDeletionPolicy;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 178
    .local p2, "snapshotsInfo":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/SnapshotDeletionPolicy;-><init>(Lorg/apache/lucene/index/IndexDeletionPolicy;)V

    .line 180
    if-eqz p2, :cond_0

    .line 183
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_1

    .line 187
    :cond_0
    return-void

    .line 183
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 184
    .local v0, "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {p0, v1, v2, v4}, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->registerSnapshotInfo(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/index/IndexCommit;)V

    goto :goto_0
.end method

.method static synthetic access$0(Lorg/apache/lucene/index/SnapshotDeletionPolicy;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->segmentsFileToIDs:Ljava/util/Map;

    return-object v0
.end method


# virtual methods
.method protected checkSnapshotted(Ljava/lang/String;)V
    .locals 3
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 194
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->isSnapshotted(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 195
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Snapshot ID "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 196
    const-string v2, " is already used - must be unique"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 195
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 198
    :cond_0
    return-void
.end method

.method public clone()Lorg/apache/lucene/index/IndexDeletionPolicy;
    .locals 3

    .prologue
    .line 378
    invoke-super {p0}, Lorg/apache/lucene/index/IndexDeletionPolicy;->clone()Lorg/apache/lucene/index/IndexDeletionPolicy;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;

    .line 379
    .local v0, "other":Lorg/apache/lucene/index/SnapshotDeletionPolicy;
    iget-object v1, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->primary:Lorg/apache/lucene/index/IndexDeletionPolicy;

    invoke-virtual {v1}, Lorg/apache/lucene/index/IndexDeletionPolicy;->clone()Lorg/apache/lucene/index/IndexDeletionPolicy;

    move-result-object v1

    iput-object v1, v0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->primary:Lorg/apache/lucene/index/IndexDeletionPolicy;

    .line 380
    const/4 v1, 0x0

    iput-object v1, v0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->lastCommit:Lorg/apache/lucene/index/IndexCommit;

    .line 381
    new-instance v1, Ljava/util/HashMap;

    iget-object v2, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->segmentsFileToIDs:Ljava/util/Map;

    invoke-direct {v1, v2}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object v1, v0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->segmentsFileToIDs:Ljava/util/Map;

    .line 382
    new-instance v1, Ljava/util/HashMap;

    iget-object v2, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->idToSnapshot:Ljava/util/Map;

    invoke-direct {v1, v2}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object v1, v0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->idToSnapshot:Ljava/util/Map;

    .line 383
    return-object v0
.end method

.method public declared-synchronized getSnapshot(Ljava/lang/String;)Lorg/apache/lucene/index/IndexCommit;
    .locals 4
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 234
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->idToSnapshot:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;

    .line 235
    .local v0, "snapshotInfo":Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;
    if-nez v0, :cond_0

    .line 236
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No snapshot exists by ID: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 234
    .end local v0    # "snapshotInfo":Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 238
    .restart local v0    # "snapshotInfo":Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;
    :cond_0
    :try_start_1
    iget-object v1, v0, Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;->commit:Lorg/apache/lucene/index/IndexCommit;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v1
.end method

.method public declared-synchronized getSnapshots()Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 248
    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 249
    .local v1, "snapshots":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v2, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->idToSnapshot:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    .line 252
    monitor-exit p0

    return-object v1

    .line 249
    :cond_0
    :try_start_1
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 250
    .local v0, "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;

    iget-object v3, v3, Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;->segmentsFileName:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 248
    .end local v0    # "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;>;"
    .end local v1    # "snapshots":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public isSnapshotted(Ljava/lang/String;)Z
    .locals 1
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 261
    iget-object v0, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->idToSnapshot:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public declared-synchronized onCommit(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lorg/apache/lucene/index/IndexCommit;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 267
    .local p1, "commits":Ljava/util/List;, "Ljava/util/List<+Lorg/apache/lucene/index/IndexCommit;>;"
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->primary:Lorg/apache/lucene/index/IndexDeletionPolicy;

    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->wrapCommits(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/IndexDeletionPolicy;->onCommit(Ljava/util/List;)V

    .line 268
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/IndexCommit;

    iput-object v0, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->lastCommit:Lorg/apache/lucene/index/IndexCommit;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 269
    monitor-exit p0

    return-void

    .line 267
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onInit(Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lorg/apache/lucene/index/IndexCommit;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 274
    .local p1, "commits":Ljava/util/List;, "Ljava/util/List<+Lorg/apache/lucene/index/IndexCommit;>;"
    monitor-enter p0

    :try_start_0
    iget-object v6, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->primary:Lorg/apache/lucene/index/IndexDeletionPolicy;

    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->wrapCommits(Ljava/util/List;)Ljava/util/List;

    move-result-object v7

    invoke-virtual {v6, v7}, Lorg/apache/lucene/index/IndexDeletionPolicy;->onInit(Ljava/util/List;)V

    .line 275
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/lucene/index/IndexCommit;

    iput-object v6, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->lastCommit:Lorg/apache/lucene/index/IndexCommit;

    .line 281
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_3

    .line 300
    const/4 v4, 0x0

    .line 301
    .local v4, "idsToRemove":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v6, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->idToSnapshot:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_1
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_4

    .line 310
    if-eqz v4, :cond_2

    .line 311
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    if-nez v7, :cond_6

    .line 316
    :cond_2
    monitor-exit p0

    return-void

    .line 281
    .end local v4    # "idsToRemove":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_3
    :try_start_1
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/IndexCommit;

    .line 282
    .local v0, "commit":Lorg/apache/lucene/index/IndexCommit;
    iget-object v6, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->segmentsFileToIDs:Ljava/util/Map;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexCommit;->getSegmentsFileName()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Set;

    .line 283
    .local v3, "ids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz v3, :cond_0

    .line 284
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 285
    .local v2, "id":Ljava/lang/String;
    iget-object v6, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->idToSnapshot:Ljava/util/Map;

    invoke-interface {v6, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;

    iput-object v0, v6, Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;->commit:Lorg/apache/lucene/index/IndexCommit;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 274
    .end local v0    # "commit":Lorg/apache/lucene/index/IndexCommit;
    .end local v2    # "id":Ljava/lang/String;
    .end local v3    # "ids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6

    .line 301
    .restart local v4    # "idsToRemove":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_4
    :try_start_2
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 302
    .local v1, "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;

    iget-object v6, v6, Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;->commit:Lorg/apache/lucene/index/IndexCommit;

    if-nez v6, :cond_1

    .line 303
    if-nez v4, :cond_5

    .line 304
    new-instance v4, Ljava/util/ArrayList;

    .end local v4    # "idsToRemove":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 306
    .restart local v4    # "idsToRemove":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_5
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 311
    .end local v1    # "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;>;"
    :cond_6
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 312
    .restart local v2    # "id":Ljava/lang/String;
    iget-object v7, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->idToSnapshot:Ljava/util/Map;

    invoke-interface {v7, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;

    .line 313
    .local v5, "info":Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;
    iget-object v7, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->segmentsFileToIDs:Ljava/util/Map;

    iget-object v8, v5, Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;->segmentsFileName:Ljava/lang/String;

    invoke-interface {v7, v8}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method protected registerSnapshotInfo(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/index/IndexCommit;)V
    .locals 3
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "segment"    # Ljava/lang/String;
    .param p3, "commit"    # Lorg/apache/lucene/index/IndexCommit;

    .prologue
    .line 202
    iget-object v1, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->idToSnapshot:Ljava/util/Map;

    new-instance v2, Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;

    invoke-direct {v2, p1, p2, p3}, Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/index/IndexCommit;)V

    invoke-interface {v1, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 203
    iget-object v1, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->segmentsFileToIDs:Ljava/util/Map;

    invoke-interface {v1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 204
    .local v0, "ids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-nez v0, :cond_0

    .line 205
    new-instance v0, Ljava/util/HashSet;

    .end local v0    # "ids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 206
    .restart local v0    # "ids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v1, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->segmentsFileToIDs:Ljava/util/Map;

    invoke-interface {v1, p2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208
    :cond_0
    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 209
    return-void
.end method

.method public declared-synchronized release(Ljava/lang/String;)V
    .locals 5
    .param p1, "id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 327
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->idToSnapshot:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;

    .line 328
    .local v1, "info":Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;
    if-nez v1, :cond_0

    .line 329
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Snapshot doesn\'t exist: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 327
    .end local v1    # "info":Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 331
    .restart local v1    # "info":Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;
    :cond_0
    :try_start_1
    iget-object v2, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->segmentsFileToIDs:Ljava/util/Map;

    iget-object v3, v1, Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;->segmentsFileName:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 332
    .local v0, "ids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz v0, :cond_1

    .line 333
    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 334
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v2

    if-nez v2, :cond_1

    .line 335
    iget-object v2, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->segmentsFileToIDs:Ljava/util/Map;

    iget-object v3, v1, Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;->segmentsFileName:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 338
    :cond_1
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized snapshot(Ljava/lang/String;)Lorg/apache/lucene/index/IndexCommit;
    .locals 2
    .param p1, "id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 363
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->lastCommit:Lorg/apache/lucene/index/IndexCommit;

    if-nez v0, :cond_0

    .line 366
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No index commit to snapshot"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 363
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 370
    :cond_0
    :try_start_1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->checkSnapshotted(Ljava/lang/String;)V

    .line 372
    iget-object v0, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->lastCommit:Lorg/apache/lucene/index/IndexCommit;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexCommit;->getSegmentsFileName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->lastCommit:Lorg/apache/lucene/index/IndexCommit;

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->registerSnapshotInfo(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/index/IndexCommit;)V

    .line 373
    iget-object v0, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->lastCommit:Lorg/apache/lucene/index/IndexCommit;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v0
.end method

.method protected wrapCommits(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lorg/apache/lucene/index/IndexCommit;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/IndexCommit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 214
    .local p1, "commits":Ljava/util/List;, "Ljava/util/List<+Lorg/apache/lucene/index/IndexCommit;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 215
    .local v1, "wrappedCommits":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/IndexCommit;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 218
    return-object v1

    .line 215
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/IndexCommit;

    .line 216
    .local v0, "ic":Lorg/apache/lucene/index/IndexCommit;
    new-instance v3, Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotCommitPoint;

    invoke-direct {v3, p0, v0}, Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotCommitPoint;-><init>(Lorg/apache/lucene/index/SnapshotDeletionPolicy;Lorg/apache/lucene/index/IndexCommit;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
