.class public abstract Lorg/apache/lucene/index/MergePolicy;
.super Ljava/lang/Object;
.source "MergePolicy.java"

# interfaces
.implements Ljava/io/Closeable;
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/MergePolicy$DocMap;,
        Lorg/apache/lucene/index/MergePolicy$MergeAbortedException;,
        Lorg/apache/lucene/index/MergePolicy$MergeException;,
        Lorg/apache/lucene/index/MergePolicy$MergeSpecification;,
        Lorg/apache/lucene/index/MergePolicy$MergeTrigger;,
        Lorg/apache/lucene/index/MergePolicy$OneMerge;
    }
.end annotation


# instance fields
.field protected writer:Lorg/apache/lucene/util/SetOnce;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/SetOnce",
            "<",
            "Lorg/apache/lucene/index/IndexWriter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 386
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 387
    new-instance v0, Lorg/apache/lucene/util/SetOnce;

    invoke-direct {v0}, Lorg/apache/lucene/util/SetOnce;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/MergePolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    .line 388
    return-void
.end method


# virtual methods
.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/index/MergePolicy;->clone()Lorg/apache/lucene/index/MergePolicy;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lorg/apache/lucene/index/MergePolicy;
    .locals 3

    .prologue
    .line 372
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/MergePolicy;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 377
    .local v0, "clone":Lorg/apache/lucene/index/MergePolicy;
    new-instance v2, Lorg/apache/lucene/util/SetOnce;

    invoke-direct {v2}, Lorg/apache/lucene/util/SetOnce;-><init>()V

    iput-object v2, v0, Lorg/apache/lucene/index/MergePolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    .line 378
    return-object v0

    .line 373
    .end local v0    # "clone":Lorg/apache/lucene/index/MergePolicy;
    :catch_0
    move-exception v1

    .line 375
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method public abstract close()V
.end method

.method public abstract findForcedDeletesMerges(Lorg/apache/lucene/index/SegmentInfos;)Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract findForcedMerges(Lorg/apache/lucene/index/SegmentInfos;ILjava/util/Map;)Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/SegmentInfos;",
            "I",
            "Ljava/util/Map",
            "<",
            "Lorg/apache/lucene/index/SegmentInfoPerCommit;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lorg/apache/lucene/index/MergePolicy$MergeSpecification;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract findMerges(Lorg/apache/lucene/index/MergePolicy$MergeTrigger;Lorg/apache/lucene/index/SegmentInfos;)Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public setIndexWriter(Lorg/apache/lucene/index/IndexWriter;)V
    .locals 1
    .param p1, "writer"    # Lorg/apache/lucene/index/IndexWriter;

    .prologue
    .line 398
    iget-object v0, p0, Lorg/apache/lucene/index/MergePolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/util/SetOnce;->set(Ljava/lang/Object;)V

    .line 399
    return-void
.end method

.method public abstract useCompoundFile(Lorg/apache/lucene/index/SegmentInfos;Lorg/apache/lucene/index/SegmentInfoPerCommit;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
