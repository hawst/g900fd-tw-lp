.class final Lorg/apache/lucene/index/TermVectorsConsumerPerField$TermVectorsPostingsArray;
.super Lorg/apache/lucene/index/ParallelPostingsArray;
.source "TermVectorsConsumerPerField.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/TermVectorsConsumerPerField;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "TermVectorsPostingsArray"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field freqs:[I

.field lastOffsets:[I

.field lastPositions:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 274
    const-class v0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/TermVectorsConsumerPerField$TermVectorsPostingsArray;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 276
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/ParallelPostingsArray;-><init>(I)V

    .line 277
    new-array v0, p1, [I

    iput-object v0, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField$TermVectorsPostingsArray;->freqs:[I

    .line 278
    new-array v0, p1, [I

    iput-object v0, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField$TermVectorsPostingsArray;->lastOffsets:[I

    .line 279
    new-array v0, p1, [I

    iput-object v0, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField$TermVectorsPostingsArray;->lastPositions:[I

    .line 280
    return-void
.end method


# virtual methods
.method bytesPerPosting()I
    .locals 1

    .prologue
    .line 305
    invoke-super {p0}, Lorg/apache/lucene/index/ParallelPostingsArray;->bytesPerPosting()I

    move-result v0

    add-int/lit8 v0, v0, 0xc

    return v0
.end method

.method copyTo(Lorg/apache/lucene/index/ParallelPostingsArray;I)V
    .locals 5
    .param p1, "toArray"    # Lorg/apache/lucene/index/ParallelPostingsArray;
    .param p2, "numToCopy"    # I

    .prologue
    const/4 v4, 0x0

    .line 293
    sget-boolean v1, Lorg/apache/lucene/index/TermVectorsConsumerPerField$TermVectorsPostingsArray;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    instance-of v1, p1, Lorg/apache/lucene/index/TermVectorsConsumerPerField$TermVectorsPostingsArray;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    :cond_0
    move-object v0, p1

    .line 294
    check-cast v0, Lorg/apache/lucene/index/TermVectorsConsumerPerField$TermVectorsPostingsArray;

    .line 296
    .local v0, "to":Lorg/apache/lucene/index/TermVectorsConsumerPerField$TermVectorsPostingsArray;
    invoke-super {p0, p1, p2}, Lorg/apache/lucene/index/ParallelPostingsArray;->copyTo(Lorg/apache/lucene/index/ParallelPostingsArray;I)V

    .line 298
    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField$TermVectorsPostingsArray;->freqs:[I

    iget-object v2, v0, Lorg/apache/lucene/index/TermVectorsConsumerPerField$TermVectorsPostingsArray;->freqs:[I

    iget v3, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField$TermVectorsPostingsArray;->size:I

    invoke-static {v1, v4, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 299
    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField$TermVectorsPostingsArray;->lastOffsets:[I

    iget-object v2, v0, Lorg/apache/lucene/index/TermVectorsConsumerPerField$TermVectorsPostingsArray;->lastOffsets:[I

    iget v3, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField$TermVectorsPostingsArray;->size:I

    invoke-static {v1, v4, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 300
    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField$TermVectorsPostingsArray;->lastPositions:[I

    iget-object v2, v0, Lorg/apache/lucene/index/TermVectorsConsumerPerField$TermVectorsPostingsArray;->lastPositions:[I

    iget v3, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField$TermVectorsPostingsArray;->size:I

    invoke-static {v1, v4, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 301
    return-void
.end method

.method newInstance(I)Lorg/apache/lucene/index/ParallelPostingsArray;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 288
    new-instance v0, Lorg/apache/lucene/index/TermVectorsConsumerPerField$TermVectorsPostingsArray;

    invoke-direct {v0, p1}, Lorg/apache/lucene/index/TermVectorsConsumerPerField$TermVectorsPostingsArray;-><init>(I)V

    return-object v0
.end method
