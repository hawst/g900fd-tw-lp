.class Lorg/apache/lucene/index/SortedSetDocValuesWriter$OrdsIterator;
.super Ljava/lang/Object;
.source "SortedSetDocValuesWriter.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/SortedSetDocValuesWriter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OrdsIterator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Ljava/lang/Number;",
        ">;"
    }
.end annotation


# instance fields
.field final counts:Lorg/apache/lucene/util/packed/AppendingLongBuffer$Iterator;

.field final currentDoc:[I

.field currentLength:I

.field currentUpto:I

.field final iter:Lorg/apache/lucene/util/packed/AppendingLongBuffer$Iterator;

.field final numOrds:J

.field final ordMap:[I

.field ordUpto:J

.field final synthetic this$0:Lorg/apache/lucene/index/SortedSetDocValuesWriter;


# direct methods
.method constructor <init>(Lorg/apache/lucene/index/SortedSetDocValuesWriter;[II)V
    .locals 2
    .param p2, "ordMap"    # [I
    .param p3, "maxCount"    # I

    .prologue
    .line 237
    iput-object p1, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter$OrdsIterator;->this$0:Lorg/apache/lucene/index/SortedSetDocValuesWriter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 227
    # getter for: Lorg/apache/lucene/index/SortedSetDocValuesWriter;->pending:Lorg/apache/lucene/util/packed/AppendingLongBuffer;
    invoke-static {p1}, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->access$0(Lorg/apache/lucene/index/SortedSetDocValuesWriter;)Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->iterator()Lorg/apache/lucene/util/packed/AppendingLongBuffer$Iterator;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter$OrdsIterator;->iter:Lorg/apache/lucene/util/packed/AppendingLongBuffer$Iterator;

    .line 228
    # getter for: Lorg/apache/lucene/index/SortedSetDocValuesWriter;->pendingCounts:Lorg/apache/lucene/util/packed/AppendingLongBuffer;
    invoke-static {p1}, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->access$1(Lorg/apache/lucene/index/SortedSetDocValuesWriter;)Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->iterator()Lorg/apache/lucene/util/packed/AppendingLongBuffer$Iterator;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter$OrdsIterator;->counts:Lorg/apache/lucene/util/packed/AppendingLongBuffer$Iterator;

    .line 238
    new-array v0, p3, [I

    iput-object v0, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter$OrdsIterator;->currentDoc:[I

    .line 239
    iput-object p2, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter$OrdsIterator;->ordMap:[I

    .line 240
    # getter for: Lorg/apache/lucene/index/SortedSetDocValuesWriter;->pending:Lorg/apache/lucene/util/packed/AppendingLongBuffer;
    invoke-static {p1}, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->access$0(Lorg/apache/lucene/index/SortedSetDocValuesWriter;)Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->size()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter$OrdsIterator;->numOrds:J

    .line 241
    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 4

    .prologue
    .line 245
    iget-wide v0, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter$OrdsIterator;->ordUpto:J

    iget-wide v2, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter$OrdsIterator;->numOrds:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next()Ljava/lang/Number;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 250
    invoke-virtual {p0}, Lorg/apache/lucene/index/SortedSetDocValuesWriter$OrdsIterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 251
    new-instance v2, Ljava/util/NoSuchElementException;

    invoke-direct {v2}, Ljava/util/NoSuchElementException;-><init>()V

    throw v2

    .line 255
    :cond_0
    iput v6, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter$OrdsIterator;->currentUpto:I

    .line 256
    iget-object v2, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter$OrdsIterator;->counts:Lorg/apache/lucene/util/packed/AppendingLongBuffer$Iterator;

    invoke-virtual {v2}, Lorg/apache/lucene/util/packed/AppendingLongBuffer$Iterator;->next()J

    move-result-wide v2

    long-to-int v2, v2

    iput v2, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter$OrdsIterator;->currentLength:I

    .line 257
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter$OrdsIterator;->currentLength:I

    if-lt v0, v2, :cond_2

    .line 260
    iget-object v2, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter$OrdsIterator;->currentDoc:[I

    iget v3, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter$OrdsIterator;->currentLength:I

    invoke-static {v2, v6, v3}, Ljava/util/Arrays;->sort([III)V

    .line 253
    .end local v0    # "i":I
    :cond_1
    iget v2, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter$OrdsIterator;->currentUpto:I

    iget v3, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter$OrdsIterator;->currentLength:I

    if-eq v2, v3, :cond_0

    .line 262
    iget-object v2, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter$OrdsIterator;->currentDoc:[I

    iget v3, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter$OrdsIterator;->currentUpto:I

    aget v1, v2, v3

    .line 263
    .local v1, "ord":I
    iget v2, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter$OrdsIterator;->currentUpto:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter$OrdsIterator;->currentUpto:I

    .line 264
    iget-wide v2, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter$OrdsIterator;->ordUpto:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter$OrdsIterator;->ordUpto:J

    .line 266
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    return-object v2

    .line 258
    .end local v1    # "ord":I
    .restart local v0    # "i":I
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter$OrdsIterator;->currentDoc:[I

    iget-object v3, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter$OrdsIterator;->ordMap:[I

    iget-object v4, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter$OrdsIterator;->iter:Lorg/apache/lucene/util/packed/AppendingLongBuffer$Iterator;

    invoke-virtual {v4}, Lorg/apache/lucene/util/packed/AppendingLongBuffer$Iterator;->next()J

    move-result-wide v4

    long-to-int v4, v4

    aget v3, v3, v4

    aput v3, v2, v0

    .line 257
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/index/SortedSetDocValuesWriter$OrdsIterator;->next()Ljava/lang/Number;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 271
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
