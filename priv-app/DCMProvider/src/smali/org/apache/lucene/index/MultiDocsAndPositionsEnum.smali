.class public final Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;
.super Lorg/apache/lucene/index/DocsAndPositionsEnum;
.source "MultiDocsAndPositionsEnum.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/MultiDocsAndPositionsEnum$EnumWithSlice;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field current:Lorg/apache/lucene/index/DocsAndPositionsEnum;

.field currentBase:I

.field doc:I

.field numSubs:I

.field private final parent:Lorg/apache/lucene/index/MultiTermsEnum;

.field final subDocsAndPositionsEnum:[Lorg/apache/lucene/index/DocsAndPositionsEnum;

.field private subs:[Lorg/apache/lucene/index/MultiDocsAndPositionsEnum$EnumWithSlice;

.field upto:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/index/MultiTermsEnum;I)V
    .locals 1
    .param p1, "parent"    # Lorg/apache/lucene/index/MultiTermsEnum;
    .param p2, "subReaderCount"    # I

    .prologue
    .line 42
    invoke-direct {p0}, Lorg/apache/lucene/index/DocsAndPositionsEnum;-><init>()V

    .line 39
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->doc:I

    .line 43
    iput-object p1, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->parent:Lorg/apache/lucene/index/MultiTermsEnum;

    .line 44
    new-array v0, p2, [Lorg/apache/lucene/index/DocsAndPositionsEnum;

    iput-object v0, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->subDocsAndPositionsEnum:[Lorg/apache/lucene/index/DocsAndPositionsEnum;

    .line 45
    return-void
.end method


# virtual methods
.method public advance(I)I
    .locals 4
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const v1, 0x7fffffff

    .line 92
    sget-boolean v2, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    iget v2, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->doc:I

    if-gt p1, v2, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 94
    :cond_0
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->current:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    if-eqz v2, :cond_3

    .line 96
    iget v2, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->currentBase:I

    if-ge p1, v2, :cond_1

    .line 98
    iget-object v2, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->current:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v2}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->nextDoc()I

    move-result v0

    .line 102
    .local v0, "doc":I
    :goto_1
    if-ne v0, v1, :cond_2

    .line 103
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->current:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    goto :goto_0

    .line 100
    .end local v0    # "doc":I
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->current:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    iget v3, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->currentBase:I

    sub-int v3, p1, v3

    invoke-virtual {v2, v3}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->advance(I)I

    move-result v0

    .restart local v0    # "doc":I
    goto :goto_1

    .line 105
    :cond_2
    iget v1, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->currentBase:I

    add-int/2addr v1, v0

    iput v1, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->doc:I

    .line 108
    .end local v0    # "doc":I
    :goto_2
    return v1

    .line 107
    :cond_3
    iget v2, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->upto:I

    iget v3, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->numSubs:I

    add-int/lit8 v3, v3, -0x1

    if-ne v2, v3, :cond_4

    .line 108
    iput v1, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->doc:I

    goto :goto_2

    .line 110
    :cond_4
    iget v2, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->upto:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->upto:I

    .line 111
    iget-object v2, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->subs:[Lorg/apache/lucene/index/MultiDocsAndPositionsEnum$EnumWithSlice;

    iget v3, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->upto:I

    aget-object v2, v2, v3

    iget-object v2, v2, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum$EnumWithSlice;->docsAndPositionsEnum:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    iput-object v2, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->current:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    .line 112
    iget-object v2, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->subs:[Lorg/apache/lucene/index/MultiDocsAndPositionsEnum$EnumWithSlice;

    iget v3, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->upto:I

    aget-object v2, v2, v3

    iget-object v2, v2, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum$EnumWithSlice;->slice:Lorg/apache/lucene/index/ReaderSlice;

    iget v2, v2, Lorg/apache/lucene/index/ReaderSlice;->start:I

    iput v2, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->currentBase:I

    goto :goto_0
.end method

.method public canReuse(Lorg/apache/lucene/index/MultiTermsEnum;)Z
    .locals 1
    .param p1, "parent"    # Lorg/apache/lucene/index/MultiTermsEnum;

    .prologue
    .line 50
    iget-object v0, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->parent:Lorg/apache/lucene/index/MultiTermsEnum;

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public cost()J
    .locals 6

    .prologue
    .line 181
    const-wide/16 v0, 0x0

    .line 182
    .local v0, "cost":J
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget v3, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->numSubs:I

    if-lt v2, v3, :cond_0

    .line 185
    return-wide v0

    .line 183
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->subs:[Lorg/apache/lucene/index/MultiDocsAndPositionsEnum$EnumWithSlice;

    aget-object v3, v3, v2

    iget-object v3, v3, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum$EnumWithSlice;->docsAndPositionsEnum:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v3}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->cost()J

    move-result-wide v4

    add-long/2addr v0, v4

    .line 182
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 87
    iget v0, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->doc:I

    return v0
.end method

.method public endOffset()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 151
    iget-object v0, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->current:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->endOffset()I

    move-result v0

    return v0
.end method

.method public freq()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 81
    sget-boolean v0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->current:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 82
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->current:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->freq()I

    move-result v0

    return v0
.end method

.method public getNumSubs()I
    .locals 1

    .prologue
    .line 71
    iget v0, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->numSubs:I

    return v0
.end method

.method public getPayload()Lorg/apache/lucene/util/BytesRef;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 156
    iget-object v0, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->current:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->getPayload()Lorg/apache/lucene/util/BytesRef;

    move-result-object v0

    return-object v0
.end method

.method public getSubs()[Lorg/apache/lucene/index/MultiDocsAndPositionsEnum$EnumWithSlice;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->subs:[Lorg/apache/lucene/index/MultiDocsAndPositionsEnum$EnumWithSlice;

    return-object v0
.end method

.method public nextDoc()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const v1, 0x7fffffff

    .line 120
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->current:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    if-nez v2, :cond_1

    .line 121
    iget v2, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->upto:I

    iget v3, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->numSubs:I

    add-int/lit8 v3, v3, -0x1

    if-ne v2, v3, :cond_0

    .line 122
    iput v1, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->doc:I

    .line 132
    :goto_1
    return v1

    .line 124
    :cond_0
    iget v2, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->upto:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->upto:I

    .line 125
    iget-object v2, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->subs:[Lorg/apache/lucene/index/MultiDocsAndPositionsEnum$EnumWithSlice;

    iget v3, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->upto:I

    aget-object v2, v2, v3

    iget-object v2, v2, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum$EnumWithSlice;->docsAndPositionsEnum:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    iput-object v2, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->current:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    .line 126
    iget-object v2, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->subs:[Lorg/apache/lucene/index/MultiDocsAndPositionsEnum$EnumWithSlice;

    iget v3, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->upto:I

    aget-object v2, v2, v3

    iget-object v2, v2, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum$EnumWithSlice;->slice:Lorg/apache/lucene/index/ReaderSlice;

    iget v2, v2, Lorg/apache/lucene/index/ReaderSlice;->start:I

    iput v2, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->currentBase:I

    .line 130
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->current:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v2}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->nextDoc()I

    move-result v0

    .line 131
    .local v0, "doc":I
    if-eq v0, v1, :cond_2

    .line 132
    iget v1, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->currentBase:I

    add-int/2addr v1, v0

    iput v1, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->doc:I

    goto :goto_1

    .line 134
    :cond_2
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->current:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    goto :goto_0
.end method

.method public nextPosition()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 141
    iget-object v0, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->current:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->nextPosition()I

    move-result v0

    return v0
.end method

.method public reset([Lorg/apache/lucene/index/MultiDocsAndPositionsEnum$EnumWithSlice;I)Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;
    .locals 4
    .param p1, "subs"    # [Lorg/apache/lucene/index/MultiDocsAndPositionsEnum$EnumWithSlice;
    .param p2, "numSubs"    # I

    .prologue
    const/4 v3, -0x1

    .line 55
    iput p2, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->numSubs:I

    .line 56
    array-length v1, p1

    new-array v1, v1, [Lorg/apache/lucene/index/MultiDocsAndPositionsEnum$EnumWithSlice;

    iput-object v1, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->subs:[Lorg/apache/lucene/index/MultiDocsAndPositionsEnum$EnumWithSlice;

    .line 57
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-lt v0, v1, :cond_0

    .line 62
    iput v3, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->upto:I

    .line 63
    iput v3, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->doc:I

    .line 64
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->current:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    .line 65
    return-object p0

    .line 58
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->subs:[Lorg/apache/lucene/index/MultiDocsAndPositionsEnum$EnumWithSlice;

    new-instance v2, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum$EnumWithSlice;

    invoke-direct {v2}, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum$EnumWithSlice;-><init>()V

    aput-object v2, v1, v0

    .line 59
    iget-object v1, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->subs:[Lorg/apache/lucene/index/MultiDocsAndPositionsEnum$EnumWithSlice;

    aget-object v1, v1, v0

    aget-object v2, p1, v0

    iget-object v2, v2, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum$EnumWithSlice;->docsAndPositionsEnum:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    iput-object v2, v1, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum$EnumWithSlice;->docsAndPositionsEnum:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    .line 60
    iget-object v1, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->subs:[Lorg/apache/lucene/index/MultiDocsAndPositionsEnum$EnumWithSlice;

    aget-object v1, v1, v0

    aget-object v2, p1, v0

    iget-object v2, v2, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum$EnumWithSlice;->slice:Lorg/apache/lucene/index/ReaderSlice;

    iput-object v2, v1, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum$EnumWithSlice;->slice:Lorg/apache/lucene/index/ReaderSlice;

    .line 57
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public startOffset()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 146
    iget-object v0, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->current:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->startOffset()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 190
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "MultiDocsAndPositionsEnum("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->getSubs()[Lorg/apache/lucene/index/MultiDocsAndPositionsEnum$EnumWithSlice;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
