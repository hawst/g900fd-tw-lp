.class public final Lorg/apache/lucene/index/ParallelAtomicReader;
.super Lorg/apache/lucene/index/AtomicReader;
.source "ParallelAtomicReader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/ParallelAtomicReader$ParallelFields;
    }
.end annotation


# instance fields
.field private final closeSubReaders:Z

.field private final completeReaderSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lorg/apache/lucene/index/AtomicReader;",
            ">;"
        }
    .end annotation
.end field

.field private final fieldInfos:Lorg/apache/lucene/index/FieldInfos;

.field private final fieldToReader:Ljava/util/SortedMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/SortedMap",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/index/AtomicReader;",
            ">;"
        }
    .end annotation
.end field

.field private final fields:Lorg/apache/lucene/index/ParallelAtomicReader$ParallelFields;

.field private final hasDeletions:Z

.field private final maxDoc:I

.field private final numDocs:I

.field private final parallelReaders:[Lorg/apache/lucene/index/AtomicReader;

.field private final storedFieldsReaders:[Lorg/apache/lucene/index/AtomicReader;

.field private final tvFieldToReader:Ljava/util/SortedMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/SortedMap",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/index/AtomicReader;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public varargs constructor <init>(Z[Lorg/apache/lucene/index/AtomicReader;)V
    .locals 0
    .param p1, "closeSubReaders"    # Z
    .param p2, "readers"    # [Lorg/apache/lucene/index/AtomicReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 71
    invoke-direct {p0, p1, p2, p2}, Lorg/apache/lucene/index/ParallelAtomicReader;-><init>(Z[Lorg/apache/lucene/index/AtomicReader;[Lorg/apache/lucene/index/AtomicReader;)V

    .line 72
    return-void
.end method

.method public constructor <init>(Z[Lorg/apache/lucene/index/AtomicReader;[Lorg/apache/lucene/index/AtomicReader;)V
    .locals 14
    .param p1, "closeSubReaders"    # Z
    .param p2, "readers"    # [Lorg/apache/lucene/index/AtomicReader;
    .param p3, "storedFieldsReaders"    # [Lorg/apache/lucene/index/AtomicReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 77
    invoke-direct {p0}, Lorg/apache/lucene/index/AtomicReader;-><init>()V

    .line 52
    new-instance v8, Lorg/apache/lucene/index/ParallelAtomicReader$ParallelFields;

    invoke-direct {v8, p0}, Lorg/apache/lucene/index/ParallelAtomicReader$ParallelFields;-><init>(Lorg/apache/lucene/index/ParallelAtomicReader;)V

    iput-object v8, p0, Lorg/apache/lucene/index/ParallelAtomicReader;->fields:Lorg/apache/lucene/index/ParallelAtomicReader$ParallelFields;

    .line 55
    new-instance v8, Ljava/util/IdentityHashMap;

    invoke-direct {v8}, Ljava/util/IdentityHashMap;-><init>()V

    invoke-static {v8}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v8

    iput-object v8, p0, Lorg/apache/lucene/index/ParallelAtomicReader;->completeReaderSet:Ljava/util/Set;

    .line 59
    new-instance v8, Ljava/util/TreeMap;

    invoke-direct {v8}, Ljava/util/TreeMap;-><init>()V

    iput-object v8, p0, Lorg/apache/lucene/index/ParallelAtomicReader;->fieldToReader:Ljava/util/SortedMap;

    .line 60
    new-instance v8, Ljava/util/TreeMap;

    invoke-direct {v8}, Ljava/util/TreeMap;-><init>()V

    iput-object v8, p0, Lorg/apache/lucene/index/ParallelAtomicReader;->tvFieldToReader:Ljava/util/SortedMap;

    .line 78
    iput-boolean p1, p0, Lorg/apache/lucene/index/ParallelAtomicReader;->closeSubReaders:Z

    .line 79
    move-object/from16 v0, p2

    array-length v8, v0

    if-nez v8, :cond_0

    move-object/from16 v0, p3

    array-length v8, v0

    if-lez v8, :cond_0

    .line 80
    new-instance v8, Ljava/lang/IllegalArgumentException;

    const-string v9, "There must be at least one main reader if storedFieldsReaders are used."

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 81
    :cond_0
    invoke-virtual/range {p2 .. p2}, [Lorg/apache/lucene/index/AtomicReader;->clone()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [Lorg/apache/lucene/index/AtomicReader;

    iput-object v8, p0, Lorg/apache/lucene/index/ParallelAtomicReader;->parallelReaders:[Lorg/apache/lucene/index/AtomicReader;

    .line 82
    invoke-virtual/range {p3 .. p3}, [Lorg/apache/lucene/index/AtomicReader;->clone()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [Lorg/apache/lucene/index/AtomicReader;

    iput-object v8, p0, Lorg/apache/lucene/index/ParallelAtomicReader;->storedFieldsReaders:[Lorg/apache/lucene/index/AtomicReader;

    .line 83
    iget-object v8, p0, Lorg/apache/lucene/index/ParallelAtomicReader;->parallelReaders:[Lorg/apache/lucene/index/AtomicReader;

    array-length v8, v8

    if-lez v8, :cond_2

    .line 84
    iget-object v8, p0, Lorg/apache/lucene/index/ParallelAtomicReader;->parallelReaders:[Lorg/apache/lucene/index/AtomicReader;

    const/4 v9, 0x0

    aget-object v4, v8, v9

    .line 85
    .local v4, "first":Lorg/apache/lucene/index/AtomicReader;
    invoke-virtual {v4}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v8

    iput v8, p0, Lorg/apache/lucene/index/ParallelAtomicReader;->maxDoc:I

    .line 86
    invoke-virtual {v4}, Lorg/apache/lucene/index/AtomicReader;->numDocs()I

    move-result v8

    iput v8, p0, Lorg/apache/lucene/index/ParallelAtomicReader;->numDocs:I

    .line 87
    invoke-virtual {v4}, Lorg/apache/lucene/index/AtomicReader;->hasDeletions()Z

    move-result v8

    iput-boolean v8, p0, Lorg/apache/lucene/index/ParallelAtomicReader;->hasDeletions:Z

    .line 92
    .end local v4    # "first":Lorg/apache/lucene/index/AtomicReader;
    :goto_0
    iget-object v8, p0, Lorg/apache/lucene/index/ParallelAtomicReader;->completeReaderSet:Ljava/util/Set;

    iget-object v9, p0, Lorg/apache/lucene/index/ParallelAtomicReader;->parallelReaders:[Lorg/apache/lucene/index/AtomicReader;

    invoke-static {v8, v9}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 93
    iget-object v8, p0, Lorg/apache/lucene/index/ParallelAtomicReader;->completeReaderSet:Ljava/util/Set;

    iget-object v9, p0, Lorg/apache/lucene/index/ParallelAtomicReader;->storedFieldsReaders:[Lorg/apache/lucene/index/AtomicReader;

    invoke-static {v8, v9}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 96
    iget-object v8, p0, Lorg/apache/lucene/index/ParallelAtomicReader;->completeReaderSet:Ljava/util/Set;

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-nez v9, :cond_3

    .line 103
    new-instance v1, Lorg/apache/lucene/index/FieldInfos$Builder;

    invoke-direct {v1}, Lorg/apache/lucene/index/FieldInfos$Builder;-><init>()V

    .line 105
    .local v1, "builder":Lorg/apache/lucene/index/FieldInfos$Builder;
    iget-object v9, p0, Lorg/apache/lucene/index/ParallelAtomicReader;->parallelReaders:[Lorg/apache/lucene/index/AtomicReader;

    array-length v10, v9

    const/4 v8, 0x0

    :goto_1
    if-lt v8, v10, :cond_4

    .line 118
    invoke-virtual {v1}, Lorg/apache/lucene/index/FieldInfos$Builder;->finish()Lorg/apache/lucene/index/FieldInfos;

    move-result-object v8

    iput-object v8, p0, Lorg/apache/lucene/index/ParallelAtomicReader;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    .line 121
    iget-object v9, p0, Lorg/apache/lucene/index/ParallelAtomicReader;->parallelReaders:[Lorg/apache/lucene/index/AtomicReader;

    array-length v10, v9

    const/4 v8, 0x0

    :goto_2
    if-lt v8, v10, :cond_7

    .line 134
    iget-object v8, p0, Lorg/apache/lucene/index/ParallelAtomicReader;->completeReaderSet:Ljava/util/Set;

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-nez v9, :cond_b

    .line 140
    return-void

    .line 89
    .end local v1    # "builder":Lorg/apache/lucene/index/FieldInfos$Builder;
    :cond_2
    const/4 v8, 0x0

    iput v8, p0, Lorg/apache/lucene/index/ParallelAtomicReader;->numDocs:I

    iput v8, p0, Lorg/apache/lucene/index/ParallelAtomicReader;->maxDoc:I

    .line 90
    const/4 v8, 0x0

    iput-boolean v8, p0, Lorg/apache/lucene/index/ParallelAtomicReader;->hasDeletions:Z

    goto :goto_0

    .line 96
    :cond_3
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/index/AtomicReader;

    .line 97
    .local v5, "reader":Lorg/apache/lucene/index/AtomicReader;
    invoke-virtual {v5}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v9

    iget v10, p0, Lorg/apache/lucene/index/ParallelAtomicReader;->maxDoc:I

    if-eq v9, v10, :cond_1

    .line 98
    new-instance v8, Ljava/lang/IllegalArgumentException;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "All readers must have same maxDoc: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v10, p0, Lorg/apache/lucene/index/ParallelAtomicReader;->maxDoc:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "!="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v5}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 105
    .end local v5    # "reader":Lorg/apache/lucene/index/AtomicReader;
    .restart local v1    # "builder":Lorg/apache/lucene/index/FieldInfos$Builder;
    :cond_4
    aget-object v5, v9, v8

    .line 106
    .restart local v5    # "reader":Lorg/apache/lucene/index/AtomicReader;
    invoke-virtual {v5}, Lorg/apache/lucene/index/AtomicReader;->getFieldInfos()Lorg/apache/lucene/index/FieldInfos;

    move-result-object v6

    .line 107
    .local v6, "readerFieldInfos":Lorg/apache/lucene/index/FieldInfos;
    invoke-virtual {v6}, Lorg/apache/lucene/index/FieldInfos;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_5
    :goto_4
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_6

    .line 105
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 107
    :cond_6
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/index/FieldInfo;

    .line 109
    .local v3, "fieldInfo":Lorg/apache/lucene/index/FieldInfo;
    iget-object v12, p0, Lorg/apache/lucene/index/ParallelAtomicReader;->fieldToReader:Ljava/util/SortedMap;

    iget-object v13, v3, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-interface {v12, v13}, Ljava/util/SortedMap;->containsKey(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_5

    .line 110
    invoke-virtual {v1, v3}, Lorg/apache/lucene/index/FieldInfos$Builder;->add(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/FieldInfo;

    .line 111
    iget-object v12, p0, Lorg/apache/lucene/index/ParallelAtomicReader;->fieldToReader:Ljava/util/SortedMap;

    iget-object v13, v3, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-interface {v12, v13, v5}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    invoke-virtual {v3}, Lorg/apache/lucene/index/FieldInfo;->hasVectors()Z

    move-result v12

    if-eqz v12, :cond_5

    .line 113
    iget-object v12, p0, Lorg/apache/lucene/index/ParallelAtomicReader;->tvFieldToReader:Ljava/util/SortedMap;

    iget-object v13, v3, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-interface {v12, v13, v5}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    .line 121
    .end local v3    # "fieldInfo":Lorg/apache/lucene/index/FieldInfo;
    .end local v5    # "reader":Lorg/apache/lucene/index/AtomicReader;
    .end local v6    # "readerFieldInfos":Lorg/apache/lucene/index/FieldInfos;
    :cond_7
    aget-object v5, v9, v8

    .line 122
    .restart local v5    # "reader":Lorg/apache/lucene/index/AtomicReader;
    invoke-virtual {v5}, Lorg/apache/lucene/index/AtomicReader;->fields()Lorg/apache/lucene/index/Fields;

    move-result-object v7

    .line 123
    .local v7, "readerFields":Lorg/apache/lucene/index/Fields;
    if-eqz v7, :cond_9

    .line 124
    invoke-virtual {v7}, Lorg/apache/lucene/index/Fields;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_8
    :goto_5
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_a

    .line 121
    :cond_9
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_2

    .line 124
    :cond_a
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 126
    .local v2, "field":Ljava/lang/String;
    iget-object v12, p0, Lorg/apache/lucene/index/ParallelAtomicReader;->fieldToReader:Ljava/util/SortedMap;

    invoke-interface {v12, v2}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    if-ne v12, v5, :cond_8

    .line 127
    iget-object v12, p0, Lorg/apache/lucene/index/ParallelAtomicReader;->fields:Lorg/apache/lucene/index/ParallelAtomicReader$ParallelFields;

    invoke-virtual {v7, v2}, Lorg/apache/lucene/index/Fields;->terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v13

    invoke-virtual {v12, v2, v13}, Lorg/apache/lucene/index/ParallelAtomicReader$ParallelFields;->addField(Ljava/lang/String;Lorg/apache/lucene/index/Terms;)V

    goto :goto_5

    .line 134
    .end local v2    # "field":Ljava/lang/String;
    .end local v5    # "reader":Lorg/apache/lucene/index/AtomicReader;
    .end local v7    # "readerFields":Lorg/apache/lucene/index/Fields;
    :cond_b
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/index/AtomicReader;

    .line 135
    .restart local v5    # "reader":Lorg/apache/lucene/index/AtomicReader;
    if-nez p1, :cond_c

    .line 136
    invoke-virtual {v5}, Lorg/apache/lucene/index/AtomicReader;->incRef()V

    .line 138
    :cond_c
    invoke-virtual {v5, p0}, Lorg/apache/lucene/index/AtomicReader;->registerParentReader(Lorg/apache/lucene/index/IndexReader;)V

    goto/16 :goto_3
.end method

.method public varargs constructor <init>([Lorg/apache/lucene/index/AtomicReader;)V
    .locals 1
    .param p1, "readers"    # [Lorg/apache/lucene/index/AtomicReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 65
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lorg/apache/lucene/index/ParallelAtomicReader;-><init>(Z[Lorg/apache/lucene/index/AtomicReader;)V

    .line 66
    return-void
.end method


# virtual methods
.method protected declared-synchronized doClose()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 244
    monitor-enter p0

    const/4 v1, 0x0

    .line 245
    .local v1, "ioe":Ljava/io/IOException;
    :try_start_0
    iget-object v3, p0, Lorg/apache/lucene/index/ParallelAtomicReader;->completeReaderSet:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 257
    if-eqz v1, :cond_3

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 244
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 245
    :cond_1
    :try_start_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/index/AtomicReader;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 247
    .local v2, "reader":Lorg/apache/lucene/index/AtomicReader;
    :try_start_2
    iget-boolean v4, p0, Lorg/apache/lucene/index/ParallelAtomicReader;->closeSubReaders:Z

    if-eqz v4, :cond_2

    .line 248
    invoke-virtual {v2}, Lorg/apache/lucene/index/AtomicReader;->close()V

    goto :goto_0

    .line 252
    :catch_0
    move-exception v0

    .line 253
    .local v0, "e":Ljava/io/IOException;
    if-nez v1, :cond_0

    move-object v1, v0

    goto :goto_0

    .line 250
    .end local v0    # "e":Ljava/io/IOException;
    :cond_2
    invoke-virtual {v2}, Lorg/apache/lucene/index/AtomicReader;->decRef()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 258
    .end local v2    # "reader":Lorg/apache/lucene/index/AtomicReader;
    :cond_3
    monitor-exit p0

    return-void
.end method

.method public document(ILorg/apache/lucene/index/StoredFieldVisitor;)V
    .locals 4
    .param p1, "docID"    # I
    .param p2, "visitor"    # Lorg/apache/lucene/index/StoredFieldVisitor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 218
    invoke-virtual {p0}, Lorg/apache/lucene/index/ParallelAtomicReader;->ensureOpen()V

    .line 219
    iget-object v2, p0, Lorg/apache/lucene/index/ParallelAtomicReader;->storedFieldsReaders:[Lorg/apache/lucene/index/AtomicReader;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v3, :cond_0

    .line 222
    return-void

    .line 219
    :cond_0
    aget-object v0, v2, v1

    .line 220
    .local v0, "reader":Lorg/apache/lucene/index/AtomicReader;
    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/index/AtomicReader;->document(ILorg/apache/lucene/index/StoredFieldVisitor;)V

    .line 219
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public fields()Lorg/apache/lucene/index/Fields;
    .locals 1

    .prologue
    .line 200
    invoke-virtual {p0}, Lorg/apache/lucene/index/ParallelAtomicReader;->ensureOpen()V

    .line 201
    iget-object v0, p0, Lorg/apache/lucene/index/ParallelAtomicReader;->fields:Lorg/apache/lucene/index/ParallelAtomicReader$ParallelFields;

    return-object v0
.end method

.method public getBinaryDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/BinaryDocValues;
    .locals 2
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 269
    invoke-virtual {p0}, Lorg/apache/lucene/index/ParallelAtomicReader;->ensureOpen()V

    .line 270
    iget-object v1, p0, Lorg/apache/lucene/index/ParallelAtomicReader;->fieldToReader:Ljava/util/SortedMap;

    invoke-interface {v1, p1}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/AtomicReader;

    .line 271
    .local v0, "reader":Lorg/apache/lucene/index/AtomicReader;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/AtomicReader;->getBinaryDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/BinaryDocValues;

    move-result-object v1

    goto :goto_0
.end method

.method public getFieldInfos()Lorg/apache/lucene/index/FieldInfos;
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lorg/apache/lucene/index/ParallelAtomicReader;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    return-object v0
.end method

.method public getLiveDocs()Lorg/apache/lucene/util/Bits;
    .locals 2

    .prologue
    .line 194
    invoke-virtual {p0}, Lorg/apache/lucene/index/ParallelAtomicReader;->ensureOpen()V

    .line 195
    iget-boolean v0, p0, Lorg/apache/lucene/index/ParallelAtomicReader;->hasDeletions:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/ParallelAtomicReader;->parallelReaders:[Lorg/apache/lucene/index/AtomicReader;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lorg/apache/lucene/index/AtomicReader;->getLiveDocs()Lorg/apache/lucene/util/Bits;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getNormValues(Ljava/lang/String;)Lorg/apache/lucene/index/NumericDocValues;
    .locals 3
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 290
    invoke-virtual {p0}, Lorg/apache/lucene/index/ParallelAtomicReader;->ensureOpen()V

    .line 291
    iget-object v2, p0, Lorg/apache/lucene/index/ParallelAtomicReader;->fieldToReader:Ljava/util/SortedMap;

    invoke-interface {v2, p1}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/AtomicReader;

    .line 292
    .local v0, "reader":Lorg/apache/lucene/index/AtomicReader;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    .line 293
    .local v1, "values":Lorg/apache/lucene/index/NumericDocValues;
    :goto_0
    return-object v1

    .line 292
    .end local v1    # "values":Lorg/apache/lucene/index/NumericDocValues;
    :cond_0
    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/AtomicReader;->getNormValues(Ljava/lang/String;)Lorg/apache/lucene/index/NumericDocValues;

    move-result-object v1

    goto :goto_0
.end method

.method public getNumericDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/NumericDocValues;
    .locals 2
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 262
    invoke-virtual {p0}, Lorg/apache/lucene/index/ParallelAtomicReader;->ensureOpen()V

    .line 263
    iget-object v1, p0, Lorg/apache/lucene/index/ParallelAtomicReader;->fieldToReader:Ljava/util/SortedMap;

    invoke-interface {v1, p1}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/AtomicReader;

    .line 264
    .local v0, "reader":Lorg/apache/lucene/index/AtomicReader;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/AtomicReader;->getNumericDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/NumericDocValues;

    move-result-object v1

    goto :goto_0
.end method

.method public getSortedDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/SortedDocValues;
    .locals 2
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 276
    invoke-virtual {p0}, Lorg/apache/lucene/index/ParallelAtomicReader;->ensureOpen()V

    .line 277
    iget-object v1, p0, Lorg/apache/lucene/index/ParallelAtomicReader;->fieldToReader:Ljava/util/SortedMap;

    invoke-interface {v1, p1}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/AtomicReader;

    .line 278
    .local v0, "reader":Lorg/apache/lucene/index/AtomicReader;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/AtomicReader;->getSortedDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/SortedDocValues;

    move-result-object v1

    goto :goto_0
.end method

.method public getSortedSetDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/SortedSetDocValues;
    .locals 2
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 283
    invoke-virtual {p0}, Lorg/apache/lucene/index/ParallelAtomicReader;->ensureOpen()V

    .line 284
    iget-object v1, p0, Lorg/apache/lucene/index/ParallelAtomicReader;->fieldToReader:Ljava/util/SortedMap;

    invoke-interface {v1, p1}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/AtomicReader;

    .line 285
    .local v0, "reader":Lorg/apache/lucene/index/AtomicReader;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/AtomicReader;->getSortedSetDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/SortedSetDocValues;

    move-result-object v1

    goto :goto_0
.end method

.method public getTermVectors(I)Lorg/apache/lucene/index/Fields;
    .locals 6
    .param p1, "docID"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 226
    invoke-virtual {p0}, Lorg/apache/lucene/index/ParallelAtomicReader;->ensureOpen()V

    .line 227
    const/4 v2, 0x0

    .line 228
    .local v2, "fields":Lorg/apache/lucene/index/ParallelAtomicReader$ParallelFields;
    iget-object v4, p0, Lorg/apache/lucene/index/ParallelAtomicReader;->tvFieldToReader:Ljava/util/SortedMap;

    invoke-interface {v4}, Ljava/util/SortedMap;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 239
    return-object v2

    .line 228
    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 229
    .local v0, "ent":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lorg/apache/lucene/index/AtomicReader;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 230
    .local v1, "fieldName":Ljava/lang/String;
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/index/AtomicReader;

    invoke-virtual {v4, p1, v1}, Lorg/apache/lucene/index/AtomicReader;->getTermVector(ILjava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v3

    .line 231
    .local v3, "vector":Lorg/apache/lucene/index/Terms;
    if-eqz v3, :cond_0

    .line 232
    if-nez v2, :cond_2

    .line 233
    new-instance v2, Lorg/apache/lucene/index/ParallelAtomicReader$ParallelFields;

    .end local v2    # "fields":Lorg/apache/lucene/index/ParallelAtomicReader$ParallelFields;
    invoke-direct {v2, p0}, Lorg/apache/lucene/index/ParallelAtomicReader$ParallelFields;-><init>(Lorg/apache/lucene/index/ParallelAtomicReader;)V

    .line 235
    .restart local v2    # "fields":Lorg/apache/lucene/index/ParallelAtomicReader$ParallelFields;
    :cond_2
    invoke-virtual {v2, v1, v3}, Lorg/apache/lucene/index/ParallelAtomicReader$ParallelFields;->addField(Ljava/lang/String;Lorg/apache/lucene/index/Terms;)V

    goto :goto_0
.end method

.method public maxDoc()I
    .locals 1

    .prologue
    .line 213
    iget v0, p0, Lorg/apache/lucene/index/ParallelAtomicReader;->maxDoc:I

    return v0
.end method

.method public numDocs()I
    .locals 1

    .prologue
    .line 207
    iget v0, p0, Lorg/apache/lucene/index/ParallelAtomicReader;->numDocs:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 144
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "ParallelAtomicReader("

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 145
    .local v0, "buffer":Ljava/lang/StringBuilder;
    iget-object v2, p0, Lorg/apache/lucene/index/ParallelAtomicReader;->completeReaderSet:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/index/AtomicReader;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 149
    const/16 v2, 0x29

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 146
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 147
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method
