.class Lorg/apache/lucene/index/DocumentsWriterFlushQueue;
.super Ljava/lang/Object;
.source "DocumentsWriterFlushQueue.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/DocumentsWriterFlushQueue$FlushTicket;,
        Lorg/apache/lucene/index/DocumentsWriterFlushQueue$GlobalDeletesTicket;,
        Lorg/apache/lucene/index/DocumentsWriterFlushQueue$SegmentFlushTicket;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final purgeLock:Ljava/util/concurrent/locks/ReentrantLock;

.field private final queue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lorg/apache/lucene/index/DocumentsWriterFlushQueue$FlushTicket;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketCount:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;->queue:Ljava/util/Queue;

    .line 34
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;->ticketCount:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 35
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;->purgeLock:Ljava/util/concurrent/locks/ReentrantLock;

    .line 30
    return-void
.end method

.method private decTickets()V
    .locals 2

    .prologue
    .line 63
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;->ticketCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    .line 64
    .local v0, "numTickets":I
    sget-boolean v1, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-gez v0, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 65
    :cond_0
    return-void
.end method

.method private incTickets()V
    .locals 2

    .prologue
    .line 58
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;->ticketCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    .line 59
    .local v0, "numTickets":I
    sget-boolean v1, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-gtz v0, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 60
    :cond_0
    return-void
.end method

.method private innerPurge(Lorg/apache/lucene/index/DocumentsWriter;)V
    .locals 5
    .param p1, "writer"    # Lorg/apache/lucene/index/DocumentsWriter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 102
    sget-boolean v3, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;->$assertionsDisabled:Z

    if-nez v3, :cond_1

    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;->purgeLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->isHeldByCurrentThread()Z

    move-result v3

    if-nez v3, :cond_1

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 120
    .local v0, "canPublish":Z
    .local v1, "head":Lorg/apache/lucene/index/DocumentsWriterFlushQueue$FlushTicket;
    .local v2, "poll":Lorg/apache/lucene/index/DocumentsWriterFlushQueue$FlushTicket;
    :cond_0
    :try_start_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 106
    .end local v0    # "canPublish":Z
    .end local v1    # "head":Lorg/apache/lucene/index/DocumentsWriterFlushQueue$FlushTicket;
    .end local v2    # "poll":Lorg/apache/lucene/index/DocumentsWriterFlushQueue$FlushTicket;
    :cond_1
    monitor-enter p0

    .line 107
    :try_start_1
    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;->queue:Ljava/util/Queue;

    invoke-interface {v3}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/DocumentsWriterFlushQueue$FlushTicket;

    .line 108
    .restart local v1    # "head":Lorg/apache/lucene/index/DocumentsWriterFlushQueue$FlushTicket;
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lorg/apache/lucene/index/DocumentsWriterFlushQueue$FlushTicket;->canPublish()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v0, 0x1

    .line 106
    .restart local v0    # "canPublish":Z
    :goto_0
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 110
    if-eqz v0, :cond_4

    .line 118
    :try_start_2
    invoke-virtual {v1, p1}, Lorg/apache/lucene/index/DocumentsWriterFlushQueue$FlushTicket;->publish(Lorg/apache/lucene/index/DocumentsWriter;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 120
    monitor-enter p0

    .line 122
    :try_start_3
    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;->queue:Ljava/util/Queue;

    invoke-interface {v3}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/index/DocumentsWriterFlushQueue$FlushTicket;

    .line 123
    .restart local v2    # "poll":Lorg/apache/lucene/index/DocumentsWriterFlushQueue$FlushTicket;
    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;->ticketCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    .line 124
    sget-boolean v3, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    if-eq v2, v1, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 120
    .end local v2    # "poll":Lorg/apache/lucene/index/DocumentsWriterFlushQueue$FlushTicket;
    :catchall_0
    move-exception v3

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v3

    .line 108
    .end local v0    # "canPublish":Z
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 106
    .end local v1    # "head":Lorg/apache/lucene/index/DocumentsWriterFlushQueue$FlushTicket;
    :catchall_1
    move-exception v3

    :try_start_4
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v3

    .line 119
    .restart local v0    # "canPublish":Z
    .restart local v1    # "head":Lorg/apache/lucene/index/DocumentsWriterFlushQueue$FlushTicket;
    :catchall_2
    move-exception v3

    .line 120
    monitor-enter p0

    .line 122
    :try_start_5
    iget-object v4, p0, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;->queue:Ljava/util/Queue;

    invoke-interface {v4}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/index/DocumentsWriterFlushQueue$FlushTicket;

    .line 123
    .restart local v2    # "poll":Lorg/apache/lucene/index/DocumentsWriterFlushQueue$FlushTicket;
    iget-object v4, p0, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;->ticketCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    .line 124
    sget-boolean v4, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;->$assertionsDisabled:Z

    if-nez v4, :cond_3

    if-eq v2, v1, :cond_3

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 120
    .end local v2    # "poll":Lorg/apache/lucene/index/DocumentsWriterFlushQueue$FlushTicket;
    :catchall_3
    move-exception v3

    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    throw v3

    .restart local v2    # "poll":Lorg/apache/lucene/index/DocumentsWriterFlushQueue$FlushTicket;
    :cond_3
    :try_start_6
    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 126
    throw v3

    .line 131
    .end local v2    # "poll":Lorg/apache/lucene/index/DocumentsWriterFlushQueue$FlushTicket;
    :cond_4
    return-void
.end method


# virtual methods
.method addDeletesAndPurge(Lorg/apache/lucene/index/DocumentsWriter;Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;)V
    .locals 4
    .param p1, "writer"    # Lorg/apache/lucene/index/DocumentsWriter;
    .param p2, "deleteQueue"    # Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 39
    monitor-enter p0

    .line 40
    :try_start_0
    invoke-direct {p0}, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;->incTickets()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 42
    const/4 v0, 0x0

    .line 44
    .local v0, "success":Z
    :try_start_1
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;->queue:Ljava/util/Queue;

    new-instance v2, Lorg/apache/lucene/index/DocumentsWriterFlushQueue$GlobalDeletesTicket;

    const/4 v3, 0x0

    invoke-virtual {p2, v3}, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->freezeGlobalBuffer(Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;)Lorg/apache/lucene/index/FrozenBufferedDeletes;

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/apache/lucene/index/DocumentsWriterFlushQueue$GlobalDeletesTicket;-><init>(Lorg/apache/lucene/index/FrozenBufferedDeletes;)V

    invoke-interface {v1, v2}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 45
    const/4 v0, 0x1

    .line 47
    if-nez v0, :cond_0

    .line 48
    :try_start_2
    invoke-direct {p0}, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;->decTickets()V

    .line 39
    :cond_0
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 54
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;->forcePurge(Lorg/apache/lucene/index/DocumentsWriter;)V

    .line 55
    return-void

    .line 46
    :catchall_0
    move-exception v1

    .line 47
    if-nez v0, :cond_1

    .line 48
    :try_start_3
    invoke-direct {p0}, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;->decTickets()V

    .line 50
    :cond_1
    throw v1

    .line 39
    .end local v0    # "success":Z
    :catchall_1
    move-exception v1

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v1
.end method

.method declared-synchronized addFlushTicket(Lorg/apache/lucene/index/DocumentsWriterPerThread;)Lorg/apache/lucene/index/DocumentsWriterFlushQueue$SegmentFlushTicket;
    .locals 3
    .param p1, "dwpt"    # Lorg/apache/lucene/index/DocumentsWriterPerThread;

    .prologue
    .line 70
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;->incTickets()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 71
    const/4 v0, 0x0

    .line 74
    .local v0, "success":Z
    :try_start_1
    new-instance v1, Lorg/apache/lucene/index/DocumentsWriterFlushQueue$SegmentFlushTicket;

    invoke-virtual {p1}, Lorg/apache/lucene/index/DocumentsWriterPerThread;->prepareFlush()Lorg/apache/lucene/index/FrozenBufferedDeletes;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/lucene/index/DocumentsWriterFlushQueue$SegmentFlushTicket;-><init>(Lorg/apache/lucene/index/FrozenBufferedDeletes;)V

    .line 75
    .local v1, "ticket":Lorg/apache/lucene/index/DocumentsWriterFlushQueue$SegmentFlushTicket;
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;->queue:Ljava/util/Queue;

    invoke-interface {v2, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 76
    const/4 v0, 0x1

    .line 79
    if-nez v0, :cond_0

    .line 80
    :try_start_2
    invoke-direct {p0}, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;->decTickets()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 77
    :cond_0
    monitor-exit p0

    return-object v1

    .line 78
    .end local v1    # "ticket":Lorg/apache/lucene/index/DocumentsWriterFlushQueue$SegmentFlushTicket;
    :catchall_0
    move-exception v2

    .line 79
    if-nez v0, :cond_1

    .line 80
    :try_start_3
    invoke-direct {p0}, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;->decTickets()V

    .line 82
    :cond_1
    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 70
    .end local v0    # "success":Z
    :catchall_1
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method declared-synchronized addSegment(Lorg/apache/lucene/index/DocumentsWriterFlushQueue$SegmentFlushTicket;Lorg/apache/lucene/index/DocumentsWriterPerThread$FlushedSegment;)V
    .locals 1
    .param p1, "ticket"    # Lorg/apache/lucene/index/DocumentsWriterFlushQueue$SegmentFlushTicket;
    .param p2, "segment"    # Lorg/apache/lucene/index/DocumentsWriterPerThread$FlushedSegment;

    .prologue
    .line 88
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1, p2}, Lorg/apache/lucene/index/DocumentsWriterFlushQueue$SegmentFlushTicket;->setSegment(Lorg/apache/lucene/index/DocumentsWriterPerThread$FlushedSegment;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 89
    monitor-exit p0

    return-void

    .line 88
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized clear()V
    .locals 2

    .prologue
    .line 159
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;->queue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 160
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;->ticketCount:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 161
    monitor-exit p0

    return-void

    .line 159
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method forcePurge(Lorg/apache/lucene/index/DocumentsWriter;)V
    .locals 2
    .param p1, "writer"    # Lorg/apache/lucene/index/DocumentsWriter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 134
    sget-boolean v0, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 135
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;->purgeLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 137
    :try_start_0
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;->innerPurge(Lorg/apache/lucene/index/DocumentsWriter;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 139
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;->purgeLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 141
    return-void

    .line 138
    :catchall_0
    move-exception v0

    .line 139
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;->purgeLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 140
    throw v0
.end method

.method public getTicketCount()I
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;->ticketCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    return v0
.end method

.method hasTickets()Z
    .locals 3

    .prologue
    .line 97
    sget-boolean v0, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;->ticketCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ticketCount should be >= 0 but was: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;->ticketCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 98
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;->ticketCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method declared-synchronized markTicketFailed(Lorg/apache/lucene/index/DocumentsWriterFlushQueue$SegmentFlushTicket;)V
    .locals 1
    .param p1, "ticket"    # Lorg/apache/lucene/index/DocumentsWriterFlushQueue$SegmentFlushTicket;

    .prologue
    .line 93
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lorg/apache/lucene/index/DocumentsWriterFlushQueue$SegmentFlushTicket;->setFailed()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 94
    monitor-exit p0

    return-void

    .line 93
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method tryPurge(Lorg/apache/lucene/index/DocumentsWriter;)V
    .locals 2
    .param p1, "writer"    # Lorg/apache/lucene/index/DocumentsWriter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 144
    sget-boolean v0, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 145
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;->purgeLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->tryLock()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 147
    :try_start_0
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;->innerPurge(Lorg/apache/lucene/index/DocumentsWriter;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 149
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;->purgeLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 152
    :cond_1
    return-void

    .line 148
    :catchall_0
    move-exception v0

    .line 149
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;->purgeLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 150
    throw v0
.end method
