.class final Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$QueryArrayNode;
.super Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;
.source "DocumentsWriterDeleteQueue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "QueryArrayNode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node",
        "<[",
        "Lorg/apache/lucene/search/Query;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>([Lorg/apache/lucene/search/Query;)V
    .locals 0
    .param p1, "query"    # [Lorg/apache/lucene/search/Query;

    .prologue
    .line 354
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;-><init>(Ljava/lang/Object;)V

    .line 355
    return-void
.end method


# virtual methods
.method apply(Lorg/apache/lucene/index/BufferedDeletes;I)V
    .locals 4
    .param p1, "bufferedDeletes"    # Lorg/apache/lucene/index/BufferedDeletes;
    .param p2, "docIDUpto"    # I

    .prologue
    .line 359
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$QueryArrayNode;->item:Ljava/lang/Object;

    check-cast v1, [Lorg/apache/lucene/search/Query;

    array-length v3, v1

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v3, :cond_0

    .line 362
    return-void

    .line 359
    :cond_0
    aget-object v0, v1, v2

    .line 360
    .local v0, "query":Lorg/apache/lucene/search/Query;
    invoke-virtual {p1, v0, p2}, Lorg/apache/lucene/index/BufferedDeletes;->addQuery(Lorg/apache/lucene/search/Query;I)V

    .line 359
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method
