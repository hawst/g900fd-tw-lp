.class public Lorg/apache/lucene/index/FilterAtomicReader$FilterFields;
.super Lorg/apache/lucene/index/Fields;
.source "FilterAtomicReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/FilterAtomicReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FilterFields"
.end annotation


# instance fields
.field protected final in:Lorg/apache/lucene/index/Fields;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/Fields;)V
    .locals 0
    .param p1, "in"    # Lorg/apache/lucene/index/Fields;

    .prologue
    .line 60
    invoke-direct {p0}, Lorg/apache/lucene/index/Fields;-><init>()V

    .line 61
    iput-object p1, p0, Lorg/apache/lucene/index/FilterAtomicReader$FilterFields;->in:Lorg/apache/lucene/index/Fields;

    .line 62
    return-void
.end method


# virtual methods
.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 66
    iget-object v0, p0, Lorg/apache/lucene/index/FilterAtomicReader$FilterFields;->in:Lorg/apache/lucene/index/Fields;

    invoke-virtual {v0}, Lorg/apache/lucene/index/Fields;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lorg/apache/lucene/index/FilterAtomicReader$FilterFields;->in:Lorg/apache/lucene/index/Fields;

    invoke-virtual {v0}, Lorg/apache/lucene/index/Fields;->size()I

    move-result v0

    return v0
.end method

.method public terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;
    .locals 1
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 71
    iget-object v0, p0, Lorg/apache/lucene/index/FilterAtomicReader$FilterFields;->in:Lorg/apache/lucene/index/Fields;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/Fields;->terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v0

    return-object v0
.end method
