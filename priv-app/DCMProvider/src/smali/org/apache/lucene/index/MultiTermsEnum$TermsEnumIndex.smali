.class Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumIndex;
.super Ljava/lang/Object;
.source "MultiTermsEnum.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/MultiTermsEnum;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "TermsEnumIndex"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumIndex;


# instance fields
.field final subIndex:I

.field final termsEnum:Lorg/apache/lucene/index/TermsEnum;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    new-array v0, v0, [Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumIndex;

    sput-object v0, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumIndex;->EMPTY_ARRAY:[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumIndex;

    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/index/TermsEnum;I)V
    .locals 0
    .param p1, "termsEnum"    # Lorg/apache/lucene/index/TermsEnum;
    .param p2, "subIndex"    # I

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p1, p0, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumIndex;->termsEnum:Lorg/apache/lucene/index/TermsEnum;

    .line 59
    iput p2, p0, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumIndex;->subIndex:I

    .line 60
    return-void
.end method
