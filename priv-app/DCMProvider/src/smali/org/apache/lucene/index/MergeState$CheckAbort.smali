.class public Lorg/apache/lucene/index/MergeState$CheckAbort;
.super Ljava/lang/Object;
.source "MergeState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/MergeState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CheckAbort"
.end annotation


# static fields
.field static final NONE:Lorg/apache/lucene/index/MergeState$CheckAbort;


# instance fields
.field private final dir:Lorg/apache/lucene/store/Directory;

.field private final merge:Lorg/apache/lucene/index/MergePolicy$OneMerge;

.field private workCount:D


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 204
    new-instance v0, Lorg/apache/lucene/index/MergeState$CheckAbort$1;

    invoke-direct {v0, v1, v1}, Lorg/apache/lucene/index/MergeState$CheckAbort$1;-><init>(Lorg/apache/lucene/index/MergePolicy$OneMerge;Lorg/apache/lucene/store/Directory;)V

    sput-object v0, Lorg/apache/lucene/index/MergeState$CheckAbort;->NONE:Lorg/apache/lucene/index/MergeState$CheckAbort;

    .line 209
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/index/MergePolicy$OneMerge;Lorg/apache/lucene/store/Directory;)V
    .locals 0
    .param p1, "merge"    # Lorg/apache/lucene/index/MergePolicy$OneMerge;
    .param p2, "dir"    # Lorg/apache/lucene/store/Directory;

    .prologue
    .line 181
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 182
    iput-object p1, p0, Lorg/apache/lucene/index/MergeState$CheckAbort;->merge:Lorg/apache/lucene/index/MergePolicy$OneMerge;

    .line 183
    iput-object p2, p0, Lorg/apache/lucene/index/MergeState$CheckAbort;->dir:Lorg/apache/lucene/store/Directory;

    .line 184
    return-void
.end method


# virtual methods
.method public work(D)V
    .locals 5
    .param p1, "units"    # D
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/MergePolicy$MergeAbortedException;
        }
    .end annotation

    .prologue
    .line 195
    iget-wide v0, p0, Lorg/apache/lucene/index/MergeState$CheckAbort;->workCount:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Lorg/apache/lucene/index/MergeState$CheckAbort;->workCount:D

    .line 196
    iget-wide v0, p0, Lorg/apache/lucene/index/MergeState$CheckAbort;->workCount:D

    const-wide v2, 0x40c3880000000000L    # 10000.0

    cmpl-double v0, v0, v2

    if-ltz v0, :cond_0

    .line 197
    iget-object v0, p0, Lorg/apache/lucene/index/MergeState$CheckAbort;->merge:Lorg/apache/lucene/index/MergePolicy$OneMerge;

    iget-object v1, p0, Lorg/apache/lucene/index/MergeState$CheckAbort;->dir:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->checkAborted(Lorg/apache/lucene/store/Directory;)V

    .line 198
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/apache/lucene/index/MergeState$CheckAbort;->workCount:D

    .line 200
    :cond_0
    return-void
.end method
