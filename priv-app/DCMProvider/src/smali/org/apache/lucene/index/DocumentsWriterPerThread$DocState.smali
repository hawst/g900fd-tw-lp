.class Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;
.super Ljava/lang/Object;
.source "DocumentsWriterPerThread.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/DocumentsWriterPerThread;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "DocState"
.end annotation


# instance fields
.field analyzer:Lorg/apache/lucene/analysis/Analyzer;

.field doc:Ljava/lang/Iterable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Iterable",
            "<+",
            "Lorg/apache/lucene/index/IndexableField;",
            ">;"
        }
    .end annotation
.end field

.field docID:I

.field final docWriter:Lorg/apache/lucene/index/DocumentsWriterPerThread;

.field infoStream:Lorg/apache/lucene/util/InfoStream;

.field maxTermPrefix:Ljava/lang/String;

.field similarity:Lorg/apache/lucene/search/similarities/Similarity;


# direct methods
.method constructor <init>(Lorg/apache/lucene/index/DocumentsWriterPerThread;Lorg/apache/lucene/util/InfoStream;)V
    .locals 0
    .param p1, "docWriter"    # Lorg/apache/lucene/index/DocumentsWriterPerThread;
    .param p2, "infoStream"    # Lorg/apache/lucene/util/InfoStream;

    .prologue
    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 109
    iput-object p1, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;->docWriter:Lorg/apache/lucene/index/DocumentsWriterPerThread;

    .line 110
    iput-object p2, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;->infoStream:Lorg/apache/lucene/util/InfoStream;

    .line 111
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 121
    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;->doc:Ljava/lang/Iterable;

    .line 122
    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    .line 123
    return-void
.end method

.method public testPoint(Ljava/lang/String;)Z
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 115
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;->docWriter:Lorg/apache/lucene/index/DocumentsWriterPerThread;

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/IndexWriter;->testPoint(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
