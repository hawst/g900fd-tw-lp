.class final Lorg/apache/lucene/index/DocumentsWriterFlushControl;
.super Ljava/lang/Object;
.source "DocumentsWriterFlushControl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/DocumentsWriterFlushControl$BlockedFlush;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private activeBytes:J

.field private final blockedFlushes:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lorg/apache/lucene/index/DocumentsWriterFlushControl$BlockedFlush;",
            ">;"
        }
    .end annotation
.end field

.field private closed:Z

.field private final config:Lorg/apache/lucene/index/LiveIndexWriterConfig;

.field private final documentsWriter:Lorg/apache/lucene/index/DocumentsWriter;

.field private flushBytes:J

.field final flushDeletes:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final flushPolicy:Lorg/apache/lucene/index/FlushPolicy;

.field private final flushQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lorg/apache/lucene/index/DocumentsWriterPerThread;",
            ">;"
        }
    .end annotation
.end field

.field private final flushingWriters:Ljava/util/IdentityHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/IdentityHashMap",
            "<",
            "Lorg/apache/lucene/index/DocumentsWriterPerThread;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private fullFlush:Z

.field private final fullFlushBuffer:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/DocumentsWriterPerThread;",
            ">;"
        }
    .end annotation
.end field

.field private final hardMaxBytesPerDWPT:J

.field maxConfiguredRamBuffer:D

.field private numDocsSinceStalled:I

.field private volatile numPending:I

.field peakActiveBytes:J

.field peakDelta:J

.field peakFlushBytes:J

.field peakNetBytes:J

.field private final perThreadPool:Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;

.field final stallControl:Lorg/apache/lucene/index/DocumentsWriterStallControl;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-class v0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/index/DocumentsWriter;Lorg/apache/lucene/index/LiveIndexWriterConfig;)V
    .locals 5
    .param p1, "documentsWriter"    # Lorg/apache/lucene/index/DocumentsWriter;
    .param p2, "config"    # Lorg/apache/lucene/index/LiveIndexWriterConfig;

    .prologue
    const/4 v4, 0x0

    const-wide/16 v2, 0x0

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-wide v2, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->activeBytes:J

    .line 47
    iput-wide v2, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->flushBytes:J

    .line 48
    iput v4, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->numPending:I

    .line 49
    iput v4, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->numDocsSinceStalled:I

    .line 50
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->flushDeletes:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 51
    iput-boolean v4, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->fullFlush:Z

    .line 52
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->flushQueue:Ljava/util/Queue;

    .line 54
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->blockedFlushes:Ljava/util/Queue;

    .line 55
    new-instance v0, Ljava/util/IdentityHashMap;

    invoke-direct {v0}, Ljava/util/IdentityHashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->flushingWriters:Ljava/util/IdentityHashMap;

    .line 58
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->maxConfiguredRamBuffer:D

    .line 59
    iput-wide v2, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->peakActiveBytes:J

    .line 60
    iput-wide v2, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->peakFlushBytes:J

    .line 61
    iput-wide v2, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->peakNetBytes:J

    .line 62
    iput-wide v2, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->peakDelta:J

    .line 66
    iput-boolean v4, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->closed:Z

    .line 525
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->fullFlushBuffer:Ljava/util/List;

    .line 71
    new-instance v0, Lorg/apache/lucene/index/DocumentsWriterStallControl;

    invoke-direct {v0}, Lorg/apache/lucene/index/DocumentsWriterStallControl;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->stallControl:Lorg/apache/lucene/index/DocumentsWriterStallControl;

    .line 72
    iget-object v0, p1, Lorg/apache/lucene/index/DocumentsWriter;->perThreadPool:Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->perThreadPool:Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;

    .line 73
    iget-object v0, p1, Lorg/apache/lucene/index/DocumentsWriter;->flushPolicy:Lorg/apache/lucene/index/FlushPolicy;

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->flushPolicy:Lorg/apache/lucene/index/FlushPolicy;

    .line 74
    invoke-virtual {p2}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getRAMPerThreadHardLimitMB()I

    move-result v0

    mul-int/lit16 v0, v0, 0x400

    mul-int/lit16 v0, v0, 0x400

    int-to-long v0, v0

    iput-wide v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->hardMaxBytesPerDWPT:J

    .line 75
    iput-object p2, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->config:Lorg/apache/lucene/index/LiveIndexWriterConfig;

    .line 76
    iput-object p1, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->documentsWriter:Lorg/apache/lucene/index/DocumentsWriter;

    .line 77
    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/index/DocumentsWriterFlushControl;)Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->perThreadPool:Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;

    return-object v0
.end method

.method private assertActiveDeleteQueue(Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;)Z
    .locals 4
    .param p1, "queue"    # Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    .prologue
    .line 512
    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->perThreadPool:Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;

    invoke-virtual {v3}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->getActiveThreadState()I

    move-result v1

    .line 513
    .local v1, "limit":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v1, :cond_0

    .line 522
    const/4 v3, 0x1

    return v3

    .line 514
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->perThreadPool:Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;

    invoke-virtual {v3, v0}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->getThreadState(I)Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;

    move-result-object v2

    .line 515
    .local v2, "next":Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;
    invoke-virtual {v2}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->lock()V

    .line 517
    :try_start_0
    sget-boolean v3, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->$assertionsDisabled:Z

    if-nez v3, :cond_1

    invoke-virtual {v2}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->isActive()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, v2, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->dwpt:Lorg/apache/lucene/index/DocumentsWriterPerThread;

    iget-object v3, v3, Lorg/apache/lucene/index/DocumentsWriterPerThread;->deleteQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    if-eq v3, p1, :cond_1

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 518
    :catchall_0
    move-exception v3

    .line 519
    invoke-virtual {v2}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->unlock()V

    .line 520
    throw v3

    .line 519
    :cond_1
    invoke-virtual {v2}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->unlock()V

    .line 513
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private assertMemory()Z
    .locals 15

    .prologue
    const/4 v14, 0x1

    const-wide/high16 v12, 0x4090000000000000L    # 1024.0

    .line 97
    iget-object v8, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->config:Lorg/apache/lucene/index/LiveIndexWriterConfig;

    invoke-virtual {v8}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getRAMBufferSizeMB()D

    move-result-wide v2

    .line 98
    .local v2, "maxRamMB":D
    const-wide/high16 v8, -0x4010000000000000L    # -1.0

    cmpl-double v8, v2, v8

    if-eqz v8, :cond_0

    .line 100
    iget-wide v8, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->maxConfiguredRamBuffer:D

    invoke-static {v2, v3, v8, v9}, Ljava/lang/Math;->max(DD)D

    move-result-wide v8

    iput-wide v8, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->maxConfiguredRamBuffer:D

    .line 101
    iget-wide v8, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->flushBytes:J

    iget-wide v10, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->activeBytes:J

    add-long v4, v8, v10

    .line 102
    .local v4, "ram":J
    iget-wide v8, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->maxConfiguredRamBuffer:D

    mul-double/2addr v8, v12

    mul-double/2addr v8, v12

    double-to-long v6, v8

    .line 109
    .local v6, "ramBufferBytes":J
    const-wide/16 v8, 0x2

    mul-long/2addr v8, v6

    iget v10, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->numPending:I

    invoke-virtual {p0}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->numFlushingDWPT()I

    move-result v11

    add-int/2addr v10, v11

    invoke-virtual {p0}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->numBlockedFlushes()I

    move-result v11

    add-int/2addr v10, v11

    int-to-long v10, v10

    iget-wide v12, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->peakDelta:J

    mul-long/2addr v10, v12

    add-long/2addr v8, v10

    iget v10, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->numDocsSinceStalled:I

    int-to-long v10, v10

    iget-wide v12, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->peakDelta:J

    mul-long/2addr v10, v12

    add-long v0, v8, v10

    .line 111
    .local v0, "expected":J
    iget-wide v8, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->peakDelta:J

    shr-long v10, v6, v14

    cmp-long v8, v8, v10

    if-gez v8, :cond_0

    .line 121
    sget-boolean v8, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->$assertionsDisabled:Z

    if-nez v8, :cond_0

    cmp-long v8, v4, v0

    if-lez v8, :cond_0

    new-instance v8, Ljava/lang/AssertionError;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "actual mem: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " byte, expected mem: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 122
    const-string v10, " byte, flush mem: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-wide v10, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->flushBytes:J

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", active mem: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-wide v10, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->activeBytes:J

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 123
    const-string v10, ", pending DWPT: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->numPending:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", flushing DWPT: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 124
    invoke-virtual {p0}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->numFlushingDWPT()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", blocked DWPT: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p0}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->numBlockedFlushes()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 125
    const-string v10, ", peakDelta mem: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-wide v10, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->peakDelta:J

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " byte"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v8

    .line 128
    .end local v0    # "expected":J
    .end local v4    # "ram":J
    .end local v6    # "ramBufferBytes":J
    :cond_0
    return v14
.end method

.method private assertNumDocsSinceStalled(Z)Z
    .locals 1
    .param p1, "stalled"    # Z

    .prologue
    .line 200
    if-eqz p1, :cond_0

    .line 201
    iget v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->numDocsSinceStalled:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->numDocsSinceStalled:I

    .line 205
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 203
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->numDocsSinceStalled:I

    goto :goto_0
.end method

.method private checkoutAndBlock(Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;)V
    .locals 5
    .param p1, "perThread"    # Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;

    .prologue
    .line 291
    invoke-virtual {p1}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->lock()V

    .line 293
    :try_start_0
    sget-boolean v3, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    iget-boolean v3, p1, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->flushPending:Z

    if-nez v3, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    const-string v4, "can not block non-pending threadstate"

    invoke-direct {v3, v4}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 300
    :catchall_0
    move-exception v3

    .line 301
    invoke-virtual {p1}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->unlock()V

    .line 302
    throw v3

    .line 294
    :cond_0
    :try_start_1
    sget-boolean v3, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->$assertionsDisabled:Z

    if-nez v3, :cond_1

    iget-boolean v3, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->fullFlush:Z

    if-nez v3, :cond_1

    new-instance v3, Ljava/lang/AssertionError;

    const-string v4, "can not block if fullFlush == false"

    invoke-direct {v3, v4}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 296
    :cond_1
    iget-wide v0, p1, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->bytesUsed:J

    .line 297
    .local v0, "bytes":J
    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->perThreadPool:Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;

    iget-boolean v4, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->closed:Z

    invoke-virtual {v3, p1, v4}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->replaceForFlush(Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;Z)Lorg/apache/lucene/index/DocumentsWriterPerThread;

    move-result-object v2

    .line 298
    .local v2, "dwpt":Lorg/apache/lucene/index/DocumentsWriterPerThread;
    iget v3, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->numPending:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->numPending:I

    .line 299
    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->blockedFlushes:Ljava/util/Queue;

    new-instance v4, Lorg/apache/lucene/index/DocumentsWriterFlushControl$BlockedFlush;

    invoke-direct {v4, v2, v0, v1}, Lorg/apache/lucene/index/DocumentsWriterFlushControl$BlockedFlush;-><init>(Lorg/apache/lucene/index/DocumentsWriterPerThread;J)V

    invoke-interface {v3, v4}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 301
    invoke-virtual {p1}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->unlock()V

    .line 303
    return-void
.end method

.method private commitPerThreadBytes(Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;)V
    .locals 6
    .param p1, "perThread"    # Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;

    .prologue
    .line 132
    iget-object v2, p1, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->dwpt:Lorg/apache/lucene/index/DocumentsWriterPerThread;

    invoke-virtual {v2}, Lorg/apache/lucene/index/DocumentsWriterPerThread;->bytesUsed()J

    move-result-wide v2

    .line 133
    iget-wide v4, p1, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->bytesUsed:J

    .line 132
    sub-long v0, v2, v4

    .line 134
    .local v0, "delta":J
    iget-wide v2, p1, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->bytesUsed:J

    add-long/2addr v2, v0

    iput-wide v2, p1, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->bytesUsed:J

    .line 140
    iget-boolean v2, p1, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->flushPending:Z

    if-eqz v2, :cond_0

    .line 141
    iget-wide v2, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->flushBytes:J

    add-long/2addr v2, v0

    iput-wide v2, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->flushBytes:J

    .line 145
    :goto_0
    sget-boolean v2, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->$assertionsDisabled:Z

    if-nez v2, :cond_1

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->updatePeaks(J)Z

    move-result v2

    if-nez v2, :cond_1

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 143
    :cond_0
    iget-wide v2, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->activeBytes:J

    add-long/2addr v2, v0

    iput-wide v2, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->activeBytes:J

    goto :goto_0

    .line 146
    :cond_1
    return-void
.end method

.method private getPerThreadsIterator(I)Ljava/util/Iterator;
    .locals 1
    .param p1, "upto"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/Iterator",
            "<",
            "Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;",
            ">;"
        }
    .end annotation

    .prologue
    .line 384
    new-instance v0, Lorg/apache/lucene/index/DocumentsWriterFlushControl$1;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/index/DocumentsWriterFlushControl$1;-><init>(Lorg/apache/lucene/index/DocumentsWriterFlushControl;I)V

    return-object v0
.end method

.method private internalTryCheckOutForFlush(Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;)Lorg/apache/lucene/index/DocumentsWriterPerThread;
    .locals 5
    .param p1, "perThread"    # Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;

    .prologue
    .line 307
    sget-boolean v3, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 308
    :cond_0
    sget-boolean v3, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->$assertionsDisabled:Z

    if-nez v3, :cond_1

    iget-boolean v3, p1, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->flushPending:Z

    if-nez v3, :cond_1

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 311
    :cond_1
    :try_start_0
    invoke-virtual {p1}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->tryLock()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v3

    if-eqz v3, :cond_5

    .line 313
    :try_start_1
    invoke-virtual {p1}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->isActive()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 314
    sget-boolean v3, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->$assertionsDisabled:Z

    if-nez v3, :cond_2

    invoke-virtual {p1}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->isHeldByCurrentThread()Z

    move-result v3

    if-nez v3, :cond_2

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 325
    :catchall_0
    move-exception v3

    .line 326
    :try_start_2
    invoke-virtual {p1}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->unlock()V

    .line 327
    throw v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 330
    :catchall_1
    move-exception v3

    .line 331
    invoke-direct {p0}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->updateStallState()Z

    .line 332
    throw v3

    .line 316
    :cond_2
    :try_start_3
    iget-wide v0, p1, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->bytesUsed:J

    .line 318
    .local v0, "bytes":J
    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->perThreadPool:Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;

    iget-boolean v4, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->closed:Z

    invoke-virtual {v3, p1, v4}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->replaceForFlush(Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;Z)Lorg/apache/lucene/index/DocumentsWriterPerThread;

    move-result-object v2

    .line 319
    .local v2, "dwpt":Lorg/apache/lucene/index/DocumentsWriterPerThread;
    sget-boolean v3, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->$assertionsDisabled:Z

    if-nez v3, :cond_3

    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->flushingWriters:Ljava/util/IdentityHashMap;

    invoke-virtual {v3, v2}, Ljava/util/IdentityHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    new-instance v3, Ljava/lang/AssertionError;

    const-string v4, "DWPT is already flushing"

    invoke-direct {v3, v4}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 321
    :cond_3
    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->flushingWriters:Ljava/util/IdentityHashMap;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Ljava/util/IdentityHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 322
    iget v3, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->numPending:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->numPending:I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 326
    :try_start_4
    invoke-virtual {p1}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->unlock()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 331
    invoke-direct {p0}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->updateStallState()Z

    .line 329
    .end local v0    # "bytes":J
    .end local v2    # "dwpt":Lorg/apache/lucene/index/DocumentsWriterPerThread;
    :goto_0
    return-object v2

    .line 326
    :cond_4
    :try_start_5
    invoke-virtual {p1}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->unlock()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 331
    :cond_5
    invoke-direct {p0}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->updateStallState()Z

    .line 329
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private pruneBlockedQueue(Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;)V
    .locals 6
    .param p1, "flushingQueue"    # Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    .prologue
    .line 559
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->blockedFlushes:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 560
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/index/DocumentsWriterFlushControl$BlockedFlush;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 571
    return-void

    .line 561
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/DocumentsWriterFlushControl$BlockedFlush;

    .line 562
    .local v0, "blockedFlush":Lorg/apache/lucene/index/DocumentsWriterFlushControl$BlockedFlush;
    iget-object v2, v0, Lorg/apache/lucene/index/DocumentsWriterFlushControl$BlockedFlush;->dwpt:Lorg/apache/lucene/index/DocumentsWriterPerThread;

    iget-object v2, v2, Lorg/apache/lucene/index/DocumentsWriterPerThread;->deleteQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    if-ne v2, p1, :cond_0

    .line 563
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 564
    sget-boolean v2, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->$assertionsDisabled:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->flushingWriters:Ljava/util/IdentityHashMap;

    iget-object v3, v0, Lorg/apache/lucene/index/DocumentsWriterFlushControl$BlockedFlush;->dwpt:Lorg/apache/lucene/index/DocumentsWriterPerThread;

    invoke-virtual {v2, v3}, Ljava/util/IdentityHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    new-instance v2, Ljava/lang/AssertionError;

    const-string v3, "DWPT is already flushing"

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 566
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->flushingWriters:Ljava/util/IdentityHashMap;

    iget-object v3, v0, Lorg/apache/lucene/index/DocumentsWriterFlushControl$BlockedFlush;->dwpt:Lorg/apache/lucene/index/DocumentsWriterPerThread;

    iget-wide v4, v0, Lorg/apache/lucene/index/DocumentsWriterFlushControl$BlockedFlush;->bytes:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/IdentityHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 568
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->flushQueue:Ljava/util/Queue;

    iget-object v3, v0, Lorg/apache/lucene/index/DocumentsWriterFlushControl$BlockedFlush;->dwpt:Lorg/apache/lucene/index/DocumentsWriterPerThread;

    invoke-interface {v2, v3}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private stallLimitBytes()J
    .locals 8

    .prologue
    const-wide/high16 v6, 0x4090000000000000L    # 1024.0

    .line 92
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->config:Lorg/apache/lucene/index/LiveIndexWriterConfig;

    invoke-virtual {v2}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getRAMBufferSizeMB()D

    move-result-wide v0

    .line 93
    .local v0, "maxRamMB":D
    const-wide/high16 v2, -0x4010000000000000L    # -1.0

    cmpl-double v2, v0, v2

    if-eqz v2, :cond_0

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    mul-double v4, v0, v6

    mul-double/2addr v4, v6

    mul-double/2addr v2, v4

    double-to-long v2, v2

    :goto_0
    return-wide v2

    :cond_0
    const-wide v2, 0x7fffffffffffffffL

    goto :goto_0
.end method

.method private updatePeaks(J)Z
    .locals 5
    .param p1, "delta"    # J

    .prologue
    .line 150
    iget-wide v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->peakActiveBytes:J

    iget-wide v2, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->activeBytes:J

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->peakActiveBytes:J

    .line 151
    iget-wide v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->peakFlushBytes:J

    iget-wide v2, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->flushBytes:J

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->peakFlushBytes:J

    .line 152
    iget-wide v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->peakNetBytes:J

    invoke-virtual {p0}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->netBytes()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->peakNetBytes:J

    .line 153
    iget-wide v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->peakDelta:J

    invoke-static {v0, v1, p1, p2}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->peakDelta:J

    .line 155
    const/4 v0, 0x1

    return v0
.end method

.method private final updateStallState()Z
    .locals 8

    .prologue
    .line 226
    sget-boolean v3, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 227
    :cond_0
    invoke-direct {p0}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->stallLimitBytes()J

    move-result-wide v0

    .line 235
    .local v0, "limit":J
    iget-wide v4, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->activeBytes:J

    iget-wide v6, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->flushBytes:J

    add-long/2addr v4, v6

    cmp-long v3, v4, v0

    if-lez v3, :cond_1

    .line 236
    iget-wide v4, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->activeBytes:J

    cmp-long v3, v4, v0

    if-gez v3, :cond_1

    .line 237
    iget-boolean v3, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->closed:Z

    if-nez v3, :cond_1

    .line 235
    const/4 v2, 0x1

    .line 238
    .local v2, "stall":Z
    :goto_0
    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->stallControl:Lorg/apache/lucene/index/DocumentsWriterStallControl;

    invoke-virtual {v3, v2}, Lorg/apache/lucene/index/DocumentsWriterStallControl;->updateStalled(Z)V

    .line 239
    return v2

    .line 235
    .end local v2    # "stall":Z
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method declared-synchronized abortFullFlushes()V
    .locals 2

    .prologue
    .line 598
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->abortPendingFlushes()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 600
    const/4 v0, 0x0

    :try_start_1
    iput-boolean v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->fullFlush:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 602
    monitor-exit p0

    return-void

    .line 599
    :catchall_0
    move-exception v0

    .line 600
    const/4 v1, 0x0

    :try_start_2
    iput-boolean v1, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->fullFlush:Z

    .line 601
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 598
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized abortPendingFlushes()V
    .locals 8

    .prologue
    .line 606
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->flushQueue:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 614
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->blockedFlushes:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_1

    .line 625
    :try_start_1
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->flushQueue:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->clear()V

    .line 626
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->blockedFlushes:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->clear()V

    .line 627
    invoke-direct {p0}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->updateStallState()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 629
    monitor-exit p0

    return-void

    .line 606
    :cond_0
    :try_start_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/DocumentsWriterPerThread;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 608
    .local v1, "dwpt":Lorg/apache/lucene/index/DocumentsWriterPerThread;
    :try_start_3
    invoke-virtual {v1}, Lorg/apache/lucene/index/DocumentsWriterPerThread;->abort()V

    .line 609
    invoke-virtual {p0, v1}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->doAfterFlush(Lorg/apache/lucene/index/DocumentsWriterPerThread;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 610
    :catch_0
    move-exception v3

    goto :goto_0

    .line 614
    .end local v1    # "dwpt":Lorg/apache/lucene/index/DocumentsWriterPerThread;
    :cond_1
    :try_start_4
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/DocumentsWriterFlushControl$BlockedFlush;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 616
    .local v0, "blockedFlush":Lorg/apache/lucene/index/DocumentsWriterFlushControl$BlockedFlush;
    :try_start_5
    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->flushingWriters:Ljava/util/IdentityHashMap;

    .line 617
    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriterFlushControl$BlockedFlush;->dwpt:Lorg/apache/lucene/index/DocumentsWriterPerThread;

    iget-wide v6, v0, Lorg/apache/lucene/index/DocumentsWriterFlushControl$BlockedFlush;->bytes:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/IdentityHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 618
    iget-object v3, v0, Lorg/apache/lucene/index/DocumentsWriterFlushControl$BlockedFlush;->dwpt:Lorg/apache/lucene/index/DocumentsWriterPerThread;

    invoke-virtual {v3}, Lorg/apache/lucene/index/DocumentsWriterPerThread;->abort()V

    .line 619
    iget-object v3, v0, Lorg/apache/lucene/index/DocumentsWriterFlushControl$BlockedFlush;->dwpt:Lorg/apache/lucene/index/DocumentsWriterPerThread;

    invoke-virtual {p0, v3}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->doAfterFlush(Lorg/apache/lucene/index/DocumentsWriterPerThread;)V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    .line 620
    :catch_1
    move-exception v3

    goto :goto_1

    .line 624
    .end local v0    # "blockedFlush":Lorg/apache/lucene/index/DocumentsWriterFlushControl$BlockedFlush;
    :catchall_0
    move-exception v2

    .line 625
    :try_start_6
    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->flushQueue:Ljava/util/Queue;

    invoke-interface {v3}, Ljava/util/Queue;->clear()V

    .line 626
    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->blockedFlushes:Ljava/util/Queue;

    invoke-interface {v3}, Ljava/util/Queue;->clear()V

    .line 627
    invoke-direct {p0}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->updateStallState()Z

    .line 628
    throw v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 606
    :catchall_1
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized activeBytes()J
    .locals 2

    .prologue
    .line 80
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->activeBytes:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method addFlushableState(Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;)V
    .locals 6
    .param p1, "perThread"    # Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;

    .prologue
    .line 528
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->documentsWriter:Lorg/apache/lucene/index/DocumentsWriter;

    iget-object v2, v2, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v3, "DWFC"

    invoke-virtual {v2, v3}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 529
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->documentsWriter:Lorg/apache/lucene/index/DocumentsWriter;

    iget-object v2, v2, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v3, "DWFC"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "addFlushableState "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p1, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->dwpt:Lorg/apache/lucene/index/DocumentsWriterPerThread;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 531
    :cond_0
    iget-object v0, p1, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->dwpt:Lorg/apache/lucene/index/DocumentsWriterPerThread;

    .line 532
    .local v0, "dwpt":Lorg/apache/lucene/index/DocumentsWriterPerThread;
    sget-boolean v2, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->$assertionsDisabled:Z

    if-nez v2, :cond_1

    invoke-virtual {p1}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->isHeldByCurrentThread()Z

    move-result v2

    if-nez v2, :cond_1

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 533
    :cond_1
    sget-boolean v2, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->$assertionsDisabled:Z

    if-nez v2, :cond_2

    invoke-virtual {p1}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->isActive()Z

    move-result v2

    if-nez v2, :cond_2

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 534
    :cond_2
    sget-boolean v2, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->$assertionsDisabled:Z

    if-nez v2, :cond_3

    iget-boolean v2, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->fullFlush:Z

    if-nez v2, :cond_3

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 535
    :cond_3
    sget-boolean v2, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->$assertionsDisabled:Z

    if-nez v2, :cond_4

    iget-object v2, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->deleteQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->documentsWriter:Lorg/apache/lucene/index/DocumentsWriter;

    iget-object v3, v3, Lorg/apache/lucene/index/DocumentsWriter;->deleteQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    if-ne v2, v3, :cond_4

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 536
    :cond_4
    invoke-virtual {v0}, Lorg/apache/lucene/index/DocumentsWriterPerThread;->getNumDocsInRAM()I

    move-result v2

    if-lez v2, :cond_8

    .line 537
    monitor-enter p0

    .line 538
    :try_start_0
    iget-boolean v2, p1, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->flushPending:Z

    if-nez v2, :cond_5

    .line 539
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->setFlushPending(Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;)V

    .line 541
    :cond_5
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->internalTryCheckOutForFlush(Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;)Lorg/apache/lucene/index/DocumentsWriterPerThread;

    move-result-object v1

    .line 542
    .local v1, "flushingDWPT":Lorg/apache/lucene/index/DocumentsWriterPerThread;
    sget-boolean v2, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->$assertionsDisabled:Z

    if-nez v2, :cond_6

    if-nez v1, :cond_6

    new-instance v2, Ljava/lang/AssertionError;

    const-string v3, "DWPT must never be null here since we hold the lock and it holds documents"

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 537
    .end local v1    # "flushingDWPT":Lorg/apache/lucene/index/DocumentsWriterPerThread;
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 543
    .restart local v1    # "flushingDWPT":Lorg/apache/lucene/index/DocumentsWriterPerThread;
    :cond_6
    :try_start_1
    sget-boolean v2, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->$assertionsDisabled:Z

    if-nez v2, :cond_7

    if-eq v0, v1, :cond_7

    new-instance v2, Ljava/lang/AssertionError;

    const-string v3, "flushControl returned different DWPT"

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 544
    :cond_7
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->fullFlushBuffer:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 537
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 553
    .end local v1    # "flushingDWPT":Lorg/apache/lucene/index/DocumentsWriterPerThread;
    :goto_0
    return-void

    .line 547
    :cond_8
    iget-boolean v2, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->closed:Z

    if-eqz v2, :cond_9

    .line 548
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->perThreadPool:Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->deactivateThreadState(Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;)V

    goto :goto_0

    .line 550
    :cond_9
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->perThreadPool:Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->reinitThreadState(Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;)V

    goto :goto_0
.end method

.method public allActiveThreadStates()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;",
            ">;"
        }
    .end annotation

    .prologue
    .line 380
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->perThreadPool:Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->getActiveThreadState()I

    move-result v0

    invoke-direct {p0, v0}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->getPerThreadsIterator(I)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method anyStalledThreads()Z
    .locals 1

    .prologue
    .line 683
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->stallControl:Lorg/apache/lucene/index/DocumentsWriterStallControl;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocumentsWriterStallControl;->anyStalledThreads()Z

    move-result v0

    return v0
.end method

.method assertBlockedFlushes(Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;)Z
    .locals 3
    .param p1, "flushingQueue"    # Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    .prologue
    .line 590
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->blockedFlushes:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 593
    const/4 v1, 0x1

    return v1

    .line 590
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/DocumentsWriterFlushControl$BlockedFlush;

    .line 591
    .local v0, "blockedFlush":Lorg/apache/lucene/index/DocumentsWriterFlushControl$BlockedFlush;
    sget-boolean v2, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    iget-object v2, v0, Lorg/apache/lucene/index/DocumentsWriterFlushControl$BlockedFlush;->dwpt:Lorg/apache/lucene/index/DocumentsWriterPerThread;

    iget-object v2, v2, Lorg/apache/lucene/index/DocumentsWriterPerThread;->deleteQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    if-eq v2, p1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1
.end method

.method declared-synchronized doAfterDocument(Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;Z)Lorg/apache/lucene/index/DocumentsWriterPerThread;
    .locals 6
    .param p1, "perThread"    # Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;
    .param p2, "isUpdate"    # Z

    .prologue
    .line 161
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->commitPerThreadBytes(Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;)V

    .line 162
    iget-boolean v2, p1, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->flushPending:Z

    if-nez v2, :cond_0

    .line 163
    if-eqz p2, :cond_2

    .line 164
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->flushPolicy:Lorg/apache/lucene/index/FlushPolicy;

    invoke-virtual {v2, p0, p1}, Lorg/apache/lucene/index/FlushPolicy;->onUpdate(Lorg/apache/lucene/index/DocumentsWriterFlushControl;Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;)V

    .line 168
    :goto_0
    iget-boolean v2, p1, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->flushPending:Z

    if-nez v2, :cond_0

    iget-wide v2, p1, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->bytesUsed:J

    iget-wide v4, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->hardMaxBytesPerDWPT:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 171
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->setFlushPending(Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;)V

    .line 175
    :cond_0
    iget-boolean v2, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->fullFlush:Z

    if-eqz v2, :cond_5

    .line 176
    iget-boolean v2, p1, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->flushPending:Z

    if-eqz v2, :cond_4

    .line 177
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->checkoutAndBlock(Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;)V

    .line 178
    invoke-virtual {p0}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->nextPendingFlush()Lorg/apache/lucene/index/DocumentsWriterPerThread;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v0

    .line 187
    .local v0, "flushingDWPT":Lorg/apache/lucene/index/DocumentsWriterPerThread;
    :goto_1
    :try_start_1
    invoke-direct {p0}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->updateStallState()Z

    move-result v1

    .line 188
    .local v1, "stalled":Z
    sget-boolean v2, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->$assertionsDisabled:Z

    if-nez v2, :cond_7

    invoke-direct {p0, v1}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->assertNumDocsSinceStalled(Z)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-direct {p0}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->assertMemory()Z

    move-result v2

    if-nez v2, :cond_7

    :cond_1
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 161
    .end local v0    # "flushingDWPT":Lorg/apache/lucene/index/DocumentsWriterPerThread;
    .end local v1    # "stalled":Z
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 166
    :cond_2
    :try_start_2
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->flushPolicy:Lorg/apache/lucene/index/FlushPolicy;

    invoke-virtual {v2, p0, p1}, Lorg/apache/lucene/index/FlushPolicy;->onInsert(Lorg/apache/lucene/index/DocumentsWriterFlushControl;Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 186
    :catchall_1
    move-exception v2

    .line 187
    :try_start_3
    invoke-direct {p0}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->updateStallState()Z

    move-result v1

    .line 188
    .restart local v1    # "stalled":Z
    sget-boolean v3, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->$assertionsDisabled:Z

    if-nez v3, :cond_6

    invoke-direct {p0, v1}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->assertNumDocsSinceStalled(Z)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-direct {p0}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->assertMemory()Z

    move-result v3

    if-nez v3, :cond_6

    :cond_3
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 180
    .end local v1    # "stalled":Z
    :cond_4
    const/4 v0, 0x0

    .line 182
    .restart local v0    # "flushingDWPT":Lorg/apache/lucene/index/DocumentsWriterPerThread;
    goto :goto_1

    .line 183
    .end local v0    # "flushingDWPT":Lorg/apache/lucene/index/DocumentsWriterPerThread;
    :cond_5
    :try_start_4
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->tryCheckoutForFlush(Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;)Lorg/apache/lucene/index/DocumentsWriterPerThread;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result-object v0

    .restart local v0    # "flushingDWPT":Lorg/apache/lucene/index/DocumentsWriterPerThread;
    goto :goto_1

    .line 189
    .end local v0    # "flushingDWPT":Lorg/apache/lucene/index/DocumentsWriterPerThread;
    .restart local v1    # "stalled":Z
    :cond_6
    :try_start_5
    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 185
    .restart local v0    # "flushingDWPT":Lorg/apache/lucene/index/DocumentsWriterPerThread;
    :cond_7
    monitor-exit p0

    return-object v0
.end method

.method declared-synchronized doAfterFlush(Lorg/apache/lucene/index/DocumentsWriterPerThread;)V
    .locals 6
    .param p1, "dwpt"    # Lorg/apache/lucene/index/DocumentsWriterPerThread;

    .prologue
    .line 209
    monitor-enter p0

    :try_start_0
    sget-boolean v1, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->flushingWriters:Ljava/util/IdentityHashMap;

    invoke-virtual {v1, p1}, Ljava/util/IdentityHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 211
    :cond_0
    :try_start_1
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->flushingWriters:Ljava/util/IdentityHashMap;

    invoke-virtual {v1, p1}, Ljava/util/IdentityHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 212
    .local v0, "bytes":Ljava/lang/Long;
    iget-wide v2, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->flushBytes:J

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sub-long/2addr v2, v4

    iput-wide v2, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->flushBytes:J

    .line 213
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->perThreadPool:Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->recycle(Lorg/apache/lucene/index/DocumentsWriterPerThread;)V

    .line 214
    sget-boolean v1, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    invoke-direct {p0}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->assertMemory()Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 215
    .end local v0    # "bytes":Ljava/lang/Long;
    :catchall_1
    move-exception v1

    .line 217
    :try_start_2
    invoke-direct {p0}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->updateStallState()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 219
    :try_start_3
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 221
    throw v1

    .line 218
    :catchall_2
    move-exception v1

    .line 219
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 220
    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 217
    .restart local v0    # "bytes":Ljava/lang/Long;
    :cond_1
    :try_start_4
    invoke-direct {p0}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->updateStallState()Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    .line 219
    :try_start_5
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 222
    monitor-exit p0

    return-void

    .line 218
    :catchall_3
    move-exception v1

    .line 219
    :try_start_6
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 220
    throw v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0
.end method

.method public doApplyAllDeletes()Z
    .locals 2

    .prologue
    .line 427
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->flushDeletes:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    return v0
.end method

.method declared-synchronized doOnAbort(Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;)V
    .locals 4
    .param p1, "state"    # Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;

    .prologue
    .line 272
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p1, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->flushPending:Z

    if-eqz v0, :cond_0

    .line 273
    iget-wide v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->flushBytes:J

    iget-wide v2, p1, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->bytesUsed:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->flushBytes:J

    .line 277
    :goto_0
    sget-boolean v0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    invoke-direct {p0}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->assertMemory()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 280
    :catchall_0
    move-exception v0

    .line 281
    :try_start_1
    invoke-direct {p0}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->updateStallState()Z

    .line 282
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 272
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 275
    :cond_0
    :try_start_2
    iget-wide v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->activeBytes:J

    iget-wide v2, p1, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->bytesUsed:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->activeBytes:J

    goto :goto_0

    .line 279
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->perThreadPool:Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;

    iget-boolean v1, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->closed:Z

    invoke-virtual {v0, p1, v1}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->replaceForFlush(Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;Z)Lorg/apache/lucene/index/DocumentsWriterPerThread;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 281
    :try_start_3
    invoke-direct {p0}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->updateStallState()Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 283
    monitor-exit p0

    return-void
.end method

.method declared-synchronized doOnDelete()V
    .locals 2

    .prologue
    .line 408
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->flushPolicy:Lorg/apache/lucene/index/FlushPolicy;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lorg/apache/lucene/index/FlushPolicy;->onDelete(Lorg/apache/lucene/index/DocumentsWriterFlushControl;Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 409
    monitor-exit p0

    return-void

    .line 408
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized finishFullFlush()V
    .locals 2

    .prologue
    .line 574
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->fullFlush:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 575
    :cond_0
    :try_start_1
    sget-boolean v0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->flushQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 576
    :cond_1
    sget-boolean v0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->flushingWriters:Ljava/util/IdentityHashMap;

    invoke-virtual {v0}, Ljava/util/IdentityHashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 578
    :cond_2
    :try_start_2
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->blockedFlushes:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 579
    sget-boolean v0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->documentsWriter:Lorg/apache/lucene/index/DocumentsWriter;

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter;->deleteQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->assertBlockedFlushes(Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;)Z

    move-result v0

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 583
    :catchall_1
    move-exception v0

    .line 584
    const/4 v1, 0x0

    :try_start_3
    iput-boolean v1, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->fullFlush:Z

    .line 585
    invoke-direct {p0}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->updateStallState()Z

    .line 586
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 580
    :cond_3
    :try_start_4
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->documentsWriter:Lorg/apache/lucene/index/DocumentsWriter;

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter;->deleteQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    invoke-direct {p0, v0}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->pruneBlockedQueue(Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;)V

    .line 581
    sget-boolean v0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->$assertionsDisabled:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->blockedFlushes:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 584
    :cond_4
    const/4 v0, 0x0

    :try_start_5
    iput-boolean v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->fullFlush:Z

    .line 585
    invoke-direct {p0}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->updateStallState()Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 587
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized flushBytes()J
    .locals 2

    .prologue
    .line 84
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->flushBytes:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getDeleteBytesUsed()J
    .locals 4

    .prologue
    .line 419
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->documentsWriter:Lorg/apache/lucene/index/DocumentsWriter;

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter;->deleteQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->bytesUsed()J

    move-result-wide v0

    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->documentsWriter:Lorg/apache/lucene/index/DocumentsWriter;

    iget-object v2, v2, Lorg/apache/lucene/index/DocumentsWriter;->indexWriter:Lorg/apache/lucene/index/IndexWriter;

    iget-object v2, v2, Lorg/apache/lucene/index/IndexWriter;->bufferedDeletesStream:Lorg/apache/lucene/index/BufferedDeletesStream;

    invoke-virtual {v2}, Lorg/apache/lucene/index/BufferedDeletesStream;->bytesUsed()J

    move-result-wide v2

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public getNumGlobalTermDeletes()I
    .locals 2

    .prologue
    .line 415
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->documentsWriter:Lorg/apache/lucene/index/DocumentsWriter;

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter;->deleteQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->numGlobalTermDeletes()I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->documentsWriter:Lorg/apache/lucene/index/DocumentsWriter;

    iget-object v1, v1, Lorg/apache/lucene/index/DocumentsWriter;->indexWriter:Lorg/apache/lucene/index/IndexWriter;

    iget-object v1, v1, Lorg/apache/lucene/index/IndexWriter;->bufferedDeletesStream:Lorg/apache/lucene/index/BufferedDeletesStream;

    invoke-virtual {v1}, Lorg/apache/lucene/index/BufferedDeletesStream;->numTerms()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method declared-synchronized isFullFlush()Z
    .locals 1

    .prologue
    .line 635
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->fullFlush:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method markForFullFlush()V
    .locals 10

    .prologue
    .line 462
    monitor-enter p0

    .line 463
    :try_start_0
    sget-boolean v5, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->$assertionsDisabled:Z

    if-nez v5, :cond_0

    iget-boolean v5, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->fullFlush:Z

    if-eqz v5, :cond_0

    new-instance v5, Ljava/lang/AssertionError;

    const-string v6, "called DWFC#markForFullFlush() while full flush is still running"

    invoke-direct {v5, v6}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v5

    .line 462
    :catchall_0
    move-exception v5

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5

    .line 464
    :cond_0
    :try_start_1
    sget-boolean v5, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->$assertionsDisabled:Z

    if-nez v5, :cond_1

    iget-object v5, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->fullFlushBuffer:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_1

    new-instance v5, Ljava/lang/AssertionError;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "full flush buffer should be empty: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->fullFlushBuffer:Ljava/util/List;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v5

    .line 465
    :cond_1
    const/4 v5, 0x1

    iput-boolean v5, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->fullFlush:Z

    .line 466
    iget-object v5, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->documentsWriter:Lorg/apache/lucene/index/DocumentsWriter;

    iget-object v0, v5, Lorg/apache/lucene/index/DocumentsWriter;->deleteQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    .line 469
    .local v0, "flushingQueue":Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;
    new-instance v3, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    iget-wide v6, v0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->generation:J

    const-wide/16 v8, 0x1

    add-long/2addr v6, v8

    invoke-direct {v3, v6, v7}, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;-><init>(J)V

    .line 470
    .local v3, "newQueue":Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;
    iget-object v5, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->documentsWriter:Lorg/apache/lucene/index/DocumentsWriter;

    iput-object v3, v5, Lorg/apache/lucene/index/DocumentsWriter;->deleteQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    .line 462
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 472
    iget-object v5, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->perThreadPool:Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;

    invoke-virtual {v5}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->getActiveThreadState()I

    move-result v2

    .line 473
    .local v2, "limit":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v2, :cond_2

    .line 497
    monitor-enter p0

    .line 502
    :try_start_2
    invoke-direct {p0, v0}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->pruneBlockedQueue(Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;)V

    .line 503
    sget-boolean v5, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->$assertionsDisabled:Z

    if-nez v5, :cond_6

    iget-object v5, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->documentsWriter:Lorg/apache/lucene/index/DocumentsWriter;

    iget-object v5, v5, Lorg/apache/lucene/index/DocumentsWriter;->deleteQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    invoke-virtual {p0, v5}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->assertBlockedFlushes(Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;)Z

    move-result v5

    if-nez v5, :cond_6

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 497
    :catchall_1
    move-exception v5

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v5

    .line 474
    :cond_2
    iget-object v5, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->perThreadPool:Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;

    invoke-virtual {v5, v1}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->getThreadState(I)Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;

    move-result-object v4

    .line 475
    .local v4, "next":Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;
    invoke-virtual {v4}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->lock()V

    .line 477
    :try_start_3
    invoke-virtual {v4}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->isActive()Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-result v5

    if-nez v5, :cond_3

    .line 494
    invoke-virtual {v4}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->unlock()V

    .line 473
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 480
    :cond_3
    :try_start_4
    sget-boolean v5, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->$assertionsDisabled:Z

    if-nez v5, :cond_4

    iget-object v5, v4, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->dwpt:Lorg/apache/lucene/index/DocumentsWriterPerThread;

    iget-object v5, v5, Lorg/apache/lucene/index/DocumentsWriterPerThread;->deleteQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    if-eq v5, v0, :cond_4

    .line 481
    iget-object v5, v4, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->dwpt:Lorg/apache/lucene/index/DocumentsWriterPerThread;

    iget-object v5, v5, Lorg/apache/lucene/index/DocumentsWriterPerThread;->deleteQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    iget-object v6, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->documentsWriter:Lorg/apache/lucene/index/DocumentsWriter;

    iget-object v6, v6, Lorg/apache/lucene/index/DocumentsWriter;->deleteQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    if-eq v5, v6, :cond_4

    new-instance v5, Ljava/lang/AssertionError;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, " flushingQueue: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 482
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 483
    const-string v7, " currentqueue: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 484
    iget-object v7, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->documentsWriter:Lorg/apache/lucene/index/DocumentsWriter;

    iget-object v7, v7, Lorg/apache/lucene/index/DocumentsWriter;->deleteQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 485
    const-string v7, " perThread queue: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 486
    iget-object v7, v4, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->dwpt:Lorg/apache/lucene/index/DocumentsWriterPerThread;

    iget-object v7, v7, Lorg/apache/lucene/index/DocumentsWriterPerThread;->deleteQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 487
    const-string v7, " numDocsInRam: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v4, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->dwpt:Lorg/apache/lucene/index/DocumentsWriterPerThread;

    invoke-virtual {v7}, Lorg/apache/lucene/index/DocumentsWriterPerThread;->getNumDocsInRAM()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 493
    :catchall_2
    move-exception v5

    .line 494
    invoke-virtual {v4}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->unlock()V

    .line 495
    throw v5

    .line 488
    :cond_4
    :try_start_5
    iget-object v5, v4, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->dwpt:Lorg/apache/lucene/index/DocumentsWriterPerThread;

    iget-object v5, v5, Lorg/apache/lucene/index/DocumentsWriterPerThread;->deleteQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    if-eq v5, v0, :cond_5

    .line 494
    invoke-virtual {v4}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->unlock()V

    goto :goto_1

    .line 492
    :cond_5
    :try_start_6
    invoke-virtual {p0, v4}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->addFlushableState(Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 494
    invoke-virtual {v4}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->unlock()V

    goto :goto_1

    .line 504
    .end local v4    # "next":Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;
    :cond_6
    :try_start_7
    iget-object v5, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->flushQueue:Ljava/util/Queue;

    iget-object v6, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->fullFlushBuffer:Ljava/util/List;

    invoke-interface {v5, v6}, Ljava/util/Queue;->addAll(Ljava/util/Collection;)Z

    .line 505
    iget-object v5, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->fullFlushBuffer:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->clear()V

    .line 506
    invoke-direct {p0}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->updateStallState()Z

    .line 497
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 508
    sget-boolean v5, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->$assertionsDisabled:Z

    if-nez v5, :cond_7

    iget-object v5, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->documentsWriter:Lorg/apache/lucene/index/DocumentsWriter;

    iget-object v5, v5, Lorg/apache/lucene/index/DocumentsWriter;->deleteQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    invoke-direct {p0, v5}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->assertActiveDeleteQueue(Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;)Z

    move-result v5

    if-nez v5, :cond_7

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 509
    :cond_7
    return-void
.end method

.method public declared-synchronized netBytes()J
    .locals 4

    .prologue
    .line 88
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->flushBytes:J

    iget-wide v2, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->activeBytes:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-long/2addr v0, v2

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method nextPendingFlush()Lorg/apache/lucene/index/DocumentsWriterPerThread;
    .locals 8

    .prologue
    .line 344
    monitor-enter p0

    .line 346
    :try_start_0
    iget-object v7, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->flushQueue:Ljava/util/Queue;

    invoke-interface {v7}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/lucene/index/DocumentsWriterPerThread;

    .local v6, "poll":Lorg/apache/lucene/index/DocumentsWriterPerThread;
    if-eqz v6, :cond_0

    .line 347
    invoke-direct {p0}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->updateStallState()Z

    .line 348
    monitor-exit p0

    .line 365
    .end local v6    # "poll":Lorg/apache/lucene/index/DocumentsWriterPerThread;
    :goto_0
    return-object v6

    .line 350
    .restart local v6    # "poll":Lorg/apache/lucene/index/DocumentsWriterPerThread;
    :cond_0
    iget-boolean v1, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->fullFlush:Z

    .line 351
    .local v1, "fullFlush":Z
    iget v5, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->numPending:I

    .line 344
    .local v5, "numPending":I
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 353
    if-lez v5, :cond_1

    if-nez v1, :cond_1

    .line 354
    iget-object v7, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->perThreadPool:Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;

    invoke-virtual {v7}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->getActiveThreadState()I

    move-result v3

    .line 355
    .local v3, "limit":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v3, :cond_1

    if-gtz v5, :cond_2

    .line 365
    .end local v2    # "i":I
    .end local v3    # "limit":I
    :cond_1
    const/4 v6, 0x0

    goto :goto_0

    .line 344
    .end local v1    # "fullFlush":Z
    .end local v5    # "numPending":I
    .end local v6    # "poll":Lorg/apache/lucene/index/DocumentsWriterPerThread;
    :catchall_0
    move-exception v7

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v7

    .line 356
    .restart local v1    # "fullFlush":Z
    .restart local v2    # "i":I
    .restart local v3    # "limit":I
    .restart local v5    # "numPending":I
    .restart local v6    # "poll":Lorg/apache/lucene/index/DocumentsWriterPerThread;
    :cond_2
    iget-object v7, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->perThreadPool:Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;

    invoke-virtual {v7, v2}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->getThreadState(I)Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;

    move-result-object v4

    .line 357
    .local v4, "next":Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;
    iget-boolean v7, v4, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->flushPending:Z

    if-eqz v7, :cond_3

    .line 358
    invoke-virtual {p0, v4}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->tryCheckoutForFlush(Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;)Lorg/apache/lucene/index/DocumentsWriterPerThread;

    move-result-object v0

    .line 359
    .local v0, "dwpt":Lorg/apache/lucene/index/DocumentsWriterPerThread;
    if-eqz v0, :cond_3

    move-object v6, v0

    .line 360
    goto :goto_0

    .line 355
    .end local v0    # "dwpt":Lorg/apache/lucene/index/DocumentsWriterPerThread;
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method numActiveDWPT()I
    .locals 1

    .prologue
    .line 435
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->perThreadPool:Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->getActiveThreadState()I

    move-result v0

    return v0
.end method

.method declared-synchronized numBlockedFlushes()I
    .locals 1

    .prologue
    .line 652
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->blockedFlushes:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized numFlushingDWPT()I
    .locals 1

    .prologue
    .line 423
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->flushingWriters:Ljava/util/IdentityHashMap;

    invoke-virtual {v0}, Ljava/util/IdentityHashMap;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized numQueuedFlushes()I
    .locals 1

    .prologue
    .line 643
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->flushQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method obtainAndLock()Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;
    .locals 5

    .prologue
    .line 439
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->perThreadPool:Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;

    .line 440
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->documentsWriter:Lorg/apache/lucene/index/DocumentsWriter;

    .line 439
    invoke-virtual {v2, v3, v4}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->getAndLock(Ljava/lang/Thread;Lorg/apache/lucene/index/DocumentsWriter;)Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;

    move-result-object v0

    .line 441
    .local v0, "perThread":Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;
    const/4 v1, 0x0

    .line 443
    .local v1, "success":Z
    :try_start_0
    invoke-virtual {v0}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->isActive()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 444
    iget-object v2, v0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->dwpt:Lorg/apache/lucene/index/DocumentsWriterPerThread;

    iget-object v2, v2, Lorg/apache/lucene/index/DocumentsWriterPerThread;->deleteQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->documentsWriter:Lorg/apache/lucene/index/DocumentsWriter;

    iget-object v3, v3, Lorg/apache/lucene/index/DocumentsWriter;->deleteQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    if-eq v2, v3, :cond_0

    .line 448
    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->addFlushableState(Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 450
    :cond_0
    const/4 v1, 0x1

    .line 454
    if-nez v1, :cond_1

    .line 455
    invoke-virtual {v0}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->unlock()V

    .line 452
    :cond_1
    return-object v0

    .line 453
    :catchall_0
    move-exception v2

    .line 454
    if-nez v1, :cond_2

    .line 455
    invoke-virtual {v0}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->unlock()V

    .line 457
    :cond_2
    throw v2
.end method

.method public setApplyAllDeletes()V
    .locals 2

    .prologue
    .line 431
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->flushDeletes:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 432
    return-void
.end method

.method declared-synchronized setClosed()V
    .locals 1

    .prologue
    .line 370
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->closed:Z

    if-nez v0, :cond_0

    .line 371
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->closed:Z

    .line 372
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->perThreadPool:Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->deactivateUnreleasedStates()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 374
    :cond_0
    monitor-exit p0

    return-void

    .line 370
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setFlushPending(Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;)V
    .locals 4
    .param p1, "perThread"    # Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;

    .prologue
    .line 258
    monitor-enter p0

    :try_start_0
    sget-boolean v2, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    iget-boolean v2, p1, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->flushPending:Z

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 259
    :cond_0
    :try_start_1
    iget-object v2, p1, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->dwpt:Lorg/apache/lucene/index/DocumentsWriterPerThread;

    invoke-virtual {v2}, Lorg/apache/lucene/index/DocumentsWriterPerThread;->getNumDocsInRAM()I

    move-result v2

    if-lez v2, :cond_1

    .line 260
    const/4 v2, 0x1

    iput-boolean v2, p1, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->flushPending:Z

    .line 261
    iget-wide v0, p1, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->bytesUsed:J

    .line 262
    .local v0, "bytes":J
    iget-wide v2, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->flushBytes:J

    add-long/2addr v2, v0

    iput-wide v2, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->flushBytes:J

    .line 263
    iget-wide v2, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->activeBytes:J

    sub-long/2addr v2, v0

    iput-wide v2, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->activeBytes:J

    .line 264
    iget v2, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->numPending:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->numPending:I

    .line 265
    sget-boolean v2, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->$assertionsDisabled:Z

    if-nez v2, :cond_1

    invoke-direct {p0}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->assertMemory()Z

    move-result v2

    if-nez v2, :cond_1

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 268
    .end local v0    # "bytes":J
    :cond_1
    monitor-exit p0

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 337
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "DocumentsWriterFlushControl [activeBytes="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->activeBytes:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 338
    const-string v1, ", flushBytes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->flushBytes:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 337
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method declared-synchronized tryCheckoutForFlush(Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;)Lorg/apache/lucene/index/DocumentsWriterPerThread;
    .locals 1
    .param p1, "perThread"    # Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;

    .prologue
    .line 287
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p1, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->flushPending:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->internalTryCheckOutForFlush(Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;)Lorg/apache/lucene/index/DocumentsWriterPerThread;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized waitForFlush()V
    .locals 2

    .prologue
    .line 243
    monitor-enter p0

    :goto_0
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->flushingWriters:Ljava/util/IdentityHashMap;

    invoke-virtual {v1}, Ljava/util/IdentityHashMap;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_0

    .line 250
    monitor-exit p0

    return-void

    .line 245
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 246
    :catch_0
    move-exception v0

    .line 247
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_2
    new-instance v1, Lorg/apache/lucene/util/ThreadInterruptedException;

    invoke-direct {v1, v0}, Lorg/apache/lucene/util/ThreadInterruptedException;-><init>(Ljava/lang/InterruptedException;)V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 243
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method waitIfStalled()V
    .locals 6

    .prologue
    .line 670
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->documentsWriter:Lorg/apache/lucene/index/DocumentsWriter;

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v1, "DWFC"

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 671
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->documentsWriter:Lorg/apache/lucene/index/DocumentsWriter;

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v1, "DWFC"

    .line 672
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "waitIfStalled: numFlushesPending: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->flushQueue:Ljava/util/Queue;

    invoke-interface {v3}, Ljava/util/Queue;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 673
    const-string v3, " netBytes: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->netBytes()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " flushBytes: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->flushBytes()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 674
    const-string v3, " fullFlush: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->fullFlush:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 672
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 671
    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 676
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->stallControl:Lorg/apache/lucene/index/DocumentsWriterStallControl;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocumentsWriterStallControl;->waitIfStalled()V

    .line 677
    return-void
.end method
