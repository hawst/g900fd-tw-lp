.class final Lorg/apache/lucene/index/NormsConsumer;
.super Lorg/apache/lucene/index/InvertedDocEndConsumer;
.source "NormsConsumer.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lorg/apache/lucene/index/NormsConsumer;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/NormsConsumer;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lorg/apache/lucene/index/InvertedDocEndConsumer;-><init>()V

    return-void
.end method


# virtual methods
.method abort()V
    .locals 0

    .prologue
    .line 38
    return-void
.end method

.method addField(Lorg/apache/lucene/index/DocInverterPerField;Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/InvertedDocEndConsumerPerField;
    .locals 1
    .param p1, "docInverterPerField"    # Lorg/apache/lucene/index/DocInverterPerField;
    .param p2, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;

    .prologue
    .line 82
    new-instance v0, Lorg/apache/lucene/index/NormsConsumerPerField;

    invoke-direct {v0, p1, p2, p0}, Lorg/apache/lucene/index/NormsConsumerPerField;-><init>(Lorg/apache/lucene/index/DocInverterPerField;Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/index/NormsConsumer;)V

    return-object v0
.end method

.method finishDocument()V
    .locals 0

    .prologue
    .line 75
    return-void
.end method

.method public flush(Ljava/util/Map;Lorg/apache/lucene/index/SegmentWriteState;)V
    .locals 10
    .param p2, "state"    # Lorg/apache/lucene/index/SegmentWriteState;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/index/InvertedDocEndConsumerPerField;",
            ">;",
            "Lorg/apache/lucene/index/SegmentWriteState;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p1, "fieldsToFlush":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lorg/apache/lucene/index/InvertedDocEndConsumerPerField;>;"
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 42
    const/4 v3, 0x0

    .line 43
    .local v3, "success":Z
    const/4 v1, 0x0

    .line 45
    .local v1, "normsConsumer":Lorg/apache/lucene/codecs/DocValuesConsumer;
    :try_start_0
    iget-object v5, p2, Lorg/apache/lucene/index/SegmentWriteState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v5}, Lorg/apache/lucene/index/FieldInfos;->hasNorms()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 46
    iget-object v5, p2, Lorg/apache/lucene/index/SegmentWriteState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v5}, Lorg/apache/lucene/index/SegmentInfo;->getCodec()Lorg/apache/lucene/codecs/Codec;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/lucene/codecs/Codec;->normsFormat()Lorg/apache/lucene/codecs/NormsFormat;

    move-result-object v2

    .line 47
    .local v2, "normsFormat":Lorg/apache/lucene/codecs/NormsFormat;
    sget-boolean v5, Lorg/apache/lucene/index/NormsConsumer;->$assertionsDisabled:Z

    if-nez v5, :cond_0

    if-nez v2, :cond_0

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 65
    .end local v2    # "normsFormat":Lorg/apache/lucene/codecs/NormsFormat;
    :catchall_0
    move-exception v5

    .line 66
    if-eqz v3, :cond_5

    new-array v6, v9, [Ljava/io/Closeable;

    .line 67
    aput-object v1, v6, v8

    invoke-static {v6}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 71
    :goto_0
    throw v5

    .line 48
    .restart local v2    # "normsFormat":Lorg/apache/lucene/codecs/NormsFormat;
    :cond_0
    :try_start_1
    invoke-virtual {v2, p2}, Lorg/apache/lucene/codecs/NormsFormat;->normsConsumer(Lorg/apache/lucene/index/SegmentWriteState;)Lorg/apache/lucene/codecs/DocValuesConsumer;

    move-result-object v1

    .line 50
    iget-object v5, p2, Lorg/apache/lucene/index/SegmentWriteState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v5}, Lorg/apache/lucene/index/FieldInfos;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v6

    if-nez v6, :cond_3

    .line 64
    .end local v2    # "normsFormat":Lorg/apache/lucene/codecs/NormsFormat;
    :cond_2
    const/4 v3, 0x1

    .line 66
    if-eqz v3, :cond_6

    new-array v5, v9, [Ljava/io/Closeable;

    .line 67
    aput-object v1, v5, v8

    invoke-static {v5}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 72
    :goto_1
    return-void

    .line 50
    .restart local v2    # "normsFormat":Lorg/apache/lucene/codecs/NormsFormat;
    :cond_3
    :try_start_2
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/FieldInfo;

    .line 51
    .local v0, "fi":Lorg/apache/lucene/index/FieldInfo;
    iget-object v6, v0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-interface {p1, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/index/NormsConsumerPerField;

    .line 54
    .local v4, "toWrite":Lorg/apache/lucene/index/NormsConsumerPerField;
    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfo;->omitsNorms()Z

    move-result v6

    if-nez v6, :cond_1

    .line 55
    if-eqz v4, :cond_4

    invoke-virtual {v4}, Lorg/apache/lucene/index/NormsConsumerPerField;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_4

    .line 56
    invoke-virtual {v4, p2, v1}, Lorg/apache/lucene/index/NormsConsumerPerField;->flush(Lorg/apache/lucene/index/SegmentWriteState;Lorg/apache/lucene/codecs/DocValuesConsumer;)V

    .line 57
    sget-boolean v6, Lorg/apache/lucene/index/NormsConsumer;->$assertionsDisabled:Z

    if-nez v6, :cond_1

    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfo;->getNormType()Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-result-object v6

    sget-object v7, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->NUMERIC:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    if-eq v6, v7, :cond_1

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 58
    :cond_4
    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfo;->isIndexed()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 59
    sget-boolean v6, Lorg/apache/lucene/index/NormsConsumer;->$assertionsDisabled:Z

    if-nez v6, :cond_1

    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfo;->getNormType()Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-result-object v6

    if-eqz v6, :cond_1

    new-instance v5, Ljava/lang/AssertionError;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "got "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfo;->getNormType()Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "; field="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 68
    .end local v0    # "fi":Lorg/apache/lucene/index/FieldInfo;
    .end local v2    # "normsFormat":Lorg/apache/lucene/codecs/NormsFormat;
    .end local v4    # "toWrite":Lorg/apache/lucene/index/NormsConsumerPerField;
    :cond_5
    new-array v6, v9, [Ljava/io/Closeable;

    .line 69
    aput-object v1, v6, v8

    invoke-static {v6}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto/16 :goto_0

    .line 68
    :cond_6
    new-array v5, v9, [Ljava/io/Closeable;

    .line 69
    aput-object v1, v5, v8

    invoke-static {v5}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto :goto_1
.end method

.method startDocument()V
    .locals 0

    .prologue
    .line 78
    return-void
.end method
