.class Lorg/apache/lucene/index/BinaryDocValuesWriter$BytesIterator;
.super Ljava/lang/Object;
.source "BinaryDocValuesWriter.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/BinaryDocValuesWriter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BytesIterator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Lorg/apache/lucene/util/BytesRef;",
        ">;"
    }
.end annotation


# instance fields
.field byteOffset:J

.field final lengthsIterator:Lorg/apache/lucene/util/packed/AppendingLongBuffer$Iterator;

.field final maxDoc:I

.field final size:I

.field final synthetic this$0:Lorg/apache/lucene/index/BinaryDocValuesWriter;

.field upto:I

.field final value:Lorg/apache/lucene/util/BytesRef;


# direct methods
.method constructor <init>(Lorg/apache/lucene/index/BinaryDocValuesWriter;I)V
    .locals 2
    .param p2, "maxDoc"    # I

    .prologue
    .line 99
    iput-object p1, p0, Lorg/apache/lucene/index/BinaryDocValuesWriter$BytesIterator;->this$0:Lorg/apache/lucene/index/BinaryDocValuesWriter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/BinaryDocValuesWriter$BytesIterator;->value:Lorg/apache/lucene/util/BytesRef;

    .line 93
    # getter for: Lorg/apache/lucene/index/BinaryDocValuesWriter;->lengths:Lorg/apache/lucene/util/packed/AppendingLongBuffer;
    invoke-static {p1}, Lorg/apache/lucene/index/BinaryDocValuesWriter;->access$0(Lorg/apache/lucene/index/BinaryDocValuesWriter;)Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->iterator()Lorg/apache/lucene/util/packed/AppendingLongBuffer$Iterator;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/BinaryDocValuesWriter$BytesIterator;->lengthsIterator:Lorg/apache/lucene/util/packed/AppendingLongBuffer$Iterator;

    .line 94
    # getter for: Lorg/apache/lucene/index/BinaryDocValuesWriter;->lengths:Lorg/apache/lucene/util/packed/AppendingLongBuffer;
    invoke-static {p1}, Lorg/apache/lucene/index/BinaryDocValuesWriter;->access$0(Lorg/apache/lucene/index/BinaryDocValuesWriter;)Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->size()J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Lorg/apache/lucene/index/BinaryDocValuesWriter$BytesIterator;->size:I

    .line 100
    iput p2, p0, Lorg/apache/lucene/index/BinaryDocValuesWriter$BytesIterator;->maxDoc:I

    .line 101
    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 2

    .prologue
    .line 105
    iget v0, p0, Lorg/apache/lucene/index/BinaryDocValuesWriter$BytesIterator;->upto:I

    iget v1, p0, Lorg/apache/lucene/index/BinaryDocValuesWriter$BytesIterator;->maxDoc:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/index/BinaryDocValuesWriter$BytesIterator;->next()Lorg/apache/lucene/util/BytesRef;

    move-result-object v0

    return-object v0
.end method

.method public next()Lorg/apache/lucene/util/BytesRef;
    .locals 7

    .prologue
    .line 110
    invoke-virtual {p0}, Lorg/apache/lucene/index/BinaryDocValuesWriter$BytesIterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 111
    new-instance v1, Ljava/util/NoSuchElementException;

    invoke-direct {v1}, Ljava/util/NoSuchElementException;-><init>()V

    throw v1

    .line 113
    :cond_0
    iget v1, p0, Lorg/apache/lucene/index/BinaryDocValuesWriter$BytesIterator;->upto:I

    iget v2, p0, Lorg/apache/lucene/index/BinaryDocValuesWriter$BytesIterator;->size:I

    if-ge v1, v2, :cond_1

    .line 114
    iget-object v1, p0, Lorg/apache/lucene/index/BinaryDocValuesWriter$BytesIterator;->lengthsIterator:Lorg/apache/lucene/util/packed/AppendingLongBuffer$Iterator;

    invoke-virtual {v1}, Lorg/apache/lucene/util/packed/AppendingLongBuffer$Iterator;->next()J

    move-result-wide v2

    long-to-int v0, v2

    .line 115
    .local v0, "length":I
    iget-object v1, p0, Lorg/apache/lucene/index/BinaryDocValuesWriter$BytesIterator;->value:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v1, v0}, Lorg/apache/lucene/util/BytesRef;->grow(I)V

    .line 116
    iget-object v1, p0, Lorg/apache/lucene/index/BinaryDocValuesWriter$BytesIterator;->value:Lorg/apache/lucene/util/BytesRef;

    iput v0, v1, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 117
    iget-object v1, p0, Lorg/apache/lucene/index/BinaryDocValuesWriter$BytesIterator;->this$0:Lorg/apache/lucene/index/BinaryDocValuesWriter;

    # getter for: Lorg/apache/lucene/index/BinaryDocValuesWriter;->pool:Lorg/apache/lucene/util/ByteBlockPool;
    invoke-static {v1}, Lorg/apache/lucene/index/BinaryDocValuesWriter;->access$1(Lorg/apache/lucene/index/BinaryDocValuesWriter;)Lorg/apache/lucene/util/ByteBlockPool;

    move-result-object v1

    iget-wide v2, p0, Lorg/apache/lucene/index/BinaryDocValuesWriter$BytesIterator;->byteOffset:J

    iget-object v4, p0, Lorg/apache/lucene/index/BinaryDocValuesWriter$BytesIterator;->value:Lorg/apache/lucene/util/BytesRef;

    iget-object v4, v4, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget-object v5, p0, Lorg/apache/lucene/index/BinaryDocValuesWriter$BytesIterator;->value:Lorg/apache/lucene/util/BytesRef;

    iget v5, v5, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget-object v6, p0, Lorg/apache/lucene/index/BinaryDocValuesWriter$BytesIterator;->value:Lorg/apache/lucene/util/BytesRef;

    iget v6, v6, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-virtual/range {v1 .. v6}, Lorg/apache/lucene/util/ByteBlockPool;->readBytes(J[BII)V

    .line 118
    iget-wide v2, p0, Lorg/apache/lucene/index/BinaryDocValuesWriter$BytesIterator;->byteOffset:J

    int-to-long v4, v0

    add-long/2addr v2, v4

    iput-wide v2, p0, Lorg/apache/lucene/index/BinaryDocValuesWriter$BytesIterator;->byteOffset:J

    .line 124
    .end local v0    # "length":I
    :goto_0
    iget v1, p0, Lorg/apache/lucene/index/BinaryDocValuesWriter$BytesIterator;->upto:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/index/BinaryDocValuesWriter$BytesIterator;->upto:I

    .line 125
    iget-object v1, p0, Lorg/apache/lucene/index/BinaryDocValuesWriter$BytesIterator;->value:Lorg/apache/lucene/util/BytesRef;

    return-object v1

    .line 122
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/index/BinaryDocValuesWriter$BytesIterator;->value:Lorg/apache/lucene/util/BytesRef;

    const/4 v2, 0x0

    iput v2, v1, Lorg/apache/lucene/util/BytesRef;->length:I

    goto :goto_0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 130
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
