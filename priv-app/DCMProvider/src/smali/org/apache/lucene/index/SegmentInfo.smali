.class public final Lorg/apache/lucene/index/SegmentInfo;
.super Ljava/lang/Object;
.source "SegmentInfo.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final NO:I = -0x1

.field public static final YES:I = 0x1


# instance fields
.field private attributes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private codec:Lorg/apache/lucene/codecs/Codec;

.field private diagnostics:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final dir:Lorg/apache/lucene/store/Directory;

.field private docCount:I

.field private isCompoundFile:Z

.field public final name:Ljava/lang/String;

.field private setFiles:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private version:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/SegmentInfo;->$assertionsDisabled:Z

    .line 48
    return-void

    .line 39
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Ljava/lang/String;IZLorg/apache/lucene/codecs/Codec;Ljava/util/Map;Ljava/util/Map;)V
    .locals 1
    .param p1, "dir"    # Lorg/apache/lucene/store/Directory;
    .param p2, "version"    # Ljava/lang/String;
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "docCount"    # I
    .param p5, "isCompoundFile"    # Z
    .param p6, "codec"    # Lorg/apache/lucene/codecs/Codec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/store/Directory;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "IZ",
            "Lorg/apache/lucene/codecs/Codec;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 88
    .local p7, "diagnostics":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .local p8, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    sget-boolean v0, Lorg/apache/lucene/index/SegmentInfo;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    instance-of v0, p1, Lorg/apache/lucene/store/TrackingDirectoryWrapper;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 91
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    .line 92
    iput-object p2, p0, Lorg/apache/lucene/index/SegmentInfo;->version:Ljava/lang/String;

    .line 93
    iput-object p3, p0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    .line 94
    iput p4, p0, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    .line 95
    iput-boolean p5, p0, Lorg/apache/lucene/index/SegmentInfo;->isCompoundFile:Z

    .line 96
    iput-object p6, p0, Lorg/apache/lucene/index/SegmentInfo;->codec:Lorg/apache/lucene/codecs/Codec;

    .line 97
    iput-object p7, p0, Lorg/apache/lucene/index/SegmentInfo;->diagnostics:Ljava/util/Map;

    .line 98
    iput-object p8, p0, Lorg/apache/lucene/index/SegmentInfo;->attributes:Ljava/util/Map;

    .line 99
    return-void
.end method

.method private checkFileNames(Ljava/util/Collection;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 263
    .local p1, "files":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    sget-object v2, Lorg/apache/lucene/index/IndexFileNames;->CODEC_FILE_PATTERN:Ljava/util/regex/Pattern;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 264
    .local v1, "m":Ljava/util/regex/Matcher;
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 270
    return-void

    .line 264
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 265
    .local v0, "file":Ljava/lang/String;
    invoke-virtual {v1, v0}, Ljava/util/regex/Matcher;->reset(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    .line 266
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v3

    if-nez v3, :cond_0

    .line 267
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "invalid codec filename \'"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\', must match: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lorg/apache/lucene/index/IndexFileNames;->CODEC_FILE_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v4}, Ljava/util/regex/Pattern;->pattern()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
.end method


# virtual methods
.method public addFile(Ljava/lang/String;)V
    .locals 1
    .param p1, "file"    # Ljava/lang/String;

    .prologue
    .line 258
    invoke-static {p1}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/lucene/index/SegmentInfo;->checkFileNames(Ljava/util/Collection;)V

    .line 259
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfo;->setFiles:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 260
    return-void
.end method

.method public addFiles(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 251
    .local p1, "files":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/SegmentInfo;->checkFileNames(Ljava/util/Collection;)V

    .line 252
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfo;->setFiles:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 253
    return-void
.end method

.method public attributes()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 306
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfo;->attributes:Ljava/util/Map;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 206
    if-ne p0, p1, :cond_1

    .line 211
    :cond_0
    :goto_0
    return v1

    .line 207
    :cond_1
    instance-of v3, p1, Lorg/apache/lucene/index/SegmentInfo;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 208
    check-cast v0, Lorg/apache/lucene/index/SegmentInfo;

    .line 209
    .local v0, "other":Lorg/apache/lucene/index/SegmentInfo;
    iget-object v3, v0, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    iget-object v4, p0, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    if-ne v3, v4, :cond_2

    iget-object v3, v0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    iget-object v4, p0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "other":Lorg/apache/lucene/index/SegmentInfo;
    :cond_3
    move v1, v2

    .line 211
    goto :goto_0
.end method

.method public files()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 160
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfo;->setFiles:Ljava/util/Set;

    if-nez v0, :cond_0

    .line 161
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "files were not computed yet"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 163
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfo;->setFiles:Ljava/util/Set;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public getAttribute(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 276
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfo;->attributes:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 277
    const/4 v0, 0x0

    .line 279
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfo;->attributes:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getCodec()Lorg/apache/lucene/codecs/Codec;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfo;->codec:Lorg/apache/lucene/codecs/Codec;

    return-object v0
.end method

.method public getDiagnostics()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 80
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfo;->diagnostics:Ljava/util/Map;

    return-object v0
.end method

.method public getDocCount()I
    .locals 2

    .prologue
    .line 144
    iget v0, p0, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 145
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "docCount isn\'t set yet"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 147
    :cond_0
    iget v0, p0, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    return v0
.end method

.method public getUseCompoundFile()Z
    .locals 1

    .prologue
    .line 124
    iget-boolean v0, p0, Lorg/apache/lucene/index/SegmentInfo;->isCompoundFile:Z

    return v0
.end method

.method public getVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfo;->version:Ljava/lang/String;

    return-object v0
.end method

.method hasSeparateNorms()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 106
    sget-object v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoFormat;->NORMGEN_KEY:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/SegmentInfo;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 217
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public putAttribute(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 294
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfo;->attributes:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 295
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentInfo;->attributes:Ljava/util/Map;

    .line 297
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfo;->attributes:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public setCodec(Lorg/apache/lucene/codecs/Codec;)V
    .locals 2
    .param p1, "codec"    # Lorg/apache/lucene/codecs/Codec;

    .prologue
    .line 129
    sget-boolean v0, Lorg/apache/lucene/index/SegmentInfo;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfo;->codec:Lorg/apache/lucene/codecs/Codec;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 130
    :cond_0
    if-nez p1, :cond_1

    .line 131
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "segmentCodecs must be non-null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 133
    :cond_1
    iput-object p1, p0, Lorg/apache/lucene/index/SegmentInfo;->codec:Lorg/apache/lucene/codecs/Codec;

    .line 134
    return-void
.end method

.method setDiagnostics(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 74
    .local p1, "diagnostics":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iput-object p1, p0, Lorg/apache/lucene/index/SegmentInfo;->diagnostics:Ljava/util/Map;

    .line 75
    return-void
.end method

.method setDocCount(I)V
    .locals 2
    .param p1, "docCount"    # I

    .prologue
    .line 152
    iget v0, p0, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 153
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "docCount was already set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 155
    :cond_0
    iput p1, p0, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    .line 156
    return-void
.end method

.method public setFiles(Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 244
    .local p1, "files":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/SegmentInfo;->checkFileNames(Ljava/util/Collection;)V

    .line 245
    iput-object p1, p0, Lorg/apache/lucene/index/SegmentInfo;->setFiles:Ljava/util/Set;

    .line 246
    return-void
.end method

.method setUseCompoundFile(Z)V
    .locals 0
    .param p1, "isCompoundFile"    # Z

    .prologue
    .line 116
    iput-boolean p1, p0, Lorg/apache/lucene/index/SegmentInfo;->isCompoundFile:Z

    .line 117
    return-void
.end method

.method public setVersion(Ljava/lang/String;)V
    .locals 0
    .param p1, "version"    # Ljava/lang/String;

    .prologue
    .line 232
    iput-object p1, p0, Lorg/apache/lucene/index/SegmentInfo;->version:Ljava/lang/String;

    .line 233
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 168
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/index/SegmentInfo;->toString(Lorg/apache/lucene/store/Directory;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString(Lorg/apache/lucene/store/Directory;I)Ljava/lang/String;
    .locals 4
    .param p1, "dir"    # Lorg/apache/lucene/store/Directory;
    .param p2, "delCount"    # I

    .prologue
    .line 183
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 184
    .local v1, "s":Ljava/lang/StringBuilder;
    iget-object v2, p0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x28

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v2, p0, Lorg/apache/lucene/index/SegmentInfo;->version:Ljava/lang/String;

    if-nez v2, :cond_2

    const-string v2, "?"

    :goto_0
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x29

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x3a

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 185
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentInfo;->getUseCompoundFile()Z

    move-result v2

    if-eqz v2, :cond_3

    const/16 v0, 0x63

    .line 186
    .local v0, "cfs":C
    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 188
    iget-object v2, p0, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    if-eq v2, p1, :cond_0

    .line 189
    const/16 v2, 0x78

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 191
    :cond_0
    iget v2, p0, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 193
    if-eqz p2, :cond_1

    .line 194
    const/16 v2, 0x2f

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 199
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 184
    .end local v0    # "cfs":C
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/index/SegmentInfo;->version:Ljava/lang/String;

    goto :goto_0

    .line 185
    :cond_3
    const/16 v0, 0x43

    goto :goto_1
.end method
