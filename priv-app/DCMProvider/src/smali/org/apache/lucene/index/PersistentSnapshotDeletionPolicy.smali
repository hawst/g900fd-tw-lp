.class public Lorg/apache/lucene/index/PersistentSnapshotDeletionPolicy;
.super Lorg/apache/lucene/index/SnapshotDeletionPolicy;
.source "PersistentSnapshotDeletionPolicy.java"

# interfaces
.implements Ljava/io/Closeable;


# static fields
.field private static final SNAPSHOTS_ID:Ljava/lang/String; = "$SNAPSHOTS_DOC$"


# instance fields
.field private final writer:Lorg/apache/lucene/index/IndexWriter;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/IndexDeletionPolicy;Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;Lorg/apache/lucene/util/Version;)V
    .locals 6
    .param p1, "primary"    # Lorg/apache/lucene/index/IndexDeletionPolicy;
    .param p2, "dir"    # Lorg/apache/lucene/store/Directory;
    .param p3, "mode"    # Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;
    .param p4, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 116
    invoke-direct {p0, p1, v4}, Lorg/apache/lucene/index/SnapshotDeletionPolicy;-><init>(Lorg/apache/lucene/index/IndexDeletionPolicy;Ljava/util/Map;)V

    .line 119
    new-instance v2, Lorg/apache/lucene/index/IndexWriter;

    new-instance v3, Lorg/apache/lucene/index/IndexWriterConfig;

    invoke-direct {v3, p4, v4}, Lorg/apache/lucene/index/IndexWriterConfig;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/Analyzer;)V

    invoke-virtual {v3, p3}, Lorg/apache/lucene/index/IndexWriterConfig;->setOpenMode(Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;)Lorg/apache/lucene/index/IndexWriterConfig;

    move-result-object v3

    invoke-direct {v2, p2, v3}, Lorg/apache/lucene/index/IndexWriter;-><init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexWriterConfig;)V

    iput-object v2, p0, Lorg/apache/lucene/index/PersistentSnapshotDeletionPolicy;->writer:Lorg/apache/lucene/index/IndexWriter;

    .line 120
    sget-object v2, Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;->APPEND:Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;

    if-eq p3, v2, :cond_0

    .line 124
    iget-object v2, p0, Lorg/apache/lucene/index/PersistentSnapshotDeletionPolicy;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v2}, Lorg/apache/lucene/index/IndexWriter;->commit()V

    .line 131
    :cond_0
    :try_start_0
    invoke-static {p2}, Lorg/apache/lucene/index/PersistentSnapshotDeletionPolicy;->readSnapshotsInfo(Lorg/apache/lucene/store/Directory;)Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 141
    return-void

    .line 131
    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 132
    .local v1, "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {p0, v2, v3, v5}, Lorg/apache/lucene/index/PersistentSnapshotDeletionPolicy;->registerSnapshotInfo(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/index/IndexCommit;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 134
    .end local v1    # "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    :catch_0
    move-exception v0

    .line 135
    .local v0, "e":Ljava/lang/RuntimeException;
    iget-object v2, p0, Lorg/apache/lucene/index/PersistentSnapshotDeletionPolicy;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v2}, Lorg/apache/lucene/index/IndexWriter;->close()V

    .line 136
    throw v0

    .line 137
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catch_1
    move-exception v0

    .line 138
    .local v0, "e":Ljava/io/IOException;
    iget-object v2, p0, Lorg/apache/lucene/index/PersistentSnapshotDeletionPolicy;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v2}, Lorg/apache/lucene/index/IndexWriter;->close()V

    .line 139
    throw v0
.end method

.method private persistSnapshotInfos(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "segment"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 192
    iget-object v3, p0, Lorg/apache/lucene/index/PersistentSnapshotDeletionPolicy;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v3}, Lorg/apache/lucene/index/IndexWriter;->deleteAll()V

    .line 193
    new-instance v0, Lorg/apache/lucene/document/Document;

    invoke-direct {v0}, Lorg/apache/lucene/document/Document;-><init>()V

    .line 194
    .local v0, "d":Lorg/apache/lucene/document/Document;
    new-instance v2, Lorg/apache/lucene/document/FieldType;

    invoke-direct {v2}, Lorg/apache/lucene/document/FieldType;-><init>()V

    .line 195
    .local v2, "ft":Lorg/apache/lucene/document/FieldType;
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lorg/apache/lucene/document/FieldType;->setStored(Z)V

    .line 196
    new-instance v3, Lorg/apache/lucene/document/Field;

    const-string v4, "$SNAPSHOTS_DOC$"

    const-string v5, ""

    invoke-direct {v3, v4, v5, v2}, Lorg/apache/lucene/document/Field;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/FieldType;)V

    invoke-virtual {v0, v3}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 197
    invoke-super {p0}, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->getSnapshots()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 200
    if-eqz p1, :cond_0

    .line 201
    new-instance v3, Lorg/apache/lucene/document/Field;

    invoke-direct {v3, p1, p2, v2}, Lorg/apache/lucene/document/Field;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/FieldType;)V

    invoke-virtual {v0, v3}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 203
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/index/PersistentSnapshotDeletionPolicy;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v3, v0}, Lorg/apache/lucene/index/IndexWriter;->addDocument(Ljava/lang/Iterable;)V

    .line 204
    iget-object v3, p0, Lorg/apache/lucene/index/PersistentSnapshotDeletionPolicy;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v3}, Lorg/apache/lucene/index/IndexWriter;->commit()V

    .line 205
    return-void

    .line 197
    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 198
    .local v1, "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v6, Lorg/apache/lucene/document/Field;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-direct {v6, v3, v4, v2}, Lorg/apache/lucene/document/Field;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/FieldType;)V

    invoke-virtual {v0, v6}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    goto :goto_0
.end method

.method public static readSnapshotsInfo(Lorg/apache/lucene/store/Directory;)Ljava/util/Map;
    .locals 8
    .param p0, "dir"    # Lorg/apache/lucene/store/Directory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/store/Directory;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 71
    invoke-static {p0}, Lorg/apache/lucene/index/DirectoryReader;->open(Lorg/apache/lucene/store/Directory;)Lorg/apache/lucene/index/DirectoryReader;

    move-result-object v3

    .line 72
    .local v3, "r":Lorg/apache/lucene/index/IndexReader;
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 74
    .local v4, "snapshots":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :try_start_0
    invoke-virtual {v3}, Lorg/apache/lucene/index/IndexReader;->numDocs()I

    move-result v2

    .line 76
    .local v2, "numDocs":I
    const/4 v5, 0x1

    if-ne v2, v5, :cond_3

    .line 77
    invoke-virtual {v3}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v3, v5}, Lorg/apache/lucene/index/IndexReader;->document(I)Lorg/apache/lucene/document/Document;

    move-result-object v0

    .line 78
    .local v0, "doc":Lorg/apache/lucene/document/Document;
    const-string v5, "$SNAPSHOTS_DOC$"

    invoke-virtual {v0, v5}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/index/IndexableField;

    move-result-object v5

    if-nez v5, :cond_0

    .line 79
    new-instance v5, Ljava/lang/IllegalStateException;

    const-string v6, "directory is not a valid snapshots store!"

    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 89
    .end local v0    # "doc":Lorg/apache/lucene/document/Document;
    .end local v2    # "numDocs":I
    :catchall_0
    move-exception v5

    .line 90
    invoke-virtual {v3}, Lorg/apache/lucene/index/IndexReader;->close()V

    .line 91
    throw v5

    .line 81
    .restart local v0    # "doc":Lorg/apache/lucene/document/Document;
    .restart local v2    # "numDocs":I
    :cond_0
    :try_start_1
    const-string v5, "$SNAPSHOTS_DOC$"

    invoke-virtual {v0, v5}, Lorg/apache/lucene/document/Document;->removeField(Ljava/lang/String;)V

    .line 82
    invoke-virtual {v0}, Lorg/apache/lucene/document/Document;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v6

    if-nez v6, :cond_2

    .line 90
    .end local v0    # "doc":Lorg/apache/lucene/document/Document;
    :cond_1
    invoke-virtual {v3}, Lorg/apache/lucene/index/IndexReader;->close()V

    .line 92
    return-object v4

    .line 82
    .restart local v0    # "doc":Lorg/apache/lucene/document/Document;
    :cond_2
    :try_start_2
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/IndexableField;

    .line 83
    .local v1, "f":Lorg/apache/lucene/index/IndexableField;
    invoke-interface {v1}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v1}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v4, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 85
    .end local v0    # "doc":Lorg/apache/lucene/document/Document;
    .end local v1    # "f":Lorg/apache/lucene/index/IndexableField;
    :cond_3
    if-eqz v2, :cond_1

    .line 86
    new-instance v5, Ljava/lang/IllegalStateException;

    .line 87
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "should be at most 1 document in the snapshots directory: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 86
    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 184
    iget-object v0, p0, Lorg/apache/lucene/index/PersistentSnapshotDeletionPolicy;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexWriter;->close()V

    .line 185
    return-void
.end method

.method public declared-synchronized onInit(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lorg/apache/lucene/index/IndexCommit;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 150
    .local p1, "commits":Ljava/util/List;, "Ljava/util/List<+Lorg/apache/lucene/index/IndexCommit;>;"
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->onInit(Ljava/util/List;)V

    .line 151
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/index/PersistentSnapshotDeletionPolicy;->persistSnapshotInfos(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 152
    monitor-exit p0

    return-void

    .line 150
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized release(Ljava/lang/String;)V
    .locals 2
    .param p1, "id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 178
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->release(Ljava/lang/String;)V

    .line 179
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/index/PersistentSnapshotDeletionPolicy;->persistSnapshotInfos(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 180
    monitor-exit p0

    return-void

    .line 178
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized snapshot(Ljava/lang/String;)Lorg/apache/lucene/index/IndexCommit;
    .locals 3
    .param p1, "id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 162
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/PersistentSnapshotDeletionPolicy;->checkSnapshotted(Ljava/lang/String;)V

    .line 163
    const-string v0, "$SNAPSHOTS_DOC$"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 164
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " is reserved and cannot be used as a snapshot id"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 162
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 166
    :cond_0
    :try_start_1
    iget-object v0, p0, Lorg/apache/lucene/index/PersistentSnapshotDeletionPolicy;->lastCommit:Lorg/apache/lucene/index/IndexCommit;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexCommit;->getSegmentsFileName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/index/PersistentSnapshotDeletionPolicy;->persistSnapshotInfos(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    invoke-super {p0, p1}, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->snapshot(Ljava/lang/String;)Lorg/apache/lucene/index/IndexCommit;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0
.end method
