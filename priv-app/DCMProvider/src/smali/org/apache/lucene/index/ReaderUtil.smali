.class public final Lorg/apache/lucene/index/ReaderUtil;
.super Ljava/lang/Object;
.source "ReaderUtil.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getTopLevelContext(Lorg/apache/lucene/index/IndexReaderContext;)Lorg/apache/lucene/index/IndexReaderContext;
    .locals 1
    .param p0, "context"    # Lorg/apache/lucene/index/IndexReaderContext;

    .prologue
    .line 37
    :goto_0
    iget-object v0, p0, Lorg/apache/lucene/index/IndexReaderContext;->parent:Lorg/apache/lucene/index/CompositeReaderContext;

    if-nez v0, :cond_0

    .line 40
    return-object p0

    .line 38
    :cond_0
    iget-object p0, p0, Lorg/apache/lucene/index/IndexReaderContext;->parent:Lorg/apache/lucene/index/CompositeReaderContext;

    goto :goto_0
.end method

.method public static subIndex(ILjava/util/List;)I
    .locals 6
    .param p0, "n"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/AtomicReaderContext;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 75
    .local p1, "leaves":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/AtomicReaderContext;>;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    .line 76
    .local v4, "size":I
    const/4 v1, 0x0

    .line 77
    .local v1, "lo":I
    add-int/lit8 v0, v4, -0x1

    .line 78
    .local v0, "hi":I
    :goto_0
    if-ge v0, v1, :cond_1

    move v2, v0

    .line 92
    :cond_0
    :goto_1
    return v2

    .line 79
    :cond_1
    add-int v5, v1, v0

    ushr-int/lit8 v2, v5, 0x1

    .line 80
    .local v2, "mid":I
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/index/AtomicReaderContext;

    iget v3, v5, Lorg/apache/lucene/index/AtomicReaderContext;->docBase:I

    .line 81
    .local v3, "midValue":I
    if-ge p0, v3, :cond_2

    .line 82
    add-int/lit8 v0, v2, -0x1

    goto :goto_0

    .line 83
    :cond_2
    if-le p0, v3, :cond_4

    .line 84
    add-int/lit8 v1, v2, 0x1

    goto :goto_0

    .line 87
    :cond_3
    add-int/lit8 v2, v2, 0x1

    .line 86
    :cond_4
    add-int/lit8 v5, v2, 0x1

    if-ge v5, v4, :cond_0

    add-int/lit8 v5, v2, 0x1

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/index/AtomicReaderContext;

    iget v5, v5, Lorg/apache/lucene/index/AtomicReaderContext;->docBase:I

    if-eq v5, v3, :cond_3

    goto :goto_1
.end method

.method public static subIndex(I[I)I
    .locals 6
    .param p0, "n"    # I
    .param p1, "docStarts"    # [I

    .prologue
    .line 49
    array-length v4, p1

    .line 50
    .local v4, "size":I
    const/4 v1, 0x0

    .line 51
    .local v1, "lo":I
    add-int/lit8 v0, v4, -0x1

    .line 52
    .local v0, "hi":I
    :goto_0
    if-ge v0, v1, :cond_1

    move v2, v0

    .line 66
    :cond_0
    :goto_1
    return v2

    .line 53
    :cond_1
    add-int v5, v1, v0

    ushr-int/lit8 v2, v5, 0x1

    .line 54
    .local v2, "mid":I
    aget v3, p1, v2

    .line 55
    .local v3, "midValue":I
    if-ge p0, v3, :cond_2

    .line 56
    add-int/lit8 v0, v2, -0x1

    goto :goto_0

    .line 57
    :cond_2
    if-le p0, v3, :cond_4

    .line 58
    add-int/lit8 v1, v2, 0x1

    goto :goto_0

    .line 61
    :cond_3
    add-int/lit8 v2, v2, 0x1

    .line 60
    :cond_4
    add-int/lit8 v5, v2, 0x1

    if-ge v5, v4, :cond_0

    add-int/lit8 v5, v2, 0x1

    aget v5, p1, v5

    if-eq v5, v3, :cond_3

    goto :goto_1
.end method
