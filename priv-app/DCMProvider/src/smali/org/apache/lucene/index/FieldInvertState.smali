.class public final Lorg/apache/lucene/index/FieldInvertState;
.super Ljava/lang/Object;
.source "FieldInvertState.java"


# instance fields
.field attributeSource:Lorg/apache/lucene/util/AttributeSource;

.field boost:F

.field length:I

.field maxTermFrequency:I

.field name:Ljava/lang/String;

.field numOverlap:I

.field offset:I

.field position:I

.field uniqueTermCount:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lorg/apache/lucene/index/FieldInvertState;->name:Ljava/lang/String;

    .line 44
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IIIIF)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "position"    # I
    .param p3, "length"    # I
    .param p4, "numOverlap"    # I
    .param p5, "offset"    # I
    .param p6, "boost"    # F

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Lorg/apache/lucene/index/FieldInvertState;->name:Ljava/lang/String;

    .line 50
    iput p2, p0, Lorg/apache/lucene/index/FieldInvertState;->position:I

    .line 51
    iput p3, p0, Lorg/apache/lucene/index/FieldInvertState;->length:I

    .line 52
    iput p4, p0, Lorg/apache/lucene/index/FieldInvertState;->numOverlap:I

    .line 53
    iput p5, p0, Lorg/apache/lucene/index/FieldInvertState;->offset:I

    .line 54
    iput p6, p0, Lorg/apache/lucene/index/FieldInvertState;->boost:F

    .line 55
    return-void
.end method


# virtual methods
.method public getAttributeSource()Lorg/apache/lucene/util/AttributeSource;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lorg/apache/lucene/index/FieldInvertState;->attributeSource:Lorg/apache/lucene/util/AttributeSource;

    return-object v0
.end method

.method public getBoost()F
    .locals 1

    .prologue
    .line 121
    iget v0, p0, Lorg/apache/lucene/index/FieldInvertState;->boost:F

    return v0
.end method

.method public getLength()I
    .locals 1

    .prologue
    .line 84
    iget v0, p0, Lorg/apache/lucene/index/FieldInvertState;->length:I

    return v0
.end method

.method public getMaxTermFrequency()I
    .locals 1

    .prologue
    .line 135
    iget v0, p0, Lorg/apache/lucene/index/FieldInvertState;->maxTermFrequency:I

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lorg/apache/lucene/index/FieldInvertState;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getNumOverlap()I
    .locals 1

    .prologue
    .line 97
    iget v0, p0, Lorg/apache/lucene/index/FieldInvertState;->numOverlap:I

    return v0
.end method

.method public getOffset()I
    .locals 1

    .prologue
    .line 111
    iget v0, p0, Lorg/apache/lucene/index/FieldInvertState;->offset:I

    return v0
.end method

.method public getPosition()I
    .locals 1

    .prologue
    .line 76
    iget v0, p0, Lorg/apache/lucene/index/FieldInvertState;->position:I

    return v0
.end method

.method public getUniqueTermCount()I
    .locals 1

    .prologue
    .line 142
    iget v0, p0, Lorg/apache/lucene/index/FieldInvertState;->uniqueTermCount:I

    return v0
.end method

.method reset()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 61
    iput v0, p0, Lorg/apache/lucene/index/FieldInvertState;->position:I

    .line 62
    iput v0, p0, Lorg/apache/lucene/index/FieldInvertState;->length:I

    .line 63
    iput v0, p0, Lorg/apache/lucene/index/FieldInvertState;->numOverlap:I

    .line 64
    iput v0, p0, Lorg/apache/lucene/index/FieldInvertState;->offset:I

    .line 65
    iput v0, p0, Lorg/apache/lucene/index/FieldInvertState;->maxTermFrequency:I

    .line 66
    iput v0, p0, Lorg/apache/lucene/index/FieldInvertState;->uniqueTermCount:I

    .line 67
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lorg/apache/lucene/index/FieldInvertState;->boost:F

    .line 68
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/index/FieldInvertState;->attributeSource:Lorg/apache/lucene/util/AttributeSource;

    .line 69
    return-void
.end method

.method public setBoost(F)V
    .locals 0
    .param p1, "boost"    # F

    .prologue
    .line 126
    iput p1, p0, Lorg/apache/lucene/index/FieldInvertState;->boost:F

    .line 127
    return-void
.end method

.method public setLength(I)V
    .locals 0
    .param p1, "length"    # I

    .prologue
    .line 89
    iput p1, p0, Lorg/apache/lucene/index/FieldInvertState;->length:I

    .line 90
    return-void
.end method

.method public setNumOverlap(I)V
    .locals 0
    .param p1, "numOverlap"    # I

    .prologue
    .line 103
    iput p1, p0, Lorg/apache/lucene/index/FieldInvertState;->numOverlap:I

    .line 104
    return-void
.end method
