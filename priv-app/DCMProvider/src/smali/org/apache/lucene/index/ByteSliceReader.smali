.class final Lorg/apache/lucene/index/ByteSliceReader;
.super Lorg/apache/lucene/store/DataInput;
.source "ByteSliceReader.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field buffer:[B

.field public bufferOffset:I

.field bufferUpto:I

.field public endIndex:I

.field level:I

.field limit:I

.field pool:Lorg/apache/lucene/util/ByteBlockPool;

.field public upto:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lorg/apache/lucene/index/ByteSliceReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/ByteSliceReader;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lorg/apache/lucene/store/DataInput;-><init>()V

    return-void
.end method


# virtual methods
.method public eof()Z
    .locals 2

    .prologue
    .line 67
    sget-boolean v0, Lorg/apache/lucene/index/ByteSliceReader;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/index/ByteSliceReader;->upto:I

    iget v1, p0, Lorg/apache/lucene/index/ByteSliceReader;->bufferOffset:I

    add-int/2addr v0, v1

    iget v1, p0, Lorg/apache/lucene/index/ByteSliceReader;->endIndex:I

    if-le v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 68
    :cond_0
    iget v0, p0, Lorg/apache/lucene/index/ByteSliceReader;->upto:I

    iget v1, p0, Lorg/apache/lucene/index/ByteSliceReader;->bufferOffset:I

    add-int/2addr v0, v1

    iget v1, p0, Lorg/apache/lucene/index/ByteSliceReader;->endIndex:I

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public init(Lorg/apache/lucene/util/ByteBlockPool;II)V
    .locals 4
    .param p1, "pool"    # Lorg/apache/lucene/util/ByteBlockPool;
    .param p2, "startIndex"    # I
    .param p3, "endIndex"    # I

    .prologue
    const v2, 0x8000

    const/4 v3, 0x0

    .line 44
    sget-boolean v1, Lorg/apache/lucene/index/ByteSliceReader;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    sub-int v1, p3, p2

    if-gez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 45
    :cond_0
    sget-boolean v1, Lorg/apache/lucene/index/ByteSliceReader;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    if-gez p2, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 46
    :cond_1
    sget-boolean v1, Lorg/apache/lucene/index/ByteSliceReader;->$assertionsDisabled:Z

    if-nez v1, :cond_2

    if-gez p3, :cond_2

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 48
    :cond_2
    iput-object p1, p0, Lorg/apache/lucene/index/ByteSliceReader;->pool:Lorg/apache/lucene/util/ByteBlockPool;

    .line 49
    iput p3, p0, Lorg/apache/lucene/index/ByteSliceReader;->endIndex:I

    .line 51
    iput v3, p0, Lorg/apache/lucene/index/ByteSliceReader;->level:I

    .line 52
    div-int v1, p2, v2

    iput v1, p0, Lorg/apache/lucene/index/ByteSliceReader;->bufferUpto:I

    .line 53
    iget v1, p0, Lorg/apache/lucene/index/ByteSliceReader;->bufferUpto:I

    mul-int/2addr v1, v2

    iput v1, p0, Lorg/apache/lucene/index/ByteSliceReader;->bufferOffset:I

    .line 54
    iget-object v1, p1, Lorg/apache/lucene/util/ByteBlockPool;->buffers:[[B

    iget v2, p0, Lorg/apache/lucene/index/ByteSliceReader;->bufferUpto:I

    aget-object v1, v1, v2

    iput-object v1, p0, Lorg/apache/lucene/index/ByteSliceReader;->buffer:[B

    .line 55
    and-int/lit16 v1, p2, 0x7fff

    iput v1, p0, Lorg/apache/lucene/index/ByteSliceReader;->upto:I

    .line 57
    sget-object v1, Lorg/apache/lucene/util/ByteBlockPool;->LEVEL_SIZE_ARRAY:[I

    aget v0, v1, v3

    .line 59
    .local v0, "firstSize":I
    add-int v1, p2, v0

    if-lt v1, p3, :cond_3

    .line 61
    and-int/lit16 v1, p3, 0x7fff

    iput v1, p0, Lorg/apache/lucene/index/ByteSliceReader;->limit:I

    .line 64
    :goto_0
    return-void

    .line 63
    :cond_3
    iget v1, p0, Lorg/apache/lucene/index/ByteSliceReader;->upto:I

    add-int/2addr v1, v0

    add-int/lit8 v1, v1, -0x4

    iput v1, p0, Lorg/apache/lucene/index/ByteSliceReader;->limit:I

    goto :goto_0
.end method

.method public nextSlice()V
    .locals 6

    .prologue
    const v5, 0x8000

    .line 101
    iget-object v2, p0, Lorg/apache/lucene/index/ByteSliceReader;->buffer:[B

    iget v3, p0, Lorg/apache/lucene/index/ByteSliceReader;->limit:I

    aget-byte v2, v2, v3

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x18

    iget-object v3, p0, Lorg/apache/lucene/index/ByteSliceReader;->buffer:[B

    iget v4, p0, Lorg/apache/lucene/index/ByteSliceReader;->limit:I

    add-int/lit8 v4, v4, 0x1

    aget-byte v3, v3, v4

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x10

    add-int/2addr v2, v3

    iget-object v3, p0, Lorg/apache/lucene/index/ByteSliceReader;->buffer:[B

    iget v4, p0, Lorg/apache/lucene/index/ByteSliceReader;->limit:I

    add-int/lit8 v4, v4, 0x2

    aget-byte v3, v3, v4

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x8

    add-int/2addr v2, v3

    iget-object v3, p0, Lorg/apache/lucene/index/ByteSliceReader;->buffer:[B

    iget v4, p0, Lorg/apache/lucene/index/ByteSliceReader;->limit:I

    add-int/lit8 v4, v4, 0x3

    aget-byte v3, v3, v4

    and-int/lit16 v3, v3, 0xff

    add-int v1, v2, v3

    .line 103
    .local v1, "nextIndex":I
    sget-object v2, Lorg/apache/lucene/util/ByteBlockPool;->NEXT_LEVEL_ARRAY:[I

    iget v3, p0, Lorg/apache/lucene/index/ByteSliceReader;->level:I

    aget v2, v2, v3

    iput v2, p0, Lorg/apache/lucene/index/ByteSliceReader;->level:I

    .line 104
    sget-object v2, Lorg/apache/lucene/util/ByteBlockPool;->LEVEL_SIZE_ARRAY:[I

    iget v3, p0, Lorg/apache/lucene/index/ByteSliceReader;->level:I

    aget v0, v2, v3

    .line 106
    .local v0, "newSize":I
    div-int v2, v1, v5

    iput v2, p0, Lorg/apache/lucene/index/ByteSliceReader;->bufferUpto:I

    .line 107
    iget v2, p0, Lorg/apache/lucene/index/ByteSliceReader;->bufferUpto:I

    mul-int/2addr v2, v5

    iput v2, p0, Lorg/apache/lucene/index/ByteSliceReader;->bufferOffset:I

    .line 109
    iget-object v2, p0, Lorg/apache/lucene/index/ByteSliceReader;->pool:Lorg/apache/lucene/util/ByteBlockPool;

    iget-object v2, v2, Lorg/apache/lucene/util/ByteBlockPool;->buffers:[[B

    iget v3, p0, Lorg/apache/lucene/index/ByteSliceReader;->bufferUpto:I

    aget-object v2, v2, v3

    iput-object v2, p0, Lorg/apache/lucene/index/ByteSliceReader;->buffer:[B

    .line 110
    and-int/lit16 v2, v1, 0x7fff

    iput v2, p0, Lorg/apache/lucene/index/ByteSliceReader;->upto:I

    .line 112
    add-int v2, v1, v0

    iget v3, p0, Lorg/apache/lucene/index/ByteSliceReader;->endIndex:I

    if-lt v2, v3, :cond_1

    .line 114
    sget-boolean v2, Lorg/apache/lucene/index/ByteSliceReader;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    iget v2, p0, Lorg/apache/lucene/index/ByteSliceReader;->endIndex:I

    sub-int/2addr v2, v1

    if-gtz v2, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 115
    :cond_0
    iget v2, p0, Lorg/apache/lucene/index/ByteSliceReader;->endIndex:I

    iget v3, p0, Lorg/apache/lucene/index/ByteSliceReader;->bufferOffset:I

    sub-int/2addr v2, v3

    iput v2, p0, Lorg/apache/lucene/index/ByteSliceReader;->limit:I

    .line 121
    :goto_0
    return-void

    .line 119
    :cond_1
    iget v2, p0, Lorg/apache/lucene/index/ByteSliceReader;->upto:I

    add-int/2addr v2, v0

    add-int/lit8 v2, v2, -0x4

    iput v2, p0, Lorg/apache/lucene/index/ByteSliceReader;->limit:I

    goto :goto_0
.end method

.method public readByte()B
    .locals 3

    .prologue
    .line 73
    sget-boolean v0, Lorg/apache/lucene/index/ByteSliceReader;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/index/ByteSliceReader;->eof()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 74
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/index/ByteSliceReader;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget v0, p0, Lorg/apache/lucene/index/ByteSliceReader;->upto:I

    iget v1, p0, Lorg/apache/lucene/index/ByteSliceReader;->limit:I

    if-le v0, v1, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 75
    :cond_1
    iget v0, p0, Lorg/apache/lucene/index/ByteSliceReader;->upto:I

    iget v1, p0, Lorg/apache/lucene/index/ByteSliceReader;->limit:I

    if-ne v0, v1, :cond_2

    .line 76
    invoke-virtual {p0}, Lorg/apache/lucene/index/ByteSliceReader;->nextSlice()V

    .line 77
    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/index/ByteSliceReader;->buffer:[B

    iget v1, p0, Lorg/apache/lucene/index/ByteSliceReader;->upto:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/index/ByteSliceReader;->upto:I

    aget-byte v0, v0, v1

    return v0
.end method

.method public readBytes([BII)V
    .locals 3
    .param p1, "b"    # [B
    .param p2, "offset"    # I
    .param p3, "len"    # I

    .prologue
    .line 125
    :goto_0
    if-gtz p3, :cond_0

    .line 140
    :goto_1
    return-void

    .line 126
    :cond_0
    iget v1, p0, Lorg/apache/lucene/index/ByteSliceReader;->limit:I

    iget v2, p0, Lorg/apache/lucene/index/ByteSliceReader;->upto:I

    sub-int v0, v1, v2

    .line 127
    .local v0, "numLeft":I
    if-ge v0, p3, :cond_1

    .line 129
    iget-object v1, p0, Lorg/apache/lucene/index/ByteSliceReader;->buffer:[B

    iget v2, p0, Lorg/apache/lucene/index/ByteSliceReader;->upto:I

    invoke-static {v1, v2, p1, p2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 130
    add-int/2addr p2, v0

    .line 131
    sub-int/2addr p3, v0

    .line 132
    invoke-virtual {p0}, Lorg/apache/lucene/index/ByteSliceReader;->nextSlice()V

    goto :goto_0

    .line 135
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/index/ByteSliceReader;->buffer:[B

    iget v2, p0, Lorg/apache/lucene/index/ByteSliceReader;->upto:I

    invoke-static {v1, v2, p1, p2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 136
    iget v1, p0, Lorg/apache/lucene/index/ByteSliceReader;->upto:I

    add-int/2addr v1, p3

    iput v1, p0, Lorg/apache/lucene/index/ByteSliceReader;->upto:I

    goto :goto_1
.end method

.method public writeTo(Lorg/apache/lucene/store/DataOutput;)J
    .locals 6
    .param p1, "out"    # Lorg/apache/lucene/store/DataOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 81
    const-wide/16 v0, 0x0

    .line 83
    .local v0, "size":J
    :goto_0
    iget v2, p0, Lorg/apache/lucene/index/ByteSliceReader;->limit:I

    iget v3, p0, Lorg/apache/lucene/index/ByteSliceReader;->bufferOffset:I

    add-int/2addr v2, v3

    iget v3, p0, Lorg/apache/lucene/index/ByteSliceReader;->endIndex:I

    if-ne v2, v3, :cond_1

    .line 84
    sget-boolean v2, Lorg/apache/lucene/index/ByteSliceReader;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    iget v2, p0, Lorg/apache/lucene/index/ByteSliceReader;->endIndex:I

    iget v3, p0, Lorg/apache/lucene/index/ByteSliceReader;->bufferOffset:I

    sub-int/2addr v2, v3

    iget v3, p0, Lorg/apache/lucene/index/ByteSliceReader;->upto:I

    if-ge v2, v3, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 85
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/index/ByteSliceReader;->buffer:[B

    iget v3, p0, Lorg/apache/lucene/index/ByteSliceReader;->upto:I

    iget v4, p0, Lorg/apache/lucene/index/ByteSliceReader;->limit:I

    iget v5, p0, Lorg/apache/lucene/index/ByteSliceReader;->upto:I

    sub-int/2addr v4, v5

    invoke-virtual {p1, v2, v3, v4}, Lorg/apache/lucene/store/DataOutput;->writeBytes([BII)V

    .line 86
    iget v2, p0, Lorg/apache/lucene/index/ByteSliceReader;->limit:I

    iget v3, p0, Lorg/apache/lucene/index/ByteSliceReader;->upto:I

    sub-int/2addr v2, v3

    int-to-long v2, v2

    add-long/2addr v0, v2

    .line 95
    return-wide v0

    .line 89
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/index/ByteSliceReader;->buffer:[B

    iget v3, p0, Lorg/apache/lucene/index/ByteSliceReader;->upto:I

    iget v4, p0, Lorg/apache/lucene/index/ByteSliceReader;->limit:I

    iget v5, p0, Lorg/apache/lucene/index/ByteSliceReader;->upto:I

    sub-int/2addr v4, v5

    invoke-virtual {p1, v2, v3, v4}, Lorg/apache/lucene/store/DataOutput;->writeBytes([BII)V

    .line 90
    iget v2, p0, Lorg/apache/lucene/index/ByteSliceReader;->limit:I

    iget v3, p0, Lorg/apache/lucene/index/ByteSliceReader;->upto:I

    sub-int/2addr v2, v3

    int-to-long v2, v2

    add-long/2addr v0, v2

    .line 91
    invoke-virtual {p0}, Lorg/apache/lucene/index/ByteSliceReader;->nextSlice()V

    goto :goto_0
.end method
