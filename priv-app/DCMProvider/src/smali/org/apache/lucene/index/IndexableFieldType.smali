.class public interface abstract Lorg/apache/lucene/index/IndexableFieldType;
.super Ljava/lang/Object;
.source "IndexableFieldType.java"


# virtual methods
.method public abstract docValueType()Lorg/apache/lucene/index/FieldInfo$DocValuesType;
.end method

.method public abstract indexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;
.end method

.method public abstract indexed()Z
.end method

.method public abstract omitNorms()Z
.end method

.method public abstract storeTermVectorOffsets()Z
.end method

.method public abstract storeTermVectorPayloads()Z
.end method

.method public abstract storeTermVectorPositions()Z
.end method

.method public abstract storeTermVectors()Z
.end method

.method public abstract stored()Z
.end method

.method public abstract tokenized()Z
.end method
