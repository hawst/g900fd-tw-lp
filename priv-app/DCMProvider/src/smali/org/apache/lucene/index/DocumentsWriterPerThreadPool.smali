.class abstract Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;
.super Ljava/lang/Object;
.source "DocumentsWriterPerThreadPool.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private documentsWriter:Lorg/apache/lucene/util/SetOnce;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/SetOnce",
            "<",
            "Lorg/apache/lucene/index/DocumentsWriter;",
            ">;"
        }
    .end annotation
.end field

.field private globalFieldMap:Lorg/apache/lucene/util/SetOnce;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/SetOnce",
            "<",
            "Lorg/apache/lucene/index/FieldInfos$FieldNumbers;",
            ">;"
        }
    .end annotation
.end field

.field private volatile numThreadStatesActive:I

.field private threadStates:[Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(I)V
    .locals 3
    .param p1, "maxNumThreadStates"    # I

    .prologue
    .line 130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 124
    new-instance v0, Lorg/apache/lucene/util/SetOnce;

    invoke-direct {v0}, Lorg/apache/lucene/util/SetOnce;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->globalFieldMap:Lorg/apache/lucene/util/SetOnce;

    .line 125
    new-instance v0, Lorg/apache/lucene/util/SetOnce;

    invoke-direct {v0}, Lorg/apache/lucene/util/SetOnce;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->documentsWriter:Lorg/apache/lucene/util/SetOnce;

    .line 131
    const/4 v0, 0x1

    if-ge p1, v0, :cond_0

    .line 132
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "maxNumThreadStates must be >= 1 but was: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 134
    :cond_0
    new-array v0, p1, [Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->threadStates:[Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;

    .line 135
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->numThreadStatesActive:I

    .line 136
    return-void
.end method

.method private declared-synchronized assertUnreleasedThreadStatesInactive()Z
    .locals 3

    .prologue
    .line 217
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->numThreadStatesActive:I

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->threadStates:[Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;

    array-length v1, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-lt v0, v1, :cond_0

    .line 225
    const/4 v1, 0x1

    monitor-exit p0

    return v1

    .line 218
    :cond_0
    :try_start_1
    sget-boolean v1, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->threadStates:[Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->tryLock()Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    const-string v2, "unreleased threadstate should not be locked"

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 217
    .end local v0    # "i":I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 220
    .restart local v0    # "i":I
    :cond_1
    :try_start_2
    sget-boolean v1, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->$assertionsDisabled:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->threadStates:[Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->isActive()Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v1, Ljava/lang/AssertionError;

    const-string v2, "expected unreleased thread state to be inactive"

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 221
    :catchall_1
    move-exception v1

    .line 222
    :try_start_3
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->threadStates:[Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->unlock()V

    .line 223
    throw v1

    .line 222
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->threadStates:[Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->unlock()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 217
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->clone()Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;
    .locals 3

    .prologue
    .line 150
    sget-boolean v2, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    iget v2, p0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->numThreadStatesActive:I

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 153
    :cond_0
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 158
    .local v0, "clone":Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;
    new-instance v2, Lorg/apache/lucene/util/SetOnce;

    invoke-direct {v2}, Lorg/apache/lucene/util/SetOnce;-><init>()V

    iput-object v2, v0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->documentsWriter:Lorg/apache/lucene/util/SetOnce;

    .line 159
    new-instance v2, Lorg/apache/lucene/util/SetOnce;

    invoke-direct {v2}, Lorg/apache/lucene/util/SetOnce;-><init>()V

    iput-object v2, v0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->globalFieldMap:Lorg/apache/lucene/util/SetOnce;

    .line 160
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->threadStates:[Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;

    array-length v2, v2

    new-array v2, v2, [Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;

    iput-object v2, v0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->threadStates:[Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;

    .line 161
    return-object v0

    .line 154
    .end local v0    # "clone":Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;
    :catch_0
    move-exception v1

    .line 156
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method deactivateThreadState(Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;)V
    .locals 1
    .param p1, "threadState"    # Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;

    .prologue
    .line 328
    sget-boolean v0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->isActive()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 329
    :cond_0
    const/4 v0, 0x0

    # invokes: Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->resetWriter(Lorg/apache/lucene/index/DocumentsWriterPerThread;)V
    invoke-static {p1, v0}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->access$0(Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;Lorg/apache/lucene/index/DocumentsWriterPerThread;)V

    .line 330
    return-void
.end method

.method declared-synchronized deactivateUnreleasedStates()V
    .locals 3

    .prologue
    .line 232
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->numThreadStatesActive:I

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->threadStates:[Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;

    array-length v2, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-lt v0, v2, :cond_0

    .line 241
    monitor-exit p0

    return-void

    .line 233
    :cond_0
    :try_start_1
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->threadStates:[Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;

    aget-object v1, v2, v0

    .line 234
    .local v1, "threadState":Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;
    invoke-virtual {v1}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->lock()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 236
    const/4 v2, 0x0

    :try_start_2
    # invokes: Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->resetWriter(Lorg/apache/lucene/index/DocumentsWriterPerThread;)V
    invoke-static {v1, v2}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->access$0(Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;Lorg/apache/lucene/index/DocumentsWriterPerThread;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 238
    :try_start_3
    invoke-virtual {v1}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->unlock()V

    .line 232
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 237
    :catchall_0
    move-exception v2

    .line 238
    invoke-virtual {v1}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->unlock()V

    .line 239
    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 232
    .end local v0    # "i":I
    .end local v1    # "threadState":Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;
    :catchall_1
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method getActiveThreadState()I
    .locals 1

    .prologue
    .line 176
    iget v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->numThreadStatesActive:I

    return v0
.end method

.method abstract getAndLock(Ljava/lang/Thread;Lorg/apache/lucene/index/DocumentsWriter;)Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;
.end method

.method getMaxThreadStates()I
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->threadStates:[Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;

    array-length v0, v0

    return v0
.end method

.method getThreadState(I)Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;
    .locals 1
    .param p1, "ord"    # I

    .prologue
    .line 277
    sget-boolean v0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->numThreadStatesActive:I

    if-lt p1, v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 278
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->threadStates:[Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;

    aget-object v0, v0, p1

    return-object v0
.end method

.method initialize(Lorg/apache/lucene/index/DocumentsWriter;Lorg/apache/lucene/index/FieldInfos$FieldNumbers;Lorg/apache/lucene/index/LiveIndexWriterConfig;)V
    .locals 7
    .param p1, "documentsWriter"    # Lorg/apache/lucene/index/DocumentsWriter;
    .param p2, "globalFieldMap"    # Lorg/apache/lucene/index/FieldInfos$FieldNumbers;
    .param p3, "config"    # Lorg/apache/lucene/index/LiveIndexWriterConfig;

    .prologue
    .line 139
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->documentsWriter:Lorg/apache/lucene/util/SetOnce;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/util/SetOnce;->set(Ljava/lang/Object;)V

    .line 140
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->globalFieldMap:Lorg/apache/lucene/util/SetOnce;

    invoke-virtual {v2, p2}, Lorg/apache/lucene/util/SetOnce;->set(Ljava/lang/Object;)V

    .line 141
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->threadStates:[Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;

    array-length v2, v2

    if-lt v0, v2, :cond_0

    .line 145
    return-void

    .line 142
    :cond_0
    new-instance v1, Lorg/apache/lucene/index/FieldInfos$Builder;

    invoke-direct {v1, p2}, Lorg/apache/lucene/index/FieldInfos$Builder;-><init>(Lorg/apache/lucene/index/FieldInfos$FieldNumbers;)V

    .line 143
    .local v1, "infos":Lorg/apache/lucene/index/FieldInfos$Builder;
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->threadStates:[Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;

    new-instance v3, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;

    new-instance v4, Lorg/apache/lucene/index/DocumentsWriterPerThread;

    iget-object v5, p1, Lorg/apache/lucene/index/DocumentsWriter;->directory:Lorg/apache/lucene/store/Directory;

    iget-object v6, p1, Lorg/apache/lucene/index/DocumentsWriter;->chain:Lorg/apache/lucene/index/DocumentsWriterPerThread$IndexingChain;

    invoke-direct {v4, v5, p1, v1, v6}, Lorg/apache/lucene/index/DocumentsWriterPerThread;-><init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/DocumentsWriter;Lorg/apache/lucene/index/FieldInfos$Builder;Lorg/apache/lucene/index/DocumentsWriterPerThread$IndexingChain;)V

    invoke-direct {v3, v4}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;-><init>(Lorg/apache/lucene/index/DocumentsWriterPerThread;)V

    aput-object v3, v2, v0

    .line 141
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method minContendedThreadState()Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;
    .locals 6

    .prologue
    .line 287
    const/4 v2, 0x0

    .line 288
    .local v2, "minThreadState":Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;
    iget v1, p0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->numThreadStatesActive:I

    .line 289
    .local v1, "limit":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v1, :cond_0

    .line 295
    return-object v2

    .line 290
    :cond_0
    iget-object v4, p0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->threadStates:[Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;

    aget-object v3, v4, v0

    .line 291
    .local v3, "state":Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;
    if-eqz v2, :cond_1

    invoke-virtual {v3}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->getQueueLength()I

    move-result v4

    invoke-virtual {v2}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->getQueueLength()I

    move-result v5

    if-ge v4, v5, :cond_2

    .line 292
    :cond_1
    move-object v2, v3

    .line 289
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method declared-synchronized newThreadState()Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 190
    monitor-enter p0

    :try_start_0
    iget v3, p0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->numThreadStatesActive:I

    iget-object v4, p0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->threadStates:[Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;

    array-length v4, v4

    if-ge v3, v4, :cond_6

    .line 191
    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->threadStates:[Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;

    iget v4, p0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->numThreadStatesActive:I

    aget-object v0, v3, v4

    .line 192
    .local v0, "threadState":Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;
    invoke-virtual {v0}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->lock()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 193
    const/4 v1, 0x1

    .line 195
    .local v1, "unlock":Z
    :try_start_1
    invoke-virtual {v0}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->isActive()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 197
    iget v2, p0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->numThreadStatesActive:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->numThreadStatesActive:I

    .line 198
    sget-boolean v2, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->$assertionsDisabled:Z

    if-nez v2, :cond_1

    iget-object v2, v0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->dwpt:Lorg/apache/lucene/index/DocumentsWriterPerThread;

    if-nez v2, :cond_1

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 206
    :catchall_0
    move-exception v2

    .line 207
    if-eqz v1, :cond_0

    .line 209
    :try_start_2
    invoke-virtual {v0}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->unlock()V

    .line 211
    :cond_0
    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 190
    .end local v0    # "threadState":Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;
    .end local v1    # "unlock":Z
    :catchall_1
    move-exception v2

    monitor-exit p0

    throw v2

    .line 199
    .restart local v0    # "threadState":Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;
    .restart local v1    # "unlock":Z
    :cond_1
    :try_start_3
    iget-object v2, v0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->dwpt:Lorg/apache/lucene/index/DocumentsWriterPerThread;

    invoke-virtual {v2}, Lorg/apache/lucene/index/DocumentsWriterPerThread;->initialize()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 200
    const/4 v1, 0x0

    .line 207
    if-eqz v1, :cond_2

    .line 209
    :try_start_4
    invoke-virtual {v0}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->unlock()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 213
    .end local v0    # "threadState":Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;
    .end local v1    # "unlock":Z
    :cond_2
    :goto_0
    monitor-exit p0

    return-object v0

    .line 204
    .restart local v0    # "threadState":Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;
    .restart local v1    # "unlock":Z
    :cond_3
    :try_start_5
    sget-boolean v3, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->$assertionsDisabled:Z

    if-nez v3, :cond_4

    invoke-direct {p0}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->assertUnreleasedThreadStatesInactive()Z

    move-result v3

    if-nez v3, :cond_4

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 207
    :cond_4
    if-eqz v1, :cond_5

    .line 209
    :try_start_6
    invoke-virtual {v0}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->unlock()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :cond_5
    move-object v0, v2

    .line 205
    goto :goto_0

    .end local v0    # "threadState":Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;
    .end local v1    # "unlock":Z
    :cond_6
    move-object v0, v2

    .line 213
    goto :goto_0
.end method

.method numDeactivatedThreadStates()I
    .locals 4

    .prologue
    .line 305
    const/4 v0, 0x0

    .line 306
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->threadStates:[Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;

    array-length v3, v3

    if-lt v1, v3, :cond_0

    .line 317
    return v0

    .line 307
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->threadStates:[Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;

    aget-object v2, v3, v1

    .line 308
    .local v2, "threadState":Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;
    invoke-virtual {v2}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->lock()V

    .line 310
    :try_start_0
    # getter for: Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->isActive:Z
    invoke-static {v2}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->access$1(Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_1

    .line 311
    add-int/lit8 v0, v0, 0x1

    .line 314
    :cond_1
    invoke-virtual {v2}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->unlock()V

    .line 306
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 313
    :catchall_0
    move-exception v3

    .line 314
    invoke-virtual {v2}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->unlock()V

    .line 315
    throw v3
.end method

.method recycle(Lorg/apache/lucene/index/DocumentsWriterPerThread;)V
    .locals 0
    .param p1, "dwpt"    # Lorg/apache/lucene/index/DocumentsWriterPerThread;

    .prologue
    .line 260
    return-void
.end method

.method reinitThreadState(Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;)V
    .locals 1
    .param p1, "threadState"    # Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;

    .prologue
    .line 339
    sget-boolean v0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    # getter for: Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->isActive:Z
    invoke-static {p1}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->access$1(Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 340
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-object v0, p1, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->dwpt:Lorg/apache/lucene/index/DocumentsWriterPerThread;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocumentsWriterPerThread;->getNumDocsInRAM()I

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 341
    :cond_1
    iget-object v0, p1, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->dwpt:Lorg/apache/lucene/index/DocumentsWriterPerThread;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocumentsWriterPerThread;->initialize()V

    .line 342
    return-void
.end method

.method replaceForFlush(Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;Z)Lorg/apache/lucene/index/DocumentsWriterPerThread;
    .locals 4
    .param p1, "threadState"    # Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;
    .param p2, "closed"    # Z

    .prologue
    .line 244
    sget-boolean v3, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    invoke-virtual {p1}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->isHeldByCurrentThread()Z

    move-result v3

    if-nez v3, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 245
    :cond_0
    sget-boolean v3, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->$assertionsDisabled:Z

    if-nez v3, :cond_1

    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->globalFieldMap:Lorg/apache/lucene/util/SetOnce;

    invoke-virtual {v3}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_1

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 246
    :cond_1
    iget-object v0, p1, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->dwpt:Lorg/apache/lucene/index/DocumentsWriterPerThread;

    .line 247
    .local v0, "dwpt":Lorg/apache/lucene/index/DocumentsWriterPerThread;
    if-nez p2, :cond_2

    .line 248
    new-instance v1, Lorg/apache/lucene/index/FieldInfos$Builder;

    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->globalFieldMap:Lorg/apache/lucene/util/SetOnce;

    invoke-virtual {v3}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/index/FieldInfos$FieldNumbers;

    invoke-direct {v1, v3}, Lorg/apache/lucene/index/FieldInfos$Builder;-><init>(Lorg/apache/lucene/index/FieldInfos$FieldNumbers;)V

    .line 249
    .local v1, "infos":Lorg/apache/lucene/index/FieldInfos$Builder;
    new-instance v2, Lorg/apache/lucene/index/DocumentsWriterPerThread;

    invoke-direct {v2, v0, v1}, Lorg/apache/lucene/index/DocumentsWriterPerThread;-><init>(Lorg/apache/lucene/index/DocumentsWriterPerThread;Lorg/apache/lucene/index/FieldInfos$Builder;)V

    .line 250
    .local v2, "newDwpt":Lorg/apache/lucene/index/DocumentsWriterPerThread;
    invoke-virtual {v2}, Lorg/apache/lucene/index/DocumentsWriterPerThread;->initialize()V

    .line 251
    # invokes: Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->resetWriter(Lorg/apache/lucene/index/DocumentsWriterPerThread;)V
    invoke-static {p1, v2}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->access$0(Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;Lorg/apache/lucene/index/DocumentsWriterPerThread;)V

    .line 255
    .end local v1    # "infos":Lorg/apache/lucene/index/FieldInfos$Builder;
    .end local v2    # "newDwpt":Lorg/apache/lucene/index/DocumentsWriterPerThread;
    :goto_0
    return-object v0

    .line 253
    :cond_2
    const/4 v3, 0x0

    # invokes: Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->resetWriter(Lorg/apache/lucene/index/DocumentsWriterPerThread;)V
    invoke-static {p1, v3}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->access$0(Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;Lorg/apache/lucene/index/DocumentsWriterPerThread;)V

    goto :goto_0
.end method
