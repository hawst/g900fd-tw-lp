.class public Lorg/apache/lucene/index/SingletonSortedSetDocValues;
.super Lorg/apache/lucene/index/SortedSetDocValues;
.source "SingletonSortedSetDocValues.java"


# instance fields
.field private docID:I

.field private final in:Lorg/apache/lucene/index/SortedDocValues;

.field private set:Z


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/SortedDocValues;)V
    .locals 0
    .param p1, "in"    # Lorg/apache/lucene/index/SortedDocValues;

    .prologue
    .line 35
    invoke-direct {p0}, Lorg/apache/lucene/index/SortedSetDocValues;-><init>()V

    .line 36
    iput-object p1, p0, Lorg/apache/lucene/index/SingletonSortedSetDocValues;->in:Lorg/apache/lucene/index/SortedDocValues;

    .line 38
    return-void
.end method


# virtual methods
.method public getValueCount()J
    .locals 2

    .prologue
    .line 64
    iget-object v0, p0, Lorg/apache/lucene/index/SingletonSortedSetDocValues;->in:Lorg/apache/lucene/index/SortedDocValues;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SortedDocValues;->getValueCount()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public lookupOrd(JLorg/apache/lucene/util/BytesRef;)V
    .locals 3
    .param p1, "ord"    # J
    .param p3, "result"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 59
    iget-object v0, p0, Lorg/apache/lucene/index/SingletonSortedSetDocValues;->in:Lorg/apache/lucene/index/SortedDocValues;

    long-to-int v1, p1

    invoke-virtual {v0, v1, p3}, Lorg/apache/lucene/index/SortedDocValues;->lookupOrd(ILorg/apache/lucene/util/BytesRef;)V

    .line 60
    return-void
.end method

.method public lookupTerm(Lorg/apache/lucene/util/BytesRef;)J
    .locals 2
    .param p1, "key"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 69
    iget-object v0, p0, Lorg/apache/lucene/index/SingletonSortedSetDocValues;->in:Lorg/apache/lucene/index/SortedDocValues;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/SortedDocValues;->lookupTerm(Lorg/apache/lucene/util/BytesRef;)I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public nextOrd()J
    .locals 2

    .prologue
    .line 42
    iget-boolean v0, p0, Lorg/apache/lucene/index/SingletonSortedSetDocValues;->set:Z

    if-eqz v0, :cond_0

    .line 43
    const-wide/16 v0, -0x1

    .line 46
    :goto_0
    return-wide v0

    .line 45
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/index/SingletonSortedSetDocValues;->set:Z

    .line 46
    iget-object v0, p0, Lorg/apache/lucene/index/SingletonSortedSetDocValues;->in:Lorg/apache/lucene/index/SortedDocValues;

    iget v1, p0, Lorg/apache/lucene/index/SingletonSortedSetDocValues;->docID:I

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/SortedDocValues;->getOrd(I)I

    move-result v0

    int-to-long v0, v0

    goto :goto_0
.end method

.method public setDocument(I)V
    .locals 1
    .param p1, "docID"    # I

    .prologue
    .line 52
    iput p1, p0, Lorg/apache/lucene/index/SingletonSortedSetDocValues;->docID:I

    .line 53
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/index/SingletonSortedSetDocValues;->set:Z

    .line 54
    return-void
.end method
