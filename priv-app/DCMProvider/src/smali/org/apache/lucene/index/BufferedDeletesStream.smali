.class Lorg/apache/lucene/index/BufferedDeletesStream;
.super Ljava/lang/Object;
.source "BufferedDeletesStream.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/BufferedDeletesStream$ApplyDeletesResult;,
        Lorg/apache/lucene/index/BufferedDeletesStream$QueryAndLimit;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final sortSegInfoByDelGen:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/index/SegmentInfoPerCommit;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

.field private final deletes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/FrozenBufferedDeletes;",
            ">;"
        }
    .end annotation
.end field

.field private final infoStream:Lorg/apache/lucene/util/InfoStream;

.field private lastDeleteTerm:Lorg/apache/lucene/index/Term;

.field private nextGen:J

.field private final numTerms:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const-class v0, Lorg/apache/lucene/index/BufferedDeletesStream;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/BufferedDeletesStream;->$assertionsDisabled:Z

    .line 134
    new-instance v0, Lorg/apache/lucene/index/BufferedDeletesStream$1;

    invoke-direct {v0}, Lorg/apache/lucene/index/BufferedDeletesStream$1;-><init>()V

    sput-object v0, Lorg/apache/lucene/index/BufferedDeletesStream;->sortSegInfoByDelGen:Ljava/util/Comparator;

    .line 146
    return-void

    .line 51
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/util/InfoStream;)V
    .locals 2
    .param p1, "infoStream"    # Lorg/apache/lucene/util/InfoStream;

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->deletes:Ljava/util/List;

    .line 59
    const-wide/16 v0, 0x1

    iput-wide v0, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->nextGen:J

    .line 65
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

    .line 66
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->numTerms:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 69
    iput-object p1, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->infoStream:Lorg/apache/lucene/util/InfoStream;

    .line 70
    return-void
.end method

.method private static applyQueryDeletes(Ljava/lang/Iterable;Lorg/apache/lucene/index/ReadersAndLiveDocs;Lorg/apache/lucene/index/SegmentReader;)J
    .locals 14
    .param p1, "rld"    # Lorg/apache/lucene/index/ReadersAndLiveDocs;
    .param p2, "reader"    # Lorg/apache/lucene/index/SegmentReader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lorg/apache/lucene/index/BufferedDeletesStream$QueryAndLimit;",
            ">;",
            "Lorg/apache/lucene/index/ReadersAndLiveDocs;",
            "Lorg/apache/lucene/index/SegmentReader;",
            ")J"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 443
    .local p0, "queriesIter":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lorg/apache/lucene/index/BufferedDeletesStream$QueryAndLimit;>;"
    const-wide/16 v2, 0x0

    .line 444
    .local v2, "delCount":J
    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/index/SegmentReader;->getContext()Lorg/apache/lucene/index/AtomicReaderContext;

    move-result-object v9

    .line 445
    .local v9, "readerContext":Lorg/apache/lucene/index/AtomicReaderContext;
    const/4 v0, 0x0

    .line 446
    .local v0, "any":Z
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-nez v11, :cond_1

    .line 472
    return-wide v2

    .line 446
    :cond_1
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/index/BufferedDeletesStream$QueryAndLimit;

    .line 447
    .local v5, "ent":Lorg/apache/lucene/index/BufferedDeletesStream$QueryAndLimit;
    iget-object v8, v5, Lorg/apache/lucene/index/BufferedDeletesStream$QueryAndLimit;->query:Lorg/apache/lucene/search/Query;

    .line 448
    .local v8, "query":Lorg/apache/lucene/search/Query;
    iget v7, v5, Lorg/apache/lucene/index/BufferedDeletesStream$QueryAndLimit;->limit:I

    .line 449
    .local v7, "limit":I
    new-instance v11, Lorg/apache/lucene/search/QueryWrapperFilter;

    invoke-direct {v11, v8}, Lorg/apache/lucene/search/QueryWrapperFilter;-><init>(Lorg/apache/lucene/search/Query;)V

    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/index/SegmentReader;->getLiveDocs()Lorg/apache/lucene/util/Bits;

    move-result-object v12

    invoke-virtual {v11, v9, v12}, Lorg/apache/lucene/search/QueryWrapperFilter;->getDocIdSet(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/DocIdSet;

    move-result-object v4

    .line 450
    .local v4, "docs":Lorg/apache/lucene/search/DocIdSet;
    if-eqz v4, :cond_0

    .line 451
    invoke-virtual {v4}, Lorg/apache/lucene/search/DocIdSet;->iterator()Lorg/apache/lucene/search/DocIdSetIterator;

    move-result-object v6

    .line 452
    .local v6, "it":Lorg/apache/lucene/search/DocIdSetIterator;
    if-eqz v6, :cond_0

    .line 454
    :cond_2
    :goto_0
    invoke-virtual {v6}, Lorg/apache/lucene/search/DocIdSetIterator;->nextDoc()I

    move-result v1

    .line 455
    .local v1, "doc":I
    if-ge v1, v7, :cond_0

    .line 459
    if-nez v0, :cond_3

    .line 460
    invoke-virtual {p1}, Lorg/apache/lucene/index/ReadersAndLiveDocs;->initWritableLiveDocs()V

    .line 461
    const/4 v0, 0x1

    .line 464
    :cond_3
    invoke-virtual {p1, v1}, Lorg/apache/lucene/index/ReadersAndLiveDocs;->delete(I)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 465
    const-wide/16 v12, 0x1

    add-long/2addr v2, v12

    .line 453
    goto :goto_0
.end method

.method private declared-synchronized applyTermDeletes(Ljava/lang/Iterable;Lorg/apache/lucene/index/ReadersAndLiveDocs;Lorg/apache/lucene/index/SegmentReader;)J
    .locals 16
    .param p2, "rld"    # Lorg/apache/lucene/index/ReadersAndLiveDocs;
    .param p3, "reader"    # Lorg/apache/lucene/index/SegmentReader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lorg/apache/lucene/index/Term;",
            ">;",
            "Lorg/apache/lucene/index/ReadersAndLiveDocs;",
            "Lorg/apache/lucene/index/SegmentReader;",
            ")J"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 361
    .local p1, "termsIter":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lorg/apache/lucene/index/Term;>;"
    monitor-enter p0

    const-wide/16 v4, 0x0

    .line 362
    .local v4, "delCount":J
    :try_start_0
    invoke-virtual/range {p3 .. p3}, Lorg/apache/lucene/index/SegmentReader;->fields()Lorg/apache/lucene/index/Fields;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v9

    .line 363
    .local v9, "fields":Lorg/apache/lucene/index/Fields;
    if-nez v9, :cond_0

    .line 365
    const-wide/16 v14, 0x0

    .line 429
    :goto_0
    monitor-exit p0

    return-wide v14

    .line 368
    :cond_0
    const/4 v12, 0x0

    .line 370
    .local v12, "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    const/4 v3, 0x0

    .line 371
    .local v3, "currentField":Ljava/lang/String;
    const/4 v7, 0x0

    .line 373
    .local v7, "docs":Lorg/apache/lucene/index/DocsEnum;
    :try_start_1
    sget-boolean v13, Lorg/apache/lucene/index/BufferedDeletesStream;->$assertionsDisabled:Z

    if-nez v13, :cond_1

    const/4 v13, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lorg/apache/lucene/index/BufferedDeletesStream;->checkDeleteTerm(Lorg/apache/lucene/index/Term;)Z

    move-result v13

    if-nez v13, :cond_1

    new-instance v13, Ljava/lang/AssertionError;

    invoke-direct {v13}, Ljava/lang/AssertionError;-><init>()V

    throw v13
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 361
    .end local v3    # "currentField":Ljava/lang/String;
    .end local v7    # "docs":Lorg/apache/lucene/index/DocsEnum;
    .end local v9    # "fields":Lorg/apache/lucene/index/Fields;
    .end local v12    # "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    :catchall_0
    move-exception v13

    monitor-exit p0

    throw v13

    .line 375
    .restart local v3    # "currentField":Ljava/lang/String;
    .restart local v7    # "docs":Lorg/apache/lucene/index/DocsEnum;
    .restart local v9    # "fields":Lorg/apache/lucene/index/Fields;
    .restart local v12    # "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    :cond_1
    const/4 v2, 0x0

    .line 378
    .local v2, "any":Z
    :try_start_2
    invoke-interface/range {p1 .. p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_2
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-nez v14, :cond_3

    move-wide v14, v4

    .line 429
    goto :goto_0

    .line 378
    :cond_3
    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lorg/apache/lucene/index/Term;

    .line 382
    .local v10, "term":Lorg/apache/lucene/index/Term;
    invoke-virtual {v10}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v14, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_5

    .line 383
    sget-boolean v14, Lorg/apache/lucene/index/BufferedDeletesStream;->$assertionsDisabled:Z

    if-nez v14, :cond_4

    if-eqz v3, :cond_4

    invoke-virtual {v10}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v3, v14}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v14

    if-ltz v14, :cond_4

    new-instance v13, Ljava/lang/AssertionError;

    invoke-direct {v13}, Ljava/lang/AssertionError;-><init>()V

    throw v13

    .line 384
    :cond_4
    invoke-virtual {v10}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v3

    .line 385
    invoke-virtual {v9, v3}, Lorg/apache/lucene/index/Fields;->terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v11

    .line 386
    .local v11, "terms":Lorg/apache/lucene/index/Terms;
    if-eqz v11, :cond_6

    .line 387
    const/4 v14, 0x0

    invoke-virtual {v11, v14}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v12

    .line 393
    .end local v11    # "terms":Lorg/apache/lucene/index/Terms;
    :cond_5
    :goto_1
    if-eqz v12, :cond_2

    .line 396
    sget-boolean v14, Lorg/apache/lucene/index/BufferedDeletesStream;->$assertionsDisabled:Z

    if-nez v14, :cond_7

    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lorg/apache/lucene/index/BufferedDeletesStream;->checkDeleteTerm(Lorg/apache/lucene/index/Term;)Z

    move-result v14

    if-nez v14, :cond_7

    new-instance v13, Ljava/lang/AssertionError;

    invoke-direct {v13}, Ljava/lang/AssertionError;-><init>()V

    throw v13

    .line 389
    .restart local v11    # "terms":Lorg/apache/lucene/index/Terms;
    :cond_6
    const/4 v12, 0x0

    goto :goto_1

    .line 400
    .end local v11    # "terms":Lorg/apache/lucene/index/Terms;
    :cond_7
    invoke-virtual {v10}, Lorg/apache/lucene/index/Term;->bytes()Lorg/apache/lucene/util/BytesRef;

    move-result-object v14

    const/4 v15, 0x0

    invoke-virtual {v12, v14, v15}, Lorg/apache/lucene/index/TermsEnum;->seekExact(Lorg/apache/lucene/util/BytesRef;Z)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 402
    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/index/ReadersAndLiveDocs;->getLiveDocs()Lorg/apache/lucene/util/Bits;

    move-result-object v14

    const/4 v15, 0x0

    invoke-virtual {v12, v14, v7, v15}, Lorg/apache/lucene/index/TermsEnum;->docs(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;I)Lorg/apache/lucene/index/DocsEnum;

    move-result-object v8

    .line 405
    .local v8, "docsEnum":Lorg/apache/lucene/index/DocsEnum;
    if-eqz v8, :cond_2

    .line 407
    :cond_8
    :goto_2
    invoke-virtual {v8}, Lorg/apache/lucene/index/DocsEnum;->nextDoc()I

    move-result v6

    .line 409
    .local v6, "docID":I
    const v14, 0x7fffffff

    if-eq v6, v14, :cond_2

    .line 417
    if-nez v2, :cond_9

    .line 418
    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/index/ReadersAndLiveDocs;->initWritableLiveDocs()V

    .line 419
    const/4 v2, 0x1

    .line 421
    :cond_9
    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Lorg/apache/lucene/index/ReadersAndLiveDocs;->delete(I)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v14

    if-eqz v14, :cond_8

    .line 422
    const-wide/16 v14, 0x1

    add-long/2addr v4, v14

    .line 406
    goto :goto_2
.end method

.method private checkDeleteStats()Z
    .locals 8

    .prologue
    .line 487
    const/4 v2, 0x0

    .line 488
    .local v2, "numTerms2":I
    const-wide/16 v0, 0x0

    .line 489
    .local v0, "bytesUsed2":J
    iget-object v4, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->deletes:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_0

    .line 493
    sget-boolean v4, Lorg/apache/lucene/index/BufferedDeletesStream;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    iget-object v4, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->numTerms:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v4

    if-eq v2, v4, :cond_1

    new-instance v4, Ljava/lang/AssertionError;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "numTerms2="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " vs "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->numTerms:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    .line 489
    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/index/FrozenBufferedDeletes;

    .line 490
    .local v3, "packet":Lorg/apache/lucene/index/FrozenBufferedDeletes;
    iget v5, v3, Lorg/apache/lucene/index/FrozenBufferedDeletes;->numTermDeletes:I

    add-int/2addr v2, v5

    .line 491
    iget v5, v3, Lorg/apache/lucene/index/FrozenBufferedDeletes;->bytesUsed:I

    int-to-long v6, v5

    add-long/2addr v0, v6

    goto :goto_0

    .line 494
    .end local v3    # "packet":Lorg/apache/lucene/index/FrozenBufferedDeletes;
    :cond_1
    sget-boolean v4, Lorg/apache/lucene/index/BufferedDeletesStream;->$assertionsDisabled:Z

    if-nez v4, :cond_2

    iget-object v4, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v4

    cmp-long v4, v0, v4

    if-eqz v4, :cond_2

    new-instance v4, Ljava/lang/AssertionError;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "bytesUsed2="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " vs "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    .line 495
    :cond_2
    const/4 v4, 0x1

    return v4
.end method

.method private checkDeleteTerm(Lorg/apache/lucene/index/Term;)Z
    .locals 3
    .param p1, "term"    # Lorg/apache/lucene/index/Term;

    .prologue
    .line 477
    if-eqz p1, :cond_0

    .line 478
    sget-boolean v0, Lorg/apache/lucene/index/BufferedDeletesStream;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->lastDeleteTerm:Lorg/apache/lucene/index/Term;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->lastDeleteTerm:Lorg/apache/lucene/index/Term;

    invoke-virtual {p1, v0}, Lorg/apache/lucene/index/Term;->compareTo(Lorg/apache/lucene/index/Term;)I

    move-result v0

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "lastTerm="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->lastDeleteTerm:Lorg/apache/lucene/index/Term;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " vs term="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 481
    :cond_0
    if-nez p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->lastDeleteTerm:Lorg/apache/lucene/index/Term;

    .line 482
    const/4 v0, 0x1

    return v0

    .line 481
    :cond_1
    new-instance v0, Lorg/apache/lucene/index/Term;

    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, Lorg/apache/lucene/index/Term;->bytes:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v2}, Lorg/apache/lucene/util/BytesRef;->deepCopyOf(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/BytesRef;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    goto :goto_0
.end method

.method private declared-synchronized prune(I)V
    .locals 6
    .param p1, "count"    # I

    .prologue
    .line 344
    monitor-enter p0

    if-lez p1, :cond_1

    .line 345
    :try_start_0
    iget-object v2, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v3, "BD"

    invoke-virtual {v2, v3}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 346
    iget-object v2, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v3, "BD"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "pruneDeletes: prune "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " packets; "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->deletes:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    sub-int/2addr v5, p1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " packets remain"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 348
    :cond_0
    const/4 v0, 0x0

    .local v0, "delIDX":I
    :goto_0
    if-lt v0, p1, :cond_2

    .line 355
    iget-object v2, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->deletes:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3, p1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 357
    .end local v0    # "delIDX":I
    :cond_1
    monitor-exit p0

    return-void

    .line 349
    .restart local v0    # "delIDX":I
    :cond_2
    :try_start_1
    iget-object v2, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->deletes:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/FrozenBufferedDeletes;

    .line 350
    .local v1, "packet":Lorg/apache/lucene/index/FrozenBufferedDeletes;
    iget-object v2, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->numTerms:Ljava/util/concurrent/atomic/AtomicInteger;

    iget v3, v1, Lorg/apache/lucene/index/FrozenBufferedDeletes;->numTermDeletes:I

    neg-int v3, v3

    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicInteger;->addAndGet(I)I

    .line 351
    sget-boolean v2, Lorg/apache/lucene/index/BufferedDeletesStream;->$assertionsDisabled:Z

    if-nez v2, :cond_3

    iget-object v2, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->numTerms:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v2

    if-gez v2, :cond_3

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 344
    .end local v0    # "delIDX":I
    .end local v1    # "packet":Lorg/apache/lucene/index/FrozenBufferedDeletes;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 352
    .restart local v0    # "delIDX":I
    .restart local v1    # "packet":Lorg/apache/lucene/index/FrozenBufferedDeletes;
    :cond_3
    :try_start_2
    iget-object v2, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

    iget v3, v1, Lorg/apache/lucene/index/FrozenBufferedDeletes;->bytesUsed:I

    neg-int v3, v3

    int-to-long v4, v3

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    .line 353
    sget-boolean v2, Lorg/apache/lucene/index/BufferedDeletesStream;->$assertionsDisabled:Z

    if-nez v2, :cond_4

    iget-object v2, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-gez v2, :cond_4

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 348
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public any()Z
    .locals 4

    .prologue
    .line 105
    iget-object v0, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized applyDeletes(Lorg/apache/lucene/index/IndexWriter$ReaderPool;Ljava/util/List;)Lorg/apache/lucene/index/BufferedDeletesStream$ApplyDeletesResult;
    .locals 32
    .param p1, "readerPool"    # Lorg/apache/lucene/index/IndexWriter$ReaderPool;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/IndexWriter$ReaderPool;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/SegmentInfoPerCommit;",
            ">;)",
            "Lorg/apache/lucene/index/BufferedDeletesStream$ApplyDeletesResult;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 152
    .local p2, "infos":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfoPerCommit;>;"
    monitor-enter p0

    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v24

    .line 154
    .local v24, "t0":J
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v21

    if-nez v21, :cond_0

    .line 155
    new-instance v21, Lorg/apache/lucene/index/BufferedDeletesStream$ApplyDeletesResult;

    const/16 v26, 0x0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/index/BufferedDeletesStream;->nextGen:J

    move-wide/from16 v28, v0

    const-wide/16 v30, 0x1

    add-long v30, v30, v28

    move-wide/from16 v0, v30

    move-object/from16 v2, p0

    iput-wide v0, v2, Lorg/apache/lucene/index/BufferedDeletesStream;->nextGen:J

    const/16 v27, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v26

    move-wide/from16 v2, v28

    move-object/from16 v4, v27

    invoke-direct {v0, v1, v2, v3, v4}, Lorg/apache/lucene/index/BufferedDeletesStream$ApplyDeletesResult;-><init>(ZJLjava/util/List;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 307
    :goto_0
    monitor-exit p0

    return-object v21

    .line 158
    :cond_0
    :try_start_1
    sget-boolean v21, Lorg/apache/lucene/index/BufferedDeletesStream;->$assertionsDisabled:Z

    if-nez v21, :cond_1

    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/index/BufferedDeletesStream;->checkDeleteStats()Z

    move-result v21

    if-nez v21, :cond_1

    new-instance v21, Ljava/lang/AssertionError;

    invoke-direct/range {v21 .. v21}, Ljava/lang/AssertionError;-><init>()V

    throw v21
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 152
    .end local v24    # "t0":J
    :catchall_0
    move-exception v21

    monitor-exit p0

    throw v21

    .line 160
    .restart local v24    # "t0":J
    :cond_1
    :try_start_2
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/BufferedDeletesStream;->any()Z

    move-result v21

    if-nez v21, :cond_3

    .line 161
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/BufferedDeletesStream;->infoStream:Lorg/apache/lucene/util/InfoStream;

    move-object/from16 v21, v0

    const-string v26, "BD"

    move-object/from16 v0, v21

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_2

    .line 162
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/BufferedDeletesStream;->infoStream:Lorg/apache/lucene/util/InfoStream;

    move-object/from16 v21, v0

    const-string v26, "BD"

    const-string v27, "applyDeletes: no deletes; skipping"

    move-object/from16 v0, v21

    move-object/from16 v1, v26

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    :cond_2
    new-instance v21, Lorg/apache/lucene/index/BufferedDeletesStream$ApplyDeletesResult;

    const/16 v26, 0x0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/index/BufferedDeletesStream;->nextGen:J

    move-wide/from16 v28, v0

    const-wide/16 v30, 0x1

    add-long v30, v30, v28

    move-wide/from16 v0, v30

    move-object/from16 v2, p0

    iput-wide v0, v2, Lorg/apache/lucene/index/BufferedDeletesStream;->nextGen:J

    const/16 v27, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v26

    move-wide/from16 v2, v28

    move-object/from16 v4, v27

    invoke-direct {v0, v1, v2, v3, v4}, Lorg/apache/lucene/index/BufferedDeletesStream$ApplyDeletesResult;-><init>(ZJLjava/util/List;)V

    goto :goto_0

    .line 167
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/BufferedDeletesStream;->infoStream:Lorg/apache/lucene/util/InfoStream;

    move-object/from16 v21, v0

    const-string v26, "BD"

    move-object/from16 v0, v21

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_4

    .line 168
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/BufferedDeletesStream;->infoStream:Lorg/apache/lucene/util/InfoStream;

    move-object/from16 v21, v0

    const-string v26, "BD"

    new-instance v27, Ljava/lang/StringBuilder;

    const-string v28, "applyDeletes: infos="

    invoke-direct/range {v27 .. v28}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v27

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, " packetCount="

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/BufferedDeletesStream;->deletes:Ljava/util/List;

    move-object/from16 v28, v0

    invoke-interface/range {v28 .. v28}, Ljava/util/List;->size()I

    move-result v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v21

    move-object/from16 v1, v26

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    :cond_4
    move-object/from16 v0, p0

    iget-wide v12, v0, Lorg/apache/lucene/index/BufferedDeletesStream;->nextGen:J

    const-wide/16 v26, 0x1

    add-long v26, v26, v12

    move-wide/from16 v0, v26

    move-object/from16 v2, p0

    iput-wide v0, v2, Lorg/apache/lucene/index/BufferedDeletesStream;->nextGen:J

    .line 173
    .local v12, "gen":J
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 174
    .local v15, "infos2":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfoPerCommit;>;"
    move-object/from16 v0, p2

    invoke-interface {v15, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 175
    sget-object v21, Lorg/apache/lucene/index/BufferedDeletesStream;->sortSegInfoByDelGen:Ljava/util/Comparator;

    move-object/from16 v0, v21

    invoke-static {v15, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 177
    const/4 v8, 0x0

    .line 178
    .local v8, "coalescedDeletes":Lorg/apache/lucene/index/CoalescedDeletes;
    const/4 v7, 0x0

    .line 180
    .local v7, "anyNewDeletes":Z
    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v21

    add-int/lit8 v16, v21, -0x1

    .line 181
    .local v16, "infosIDX":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/BufferedDeletesStream;->deletes:Ljava/util/List;

    move-object/from16 v21, v0

    invoke-interface/range {v21 .. v21}, Ljava/util/List;->size()I

    move-result v21

    add-int/lit8 v10, v21, -0x1

    .line 183
    .local v10, "delIDX":I
    const/4 v6, 0x0

    .line 185
    .local v6, "allDeleted":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfoPerCommit;>;"
    :goto_1
    if-gez v16, :cond_5

    .line 301
    sget-boolean v21, Lorg/apache/lucene/index/BufferedDeletesStream;->$assertionsDisabled:Z

    if-nez v21, :cond_20

    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/index/BufferedDeletesStream;->checkDeleteStats()Z

    move-result v21

    if-nez v21, :cond_20

    new-instance v21, Ljava/lang/AssertionError;

    invoke-direct/range {v21 .. v21}, Ljava/lang/AssertionError;-><init>()V

    throw v21

    .line 188
    :cond_5
    if-ltz v10, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/BufferedDeletesStream;->deletes:Ljava/util/List;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lorg/apache/lucene/index/FrozenBufferedDeletes;

    move-object/from16 v17, v21

    .line 189
    .local v17, "packet":Lorg/apache/lucene/index/FrozenBufferedDeletes;
    :goto_2
    invoke-interface/range {v15 .. v16}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .line 190
    .local v14, "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    invoke-virtual {v14}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->getBufferedDeletesGen()J

    move-result-wide v22

    .line 192
    .local v22, "segGen":J
    if-eqz v17, :cond_9

    invoke-virtual/range {v17 .. v17}, Lorg/apache/lucene/index/FrozenBufferedDeletes;->delGen()J

    move-result-wide v26

    cmp-long v21, v22, v26

    if-gez v21, :cond_9

    .line 194
    if-nez v8, :cond_6

    .line 195
    new-instance v8, Lorg/apache/lucene/index/CoalescedDeletes;

    .end local v8    # "coalescedDeletes":Lorg/apache/lucene/index/CoalescedDeletes;
    invoke-direct {v8}, Lorg/apache/lucene/index/CoalescedDeletes;-><init>()V

    .line 197
    .restart local v8    # "coalescedDeletes":Lorg/apache/lucene/index/CoalescedDeletes;
    :cond_6
    move-object/from16 v0, v17

    iget-boolean v0, v0, Lorg/apache/lucene/index/FrozenBufferedDeletes;->isSegmentPrivate:Z

    move/from16 v21, v0

    if-nez v21, :cond_7

    .line 205
    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Lorg/apache/lucene/index/CoalescedDeletes;->update(Lorg/apache/lucene/index/FrozenBufferedDeletes;)V

    .line 208
    :cond_7
    add-int/lit8 v10, v10, -0x1

    .line 209
    goto :goto_1

    .line 188
    .end local v14    # "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .end local v17    # "packet":Lorg/apache/lucene/index/FrozenBufferedDeletes;
    .end local v22    # "segGen":J
    :cond_8
    const/16 v17, 0x0

    goto :goto_2

    .line 209
    .restart local v14    # "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .restart local v17    # "packet":Lorg/apache/lucene/index/FrozenBufferedDeletes;
    .restart local v22    # "segGen":J
    :cond_9
    if-eqz v17, :cond_16

    invoke-virtual/range {v17 .. v17}, Lorg/apache/lucene/index/FrozenBufferedDeletes;->delGen()J

    move-result-wide v26

    cmp-long v21, v22, v26

    if-nez v21, :cond_16

    .line 210
    sget-boolean v21, Lorg/apache/lucene/index/BufferedDeletesStream;->$assertionsDisabled:Z

    if-nez v21, :cond_a

    move-object/from16 v0, v17

    iget-boolean v0, v0, Lorg/apache/lucene/index/FrozenBufferedDeletes;->isSegmentPrivate:Z

    move/from16 v21, v0

    if-nez v21, :cond_a

    new-instance v21, Ljava/lang/AssertionError;

    new-instance v26, Ljava/lang/StringBuilder;

    const-string v27, "Packet and Segments deletegen can only match on a segment private del packet gen="

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v26

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v21

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v21

    .line 214
    :cond_a
    sget-boolean v21, Lorg/apache/lucene/index/BufferedDeletesStream;->$assertionsDisabled:Z

    if-nez v21, :cond_b

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->infoIsLive(Lorg/apache/lucene/index/SegmentInfoPerCommit;)Z

    move-result v21

    if-nez v21, :cond_b

    new-instance v21, Ljava/lang/AssertionError;

    invoke-direct/range {v21 .. v21}, Ljava/lang/AssertionError;-><init>()V

    throw v21

    .line 215
    :cond_b
    const/16 v21, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v14, v1}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->get(Lorg/apache/lucene/index/SegmentInfoPerCommit;Z)Lorg/apache/lucene/index/ReadersAndLiveDocs;

    move-result-object v19

    .line 216
    .local v19, "rld":Lorg/apache/lucene/index/ReadersAndLiveDocs;
    sget-object v21, Lorg/apache/lucene/store/IOContext;->READ:Lorg/apache/lucene/store/IOContext;

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/ReadersAndLiveDocs;->getReader(Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/index/SegmentReader;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v18

    .line 217
    .local v18, "reader":Lorg/apache/lucene/index/SegmentReader;
    const/4 v9, 0x0

    .line 220
    .local v9, "delCount":I
    if-eqz v8, :cond_c

    .line 222
    int-to-long v0, v9

    move-wide/from16 v26, v0

    :try_start_3
    invoke-virtual {v8}, Lorg/apache/lucene/index/CoalescedDeletes;->termsIterable()Ljava/lang/Iterable;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v19

    move-object/from16 v3, v18

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/index/BufferedDeletesStream;->applyTermDeletes(Ljava/lang/Iterable;Lorg/apache/lucene/index/ReadersAndLiveDocs;Lorg/apache/lucene/index/SegmentReader;)J

    move-result-wide v28

    add-long v26, v26, v28

    move-wide/from16 v0, v26

    long-to-int v9, v0

    .line 223
    int-to-long v0, v9

    move-wide/from16 v26, v0

    invoke-virtual {v8}, Lorg/apache/lucene/index/CoalescedDeletes;->queriesIterable()Ljava/lang/Iterable;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v19

    move-object/from16 v2, v18

    invoke-static {v0, v1, v2}, Lorg/apache/lucene/index/BufferedDeletesStream;->applyQueryDeletes(Ljava/lang/Iterable;Lorg/apache/lucene/index/ReadersAndLiveDocs;Lorg/apache/lucene/index/SegmentReader;)J

    move-result-wide v28

    add-long v26, v26, v28

    move-wide/from16 v0, v26

    long-to-int v9, v0

    .line 228
    :cond_c
    int-to-long v0, v9

    move-wide/from16 v26, v0

    invoke-virtual/range {v17 .. v17}, Lorg/apache/lucene/index/FrozenBufferedDeletes;->queriesIterable()Ljava/lang/Iterable;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v19

    move-object/from16 v2, v18

    invoke-static {v0, v1, v2}, Lorg/apache/lucene/index/BufferedDeletesStream;->applyQueryDeletes(Ljava/lang/Iterable;Lorg/apache/lucene/index/ReadersAndLiveDocs;Lorg/apache/lucene/index/SegmentReader;)J

    move-result-wide v28

    add-long v26, v26, v28

    move-wide/from16 v0, v26

    long-to-int v9, v0

    .line 229
    move-object/from16 v0, v19

    iget-object v0, v0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->getDelCount()I

    move-result v21

    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/index/ReadersAndLiveDocs;->getPendingDeleteCount()I

    move-result v26

    add-int v11, v21, v26

    .line 230
    .local v11, "fullDelCount":I
    sget-boolean v21, Lorg/apache/lucene/index/BufferedDeletesStream;->$assertionsDisabled:Z

    if-nez v21, :cond_d

    move-object/from16 v0, v19

    iget-object v0, v0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v21

    move/from16 v0, v21

    if-le v11, v0, :cond_d

    new-instance v21, Ljava/lang/AssertionError;

    invoke-direct/range {v21 .. v21}, Ljava/lang/AssertionError;-><init>()V

    throw v21
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 232
    .end local v11    # "fullDelCount":I
    :catchall_1
    move-exception v21

    .line 233
    :try_start_4
    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/ReadersAndLiveDocs;->release(Lorg/apache/lucene/index/SegmentReader;)V

    .line 234
    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->release(Lorg/apache/lucene/index/ReadersAndLiveDocs;)V

    .line 235
    throw v21
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 231
    .restart local v11    # "fullDelCount":I
    :cond_d
    :try_start_5
    move-object/from16 v0, v19

    iget-object v0, v0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result v21

    move/from16 v0, v21

    if-ne v11, v0, :cond_12

    const/16 v20, 0x1

    .line 233
    .local v20, "segAllDeletes":Z
    :goto_3
    :try_start_6
    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/ReadersAndLiveDocs;->release(Lorg/apache/lucene/index/SegmentReader;)V

    .line 234
    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->release(Lorg/apache/lucene/index/ReadersAndLiveDocs;)V

    .line 236
    if-lez v9, :cond_13

    const/16 v21, 0x1

    :goto_4
    or-int v7, v7, v21

    .line 238
    if-eqz v20, :cond_f

    .line 239
    if-nez v6, :cond_e

    .line 240
    new-instance v6, Ljava/util/ArrayList;

    .end local v6    # "allDeleted":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfoPerCommit;>;"
    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 242
    .restart local v6    # "allDeleted":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfoPerCommit;>;"
    :cond_e
    invoke-interface {v6, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 245
    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/BufferedDeletesStream;->infoStream:Lorg/apache/lucene/util/InfoStream;

    move-object/from16 v21, v0

    const-string v26, "BD"

    move-object/from16 v0, v21

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_10

    .line 246
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/BufferedDeletesStream;->infoStream:Lorg/apache/lucene/util/InfoStream;

    move-object/from16 v26, v0

    const-string v27, "BD"

    new-instance v21, Ljava/lang/StringBuilder;

    const-string v28, "seg="

    move-object/from16 v0, v21

    move-object/from16 v1, v28

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v28, " segGen="

    move-object/from16 v0, v21

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v23}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v28, " segDeletes=["

    move-object/from16 v0, v21

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v28, "]; coalesced deletes=["

    move-object/from16 v0, v21

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    if-nez v8, :cond_14

    const-string v21, "null"

    :goto_5
    move-object/from16 v0, v28

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v28, "] newDelCount="

    move-object/from16 v0, v21

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v28

    if-eqz v20, :cond_15

    const-string v21, " 100% deleted"

    :goto_6
    move-object/from16 v0, v28

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    :cond_10
    if-nez v8, :cond_11

    .line 250
    new-instance v8, Lorg/apache/lucene/index/CoalescedDeletes;

    .end local v8    # "coalescedDeletes":Lorg/apache/lucene/index/CoalescedDeletes;
    invoke-direct {v8}, Lorg/apache/lucene/index/CoalescedDeletes;-><init>()V

    .line 258
    .restart local v8    # "coalescedDeletes":Lorg/apache/lucene/index/CoalescedDeletes;
    :cond_11
    add-int/lit8 v10, v10, -0x1

    .line 259
    add-int/lit8 v16, v16, -0x1

    .line 260
    invoke-virtual {v14, v12, v13}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->setBufferedDeletesGen(J)V

    goto/16 :goto_1

    .line 231
    .end local v20    # "segAllDeletes":Z
    :cond_12
    const/16 v20, 0x0

    goto/16 :goto_3

    .line 236
    .restart local v20    # "segAllDeletes":Z
    :cond_13
    const/16 v21, 0x0

    goto/16 :goto_4

    :cond_14
    move-object/from16 v21, v8

    .line 246
    goto :goto_5

    :cond_15
    const-string v21, ""

    goto :goto_6

    .line 265
    .end local v9    # "delCount":I
    .end local v11    # "fullDelCount":I
    .end local v18    # "reader":Lorg/apache/lucene/index/SegmentReader;
    .end local v19    # "rld":Lorg/apache/lucene/index/ReadersAndLiveDocs;
    .end local v20    # "segAllDeletes":Z
    :cond_16
    if-eqz v8, :cond_1b

    .line 267
    sget-boolean v21, Lorg/apache/lucene/index/BufferedDeletesStream;->$assertionsDisabled:Z

    if-nez v21, :cond_17

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->infoIsLive(Lorg/apache/lucene/index/SegmentInfoPerCommit;)Z

    move-result v21

    if-nez v21, :cond_17

    new-instance v21, Ljava/lang/AssertionError;

    invoke-direct/range {v21 .. v21}, Ljava/lang/AssertionError;-><init>()V

    throw v21

    .line 268
    :cond_17
    const/16 v21, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v14, v1}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->get(Lorg/apache/lucene/index/SegmentInfoPerCommit;Z)Lorg/apache/lucene/index/ReadersAndLiveDocs;

    move-result-object v19

    .line 269
    .restart local v19    # "rld":Lorg/apache/lucene/index/ReadersAndLiveDocs;
    sget-object v21, Lorg/apache/lucene/store/IOContext;->READ:Lorg/apache/lucene/store/IOContext;

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/ReadersAndLiveDocs;->getReader(Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/index/SegmentReader;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result-object v18

    .line 270
    .restart local v18    # "reader":Lorg/apache/lucene/index/SegmentReader;
    const/4 v9, 0x0

    .line 273
    .restart local v9    # "delCount":I
    int-to-long v0, v9

    move-wide/from16 v26, v0

    :try_start_7
    invoke-virtual {v8}, Lorg/apache/lucene/index/CoalescedDeletes;->termsIterable()Ljava/lang/Iterable;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v19

    move-object/from16 v3, v18

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/index/BufferedDeletesStream;->applyTermDeletes(Ljava/lang/Iterable;Lorg/apache/lucene/index/ReadersAndLiveDocs;Lorg/apache/lucene/index/SegmentReader;)J

    move-result-wide v28

    add-long v26, v26, v28

    move-wide/from16 v0, v26

    long-to-int v9, v0

    .line 274
    int-to-long v0, v9

    move-wide/from16 v26, v0

    invoke-virtual {v8}, Lorg/apache/lucene/index/CoalescedDeletes;->queriesIterable()Ljava/lang/Iterable;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v19

    move-object/from16 v2, v18

    invoke-static {v0, v1, v2}, Lorg/apache/lucene/index/BufferedDeletesStream;->applyQueryDeletes(Ljava/lang/Iterable;Lorg/apache/lucene/index/ReadersAndLiveDocs;Lorg/apache/lucene/index/SegmentReader;)J

    move-result-wide v28

    add-long v26, v26, v28

    move-wide/from16 v0, v26

    long-to-int v9, v0

    .line 275
    move-object/from16 v0, v19

    iget-object v0, v0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->getDelCount()I

    move-result v21

    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/index/ReadersAndLiveDocs;->getPendingDeleteCount()I

    move-result v26

    add-int v11, v21, v26

    .line 276
    .restart local v11    # "fullDelCount":I
    sget-boolean v21, Lorg/apache/lucene/index/BufferedDeletesStream;->$assertionsDisabled:Z

    if-nez v21, :cond_18

    move-object/from16 v0, v19

    iget-object v0, v0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v21

    move/from16 v0, v21

    if-le v11, v0, :cond_18

    new-instance v21, Ljava/lang/AssertionError;

    invoke-direct/range {v21 .. v21}, Ljava/lang/AssertionError;-><init>()V

    throw v21
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 278
    .end local v11    # "fullDelCount":I
    :catchall_2
    move-exception v21

    .line 279
    :try_start_8
    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/ReadersAndLiveDocs;->release(Lorg/apache/lucene/index/SegmentReader;)V

    .line 280
    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->release(Lorg/apache/lucene/index/ReadersAndLiveDocs;)V

    .line 281
    throw v21
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 277
    .restart local v11    # "fullDelCount":I
    :cond_18
    :try_start_9
    move-object/from16 v0, v19

    iget-object v0, v0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    move-result v21

    move/from16 v0, v21

    if-ne v11, v0, :cond_1c

    const/16 v20, 0x1

    .line 279
    .restart local v20    # "segAllDeletes":Z
    :goto_7
    :try_start_a
    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/ReadersAndLiveDocs;->release(Lorg/apache/lucene/index/SegmentReader;)V

    .line 280
    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->release(Lorg/apache/lucene/index/ReadersAndLiveDocs;)V

    .line 282
    if-lez v9, :cond_1d

    const/16 v21, 0x1

    :goto_8
    or-int v7, v7, v21

    .line 284
    if-eqz v20, :cond_1a

    .line 285
    if-nez v6, :cond_19

    .line 286
    new-instance v6, Ljava/util/ArrayList;

    .end local v6    # "allDeleted":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfoPerCommit;>;"
    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 288
    .restart local v6    # "allDeleted":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfoPerCommit;>;"
    :cond_19
    invoke-interface {v6, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 291
    :cond_1a
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/BufferedDeletesStream;->infoStream:Lorg/apache/lucene/util/InfoStream;

    move-object/from16 v21, v0

    const-string v26, "BD"

    move-object/from16 v0, v21

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_1b

    .line 292
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/BufferedDeletesStream;->infoStream:Lorg/apache/lucene/util/InfoStream;

    move-object/from16 v26, v0

    const-string v27, "BD"

    new-instance v21, Ljava/lang/StringBuilder;

    const-string v28, "seg="

    move-object/from16 v0, v21

    move-object/from16 v1, v28

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v28, " segGen="

    move-object/from16 v0, v21

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v23}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v28, " coalesced deletes=["

    move-object/from16 v0, v21

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    if-nez v8, :cond_1e

    const-string v21, "null"

    :goto_9
    move-object/from16 v0, v28

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v28, "] newDelCount="

    move-object/from16 v0, v21

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v28

    if-eqz v20, :cond_1f

    const-string v21, " 100% deleted"

    :goto_a
    move-object/from16 v0, v28

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    .end local v9    # "delCount":I
    .end local v11    # "fullDelCount":I
    .end local v18    # "reader":Lorg/apache/lucene/index/SegmentReader;
    .end local v19    # "rld":Lorg/apache/lucene/index/ReadersAndLiveDocs;
    .end local v20    # "segAllDeletes":Z
    :cond_1b
    invoke-virtual {v14, v12, v13}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->setBufferedDeletesGen(J)V

    .line 297
    add-int/lit8 v16, v16, -0x1

    goto/16 :goto_1

    .line 277
    .restart local v9    # "delCount":I
    .restart local v11    # "fullDelCount":I
    .restart local v18    # "reader":Lorg/apache/lucene/index/SegmentReader;
    .restart local v19    # "rld":Lorg/apache/lucene/index/ReadersAndLiveDocs;
    :cond_1c
    const/16 v20, 0x0

    goto/16 :goto_7

    .line 282
    .restart local v20    # "segAllDeletes":Z
    :cond_1d
    const/16 v21, 0x0

    goto/16 :goto_8

    :cond_1e
    move-object/from16 v21, v8

    .line 292
    goto :goto_9

    :cond_1f
    const-string v21, ""

    goto :goto_a

    .line 302
    .end local v9    # "delCount":I
    .end local v11    # "fullDelCount":I
    .end local v14    # "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .end local v17    # "packet":Lorg/apache/lucene/index/FrozenBufferedDeletes;
    .end local v18    # "reader":Lorg/apache/lucene/index/SegmentReader;
    .end local v19    # "rld":Lorg/apache/lucene/index/ReadersAndLiveDocs;
    .end local v20    # "segAllDeletes":Z
    .end local v22    # "segGen":J
    :cond_20
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/BufferedDeletesStream;->infoStream:Lorg/apache/lucene/util/InfoStream;

    move-object/from16 v21, v0

    const-string v26, "BD"

    move-object/from16 v0, v21

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_21

    .line 303
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/BufferedDeletesStream;->infoStream:Lorg/apache/lucene/util/InfoStream;

    move-object/from16 v21, v0

    const-string v26, "BD"

    new-instance v27, Ljava/lang/StringBuilder;

    const-string v28, "applyDeletes took "

    invoke-direct/range {v27 .. v28}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v28

    sub-long v28, v28, v24

    invoke-virtual/range {v27 .. v29}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, " msec"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v21

    move-object/from16 v1, v26

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    :cond_21
    new-instance v21, Lorg/apache/lucene/index/BufferedDeletesStream$ApplyDeletesResult;

    move-object/from16 v0, v21

    invoke-direct {v0, v7, v12, v13, v6}, Lorg/apache/lucene/index/BufferedDeletesStream$ApplyDeletesResult;-><init>(ZJLjava/util/List;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto/16 :goto_0
.end method

.method public bytesUsed()J
    .locals 2

    .prologue
    .line 113
    iget-object v0, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    return-wide v0
.end method

.method public declared-synchronized clear()V
    .locals 4

    .prologue
    .line 98
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->deletes:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 99
    const-wide/16 v0, 0x1

    iput-wide v0, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->nextGen:J

    .line 100
    iget-object v0, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->numTerms:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 101
    iget-object v0, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 102
    monitor-exit p0

    return-void

    .line 98
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized getNextGen()J
    .locals 4

    .prologue
    .line 311
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->nextGen:J

    const-wide/16 v2, 0x1

    add-long/2addr v2, v0

    iput-wide v2, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->nextGen:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public numTerms()I
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->numTerms:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    return v0
.end method

.method public declared-synchronized prune(Lorg/apache/lucene/index/SegmentInfos;)V
    .locals 9
    .param p1, "segmentInfos"    # Lorg/apache/lucene/index/SegmentInfos;

    .prologue
    .line 319
    monitor-enter p0

    :try_start_0
    sget-boolean v3, Lorg/apache/lucene/index/BufferedDeletesStream;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/index/BufferedDeletesStream;->checkDeleteStats()Z

    move-result v3

    if-nez v3, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 320
    :cond_0
    const-wide v4, 0x7fffffffffffffffL

    .line 321
    .local v4, "minGen":J
    :try_start_1
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfos;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_2

    .line 325
    iget-object v3, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v6, "BD"

    invoke-virtual {v3, v6}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 326
    iget-object v3, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v6, "BD"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "prune sis="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " minGen="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " packetCount="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->deletes:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->deletes:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    .line 329
    .local v2, "limit":I
    const/4 v0, 0x0

    .local v0, "delIDX":I
    :goto_1
    if-lt v0, v2, :cond_3

    .line 338
    invoke-direct {p0, v2}, Lorg/apache/lucene/index/BufferedDeletesStream;->prune(I)V

    .line 339
    sget-boolean v3, Lorg/apache/lucene/index/BufferedDeletesStream;->$assertionsDisabled:Z

    if-nez v3, :cond_5

    invoke-virtual {p0}, Lorg/apache/lucene/index/BufferedDeletesStream;->any()Z

    move-result v3

    if-eqz v3, :cond_5

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 321
    .end local v0    # "delIDX":I
    .end local v2    # "limit":I
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .line 322
    .local v1, "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->getBufferedDeletesGen()J

    move-result-wide v6

    invoke-static {v6, v7, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    goto :goto_0

    .line 330
    .end local v1    # "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .restart local v0    # "delIDX":I
    .restart local v2    # "limit":I
    :cond_3
    iget-object v3, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->deletes:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/index/FrozenBufferedDeletes;

    invoke-virtual {v3}, Lorg/apache/lucene/index/FrozenBufferedDeletes;->delGen()J

    move-result-wide v6

    cmp-long v3, v6, v4

    if-ltz v3, :cond_4

    .line 331
    invoke-direct {p0, v0}, Lorg/apache/lucene/index/BufferedDeletesStream;->prune(I)V

    .line 332
    sget-boolean v3, Lorg/apache/lucene/index/BufferedDeletesStream;->$assertionsDisabled:Z

    if-nez v3, :cond_6

    invoke-direct {p0}, Lorg/apache/lucene/index/BufferedDeletesStream;->checkDeleteStats()Z

    move-result v3

    if-nez v3, :cond_6

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 329
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 340
    :cond_5
    sget-boolean v3, Lorg/apache/lucene/index/BufferedDeletesStream;->$assertionsDisabled:Z

    if-nez v3, :cond_6

    invoke-direct {p0}, Lorg/apache/lucene/index/BufferedDeletesStream;->checkDeleteStats()Z

    move-result v3

    if-nez v3, :cond_6

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 341
    :cond_6
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized push(Lorg/apache/lucene/index/FrozenBufferedDeletes;)J
    .locals 6
    .param p1, "packet"    # Lorg/apache/lucene/index/FrozenBufferedDeletes;

    .prologue
    .line 82
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->nextGen:J

    const-wide/16 v2, 0x1

    add-long/2addr v2, v0

    iput-wide v2, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->nextGen:J

    invoke-virtual {p1, v0, v1}, Lorg/apache/lucene/index/FrozenBufferedDeletes;->setDelGen(J)V

    .line 83
    sget-boolean v0, Lorg/apache/lucene/index/BufferedDeletesStream;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lorg/apache/lucene/index/FrozenBufferedDeletes;->any()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 82
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 84
    :cond_0
    :try_start_1
    sget-boolean v0, Lorg/apache/lucene/index/BufferedDeletesStream;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    invoke-direct {p0}, Lorg/apache/lucene/index/BufferedDeletesStream;->checkDeleteStats()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 85
    :cond_1
    sget-boolean v0, Lorg/apache/lucene/index/BufferedDeletesStream;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    invoke-virtual {p1}, Lorg/apache/lucene/index/FrozenBufferedDeletes;->delGen()J

    move-result-wide v0

    iget-wide v2, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->nextGen:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 86
    :cond_2
    sget-boolean v0, Lorg/apache/lucene/index/BufferedDeletesStream;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->deletes:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->deletes:Ljava/util/List;

    iget-object v1, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->deletes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/FrozenBufferedDeletes;

    invoke-virtual {v0}, Lorg/apache/lucene/index/FrozenBufferedDeletes;->delGen()J

    move-result-wide v0

    invoke-virtual {p1}, Lorg/apache/lucene/index/FrozenBufferedDeletes;->delGen()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-ltz v0, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Delete packets must be in order"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 87
    :cond_3
    iget-object v0, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->deletes:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 88
    iget-object v0, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->numTerms:Ljava/util/concurrent/atomic/AtomicInteger;

    iget v1, p1, Lorg/apache/lucene/index/FrozenBufferedDeletes;->numTermDeletes:I

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->addAndGet(I)I

    .line 89
    iget-object v0, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

    iget v1, p1, Lorg/apache/lucene/index/FrozenBufferedDeletes;->bytesUsed:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    .line 90
    iget-object v0, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v1, "BD"

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 91
    iget-object v0, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v1, "BD"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "push deletes "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " delGen="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lorg/apache/lucene/index/FrozenBufferedDeletes;->delGen()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " packetCount="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->deletes:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " totBytesUsed="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    :cond_4
    sget-boolean v0, Lorg/apache/lucene/index/BufferedDeletesStream;->$assertionsDisabled:Z

    if-nez v0, :cond_5

    invoke-direct {p0}, Lorg/apache/lucene/index/BufferedDeletesStream;->checkDeleteStats()Z

    move-result v0

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 94
    :cond_5
    invoke-virtual {p1}, Lorg/apache/lucene/index/FrozenBufferedDeletes;->delGen()J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v0

    monitor-exit p0

    return-wide v0
.end method
