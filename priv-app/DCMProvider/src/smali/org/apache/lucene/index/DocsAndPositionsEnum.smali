.class public abstract Lorg/apache/lucene/index/DocsAndPositionsEnum;
.super Lorg/apache/lucene/index/DocsEnum;
.source "DocsAndPositionsEnum.java"


# static fields
.field public static final FLAG_OFFSETS:I = 0x1

.field public static final FLAG_PAYLOADS:I = 0x2


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lorg/apache/lucene/index/DocsEnum;-><init>()V

    .line 39
    return-void
.end method


# virtual methods
.method public abstract endOffset()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getPayload()Lorg/apache/lucene/util/BytesRef;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract nextPosition()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract startOffset()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
