.class Lorg/apache/lucene/index/MergedIterator$TermMergeQueue;
.super Lorg/apache/lucene/util/PriorityQueue;
.source "MergedIterator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/MergedIterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TermMergeQueue"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<C::",
        "Ljava/lang/Comparable",
        "<TC;>;>",
        "Lorg/apache/lucene/util/PriorityQueue",
        "<",
        "Lorg/apache/lucene/index/MergedIterator$SubIterator",
        "<TC;>;>;"
    }
.end annotation


# direct methods
.method constructor <init>(I)V
    .locals 0
    .param p1, "size"    # I

    .prologue
    .line 137
    .local p0, "this":Lorg/apache/lucene/index/MergedIterator$TermMergeQueue;, "Lorg/apache/lucene/index/MergedIterator<TT;>.TermMergeQueue<TC;>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/util/PriorityQueue;-><init>(I)V

    .line 138
    return-void
.end method


# virtual methods
.method protected bridge synthetic lessThan(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/index/MergedIterator$SubIterator;

    check-cast p2, Lorg/apache/lucene/index/MergedIterator$SubIterator;

    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/index/MergedIterator$TermMergeQueue;->lessThan(Lorg/apache/lucene/index/MergedIterator$SubIterator;Lorg/apache/lucene/index/MergedIterator$SubIterator;)Z

    move-result v0

    return v0
.end method

.method protected lessThan(Lorg/apache/lucene/index/MergedIterator$SubIterator;Lorg/apache/lucene/index/MergedIterator$SubIterator;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/MergedIterator$SubIterator",
            "<TC;>;",
            "Lorg/apache/lucene/index/MergedIterator$SubIterator",
            "<TC;>;)Z"
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/index/MergedIterator$TermMergeQueue;, "Lorg/apache/lucene/index/MergedIterator<TT;>.TermMergeQueue<TC;>;"
    .local p1, "a":Lorg/apache/lucene/index/MergedIterator$SubIterator;, "Lorg/apache/lucene/index/MergedIterator$SubIterator<TC;>;"
    .local p2, "b":Lorg/apache/lucene/index/MergedIterator$SubIterator;, "Lorg/apache/lucene/index/MergedIterator$SubIterator<TC;>;"
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 142
    iget-object v3, p1, Lorg/apache/lucene/index/MergedIterator$SubIterator;->current:Ljava/lang/Comparable;

    iget-object v4, p2, Lorg/apache/lucene/index/MergedIterator$SubIterator;->current:Ljava/lang/Comparable;

    invoke-interface {v3, v4}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v0

    .line 143
    .local v0, "cmp":I
    if-eqz v0, :cond_2

    .line 144
    if-gez v0, :cond_1

    .line 146
    :cond_0
    :goto_0
    return v1

    :cond_1
    move v1, v2

    .line 144
    goto :goto_0

    .line 146
    :cond_2
    iget v3, p1, Lorg/apache/lucene/index/MergedIterator$SubIterator;->index:I

    iget v4, p2, Lorg/apache/lucene/index/MergedIterator$SubIterator;->index:I

    if-lt v3, v4, :cond_0

    move v1, v2

    goto :goto_0
.end method
