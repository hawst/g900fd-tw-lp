.class final Lorg/apache/lucene/index/SegmentCoreReaders;
.super Ljava/lang/Object;
.source "SegmentCoreReaders.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final cfsReader:Lorg/apache/lucene/store/CompoundFileDirectory;

.field private final coreClosedListeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lorg/apache/lucene/index/SegmentReader$CoreClosedListener;",
            ">;"
        }
    .end annotation
.end field

.field final docValuesLocal:Lorg/apache/lucene/util/CloseableThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/CloseableThreadLocal",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field final dvProducer:Lorg/apache/lucene/codecs/DocValuesProducer;

.field final fieldInfos:Lorg/apache/lucene/index/FieldInfos;

.field final fields:Lorg/apache/lucene/codecs/FieldsProducer;

.field final fieldsReaderLocal:Lorg/apache/lucene/util/CloseableThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/CloseableThreadLocal",
            "<",
            "Lorg/apache/lucene/codecs/StoredFieldsReader;",
            ">;"
        }
    .end annotation
.end field

.field final fieldsReaderOrig:Lorg/apache/lucene/codecs/StoredFieldsReader;

.field final normsLocal:Lorg/apache/lucene/util/CloseableThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/CloseableThreadLocal",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field final normsProducer:Lorg/apache/lucene/codecs/DocValuesProducer;

.field private final owner:Lorg/apache/lucene/index/SegmentReader;

.field private final ref:Ljava/util/concurrent/atomic/AtomicInteger;

.field final termVectorsLocal:Lorg/apache/lucene/util/CloseableThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/CloseableThreadLocal",
            "<",
            "Lorg/apache/lucene/codecs/TermVectorsReader;",
            ">;"
        }
    .end annotation
.end field

.field final termVectorsReaderOrig:Lorg/apache/lucene/codecs/TermVectorsReader;

.field final termsIndexDivisor:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-class v0, Lorg/apache/lucene/index/SegmentCoreReaders;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/SegmentCoreReaders;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/index/SegmentReader;Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfoPerCommit;Lorg/apache/lucene/store/IOContext;I)V
    .locals 9
    .param p1, "owner"    # Lorg/apache/lucene/index/SegmentReader;
    .param p2, "dir"    # Lorg/apache/lucene/store/Directory;
    .param p3, "si"    # Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .param p4, "context"    # Lorg/apache/lucene/store/IOContext;
    .param p5, "termsIndexDivisor"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    new-instance v2, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v2, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->ref:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 72
    new-instance v2, Lorg/apache/lucene/index/SegmentCoreReaders$1;

    invoke-direct {v2, p0}, Lorg/apache/lucene/index/SegmentCoreReaders$1;-><init>(Lorg/apache/lucene/index/SegmentCoreReaders;)V

    iput-object v2, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->fieldsReaderLocal:Lorg/apache/lucene/util/CloseableThreadLocal;

    .line 79
    new-instance v2, Lorg/apache/lucene/index/SegmentCoreReaders$2;

    invoke-direct {v2, p0}, Lorg/apache/lucene/index/SegmentCoreReaders$2;-><init>(Lorg/apache/lucene/index/SegmentCoreReaders;)V

    iput-object v2, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->termVectorsLocal:Lorg/apache/lucene/util/CloseableThreadLocal;

    .line 86
    new-instance v2, Lorg/apache/lucene/index/SegmentCoreReaders$3;

    invoke-direct {v2, p0}, Lorg/apache/lucene/index/SegmentCoreReaders$3;-><init>(Lorg/apache/lucene/index/SegmentCoreReaders;)V

    iput-object v2, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->docValuesLocal:Lorg/apache/lucene/util/CloseableThreadLocal;

    .line 93
    new-instance v2, Lorg/apache/lucene/index/SegmentCoreReaders$4;

    invoke-direct {v2, p0}, Lorg/apache/lucene/index/SegmentCoreReaders$4;-><init>(Lorg/apache/lucene/index/SegmentCoreReaders;)V

    iput-object v2, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->normsLocal:Lorg/apache/lucene/util/CloseableThreadLocal;

    .line 101
    new-instance v2, Ljava/util/LinkedHashSet;

    invoke-direct {v2}, Ljava/util/LinkedHashSet;-><init>()V

    invoke-static {v2}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->coreClosedListeners:Ljava/util/Set;

    .line 105
    if-nez p5, :cond_0

    .line 106
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "indexDivisor must be < 0 (don\'t load terms index) or greater than 0 (got 0)"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 109
    :cond_0
    iget-object v2, p3, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v2}, Lorg/apache/lucene/index/SegmentInfo;->getCodec()Lorg/apache/lucene/codecs/Codec;

    move-result-object v6

    .line 112
    .local v6, "codec":Lorg/apache/lucene/codecs/Codec;
    const/4 v8, 0x0

    .line 115
    .local v8, "success":Z
    :try_start_0
    iget-object v2, p3, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v2}, Lorg/apache/lucene/index/SegmentInfo;->getUseCompoundFile()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 116
    new-instance v1, Lorg/apache/lucene/store/CompoundFileDirectory;

    iget-object v2, p3, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v2, v2, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    const-string v3, ""

    const-string v4, "cfs"

    invoke-static {v2, v3, v4}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, p2, v2, p4, v3}, Lorg/apache/lucene/store/CompoundFileDirectory;-><init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/store/IOContext;Z)V

    iput-object v1, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->cfsReader:Lorg/apache/lucene/store/CompoundFileDirectory;

    .line 121
    .local v1, "cfsDir":Lorg/apache/lucene/store/Directory;
    :goto_0
    invoke-virtual {v6}, Lorg/apache/lucene/codecs/Codec;->fieldInfosFormat()Lorg/apache/lucene/codecs/FieldInfosFormat;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/lucene/codecs/FieldInfosFormat;->getFieldInfosReader()Lorg/apache/lucene/codecs/FieldInfosReader;

    move-result-object v2

    iget-object v3, p3, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v3, v3, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    sget-object v4, Lorg/apache/lucene/store/IOContext;->READONCE:Lorg/apache/lucene/store/IOContext;

    invoke-virtual {v2, v1, v3, v4}, Lorg/apache/lucene/codecs/FieldInfosReader;->read(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/index/FieldInfos;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    .line 123
    iput p5, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->termsIndexDivisor:I

    .line 124
    invoke-virtual {v6}, Lorg/apache/lucene/codecs/Codec;->postingsFormat()Lorg/apache/lucene/codecs/PostingsFormat;

    move-result-object v7

    .line 125
    .local v7, "format":Lorg/apache/lucene/codecs/PostingsFormat;
    new-instance v0, Lorg/apache/lucene/index/SegmentReadState;

    iget-object v2, p3, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v3, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    move-object v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/index/SegmentReadState;-><init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/store/IOContext;I)V

    .line 127
    .local v0, "segmentReadState":Lorg/apache/lucene/index/SegmentReadState;
    invoke-virtual {v7, v0}, Lorg/apache/lucene/codecs/PostingsFormat;->fieldsProducer(Lorg/apache/lucene/index/SegmentReadState;)Lorg/apache/lucene/codecs/FieldsProducer;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->fields:Lorg/apache/lucene/codecs/FieldsProducer;

    .line 128
    sget-boolean v2, Lorg/apache/lucene/index/SegmentCoreReaders;->$assertionsDisabled:Z

    if-nez v2, :cond_3

    iget-object v2, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->fields:Lorg/apache/lucene/codecs/FieldsProducer;

    if-nez v2, :cond_3

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 156
    .end local v0    # "segmentReadState":Lorg/apache/lucene/index/SegmentReadState;
    .end local v1    # "cfsDir":Lorg/apache/lucene/store/Directory;
    .end local v7    # "format":Lorg/apache/lucene/codecs/PostingsFormat;
    :catchall_0
    move-exception v2

    .line 157
    if-nez v8, :cond_1

    .line 158
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentCoreReaders;->decRef()V

    .line 160
    :cond_1
    throw v2

    .line 118
    :cond_2
    const/4 v2, 0x0

    :try_start_1
    iput-object v2, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->cfsReader:Lorg/apache/lucene/store/CompoundFileDirectory;

    .line 119
    move-object v1, p2

    .restart local v1    # "cfsDir":Lorg/apache/lucene/store/Directory;
    goto :goto_0

    .line 133
    .restart local v0    # "segmentReadState":Lorg/apache/lucene/index/SegmentReadState;
    .restart local v7    # "format":Lorg/apache/lucene/codecs/PostingsFormat;
    :cond_3
    iget-object v2, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v2}, Lorg/apache/lucene/index/FieldInfos;->hasDocValues()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 134
    invoke-virtual {v6}, Lorg/apache/lucene/codecs/Codec;->docValuesFormat()Lorg/apache/lucene/codecs/DocValuesFormat;

    move-result-object v2

    invoke-virtual {v2, v0}, Lorg/apache/lucene/codecs/DocValuesFormat;->fieldsProducer(Lorg/apache/lucene/index/SegmentReadState;)Lorg/apache/lucene/codecs/DocValuesProducer;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->dvProducer:Lorg/apache/lucene/codecs/DocValuesProducer;

    .line 135
    sget-boolean v2, Lorg/apache/lucene/index/SegmentCoreReaders;->$assertionsDisabled:Z

    if-nez v2, :cond_5

    iget-object v2, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->dvProducer:Lorg/apache/lucene/codecs/DocValuesProducer;

    if-nez v2, :cond_5

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 137
    :cond_4
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->dvProducer:Lorg/apache/lucene/codecs/DocValuesProducer;

    .line 140
    :cond_5
    iget-object v2, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v2}, Lorg/apache/lucene/index/FieldInfos;->hasNorms()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 141
    invoke-virtual {v6}, Lorg/apache/lucene/codecs/Codec;->normsFormat()Lorg/apache/lucene/codecs/NormsFormat;

    move-result-object v2

    invoke-virtual {v2, v0}, Lorg/apache/lucene/codecs/NormsFormat;->normsProducer(Lorg/apache/lucene/index/SegmentReadState;)Lorg/apache/lucene/codecs/DocValuesProducer;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->normsProducer:Lorg/apache/lucene/codecs/DocValuesProducer;

    .line 142
    sget-boolean v2, Lorg/apache/lucene/index/SegmentCoreReaders;->$assertionsDisabled:Z

    if-nez v2, :cond_7

    iget-object v2, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->normsProducer:Lorg/apache/lucene/codecs/DocValuesProducer;

    if-nez v2, :cond_7

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 144
    :cond_6
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->normsProducer:Lorg/apache/lucene/codecs/DocValuesProducer;

    .line 147
    :cond_7
    iget-object v2, p3, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v2}, Lorg/apache/lucene/index/SegmentInfo;->getCodec()Lorg/apache/lucene/codecs/Codec;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/lucene/codecs/Codec;->storedFieldsFormat()Lorg/apache/lucene/codecs/StoredFieldsFormat;

    move-result-object v2

    iget-object v3, p3, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v4, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v2, v1, v3, v4, p4}, Lorg/apache/lucene/codecs/StoredFieldsFormat;->fieldsReader(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/codecs/StoredFieldsReader;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->fieldsReaderOrig:Lorg/apache/lucene/codecs/StoredFieldsReader;

    .line 149
    iget-object v2, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v2}, Lorg/apache/lucene/index/FieldInfos;->hasVectors()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 150
    iget-object v2, p3, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v2}, Lorg/apache/lucene/index/SegmentInfo;->getCodec()Lorg/apache/lucene/codecs/Codec;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/lucene/codecs/Codec;->termVectorsFormat()Lorg/apache/lucene/codecs/TermVectorsFormat;

    move-result-object v2

    iget-object v3, p3, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v4, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v2, v1, v3, v4, p4}, Lorg/apache/lucene/codecs/TermVectorsFormat;->vectorsReader(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/codecs/TermVectorsReader;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->termVectorsReaderOrig:Lorg/apache/lucene/codecs/TermVectorsReader;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 155
    :goto_1
    const/4 v8, 0x1

    .line 157
    if-nez v8, :cond_8

    .line 158
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentCoreReaders;->decRef()V

    .line 166
    :cond_8
    iput-object p1, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->owner:Lorg/apache/lucene/index/SegmentReader;

    .line 167
    return-void

    .line 152
    :cond_9
    const/4 v2, 0x0

    :try_start_2
    iput-object v2, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->termVectorsReaderOrig:Lorg/apache/lucene/codecs/TermVectorsReader;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method private notifyCoreClosedListeners()V
    .locals 4

    .prologue
    .line 317
    iget-object v2, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->coreClosedListeners:Ljava/util/Set;

    monitor-enter v2

    .line 318
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->coreClosedListeners:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 317
    monitor-exit v2

    .line 322
    return-void

    .line 318
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/SegmentReader$CoreClosedListener;

    .line 319
    .local v0, "listener":Lorg/apache/lucene/index/SegmentReader$CoreClosedListener;
    iget-object v3, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->owner:Lorg/apache/lucene/index/SegmentReader;

    invoke-interface {v0, v3}, Lorg/apache/lucene/index/SegmentReader$CoreClosedListener;->onClose(Lorg/apache/lucene/index/SegmentReader;)V

    goto :goto_0

    .line 317
    .end local v0    # "listener":Lorg/apache/lucene/index/SegmentReader$CoreClosedListener;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method


# virtual methods
.method addCoreClosedListener(Lorg/apache/lucene/index/SegmentReader$CoreClosedListener;)V
    .locals 1
    .param p1, "listener"    # Lorg/apache/lucene/index/SegmentReader$CoreClosedListener;

    .prologue
    .line 325
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->coreClosedListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 326
    return-void
.end method

.method decRef()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 309
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->ref:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0xa

    new-array v0, v0, [Ljava/io/Closeable;

    const/4 v1, 0x0

    .line 310
    iget-object v2, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->termVectorsLocal:Lorg/apache/lucene/util/CloseableThreadLocal;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->fieldsReaderLocal:Lorg/apache/lucene/util/CloseableThreadLocal;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->docValuesLocal:Lorg/apache/lucene/util/CloseableThreadLocal;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->normsLocal:Lorg/apache/lucene/util/CloseableThreadLocal;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->fields:Lorg/apache/lucene/codecs/FieldsProducer;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->dvProducer:Lorg/apache/lucene/codecs/DocValuesProducer;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    .line 311
    iget-object v2, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->termVectorsReaderOrig:Lorg/apache/lucene/codecs/TermVectorsReader;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->fieldsReaderOrig:Lorg/apache/lucene/codecs/StoredFieldsReader;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->cfsReader:Lorg/apache/lucene/store/CompoundFileDirectory;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->normsProducer:Lorg/apache/lucene/codecs/DocValuesProducer;

    aput-object v2, v0, v1

    .line 310
    invoke-static {v0}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 312
    invoke-direct {p0}, Lorg/apache/lucene/index/SegmentCoreReaders;->notifyCoreClosedListeners()V

    .line 314
    :cond_0
    return-void
.end method

.method getBinaryDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/BinaryDocValues;
    .locals 5
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 202
    iget-object v3, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v3, p1}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(Ljava/lang/String;)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v2

    .line 203
    .local v2, "fi":Lorg/apache/lucene/index/FieldInfo;
    if-nez v2, :cond_1

    .line 226
    :cond_0
    :goto_0
    return-object v1

    .line 207
    :cond_1
    invoke-virtual {v2}, Lorg/apache/lucene/index/FieldInfo;->getDocValuesType()Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 211
    invoke-virtual {v2}, Lorg/apache/lucene/index/FieldInfo;->getDocValuesType()Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-result-object v3

    sget-object v4, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->BINARY:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    if-ne v3, v4, :cond_0

    .line 216
    sget-boolean v3, Lorg/apache/lucene/index/SegmentCoreReaders;->$assertionsDisabled:Z

    if-nez v3, :cond_2

    iget-object v3, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->dvProducer:Lorg/apache/lucene/codecs/DocValuesProducer;

    if-nez v3, :cond_2

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 218
    :cond_2
    iget-object v3, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->docValuesLocal:Lorg/apache/lucene/util/CloseableThreadLocal;

    invoke-virtual {v3}, Lorg/apache/lucene/util/CloseableThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 220
    .local v0, "dvFields":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/BinaryDocValues;

    .line 221
    .local v1, "dvs":Lorg/apache/lucene/index/BinaryDocValues;
    if-nez v1, :cond_0

    .line 222
    iget-object v3, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->dvProducer:Lorg/apache/lucene/codecs/DocValuesProducer;

    invoke-virtual {v3, v2}, Lorg/apache/lucene/codecs/DocValuesProducer;->getBinary(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/BinaryDocValues;

    move-result-object v1

    .line 223
    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method getNormValues(Ljava/lang/String;)Lorg/apache/lucene/index/NumericDocValues;
    .locals 4
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 286
    iget-object v3, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v3, p1}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(Ljava/lang/String;)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v0

    .line 287
    .local v0, "fi":Lorg/apache/lucene/index/FieldInfo;
    if-nez v0, :cond_1

    .line 305
    :cond_0
    :goto_0
    return-object v2

    .line 291
    :cond_1
    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfo;->hasNorms()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 295
    sget-boolean v3, Lorg/apache/lucene/index/SegmentCoreReaders;->$assertionsDisabled:Z

    if-nez v3, :cond_2

    iget-object v3, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->normsProducer:Lorg/apache/lucene/codecs/DocValuesProducer;

    if-nez v3, :cond_2

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 297
    :cond_2
    iget-object v3, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->normsLocal:Lorg/apache/lucene/util/CloseableThreadLocal;

    invoke-virtual {v3}, Lorg/apache/lucene/util/CloseableThreadLocal;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    .line 299
    .local v1, "normFields":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/index/NumericDocValues;

    .line 300
    .local v2, "norms":Lorg/apache/lucene/index/NumericDocValues;
    if-nez v2, :cond_0

    .line 301
    iget-object v3, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->normsProducer:Lorg/apache/lucene/codecs/DocValuesProducer;

    invoke-virtual {v3, v0}, Lorg/apache/lucene/codecs/DocValuesProducer;->getNumeric(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/NumericDocValues;

    move-result-object v2

    .line 302
    invoke-interface {v1, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method getNumericDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/NumericDocValues;
    .locals 5
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 174
    iget-object v3, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v3, p1}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(Ljava/lang/String;)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v2

    .line 175
    .local v2, "fi":Lorg/apache/lucene/index/FieldInfo;
    if-nez v2, :cond_1

    .line 198
    :cond_0
    :goto_0
    return-object v1

    .line 179
    :cond_1
    invoke-virtual {v2}, Lorg/apache/lucene/index/FieldInfo;->getDocValuesType()Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 183
    invoke-virtual {v2}, Lorg/apache/lucene/index/FieldInfo;->getDocValuesType()Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-result-object v3

    sget-object v4, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->NUMERIC:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    if-ne v3, v4, :cond_0

    .line 188
    sget-boolean v3, Lorg/apache/lucene/index/SegmentCoreReaders;->$assertionsDisabled:Z

    if-nez v3, :cond_2

    iget-object v3, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->dvProducer:Lorg/apache/lucene/codecs/DocValuesProducer;

    if-nez v3, :cond_2

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 190
    :cond_2
    iget-object v3, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->docValuesLocal:Lorg/apache/lucene/util/CloseableThreadLocal;

    invoke-virtual {v3}, Lorg/apache/lucene/util/CloseableThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 192
    .local v0, "dvFields":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/NumericDocValues;

    .line 193
    .local v1, "dvs":Lorg/apache/lucene/index/NumericDocValues;
    if-nez v1, :cond_0

    .line 194
    iget-object v3, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->dvProducer:Lorg/apache/lucene/codecs/DocValuesProducer;

    invoke-virtual {v3, v2}, Lorg/apache/lucene/codecs/DocValuesProducer;->getNumeric(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/NumericDocValues;

    move-result-object v1

    .line 195
    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method getSortedDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/SortedDocValues;
    .locals 5
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 230
    iget-object v3, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v3, p1}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(Ljava/lang/String;)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v2

    .line 231
    .local v2, "fi":Lorg/apache/lucene/index/FieldInfo;
    if-nez v2, :cond_1

    .line 254
    :cond_0
    :goto_0
    return-object v1

    .line 235
    :cond_1
    invoke-virtual {v2}, Lorg/apache/lucene/index/FieldInfo;->getDocValuesType()Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 239
    invoke-virtual {v2}, Lorg/apache/lucene/index/FieldInfo;->getDocValuesType()Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-result-object v3

    sget-object v4, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->SORTED:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    if-ne v3, v4, :cond_0

    .line 244
    sget-boolean v3, Lorg/apache/lucene/index/SegmentCoreReaders;->$assertionsDisabled:Z

    if-nez v3, :cond_2

    iget-object v3, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->dvProducer:Lorg/apache/lucene/codecs/DocValuesProducer;

    if-nez v3, :cond_2

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 246
    :cond_2
    iget-object v3, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->docValuesLocal:Lorg/apache/lucene/util/CloseableThreadLocal;

    invoke-virtual {v3}, Lorg/apache/lucene/util/CloseableThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 248
    .local v0, "dvFields":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/SortedDocValues;

    .line 249
    .local v1, "dvs":Lorg/apache/lucene/index/SortedDocValues;
    if-nez v1, :cond_0

    .line 250
    iget-object v3, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->dvProducer:Lorg/apache/lucene/codecs/DocValuesProducer;

    invoke-virtual {v3, v2}, Lorg/apache/lucene/codecs/DocValuesProducer;->getSorted(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/SortedDocValues;

    move-result-object v1

    .line 251
    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method getSortedSetDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/SortedSetDocValues;
    .locals 5
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 258
    iget-object v3, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v3, p1}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(Ljava/lang/String;)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v2

    .line 259
    .local v2, "fi":Lorg/apache/lucene/index/FieldInfo;
    if-nez v2, :cond_1

    .line 282
    :cond_0
    :goto_0
    return-object v1

    .line 263
    :cond_1
    invoke-virtual {v2}, Lorg/apache/lucene/index/FieldInfo;->getDocValuesType()Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 267
    invoke-virtual {v2}, Lorg/apache/lucene/index/FieldInfo;->getDocValuesType()Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-result-object v3

    sget-object v4, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->SORTED_SET:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    if-ne v3, v4, :cond_0

    .line 272
    sget-boolean v3, Lorg/apache/lucene/index/SegmentCoreReaders;->$assertionsDisabled:Z

    if-nez v3, :cond_2

    iget-object v3, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->dvProducer:Lorg/apache/lucene/codecs/DocValuesProducer;

    if-nez v3, :cond_2

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 274
    :cond_2
    iget-object v3, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->docValuesLocal:Lorg/apache/lucene/util/CloseableThreadLocal;

    invoke-virtual {v3}, Lorg/apache/lucene/util/CloseableThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 276
    .local v0, "dvFields":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/SortedSetDocValues;

    .line 277
    .local v1, "dvs":Lorg/apache/lucene/index/SortedSetDocValues;
    if-nez v1, :cond_0

    .line 278
    iget-object v3, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->dvProducer:Lorg/apache/lucene/codecs/DocValuesProducer;

    invoke-virtual {v3, v2}, Lorg/apache/lucene/codecs/DocValuesProducer;->getSortedSet(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/SortedSetDocValues;

    move-result-object v1

    .line 279
    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method incRef()V
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->ref:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 171
    return-void
.end method

.method removeCoreClosedListener(Lorg/apache/lucene/index/SegmentReader$CoreClosedListener;)V
    .locals 1
    .param p1, "listener"    # Lorg/apache/lucene/index/SegmentReader$CoreClosedListener;

    .prologue
    .line 329
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->coreClosedListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 330
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 334
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SegmentCoreReader(owner="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->owner:Lorg/apache/lucene/index/SegmentReader;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
