.class Lorg/apache/lucene/index/CoalescedDeletes;
.super Ljava/lang/Object;
.source "CoalescedDeletes.java"


# instance fields
.field final iterables:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Iterable",
            "<",
            "Lorg/apache/lucene/index/Term;",
            ">;>;"
        }
    .end annotation
.end field

.field final queries:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lorg/apache/lucene/search/Query;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/CoalescedDeletes;->queries:Ljava/util/Map;

    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/CoalescedDeletes;->iterables:Ljava/util/List;

    .line 29
    return-void
.end method


# virtual methods
.method public queriesIterable()Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "Lorg/apache/lucene/index/BufferedDeletesStream$QueryAndLimit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 63
    new-instance v0, Lorg/apache/lucene/index/CoalescedDeletes$2;

    invoke-direct {v0, p0}, Lorg/apache/lucene/index/CoalescedDeletes$2;-><init>(Lorg/apache/lucene/index/CoalescedDeletes;)V

    return-object v0
.end method

.method public termsIterable()Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "Lorg/apache/lucene/index/Term;",
            ">;"
        }
    .end annotation

    .prologue
    .line 49
    new-instance v0, Lorg/apache/lucene/index/CoalescedDeletes$1;

    invoke-direct {v0, p0}, Lorg/apache/lucene/index/CoalescedDeletes$1;-><init>(Lorg/apache/lucene/index/CoalescedDeletes;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 36
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CoalescedDeletes(termSets="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/index/CoalescedDeletes;->iterables:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",queries="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/index/CoalescedDeletes;->queries:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method update(Lorg/apache/lucene/index/FrozenBufferedDeletes;)V
    .locals 4
    .param p1, "in"    # Lorg/apache/lucene/index/FrozenBufferedDeletes;

    .prologue
    .line 40
    iget-object v2, p0, Lorg/apache/lucene/index/CoalescedDeletes;->iterables:Ljava/util/List;

    invoke-virtual {p1}, Lorg/apache/lucene/index/FrozenBufferedDeletes;->termsIterable()Ljava/lang/Iterable;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 42
    const/4 v1, 0x0

    .local v1, "queryIdx":I
    :goto_0
    iget-object v2, p1, Lorg/apache/lucene/index/FrozenBufferedDeletes;->queries:[Lorg/apache/lucene/search/Query;

    array-length v2, v2

    if-lt v1, v2, :cond_0

    .line 46
    return-void

    .line 43
    :cond_0
    iget-object v2, p1, Lorg/apache/lucene/index/FrozenBufferedDeletes;->queries:[Lorg/apache/lucene/search/Query;

    aget-object v0, v2, v1

    .line 44
    .local v0, "query":Lorg/apache/lucene/search/Query;
    iget-object v2, p0, Lorg/apache/lucene/index/CoalescedDeletes;->queries:Ljava/util/Map;

    sget-object v3, Lorg/apache/lucene/index/BufferedDeletes;->MAX_INT:Ljava/lang/Integer;

    invoke-interface {v2, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
