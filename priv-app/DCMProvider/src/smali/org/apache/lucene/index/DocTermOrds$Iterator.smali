.class Lorg/apache/lucene/index/DocTermOrds$Iterator;
.super Lorg/apache/lucene/index/SortedSetDocValues;
.source "DocTermOrds.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/DocTermOrds;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Iterator"
.end annotation


# instance fields
.field private arr:[B

.field final buffer:[I

.field bufferLength:I

.field bufferUpto:I

.field final reader:Lorg/apache/lucene/index/AtomicReader;

.field final te:Lorg/apache/lucene/index/TermsEnum;

.field final synthetic this$0:Lorg/apache/lucene/index/DocTermOrds;

.field private tnum:I

.field private upto:I


# direct methods
.method constructor <init>(Lorg/apache/lucene/index/DocTermOrds;Lorg/apache/lucene/index/AtomicReader;)V
    .locals 1
    .param p2, "reader"    # Lorg/apache/lucene/index/AtomicReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 789
    iput-object p1, p0, Lorg/apache/lucene/index/DocTermOrds$Iterator;->this$0:Lorg/apache/lucene/index/DocTermOrds;

    invoke-direct {p0}, Lorg/apache/lucene/index/SortedSetDocValues;-><init>()V

    .line 781
    const/4 v0, 0x5

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/apache/lucene/index/DocTermOrds$Iterator;->buffer:[I

    .line 790
    iput-object p2, p0, Lorg/apache/lucene/index/DocTermOrds$Iterator;->reader:Lorg/apache/lucene/index/AtomicReader;

    .line 791
    invoke-virtual {p0}, Lorg/apache/lucene/index/DocTermOrds$Iterator;->termsEnum()Lorg/apache/lucene/index/TermsEnum;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/DocTermOrds$Iterator;->te:Lorg/apache/lucene/index/TermsEnum;

    .line 792
    return-void
.end method


# virtual methods
.method public getValueCount()J
    .locals 2

    .prologue
    .line 886
    iget-object v0, p0, Lorg/apache/lucene/index/DocTermOrds$Iterator;->this$0:Lorg/apache/lucene/index/DocTermOrds;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocTermOrds;->numTerms()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public lookupOrd(JLorg/apache/lucene/util/BytesRef;)V
    .locals 5
    .param p1, "ord"    # J
    .param p3, "result"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 873
    const/4 v1, 0x0

    .line 875
    .local v1, "ref":Lorg/apache/lucene/util/BytesRef;
    :try_start_0
    iget-object v2, p0, Lorg/apache/lucene/index/DocTermOrds$Iterator;->this$0:Lorg/apache/lucene/index/DocTermOrds;

    iget-object v3, p0, Lorg/apache/lucene/index/DocTermOrds$Iterator;->te:Lorg/apache/lucene/index/TermsEnum;

    long-to-int v4, p1

    invoke-virtual {v2, v3, v4}, Lorg/apache/lucene/index/DocTermOrds;->lookupTerm(Lorg/apache/lucene/index/TermsEnum;I)Lorg/apache/lucene/util/BytesRef;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 879
    iget-object v2, v1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iput-object v2, p3, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 880
    iget v2, v1, Lorg/apache/lucene/util/BytesRef;->offset:I

    iput v2, p3, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 881
    iget v2, v1, Lorg/apache/lucene/util/BytesRef;->length:I

    iput v2, p3, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 882
    return-void

    .line 876
    :catch_0
    move-exception v0

    .line 877
    .local v0, "e":Ljava/io/IOException;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method public lookupTerm(Lorg/apache/lucene/util/BytesRef;)J
    .locals 6
    .param p1, "key"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 892
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/index/DocTermOrds$Iterator;->te:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/index/TermsEnum;->seekCeil(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    move-result-object v1

    sget-object v2, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    if-ne v1, v2, :cond_0

    .line 893
    iget-object v1, p0, Lorg/apache/lucene/index/DocTermOrds$Iterator;->te:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v1}, Lorg/apache/lucene/index/TermsEnum;->ord()J

    move-result-wide v2

    .line 895
    :goto_0
    return-wide v2

    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/DocTermOrds$Iterator;->te:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v1}, Lorg/apache/lucene/index/TermsEnum;->ord()J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    neg-long v2, v2

    const-wide/16 v4, 0x1

    sub-long/2addr v2, v4

    goto :goto_0

    .line 897
    :catch_0
    move-exception v0

    .line 898
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public nextOrd()J
    .locals 3

    .prologue
    .line 796
    :goto_0
    iget v0, p0, Lorg/apache/lucene/index/DocTermOrds$Iterator;->bufferUpto:I

    iget v1, p0, Lorg/apache/lucene/index/DocTermOrds$Iterator;->bufferLength:I

    if-eq v0, v1, :cond_0

    .line 804
    iget-object v0, p0, Lorg/apache/lucene/index/DocTermOrds$Iterator;->buffer:[I

    iget v1, p0, Lorg/apache/lucene/index/DocTermOrds$Iterator;->bufferUpto:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/index/DocTermOrds$Iterator;->bufferUpto:I

    aget v0, v0, v1

    int-to-long v0, v0

    :goto_1
    return-wide v0

    .line 797
    :cond_0
    iget v0, p0, Lorg/apache/lucene/index/DocTermOrds$Iterator;->bufferLength:I

    iget-object v1, p0, Lorg/apache/lucene/index/DocTermOrds$Iterator;->buffer:[I

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 798
    const-wide/16 v0, -0x1

    goto :goto_1

    .line 800
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/index/DocTermOrds$Iterator;->buffer:[I

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/DocTermOrds$Iterator;->read([I)I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/index/DocTermOrds$Iterator;->bufferLength:I

    .line 801
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/index/DocTermOrds$Iterator;->bufferUpto:I

    goto :goto_0
.end method

.method read([I)I
    .locals 8
    .param p1, "buffer"    # [I

    .prologue
    .line 811
    const/4 v1, 0x0

    .line 812
    .local v1, "bufferUpto":I
    iget-object v5, p0, Lorg/apache/lucene/index/DocTermOrds$Iterator;->arr:[B

    if-nez v5, :cond_3

    .line 815
    iget v3, p0, Lorg/apache/lucene/index/DocTermOrds$Iterator;->upto:I

    .line 816
    .local v3, "code":I
    const/4 v4, 0x0

    .local v4, "delta":I
    move v2, v1

    .line 818
    .end local v1    # "bufferUpto":I
    .local v2, "bufferUpto":I
    :goto_0
    shl-int/lit8 v5, v4, 0x7

    and-int/lit8 v6, v3, 0x7f

    or-int v4, v5, v6

    .line 819
    and-int/lit16 v5, v3, 0x80

    if-nez v5, :cond_5

    .line 820
    if-nez v4, :cond_1

    move v1, v2

    .line 849
    .end local v2    # "bufferUpto":I
    .end local v3    # "code":I
    .restart local v1    # "bufferUpto":I
    :cond_0
    :goto_1
    return v1

    .line 821
    .end local v1    # "bufferUpto":I
    .restart local v2    # "bufferUpto":I
    .restart local v3    # "code":I
    :cond_1
    iget v5, p0, Lorg/apache/lucene/index/DocTermOrds$Iterator;->tnum:I

    add-int/lit8 v6, v4, -0x2

    add-int/2addr v5, v6

    iput v5, p0, Lorg/apache/lucene/index/DocTermOrds$Iterator;->tnum:I

    .line 822
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "bufferUpto":I
    .restart local v1    # "bufferUpto":I
    iget-object v5, p0, Lorg/apache/lucene/index/DocTermOrds$Iterator;->this$0:Lorg/apache/lucene/index/DocTermOrds;

    iget v5, v5, Lorg/apache/lucene/index/DocTermOrds;->ordBase:I

    iget v6, p0, Lorg/apache/lucene/index/DocTermOrds$Iterator;->tnum:I

    add-int/2addr v5, v6

    aput v5, p1, v2

    .line 824
    const/4 v4, 0x0

    .line 826
    :goto_2
    ushr-int/lit8 v3, v3, 0x8

    move v2, v1

    .line 817
    .end local v1    # "bufferUpto":I
    .restart local v2    # "bufferUpto":I
    goto :goto_0

    .end local v3    # "code":I
    .local v0, "b":B
    :cond_2
    move v1, v2

    .line 831
    .end local v0    # "b":B
    .end local v2    # "bufferUpto":I
    .end local v4    # "delta":I
    .restart local v1    # "bufferUpto":I
    :cond_3
    const/4 v4, 0x0

    .line 833
    .restart local v4    # "delta":I
    :cond_4
    iget-object v5, p0, Lorg/apache/lucene/index/DocTermOrds$Iterator;->arr:[B

    iget v6, p0, Lorg/apache/lucene/index/DocTermOrds$Iterator;->upto:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/index/DocTermOrds$Iterator;->upto:I

    aget-byte v0, v5, v6

    .line 834
    .restart local v0    # "b":B
    shl-int/lit8 v5, v4, 0x7

    and-int/lit8 v6, v0, 0x7f

    or-int v4, v5, v6

    .line 836
    and-int/lit16 v5, v0, 0x80

    if-nez v5, :cond_4

    .line 839
    if-eqz v4, :cond_0

    .line 840
    iget v5, p0, Lorg/apache/lucene/index/DocTermOrds$Iterator;->tnum:I

    add-int/lit8 v6, v4, -0x2

    add-int/2addr v5, v6

    iput v5, p0, Lorg/apache/lucene/index/DocTermOrds$Iterator;->tnum:I

    .line 842
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "bufferUpto":I
    .restart local v2    # "bufferUpto":I
    iget-object v5, p0, Lorg/apache/lucene/index/DocTermOrds$Iterator;->this$0:Lorg/apache/lucene/index/DocTermOrds;

    iget v5, v5, Lorg/apache/lucene/index/DocTermOrds;->ordBase:I

    iget v6, p0, Lorg/apache/lucene/index/DocTermOrds$Iterator;->tnum:I

    add-int/2addr v5, v6

    aput v5, p1, v1

    .line 843
    array-length v5, p1

    if-ne v2, v5, :cond_2

    move v1, v2

    .end local v2    # "bufferUpto":I
    .restart local v1    # "bufferUpto":I
    goto :goto_1

    .end local v0    # "b":B
    .end local v1    # "bufferUpto":I
    .restart local v2    # "bufferUpto":I
    .restart local v3    # "code":I
    :cond_5
    move v1, v2

    .end local v2    # "bufferUpto":I
    .restart local v1    # "bufferUpto":I
    goto :goto_2
.end method

.method public setDocument(I)V
    .locals 5
    .param p1, "docID"    # I

    .prologue
    const/4 v4, 0x0

    .line 854
    iput v4, p0, Lorg/apache/lucene/index/DocTermOrds$Iterator;->tnum:I

    .line 855
    iget-object v2, p0, Lorg/apache/lucene/index/DocTermOrds$Iterator;->this$0:Lorg/apache/lucene/index/DocTermOrds;

    iget-object v2, v2, Lorg/apache/lucene/index/DocTermOrds;->index:[I

    aget v0, v2, p1

    .line 856
    .local v0, "code":I
    and-int/lit16 v2, v0, 0xff

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 858
    ushr-int/lit8 v2, v0, 0x8

    iput v2, p0, Lorg/apache/lucene/index/DocTermOrds$Iterator;->upto:I

    .line 860
    ushr-int/lit8 v2, p1, 0x10

    and-int/lit16 v1, v2, 0xff

    .line 861
    .local v1, "whichArray":I
    iget-object v2, p0, Lorg/apache/lucene/index/DocTermOrds$Iterator;->this$0:Lorg/apache/lucene/index/DocTermOrds;

    iget-object v2, v2, Lorg/apache/lucene/index/DocTermOrds;->tnums:[[B

    aget-object v2, v2, v1

    iput-object v2, p0, Lorg/apache/lucene/index/DocTermOrds$Iterator;->arr:[B

    .line 867
    .end local v1    # "whichArray":I
    :goto_0
    iput v4, p0, Lorg/apache/lucene/index/DocTermOrds$Iterator;->bufferUpto:I

    .line 868
    iget-object v2, p0, Lorg/apache/lucene/index/DocTermOrds$Iterator;->buffer:[I

    invoke-virtual {p0, v2}, Lorg/apache/lucene/index/DocTermOrds$Iterator;->read([I)I

    move-result v2

    iput v2, p0, Lorg/apache/lucene/index/DocTermOrds$Iterator;->bufferLength:I

    .line 869
    return-void

    .line 864
    :cond_0
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/apache/lucene/index/DocTermOrds$Iterator;->arr:[B

    .line 865
    iput v0, p0, Lorg/apache/lucene/index/DocTermOrds$Iterator;->upto:I

    goto :goto_0
.end method

.method public termsEnum()Lorg/apache/lucene/index/TermsEnum;
    .locals 3

    .prologue
    .line 905
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/index/DocTermOrds$Iterator;->this$0:Lorg/apache/lucene/index/DocTermOrds;

    iget-object v2, p0, Lorg/apache/lucene/index/DocTermOrds$Iterator;->reader:Lorg/apache/lucene/index/AtomicReader;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/index/DocTermOrds;->getOrdTermsEnum(Lorg/apache/lucene/index/AtomicReader;)Lorg/apache/lucene/index/TermsEnum;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 906
    :catch_0
    move-exception v0

    .line 907
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1}, Ljava/lang/RuntimeException;-><init>()V

    throw v1
.end method
