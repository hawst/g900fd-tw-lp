.class public abstract Lorg/apache/lucene/index/StoredFieldVisitor;
.super Ljava/lang/Object;
.source "StoredFieldVisitor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/StoredFieldVisitor$Status;
    }
.end annotation


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    return-void
.end method


# virtual methods
.method public binaryField(Lorg/apache/lucene/index/FieldInfo;[B)V
    .locals 0
    .param p1, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;
    .param p2, "value"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 54
    return-void
.end method

.method public doubleField(Lorg/apache/lucene/index/FieldInfo;D)V
    .locals 0
    .param p1, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;
    .param p2, "value"    # D
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 74
    return-void
.end method

.method public floatField(Lorg/apache/lucene/index/FieldInfo;F)V
    .locals 0
    .param p1, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;
    .param p2, "value"    # F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 70
    return-void
.end method

.method public intField(Lorg/apache/lucene/index/FieldInfo;I)V
    .locals 0
    .param p1, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;
    .param p2, "value"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 62
    return-void
.end method

.method public longField(Lorg/apache/lucene/index/FieldInfo;J)V
    .locals 0
    .param p1, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;
    .param p2, "value"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 66
    return-void
.end method

.method public abstract needsField(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/StoredFieldVisitor$Status;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public stringField(Lorg/apache/lucene/index/FieldInfo;Ljava/lang/String;)V
    .locals 0
    .param p1, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;
    .param p2, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58
    return-void
.end method
