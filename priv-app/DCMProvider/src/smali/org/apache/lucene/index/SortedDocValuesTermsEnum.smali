.class Lorg/apache/lucene/index/SortedDocValuesTermsEnum;
.super Lorg/apache/lucene/index/TermsEnum;
.source "SortedDocValuesTermsEnum.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private currentOrd:I

.field private final term:Lorg/apache/lucene/util/BytesRef;

.field private final values:Lorg/apache/lucene/index/SortedDocValues;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lorg/apache/lucene/index/SortedDocValuesTermsEnum;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/SortedDocValuesTermsEnum;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/index/SortedDocValues;)V
    .locals 1
    .param p1, "values"    # Lorg/apache/lucene/index/SortedDocValues;

    .prologue
    .line 35
    invoke-direct {p0}, Lorg/apache/lucene/index/TermsEnum;-><init>()V

    .line 31
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/index/SortedDocValuesTermsEnum;->currentOrd:I

    .line 32
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/SortedDocValuesTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    .line 36
    iput-object p1, p0, Lorg/apache/lucene/index/SortedDocValuesTermsEnum;->values:Lorg/apache/lucene/index/SortedDocValues;

    .line 37
    return-void
.end method


# virtual methods
.method public docFreq()I
    .locals 1

    .prologue
    .line 109
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public docs(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;I)Lorg/apache/lucene/index/DocsEnum;
    .locals 1
    .param p1, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p2, "reuse"    # Lorg/apache/lucene/index/DocsEnum;
    .param p3, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 119
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public docsAndPositions(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsAndPositionsEnum;I)Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .locals 1
    .param p1, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p2, "reuse"    # Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .param p3, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 124
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getComparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation

    .prologue
    .line 129
    invoke-static {}, Lorg/apache/lucene/util/BytesRef;->getUTF8SortedAsUnicodeComparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public next()Lorg/apache/lucene/util/BytesRef;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 89
    iget v0, p0, Lorg/apache/lucene/index/SortedDocValuesTermsEnum;->currentOrd:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/index/SortedDocValuesTermsEnum;->currentOrd:I

    .line 90
    iget v0, p0, Lorg/apache/lucene/index/SortedDocValuesTermsEnum;->currentOrd:I

    iget-object v1, p0, Lorg/apache/lucene/index/SortedDocValuesTermsEnum;->values:Lorg/apache/lucene/index/SortedDocValues;

    invoke-virtual {v1}, Lorg/apache/lucene/index/SortedDocValues;->getValueCount()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 91
    const/4 v0, 0x0

    .line 94
    :goto_0
    return-object v0

    .line 93
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/SortedDocValuesTermsEnum;->values:Lorg/apache/lucene/index/SortedDocValues;

    iget v1, p0, Lorg/apache/lucene/index/SortedDocValuesTermsEnum;->currentOrd:I

    iget-object v2, p0, Lorg/apache/lucene/index/SortedDocValuesTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/index/SortedDocValues;->lookupOrd(ILorg/apache/lucene/util/BytesRef;)V

    .line 94
    iget-object v0, p0, Lorg/apache/lucene/index/SortedDocValuesTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    goto :goto_0
.end method

.method public ord()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 104
    iget v0, p0, Lorg/apache/lucene/index/SortedDocValuesTermsEnum;->currentOrd:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public seekCeil(Lorg/apache/lucene/util/BytesRef;Z)Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    .locals 4
    .param p1, "text"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "useCache"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 41
    iget-object v1, p0, Lorg/apache/lucene/index/SortedDocValuesTermsEnum;->values:Lorg/apache/lucene/index/SortedDocValues;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/index/SortedDocValues;->lookupTerm(Lorg/apache/lucene/util/BytesRef;)I

    move-result v0

    .line 42
    .local v0, "ord":I
    if-ltz v0, :cond_0

    .line 43
    iput v0, p0, Lorg/apache/lucene/index/SortedDocValuesTermsEnum;->currentOrd:I

    .line 44
    iget-object v1, p0, Lorg/apache/lucene/index/SortedDocValuesTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    const/4 v2, 0x0

    iput v2, v1, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 48
    iget-object v1, p0, Lorg/apache/lucene/index/SortedDocValuesTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget v2, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    new-array v2, v2, [B

    iput-object v2, v1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 49
    iget-object v1, p0, Lorg/apache/lucene/index/SortedDocValuesTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/util/BytesRef;->copyBytes(Lorg/apache/lucene/util/BytesRef;)V

    .line 50
    sget-object v1, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    .line 58
    :goto_0
    return-object v1

    .line 52
    :cond_0
    neg-int v1, v0

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/lucene/index/SortedDocValuesTermsEnum;->currentOrd:I

    .line 53
    iget v1, p0, Lorg/apache/lucene/index/SortedDocValuesTermsEnum;->currentOrd:I

    iget-object v2, p0, Lorg/apache/lucene/index/SortedDocValuesTermsEnum;->values:Lorg/apache/lucene/index/SortedDocValues;

    invoke-virtual {v2}, Lorg/apache/lucene/index/SortedDocValues;->getValueCount()I

    move-result v2

    if-ne v1, v2, :cond_1

    .line 54
    sget-object v1, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->END:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    goto :goto_0

    .line 57
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/index/SortedDocValuesTermsEnum;->values:Lorg/apache/lucene/index/SortedDocValues;

    iget v2, p0, Lorg/apache/lucene/index/SortedDocValuesTermsEnum;->currentOrd:I

    iget-object v3, p0, Lorg/apache/lucene/index/SortedDocValuesTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/index/SortedDocValues;->lookupOrd(ILorg/apache/lucene/util/BytesRef;)V

    .line 58
    sget-object v1, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->NOT_FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    goto :goto_0
.end method

.method public seekExact(J)V
    .locals 3
    .param p1, "ord"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 82
    sget-boolean v0, Lorg/apache/lucene/index/SortedDocValuesTermsEnum;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/SortedDocValuesTermsEnum;->values:Lorg/apache/lucene/index/SortedDocValues;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SortedDocValues;->getValueCount()I

    move-result v0

    int-to-long v0, v0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 83
    :cond_1
    long-to-int v0, p1

    iput v0, p0, Lorg/apache/lucene/index/SortedDocValuesTermsEnum;->currentOrd:I

    .line 84
    iget-object v0, p0, Lorg/apache/lucene/index/SortedDocValuesTermsEnum;->values:Lorg/apache/lucene/index/SortedDocValues;

    iget v1, p0, Lorg/apache/lucene/index/SortedDocValuesTermsEnum;->currentOrd:I

    iget-object v2, p0, Lorg/apache/lucene/index/SortedDocValuesTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/index/SortedDocValues;->lookupOrd(ILorg/apache/lucene/util/BytesRef;)V

    .line 85
    return-void
.end method

.method public seekExact(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/index/TermState;)V
    .locals 2
    .param p1, "term"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "state"    # Lorg/apache/lucene/index/TermState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 134
    sget-boolean v0, Lorg/apache/lucene/index/SortedDocValuesTermsEnum;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-eqz p2, :cond_0

    instance-of v0, p2, Lorg/apache/lucene/index/OrdTermState;

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 135
    :cond_1
    check-cast p2, Lorg/apache/lucene/index/OrdTermState;

    .end local p2    # "state":Lorg/apache/lucene/index/TermState;
    iget-wide v0, p2, Lorg/apache/lucene/index/OrdTermState;->ord:J

    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/index/SortedDocValuesTermsEnum;->seekExact(J)V

    .line 136
    return-void
.end method

.method public seekExact(Lorg/apache/lucene/util/BytesRef;Z)Z
    .locals 3
    .param p1, "text"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "useCache"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 65
    iget-object v2, p0, Lorg/apache/lucene/index/SortedDocValuesTermsEnum;->values:Lorg/apache/lucene/index/SortedDocValues;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/index/SortedDocValues;->lookupTerm(Lorg/apache/lucene/util/BytesRef;)I

    move-result v0

    .line 66
    .local v0, "ord":I
    if-ltz v0, :cond_0

    .line 67
    iget-object v2, p0, Lorg/apache/lucene/index/SortedDocValuesTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iput v1, v2, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 71
    iget-object v1, p0, Lorg/apache/lucene/index/SortedDocValuesTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget v2, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    new-array v2, v2, [B

    iput-object v2, v1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 72
    iget-object v1, p0, Lorg/apache/lucene/index/SortedDocValuesTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/util/BytesRef;->copyBytes(Lorg/apache/lucene/util/BytesRef;)V

    .line 73
    iput v0, p0, Lorg/apache/lucene/index/SortedDocValuesTermsEnum;->currentOrd:I

    .line 74
    const/4 v1, 0x1

    .line 76
    :cond_0
    return v1
.end method

.method public term()Lorg/apache/lucene/util/BytesRef;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 99
    iget-object v0, p0, Lorg/apache/lucene/index/SortedDocValuesTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    return-object v0
.end method

.method public termState()Lorg/apache/lucene/index/TermState;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 140
    new-instance v0, Lorg/apache/lucene/index/OrdTermState;

    invoke-direct {v0}, Lorg/apache/lucene/index/OrdTermState;-><init>()V

    .line 141
    .local v0, "state":Lorg/apache/lucene/index/OrdTermState;
    iget v1, p0, Lorg/apache/lucene/index/SortedDocValuesTermsEnum;->currentOrd:I

    int-to-long v2, v1

    iput-wide v2, v0, Lorg/apache/lucene/index/OrdTermState;->ord:J

    .line 142
    return-object v0
.end method

.method public totalTermFreq()J
    .locals 2

    .prologue
    .line 114
    const-wide/16 v0, -0x1

    return-wide v0
.end method
