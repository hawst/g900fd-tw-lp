.class final Lorg/apache/lucene/index/MultiTermsEnum$TermMergeQueue;
.super Lorg/apache/lucene/util/PriorityQueue;
.source "MultiTermsEnum.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/MultiTermsEnum;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "TermMergeQueue"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/util/PriorityQueue",
        "<",
        "Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;",
        ">;"
    }
.end annotation


# instance fields
.field termComp:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(I)V
    .locals 0
    .param p1, "size"    # I

    .prologue
    .line 524
    invoke-direct {p0, p1}, Lorg/apache/lucene/util/PriorityQueue;-><init>(I)V

    .line 525
    return-void
.end method


# virtual methods
.method protected bridge synthetic lessThan(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

    check-cast p2, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/index/MultiTermsEnum$TermMergeQueue;->lessThan(Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;)Z

    move-result v0

    return v0
.end method

.method protected lessThan(Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;)Z
    .locals 6
    .param p1, "termsA"    # Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;
    .param p2, "termsB"    # Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 529
    iget-object v3, p0, Lorg/apache/lucene/index/MultiTermsEnum$TermMergeQueue;->termComp:Ljava/util/Comparator;

    iget-object v4, p1, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->current:Lorg/apache/lucene/util/BytesRef;

    iget-object v5, p2, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->current:Lorg/apache/lucene/util/BytesRef;

    invoke-interface {v3, v4, v5}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    .line 530
    .local v0, "cmp":I
    if-eqz v0, :cond_2

    .line 531
    if-gez v0, :cond_1

    .line 533
    :cond_0
    :goto_0
    return v1

    :cond_1
    move v1, v2

    .line 531
    goto :goto_0

    .line 533
    :cond_2
    # getter for: Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->subSlice:Lorg/apache/lucene/index/ReaderSlice;
    invoke-static {p1}, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->access$0(Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;)Lorg/apache/lucene/index/ReaderSlice;

    move-result-object v3

    iget v3, v3, Lorg/apache/lucene/index/ReaderSlice;->start:I

    # getter for: Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->subSlice:Lorg/apache/lucene/index/ReaderSlice;
    invoke-static {p2}, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->access$0(Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;)Lorg/apache/lucene/index/ReaderSlice;

    move-result-object v4

    iget v4, v4, Lorg/apache/lucene/index/ReaderSlice;->start:I

    if-lt v3, v4, :cond_0

    move v1, v2

    goto :goto_0
.end method
