.class Lorg/apache/lucene/index/TermsEnum$1;
.super Lorg/apache/lucene/index/TermsEnum;
.source "TermsEnum.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/TermsEnum;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 228
    invoke-direct {p0}, Lorg/apache/lucene/index/TermsEnum;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public declared-synchronized attributes()Lorg/apache/lucene/util/AttributeSource;
    .locals 1

    .prologue
    .line 277
    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Lorg/apache/lucene/index/TermsEnum;->attributes()Lorg/apache/lucene/util/AttributeSource;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public docFreq()I
    .locals 2

    .prologue
    .line 247
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "this method should never be called"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public docs(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;I)Lorg/apache/lucene/index/DocsEnum;
    .locals 2
    .param p1, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p2, "reuse"    # Lorg/apache/lucene/index/DocsEnum;
    .param p3, "flags"    # I

    .prologue
    .line 262
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "this method should never be called"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public docsAndPositions(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsAndPositionsEnum;I)Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .locals 2
    .param p1, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p2, "reuse"    # Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .param p3, "flags"    # I

    .prologue
    .line 267
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "this method should never be called"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getComparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation

    .prologue
    .line 242
    const/4 v0, 0x0

    return-object v0
.end method

.method public next()Lorg/apache/lucene/util/BytesRef;
    .locals 1

    .prologue
    .line 272
    const/4 v0, 0x0

    return-object v0
.end method

.method public ord()J
    .locals 2

    .prologue
    .line 257
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "this method should never be called"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public seekCeil(Lorg/apache/lucene/util/BytesRef;Z)Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    .locals 1
    .param p1, "term"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "useCache"    # Z

    .prologue
    .line 230
    sget-object v0, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->END:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    return-object v0
.end method

.method public seekExact(J)V
    .locals 0
    .param p1, "ord"    # J

    .prologue
    .line 233
    return-void
.end method

.method public seekExact(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/index/TermState;)V
    .locals 2
    .param p1, "term"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "state"    # Lorg/apache/lucene/index/TermState;

    .prologue
    .line 287
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "this method should never be called"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public term()Lorg/apache/lucene/util/BytesRef;
    .locals 2

    .prologue
    .line 237
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "this method should never be called"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public termState()Lorg/apache/lucene/index/TermState;
    .locals 2

    .prologue
    .line 282
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "this method should never be called"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public totalTermFreq()J
    .locals 2

    .prologue
    .line 252
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "this method should never be called"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
