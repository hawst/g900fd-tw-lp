.class Lorg/apache/lucene/index/DocumentsWriterPerThread$1;
.super Lorg/apache/lucene/index/DocumentsWriterPerThread$IndexingChain;
.source "DocumentsWriterPerThread.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/DocumentsWriterPerThread;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Lorg/apache/lucene/index/DocumentsWriterPerThread$IndexingChain;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method getChain(Lorg/apache/lucene/index/DocumentsWriterPerThread;)Lorg/apache/lucene/index/DocConsumer;
    .locals 10
    .param p1, "documentsWriterPerThread"    # Lorg/apache/lucene/index/DocumentsWriterPerThread;

    .prologue
    .line 85
    new-instance v4, Lorg/apache/lucene/index/TermVectorsConsumer;

    invoke-direct {v4, p1}, Lorg/apache/lucene/index/TermVectorsConsumer;-><init>(Lorg/apache/lucene/index/DocumentsWriterPerThread;)V

    .line 86
    .local v4, "termVectorsWriter":Lorg/apache/lucene/index/TermsHashConsumer;
    new-instance v1, Lorg/apache/lucene/index/FreqProxTermsWriter;

    invoke-direct {v1}, Lorg/apache/lucene/index/FreqProxTermsWriter;-><init>()V

    .line 88
    .local v1, "freqProxWriter":Lorg/apache/lucene/index/TermsHashConsumer;
    new-instance v5, Lorg/apache/lucene/index/TermsHash;

    const/4 v6, 0x1

    .line 89
    new-instance v7, Lorg/apache/lucene/index/TermsHash;

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct {v7, p1, v4, v8, v9}, Lorg/apache/lucene/index/TermsHash;-><init>(Lorg/apache/lucene/index/DocumentsWriterPerThread;Lorg/apache/lucene/index/TermsHashConsumer;ZLorg/apache/lucene/index/TermsHash;)V

    .line 88
    invoke-direct {v5, p1, v1, v6, v7}, Lorg/apache/lucene/index/TermsHash;-><init>(Lorg/apache/lucene/index/DocumentsWriterPerThread;Lorg/apache/lucene/index/TermsHashConsumer;ZLorg/apache/lucene/index/TermsHash;)V

    .line 90
    .local v5, "termsHash":Lorg/apache/lucene/index/InvertedDocConsumer;
    new-instance v2, Lorg/apache/lucene/index/NormsConsumer;

    invoke-direct {v2}, Lorg/apache/lucene/index/NormsConsumer;-><init>()V

    .line 91
    .local v2, "normsWriter":Lorg/apache/lucene/index/NormsConsumer;
    new-instance v0, Lorg/apache/lucene/index/DocInverter;

    iget-object v6, p1, Lorg/apache/lucene/index/DocumentsWriterPerThread;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    invoke-direct {v0, v6, v5, v2}, Lorg/apache/lucene/index/DocInverter;-><init>(Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;Lorg/apache/lucene/index/InvertedDocConsumer;Lorg/apache/lucene/index/InvertedDocEndConsumer;)V

    .line 92
    .local v0, "docInverter":Lorg/apache/lucene/index/DocInverter;
    new-instance v3, Lorg/apache/lucene/index/TwoStoredFieldsConsumers;

    .line 93
    new-instance v6, Lorg/apache/lucene/index/StoredFieldsProcessor;

    invoke-direct {v6, p1}, Lorg/apache/lucene/index/StoredFieldsProcessor;-><init>(Lorg/apache/lucene/index/DocumentsWriterPerThread;)V

    .line 94
    new-instance v7, Lorg/apache/lucene/index/DocValuesProcessor;

    iget-object v8, p1, Lorg/apache/lucene/index/DocumentsWriterPerThread;->bytesUsed:Lorg/apache/lucene/util/Counter;

    invoke-direct {v7, v8}, Lorg/apache/lucene/index/DocValuesProcessor;-><init>(Lorg/apache/lucene/util/Counter;)V

    .line 92
    invoke-direct {v3, v6, v7}, Lorg/apache/lucene/index/TwoStoredFieldsConsumers;-><init>(Lorg/apache/lucene/index/StoredFieldsConsumer;Lorg/apache/lucene/index/StoredFieldsConsumer;)V

    .line 95
    .local v3, "storedFields":Lorg/apache/lucene/index/StoredFieldsConsumer;
    new-instance v6, Lorg/apache/lucene/index/DocFieldProcessor;

    invoke-direct {v6, p1, v0, v3}, Lorg/apache/lucene/index/DocFieldProcessor;-><init>(Lorg/apache/lucene/index/DocumentsWriterPerThread;Lorg/apache/lucene/index/DocFieldConsumer;Lorg/apache/lucene/index/StoredFieldsConsumer;)V

    return-object v6
.end method
