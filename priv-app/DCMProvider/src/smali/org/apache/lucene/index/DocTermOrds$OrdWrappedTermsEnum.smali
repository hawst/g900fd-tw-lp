.class final Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;
.super Lorg/apache/lucene/index/TermsEnum;
.source "DocTermOrds.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/DocTermOrds;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "OrdWrappedTermsEnum"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private ord:J

.field private term:Lorg/apache/lucene/util/BytesRef;

.field private final termsEnum:Lorg/apache/lucene/index/TermsEnum;

.field final synthetic this$0:Lorg/apache/lucene/index/DocTermOrds;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 604
    const-class v0, Lorg/apache/lucene/index/DocTermOrds;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/index/DocTermOrds;Lorg/apache/lucene/index/AtomicReader;)V
    .locals 2
    .param p2, "reader"    # Lorg/apache/lucene/index/AtomicReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 609
    iput-object p1, p0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->this$0:Lorg/apache/lucene/index/DocTermOrds;

    invoke-direct {p0}, Lorg/apache/lucene/index/TermsEnum;-><init>()V

    .line 607
    # getter for: Lorg/apache/lucene/index/DocTermOrds;->indexInterval:I
    invoke-static {p1}, Lorg/apache/lucene/index/DocTermOrds;->access$0(Lorg/apache/lucene/index/DocTermOrds;)I

    move-result v0

    neg-int v0, v0

    add-int/lit8 v0, v0, -0x1

    int-to-long v0, v0

    iput-wide v0, p0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->ord:J

    .line 610
    sget-boolean v0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p1, Lorg/apache/lucene/index/DocTermOrds;->indexedTermsArray:[Lorg/apache/lucene/util/BytesRef;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 611
    :cond_0
    invoke-virtual {p2}, Lorg/apache/lucene/index/AtomicReader;->fields()Lorg/apache/lucene/index/Fields;

    move-result-object v0

    iget-object v1, p1, Lorg/apache/lucene/index/DocTermOrds;->field:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/Fields;->terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->termsEnum:Lorg/apache/lucene/index/TermsEnum;

    .line 612
    return-void
.end method

.method private setTerm()Lorg/apache/lucene/util/BytesRef;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 752
    iget-object v0, p0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->termsEnum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermsEnum;->term()Lorg/apache/lucene/util/BytesRef;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    .line 754
    iget-object v0, p0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->this$0:Lorg/apache/lucene/index/DocTermOrds;

    iget-object v0, v0, Lorg/apache/lucene/index/DocTermOrds;->prefix:Lorg/apache/lucene/util/BytesRef;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget-object v1, p0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->this$0:Lorg/apache/lucene/index/DocTermOrds;

    iget-object v1, v1, Lorg/apache/lucene/index/DocTermOrds;->prefix:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v0, v1}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 755
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    .line 757
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    return-object v0
.end method


# virtual methods
.method public docFreq()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 648
    iget-object v0, p0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->termsEnum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermsEnum;->docFreq()I

    move-result v0

    return v0
.end method

.method public docs(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;I)Lorg/apache/lucene/index/DocsEnum;
    .locals 1
    .param p1, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p2, "reuse"    # Lorg/apache/lucene/index/DocsEnum;
    .param p3, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 621
    iget-object v0, p0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->termsEnum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/lucene/index/TermsEnum;->docs(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;I)Lorg/apache/lucene/index/DocsEnum;

    move-result-object v0

    return-object v0
.end method

.method public docsAndPositions(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsAndPositionsEnum;I)Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .locals 1
    .param p1, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p2, "reuse"    # Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .param p3, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 626
    iget-object v0, p0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->termsEnum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/lucene/index/TermsEnum;->docsAndPositions(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsAndPositionsEnum;I)Lorg/apache/lucene/index/DocsAndPositionsEnum;

    move-result-object v0

    return-object v0
.end method

.method public getComparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation

    .prologue
    .line 616
    iget-object v0, p0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->termsEnum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermsEnum;->getComparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public next()Lorg/apache/lucene/util/BytesRef;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v6, 0x0

    const/4 v0, 0x0

    .line 636
    iget-wide v2, p0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->ord:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->ord:J

    cmp-long v1, v2, v6

    if-gez v1, :cond_0

    .line 637
    iput-wide v6, p0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->ord:J

    .line 639
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->termsEnum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v1}, Lorg/apache/lucene/index/TermsEnum;->next()Lorg/apache/lucene/util/BytesRef;

    move-result-object v1

    if-nez v1, :cond_1

    .line 640
    iput-object v0, p0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    .line 643
    :goto_0
    return-object v0

    :cond_1
    invoke-direct {p0}, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->setTerm()Lorg/apache/lucene/util/BytesRef;

    move-result-object v0

    goto :goto_0
.end method

.method public ord()J
    .locals 4

    .prologue
    .line 658
    iget-object v0, p0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->this$0:Lorg/apache/lucene/index/DocTermOrds;

    iget v0, v0, Lorg/apache/lucene/index/DocTermOrds;->ordBase:I

    int-to-long v0, v0

    iget-wide v2, p0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->ord:J

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public seekCeil(Lorg/apache/lucene/util/BytesRef;Z)Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    .locals 6
    .param p1, "target"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "useCache"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 665
    iget-object v2, p0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/util/BytesRef;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 666
    sget-object v2, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    .line 718
    :goto_0
    return-object v2

    .line 669
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->this$0:Lorg/apache/lucene/index/DocTermOrds;

    iget-object v2, v2, Lorg/apache/lucene/index/DocTermOrds;->indexedTermsArray:[Lorg/apache/lucene/util/BytesRef;

    invoke-static {v2, p1}, Ljava/util/Arrays;->binarySearch([Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v1

    .line 671
    .local v1, "startIdx":I
    if-ltz v1, :cond_3

    .line 673
    iget-object v2, p0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->termsEnum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/index/TermsEnum;->seekCeil(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    move-result-object v0

    .line 674
    .local v0, "seekStatus":Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    sget-boolean v2, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->$assertionsDisabled:Z

    if-nez v2, :cond_1

    sget-object v2, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    if-eq v0, v2, :cond_1

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 675
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->this$0:Lorg/apache/lucene/index/DocTermOrds;

    # getter for: Lorg/apache/lucene/index/DocTermOrds;->indexIntervalBits:I
    invoke-static {v2}, Lorg/apache/lucene/index/DocTermOrds;->access$1(Lorg/apache/lucene/index/DocTermOrds;)I

    move-result v2

    shl-int v2, v1, v2

    int-to-long v2, v2

    iput-wide v2, p0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->ord:J

    .line 676
    invoke-direct {p0}, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->setTerm()Lorg/apache/lucene/util/BytesRef;

    .line 677
    sget-boolean v2, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->$assertionsDisabled:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    if-nez v2, :cond_2

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 678
    :cond_2
    sget-object v2, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    goto :goto_0

    .line 682
    .end local v0    # "seekStatus":Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    :cond_3
    neg-int v2, v1

    add-int/lit8 v1, v2, -0x1

    .line 684
    if-nez v1, :cond_6

    .line 686
    iget-object v2, p0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->termsEnum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/index/TermsEnum;->seekCeil(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    move-result-object v0

    .line 687
    .restart local v0    # "seekStatus":Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    sget-boolean v2, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->$assertionsDisabled:Z

    if-nez v2, :cond_4

    sget-object v2, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->NOT_FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    if-eq v0, v2, :cond_4

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 688
    :cond_4
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->ord:J

    .line 689
    invoke-direct {p0}, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->setTerm()Lorg/apache/lucene/util/BytesRef;

    .line 690
    sget-boolean v2, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->$assertionsDisabled:Z

    if-nez v2, :cond_5

    iget-object v2, p0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    if-nez v2, :cond_5

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 691
    :cond_5
    sget-object v2, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->NOT_FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    goto :goto_0

    .line 695
    .end local v0    # "seekStatus":Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    :cond_6
    add-int/lit8 v1, v1, -0x1

    .line 697
    iget-wide v2, p0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->ord:J

    iget-object v4, p0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->this$0:Lorg/apache/lucene/index/DocTermOrds;

    # getter for: Lorg/apache/lucene/index/DocTermOrds;->indexIntervalBits:I
    invoke-static {v4}, Lorg/apache/lucene/index/DocTermOrds;->access$1(Lorg/apache/lucene/index/DocTermOrds;)I

    move-result v4

    shr-long/2addr v2, v4

    int-to-long v4, v1

    cmp-long v2, v2, v4

    if-nez v2, :cond_7

    iget-object v2, p0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/util/BytesRef;->compareTo(Lorg/apache/lucene/util/BytesRef;)I

    move-result v2

    if-lez v2, :cond_a

    .line 702
    :cond_7
    iget-object v2, p0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->termsEnum:Lorg/apache/lucene/index/TermsEnum;

    iget-object v3, p0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->this$0:Lorg/apache/lucene/index/DocTermOrds;

    iget-object v3, v3, Lorg/apache/lucene/index/DocTermOrds;->indexedTermsArray:[Lorg/apache/lucene/util/BytesRef;

    aget-object v3, v3, v1

    invoke-virtual {v2, v3}, Lorg/apache/lucene/index/TermsEnum;->seekCeil(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    move-result-object v0

    .line 703
    .restart local v0    # "seekStatus":Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    sget-boolean v2, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->$assertionsDisabled:Z

    if-nez v2, :cond_8

    sget-object v2, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    if-eq v0, v2, :cond_8

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 704
    :cond_8
    iget-object v2, p0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->this$0:Lorg/apache/lucene/index/DocTermOrds;

    # getter for: Lorg/apache/lucene/index/DocTermOrds;->indexIntervalBits:I
    invoke-static {v2}, Lorg/apache/lucene/index/DocTermOrds;->access$1(Lorg/apache/lucene/index/DocTermOrds;)I

    move-result v2

    shl-int v2, v1, v2

    int-to-long v2, v2

    iput-wide v2, p0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->ord:J

    .line 705
    invoke-direct {p0}, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->setTerm()Lorg/apache/lucene/util/BytesRef;

    .line 706
    sget-boolean v2, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->$assertionsDisabled:Z

    if-nez v2, :cond_a

    iget-object v2, p0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    if-nez v2, :cond_a

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 710
    .end local v0    # "seekStatus":Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    :cond_9
    invoke-virtual {p0}, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->next()Lorg/apache/lucene/util/BytesRef;

    .line 709
    :cond_a
    iget-object v2, p0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/util/BytesRef;->compareTo(Lorg/apache/lucene/util/BytesRef;)I

    move-result v2

    if-ltz v2, :cond_9

    .line 713
    :cond_b
    iget-object v2, p0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    if-nez v2, :cond_c

    .line 714
    sget-object v2, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->END:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    goto/16 :goto_0

    .line 715
    :cond_c
    iget-object v2, p0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/util/BytesRef;->compareTo(Lorg/apache/lucene/util/BytesRef;)I

    move-result v2

    if-nez v2, :cond_d

    .line 716
    sget-object v2, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    goto/16 :goto_0

    .line 718
    :cond_d
    sget-object v2, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->NOT_FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    goto/16 :goto_0
.end method

.method public seekExact(J)V
    .locals 11
    .param p1, "targetOrd"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 724
    iget-object v5, p0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->this$0:Lorg/apache/lucene/index/DocTermOrds;

    iget v5, v5, Lorg/apache/lucene/index/DocTermOrds;->ordBase:I

    int-to-long v6, v5

    sub-long v6, p1, v6

    iget-wide v8, p0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->ord:J

    sub-long/2addr v6, v8

    long-to-int v2, v6

    .line 726
    .local v2, "delta":I
    if-ltz v2, :cond_0

    iget-object v5, p0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->this$0:Lorg/apache/lucene/index/DocTermOrds;

    # getter for: Lorg/apache/lucene/index/DocTermOrds;->indexInterval:I
    invoke-static {v5}, Lorg/apache/lucene/index/DocTermOrds;->access$0(Lorg/apache/lucene/index/DocTermOrds;)I

    move-result v5

    if-le v2, v5, :cond_3

    .line 727
    :cond_0
    iget-object v5, p0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->this$0:Lorg/apache/lucene/index/DocTermOrds;

    # getter for: Lorg/apache/lucene/index/DocTermOrds;->indexIntervalBits:I
    invoke-static {v5}, Lorg/apache/lucene/index/DocTermOrds;->access$1(Lorg/apache/lucene/index/DocTermOrds;)I

    move-result v5

    ushr-long v6, p1, v5

    long-to-int v3, v6

    .line 728
    .local v3, "idx":I
    iget-object v5, p0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->this$0:Lorg/apache/lucene/index/DocTermOrds;

    iget-object v5, v5, Lorg/apache/lucene/index/DocTermOrds;->indexedTermsArray:[Lorg/apache/lucene/util/BytesRef;

    aget-object v0, v5, v3

    .line 730
    .local v0, "base":Lorg/apache/lucene/util/BytesRef;
    iget-object v5, p0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->this$0:Lorg/apache/lucene/index/DocTermOrds;

    # getter for: Lorg/apache/lucene/index/DocTermOrds;->indexIntervalBits:I
    invoke-static {v5}, Lorg/apache/lucene/index/DocTermOrds;->access$1(Lorg/apache/lucene/index/DocTermOrds;)I

    move-result v5

    shl-int v5, v3, v5

    int-to-long v6, v5

    iput-wide v6, p0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->ord:J

    .line 731
    iget-wide v6, p0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->ord:J

    sub-long v6, p1, v6

    long-to-int v2, v6

    .line 732
    iget-object v5, p0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->termsEnum:Lorg/apache/lucene/index/TermsEnum;

    const/4 v6, 0x1

    invoke-virtual {v5, v0, v6}, Lorg/apache/lucene/index/TermsEnum;->seekCeil(Lorg/apache/lucene/util/BytesRef;Z)Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    move-result-object v4

    .line 733
    .local v4, "seekStatus":Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    sget-boolean v5, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->$assertionsDisabled:Z

    if-nez v5, :cond_3

    sget-object v5, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    if-eq v4, v5, :cond_3

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 739
    .end local v0    # "base":Lorg/apache/lucene/util/BytesRef;
    .end local v3    # "idx":I
    .end local v4    # "seekStatus":Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    :cond_1
    iget-object v5, p0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->termsEnum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v5}, Lorg/apache/lucene/index/TermsEnum;->next()Lorg/apache/lucene/util/BytesRef;

    move-result-object v1

    .line 740
    .local v1, "br":Lorg/apache/lucene/util/BytesRef;
    if-nez v1, :cond_2

    .line 741
    sget-boolean v5, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->$assertionsDisabled:Z

    if-nez v5, :cond_4

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 744
    :cond_2
    iget-wide v6, p0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->ord:J

    const-wide/16 v8, 0x1

    add-long/2addr v6, v8

    iput-wide v6, p0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->ord:J

    .line 738
    .end local v1    # "br":Lorg/apache/lucene/util/BytesRef;
    :cond_3
    add-int/lit8 v2, v2, -0x1

    if-gez v2, :cond_1

    .line 747
    invoke-direct {p0}, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->setTerm()Lorg/apache/lucene/util/BytesRef;

    .line 748
    sget-boolean v5, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->$assertionsDisabled:Z

    if-nez v5, :cond_4

    iget-object v5, p0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    if-nez v5, :cond_4

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 749
    :cond_4
    return-void
.end method

.method public term()Lorg/apache/lucene/util/BytesRef;
    .locals 1

    .prologue
    .line 631
    iget-object v0, p0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    return-object v0
.end method

.method public totalTermFreq()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 653
    iget-object v0, p0, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;->termsEnum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermsEnum;->totalTermFreq()J

    move-result-wide v0

    return-wide v0
.end method
