.class public Lorg/apache/lucene/index/MergePolicy$OneMerge;
.super Ljava/lang/Object;
.source "MergePolicy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/MergePolicy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "OneMerge"
.end annotation


# instance fields
.field aborted:Z

.field error:Ljava/lang/Throwable;

.field public volatile estimatedMergeBytes:J

.field info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

.field isExternal:Z

.field maxNumSegments:I

.field mergeGen:J

.field paused:Z

.field readers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/SegmentReader;",
            ">;"
        }
    .end annotation
.end field

.field registerDone:Z

.field public final segments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/SegmentInfoPerCommit;",
            ">;"
        }
    .end annotation
.end field

.field public final totalDocCount:I

.field volatile totalMergeBytes:J


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/SegmentInfoPerCommit;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 122
    .local p1, "segments":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfoPerCommit;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    const/4 v2, -0x1

    iput v2, p0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->maxNumSegments:I

    .line 123
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_0

    .line 124
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "segments must include at least one segment"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 126
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    .line 127
    const/4 v0, 0x0

    .line 128
    .local v0, "count":I
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 131
    iput v0, p0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->totalDocCount:I

    .line 132
    return-void

    .line 128
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .line 129
    .local v1, "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    iget-object v3, v1, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v3}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v3

    add-int/2addr v0, v3

    goto :goto_0
.end method


# virtual methods
.method declared-synchronized abort()V
    .locals 1

    .prologue
    .line 191
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->aborted:Z

    .line 192
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 193
    monitor-exit p0

    return-void

    .line 191
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized checkAborted(Lorg/apache/lucene/store/Directory;)V
    .locals 4
    .param p1, "dir"    # Lorg/apache/lucene/store/Directory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/MergePolicy$MergeAbortedException;
        }
    .end annotation

    .prologue
    .line 203
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->aborted:Z

    if-eqz v1, :cond_1

    .line 204
    new-instance v1, Lorg/apache/lucene/index/MergePolicy$MergeAbortedException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "merge is aborted: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segString(Lorg/apache/lucene/store/Directory;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/lucene/index/MergePolicy$MergeAbortedException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 203
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 211
    :cond_0
    const-wide/16 v2, 0x3e8

    :try_start_1
    invoke-virtual {p0, v2, v3}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 215
    :try_start_2
    iget-boolean v1, p0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->aborted:Z

    if-eqz v1, :cond_1

    .line 216
    new-instance v1, Lorg/apache/lucene/index/MergePolicy$MergeAbortedException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "merge is aborted: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segString(Lorg/apache/lucene/store/Directory;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/lucene/index/MergePolicy$MergeAbortedException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 212
    :catch_0
    move-exception v0

    .line 213
    .local v0, "ie":Ljava/lang/InterruptedException;
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 207
    .end local v0    # "ie":Ljava/lang/InterruptedException;
    :cond_1
    iget-boolean v1, p0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->paused:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-nez v1, :cond_0

    .line 219
    monitor-exit p0

    return-void
.end method

.method public getDocMap(Lorg/apache/lucene/index/MergeState;)Lorg/apache/lucene/index/MergePolicy$DocMap;
    .locals 1
    .param p1, "mergeState"    # Lorg/apache/lucene/index/MergeState;

    .prologue
    .line 167
    new-instance v0, Lorg/apache/lucene/index/MergePolicy$OneMerge$1;

    invoke-direct {v0, p0}, Lorg/apache/lucene/index/MergePolicy$OneMerge$1;-><init>(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    return-object v0
.end method

.method declared-synchronized getException()Ljava/lang/Throwable;
    .locals 1

    .prologue
    .line 184
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->error:Ljava/lang/Throwable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getMergeInfo()Lorg/apache/lucene/store/MergeInfo;
    .locals 6

    .prologue
    .line 283
    new-instance v0, Lorg/apache/lucene/store/MergeInfo;

    iget v1, p0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->totalDocCount:I

    iget-wide v2, p0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->estimatedMergeBytes:J

    iget-boolean v4, p0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->isExternal:Z

    iget v5, p0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->maxNumSegments:I

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/store/MergeInfo;-><init>(IJZI)V

    return-object v0
.end method

.method public getMergeReaders()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/AtomicReader;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 141
    iget-object v2, p0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->readers:Ljava/util/List;

    if-nez v2, :cond_0

    .line 142
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "IndexWriter has not initialized readers from the segment infos yet"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 144
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->readers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 145
    .local v1, "readers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/AtomicReader;>;"
    iget-object v2, p0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->readers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 150
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    return-object v2

    .line 145
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/AtomicReader;

    .line 146
    .local v0, "reader":Lorg/apache/lucene/index/AtomicReader;
    invoke-virtual {v0}, Lorg/apache/lucene/index/AtomicReader;->numDocs()I

    move-result v3

    if-lez v3, :cond_1

    .line 147
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public declared-synchronized getPause()Z
    .locals 1

    .prologue
    .line 236
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->paused:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized isAborted()Z
    .locals 1

    .prologue
    .line 197
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->aborted:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public segString(Lorg/apache/lucene/store/Directory;)Ljava/lang/String;
    .locals 5
    .param p1, "dir"    # Lorg/apache/lucene/store/Directory;

    .prologue
    .line 242
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 243
    .local v0, "b":Ljava/lang/StringBuilder;
    iget-object v3, p0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    .line 244
    .local v2, "numSegments":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v2, :cond_3

    .line 248
    iget-object v3, p0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    if-eqz v3, :cond_0

    .line 249
    const-string v3, " into "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    iget-object v4, v4, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v4, v4, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 251
    :cond_0
    iget v3, p0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->maxNumSegments:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    .line 252
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, " [maxNumSegments="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->maxNumSegments:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 253
    :cond_1
    iget-boolean v3, p0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->aborted:Z

    if-eqz v3, :cond_2

    .line 254
    const-string v3, " [ABORTED]"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 256
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 245
    :cond_3
    if-lez v1, :cond_4

    const/16 v3, 0x20

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 246
    :cond_4
    iget-object v3, p0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    const/4 v4, 0x0

    invoke-virtual {v3, p1, v4}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->toString(Lorg/apache/lucene/store/Directory;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 244
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method declared-synchronized setException(Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "error"    # Ljava/lang/Throwable;

    .prologue
    .line 178
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->error:Ljava/lang/Throwable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 179
    monitor-exit p0

    return-void

    .line 178
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setInfo(Lorg/apache/lucene/index/SegmentInfoPerCommit;)V
    .locals 0
    .param p1, "info"    # Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .prologue
    .line 158
    iput-object p1, p0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .line 159
    return-void
.end method

.method public declared-synchronized setPause(Z)V
    .locals 1
    .param p1, "paused"    # Z

    .prologue
    .line 225
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->paused:Z

    .line 226
    if-nez p1, :cond_0

    .line 228
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 230
    :cond_0
    monitor-exit p0

    return-void

    .line 225
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public totalBytesSize()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 266
    iget-wide v0, p0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->totalMergeBytes:J

    return-wide v0
.end method

.method public totalNumDocs()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 274
    const/4 v1, 0x0

    .line 275
    .local v1, "total":I
    iget-object v2, p0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 278
    return v1

    .line 275
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .line 276
    .local v0, "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    iget-object v3, v0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v3}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v3

    add-int/2addr v1, v3

    goto :goto_0
.end method
