.class final Lorg/apache/lucene/index/FreqProxTermsWriterPerField;
.super Lorg/apache/lucene/index/TermsHashConsumerPerField;
.source "FreqProxTermsWriterPerField.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/index/TermsHashConsumerPerField;",
        "Ljava/lang/Comparable",
        "<",
        "Lorg/apache/lucene/index/FreqProxTermsWriterPerField;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

.field final fieldInfo:Lorg/apache/lucene/index/FieldInfo;

.field final fieldState:Lorg/apache/lucene/index/FieldInvertState;

.field private hasFreq:Z

.field private hasOffsets:Z

.field hasPayloads:Z

.field private hasProx:Z

.field offsetAttribute:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

.field final parent:Lorg/apache/lucene/index/FreqProxTermsWriter;

.field payload:Lorg/apache/lucene/util/BytesRef;

.field payloadAttribute:Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

.field final termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-class v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/index/TermsHashPerField;Lorg/apache/lucene/index/FreqProxTermsWriter;Lorg/apache/lucene/index/FieldInfo;)V
    .locals 1
    .param p1, "termsHashPerField"    # Lorg/apache/lucene/index/TermsHashPerField;
    .param p2, "parent"    # Lorg/apache/lucene/index/FreqProxTermsWriter;
    .param p3, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;

    .prologue
    .line 51
    invoke-direct {p0}, Lorg/apache/lucene/index/TermsHashConsumerPerField;-><init>()V

    .line 52
    iput-object p1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    .line 53
    iput-object p2, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->parent:Lorg/apache/lucene/index/FreqProxTermsWriter;

    .line 54
    iput-object p3, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    .line 55
    iget-object v0, p1, Lorg/apache/lucene/index/TermsHashPerField;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    iput-object v0, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    .line 56
    iget-object v0, p1, Lorg/apache/lucene/index/TermsHashPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iput-object v0, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    .line 57
    invoke-virtual {p3}, Lorg/apache/lucene/index/FieldInfo;->getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->setIndexOptions(Lorg/apache/lucene/index/FieldInfo$IndexOptions;)V

    .line 58
    return-void
.end method

.method private setIndexOptions(Lorg/apache/lucene/index/FieldInfo$IndexOptions;)V
    .locals 3
    .param p1, "indexOptions"    # Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 95
    if-nez p1, :cond_0

    .line 97
    iput-boolean v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->hasOffsets:Z

    iput-boolean v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->hasProx:Z

    iput-boolean v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->hasFreq:Z

    .line 103
    :goto_0
    return-void

    .line 99
    :cond_0
    sget-object v0, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {p1, v0}, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-ltz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->hasFreq:Z

    .line 100
    sget-object v0, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {p1, v0}, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-ltz v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->hasProx:Z

    .line 101
    sget-object v0, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {p1, v0}, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-ltz v0, :cond_3

    :goto_3
    iput-boolean v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->hasOffsets:Z

    goto :goto_0

    :cond_1
    move v0, v2

    .line 99
    goto :goto_1

    :cond_2
    move v0, v2

    .line 100
    goto :goto_2

    :cond_3
    move v1, v2

    .line 101
    goto :goto_3
.end method


# virtual methods
.method public abort()V
    .locals 0

    .prologue
    .line 322
    return-void
.end method

.method addTerm(I)V
    .locals 5
    .param p1, "termID"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 194
    sget-boolean v1, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    const-string v2, "FreqProxTermsWriterPerField.addTerm start"

    invoke-virtual {v1, v2}, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;->testPoint(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 196
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    iget-object v0, v1, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    check-cast v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;

    .line 198
    .local v0, "postings":Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;
    sget-boolean v1, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->hasFreq:Z

    if-eqz v1, :cond_1

    iget-object v1, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->termFreqs:[I

    aget v1, v1, p1

    if-gtz v1, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 200
    :cond_1
    iget-boolean v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->hasFreq:Z

    if-nez v1, :cond_5

    .line 201
    sget-boolean v1, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->$assertionsDisabled:Z

    if-nez v1, :cond_2

    iget-object v1, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->termFreqs:[I

    if-eqz v1, :cond_2

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 202
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    iget v1, v1, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;->docID:I

    iget-object v2, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastDocIDs:[I

    aget v2, v2, p1

    if-eq v1, v2, :cond_4

    .line 203
    sget-boolean v1, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->$assertionsDisabled:Z

    if-nez v1, :cond_3

    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    iget v1, v1, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;->docID:I

    iget-object v2, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastDocIDs:[I

    aget v2, v2, p1

    if-gt v1, v2, :cond_3

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 204
    :cond_3
    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    iget-object v2, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastDocCodes:[I

    aget v2, v2, p1

    invoke-virtual {v1, v4, v2}, Lorg/apache/lucene/index/TermsHashPerField;->writeVInt(II)V

    .line 205
    iget-object v1, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastDocCodes:[I

    iget-object v2, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    iget v2, v2, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;->docID:I

    iget-object v3, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastDocIDs:[I

    aget v3, v3, p1

    sub-int/2addr v2, v3

    aput v2, v1, p1

    .line 206
    iget-object v1, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastDocIDs:[I

    iget-object v2, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    iget v2, v2, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;->docID:I

    aput v2, v1, p1

    .line 207
    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iget v2, v1, Lorg/apache/lucene/index/FieldInvertState;->uniqueTermCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lorg/apache/lucene/index/FieldInvertState;->uniqueTermCount:I

    .line 245
    :cond_4
    :goto_0
    return-void

    .line 209
    :cond_5
    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    iget v1, v1, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;->docID:I

    iget-object v2, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastDocIDs:[I

    aget v2, v2, p1

    if-eq v1, v2, :cond_a

    .line 210
    sget-boolean v1, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->$assertionsDisabled:Z

    if-nez v1, :cond_6

    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    iget v1, v1, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;->docID:I

    iget-object v2, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastDocIDs:[I

    aget v2, v2, p1

    if-gt v1, v2, :cond_6

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "id: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    iget v3, v3, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;->docID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " postings ID: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastDocIDs:[I

    aget v3, v3, p1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " termID: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 216
    :cond_6
    iget-object v1, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->termFreqs:[I

    aget v1, v1, p1

    if-ne v3, v1, :cond_8

    .line 217
    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    iget-object v2, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastDocCodes:[I

    aget v2, v2, p1

    or-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v4, v2}, Lorg/apache/lucene/index/TermsHashPerField;->writeVInt(II)V

    .line 222
    :goto_1
    iget-object v1, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->termFreqs:[I

    aput v3, v1, p1

    .line 223
    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iget-object v2, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iget v2, v2, Lorg/apache/lucene/index/FieldInvertState;->maxTermFrequency:I

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    iput v2, v1, Lorg/apache/lucene/index/FieldInvertState;->maxTermFrequency:I

    .line 224
    iget-object v1, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastDocCodes:[I

    iget-object v2, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    iget v2, v2, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;->docID:I

    iget-object v3, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastDocIDs:[I

    aget v3, v3, p1

    sub-int/2addr v2, v3

    shl-int/lit8 v2, v2, 0x1

    aput v2, v1, p1

    .line 225
    iget-object v1, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastDocIDs:[I

    iget-object v2, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    iget v2, v2, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;->docID:I

    aput v2, v1, p1

    .line 226
    iget-boolean v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->hasProx:Z

    if-eqz v1, :cond_9

    .line 227
    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iget v1, v1, Lorg/apache/lucene/index/FieldInvertState;->position:I

    invoke-virtual {p0, p1, v1}, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->writeProx(II)V

    .line 228
    iget-boolean v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->hasOffsets:Z

    if-eqz v1, :cond_7

    .line 229
    iget-object v1, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastOffsets:[I

    aput v4, v1, p1

    .line 230
    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iget v1, v1, Lorg/apache/lucene/index/FieldInvertState;->offset:I

    invoke-virtual {p0, p1, v1}, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->writeOffsets(II)V

    .line 235
    :cond_7
    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iget v2, v1, Lorg/apache/lucene/index/FieldInvertState;->uniqueTermCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lorg/apache/lucene/index/FieldInvertState;->uniqueTermCount:I

    goto/16 :goto_0

    .line 219
    :cond_8
    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    iget-object v2, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastDocCodes:[I

    aget v2, v2, p1

    invoke-virtual {v1, v4, v2}, Lorg/apache/lucene/index/TermsHashPerField;->writeVInt(II)V

    .line 220
    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    iget-object v2, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->termFreqs:[I

    aget v2, v2, p1

    invoke-virtual {v1, v4, v2}, Lorg/apache/lucene/index/TermsHashPerField;->writeVInt(II)V

    goto :goto_1

    .line 233
    :cond_9
    sget-boolean v1, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->$assertionsDisabled:Z

    if-nez v1, :cond_7

    iget-boolean v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->hasOffsets:Z

    if-eqz v1, :cond_7

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 237
    :cond_a
    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iget-object v2, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iget v2, v2, Lorg/apache/lucene/index/FieldInvertState;->maxTermFrequency:I

    iget-object v3, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->termFreqs:[I

    aget v4, v3, p1

    add-int/lit8 v4, v4, 0x1

    aput v4, v3, p1

    invoke-static {v2, v4}, Ljava/lang/Math;->max(II)I

    move-result v2

    iput v2, v1, Lorg/apache/lucene/index/FieldInvertState;->maxTermFrequency:I

    .line 238
    iget-boolean v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->hasProx:Z

    if-eqz v1, :cond_b

    .line 239
    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iget v1, v1, Lorg/apache/lucene/index/FieldInvertState;->position:I

    iget-object v2, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastPositions:[I

    aget v2, v2, p1

    sub-int/2addr v1, v2

    invoke-virtual {p0, p1, v1}, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->writeProx(II)V

    .line 241
    :cond_b
    iget-boolean v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->hasOffsets:Z

    if-eqz v1, :cond_4

    .line 242
    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iget v1, v1, Lorg/apache/lucene/index/FieldInvertState;->offset:I

    invoke-virtual {p0, p1, v1}, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->writeOffsets(II)V

    goto/16 :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;

    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->compareTo(Lorg/apache/lucene/index/FreqProxTermsWriterPerField;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lorg/apache/lucene/index/FreqProxTermsWriterPerField;)I
    .locals 2
    .param p1, "other"    # Lorg/apache/lucene/index/FreqProxTermsWriterPerField;

    .prologue
    .line 83
    iget-object v0, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    iget-object v0, v0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    iget-object v1, p1, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    iget-object v1, v1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method createPostingsArray(I)Lorg/apache/lucene/index/ParallelPostingsArray;
    .locals 4
    .param p1, "size"    # I

    .prologue
    .line 249
    new-instance v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;

    iget-boolean v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->hasFreq:Z

    iget-boolean v2, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->hasProx:Z

    iget-boolean v3, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->hasOffsets:Z

    invoke-direct {v0, p1, v1, v2, v3}, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;-><init>(IZZZ)V

    return-object v0
.end method

.method finish()V
    .locals 1

    .prologue
    .line 71
    iget-boolean v0, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->hasPayloads:Z

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfo;->setStorePayloads()V

    .line 74
    :cond_0
    return-void
.end method

.method flush(Ljava/lang/String;Lorg/apache/lucene/codecs/FieldsConsumer;Lorg/apache/lucene/index/SegmentWriteState;)V
    .locals 50
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "consumer"    # Lorg/apache/lucene/codecs/FieldsConsumer;
    .param p3, "state"    # Lorg/apache/lucene/index/SegmentWriteState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 332
    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    invoke-virtual {v8}, Lorg/apache/lucene/index/FieldInfo;->isIndexed()Z

    move-result v8

    if-nez v8, :cond_0

    .line 554
    :goto_0
    return-void

    .line 336
    :cond_0
    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    move-object/from16 v0, p2

    invoke-virtual {v0, v8}, Lorg/apache/lucene/codecs/FieldsConsumer;->addField(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/codecs/TermsConsumer;

    move-result-object v7

    .line 337
    .local v7, "termsConsumer":Lorg/apache/lucene/codecs/TermsConsumer;
    invoke-virtual {v7}, Lorg/apache/lucene/codecs/TermsConsumer;->getComparator()Ljava/util/Comparator;

    move-result-object v35

    .line 347
    .local v35, "termComp":Ljava/util/Comparator;, "Ljava/util/Comparator<Lorg/apache/lucene/util/BytesRef;>;"
    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    invoke-virtual {v8}, Lorg/apache/lucene/index/FieldInfo;->getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v13

    .line 348
    .local v13, "currentFieldIndexOptions":Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    sget-boolean v8, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->$assertionsDisabled:Z

    if-nez v8, :cond_1

    if-nez v13, :cond_1

    new-instance v8, Ljava/lang/AssertionError;

    invoke-direct {v8}, Ljava/lang/AssertionError;-><init>()V

    throw v8

    .line 350
    :cond_1
    sget-object v8, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {v13, v8}, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->compareTo(Ljava/lang/Enum;)I

    move-result v8

    if-ltz v8, :cond_2

    const/16 v49, 0x1

    .line 351
    .local v49, "writeTermFreq":Z
    :goto_1
    sget-object v8, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {v13, v8}, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->compareTo(Ljava/lang/Enum;)I

    move-result v8

    if-ltz v8, :cond_3

    const/16 v48, 0x1

    .line 352
    .local v48, "writePositions":Z
    :goto_2
    sget-object v8, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {v13, v8}, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->compareTo(Ljava/lang/Enum;)I

    move-result v8

    if-ltz v8, :cond_4

    const/16 v47, 0x1

    .line 354
    .local v47, "writeOffsets":Z
    :goto_3
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->hasFreq:Z

    move/from16 v32, v0

    .line 355
    .local v32, "readTermFreq":Z
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->hasProx:Z

    move/from16 v31, v0

    .line 356
    .local v31, "readPositions":Z
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->hasOffsets:Z

    move/from16 v30, v0

    .line 361
    .local v30, "readOffsets":Z
    sget-boolean v8, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->$assertionsDisabled:Z

    if-nez v8, :cond_5

    if-eqz v49, :cond_5

    if-nez v32, :cond_5

    new-instance v8, Ljava/lang/AssertionError;

    invoke-direct {v8}, Ljava/lang/AssertionError;-><init>()V

    throw v8

    .line 350
    .end local v30    # "readOffsets":Z
    .end local v31    # "readPositions":Z
    .end local v32    # "readTermFreq":Z
    .end local v47    # "writeOffsets":Z
    .end local v48    # "writePositions":Z
    .end local v49    # "writeTermFreq":Z
    :cond_2
    const/16 v49, 0x0

    goto :goto_1

    .line 351
    .restart local v49    # "writeTermFreq":Z
    :cond_3
    const/16 v48, 0x0

    goto :goto_2

    .line 352
    .restart local v48    # "writePositions":Z
    :cond_4
    const/16 v47, 0x0

    goto :goto_3

    .line 362
    .restart local v30    # "readOffsets":Z
    .restart local v31    # "readPositions":Z
    .restart local v32    # "readTermFreq":Z
    .restart local v47    # "writeOffsets":Z
    :cond_5
    sget-boolean v8, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->$assertionsDisabled:Z

    if-nez v8, :cond_6

    if-eqz v48, :cond_6

    if-nez v31, :cond_6

    new-instance v8, Ljava/lang/AssertionError;

    invoke-direct {v8}, Ljava/lang/AssertionError;-><init>()V

    throw v8

    .line 363
    :cond_6
    sget-boolean v8, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->$assertionsDisabled:Z

    if-nez v8, :cond_7

    if-eqz v47, :cond_7

    if-nez v30, :cond_7

    new-instance v8, Ljava/lang/AssertionError;

    invoke-direct {v8}, Ljava/lang/AssertionError;-><init>()V

    throw v8

    .line 365
    :cond_7
    sget-boolean v8, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->$assertionsDisabled:Z

    if-nez v8, :cond_8

    if-eqz v47, :cond_8

    if-nez v48, :cond_8

    new-instance v8, Ljava/lang/AssertionError;

    invoke-direct {v8}, Ljava/lang/AssertionError;-><init>()V

    throw v8

    .line 368
    :cond_8
    move-object/from16 v0, p3

    iget-object v8, v0, Lorg/apache/lucene/index/SegmentWriteState;->segDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    if-eqz v8, :cond_9

    move-object/from16 v0, p3

    iget-object v8, v0, Lorg/apache/lucene/index/SegmentWriteState;->segDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    iget-object v8, v8, Lorg/apache/lucene/index/BufferedDeletes;->terms:Ljava/util/Map;

    invoke-interface {v8}, Ljava/util/Map;->size()I

    move-result v8

    if-lez v8, :cond_9

    .line 369
    move-object/from16 v0, p3

    iget-object v8, v0, Lorg/apache/lucene/index/SegmentWriteState;->segDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    iget-object v0, v8, Lorg/apache/lucene/index/BufferedDeletes;->terms:Ljava/util/Map;

    move-object/from16 v33, v0

    .line 374
    .local v33, "segDeletes":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/index/Term;Ljava/lang/Integer;>;"
    :goto_4
    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    move-object/from16 v0, v35

    invoke-virtual {v8, v0}, Lorg/apache/lucene/index/TermsHashPerField;->sortPostings(Ljava/util/Comparator;)[I

    move-result-object v40

    .line 375
    .local v40, "termIDs":[I
    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    iget-object v8, v8, Lorg/apache/lucene/index/TermsHashPerField;->bytesHash:Lorg/apache/lucene/util/BytesRefHash;

    invoke-virtual {v8}, Lorg/apache/lucene/util/BytesRefHash;->size()I

    move-result v22

    .line 376
    .local v22, "numTerms":I
    new-instance v41, Lorg/apache/lucene/util/BytesRef;

    invoke-direct/range {v41 .. v41}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    .line 377
    .local v41, "text":Lorg/apache/lucene/util/BytesRef;
    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    iget-object v0, v8, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    move-object/from16 v26, v0

    check-cast v26, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;

    .line 378
    .local v26, "postings":Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;
    new-instance v19, Lorg/apache/lucene/index/ByteSliceReader;

    invoke-direct/range {v19 .. v19}, Lorg/apache/lucene/index/ByteSliceReader;-><init>()V

    .line 379
    .local v19, "freq":Lorg/apache/lucene/index/ByteSliceReader;
    new-instance v29, Lorg/apache/lucene/index/ByteSliceReader;

    invoke-direct/range {v29 .. v29}, Lorg/apache/lucene/index/ByteSliceReader;-><init>()V

    .line 381
    .local v29, "prox":Lorg/apache/lucene/index/ByteSliceReader;
    new-instance v46, Lorg/apache/lucene/util/FixedBitSet;

    move-object/from16 v0, p3

    iget-object v8, v0, Lorg/apache/lucene/index/SegmentWriteState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v8}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v8

    move-object/from16 v0, v46

    invoke-direct {v0, v8}, Lorg/apache/lucene/util/FixedBitSet;-><init>(I)V

    .line 382
    .local v46, "visitedDocs":Lorg/apache/lucene/util/FixedBitSet;
    const-wide/16 v36, 0x0

    .line 383
    .local v36, "sumTotalTermFreq":J
    const-wide/16 v10, 0x0

    .line 385
    .local v10, "sumDocFreq":J
    new-instance v28, Lorg/apache/lucene/index/Term;

    move-object/from16 v0, v28

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;)V

    .line 386
    .local v28, "protoTerm":Lorg/apache/lucene/index/Term;
    const/16 v20, 0x0

    .local v20, "i":I
    :goto_5
    move/from16 v0, v20

    move/from16 v1, v22

    if-lt v0, v1, :cond_a

    .line 553
    if-eqz v49, :cond_26

    move-wide/from16 v8, v36

    :goto_6
    invoke-virtual/range {v46 .. v46}, Lorg/apache/lucene/util/FixedBitSet;->cardinality()I

    move-result v12

    invoke-virtual/range {v7 .. v12}, Lorg/apache/lucene/codecs/TermsConsumer;->finish(JJI)V

    goto/16 :goto_0

    .line 371
    .end local v10    # "sumDocFreq":J
    .end local v19    # "freq":Lorg/apache/lucene/index/ByteSliceReader;
    .end local v20    # "i":I
    .end local v22    # "numTerms":I
    .end local v26    # "postings":Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;
    .end local v28    # "protoTerm":Lorg/apache/lucene/index/Term;
    .end local v29    # "prox":Lorg/apache/lucene/index/ByteSliceReader;
    .end local v33    # "segDeletes":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/index/Term;Ljava/lang/Integer;>;"
    .end local v36    # "sumTotalTermFreq":J
    .end local v40    # "termIDs":[I
    .end local v41    # "text":Lorg/apache/lucene/util/BytesRef;
    .end local v46    # "visitedDocs":Lorg/apache/lucene/util/FixedBitSet;
    :cond_9
    const/16 v33, 0x0

    .restart local v33    # "segDeletes":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/index/Term;Ljava/lang/Integer;>;"
    goto :goto_4

    .line 387
    .restart local v10    # "sumDocFreq":J
    .restart local v19    # "freq":Lorg/apache/lucene/index/ByteSliceReader;
    .restart local v20    # "i":I
    .restart local v22    # "numTerms":I
    .restart local v26    # "postings":Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;
    .restart local v28    # "protoTerm":Lorg/apache/lucene/index/Term;
    .restart local v29    # "prox":Lorg/apache/lucene/index/ByteSliceReader;
    .restart local v36    # "sumTotalTermFreq":J
    .restart local v40    # "termIDs":[I
    .restart local v41    # "text":Lorg/apache/lucene/util/BytesRef;
    .restart local v46    # "visitedDocs":Lorg/apache/lucene/util/FixedBitSet;
    :cond_a
    aget v39, v40, v20

    .line 390
    .local v39, "termID":I
    move-object/from16 v0, v26

    iget-object v8, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->textStarts:[I

    aget v42, v8, v39

    .line 391
    .local v42, "textStart":I
    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    iget-object v8, v8, Lorg/apache/lucene/index/TermsHashPerField;->bytePool:Lorg/apache/lucene/util/ByteBlockPool;

    move-object/from16 v0, v41

    move/from16 v1, v42

    invoke-virtual {v8, v0, v1}, Lorg/apache/lucene/util/ByteBlockPool;->setBytesRef(Lorg/apache/lucene/util/BytesRef;I)V

    .line 393
    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    const/4 v9, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v39

    invoke-virtual {v8, v0, v1, v9}, Lorg/apache/lucene/index/TermsHashPerField;->initReader(Lorg/apache/lucene/index/ByteSliceReader;II)V

    .line 394
    if-nez v31, :cond_b

    if-eqz v30, :cond_c

    .line 395
    :cond_b
    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    const/4 v9, 0x1

    move-object/from16 v0, v29

    move/from16 v1, v39

    invoke-virtual {v8, v0, v1, v9}, Lorg/apache/lucene/index/TermsHashPerField;->initReader(Lorg/apache/lucene/index/ByteSliceReader;II)V

    .line 404
    :cond_c
    move-object/from16 v0, v41

    invoke-virtual {v7, v0}, Lorg/apache/lucene/codecs/TermsConsumer;->startTerm(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/codecs/PostingsConsumer;

    move-result-object v27

    .line 407
    .local v27, "postingsConsumer":Lorg/apache/lucene/codecs/PostingsConsumer;
    if-eqz v33, :cond_f

    .line 408
    move-object/from16 v0, v41

    move-object/from16 v1, v28

    iput-object v0, v1, Lorg/apache/lucene/index/Term;->bytes:Lorg/apache/lucene/util/BytesRef;

    .line 409
    move-object/from16 v0, v33

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Integer;

    .line 410
    .local v17, "docIDUpto":Ljava/lang/Integer;
    if-eqz v17, :cond_e

    .line 411
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v14

    .line 422
    .end local v17    # "docIDUpto":Ljava/lang/Integer;
    .local v14, "delDocLimit":I
    :goto_7
    const/4 v15, 0x0

    .line 423
    .local v15, "docFreq":I
    const-wide/16 v44, 0x0

    .line 424
    .local v44, "totTF":J
    const/16 v16, 0x0

    .line 429
    .local v16, "docID":I
    :goto_8
    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/index/ByteSliceReader;->eof()Z

    move-result v8

    if-eqz v8, :cond_11

    .line 430
    move-object/from16 v0, v26

    iget-object v8, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastDocCodes:[I

    aget v8, v8, v39

    const/4 v9, -0x1

    if-eq v8, v9, :cond_24

    .line 432
    move-object/from16 v0, v26

    iget-object v8, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastDocIDs:[I

    aget v16, v8, v39

    .line 433
    if-eqz v32, :cond_10

    .line 434
    move-object/from16 v0, v26

    iget-object v8, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->termFreqs:[I

    aget v38, v8, v39

    .line 438
    .local v38, "termFreq":I
    :goto_9
    move-object/from16 v0, v26

    iget-object v8, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastDocCodes:[I

    const/4 v9, -0x1

    aput v9, v8, v39

    .line 460
    :cond_d
    add-int/lit8 v15, v15, 0x1

    .line 461
    sget-boolean v8, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->$assertionsDisabled:Z

    if-nez v8, :cond_14

    move-object/from16 v0, p3

    iget-object v8, v0, Lorg/apache/lucene/index/SegmentWriteState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v8}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v8

    move/from16 v0, v16

    if-lt v0, v8, :cond_14

    new-instance v8, Ljava/lang/AssertionError;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v12, "doc="

    invoke-direct {v9, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v16

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v12, " maxDoc="

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, p3

    iget-object v12, v0, Lorg/apache/lucene/index/SegmentWriteState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v12}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v12

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v8

    .line 413
    .end local v14    # "delDocLimit":I
    .end local v15    # "docFreq":I
    .end local v16    # "docID":I
    .end local v38    # "termFreq":I
    .end local v44    # "totTF":J
    .restart local v17    # "docIDUpto":Ljava/lang/Integer;
    :cond_e
    const/4 v14, 0x0

    .line 415
    .restart local v14    # "delDocLimit":I
    goto :goto_7

    .line 416
    .end local v14    # "delDocLimit":I
    .end local v17    # "docIDUpto":Ljava/lang/Integer;
    :cond_f
    const/4 v14, 0x0

    .restart local v14    # "delDocLimit":I
    goto :goto_7

    .line 436
    .restart local v15    # "docFreq":I
    .restart local v16    # "docID":I
    .restart local v44    # "totTF":J
    :cond_10
    const/16 v38, -0x1

    .restart local v38    # "termFreq":I
    goto :goto_9

    .line 444
    .end local v38    # "termFreq":I
    :cond_11
    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/index/ByteSliceReader;->readVInt()I

    move-result v6

    .line 445
    .local v6, "code":I
    if-nez v32, :cond_12

    .line 446
    add-int v16, v16, v6

    .line 447
    const/16 v38, -0x1

    .line 457
    .restart local v38    # "termFreq":I
    :goto_a
    sget-boolean v8, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->$assertionsDisabled:Z

    if-nez v8, :cond_d

    move-object/from16 v0, v26

    iget-object v8, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastDocIDs:[I

    aget v8, v8, v39

    move/from16 v0, v16

    if-ne v0, v8, :cond_d

    new-instance v8, Ljava/lang/AssertionError;

    invoke-direct {v8}, Ljava/lang/AssertionError;-><init>()V

    throw v8

    .line 449
    .end local v38    # "termFreq":I
    :cond_12
    ushr-int/lit8 v8, v6, 0x1

    add-int v16, v16, v8

    .line 450
    and-int/lit8 v8, v6, 0x1

    if-eqz v8, :cond_13

    .line 451
    const/16 v38, 0x1

    .line 452
    .restart local v38    # "termFreq":I
    goto :goto_a

    .line 453
    .end local v38    # "termFreq":I
    :cond_13
    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/index/ByteSliceReader;->readVInt()I

    move-result v38

    .restart local v38    # "termFreq":I
    goto :goto_a

    .line 474
    .end local v6    # "code":I
    :cond_14
    move-object/from16 v0, v46

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/FixedBitSet;->set(I)V

    .line 475
    if-eqz v49, :cond_19

    move/from16 v8, v38

    :goto_b
    move-object/from16 v0, v27

    move/from16 v1, v16

    invoke-virtual {v0, v1, v8}, Lorg/apache/lucene/codecs/PostingsConsumer;->startDoc(II)V

    .line 476
    move/from16 v0, v16

    if-ge v0, v14, :cond_16

    .line 482
    move-object/from16 v0, p3

    iget-object v8, v0, Lorg/apache/lucene/index/SegmentWriteState;->liveDocs:Lorg/apache/lucene/util/MutableBits;

    if-nez v8, :cond_15

    .line 483
    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    iget-object v8, v8, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;->docWriter:Lorg/apache/lucene/index/DocumentsWriterPerThread;

    iget-object v8, v8, Lorg/apache/lucene/index/DocumentsWriterPerThread;->codec:Lorg/apache/lucene/codecs/Codec;

    invoke-virtual {v8}, Lorg/apache/lucene/codecs/Codec;->liveDocsFormat()Lorg/apache/lucene/codecs/LiveDocsFormat;

    move-result-object v8

    move-object/from16 v0, p3

    iget-object v9, v0, Lorg/apache/lucene/index/SegmentWriteState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v9}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v9

    invoke-virtual {v8, v9}, Lorg/apache/lucene/codecs/LiveDocsFormat;->newLiveDocs(I)Lorg/apache/lucene/util/MutableBits;

    move-result-object v8

    move-object/from16 v0, p3

    iput-object v8, v0, Lorg/apache/lucene/index/SegmentWriteState;->liveDocs:Lorg/apache/lucene/util/MutableBits;

    .line 485
    :cond_15
    move-object/from16 v0, p3

    iget-object v8, v0, Lorg/apache/lucene/index/SegmentWriteState;->liveDocs:Lorg/apache/lucene/util/MutableBits;

    move/from16 v0, v16

    invoke-interface {v8, v0}, Lorg/apache/lucene/util/MutableBits;->get(I)Z

    move-result v8

    if-eqz v8, :cond_16

    .line 486
    move-object/from16 v0, p3

    iget v8, v0, Lorg/apache/lucene/index/SegmentWriteState;->delCountOnFlush:I

    add-int/lit8 v8, v8, 0x1

    move-object/from16 v0, p3

    iput v8, v0, Lorg/apache/lucene/index/SegmentWriteState;->delCountOnFlush:I

    .line 487
    move-object/from16 v0, p3

    iget-object v8, v0, Lorg/apache/lucene/index/SegmentWriteState;->liveDocs:Lorg/apache/lucene/util/MutableBits;

    move/from16 v0, v16

    invoke-interface {v8, v0}, Lorg/apache/lucene/util/MutableBits;->clear(I)V

    .line 491
    :cond_16
    move/from16 v0, v38

    int-to-long v8, v0

    add-long v44, v44, v8

    .line 497
    if-nez v31, :cond_17

    if-eqz v30, :cond_18

    .line 499
    :cond_17
    const/16 v25, 0x0

    .line 500
    .local v25, "position":I
    const/16 v23, 0x0

    .line 501
    .local v23, "offset":I
    const/16 v21, 0x0

    .local v21, "j":I
    :goto_c
    move/from16 v0, v21

    move/from16 v1, v38

    if-lt v0, v1, :cond_1a

    .line 546
    .end local v21    # "j":I
    .end local v23    # "offset":I
    .end local v25    # "position":I
    :cond_18
    invoke-virtual/range {v27 .. v27}, Lorg/apache/lucene/codecs/PostingsConsumer;->finishDoc()V

    goto/16 :goto_8

    .line 475
    :cond_19
    const/4 v8, -0x1

    goto :goto_b

    .line 504
    .restart local v21    # "j":I
    .restart local v23    # "offset":I
    .restart local v25    # "position":I
    :cond_1a
    if-eqz v31, :cond_21

    .line 505
    invoke-virtual/range {v29 .. v29}, Lorg/apache/lucene/index/ByteSliceReader;->readVInt()I

    move-result v6

    .line 506
    .restart local v6    # "code":I
    ushr-int/lit8 v8, v6, 0x1

    add-int v25, v25, v8

    .line 508
    and-int/lit8 v8, v6, 0x1

    if-eqz v8, :cond_1e

    .line 511
    invoke-virtual/range {v29 .. v29}, Lorg/apache/lucene/index/ByteSliceReader;->readVInt()I

    move-result v24

    .line 513
    .local v24, "payloadLength":I
    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->payload:Lorg/apache/lucene/util/BytesRef;

    if-nez v8, :cond_1d

    .line 514
    new-instance v8, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v8}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    move-object/from16 v0, p0

    iput-object v8, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->payload:Lorg/apache/lucene/util/BytesRef;

    .line 515
    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->payload:Lorg/apache/lucene/util/BytesRef;

    move/from16 v0, v24

    new-array v9, v0, [B

    iput-object v9, v8, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 520
    :cond_1b
    :goto_d
    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->payload:Lorg/apache/lucene/util/BytesRef;

    iget-object v8, v8, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    const/4 v9, 0x0

    move-object/from16 v0, v29

    move/from16 v1, v24

    invoke-virtual {v0, v8, v9, v1}, Lorg/apache/lucene/index/ByteSliceReader;->readBytes([BII)V

    .line 521
    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->payload:Lorg/apache/lucene/util/BytesRef;

    move/from16 v0, v24

    iput v0, v8, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 522
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->payload:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v43, v0

    .line 528
    .end local v24    # "payloadLength":I
    .local v43, "thisPayload":Lorg/apache/lucene/util/BytesRef;
    :goto_e
    if-eqz v30, :cond_23

    .line 529
    invoke-virtual/range {v29 .. v29}, Lorg/apache/lucene/index/ByteSliceReader;->readVInt()I

    move-result v8

    add-int v34, v23, v8

    .line 530
    .local v34, "startOffset":I
    invoke-virtual/range {v29 .. v29}, Lorg/apache/lucene/index/ByteSliceReader;->readVInt()I

    move-result v8

    add-int v18, v34, v8

    .line 531
    .local v18, "endOffset":I
    if-eqz v48, :cond_20

    .line 532
    if-eqz v47, :cond_22

    .line 533
    sget-boolean v8, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->$assertionsDisabled:Z

    if-nez v8, :cond_1f

    if-ltz v34, :cond_1c

    move/from16 v0, v18

    move/from16 v1, v34

    if-ge v0, v1, :cond_1f

    :cond_1c
    new-instance v8, Ljava/lang/AssertionError;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v12, "startOffset="

    invoke-direct {v9, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v34

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v12, ",endOffset="

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move/from16 v0, v18

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v12, ",offset="

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move/from16 v0, v23

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v8

    .line 516
    .end local v18    # "endOffset":I
    .end local v34    # "startOffset":I
    .end local v43    # "thisPayload":Lorg/apache/lucene/util/BytesRef;
    .restart local v24    # "payloadLength":I
    :cond_1d
    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->payload:Lorg/apache/lucene/util/BytesRef;

    iget-object v8, v8, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    array-length v8, v8

    move/from16 v0, v24

    if-ge v8, v0, :cond_1b

    .line 517
    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->payload:Lorg/apache/lucene/util/BytesRef;

    move/from16 v0, v24

    invoke-virtual {v8, v0}, Lorg/apache/lucene/util/BytesRef;->grow(I)V

    goto :goto_d

    .line 525
    .end local v24    # "payloadLength":I
    :cond_1e
    const/16 v43, 0x0

    .restart local v43    # "thisPayload":Lorg/apache/lucene/util/BytesRef;
    goto :goto_e

    .line 534
    .restart local v18    # "endOffset":I
    .restart local v34    # "startOffset":I
    :cond_1f
    move-object/from16 v0, v27

    move/from16 v1, v25

    move-object/from16 v2, v43

    move/from16 v3, v34

    move/from16 v4, v18

    invoke-virtual {v0, v1, v2, v3, v4}, Lorg/apache/lucene/codecs/PostingsConsumer;->addPosition(ILorg/apache/lucene/util/BytesRef;II)V

    .line 539
    :cond_20
    :goto_f
    move/from16 v23, v34

    .line 501
    .end local v6    # "code":I
    .end local v18    # "endOffset":I
    .end local v34    # "startOffset":I
    .end local v43    # "thisPayload":Lorg/apache/lucene/util/BytesRef;
    :cond_21
    :goto_10
    add-int/lit8 v21, v21, 0x1

    goto/16 :goto_c

    .line 536
    .restart local v6    # "code":I
    .restart local v18    # "endOffset":I
    .restart local v34    # "startOffset":I
    .restart local v43    # "thisPayload":Lorg/apache/lucene/util/BytesRef;
    :cond_22
    const/4 v8, -0x1

    const/4 v9, -0x1

    move-object/from16 v0, v27

    move/from16 v1, v25

    move-object/from16 v2, v43

    invoke-virtual {v0, v1, v2, v8, v9}, Lorg/apache/lucene/codecs/PostingsConsumer;->addPosition(ILorg/apache/lucene/util/BytesRef;II)V

    goto :goto_f

    .line 540
    .end local v18    # "endOffset":I
    .end local v34    # "startOffset":I
    :cond_23
    if-eqz v48, :cond_21

    .line 541
    const/4 v8, -0x1

    const/4 v9, -0x1

    move-object/from16 v0, v27

    move/from16 v1, v25

    move-object/from16 v2, v43

    invoke-virtual {v0, v1, v2, v8, v9}, Lorg/apache/lucene/codecs/PostingsConsumer;->addPosition(ILorg/apache/lucene/util/BytesRef;II)V

    goto :goto_10

    .line 548
    .end local v6    # "code":I
    .end local v21    # "j":I
    .end local v23    # "offset":I
    .end local v25    # "position":I
    .end local v38    # "termFreq":I
    .end local v43    # "thisPayload":Lorg/apache/lucene/util/BytesRef;
    :cond_24
    new-instance v12, Lorg/apache/lucene/codecs/TermStats;

    if-eqz v49, :cond_25

    move-wide/from16 v8, v44

    :goto_11
    invoke-direct {v12, v15, v8, v9}, Lorg/apache/lucene/codecs/TermStats;-><init>(IJ)V

    move-object/from16 v0, v41

    invoke-virtual {v7, v0, v12}, Lorg/apache/lucene/codecs/TermsConsumer;->finishTerm(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/codecs/TermStats;)V

    .line 549
    add-long v36, v36, v44

    .line 550
    int-to-long v8, v15

    add-long/2addr v10, v8

    .line 386
    add-int/lit8 v20, v20, 0x1

    goto/16 :goto_5

    .line 548
    :cond_25
    const-wide/16 v8, -0x1

    goto :goto_11

    .line 553
    .end local v14    # "delDocLimit":I
    .end local v15    # "docFreq":I
    .end local v16    # "docID":I
    .end local v27    # "postingsConsumer":Lorg/apache/lucene/codecs/PostingsConsumer;
    .end local v39    # "termID":I
    .end local v42    # "textStart":I
    .end local v44    # "totTF":J
    :cond_26
    const-wide/16 v8, -0x1

    goto/16 :goto_6
.end method

.method getStreamCount()I
    .locals 1

    .prologue
    .line 62
    iget-boolean v0, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->hasProx:Z

    if-nez v0, :cond_0

    .line 63
    const/4 v0, 0x1

    .line 65
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method newTerm(I)V
    .locals 4
    .param p1, "termID"    # I

    .prologue
    const/4 v3, 0x1

    .line 169
    sget-boolean v1, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    const-string v2, "FreqProxTermsWriterPerField.newTerm start"

    invoke-virtual {v1, v2}, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;->testPoint(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 171
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    iget-object v0, v1, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    check-cast v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;

    .line 172
    .local v0, "postings":Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;
    iget-object v1, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastDocIDs:[I

    iget-object v2, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    iget v2, v2, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;->docID:I

    aput v2, v1, p1

    .line 173
    iget-boolean v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->hasFreq:Z

    if-nez v1, :cond_2

    .line 174
    iget-object v1, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastDocCodes:[I

    iget-object v2, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    iget v2, v2, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;->docID:I

    aput v2, v1, p1

    .line 187
    :cond_1
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iget-object v2, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iget v2, v2, Lorg/apache/lucene/index/FieldInvertState;->maxTermFrequency:I

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    iput v2, v1, Lorg/apache/lucene/index/FieldInvertState;->maxTermFrequency:I

    .line 188
    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iget v2, v1, Lorg/apache/lucene/index/FieldInvertState;->uniqueTermCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lorg/apache/lucene/index/FieldInvertState;->uniqueTermCount:I

    .line 189
    return-void

    .line 176
    :cond_2
    iget-object v1, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastDocCodes:[I

    iget-object v2, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    iget v2, v2, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;->docID:I

    shl-int/lit8 v2, v2, 0x1

    aput v2, v1, p1

    .line 177
    iget-object v1, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->termFreqs:[I

    aput v3, v1, p1

    .line 178
    iget-boolean v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->hasProx:Z

    if-eqz v1, :cond_3

    .line 179
    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iget v1, v1, Lorg/apache/lucene/index/FieldInvertState;->position:I

    invoke-virtual {p0, p1, v1}, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->writeProx(II)V

    .line 180
    iget-boolean v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->hasOffsets:Z

    if-eqz v1, :cond_1

    .line 181
    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iget v1, v1, Lorg/apache/lucene/index/FieldInvertState;->offset:I

    invoke-virtual {p0, p1, v1}, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->writeOffsets(II)V

    goto :goto_0

    .line 184
    :cond_3
    sget-boolean v1, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->hasOffsets:Z

    if-eqz v1, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1
.end method

.method reset()V
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfo;->getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->setIndexOptions(Lorg/apache/lucene/index/FieldInfo$IndexOptions;)V

    .line 91
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->payloadAttribute:Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

    .line 92
    return-void
.end method

.method skippingLongTerm()V
    .locals 0

    .prologue
    .line 79
    return-void
.end method

.method start(Lorg/apache/lucene/index/IndexableField;)V
    .locals 3
    .param p1, "f"    # Lorg/apache/lucene/index/IndexableField;

    .prologue
    const/4 v2, 0x0

    .line 117
    iget-object v0, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iget-object v0, v0, Lorg/apache/lucene/index/FieldInvertState;->attributeSource:Lorg/apache/lucene/util/AttributeSource;

    const-class v1, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/AttributeSource;->hasAttribute(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iget-object v0, v0, Lorg/apache/lucene/index/FieldInvertState;->attributeSource:Lorg/apache/lucene/util/AttributeSource;

    const-class v1, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/AttributeSource;->getAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

    iput-object v0, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->payloadAttribute:Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

    .line 122
    :goto_0
    iget-boolean v0, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->hasOffsets:Z

    if-eqz v0, :cond_1

    .line 123
    iget-object v0, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iget-object v0, v0, Lorg/apache/lucene/index/FieldInvertState;->attributeSource:Lorg/apache/lucene/util/AttributeSource;

    const-class v1, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/AttributeSource;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iput-object v0, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->offsetAttribute:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 127
    :goto_1
    return-void

    .line 120
    :cond_0
    iput-object v2, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->payloadAttribute:Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

    goto :goto_0

    .line 125
    :cond_1
    iput-object v2, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->offsetAttribute:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    goto :goto_1
.end method

.method start([Lorg/apache/lucene/index/IndexableField;I)Z
    .locals 2
    .param p1, "fields"    # [Lorg/apache/lucene/index/IndexableField;
    .param p2, "count"    # I

    .prologue
    .line 107
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, p2, :cond_0

    .line 112
    const/4 v1, 0x0

    :goto_1
    return v1

    .line 108
    :cond_0
    aget-object v1, p1, v0

    invoke-interface {v1}, Lorg/apache/lucene/index/IndexableField;->fieldType()Lorg/apache/lucene/index/IndexableFieldType;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/lucene/index/IndexableFieldType;->indexed()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 109
    const/4 v1, 0x1

    goto :goto_1

    .line 107
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method writeOffsets(II)V
    .locals 6
    .param p1, "termID"    # I
    .param p2, "offsetAccum"    # I

    .prologue
    const/4 v5, 0x1

    .line 153
    sget-boolean v3, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    iget-boolean v3, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->hasOffsets:Z

    if-nez v3, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 154
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->offsetAttribute:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->startOffset()I

    move-result v3

    add-int v2, p2, v3

    .line 155
    .local v2, "startOffset":I
    iget-object v3, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->offsetAttribute:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->endOffset()I

    move-result v3

    add-int v0, p2, v3

    .line 157
    .local v0, "endOffset":I
    iget-object v3, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    iget-object v1, v3, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    check-cast v1, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;

    .line 158
    .local v1, "postings":Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;
    sget-boolean v3, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->$assertionsDisabled:Z

    if-nez v3, :cond_1

    iget-object v3, v1, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastOffsets:[I

    aget v3, v3, p1

    sub-int v3, v2, v3

    if-gez v3, :cond_1

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 159
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    iget-object v4, v1, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastOffsets:[I

    aget v4, v4, p1

    sub-int v4, v2, v4

    invoke-virtual {v3, v5, v4}, Lorg/apache/lucene/index/TermsHashPerField;->writeVInt(II)V

    .line 160
    iget-object v3, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    sub-int v4, v0, v2

    invoke-virtual {v3, v5, v4}, Lorg/apache/lucene/index/TermsHashPerField;->writeVInt(II)V

    .line 162
    iget-object v3, v1, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastOffsets:[I

    aput v2, v3, p1

    .line 163
    return-void
.end method

.method writeProx(II)V
    .locals 7
    .param p1, "termID"    # I
    .param p2, "proxCode"    # I

    .prologue
    const/4 v6, 0x1

    .line 131
    sget-boolean v2, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->hasProx:Z

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 133
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->payloadAttribute:Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

    if-nez v2, :cond_1

    .line 134
    const/4 v0, 0x0

    .line 139
    .local v0, "payload":Lorg/apache/lucene/util/BytesRef;
    :goto_0
    if-eqz v0, :cond_2

    iget v2, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    if-lez v2, :cond_2

    .line 140
    iget-object v2, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    shl-int/lit8 v3, p2, 0x1

    or-int/lit8 v3, v3, 0x1

    invoke-virtual {v2, v6, v3}, Lorg/apache/lucene/index/TermsHashPerField;->writeVInt(II)V

    .line 141
    iget-object v2, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    iget v3, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-virtual {v2, v6, v3}, Lorg/apache/lucene/index/TermsHashPerField;->writeVInt(II)V

    .line 142
    iget-object v2, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    iget-object v3, v0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v4, v0, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget v5, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-virtual {v2, v6, v3, v4, v5}, Lorg/apache/lucene/index/TermsHashPerField;->writeBytes(I[BII)V

    .line 143
    iput-boolean v6, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->hasPayloads:Z

    .line 148
    :goto_1
    iget-object v2, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    iget-object v1, v2, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    check-cast v1, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;

    .line 149
    .local v1, "postings":Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;
    iget-object v2, v1, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastPositions:[I

    iget-object v3, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iget v3, v3, Lorg/apache/lucene/index/FieldInvertState;->position:I

    aput v3, v2, p1

    .line 150
    return-void

    .line 136
    .end local v0    # "payload":Lorg/apache/lucene/util/BytesRef;
    .end local v1    # "postings":Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->payloadAttribute:Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

    invoke-interface {v2}, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;->getPayload()Lorg/apache/lucene/util/BytesRef;

    move-result-object v0

    .restart local v0    # "payload":Lorg/apache/lucene/util/BytesRef;
    goto :goto_0

    .line 145
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    shl-int/lit8 v3, p2, 0x1

    invoke-virtual {v2, v6, v3}, Lorg/apache/lucene/index/TermsHashPerField;->writeVInt(II)V

    goto :goto_1
.end method
