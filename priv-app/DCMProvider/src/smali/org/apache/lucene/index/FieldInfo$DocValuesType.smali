.class public final enum Lorg/apache/lucene/index/FieldInfo$DocValuesType;
.super Ljava/lang/Enum;
.source "FieldInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/FieldInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DocValuesType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/lucene/index/FieldInfo$DocValuesType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum BINARY:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

.field private static final synthetic ENUM$VALUES:[Lorg/apache/lucene/index/FieldInfo$DocValuesType;

.field public static final enum NUMERIC:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

.field public static final enum SORTED:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

.field public static final enum SORTED_SET:Lorg/apache/lucene/index/FieldInfo$DocValuesType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 90
    new-instance v0, Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    const-string v1, "NUMERIC"

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/index/FieldInfo$DocValuesType;-><init>(Ljava/lang/String;I)V

    .line 93
    sput-object v0, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->NUMERIC:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    .line 94
    new-instance v0, Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    const-string v1, "BINARY"

    invoke-direct {v0, v1, v3}, Lorg/apache/lucene/index/FieldInfo$DocValuesType;-><init>(Ljava/lang/String;I)V

    .line 97
    sput-object v0, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->BINARY:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    .line 98
    new-instance v0, Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    const-string v1, "SORTED"

    invoke-direct {v0, v1, v4}, Lorg/apache/lucene/index/FieldInfo$DocValuesType;-><init>(Ljava/lang/String;I)V

    .line 104
    sput-object v0, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->SORTED:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    .line 105
    new-instance v0, Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    const-string v1, "SORTED_SET"

    invoke-direct {v0, v1, v5}, Lorg/apache/lucene/index/FieldInfo$DocValuesType;-><init>(Ljava/lang/String;I)V

    .line 111
    sput-object v0, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->SORTED_SET:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    .line 89
    const/4 v0, 0x4

    new-array v0, v0, [Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    sget-object v1, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->NUMERIC:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    aput-object v1, v0, v2

    sget-object v1, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->BINARY:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    aput-object v1, v0, v3

    sget-object v1, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->SORTED:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    aput-object v1, v0, v4

    sget-object v1, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->SORTED_SET:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    aput-object v1, v0, v5

    sput-object v0, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->ENUM$VALUES:[Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 89
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/lucene/index/FieldInfo$DocValuesType;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    return-object v0
.end method

.method public static values()[Lorg/apache/lucene/index/FieldInfo$DocValuesType;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->ENUM$VALUES:[Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    array-length v1, v0

    new-array v2, v1, [Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
