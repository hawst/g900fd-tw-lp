.class final Lorg/apache/lucene/index/FieldInfos$Builder;
.super Ljava/lang/Object;
.source "FieldInfos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/FieldInfos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "Builder"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final byName:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/index/FieldInfo;",
            ">;"
        }
    .end annotation
.end field

.field final globalFieldNumbers:Lorg/apache/lucene/index/FieldInfos$FieldNumbers;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 233
    const-class v0, Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/FieldInfos$Builder;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 238
    new-instance v0, Lorg/apache/lucene/index/FieldInfos$FieldNumbers;

    invoke-direct {v0}, Lorg/apache/lucene/index/FieldInfos$FieldNumbers;-><init>()V

    invoke-direct {p0, v0}, Lorg/apache/lucene/index/FieldInfos$Builder;-><init>(Lorg/apache/lucene/index/FieldInfos$FieldNumbers;)V

    .line 239
    return-void
.end method

.method constructor <init>(Lorg/apache/lucene/index/FieldInfos$FieldNumbers;)V
    .locals 1
    .param p1, "globalFieldNumbers"    # Lorg/apache/lucene/index/FieldInfos$FieldNumbers;

    .prologue
    .line 244
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 234
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/FieldInfos$Builder;->byName:Ljava/util/HashMap;

    .line 245
    sget-boolean v0, Lorg/apache/lucene/index/FieldInfos$Builder;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 246
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/index/FieldInfos$Builder;->globalFieldNumbers:Lorg/apache/lucene/index/FieldInfos$FieldNumbers;

    .line 247
    return-void
.end method

.method private addOrUpdateInternal(Ljava/lang/String;IZZZZLorg/apache/lucene/index/FieldInfo$IndexOptions;Lorg/apache/lucene/index/FieldInfo$DocValuesType;Lorg/apache/lucene/index/FieldInfo$DocValuesType;)Lorg/apache/lucene/index/FieldInfo;
    .locals 12
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "preferredFieldNumber"    # I
    .param p3, "isIndexed"    # Z
    .param p4, "storeTermVector"    # Z
    .param p5, "omitNorms"    # Z
    .param p6, "storePayloads"    # Z
    .param p7, "indexOptions"    # Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    .param p8, "docValues"    # Lorg/apache/lucene/index/FieldInfo$DocValuesType;
    .param p9, "normType"    # Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    .prologue
    .line 274
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/FieldInfos$Builder;->fieldInfo(Ljava/lang/String;)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v1

    .line 275
    .local v1, "fi":Lorg/apache/lucene/index/FieldInfo;
    if-nez v1, :cond_3

    .line 281
    iget-object v2, p0, Lorg/apache/lucene/index/FieldInfos$Builder;->globalFieldNumbers:Lorg/apache/lucene/index/FieldInfos$FieldNumbers;

    move-object/from16 v0, p8

    invoke-virtual {v2, p1, p2, v0}, Lorg/apache/lucene/index/FieldInfos$FieldNumbers;->addOrGet(Ljava/lang/String;ILorg/apache/lucene/index/FieldInfo$DocValuesType;)I

    move-result v4

    .line 282
    .local v4, "fieldNumber":I
    new-instance v1, Lorg/apache/lucene/index/FieldInfo;

    .end local v1    # "fi":Lorg/apache/lucene/index/FieldInfo;
    const/4 v11, 0x0

    move-object v2, p1

    move v3, p3

    move/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v1 .. v11}, Lorg/apache/lucene/index/FieldInfo;-><init>(Ljava/lang/String;ZIZZZLorg/apache/lucene/index/FieldInfo$IndexOptions;Lorg/apache/lucene/index/FieldInfo$DocValuesType;Lorg/apache/lucene/index/FieldInfo$DocValuesType;Ljava/util/Map;)V

    .line 283
    .restart local v1    # "fi":Lorg/apache/lucene/index/FieldInfo;
    sget-boolean v2, Lorg/apache/lucene/index/FieldInfos$Builder;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lorg/apache/lucene/index/FieldInfos$Builder;->byName:Ljava/util/HashMap;

    iget-object v3, v1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 284
    :cond_0
    sget-boolean v2, Lorg/apache/lucene/index/FieldInfos$Builder;->$assertionsDisabled:Z

    if-nez v2, :cond_1

    iget-object v2, p0, Lorg/apache/lucene/index/FieldInfos$Builder;->globalFieldNumbers:Lorg/apache/lucene/index/FieldInfos$FieldNumbers;

    iget v3, v1, Lorg/apache/lucene/index/FieldInfo;->number:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-object v5, v1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {v1}, Lorg/apache/lucene/index/FieldInfo;->getDocValuesType()Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-result-object v6

    invoke-virtual {v2, v3, v5, v6}, Lorg/apache/lucene/index/FieldInfos$FieldNumbers;->containsConsistent(Ljava/lang/Integer;Ljava/lang/String;Lorg/apache/lucene/index/FieldInfo$DocValuesType;)Z

    move-result v2

    if-nez v2, :cond_1

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 285
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/index/FieldInfos$Builder;->byName:Ljava/util/HashMap;

    iget-object v3, v1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {v2, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 297
    .end local v4    # "fieldNumber":I
    :cond_2
    :goto_0
    return-object v1

    :cond_3
    move-object v5, v1

    move v6, p3

    move/from16 v7, p4

    move/from16 v8, p5

    move/from16 v9, p6

    move-object/from16 v10, p7

    .line 287
    invoke-virtual/range {v5 .. v10}, Lorg/apache/lucene/index/FieldInfo;->update(ZZZZLorg/apache/lucene/index/FieldInfo$IndexOptions;)V

    .line 289
    if-eqz p8, :cond_4

    .line 290
    move-object/from16 v0, p8

    invoke-virtual {v1, v0}, Lorg/apache/lucene/index/FieldInfo;->setDocValuesType(Lorg/apache/lucene/index/FieldInfo$DocValuesType;)V

    .line 293
    :cond_4
    invoke-virtual {v1}, Lorg/apache/lucene/index/FieldInfo;->omitsNorms()Z

    move-result v2

    if-nez v2, :cond_2

    if-eqz p9, :cond_2

    .line 294
    move-object/from16 v0, p9

    invoke-virtual {v1, v0}, Lorg/apache/lucene/index/FieldInfo;->setNormValueType(Lorg/apache/lucene/index/FieldInfo$DocValuesType;)V

    goto :goto_0
.end method


# virtual methods
.method public add(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/FieldInfo;
    .locals 10
    .param p1, "fi"    # Lorg/apache/lucene/index/FieldInfo;

    .prologue
    .line 302
    iget-object v1, p1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    iget v2, p1, Lorg/apache/lucene/index/FieldInfo;->number:I

    invoke-virtual {p1}, Lorg/apache/lucene/index/FieldInfo;->isIndexed()Z

    move-result v3

    invoke-virtual {p1}, Lorg/apache/lucene/index/FieldInfo;->hasVectors()Z

    move-result v4

    .line 303
    invoke-virtual {p1}, Lorg/apache/lucene/index/FieldInfo;->omitsNorms()Z

    move-result v5

    invoke-virtual {p1}, Lorg/apache/lucene/index/FieldInfo;->hasPayloads()Z

    move-result v6

    .line 304
    invoke-virtual {p1}, Lorg/apache/lucene/index/FieldInfo;->getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v7

    invoke-virtual {p1}, Lorg/apache/lucene/index/FieldInfo;->getDocValuesType()Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-result-object v8

    invoke-virtual {p1}, Lorg/apache/lucene/index/FieldInfo;->getNormType()Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-result-object v9

    move-object v0, p0

    .line 302
    invoke-direct/range {v0 .. v9}, Lorg/apache/lucene/index/FieldInfos$Builder;->addOrUpdateInternal(Ljava/lang/String;IZZZZLorg/apache/lucene/index/FieldInfo$IndexOptions;Lorg/apache/lucene/index/FieldInfo$DocValuesType;Lorg/apache/lucene/index/FieldInfo$DocValuesType;)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v0

    return-object v0
.end method

.method public add(Lorg/apache/lucene/index/FieldInfos;)V
    .locals 3
    .param p1, "other"    # Lorg/apache/lucene/index/FieldInfos;

    .prologue
    .line 250
    invoke-virtual {p1}, Lorg/apache/lucene/index/FieldInfos;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 253
    return-void

    .line 250
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/FieldInfo;

    .line 251
    .local v0, "fieldInfo":Lorg/apache/lucene/index/FieldInfo;
    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/FieldInfos$Builder;->add(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/FieldInfo;

    goto :goto_0
.end method

.method public addOrUpdate(Ljava/lang/String;Lorg/apache/lucene/index/IndexableFieldType;)Lorg/apache/lucene/index/FieldInfo;
    .locals 10
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "fieldType"    # Lorg/apache/lucene/index/IndexableFieldType;

    .prologue
    const/4 v4, 0x0

    .line 266
    const/4 v2, -0x1

    invoke-interface {p2}, Lorg/apache/lucene/index/IndexableFieldType;->indexed()Z

    move-result v3

    .line 267
    invoke-interface {p2}, Lorg/apache/lucene/index/IndexableFieldType;->omitNorms()Z

    move-result v5

    .line 268
    invoke-interface {p2}, Lorg/apache/lucene/index/IndexableFieldType;->indexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v7

    invoke-interface {p2}, Lorg/apache/lucene/index/IndexableFieldType;->docValueType()Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-result-object v8

    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, p1

    move v6, v4

    .line 266
    invoke-direct/range {v0 .. v9}, Lorg/apache/lucene/index/FieldInfos$Builder;->addOrUpdateInternal(Ljava/lang/String;IZZZZLorg/apache/lucene/index/FieldInfo$IndexOptions;Lorg/apache/lucene/index/FieldInfo$DocValuesType;Lorg/apache/lucene/index/FieldInfo$DocValuesType;)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v0

    return-object v0
.end method

.method public fieldInfo(Ljava/lang/String;)Lorg/apache/lucene/index/FieldInfo;
    .locals 1
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 308
    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfos$Builder;->byName:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/FieldInfo;

    return-object v0
.end method

.method final finish()Lorg/apache/lucene/index/FieldInfos;
    .locals 3

    .prologue
    .line 312
    new-instance v1, Lorg/apache/lucene/index/FieldInfos;

    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfos$Builder;->byName:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    iget-object v2, p0, Lorg/apache/lucene/index/FieldInfos$Builder;->byName:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    new-array v2, v2, [Lorg/apache/lucene/index/FieldInfo;

    invoke-interface {v0, v2}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/lucene/index/FieldInfo;

    invoke-direct {v1, v0}, Lorg/apache/lucene/index/FieldInfos;-><init>([Lorg/apache/lucene/index/FieldInfo;)V

    return-object v1
.end method
