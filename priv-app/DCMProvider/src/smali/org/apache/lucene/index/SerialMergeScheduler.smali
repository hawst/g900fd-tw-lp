.class public Lorg/apache/lucene/index/SerialMergeScheduler;
.super Lorg/apache/lucene/index/MergeScheduler;
.source "SerialMergeScheduler.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lorg/apache/lucene/index/MergeScheduler;-><init>()V

    .line 28
    return-void
.end method


# virtual methods
.method public close()V
    .locals 0

    .prologue
    .line 45
    return-void
.end method

.method public declared-synchronized merge(Lorg/apache/lucene/index/IndexWriter;)V
    .locals 2
    .param p1, "writer"    # Lorg/apache/lucene/index/IndexWriter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 37
    monitor-enter p0

    :goto_0
    :try_start_0
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexWriter;->getNextMerge()Lorg/apache/lucene/index/MergePolicy$OneMerge;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 38
    .local v0, "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    if-nez v0, :cond_0

    .line 42
    monitor-exit p0

    return-void

    .line 40
    :cond_0
    :try_start_1
    invoke-virtual {p1, v0}, Lorg/apache/lucene/index/IndexWriter;->merge(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 37
    .end local v0    # "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
