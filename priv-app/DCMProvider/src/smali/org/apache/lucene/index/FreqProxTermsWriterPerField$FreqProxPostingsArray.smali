.class final Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;
.super Lorg/apache/lucene/index/ParallelPostingsArray;
.source "FreqProxTermsWriterPerField.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/FreqProxTermsWriterPerField;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "FreqProxPostingsArray"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field lastDocCodes:[I

.field lastDocIDs:[I

.field lastOffsets:[I

.field lastPositions:[I

.field termFreqs:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 252
    const-class v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(IZZZ)V
    .locals 1
    .param p1, "size"    # I
    .param p2, "writeFreqs"    # Z
    .param p3, "writeProx"    # Z
    .param p4, "writeOffsets"    # Z

    .prologue
    .line 254
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/ParallelPostingsArray;-><init>(I)V

    .line 255
    if-eqz p2, :cond_0

    .line 256
    new-array v0, p1, [I

    iput-object v0, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->termFreqs:[I

    .line 258
    :cond_0
    new-array v0, p1, [I

    iput-object v0, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastDocIDs:[I

    .line 259
    new-array v0, p1, [I

    iput-object v0, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastDocCodes:[I

    .line 260
    if-eqz p3, :cond_2

    .line 261
    new-array v0, p1, [I

    iput-object v0, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastPositions:[I

    .line 262
    if-eqz p4, :cond_1

    .line 263
    new-array v0, p1, [I

    iput-object v0, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastOffsets:[I

    .line 269
    :cond_1
    return-void

    .line 266
    :cond_2
    sget-boolean v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-eqz p4, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method


# virtual methods
.method bytesPerPosting()I
    .locals 2

    .prologue
    .line 307
    const/16 v0, 0x14

    .line 308
    .local v0, "bytes":I
    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastPositions:[I

    if-eqz v1, :cond_0

    .line 309
    add-int/lit8 v0, v0, 0x4

    .line 311
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastOffsets:[I

    if-eqz v1, :cond_1

    .line 312
    add-int/lit8 v0, v0, 0x4

    .line 314
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->termFreqs:[I

    if-eqz v1, :cond_2

    .line 315
    add-int/lit8 v0, v0, 0x4

    .line 318
    :cond_2
    return v0
.end method

.method copyTo(Lorg/apache/lucene/index/ParallelPostingsArray;I)V
    .locals 4
    .param p1, "toArray"    # Lorg/apache/lucene/index/ParallelPostingsArray;
    .param p2, "numToCopy"    # I

    .prologue
    const/4 v3, 0x0

    .line 284
    sget-boolean v1, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    instance-of v1, p1, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    :cond_0
    move-object v0, p1

    .line 285
    check-cast v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;

    .line 287
    .local v0, "to":Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;
    invoke-super {p0, p1, p2}, Lorg/apache/lucene/index/ParallelPostingsArray;->copyTo(Lorg/apache/lucene/index/ParallelPostingsArray;I)V

    .line 289
    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastDocIDs:[I

    iget-object v2, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastDocIDs:[I

    invoke-static {v1, v3, v2, v3, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 290
    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastDocCodes:[I

    iget-object v2, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastDocCodes:[I

    invoke-static {v1, v3, v2, v3, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 291
    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastPositions:[I

    if-eqz v1, :cond_2

    .line 292
    sget-boolean v1, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    iget-object v1, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastPositions:[I

    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 293
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastPositions:[I

    iget-object v2, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastPositions:[I

    invoke-static {v1, v3, v2, v3, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 295
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastOffsets:[I

    if-eqz v1, :cond_4

    .line 296
    sget-boolean v1, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->$assertionsDisabled:Z

    if-nez v1, :cond_3

    iget-object v1, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastOffsets:[I

    if-nez v1, :cond_3

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 297
    :cond_3
    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastOffsets:[I

    iget-object v2, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastOffsets:[I

    invoke-static {v1, v3, v2, v3, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 299
    :cond_4
    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->termFreqs:[I

    if-eqz v1, :cond_6

    .line 300
    sget-boolean v1, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->$assertionsDisabled:Z

    if-nez v1, :cond_5

    iget-object v1, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->termFreqs:[I

    if-nez v1, :cond_5

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 301
    :cond_5
    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->termFreqs:[I

    iget-object v2, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->termFreqs:[I

    invoke-static {v1, v3, v2, v3, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 303
    :cond_6
    return-void
.end method

.method newInstance(I)Lorg/apache/lucene/index/ParallelPostingsArray;
    .locals 6
    .param p1, "size"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 279
    new-instance v4, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;

    iget-object v0, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->termFreqs:[I

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iget-object v3, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastPositions:[I

    if-eqz v3, :cond_1

    move v3, v1

    :goto_1
    iget-object v5, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastOffsets:[I

    if-eqz v5, :cond_2

    :goto_2
    invoke-direct {v4, p1, v0, v3, v1}, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;-><init>(IZZZ)V

    return-object v4

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v3, v2

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2
.end method
