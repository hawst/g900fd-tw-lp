.class Lorg/apache/lucene/index/SortedDocValuesWriter$OrdsIterator;
.super Ljava/lang/Object;
.source "SortedDocValuesWriter.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/SortedDocValuesWriter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OrdsIterator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Ljava/lang/Number;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field docUpto:I

.field final iter:Lorg/apache/lucene/util/packed/AppendingLongBuffer$Iterator;

.field final maxDoc:I

.field final ordMap:[I

.field final synthetic this$0:Lorg/apache/lucene/index/SortedDocValuesWriter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 178
    const-class v0, Lorg/apache/lucene/index/SortedDocValuesWriter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/SortedDocValuesWriter$OrdsIterator;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/index/SortedDocValuesWriter;[II)V
    .locals 4
    .param p2, "ordMap"    # [I
    .param p3, "maxDoc"    # I

    .prologue
    .line 184
    iput-object p1, p0, Lorg/apache/lucene/index/SortedDocValuesWriter$OrdsIterator;->this$0:Lorg/apache/lucene/index/SortedDocValuesWriter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 179
    # getter for: Lorg/apache/lucene/index/SortedDocValuesWriter;->pending:Lorg/apache/lucene/util/packed/AppendingLongBuffer;
    invoke-static {p1}, Lorg/apache/lucene/index/SortedDocValuesWriter;->access$0(Lorg/apache/lucene/index/SortedDocValuesWriter;)Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->iterator()Lorg/apache/lucene/util/packed/AppendingLongBuffer$Iterator;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/SortedDocValuesWriter$OrdsIterator;->iter:Lorg/apache/lucene/util/packed/AppendingLongBuffer$Iterator;

    .line 185
    iput-object p2, p0, Lorg/apache/lucene/index/SortedDocValuesWriter$OrdsIterator;->ordMap:[I

    .line 186
    iput p3, p0, Lorg/apache/lucene/index/SortedDocValuesWriter$OrdsIterator;->maxDoc:I

    .line 187
    sget-boolean v0, Lorg/apache/lucene/index/SortedDocValuesWriter$OrdsIterator;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    # getter for: Lorg/apache/lucene/index/SortedDocValuesWriter;->pending:Lorg/apache/lucene/util/packed/AppendingLongBuffer;
    invoke-static {p1}, Lorg/apache/lucene/index/SortedDocValuesWriter;->access$0(Lorg/apache/lucene/index/SortedDocValuesWriter;)Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->size()J

    move-result-wide v0

    int-to-long v2, p3

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 188
    :cond_0
    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 2

    .prologue
    .line 192
    iget v0, p0, Lorg/apache/lucene/index/SortedDocValuesWriter$OrdsIterator;->docUpto:I

    iget v1, p0, Lorg/apache/lucene/index/SortedDocValuesWriter$OrdsIterator;->maxDoc:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next()Ljava/lang/Number;
    .locals 4

    .prologue
    .line 197
    invoke-virtual {p0}, Lorg/apache/lucene/index/SortedDocValuesWriter$OrdsIterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 198
    new-instance v1, Ljava/util/NoSuchElementException;

    invoke-direct {v1}, Ljava/util/NoSuchElementException;-><init>()V

    throw v1

    .line 200
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/SortedDocValuesWriter$OrdsIterator;->iter:Lorg/apache/lucene/util/packed/AppendingLongBuffer$Iterator;

    invoke-virtual {v1}, Lorg/apache/lucene/util/packed/AppendingLongBuffer$Iterator;->next()J

    move-result-wide v2

    long-to-int v0, v2

    .line 201
    .local v0, "ord":I
    iget v1, p0, Lorg/apache/lucene/index/SortedDocValuesWriter$OrdsIterator;->docUpto:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/index/SortedDocValuesWriter$OrdsIterator;->docUpto:I

    .line 203
    iget-object v1, p0, Lorg/apache/lucene/index/SortedDocValuesWriter$OrdsIterator;->ordMap:[I

    aget v1, v1, v0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/index/SortedDocValuesWriter$OrdsIterator;->next()Ljava/lang/Number;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 208
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
