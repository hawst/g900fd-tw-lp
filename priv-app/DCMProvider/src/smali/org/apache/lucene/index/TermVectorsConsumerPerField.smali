.class final Lorg/apache/lucene/index/TermVectorsConsumerPerField;
.super Lorg/apache/lucene/index/TermsHashConsumerPerField;
.source "TermVectorsConsumerPerField.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/TermVectorsConsumerPerField$TermVectorsPostingsArray;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field doVectorOffsets:Z

.field doVectorPayloads:Z

.field doVectorPositions:Z

.field doVectors:Z

.field final docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

.field final fieldInfo:Lorg/apache/lucene/index/FieldInfo;

.field final fieldState:Lorg/apache/lucene/index/FieldInvertState;

.field hasPayloads:Z

.field maxNumPostings:I

.field offsetAttribute:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

.field payloadAttribute:Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

.field final termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

.field final termsWriter:Lorg/apache/lucene/index/TermVectorsConsumer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/index/TermsHashPerField;Lorg/apache/lucene/index/TermVectorsConsumer;Lorg/apache/lucene/index/FieldInfo;)V
    .locals 1
    .param p1, "termsHashPerField"    # Lorg/apache/lucene/index/TermsHashPerField;
    .param p2, "termsWriter"    # Lorg/apache/lucene/index/TermVectorsConsumer;
    .param p3, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;

    .prologue
    .line 47
    invoke-direct {p0}, Lorg/apache/lucene/index/TermsHashConsumerPerField;-><init>()V

    .line 48
    iput-object p1, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    .line 49
    iput-object p2, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->termsWriter:Lorg/apache/lucene/index/TermVectorsConsumer;

    .line 50
    iput-object p3, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    .line 51
    iget-object v0, p1, Lorg/apache/lucene/index/TermsHashPerField;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    iput-object v0, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    .line 52
    iget-object v0, p1, Lorg/apache/lucene/index/TermsHashPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iput-object v0, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    .line 53
    return-void
.end method


# virtual methods
.method public abort()V
    .locals 0

    .prologue
    .line 124
    return-void
.end method

.method addTerm(I)V
    .locals 3
    .param p1, "termID"    # I

    .prologue
    .line 258
    sget-boolean v1, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    const-string v2, "TermVectorsTermsWriterPerField.addTerm start"

    invoke-virtual {v1, v2}, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;->testPoint(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 259
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    iget-object v0, v1, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    check-cast v0, Lorg/apache/lucene/index/TermVectorsConsumerPerField$TermVectorsPostingsArray;

    .line 261
    .local v0, "postings":Lorg/apache/lucene/index/TermVectorsConsumerPerField$TermVectorsPostingsArray;
    iget-object v1, v0, Lorg/apache/lucene/index/TermVectorsConsumerPerField$TermVectorsPostingsArray;->freqs:[I

    aget v2, v1, p1

    add-int/lit8 v2, v2, 0x1

    aput v2, v1, p1

    .line 263
    invoke-virtual {p0, v0, p1}, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->writeProx(Lorg/apache/lucene/index/TermVectorsConsumerPerField$TermVectorsPostingsArray;I)V

    .line 264
    return-void
.end method

.method createPostingsArray(I)Lorg/apache/lucene/index/ParallelPostingsArray;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 271
    new-instance v0, Lorg/apache/lucene/index/TermVectorsConsumerPerField$TermVectorsPostingsArray;

    invoke-direct {v0, p1}, Lorg/apache/lucene/index/TermVectorsConsumerPerField$TermVectorsPostingsArray;-><init>(I)V

    return-object v0
.end method

.method finish()V
    .locals 1

    .prologue
    .line 131
    iget-boolean v0, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->doVectors:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    iget-object v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->bytesHash:Lorg/apache/lucene/util/BytesRefHash;

    invoke-virtual {v0}, Lorg/apache/lucene/util/BytesRefHash;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 136
    :cond_0
    :goto_0
    return-void

    .line 135
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->termsWriter:Lorg/apache/lucene/index/TermVectorsConsumer;

    invoke-virtual {v0, p0}, Lorg/apache/lucene/index/TermVectorsConsumer;->addFieldToFlush(Lorg/apache/lucene/index/TermVectorsConsumerPerField;)V

    goto :goto_0
.end method

.method finishDocument()V
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 139
    sget-boolean v1, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    const-string v3, "TermVectorsTermsWriterPerField.finish start"

    invoke-virtual {v1, v3}, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;->testPoint(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 141
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    iget-object v1, v1, Lorg/apache/lucene/index/TermsHashPerField;->bytesHash:Lorg/apache/lucene/util/BytesRefHash;

    invoke-virtual {v1}, Lorg/apache/lucene/util/BytesRefHash;->size()I

    move-result v2

    .line 143
    .local v2, "numPostings":I
    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->termsWriter:Lorg/apache/lucene/index/TermVectorsConsumer;

    iget-object v6, v1, Lorg/apache/lucene/index/TermVectorsConsumer;->flushTerm:Lorg/apache/lucene/util/BytesRef;

    .line 145
    .local v6, "flushTerm":Lorg/apache/lucene/util/BytesRef;
    sget-boolean v1, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    if-gez v2, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 147
    :cond_1
    iget v1, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->maxNumPostings:I

    if-le v2, v1, :cond_2

    .line 148
    iput v2, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->maxNumPostings:I

    .line 154
    :cond_2
    sget-boolean v1, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->$assertionsDisabled:Z

    if-nez v1, :cond_3

    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->termsWriter:Lorg/apache/lucene/index/TermVectorsConsumer;

    iget-object v3, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    invoke-virtual {v1, v3}, Lorg/apache/lucene/index/TermVectorsConsumer;->vectorFieldsInOrder(Lorg/apache/lucene/index/FieldInfo;)Z

    move-result v1

    if-nez v1, :cond_3

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 156
    :cond_3
    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    iget-object v11, v1, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    check-cast v11, Lorg/apache/lucene/index/TermVectorsConsumerPerField$TermVectorsPostingsArray;

    .line 157
    .local v11, "postings":Lorg/apache/lucene/index/TermVectorsConsumerPerField$TermVectorsPostingsArray;
    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->termsWriter:Lorg/apache/lucene/index/TermVectorsConsumer;

    iget-object v0, v1, Lorg/apache/lucene/index/TermVectorsConsumer;->writer:Lorg/apache/lucene/codecs/TermVectorsWriter;

    .line 159
    .local v0, "tv":Lorg/apache/lucene/codecs/TermVectorsWriter;
    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/TermVectorsWriter;->getComparator()Ljava/util/Comparator;

    move-result-object v3

    invoke-virtual {v1, v3}, Lorg/apache/lucene/index/TermsHashPerField;->sortPostings(Ljava/util/Comparator;)[I

    move-result-object v14

    .line 161
    .local v14, "termIDs":[I
    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    iget-boolean v3, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->doVectorPositions:Z

    iget-boolean v4, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->doVectorOffsets:Z

    iget-boolean v5, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->hasPayloads:Z

    invoke-virtual/range {v0 .. v5}, Lorg/apache/lucene/codecs/TermVectorsWriter;->startField(Lorg/apache/lucene/index/FieldInfo;IZZZ)V

    .line 163
    iget-boolean v1, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->doVectorPositions:Z

    if-eqz v1, :cond_4

    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->termsWriter:Lorg/apache/lucene/index/TermVectorsConsumer;

    iget-object v10, v1, Lorg/apache/lucene/index/TermVectorsConsumer;->vectorSliceReaderPos:Lorg/apache/lucene/index/ByteSliceReader;

    .line 164
    .local v10, "posReader":Lorg/apache/lucene/index/ByteSliceReader;
    :goto_0
    iget-boolean v1, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->doVectorOffsets:Z

    if-eqz v1, :cond_5

    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->termsWriter:Lorg/apache/lucene/index/TermVectorsConsumer;

    iget-object v9, v1, Lorg/apache/lucene/index/TermVectorsConsumer;->vectorSliceReaderOff:Lorg/apache/lucene/index/ByteSliceReader;

    .line 166
    .local v9, "offReader":Lorg/apache/lucene/index/ByteSliceReader;
    :goto_1
    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    iget-object v12, v1, Lorg/apache/lucene/index/TermsHashPerField;->termBytePool:Lorg/apache/lucene/util/ByteBlockPool;

    .line 168
    .local v12, "termBytePool":Lorg/apache/lucene/util/ByteBlockPool;
    const/4 v8, 0x0

    .local v8, "j":I
    :goto_2
    if-lt v8, v2, :cond_6

    .line 187
    invoke-virtual {v0}, Lorg/apache/lucene/codecs/TermVectorsWriter;->finishField()V

    .line 189
    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    invoke-virtual {v1}, Lorg/apache/lucene/index/TermsHashPerField;->reset()V

    .line 191
    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    invoke-virtual {v1}, Lorg/apache/lucene/index/FieldInfo;->setStoreTermVectors()V

    .line 192
    return-void

    .line 163
    .end local v8    # "j":I
    .end local v9    # "offReader":Lorg/apache/lucene/index/ByteSliceReader;
    .end local v10    # "posReader":Lorg/apache/lucene/index/ByteSliceReader;
    .end local v12    # "termBytePool":Lorg/apache/lucene/util/ByteBlockPool;
    :cond_4
    const/4 v10, 0x0

    goto :goto_0

    .line 164
    .restart local v10    # "posReader":Lorg/apache/lucene/index/ByteSliceReader;
    :cond_5
    const/4 v9, 0x0

    goto :goto_1

    .line 169
    .restart local v8    # "j":I
    .restart local v9    # "offReader":Lorg/apache/lucene/index/ByteSliceReader;
    .restart local v12    # "termBytePool":Lorg/apache/lucene/util/ByteBlockPool;
    :cond_6
    aget v13, v14, v8

    .line 170
    .local v13, "termID":I
    iget-object v1, v11, Lorg/apache/lucene/index/TermVectorsConsumerPerField$TermVectorsPostingsArray;->freqs:[I

    aget v7, v1, v13

    .line 173
    .local v7, "freq":I
    iget-object v1, v11, Lorg/apache/lucene/index/TermVectorsConsumerPerField$TermVectorsPostingsArray;->textStarts:[I

    aget v1, v1, v13

    invoke-virtual {v12, v6, v1}, Lorg/apache/lucene/util/ByteBlockPool;->setBytesRef(Lorg/apache/lucene/util/BytesRef;I)V

    .line 174
    invoke-virtual {v0, v6, v7}, Lorg/apache/lucene/codecs/TermVectorsWriter;->startTerm(Lorg/apache/lucene/util/BytesRef;I)V

    .line 176
    iget-boolean v1, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->doVectorPositions:Z

    if-nez v1, :cond_7

    iget-boolean v1, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->doVectorOffsets:Z

    if-eqz v1, :cond_a

    .line 177
    :cond_7
    if-eqz v10, :cond_8

    .line 178
    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    const/4 v3, 0x0

    invoke-virtual {v1, v10, v13, v3}, Lorg/apache/lucene/index/TermsHashPerField;->initReader(Lorg/apache/lucene/index/ByteSliceReader;II)V

    .line 180
    :cond_8
    if-eqz v9, :cond_9

    .line 181
    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    const/4 v3, 0x1

    invoke-virtual {v1, v9, v13, v3}, Lorg/apache/lucene/index/TermsHashPerField;->initReader(Lorg/apache/lucene/index/ByteSliceReader;II)V

    .line 183
    :cond_9
    invoke-virtual {v0, v7, v10, v9}, Lorg/apache/lucene/codecs/TermVectorsWriter;->addProx(ILorg/apache/lucene/store/DataInput;Lorg/apache/lucene/store/DataInput;)V

    .line 185
    :cond_a
    invoke-virtual {v0}, Lorg/apache/lucene/codecs/TermVectorsWriter;->finishTerm()V

    .line 168
    add-int/lit8 v8, v8, 0x1

    goto :goto_2
.end method

.method getStreamCount()I
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x2

    return v0
.end method

.method newTerm(I)V
    .locals 4
    .param p1, "termID"    # I

    .prologue
    const/4 v3, 0x0

    .line 246
    sget-boolean v1, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    const-string v2, "TermVectorsTermsWriterPerField.newTerm start"

    invoke-virtual {v1, v2}, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;->testPoint(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 247
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    iget-object v0, v1, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    check-cast v0, Lorg/apache/lucene/index/TermVectorsConsumerPerField$TermVectorsPostingsArray;

    .line 249
    .local v0, "postings":Lorg/apache/lucene/index/TermVectorsConsumerPerField$TermVectorsPostingsArray;
    iget-object v1, v0, Lorg/apache/lucene/index/TermVectorsConsumerPerField$TermVectorsPostingsArray;->freqs:[I

    const/4 v2, 0x1

    aput v2, v1, p1

    .line 250
    iget-object v1, v0, Lorg/apache/lucene/index/TermVectorsConsumerPerField$TermVectorsPostingsArray;->lastOffsets:[I

    aput v3, v1, p1

    .line 251
    iget-object v1, v0, Lorg/apache/lucene/index/TermVectorsConsumerPerField$TermVectorsPostingsArray;->lastPositions:[I

    aput v3, v1, p1

    .line 253
    invoke-virtual {p0, v0, p1}, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->writeProx(Lorg/apache/lucene/index/TermVectorsConsumerPerField$TermVectorsPostingsArray;I)V

    .line 254
    return-void
.end method

.method shrinkHash()V
    .locals 2

    .prologue
    .line 195
    iget-object v0, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    iget v1, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->maxNumPostings:I

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/TermsHashPerField;->shrinkHash(I)V

    .line 196
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->maxNumPostings:I

    .line 197
    return-void
.end method

.method skippingLongTerm()V
    .locals 0

    .prologue
    .line 267
    return-void
.end method

.method start(Lorg/apache/lucene/index/IndexableField;)V
    .locals 3
    .param p1, "f"    # Lorg/apache/lucene/index/IndexableField;

    .prologue
    const/4 v2, 0x0

    .line 201
    iget-boolean v0, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->doVectorOffsets:Z

    if-eqz v0, :cond_0

    .line 202
    iget-object v0, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iget-object v0, v0, Lorg/apache/lucene/index/FieldInvertState;->attributeSource:Lorg/apache/lucene/util/AttributeSource;

    const-class v1, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/AttributeSource;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iput-object v0, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->offsetAttribute:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 206
    :goto_0
    iget-boolean v0, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->doVectorPayloads:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iget-object v0, v0, Lorg/apache/lucene/index/FieldInvertState;->attributeSource:Lorg/apache/lucene/util/AttributeSource;

    const-class v1, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/AttributeSource;->hasAttribute(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 207
    iget-object v0, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iget-object v0, v0, Lorg/apache/lucene/index/FieldInvertState;->attributeSource:Lorg/apache/lucene/util/AttributeSource;

    const-class v1, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/AttributeSource;->getAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

    iput-object v0, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->payloadAttribute:Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

    .line 211
    :goto_1
    return-void

    .line 204
    :cond_0
    iput-object v2, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->offsetAttribute:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    goto :goto_0

    .line 209
    :cond_1
    iput-object v2, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->payloadAttribute:Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

    goto :goto_1
.end method

.method start([Lorg/apache/lucene/index/IndexableField;I)Z
    .locals 5
    .param p1, "fields"    # [Lorg/apache/lucene/index/IndexableField;
    .param p2, "count"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 62
    iput-boolean v2, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->doVectors:Z

    .line 63
    iput-boolean v2, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->doVectorPositions:Z

    .line 64
    iput-boolean v2, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->doVectorOffsets:Z

    .line 65
    iput-boolean v2, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->doVectorPayloads:Z

    .line 66
    iput-boolean v2, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->hasPayloads:Z

    .line 68
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, p2, :cond_1

    .line 108
    iget-boolean v2, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->doVectors:Z

    if-eqz v2, :cond_0

    .line 109
    iget-object v2, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->termsWriter:Lorg/apache/lucene/index/TermVectorsConsumer;

    iput-boolean v4, v2, Lorg/apache/lucene/index/TermVectorsConsumer;->hasVectors:Z

    .line 110
    iget-object v2, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    iget-object v2, v2, Lorg/apache/lucene/index/TermsHashPerField;->bytesHash:Lorg/apache/lucene/util/BytesRefHash;

    invoke-virtual {v2}, Lorg/apache/lucene/util/BytesRefHash;->size()I

    move-result v2

    if-eqz v2, :cond_0

    .line 114
    iget-object v2, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    invoke-virtual {v2}, Lorg/apache/lucene/index/TermsHashPerField;->reset()V

    .line 121
    :cond_0
    iget-boolean v2, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->doVectors:Z

    return v2

    .line 69
    :cond_1
    aget-object v0, p1, v1

    .line 70
    .local v0, "field":Lorg/apache/lucene/index/IndexableField;
    invoke-interface {v0}, Lorg/apache/lucene/index/IndexableField;->fieldType()Lorg/apache/lucene/index/IndexableFieldType;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/lucene/index/IndexableFieldType;->indexed()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 71
    invoke-interface {v0}, Lorg/apache/lucene/index/IndexableField;->fieldType()Lorg/apache/lucene/index/IndexableFieldType;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/lucene/index/IndexableFieldType;->storeTermVectors()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 72
    iput-boolean v4, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->doVectors:Z

    .line 73
    iget-boolean v2, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->doVectorPositions:Z

    invoke-interface {v0}, Lorg/apache/lucene/index/IndexableField;->fieldType()Lorg/apache/lucene/index/IndexableFieldType;

    move-result-object v3

    invoke-interface {v3}, Lorg/apache/lucene/index/IndexableFieldType;->storeTermVectorPositions()Z

    move-result v3

    or-int/2addr v2, v3

    iput-boolean v2, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->doVectorPositions:Z

    .line 74
    iget-boolean v2, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->doVectorOffsets:Z

    invoke-interface {v0}, Lorg/apache/lucene/index/IndexableField;->fieldType()Lorg/apache/lucene/index/IndexableFieldType;

    move-result-object v3

    invoke-interface {v3}, Lorg/apache/lucene/index/IndexableFieldType;->storeTermVectorOffsets()Z

    move-result v3

    or-int/2addr v2, v3

    iput-boolean v2, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->doVectorOffsets:Z

    .line 75
    iget-boolean v2, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->doVectorPositions:Z

    if-eqz v2, :cond_3

    .line 76
    iget-boolean v2, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->doVectorPayloads:Z

    invoke-interface {v0}, Lorg/apache/lucene/index/IndexableField;->fieldType()Lorg/apache/lucene/index/IndexableFieldType;

    move-result-object v3

    invoke-interface {v3}, Lorg/apache/lucene/index/IndexableFieldType;->storeTermVectorPayloads()Z

    move-result v3

    or-int/2addr v2, v3

    iput-boolean v2, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->doVectorPayloads:Z

    .line 68
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 77
    :cond_3
    invoke-interface {v0}, Lorg/apache/lucene/index/IndexableField;->fieldType()Lorg/apache/lucene/index/IndexableFieldType;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/lucene/index/IndexableFieldType;->storeTermVectorPayloads()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 79
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "cannot index term vector payloads for field: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " without term vector positions"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 82
    :cond_4
    invoke-interface {v0}, Lorg/apache/lucene/index/IndexableField;->fieldType()Lorg/apache/lucene/index/IndexableFieldType;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/lucene/index/IndexableFieldType;->storeTermVectorOffsets()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 83
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "cannot index term vector offsets when term vectors are not indexed (field=\""

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 85
    :cond_5
    invoke-interface {v0}, Lorg/apache/lucene/index/IndexableField;->fieldType()Lorg/apache/lucene/index/IndexableFieldType;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/lucene/index/IndexableFieldType;->storeTermVectorPositions()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 86
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "cannot index term vector positions when term vectors are not indexed (field=\""

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 88
    :cond_6
    invoke-interface {v0}, Lorg/apache/lucene/index/IndexableField;->fieldType()Lorg/apache/lucene/index/IndexableFieldType;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/lucene/index/IndexableFieldType;->storeTermVectorPayloads()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 89
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "cannot index term vector payloads when term vectors are not indexed (field=\""

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 93
    :cond_7
    invoke-interface {v0}, Lorg/apache/lucene/index/IndexableField;->fieldType()Lorg/apache/lucene/index/IndexableFieldType;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/lucene/index/IndexableFieldType;->storeTermVectors()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 94
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "cannot index term vectors when field is not indexed (field=\""

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 96
    :cond_8
    invoke-interface {v0}, Lorg/apache/lucene/index/IndexableField;->fieldType()Lorg/apache/lucene/index/IndexableFieldType;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/lucene/index/IndexableFieldType;->storeTermVectorOffsets()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 97
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "cannot index term vector offsets when field is not indexed (field=\""

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 99
    :cond_9
    invoke-interface {v0}, Lorg/apache/lucene/index/IndexableField;->fieldType()Lorg/apache/lucene/index/IndexableFieldType;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/lucene/index/IndexableFieldType;->storeTermVectorPositions()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 100
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "cannot index term vector positions when field is not indexed (field=\""

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 102
    :cond_a
    invoke-interface {v0}, Lorg/apache/lucene/index/IndexableField;->fieldType()Lorg/apache/lucene/index/IndexableFieldType;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/lucene/index/IndexableFieldType;->storeTermVectorPayloads()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 103
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "cannot index term vector payloads when field is not indexed (field=\""

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method writeProx(Lorg/apache/lucene/index/TermVectorsConsumerPerField$TermVectorsPostingsArray;I)V
    .locals 10
    .param p1, "postings"    # Lorg/apache/lucene/index/TermVectorsConsumerPerField$TermVectorsPostingsArray;
    .param p2, "termID"    # I

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 214
    iget-boolean v4, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->doVectorOffsets:Z

    if-eqz v4, :cond_0

    .line 215
    iget-object v4, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iget v4, v4, Lorg/apache/lucene/index/FieldInvertState;->offset:I

    iget-object v5, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->offsetAttribute:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v5}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->startOffset()I

    move-result v5

    add-int v3, v4, v5

    .line 216
    .local v3, "startOffset":I
    iget-object v4, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iget v4, v4, Lorg/apache/lucene/index/FieldInvertState;->offset:I

    iget-object v5, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->offsetAttribute:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v5}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->endOffset()I

    move-result v5

    add-int v0, v4, v5

    .line 218
    .local v0, "endOffset":I
    iget-object v4, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    iget-object v5, p1, Lorg/apache/lucene/index/TermVectorsConsumerPerField$TermVectorsPostingsArray;->lastOffsets:[I

    aget v5, v5, p2

    sub-int v5, v3, v5

    invoke-virtual {v4, v9, v5}, Lorg/apache/lucene/index/TermsHashPerField;->writeVInt(II)V

    .line 219
    iget-object v4, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    sub-int v5, v0, v3

    invoke-virtual {v4, v9, v5}, Lorg/apache/lucene/index/TermsHashPerField;->writeVInt(II)V

    .line 220
    iget-object v4, p1, Lorg/apache/lucene/index/TermVectorsConsumerPerField$TermVectorsPostingsArray;->lastOffsets:[I

    aput v0, v4, p2

    .line 223
    .end local v0    # "endOffset":I
    .end local v3    # "startOffset":I
    :cond_0
    iget-boolean v4, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->doVectorPositions:Z

    if-eqz v4, :cond_1

    .line 225
    iget-object v4, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->payloadAttribute:Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

    if-nez v4, :cond_2

    .line 226
    const/4 v1, 0x0

    .line 231
    .local v1, "payload":Lorg/apache/lucene/util/BytesRef;
    :goto_0
    iget-object v4, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iget v4, v4, Lorg/apache/lucene/index/FieldInvertState;->position:I

    iget-object v5, p1, Lorg/apache/lucene/index/TermVectorsConsumerPerField$TermVectorsPostingsArray;->lastPositions:[I

    aget v5, v5, p2

    sub-int v2, v4, v5

    .line 232
    .local v2, "pos":I
    if-eqz v1, :cond_3

    iget v4, v1, Lorg/apache/lucene/util/BytesRef;->length:I

    if-lez v4, :cond_3

    .line 233
    iget-object v4, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    shl-int/lit8 v5, v2, 0x1

    or-int/lit8 v5, v5, 0x1

    invoke-virtual {v4, v8, v5}, Lorg/apache/lucene/index/TermsHashPerField;->writeVInt(II)V

    .line 234
    iget-object v4, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    iget v5, v1, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-virtual {v4, v8, v5}, Lorg/apache/lucene/index/TermsHashPerField;->writeVInt(II)V

    .line 235
    iget-object v4, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    iget-object v5, v1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v6, v1, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget v7, v1, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-virtual {v4, v8, v5, v6, v7}, Lorg/apache/lucene/index/TermsHashPerField;->writeBytes(I[BII)V

    .line 236
    iput-boolean v9, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->hasPayloads:Z

    .line 240
    :goto_1
    iget-object v4, p1, Lorg/apache/lucene/index/TermVectorsConsumerPerField$TermVectorsPostingsArray;->lastPositions:[I

    iget-object v5, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iget v5, v5, Lorg/apache/lucene/index/FieldInvertState;->position:I

    aput v5, v4, p2

    .line 242
    .end local v1    # "payload":Lorg/apache/lucene/util/BytesRef;
    .end local v2    # "pos":I
    :cond_1
    return-void

    .line 228
    :cond_2
    iget-object v4, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->payloadAttribute:Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

    invoke-interface {v4}, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;->getPayload()Lorg/apache/lucene/util/BytesRef;

    move-result-object v1

    .restart local v1    # "payload":Lorg/apache/lucene/util/BytesRef;
    goto :goto_0

    .line 238
    .restart local v2    # "pos":I
    :cond_3
    iget-object v4, p0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    shl-int/lit8 v5, v2, 0x1

    invoke-virtual {v4, v8, v5}, Lorg/apache/lucene/index/TermsHashPerField;->writeVInt(II)V

    goto :goto_1
.end method
