.class Lorg/apache/lucene/index/SortedDocValuesWriter;
.super Lorg/apache/lucene/index/DocValuesWriter;
.source "SortedDocValuesWriter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/SortedDocValuesWriter$OrdsIterator;,
        Lorg/apache/lucene/index/SortedDocValuesWriter$ValuesIterator;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final EMPTY:Lorg/apache/lucene/util/BytesRef;


# instance fields
.field private bytesUsed:J

.field private final fieldInfo:Lorg/apache/lucene/index/FieldInfo;

.field final hash:Lorg/apache/lucene/util/BytesRefHash;

.field private final iwBytesUsed:Lorg/apache/lucene/util/Counter;

.field private pending:Lorg/apache/lucene/util/packed/AppendingLongBuffer;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 37
    const-class v0, Lorg/apache/lucene/index/SortedDocValuesWriter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/SortedDocValuesWriter;->$assertionsDisabled:Z

    .line 44
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    sget-object v1, Lorg/apache/lucene/util/BytesRef;->EMPTY_BYTES:[B

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>([B)V

    sput-object v0, Lorg/apache/lucene/index/SortedDocValuesWriter;->EMPTY:Lorg/apache/lucene/util/BytesRef;

    return-void

    .line 37
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/util/Counter;)V
    .locals 4
    .param p1, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;
    .param p2, "iwBytesUsed"    # Lorg/apache/lucene/util/Counter;

    .prologue
    const/16 v3, 0x10

    .line 46
    invoke-direct {p0}, Lorg/apache/lucene/index/DocValuesWriter;-><init>()V

    .line 47
    iput-object p1, p0, Lorg/apache/lucene/index/SortedDocValuesWriter;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    .line 48
    iput-object p2, p0, Lorg/apache/lucene/index/SortedDocValuesWriter;->iwBytesUsed:Lorg/apache/lucene/util/Counter;

    .line 49
    new-instance v0, Lorg/apache/lucene/util/BytesRefHash;

    .line 50
    new-instance v1, Lorg/apache/lucene/util/ByteBlockPool;

    .line 51
    new-instance v2, Lorg/apache/lucene/util/ByteBlockPool$DirectTrackingAllocator;

    invoke-direct {v2, p2}, Lorg/apache/lucene/util/ByteBlockPool$DirectTrackingAllocator;-><init>(Lorg/apache/lucene/util/Counter;)V

    .line 50
    invoke-direct {v1, v2}, Lorg/apache/lucene/util/ByteBlockPool;-><init>(Lorg/apache/lucene/util/ByteBlockPool$Allocator;)V

    .line 53
    new-instance v2, Lorg/apache/lucene/util/BytesRefHash$DirectBytesStartArray;

    invoke-direct {v2, v3, p2}, Lorg/apache/lucene/util/BytesRefHash$DirectBytesStartArray;-><init>(ILorg/apache/lucene/util/Counter;)V

    invoke-direct {v0, v1, v3, v2}, Lorg/apache/lucene/util/BytesRefHash;-><init>(Lorg/apache/lucene/util/ByteBlockPool;ILorg/apache/lucene/util/BytesRefHash$BytesStartArray;)V

    .line 49
    iput-object v0, p0, Lorg/apache/lucene/index/SortedDocValuesWriter;->hash:Lorg/apache/lucene/util/BytesRefHash;

    .line 54
    new-instance v0, Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    invoke-direct {v0}, Lorg/apache/lucene/util/packed/AppendingLongBuffer;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/SortedDocValuesWriter;->pending:Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    .line 55
    iget-object v0, p0, Lorg/apache/lucene/index/SortedDocValuesWriter;->pending:Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    invoke-virtual {v0}, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->ramBytesUsed()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/lucene/index/SortedDocValuesWriter;->bytesUsed:J

    .line 56
    iget-wide v0, p0, Lorg/apache/lucene/index/SortedDocValuesWriter;->bytesUsed:J

    invoke-virtual {p2, v0, v1}, Lorg/apache/lucene/util/Counter;->addAndGet(J)J

    .line 57
    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/index/SortedDocValuesWriter;)Lorg/apache/lucene/util/packed/AppendingLongBuffer;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lorg/apache/lucene/index/SortedDocValuesWriter;->pending:Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    return-object v0
.end method

.method private addOneValue(Lorg/apache/lucene/util/BytesRef;)V
    .locals 4
    .param p1, "value"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 86
    iget-object v1, p0, Lorg/apache/lucene/index/SortedDocValuesWriter;->hash:Lorg/apache/lucene/util/BytesRefHash;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/util/BytesRefHash;->add(Lorg/apache/lucene/util/BytesRef;)I

    move-result v0

    .line 87
    .local v0, "termID":I
    if-gez v0, :cond_0

    .line 88
    neg-int v1, v0

    add-int/lit8 v0, v1, -0x1

    .line 97
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/index/SortedDocValuesWriter;->pending:Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->add(J)V

    .line 98
    invoke-direct {p0}, Lorg/apache/lucene/index/SortedDocValuesWriter;->updateBytesUsed()V

    .line 99
    return-void

    .line 94
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/SortedDocValuesWriter;->iwBytesUsed:Lorg/apache/lucene/util/Counter;

    const-wide/16 v2, 0x8

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/util/Counter;->addAndGet(J)J

    goto :goto_0
.end method

.method private updateBytesUsed()V
    .locals 6

    .prologue
    .line 102
    iget-object v2, p0, Lorg/apache/lucene/index/SortedDocValuesWriter;->pending:Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    invoke-virtual {v2}, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->ramBytesUsed()J

    move-result-wide v0

    .line 103
    .local v0, "newBytesUsed":J
    iget-object v2, p0, Lorg/apache/lucene/index/SortedDocValuesWriter;->iwBytesUsed:Lorg/apache/lucene/util/Counter;

    iget-wide v4, p0, Lorg/apache/lucene/index/SortedDocValuesWriter;->bytesUsed:J

    sub-long v4, v0, v4

    invoke-virtual {v2, v4, v5}, Lorg/apache/lucene/util/Counter;->addAndGet(J)J

    .line 104
    iput-wide v0, p0, Lorg/apache/lucene/index/SortedDocValuesWriter;->bytesUsed:J

    .line 105
    return-void
.end method


# virtual methods
.method public abort()V
    .locals 0

    .prologue
    .line 142
    return-void
.end method

.method public addValue(ILorg/apache/lucene/util/BytesRef;)V
    .locals 5
    .param p1, "docID"    # I
    .param p2, "value"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    const/16 v4, 0x7ffe

    .line 60
    int-to-long v0, p1

    iget-object v2, p0, Lorg/apache/lucene/index/SortedDocValuesWriter;->pending:Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    invoke-virtual {v2}, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->size()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 61
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DocValuesField \""

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/apache/lucene/index/SortedDocValuesWriter;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    iget-object v2, v2, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\" appears more than once in this document (only one value is allowed per field)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 63
    :cond_0
    if-nez p2, :cond_1

    .line 64
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "field \""

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/apache/lucene/index/SortedDocValuesWriter;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    iget-object v2, v2, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\": null value not allowed"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 66
    :cond_1
    iget v0, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    if-le v0, v4, :cond_3

    .line 67
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DocValuesField \""

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/apache/lucene/index/SortedDocValuesWriter;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    iget-object v2, v2, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\" is too large, must be <= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 72
    :cond_2
    sget-object v0, Lorg/apache/lucene/index/SortedDocValuesWriter;->EMPTY:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v0}, Lorg/apache/lucene/index/SortedDocValuesWriter;->addOneValue(Lorg/apache/lucene/util/BytesRef;)V

    .line 71
    :cond_3
    iget-object v0, p0, Lorg/apache/lucene/index/SortedDocValuesWriter;->pending:Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    invoke-virtual {v0}, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->size()J

    move-result-wide v0

    int-to-long v2, p1

    cmp-long v0, v0, v2

    if-ltz v0, :cond_2

    .line 75
    invoke-direct {p0, p2}, Lorg/apache/lucene/index/SortedDocValuesWriter;->addOneValue(Lorg/apache/lucene/util/BytesRef;)V

    .line 76
    return-void
.end method

.method public finish(I)V
    .locals 4
    .param p1, "maxDoc"    # I

    .prologue
    .line 80
    :goto_0
    iget-object v0, p0, Lorg/apache/lucene/index/SortedDocValuesWriter;->pending:Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    invoke-virtual {v0}, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->size()J

    move-result-wide v0

    int-to-long v2, p1

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 83
    return-void

    .line 81
    :cond_0
    sget-object v0, Lorg/apache/lucene/index/SortedDocValuesWriter;->EMPTY:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v0}, Lorg/apache/lucene/index/SortedDocValuesWriter;->addOneValue(Lorg/apache/lucene/util/BytesRef;)V

    goto :goto_0
.end method

.method public flush(Lorg/apache/lucene/index/SegmentWriteState;Lorg/apache/lucene/codecs/DocValuesConsumer;)V
    .locals 10
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentWriteState;
    .param p2, "dvConsumer"    # Lorg/apache/lucene/codecs/DocValuesConsumer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 109
    iget-object v5, p1, Lorg/apache/lucene/index/SegmentWriteState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v5}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v0

    .line 111
    .local v0, "maxDoc":I
    sget-boolean v5, Lorg/apache/lucene/index/SortedDocValuesWriter;->$assertionsDisabled:Z

    if-nez v5, :cond_0

    iget-object v5, p0, Lorg/apache/lucene/index/SortedDocValuesWriter;->pending:Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    invoke-virtual {v5}, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->size()J

    move-result-wide v6

    int-to-long v8, v0

    cmp-long v5, v6, v8

    if-eqz v5, :cond_0

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 112
    :cond_0
    iget-object v5, p0, Lorg/apache/lucene/index/SortedDocValuesWriter;->hash:Lorg/apache/lucene/util/BytesRefHash;

    invoke-virtual {v5}, Lorg/apache/lucene/util/BytesRefHash;->size()I

    move-result v4

    .line 114
    .local v4, "valueCount":I
    iget-object v5, p0, Lorg/apache/lucene/index/SortedDocValuesWriter;->hash:Lorg/apache/lucene/util/BytesRefHash;

    invoke-static {}, Lorg/apache/lucene/util/BytesRef;->getUTF8SortedAsUnicodeComparator()Ljava/util/Comparator;

    move-result-object v6

    invoke-virtual {v5, v6}, Lorg/apache/lucene/util/BytesRefHash;->sort(Ljava/util/Comparator;)[I

    move-result-object v3

    .line 115
    .local v3, "sortedValues":[I
    new-array v2, v4, [I

    .line 117
    .local v2, "ordMap":[I
    const/4 v1, 0x0

    .local v1, "ord":I
    :goto_0
    if-lt v1, v4, :cond_1

    .line 121
    iget-object v5, p0, Lorg/apache/lucene/index/SortedDocValuesWriter;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    .line 124
    new-instance v6, Lorg/apache/lucene/index/SortedDocValuesWriter$1;

    invoke-direct {v6, p0, v3, v4}, Lorg/apache/lucene/index/SortedDocValuesWriter$1;-><init>(Lorg/apache/lucene/index/SortedDocValuesWriter;[II)V

    .line 132
    new-instance v7, Lorg/apache/lucene/index/SortedDocValuesWriter$2;

    invoke-direct {v7, p0, v2, v0}, Lorg/apache/lucene/index/SortedDocValuesWriter$2;-><init>(Lorg/apache/lucene/index/SortedDocValuesWriter;[II)V

    .line 121
    invoke-virtual {p2, v5, v6, v7}, Lorg/apache/lucene/codecs/DocValuesConsumer;->addSortedField(Lorg/apache/lucene/index/FieldInfo;Ljava/lang/Iterable;Ljava/lang/Iterable;)V

    .line 138
    return-void

    .line 118
    :cond_1
    aget v5, v3, v1

    aput v1, v2, v5

    .line 117
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
