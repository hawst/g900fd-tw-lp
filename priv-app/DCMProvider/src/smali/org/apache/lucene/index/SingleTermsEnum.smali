.class public final Lorg/apache/lucene/index/SingleTermsEnum;
.super Lorg/apache/lucene/index/FilteredTermsEnum;
.source "SingleTermsEnum.java"


# instance fields
.field private final singleRef:Lorg/apache/lucene/util/BytesRef;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/TermsEnum;Lorg/apache/lucene/util/BytesRef;)V
    .locals 0
    .param p1, "tenum"    # Lorg/apache/lucene/index/TermsEnum;
    .param p2, "termText"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/FilteredTermsEnum;-><init>(Lorg/apache/lucene/index/TermsEnum;)V

    .line 42
    iput-object p2, p0, Lorg/apache/lucene/index/SingleTermsEnum;->singleRef:Lorg/apache/lucene/util/BytesRef;

    .line 43
    invoke-virtual {p0, p2}, Lorg/apache/lucene/index/SingleTermsEnum;->setInitialSeekTerm(Lorg/apache/lucene/util/BytesRef;)V

    .line 44
    return-void
.end method


# virtual methods
.method protected accept(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;
    .locals 1
    .param p1, "term"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 48
    iget-object v0, p0, Lorg/apache/lucene/index/SingleTermsEnum;->singleRef:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {p1, v0}, Lorg/apache/lucene/util/BytesRef;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;->YES:Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;->END:Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

    goto :goto_0
.end method
