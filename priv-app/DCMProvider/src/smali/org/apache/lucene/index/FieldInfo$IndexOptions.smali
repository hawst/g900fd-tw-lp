.class public final enum Lorg/apache/lucene/index/FieldInfo$IndexOptions;
.super Ljava/lang/Enum;
.source "FieldInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/FieldInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "IndexOptions"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/lucene/index/FieldInfo$IndexOptions;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum DOCS_AND_FREQS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

.field public static final enum DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

.field public static final enum DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

.field public static final enum DOCS_ONLY:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

.field private static final synthetic ENUM$VALUES:[Lorg/apache/lucene/index/FieldInfo$IndexOptions;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 55
    new-instance v0, Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    const-string v1, "DOCS_ONLY"

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/index/FieldInfo$IndexOptions;-><init>(Ljava/lang/String;I)V

    .line 64
    sput-object v0, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_ONLY:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    .line 65
    new-instance v0, Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    const-string v1, "DOCS_AND_FREQS"

    invoke-direct {v0, v1, v3}, Lorg/apache/lucene/index/FieldInfo$IndexOptions;-><init>(Ljava/lang/String;I)V

    .line 70
    sput-object v0, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    .line 71
    new-instance v0, Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    const-string v1, "DOCS_AND_FREQS_AND_POSITIONS"

    invoke-direct {v0, v1, v4}, Lorg/apache/lucene/index/FieldInfo$IndexOptions;-><init>(Ljava/lang/String;I)V

    .line 76
    sput-object v0, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    .line 77
    new-instance v0, Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    const-string v1, "DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS"

    invoke-direct {v0, v1, v5}, Lorg/apache/lucene/index/FieldInfo$IndexOptions;-><init>(Ljava/lang/String;I)V

    .line 81
    sput-object v0, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    .line 54
    const/4 v0, 0x4

    new-array v0, v0, [Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    sget-object v1, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_ONLY:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    aput-object v1, v0, v2

    sget-object v1, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    aput-object v1, v0, v3

    sget-object v1, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    aput-object v1, v0, v4

    sget-object v1, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    aput-object v1, v0, v5

    sput-object v0, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->ENUM$VALUES:[Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    return-object v0
.end method

.method public static values()[Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->ENUM$VALUES:[Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    array-length v1, v0

    new-array v2, v1, [Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
