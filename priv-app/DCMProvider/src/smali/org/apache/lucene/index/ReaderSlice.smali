.class public final Lorg/apache/lucene/index/ReaderSlice;
.super Ljava/lang/Object;
.source "ReaderSlice.java"


# static fields
.field public static final EMPTY_ARRAY:[Lorg/apache/lucene/index/ReaderSlice;


# instance fields
.field public final length:I

.field public final readerIndex:I

.field public final start:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x0

    new-array v0, v0, [Lorg/apache/lucene/index/ReaderSlice;

    sput-object v0, Lorg/apache/lucene/index/ReaderSlice;->EMPTY_ARRAY:[Lorg/apache/lucene/index/ReaderSlice;

    return-void
.end method

.method public constructor <init>(III)V
    .locals 0
    .param p1, "start"    # I
    .param p2, "length"    # I
    .param p3, "readerIndex"    # I

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput p1, p0, Lorg/apache/lucene/index/ReaderSlice;->start:I

    .line 42
    iput p2, p0, Lorg/apache/lucene/index/ReaderSlice;->length:I

    .line 43
    iput p3, p0, Lorg/apache/lucene/index/ReaderSlice;->readerIndex:I

    .line 44
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 48
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "slice start="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lorg/apache/lucene/index/ReaderSlice;->start:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " length="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/index/ReaderSlice;->length:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " readerIndex="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/index/ReaderSlice;->readerIndex:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
