.class public Lorg/apache/lucene/index/ConcurrentMergeScheduler;
.super Lorg/apache/lucene/index/MergeScheduler;
.source "ConcurrentMergeScheduler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field protected static final compareByMergeDocCount:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field protected dir:Lorg/apache/lucene/store/Directory;

.field private maxMergeCount:I

.field private maxThreadCount:I

.field protected mergeThreadCount:I

.field private mergeThreadPriority:I

.field protected mergeThreads:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;",
            ">;"
        }
    .end annotation
.end field

.field private suppressExceptions:Z

.field protected writer:Lorg/apache/lucene/index/IndexWriter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-class v0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->$assertionsDisabled:Z

    .line 147
    new-instance v0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$1;

    invoke-direct {v0}, Lorg/apache/lucene/index/ConcurrentMergeScheduler$1;-><init>()V

    sput-object v0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->compareByMergeDocCount:Ljava/util/Comparator;

    .line 158
    return-void

    .line 46
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 80
    invoke-direct {p0}, Lorg/apache/lucene/index/MergeScheduler;-><init>()V

    .line 48
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->mergeThreadPriority:I

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->mergeThreads:Ljava/util/List;

    .line 62
    const/4 v0, 0x1

    iput v0, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->maxThreadCount:I

    .line 66
    const/4 v0, 0x2

    iput v0, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->maxMergeCount:I

    .line 81
    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/index/ConcurrentMergeScheduler;)Z
    .locals 1

    .prologue
    .line 544
    iget-boolean v0, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->suppressExceptions:Z

    return v0
.end method

.method private declared-synchronized initMergeThreadPriority()V
    .locals 3

    .prologue
    const/16 v2, 0xa

    .line 247
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->mergeThreadPriority:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 250
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getPriority()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->mergeThreadPriority:I

    .line 251
    iget v0, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->mergeThreadPriority:I

    if-le v0, v2, :cond_0

    .line 252
    const/16 v0, 0xa

    iput v0, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->mergeThreadPriority:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 254
    :cond_0
    monitor-exit p0

    return-void

    .line 247
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method clearSuppressExceptions()V
    .locals 1

    .prologue
    .line 553
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->suppressExceptions:Z

    .line 554
    return-void
.end method

.method public clone()Lorg/apache/lucene/index/MergeScheduler;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 567
    invoke-super {p0}, Lorg/apache/lucene/index/MergeScheduler;->clone()Lorg/apache/lucene/index/MergeScheduler;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;

    .line 568
    .local v0, "clone":Lorg/apache/lucene/index/ConcurrentMergeScheduler;
    iput-object v1, v0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->writer:Lorg/apache/lucene/index/IndexWriter;

    .line 569
    iput-object v1, v0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->dir:Lorg/apache/lucene/store/Directory;

    .line 570
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->mergeThreads:Ljava/util/List;

    .line 571
    return-object v0
.end method

.method public close()V
    .locals 0

    .prologue
    .line 258
    invoke-virtual {p0}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->sync()V

    .line 259
    return-void
.end method

.method protected doMerge(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V
    .locals 1
    .param p1, "merge"    # Lorg/apache/lucene/index/MergePolicy$OneMerge;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 401
    iget-object v0, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/IndexWriter;->merge(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    .line 402
    return-void
.end method

.method public getMaxMergeCount()I
    .locals 1

    .prologue
    .line 121
    iget v0, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->maxMergeCount:I

    return v0
.end method

.method public getMaxThreadCount()I
    .locals 1

    .prologue
    .line 100
    iget v0, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->maxThreadCount:I

    return v0
.end method

.method protected declared-synchronized getMergeThread(Lorg/apache/lucene/index/IndexWriter;Lorg/apache/lucene/index/MergePolicy$OneMerge;)Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;
    .locals 4
    .param p1, "writer"    # Lorg/apache/lucene/index/IndexWriter;
    .param p2, "merge"    # Lorg/apache/lucene/index/MergePolicy$OneMerge;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 406
    monitor-enter p0

    :try_start_0
    new-instance v0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;

    invoke-direct {v0, p0, p1, p2}, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;-><init>(Lorg/apache/lucene/index/ConcurrentMergeScheduler;Lorg/apache/lucene/index/IndexWriter;Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    .line 407
    .local v0, "thread":Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;
    iget v1, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->mergeThreadPriority:I

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->setThreadPriority(I)V

    .line 408
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->setDaemon(Z)V

    .line 409
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Lucene Merge Thread #"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->mergeThreadCount:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->mergeThreadCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->setName(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 410
    monitor-exit p0

    return-object v0

    .line 406
    .end local v0    # "thread":Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized getMergeThreadPriority()I
    .locals 1

    .prologue
    .line 129
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->initMergeThreadPriority()V

    .line 130
    iget v0, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->mergeThreadPriority:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    .line 129
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected handleMergeException(Ljava/lang/Throwable;)V
    .locals 4
    .param p1, "exc"    # Ljava/lang/Throwable;

    .prologue
    .line 537
    const-wide/16 v2, 0xfa

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 541
    new-instance v1, Lorg/apache/lucene/index/MergePolicy$MergeException;

    iget-object v2, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->dir:Lorg/apache/lucene/store/Directory;

    invoke-direct {v1, p1, v2}, Lorg/apache/lucene/index/MergePolicy$MergeException;-><init>(Ljava/lang/Throwable;Lorg/apache/lucene/store/Directory;)V

    throw v1

    .line 538
    :catch_0
    move-exception v0

    .line 539
    .local v0, "ie":Ljava/lang/InterruptedException;
    new-instance v1, Lorg/apache/lucene/util/ThreadInterruptedException;

    invoke-direct {v1, v0}, Lorg/apache/lucene/util/ThreadInterruptedException;-><init>(Ljava/lang/InterruptedException;)V

    throw v1
.end method

.method public declared-synchronized merge(Lorg/apache/lucene/index/IndexWriter;)V
    .locals 10
    .param p1, "writer"    # Lorg/apache/lucene/index/IndexWriter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 309
    monitor-enter p0

    :try_start_0
    sget-boolean v6, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->$assertionsDisabled:Z

    if-nez v6, :cond_0

    invoke-static {p1}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6

    .line 311
    :cond_0
    :try_start_1
    iput-object p1, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->writer:Lorg/apache/lucene/index/IndexWriter;

    .line 313
    invoke-direct {p0}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->initMergeThreadPriority()V

    .line 315
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexWriter;->getDirectory()Lorg/apache/lucene/store/Directory;

    move-result-object v6

    iput-object v6, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->dir:Lorg/apache/lucene/store/Directory;

    .line 324
    invoke-virtual {p0}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->verbose()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 325
    const-string v6, "now merge"

    invoke-virtual {p0, v6}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->message(Ljava/lang/String;)V

    .line 326
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "  index: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexWriter;->segString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->message(Ljava/lang/String;)V

    .line 333
    :cond_1
    :goto_0
    const-wide/16 v4, 0x0

    .line 334
    .local v4, "startStallTime":J
    :goto_1
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexWriter;->hasPendingMerges()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {p0}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->mergeThreadCount()I

    move-result v6

    iget v7, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->maxMergeCount:I

    if-ge v6, v7, :cond_5

    .line 355
    :cond_2
    invoke-virtual {p0}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->verbose()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 356
    const-wide/16 v6, 0x0

    cmp-long v6, v4, v6

    if-eqz v6, :cond_3

    .line 357
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "  stalled for "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long/2addr v8, v4

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " msec"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->message(Ljava/lang/String;)V

    .line 361
    :cond_3
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexWriter;->getNextMerge()Lorg/apache/lucene/index/MergePolicy$OneMerge;

    move-result-object v1

    .line 362
    .local v1, "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    if-nez v1, :cond_7

    .line 363
    invoke-virtual {p0}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->verbose()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 364
    const-string v6, "  no more merges pending; now return"

    invoke-virtual {p0, v6}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->message(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 366
    :cond_4
    monitor-exit p0

    return-void

    .line 344
    .end local v1    # "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    :cond_5
    :try_start_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 345
    invoke-virtual {p0}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->verbose()Z

    move-result v6

    if-eqz v6, :cond_6

    .line 346
    const-string v6, "    too many merges; stalling..."

    invoke-virtual {p0, v6}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->message(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 349
    :cond_6
    :try_start_3
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 350
    :catch_0
    move-exception v0

    .line 351
    .local v0, "ie":Ljava/lang/InterruptedException;
    :try_start_4
    new-instance v6, Lorg/apache/lucene/util/ThreadInterruptedException;

    invoke-direct {v6, v0}, Lorg/apache/lucene/util/ThreadInterruptedException;-><init>(Ljava/lang/InterruptedException;)V

    throw v6
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 369
    .end local v0    # "ie":Ljava/lang/InterruptedException;
    .restart local v1    # "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    :cond_7
    const/4 v3, 0x0

    .line 371
    .local v3, "success":Z
    :try_start_5
    invoke-virtual {p0}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->verbose()Z

    move-result v6

    if-eqz v6, :cond_8

    .line 372
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "  consider merge "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, v1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    invoke-virtual {p1, v7}, Lorg/apache/lucene/index/IndexWriter;->segString(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->message(Ljava/lang/String;)V

    .line 377
    :cond_8
    invoke-virtual {p0, p1, v1}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->getMergeThread(Lorg/apache/lucene/index/IndexWriter;Lorg/apache/lucene/index/MergePolicy$OneMerge;)Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;

    move-result-object v2

    .line 378
    .local v2, "merger":Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;
    iget-object v6, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->mergeThreads:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 379
    invoke-virtual {p0}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->verbose()Z

    move-result v6

    if-eqz v6, :cond_9

    .line 380
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "    launch new thread ["

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->message(Ljava/lang/String;)V

    .line 383
    :cond_9
    invoke-virtual {v2}, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->start()V

    .line 388
    invoke-virtual {p0}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->updateMergeThreads()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 390
    const/4 v3, 0x1

    .line 392
    if-nez v3, :cond_1

    .line 393
    :try_start_6
    invoke-virtual {p1, v1}, Lorg/apache/lucene/index/IndexWriter;->mergeFinish(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    goto/16 :goto_0

    .line 391
    .end local v2    # "merger":Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;
    :catchall_1
    move-exception v6

    .line 392
    if-nez v3, :cond_a

    .line 393
    invoke-virtual {p1, v1}, Lorg/apache/lucene/index/IndexWriter;->mergeFinish(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    .line 395
    :cond_a
    throw v6
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0
.end method

.method protected declared-synchronized mergeThreadCount()I
    .locals 4

    .prologue
    .line 297
    monitor-enter p0

    const/4 v0, 0x0

    .line 298
    .local v0, "count":I
    :try_start_0
    iget-object v2, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->mergeThreads:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_1

    .line 303
    monitor-exit p0

    return v0

    .line 298
    :cond_1
    :try_start_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;

    .line 299
    .local v1, "mt":Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;
    invoke-virtual {v1}, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->isAlive()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v1}, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->getCurrentMerge()Lorg/apache/lucene/index/MergePolicy$OneMerge;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    if-eqz v3, :cond_0

    .line 300
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 297
    .end local v1    # "mt":Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method protected message(Ljava/lang/String;)V
    .locals 2
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 243
    iget-object v0, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->writer:Lorg/apache/lucene/index/IndexWriter;

    iget-object v0, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v1, "CMS"

    invoke-virtual {v0, v1, p1}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    return-void
.end method

.method public setMaxMergeCount(I)V
    .locals 3
    .param p1, "count"    # I

    .prologue
    .line 110
    const/4 v0, 0x1

    if-ge p1, v0, :cond_0

    .line 111
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "count should be at least 1"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 113
    :cond_0
    iget v0, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->maxThreadCount:I

    if-ge p1, v0, :cond_1

    .line 114
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "count should be >= maxThreadCount (= "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->maxThreadCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 116
    :cond_1
    iput p1, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->maxMergeCount:I

    .line 117
    return-void
.end method

.method public setMaxThreadCount(I)V
    .locals 3
    .param p1, "count"    # I

    .prologue
    .line 87
    const/4 v0, 0x1

    if-ge p1, v0, :cond_0

    .line 88
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "count should be at least 1"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 90
    :cond_0
    iget v0, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->maxMergeCount:I

    if-le p1, v0, :cond_1

    .line 91
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "count should be <= maxMergeCount (= "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->maxMergeCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 93
    :cond_1
    iput p1, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->maxThreadCount:I

    .line 94
    return-void
.end method

.method public declared-synchronized setMergeThreadPriority(I)V
    .locals 2
    .param p1, "pri"    # I

    .prologue
    .line 140
    monitor-enter p0

    const/16 v0, 0xa

    if-gt p1, v0, :cond_0

    const/4 v0, 0x1

    if-ge p1, v0, :cond_1

    .line 141
    :cond_0
    :try_start_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "priority must be in range 1 .. 10 inclusive"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 140
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 142
    :cond_1
    :try_start_1
    iput p1, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->mergeThreadPriority:I

    .line 143
    invoke-virtual {p0}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->updateMergeThreads()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 144
    monitor-exit p0

    return-void
.end method

.method setSuppressExceptions()V
    .locals 1

    .prologue
    .line 548
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->suppressExceptions:Z

    .line 549
    return-void
.end method

.method public sync()V
    .locals 6

    .prologue
    .line 263
    const/4 v1, 0x0

    .line 266
    .local v1, "interrupted":Z
    :goto_0
    const/4 v3, 0x0

    .line 267
    .local v3, "toSync":Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;
    :try_start_0
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 268
    :try_start_1
    iget-object v4, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->mergeThreads:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_1

    .line 267
    :goto_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 275
    if-eqz v3, :cond_3

    .line 277
    :try_start_2
    invoke-virtual {v3}, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->join()V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 278
    :catch_0
    move-exception v0

    .line 280
    .local v0, "ie":Ljava/lang/InterruptedException;
    const/4 v1, 0x1

    .line 265
    goto :goto_0

    .line 268
    .end local v0    # "ie":Ljava/lang/InterruptedException;
    :cond_1
    :try_start_3
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;

    .line 269
    .local v2, "t":Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;
    invoke-virtual {v2}, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->isAlive()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 270
    move-object v3, v2

    .line 271
    goto :goto_1

    .line 267
    .end local v2    # "t":Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;
    :catchall_0
    move-exception v4

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 286
    :catchall_1
    move-exception v4

    .line 288
    if-eqz v1, :cond_2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Thread;->interrupt()V

    .line 289
    :cond_2
    throw v4

    .line 288
    :cond_3
    if-eqz v1, :cond_4

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Thread;->interrupt()V

    .line 290
    :cond_4
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 558
    new-instance v0, Ljava/lang/StringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 559
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v1, "maxThreadCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->maxThreadCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 560
    const-string v1, "maxMergeCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->maxMergeCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 561
    const-string v1, "mergeThreadPriority="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->mergeThreadPriority:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 562
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method protected declared-synchronized updateMergeThreads()V
    .locals 9

    .prologue
    .line 170
    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 172
    .local v1, "activeMerges":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;>;"
    const/4 v6, 0x0

    .line 173
    .local v6, "threadIdx":I
    :goto_0
    iget-object v7, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->mergeThreads:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-lt v6, v7, :cond_0

    .line 187
    sget-object v7, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->compareByMergeDocCount:Ljava/util/Comparator;

    invoke-static {v1, v7}, Lorg/apache/lucene/util/CollectionUtil;->mergeSort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 189
    iget v5, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->mergeThreadPriority:I

    .line 190
    .local v5, "pri":I
    invoke-interface {v1}, Ljava/util/List;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 191
    .local v0, "activeMergeCount":I
    const/4 v6, 0x0

    :goto_1
    if-lt v6, v0, :cond_3

    .line 222
    monitor-exit p0

    return-void

    .line 174
    .end local v0    # "activeMergeCount":I
    .end local v5    # "pri":I
    :cond_0
    :try_start_1
    iget-object v7, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->mergeThreads:Ljava/util/List;

    invoke-interface {v7, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;

    .line 175
    .local v4, "mergeThread":Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;
    invoke-virtual {v4}, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->isAlive()Z

    move-result v7

    if-nez v7, :cond_1

    .line 177
    iget-object v7, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->mergeThreads:Ljava/util/List;

    invoke-interface {v7, v6}, Ljava/util/List;->remove(I)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 170
    .end local v1    # "activeMerges":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;>;"
    .end local v4    # "mergeThread":Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;
    .end local v6    # "threadIdx":I
    :catchall_0
    move-exception v7

    monitor-exit p0

    throw v7

    .line 180
    .restart local v1    # "activeMerges":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;>;"
    .restart local v4    # "mergeThread":Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;
    .restart local v6    # "threadIdx":I
    :cond_1
    :try_start_2
    invoke-virtual {v4}, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->getCurrentMerge()Lorg/apache/lucene/index/MergePolicy$OneMerge;

    move-result-object v7

    if-eqz v7, :cond_2

    .line 181
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 183
    :cond_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 192
    .end local v4    # "mergeThread":Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;
    .restart local v0    # "activeMergeCount":I
    .restart local v5    # "pri":I
    :cond_3
    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;

    .line 193
    .restart local v4    # "mergeThread":Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;
    invoke-virtual {v4}, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->getCurrentMerge()Lorg/apache/lucene/index/MergePolicy$OneMerge;

    move-result-object v3

    .line 194
    .local v3, "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    if-nez v3, :cond_5

    .line 191
    :cond_4
    :goto_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 199
    :cond_5
    iget v7, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->maxThreadCount:I

    sub-int v7, v0, v7

    if-ge v6, v7, :cond_9

    const/4 v2, 0x1

    .line 201
    .local v2, "doPause":Z
    :goto_3
    invoke-virtual {p0}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->verbose()Z

    move-result v7

    if-eqz v7, :cond_6

    .line 202
    invoke-virtual {v3}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->getPause()Z

    move-result v7

    if-eq v2, v7, :cond_6

    .line 203
    if-eqz v2, :cond_a

    .line 204
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "pause thread "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->message(Ljava/lang/String;)V

    .line 210
    :cond_6
    :goto_4
    invoke-virtual {v3}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->getPause()Z

    move-result v7

    if-eq v2, v7, :cond_7

    .line 211
    invoke-virtual {v3, v2}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->setPause(Z)V

    .line 214
    :cond_7
    if-nez v2, :cond_4

    .line 215
    invoke-virtual {p0}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->verbose()Z

    move-result v7

    if-eqz v7, :cond_8

    .line 216
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "set priority of merge thread "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " to "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->message(Ljava/lang/String;)V

    .line 218
    :cond_8
    invoke-virtual {v4, v5}, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->setThreadPriority(I)V

    .line 219
    const/16 v7, 0xa

    add-int/lit8 v8, v5, 0x1

    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    move-result v5

    goto :goto_2

    .line 199
    .end local v2    # "doPause":Z
    :cond_9
    const/4 v2, 0x0

    goto :goto_3

    .line 206
    .restart local v2    # "doPause":Z
    :cond_a
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "unpause thread "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->message(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_4
.end method

.method protected verbose()Z
    .locals 2

    .prologue
    .line 235
    iget-object v0, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->writer:Lorg/apache/lucene/index/IndexWriter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->writer:Lorg/apache/lucene/index/IndexWriter;

    iget-object v0, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v1, "CMS"

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
