.class final Lorg/apache/lucene/index/MergedIterator;
.super Ljava/lang/Object;
.source "MergedIterator.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/MergedIterator$SubIterator;,
        Lorg/apache/lucene/index/MergedIterator$TermMergeQueue;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Ljava/lang/Comparable",
        "<TT;>;>",
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TT;>;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private current:Ljava/lang/Comparable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private numTop:I

.field private final queue:Lorg/apache/lucene/index/MergedIterator$TermMergeQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/index/MergedIterator$TermMergeQueue",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final top:[Lorg/apache/lucene/index/MergedIterator$SubIterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lorg/apache/lucene/index/MergedIterator$SubIterator",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-class v0, Lorg/apache/lucene/index/MergedIterator;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/MergedIterator;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public varargs constructor <init>([Ljava/util/Iterator;)V
    .locals 7
    .param p1, "iterators"    # [Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/util/Iterator",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 51
    .local p0, "this":Lorg/apache/lucene/index/MergedIterator;, "Lorg/apache/lucene/index/MergedIterator<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    new-instance v4, Lorg/apache/lucene/index/MergedIterator$TermMergeQueue;

    array-length v5, p1

    invoke-direct {v4, v5}, Lorg/apache/lucene/index/MergedIterator$TermMergeQueue;-><init>(I)V

    iput-object v4, p0, Lorg/apache/lucene/index/MergedIterator;->queue:Lorg/apache/lucene/index/MergedIterator$TermMergeQueue;

    .line 53
    array-length v4, p1

    new-array v4, v4, [Lorg/apache/lucene/index/MergedIterator$SubIterator;

    iput-object v4, p0, Lorg/apache/lucene/index/MergedIterator;->top:[Lorg/apache/lucene/index/MergedIterator$SubIterator;

    .line 54
    const/4 v0, 0x0

    .line 55
    .local v0, "index":I
    array-length v6, p1

    const/4 v4, 0x0

    move v5, v4

    move v1, v0

    .end local v0    # "index":I
    .local v1, "index":I
    :goto_0
    if-lt v5, v6, :cond_0

    .line 64
    return-void

    .line 55
    :cond_0
    aget-object v2, p1, v5

    .line 56
    .local v2, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<TT;>;"
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 57
    new-instance v3, Lorg/apache/lucene/index/MergedIterator$SubIterator;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Lorg/apache/lucene/index/MergedIterator$SubIterator;-><init>(Lorg/apache/lucene/index/MergedIterator$SubIterator;)V

    .line 58
    .local v3, "sub":Lorg/apache/lucene/index/MergedIterator$SubIterator;, "Lorg/apache/lucene/index/MergedIterator$SubIterator<TT;>;"
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Comparable;

    iput-object v4, v3, Lorg/apache/lucene/index/MergedIterator$SubIterator;->current:Ljava/lang/Comparable;

    .line 59
    iput-object v2, v3, Lorg/apache/lucene/index/MergedIterator$SubIterator;->iterator:Ljava/util/Iterator;

    .line 60
    add-int/lit8 v0, v1, 0x1

    .end local v1    # "index":I
    .restart local v0    # "index":I
    iput v1, v3, Lorg/apache/lucene/index/MergedIterator$SubIterator;->index:I

    .line 61
    iget-object v4, p0, Lorg/apache/lucene/index/MergedIterator;->queue:Lorg/apache/lucene/index/MergedIterator$TermMergeQueue;

    invoke-virtual {v4, v3}, Lorg/apache/lucene/index/MergedIterator$TermMergeQueue;->add(Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    .end local v3    # "sub":Lorg/apache/lucene/index/MergedIterator$SubIterator;, "Lorg/apache/lucene/index/MergedIterator$SubIterator<TT;>;"
    :goto_1
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    move v1, v0

    .end local v0    # "index":I
    .restart local v1    # "index":I
    goto :goto_0

    :cond_1
    move v0, v1

    .end local v1    # "index":I
    .restart local v0    # "index":I
    goto :goto_1
.end method

.method private pullTop()V
    .locals 4

    .prologue
    .local p0, "this":Lorg/apache/lucene/index/MergedIterator;, "Lorg/apache/lucene/index/MergedIterator<TT;>;"
    const/4 v3, 0x0

    .line 104
    sget-boolean v0, Lorg/apache/lucene/index/MergedIterator;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/index/MergedIterator;->numTop:I

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 106
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/MergedIterator;->top:[Lorg/apache/lucene/index/MergedIterator$SubIterator;

    iget v2, p0, Lorg/apache/lucene/index/MergedIterator;->numTop:I

    add-int/lit8 v0, v2, 0x1

    iput v0, p0, Lorg/apache/lucene/index/MergedIterator;->numTop:I

    iget-object v0, p0, Lorg/apache/lucene/index/MergedIterator;->queue:Lorg/apache/lucene/index/MergedIterator$TermMergeQueue;

    invoke-virtual {v0}, Lorg/apache/lucene/index/MergedIterator$TermMergeQueue;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/MergedIterator$SubIterator;

    aput-object v0, v1, v2

    .line 107
    iget-object v0, p0, Lorg/apache/lucene/index/MergedIterator;->queue:Lorg/apache/lucene/index/MergedIterator$TermMergeQueue;

    invoke-virtual {v0}, Lorg/apache/lucene/index/MergedIterator$TermMergeQueue;->size()I

    move-result v0

    if-eqz v0, :cond_1

    .line 108
    iget-object v0, p0, Lorg/apache/lucene/index/MergedIterator;->queue:Lorg/apache/lucene/index/MergedIterator$TermMergeQueue;

    invoke-virtual {v0}, Lorg/apache/lucene/index/MergedIterator$TermMergeQueue;->top()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/MergedIterator$SubIterator;

    iget-object v0, v0, Lorg/apache/lucene/index/MergedIterator$SubIterator;->current:Ljava/lang/Comparable;

    iget-object v1, p0, Lorg/apache/lucene/index/MergedIterator;->top:[Lorg/apache/lucene/index/MergedIterator$SubIterator;

    aget-object v1, v1, v3

    iget-object v1, v1, Lorg/apache/lucene/index/MergedIterator$SubIterator;->current:Ljava/lang/Comparable;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 112
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/index/MergedIterator;->top:[Lorg/apache/lucene/index/MergedIterator$SubIterator;

    aget-object v0, v0, v3

    iget-object v0, v0, Lorg/apache/lucene/index/MergedIterator$SubIterator;->current:Ljava/lang/Comparable;

    iput-object v0, p0, Lorg/apache/lucene/index/MergedIterator;->current:Ljava/lang/Comparable;

    .line 113
    return-void
.end method

.method private pushTop()V
    .locals 3

    .prologue
    .line 117
    .local p0, "this":Lorg/apache/lucene/index/MergedIterator;, "Lorg/apache/lucene/index/MergedIterator<TT;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lorg/apache/lucene/index/MergedIterator;->numTop:I

    if-lt v0, v1, :cond_0

    .line 126
    const/4 v1, 0x0

    iput v1, p0, Lorg/apache/lucene/index/MergedIterator;->numTop:I

    .line 127
    return-void

    .line 118
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/MergedIterator;->top:[Lorg/apache/lucene/index/MergedIterator$SubIterator;

    aget-object v1, v1, v0

    iget-object v1, v1, Lorg/apache/lucene/index/MergedIterator$SubIterator;->iterator:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 119
    iget-object v1, p0, Lorg/apache/lucene/index/MergedIterator;->top:[Lorg/apache/lucene/index/MergedIterator$SubIterator;

    aget-object v2, v1, v0

    iget-object v1, p0, Lorg/apache/lucene/index/MergedIterator;->top:[Lorg/apache/lucene/index/MergedIterator$SubIterator;

    aget-object v1, v1, v0

    iget-object v1, v1, Lorg/apache/lucene/index/MergedIterator$SubIterator;->iterator:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Comparable;

    iput-object v1, v2, Lorg/apache/lucene/index/MergedIterator$SubIterator;->current:Ljava/lang/Comparable;

    .line 120
    iget-object v1, p0, Lorg/apache/lucene/index/MergedIterator;->queue:Lorg/apache/lucene/index/MergedIterator$TermMergeQueue;

    iget-object v2, p0, Lorg/apache/lucene/index/MergedIterator;->top:[Lorg/apache/lucene/index/MergedIterator$SubIterator;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Lorg/apache/lucene/index/MergedIterator$TermMergeQueue;->add(Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 123
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/index/MergedIterator;->top:[Lorg/apache/lucene/index/MergedIterator$SubIterator;

    aget-object v1, v1, v0

    const/4 v2, 0x0

    iput-object v2, v1, Lorg/apache/lucene/index/MergedIterator$SubIterator;->current:Ljava/lang/Comparable;

    goto :goto_1
.end method


# virtual methods
.method public hasNext()Z
    .locals 3

    .prologue
    .local p0, "this":Lorg/apache/lucene/index/MergedIterator;, "Lorg/apache/lucene/index/MergedIterator<TT;>;"
    const/4 v1, 0x1

    .line 68
    iget-object v2, p0, Lorg/apache/lucene/index/MergedIterator;->queue:Lorg/apache/lucene/index/MergedIterator$TermMergeQueue;

    invoke-virtual {v2}, Lorg/apache/lucene/index/MergedIterator$TermMergeQueue;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 77
    :cond_0
    :goto_0
    return v1

    .line 72
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget v2, p0, Lorg/apache/lucene/index/MergedIterator;->numTop:I

    if-lt v0, v2, :cond_2

    .line 77
    const/4 v1, 0x0

    goto :goto_0

    .line 73
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/index/MergedIterator;->top:[Lorg/apache/lucene/index/MergedIterator$SubIterator;

    aget-object v2, v2, v0

    iget-object v2, v2, Lorg/apache/lucene/index/MergedIterator$SubIterator;->iterator:Ljava/util/Iterator;

    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 72
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public next()Ljava/lang/Comparable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 83
    .local p0, "this":Lorg/apache/lucene/index/MergedIterator;, "Lorg/apache/lucene/index/MergedIterator<TT;>;"
    invoke-direct {p0}, Lorg/apache/lucene/index/MergedIterator;->pushTop()V

    .line 86
    iget-object v0, p0, Lorg/apache/lucene/index/MergedIterator;->queue:Lorg/apache/lucene/index/MergedIterator$TermMergeQueue;

    invoke-virtual {v0}, Lorg/apache/lucene/index/MergedIterator$TermMergeQueue;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 87
    invoke-direct {p0}, Lorg/apache/lucene/index/MergedIterator;->pullTop()V

    .line 91
    :goto_0
    iget-object v0, p0, Lorg/apache/lucene/index/MergedIterator;->current:Ljava/lang/Comparable;

    if-nez v0, :cond_1

    .line 92
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 89
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/index/MergedIterator;->current:Ljava/lang/Comparable;

    goto :goto_0

    .line 94
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/index/MergedIterator;->current:Ljava/lang/Comparable;

    return-object v0
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/index/MergedIterator;->next()Ljava/lang/Comparable;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 99
    .local p0, "this":Lorg/apache/lucene/index/MergedIterator;, "Lorg/apache/lucene/index/MergedIterator<TT;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
