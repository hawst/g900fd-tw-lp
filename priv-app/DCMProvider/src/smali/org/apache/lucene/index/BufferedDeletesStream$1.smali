.class Lorg/apache/lucene/index/BufferedDeletesStream$1;
.super Ljava/lang/Object;
.source "BufferedDeletesStream.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/BufferedDeletesStream;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lorg/apache/lucene/index/SegmentInfoPerCommit;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    check-cast p2, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/index/BufferedDeletesStream$1;->compare(Lorg/apache/lucene/index/SegmentInfoPerCommit;Lorg/apache/lucene/index/SegmentInfoPerCommit;)I

    move-result v0

    return v0
.end method

.method public compare(Lorg/apache/lucene/index/SegmentInfoPerCommit;Lorg/apache/lucene/index/SegmentInfoPerCommit;)I
    .locals 8
    .param p1, "si1"    # Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .param p2, "si2"    # Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .prologue
    const-wide/16 v6, 0x0

    .line 137
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->getBufferedDeletesGen()J

    move-result-wide v2

    invoke-virtual {p2}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->getBufferedDeletesGen()J

    move-result-wide v4

    sub-long v0, v2, v4

    .line 138
    .local v0, "cmp":J
    cmp-long v2, v0, v6

    if-lez v2, :cond_0

    .line 139
    const/4 v2, 0x1

    .line 143
    :goto_0
    return v2

    .line 140
    :cond_0
    cmp-long v2, v0, v6

    if-gez v2, :cond_1

    .line 141
    const/4 v2, -0x1

    goto :goto_0

    .line 143
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method
