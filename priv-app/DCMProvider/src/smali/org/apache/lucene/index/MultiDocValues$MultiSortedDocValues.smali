.class public Lorg/apache/lucene/index/MultiDocValues$MultiSortedDocValues;
.super Lorg/apache/lucene/index/SortedDocValues;
.source "MultiDocValues.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/MultiDocValues;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MultiSortedDocValues"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field public final docStarts:[I

.field public final mapping:Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;

.field public final values:[Lorg/apache/lucene/index/SortedDocValues;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 369
    const-class v0, Lorg/apache/lucene/index/MultiDocValues;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/MultiDocValues$MultiSortedDocValues;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>([Lorg/apache/lucene/index/SortedDocValues;[ILorg/apache/lucene/index/MultiDocValues$OrdinalMap;)V
    .locals 2
    .param p1, "values"    # [Lorg/apache/lucene/index/SortedDocValues;
    .param p2, "docStarts"    # [I
    .param p3, "mapping"    # Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 378
    invoke-direct {p0}, Lorg/apache/lucene/index/SortedDocValues;-><init>()V

    .line 379
    sget-boolean v0, Lorg/apache/lucene/index/MultiDocValues$MultiSortedDocValues;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    array-length v0, p1

    iget-object v1, p3, Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;->ordDeltas:[Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;

    array-length v1, v1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 380
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/index/MultiDocValues$MultiSortedDocValues;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    array-length v0, p2

    array-length v1, p1

    add-int/lit8 v1, v1, 0x1

    if-eq v0, v1, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 381
    :cond_1
    iput-object p1, p0, Lorg/apache/lucene/index/MultiDocValues$MultiSortedDocValues;->values:[Lorg/apache/lucene/index/SortedDocValues;

    .line 382
    iput-object p2, p0, Lorg/apache/lucene/index/MultiDocValues$MultiSortedDocValues;->docStarts:[I

    .line 383
    iput-object p3, p0, Lorg/apache/lucene/index/MultiDocValues$MultiSortedDocValues;->mapping:Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;

    .line 384
    return-void
.end method


# virtual methods
.method public getOrd(I)I
    .locals 6
    .param p1, "docID"    # I

    .prologue
    .line 388
    iget-object v2, p0, Lorg/apache/lucene/index/MultiDocValues$MultiSortedDocValues;->docStarts:[I

    invoke-static {p1, v2}, Lorg/apache/lucene/index/ReaderUtil;->subIndex(I[I)I

    move-result v1

    .line 389
    .local v1, "subIndex":I
    iget-object v2, p0, Lorg/apache/lucene/index/MultiDocValues$MultiSortedDocValues;->values:[Lorg/apache/lucene/index/SortedDocValues;

    aget-object v2, v2, v1

    iget-object v3, p0, Lorg/apache/lucene/index/MultiDocValues$MultiSortedDocValues;->docStarts:[I

    aget v3, v3, v1

    sub-int v3, p1, v3

    invoke-virtual {v2, v3}, Lorg/apache/lucene/index/SortedDocValues;->getOrd(I)I

    move-result v0

    .line 390
    .local v0, "segmentOrd":I
    iget-object v2, p0, Lorg/apache/lucene/index/MultiDocValues$MultiSortedDocValues;->mapping:Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;

    int-to-long v4, v0

    invoke-virtual {v2, v1, v4, v5}, Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;->getGlobalOrd(IJ)J

    move-result-wide v2

    long-to-int v2, v2

    return v2
.end method

.method public getValueCount()I
    .locals 2

    .prologue
    .line 402
    iget-object v0, p0, Lorg/apache/lucene/index/MultiDocValues$MultiSortedDocValues;->mapping:Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;

    invoke-virtual {v0}, Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;->getValueCount()J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public lookupOrd(ILorg/apache/lucene/util/BytesRef;)V
    .locals 6
    .param p1, "ord"    # I
    .param p2, "result"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 395
    iget-object v2, p0, Lorg/apache/lucene/index/MultiDocValues$MultiSortedDocValues;->mapping:Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;

    int-to-long v4, p1

    invoke-virtual {v2, v4, v5}, Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;->getSegmentNumber(J)I

    move-result v1

    .line 396
    .local v1, "subIndex":I
    iget-object v2, p0, Lorg/apache/lucene/index/MultiDocValues$MultiSortedDocValues;->mapping:Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;

    int-to-long v4, p1

    invoke-virtual {v2, v1, v4, v5}, Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;->getSegmentOrd(IJ)J

    move-result-wide v2

    long-to-int v0, v2

    .line 397
    .local v0, "segmentOrd":I
    iget-object v2, p0, Lorg/apache/lucene/index/MultiDocValues$MultiSortedDocValues;->values:[Lorg/apache/lucene/index/SortedDocValues;

    aget-object v2, v2, v1

    invoke-virtual {v2, v0, p2}, Lorg/apache/lucene/index/SortedDocValues;->lookupOrd(ILorg/apache/lucene/util/BytesRef;)V

    .line 398
    return-void
.end method
