.class Lorg/apache/lucene/index/MergeState$DocMap$1;
.super Lorg/apache/lucene/index/MergeState$DocMap;
.source "MergeState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/index/MergeState$DocMap;->build(ILorg/apache/lucene/util/Bits;)Lorg/apache/lucene/index/MergeState$DocMap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic val$docMap:Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;

.field private final synthetic val$liveDocs:Lorg/apache/lucene/util/Bits;

.field private final synthetic val$maxDoc:I

.field private final synthetic val$numDeletedDocs:I


# direct methods
.method constructor <init>(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;II)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/index/MergeState$DocMap$1;->val$liveDocs:Lorg/apache/lucene/util/Bits;

    iput-object p2, p0, Lorg/apache/lucene/index/MergeState$DocMap$1;->val$docMap:Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;

    iput p3, p0, Lorg/apache/lucene/index/MergeState$DocMap$1;->val$maxDoc:I

    iput p4, p0, Lorg/apache/lucene/index/MergeState$DocMap$1;->val$numDeletedDocs:I

    .line 82
    invoke-direct {p0}, Lorg/apache/lucene/index/MergeState$DocMap;-><init>()V

    return-void
.end method


# virtual methods
.method public get(I)I
    .locals 4
    .param p1, "docID"    # I

    .prologue
    .line 86
    iget-object v0, p0, Lorg/apache/lucene/index/MergeState$DocMap$1;->val$liveDocs:Lorg/apache/lucene/util/Bits;

    invoke-interface {v0, p1}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 87
    const/4 v0, -0x1

    .line 89
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/MergeState$DocMap$1;->val$docMap:Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;

    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;->get(J)J

    move-result-wide v0

    long-to-int v0, v0

    goto :goto_0
.end method

.method public maxDoc()I
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lorg/apache/lucene/index/MergeState$DocMap$1;->val$maxDoc:I

    return v0
.end method

.method public numDeletedDocs()I
    .locals 1

    .prologue
    .line 99
    iget v0, p0, Lorg/apache/lucene/index/MergeState$DocMap$1;->val$numDeletedDocs:I

    return v0
.end method
