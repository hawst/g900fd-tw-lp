.class abstract Lorg/apache/lucene/index/TermsHashConsumer;
.super Ljava/lang/Object;
.source "TermsHashConsumer.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract abort()V
.end method

.method public abstract addField(Lorg/apache/lucene/index/TermsHashPerField;Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/TermsHashConsumerPerField;
.end method

.method abstract finishDocument(Lorg/apache/lucene/index/TermsHash;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method abstract flush(Ljava/util/Map;Lorg/apache/lucene/index/SegmentWriteState;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/index/TermsHashConsumerPerField;",
            ">;",
            "Lorg/apache/lucene/index/SegmentWriteState;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method abstract startDocument()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
