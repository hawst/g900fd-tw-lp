.class final Lorg/apache/lucene/index/DocFieldProcessor;
.super Lorg/apache/lucene/index/DocConsumer;
.source "DocFieldProcessor.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final fieldsComp:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/index/DocFieldProcessorPerField;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final bytesUsed:Lorg/apache/lucene/util/Counter;

.field final codec:Lorg/apache/lucene/codecs/Codec;

.field final consumer:Lorg/apache/lucene/index/DocFieldConsumer;

.field final docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

.field fieldCount:I

.field fieldGen:I

.field fieldHash:[Lorg/apache/lucene/index/DocFieldProcessorPerField;

.field fields:[Lorg/apache/lucene/index/DocFieldProcessorPerField;

.field hashMask:I

.field final storedConsumer:Lorg/apache/lucene/index/StoredFieldsConsumer;

.field totalFieldCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lorg/apache/lucene/index/DocFieldProcessor;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/DocFieldProcessor;->$assertionsDisabled:Z

    .line 263
    new-instance v0, Lorg/apache/lucene/index/DocFieldProcessor$1;

    invoke-direct {v0}, Lorg/apache/lucene/index/DocFieldProcessor$1;-><init>()V

    sput-object v0, Lorg/apache/lucene/index/DocFieldProcessor;->fieldsComp:Ljava/util/Comparator;

    .line 268
    return-void

    .line 41
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/index/DocumentsWriterPerThread;Lorg/apache/lucene/index/DocFieldConsumer;Lorg/apache/lucene/index/StoredFieldsConsumer;)V
    .locals 2
    .param p1, "docWriter"    # Lorg/apache/lucene/index/DocumentsWriterPerThread;
    .param p2, "consumer"    # Lorg/apache/lucene/index/DocFieldConsumer;
    .param p3, "storedConsumer"    # Lorg/apache/lucene/index/StoredFieldsConsumer;

    .prologue
    const/4 v1, 0x1

    .line 61
    invoke-direct {p0}, Lorg/apache/lucene/index/DocConsumer;-><init>()V

    .line 48
    new-array v0, v1, [Lorg/apache/lucene/index/DocFieldProcessorPerField;

    iput-object v0, p0, Lorg/apache/lucene/index/DocFieldProcessor;->fields:[Lorg/apache/lucene/index/DocFieldProcessorPerField;

    .line 52
    const/4 v0, 0x2

    new-array v0, v0, [Lorg/apache/lucene/index/DocFieldProcessorPerField;

    iput-object v0, p0, Lorg/apache/lucene/index/DocFieldProcessor;->fieldHash:[Lorg/apache/lucene/index/DocFieldProcessorPerField;

    .line 53
    iput v1, p0, Lorg/apache/lucene/index/DocFieldProcessor;->hashMask:I

    .line 62
    iget-object v0, p1, Lorg/apache/lucene/index/DocumentsWriterPerThread;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    iput-object v0, p0, Lorg/apache/lucene/index/DocFieldProcessor;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    .line 63
    iget-object v0, p1, Lorg/apache/lucene/index/DocumentsWriterPerThread;->codec:Lorg/apache/lucene/codecs/Codec;

    iput-object v0, p0, Lorg/apache/lucene/index/DocFieldProcessor;->codec:Lorg/apache/lucene/codecs/Codec;

    .line 64
    iget-object v0, p1, Lorg/apache/lucene/index/DocumentsWriterPerThread;->bytesUsed:Lorg/apache/lucene/util/Counter;

    iput-object v0, p0, Lorg/apache/lucene/index/DocFieldProcessor;->bytesUsed:Lorg/apache/lucene/util/Counter;

    .line 65
    iput-object p2, p0, Lorg/apache/lucene/index/DocFieldProcessor;->consumer:Lorg/apache/lucene/index/DocFieldConsumer;

    .line 66
    iput-object p3, p0, Lorg/apache/lucene/index/DocFieldProcessor;->storedConsumer:Lorg/apache/lucene/index/StoredFieldsConsumer;

    .line 67
    return-void
.end method

.method private rehash()V
    .locals 8

    .prologue
    .line 157
    iget-object v7, p0, Lorg/apache/lucene/index/DocFieldProcessor;->fieldHash:[Lorg/apache/lucene/index/DocFieldProcessorPerField;

    array-length v7, v7

    mul-int/lit8 v5, v7, 0x2

    .line 158
    .local v5, "newHashSize":I
    sget-boolean v7, Lorg/apache/lucene/index/DocFieldProcessor;->$assertionsDisabled:Z

    if-nez v7, :cond_0

    iget-object v7, p0, Lorg/apache/lucene/index/DocFieldProcessor;->fieldHash:[Lorg/apache/lucene/index/DocFieldProcessorPerField;

    array-length v7, v7

    if-gt v5, v7, :cond_0

    new-instance v7, Ljava/lang/AssertionError;

    invoke-direct {v7}, Ljava/lang/AssertionError;-><init>()V

    throw v7

    .line 160
    :cond_0
    new-array v3, v5, [Lorg/apache/lucene/index/DocFieldProcessorPerField;

    .line 163
    .local v3, "newHashArray":[Lorg/apache/lucene/index/DocFieldProcessorPerField;
    add-int/lit8 v4, v5, -0x1

    .line 164
    .local v4, "newHashMask":I
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_0
    iget-object v7, p0, Lorg/apache/lucene/index/DocFieldProcessor;->fieldHash:[Lorg/apache/lucene/index/DocFieldProcessorPerField;

    array-length v7, v7

    if-lt v2, v7, :cond_1

    .line 175
    iput-object v3, p0, Lorg/apache/lucene/index/DocFieldProcessor;->fieldHash:[Lorg/apache/lucene/index/DocFieldProcessorPerField;

    .line 176
    iput v4, p0, Lorg/apache/lucene/index/DocFieldProcessor;->hashMask:I

    .line 177
    return-void

    .line 165
    :cond_1
    iget-object v7, p0, Lorg/apache/lucene/index/DocFieldProcessor;->fieldHash:[Lorg/apache/lucene/index/DocFieldProcessorPerField;

    aget-object v0, v7, v2

    .line 166
    .local v0, "fp0":Lorg/apache/lucene/index/DocFieldProcessorPerField;
    :goto_1
    if-nez v0, :cond_2

    .line 164
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 167
    :cond_2
    iget-object v7, v0, Lorg/apache/lucene/index/DocFieldProcessorPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    iget-object v7, v7, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->hashCode()I

    move-result v7

    and-int v1, v7, v4

    .line 168
    .local v1, "hashPos2":I
    iget-object v6, v0, Lorg/apache/lucene/index/DocFieldProcessorPerField;->next:Lorg/apache/lucene/index/DocFieldProcessorPerField;

    .line 169
    .local v6, "nextFP0":Lorg/apache/lucene/index/DocFieldProcessorPerField;
    aget-object v7, v3, v1

    iput-object v7, v0, Lorg/apache/lucene/index/DocFieldProcessorPerField;->next:Lorg/apache/lucene/index/DocFieldProcessorPerField;

    .line 170
    aput-object v0, v3, v1

    .line 171
    move-object v0, v6

    goto :goto_1
.end method


# virtual methods
.method public abort()V
    .locals 7

    .prologue
    .line 93
    const/4 v3, 0x0

    .line 95
    .local v3, "th":Ljava/lang/Throwable;
    iget-object v5, p0, Lorg/apache/lucene/index/DocFieldProcessor;->fieldHash:[Lorg/apache/lucene/index/DocFieldProcessorPerField;

    array-length v6, v5

    const/4 v4, 0x0

    :goto_0
    if-lt v4, v6, :cond_2

    .line 110
    :try_start_0
    iget-object v4, p0, Lorg/apache/lucene/index/DocFieldProcessor;->storedConsumer:Lorg/apache/lucene/index/StoredFieldsConsumer;

    invoke-virtual {v4}, Lorg/apache/lucene/index/StoredFieldsConsumer;->abort()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 118
    :cond_0
    :goto_1
    :try_start_1
    iget-object v4, p0, Lorg/apache/lucene/index/DocFieldProcessor;->consumer:Lorg/apache/lucene/index/DocFieldConsumer;

    invoke-virtual {v4}, Lorg/apache/lucene/index/DocFieldConsumer;->abort()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_2

    .line 126
    :cond_1
    :goto_2
    if-eqz v3, :cond_7

    .line 127
    instance-of v4, v3, Ljava/lang/RuntimeException;

    if-eqz v4, :cond_5

    check-cast v3, Ljava/lang/RuntimeException;

    .end local v3    # "th":Ljava/lang/Throwable;
    throw v3

    .line 95
    .restart local v3    # "th":Ljava/lang/Throwable;
    :cond_2
    aget-object v0, v5, v4

    .line 96
    .local v0, "field":Lorg/apache/lucene/index/DocFieldProcessorPerField;
    :goto_3
    if-nez v0, :cond_3

    .line 95
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 97
    :cond_3
    iget-object v1, v0, Lorg/apache/lucene/index/DocFieldProcessorPerField;->next:Lorg/apache/lucene/index/DocFieldProcessorPerField;

    .line 99
    .local v1, "next":Lorg/apache/lucene/index/DocFieldProcessorPerField;
    :try_start_2
    invoke-virtual {v0}, Lorg/apache/lucene/index/DocFieldProcessorPerField;->abort()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    .line 105
    :cond_4
    :goto_4
    move-object v0, v1

    goto :goto_3

    .line 100
    :catch_0
    move-exception v2

    .line 101
    .local v2, "t":Ljava/lang/Throwable;
    if-nez v3, :cond_4

    .line 102
    move-object v3, v2

    goto :goto_4

    .line 111
    .end local v0    # "field":Lorg/apache/lucene/index/DocFieldProcessorPerField;
    .end local v1    # "next":Lorg/apache/lucene/index/DocFieldProcessorPerField;
    .end local v2    # "t":Ljava/lang/Throwable;
    :catch_1
    move-exception v2

    .line 112
    .restart local v2    # "t":Ljava/lang/Throwable;
    if-nez v3, :cond_0

    .line 113
    move-object v3, v2

    goto :goto_1

    .line 119
    .end local v2    # "t":Ljava/lang/Throwable;
    :catch_2
    move-exception v2

    .line 120
    .restart local v2    # "t":Ljava/lang/Throwable;
    if-nez v3, :cond_1

    .line 121
    move-object v3, v2

    goto :goto_2

    .line 128
    .end local v2    # "t":Ljava/lang/Throwable;
    :cond_5
    instance-of v4, v3, Ljava/lang/Error;

    if-eqz v4, :cond_6

    check-cast v3, Ljava/lang/Error;

    .end local v3    # "th":Ljava/lang/Throwable;
    throw v3

    .line 130
    .restart local v3    # "th":Ljava/lang/Throwable;
    :cond_6
    new-instance v4, Ljava/lang/RuntimeException;

    invoke-direct {v4, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v4

    .line 132
    :cond_7
    return-void
.end method

.method doAfterFlush()V
    .locals 1

    .prologue
    .line 151
    const/4 v0, 0x2

    new-array v0, v0, [Lorg/apache/lucene/index/DocFieldProcessorPerField;

    iput-object v0, p0, Lorg/apache/lucene/index/DocFieldProcessor;->fieldHash:[Lorg/apache/lucene/index/DocFieldProcessorPerField;

    .line 152
    const/4 v0, 0x1

    iput v0, p0, Lorg/apache/lucene/index/DocFieldProcessor;->hashMask:I

    .line 153
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/index/DocFieldProcessor;->totalFieldCount:I

    .line 154
    return-void
.end method

.method public fields()Ljava/util/Collection;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/index/DocFieldConsumerPerField;",
            ">;"
        }
    .end annotation

    .prologue
    .line 135
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 136
    .local v1, "fields":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/index/DocFieldConsumerPerField;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lorg/apache/lucene/index/DocFieldProcessor;->fieldHash:[Lorg/apache/lucene/index/DocFieldProcessorPerField;

    array-length v3, v3

    if-lt v2, v3, :cond_0

    .line 143
    sget-boolean v3, Lorg/apache/lucene/index/DocFieldProcessor;->$assertionsDisabled:Z

    if-nez v3, :cond_2

    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v3

    iget v4, p0, Lorg/apache/lucene/index/DocFieldProcessor;->totalFieldCount:I

    if-eq v3, v4, :cond_2

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 137
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/index/DocFieldProcessor;->fieldHash:[Lorg/apache/lucene/index/DocFieldProcessorPerField;

    aget-object v0, v3, v2

    .line 138
    .local v0, "field":Lorg/apache/lucene/index/DocFieldProcessorPerField;
    :goto_1
    if-nez v0, :cond_1

    .line 136
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 139
    :cond_1
    iget-object v3, v0, Lorg/apache/lucene/index/DocFieldProcessorPerField;->consumer:Lorg/apache/lucene/index/DocFieldConsumerPerField;

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 140
    iget-object v0, v0, Lorg/apache/lucene/index/DocFieldProcessorPerField;->next:Lorg/apache/lucene/index/DocFieldProcessorPerField;

    goto :goto_1

    .line 144
    .end local v0    # "field":Lorg/apache/lucene/index/DocFieldProcessorPerField;
    :cond_2
    return-object v1
.end method

.method finishDocument()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 273
    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/DocFieldProcessor;->storedConsumer:Lorg/apache/lucene/index/StoredFieldsConsumer;

    invoke-virtual {v0}, Lorg/apache/lucene/index/StoredFieldsConsumer;->finishDocument()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 275
    iget-object v0, p0, Lorg/apache/lucene/index/DocFieldProcessor;->consumer:Lorg/apache/lucene/index/DocFieldConsumer;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocFieldConsumer;->finishDocument()V

    .line 277
    return-void

    .line 274
    :catchall_0
    move-exception v0

    .line 275
    iget-object v1, p0, Lorg/apache/lucene/index/DocFieldProcessor;->consumer:Lorg/apache/lucene/index/DocFieldConsumer;

    invoke-virtual {v1}, Lorg/apache/lucene/index/DocFieldConsumer;->finishDocument()V

    .line 276
    throw v0
.end method

.method public flush(Lorg/apache/lucene/index/SegmentWriteState;)V
    .locals 8
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentWriteState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 72
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 73
    .local v0, "childFields":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lorg/apache/lucene/index/DocFieldConsumerPerField;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/index/DocFieldProcessor;->fields()Ljava/util/Collection;

    move-result-object v2

    .line 74
    .local v2, "fields":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/index/DocFieldConsumerPerField;>;"
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_0

    .line 78
    sget-boolean v4, Lorg/apache/lucene/index/DocFieldProcessor;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    invoke-interface {v2}, Ljava/util/Collection;->size()I

    move-result v4

    iget v5, p0, Lorg/apache/lucene/index/DocFieldProcessor;->totalFieldCount:I

    if-eq v4, v5, :cond_1

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 74
    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/DocFieldConsumerPerField;

    .line 75
    .local v1, "f":Lorg/apache/lucene/index/DocFieldConsumerPerField;
    invoke-virtual {v1}, Lorg/apache/lucene/index/DocFieldConsumerPerField;->getFieldInfo()Lorg/apache/lucene/index/FieldInfo;

    move-result-object v5

    iget-object v5, v5, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-interface {v0, v5, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 80
    .end local v1    # "f":Lorg/apache/lucene/index/DocFieldConsumerPerField;
    :cond_1
    iget-object v4, p0, Lorg/apache/lucene/index/DocFieldProcessor;->storedConsumer:Lorg/apache/lucene/index/StoredFieldsConsumer;

    invoke-virtual {v4, p1}, Lorg/apache/lucene/index/StoredFieldsConsumer;->flush(Lorg/apache/lucene/index/SegmentWriteState;)V

    .line 81
    iget-object v4, p0, Lorg/apache/lucene/index/DocFieldProcessor;->consumer:Lorg/apache/lucene/index/DocFieldConsumer;

    invoke-virtual {v4, v0, p1}, Lorg/apache/lucene/index/DocFieldConsumer;->flush(Ljava/util/Map;Lorg/apache/lucene/index/SegmentWriteState;)V

    .line 87
    iget-object v4, p0, Lorg/apache/lucene/index/DocFieldProcessor;->codec:Lorg/apache/lucene/codecs/Codec;

    invoke-virtual {v4}, Lorg/apache/lucene/codecs/Codec;->fieldInfosFormat()Lorg/apache/lucene/codecs/FieldInfosFormat;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/lucene/codecs/FieldInfosFormat;->getFieldInfosWriter()Lorg/apache/lucene/codecs/FieldInfosWriter;

    move-result-object v3

    .line 88
    .local v3, "infosWriter":Lorg/apache/lucene/codecs/FieldInfosWriter;
    iget-object v4, p1, Lorg/apache/lucene/index/SegmentWriteState;->directory:Lorg/apache/lucene/store/Directory;

    iget-object v5, p1, Lorg/apache/lucene/index/SegmentWriteState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v5, v5, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    iget-object v6, p1, Lorg/apache/lucene/index/SegmentWriteState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    sget-object v7, Lorg/apache/lucene/store/IOContext;->DEFAULT:Lorg/apache/lucene/store/IOContext;

    invoke-virtual {v3, v4, v5, v6, v7}, Lorg/apache/lucene/codecs/FieldInfosWriter;->write(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/store/IOContext;)V

    .line 89
    return-void
.end method

.method public processDocument(Lorg/apache/lucene/index/FieldInfos$Builder;)V
    .locals 16
    .param p1, "fieldInfos"    # Lorg/apache/lucene/index/FieldInfos$Builder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 182
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/index/DocFieldProcessor;->consumer:Lorg/apache/lucene/index/DocFieldConsumer;

    invoke-virtual {v11}, Lorg/apache/lucene/index/DocFieldConsumer;->startDocument()V

    .line 183
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/index/DocFieldProcessor;->storedConsumer:Lorg/apache/lucene/index/StoredFieldsConsumer;

    invoke-virtual {v11}, Lorg/apache/lucene/index/StoredFieldsConsumer;->startDocument()V

    .line 185
    const/4 v11, 0x0

    move-object/from16 v0, p0

    iput v11, v0, Lorg/apache/lucene/index/DocFieldProcessor;->fieldCount:I

    .line 187
    move-object/from16 v0, p0

    iget v10, v0, Lorg/apache/lucene/index/DocFieldProcessor;->fieldGen:I

    add-int/lit8 v11, v10, 0x1

    move-object/from16 v0, p0

    iput v11, v0, Lorg/apache/lucene/index/DocFieldProcessor;->fieldGen:I

    .line 194
    .local v10, "thisFieldGen":I
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/index/DocFieldProcessor;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    iget-object v11, v11, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;->doc:Ljava/lang/Iterable;

    invoke-interface {v11}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_1

    .line 251
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/index/DocFieldProcessor;->fields:[Lorg/apache/lucene/index/DocFieldProcessorPerField;

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iget v13, v0, Lorg/apache/lucene/index/DocFieldProcessor;->fieldCount:I

    sget-object v14, Lorg/apache/lucene/index/DocFieldProcessor;->fieldsComp:Ljava/util/Comparator;

    invoke-static {v11, v12, v13, v14}, Lorg/apache/lucene/util/ArrayUtil;->quickSort([Ljava/lang/Object;IILjava/util/Comparator;)V

    .line 252
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    move-object/from16 v0, p0

    iget v11, v0, Lorg/apache/lucene/index/DocFieldProcessor;->fieldCount:I

    if-lt v6, v11, :cond_8

    .line 257
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/index/DocFieldProcessor;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    iget-object v11, v11, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;->maxTermPrefix:Ljava/lang/String;

    if-eqz v11, :cond_0

    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/index/DocFieldProcessor;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    iget-object v11, v11, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v12, "IW"

    invoke-virtual {v11, v12}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 258
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/index/DocFieldProcessor;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    iget-object v11, v11, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v12, "IW"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "WARNING: document contains at least one immense term (whose UTF8 encoding is longer than the max length 32766), all of which were skipped.  Please correct the analyzer to not produce such terms.  The prefix of the first immense term is: \'"

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/index/DocFieldProcessor;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    iget-object v14, v14, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;->maxTermPrefix:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "...\'"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/index/DocFieldProcessor;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    const/4 v12, 0x0

    iput-object v12, v11, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;->maxTermPrefix:Ljava/lang/String;

    .line 261
    :cond_0
    return-void

    .line 194
    .end local v6    # "i":I
    :cond_1
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/index/IndexableField;

    .line 195
    .local v2, "field":Lorg/apache/lucene/index/IndexableField;
    invoke-interface {v2}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v3

    .line 198
    .local v3, "fieldName":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v12

    move-object/from16 v0, p0

    iget v13, v0, Lorg/apache/lucene/index/DocFieldProcessor;->hashMask:I

    and-int v5, v12, v13

    .line 199
    .local v5, "hashPos":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/index/DocFieldProcessor;->fieldHash:[Lorg/apache/lucene/index/DocFieldProcessorPerField;

    aget-object v4, v12, v5

    .line 200
    .local v4, "fp":Lorg/apache/lucene/index/DocFieldProcessorPerField;
    :goto_2
    if-eqz v4, :cond_2

    iget-object v12, v4, Lorg/apache/lucene/index/DocFieldProcessorPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    iget-object v12, v12, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {v12, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 204
    :cond_2
    if-nez v4, :cond_7

    .line 211
    invoke-interface {v2}, Lorg/apache/lucene/index/IndexableField;->fieldType()Lorg/apache/lucene/index/IndexableFieldType;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, Lorg/apache/lucene/index/FieldInfos$Builder;->addOrUpdate(Ljava/lang/String;Lorg/apache/lucene/index/IndexableFieldType;)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v1

    .line 213
    .local v1, "fi":Lorg/apache/lucene/index/FieldInfo;
    new-instance v4, Lorg/apache/lucene/index/DocFieldProcessorPerField;

    .end local v4    # "fp":Lorg/apache/lucene/index/DocFieldProcessorPerField;
    move-object/from16 v0, p0

    invoke-direct {v4, v0, v1}, Lorg/apache/lucene/index/DocFieldProcessorPerField;-><init>(Lorg/apache/lucene/index/DocFieldProcessor;Lorg/apache/lucene/index/FieldInfo;)V

    .line 214
    .restart local v4    # "fp":Lorg/apache/lucene/index/DocFieldProcessorPerField;
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/index/DocFieldProcessor;->fieldHash:[Lorg/apache/lucene/index/DocFieldProcessorPerField;

    aget-object v12, v12, v5

    iput-object v12, v4, Lorg/apache/lucene/index/DocFieldProcessorPerField;->next:Lorg/apache/lucene/index/DocFieldProcessorPerField;

    .line 215
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/index/DocFieldProcessor;->fieldHash:[Lorg/apache/lucene/index/DocFieldProcessorPerField;

    aput-object v4, v12, v5

    .line 216
    move-object/from16 v0, p0

    iget v12, v0, Lorg/apache/lucene/index/DocFieldProcessor;->totalFieldCount:I

    add-int/lit8 v12, v12, 0x1

    move-object/from16 v0, p0

    iput v12, v0, Lorg/apache/lucene/index/DocFieldProcessor;->totalFieldCount:I

    .line 218
    move-object/from16 v0, p0

    iget v12, v0, Lorg/apache/lucene/index/DocFieldProcessor;->totalFieldCount:I

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/index/DocFieldProcessor;->fieldHash:[Lorg/apache/lucene/index/DocFieldProcessorPerField;

    array-length v13, v13

    div-int/lit8 v13, v13, 0x2

    if-lt v12, v13, :cond_3

    .line 219
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/index/DocFieldProcessor;->rehash()V

    .line 225
    .end local v1    # "fi":Lorg/apache/lucene/index/FieldInfo;
    :cond_3
    :goto_3
    iget v12, v4, Lorg/apache/lucene/index/DocFieldProcessorPerField;->lastGen:I

    if-eq v10, v12, :cond_5

    .line 228
    const/4 v12, 0x0

    iput v12, v4, Lorg/apache/lucene/index/DocFieldProcessorPerField;->fieldCount:I

    .line 230
    move-object/from16 v0, p0

    iget v12, v0, Lorg/apache/lucene/index/DocFieldProcessor;->fieldCount:I

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/index/DocFieldProcessor;->fields:[Lorg/apache/lucene/index/DocFieldProcessorPerField;

    array-length v13, v13

    if-ne v12, v13, :cond_4

    .line 231
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/index/DocFieldProcessor;->fields:[Lorg/apache/lucene/index/DocFieldProcessorPerField;

    array-length v12, v12

    mul-int/lit8 v8, v12, 0x2

    .line 232
    .local v8, "newSize":I
    new-array v7, v8, [Lorg/apache/lucene/index/DocFieldProcessorPerField;

    .line 233
    .local v7, "newArray":[Lorg/apache/lucene/index/DocFieldProcessorPerField;
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/index/DocFieldProcessor;->fields:[Lorg/apache/lucene/index/DocFieldProcessorPerField;

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/index/DocFieldProcessor;->fieldCount:I

    invoke-static {v12, v13, v7, v14, v15}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 234
    move-object/from16 v0, p0

    iput-object v7, v0, Lorg/apache/lucene/index/DocFieldProcessor;->fields:[Lorg/apache/lucene/index/DocFieldProcessorPerField;

    .line 237
    .end local v7    # "newArray":[Lorg/apache/lucene/index/DocFieldProcessorPerField;
    .end local v8    # "newSize":I
    :cond_4
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/index/DocFieldProcessor;->fields:[Lorg/apache/lucene/index/DocFieldProcessorPerField;

    move-object/from16 v0, p0

    iget v13, v0, Lorg/apache/lucene/index/DocFieldProcessor;->fieldCount:I

    add-int/lit8 v14, v13, 0x1

    move-object/from16 v0, p0

    iput v14, v0, Lorg/apache/lucene/index/DocFieldProcessor;->fieldCount:I

    aput-object v4, v12, v13

    .line 238
    iput v10, v4, Lorg/apache/lucene/index/DocFieldProcessorPerField;->lastGen:I

    .line 241
    :cond_5
    invoke-virtual {v4, v2}, Lorg/apache/lucene/index/DocFieldProcessorPerField;->addField(Lorg/apache/lucene/index/IndexableField;)V

    .line 242
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/index/DocFieldProcessor;->storedConsumer:Lorg/apache/lucene/index/StoredFieldsConsumer;

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/index/DocFieldProcessor;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    iget v13, v13, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;->docID:I

    iget-object v14, v4, Lorg/apache/lucene/index/DocFieldProcessorPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    invoke-virtual {v12, v13, v2, v14}, Lorg/apache/lucene/index/StoredFieldsConsumer;->addField(ILorg/apache/lucene/index/IndexableField;Lorg/apache/lucene/index/FieldInfo;)V

    goto/16 :goto_0

    .line 201
    :cond_6
    iget-object v4, v4, Lorg/apache/lucene/index/DocFieldProcessorPerField;->next:Lorg/apache/lucene/index/DocFieldProcessorPerField;

    goto/16 :goto_2

    .line 222
    :cond_7
    iget-object v12, v4, Lorg/apache/lucene/index/DocFieldProcessorPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    invoke-interface {v2}, Lorg/apache/lucene/index/IndexableField;->fieldType()Lorg/apache/lucene/index/IndexableFieldType;

    move-result-object v13

    invoke-virtual {v12, v13}, Lorg/apache/lucene/index/FieldInfo;->update(Lorg/apache/lucene/index/IndexableFieldType;)V

    goto :goto_3

    .line 253
    .end local v2    # "field":Lorg/apache/lucene/index/IndexableField;
    .end local v3    # "fieldName":Ljava/lang/String;
    .end local v4    # "fp":Lorg/apache/lucene/index/DocFieldProcessorPerField;
    .end local v5    # "hashPos":I
    .restart local v6    # "i":I
    :cond_8
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/index/DocFieldProcessor;->fields:[Lorg/apache/lucene/index/DocFieldProcessorPerField;

    aget-object v9, v11, v6

    .line 254
    .local v9, "perField":Lorg/apache/lucene/index/DocFieldProcessorPerField;
    iget-object v11, v9, Lorg/apache/lucene/index/DocFieldProcessorPerField;->consumer:Lorg/apache/lucene/index/DocFieldConsumerPerField;

    iget-object v12, v9, Lorg/apache/lucene/index/DocFieldProcessorPerField;->fields:[Lorg/apache/lucene/index/IndexableField;

    iget v13, v9, Lorg/apache/lucene/index/DocFieldProcessorPerField;->fieldCount:I

    invoke-virtual {v11, v12, v13}, Lorg/apache/lucene/index/DocFieldConsumerPerField;->processFields([Lorg/apache/lucene/index/IndexableField;I)V

    .line 252
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_1
.end method
