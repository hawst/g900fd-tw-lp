.class public Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;
.super Lorg/apache/lucene/search/Query;
.source "FuzzyLikeThisQuery.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$FieldVals;,
        Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;,
        Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTermQueue;
    }
.end annotation


# static fields
.field static sim:Lorg/apache/lucene/search/similarities/TFIDFSimilarity;


# instance fields
.field MAX_VARIANTS_PER_TERM:I

.field analyzer:Lorg/apache/lucene/analysis/Analyzer;

.field fieldVals:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$FieldVals;",
            ">;"
        }
    .end annotation
.end field

.field ignoreTF:Z

.field private maxNumTerms:I

.field q:Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTermQueue;

.field rewrittenQuery:Lorg/apache/lucene/search/Query;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    new-instance v0, Lorg/apache/lucene/search/similarities/DefaultSimilarity;

    invoke-direct {v0}, Lorg/apache/lucene/search/similarities/DefaultSimilarity;-><init>()V

    sput-object v0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;->sim:Lorg/apache/lucene/search/similarities/TFIDFSimilarity;

    return-void
.end method

.method public constructor <init>(ILorg/apache/lucene/analysis/Analyzer;)V
    .locals 1
    .param p1, "maxNumTerms"    # I
    .param p2, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;

    .prologue
    .line 118
    invoke-direct {p0}, Lorg/apache/lucene/search/Query;-><init>()V

    .line 63
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;->rewrittenQuery:Lorg/apache/lucene/search/Query;

    .line 64
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;->fieldVals:Ljava/util/ArrayList;

    .line 68
    const/16 v0, 0x32

    iput v0, p0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;->MAX_VARIANTS_PER_TERM:I

    .line 69
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;->ignoreTF:Z

    .line 120
    new-instance v0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTermQueue;

    invoke-direct {v0, p1}, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTermQueue;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;->q:Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTermQueue;

    .line 121
    iput-object p2, p0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    .line 122
    iput p1, p0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;->maxNumTerms:I

    .line 123
    return-void
.end method

.method private addTerms(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$FieldVals;)V
    .locals 32
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "f"    # Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$FieldVals;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 196
    move-object/from16 v0, p2

    iget-object v8, v0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$FieldVals;->queryString:Ljava/lang/String;

    if-nez v8, :cond_1

    .line 258
    :cond_0
    :goto_0
    return-void

    .line 197
    :cond_1
    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    move-object/from16 v0, p2

    iget-object v9, v0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$FieldVals;->fieldName:Ljava/lang/String;

    new-instance v28, Ljava/io/StringReader;

    move-object/from16 v0, p2

    iget-object v0, v0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$FieldVals;->queryString:Ljava/lang/String;

    move-object/from16 v29, v0

    invoke-direct/range {v28 .. v29}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v28

    invoke-virtual {v8, v9, v0}, Lorg/apache/lucene/analysis/Analyzer;->tokenStream(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/TokenStream;

    move-result-object v26

    .line 198
    .local v26, "ts":Lorg/apache/lucene/analysis/TokenStream;
    const-class v8, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    move-object/from16 v0, v26

    invoke-virtual {v0, v8}, Lorg/apache/lucene/analysis/TokenStream;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v24

    check-cast v24, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 200
    .local v24, "termAtt":Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/IndexReader;->numDocs()I

    move-result v12

    .line 201
    .local v12, "corpusNumDocs":I
    new-instance v19, Ljava/util/HashSet;

    invoke-direct/range {v19 .. v19}, Ljava/util/HashSet;-><init>()V

    .line 202
    .local v19, "processedTerms":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-virtual/range {v26 .. v26}, Lorg/apache/lucene/analysis/TokenStream;->reset()V

    .line 203
    move-object/from16 v0, p2

    iget-object v8, v0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$FieldVals;->fieldName:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-static {v0, v8}, Lorg/apache/lucene/index/MultiFields;->getTerms(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v5

    .line 204
    .local v5, "terms":Lorg/apache/lucene/index/Terms;
    if-eqz v5, :cond_0

    .line 207
    :cond_2
    invoke-virtual/range {v26 .. v26}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v8

    if-nez v8, :cond_3

    .line 256
    invoke-virtual/range {v26 .. v26}, Lorg/apache/lucene/analysis/TokenStream;->end()V

    .line 257
    invoke-virtual/range {v26 .. v26}, Lorg/apache/lucene/analysis/TokenStream;->close()V

    goto :goto_0

    .line 208
    :cond_3
    invoke-interface/range {v24 .. v24}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->toString()Ljava/lang/String;

    move-result-object v23

    .line 209
    .local v23, "term":Ljava/lang/String;
    move-object/from16 v0, v19

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 210
    move-object/from16 v0, v19

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 211
    new-instance v27, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTermQueue;

    move-object/from16 v0, p0

    iget v8, v0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;->MAX_VARIANTS_PER_TERM:I

    move-object/from16 v0, v27

    invoke-direct {v0, v8}, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTermQueue;-><init>(I)V

    .line 212
    .local v27, "variantsQ":Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTermQueue;
    const/16 v16, 0x0

    .line 213
    .local v16, "minScore":F
    new-instance v7, Lorg/apache/lucene/index/Term;

    move-object/from16 v0, p2

    iget-object v8, v0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$FieldVals;->fieldName:Ljava/lang/String;

    move-object/from16 v0, v23

    invoke-direct {v7, v8, v0}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    .local v7, "startTerm":Lorg/apache/lucene/index/Term;
    new-instance v6, Lorg/apache/lucene/util/AttributeSource;

    invoke-direct {v6}, Lorg/apache/lucene/util/AttributeSource;-><init>()V

    .line 216
    .local v6, "atts":Lorg/apache/lucene/util/AttributeSource;
    const-class v8, Lorg/apache/lucene/search/MaxNonCompetitiveBoostAttribute;

    invoke-virtual {v6, v8}, Lorg/apache/lucene/util/AttributeSource;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v15

    check-cast v15, Lorg/apache/lucene/search/MaxNonCompetitiveBoostAttribute;

    .line 217
    .local v15, "maxBoostAtt":Lorg/apache/lucene/search/MaxNonCompetitiveBoostAttribute;
    new-instance v4, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;

    move-object/from16 v0, p2

    iget v8, v0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$FieldVals;->minSimilarity:F

    move-object/from16 v0, p2

    iget v9, v0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$FieldVals;->prefixLength:I

    invoke-direct/range {v4 .. v9}, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;-><init>(Lorg/apache/lucene/index/Terms;Lorg/apache/lucene/util/AttributeSource;Lorg/apache/lucene/index/Term;FI)V

    .line 219
    .local v4, "fe":Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;
    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Lorg/apache/lucene/index/IndexReader;->docFreq(Lorg/apache/lucene/index/Term;)I

    move-result v13

    .line 220
    .local v13, "df":I
    const/16 v17, 0x0

    .line 221
    .local v17, "numVariants":I
    const/16 v25, 0x0

    .line 224
    .local v25, "totalVariantDocFreqs":I
    invoke-virtual {v4}, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;->attributes()Lorg/apache/lucene/util/AttributeSource;

    move-result-object v8

    const-class v9, Lorg/apache/lucene/search/BoostAttribute;

    invoke-virtual {v8, v9}, Lorg/apache/lucene/util/AttributeSource;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v11

    check-cast v11, Lorg/apache/lucene/search/BoostAttribute;

    .line 225
    .local v11, "boostAtt":Lorg/apache/lucene/search/BoostAttribute;
    :goto_1
    invoke-virtual {v4}, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;->next()Lorg/apache/lucene/util/BytesRef;

    move-result-object v18

    .local v18, "possibleMatch":Lorg/apache/lucene/util/BytesRef;
    if-nez v18, :cond_5

    .line 237
    if-lez v17, :cond_2

    .line 238
    div-int v10, v25, v17

    .line 239
    .local v10, "avgDf":I
    if-nez v13, :cond_4

    .line 241
    move v13, v10

    .line 247
    :cond_4
    invoke-virtual/range {v27 .. v27}, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTermQueue;->size()I

    move-result v21

    .line 248
    .local v21, "size":I
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_2
    move/from16 v0, v21

    if-ge v14, v0, :cond_2

    .line 249
    invoke-virtual/range {v27 .. v27}, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTermQueue;->pop()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;

    .line 250
    .local v22, "st":Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;
    move-object/from16 v0, v22

    iget v8, v0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;->score:F

    move-object/from16 v0, v22

    iget v9, v0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;->score:F

    mul-float/2addr v8, v9

    sget-object v9, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;->sim:Lorg/apache/lucene/search/similarities/TFIDFSimilarity;

    int-to-long v0, v13

    move-wide/from16 v28, v0

    int-to-long v0, v12

    move-wide/from16 v30, v0

    move-wide/from16 v0, v28

    move-wide/from16 v2, v30

    invoke-virtual {v9, v0, v1, v2, v3}, Lorg/apache/lucene/search/similarities/TFIDFSimilarity;->idf(JJ)F

    move-result v9

    mul-float/2addr v8, v9

    move-object/from16 v0, v22

    iput v8, v0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;->score:F

    .line 251
    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;->q:Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTermQueue;

    move-object/from16 v0, v22

    invoke-virtual {v8, v0}, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTermQueue;->insertWithOverflow(Ljava/lang/Object;)Ljava/lang/Object;

    .line 248
    add-int/lit8 v14, v14, 0x1

    goto :goto_2

    .line 226
    .end local v10    # "avgDf":I
    .end local v14    # "i":I
    .end local v21    # "size":I
    .end local v22    # "st":Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;
    :cond_5
    add-int/lit8 v17, v17, 0x1

    .line 227
    invoke-virtual {v4}, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;->docFreq()I

    move-result v8

    add-int v25, v25, v8

    .line 228
    invoke-interface {v11}, Lorg/apache/lucene/search/BoostAttribute;->getBoost()F

    move-result v20

    .line 229
    .local v20, "score":F
    invoke-virtual/range {v27 .. v27}, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTermQueue;->size()I

    move-result v8

    move-object/from16 v0, p0

    iget v9, v0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;->MAX_VARIANTS_PER_TERM:I

    if-lt v8, v9, :cond_6

    cmpl-float v8, v20, v16

    if-lez v8, :cond_7

    .line 230
    :cond_6
    new-instance v22, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;

    new-instance v8, Lorg/apache/lucene/index/Term;

    invoke-virtual {v7}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v9

    invoke-static/range {v18 .. v18}, Lorg/apache/lucene/util/BytesRef;->deepCopyOf(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/BytesRef;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-direct {v8, v9, v0}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    move-object/from16 v0, v22

    move/from16 v1, v20

    invoke-direct {v0, v8, v1, v7}, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;-><init>(Lorg/apache/lucene/index/Term;FLorg/apache/lucene/index/Term;)V

    .line 231
    .restart local v22    # "st":Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;
    move-object/from16 v0, v27

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTermQueue;->insertWithOverflow(Ljava/lang/Object;)Ljava/lang/Object;

    .line 232
    invoke-virtual/range {v27 .. v27}, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTermQueue;->top()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;

    iget v0, v8, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;->score:F

    move/from16 v16, v0

    .line 234
    .end local v22    # "st":Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;
    :cond_7
    invoke-virtual/range {v27 .. v27}, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTermQueue;->size()I

    move-result v8

    move-object/from16 v0, p0

    iget v9, v0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;->MAX_VARIANTS_PER_TERM:I

    if-lt v8, v9, :cond_8

    move/from16 v8, v16

    :goto_3
    invoke-interface {v15, v8}, Lorg/apache/lucene/search/MaxNonCompetitiveBoostAttribute;->setMaxNonCompetitiveBoost(F)V

    goto/16 :goto_1

    :cond_8
    const/high16 v8, -0x800000    # Float.NEGATIVE_INFINITY

    goto :goto_3
.end method


# virtual methods
.method public addTerms(Ljava/lang/String;Ljava/lang/String;FI)V
    .locals 7
    .param p1, "queryString"    # Ljava/lang/String;
    .param p2, "fieldName"    # Ljava/lang/String;
    .param p3, "minSimilarity"    # F
    .param p4, "prefixLength"    # I

    .prologue
    .line 191
    iget-object v6, p0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;->fieldVals:Ljava/util/ArrayList;

    new-instance v0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$FieldVals;

    move-object v1, p0

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$FieldVals;-><init>(Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;Ljava/lang/String;FILjava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 192
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 86
    if-ne p0, p1, :cond_1

    .line 110
    :cond_0
    :goto_0
    return v1

    .line 88
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 89
    goto :goto_0

    .line 90
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 91
    goto :goto_0

    .line 92
    :cond_3
    invoke-super {p0, p1}, Lorg/apache/lucene/search/Query;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    .line 93
    goto :goto_0

    :cond_4
    move-object v0, p1

    .line 95
    check-cast v0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;

    .line 96
    .local v0, "other":Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;
    iget-object v3, p0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    if-nez v3, :cond_5

    .line 97
    iget-object v3, v0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    if-eqz v3, :cond_6

    move v1, v2

    .line 98
    goto :goto_0

    .line 99
    :cond_5
    iget-object v3, p0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    iget-object v4, v0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    move v1, v2

    .line 100
    goto :goto_0

    .line 101
    :cond_6
    iget-object v3, p0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;->fieldVals:Ljava/util/ArrayList;

    if-nez v3, :cond_7

    .line 102
    iget-object v3, v0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;->fieldVals:Ljava/util/ArrayList;

    if-eqz v3, :cond_8

    move v1, v2

    .line 103
    goto :goto_0

    .line 104
    :cond_7
    iget-object v3, p0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;->fieldVals:Ljava/util/ArrayList;

    iget-object v4, v0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;->fieldVals:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    move v1, v2

    .line 105
    goto :goto_0

    .line 106
    :cond_8
    iget-boolean v3, p0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;->ignoreTF:Z

    iget-boolean v4, v0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;->ignoreTF:Z

    if-eq v3, v4, :cond_9

    move v1, v2

    .line 107
    goto :goto_0

    .line 108
    :cond_9
    iget v3, p0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;->maxNumTerms:I

    iget v4, v0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;->maxNumTerms:I

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 109
    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 74
    const/16 v0, 0x1f

    .line 75
    .local v0, "prime":I
    invoke-super {p0}, Lorg/apache/lucene/search/Query;->hashCode()I

    move-result v1

    .line 76
    .local v1, "result":I
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    add-int v1, v4, v2

    .line 77
    mul-int/lit8 v2, v1, 0x1f

    .line 78
    iget-object v4, p0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;->fieldVals:Ljava/util/ArrayList;

    if-nez v4, :cond_1

    .line 77
    :goto_1
    add-int v1, v2, v3

    .line 79
    mul-int/lit8 v3, v1, 0x1f

    iget-boolean v2, p0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;->ignoreTF:Z

    if-eqz v2, :cond_2

    const/16 v2, 0x4cf

    :goto_2
    add-int v1, v3, v2

    .line 80
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;->maxNumTerms:I

    add-int v1, v2, v3

    .line 81
    return v1

    .line 76
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_0

    .line 78
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;->fieldVals:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->hashCode()I

    move-result v3

    goto :goto_1

    .line 79
    :cond_2
    const/16 v2, 0x4d5

    goto :goto_2
.end method

.method public isIgnoreTF()Z
    .locals 1

    .prologue
    .line 373
    iget-boolean v0, p0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;->ignoreTF:Z

    return v0
.end method

.method public rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;
    .locals 17
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 263
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;->rewrittenQuery:Lorg/apache/lucene/search/Query;

    if-eqz v15, :cond_0

    .line 265
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;->rewrittenQuery:Lorg/apache/lucene/search/Query;

    .line 325
    :goto_0
    return-object v2

    .line 268
    :cond_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;->fieldVals:Ljava/util/ArrayList;

    invoke-virtual {v15}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$FieldVals;>;"
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-nez v15, :cond_1

    .line 273
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;->fieldVals:Ljava/util/ArrayList;

    invoke-virtual {v15}, Ljava/util/ArrayList;->clear()V

    .line 275
    new-instance v2, Lorg/apache/lucene/search/BooleanQuery;

    invoke-direct {v2}, Lorg/apache/lucene/search/BooleanQuery;-><init>()V

    .line 281
    .local v2, "bq":Lorg/apache/lucene/search/BooleanQuery;
    new-instance v13, Ljava/util/HashMap;

    invoke-direct {v13}, Ljava/util/HashMap;-><init>()V

    .line 282
    .local v13, "variantQueries":Ljava/util/HashMap;, "Ljava/util/HashMap<Lorg/apache/lucene/index/Term;Ljava/util/ArrayList<Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;>;>;"
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;->q:Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTermQueue;

    invoke-virtual {v15}, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTermQueue;->size()I

    move-result v9

    .line 283
    .local v9, "size":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_2
    if-lt v4, v9, :cond_2

    .line 295
    invoke-virtual {v13}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v15

    invoke-interface {v15}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/ArrayList<Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;>;>;"
    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-nez v15, :cond_4

    .line 323
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;->getBoost()F

    move-result v15

    invoke-virtual {v2, v15}, Lorg/apache/lucene/search/BooleanQuery;->setBoost(F)V

    .line 324
    move-object/from16 v0, p0

    iput-object v2, v0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;->rewrittenQuery:Lorg/apache/lucene/search/Query;

    goto :goto_0

    .line 269
    .end local v2    # "bq":Lorg/apache/lucene/search/BooleanQuery;
    .end local v4    # "i":I
    .end local v5    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/ArrayList<Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;>;>;"
    .end local v9    # "size":I
    .end local v13    # "variantQueries":Ljava/util/HashMap;, "Ljava/util/HashMap<Lorg/apache/lucene/index/Term;Ljava/util/ArrayList<Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;>;>;"
    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$FieldVals;

    .line 270
    .local v3, "f":Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$FieldVals;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3}, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;->addTerms(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$FieldVals;)V

    goto :goto_1

    .line 285
    .end local v3    # "f":Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$FieldVals;
    .restart local v2    # "bq":Lorg/apache/lucene/search/BooleanQuery;
    .restart local v4    # "i":I
    .restart local v9    # "size":I
    .restart local v13    # "variantQueries":Ljava/util/HashMap;, "Ljava/util/HashMap<Lorg/apache/lucene/index/Term;Ljava/util/ArrayList<Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;>;>;"
    :cond_2
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;->q:Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTermQueue;

    invoke-virtual {v15}, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTermQueue;->pop()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;

    .line 286
    .local v10, "st":Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;
    iget-object v15, v10, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;->fuzziedSourceTerm:Lorg/apache/lucene/index/Term;

    invoke-virtual {v13, v15}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/ArrayList;

    .line 287
    .local v8, "l":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;>;"
    if-nez v8, :cond_3

    .line 289
    new-instance v8, Ljava/util/ArrayList;

    .end local v8    # "l":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;>;"
    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 290
    .restart local v8    # "l":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;>;"
    iget-object v15, v10, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;->fuzziedSourceTerm:Lorg/apache/lucene/index/Term;

    invoke-virtual {v13, v15, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 292
    :cond_3
    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 283
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 297
    .end local v8    # "l":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;>;"
    .end local v10    # "st":Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;
    .restart local v5    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/ArrayList<Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;>;>;"
    :cond_4
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/util/ArrayList;

    .line 298
    .local v14, "variants":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;>;"
    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v15

    const/16 v16, 0x1

    move/from16 v0, v16

    if-ne v15, v0, :cond_6

    .line 301
    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;

    .line 302
    .restart local v10    # "st":Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;->ignoreTF:Z

    if-eqz v15, :cond_5

    new-instance v12, Lorg/apache/lucene/search/ConstantScoreQuery;

    new-instance v15, Lorg/apache/lucene/search/TermQuery;

    iget-object v0, v10, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;->term:Lorg/apache/lucene/index/Term;

    move-object/from16 v16, v0

    invoke-direct/range {v15 .. v16}, Lorg/apache/lucene/search/TermQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    invoke-direct {v12, v15}, Lorg/apache/lucene/search/ConstantScoreQuery;-><init>(Lorg/apache/lucene/search/Query;)V

    .line 303
    .local v12, "tq":Lorg/apache/lucene/search/Query;
    :goto_4
    iget v15, v10, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;->score:F

    invoke-virtual {v12, v15}, Lorg/apache/lucene/search/Query;->setBoost(F)V

    .line 304
    sget-object v15, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v2, v12, v15}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    goto :goto_3

    .line 302
    .end local v12    # "tq":Lorg/apache/lucene/search/Query;
    :cond_5
    new-instance v12, Lorg/apache/lucene/search/TermQuery;

    iget-object v15, v10, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;->term:Lorg/apache/lucene/index/Term;

    const/16 v16, 0x1

    move/from16 v0, v16

    invoke-direct {v12, v15, v0}, Lorg/apache/lucene/search/TermQuery;-><init>(Lorg/apache/lucene/index/Term;I)V

    goto :goto_4

    .line 308
    .end local v10    # "st":Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;
    :cond_6
    new-instance v11, Lorg/apache/lucene/search/BooleanQuery;

    const/4 v15, 0x1

    invoke-direct {v11, v15}, Lorg/apache/lucene/search/BooleanQuery;-><init>(Z)V

    .line 309
    .local v11, "termVariants":Lorg/apache/lucene/search/BooleanQuery;
    invoke-virtual {v14}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .line 310
    .local v7, "iterator2":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;>;"
    :goto_5
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-nez v15, :cond_7

    .line 318
    sget-object v15, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v2, v11, v15}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    goto/16 :goto_3

    .line 312
    :cond_7
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;

    .line 314
    .restart local v10    # "st":Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;->ignoreTF:Z

    if-eqz v15, :cond_8

    new-instance v12, Lorg/apache/lucene/search/ConstantScoreQuery;

    new-instance v15, Lorg/apache/lucene/search/TermQuery;

    iget-object v0, v10, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;->term:Lorg/apache/lucene/index/Term;

    move-object/from16 v16, v0

    invoke-direct/range {v15 .. v16}, Lorg/apache/lucene/search/TermQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    invoke-direct {v12, v15}, Lorg/apache/lucene/search/ConstantScoreQuery;-><init>(Lorg/apache/lucene/search/Query;)V

    .line 315
    .restart local v12    # "tq":Lorg/apache/lucene/search/Query;
    :goto_6
    iget v15, v10, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;->score:F

    invoke-virtual {v12, v15}, Lorg/apache/lucene/search/Query;->setBoost(F)V

    .line 316
    sget-object v15, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v11, v12, v15}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    goto :goto_5

    .line 314
    .end local v12    # "tq":Lorg/apache/lucene/search/Query;
    :cond_8
    new-instance v12, Lorg/apache/lucene/search/TermQuery;

    iget-object v15, v10, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;->term:Lorg/apache/lucene/index/Term;

    const/16 v16, 0x1

    move/from16 v0, v16

    invoke-direct {v12, v15, v0}, Lorg/apache/lucene/search/TermQuery;-><init>(Lorg/apache/lucene/index/Term;I)V

    goto :goto_6
.end method

.method public setIgnoreTF(Z)V
    .locals 0
    .param p1, "ignoreTF"    # Z

    .prologue
    .line 379
    iput-boolean p1, p0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;->ignoreTF:Z

    .line 380
    return-void
.end method

.method public toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 367
    const/4 v0, 0x0

    return-object v0
.end method
