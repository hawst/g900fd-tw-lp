.class public final Lorg/apache/lucene/sandbox/queries/SlowCollatedStringComparator;
.super Lorg/apache/lucene/search/FieldComparator;
.source "SlowCollatedStringComparator.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/FieldComparator",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private bottom:Ljava/lang/String;

.field final collator:Ljava/text/Collator;

.field private currentDocTerms:Lorg/apache/lucene/index/BinaryDocValues;

.field private final field:Ljava/lang/String;

.field private final tempBR:Lorg/apache/lucene/util/BytesRef;

.field private final values:[Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/text/Collator;)V
    .locals 1
    .param p1, "numHits"    # I
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "collator"    # Ljava/text/Collator;

    .prologue
    .line 47
    invoke-direct {p0}, Lorg/apache/lucene/search/FieldComparator;-><init>()V

    .line 45
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedStringComparator;->tempBR:Lorg/apache/lucene/util/BytesRef;

    .line 48
    new-array v0, p1, [Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedStringComparator;->values:[Ljava/lang/String;

    .line 49
    iput-object p2, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedStringComparator;->field:Ljava/lang/String;

    .line 50
    iput-object p3, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedStringComparator;->collator:Ljava/text/Collator;

    .line 51
    return-void
.end method


# virtual methods
.method public compare(II)I
    .locals 3
    .param p1, "slot1"    # I
    .param p2, "slot2"    # I

    .prologue
    .line 55
    iget-object v2, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedStringComparator;->values:[Ljava/lang/String;

    aget-object v0, v2, p1

    .line 56
    .local v0, "val1":Ljava/lang/String;
    iget-object v2, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedStringComparator;->values:[Ljava/lang/String;

    aget-object v1, v2, p2

    .line 57
    .local v1, "val2":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 58
    if-nez v1, :cond_0

    .line 59
    const/4 v2, 0x0

    .line 65
    :goto_0
    return v2

    .line 61
    :cond_0
    const/4 v2, -0x1

    goto :goto_0

    .line 62
    :cond_1
    if-nez v1, :cond_2

    .line 63
    const/4 v2, 0x1

    goto :goto_0

    .line 65
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedStringComparator;->collator:Ljava/text/Collator;

    invoke-virtual {v2, v0, v1}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    goto :goto_0
.end method

.method public compareBottom(I)I
    .locals 3
    .param p1, "doc"    # I

    .prologue
    .line 70
    iget-object v1, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedStringComparator;->currentDocTerms:Lorg/apache/lucene/index/BinaryDocValues;

    iget-object v2, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedStringComparator;->tempBR:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v1, p1, v2}, Lorg/apache/lucene/index/BinaryDocValues;->get(ILorg/apache/lucene/util/BytesRef;)V

    .line 71
    iget-object v1, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedStringComparator;->tempBR:Lorg/apache/lucene/util/BytesRef;

    iget-object v1, v1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    sget-object v2, Lorg/apache/lucene/index/BinaryDocValues;->MISSING:[B

    if-ne v1, v2, :cond_0

    const/4 v0, 0x0

    .line 72
    .local v0, "val2":Ljava/lang/String;
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedStringComparator;->bottom:Ljava/lang/String;

    if-nez v1, :cond_2

    .line 73
    if-nez v0, :cond_1

    .line 74
    const/4 v1, 0x0

    .line 80
    :goto_1
    return v1

    .line 71
    .end local v0    # "val2":Ljava/lang/String;
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedStringComparator;->tempBR:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v1}, Lorg/apache/lucene/util/BytesRef;->utf8ToString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 76
    .restart local v0    # "val2":Ljava/lang/String;
    :cond_1
    const/4 v1, -0x1

    goto :goto_1

    .line 77
    :cond_2
    if-nez v0, :cond_3

    .line 78
    const/4 v1, 0x1

    goto :goto_1

    .line 80
    :cond_3
    iget-object v1, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedStringComparator;->collator:Ljava/text/Collator;

    iget-object v2, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedStringComparator;->bottom:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    goto :goto_1
.end method

.method public bridge synthetic compareDocToValue(ILjava/lang/Object;)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    check-cast p2, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/sandbox/queries/SlowCollatedStringComparator;->compareDocToValue(ILjava/lang/String;)I

    move-result v0

    return v0
.end method

.method public compareDocToValue(ILjava/lang/String;)I
    .locals 3
    .param p1, "doc"    # I
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 125
    iget-object v1, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedStringComparator;->currentDocTerms:Lorg/apache/lucene/index/BinaryDocValues;

    iget-object v2, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedStringComparator;->tempBR:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v1, p1, v2}, Lorg/apache/lucene/index/BinaryDocValues;->get(ILorg/apache/lucene/util/BytesRef;)V

    .line 127
    iget-object v1, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedStringComparator;->tempBR:Lorg/apache/lucene/util/BytesRef;

    iget-object v1, v1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    sget-object v2, Lorg/apache/lucene/index/BinaryDocValues;->MISSING:[B

    if-ne v1, v2, :cond_0

    .line 128
    const/4 v0, 0x0

    .line 132
    .local v0, "docValue":Ljava/lang/String;
    :goto_0
    invoke-virtual {p0, v0, p2}, Lorg/apache/lucene/sandbox/queries/SlowCollatedStringComparator;->compareValues(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    return v1

    .line 130
    .end local v0    # "docValue":Ljava/lang/String;
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedStringComparator;->tempBR:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v1}, Lorg/apache/lucene/util/BytesRef;->utf8ToString()Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "docValue":Ljava/lang/String;
    goto :goto_0
.end method

.method public bridge synthetic compareValues(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Ljava/lang/String;

    check-cast p2, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/sandbox/queries/SlowCollatedStringComparator;->compareValues(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public compareValues(Ljava/lang/String;Ljava/lang/String;)I
    .locals 1
    .param p1, "first"    # Ljava/lang/String;
    .param p2, "second"    # Ljava/lang/String;

    .prologue
    .line 111
    if-nez p1, :cond_1

    .line 112
    if-nez p2, :cond_0

    .line 113
    const/4 v0, 0x0

    .line 119
    :goto_0
    return v0

    .line 115
    :cond_0
    const/4 v0, -0x1

    goto :goto_0

    .line 116
    :cond_1
    if-nez p2, :cond_2

    .line 117
    const/4 v0, 0x1

    goto :goto_0

    .line 119
    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedStringComparator;->collator:Ljava/text/Collator;

    invoke-virtual {v0, p1, p2}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public copy(II)V
    .locals 2
    .param p1, "slot"    # I
    .param p2, "doc"    # I

    .prologue
    .line 85
    iget-object v0, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedStringComparator;->currentDocTerms:Lorg/apache/lucene/index/BinaryDocValues;

    iget-object v1, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedStringComparator;->tempBR:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v0, p2, v1}, Lorg/apache/lucene/index/BinaryDocValues;->get(ILorg/apache/lucene/util/BytesRef;)V

    .line 86
    iget-object v0, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedStringComparator;->tempBR:Lorg/apache/lucene/util/BytesRef;

    iget-object v0, v0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    sget-object v1, Lorg/apache/lucene/index/BinaryDocValues;->MISSING:[B

    if-ne v0, v1, :cond_0

    .line 87
    iget-object v0, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedStringComparator;->values:[Ljava/lang/String;

    const/4 v1, 0x0

    aput-object v1, v0, p1

    .line 91
    :goto_0
    return-void

    .line 89
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedStringComparator;->values:[Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedStringComparator;->tempBR:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v1}, Lorg/apache/lucene/util/BytesRef;->utf8ToString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, p1

    goto :goto_0
.end method

.method public setBottom(I)V
    .locals 1
    .param p1, "bottom"    # I

    .prologue
    .line 101
    iget-object v0, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedStringComparator;->values:[Ljava/lang/String;

    aget-object v0, v0, p1

    iput-object v0, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedStringComparator;->bottom:Ljava/lang/String;

    .line 102
    return-void
.end method

.method public setNextReader(Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/search/FieldComparator;
    .locals 3
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/AtomicReaderContext;",
            ")",
            "Lorg/apache/lucene/search/FieldComparator",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 95
    sget-object v0, Lorg/apache/lucene/search/FieldCache;->DEFAULT:Lorg/apache/lucene/search/FieldCache;

    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedStringComparator;->field:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lorg/apache/lucene/search/FieldCache;->getTerms(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;)Lorg/apache/lucene/index/BinaryDocValues;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedStringComparator;->currentDocTerms:Lorg/apache/lucene/index/BinaryDocValues;

    .line 96
    return-object p0
.end method

.method public bridge synthetic value(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/sandbox/queries/SlowCollatedStringComparator;->value(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public value(I)Ljava/lang/String;
    .locals 1
    .param p1, "slot"    # I

    .prologue
    .line 106
    iget-object v0, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedStringComparator;->values:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method
