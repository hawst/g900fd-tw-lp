.class Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum$LinearFuzzyTermsEnum;
.super Lorg/apache/lucene/index/FilteredTermsEnum;
.source "SlowFuzzyTermsEnum.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LinearFuzzyTermsEnum"
.end annotation


# instance fields
.field private final boostAtt:Lorg/apache/lucene/search/BoostAttribute;

.field private d:[I

.field private p:[I

.field private final prefixBytesRef:Lorg/apache/lucene/util/BytesRef;

.field private final text:[I

.field final synthetic this$0:Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;

.field private final utf32:Lorg/apache/lucene/util/IntsRef;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 88
    iput-object p1, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum$LinearFuzzyTermsEnum;->this$0:Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;

    .line 89
    # getter for: Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;->terms:Lorg/apache/lucene/index/Terms;
    invoke-static {p1}, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;->access$0(Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;)Lorg/apache/lucene/index/Terms;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v1

    invoke-direct {p0, v1}, Lorg/apache/lucene/index/FilteredTermsEnum;-><init>(Lorg/apache/lucene/index/TermsEnum;)V

    .line 76
    invoke-virtual {p0}, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum$LinearFuzzyTermsEnum;->attributes()Lorg/apache/lucene/util/AttributeSource;

    move-result-object v1

    const-class v2, Lorg/apache/lucene/search/BoostAttribute;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/AttributeSource;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/search/BoostAttribute;

    iput-object v1, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum$LinearFuzzyTermsEnum;->boostAtt:Lorg/apache/lucene/search/BoostAttribute;

    .line 103
    new-instance v1, Lorg/apache/lucene/util/IntsRef;

    const/16 v2, 0x14

    invoke-direct {v1, v2}, Lorg/apache/lucene/util/IntsRef;-><init>(I)V

    iput-object v1, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum$LinearFuzzyTermsEnum;->utf32:Lorg/apache/lucene/util/IntsRef;

    .line 91
    # getter for: Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;->termLength:I
    invoke-static {p1}, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;->access$1(Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;)I

    move-result v1

    # getter for: Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;->realPrefixLength:I
    invoke-static {p1}, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;->access$2(Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;)I

    move-result v2

    sub-int/2addr v1, v2

    new-array v1, v1, [I

    iput-object v1, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum$LinearFuzzyTermsEnum;->text:[I

    .line 92
    # getter for: Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;->termText:[I
    invoke-static {p1}, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;->access$3(Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;)[I

    move-result-object v1

    # getter for: Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;->realPrefixLength:I
    invoke-static {p1}, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;->access$2(Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;)I

    move-result v2

    iget-object v3, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum$LinearFuzzyTermsEnum;->text:[I

    iget-object v4, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum$LinearFuzzyTermsEnum;->text:[I

    array-length v4, v4

    invoke-static {v1, v2, v3, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 93
    # getter for: Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;->termText:[I
    invoke-static {p1}, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;->access$3(Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;)[I

    move-result-object v1

    # getter for: Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;->realPrefixLength:I
    invoke-static {p1}, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;->access$2(Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;)I

    move-result v2

    invoke-static {v1, v5, v2}, Lorg/apache/lucene/util/UnicodeUtil;->newString([III)Ljava/lang/String;

    move-result-object v0

    .line 94
    .local v0, "prefix":Ljava/lang/String;
    new-instance v1, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v1, v0}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    iput-object v1, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum$LinearFuzzyTermsEnum;->prefixBytesRef:Lorg/apache/lucene/util/BytesRef;

    .line 95
    iget-object v1, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum$LinearFuzzyTermsEnum;->text:[I

    array-length v1, v1

    add-int/lit8 v1, v1, 0x1

    new-array v1, v1, [I

    iput-object v1, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum$LinearFuzzyTermsEnum;->d:[I

    .line 96
    iget-object v1, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum$LinearFuzzyTermsEnum;->text:[I

    array-length v1, v1

    add-int/lit8 v1, v1, 0x1

    new-array v1, v1, [I

    iput-object v1, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum$LinearFuzzyTermsEnum;->p:[I

    .line 98
    iget-object v1, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum$LinearFuzzyTermsEnum;->prefixBytesRef:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {p0, v1}, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum$LinearFuzzyTermsEnum;->setInitialSeekTerm(Lorg/apache/lucene/util/BytesRef;)V

    .line 99
    return-void
.end method

.method private calculateMaxDistance(I)I
    .locals 4
    .param p1, "m"    # I

    .prologue
    .line 245
    iget-object v0, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum$LinearFuzzyTermsEnum;->this$0:Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;

    # getter for: Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;->raw:Z
    invoke-static {v0}, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;->access$6(Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum$LinearFuzzyTermsEnum;->this$0:Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;

    # getter for: Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;->maxEdits:I
    invoke-static {v0}, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;->access$7(Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum$LinearFuzzyTermsEnum;->this$0:Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;

    # getter for: Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;->maxEdits:I
    invoke-static {v0}, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;->access$7(Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;)I

    move-result v0

    .line 246
    const/high16 v1, 0x3f800000    # 1.0f

    iget-object v2, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum$LinearFuzzyTermsEnum;->this$0:Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;

    # getter for: Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;->minSimilarity:F
    invoke-static {v2}, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;->access$4(Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;)F

    move-result v2

    sub-float/2addr v1, v2

    iget-object v2, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum$LinearFuzzyTermsEnum;->text:[I

    array-length v2, v2

    invoke-static {v2, p1}, Ljava/lang/Math;->min(II)I

    move-result v2

    iget-object v3, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum$LinearFuzzyTermsEnum;->this$0:Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;

    # getter for: Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;->realPrefixLength:I
    invoke-static {v3}, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;->access$2(Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;)I

    move-result v3

    add-int/2addr v2, v3

    int-to-float v2, v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    .line 245
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0
.end method

.method private final similarity([III)F
    .locals 12
    .param p1, "target"    # [I
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 165
    move v4, p3

    .line 166
    .local v4, "m":I
    iget-object v8, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum$LinearFuzzyTermsEnum;->text:[I

    array-length v6, v8

    .line 167
    .local v6, "n":I
    if-nez v6, :cond_1

    .line 170
    iget-object v8, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum$LinearFuzzyTermsEnum;->this$0:Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;

    # getter for: Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;->realPrefixLength:I
    invoke-static {v8}, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;->access$2(Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;)I

    move-result v8

    if-nez v8, :cond_0

    const/4 v8, 0x0

    .line 234
    :goto_0
    return v8

    .line 170
    :cond_0
    const/high16 v8, 0x3f800000    # 1.0f

    int-to-float v9, v4

    iget-object v10, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum$LinearFuzzyTermsEnum;->this$0:Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;

    # getter for: Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;->realPrefixLength:I
    invoke-static {v10}, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;->access$2(Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;)I

    move-result v10

    int-to-float v10, v10

    div-float/2addr v9, v10

    sub-float/2addr v8, v9

    goto :goto_0

    .line 172
    :cond_1
    if-nez v4, :cond_3

    .line 173
    iget-object v8, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum$LinearFuzzyTermsEnum;->this$0:Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;

    # getter for: Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;->realPrefixLength:I
    invoke-static {v8}, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;->access$2(Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;)I

    move-result v8

    if-nez v8, :cond_2

    const/4 v8, 0x0

    goto :goto_0

    :cond_2
    const/high16 v8, 0x3f800000    # 1.0f

    int-to-float v9, v6

    iget-object v10, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum$LinearFuzzyTermsEnum;->this$0:Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;

    # getter for: Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;->realPrefixLength:I
    invoke-static {v10}, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;->access$2(Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;)I

    move-result v10

    int-to-float v10, v10

    div-float/2addr v9, v10

    sub-float/2addr v8, v9

    goto :goto_0

    .line 176
    :cond_3
    invoke-direct {p0, v4}, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum$LinearFuzzyTermsEnum;->calculateMaxDistance(I)I

    move-result v5

    .line 178
    .local v5, "maxDistance":I
    sub-int v8, v4, v6

    invoke-static {v8}, Ljava/lang/Math;->abs(I)I

    move-result v8

    if-ge v5, v8, :cond_4

    .line 186
    const/high16 v8, -0x800000    # Float.NEGATIVE_INFINITY

    goto :goto_0

    .line 190
    :cond_4
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-le v2, v6, :cond_5

    .line 195
    const/4 v3, 0x1

    .local v3, "j":I
    :goto_2
    if-le v3, v4, :cond_6

    .line 234
    const/high16 v8, 0x3f800000    # 1.0f

    iget-object v9, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum$LinearFuzzyTermsEnum;->p:[I

    aget v9, v9, v6

    int-to-float v9, v9

    iget-object v10, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum$LinearFuzzyTermsEnum;->this$0:Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;

    # getter for: Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;->realPrefixLength:I
    invoke-static {v10}, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;->access$2(Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;)I

    move-result v10

    invoke-static {v6, v4}, Ljava/lang/Math;->min(II)I

    move-result v11

    add-int/2addr v10, v11

    int-to-float v10, v10

    div-float/2addr v9, v10

    sub-float/2addr v8, v9

    goto :goto_0

    .line 191
    .end local v3    # "j":I
    :cond_5
    iget-object v8, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum$LinearFuzzyTermsEnum;->p:[I

    aput v2, v8, v2

    .line 190
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 196
    .restart local v3    # "j":I
    :cond_6
    move v1, v4

    .line 197
    .local v1, "bestPossibleEditDistance":I
    add-int v8, p2, v3

    add-int/lit8 v8, v8, -0x1

    aget v7, p1, v8

    .line 198
    .local v7, "t_j":I
    iget-object v8, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum$LinearFuzzyTermsEnum;->d:[I

    const/4 v9, 0x0

    aput v3, v8, v9

    .line 200
    const/4 v2, 0x1

    :goto_3
    if-le v2, v6, :cond_7

    .line 214
    if-le v3, v5, :cond_9

    if-le v1, v5, :cond_9

    .line 217
    const/high16 v8, -0x800000    # Float.NEGATIVE_INFINITY

    goto :goto_0

    .line 202
    :cond_7
    iget-object v8, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum$LinearFuzzyTermsEnum;->text:[I

    add-int/lit8 v9, v2, -0x1

    aget v8, v8, v9

    if-eq v7, v8, :cond_8

    .line 203
    iget-object v8, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum$LinearFuzzyTermsEnum;->d:[I

    iget-object v9, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum$LinearFuzzyTermsEnum;->d:[I

    add-int/lit8 v10, v2, -0x1

    aget v9, v9, v10

    iget-object v10, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum$LinearFuzzyTermsEnum;->p:[I

    aget v10, v10, v2

    invoke-static {v9, v10}, Ljava/lang/Math;->min(II)I

    move-result v9

    iget-object v10, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum$LinearFuzzyTermsEnum;->p:[I

    add-int/lit8 v11, v2, -0x1

    aget v10, v10, v11

    invoke-static {v9, v10}, Ljava/lang/Math;->min(II)I

    move-result v9

    add-int/lit8 v9, v9, 0x1

    aput v9, v8, v2

    .line 207
    :goto_4
    iget-object v8, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum$LinearFuzzyTermsEnum;->d:[I

    aget v8, v8, v2

    invoke-static {v1, v8}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 200
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 205
    :cond_8
    iget-object v8, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum$LinearFuzzyTermsEnum;->d:[I

    iget-object v9, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum$LinearFuzzyTermsEnum;->d:[I

    add-int/lit8 v10, v2, -0x1

    aget v9, v9, v10

    add-int/lit8 v9, v9, 0x1

    iget-object v10, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum$LinearFuzzyTermsEnum;->p:[I

    aget v10, v10, v2

    add-int/lit8 v10, v10, 0x1

    invoke-static {v9, v10}, Ljava/lang/Math;->min(II)I

    move-result v9

    iget-object v10, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum$LinearFuzzyTermsEnum;->p:[I

    add-int/lit8 v11, v2, -0x1

    aget v10, v10, v11

    invoke-static {v9, v10}, Ljava/lang/Math;->min(II)I

    move-result v9

    aput v9, v8, v2

    goto :goto_4

    .line 221
    :cond_9
    iget-object v0, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum$LinearFuzzyTermsEnum;->p:[I

    .line 222
    .local v0, "_d":[I
    iget-object v8, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum$LinearFuzzyTermsEnum;->d:[I

    iput-object v8, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum$LinearFuzzyTermsEnum;->p:[I

    .line 223
    iput-object v0, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum$LinearFuzzyTermsEnum;->d:[I

    .line 195
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2
.end method


# virtual methods
.method protected final accept(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;
    .locals 5
    .param p1, "term"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 111
    iget-object v1, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum$LinearFuzzyTermsEnum;->prefixBytesRef:Lorg/apache/lucene/util/BytesRef;

    invoke-static {p1, v1}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 112
    iget-object v1, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum$LinearFuzzyTermsEnum;->utf32:Lorg/apache/lucene/util/IntsRef;

    invoke-static {p1, v1}, Lorg/apache/lucene/util/UnicodeUtil;->UTF8toUTF32(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/IntsRef;)V

    .line 113
    iget-object v1, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum$LinearFuzzyTermsEnum;->utf32:Lorg/apache/lucene/util/IntsRef;

    iget-object v1, v1, Lorg/apache/lucene/util/IntsRef;->ints:[I

    iget-object v2, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum$LinearFuzzyTermsEnum;->this$0:Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;

    # getter for: Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;->realPrefixLength:I
    invoke-static {v2}, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;->access$2(Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;)I

    move-result v2

    iget-object v3, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum$LinearFuzzyTermsEnum;->utf32:Lorg/apache/lucene/util/IntsRef;

    iget v3, v3, Lorg/apache/lucene/util/IntsRef;->length:I

    iget-object v4, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum$LinearFuzzyTermsEnum;->this$0:Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;

    # getter for: Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;->realPrefixLength:I
    invoke-static {v4}, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;->access$2(Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;)I

    move-result v4

    sub-int/2addr v3, v4

    invoke-direct {p0, v1, v2, v3}, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum$LinearFuzzyTermsEnum;->similarity([III)F

    move-result v0

    .line 114
    .local v0, "similarity":F
    iget-object v1, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum$LinearFuzzyTermsEnum;->this$0:Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;

    # getter for: Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;->minSimilarity:F
    invoke-static {v1}, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;->access$4(Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;)F

    move-result v1

    cmpl-float v1, v0, v1

    if-lez v1, :cond_0

    .line 115
    iget-object v1, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum$LinearFuzzyTermsEnum;->boostAtt:Lorg/apache/lucene/search/BoostAttribute;

    iget-object v2, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum$LinearFuzzyTermsEnum;->this$0:Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;

    # getter for: Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;->minSimilarity:F
    invoke-static {v2}, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;->access$4(Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;)F

    move-result v2

    sub-float v2, v0, v2

    iget-object v3, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum$LinearFuzzyTermsEnum;->this$0:Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;

    # getter for: Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;->scale_factor:F
    invoke-static {v3}, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;->access$5(Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;)F

    move-result v3

    mul-float/2addr v2, v3

    invoke-interface {v1, v2}, Lorg/apache/lucene/search/BoostAttribute;->setBoost(F)V

    .line 116
    sget-object v1, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;->YES:Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

    .line 119
    .end local v0    # "similarity":F
    :goto_0
    return-object v1

    .line 117
    .restart local v0    # "similarity":F
    :cond_0
    sget-object v1, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;->NO:Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

    goto :goto_0

    .line 119
    .end local v0    # "similarity":F
    :cond_1
    sget-object v1, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;->END:Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

    goto :goto_0
.end method
