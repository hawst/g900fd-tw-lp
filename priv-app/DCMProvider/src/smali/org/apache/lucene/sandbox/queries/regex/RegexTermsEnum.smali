.class public Lorg/apache/lucene/sandbox/queries/regex/RegexTermsEnum;
.super Lorg/apache/lucene/index/FilteredTermsEnum;
.source "RegexTermsEnum.java"


# instance fields
.field private final prefixRef:Lorg/apache/lucene/util/BytesRef;

.field private regexImpl:Lorg/apache/lucene/sandbox/queries/regex/RegexCapabilities$RegexMatcher;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/TermsEnum;Lorg/apache/lucene/index/Term;Lorg/apache/lucene/sandbox/queries/regex/RegexCapabilities;)V
    .locals 3
    .param p1, "tenum"    # Lorg/apache/lucene/index/TermsEnum;
    .param p2, "term"    # Lorg/apache/lucene/index/Term;
    .param p3, "regexCap"    # Lorg/apache/lucene/sandbox/queries/regex/RegexCapabilities;

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/FilteredTermsEnum;-><init>(Lorg/apache/lucene/index/TermsEnum;)V

    .line 42
    invoke-virtual {p2}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v1

    .line 43
    .local v1, "text":Ljava/lang/String;
    invoke-interface {p3, v1}, Lorg/apache/lucene/sandbox/queries/regex/RegexCapabilities;->compile(Ljava/lang/String;)Lorg/apache/lucene/sandbox/queries/regex/RegexCapabilities$RegexMatcher;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/lucene/sandbox/queries/regex/RegexTermsEnum;->regexImpl:Lorg/apache/lucene/sandbox/queries/regex/RegexCapabilities$RegexMatcher;

    .line 45
    iget-object v2, p0, Lorg/apache/lucene/sandbox/queries/regex/RegexTermsEnum;->regexImpl:Lorg/apache/lucene/sandbox/queries/regex/RegexCapabilities$RegexMatcher;

    invoke-interface {v2}, Lorg/apache/lucene/sandbox/queries/regex/RegexCapabilities$RegexMatcher;->prefix()Ljava/lang/String;

    move-result-object v0

    .line 46
    .local v0, "pre":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 47
    const-string v0, ""

    .line 50
    :cond_0
    new-instance v2, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v2, v0}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    iput-object v2, p0, Lorg/apache/lucene/sandbox/queries/regex/RegexTermsEnum;->prefixRef:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {p0, v2}, Lorg/apache/lucene/sandbox/queries/regex/RegexTermsEnum;->setInitialSeekTerm(Lorg/apache/lucene/util/BytesRef;)V

    .line 51
    return-void
.end method


# virtual methods
.method protected accept(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;
    .locals 1
    .param p1, "term"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 55
    iget-object v0, p0, Lorg/apache/lucene/sandbox/queries/regex/RegexTermsEnum;->prefixRef:Lorg/apache/lucene/util/BytesRef;

    invoke-static {p1, v0}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 58
    iget-object v0, p0, Lorg/apache/lucene/sandbox/queries/regex/RegexTermsEnum;->regexImpl:Lorg/apache/lucene/sandbox/queries/regex/RegexCapabilities$RegexMatcher;

    invoke-interface {v0, p1}, Lorg/apache/lucene/sandbox/queries/regex/RegexCapabilities$RegexMatcher;->match(Lorg/apache/lucene/util/BytesRef;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;->YES:Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

    .line 60
    :goto_0
    return-object v0

    .line 58
    :cond_0
    sget-object v0, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;->NO:Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

    goto :goto_0

    .line 60
    :cond_1
    sget-object v0, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;->NO:Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

    goto :goto_0
.end method
