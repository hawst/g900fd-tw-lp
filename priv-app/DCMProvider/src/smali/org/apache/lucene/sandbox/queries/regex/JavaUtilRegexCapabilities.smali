.class public Lorg/apache/lucene/sandbox/queries/regex/JavaUtilRegexCapabilities;
.super Ljava/lang/Object;
.source "JavaUtilRegexCapabilities.java"

# interfaces
.implements Lorg/apache/lucene/sandbox/queries/regex/RegexCapabilities;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/sandbox/queries/regex/JavaUtilRegexCapabilities$JavaUtilRegexMatcher;
    }
.end annotation


# static fields
.field public static final FLAG_CANON_EQ:I = 0x80

.field public static final FLAG_CASE_INSENSITIVE:I = 0x2

.field public static final FLAG_COMMENTS:I = 0x4

.field public static final FLAG_DOTALL:I = 0x20

.field public static final FLAG_LITERAL:I = 0x10

.field public static final FLAG_MULTILINE:I = 0x8

.field public static final FLAG_UNICODE_CASE:I = 0x40

.field public static final FLAG_UNIX_LINES:I = 0x1


# instance fields
.field private flags:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput v0, p0, Lorg/apache/lucene/sandbox/queries/regex/JavaUtilRegexCapabilities;->flags:I

    .line 56
    iput v0, p0, Lorg/apache/lucene/sandbox/queries/regex/JavaUtilRegexCapabilities;->flags:I

    .line 57
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "flags"    # I

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/sandbox/queries/regex/JavaUtilRegexCapabilities;->flags:I

    .line 72
    iput p1, p0, Lorg/apache/lucene/sandbox/queries/regex/JavaUtilRegexCapabilities;->flags:I

    .line 73
    return-void
.end method


# virtual methods
.method public compile(Ljava/lang/String;)Lorg/apache/lucene/sandbox/queries/regex/RegexCapabilities$RegexMatcher;
    .locals 2
    .param p1, "regex"    # Ljava/lang/String;

    .prologue
    .line 77
    new-instance v0, Lorg/apache/lucene/sandbox/queries/regex/JavaUtilRegexCapabilities$JavaUtilRegexMatcher;

    iget v1, p0, Lorg/apache/lucene/sandbox/queries/regex/JavaUtilRegexCapabilities;->flags:I

    invoke-direct {v0, p0, p1, v1}, Lorg/apache/lucene/sandbox/queries/regex/JavaUtilRegexCapabilities$JavaUtilRegexMatcher;-><init>(Lorg/apache/lucene/sandbox/queries/regex/JavaUtilRegexCapabilities;Ljava/lang/String;I)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 90
    if-ne p0, p1, :cond_1

    .line 101
    :cond_0
    :goto_0
    return v1

    .line 93
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 94
    goto :goto_0

    .line 96
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 97
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 100
    check-cast v0, Lorg/apache/lucene/sandbox/queries/regex/JavaUtilRegexCapabilities;

    .line 101
    .local v0, "other":Lorg/apache/lucene/sandbox/queries/regex/JavaUtilRegexCapabilities;
    iget v3, p0, Lorg/apache/lucene/sandbox/queries/regex/JavaUtilRegexCapabilities;->flags:I

    iget v4, v0, Lorg/apache/lucene/sandbox/queries/regex/JavaUtilRegexCapabilities;->flags:I

    if-eq v3, v4, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 82
    const/16 v0, 0x1f

    .line 83
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 84
    .local v1, "result":I
    iget v2, p0, Lorg/apache/lucene/sandbox/queries/regex/JavaUtilRegexCapabilities;->flags:I

    add-int/lit8 v1, v2, 0x1f

    .line 85
    return v1
.end method
