.class public Lorg/apache/lucene/sandbox/queries/regex/RegexQuery;
.super Lorg/apache/lucene/search/MultiTermQuery;
.source "RegexQuery.java"

# interfaces
.implements Lorg/apache/lucene/sandbox/queries/regex/RegexQueryCapable;


# instance fields
.field private regexImpl:Lorg/apache/lucene/sandbox/queries/regex/RegexCapabilities;

.field private term:Lorg/apache/lucene/index/Term;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/Term;)V
    .locals 1
    .param p1, "term"    # Lorg/apache/lucene/index/Term;

    .prologue
    .line 46
    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/lucene/search/MultiTermQuery;-><init>(Ljava/lang/String;)V

    .line 41
    new-instance v0, Lorg/apache/lucene/sandbox/queries/regex/JavaUtilRegexCapabilities;

    invoke-direct {v0}, Lorg/apache/lucene/sandbox/queries/regex/JavaUtilRegexCapabilities;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/sandbox/queries/regex/RegexQuery;->regexImpl:Lorg/apache/lucene/sandbox/queries/regex/RegexCapabilities;

    .line 47
    iput-object p1, p0, Lorg/apache/lucene/sandbox/queries/regex/RegexQuery;->term:Lorg/apache/lucene/index/Term;

    .line 48
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 92
    if-ne p0, p1, :cond_1

    .line 119
    :cond_0
    :goto_0
    return v1

    .line 95
    :cond_1
    invoke-super {p0, p1}, Lorg/apache/lucene/search/MultiTermQuery;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    move v1, v2

    .line 96
    goto :goto_0

    .line 98
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 99
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 102
    check-cast v0, Lorg/apache/lucene/sandbox/queries/regex/RegexQuery;

    .line 103
    .local v0, "other":Lorg/apache/lucene/sandbox/queries/regex/RegexQuery;
    iget-object v3, p0, Lorg/apache/lucene/sandbox/queries/regex/RegexQuery;->regexImpl:Lorg/apache/lucene/sandbox/queries/regex/RegexCapabilities;

    if-nez v3, :cond_4

    .line 104
    iget-object v3, v0, Lorg/apache/lucene/sandbox/queries/regex/RegexQuery;->regexImpl:Lorg/apache/lucene/sandbox/queries/regex/RegexCapabilities;

    if-eqz v3, :cond_5

    move v1, v2

    .line 105
    goto :goto_0

    .line 107
    :cond_4
    iget-object v3, p0, Lorg/apache/lucene/sandbox/queries/regex/RegexQuery;->regexImpl:Lorg/apache/lucene/sandbox/queries/regex/RegexCapabilities;

    iget-object v4, v0, Lorg/apache/lucene/sandbox/queries/regex/RegexQuery;->regexImpl:Lorg/apache/lucene/sandbox/queries/regex/RegexCapabilities;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    move v1, v2

    .line 108
    goto :goto_0

    .line 111
    :cond_5
    iget-object v3, p0, Lorg/apache/lucene/sandbox/queries/regex/RegexQuery;->term:Lorg/apache/lucene/index/Term;

    if-nez v3, :cond_6

    .line 112
    iget-object v3, v0, Lorg/apache/lucene/sandbox/queries/regex/RegexQuery;->term:Lorg/apache/lucene/index/Term;

    if-eqz v3, :cond_0

    move v1, v2

    .line 113
    goto :goto_0

    .line 115
    :cond_6
    iget-object v3, p0, Lorg/apache/lucene/sandbox/queries/regex/RegexQuery;->term:Lorg/apache/lucene/index/Term;

    iget-object v4, v0, Lorg/apache/lucene/sandbox/queries/regex/RegexQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/index/Term;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 116
    goto :goto_0
.end method

.method public getRegexImplementation()Lorg/apache/lucene/sandbox/queries/regex/RegexCapabilities;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lorg/apache/lucene/sandbox/queries/regex/RegexQuery;->regexImpl:Lorg/apache/lucene/sandbox/queries/regex/RegexCapabilities;

    return-object v0
.end method

.method public getTerm()Lorg/apache/lucene/index/Term;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lorg/apache/lucene/sandbox/queries/regex/RegexQuery;->term:Lorg/apache/lucene/index/Term;

    return-object v0
.end method

.method protected getTermsEnum(Lorg/apache/lucene/index/Terms;Lorg/apache/lucene/util/AttributeSource;)Lorg/apache/lucene/index/FilteredTermsEnum;
    .locals 4
    .param p1, "terms"    # Lorg/apache/lucene/index/Terms;
    .param p2, "atts"    # Lorg/apache/lucene/util/AttributeSource;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 66
    new-instance v0, Lorg/apache/lucene/sandbox/queries/regex/RegexTermsEnum;

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/sandbox/queries/regex/RegexQuery;->term:Lorg/apache/lucene/index/Term;

    iget-object v3, p0, Lorg/apache/lucene/sandbox/queries/regex/RegexQuery;->regexImpl:Lorg/apache/lucene/sandbox/queries/regex/RegexCapabilities;

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/sandbox/queries/regex/RegexTermsEnum;-><init>(Lorg/apache/lucene/index/TermsEnum;Lorg/apache/lucene/index/Term;Lorg/apache/lucene/sandbox/queries/regex/RegexCapabilities;)V

    return-object v0
.end method

.method protected bridge synthetic getTermsEnum(Lorg/apache/lucene/index/Terms;Lorg/apache/lucene/util/AttributeSource;)Lorg/apache/lucene/index/TermsEnum;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/sandbox/queries/regex/RegexQuery;->getTermsEnum(Lorg/apache/lucene/index/Terms;Lorg/apache/lucene/util/AttributeSource;)Lorg/apache/lucene/index/FilteredTermsEnum;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 83
    const/16 v0, 0x1f

    .line 84
    .local v0, "prime":I
    invoke-super {p0}, Lorg/apache/lucene/search/MultiTermQuery;->hashCode()I

    move-result v1

    .line 85
    .local v1, "result":I
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lorg/apache/lucene/sandbox/queries/regex/RegexQuery;->regexImpl:Lorg/apache/lucene/sandbox/queries/regex/RegexCapabilities;

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    add-int v1, v4, v2

    .line 86
    mul-int/lit8 v2, v1, 0x1f

    iget-object v4, p0, Lorg/apache/lucene/sandbox/queries/regex/RegexQuery;->term:Lorg/apache/lucene/index/Term;

    if-nez v4, :cond_1

    :goto_1
    add-int v1, v2, v3

    .line 87
    return v1

    .line 85
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/sandbox/queries/regex/RegexQuery;->regexImpl:Lorg/apache/lucene/sandbox/queries/regex/RegexCapabilities;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_0

    .line 86
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/sandbox/queries/regex/RegexQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v3}, Lorg/apache/lucene/index/Term;->hashCode()I

    move-result v3

    goto :goto_1
.end method

.method public setRegexImplementation(Lorg/apache/lucene/sandbox/queries/regex/RegexCapabilities;)V
    .locals 0
    .param p1, "impl"    # Lorg/apache/lucene/sandbox/queries/regex/RegexCapabilities;

    .prologue
    .line 56
    iput-object p1, p0, Lorg/apache/lucene/sandbox/queries/regex/RegexQuery;->regexImpl:Lorg/apache/lucene/sandbox/queries/regex/RegexCapabilities;

    .line 57
    return-void
.end method

.method public toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 71
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 72
    .local v0, "buffer":Ljava/lang/StringBuilder;
    iget-object v1, p0, Lorg/apache/lucene/sandbox/queries/regex/RegexQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 73
    iget-object v1, p0, Lorg/apache/lucene/sandbox/queries/regex/RegexQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 74
    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/sandbox/queries/regex/RegexQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v1}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 77
    invoke-virtual {p0}, Lorg/apache/lucene/sandbox/queries/regex/RegexQuery;->getBoost()F

    move-result v1

    invoke-static {v1}, Lorg/apache/lucene/util/ToStringUtils;->boost(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
