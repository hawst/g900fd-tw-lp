.class public final Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;
.super Lorg/apache/lucene/search/FuzzyTermsEnum;
.source "SlowFuzzyTermsEnum.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum$LinearFuzzyTermsEnum;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/Terms;Lorg/apache/lucene/util/AttributeSource;Lorg/apache/lucene/index/Term;FI)V
    .locals 7
    .param p1, "terms"    # Lorg/apache/lucene/index/Terms;
    .param p2, "atts"    # Lorg/apache/lucene/util/AttributeSource;
    .param p3, "term"    # Lorg/apache/lucene/index/Term;
    .param p4, "minSimilarity"    # F
    .param p5, "prefixLength"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 48
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/search/FuzzyTermsEnum;-><init>(Lorg/apache/lucene/index/Terms;Lorg/apache/lucene/util/AttributeSource;Lorg/apache/lucene/index/Term;FIZ)V

    .line 49
    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;)Lorg/apache/lucene/index/Terms;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;->terms:Lorg/apache/lucene/index/Terms;

    return-object v0
.end method

.method static synthetic access$1(Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;)I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;->termLength:I

    return v0
.end method

.method static synthetic access$2(Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;)I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;->realPrefixLength:I

    return v0
.end method

.method static synthetic access$3(Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;)[I
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;->termText:[I

    return-object v0
.end method

.method static synthetic access$4(Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;)F
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;->minSimilarity:F

    return v0
.end method

.method static synthetic access$5(Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;)F
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;->scale_factor:F

    return v0
.end method

.method static synthetic access$6(Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;)Z
    .locals 1

    .prologue
    .line 44
    iget-boolean v0, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;->raw:Z

    return v0
.end method

.method static synthetic access$7(Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;)I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;->maxEdits:I

    return v0
.end method


# virtual methods
.method protected maxEditDistanceChanged(Lorg/apache/lucene/util/BytesRef;IZ)V
    .locals 2
    .param p1, "lastTerm"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "maxEdits"    # I
    .param p3, "init"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 54
    invoke-virtual {p0, p2, p1}, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;->getAutomatonEnum(ILorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v0

    .line 55
    .local v0, "newEnum":Lorg/apache/lucene/index/TermsEnum;
    if-eqz v0, :cond_1

    .line 56
    invoke-virtual {p0, v0}, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;->setEnum(Lorg/apache/lucene/index/TermsEnum;)V

    .line 60
    :cond_0
    :goto_0
    return-void

    .line 57
    :cond_1
    if-eqz p3, :cond_0

    .line 58
    new-instance v1, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum$LinearFuzzyTermsEnum;

    invoke-direct {v1, p0}, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum$LinearFuzzyTermsEnum;-><init>(Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;)V

    invoke-virtual {p0, v1}, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;->setEnum(Lorg/apache/lucene/index/TermsEnum;)V

    goto :goto_0
.end method
