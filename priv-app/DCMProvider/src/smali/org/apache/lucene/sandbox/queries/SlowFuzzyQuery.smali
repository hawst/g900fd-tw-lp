.class public Lorg/apache/lucene/sandbox/queries/SlowFuzzyQuery;
.super Lorg/apache/lucene/search/MultiTermQuery;
.source "SlowFuzzyQuery.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final defaultMaxExpansions:I = 0x32

.field public static final defaultMinSimilarity:F = 2.0f

.field public static final defaultPrefixLength:I


# instance fields
.field private minimumSimilarity:F

.field private prefixLength:I

.field protected term:Lorg/apache/lucene/index/Term;

.field private termLongEnough:Z


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/Term;)V
    .locals 3
    .param p1, "term"    # Lorg/apache/lucene/index/Term;

    .prologue
    .line 124
    const/high16 v0, 0x40000000    # 2.0f

    const/4 v1, 0x0

    const/16 v2, 0x32

    invoke-direct {p0, p1, v0, v1, v2}, Lorg/apache/lucene/sandbox/queries/SlowFuzzyQuery;-><init>(Lorg/apache/lucene/index/Term;FII)V

    .line 125
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/index/Term;F)V
    .locals 2
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .param p2, "minimumSimilarity"    # F

    .prologue
    .line 117
    const/4 v0, 0x0

    const/16 v1, 0x32

    invoke-direct {p0, p1, p2, v0, v1}, Lorg/apache/lucene/sandbox/queries/SlowFuzzyQuery;-><init>(Lorg/apache/lucene/index/Term;FII)V

    .line 118
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/index/Term;FI)V
    .locals 1
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .param p2, "minimumSimilarity"    # F
    .param p3, "prefixLength"    # I

    .prologue
    .line 110
    const/16 v0, 0x32

    invoke-direct {p0, p1, p2, p3, v0}, Lorg/apache/lucene/sandbox/queries/SlowFuzzyQuery;-><init>(Lorg/apache/lucene/index/Term;FII)V

    .line 111
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/index/Term;FII)V
    .locals 5
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .param p2, "minimumSimilarity"    # F
    .param p3, "prefixLength"    # I
    .param p4, "maxExpansions"    # I

    .prologue
    const/4 v3, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    .line 82
    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lorg/apache/lucene/search/MultiTermQuery;-><init>(Ljava/lang/String;)V

    .line 51
    iput-boolean v3, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyQuery;->termLongEnough:Z

    .line 83
    iput-object p1, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyQuery;->term:Lorg/apache/lucene/index/Term;

    .line 85
    cmpl-float v2, p2, v4

    if-ltz v2, :cond_0

    float-to-int v2, p2

    int-to-float v2, v2

    cmpl-float v2, p2, v2

    if-eqz v2, :cond_0

    .line 86
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "fractional edit distances are not allowed"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 87
    :cond_0
    const/4 v2, 0x0

    cmpg-float v2, p2, v2

    if-gez v2, :cond_1

    .line 88
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "minimumSimilarity < 0"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 89
    :cond_1
    if-gez p3, :cond_2

    .line 90
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "prefixLength < 0"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 91
    :cond_2
    if-gez p4, :cond_3

    .line 92
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "maxExpansions < 0"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 94
    :cond_3
    new-instance v2, Lorg/apache/lucene/search/MultiTermQuery$TopTermsScoringBooleanQueryRewrite;

    invoke-direct {v2, p4}, Lorg/apache/lucene/search/MultiTermQuery$TopTermsScoringBooleanQueryRewrite;-><init>(I)V

    invoke-virtual {p0, v2}, Lorg/apache/lucene/sandbox/queries/SlowFuzzyQuery;->setRewriteMethod(Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;)V

    .line 96
    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v1

    .line 97
    .local v1, "text":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v1, v3, v2}, Ljava/lang/String;->codePointCount(II)I

    move-result v0

    .line 98
    .local v0, "len":I
    if-lez v0, :cond_5

    cmpl-float v2, p2, v4

    if-gez v2, :cond_4

    int-to-float v2, v0

    sub-float v3, v4, p2

    div-float v3, v4, v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_5

    .line 99
    :cond_4
    const/4 v2, 0x1

    iput-boolean v2, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyQuery;->termLongEnough:Z

    .line 102
    :cond_5
    iput p2, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyQuery;->minimumSimilarity:F

    .line 103
    iput p3, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyQuery;->prefixLength:I

    .line 104
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 185
    if-ne p0, p1, :cond_1

    .line 202
    :cond_0
    :goto_0
    return v1

    .line 187
    :cond_1
    invoke-super {p0, p1}, Lorg/apache/lucene/search/MultiTermQuery;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    move v1, v2

    .line 188
    goto :goto_0

    .line 189
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 190
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 191
    check-cast v0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyQuery;

    .line 192
    .local v0, "other":Lorg/apache/lucene/sandbox/queries/SlowFuzzyQuery;
    iget v3, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyQuery;->minimumSimilarity:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 193
    iget v4, v0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyQuery;->minimumSimilarity:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 192
    if-eq v3, v4, :cond_4

    move v1, v2

    .line 194
    goto :goto_0

    .line 195
    :cond_4
    iget v3, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyQuery;->prefixLength:I

    iget v4, v0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyQuery;->prefixLength:I

    if-eq v3, v4, :cond_5

    move v1, v2

    .line 196
    goto :goto_0

    .line 197
    :cond_5
    iget-object v3, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyQuery;->term:Lorg/apache/lucene/index/Term;

    if-nez v3, :cond_6

    .line 198
    iget-object v3, v0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyQuery;->term:Lorg/apache/lucene/index/Term;

    if-eqz v3, :cond_0

    move v1, v2

    .line 199
    goto :goto_0

    .line 200
    :cond_6
    iget-object v3, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyQuery;->term:Lorg/apache/lucene/index/Term;

    iget-object v4, v0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/index/Term;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 201
    goto :goto_0
.end method

.method public getMinSimilarity()F
    .locals 1

    .prologue
    .line 132
    iget v0, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyQuery;->minimumSimilarity:F

    return v0
.end method

.method public getPrefixLength()I
    .locals 1

    .prologue
    .line 141
    iget v0, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyQuery;->prefixLength:I

    return v0
.end method

.method public getTerm()Lorg/apache/lucene/index/Term;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyQuery;->term:Lorg/apache/lucene/index/Term;

    return-object v0
.end method

.method protected getTermsEnum(Lorg/apache/lucene/index/Terms;Lorg/apache/lucene/util/AttributeSource;)Lorg/apache/lucene/index/TermsEnum;
    .locals 6
    .param p1, "terms"    # Lorg/apache/lucene/index/Terms;
    .param p2, "atts"    # Lorg/apache/lucene/util/AttributeSource;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 146
    iget-boolean v0, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyQuery;->termLongEnough:Z

    if-nez v0, :cond_0

    .line 147
    new-instance v0, Lorg/apache/lucene/index/SingleTermsEnum;

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v2}, Lorg/apache/lucene/index/Term;->bytes()Lorg/apache/lucene/util/BytesRef;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/index/SingleTermsEnum;-><init>(Lorg/apache/lucene/index/TermsEnum;Lorg/apache/lucene/util/BytesRef;)V

    .line 149
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;

    invoke-virtual {p0}, Lorg/apache/lucene/sandbox/queries/SlowFuzzyQuery;->getTerm()Lorg/apache/lucene/index/Term;

    move-result-object v3

    iget v4, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyQuery;->minimumSimilarity:F

    iget v5, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyQuery;->prefixLength:I

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/sandbox/queries/SlowFuzzyTermsEnum;-><init>(Lorg/apache/lucene/index/Terms;Lorg/apache/lucene/util/AttributeSource;Lorg/apache/lucene/index/Term;FI)V

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 175
    const/16 v0, 0x1f

    .line 176
    .local v0, "prime":I
    invoke-super {p0}, Lorg/apache/lucene/search/MultiTermQuery;->hashCode()I

    move-result v1

    .line 177
    .local v1, "result":I
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyQuery;->minimumSimilarity:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    add-int v1, v2, v3

    .line 178
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyQuery;->prefixLength:I

    add-int v1, v2, v3

    .line 179
    mul-int/lit8 v3, v1, 0x1f

    iget-object v2, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyQuery;->term:Lorg/apache/lucene/index/Term;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    add-int v1, v3, v2

    .line 180
    return v1

    .line 179
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v2}, Lorg/apache/lucene/index/Term;->hashCode()I

    move-result v2

    goto :goto_0
.end method

.method public toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 161
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 162
    .local v0, "buffer":Ljava/lang/StringBuilder;
    iget-object v1, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 163
    iget-object v1, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 164
    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 166
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v1}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 167
    const/16 v1, 0x7e

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 168
    iget v1, p0, Lorg/apache/lucene/sandbox/queries/SlowFuzzyQuery;->minimumSimilarity:F

    invoke-static {v1}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 169
    invoke-virtual {p0}, Lorg/apache/lucene/sandbox/queries/SlowFuzzyQuery;->getBoost()F

    move-result v1

    invoke-static {v1}, Lorg/apache/lucene/util/ToStringUtils;->boost(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 170
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
