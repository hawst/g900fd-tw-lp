.class Lorg/apache/lucene/sandbox/queries/regex/JavaUtilRegexCapabilities$JavaUtilRegexMatcher;
.super Ljava/lang/Object;
.source "JavaUtilRegexCapabilities.java"

# interfaces
.implements Lorg/apache/lucene/sandbox/queries/regex/RegexCapabilities$RegexMatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/sandbox/queries/regex/JavaUtilRegexCapabilities;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "JavaUtilRegexMatcher"
.end annotation


# instance fields
.field private final matcher:Ljava/util/regex/Matcher;

.field private final pattern:Ljava/util/regex/Pattern;

.field final synthetic this$0:Lorg/apache/lucene/sandbox/queries/regex/JavaUtilRegexCapabilities;

.field private final utf16:Lorg/apache/lucene/util/CharsRef;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/sandbox/queries/regex/JavaUtilRegexCapabilities;Ljava/lang/String;I)V
    .locals 2
    .param p2, "regex"    # Ljava/lang/String;
    .param p3, "flags"    # I

    .prologue
    .line 109
    iput-object p1, p0, Lorg/apache/lucene/sandbox/queries/regex/JavaUtilRegexCapabilities$JavaUtilRegexMatcher;->this$0:Lorg/apache/lucene/sandbox/queries/regex/JavaUtilRegexCapabilities;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107
    new-instance v0, Lorg/apache/lucene/util/CharsRef;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/CharsRef;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/sandbox/queries/regex/JavaUtilRegexCapabilities$JavaUtilRegexMatcher;->utf16:Lorg/apache/lucene/util/CharsRef;

    .line 110
    invoke-static {p2, p3}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/sandbox/queries/regex/JavaUtilRegexCapabilities$JavaUtilRegexMatcher;->pattern:Ljava/util/regex/Pattern;

    .line 111
    iget-object v0, p0, Lorg/apache/lucene/sandbox/queries/regex/JavaUtilRegexCapabilities$JavaUtilRegexMatcher;->pattern:Ljava/util/regex/Pattern;

    iget-object v1, p0, Lorg/apache/lucene/sandbox/queries/regex/JavaUtilRegexCapabilities$JavaUtilRegexMatcher;->utf16:Lorg/apache/lucene/util/CharsRef;

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/sandbox/queries/regex/JavaUtilRegexCapabilities$JavaUtilRegexMatcher;->matcher:Ljava/util/regex/Matcher;

    .line 112
    return-void
.end method


# virtual methods
.method public match(Lorg/apache/lucene/util/BytesRef;)Z
    .locals 4
    .param p1, "term"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 116
    iget-object v0, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v1, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget v2, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    iget-object v3, p0, Lorg/apache/lucene/sandbox/queries/regex/JavaUtilRegexCapabilities$JavaUtilRegexMatcher;->utf16:Lorg/apache/lucene/util/CharsRef;

    invoke-static {v0, v1, v2, v3}, Lorg/apache/lucene/util/UnicodeUtil;->UTF8toUTF16([BIILorg/apache/lucene/util/CharsRef;)V

    .line 117
    iget-object v0, p0, Lorg/apache/lucene/sandbox/queries/regex/JavaUtilRegexCapabilities$JavaUtilRegexMatcher;->matcher:Ljava/util/regex/Matcher;

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->reset()Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    return v0
.end method

.method public prefix()Ljava/lang/String;
    .locals 1

    .prologue
    .line 122
    const/4 v0, 0x0

    return-object v0
.end method
