.class public Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities;
.super Ljava/lang/Object;
.source "JakartaRegexpCapabilities.java"

# interfaces
.implements Lorg/apache/lucene/sandbox/queries/regex/RegexCapabilities;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities$JakartaRegexMatcher;
    }
.end annotation


# static fields
.field public static final FLAG_MATCH_CASEINDEPENDENT:I = 0x1

.field public static final FLAG_MATCH_NORMAL:I

.field private static getPrefixMethod:Ljava/lang/reflect/Method;

.field private static prefixField:Ljava/lang/reflect/Field;


# instance fields
.field private flags:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 42
    :try_start_0
    const-class v1, Lorg/apache/regexp/REProgram;

    const-string v2, "getPrefix"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Class;

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    sput-object v1, Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities;->getPrefixMethod:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 47
    .local v0, "e":Ljava/lang/Exception;
    :goto_0
    :try_start_1
    const-class v1, Lorg/apache/regexp/REProgram;

    const-string v2, "prefix"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    sput-object v1, Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities;->prefixField:Ljava/lang/reflect/Field;

    .line 48
    sget-object v1, Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities;->prefixField:Ljava/lang/reflect/Field;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/AccessibleObject;->setAccessible(Z)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 67
    :goto_1
    return-void

    .line 43
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_0
    move-exception v0

    .line 44
    .restart local v0    # "e":Ljava/lang/Exception;
    sput-object v4, Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities;->getPrefixMethod:Ljava/lang/reflect/Method;

    goto :goto_0

    .line 49
    :catch_1
    move-exception v0

    .line 50
    sput-object v4, Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities;->prefixField:Ljava/lang/reflect/Field;

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities;->flags:I

    .line 72
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "flags"    # I

    .prologue
    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities;->flags:I

    .line 81
    iput p1, p0, Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities;->flags:I

    .line 82
    return-void
.end method

.method static synthetic access$0()Ljava/lang/reflect/Method;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities;->getPrefixMethod:Ljava/lang/reflect/Method;

    return-object v0
.end method

.method static synthetic access$1()Ljava/lang/reflect/Field;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities;->prefixField:Ljava/lang/reflect/Field;

    return-object v0
.end method


# virtual methods
.method public compile(Ljava/lang/String;)Lorg/apache/lucene/sandbox/queries/regex/RegexCapabilities$RegexMatcher;
    .locals 2
    .param p1, "regex"    # Ljava/lang/String;

    .prologue
    .line 86
    new-instance v0, Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities$JakartaRegexMatcher;

    iget v1, p0, Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities;->flags:I

    invoke-direct {v0, p0, p1, v1}, Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities$JakartaRegexMatcher;-><init>(Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities;Ljava/lang/String;I)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 99
    if-ne p0, p1, :cond_1

    .line 110
    :cond_0
    :goto_0
    return v1

    .line 102
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 103
    goto :goto_0

    .line 105
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 106
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 109
    check-cast v0, Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities;

    .line 110
    .local v0, "other":Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities;
    iget v3, p0, Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities;->flags:I

    iget v4, v0, Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities;->flags:I

    if-eq v3, v4, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 91
    const/16 v0, 0x1f

    .line 92
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 93
    .local v1, "result":I
    iget v2, p0, Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities;->flags:I

    add-int/lit8 v1, v2, 0x1f

    .line 94
    return v1
.end method
