.class Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$FieldVals;
.super Ljava/lang/Object;
.source "FuzzyLikeThisQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "FieldVals"
.end annotation


# instance fields
.field fieldName:Ljava/lang/String;

.field minSimilarity:F

.field prefixLength:I

.field queryString:Ljava/lang/String;

.field final synthetic this$0:Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;Ljava/lang/String;FILjava/lang/String;)V
    .locals 0
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "similarity"    # F
    .param p4, "length"    # I
    .param p5, "queryString"    # Ljava/lang/String;

    .prologue
    .line 132
    iput-object p1, p0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$FieldVals;->this$0:Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;

    .line 131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 133
    iput-object p2, p0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$FieldVals;->fieldName:Ljava/lang/String;

    .line 134
    iput p3, p0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$FieldVals;->minSimilarity:F

    .line 135
    iput p4, p0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$FieldVals;->prefixLength:I

    .line 136
    iput-object p5, p0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$FieldVals;->queryString:Ljava/lang/String;

    .line 137
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 154
    if-ne p0, p1, :cond_1

    .line 176
    :cond_0
    :goto_0
    return v1

    .line 156
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 157
    goto :goto_0

    .line 158
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 159
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 160
    check-cast v0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$FieldVals;

    .line 161
    .local v0, "other":Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$FieldVals;
    iget-object v3, p0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$FieldVals;->fieldName:Ljava/lang/String;

    if-nez v3, :cond_4

    .line 162
    iget-object v3, v0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$FieldVals;->fieldName:Ljava/lang/String;

    if-eqz v3, :cond_5

    move v1, v2

    .line 163
    goto :goto_0

    .line 164
    :cond_4
    iget-object v3, p0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$FieldVals;->fieldName:Ljava/lang/String;

    iget-object v4, v0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$FieldVals;->fieldName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    move v1, v2

    .line 165
    goto :goto_0

    .line 166
    :cond_5
    iget v3, p0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$FieldVals;->minSimilarity:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 167
    iget v4, v0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$FieldVals;->minSimilarity:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 166
    if-eq v3, v4, :cond_6

    move v1, v2

    .line 168
    goto :goto_0

    .line 169
    :cond_6
    iget v3, p0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$FieldVals;->prefixLength:I

    iget v4, v0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$FieldVals;->prefixLength:I

    if-eq v3, v4, :cond_7

    move v1, v2

    .line 170
    goto :goto_0

    .line 171
    :cond_7
    iget-object v3, p0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$FieldVals;->queryString:Ljava/lang/String;

    if-nez v3, :cond_8

    .line 172
    iget-object v3, v0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$FieldVals;->queryString:Ljava/lang/String;

    if-eqz v3, :cond_0

    move v1, v2

    .line 173
    goto :goto_0

    .line 174
    :cond_8
    iget-object v3, p0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$FieldVals;->queryString:Ljava/lang/String;

    iget-object v4, v0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$FieldVals;->queryString:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 175
    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 141
    const/16 v0, 0x1f

    .line 142
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 144
    .local v1, "result":I
    iget-object v2, p0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$FieldVals;->fieldName:Ljava/lang/String;

    if-nez v2, :cond_0

    move v2, v3

    .line 143
    :goto_0
    add-int/lit8 v1, v2, 0x1f

    .line 145
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$FieldVals;->minSimilarity:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    add-int v1, v2, v4

    .line 146
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$FieldVals;->prefixLength:I

    add-int v1, v2, v4

    .line 147
    mul-int/lit8 v2, v1, 0x1f

    .line 148
    iget-object v4, p0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$FieldVals;->queryString:Ljava/lang/String;

    if-nez v4, :cond_1

    .line 147
    :goto_1
    add-int v1, v2, v3

    .line 149
    return v1

    .line 144
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$FieldVals;->fieldName:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_0

    .line 148
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$FieldVals;->queryString:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    goto :goto_1
.end method
