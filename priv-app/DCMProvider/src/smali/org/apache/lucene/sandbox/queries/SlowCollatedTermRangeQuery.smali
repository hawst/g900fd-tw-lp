.class public Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;
.super Lorg/apache/lucene/search/MultiTermQuery;
.source "SlowCollatedTermRangeQuery.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private collator:Ljava/text/Collator;

.field private includeLower:Z

.field private includeUpper:Z

.field private lowerTerm:Ljava/lang/String;

.field private upperTerm:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/text/Collator;)V
    .locals 0
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "lowerTerm"    # Ljava/lang/String;
    .param p3, "upperTerm"    # Ljava/lang/String;
    .param p4, "includeLower"    # Z
    .param p5, "includeUpper"    # Z
    .param p6, "collator"    # Ljava/text/Collator;

    .prologue
    .line 75
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/MultiTermQuery;-><init>(Ljava/lang/String;)V

    .line 76
    iput-object p2, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->lowerTerm:Ljava/lang/String;

    .line 77
    iput-object p3, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->upperTerm:Ljava/lang/String;

    .line 78
    iput-boolean p4, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->includeLower:Z

    .line 79
    iput-boolean p5, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->includeUpper:Z

    .line 80
    iput-object p6, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->collator:Ljava/text/Collator;

    .line 81
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 150
    if-ne p0, p1, :cond_1

    .line 176
    :cond_0
    :goto_0
    return v1

    .line 152
    :cond_1
    invoke-super {p0, p1}, Lorg/apache/lucene/search/MultiTermQuery;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    move v1, v2

    .line 153
    goto :goto_0

    .line 154
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 155
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 156
    check-cast v0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;

    .line 157
    .local v0, "other":Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;
    iget-object v3, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->collator:Ljava/text/Collator;

    if-nez v3, :cond_4

    .line 158
    iget-object v3, v0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->collator:Ljava/text/Collator;

    if-eqz v3, :cond_5

    move v1, v2

    .line 159
    goto :goto_0

    .line 160
    :cond_4
    iget-object v3, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->collator:Ljava/text/Collator;

    iget-object v4, v0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->collator:Ljava/text/Collator;

    invoke-virtual {v3, v4}, Ljava/text/Collator;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    move v1, v2

    .line 161
    goto :goto_0

    .line 162
    :cond_5
    iget-boolean v3, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->includeLower:Z

    iget-boolean v4, v0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->includeLower:Z

    if-eq v3, v4, :cond_6

    move v1, v2

    .line 163
    goto :goto_0

    .line 164
    :cond_6
    iget-boolean v3, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->includeUpper:Z

    iget-boolean v4, v0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->includeUpper:Z

    if-eq v3, v4, :cond_7

    move v1, v2

    .line 165
    goto :goto_0

    .line 166
    :cond_7
    iget-object v3, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->lowerTerm:Ljava/lang/String;

    if-nez v3, :cond_8

    .line 167
    iget-object v3, v0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->lowerTerm:Ljava/lang/String;

    if-eqz v3, :cond_9

    move v1, v2

    .line 168
    goto :goto_0

    .line 169
    :cond_8
    iget-object v3, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->lowerTerm:Ljava/lang/String;

    iget-object v4, v0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->lowerTerm:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    move v1, v2

    .line 170
    goto :goto_0

    .line 171
    :cond_9
    iget-object v3, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->upperTerm:Ljava/lang/String;

    if-nez v3, :cond_a

    .line 172
    iget-object v3, v0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->upperTerm:Ljava/lang/String;

    if-eqz v3, :cond_0

    move v1, v2

    .line 173
    goto :goto_0

    .line 174
    :cond_a
    iget-object v3, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->upperTerm:Ljava/lang/String;

    iget-object v4, v0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->upperTerm:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 175
    goto :goto_0
.end method

.method public field()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 116
    invoke-virtual {p0}, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->getField()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCollator()Ljava/text/Collator;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->collator:Ljava/text/Collator;

    return-object v0
.end method

.method public getLowerTerm()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->lowerTerm:Ljava/lang/String;

    return-object v0
.end method

.method protected getTermsEnum(Lorg/apache/lucene/index/Terms;Lorg/apache/lucene/util/AttributeSource;)Lorg/apache/lucene/index/TermsEnum;
    .locals 7
    .param p1, "terms"    # Lorg/apache/lucene/index/Terms;
    .param p2, "atts"    # Lorg/apache/lucene/util/AttributeSource;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 100
    iget-object v0, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->lowerTerm:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->upperTerm:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->collator:Ljava/text/Collator;

    iget-object v2, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->lowerTerm:Ljava/lang/String;

    iget-object v3, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->upperTerm:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_1

    .line 101
    sget-object v1, Lorg/apache/lucene/index/TermsEnum;->EMPTY:Lorg/apache/lucene/index/TermsEnum;

    .line 109
    :cond_0
    :goto_0
    return-object v1

    .line 104
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v1

    .line 106
    .local v1, "tenum":Lorg/apache/lucene/index/TermsEnum;
    iget-object v0, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->lowerTerm:Ljava/lang/String;

    if-nez v0, :cond_2

    iget-object v0, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->upperTerm:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 109
    :cond_2
    new-instance v0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeTermsEnum;

    .line 110
    iget-object v2, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->lowerTerm:Ljava/lang/String;

    iget-object v3, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->upperTerm:Ljava/lang/String;

    iget-boolean v4, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->includeLower:Z

    iget-boolean v5, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->includeUpper:Z

    iget-object v6, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->collator:Ljava/text/Collator;

    .line 109
    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeTermsEnum;-><init>(Lorg/apache/lucene/index/TermsEnum;Ljava/lang/String;Ljava/lang/String;ZZLjava/text/Collator;)V

    move-object v1, v0

    goto :goto_0
.end method

.method public getUpperTerm()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->upperTerm:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 7

    .prologue
    const/16 v5, 0x4d5

    const/16 v4, 0x4cf

    const/4 v3, 0x0

    .line 138
    const/16 v0, 0x1f

    .line 139
    .local v0, "prime":I
    invoke-super {p0}, Lorg/apache/lucene/search/MultiTermQuery;->hashCode()I

    move-result v1

    .line 140
    .local v1, "result":I
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->collator:Ljava/text/Collator;

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    add-int v1, v6, v2

    .line 141
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->includeLower:Z

    if-eqz v2, :cond_1

    move v2, v4

    :goto_1
    add-int v1, v6, v2

    .line 142
    mul-int/lit8 v2, v1, 0x1f

    iget-boolean v6, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->includeUpper:Z

    if-eqz v6, :cond_2

    :goto_2
    add-int v1, v2, v4

    .line 143
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->lowerTerm:Ljava/lang/String;

    if-nez v2, :cond_3

    move v2, v3

    :goto_3
    add-int v1, v4, v2

    .line 144
    mul-int/lit8 v2, v1, 0x1f

    iget-object v4, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->upperTerm:Ljava/lang/String;

    if-nez v4, :cond_4

    :goto_4
    add-int v1, v2, v3

    .line 145
    return v1

    .line 140
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->collator:Ljava/text/Collator;

    invoke-virtual {v2}, Ljava/text/Collator;->hashCode()I

    move-result v2

    goto :goto_0

    :cond_1
    move v2, v5

    .line 141
    goto :goto_1

    :cond_2
    move v4, v5

    .line 142
    goto :goto_2

    .line 143
    :cond_3
    iget-object v2, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->lowerTerm:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_3

    .line 144
    :cond_4
    iget-object v3, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->upperTerm:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    goto :goto_4
.end method

.method public includesLower()Z
    .locals 1

    .prologue
    .line 90
    iget-boolean v0, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->includeLower:Z

    return v0
.end method

.method public includesUpper()Z
    .locals 1

    .prologue
    .line 93
    iget-boolean v0, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->includeUpper:Z

    return v0
.end method

.method public toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 122
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 123
    .local v0, "buffer":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->getField()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 124
    invoke-virtual {p0}, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->getField()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 125
    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 127
    :cond_0
    iget-boolean v1, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->includeLower:Z

    if-eqz v1, :cond_1

    const/16 v1, 0x5b

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 128
    iget-object v1, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->lowerTerm:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->lowerTerm:Ljava/lang/String;

    :goto_1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 129
    const-string v1, " TO "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 130
    iget-object v1, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->upperTerm:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->upperTerm:Ljava/lang/String;

    :goto_2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 131
    iget-boolean v1, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->includeUpper:Z

    if-eqz v1, :cond_4

    const/16 v1, 0x5d

    :goto_3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 132
    invoke-virtual {p0}, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->getBoost()F

    move-result v1

    invoke-static {v1}, Lorg/apache/lucene/util/ToStringUtils;->boost(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 133
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 127
    :cond_1
    const/16 v1, 0x7b

    goto :goto_0

    .line 128
    :cond_2
    const-string v1, "*"

    goto :goto_1

    .line 130
    :cond_3
    const-string v1, "*"

    goto :goto_2

    .line 131
    :cond_4
    const/16 v1, 0x7d

    goto :goto_3
.end method
