.class Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities$JakartaRegexMatcher;
.super Ljava/lang/Object;
.source "JakartaRegexpCapabilities.java"

# interfaces
.implements Lorg/apache/lucene/sandbox/queries/regex/RegexCapabilities$RegexMatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "JakartaRegexMatcher"
.end annotation


# instance fields
.field private regexp:Lorg/apache/regexp/RE;

.field final synthetic this$0:Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities;

.field private final utf16:Lorg/apache/lucene/util/CharsRef;

.field private final utf16wrapper:Lorg/apache/regexp/CharacterIterator;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities;Ljava/lang/String;I)V
    .locals 2
    .param p2, "regex"    # Ljava/lang/String;
    .param p3, "flags"    # I

    .prologue
    .line 141
    iput-object p1, p0, Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities$JakartaRegexMatcher;->this$0:Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 116
    new-instance v0, Lorg/apache/lucene/util/CharsRef;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/CharsRef;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities$JakartaRegexMatcher;->utf16:Lorg/apache/lucene/util/CharsRef;

    .line 117
    new-instance v0, Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities$JakartaRegexMatcher$1;

    invoke-direct {v0, p0}, Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities$JakartaRegexMatcher$1;-><init>(Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities$JakartaRegexMatcher;)V

    iput-object v0, p0, Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities$JakartaRegexMatcher;->utf16wrapper:Lorg/apache/regexp/CharacterIterator;

    .line 142
    new-instance v0, Lorg/apache/regexp/RE;

    invoke-direct {v0, p2, p3}, Lorg/apache/regexp/RE;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities$JakartaRegexMatcher;->regexp:Lorg/apache/regexp/RE;

    .line 143
    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities$JakartaRegexMatcher;)Lorg/apache/lucene/util/CharsRef;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities$JakartaRegexMatcher;->utf16:Lorg/apache/lucene/util/CharsRef;

    return-object v0
.end method


# virtual methods
.method public match(Lorg/apache/lucene/util/BytesRef;)Z
    .locals 4
    .param p1, "term"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 147
    iget-object v0, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v1, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget v2, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    iget-object v3, p0, Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities$JakartaRegexMatcher;->utf16:Lorg/apache/lucene/util/CharsRef;

    invoke-static {v0, v1, v2, v3}, Lorg/apache/lucene/util/UnicodeUtil;->UTF8toUTF16([BIILorg/apache/lucene/util/CharsRef;)V

    .line 148
    iget-object v0, p0, Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities$JakartaRegexMatcher;->regexp:Lorg/apache/regexp/RE;

    iget-object v1, p0, Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities$JakartaRegexMatcher;->utf16wrapper:Lorg/apache/regexp/CharacterIterator;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/apache/regexp/RE;->match(Lorg/apache/regexp/CharacterIterator;I)Z

    move-result v0

    return v0
.end method

.method public prefix()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 155
    :try_start_0
    # getter for: Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities;->getPrefixMethod:Ljava/lang/reflect/Method;
    invoke-static {}, Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities;->access$0()Ljava/lang/reflect/Method;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 156
    # getter for: Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities;->getPrefixMethod:Ljava/lang/reflect/Method;
    invoke-static {}, Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities;->access$0()Ljava/lang/reflect/Method;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities$JakartaRegexMatcher;->regexp:Lorg/apache/regexp/RE;

    invoke-virtual {v4}, Lorg/apache/regexp/RE;->getProgram()Lorg/apache/regexp/REProgram;

    move-result-object v4

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v3, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [C

    .line 162
    .local v1, "prefix":[C
    :goto_0
    if-nez v1, :cond_2

    .line 165
    .end local v1    # "prefix":[C
    :cond_0
    :goto_1
    return-object v2

    .line 157
    :cond_1
    # getter for: Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities;->prefixField:Ljava/lang/reflect/Field;
    invoke-static {}, Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities;->access$1()Ljava/lang/reflect/Field;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 158
    # getter for: Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities;->prefixField:Ljava/lang/reflect/Field;
    invoke-static {}, Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities;->access$1()Ljava/lang/reflect/Field;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities$JakartaRegexMatcher;->regexp:Lorg/apache/regexp/RE;

    invoke-virtual {v4}, Lorg/apache/regexp/RE;->getProgram()Lorg/apache/regexp/REProgram;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [C

    .line 159
    .restart local v1    # "prefix":[C
    goto :goto_0

    .line 162
    :cond_2
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v1}, Ljava/lang/String;-><init>([C)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v2, v3

    goto :goto_1

    .line 163
    .end local v1    # "prefix":[C
    :catch_0
    move-exception v0

    .line 165
    .local v0, "e":Ljava/lang/Exception;
    goto :goto_1
.end method
