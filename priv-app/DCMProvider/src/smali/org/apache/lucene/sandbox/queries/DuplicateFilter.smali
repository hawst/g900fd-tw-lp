.class public Lorg/apache/lucene/sandbox/queries/DuplicateFilter;
.super Lorg/apache/lucene/search/Filter;
.source "DuplicateFilter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/sandbox/queries/DuplicateFilter$KeepMode;,
        Lorg/apache/lucene/sandbox/queries/DuplicateFilter$ProcessingMode;
    }
.end annotation


# instance fields
.field private fieldName:Ljava/lang/String;

.field private keepMode:Lorg/apache/lucene/sandbox/queries/DuplicateFilter$KeepMode;

.field private processingMode:Lorg/apache/lucene/sandbox/queries/DuplicateFilter$ProcessingMode;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 72
    sget-object v0, Lorg/apache/lucene/sandbox/queries/DuplicateFilter$KeepMode;->KM_USE_LAST_OCCURRENCE:Lorg/apache/lucene/sandbox/queries/DuplicateFilter$KeepMode;

    sget-object v1, Lorg/apache/lucene/sandbox/queries/DuplicateFilter$ProcessingMode;->PM_FULL_VALIDATION:Lorg/apache/lucene/sandbox/queries/DuplicateFilter$ProcessingMode;

    invoke-direct {p0, p1, v0, v1}, Lorg/apache/lucene/sandbox/queries/DuplicateFilter;-><init>(Ljava/lang/String;Lorg/apache/lucene/sandbox/queries/DuplicateFilter$KeepMode;Lorg/apache/lucene/sandbox/queries/DuplicateFilter$ProcessingMode;)V

    .line 73
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lorg/apache/lucene/sandbox/queries/DuplicateFilter$KeepMode;Lorg/apache/lucene/sandbox/queries/DuplicateFilter$ProcessingMode;)V
    .locals 0
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "keepMode"    # Lorg/apache/lucene/sandbox/queries/DuplicateFilter$KeepMode;
    .param p3, "processingMode"    # Lorg/apache/lucene/sandbox/queries/DuplicateFilter$ProcessingMode;

    .prologue
    .line 75
    invoke-direct {p0}, Lorg/apache/lucene/search/Filter;-><init>()V

    .line 76
    iput-object p1, p0, Lorg/apache/lucene/sandbox/queries/DuplicateFilter;->fieldName:Ljava/lang/String;

    .line 77
    iput-object p2, p0, Lorg/apache/lucene/sandbox/queries/DuplicateFilter;->keepMode:Lorg/apache/lucene/sandbox/queries/DuplicateFilter$KeepMode;

    .line 78
    iput-object p3, p0, Lorg/apache/lucene/sandbox/queries/DuplicateFilter;->processingMode:Lorg/apache/lucene/sandbox/queries/DuplicateFilter$ProcessingMode;

    .line 79
    return-void
.end method

.method private correctBits(Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/util/FixedBitSet;
    .locals 10
    .param p1, "reader"    # Lorg/apache/lucene/index/AtomicReader;
    .param p2, "acceptDocs"    # Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const v9, 0x7fffffff

    .line 91
    new-instance v0, Lorg/apache/lucene/util/FixedBitSet;

    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v7

    invoke-direct {v0, v7}, Lorg/apache/lucene/util/FixedBitSet;-><init>(I)V

    .line 92
    .local v0, "bits":Lorg/apache/lucene/util/FixedBitSet;
    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReader;->fields()Lorg/apache/lucene/index/Fields;

    move-result-object v7

    iget-object v8, p0, Lorg/apache/lucene/sandbox/queries/DuplicateFilter;->fieldName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lorg/apache/lucene/index/Fields;->terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v5

    .line 94
    .local v5, "terms":Lorg/apache/lucene/index/Terms;
    if-nez v5, :cond_1

    .line 124
    :cond_0
    return-object v0

    .line 98
    :cond_1
    const/4 v7, 0x0

    invoke-virtual {v5, v7}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v6

    .line 99
    .local v6, "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    const/4 v3, 0x0

    .line 101
    .local v3, "docs":Lorg/apache/lucene/index/DocsEnum;
    :cond_2
    :goto_0
    invoke-virtual {v6}, Lorg/apache/lucene/index/TermsEnum;->next()Lorg/apache/lucene/util/BytesRef;

    move-result-object v1

    .line 102
    .local v1, "currTerm":Lorg/apache/lucene/util/BytesRef;
    if-eqz v1, :cond_0

    .line 105
    const/4 v7, 0x0

    invoke-virtual {v6, p2, v3, v7}, Lorg/apache/lucene/index/TermsEnum;->docs(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;I)Lorg/apache/lucene/index/DocsEnum;

    move-result-object v3

    .line 106
    invoke-virtual {v3}, Lorg/apache/lucene/index/DocsEnum;->nextDoc()I

    move-result v2

    .line 107
    .local v2, "doc":I
    if-eq v2, v9, :cond_2

    .line 108
    iget-object v7, p0, Lorg/apache/lucene/sandbox/queries/DuplicateFilter;->keepMode:Lorg/apache/lucene/sandbox/queries/DuplicateFilter$KeepMode;

    sget-object v8, Lorg/apache/lucene/sandbox/queries/DuplicateFilter$KeepMode;->KM_USE_FIRST_OCCURRENCE:Lorg/apache/lucene/sandbox/queries/DuplicateFilter$KeepMode;

    if-ne v7, v8, :cond_3

    .line 109
    invoke-virtual {v0, v2}, Lorg/apache/lucene/util/FixedBitSet;->set(I)V

    goto :goto_0

    .line 111
    :cond_3
    move v4, v2

    .line 113
    .local v4, "lastDoc":I
    :cond_4
    move v4, v2

    .line 114
    invoke-virtual {v3}, Lorg/apache/lucene/index/DocsEnum;->nextDoc()I

    move-result v2

    .line 115
    if-ne v2, v9, :cond_4

    .line 119
    invoke-virtual {v0, v4}, Lorg/apache/lucene/util/FixedBitSet;->set(I)V

    goto :goto_0
.end method

.method private fastBits(Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/util/FixedBitSet;
    .locals 11
    .param p1, "reader"    # Lorg/apache/lucene/index/AtomicReader;
    .param p2, "acceptDocs"    # Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const v10, 0x7fffffff

    const/4 v9, 0x0

    .line 128
    new-instance v0, Lorg/apache/lucene/util/FixedBitSet;

    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v7

    invoke-direct {v0, v7}, Lorg/apache/lucene/util/FixedBitSet;-><init>(I)V

    .line 129
    .local v0, "bits":Lorg/apache/lucene/util/FixedBitSet;
    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v7

    invoke-virtual {v0, v9, v7}, Lorg/apache/lucene/util/FixedBitSet;->set(II)V

    .line 130
    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReader;->fields()Lorg/apache/lucene/index/Fields;

    move-result-object v7

    iget-object v8, p0, Lorg/apache/lucene/sandbox/queries/DuplicateFilter;->fieldName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lorg/apache/lucene/index/Fields;->terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v5

    .line 132
    .local v5, "terms":Lorg/apache/lucene/index/Terms;
    if-nez v5, :cond_1

    .line 171
    :cond_0
    return-object v0

    .line 136
    :cond_1
    const/4 v7, 0x0

    invoke-virtual {v5, v7}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v6

    .line 137
    .local v6, "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    const/4 v3, 0x0

    .line 139
    .local v3, "docs":Lorg/apache/lucene/index/DocsEnum;
    :cond_2
    :goto_0
    invoke-virtual {v6}, Lorg/apache/lucene/index/TermsEnum;->next()Lorg/apache/lucene/util/BytesRef;

    move-result-object v1

    .line 140
    .local v1, "currTerm":Lorg/apache/lucene/util/BytesRef;
    if-eqz v1, :cond_0

    .line 143
    invoke-virtual {v6}, Lorg/apache/lucene/index/TermsEnum;->docFreq()I

    move-result v7

    const/4 v8, 0x1

    if-le v7, v8, :cond_2

    .line 145
    invoke-virtual {v6, p2, v3, v9}, Lorg/apache/lucene/index/TermsEnum;->docs(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;I)Lorg/apache/lucene/index/DocsEnum;

    move-result-object v3

    .line 146
    invoke-virtual {v3}, Lorg/apache/lucene/index/DocsEnum;->nextDoc()I

    move-result v2

    .line 147
    .local v2, "doc":I
    if-eq v2, v10, :cond_3

    .line 148
    iget-object v7, p0, Lorg/apache/lucene/sandbox/queries/DuplicateFilter;->keepMode:Lorg/apache/lucene/sandbox/queries/DuplicateFilter$KeepMode;

    sget-object v8, Lorg/apache/lucene/sandbox/queries/DuplicateFilter$KeepMode;->KM_USE_FIRST_OCCURRENCE:Lorg/apache/lucene/sandbox/queries/DuplicateFilter$KeepMode;

    if-ne v7, v8, :cond_3

    .line 149
    invoke-virtual {v3}, Lorg/apache/lucene/index/DocsEnum;->nextDoc()I

    move-result v2

    .line 153
    :cond_3
    const/4 v4, -0x1

    .line 155
    .local v4, "lastDoc":I
    :cond_4
    move v4, v2

    .line 156
    invoke-virtual {v0, v4}, Lorg/apache/lucene/util/FixedBitSet;->clear(I)V

    .line 157
    invoke-virtual {v3}, Lorg/apache/lucene/index/DocsEnum;->nextDoc()I

    move-result v2

    .line 158
    if-ne v2, v10, :cond_4

    .line 163
    iget-object v7, p0, Lorg/apache/lucene/sandbox/queries/DuplicateFilter;->keepMode:Lorg/apache/lucene/sandbox/queries/DuplicateFilter$KeepMode;

    sget-object v8, Lorg/apache/lucene/sandbox/queries/DuplicateFilter$KeepMode;->KM_USE_LAST_OCCURRENCE:Lorg/apache/lucene/sandbox/queries/DuplicateFilter$KeepMode;

    if-ne v7, v8, :cond_2

    .line 165
    invoke-virtual {v0, v4}, Lorg/apache/lucene/util/FixedBitSet;->set(I)V

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 192
    if-ne p0, p1, :cond_1

    .line 200
    :cond_0
    :goto_0
    return v1

    .line 195
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    .line 196
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 199
    check-cast v0, Lorg/apache/lucene/sandbox/queries/DuplicateFilter;

    .line 200
    .local v0, "other":Lorg/apache/lucene/sandbox/queries/DuplicateFilter;
    iget-object v3, p0, Lorg/apache/lucene/sandbox/queries/DuplicateFilter;->keepMode:Lorg/apache/lucene/sandbox/queries/DuplicateFilter$KeepMode;

    iget-object v4, v0, Lorg/apache/lucene/sandbox/queries/DuplicateFilter;->keepMode:Lorg/apache/lucene/sandbox/queries/DuplicateFilter$KeepMode;

    if-ne v3, v4, :cond_4

    .line 201
    iget-object v3, p0, Lorg/apache/lucene/sandbox/queries/DuplicateFilter;->processingMode:Lorg/apache/lucene/sandbox/queries/DuplicateFilter$ProcessingMode;

    iget-object v4, v0, Lorg/apache/lucene/sandbox/queries/DuplicateFilter;->processingMode:Lorg/apache/lucene/sandbox/queries/DuplicateFilter$ProcessingMode;

    if-ne v3, v4, :cond_4

    .line 202
    iget-object v3, p0, Lorg/apache/lucene/sandbox/queries/DuplicateFilter;->fieldName:Ljava/lang/String;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lorg/apache/lucene/sandbox/queries/DuplicateFilter;->fieldName:Ljava/lang/String;

    iget-object v4, v0, Lorg/apache/lucene/sandbox/queries/DuplicateFilter;->fieldName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_4
    move v1, v2

    .line 200
    goto :goto_0
.end method

.method public getDocIdSet(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/DocIdSet;
    .locals 2
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "acceptDocs"    # Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 83
    iget-object v0, p0, Lorg/apache/lucene/sandbox/queries/DuplicateFilter;->processingMode:Lorg/apache/lucene/sandbox/queries/DuplicateFilter$ProcessingMode;

    sget-object v1, Lorg/apache/lucene/sandbox/queries/DuplicateFilter$ProcessingMode;->PM_FAST_INVALIDATION:Lorg/apache/lucene/sandbox/queries/DuplicateFilter$ProcessingMode;

    if-ne v0, v1, :cond_0

    .line 84
    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lorg/apache/lucene/sandbox/queries/DuplicateFilter;->fastBits(Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/util/FixedBitSet;

    move-result-object v0

    .line 86
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lorg/apache/lucene/sandbox/queries/DuplicateFilter;->correctBits(Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/util/FixedBitSet;

    move-result-object v0

    goto :goto_0
.end method

.method public getFieldName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lorg/apache/lucene/sandbox/queries/DuplicateFilter;->fieldName:Ljava/lang/String;

    return-object v0
.end method

.method public getKeepMode()Lorg/apache/lucene/sandbox/queries/DuplicateFilter$KeepMode;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lorg/apache/lucene/sandbox/queries/DuplicateFilter;->keepMode:Lorg/apache/lucene/sandbox/queries/DuplicateFilter$KeepMode;

    return-object v0
.end method

.method public getProcessingMode()Lorg/apache/lucene/sandbox/queries/DuplicateFilter$ProcessingMode;
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lorg/apache/lucene/sandbox/queries/DuplicateFilter;->processingMode:Lorg/apache/lucene/sandbox/queries/DuplicateFilter$ProcessingMode;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 207
    const/16 v0, 0xd9

    .line 208
    .local v0, "hash":I
    iget-object v1, p0, Lorg/apache/lucene/sandbox/queries/DuplicateFilter;->keepMode:Lorg/apache/lucene/sandbox/queries/DuplicateFilter$KeepMode;

    invoke-virtual {v1}, Lorg/apache/lucene/sandbox/queries/DuplicateFilter$KeepMode;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x1a47

    .line 209
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lorg/apache/lucene/sandbox/queries/DuplicateFilter;->processingMode:Lorg/apache/lucene/sandbox/queries/DuplicateFilter$ProcessingMode;

    invoke-virtual {v2}, Lorg/apache/lucene/sandbox/queries/DuplicateFilter$ProcessingMode;->hashCode()I

    move-result v2

    add-int v0, v1, v2

    .line 210
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lorg/apache/lucene/sandbox/queries/DuplicateFilter;->fieldName:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int v0, v1, v2

    .line 211
    return v0
.end method

.method public setFieldName(Ljava/lang/String;)V
    .locals 0
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 179
    iput-object p1, p0, Lorg/apache/lucene/sandbox/queries/DuplicateFilter;->fieldName:Ljava/lang/String;

    .line 180
    return-void
.end method

.method public setKeepMode(Lorg/apache/lucene/sandbox/queries/DuplicateFilter$KeepMode;)V
    .locals 0
    .param p1, "keepMode"    # Lorg/apache/lucene/sandbox/queries/DuplicateFilter$KeepMode;

    .prologue
    .line 187
    iput-object p1, p0, Lorg/apache/lucene/sandbox/queries/DuplicateFilter;->keepMode:Lorg/apache/lucene/sandbox/queries/DuplicateFilter$KeepMode;

    .line 188
    return-void
.end method

.method public setProcessingMode(Lorg/apache/lucene/sandbox/queries/DuplicateFilter$ProcessingMode;)V
    .locals 0
    .param p1, "processingMode"    # Lorg/apache/lucene/sandbox/queries/DuplicateFilter$ProcessingMode;

    .prologue
    .line 219
    iput-object p1, p0, Lorg/apache/lucene/sandbox/queries/DuplicateFilter;->processingMode:Lorg/apache/lucene/sandbox/queries/DuplicateFilter$ProcessingMode;

    .line 220
    return-void
.end method
