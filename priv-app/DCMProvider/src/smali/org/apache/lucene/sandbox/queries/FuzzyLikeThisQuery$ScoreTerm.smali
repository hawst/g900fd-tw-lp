.class Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;
.super Ljava/lang/Object;
.source "FuzzyLikeThisQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ScoreTerm"
.end annotation


# instance fields
.field fuzziedSourceTerm:Lorg/apache/lucene/index/Term;

.field public score:F

.field public term:Lorg/apache/lucene/index/Term;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/Term;FLorg/apache/lucene/index/Term;)V
    .locals 0
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .param p2, "score"    # F
    .param p3, "fuzziedSourceTerm"    # Lorg/apache/lucene/index/Term;

    .prologue
    .line 336
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 337
    iput-object p1, p0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;->term:Lorg/apache/lucene/index/Term;

    .line 338
    iput p2, p0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;->score:F

    .line 339
    iput-object p3, p0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;->fuzziedSourceTerm:Lorg/apache/lucene/index/Term;

    .line 340
    return-void
.end method
