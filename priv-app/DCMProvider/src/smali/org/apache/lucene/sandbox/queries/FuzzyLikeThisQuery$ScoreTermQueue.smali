.class Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTermQueue;
.super Lorg/apache/lucene/util/PriorityQueue;
.source "FuzzyLikeThisQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ScoreTermQueue"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/util/PriorityQueue",
        "<",
        "Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1, "size"    # I

    .prologue
    .line 345
    invoke-direct {p0, p1}, Lorg/apache/lucene/util/PriorityQueue;-><init>(I)V

    .line 346
    return-void
.end method


# virtual methods
.method protected bridge synthetic lessThan(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;

    check-cast p2, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;

    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTermQueue;->lessThan(Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;)Z

    move-result v0

    return v0
.end method

.method protected lessThan(Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;)Z
    .locals 4
    .param p1, "termA"    # Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;
    .param p2, "termB"    # Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 353
    iget v2, p1, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;->score:F

    iget v3, p2, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;->score:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_2

    .line 354
    iget-object v2, p1, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;->term:Lorg/apache/lucene/index/Term;

    iget-object v3, p2, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/index/Term;->compareTo(Lorg/apache/lucene/index/Term;)I

    move-result v2

    if-lez v2, :cond_1

    .line 356
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 354
    goto :goto_0

    .line 356
    :cond_2
    iget v2, p1, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;->score:F

    iget v3, p2, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery$ScoreTerm;->score:F

    cmpg-float v2, v2, v3

    if-ltz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method
