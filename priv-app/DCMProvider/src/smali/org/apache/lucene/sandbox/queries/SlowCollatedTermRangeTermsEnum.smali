.class public Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeTermsEnum;
.super Lorg/apache/lucene/index/FilteredTermsEnum;
.source "SlowCollatedTermRangeTermsEnum.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private collator:Ljava/text/Collator;

.field private includeLower:Z

.field private includeUpper:Z

.field private lowerTermText:Ljava/lang/String;

.field private upperTermText:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/TermsEnum;Ljava/lang/String;Ljava/lang/String;ZZLjava/text/Collator;)V
    .locals 2
    .param p1, "tenum"    # Lorg/apache/lucene/index/TermsEnum;
    .param p2, "lowerTermText"    # Ljava/lang/String;
    .param p3, "upperTermText"    # Ljava/lang/String;
    .param p4, "includeLower"    # Z
    .param p5, "includeUpper"    # Z
    .param p6, "collator"    # Ljava/text/Collator;

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/FilteredTermsEnum;-><init>(Lorg/apache/lucene/index/TermsEnum;)V

    .line 70
    iput-object p6, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeTermsEnum;->collator:Ljava/text/Collator;

    .line 71
    iput-object p3, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeTermsEnum;->upperTermText:Ljava/lang/String;

    .line 72
    iput-object p2, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeTermsEnum;->lowerTermText:Ljava/lang/String;

    .line 73
    iput-boolean p4, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeTermsEnum;->includeLower:Z

    .line 74
    iput-boolean p5, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeTermsEnum;->includeUpper:Z

    .line 78
    iget-object v1, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeTermsEnum;->lowerTermText:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 79
    const-string v1, ""

    iput-object v1, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeTermsEnum;->lowerTermText:Ljava/lang/String;

    .line 80
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeTermsEnum;->includeLower:Z

    .line 84
    :cond_0
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, ""

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    .line 85
    .local v0, "startBytesRef":Lorg/apache/lucene/util/BytesRef;
    invoke-virtual {p0, v0}, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeTermsEnum;->setInitialSeekTerm(Lorg/apache/lucene/util/BytesRef;)V

    .line 86
    return-void
.end method


# virtual methods
.method protected accept(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;
    .locals 3
    .param p1, "term"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 90
    iget-boolean v0, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeTermsEnum;->includeLower:Z

    if-eqz v0, :cond_2

    .line 91
    iget-object v0, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeTermsEnum;->collator:Ljava/text/Collator;

    invoke-virtual {p1}, Lorg/apache/lucene/util/BytesRef;->utf8ToString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeTermsEnum;->lowerTermText:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_3

    .line 93
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeTermsEnum;->upperTermText:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 94
    iget-boolean v0, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeTermsEnum;->includeUpper:Z

    if-eqz v0, :cond_4

    .line 95
    iget-object v0, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeTermsEnum;->collator:Ljava/text/Collator;

    invoke-virtual {p1}, Lorg/apache/lucene/util/BytesRef;->utf8ToString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeTermsEnum;->upperTermText:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-gtz v0, :cond_3

    .line 97
    :cond_1
    :goto_0
    sget-object v0, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;->YES:Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

    .line 99
    :goto_1
    return-object v0

    .line 92
    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeTermsEnum;->collator:Ljava/text/Collator;

    invoke-virtual {p1}, Lorg/apache/lucene/util/BytesRef;->utf8ToString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeTermsEnum;->lowerTermText:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-gtz v0, :cond_0

    .line 99
    :cond_3
    sget-object v0, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;->NO:Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

    goto :goto_1

    .line 96
    :cond_4
    iget-object v0, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeTermsEnum;->collator:Ljava/text/Collator;

    invoke-virtual {p1}, Lorg/apache/lucene/util/BytesRef;->utf8ToString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeTermsEnum;->upperTermText:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-gez v0, :cond_3

    goto :goto_0
.end method
