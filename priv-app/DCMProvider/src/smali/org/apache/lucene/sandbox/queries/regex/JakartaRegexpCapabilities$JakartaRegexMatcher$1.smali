.class Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities$JakartaRegexMatcher$1;
.super Ljava/lang/Object;
.source "JakartaRegexpCapabilities.java"

# interfaces
.implements Lorg/apache/regexp/CharacterIterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities$JakartaRegexMatcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities$JakartaRegexMatcher;


# direct methods
.method constructor <init>(Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities$JakartaRegexMatcher;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities$JakartaRegexMatcher$1;->this$1:Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities$JakartaRegexMatcher;

    .line 117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public charAt(I)C
    .locals 1
    .param p1, "pos"    # I

    .prologue
    .line 121
    iget-object v0, p0, Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities$JakartaRegexMatcher$1;->this$1:Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities$JakartaRegexMatcher;

    # getter for: Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities$JakartaRegexMatcher;->utf16:Lorg/apache/lucene/util/CharsRef;
    invoke-static {v0}, Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities$JakartaRegexMatcher;->access$0(Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities$JakartaRegexMatcher;)Lorg/apache/lucene/util/CharsRef;

    move-result-object v0

    iget-object v0, v0, Lorg/apache/lucene/util/CharsRef;->chars:[C

    aget-char v0, v0, p1

    return v0
.end method

.method public isEnd(I)Z
    .locals 1
    .param p1, "pos"    # I

    .prologue
    .line 126
    iget-object v0, p0, Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities$JakartaRegexMatcher$1;->this$1:Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities$JakartaRegexMatcher;

    # getter for: Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities$JakartaRegexMatcher;->utf16:Lorg/apache/lucene/util/CharsRef;
    invoke-static {v0}, Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities$JakartaRegexMatcher;->access$0(Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities$JakartaRegexMatcher;)Lorg/apache/lucene/util/CharsRef;

    move-result-object v0

    iget v0, v0, Lorg/apache/lucene/util/CharsRef;->length:I

    if-lt p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public substring(I)Ljava/lang/String;
    .locals 1
    .param p1, "beginIndex"    # I

    .prologue
    .line 131
    iget-object v0, p0, Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities$JakartaRegexMatcher$1;->this$1:Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities$JakartaRegexMatcher;

    # getter for: Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities$JakartaRegexMatcher;->utf16:Lorg/apache/lucene/util/CharsRef;
    invoke-static {v0}, Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities$JakartaRegexMatcher;->access$0(Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities$JakartaRegexMatcher;)Lorg/apache/lucene/util/CharsRef;

    move-result-object v0

    iget v0, v0, Lorg/apache/lucene/util/CharsRef;->length:I

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities$JakartaRegexMatcher$1;->substring(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public substring(II)Ljava/lang/String;
    .locals 3
    .param p1, "beginIndex"    # I
    .param p2, "endIndex"    # I

    .prologue
    .line 136
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities$JakartaRegexMatcher$1;->this$1:Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities$JakartaRegexMatcher;

    # getter for: Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities$JakartaRegexMatcher;->utf16:Lorg/apache/lucene/util/CharsRef;
    invoke-static {v1}, Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities$JakartaRegexMatcher;->access$0(Lorg/apache/lucene/sandbox/queries/regex/JakartaRegexpCapabilities$JakartaRegexMatcher;)Lorg/apache/lucene/util/CharsRef;

    move-result-object v1

    iget-object v1, v1, Lorg/apache/lucene/util/CharsRef;->chars:[C

    sub-int v2, p2, p1

    invoke-direct {v0, v1, p1, v2}, Ljava/lang/String;-><init>([CII)V

    return-object v0
.end method
