.class public Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeFilter;
.super Lorg/apache/lucene/search/MultiTermQueryWrapperFilter;
.source "SlowCollatedTermRangeFilter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/MultiTermQueryWrapperFilter",
        "<",
        "Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/text/Collator;)V
    .locals 7
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "lowerTerm"    # Ljava/lang/String;
    .param p3, "upperTerm"    # Ljava/lang/String;
    .param p4, "includeLower"    # Z
    .param p5, "includeUpper"    # Z
    .param p6, "collator"    # Ljava/text/Collator;

    .prologue
    .line 57
    new-instance v0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/text/Collator;)V

    invoke-direct {p0, v0}, Lorg/apache/lucene/search/MultiTermQueryWrapperFilter;-><init>(Lorg/apache/lucene/search/MultiTermQuery;)V

    .line 58
    return-void
.end method


# virtual methods
.method public getCollator()Ljava/text/Collator;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeFilter;->query:Lorg/apache/lucene/search/MultiTermQuery;

    check-cast v0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;

    invoke-virtual {v0}, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->getCollator()Ljava/text/Collator;

    move-result-object v0

    return-object v0
.end method

.method public getLowerTerm()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeFilter;->query:Lorg/apache/lucene/search/MultiTermQuery;

    check-cast v0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;

    invoke-virtual {v0}, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->getLowerTerm()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUpperTerm()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeFilter;->query:Lorg/apache/lucene/search/MultiTermQuery;

    check-cast v0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;

    invoke-virtual {v0}, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->getUpperTerm()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public includesLower()Z
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeFilter;->query:Lorg/apache/lucene/search/MultiTermQuery;

    check-cast v0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;

    invoke-virtual {v0}, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->includesLower()Z

    move-result v0

    return v0
.end method

.method public includesUpper()Z
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeFilter;->query:Lorg/apache/lucene/search/MultiTermQuery;

    check-cast v0, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;

    invoke-virtual {v0}, Lorg/apache/lucene/sandbox/queries/SlowCollatedTermRangeQuery;->includesUpper()Z

    move-result v0

    return v0
.end method
