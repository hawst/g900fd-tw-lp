.class public Lorg/apache/lucene/queryparser/xml/DOMUtils;
.super Ljava/lang/Object;
.source "DOMUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getAttribute(Lorg/w3c/dom/Element;Ljava/lang/String;F)F
    .locals 2
    .param p0, "element"    # Lorg/w3c/dom/Element;
    .param p1, "attributeName"    # Ljava/lang/String;
    .param p2, "deflt"    # F

    .prologue
    .line 133
    invoke-interface {p0, p1}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 134
    .local v0, "result":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .end local p2    # "deflt":F
    :cond_0
    :goto_0
    return p2

    .restart local p2    # "deflt":F
    :cond_1
    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result p2

    goto :goto_0
.end method

.method public static getAttribute(Lorg/w3c/dom/Element;Ljava/lang/String;I)I
    .locals 2
    .param p0, "element"    # Lorg/w3c/dom/Element;
    .param p1, "attributeName"    # Ljava/lang/String;
    .param p2, "deflt"    # I

    .prologue
    .line 138
    invoke-interface {p0, p1}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 139
    .local v0, "result":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .end local p2    # "deflt":I
    :cond_0
    :goto_0
    return p2

    .restart local p2    # "deflt":I
    :cond_1
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p2

    goto :goto_0
.end method

.method public static getAttribute(Lorg/w3c/dom/Element;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "element"    # Lorg/w3c/dom/Element;
    .param p1, "attributeName"    # Ljava/lang/String;
    .param p2, "deflt"    # Ljava/lang/String;

    .prologue
    .line 128
    invoke-interface {p0, p1}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 129
    .local v0, "result":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    move-object v0, p2

    .end local v0    # "result":Ljava/lang/String;
    :cond_1
    return-object v0
.end method

.method public static getAttribute(Lorg/w3c/dom/Element;Ljava/lang/String;Z)Z
    .locals 2
    .param p0, "element"    # Lorg/w3c/dom/Element;
    .param p1, "attributeName"    # Ljava/lang/String;
    .param p2, "deflt"    # Z

    .prologue
    .line 144
    invoke-interface {p0, p1}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 145
    .local v0, "result":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .end local p2    # "deflt":Z
    :cond_0
    :goto_0
    return p2

    .restart local p2    # "deflt":Z
    :cond_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    goto :goto_0
.end method

.method public static getAttributeOrFail(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "e"    # Lorg/w3c/dom/Element;
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/xml/ParserException;
        }
    .end annotation

    .prologue
    .line 52
    invoke-interface {p0, p1}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 53
    .local v0, "v":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 54
    new-instance v1, Lorg/apache/lucene/queryparser/xml/ParserException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-interface {p0}, Lorg/w3c/dom/Element;->getTagName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " missing \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 55
    const-string v3, "\" attribute"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 54
    invoke-direct {v1, v2}, Lorg/apache/lucene/queryparser/xml/ParserException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 57
    :cond_0
    return-object v0
.end method

.method public static getAttributeWithInheritance(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "element"    # Lorg/w3c/dom/Element;
    .param p1, "attributeName"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 95
    invoke-interface {p0, p1}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 96
    .local v2, "result":Ljava/lang/String;
    if-eqz v2, :cond_0

    const-string v4, ""

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 97
    :cond_0
    invoke-interface {p0}, Lorg/w3c/dom/Element;->getParentNode()Lorg/w3c/dom/Node;

    move-result-object v0

    .line 98
    .local v0, "n":Lorg/w3c/dom/Node;
    if-eq v0, p0, :cond_1

    if-nez v0, :cond_3

    :cond_1
    move-object v2, v3

    .line 107
    .end local v0    # "n":Lorg/w3c/dom/Node;
    .end local v2    # "result":Ljava/lang/String;
    :cond_2
    :goto_0
    return-object v2

    .line 101
    .restart local v0    # "n":Lorg/w3c/dom/Node;
    .restart local v2    # "result":Ljava/lang/String;
    :cond_3
    instance-of v4, v0, Lorg/w3c/dom/Element;

    if-eqz v4, :cond_4

    move-object v1, v0

    .line 102
    check-cast v1, Lorg/w3c/dom/Element;

    .line 103
    .local v1, "parent":Lorg/w3c/dom/Element;
    invoke-static {v1, p1}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttributeWithInheritance(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .end local v1    # "parent":Lorg/w3c/dom/Element;
    :cond_4
    move-object v2, v3

    .line 105
    goto :goto_0
.end method

.method public static getAttributeWithInheritanceOrFail(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "e"    # Lorg/w3c/dom/Element;
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/xml/ParserException;
        }
    .end annotation

    .prologue
    .line 61
    invoke-static {p0, p1}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttributeWithInheritance(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 62
    .local v0, "v":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 63
    new-instance v1, Lorg/apache/lucene/queryparser/xml/ParserException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-interface {p0}, Lorg/w3c/dom/Element;->getTagName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " missing \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 64
    const-string v3, "\" attribute"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 63
    invoke-direct {v1, v2}, Lorg/apache/lucene/queryparser/xml/ParserException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 66
    :cond_0
    return-object v0
.end method

.method public static getChildByTagName(Lorg/w3c/dom/Element;Ljava/lang/String;)Lorg/w3c/dom/Element;
    .locals 3
    .param p0, "e"    # Lorg/w3c/dom/Element;
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 81
    invoke-interface {p0}, Lorg/w3c/dom/Element;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v0

    .local v0, "kid":Lorg/w3c/dom/Node;
    :goto_0
    if-nez v0, :cond_0

    .line 86
    const/4 v0, 0x0

    .end local v0    # "kid":Lorg/w3c/dom/Node;
    :goto_1
    return-object v0

    .line 82
    .restart local v0    # "kid":Lorg/w3c/dom/Node;
    :cond_0
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 83
    check-cast v0, Lorg/w3c/dom/Element;

    goto :goto_1

    .line 81
    :cond_1
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNextSibling()Lorg/w3c/dom/Node;

    move-result-object v0

    goto :goto_0
.end method

.method public static getChildByTagOrFail(Lorg/w3c/dom/Element;Ljava/lang/String;)Lorg/w3c/dom/Element;
    .locals 4
    .param p0, "e"    # Lorg/w3c/dom/Element;
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/xml/ParserException;
        }
    .end annotation

    .prologue
    .line 34
    invoke-static {p0, p1}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getChildByTagName(Lorg/w3c/dom/Element;Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    .line 35
    .local v0, "kid":Lorg/w3c/dom/Element;
    if-nez v0, :cond_0

    .line 36
    new-instance v1, Lorg/apache/lucene/queryparser/xml/ParserException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-interface {p0}, Lorg/w3c/dom/Element;->getTagName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " missing \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 37
    const-string v3, "\" child element"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 36
    invoke-direct {v1, v2}, Lorg/apache/lucene/queryparser/xml/ParserException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 39
    :cond_0
    return-object v0
.end method

.method public static getChildTextByTagName(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "e"    # Lorg/w3c/dom/Element;
    .param p1, "tagName"    # Ljava/lang/String;

    .prologue
    .line 113
    invoke-static {p0, p1}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getChildByTagName(Lorg/w3c/dom/Element;Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    .line 114
    .local v0, "child":Lorg/w3c/dom/Element;
    if-eqz v0, :cond_0

    invoke-static {v0}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getText(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getFirstChildElement(Lorg/w3c/dom/Element;)Lorg/w3c/dom/Element;
    .locals 3
    .param p0, "element"    # Lorg/w3c/dom/Element;

    .prologue
    .line 158
    invoke-interface {p0}, Lorg/w3c/dom/Element;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v0

    .local v0, "kid":Lorg/w3c/dom/Node;
    :goto_0
    if-nez v0, :cond_0

    .line 163
    const/4 v0, 0x0

    .end local v0    # "kid":Lorg/w3c/dom/Node;
    :goto_1
    return-object v0

    .line 159
    .restart local v0    # "kid":Lorg/w3c/dom/Node;
    :cond_0
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 160
    check-cast v0, Lorg/w3c/dom/Element;

    goto :goto_1

    .line 158
    :cond_1
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNextSibling()Lorg/w3c/dom/Node;

    move-result-object v0

    goto :goto_0
.end method

.method public static getFirstChildOrFail(Lorg/w3c/dom/Element;)Lorg/w3c/dom/Element;
    .locals 4
    .param p0, "e"    # Lorg/w3c/dom/Element;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/xml/ParserException;
        }
    .end annotation

    .prologue
    .line 43
    invoke-static {p0}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getFirstChildElement(Lorg/w3c/dom/Element;)Lorg/w3c/dom/Element;

    move-result-object v0

    .line 44
    .local v0, "kid":Lorg/w3c/dom/Element;
    if-nez v0, :cond_0

    .line 45
    new-instance v1, Lorg/apache/lucene/queryparser/xml/ParserException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-interface {p0}, Lorg/w3c/dom/Element;->getTagName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 46
    const-string v3, " does not contain a child element"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 45
    invoke-direct {v1, v2}, Lorg/apache/lucene/queryparser/xml/ParserException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 48
    :cond_0
    return-object v0
.end method

.method public static getNonBlankTextOrFail(Lorg/w3c/dom/Element;)Ljava/lang/String;
    .locals 4
    .param p0, "e"    # Lorg/w3c/dom/Element;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/xml/ParserException;
        }
    .end annotation

    .prologue
    .line 70
    invoke-static {p0}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getText(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v0

    .line 71
    .local v0, "v":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 72
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 73
    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_2

    .line 74
    :cond_1
    new-instance v1, Lorg/apache/lucene/queryparser/xml/ParserException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-interface {p0}, Lorg/w3c/dom/Element;->getTagName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " has no text"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/lucene/queryparser/xml/ParserException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 76
    :cond_2
    return-object v0
.end method

.method public static getText(Lorg/w3c/dom/Node;)Ljava/lang/String;
    .locals 2
    .param p0, "e"    # Lorg/w3c/dom/Node;

    .prologue
    .line 152
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 153
    .local v0, "sb":Ljava/lang/StringBuilder;
    invoke-static {p0, v0}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getTextBuffer(Lorg/w3c/dom/Node;Ljava/lang/StringBuilder;)V

    .line 154
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private static getTextBuffer(Lorg/w3c/dom/Node;Ljava/lang/StringBuilder;)V
    .locals 2
    .param p0, "e"    # Lorg/w3c/dom/Node;
    .param p1, "sb"    # Ljava/lang/StringBuilder;

    .prologue
    .line 167
    invoke-interface {p0}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v0

    .local v0, "kid":Lorg/w3c/dom/Node;
    :goto_0
    if-nez v0, :cond_0

    .line 183
    return-void

    .line 168
    :cond_0
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 167
    :goto_1
    :pswitch_0
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNextSibling()Lorg/w3c/dom/Node;

    move-result-object v0

    goto :goto_0

    .line 170
    :pswitch_1
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 174
    :pswitch_2
    invoke-static {v0, p1}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getTextBuffer(Lorg/w3c/dom/Node;Ljava/lang/StringBuilder;)V

    goto :goto_1

    .line 178
    :pswitch_3
    invoke-static {v0, p1}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getTextBuffer(Lorg/w3c/dom/Node;Ljava/lang/StringBuilder;)V

    goto :goto_1

    .line 168
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static insertChild(Lorg/w3c/dom/Element;Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Element;
    .locals 2
    .param p0, "parent"    # Lorg/w3c/dom/Element;
    .param p1, "tagName"    # Ljava/lang/String;
    .param p2, "text"    # Ljava/lang/String;

    .prologue
    .line 119
    invoke-interface {p0}, Lorg/w3c/dom/Element;->getOwnerDocument()Lorg/w3c/dom/Document;

    move-result-object v1

    invoke-interface {v1, p1}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    .line 120
    .local v0, "child":Lorg/w3c/dom/Element;
    invoke-interface {p0, v0}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 121
    if-eqz p2, :cond_0

    .line 122
    invoke-interface {v0}, Lorg/w3c/dom/Element;->getOwnerDocument()Lorg/w3c/dom/Document;

    move-result-object v1

    invoke-interface {v1, p2}, Lorg/w3c/dom/Document;->createTextNode(Ljava/lang/String;)Lorg/w3c/dom/Text;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 124
    :cond_0
    return-object v0
.end method

.method public static loadXML(Ljava/io/Reader;)Lorg/w3c/dom/Document;
    .locals 7
    .param p0, "is"    # Ljava/io/Reader;

    .prologue
    .line 192
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v1

    .line 193
    .local v1, "dbf":Ljavax/xml/parsers/DocumentBuilderFactory;
    const/4 v0, 0x0

    .line 196
    .local v0, "db":Ljavax/xml/parsers/DocumentBuilder;
    :try_start_0
    invoke-virtual {v1}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 203
    const/4 v2, 0x0

    .line 205
    .local v2, "doc":Lorg/w3c/dom/Document;
    :try_start_1
    new-instance v4, Lorg/xml/sax/InputSource;

    invoke-direct {v4, p0}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/Reader;)V

    invoke-virtual {v0, v4}, Ljavax/xml/parsers/DocumentBuilder;->parse(Lorg/xml/sax/InputSource;)Lorg/w3c/dom/Document;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v2

    .line 212
    return-object v2

    .line 198
    .end local v2    # "doc":Lorg/w3c/dom/Document;
    :catch_0
    move-exception v3

    .line 199
    .local v3, "se":Ljava/lang/Exception;
    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "Parser configuration error"

    invoke-direct {v4, v5, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4

    .line 208
    .end local v3    # "se":Ljava/lang/Exception;
    .restart local v2    # "doc":Lorg/w3c/dom/Document;
    :catch_1
    move-exception v3

    .line 209
    .restart local v3    # "se":Ljava/lang/Exception;
    new-instance v4, Ljava/lang/RuntimeException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Error parsing file:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
.end method
