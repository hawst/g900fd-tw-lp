.class public Lorg/apache/lucene/queryparser/flexible/standard/processors/OpenRangeQueryNodeProcessor;
.super Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl;
.source "OpenRangeQueryNodeProcessor.java"


# static fields
.field public static final OPEN_RANGE_TOKEN:Ljava/lang/String; = "*"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl;-><init>()V

    return-void
.end method


# virtual methods
.method protected postProcessNode(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 8
    .param p1, "node"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 42
    instance-of v5, p1, Lorg/apache/lucene/queryparser/flexible/standard/nodes/TermRangeQueryNode;

    if-eqz v5, :cond_4

    move-object v2, p1

    .line 43
    check-cast v2, Lorg/apache/lucene/queryparser/flexible/standard/nodes/TermRangeQueryNode;

    .line 44
    .local v2, "rangeNode":Lorg/apache/lucene/queryparser/flexible/standard/nodes/TermRangeQueryNode;
    invoke-virtual {v2}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/TermRangeQueryNode;->getLowerBound()Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldValuePairQueryNode;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;

    .line 45
    .local v0, "lowerNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    invoke-virtual {v2}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/TermRangeQueryNode;->getUpperBound()Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldValuePairQueryNode;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;

    .line 46
    .local v3, "upperNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    invoke-virtual {v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    .line 47
    .local v1, "lowerText":Ljava/lang/CharSequence;
    invoke-virtual {v3}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    .line 49
    .local v4, "upperText":Ljava/lang/CharSequence;
    const-string v5, "*"

    invoke-virtual {v3}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->getTextAsString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 50
    instance-of v5, v4, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;

    if-eqz v5, :cond_0

    move-object v5, v4

    check-cast v5, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;

    .line 51
    invoke-virtual {v5, v7}, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;->wasEscaped(I)Z

    move-result v5

    if-nez v5, :cond_1

    .line 52
    :cond_0
    const-string v4, ""

    .line 55
    :cond_1
    const-string v5, "*"

    invoke-virtual {v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->getTextAsString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 56
    instance-of v5, v1, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;

    if-eqz v5, :cond_2

    move-object v5, v1

    check-cast v5, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;

    .line 57
    invoke-virtual {v5, v7}, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;->wasEscaped(I)Z

    move-result v5

    if-nez v5, :cond_3

    .line 58
    :cond_2
    const-string v1, ""

    .line 61
    :cond_3
    invoke-virtual {v0, v1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->setText(Ljava/lang/CharSequence;)V

    .line 62
    invoke-virtual {v3, v4}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->setText(Ljava/lang/CharSequence;)V

    .line 65
    .end local v0    # "lowerNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    .end local v1    # "lowerText":Ljava/lang/CharSequence;
    .end local v2    # "rangeNode":Lorg/apache/lucene/queryparser/flexible/standard/nodes/TermRangeQueryNode;
    .end local v3    # "upperNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    .end local v4    # "upperText":Ljava/lang/CharSequence;
    :cond_4
    return-object p1
.end method

.method protected preProcessNode(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 0
    .param p1, "node"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 71
    return-object p1
.end method

.method protected setChildrenOrder(Ljava/util/List;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 77
    .local p1, "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    return-object p1
.end method
