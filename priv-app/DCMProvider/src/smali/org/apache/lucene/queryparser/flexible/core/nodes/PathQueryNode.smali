.class public Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode;
.super Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;
.source "PathQueryNode.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode$QueryText;
    }
.end annotation


# instance fields
.field private values:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode$QueryText;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode$QueryText;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 116
    .local p1, "pathElements":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode$QueryText;>;"
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;-><init>()V

    .line 110
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode;->values:Ljava/util/List;

    .line 117
    iput-object p1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode;->values:Ljava/util/List;

    .line 118
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    .line 120
    new-instance v0, Ljava/lang/RuntimeException;

    .line 121
    const-string v1, "PathQuerynode requires more 2 or more path elements."

    .line 120
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 123
    :cond_0
    return-void
.end method

.method private getPathString()Ljava/lang/CharSequence;
    .locals 5

    .prologue
    .line 177
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 179
    .local v0, "path":Ljava/lang/StringBuilder;
    iget-object v2, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode;->values:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 182
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 179
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode$QueryText;

    .line 180
    .local v1, "pathelement":Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode$QueryText;
    const-string v3, "/"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v1, Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode$QueryText;->value:Ljava/lang/CharSequence;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method


# virtual methods
.method public cloneTree()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 208
    invoke-super {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;->cloneTree()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode;

    .line 211
    .local v0, "clone":Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode;
    iget-object v3, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode;->values:Ljava/util/List;

    if-eqz v3, :cond_0

    .line 212
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 213
    .local v1, "localValues":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode$QueryText;>;"
    iget-object v3, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode;->values:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 216
    iput-object v1, v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode;->values:Ljava/util/List;

    .line 219
    .end local v1    # "localValues":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode$QueryText;>;"
    :cond_0
    return-object v0

    .line 213
    .restart local v1    # "localValues":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode$QueryText;>;"
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode$QueryText;

    .line 214
    .local v2, "value":Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode$QueryText;
    invoke-virtual {v2}, Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode$QueryText;->clone()Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode$QueryText;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public getFirstPathElement()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 156
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode;->values:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode$QueryText;

    iget-object v0, v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode$QueryText;->value:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getPathElement(I)Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode$QueryText;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 147
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode;->values:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode$QueryText;

    return-object v0
.end method

.method public getPathElements()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode$QueryText;",
            ">;"
        }
    .end annotation

    .prologue
    .line 131
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode;->values:Ljava/util/List;

    return-object v0
.end method

.method public getPathElements(I)Ljava/util/List;
    .locals 3
    .param p1, "startIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode$QueryText;",
            ">;"
        }
    .end annotation

    .prologue
    .line 165
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 166
    .local v1, "rValues":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode$QueryText;>;"
    move v0, p1

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode;->values:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lt v0, v2, :cond_0

    .line 173
    return-object v1

    .line 168
    :cond_0
    :try_start_0
    iget-object v2, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode;->values:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode$QueryText;

    invoke-virtual {v2}, Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode$QueryText;->clone()Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode$QueryText;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 166
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 169
    :catch_0
    move-exception v2

    goto :goto_1
.end method

.method public setPathElements(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode$QueryText;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 138
    .local p1, "elements":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode$QueryText;>;"
    iput-object p1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode;->values:Ljava/util/List;

    .line 139
    return-void
.end method

.method public toQueryString(Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;)Ljava/lang/CharSequence;
    .locals 7
    .param p1, "escaper"    # Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;

    .prologue
    .line 187
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 188
    .local v0, "path":Ljava/lang/StringBuilder;
    const-string v3, "/"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode;->getFirstPathElement()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 190
    const/4 v3, 0x1

    invoke-virtual {p0, v3}, Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode;->getPathElements(I)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_0

    .line 195
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 190
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode$QueryText;

    .line 191
    .local v1, "pathelement":Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode$QueryText;
    iget-object v4, v1, Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode$QueryText;->value:Ljava/lang/CharSequence;

    .line 192
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    sget-object v6, Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax$Type;->STRING:Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax$Type;

    .line 191
    invoke-interface {p1, v4, v5, v6}, Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;->escape(Ljava/lang/CharSequence;Ljava/util/Locale;Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax$Type;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 193
    .local v2, "value":Ljava/lang/CharSequence;
    const-string v4, "/\""

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 200
    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode;->values:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode$QueryText;

    .line 202
    .local v0, "text":Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode$QueryText;
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "<path start=\'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode$QueryText;->begin:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' end=\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode$QueryText;->end:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' path=\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 203
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode;->getPathString()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'/>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 202
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
