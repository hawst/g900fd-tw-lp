.class public final Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;
.super Ljava/lang/Object;
.source "UnescapedCharSequence.java"

# interfaces
.implements Ljava/lang/CharSequence;


# instance fields
.field private chars:[C

.field private wasEscaped:[Z


# direct methods
.method public constructor <init>(Ljava/lang/CharSequence;)V
    .locals 3
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    new-array v1, v1, [C

    iput-object v1, p0, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;->chars:[C

    .line 46
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    new-array v1, v1, [Z

    iput-object v1, p0, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;->wasEscaped:[Z

    .line 47
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 51
    return-void

    .line 48
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;->chars:[C

    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    aput-char v2, v1, v0

    .line 49
    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;->wasEscaped:[Z

    const/4 v2, 0x0

    aput-boolean v2, v1, v0

    .line 47
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private constructor <init>(Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;)V
    .locals 3
    .param p1, "text"    # Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    invoke-virtual {p1}, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;->length()I

    move-result v1

    new-array v1, v1, [C

    iput-object v1, p0, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;->chars:[C

    .line 59
    invoke-virtual {p1}, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;->length()I

    move-result v1

    new-array v1, v1, [Z

    iput-object v1, p0, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;->wasEscaped:[Z

    .line 60
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;->length()I

    move-result v1

    if-le v0, v1, :cond_0

    .line 64
    return-void

    .line 61
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;->chars:[C

    iget-object v2, p1, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;->chars:[C

    aget-char v2, v2, v0

    aput-char v2, v1, v0

    .line 62
    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;->wasEscaped:[Z

    iget-object v2, p1, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;->wasEscaped:[Z

    aget-boolean v2, v2, v0

    aput-boolean v2, v1, v0

    .line 60
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public constructor <init>([C[ZII)V
    .locals 2
    .param p1, "chars"    # [C
    .param p2, "wasEscaped"    # [Z
    .param p3, "offset"    # I
    .param p4, "length"    # I

    .prologue
    const/4 v1, 0x0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-array v0, p4, [C

    iput-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;->chars:[C

    .line 36
    new-array v0, p4, [Z

    iput-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;->wasEscaped:[Z

    .line 37
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;->chars:[C

    invoke-static {p1, p3, v0, v1, p4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 38
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;->wasEscaped:[Z

    invoke-static {p2, p3, v0, v1, p4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 39
    return-void
.end method

.method public static toLowerCase(Ljava/lang/CharSequence;Ljava/util/Locale;)Ljava/lang/CharSequence;
    .locals 5
    .param p0, "text"    # Ljava/lang/CharSequence;
    .param p1, "locale"    # Ljava/util/Locale;

    .prologue
    .line 146
    instance-of v2, p0, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;

    if-eqz v2, :cond_0

    .line 147
    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    .line 148
    .local v0, "chars":[C
    check-cast p0, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;

    .end local p0    # "text":Ljava/lang/CharSequence;
    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;->wasEscaped:[Z

    .line 149
    .local v1, "wasEscaped":[Z
    new-instance v2, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;

    const/4 v3, 0x0

    array-length v4, v0

    invoke-direct {v2, v0, v1, v3, v4}, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;-><init>([C[ZII)V

    .line 151
    .end local v0    # "chars":[C
    .end local v1    # "wasEscaped":[Z
    :goto_0
    return-object v2

    .restart local p0    # "text":Ljava/lang/CharSequence;
    :cond_0
    new-instance v2, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;

    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;-><init>(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public static final wasEscaped(Ljava/lang/CharSequence;I)Z
    .locals 1
    .param p0, "text"    # Ljava/lang/CharSequence;
    .param p1, "index"    # I

    .prologue
    .line 140
    instance-of v0, p0, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;

    if-eqz v0, :cond_0

    .line 141
    check-cast p0, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;

    .end local p0    # "text":Ljava/lang/CharSequence;
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;->wasEscaped:[Z

    aget-boolean v0, v0, p1

    .line 142
    :goto_0
    return v0

    .restart local p0    # "text":Ljava/lang/CharSequence;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public charAt(I)C
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 68
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;->chars:[C

    aget-char v0, v0, p1

    return v0
.end method

.method public length()I
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;->chars:[C

    array-length v0, v0

    return v0
.end method

.method public subSequence(II)Ljava/lang/CharSequence;
    .locals 4
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 78
    sub-int v0, p2, p1

    .line 80
    .local v0, "newLength":I
    new-instance v1, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;

    iget-object v2, p0, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;->chars:[C

    iget-object v3, p0, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;->wasEscaped:[Z

    invoke-direct {v1, v2, v3, p1, v0}, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;-><init>([C[ZII)V

    return-object v1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 86
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;->chars:[C

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([C)V

    return-object v0
.end method

.method public toStringEscaped()Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v3, 0x5c

    .line 96
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 97
    .local v1, "result":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;->length()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 105
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 98
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;->chars:[C

    aget-char v2, v2, v0

    if-ne v2, v3, :cond_2

    .line 99
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 103
    :cond_1
    :goto_1
    iget-object v2, p0, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;->chars:[C

    aget-char v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 97
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 100
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;->wasEscaped:[Z

    aget-boolean v2, v2, v0

    if-eqz v2, :cond_1

    .line 101
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method public toStringEscaped([C)Ljava/lang/String;
    .locals 7
    .param p1, "enabledChars"    # [C

    .prologue
    const/16 v6, 0x5c

    .line 117
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 118
    .local v2, "result":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;->length()I

    move-result v3

    if-lt v1, v3, :cond_0

    .line 132
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 119
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;->chars:[C

    aget-char v3, v3, v1

    if-ne v3, v6, :cond_2

    .line 120
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 130
    :cond_1
    :goto_1
    iget-object v3, p0, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;->chars:[C

    aget-char v3, v3, v1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 118
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 122
    :cond_2
    array-length v4, p1

    const/4 v3, 0x0

    :goto_2
    if-ge v3, v4, :cond_1

    aget-char v0, p1, v3

    .line 123
    .local v0, "character":C
    iget-object v5, p0, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;->chars:[C

    aget-char v5, v5, v1

    if-ne v5, v0, :cond_3

    iget-object v5, p0, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;->wasEscaped:[Z

    aget-boolean v5, v5, v1

    if-eqz v5, :cond_3

    .line 124
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 122
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2
.end method

.method public wasEscaped(I)Z
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 136
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;->wasEscaped:[Z

    aget-boolean v0, v0, p1

    return v0
.end method
