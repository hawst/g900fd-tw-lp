.class public Lorg/apache/lucene/queryparser/xml/CorePlusExtensionsParser;
.super Lorg/apache/lucene/queryparser/xml/CoreParser;
.source "CorePlusExtensionsParser.java"


# direct methods
.method public constructor <init>(Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer;)V
    .locals 1
    .param p1, "defaultField"    # Ljava/lang/String;
    .param p2, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;

    .prologue
    .line 47
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/queryparser/xml/CorePlusExtensionsParser;-><init>(Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer;Lorg/apache/lucene/queryparser/classic/QueryParser;)V

    .line 48
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer;Lorg/apache/lucene/queryparser/classic/QueryParser;)V
    .locals 5
    .param p1, "defaultField"    # Ljava/lang/String;
    .param p2, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;
    .param p3, "parser"    # Lorg/apache/lucene/queryparser/classic/QueryParser;

    .prologue
    .line 51
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/queryparser/xml/CoreParser;-><init>(Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer;Lorg/apache/lucene/queryparser/classic/QueryParser;)V

    .line 52
    iget-object v1, p0, Lorg/apache/lucene/queryparser/xml/CorePlusExtensionsParser;->filterFactory:Lorg/apache/lucene/queryparser/xml/FilterBuilderFactory;

    const-string v2, "TermsFilter"

    new-instance v3, Lorg/apache/lucene/queryparser/xml/builders/TermsFilterBuilder;

    invoke-direct {v3, p2}, Lorg/apache/lucene/queryparser/xml/builders/TermsFilterBuilder;-><init>(Lorg/apache/lucene/analysis/Analyzer;)V

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/queryparser/xml/FilterBuilderFactory;->addBuilder(Ljava/lang/String;Lorg/apache/lucene/queryparser/xml/FilterBuilder;)V

    .line 53
    iget-object v1, p0, Lorg/apache/lucene/queryparser/xml/CorePlusExtensionsParser;->filterFactory:Lorg/apache/lucene/queryparser/xml/FilterBuilderFactory;

    const-string v2, "BooleanFilter"

    new-instance v3, Lorg/apache/lucene/queryparser/xml/builders/BooleanFilterBuilder;

    iget-object v4, p0, Lorg/apache/lucene/queryparser/xml/CorePlusExtensionsParser;->filterFactory:Lorg/apache/lucene/queryparser/xml/FilterBuilderFactory;

    invoke-direct {v3, v4}, Lorg/apache/lucene/queryparser/xml/builders/BooleanFilterBuilder;-><init>(Lorg/apache/lucene/queryparser/xml/FilterBuilder;)V

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/queryparser/xml/FilterBuilderFactory;->addBuilder(Ljava/lang/String;Lorg/apache/lucene/queryparser/xml/FilterBuilder;)V

    .line 54
    iget-object v1, p0, Lorg/apache/lucene/queryparser/xml/CorePlusExtensionsParser;->filterFactory:Lorg/apache/lucene/queryparser/xml/FilterBuilderFactory;

    const-string v2, "DuplicateFilter"

    new-instance v3, Lorg/apache/lucene/queryparser/xml/builders/DuplicateFilterBuilder;

    invoke-direct {v3}, Lorg/apache/lucene/queryparser/xml/builders/DuplicateFilterBuilder;-><init>()V

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/queryparser/xml/FilterBuilderFactory;->addBuilder(Ljava/lang/String;Lorg/apache/lucene/queryparser/xml/FilterBuilder;)V

    .line 55
    const/4 v1, 0x1

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "contents"

    aput-object v2, v0, v1

    .line 56
    .local v0, "fields":[Ljava/lang/String;
    iget-object v1, p0, Lorg/apache/lucene/queryparser/xml/CorePlusExtensionsParser;->queryFactory:Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;

    const-string v2, "LikeThisQuery"

    new-instance v3, Lorg/apache/lucene/queryparser/xml/builders/LikeThisQueryBuilder;

    invoke-direct {v3, p2, v0}, Lorg/apache/lucene/queryparser/xml/builders/LikeThisQueryBuilder;-><init>(Lorg/apache/lucene/analysis/Analyzer;[Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;->addBuilder(Ljava/lang/String;Lorg/apache/lucene/queryparser/xml/QueryBuilder;)V

    .line 57
    iget-object v1, p0, Lorg/apache/lucene/queryparser/xml/CorePlusExtensionsParser;->queryFactory:Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;

    const-string v2, "BoostingQuery"

    new-instance v3, Lorg/apache/lucene/queryparser/xml/builders/BoostingQueryBuilder;

    iget-object v4, p0, Lorg/apache/lucene/queryparser/xml/CorePlusExtensionsParser;->queryFactory:Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;

    invoke-direct {v3, v4}, Lorg/apache/lucene/queryparser/xml/builders/BoostingQueryBuilder;-><init>(Lorg/apache/lucene/queryparser/xml/QueryBuilder;)V

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;->addBuilder(Ljava/lang/String;Lorg/apache/lucene/queryparser/xml/QueryBuilder;)V

    .line 58
    iget-object v1, p0, Lorg/apache/lucene/queryparser/xml/CorePlusExtensionsParser;->queryFactory:Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;

    const-string v2, "FuzzyLikeThisQuery"

    new-instance v3, Lorg/apache/lucene/queryparser/xml/builders/FuzzyLikeThisQueryBuilder;

    invoke-direct {v3, p2}, Lorg/apache/lucene/queryparser/xml/builders/FuzzyLikeThisQueryBuilder;-><init>(Lorg/apache/lucene/analysis/Analyzer;)V

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;->addBuilder(Ljava/lang/String;Lorg/apache/lucene/queryparser/xml/QueryBuilder;)V

    .line 60
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/analysis/Analyzer;Lorg/apache/lucene/queryparser/classic/QueryParser;)V
    .locals 1
    .param p1, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;
    .param p2, "parser"    # Lorg/apache/lucene/queryparser/classic/QueryParser;

    .prologue
    .line 38
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1, p2}, Lorg/apache/lucene/queryparser/xml/CorePlusExtensionsParser;-><init>(Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer;Lorg/apache/lucene/queryparser/classic/QueryParser;)V

    .line 39
    return-void
.end method
