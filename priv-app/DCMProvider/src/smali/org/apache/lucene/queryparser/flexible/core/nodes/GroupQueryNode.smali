.class public Lorg/apache/lucene/queryparser/flexible/core/nodes/GroupQueryNode;
.super Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;
.source "GroupQueryNode.java"


# direct methods
.method public constructor <init>(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)V
    .locals 6
    .param p1, "query"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    .prologue
    const/4 v5, 0x0

    .line 40
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;-><init>()V

    .line 41
    if-nez p1, :cond_0

    .line 42
    new-instance v0, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeError;

    new-instance v1, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;

    .line 43
    sget-object v2, Lorg/apache/lucene/queryparser/flexible/core/messages/QueryParserMessages;->PARAMETER_VALUE_NOT_SUPPORTED:Ljava/lang/String;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "query"

    aput-object v4, v3, v5

    const/4 v4, 0x1

    const-string v5, "null"

    aput-object v5, v3, v4

    invoke-direct {v1, v2, v3}, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 42
    invoke-direct {v0, v1}, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeError;-><init>(Lorg/apache/lucene/queryparser/flexible/messages/Message;)V

    throw v0

    .line 46
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/GroupQueryNode;->allocate()V

    .line 47
    invoke-virtual {p0, v5}, Lorg/apache/lucene/queryparser/flexible/core/nodes/GroupQueryNode;->setLeaf(Z)V

    .line 48
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/GroupQueryNode;->add(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)V

    .line 49
    return-void
.end method


# virtual methods
.method public cloneTree()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 70
    invoke-super {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;->cloneTree()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/GroupQueryNode;

    .line 72
    .local v0, "clone":Lorg/apache/lucene/queryparser/flexible/core/nodes/GroupQueryNode;
    return-object v0
.end method

.method public getChild()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 2

    .prologue
    .line 52
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/GroupQueryNode;->getChildren()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    return-object v0
.end method

.method public setChild(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)V
    .locals 1
    .param p1, "child"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    .prologue
    .line 76
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 77
    .local v0, "list":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 78
    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/GroupQueryNode;->set(Ljava/util/List;)V

    .line 79
    return-void
.end method

.method public toQueryString(Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;)Ljava/lang/CharSequence;
    .locals 2
    .param p1, "escapeSyntaxParser"    # Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;

    .prologue
    .line 62
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/GroupQueryNode;->getChild()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v0

    if-nez v0, :cond_0

    .line 63
    const-string v0, ""

    .line 65
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "( "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/GroupQueryNode;->getChild()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v1

    invoke-interface {v1, p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->toQueryString(Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " )"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 57
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "<group>\n"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/GroupQueryNode;->getChild()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n</group>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
