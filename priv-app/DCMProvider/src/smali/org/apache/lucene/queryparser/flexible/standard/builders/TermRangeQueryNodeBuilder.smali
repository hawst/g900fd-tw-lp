.class public Lorg/apache/lucene/queryparser/flexible/standard/builders/TermRangeQueryNodeBuilder;
.super Ljava/lang/Object;
.source "TermRangeQueryNodeBuilder.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/flexible/standard/builders/StandardQueryBuilder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    return-void
.end method


# virtual methods
.method public bridge synthetic build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/TermRangeQueryNodeBuilder;->build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/search/TermRangeQuery;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/search/Query;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/TermRangeQueryNodeBuilder;->build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/search/TermRangeQuery;

    move-result-object v0

    return-object v0
.end method

.method public build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/search/TermRangeQuery;
    .locals 10
    .param p1, "queryNode"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 41
    move-object v4, p1

    check-cast v4, Lorg/apache/lucene/queryparser/flexible/standard/nodes/TermRangeQueryNode;

    .line 42
    .local v4, "rangeNode":Lorg/apache/lucene/queryparser/flexible/standard/nodes/TermRangeQueryNode;
    invoke-virtual {v4}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/TermRangeQueryNode;->getUpperBound()Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldValuePairQueryNode;

    move-result-object v6

    check-cast v6, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;

    .line 43
    .local v6, "upper":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    invoke-virtual {v4}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/TermRangeQueryNode;->getLowerBound()Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldValuePairQueryNode;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;

    .line 45
    .local v1, "lower":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    invoke-virtual {v4}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/TermRangeQueryNode;->getField()Ljava/lang/CharSequence;

    move-result-object v8

    invoke-static {v8}, Lorg/apache/lucene/queryparser/flexible/core/util/StringUtils;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 46
    .local v0, "field":Ljava/lang/String;
    invoke-virtual {v1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->getTextAsString()Ljava/lang/String;

    move-result-object v2

    .line 47
    .local v2, "lowerText":Ljava/lang/String;
    invoke-virtual {v6}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->getTextAsString()Ljava/lang/String;

    move-result-object v7

    .line 49
    .local v7, "upperText":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v8

    if-nez v8, :cond_0

    .line 50
    const/4 v2, 0x0

    .line 53
    :cond_0
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v8

    if-nez v8, :cond_1

    .line 54
    const/4 v7, 0x0

    .line 58
    :cond_1
    invoke-virtual {v4}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/TermRangeQueryNode;->isLowerInclusive()Z

    move-result v8

    invoke-virtual {v4}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/TermRangeQueryNode;->isUpperInclusive()Z

    move-result v9

    .line 57
    invoke-static {v0, v2, v7, v8, v9}, Lorg/apache/lucene/search/TermRangeQuery;->newStringRange(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Lorg/apache/lucene/search/TermRangeQuery;

    move-result-object v5

    .line 61
    .local v5, "rangeQuery":Lorg/apache/lucene/search/TermRangeQuery;
    const-string v8, "MultiTermRewriteMethodConfiguration"

    invoke-interface {p1, v8}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->getTag(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    .line 60
    check-cast v3, Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    .line 62
    .local v3, "method":Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;
    if-eqz v3, :cond_2

    .line 63
    invoke-virtual {v5, v3}, Lorg/apache/lucene/search/TermRangeQuery;->setRewriteMethod(Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;)V

    .line 66
    :cond_2
    return-object v5
.end method
