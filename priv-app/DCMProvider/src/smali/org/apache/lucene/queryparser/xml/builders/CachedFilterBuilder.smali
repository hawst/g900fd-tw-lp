.class public Lorg/apache/lucene/queryparser/xml/builders/CachedFilterBuilder;
.super Ljava/lang/Object;
.source "CachedFilterBuilder.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/xml/FilterBuilder;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/queryparser/xml/builders/CachedFilterBuilder$LRUCache;
    }
.end annotation


# instance fields
.field private final cacheSize:I

.field private filterCache:Lorg/apache/lucene/queryparser/xml/builders/CachedFilterBuilder$LRUCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/queryparser/xml/builders/CachedFilterBuilder$LRUCache",
            "<",
            "Ljava/lang/Object;",
            "Lorg/apache/lucene/search/Filter;",
            ">;"
        }
    .end annotation
.end field

.field private final filterFactory:Lorg/apache/lucene/queryparser/xml/FilterBuilderFactory;

.field private final queryFactory:Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;Lorg/apache/lucene/queryparser/xml/FilterBuilderFactory;I)V
    .locals 0
    .param p1, "queryFactory"    # Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;
    .param p2, "filterFactory"    # Lorg/apache/lucene/queryparser/xml/FilterBuilderFactory;
    .param p3, "cacheSize"    # I

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p1, p0, Lorg/apache/lucene/queryparser/xml/builders/CachedFilterBuilder;->queryFactory:Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;

    .line 59
    iput-object p2, p0, Lorg/apache/lucene/queryparser/xml/builders/CachedFilterBuilder;->filterFactory:Lorg/apache/lucene/queryparser/xml/FilterBuilderFactory;

    .line 60
    iput p3, p0, Lorg/apache/lucene/queryparser/xml/builders/CachedFilterBuilder;->cacheSize:I

    .line 61
    return-void
.end method


# virtual methods
.method public declared-synchronized getFilter(Lorg/w3c/dom/Element;)Lorg/apache/lucene/search/Filter;
    .locals 9
    .param p1, "e"    # Lorg/w3c/dom/Element;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/xml/ParserException;
        }
    .end annotation

    .prologue
    .line 65
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getFirstChildOrFail(Lorg/w3c/dom/Element;)Lorg/w3c/dom/Element;

    move-result-object v3

    .line 67
    .local v3, "childElement":Lorg/w3c/dom/Element;
    iget-object v7, p0, Lorg/apache/lucene/queryparser/xml/builders/CachedFilterBuilder;->filterCache:Lorg/apache/lucene/queryparser/xml/builders/CachedFilterBuilder$LRUCache;

    if-nez v7, :cond_0

    .line 68
    new-instance v7, Lorg/apache/lucene/queryparser/xml/builders/CachedFilterBuilder$LRUCache;

    iget v8, p0, Lorg/apache/lucene/queryparser/xml/builders/CachedFilterBuilder;->cacheSize:I

    invoke-direct {v7, v8}, Lorg/apache/lucene/queryparser/xml/builders/CachedFilterBuilder$LRUCache;-><init>(I)V

    iput-object v7, p0, Lorg/apache/lucene/queryparser/xml/builders/CachedFilterBuilder;->filterCache:Lorg/apache/lucene/queryparser/xml/builders/CachedFilterBuilder$LRUCache;

    .line 73
    :cond_0
    iget-object v7, p0, Lorg/apache/lucene/queryparser/xml/builders/CachedFilterBuilder;->queryFactory:Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;

    invoke-interface {v3}, Lorg/w3c/dom/Element;->getNodeName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;->getQueryBuilder(Ljava/lang/String;)Lorg/apache/lucene/queryparser/xml/QueryBuilder;

    move-result-object v6

    .line 74
    .local v6, "qb":Lorg/apache/lucene/queryparser/xml/QueryBuilder;
    const/4 v0, 0x0

    .line 75
    .local v0, "cacheKey":Ljava/lang/Object;
    const/4 v5, 0x0

    .line 76
    .local v5, "q":Lorg/apache/lucene/search/Query;
    const/4 v4, 0x0

    .line 77
    .local v4, "f":Lorg/apache/lucene/search/Filter;
    if-eqz v6, :cond_1

    .line 78
    invoke-interface {v6, v3}, Lorg/apache/lucene/queryparser/xml/QueryBuilder;->getQuery(Lorg/w3c/dom/Element;)Lorg/apache/lucene/search/Query;

    move-result-object v5

    .line 79
    move-object v0, v5

    .line 84
    .end local v0    # "cacheKey":Ljava/lang/Object;
    :goto_0
    iget-object v7, p0, Lorg/apache/lucene/queryparser/xml/builders/CachedFilterBuilder;->filterCache:Lorg/apache/lucene/queryparser/xml/builders/CachedFilterBuilder$LRUCache;

    invoke-virtual {v7, v0}, Lorg/apache/lucene/queryparser/xml/builders/CachedFilterBuilder$LRUCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/search/Filter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 85
    .local v1, "cachedFilter":Lorg/apache/lucene/search/Filter;
    if-eqz v1, :cond_2

    move-object v2, v1

    .line 97
    .end local v1    # "cachedFilter":Lorg/apache/lucene/search/Filter;
    .local v2, "cachedFilter":Lorg/apache/lucene/search/Filter;
    :goto_1
    monitor-exit p0

    return-object v2

    .line 81
    .end local v2    # "cachedFilter":Lorg/apache/lucene/search/Filter;
    .restart local v0    # "cacheKey":Ljava/lang/Object;
    :cond_1
    :try_start_1
    iget-object v7, p0, Lorg/apache/lucene/queryparser/xml/builders/CachedFilterBuilder;->filterFactory:Lorg/apache/lucene/queryparser/xml/FilterBuilderFactory;

    invoke-virtual {v7, v3}, Lorg/apache/lucene/queryparser/xml/FilterBuilderFactory;->getFilter(Lorg/w3c/dom/Element;)Lorg/apache/lucene/search/Filter;

    move-result-object v4

    .line 82
    move-object v0, v4

    .local v0, "cacheKey":Lorg/apache/lucene/search/Filter;
    goto :goto_0

    .line 90
    .end local v0    # "cacheKey":Lorg/apache/lucene/search/Filter;
    .restart local v1    # "cachedFilter":Lorg/apache/lucene/search/Filter;
    :cond_2
    if-eqz v6, :cond_3

    .line 91
    new-instance v1, Lorg/apache/lucene/search/QueryWrapperFilter;

    .end local v1    # "cachedFilter":Lorg/apache/lucene/search/Filter;
    invoke-direct {v1, v5}, Lorg/apache/lucene/search/QueryWrapperFilter;-><init>(Lorg/apache/lucene/search/Query;)V

    .line 96
    .restart local v1    # "cachedFilter":Lorg/apache/lucene/search/Filter;
    :goto_2
    iget-object v7, p0, Lorg/apache/lucene/queryparser/xml/builders/CachedFilterBuilder;->filterCache:Lorg/apache/lucene/queryparser/xml/builders/CachedFilterBuilder$LRUCache;

    invoke-virtual {v7, v0, v1}, Lorg/apache/lucene/queryparser/xml/builders/CachedFilterBuilder$LRUCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v2, v1

    .line 97
    .end local v1    # "cachedFilter":Lorg/apache/lucene/search/Filter;
    .restart local v2    # "cachedFilter":Lorg/apache/lucene/search/Filter;
    goto :goto_1

    .line 93
    .end local v2    # "cachedFilter":Lorg/apache/lucene/search/Filter;
    .restart local v1    # "cachedFilter":Lorg/apache/lucene/search/Filter;
    :cond_3
    new-instance v1, Lorg/apache/lucene/search/CachingWrapperFilter;

    .end local v1    # "cachedFilter":Lorg/apache/lucene/search/Filter;
    invoke-direct {v1, v4}, Lorg/apache/lucene/search/CachingWrapperFilter;-><init>(Lorg/apache/lucene/search/Filter;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .restart local v1    # "cachedFilter":Lorg/apache/lucene/search/Filter;
    goto :goto_2

    .line 65
    .end local v1    # "cachedFilter":Lorg/apache/lucene/search/Filter;
    .end local v3    # "childElement":Lorg/w3c/dom/Element;
    .end local v4    # "f":Lorg/apache/lucene/search/Filter;
    .end local v5    # "q":Lorg/apache/lucene/search/Query;
    .end local v6    # "qb":Lorg/apache/lucene/queryparser/xml/QueryBuilder;
    :catchall_0
    move-exception v7

    monitor-exit p0

    throw v7
.end method
