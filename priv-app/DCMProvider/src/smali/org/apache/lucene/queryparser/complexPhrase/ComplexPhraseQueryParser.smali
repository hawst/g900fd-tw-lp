.class public Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser;
.super Lorg/apache/lucene/queryparser/classic/QueryParser;
.source "ComplexPhraseQueryParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser$ComplexPhraseQuery;
    }
.end annotation


# instance fields
.field private complexPhrases:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser$ComplexPhraseQuery;",
            ">;"
        }
    .end annotation
.end field

.field private currentPhraseQuery:Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser$ComplexPhraseQuery;

.field private isPass2ResolvingPhrases:Z


# direct methods
.method public constructor <init>(Lorg/apache/lucene/util/Version;Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "f"    # Ljava/lang/String;
    .param p3, "a"    # Lorg/apache/lucene/analysis/Analyzer;

    .prologue
    const/4 v0, 0x0

    .line 72
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/queryparser/classic/QueryParser;-><init>(Lorg/apache/lucene/util/Version;Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer;)V

    .line 65
    iput-object v0, p0, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser;->complexPhrases:Ljava/util/ArrayList;

    .line 69
    iput-object v0, p0, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser;->currentPhraseQuery:Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser$ComplexPhraseQuery;

    .line 73
    return-void
.end method

.method private checkPhraseClauseIsForSameField(Ljava/lang/String;)V
    .locals 3
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/classic/ParseException;
        }
    .end annotation

    .prologue
    .line 146
    iget-object v0, p0, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser;->currentPhraseQuery:Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser$ComplexPhraseQuery;

    iget-object v0, v0, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser$ComplexPhraseQuery;->field:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 147
    new-instance v0, Lorg/apache/lucene/queryparser/classic/ParseException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot have clause for field \""

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 148
    const-string v2, "\" nested in phrase "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for field \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser;->currentPhraseQuery:Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser$ComplexPhraseQuery;

    iget-object v2, v2, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser$ComplexPhraseQuery;->field:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 149
    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 147
    invoke-direct {v0, v1}, Lorg/apache/lucene/queryparser/classic/ParseException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 151
    :cond_0
    return-void
.end method


# virtual methods
.method protected getFieldQuery(Ljava/lang/String;Ljava/lang/String;I)Lorg/apache/lucene/search/Query;
    .locals 2
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "queryText"    # Ljava/lang/String;
    .param p3, "slop"    # I

    .prologue
    .line 77
    new-instance v0, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser$ComplexPhraseQuery;

    invoke-direct {v0, p1, p2, p3}, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser$ComplexPhraseQuery;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    .line 78
    .local v0, "cpq":Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser$ComplexPhraseQuery;
    iget-object v1, p0, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser;->complexPhrases:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 81
    return-object v0
.end method

.method protected getFuzzyQuery(Ljava/lang/String;Ljava/lang/String;F)Lorg/apache/lucene/search/Query;
    .locals 1
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "termStr"    # Ljava/lang/String;
    .param p3, "minSimilarity"    # F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/classic/ParseException;
        }
    .end annotation

    .prologue
    .line 187
    iget-boolean v0, p0, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser;->isPass2ResolvingPhrases:Z

    if-eqz v0, :cond_0

    .line 188
    invoke-direct {p0, p1}, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser;->checkPhraseClauseIsForSameField(Ljava/lang/String;)V

    .line 190
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lorg/apache/lucene/queryparser/classic/QueryParser;->getFuzzyQuery(Ljava/lang/String;Ljava/lang/String;F)Lorg/apache/lucene/search/Query;

    move-result-object v0

    return-object v0
.end method

.method protected getRangeQuery(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Lorg/apache/lucene/search/Query;
    .locals 1
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "part1"    # Ljava/lang/String;
    .param p3, "part2"    # Ljava/lang/String;
    .param p4, "startInclusive"    # Z
    .param p5, "endInclusive"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/classic/ParseException;
        }
    .end annotation

    .prologue
    .line 165
    iget-boolean v0, p0, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser;->isPass2ResolvingPhrases:Z

    if-eqz v0, :cond_0

    .line 166
    invoke-direct {p0, p1}, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser;->checkPhraseClauseIsForSameField(Ljava/lang/String;)V

    .line 168
    :cond_0
    invoke-super/range {p0 .. p5}, Lorg/apache/lucene/queryparser/classic/QueryParser;->getRangeQuery(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Lorg/apache/lucene/search/Query;

    move-result-object v0

    return-object v0
.end method

.method protected getWildcardQuery(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/lucene/search/Query;
    .locals 1
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "termStr"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/classic/ParseException;
        }
    .end annotation

    .prologue
    .line 156
    iget-boolean v0, p0, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser;->isPass2ResolvingPhrases:Z

    if-eqz v0, :cond_0

    .line 157
    invoke-direct {p0, p1}, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser;->checkPhraseClauseIsForSameField(Ljava/lang/String;)V

    .line 159
    :cond_0
    invoke-super {p0, p1, p2}, Lorg/apache/lucene/queryparser/classic/QueryParser;->getWildcardQuery(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/lucene/search/Query;

    move-result-object v0

    return-object v0
.end method

.method protected newRangeQuery(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Lorg/apache/lucene/search/Query;
    .locals 2
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "part1"    # Ljava/lang/String;
    .param p3, "part2"    # Ljava/lang/String;
    .param p4, "startInclusive"    # Z
    .param p5, "endInclusive"    # Z

    .prologue
    .line 174
    iget-boolean v1, p0, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser;->isPass2ResolvingPhrases:Z

    if-eqz v1, :cond_0

    .line 177
    invoke-static {p1, p2, p3, p4, p5}, Lorg/apache/lucene/search/TermRangeQuery;->newStringRange(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Lorg/apache/lucene/search/TermRangeQuery;

    move-result-object v0

    .line 178
    .local v0, "rangeQuery":Lorg/apache/lucene/search/TermRangeQuery;
    sget-object v1, Lorg/apache/lucene/search/MultiTermQuery;->SCORING_BOOLEAN_QUERY_REWRITE:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/TermRangeQuery;->setRewriteMethod(Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;)V

    .line 181
    .end local v0    # "rangeQuery":Lorg/apache/lucene/search/TermRangeQuery;
    :goto_0
    return-object v0

    :cond_0
    invoke-super/range {p0 .. p5}, Lorg/apache/lucene/queryparser/classic/QueryParser;->newRangeQuery(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Lorg/apache/lucene/search/Query;

    move-result-object v0

    goto :goto_0
.end method

.method protected newTermQuery(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/search/Query;
    .locals 3
    .param p1, "term"    # Lorg/apache/lucene/index/Term;

    .prologue
    .line 133
    iget-boolean v1, p0, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser;->isPass2ResolvingPhrases:Z

    if-eqz v1, :cond_0

    .line 135
    :try_start_0
    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser;->checkPhraseClauseIsForSameField(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/apache/lucene/queryparser/classic/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 140
    :cond_0
    invoke-super {p0, p1}, Lorg/apache/lucene/queryparser/classic/QueryParser;->newTermQuery(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/search/Query;

    move-result-object v1

    return-object v1

    .line 136
    :catch_0
    move-exception v0

    .line 137
    .local v0, "pe":Lorg/apache/lucene/queryparser/classic/ParseException;
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Error parsing complex phrase"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public parse(Ljava/lang/String;)Lorg/apache/lucene/search/Query;
    .locals 5
    .param p1, "query"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/classic/ParseException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 86
    iget-boolean v3, p0, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser;->isPass2ResolvingPhrases:Z

    if-eqz v3, :cond_0

    .line 87
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser;->getMultiTermRewriteMethod()Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    move-result-object v1

    .line 98
    .local v1, "oldMethod":Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;
    :try_start_0
    sget-object v3, Lorg/apache/lucene/search/MultiTermQuery;->SCORING_BOOLEAN_QUERY_REWRITE:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    invoke-virtual {p0, v3}, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser;->setMultiTermRewriteMethod(Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;)V

    .line 99
    invoke-super {p0, p1}, Lorg/apache/lucene/queryparser/classic/QueryParser;->parse(Ljava/lang/String;)Lorg/apache/lucene/search/Query;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 101
    invoke-virtual {p0, v1}, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser;->setMultiTermRewriteMethod(Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;)V

    .line 124
    .end local v1    # "oldMethod":Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;
    :goto_0
    return-object v2

    .line 100
    .restart local v1    # "oldMethod":Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;
    :catchall_0
    move-exception v3

    .line 101
    invoke-virtual {p0, v1}, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser;->setMultiTermRewriteMethod(Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;)V

    .line 102
    throw v3

    .line 107
    .end local v1    # "oldMethod":Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser;->complexPhrases:Ljava/util/ArrayList;

    .line 108
    invoke-super {p0, p1}, Lorg/apache/lucene/queryparser/classic/QueryParser;->parse(Ljava/lang/String;)Lorg/apache/lucene/search/Query;

    move-result-object v2

    .line 113
    .local v2, "q":Lorg/apache/lucene/search/Query;
    const/4 v3, 0x1

    iput-boolean v3, p0, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser;->isPass2ResolvingPhrases:Z

    .line 115
    :try_start_1
    iget-object v3, p0, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser;->complexPhrases:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser$ComplexPhraseQuery;>;"
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v3

    if-nez v3, :cond_1

    .line 122
    iput-boolean v4, p0, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser;->isPass2ResolvingPhrases:Z

    goto :goto_0

    .line 116
    :cond_1
    :try_start_2
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser$ComplexPhraseQuery;

    iput-object v3, p0, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser;->currentPhraseQuery:Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser$ComplexPhraseQuery;

    .line 119
    iget-object v3, p0, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser;->currentPhraseQuery:Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser$ComplexPhraseQuery;

    invoke-virtual {v3, p0}, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser$ComplexPhraseQuery;->parsePhraseElements(Lorg/apache/lucene/queryparser/classic/QueryParser;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_1

    .line 121
    .end local v0    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser$ComplexPhraseQuery;>;"
    :catchall_1
    move-exception v3

    .line 122
    iput-boolean v4, p0, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser;->isPass2ResolvingPhrases:Z

    .line 123
    throw v3
.end method
