.class public Lorg/apache/lucene/queryparser/surround/query/SrndTermQuery;
.super Lorg/apache/lucene/queryparser/surround/query/SimpleTerm;
.source "SrndTermQuery.java"


# instance fields
.field private final termText:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 0
    .param p1, "termText"    # Ljava/lang/String;
    .param p2, "quoted"    # Z

    .prologue
    .line 34
    invoke-direct {p0, p2}, Lorg/apache/lucene/queryparser/surround/query/SimpleTerm;-><init>(Z)V

    .line 35
    iput-object p1, p0, Lorg/apache/lucene/queryparser/surround/query/SrndTermQuery;->termText:Ljava/lang/String;

    .line 36
    return-void
.end method


# virtual methods
.method public getLuceneTerm(Ljava/lang/String;)Lorg/apache/lucene/index/Term;
    .locals 2
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 42
    new-instance v0, Lorg/apache/lucene/index/Term;

    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/query/SrndTermQuery;->getTermText()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public getTermText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lorg/apache/lucene/queryparser/surround/query/SrndTermQuery;->termText:Ljava/lang/String;

    return-object v0
.end method

.method public toStringUnquoted()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/query/SrndTermQuery;->getTermText()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public visitMatchingTerms(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/queryparser/surround/query/SimpleTerm$MatchingTermVisitor;)V
    .locals 5
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "fieldName"    # Ljava/lang/String;
    .param p3, "mtv"    # Lorg/apache/lucene/queryparser/surround/query/SimpleTerm$MatchingTermVisitor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55
    invoke-static {p1, p2}, Lorg/apache/lucene/index/MultiFields;->getTerms(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v1

    .line 56
    .local v1, "terms":Lorg/apache/lucene/index/Terms;
    if-eqz v1, :cond_0

    .line 57
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v2

    .line 59
    .local v2, "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    new-instance v3, Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/query/SrndTermQuery;->getTermText()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v2, v3}, Lorg/apache/lucene/index/TermsEnum;->seekCeil(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    move-result-object v0

    .line 60
    .local v0, "status":Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    sget-object v3, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    if-ne v0, v3, :cond_0

    .line 61
    invoke-virtual {p0, p2}, Lorg/apache/lucene/queryparser/surround/query/SrndTermQuery;->getLuceneTerm(Ljava/lang/String;)Lorg/apache/lucene/index/Term;

    move-result-object v3

    invoke-interface {p3, v3}, Lorg/apache/lucene/queryparser/surround/query/SimpleTerm$MatchingTermVisitor;->visitMatchingTerm(Lorg/apache/lucene/index/Term;)V

    .line 64
    .end local v0    # "status":Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    .end local v2    # "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    :cond_0
    return-void
.end method
