.class public Lorg/apache/lucene/queryparser/xml/builders/TermsFilterBuilder;
.super Ljava/lang/Object;
.source "TermsFilterBuilder.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/xml/FilterBuilder;


# instance fields
.field private final analyzer:Lorg/apache/lucene/analysis/Analyzer;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/Analyzer;)V
    .locals 0
    .param p1, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lorg/apache/lucene/queryparser/xml/builders/TermsFilterBuilder;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    .line 46
    return-void
.end method


# virtual methods
.method public getFilter(Lorg/w3c/dom/Element;)Lorg/apache/lucene/search/Filter;
    .locals 11
    .param p1, "e"    # Lorg/w3c/dom/Element;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/xml/ParserException;
        }
    .end annotation

    .prologue
    .line 55
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 56
    .local v5, "terms":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/util/BytesRef;>;"
    invoke-static {p1}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getNonBlankTextOrFail(Lorg/w3c/dom/Element;)Ljava/lang/String;

    move-result-object v6

    .line 57
    .local v6, "text":Ljava/lang/String;
    const-string v8, "fieldName"

    invoke-static {p1, v8}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttributeWithInheritanceOrFail(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 60
    .local v1, "fieldName":Ljava/lang/String;
    :try_start_0
    iget-object v8, p0, Lorg/apache/lucene/queryparser/xml/builders/TermsFilterBuilder;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    new-instance v9, Ljava/io/StringReader;

    invoke-direct {v9, v6}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v1, v9}, Lorg/apache/lucene/analysis/Analyzer;->tokenStream(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/TokenStream;

    move-result-object v7

    .line 61
    .local v7, "ts":Lorg/apache/lucene/analysis/TokenStream;
    const-class v8, Lorg/apache/lucene/analysis/tokenattributes/TermToBytesRefAttribute;

    invoke-virtual {v7, v8}, Lorg/apache/lucene/analysis/TokenStream;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/analysis/tokenattributes/TermToBytesRefAttribute;

    .line 62
    .local v4, "termAtt":Lorg/apache/lucene/analysis/tokenattributes/TermToBytesRefAttribute;
    const/4 v3, 0x0

    .line 63
    .local v3, "term":Lorg/apache/lucene/index/Term;
    invoke-interface {v4}, Lorg/apache/lucene/analysis/tokenattributes/TermToBytesRefAttribute;->getBytesRef()Lorg/apache/lucene/util/BytesRef;

    move-result-object v0

    .line 64
    .local v0, "bytes":Lorg/apache/lucene/util/BytesRef;
    invoke-virtual {v7}, Lorg/apache/lucene/analysis/TokenStream;->reset()V

    .line 65
    :goto_0
    invoke-virtual {v7}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v8

    if-nez v8, :cond_0

    .line 69
    invoke-virtual {v7}, Lorg/apache/lucene/analysis/TokenStream;->end()V

    .line 70
    invoke-virtual {v7}, Lorg/apache/lucene/analysis/TokenStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 75
    new-instance v8, Lorg/apache/lucene/queries/TermsFilter;

    invoke-direct {v8, v1, v5}, Lorg/apache/lucene/queries/TermsFilter;-><init>(Ljava/lang/String;Ljava/util/List;)V

    return-object v8

    .line 66
    :cond_0
    :try_start_1
    invoke-interface {v4}, Lorg/apache/lucene/analysis/tokenattributes/TermToBytesRefAttribute;->fillBytesRef()I

    .line 67
    invoke-static {v0}, Lorg/apache/lucene/util/BytesRef;->deepCopyOf(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/BytesRef;

    move-result-object v8

    invoke-interface {v5, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 72
    .end local v0    # "bytes":Lorg/apache/lucene/util/BytesRef;
    .end local v3    # "term":Lorg/apache/lucene/index/Term;
    .end local v4    # "termAtt":Lorg/apache/lucene/analysis/tokenattributes/TermToBytesRefAttribute;
    .end local v7    # "ts":Lorg/apache/lucene/analysis/TokenStream;
    :catch_0
    move-exception v2

    .line 73
    .local v2, "ioe":Ljava/io/IOException;
    new-instance v8, Ljava/lang/RuntimeException;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Error constructing terms from index:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v8
.end method
