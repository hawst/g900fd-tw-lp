.class public Lorg/apache/lucene/queryparser/flexible/standard/processors/FuzzyQueryNodeProcessor;
.super Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl;
.source "FuzzyQueryNodeProcessor.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl;-><init>()V

    return-void
.end method


# virtual methods
.method protected postProcessNode(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 0
    .param p1, "node"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 48
    return-object p1
.end method

.method protected preProcessNode(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 5
    .param p1, "node"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 55
    instance-of v3, p1, Lorg/apache/lucene/queryparser/flexible/core/nodes/FuzzyQueryNode;

    if-eqz v3, :cond_0

    move-object v2, p1

    .line 56
    check-cast v2, Lorg/apache/lucene/queryparser/flexible/core/nodes/FuzzyQueryNode;

    .line 57
    .local v2, "fuzzyNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/FuzzyQueryNode;
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/processors/FuzzyQueryNodeProcessor;->getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    move-result-object v0

    .line 59
    .local v0, "config":Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;
    const/4 v1, 0x0

    .line 61
    .local v1, "fuzzyConfig":Lorg/apache/lucene/queryparser/flexible/standard/config/FuzzyConfig;
    if-eqz v0, :cond_1

    sget-object v3, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->FUZZY_CONFIG:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-virtual {v0, v3}, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->get(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "fuzzyConfig":Lorg/apache/lucene/queryparser/flexible/standard/config/FuzzyConfig;
    check-cast v1, Lorg/apache/lucene/queryparser/flexible/standard/config/FuzzyConfig;

    .restart local v1    # "fuzzyConfig":Lorg/apache/lucene/queryparser/flexible/standard/config/FuzzyConfig;
    if-eqz v1, :cond_1

    .line 62
    invoke-virtual {v1}, Lorg/apache/lucene/queryparser/flexible/standard/config/FuzzyConfig;->getPrefixLength()I

    move-result v3

    invoke-virtual {v2, v3}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FuzzyQueryNode;->setPrefixLength(I)V

    .line 64
    invoke-virtual {v2}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FuzzyQueryNode;->getSimilarity()F

    move-result v3

    cmpg-float v3, v3, v4

    if-gez v3, :cond_0

    .line 65
    invoke-virtual {v1}, Lorg/apache/lucene/queryparser/flexible/standard/config/FuzzyConfig;->getMinSimilarity()F

    move-result v3

    invoke-virtual {v2, v3}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FuzzyQueryNode;->setSimilarity(F)V

    .line 74
    .end local v0    # "config":Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;
    .end local v1    # "fuzzyConfig":Lorg/apache/lucene/queryparser/flexible/standard/config/FuzzyConfig;
    .end local v2    # "fuzzyNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/FuzzyQueryNode;
    :cond_0
    return-object p1

    .line 68
    .restart local v0    # "config":Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;
    .restart local v1    # "fuzzyConfig":Lorg/apache/lucene/queryparser/flexible/standard/config/FuzzyConfig;
    .restart local v2    # "fuzzyNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/FuzzyQueryNode;
    :cond_1
    invoke-virtual {v2}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FuzzyQueryNode;->getSimilarity()F

    move-result v3

    cmpg-float v3, v3, v4

    if-gez v3, :cond_0

    .line 69
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "No FUZZY_CONFIG set in the config"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method protected setChildrenOrder(Ljava/util/List;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 82
    .local p1, "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    return-object p1
.end method
