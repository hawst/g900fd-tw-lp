.class public Lorg/apache/lucene/queryparser/flexible/standard/processors/MultiTermRewriteMethodProcessor;
.super Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl;
.source "MultiTermRewriteMethodProcessor.java"


# static fields
.field public static final TAG_ID:Ljava/lang/String; = "MultiTermRewriteMethodConfiguration"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl;-><init>()V

    return-void
.end method


# virtual methods
.method protected postProcessNode(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 3
    .param p1, "node"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    .prologue
    .line 45
    instance-of v1, p1, Lorg/apache/lucene/queryparser/flexible/standard/nodes/WildcardQueryNode;

    if-nez v1, :cond_0

    .line 46
    instance-of v1, p1, Lorg/apache/lucene/queryparser/flexible/standard/nodes/AbstractRangeQueryNode;

    if-nez v1, :cond_0

    instance-of v1, p1, Lorg/apache/lucene/queryparser/flexible/standard/nodes/RegexpQueryNode;

    if-eqz v1, :cond_2

    .line 48
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/processors/MultiTermRewriteMethodProcessor;->getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    move-result-object v1

    sget-object v2, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->MULTI_TERM_REWRITE_METHOD:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->get(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    .line 50
    .local v0, "rewriteMethod":Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;
    if-nez v0, :cond_1

    .line 53
    new-instance v1, Ljava/lang/IllegalArgumentException;

    .line 54
    const-string v2, "StandardQueryConfigHandler.ConfigurationKeys.MULTI_TERM_REWRITE_METHOD should be set on the QueryConfigHandler"

    .line 53
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 58
    :cond_1
    const-string v1, "MultiTermRewriteMethodConfiguration"

    invoke-interface {p1, v1, v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->setTag(Ljava/lang/String;Ljava/lang/Object;)V

    .line 62
    .end local v0    # "rewriteMethod":Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;
    :cond_2
    return-object p1
.end method

.method protected preProcessNode(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 0
    .param p1, "node"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    .prologue
    .line 67
    return-object p1
.end method

.method protected setChildrenOrder(Ljava/util/List;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 72
    .local p1, "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    return-object p1
.end method
