.class final enum Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;
.super Ljava/lang/Enum;
.source "QueryNodeOperation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "ANDOperation"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum BOTH:Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;

.field private static final synthetic ENUM$VALUES:[Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;

.field public static final enum NONE:Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;

.field public static final enum Q1:Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;

.field public static final enum Q2:Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 37
    new-instance v0, Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;

    const-string v1, "BOTH"

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;->BOTH:Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;

    new-instance v0, Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;

    const-string v1, "Q1"

    invoke-direct {v0, v1, v3}, Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;->Q1:Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;

    new-instance v0, Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;

    const-string v1, "Q2"

    invoke-direct {v0, v1, v4}, Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;->Q2:Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;

    new-instance v0, Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v5}, Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;->NONE:Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;

    .line 36
    const/4 v0, 0x4

    new-array v0, v0, [Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;

    sget-object v1, Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;->BOTH:Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;

    aput-object v1, v0, v2

    sget-object v1, Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;->Q1:Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;

    aput-object v1, v0, v3

    sget-object v1, Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;->Q2:Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;

    aput-object v1, v0, v4

    sget-object v1, Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;->NONE:Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;

    aput-object v1, v0, v5

    sput-object v0, Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;->ENUM$VALUES:[Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;

    return-object v0
.end method

.method public static values()[Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;->ENUM$VALUES:[Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;

    array-length v1, v0

    new-array v2, v1, [Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
