.class public Lorg/apache/lucene/queryparser/xml/builders/RangeFilterBuilder;
.super Ljava/lang/Object;
.source "RangeFilterBuilder.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/xml/FilterBuilder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getFilter(Lorg/w3c/dom/Element;)Lorg/apache/lucene/search/Filter;
    .locals 7
    .param p1, "e"    # Lorg/w3c/dom/Element;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/xml/ParserException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 37
    const-string v5, "fieldName"

    invoke-static {p1, v5}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttributeWithInheritance(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 39
    .local v0, "fieldName":Ljava/lang/String;
    const-string v5, "lowerTerm"

    invoke-interface {p1, v5}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 40
    .local v3, "lowerTerm":Ljava/lang/String;
    const-string v5, "upperTerm"

    invoke-interface {p1, v5}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 41
    .local v4, "upperTerm":Ljava/lang/String;
    const-string v5, "includeLower"

    invoke-static {p1, v5, v6}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttribute(Lorg/w3c/dom/Element;Ljava/lang/String;Z)Z

    move-result v1

    .line 42
    .local v1, "includeLower":Z
    const-string v5, "includeUpper"

    invoke-static {p1, v5, v6}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttribute(Lorg/w3c/dom/Element;Ljava/lang/String;Z)Z

    move-result v2

    .line 43
    .local v2, "includeUpper":Z
    invoke-static {v0, v3, v4, v1, v2}, Lorg/apache/lucene/search/TermRangeFilter;->newStringRange(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Lorg/apache/lucene/search/TermRangeFilter;

    move-result-object v5

    return-object v5
.end method
