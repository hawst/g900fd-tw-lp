.class public Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;
.super Ljava/lang/Object;
.source "MessageImpl.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/flexible/messages/Message;


# instance fields
.field private arguments:[Ljava/lang/Object;

.field private key:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    iput-object v0, p0, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;->arguments:[Ljava/lang/Object;

    .line 33
    iput-object p1, p0, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;->key:Ljava/lang/String;

    .line 35
    return-void
.end method

.method public varargs constructor <init>(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 0
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "args"    # [Ljava/lang/Object;

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;-><init>(Ljava/lang/String;)V

    .line 39
    iput-object p2, p0, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;->arguments:[Ljava/lang/Object;

    .line 40
    return-void
.end method


# virtual methods
.method public getArguments()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;->arguments:[Ljava/lang/Object;

    return-object v0
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;->key:Ljava/lang/String;

    return-object v0
.end method

.method public getLocalizedMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;->getLocalizedMessage(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLocalizedMessage(Ljava/util/Locale;)Ljava/lang/String;
    .locals 2
    .param p1, "locale"    # Ljava/util/Locale;

    .prologue
    .line 59
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;->getArguments()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lorg/apache/lucene/queryparser/flexible/messages/NLS;->getLocalizedMessage(Ljava/lang/String;Ljava/util/Locale;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 64
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;->getArguments()[Ljava/lang/Object;

    move-result-object v0

    .line 65
    .local v0, "args":[Ljava/lang/Object;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 66
    .local v2, "sb":Ljava/lang/StringBuilder;
    if-eqz v0, :cond_0

    .line 67
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, v0

    if-lt v1, v3, :cond_1

    .line 71
    .end local v1    # "i":I
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 68
    .restart local v1    # "i":I
    :cond_1
    if-nez v1, :cond_2

    const-string v3, " "

    :goto_1
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, v0, v1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 67
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 68
    :cond_2
    const-string v3, ", "

    goto :goto_1
.end method
