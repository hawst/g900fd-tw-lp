.class public Lorg/apache/lucene/queryparser/xml/builders/SpanOrTermsBuilder;
.super Lorg/apache/lucene/queryparser/xml/builders/SpanBuilderBase;
.source "SpanOrTermsBuilder.java"


# instance fields
.field private final analyzer:Lorg/apache/lucene/analysis/Analyzer;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/Analyzer;)V
    .locals 0
    .param p1, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;

    .prologue
    .line 43
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/xml/builders/SpanBuilderBase;-><init>()V

    .line 44
    iput-object p1, p0, Lorg/apache/lucene/queryparser/xml/builders/SpanOrTermsBuilder;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    .line 45
    return-void
.end method


# virtual methods
.method public getSpanQuery(Lorg/w3c/dom/Element;)Lorg/apache/lucene/search/spans/SpanQuery;
    .locals 12
    .param p1, "e"    # Lorg/w3c/dom/Element;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/xml/ParserException;
        }
    .end annotation

    .prologue
    .line 49
    const-string v9, "fieldName"

    invoke-static {p1, v9}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttributeWithInheritanceOrFail(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 50
    .local v2, "fieldName":Ljava/lang/String;
    invoke-static {p1}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getNonBlankTextOrFail(Lorg/w3c/dom/Element;)Ljava/lang/String;

    move-result-object v8

    .line 53
    .local v8, "value":Ljava/lang/String;
    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 54
    .local v1, "clausesList":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/spans/SpanQuery;>;"
    iget-object v9, p0, Lorg/apache/lucene/queryparser/xml/builders/SpanOrTermsBuilder;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    new-instance v10, Ljava/io/StringReader;

    invoke-direct {v10, v8}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v2, v10}, Lorg/apache/lucene/analysis/Analyzer;->tokenStream(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/TokenStream;

    move-result-object v7

    .line 55
    .local v7, "ts":Lorg/apache/lucene/analysis/TokenStream;
    const-class v9, Lorg/apache/lucene/analysis/tokenattributes/TermToBytesRefAttribute;

    invoke-virtual {v7, v9}, Lorg/apache/lucene/analysis/TokenStream;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v6

    check-cast v6, Lorg/apache/lucene/analysis/tokenattributes/TermToBytesRefAttribute;

    .line 56
    .local v6, "termAtt":Lorg/apache/lucene/analysis/tokenattributes/TermToBytesRefAttribute;
    invoke-interface {v6}, Lorg/apache/lucene/analysis/tokenattributes/TermToBytesRefAttribute;->getBytesRef()Lorg/apache/lucene/util/BytesRef;

    move-result-object v0

    .line 57
    .local v0, "bytes":Lorg/apache/lucene/util/BytesRef;
    invoke-virtual {v7}, Lorg/apache/lucene/analysis/TokenStream;->reset()V

    .line 58
    :goto_0
    invoke-virtual {v7}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v9

    if-nez v9, :cond_0

    .line 63
    invoke-virtual {v7}, Lorg/apache/lucene/analysis/TokenStream;->end()V

    .line 64
    invoke-virtual {v7}, Lorg/apache/lucene/analysis/TokenStream;->close()V

    .line 65
    new-instance v4, Lorg/apache/lucene/search/spans/SpanOrQuery;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v9

    new-array v9, v9, [Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-interface {v1, v9}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-direct {v4, v9}, Lorg/apache/lucene/search/spans/SpanOrQuery;-><init>([Lorg/apache/lucene/search/spans/SpanQuery;)V

    .line 66
    .local v4, "soq":Lorg/apache/lucene/search/spans/SpanOrQuery;
    const-string v9, "boost"

    const/high16 v10, 0x3f800000    # 1.0f

    invoke-static {p1, v9, v10}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttribute(Lorg/w3c/dom/Element;Ljava/lang/String;F)F

    move-result v9

    invoke-virtual {v4, v9}, Lorg/apache/lucene/search/spans/SpanOrQuery;->setBoost(F)V

    .line 67
    return-object v4

    .line 59
    .end local v4    # "soq":Lorg/apache/lucene/search/spans/SpanOrQuery;
    :cond_0
    invoke-interface {v6}, Lorg/apache/lucene/analysis/tokenattributes/TermToBytesRefAttribute;->fillBytesRef()I

    .line 60
    new-instance v5, Lorg/apache/lucene/search/spans/SpanTermQuery;

    new-instance v9, Lorg/apache/lucene/index/Term;

    invoke-static {v0}, Lorg/apache/lucene/util/BytesRef;->deepCopyOf(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/BytesRef;

    move-result-object v10

    invoke-direct {v9, v2, v10}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    invoke-direct {v5, v9}, Lorg/apache/lucene/search/spans/SpanTermQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    .line 61
    .local v5, "stq":Lorg/apache/lucene/search/spans/SpanTermQuery;
    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 69
    .end local v0    # "bytes":Lorg/apache/lucene/util/BytesRef;
    .end local v1    # "clausesList":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/spans/SpanQuery;>;"
    .end local v5    # "stq":Lorg/apache/lucene/search/spans/SpanTermQuery;
    .end local v6    # "termAtt":Lorg/apache/lucene/analysis/tokenattributes/TermToBytesRefAttribute;
    .end local v7    # "ts":Lorg/apache/lucene/analysis/TokenStream;
    :catch_0
    move-exception v3

    .line 70
    .local v3, "ioe":Ljava/io/IOException;
    new-instance v9, Lorg/apache/lucene/queryparser/xml/ParserException;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "IOException parsing value:"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Lorg/apache/lucene/queryparser/xml/ParserException;-><init>(Ljava/lang/String;)V

    throw v9
.end method
