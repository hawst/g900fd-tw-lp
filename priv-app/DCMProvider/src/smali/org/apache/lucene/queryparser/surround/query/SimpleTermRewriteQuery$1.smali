.class Lorg/apache/lucene/queryparser/surround/query/SimpleTermRewriteQuery$1;
.super Ljava/lang/Object;
.source "SimpleTermRewriteQuery.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/surround/query/SimpleTerm$MatchingTermVisitor;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/queryparser/surround/query/SimpleTermRewriteQuery;->rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/queryparser/surround/query/SimpleTermRewriteQuery;

.field private final synthetic val$luceneSubQueries:Ljava/util/List;


# direct methods
.method constructor <init>(Lorg/apache/lucene/queryparser/surround/query/SimpleTermRewriteQuery;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/queryparser/surround/query/SimpleTermRewriteQuery$1;->this$0:Lorg/apache/lucene/queryparser/surround/query/SimpleTermRewriteQuery;

    iput-object p2, p0, Lorg/apache/lucene/queryparser/surround/query/SimpleTermRewriteQuery$1;->val$luceneSubQueries:Ljava/util/List;

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public visitMatchingTerm(Lorg/apache/lucene/index/Term;)V
    .locals 2
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 43
    iget-object v0, p0, Lorg/apache/lucene/queryparser/surround/query/SimpleTermRewriteQuery$1;->val$luceneSubQueries:Ljava/util/List;

    iget-object v1, p0, Lorg/apache/lucene/queryparser/surround/query/SimpleTermRewriteQuery$1;->this$0:Lorg/apache/lucene/queryparser/surround/query/SimpleTermRewriteQuery;

    iget-object v1, v1, Lorg/apache/lucene/queryparser/surround/query/SimpleTermRewriteQuery;->qf:Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;->newTermQuery(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/search/TermQuery;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 44
    return-void
.end method
