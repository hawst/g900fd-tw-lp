.class public abstract Lorg/apache/lucene/queryparser/classic/QueryParserBase;
.super Ljava/lang/Object;
.source "QueryParserBase.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/flexible/standard/CommonQueryParserConfiguration;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/queryparser/classic/QueryParserBase$MethodRemovedUseAnother;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final AND_OPERATOR:Lorg/apache/lucene/queryparser/classic/QueryParser$Operator;

.field static final CONJ_AND:I = 0x1

.field static final CONJ_NONE:I = 0x0

.field static final CONJ_OR:I = 0x2

.field static final MOD_NONE:I = 0x0

.field static final MOD_NOT:I = 0xa

.field static final MOD_REQ:I = 0xb

.field public static final OR_OPERATOR:Lorg/apache/lucene/queryparser/classic/QueryParser$Operator;


# instance fields
.field allowLeadingWildcard:Z

.field analyzeRangeTerms:Z

.field analyzer:Lorg/apache/lucene/analysis/Analyzer;

.field autoGeneratePhraseQueries:Z

.field dateResolution:Lorg/apache/lucene/document/DateTools$Resolution;

.field enablePositionIncrements:Z

.field field:Ljava/lang/String;

.field fieldToDateResolution:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/document/DateTools$Resolution;",
            ">;"
        }
    .end annotation
.end field

.field fuzzyMinSim:F

.field fuzzyPrefixLength:I

.field locale:Ljava/util/Locale;

.field lowercaseExpandedTerms:Z

.field multiTermRewriteMethod:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

.field operator:Lorg/apache/lucene/queryparser/classic/QueryParser$Operator;

.field phraseSlop:I

.field timeZone:Ljava/util/TimeZone;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-class v0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->$assertionsDisabled:Z

    .line 58
    sget-object v0, Lorg/apache/lucene/queryparser/classic/QueryParser$Operator;->AND:Lorg/apache/lucene/queryparser/classic/QueryParser$Operator;

    sput-object v0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->AND_OPERATOR:Lorg/apache/lucene/queryparser/classic/QueryParser$Operator;

    .line 60
    sget-object v0, Lorg/apache/lucene/queryparser/classic/QueryParser$Operator;->OR:Lorg/apache/lucene/queryparser/classic/QueryParser$Operator;

    sput-object v0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->OR_OPERATOR:Lorg/apache/lucene/queryparser/classic/QueryParser$Operator;

    return-void

    .line 42
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    sget-object v0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->OR_OPERATOR:Lorg/apache/lucene/queryparser/classic/QueryParser$Operator;

    iput-object v0, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->operator:Lorg/apache/lucene/queryparser/classic/QueryParser$Operator;

    .line 65
    iput-boolean v2, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->lowercaseExpandedTerms:Z

    .line 66
    sget-object v0, Lorg/apache/lucene/search/MultiTermQuery;->CONSTANT_SCORE_AUTO_REWRITE_DEFAULT:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    iput-object v0, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->multiTermRewriteMethod:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    .line 67
    iput-boolean v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->allowLeadingWildcard:Z

    .line 68
    iput-boolean v2, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->enablePositionIncrements:Z

    .line 72
    iput v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->phraseSlop:I

    .line 73
    const/high16 v0, 0x40000000    # 2.0f

    iput v0, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->fuzzyMinSim:F

    .line 74
    iput v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->fuzzyPrefixLength:I

    .line 75
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->locale:Ljava/util/Locale;

    .line 76
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->timeZone:Ljava/util/TimeZone;

    .line 79
    iput-object v3, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->dateResolution:Lorg/apache/lucene/document/DateTools$Resolution;

    .line 81
    iput-object v3, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->fieldToDateResolution:Ljava/util/Map;

    .line 85
    iput-boolean v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->analyzeRangeTerms:Z

    .line 91
    return-void
.end method

.method private analyzeMultitermTerm(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/lucene/util/BytesRef;
    .locals 1
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "part"    # Ljava/lang/String;

    .prologue
    .line 813
    iget-object v0, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    invoke-virtual {p0, p1, p2, v0}, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->analyzeMultitermTerm(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer;)Lorg/apache/lucene/util/BytesRef;

    move-result-object v0

    return-object v0
.end method

.method public static escape(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    const/16 v4, 0x5c

    .line 1222
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 1223
    .local v2, "sb":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-lt v1, v3, :cond_0

    .line 1233
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 1224
    :cond_0
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 1226
    .local v0, "c":C
    if-eq v0, v4, :cond_1

    const/16 v3, 0x2b

    if-eq v0, v3, :cond_1

    const/16 v3, 0x2d

    if-eq v0, v3, :cond_1

    const/16 v3, 0x21

    if-eq v0, v3, :cond_1

    const/16 v3, 0x28

    if-eq v0, v3, :cond_1

    const/16 v3, 0x29

    if-eq v0, v3, :cond_1

    const/16 v3, 0x3a

    if-eq v0, v3, :cond_1

    .line 1227
    const/16 v3, 0x5e

    if-eq v0, v3, :cond_1

    const/16 v3, 0x5b

    if-eq v0, v3, :cond_1

    const/16 v3, 0x5d

    if-eq v0, v3, :cond_1

    const/16 v3, 0x22

    if-eq v0, v3, :cond_1

    const/16 v3, 0x7b

    if-eq v0, v3, :cond_1

    const/16 v3, 0x7d

    if-eq v0, v3, :cond_1

    const/16 v3, 0x7e

    if-eq v0, v3, :cond_1

    .line 1228
    const/16 v3, 0x2a

    if-eq v0, v3, :cond_1

    const/16 v3, 0x3f

    if-eq v0, v3, :cond_1

    const/16 v3, 0x7c

    if-eq v0, v3, :cond_1

    const/16 v3, 0x26

    if-eq v0, v3, :cond_1

    const/16 v3, 0x2f

    if-ne v0, v3, :cond_2

    .line 1229
    :cond_1
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1231
    :cond_2
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1223
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method static final hexToInt(C)I
    .locals 3
    .param p0, "c"    # C
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/classic/ParseException;
        }
    .end annotation

    .prologue
    .line 1206
    const/16 v0, 0x30

    if-gt v0, p0, :cond_0

    const/16 v0, 0x39

    if-gt p0, v0, :cond_0

    .line 1207
    add-int/lit8 v0, p0, -0x30

    .line 1211
    :goto_0
    return v0

    .line 1208
    :cond_0
    const/16 v0, 0x61

    if-gt v0, p0, :cond_1

    const/16 v0, 0x66

    if-gt p0, v0, :cond_1

    .line 1209
    add-int/lit8 v0, p0, -0x61

    add-int/lit8 v0, v0, 0xa

    goto :goto_0

    .line 1210
    :cond_1
    const/16 v0, 0x41

    if-gt v0, p0, :cond_2

    const/16 v0, 0x46

    if-gt p0, v0, :cond_2

    .line 1211
    add-int/lit8 v0, p0, -0x41

    add-int/lit8 v0, v0, 0xa

    goto :goto_0

    .line 1213
    :cond_2
    new-instance v0, Lorg/apache/lucene/queryparser/classic/ParseException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Non-hex character in Unicode escape sequence: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/lucene/queryparser/classic/ParseException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public abstract ReInit(Lorg/apache/lucene/queryparser/classic/CharStream;)V
.end method

.method public abstract TopLevelQuery(Ljava/lang/String;)Lorg/apache/lucene/search/Query;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/classic/ParseException;
        }
    .end annotation
.end method

.method protected addClause(Ljava/util/List;IILorg/apache/lucene/search/Query;)V
    .locals 8
    .param p2, "conj"    # I
    .param p3, "mods"    # I
    .param p4, "q"    # Lorg/apache/lucene/search/Query;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/search/BooleanClause;",
            ">;II",
            "Lorg/apache/lucene/search/Query;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "clauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/BooleanClause;>;"
    const/16 v7, 0xa

    const/4 v6, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 446
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_0

    if-ne p2, v3, :cond_0

    .line 447
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/BooleanClause;

    .line 448
    .local v0, "c":Lorg/apache/lucene/search/BooleanClause;
    invoke-virtual {v0}, Lorg/apache/lucene/search/BooleanClause;->isProhibited()Z

    move-result v4

    if-nez v4, :cond_0

    .line 449
    sget-object v4, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v0, v4}, Lorg/apache/lucene/search/BooleanClause;->setOccur(Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 452
    .end local v0    # "c":Lorg/apache/lucene/search/BooleanClause;
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_1

    iget-object v4, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->operator:Lorg/apache/lucene/queryparser/classic/QueryParser$Operator;

    sget-object v5, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->AND_OPERATOR:Lorg/apache/lucene/queryparser/classic/QueryParser$Operator;

    if-ne v4, v5, :cond_1

    if-ne p2, v6, :cond_1

    .line 457
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/BooleanClause;

    .line 458
    .restart local v0    # "c":Lorg/apache/lucene/search/BooleanClause;
    invoke-virtual {v0}, Lorg/apache/lucene/search/BooleanClause;->isProhibited()Z

    move-result v4

    if-nez v4, :cond_1

    .line 459
    sget-object v4, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v0, v4}, Lorg/apache/lucene/search/BooleanClause;->setOccur(Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 464
    .end local v0    # "c":Lorg/apache/lucene/search/BooleanClause;
    :cond_1
    if-nez p4, :cond_2

    .line 489
    :goto_0
    return-void

    .line 467
    :cond_2
    iget-object v4, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->operator:Lorg/apache/lucene/queryparser/classic/QueryParser$Operator;

    sget-object v5, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->OR_OPERATOR:Lorg/apache/lucene/queryparser/classic/QueryParser$Operator;

    if-ne v4, v5, :cond_6

    .line 470
    if-ne p3, v7, :cond_5

    move v1, v3

    .line 471
    .local v1, "prohibited":Z
    :goto_1
    const/16 v4, 0xb

    if-ne p3, v4, :cond_3

    move v2, v3

    .line 472
    .local v2, "required":Z
    :cond_3
    if-ne p2, v3, :cond_4

    if-nez v1, :cond_4

    .line 473
    const/4 v2, 0x1

    .line 481
    :cond_4
    :goto_2
    if-eqz v2, :cond_9

    if-nez v1, :cond_9

    .line 482
    sget-object v3, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {p0, p4, v3}, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->newBooleanClause(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)Lorg/apache/lucene/search/BooleanClause;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .end local v1    # "prohibited":Z
    .end local v2    # "required":Z
    :cond_5
    move v1, v2

    .line 470
    goto :goto_1

    .line 478
    :cond_6
    if-ne p3, v7, :cond_8

    move v1, v3

    .line 479
    .restart local v1    # "prohibited":Z
    :goto_3
    if-nez v1, :cond_7

    if-eq p2, v6, :cond_7

    move v2, v3

    .restart local v2    # "required":Z
    :cond_7
    goto :goto_2

    .end local v1    # "prohibited":Z
    .end local v2    # "required":Z
    :cond_8
    move v1, v2

    .line 478
    goto :goto_3

    .line 483
    .restart local v1    # "prohibited":Z
    .restart local v2    # "required":Z
    :cond_9
    if-nez v2, :cond_a

    if-nez v1, :cond_a

    .line 484
    sget-object v3, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {p0, p4, v3}, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->newBooleanClause(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)Lorg/apache/lucene/search/BooleanClause;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 485
    :cond_a
    if-nez v2, :cond_b

    if-eqz v1, :cond_b

    .line 486
    sget-object v3, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST_NOT:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {p0, p4, v3}, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->newBooleanClause(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)Lorg/apache/lucene/search/BooleanClause;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 488
    :cond_b
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "Clause cannot be both required and prohibited"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method protected analyzeMultitermTerm(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer;)Lorg/apache/lucene/util/BytesRef;
    .locals 7
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "part"    # Ljava/lang/String;
    .param p3, "analyzerIn"    # Lorg/apache/lucene/analysis/Analyzer;

    .prologue
    .line 819
    if-nez p3, :cond_0

    iget-object p3, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    .line 822
    :cond_0
    :try_start_0
    new-instance v4, Ljava/io/StringReader;

    invoke-direct {v4, p2}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3, p1, v4}, Lorg/apache/lucene/analysis/Analyzer;->tokenStream(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/TokenStream;

    move-result-object v2

    .line 823
    .local v2, "source":Lorg/apache/lucene/analysis/TokenStream;
    invoke-virtual {v2}, Lorg/apache/lucene/analysis/TokenStream;->reset()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 828
    const-class v4, Lorg/apache/lucene/analysis/tokenattributes/TermToBytesRefAttribute;

    invoke-virtual {v2, v4}, Lorg/apache/lucene/analysis/TokenStream;->getAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/analysis/tokenattributes/TermToBytesRefAttribute;

    .line 829
    .local v3, "termAtt":Lorg/apache/lucene/analysis/tokenattributes/TermToBytesRefAttribute;
    invoke-interface {v3}, Lorg/apache/lucene/analysis/tokenattributes/TermToBytesRefAttribute;->getBytesRef()Lorg/apache/lucene/util/BytesRef;

    move-result-object v0

    .line 832
    .local v0, "bytes":Lorg/apache/lucene/util/BytesRef;
    :try_start_1
    invoke-virtual {v2}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v4

    if-nez v4, :cond_1

    .line 833
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "analyzer returned no terms for multiTerm term: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 837
    :catch_0
    move-exception v1

    .line 838
    .local v1, "e":Ljava/io/IOException;
    new-instance v4, Ljava/lang/RuntimeException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "error analyzing range part: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4

    .line 824
    .end local v0    # "bytes":Lorg/apache/lucene/util/BytesRef;
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "source":Lorg/apache/lucene/analysis/TokenStream;
    .end local v3    # "termAtt":Lorg/apache/lucene/analysis/tokenattributes/TermToBytesRefAttribute;
    :catch_1
    move-exception v1

    .line 825
    .restart local v1    # "e":Ljava/io/IOException;
    new-instance v4, Ljava/lang/RuntimeException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Unable to initialize TokenStream to analyze multiTerm term: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4

    .line 834
    .end local v1    # "e":Ljava/io/IOException;
    .restart local v0    # "bytes":Lorg/apache/lucene/util/BytesRef;
    .restart local v2    # "source":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v3    # "termAtt":Lorg/apache/lucene/analysis/tokenattributes/TermToBytesRefAttribute;
    :cond_1
    :try_start_2
    invoke-interface {v3}, Lorg/apache/lucene/analysis/tokenattributes/TermToBytesRefAttribute;->fillBytesRef()I

    .line 835
    invoke-virtual {v2}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 836
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "analyzer returned too many terms for multiTerm term: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 842
    :cond_2
    :try_start_3
    invoke-virtual {v2}, Lorg/apache/lucene/analysis/TokenStream;->end()V

    .line 843
    invoke-virtual {v2}, Lorg/apache/lucene/analysis/TokenStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 848
    invoke-static {v0}, Lorg/apache/lucene/util/BytesRef;->deepCopyOf(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/BytesRef;

    move-result-object v4

    return-object v4

    .line 844
    :catch_2
    move-exception v1

    .line 845
    .restart local v1    # "e":Ljava/io/IOException;
    new-instance v4, Ljava/lang/RuntimeException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Unable to end & close TokenStream after analyzing multiTerm term: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
.end method

.method discardEscapeChar(Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p1, "input"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/classic/ParseException;
        }
    .end annotation

    .prologue
    .line 1146
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v8

    new-array v7, v8, [C

    .line 1151
    .local v7, "output":[C
    const/4 v5, 0x0

    .line 1155
    .local v5, "length":I
    const/4 v4, 0x0

    .line 1159
    .local v4, "lastCharWasEscapeChar":Z
    const/4 v1, 0x0

    .line 1162
    .local v1, "codePointMultiplier":I
    const/4 v0, 0x0

    .line 1164
    .local v0, "codePoint":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v8

    if-lt v3, v8, :cond_0

    .line 1193
    if-lez v1, :cond_6

    .line 1194
    new-instance v8, Lorg/apache/lucene/queryparser/classic/ParseException;

    const-string v9, "Truncated unicode escape sequence."

    invoke-direct {v8, v9}, Lorg/apache/lucene/queryparser/classic/ParseException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 1165
    :cond_0
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 1166
    .local v2, "curChar":C
    if-lez v1, :cond_2

    .line 1167
    invoke-static {v2}, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->hexToInt(C)I

    move-result v8

    mul-int/2addr v8, v1

    add-int/2addr v0, v8

    .line 1168
    ushr-int/lit8 v1, v1, 0x4

    .line 1169
    if-nez v1, :cond_1

    .line 1170
    add-int/lit8 v6, v5, 0x1

    .end local v5    # "length":I
    .local v6, "length":I
    int-to-char v8, v0

    aput-char v8, v7, v5

    .line 1171
    const/4 v0, 0x0

    move v5, v6

    .line 1164
    .end local v6    # "length":I
    .restart local v5    # "length":I
    :cond_1
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1173
    :cond_2
    if-eqz v4, :cond_4

    .line 1174
    const/16 v8, 0x75

    if-ne v2, v8, :cond_3

    .line 1176
    const/16 v1, 0x1000

    .line 1182
    :goto_2
    const/4 v4, 0x0

    .line 1183
    goto :goto_1

    .line 1179
    :cond_3
    aput-char v2, v7, v5

    .line 1180
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 1184
    :cond_4
    const/16 v8, 0x5c

    if-ne v2, v8, :cond_5

    .line 1185
    const/4 v4, 0x1

    .line 1186
    goto :goto_1

    .line 1187
    :cond_5
    aput-char v2, v7, v5

    .line 1188
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 1197
    .end local v2    # "curChar":C
    :cond_6
    if-eqz v4, :cond_7

    .line 1198
    new-instance v8, Lorg/apache/lucene/queryparser/classic/ParseException;

    const-string v9, "Term can not end with escape character."

    invoke-direct {v8, v9}, Lorg/apache/lucene/queryparser/classic/ParseException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 1201
    :cond_7
    new-instance v8, Ljava/lang/String;

    const/4 v9, 0x0

    invoke-direct {v8, v7, v9, v5}, Ljava/lang/String;-><init>([CII)V

    return-object v8
.end method

.method public getAllowLeadingWildcard()Z
    .locals 1

    .prologue
    .line 251
    iget-boolean v0, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->allowLeadingWildcard:Z

    return v0
.end method

.method public getAnalyzeRangeTerms()Z
    .locals 1

    .prologue
    .line 438
    iget-boolean v0, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->analyzeRangeTerms:Z

    return v0
.end method

.method public getAnalyzer()Lorg/apache/lucene/analysis/Analyzer;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    return-object v0
.end method

.method public final getAutoGeneratePhraseQueries()Z
    .locals 1

    .prologue
    .line 162
    iget-boolean v0, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->autoGeneratePhraseQueries:Z

    return v0
.end method

.method protected getBooleanQuery(Ljava/util/List;)Lorg/apache/lucene/search/Query;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/search/BooleanClause;",
            ">;)",
            "Lorg/apache/lucene/search/Query;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/classic/ParseException;
        }
    .end annotation

    .prologue
    .line 915
    .local p1, "clauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/BooleanClause;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->getBooleanQuery(Ljava/util/List;Z)Lorg/apache/lucene/search/Query;

    move-result-object v0

    return-object v0
.end method

.method protected getBooleanQuery(Ljava/util/List;Z)Lorg/apache/lucene/search/Query;
    .locals 4
    .param p2, "disableCoord"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/search/BooleanClause;",
            ">;Z)",
            "Lorg/apache/lucene/search/Query;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/classic/ParseException;
        }
    .end annotation

    .prologue
    .line 935
    .local p1, "clauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/BooleanClause;>;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_1

    .line 936
    const/4 v1, 0x0

    .line 942
    :cond_0
    return-object v1

    .line 938
    :cond_1
    invoke-virtual {p0, p2}, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->newBooleanQuery(Z)Lorg/apache/lucene/search/BooleanQuery;

    move-result-object v1

    .line 939
    .local v1, "query":Lorg/apache/lucene/search/BooleanQuery;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/BooleanClause;

    .line 940
    .local v0, "clause":Lorg/apache/lucene/search/BooleanClause;
    invoke-virtual {v1, v0}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/BooleanClause;)V

    goto :goto_0
.end method

.method public getDateResolution(Ljava/lang/String;)Lorg/apache/lucene/document/DateTools$Resolution;
    .locals 3
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 405
    if-nez p1, :cond_0

    .line 406
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Field cannot be null."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 409
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->fieldToDateResolution:Ljava/util/Map;

    if-nez v1, :cond_2

    .line 411
    iget-object v0, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->dateResolution:Lorg/apache/lucene/document/DateTools$Resolution;

    .line 420
    :cond_1
    :goto_0
    return-object v0

    .line 414
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->fieldToDateResolution:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/document/DateTools$Resolution;

    .line 415
    .local v0, "resolution":Lorg/apache/lucene/document/DateTools$Resolution;
    if-nez v0, :cond_1

    .line 417
    iget-object v0, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->dateResolution:Lorg/apache/lucene/document/DateTools$Resolution;

    goto :goto_0
.end method

.method public getDefaultOperator()Lorg/apache/lucene/queryparser/classic/QueryParser$Operator;
    .locals 1

    .prologue
    .line 295
    iget-object v0, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->operator:Lorg/apache/lucene/queryparser/classic/QueryParser$Operator;

    return-object v0
.end method

.method public getEnablePositionIncrements()Z
    .locals 1

    .prologue
    .line 274
    iget-boolean v0, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->enablePositionIncrements:Z

    return v0
.end method

.method public getField()Ljava/lang/String;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->field:Ljava/lang/String;

    return-object v0
.end method

.method protected getFieldQuery(Ljava/lang/String;Ljava/lang/String;I)Lorg/apache/lucene/search/Query;
    .locals 2
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "queryText"    # Ljava/lang/String;
    .param p3, "slop"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/classic/ParseException;
        }
    .end annotation

    .prologue
    .line 678
    const/4 v1, 0x1

    invoke-virtual {p0, p1, p2, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->getFieldQuery(Ljava/lang/String;Ljava/lang/String;Z)Lorg/apache/lucene/search/Query;

    move-result-object v0

    .line 680
    .local v0, "query":Lorg/apache/lucene/search/Query;
    instance-of v1, v0, Lorg/apache/lucene/search/PhraseQuery;

    if-eqz v1, :cond_0

    move-object v1, v0

    .line 681
    check-cast v1, Lorg/apache/lucene/search/PhraseQuery;

    invoke-virtual {v1, p3}, Lorg/apache/lucene/search/PhraseQuery;->setSlop(I)V

    .line 683
    :cond_0
    instance-of v1, v0, Lorg/apache/lucene/search/MultiPhraseQuery;

    if-eqz v1, :cond_1

    move-object v1, v0

    .line 684
    check-cast v1, Lorg/apache/lucene/search/MultiPhraseQuery;

    invoke-virtual {v1, p3}, Lorg/apache/lucene/search/MultiPhraseQuery;->setSlop(I)V

    .line 687
    :cond_1
    return-object v0
.end method

.method protected getFieldQuery(Ljava/lang/String;Ljava/lang/String;Z)Lorg/apache/lucene/search/Query;
    .locals 1
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "queryText"    # Ljava/lang/String;
    .param p3, "quoted"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/classic/ParseException;
        }
    .end annotation

    .prologue
    .line 495
    iget-object v0, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    invoke-virtual {p0, v0, p1, p2, p3}, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->newFieldQuery(Lorg/apache/lucene/analysis/Analyzer;Ljava/lang/String;Ljava/lang/String;Z)Lorg/apache/lucene/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public getFuzzyMinSim()F
    .locals 1

    .prologue
    .line 183
    iget v0, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->fuzzyMinSim:F

    return v0
.end method

.method public getFuzzyPrefixLength()I
    .locals 1

    .prologue
    .line 201
    iget v0, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->fuzzyPrefixLength:I

    return v0
.end method

.method protected getFuzzyQuery(Ljava/lang/String;Ljava/lang/String;F)Lorg/apache/lucene/search/Query;
    .locals 2
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "termStr"    # Ljava/lang/String;
    .param p3, "minSimilarity"    # F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/classic/ParseException;
        }
    .end annotation

    .prologue
    .line 1056
    iget-boolean v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->lowercaseExpandedTerms:Z

    if-eqz v1, :cond_0

    .line 1057
    iget-object v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->locale:Ljava/util/Locale;

    invoke-virtual {p2, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p2

    .line 1059
    :cond_0
    new-instance v0, Lorg/apache/lucene/index/Term;

    invoke-direct {v0, p1, p2}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1060
    .local v0, "t":Lorg/apache/lucene/index/Term;
    iget v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->fuzzyPrefixLength:I

    invoke-virtual {p0, v0, p3, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->newFuzzyQuery(Lorg/apache/lucene/index/Term;FI)Lorg/apache/lucene/search/Query;

    move-result-object v1

    return-object v1
.end method

.method public getLocale()Ljava/util/Locale;
    .locals 1

    .prologue
    .line 354
    iget-object v0, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->locale:Ljava/util/Locale;

    return-object v0
.end method

.method public getLowercaseExpandedTerms()Z
    .locals 1

    .prologue
    .line 313
    iget-boolean v0, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->lowercaseExpandedTerms:Z

    return v0
.end method

.method public getMultiTermRewriteMethod()Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;
    .locals 1

    .prologue
    .line 337
    iget-object v0, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->multiTermRewriteMethod:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    return-object v0
.end method

.method public getPhraseSlop()I
    .locals 1

    .prologue
    .line 227
    iget v0, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->phraseSlop:I

    return v0
.end method

.method protected getPrefixQuery(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/lucene/search/Query;
    .locals 3
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "termStr"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/classic/ParseException;
        }
    .end annotation

    .prologue
    .line 1034
    iget-boolean v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->allowLeadingWildcard:Z

    if-nez v1, :cond_0

    const-string v1, "*"

    invoke-virtual {p2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1035
    new-instance v1, Lorg/apache/lucene/queryparser/classic/ParseException;

    const-string v2, "\'*\' not allowed as first character in PrefixQuery"

    invoke-direct {v1, v2}, Lorg/apache/lucene/queryparser/classic/ParseException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1036
    :cond_0
    iget-boolean v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->lowercaseExpandedTerms:Z

    if-eqz v1, :cond_1

    .line 1037
    iget-object v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->locale:Ljava/util/Locale;

    invoke-virtual {p2, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p2

    .line 1039
    :cond_1
    new-instance v0, Lorg/apache/lucene/index/Term;

    invoke-direct {v0, p1, p2}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1040
    .local v0, "t":Lorg/apache/lucene/index/Term;
    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->newPrefixQuery(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/search/Query;

    move-result-object v1

    return-object v1
.end method

.method protected getRangeQuery(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Lorg/apache/lucene/search/Query;
    .locals 6
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "part1"    # Ljava/lang/String;
    .param p3, "part2"    # Ljava/lang/String;
    .param p4, "startInclusive"    # Z
    .param p5, "endInclusive"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/classic/ParseException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 696
    iget-boolean v5, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->lowercaseExpandedTerms:Z

    if-eqz v5, :cond_0

    .line 697
    if-nez p2, :cond_2

    move-object p2, v4

    .line 698
    :goto_0
    if-nez p3, :cond_3

    move-object p3, v4

    .line 702
    :cond_0
    :goto_1
    const/4 v4, 0x3

    iget-object v5, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->locale:Ljava/util/Locale;

    invoke-static {v4, v5}, Ljava/text/DateFormat;->getDateInstance(ILjava/util/Locale;)Ljava/text/DateFormat;

    move-result-object v2

    .line 703
    .local v2, "df":Ljava/text/DateFormat;
    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Ljava/text/DateFormat;->setLenient(Z)V

    .line 704
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->getDateResolution(Ljava/lang/String;)Lorg/apache/lucene/document/DateTools$Resolution;

    move-result-object v3

    .line 707
    .local v3, "resolution":Lorg/apache/lucene/document/DateTools$Resolution;
    :try_start_0
    invoke-virtual {v2, p2}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v4

    invoke-static {v4, v3}, Lorg/apache/lucene/document/DateTools;->dateToString(Ljava/util/Date;Lorg/apache/lucene/document/DateTools$Resolution;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object p2

    .line 711
    :goto_2
    :try_start_1
    invoke-virtual {v2, p3}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    .line 712
    .local v1, "d2":Ljava/util/Date;
    if-eqz p5, :cond_1

    .line 716
    iget-object v4, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->timeZone:Ljava/util/TimeZone;

    iget-object v5, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->locale:Ljava/util/Locale;

    invoke-static {v4, v5}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v0

    .line 717
    .local v0, "cal":Ljava/util/Calendar;
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 718
    const/16 v4, 0xb

    const/16 v5, 0x17

    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->set(II)V

    .line 719
    const/16 v4, 0xc

    const/16 v5, 0x3b

    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->set(II)V

    .line 720
    const/16 v4, 0xd

    const/16 v5, 0x3b

    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->set(II)V

    .line 721
    const/16 v4, 0xe

    const/16 v5, 0x3e7

    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->set(II)V

    .line 722
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    .line 724
    .end local v0    # "cal":Ljava/util/Calendar;
    :cond_1
    invoke-static {v1, v3}, Lorg/apache/lucene/document/DateTools;->dateToString(Ljava/util/Date;Lorg/apache/lucene/document/DateTools$Resolution;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object p3

    .line 727
    .end local v1    # "d2":Ljava/util/Date;
    :goto_3
    invoke-virtual/range {p0 .. p5}, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->newRangeQuery(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Lorg/apache/lucene/search/Query;

    move-result-object v4

    return-object v4

    .line 697
    .end local v2    # "df":Ljava/text/DateFormat;
    .end local v3    # "resolution":Lorg/apache/lucene/document/DateTools$Resolution;
    :cond_2
    iget-object v5, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->locale:Ljava/util/Locale;

    invoke-virtual {p2, v5}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    .line 698
    :cond_3
    iget-object v4, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->locale:Ljava/util/Locale;

    invoke-virtual {p3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p3

    goto :goto_1

    .line 725
    .restart local v2    # "df":Ljava/text/DateFormat;
    .restart local v3    # "resolution":Lorg/apache/lucene/document/DateTools$Resolution;
    :catch_0
    move-exception v4

    goto :goto_3

    .line 708
    :catch_1
    move-exception v4

    goto :goto_2
.end method

.method protected getRegexpQuery(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/lucene/search/Query;
    .locals 2
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "termStr"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/classic/ParseException;
        }
    .end annotation

    .prologue
    .line 1002
    iget-boolean v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->lowercaseExpandedTerms:Z

    if-eqz v1, :cond_0

    .line 1003
    iget-object v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->locale:Ljava/util/Locale;

    invoke-virtual {p2, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p2

    .line 1005
    :cond_0
    new-instance v0, Lorg/apache/lucene/index/Term;

    invoke-direct {v0, p1, p2}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1006
    .local v0, "t":Lorg/apache/lucene/index/Term;
    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->newRegexpQuery(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/search/Query;

    move-result-object v1

    return-object v1
.end method

.method public getTimeZone()Ljava/util/TimeZone;
    .locals 1

    .prologue
    .line 364
    iget-object v0, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->timeZone:Ljava/util/TimeZone;

    return-object v0
.end method

.method protected getWildcardQuery(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/lucene/search/Query;
    .locals 3
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "termStr"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/classic/ParseException;
        }
    .end annotation

    .prologue
    .line 968
    const-string v1, "*"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 969
    const-string v1, "*"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->newMatchAllDocsQuery()Lorg/apache/lucene/search/Query;

    move-result-object v1

    .line 977
    :goto_0
    return-object v1

    .line 971
    :cond_0
    iget-boolean v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->allowLeadingWildcard:Z

    if-nez v1, :cond_2

    const-string v1, "*"

    invoke-virtual {p2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "?"

    invoke-virtual {p2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 972
    :cond_1
    new-instance v1, Lorg/apache/lucene/queryparser/classic/ParseException;

    const-string v2, "\'*\' or \'?\' not allowed as first character in WildcardQuery"

    invoke-direct {v1, v2}, Lorg/apache/lucene/queryparser/classic/ParseException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 973
    :cond_2
    iget-boolean v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->lowercaseExpandedTerms:Z

    if-eqz v1, :cond_3

    .line 974
    iget-object v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->locale:Ljava/util/Locale;

    invoke-virtual {p2, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p2

    .line 976
    :cond_3
    new-instance v0, Lorg/apache/lucene/index/Term;

    invoke-direct {v0, p1, p2}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 977
    .local v0, "t":Lorg/apache/lucene/index/Term;
    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->newWildcardQuery(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/search/Query;

    move-result-object v1

    goto :goto_0
.end method

.method handleBareFuzzy(Ljava/lang/String;Lorg/apache/lucene/queryparser/classic/Token;Ljava/lang/String;)Lorg/apache/lucene/search/Query;
    .locals 4
    .param p1, "qfield"    # Ljava/lang/String;
    .param p2, "fuzzySlop"    # Lorg/apache/lucene/queryparser/classic/Token;
    .param p3, "termImage"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/classic/ParseException;
        }
    .end annotation

    .prologue
    .line 1088
    iget v0, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->fuzzyMinSim:F

    .line 1090
    .local v0, "fms":F
    :try_start_0
    iget-object v2, p2, Lorg/apache/lucene/queryparser/classic/Token;->image:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 1092
    :goto_0
    const/4 v2, 0x0

    cmpg-float v2, v0, v2

    if-gez v2, :cond_0

    .line 1093
    new-instance v2, Lorg/apache/lucene/queryparser/classic/ParseException;

    const-string v3, "Minimum similarity for a FuzzyQuery has to be between 0.0f and 1.0f !"

    invoke-direct {v2, v3}, Lorg/apache/lucene/queryparser/classic/ParseException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1094
    :cond_0
    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v2, v0, v2

    if-ltz v2, :cond_1

    float-to-int v2, v0

    int-to-float v2, v2

    cmpl-float v2, v0, v2

    if-eqz v2, :cond_1

    .line 1095
    new-instance v2, Lorg/apache/lucene/queryparser/classic/ParseException;

    const-string v3, "Fractional edit distances are not allowed!"

    invoke-direct {v2, v3}, Lorg/apache/lucene/queryparser/classic/ParseException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1097
    :cond_1
    invoke-virtual {p0, p1, p3, v0}, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->getFuzzyQuery(Ljava/lang/String;Ljava/lang/String;F)Lorg/apache/lucene/search/Query;

    move-result-object v1

    .line 1098
    .local v1, "q":Lorg/apache/lucene/search/Query;
    return-object v1

    .line 1091
    .end local v1    # "q":Lorg/apache/lucene/search/Query;
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method handleBareTokenQuery(Ljava/lang/String;Lorg/apache/lucene/queryparser/classic/Token;Lorg/apache/lucene/queryparser/classic/Token;ZZZZ)Lorg/apache/lucene/search/Query;
    .locals 5
    .param p1, "qfield"    # Ljava/lang/String;
    .param p2, "term"    # Lorg/apache/lucene/queryparser/classic/Token;
    .param p3, "fuzzySlop"    # Lorg/apache/lucene/queryparser/classic/Token;
    .param p4, "prefix"    # Z
    .param p5, "wildcard"    # Z
    .param p6, "fuzzy"    # Z
    .param p7, "regexp"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/classic/ParseException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 1068
    iget-object v2, p2, Lorg/apache/lucene/queryparser/classic/Token;->image:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->discardEscapeChar(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1069
    .local v1, "termImage":Ljava/lang/String;
    if-eqz p5, :cond_0

    .line 1070
    iget-object v2, p2, Lorg/apache/lucene/queryparser/classic/Token;->image:Ljava/lang/String;

    invoke-virtual {p0, p1, v2}, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->getWildcardQuery(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/lucene/search/Query;

    move-result-object v0

    .line 1082
    .local v0, "q":Lorg/apache/lucene/search/Query;
    :goto_0
    return-object v0

    .line 1071
    .end local v0    # "q":Lorg/apache/lucene/search/Query;
    :cond_0
    if-eqz p4, :cond_1

    .line 1073
    iget-object v2, p2, Lorg/apache/lucene/queryparser/classic/Token;->image:Ljava/lang/String;

    .line 1074
    iget-object v3, p2, Lorg/apache/lucene/queryparser/classic/Token;->image:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    .line 1073
    invoke-virtual {v2, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->discardEscapeChar(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1072
    invoke-virtual {p0, p1, v2}, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->getPrefixQuery(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/lucene/search/Query;

    move-result-object v0

    .line 1075
    .restart local v0    # "q":Lorg/apache/lucene/search/Query;
    goto :goto_0

    .end local v0    # "q":Lorg/apache/lucene/search/Query;
    :cond_1
    if-eqz p7, :cond_2

    .line 1076
    iget-object v2, p2, Lorg/apache/lucene/queryparser/classic/Token;->image:Ljava/lang/String;

    const/4 v3, 0x1

    iget-object v4, p2, Lorg/apache/lucene/queryparser/classic/Token;->image:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, p1, v2}, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->getRegexpQuery(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/lucene/search/Query;

    move-result-object v0

    .line 1077
    .restart local v0    # "q":Lorg/apache/lucene/search/Query;
    goto :goto_0

    .end local v0    # "q":Lorg/apache/lucene/search/Query;
    :cond_2
    if-eqz p6, :cond_3

    .line 1078
    invoke-virtual {p0, p1, p3, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->handleBareFuzzy(Ljava/lang/String;Lorg/apache/lucene/queryparser/classic/Token;Ljava/lang/String;)Lorg/apache/lucene/search/Query;

    move-result-object v0

    .line 1079
    .restart local v0    # "q":Lorg/apache/lucene/search/Query;
    goto :goto_0

    .line 1080
    .end local v0    # "q":Lorg/apache/lucene/search/Query;
    :cond_3
    invoke-virtual {p0, p1, v1, v4}, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->getFieldQuery(Ljava/lang/String;Ljava/lang/String;Z)Lorg/apache/lucene/search/Query;

    move-result-object v0

    .restart local v0    # "q":Lorg/apache/lucene/search/Query;
    goto :goto_0
.end method

.method handleBoost(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/queryparser/classic/Token;)Lorg/apache/lucene/search/Query;
    .locals 2
    .param p1, "q"    # Lorg/apache/lucene/search/Query;
    .param p2, "boost"    # Lorg/apache/lucene/queryparser/classic/Token;

    .prologue
    .line 1115
    if-eqz p2, :cond_0

    .line 1116
    const/high16 v0, 0x3f800000    # 1.0f

    .line 1118
    .local v0, "f":F
    :try_start_0
    iget-object v1, p2, Lorg/apache/lucene/queryparser/classic/Token;->image:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 1127
    :goto_0
    if-eqz p1, :cond_0

    .line 1128
    invoke-virtual {p1, v0}, Lorg/apache/lucene/search/Query;->setBoost(F)V

    .line 1131
    .end local v0    # "f":F
    :cond_0
    return-object p1

    .line 1120
    .restart local v0    # "f":F
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method handleQuotedTerm(Ljava/lang/String;Lorg/apache/lucene/queryparser/classic/Token;Lorg/apache/lucene/queryparser/classic/Token;)Lorg/apache/lucene/search/Query;
    .locals 4
    .param p1, "qfield"    # Ljava/lang/String;
    .param p2, "term"    # Lorg/apache/lucene/queryparser/classic/Token;
    .param p3, "fuzzySlop"    # Lorg/apache/lucene/queryparser/classic/Token;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/classic/ParseException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 1103
    iget v0, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->phraseSlop:I

    .line 1104
    .local v0, "s":I
    if-eqz p3, :cond_0

    .line 1106
    :try_start_0
    iget-object v1, p3, Lorg/apache/lucene/queryparser/classic/Token;->image:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 1110
    :cond_0
    :goto_0
    iget-object v1, p2, Lorg/apache/lucene/queryparser/classic/Token;->image:Ljava/lang/String;

    iget-object v2, p2, Lorg/apache/lucene/queryparser/classic/Token;->image:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->discardEscapeChar(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1, v1, v0}, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->getFieldQuery(Ljava/lang/String;Ljava/lang/String;I)Lorg/apache/lucene/search/Query;

    move-result-object v1

    return-object v1

    .line 1108
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public init(Lorg/apache/lucene/util/Version;Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "f"    # Ljava/lang/String;
    .param p3, "a"    # Lorg/apache/lucene/analysis/Analyzer;

    .prologue
    .line 99
    iput-object p3, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    .line 100
    iput-object p2, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->field:Ljava/lang/String;

    .line 101
    sget-object v0, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    invoke-virtual {p1, v0}, Lorg/apache/lucene/util/Version;->onOrAfter(Lorg/apache/lucene/util/Version;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->setAutoGeneratePhraseQueries(Z)V

    .line 106
    :goto_0
    return-void

    .line 104
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->setAutoGeneratePhraseQueries(Z)V

    goto :goto_0
.end method

.method protected newBooleanClause(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)Lorg/apache/lucene/search/BooleanClause;
    .locals 1
    .param p1, "q"    # Lorg/apache/lucene/search/Query;
    .param p2, "occur"    # Lorg/apache/lucene/search/BooleanClause$Occur;

    .prologue
    .line 746
    new-instance v0, Lorg/apache/lucene/search/BooleanClause;

    invoke-direct {v0, p1, p2}, Lorg/apache/lucene/search/BooleanClause;-><init>(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    return-object v0
.end method

.method protected newBooleanQuery(Z)Lorg/apache/lucene/search/BooleanQuery;
    .locals 1
    .param p1, "disableCoord"    # Z

    .prologue
    .line 736
    new-instance v0, Lorg/apache/lucene/search/BooleanQuery;

    invoke-direct {v0, p1}, Lorg/apache/lucene/search/BooleanQuery;-><init>(Z)V

    return-object v0
.end method

.method protected newFieldQuery(Lorg/apache/lucene/analysis/Analyzer;Ljava/lang/String;Ljava/lang/String;Z)Lorg/apache/lucene/search/Query;
    .locals 26
    .param p1, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "queryText"    # Ljava/lang/String;
    .param p4, "quoted"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/classic/ParseException;
        }
    .end annotation

    .prologue
    .line 507
    :try_start_0
    new-instance v24, Ljava/io/StringReader;

    move-object/from16 v0, v24

    move-object/from16 v1, p3

    invoke-direct {v0, v1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/analysis/Analyzer;->tokenStream(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/TokenStream;

    move-result-object v22

    .line 508
    .local v22, "source":Lorg/apache/lucene/analysis/TokenStream;
    invoke-virtual/range {v22 .. v22}, Lorg/apache/lucene/analysis/TokenStream;->reset()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 514
    new-instance v3, Lorg/apache/lucene/analysis/CachingTokenFilter;

    move-object/from16 v0, v22

    invoke-direct {v3, v0}, Lorg/apache/lucene/analysis/CachingTokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 515
    .local v3, "buffer":Lorg/apache/lucene/analysis/CachingTokenFilter;
    const/16 v23, 0x0

    .line 516
    .local v23, "termAtt":Lorg/apache/lucene/analysis/tokenattributes/TermToBytesRefAttribute;
    const/4 v15, 0x0

    .line 517
    .local v15, "posIncrAtt":Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;
    const/4 v12, 0x0

    .line 519
    .local v12, "numTokens":I
    invoke-virtual {v3}, Lorg/apache/lucene/analysis/CachingTokenFilter;->reset()V

    .line 521
    const-class v24, Lorg/apache/lucene/analysis/tokenattributes/TermToBytesRefAttribute;

    move-object/from16 v0, v24

    invoke-virtual {v3, v0}, Lorg/apache/lucene/analysis/CachingTokenFilter;->hasAttribute(Ljava/lang/Class;)Z

    move-result v24

    if-eqz v24, :cond_0

    .line 522
    const-class v24, Lorg/apache/lucene/analysis/tokenattributes/TermToBytesRefAttribute;

    move-object/from16 v0, v24

    invoke-virtual {v3, v0}, Lorg/apache/lucene/analysis/CachingTokenFilter;->getAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v23

    .end local v23    # "termAtt":Lorg/apache/lucene/analysis/tokenattributes/TermToBytesRefAttribute;
    check-cast v23, Lorg/apache/lucene/analysis/tokenattributes/TermToBytesRefAttribute;

    .line 524
    .restart local v23    # "termAtt":Lorg/apache/lucene/analysis/tokenattributes/TermToBytesRefAttribute;
    :cond_0
    const-class v24, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    move-object/from16 v0, v24

    invoke-virtual {v3, v0}, Lorg/apache/lucene/analysis/CachingTokenFilter;->hasAttribute(Ljava/lang/Class;)Z

    move-result v24

    if-eqz v24, :cond_1

    .line 525
    const-class v24, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    move-object/from16 v0, v24

    invoke-virtual {v3, v0}, Lorg/apache/lucene/analysis/CachingTokenFilter;->getAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v15

    .end local v15    # "posIncrAtt":Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;
    check-cast v15, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    .line 528
    .restart local v15    # "posIncrAtt":Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;
    :cond_1
    const/16 v17, 0x0

    .line 529
    .local v17, "positionCount":I
    const/16 v21, 0x0

    .line 531
    .local v21, "severalTokensAtSamePosition":Z
    const/4 v7, 0x0

    .line 532
    .local v7, "hasMoreTokens":Z
    if-eqz v23, :cond_2

    .line 534
    :try_start_1
    invoke-virtual {v3}, Lorg/apache/lucene/analysis/CachingTokenFilter;->incrementToken()Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6

    move-result v7

    .line 535
    :goto_0
    if-nez v7, :cond_4

    .line 551
    :cond_2
    :goto_1
    :try_start_2
    invoke-virtual {v3}, Lorg/apache/lucene/analysis/CachingTokenFilter;->reset()V

    .line 554
    invoke-virtual/range {v22 .. v22}, Lorg/apache/lucene/analysis/TokenStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 562
    if-nez v23, :cond_7

    const/4 v4, 0x0

    .line 564
    .local v4, "bytes":Lorg/apache/lucene/util/BytesRef;
    :goto_2
    if-nez v12, :cond_8

    .line 565
    const/16 v19, 0x0

    .line 662
    :cond_3
    :goto_3
    return-object v19

    .line 509
    .end local v3    # "buffer":Lorg/apache/lucene/analysis/CachingTokenFilter;
    .end local v4    # "bytes":Lorg/apache/lucene/util/BytesRef;
    .end local v7    # "hasMoreTokens":Z
    .end local v12    # "numTokens":I
    .end local v15    # "posIncrAtt":Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;
    .end local v17    # "positionCount":I
    .end local v21    # "severalTokensAtSamePosition":Z
    .end local v22    # "source":Lorg/apache/lucene/analysis/TokenStream;
    .end local v23    # "termAtt":Lorg/apache/lucene/analysis/tokenattributes/TermToBytesRefAttribute;
    :catch_0
    move-exception v6

    .line 510
    .local v6, "e":Ljava/io/IOException;
    new-instance v14, Lorg/apache/lucene/queryparser/classic/ParseException;

    const-string v24, "Unable to initialize TokenStream to analyze query text"

    move-object/from16 v0, v24

    invoke-direct {v14, v0}, Lorg/apache/lucene/queryparser/classic/ParseException;-><init>(Ljava/lang/String;)V

    .line 511
    .local v14, "p":Lorg/apache/lucene/queryparser/classic/ParseException;
    invoke-virtual {v14, v6}, Lorg/apache/lucene/queryparser/classic/ParseException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 512
    throw v14

    .line 536
    .end local v6    # "e":Ljava/io/IOException;
    .end local v14    # "p":Lorg/apache/lucene/queryparser/classic/ParseException;
    .restart local v3    # "buffer":Lorg/apache/lucene/analysis/CachingTokenFilter;
    .restart local v7    # "hasMoreTokens":Z
    .restart local v12    # "numTokens":I
    .restart local v15    # "posIncrAtt":Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;
    .restart local v17    # "positionCount":I
    .restart local v21    # "severalTokensAtSamePosition":Z
    .restart local v22    # "source":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v23    # "termAtt":Lorg/apache/lucene/analysis/tokenattributes/TermToBytesRefAttribute;
    :cond_4
    add-int/lit8 v12, v12, 0x1

    .line 537
    if-eqz v15, :cond_5

    :try_start_3
    invoke-interface {v15}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->getPositionIncrement()I

    move-result v18

    .line 538
    .local v18, "positionIncrement":I
    :goto_4
    if-eqz v18, :cond_6

    .line 539
    add-int v17, v17, v18

    .line 543
    :goto_5
    invoke-virtual {v3}, Lorg/apache/lucene/analysis/CachingTokenFilter;->incrementToken()Z
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_6

    move-result v7

    goto :goto_0

    .line 537
    .end local v18    # "positionIncrement":I
    :cond_5
    const/16 v18, 0x1

    goto :goto_4

    .line 541
    .restart local v18    # "positionIncrement":I
    :cond_6
    const/16 v21, 0x1

    goto :goto_5

    .line 556
    .end local v18    # "positionIncrement":I
    :catch_1
    move-exception v6

    .line 557
    .restart local v6    # "e":Ljava/io/IOException;
    new-instance v14, Lorg/apache/lucene/queryparser/classic/ParseException;

    const-string v24, "Cannot close TokenStream analyzing query text"

    move-object/from16 v0, v24

    invoke-direct {v14, v0}, Lorg/apache/lucene/queryparser/classic/ParseException;-><init>(Ljava/lang/String;)V

    .line 558
    .restart local v14    # "p":Lorg/apache/lucene/queryparser/classic/ParseException;
    invoke-virtual {v14, v6}, Lorg/apache/lucene/queryparser/classic/ParseException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 559
    throw v14

    .line 562
    .end local v6    # "e":Ljava/io/IOException;
    .end local v14    # "p":Lorg/apache/lucene/queryparser/classic/ParseException;
    :cond_7
    invoke-interface/range {v23 .. v23}, Lorg/apache/lucene/analysis/tokenattributes/TermToBytesRefAttribute;->getBytesRef()Lorg/apache/lucene/util/BytesRef;

    move-result-object v4

    goto :goto_2

    .line 566
    .restart local v4    # "bytes":Lorg/apache/lucene/util/BytesRef;
    :cond_8
    const/16 v24, 0x1

    move/from16 v0, v24

    if-ne v12, v0, :cond_a

    .line 568
    :try_start_4
    invoke-virtual {v3}, Lorg/apache/lucene/analysis/CachingTokenFilter;->incrementToken()Z

    move-result v8

    .line 569
    .local v8, "hasNext":Z
    sget-boolean v24, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->$assertionsDisabled:Z

    if-nez v24, :cond_9

    if-nez v8, :cond_9

    new-instance v24, Ljava/lang/AssertionError;

    invoke-direct/range {v24 .. v24}, Ljava/lang/AssertionError;-><init>()V

    throw v24
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 571
    .end local v8    # "hasNext":Z
    :catch_2
    move-exception v24

    .line 574
    :goto_6
    new-instance v24, Lorg/apache/lucene/index/Term;

    invoke-static {v4}, Lorg/apache/lucene/util/BytesRef;->deepCopyOf(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/BytesRef;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, p2

    move-object/from16 v2, v25

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->newTermQuery(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/search/Query;

    move-result-object v19

    goto :goto_3

    .line 570
    .restart local v8    # "hasNext":Z
    :cond_9
    :try_start_5
    invoke-interface/range {v23 .. v23}, Lorg/apache/lucene/analysis/tokenattributes/TermToBytesRefAttribute;->fillBytesRef()I
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_6

    .line 576
    .end local v8    # "hasNext":Z
    :cond_a
    if-nez v21, :cond_b

    if-nez p4, :cond_18

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->autoGeneratePhraseQueries:Z

    move/from16 v24, v0

    if-nez v24, :cond_18

    .line 577
    :cond_b
    const/16 v24, 0x1

    move/from16 v0, v17

    move/from16 v1, v24

    if-eq v0, v1, :cond_c

    if-nez p4, :cond_11

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->autoGeneratePhraseQueries:Z

    move/from16 v24, v0

    if-nez v24, :cond_11

    .line 579
    :cond_c
    const/16 v24, 0x1

    move/from16 v0, v17

    move/from16 v1, v24

    if-ne v0, v1, :cond_d

    const/16 v24, 0x1

    :goto_7
    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->newBooleanQuery(Z)Lorg/apache/lucene/search/BooleanQuery;

    move-result-object v20

    .line 581
    .local v20, "q":Lorg/apache/lucene/search/BooleanQuery;
    const/16 v24, 0x1

    move/from16 v0, v17

    move/from16 v1, v24

    if-le v0, v1, :cond_e

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->operator:Lorg/apache/lucene/queryparser/classic/QueryParser$Operator;

    move-object/from16 v24, v0

    sget-object v25, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->AND_OPERATOR:Lorg/apache/lucene/queryparser/classic/QueryParser$Operator;

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    if-ne v0, v1, :cond_e

    .line 582
    sget-object v13, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    .line 584
    .local v13, "occur":Lorg/apache/lucene/search/BooleanClause$Occur;
    :goto_8
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_9
    if-lt v9, v12, :cond_f

    move-object/from16 v19, v20

    .line 596
    goto/16 :goto_3

    .line 579
    .end local v9    # "i":I
    .end local v13    # "occur":Lorg/apache/lucene/search/BooleanClause$Occur;
    .end local v20    # "q":Lorg/apache/lucene/search/BooleanQuery;
    :cond_d
    const/16 v24, 0x0

    goto :goto_7

    .line 582
    .restart local v20    # "q":Lorg/apache/lucene/search/BooleanQuery;
    :cond_e
    sget-object v13, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    goto :goto_8

    .line 586
    .restart local v9    # "i":I
    .restart local v13    # "occur":Lorg/apache/lucene/search/BooleanClause$Occur;
    :cond_f
    :try_start_6
    invoke-virtual {v3}, Lorg/apache/lucene/analysis/CachingTokenFilter;->incrementToken()Z

    move-result v8

    .line 587
    .restart local v8    # "hasNext":Z
    sget-boolean v24, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->$assertionsDisabled:Z

    if-nez v24, :cond_10

    if-nez v8, :cond_10

    new-instance v24, Ljava/lang/AssertionError;

    invoke-direct/range {v24 .. v24}, Ljava/lang/AssertionError;-><init>()V

    throw v24
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 589
    .end local v8    # "hasNext":Z
    :catch_3
    move-exception v24

    .line 593
    :goto_a
    new-instance v24, Lorg/apache/lucene/index/Term;

    invoke-static {v4}, Lorg/apache/lucene/util/BytesRef;->deepCopyOf(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/BytesRef;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, p2

    move-object/from16 v2, v25

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 592
    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->newTermQuery(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/search/Query;

    move-result-object v5

    .line 594
    .local v5, "currentQuery":Lorg/apache/lucene/search/Query;
    move-object/from16 v0, v20

    invoke-virtual {v0, v5, v13}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 584
    add-int/lit8 v9, v9, 0x1

    goto :goto_9

    .line 588
    .end local v5    # "currentQuery":Lorg/apache/lucene/search/Query;
    .restart local v8    # "hasNext":Z
    :cond_10
    :try_start_7
    invoke-interface/range {v23 .. v23}, Lorg/apache/lucene/analysis/tokenattributes/TermToBytesRefAttribute;->fillBytesRef()I
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    goto :goto_a

    .line 600
    .end local v8    # "hasNext":Z
    .end local v9    # "i":I
    .end local v13    # "occur":Lorg/apache/lucene/search/BooleanClause$Occur;
    .end local v20    # "q":Lorg/apache/lucene/search/BooleanQuery;
    :cond_11
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->newMultiPhraseQuery()Lorg/apache/lucene/search/MultiPhraseQuery;

    move-result-object v10

    .line 601
    .local v10, "mpq":Lorg/apache/lucene/search/MultiPhraseQuery;
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->phraseSlop:I

    move/from16 v24, v0

    move/from16 v0, v24

    invoke-virtual {v10, v0}, Lorg/apache/lucene/search/MultiPhraseQuery;->setSlop(I)V

    .line 602
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 603
    .local v11, "multiTerms":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/Term;>;"
    const/16 v16, -0x1

    .line 604
    .local v16, "position":I
    const/4 v9, 0x0

    .restart local v9    # "i":I
    :goto_b
    if-lt v9, v12, :cond_12

    .line 628
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->enablePositionIncrements:Z

    move/from16 v24, v0

    if-eqz v24, :cond_17

    .line 629
    const/16 v24, 0x0

    move/from16 v0, v24

    new-array v0, v0, [Lorg/apache/lucene/index/Term;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-interface {v11, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v24

    check-cast v24, [Lorg/apache/lucene/index/Term;

    move-object/from16 v0, v24

    move/from16 v1, v16

    invoke-virtual {v10, v0, v1}, Lorg/apache/lucene/search/MultiPhraseQuery;->add([Lorg/apache/lucene/index/Term;I)V

    :goto_c
    move-object/from16 v19, v10

    .line 633
    goto/16 :goto_3

    .line 605
    :cond_12
    const/16 v18, 0x1

    .line 607
    .restart local v18    # "positionIncrement":I
    :try_start_8
    invoke-virtual {v3}, Lorg/apache/lucene/analysis/CachingTokenFilter;->incrementToken()Z

    move-result v8

    .line 608
    .restart local v8    # "hasNext":Z
    sget-boolean v24, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->$assertionsDisabled:Z

    if-nez v24, :cond_15

    if-nez v8, :cond_15

    new-instance v24, Ljava/lang/AssertionError;

    invoke-direct/range {v24 .. v24}, Ljava/lang/AssertionError;-><init>()V

    throw v24
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    .line 613
    .end local v8    # "hasNext":Z
    :catch_4
    move-exception v24

    .line 617
    :cond_13
    :goto_d
    if-lez v18, :cond_14

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v24

    if-lez v24, :cond_14

    .line 618
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->enablePositionIncrements:Z

    move/from16 v24, v0

    if-eqz v24, :cond_16

    .line 619
    const/16 v24, 0x0

    move/from16 v0, v24

    new-array v0, v0, [Lorg/apache/lucene/index/Term;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-interface {v11, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v24

    check-cast v24, [Lorg/apache/lucene/index/Term;

    move-object/from16 v0, v24

    move/from16 v1, v16

    invoke-virtual {v10, v0, v1}, Lorg/apache/lucene/search/MultiPhraseQuery;->add([Lorg/apache/lucene/index/Term;I)V

    .line 623
    :goto_e
    invoke-interface {v11}, Ljava/util/List;->clear()V

    .line 625
    :cond_14
    add-int v16, v16, v18

    .line 626
    new-instance v24, Lorg/apache/lucene/index/Term;

    invoke-static {v4}, Lorg/apache/lucene/util/BytesRef;->deepCopyOf(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/BytesRef;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, p2

    move-object/from16 v2, v25

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    move-object/from16 v0, v24

    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 604
    add-int/lit8 v9, v9, 0x1

    goto :goto_b

    .line 609
    .restart local v8    # "hasNext":Z
    :cond_15
    :try_start_9
    invoke-interface/range {v23 .. v23}, Lorg/apache/lucene/analysis/tokenattributes/TermToBytesRefAttribute;->fillBytesRef()I

    .line 610
    if-eqz v15, :cond_13

    .line 611
    invoke-interface {v15}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->getPositionIncrement()I
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_4

    move-result v18

    goto :goto_d

    .line 621
    .end local v8    # "hasNext":Z
    :cond_16
    const/16 v24, 0x0

    move/from16 v0, v24

    new-array v0, v0, [Lorg/apache/lucene/index/Term;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-interface {v11, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v24

    check-cast v24, [Lorg/apache/lucene/index/Term;

    move-object/from16 v0, v24

    invoke-virtual {v10, v0}, Lorg/apache/lucene/search/MultiPhraseQuery;->add([Lorg/apache/lucene/index/Term;)V

    goto :goto_e

    .line 631
    .end local v18    # "positionIncrement":I
    :cond_17
    const/16 v24, 0x0

    move/from16 v0, v24

    new-array v0, v0, [Lorg/apache/lucene/index/Term;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-interface {v11, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v24

    check-cast v24, [Lorg/apache/lucene/index/Term;

    move-object/from16 v0, v24

    invoke-virtual {v10, v0}, Lorg/apache/lucene/search/MultiPhraseQuery;->add([Lorg/apache/lucene/index/Term;)V

    goto/16 :goto_c

    .line 637
    .end local v9    # "i":I
    .end local v10    # "mpq":Lorg/apache/lucene/search/MultiPhraseQuery;
    .end local v11    # "multiTerms":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/Term;>;"
    .end local v16    # "position":I
    :cond_18
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->newPhraseQuery()Lorg/apache/lucene/search/PhraseQuery;

    move-result-object v19

    .line 638
    .local v19, "pq":Lorg/apache/lucene/search/PhraseQuery;
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->phraseSlop:I

    move/from16 v24, v0

    move-object/from16 v0, v19

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/PhraseQuery;->setSlop(I)V

    .line 639
    const/16 v16, -0x1

    .line 641
    .restart local v16    # "position":I
    const/4 v9, 0x0

    .restart local v9    # "i":I
    :goto_f
    if-ge v9, v12, :cond_3

    .line 642
    const/16 v18, 0x1

    .line 645
    .restart local v18    # "positionIncrement":I
    :try_start_a
    invoke-virtual {v3}, Lorg/apache/lucene/analysis/CachingTokenFilter;->incrementToken()Z

    move-result v8

    .line 646
    .restart local v8    # "hasNext":Z
    sget-boolean v24, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->$assertionsDisabled:Z

    if-nez v24, :cond_1a

    if-nez v8, :cond_1a

    new-instance v24, Ljava/lang/AssertionError;

    invoke-direct/range {v24 .. v24}, Ljava/lang/AssertionError;-><init>()V

    throw v24
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_5

    .line 651
    .end local v8    # "hasNext":Z
    :catch_5
    move-exception v24

    .line 655
    :cond_19
    :goto_10
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->enablePositionIncrements:Z

    move/from16 v24, v0

    if-eqz v24, :cond_1b

    .line 656
    add-int v16, v16, v18

    .line 657
    new-instance v24, Lorg/apache/lucene/index/Term;

    invoke-static {v4}, Lorg/apache/lucene/util/BytesRef;->deepCopyOf(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/BytesRef;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, p2

    move-object/from16 v2, v25

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    move-object/from16 v0, v19

    move-object/from16 v1, v24

    move/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/search/PhraseQuery;->add(Lorg/apache/lucene/index/Term;I)V

    .line 641
    :goto_11
    add-int/lit8 v9, v9, 0x1

    goto :goto_f

    .line 647
    .restart local v8    # "hasNext":Z
    :cond_1a
    :try_start_b
    invoke-interface/range {v23 .. v23}, Lorg/apache/lucene/analysis/tokenattributes/TermToBytesRefAttribute;->fillBytesRef()I

    .line 648
    if-eqz v15, :cond_19

    .line 649
    invoke-interface {v15}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->getPositionIncrement()I
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_5

    move-result v18

    goto :goto_10

    .line 659
    .end local v8    # "hasNext":Z
    :cond_1b
    new-instance v24, Lorg/apache/lucene/index/Term;

    invoke-static {v4}, Lorg/apache/lucene/util/BytesRef;->deepCopyOf(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/BytesRef;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, p2

    move-object/from16 v2, v25

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    move-object/from16 v0, v19

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/PhraseQuery;->add(Lorg/apache/lucene/index/Term;)V

    goto :goto_11

    .line 545
    .end local v4    # "bytes":Lorg/apache/lucene/util/BytesRef;
    .end local v9    # "i":I
    .end local v16    # "position":I
    .end local v18    # "positionIncrement":I
    .end local v19    # "pq":Lorg/apache/lucene/search/PhraseQuery;
    :catch_6
    move-exception v24

    goto/16 :goto_1
.end method

.method protected newFuzzyQuery(Lorg/apache/lucene/index/Term;FI)Lorg/apache/lucene/search/Query;
    .locals 4
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .param p2, "minimumSimilarity"    # F
    .param p3, "prefixLength"    # I

    .prologue
    .line 805
    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v1

    .line 807
    .local v1, "text":Ljava/lang/String;
    const/4 v2, 0x0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->codePointCount(II)I

    move-result v2

    .line 806
    invoke-static {p2, v2}, Lorg/apache/lucene/search/FuzzyQuery;->floatToEdits(FI)I

    move-result v0

    .line 808
    .local v0, "numEdits":I
    new-instance v2, Lorg/apache/lucene/search/FuzzyQuery;

    invoke-direct {v2, p1, v0, p3}, Lorg/apache/lucene/search/FuzzyQuery;-><init>(Lorg/apache/lucene/index/Term;II)V

    return-object v2
.end method

.method protected newMatchAllDocsQuery()Lorg/apache/lucene/search/Query;
    .locals 1

    .prologue
    .line 887
    new-instance v0, Lorg/apache/lucene/search/MatchAllDocsQuery;

    invoke-direct {v0}, Lorg/apache/lucene/search/MatchAllDocsQuery;-><init>()V

    return-object v0
.end method

.method protected newMultiPhraseQuery()Lorg/apache/lucene/search/MultiPhraseQuery;
    .locals 1

    .prologue
    .line 771
    new-instance v0, Lorg/apache/lucene/search/MultiPhraseQuery;

    invoke-direct {v0}, Lorg/apache/lucene/search/MultiPhraseQuery;-><init>()V

    return-object v0
.end method

.method protected newPhraseQuery()Lorg/apache/lucene/search/PhraseQuery;
    .locals 1

    .prologue
    .line 763
    new-instance v0, Lorg/apache/lucene/search/PhraseQuery;

    invoke-direct {v0}, Lorg/apache/lucene/search/PhraseQuery;-><init>()V

    return-object v0
.end method

.method protected newPrefixQuery(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/search/Query;
    .locals 2
    .param p1, "prefix"    # Lorg/apache/lucene/index/Term;

    .prologue
    .line 780
    new-instance v0, Lorg/apache/lucene/search/PrefixQuery;

    invoke-direct {v0, p1}, Lorg/apache/lucene/search/PrefixQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    .line 781
    .local v0, "query":Lorg/apache/lucene/search/PrefixQuery;
    iget-object v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->multiTermRewriteMethod:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/PrefixQuery;->setRewriteMethod(Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;)V

    .line 782
    return-object v0
.end method

.method protected newRangeQuery(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Lorg/apache/lucene/search/Query;
    .locals 6
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "part1"    # Ljava/lang/String;
    .param p3, "part2"    # Ljava/lang/String;
    .param p4, "startInclusive"    # Z
    .param p5, "endInclusive"    # Z

    .prologue
    .line 864
    if-nez p2, :cond_0

    .line 865
    const/4 v2, 0x0

    .line 870
    .local v2, "start":Lorg/apache/lucene/util/BytesRef;
    :goto_0
    if-nez p3, :cond_2

    .line 871
    const/4 v3, 0x0

    .line 876
    .local v3, "end":Lorg/apache/lucene/util/BytesRef;
    :goto_1
    new-instance v0, Lorg/apache/lucene/search/TermRangeQuery;

    move-object v1, p1

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/search/TermRangeQuery;-><init>(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;ZZ)V

    .line 878
    .local v0, "query":Lorg/apache/lucene/search/TermRangeQuery;
    iget-object v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->multiTermRewriteMethod:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/TermRangeQuery;->setRewriteMethod(Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;)V

    .line 879
    return-object v0

    .line 867
    .end local v0    # "query":Lorg/apache/lucene/search/TermRangeQuery;
    .end local v2    # "start":Lorg/apache/lucene/util/BytesRef;
    .end local v3    # "end":Lorg/apache/lucene/util/BytesRef;
    :cond_0
    iget-boolean v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->analyzeRangeTerms:Z

    if-eqz v1, :cond_1

    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->analyzeMultitermTerm(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/lucene/util/BytesRef;

    move-result-object v2

    .restart local v2    # "start":Lorg/apache/lucene/util/BytesRef;
    :goto_2
    goto :goto_0

    .end local v2    # "start":Lorg/apache/lucene/util/BytesRef;
    :cond_1
    new-instance v2, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v2, p2}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 873
    .restart local v2    # "start":Lorg/apache/lucene/util/BytesRef;
    :cond_2
    iget-boolean v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->analyzeRangeTerms:Z

    if-eqz v1, :cond_3

    invoke-direct {p0, p1, p3}, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->analyzeMultitermTerm(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/lucene/util/BytesRef;

    move-result-object v3

    .restart local v3    # "end":Lorg/apache/lucene/util/BytesRef;
    :goto_3
    goto :goto_1

    .end local v3    # "end":Lorg/apache/lucene/util/BytesRef;
    :cond_3
    new-instance v3, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v3, p3}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    goto :goto_3
.end method

.method protected newRegexpQuery(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/search/Query;
    .locals 2
    .param p1, "regexp"    # Lorg/apache/lucene/index/Term;

    .prologue
    .line 791
    new-instance v0, Lorg/apache/lucene/search/RegexpQuery;

    invoke-direct {v0, p1}, Lorg/apache/lucene/search/RegexpQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    .line 792
    .local v0, "query":Lorg/apache/lucene/search/RegexpQuery;
    iget-object v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->multiTermRewriteMethod:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/RegexpQuery;->setRewriteMethod(Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;)V

    .line 793
    return-object v0
.end method

.method protected newTermQuery(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/search/Query;
    .locals 1
    .param p1, "term"    # Lorg/apache/lucene/index/Term;

    .prologue
    .line 755
    new-instance v0, Lorg/apache/lucene/search/TermQuery;

    invoke-direct {v0, p1}, Lorg/apache/lucene/search/TermQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    return-object v0
.end method

.method protected newWildcardQuery(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/search/Query;
    .locals 2
    .param p1, "t"    # Lorg/apache/lucene/index/Term;

    .prologue
    .line 896
    new-instance v0, Lorg/apache/lucene/search/WildcardQuery;

    invoke-direct {v0, p1}, Lorg/apache/lucene/search/WildcardQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    .line 897
    .local v0, "query":Lorg/apache/lucene/search/WildcardQuery;
    iget-object v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->multiTermRewriteMethod:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/WildcardQuery;->setRewriteMethod(Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;)V

    .line 898
    return-object v0
.end method

.method public parse(Ljava/lang/String;)Lorg/apache/lucene/search/Query;
    .locals 6
    .param p1, "query"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/classic/ParseException;
        }
    .end annotation

    .prologue
    .line 118
    new-instance v4, Lorg/apache/lucene/queryparser/classic/FastCharStream;

    new-instance v5, Ljava/io/StringReader;

    invoke-direct {v5, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v4, v5}, Lorg/apache/lucene/queryparser/classic/FastCharStream;-><init>(Ljava/io/Reader;)V

    invoke-virtual {p0, v4}, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->ReInit(Lorg/apache/lucene/queryparser/classic/CharStream;)V

    .line 121
    :try_start_0
    iget-object v4, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->field:Ljava/lang/String;

    invoke-virtual {p0, v4}, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->TopLevelQuery(Ljava/lang/String;)Lorg/apache/lucene/search/Query;

    move-result-object v1

    .line 122
    .local v1, "res":Lorg/apache/lucene/search/Query;
    if-eqz v1, :cond_0

    .end local v1    # "res":Lorg/apache/lucene/search/Query;
    :goto_0
    return-object v1

    .restart local v1    # "res":Lorg/apache/lucene/search/Query;
    :cond_0
    const/4 v4, 0x0

    invoke-virtual {p0, v4}, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->newBooleanQuery(Z)Lorg/apache/lucene/search/BooleanQuery;
    :try_end_0
    .catch Lorg/apache/lucene/queryparser/classic/ParseException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/apache/lucene/queryparser/classic/TokenMgrError; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/apache/lucene/search/BooleanQuery$TooManyClauses; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v1

    goto :goto_0

    .line 124
    .end local v1    # "res":Lorg/apache/lucene/search/Query;
    :catch_0
    move-exception v3

    .line 126
    .local v3, "tme":Lorg/apache/lucene/queryparser/classic/ParseException;
    new-instance v0, Lorg/apache/lucene/queryparser/classic/ParseException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Cannot parse \'"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\': "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v3}, Lorg/apache/lucene/queryparser/classic/ParseException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4}, Lorg/apache/lucene/queryparser/classic/ParseException;-><init>(Ljava/lang/String;)V

    .line 127
    .local v0, "e":Lorg/apache/lucene/queryparser/classic/ParseException;
    invoke-virtual {v0, v3}, Lorg/apache/lucene/queryparser/classic/ParseException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 128
    throw v0

    .line 130
    .end local v0    # "e":Lorg/apache/lucene/queryparser/classic/ParseException;
    .end local v3    # "tme":Lorg/apache/lucene/queryparser/classic/ParseException;
    :catch_1
    move-exception v3

    .line 131
    .local v3, "tme":Lorg/apache/lucene/queryparser/classic/TokenMgrError;
    new-instance v0, Lorg/apache/lucene/queryparser/classic/ParseException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Cannot parse \'"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\': "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v3}, Lorg/apache/lucene/queryparser/classic/TokenMgrError;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4}, Lorg/apache/lucene/queryparser/classic/ParseException;-><init>(Ljava/lang/String;)V

    .line 132
    .restart local v0    # "e":Lorg/apache/lucene/queryparser/classic/ParseException;
    invoke-virtual {v0, v3}, Lorg/apache/lucene/queryparser/classic/ParseException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 133
    throw v0

    .line 135
    .end local v0    # "e":Lorg/apache/lucene/queryparser/classic/ParseException;
    .end local v3    # "tme":Lorg/apache/lucene/queryparser/classic/TokenMgrError;
    :catch_2
    move-exception v2

    .line 136
    .local v2, "tmc":Lorg/apache/lucene/search/BooleanQuery$TooManyClauses;
    new-instance v0, Lorg/apache/lucene/queryparser/classic/ParseException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Cannot parse \'"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\': too many boolean clauses"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4}, Lorg/apache/lucene/queryparser/classic/ParseException;-><init>(Ljava/lang/String;)V

    .line 137
    .restart local v0    # "e":Lorg/apache/lucene/queryparser/classic/ParseException;
    invoke-virtual {v0, v2}, Lorg/apache/lucene/queryparser/classic/ParseException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 138
    throw v0
.end method

.method public setAllowLeadingWildcard(Z)V
    .locals 0
    .param p1, "allowLeadingWildcard"    # Z

    .prologue
    .line 243
    iput-boolean p1, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->allowLeadingWildcard:Z

    .line 244
    return-void
.end method

.method public setAnalyzeRangeTerms(Z)V
    .locals 0
    .param p1, "analyzeRangeTerms"    # Z

    .prologue
    .line 431
    iput-boolean p1, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->analyzeRangeTerms:Z

    .line 432
    return-void
.end method

.method public final setAutoGeneratePhraseQueries(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 175
    iput-boolean p1, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->autoGeneratePhraseQueries:Z

    .line 176
    return-void
.end method

.method public setDateResolution(Ljava/lang/String;Lorg/apache/lucene/document/DateTools$Resolution;)V
    .locals 2
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "dateResolution"    # Lorg/apache/lucene/document/DateTools$Resolution;

    .prologue
    .line 386
    if-nez p1, :cond_0

    .line 387
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Field cannot be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 390
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->fieldToDateResolution:Ljava/util/Map;

    if-nez v0, :cond_1

    .line 392
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->fieldToDateResolution:Ljava/util/Map;

    .line 395
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->fieldToDateResolution:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 396
    return-void
.end method

.method public setDateResolution(Lorg/apache/lucene/document/DateTools$Resolution;)V
    .locals 0
    .param p1, "dateResolution"    # Lorg/apache/lucene/document/DateTools$Resolution;

    .prologue
    .line 376
    iput-object p1, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->dateResolution:Lorg/apache/lucene/document/DateTools$Resolution;

    .line 377
    return-void
.end method

.method public setDefaultOperator(Lorg/apache/lucene/queryparser/classic/QueryParser$Operator;)V
    .locals 0
    .param p1, "op"    # Lorg/apache/lucene/queryparser/classic/QueryParser$Operator;

    .prologue
    .line 286
    iput-object p1, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->operator:Lorg/apache/lucene/queryparser/classic/QueryParser$Operator;

    .line 287
    return-void
.end method

.method public setEnablePositionIncrements(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 266
    iput-boolean p1, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->enablePositionIncrements:Z

    .line 267
    return-void
.end method

.method public setFuzzyMinSim(F)V
    .locals 0
    .param p1, "fuzzyMinSim"    # F

    .prologue
    .line 192
    iput p1, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->fuzzyMinSim:F

    .line 193
    return-void
.end method

.method public setFuzzyPrefixLength(I)V
    .locals 0
    .param p1, "fuzzyPrefixLength"    # I

    .prologue
    .line 210
    iput p1, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->fuzzyPrefixLength:I

    .line 211
    return-void
.end method

.method public setLocale(Ljava/util/Locale;)V
    .locals 0
    .param p1, "locale"    # Ljava/util/Locale;

    .prologue
    .line 346
    iput-object p1, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->locale:Ljava/util/Locale;

    .line 347
    return-void
.end method

.method public setLowercaseExpandedTerms(Z)V
    .locals 0
    .param p1, "lowercaseExpandedTerms"    # Z

    .prologue
    .line 305
    iput-boolean p1, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->lowercaseExpandedTerms:Z

    .line 306
    return-void
.end method

.method public setMultiTermRewriteMethod(Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;)V
    .locals 0
    .param p1, "method"    # Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    .prologue
    .line 328
    iput-object p1, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->multiTermRewriteMethod:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    .line 329
    return-void
.end method

.method public setPhraseSlop(I)V
    .locals 0
    .param p1, "phraseSlop"    # I

    .prologue
    .line 219
    iput p1, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->phraseSlop:I

    .line 220
    return-void
.end method

.method public setTimeZone(Ljava/util/TimeZone;)V
    .locals 0
    .param p1, "timeZone"    # Ljava/util/TimeZone;

    .prologue
    .line 359
    iput-object p1, p0, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->timeZone:Ljava/util/TimeZone;

    .line 360
    return-void
.end method
