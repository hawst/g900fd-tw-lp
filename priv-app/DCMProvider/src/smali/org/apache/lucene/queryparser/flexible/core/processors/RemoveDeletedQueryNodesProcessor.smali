.class public Lorg/apache/lucene/queryparser/flexible/core/processors/RemoveDeletedQueryNodesProcessor;
.super Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl;
.source "RemoveDeletedQueryNodesProcessor.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl;-><init>()V

    .line 38
    return-void
.end method


# virtual methods
.method protected postProcessNode(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 4
    .param p1, "node"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 58
    invoke-interface {p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->isLeaf()Z

    move-result v3

    if-nez v3, :cond_2

    .line 59
    invoke-interface {p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->getChildren()Ljava/util/List;

    move-result-object v0

    .line 60
    .local v0, "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    const/4 v2, 0x0

    .line 62
    .local v2, "removeBoolean":Z
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_3

    .line 63
    :cond_0
    const/4 v2, 0x1

    .line 80
    :cond_1
    :goto_0
    if-eqz v2, :cond_2

    .line 81
    new-instance p1, Lorg/apache/lucene/queryparser/flexible/core/nodes/DeletedQueryNode;

    .end local p1    # "node":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    invoke-direct {p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/DeletedQueryNode;-><init>()V

    .line 86
    .end local v0    # "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    .end local v2    # "removeBoolean":Z
    :cond_2
    return-object p1

    .line 66
    .restart local v0    # "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    .restart local v2    # "removeBoolean":Z
    .restart local p1    # "node":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    :cond_3
    const/4 v2, 0x1

    .line 68
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 70
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    instance-of v3, v3, Lorg/apache/lucene/queryparser/flexible/core/nodes/DeletedQueryNode;

    if-nez v3, :cond_4

    .line 71
    const/4 v2, 0x0

    .line 72
    goto :goto_0
.end method

.method protected preProcessNode(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 0
    .param p1, "node"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 109
    return-object p1
.end method

.method public process(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 1
    .param p1, "queryTree"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 42
    invoke-super {p0, p1}, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl;->process(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object p1

    .line 44
    instance-of v0, p1, Lorg/apache/lucene/queryparser/flexible/core/nodes/DeletedQueryNode;

    if-eqz v0, :cond_0

    .line 45
    instance-of v0, p1, Lorg/apache/lucene/queryparser/flexible/core/nodes/MatchNoDocsQueryNode;

    if-nez v0, :cond_0

    .line 47
    new-instance p1, Lorg/apache/lucene/queryparser/flexible/core/nodes/MatchNoDocsQueryNode;

    .end local p1    # "queryTree":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    invoke-direct {p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/MatchNoDocsQueryNode;-><init>()V

    .line 51
    :cond_0
    return-object p1
.end method

.method protected setChildrenOrder(Ljava/util/List;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 94
    .local p1, "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-lt v0, v2, :cond_0

    .line 102
    return-object p1

    .line 96
    :cond_0
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Lorg/apache/lucene/queryparser/flexible/core/nodes/DeletedQueryNode;

    if-eqz v2, :cond_1

    .line 97
    add-int/lit8 v1, v0, -0x1

    .end local v0    # "i":I
    .local v1, "i":I
    invoke-interface {p1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move v0, v1

    .line 94
    .end local v1    # "i":I
    .restart local v0    # "i":I
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
