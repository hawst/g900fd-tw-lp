.class public interface abstract Lorg/apache/lucene/queryparser/flexible/standard/CommonQueryParserConfiguration;
.super Ljava/lang/Object;
.source "CommonQueryParserConfiguration.java"


# virtual methods
.method public abstract getAllowLeadingWildcard()Z
.end method

.method public abstract getAnalyzer()Lorg/apache/lucene/analysis/Analyzer;
.end method

.method public abstract getEnablePositionIncrements()Z
.end method

.method public abstract getFuzzyMinSim()F
.end method

.method public abstract getFuzzyPrefixLength()I
.end method

.method public abstract getLocale()Ljava/util/Locale;
.end method

.method public abstract getLowercaseExpandedTerms()Z
.end method

.method public abstract getMultiTermRewriteMethod()Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;
.end method

.method public abstract getPhraseSlop()I
.end method

.method public abstract getTimeZone()Ljava/util/TimeZone;
.end method

.method public abstract setAllowLeadingWildcard(Z)V
.end method

.method public abstract setDateResolution(Lorg/apache/lucene/document/DateTools$Resolution;)V
.end method

.method public abstract setEnablePositionIncrements(Z)V
.end method

.method public abstract setFuzzyMinSim(F)V
.end method

.method public abstract setFuzzyPrefixLength(I)V
.end method

.method public abstract setLocale(Ljava/util/Locale;)V
.end method

.method public abstract setLowercaseExpandedTerms(Z)V
.end method

.method public abstract setMultiTermRewriteMethod(Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;)V
.end method

.method public abstract setPhraseSlop(I)V
.end method

.method public abstract setTimeZone(Ljava/util/TimeZone;)V
.end method
