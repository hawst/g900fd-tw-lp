.class public Lorg/apache/lucene/queryparser/xml/builders/ConstantScoreQueryBuilder;
.super Ljava/lang/Object;
.source "ConstantScoreQueryBuilder.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/xml/QueryBuilder;


# instance fields
.field private final filterFactory:Lorg/apache/lucene/queryparser/xml/FilterBuilderFactory;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/queryparser/xml/FilterBuilderFactory;)V
    .locals 0
    .param p1, "filterFactory"    # Lorg/apache/lucene/queryparser/xml/FilterBuilderFactory;

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lorg/apache/lucene/queryparser/xml/builders/ConstantScoreQueryBuilder;->filterFactory:Lorg/apache/lucene/queryparser/xml/FilterBuilderFactory;

    .line 36
    return-void
.end method


# virtual methods
.method public getQuery(Lorg/w3c/dom/Element;)Lorg/apache/lucene/search/Query;
    .locals 4
    .param p1, "e"    # Lorg/w3c/dom/Element;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/xml/ParserException;
        }
    .end annotation

    .prologue
    .line 40
    invoke-static {p1}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getFirstChildOrFail(Lorg/w3c/dom/Element;)Lorg/w3c/dom/Element;

    move-result-object v0

    .line 42
    .local v0, "filterElem":Lorg/w3c/dom/Element;
    new-instance v1, Lorg/apache/lucene/search/ConstantScoreQuery;

    iget-object v2, p0, Lorg/apache/lucene/queryparser/xml/builders/ConstantScoreQueryBuilder;->filterFactory:Lorg/apache/lucene/queryparser/xml/FilterBuilderFactory;

    invoke-virtual {v2, v0}, Lorg/apache/lucene/queryparser/xml/FilterBuilderFactory;->getFilter(Lorg/w3c/dom/Element;)Lorg/apache/lucene/search/Filter;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/lucene/search/ConstantScoreQuery;-><init>(Lorg/apache/lucene/search/Filter;)V

    .line 43
    .local v1, "q":Lorg/apache/lucene/search/Query;
    const-string v2, "boost"

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-static {p1, v2, v3}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttribute(Lorg/w3c/dom/Element;Ljava/lang/String;F)F

    move-result v2

    invoke-virtual {v1, v2}, Lorg/apache/lucene/search/Query;->setBoost(F)V

    .line 44
    return-object v1
.end method
