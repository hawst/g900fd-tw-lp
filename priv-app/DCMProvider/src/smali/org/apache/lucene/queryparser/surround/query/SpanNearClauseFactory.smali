.class public Lorg/apache/lucene/queryparser/surround/query/SpanNearClauseFactory;
.super Ljava/lang/Object;
.source "SpanNearClauseFactory.java"


# instance fields
.field private fieldName:Ljava/lang/String;

.field private qf:Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;

.field private reader:Lorg/apache/lucene/index/IndexReader;

.field private weightBySpanQuery:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lorg/apache/lucene/search/spans/SpanQuery;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;)V
    .locals 1
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "fieldName"    # Ljava/lang/String;
    .param p3, "qf"    # Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    iput-object p1, p0, Lorg/apache/lucene/queryparser/surround/query/SpanNearClauseFactory;->reader:Lorg/apache/lucene/index/IndexReader;

    .line 73
    iput-object p2, p0, Lorg/apache/lucene/queryparser/surround/query/SpanNearClauseFactory;->fieldName:Ljava/lang/String;

    .line 74
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/queryparser/surround/query/SpanNearClauseFactory;->weightBySpanQuery:Ljava/util/HashMap;

    .line 75
    iput-object p3, p0, Lorg/apache/lucene/queryparser/surround/query/SpanNearClauseFactory;->qf:Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;

    .line 76
    return-void
.end method


# virtual methods
.method public addSpanQuery(Lorg/apache/lucene/search/Query;)V
    .locals 3
    .param p1, "q"    # Lorg/apache/lucene/search/Query;

    .prologue
    .line 108
    sget-object v0, Lorg/apache/lucene/queryparser/surround/query/SrndQuery;->theEmptyLcnQuery:Lorg/apache/lucene/search/Query;

    if-ne p1, v0, :cond_0

    .line 113
    :goto_0
    return-void

    .line 110
    :cond_0
    instance-of v0, p1, Lorg/apache/lucene/search/spans/SpanQuery;

    if-nez v0, :cond_1

    .line 111
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Expected SpanQuery: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/query/SpanNearClauseFactory;->getFieldName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lorg/apache/lucene/search/Query;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :cond_1
    move-object v0, p1

    .line 112
    check-cast v0, Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {p1}, Lorg/apache/lucene/search/Query;->getBoost()F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/queryparser/surround/query/SpanNearClauseFactory;->addSpanQueryWeighted(Lorg/apache/lucene/search/spans/SpanQuery;F)V

    goto :goto_0
.end method

.method protected addSpanQueryWeighted(Lorg/apache/lucene/search/spans/SpanQuery;F)V
    .locals 2
    .param p1, "sq"    # Lorg/apache/lucene/search/spans/SpanQuery;
    .param p2, "weight"    # F

    .prologue
    .line 93
    iget-object v1, p0, Lorg/apache/lucene/queryparser/surround/query/SpanNearClauseFactory;->weightBySpanQuery:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    .line 94
    .local v0, "w":Ljava/lang/Float;
    if-eqz v0, :cond_0

    .line 95
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v1

    add-float/2addr v1, p2

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    .line 98
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/queryparser/surround/query/SpanNearClauseFactory;->weightBySpanQuery:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    return-void

    .line 97
    :cond_0
    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    goto :goto_0
.end method

.method public addTermWeighted(Lorg/apache/lucene/index/Term;F)V
    .locals 2
    .param p1, "t"    # Lorg/apache/lucene/index/Term;
    .param p2, "weight"    # F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 102
    iget-object v1, p0, Lorg/apache/lucene/queryparser/surround/query/SpanNearClauseFactory;->qf:Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;->newSpanTermQuery(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/search/spans/SpanTermQuery;

    move-result-object v0

    .line 104
    .local v0, "stq":Lorg/apache/lucene/search/spans/SpanTermQuery;
    invoke-virtual {p0, v0, p2}, Lorg/apache/lucene/queryparser/surround/query/SpanNearClauseFactory;->addSpanQueryWeighted(Lorg/apache/lucene/search/spans/SpanQuery;F)V

    .line 105
    return-void
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lorg/apache/lucene/queryparser/surround/query/SpanNearClauseFactory;->weightBySpanQuery:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    return-void
.end method

.method public getBasicQueryFactory()Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lorg/apache/lucene/queryparser/surround/query/SpanNearClauseFactory;->qf:Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;

    return-object v0
.end method

.method public getFieldName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lorg/apache/lucene/queryparser/surround/query/SpanNearClauseFactory;->fieldName:Ljava/lang/String;

    return-object v0
.end method

.method public getIndexReader()Lorg/apache/lucene/index/IndexReader;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lorg/apache/lucene/queryparser/surround/query/SpanNearClauseFactory;->reader:Lorg/apache/lucene/index/IndexReader;

    return-object v0
.end method

.method public makeSpanClause()Lorg/apache/lucene/search/spans/SpanQuery;
    .locals 7

    .prologue
    .line 116
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/query/SpanNearClauseFactory;->size()I

    move-result v5

    new-array v2, v5, [Lorg/apache/lucene/search/spans/SpanQuery;

    .line 117
    .local v2, "spanQueries":[Lorg/apache/lucene/search/spans/SpanQuery;
    iget-object v5, p0, Lorg/apache/lucene/queryparser/surround/query/SpanNearClauseFactory;->weightBySpanQuery:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 118
    .local v4, "sqi":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/search/spans/SpanQuery;>;"
    const/4 v0, 0x0

    .line 119
    .local v0, "i":I
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_0

    .line 125
    array-length v5, v2

    const/4 v6, 0x1

    if-ne v5, v6, :cond_1

    .line 126
    const/4 v5, 0x0

    aget-object v5, v2, v5

    .line 128
    :goto_1
    return-object v5

    .line 120
    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/search/spans/SpanQuery;

    .line 121
    .local v3, "sq":Lorg/apache/lucene/search/spans/SpanQuery;
    iget-object v5, p0, Lorg/apache/lucene/queryparser/surround/query/SpanNearClauseFactory;->weightBySpanQuery:Ljava/util/HashMap;

    invoke-virtual {v5, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    invoke-virtual {v3, v5}, Lorg/apache/lucene/search/spans/SpanQuery;->setBoost(F)V

    .line 122
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .local v1, "i":I
    aput-object v3, v2, v0

    move v0, v1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    goto :goto_0

    .line 128
    .end local v3    # "sq":Lorg/apache/lucene/search/spans/SpanQuery;
    :cond_1
    new-instance v5, Lorg/apache/lucene/search/spans/SpanOrQuery;

    invoke-direct {v5, v2}, Lorg/apache/lucene/search/spans/SpanOrQuery;-><init>([Lorg/apache/lucene/search/spans/SpanQuery;)V

    goto :goto_1
.end method

.method public size()I
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lorg/apache/lucene/queryparser/surround/query/SpanNearClauseFactory;->weightBySpanQuery:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    return v0
.end method
