.class public Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;
.super Ljava/lang/Object;
.source "QueryBuilderFactory.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/xml/QueryBuilder;


# instance fields
.field builders:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/queryparser/xml/QueryBuilder;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;->builders:Ljava/util/HashMap;

    .line 30
    return-void
.end method


# virtual methods
.method public addBuilder(Ljava/lang/String;Lorg/apache/lucene/queryparser/xml/QueryBuilder;)V
    .locals 1
    .param p1, "nodeName"    # Ljava/lang/String;
    .param p2, "builder"    # Lorg/apache/lucene/queryparser/xml/QueryBuilder;

    .prologue
    .line 44
    iget-object v0, p0, Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;->builders:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    return-void
.end method

.method public getQuery(Lorg/w3c/dom/Element;)Lorg/apache/lucene/search/Query;
    .locals 4
    .param p1, "n"    # Lorg/w3c/dom/Element;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/xml/ParserException;
        }
    .end annotation

    .prologue
    .line 36
    iget-object v1, p0, Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;->builders:Ljava/util/HashMap;

    invoke-interface {p1}, Lorg/w3c/dom/Element;->getNodeName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/xml/QueryBuilder;

    .line 37
    .local v0, "builder":Lorg/apache/lucene/queryparser/xml/QueryBuilder;
    if-nez v0, :cond_0

    .line 38
    new-instance v1, Lorg/apache/lucene/queryparser/xml/ParserException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No QueryObjectBuilder defined for node "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lorg/w3c/dom/Element;->getNodeName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/lucene/queryparser/xml/ParserException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 40
    :cond_0
    invoke-interface {v0, p1}, Lorg/apache/lucene/queryparser/xml/QueryBuilder;->getQuery(Lorg/w3c/dom/Element;)Lorg/apache/lucene/search/Query;

    move-result-object v1

    return-object v1
.end method

.method public getQueryBuilder(Ljava/lang/String;)Lorg/apache/lucene/queryparser/xml/QueryBuilder;
    .locals 1
    .param p1, "nodeName"    # Ljava/lang/String;

    .prologue
    .line 48
    iget-object v0, p0, Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;->builders:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/xml/QueryBuilder;

    return-object v0
.end method
