.class public Lorg/apache/lucene/queryparser/flexible/core/QueryNodeParseException;
.super Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
.source "QueryNodeParseException.java"


# instance fields
.field private beginColumn:I

.field private beginLine:I

.field private errorToken:Ljava/lang/String;

.field private query:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "throwable"    # Ljava/lang/Throwable;

    .prologue
    const/4 v0, -0x1

    .line 49
    invoke-direct {p0, p1}, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;-><init>(Ljava/lang/Throwable;)V

    .line 38
    iput v0, p0, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeParseException;->beginColumn:I

    .line 40
    iput v0, p0, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeParseException;->beginLine:I

    .line 42
    const-string v0, ""

    iput-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeParseException;->errorToken:Ljava/lang/String;

    .line 50
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/queryparser/flexible/messages/Message;)V
    .locals 1
    .param p1, "message"    # Lorg/apache/lucene/queryparser/flexible/messages/Message;

    .prologue
    const/4 v0, -0x1

    .line 45
    invoke-direct {p0, p1}, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;-><init>(Lorg/apache/lucene/queryparser/flexible/messages/Message;)V

    .line 38
    iput v0, p0, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeParseException;->beginColumn:I

    .line 40
    iput v0, p0, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeParseException;->beginLine:I

    .line 42
    const-string v0, ""

    iput-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeParseException;->errorToken:Ljava/lang/String;

    .line 46
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/queryparser/flexible/messages/Message;Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "message"    # Lorg/apache/lucene/queryparser/flexible/messages/Message;
    .param p2, "throwable"    # Ljava/lang/Throwable;

    .prologue
    const/4 v0, -0x1

    .line 53
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;-><init>(Lorg/apache/lucene/queryparser/flexible/messages/Message;Ljava/lang/Throwable;)V

    .line 38
    iput v0, p0, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeParseException;->beginColumn:I

    .line 40
    iput v0, p0, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeParseException;->beginLine:I

    .line 42
    const-string v0, ""

    iput-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeParseException;->errorToken:Ljava/lang/String;

    .line 54
    return-void
.end method


# virtual methods
.method public getBeginColumn()I
    .locals 1

    .prologue
    .line 101
    iget v0, p0, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeParseException;->beginColumn:I

    return v0
.end method

.method public getBeginLine()I
    .locals 1

    .prologue
    .line 90
    iget v0, p0, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeParseException;->beginLine:I

    return v0
.end method

.method public getErrorToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeParseException;->errorToken:Ljava/lang/String;

    return-object v0
.end method

.method public getQuery()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeParseException;->query:Ljava/lang/CharSequence;

    return-object v0
.end method

.method protected setBeginColumn(I)V
    .locals 0
    .param p1, "beginColumn"    # I

    .prologue
    .line 117
    iput p1, p0, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeParseException;->beginColumn:I

    .line 118
    return-void
.end method

.method protected setBeginLine(I)V
    .locals 0
    .param p1, "beginLine"    # I

    .prologue
    .line 109
    iput p1, p0, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeParseException;->beginLine:I

    .line 110
    return-void
.end method

.method protected setErrorToken(Ljava/lang/String;)V
    .locals 0
    .param p1, "errorToken"    # Ljava/lang/String;

    .prologue
    .line 71
    iput-object p1, p0, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeParseException;->errorToken:Ljava/lang/String;

    .line 72
    return-void
.end method

.method public setNonLocalizedMessage(Lorg/apache/lucene/queryparser/flexible/messages/Message;)V
    .locals 0
    .param p1, "message"    # Lorg/apache/lucene/queryparser/flexible/messages/Message;

    .prologue
    .line 79
    iput-object p1, p0, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeParseException;->message:Lorg/apache/lucene/queryparser/flexible/messages/Message;

    .line 80
    return-void
.end method

.method public setQuery(Ljava/lang/CharSequence;)V
    .locals 5
    .param p1, "query"    # Ljava/lang/CharSequence;

    .prologue
    .line 57
    iput-object p1, p0, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeParseException;->query:Ljava/lang/CharSequence;

    .line 58
    new-instance v0, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;

    .line 59
    sget-object v1, Lorg/apache/lucene/queryparser/flexible/core/messages/QueryParserMessages;->INVALID_SYNTAX_CANNOT_PARSE:Ljava/lang/String;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    const-string v4, ""

    aput-object v4, v2, v3

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 58
    iput-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeParseException;->message:Lorg/apache/lucene/queryparser/flexible/messages/Message;

    .line 60
    return-void
.end method
