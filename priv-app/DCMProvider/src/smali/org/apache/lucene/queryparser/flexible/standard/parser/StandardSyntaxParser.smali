.class public Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;
.super Ljava/lang/Object;
.source "StandardSyntaxParser.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/flexible/core/parser/SyntaxParser;
.implements Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserConstants;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$JJCalls;,
        Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$LookaheadSuccess;
    }
.end annotation


# static fields
.field private static final CONJ_AND:I = 0x2

.field private static final CONJ_NONE:I = 0x0

.field private static final CONJ_OR:I = 0x2

.field private static jj_la1_0:[I

.field private static jj_la1_1:[I


# instance fields
.field private final jj_2_rtns:[Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$JJCalls;

.field private jj_endpos:I

.field private jj_expentries:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<[I>;"
        }
    .end annotation
.end field

.field private jj_expentry:[I

.field private jj_gc:I

.field private jj_gen:I

.field private jj_kind:I

.field private jj_la:I

.field private final jj_la1:[I

.field private jj_lastpos:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

.field private jj_lasttokens:[I

.field private final jj_ls:Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$LookaheadSuccess;

.field public jj_nt:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

.field private jj_ntk:I

.field private jj_rescan:Z

.field private jj_scanpos:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

.field public token:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

.field public token_source:Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .prologue
    .line 862
    invoke-static {}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_la1_init_0()V

    .line 863
    invoke-static {}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_la1_init_1()V

    .line 864
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 55
    new-instance v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/FastCharStream;

    new-instance v1, Ljava/io/StringReader;

    const-string v2, ""

    invoke-direct {v1, v2}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/FastCharStream;-><init>(Ljava/io/Reader;)V

    invoke-direct {p0, v0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;-><init>(Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;)V

    .line 56
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;)V
    .locals 6
    .param p1, "stream"    # Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;

    .prologue
    const/16 v5, 0x1c

    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 876
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 858
    new-array v1, v5, [I

    iput-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_la1:[I

    .line 871
    const/4 v1, 0x2

    new-array v1, v1, [Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$JJCalls;

    iput-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_2_rtns:[Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$JJCalls;

    .line 872
    iput-boolean v4, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_rescan:Z

    .line 873
    iput v4, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_gc:I

    .line 940
    new-instance v1, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$LookaheadSuccess;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$LookaheadSuccess;-><init>(Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$LookaheadSuccess;)V

    iput-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ls:Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$LookaheadSuccess;

    .line 989
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_expentries:Ljava/util/List;

    .line 991
    iput v3, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_kind:I

    .line 992
    const/16 v1, 0x64

    new-array v1, v1, [I

    iput-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_lasttokens:[I

    .line 877
    new-instance v1, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;

    invoke-direct {v1, p1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;-><init>(Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;)V

    iput-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->token_source:Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;

    .line 878
    new-instance v1, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    invoke-direct {v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->token:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 879
    iput v3, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    .line 880
    iput v4, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_gen:I

    .line 881
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v5, :cond_0

    .line 882
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_2_rtns:[Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$JJCalls;

    array-length v1, v1

    if-lt v0, v1, :cond_1

    .line 883
    return-void

    .line 881
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_la1:[I

    aput v3, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 882
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_2_rtns:[Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$JJCalls;

    new-instance v2, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$JJCalls;

    invoke-direct {v2}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$JJCalls;-><init>()V

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public constructor <init>(Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;)V
    .locals 6
    .param p1, "tm"    # Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;

    .prologue
    const/16 v5, 0x1c

    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 896
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 858
    new-array v1, v5, [I

    iput-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_la1:[I

    .line 871
    const/4 v1, 0x2

    new-array v1, v1, [Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$JJCalls;

    iput-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_2_rtns:[Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$JJCalls;

    .line 872
    iput-boolean v4, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_rescan:Z

    .line 873
    iput v4, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_gc:I

    .line 940
    new-instance v1, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$LookaheadSuccess;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$LookaheadSuccess;-><init>(Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$LookaheadSuccess;)V

    iput-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ls:Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$LookaheadSuccess;

    .line 989
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_expentries:Ljava/util/List;

    .line 991
    iput v3, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_kind:I

    .line 992
    const/16 v1, 0x64

    new-array v1, v1, [I

    iput-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_lasttokens:[I

    .line 897
    iput-object p1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->token_source:Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;

    .line 898
    new-instance v1, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    invoke-direct {v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->token:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 899
    iput v3, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    .line 900
    iput v4, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_gen:I

    .line 901
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v5, :cond_0

    .line 902
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_2_rtns:[Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$JJCalls;

    array-length v1, v1

    if-lt v0, v1, :cond_1

    .line 903
    return-void

    .line 901
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_la1:[I

    aput v3, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 902
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_2_rtns:[Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$JJCalls;

    new-instance v2, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$JJCalls;

    invoke-direct {v2}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$JJCalls;-><init>()V

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private jj_2_1(I)Z
    .locals 4
    .param p1, "xla"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 722
    iput p1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_la:I

    iget-object v3, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->token:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    iput-object v3, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_scanpos:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    iput-object v3, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_lastpos:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 723
    :try_start_0
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_3_1()Z
    :try_end_0
    .catch Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$LookaheadSuccess; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-eqz v3, :cond_0

    move v1, v2

    .line 725
    :cond_0
    invoke-direct {p0, v2, p1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_save(II)V

    .line 724
    :goto_0
    return v1

    :catch_0
    move-exception v0

    .line 725
    .local v0, "ls":Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$LookaheadSuccess;
    invoke-direct {p0, v2, p1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_save(II)V

    goto :goto_0

    .end local v0    # "ls":Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$LookaheadSuccess;
    :catchall_0
    move-exception v1

    invoke-direct {p0, v2, p1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_save(II)V

    throw v1
.end method

.method private jj_2_2(I)Z
    .locals 3
    .param p1, "xla"    # I

    .prologue
    const/4 v2, 0x1

    .line 729
    iput p1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_la:I

    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->token:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    iput-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_scanpos:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    iput-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_lastpos:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 730
    :try_start_0
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_3_2()Z
    :try_end_0
    .catch Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$LookaheadSuccess; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    .line 732
    :goto_0
    invoke-direct {p0, v2, p1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_save(II)V

    .line 731
    :goto_1
    return v1

    :cond_0
    move v1, v2

    .line 730
    goto :goto_0

    .line 731
    :catch_0
    move-exception v0

    .line 732
    .local v0, "ls":Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$LookaheadSuccess;
    invoke-direct {p0, v2, p1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_save(II)V

    move v1, v2

    .line 731
    goto :goto_1

    .line 732
    .end local v0    # "ls":Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$LookaheadSuccess;
    :catchall_0
    move-exception v1

    invoke-direct {p0, v2, p1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_save(II)V

    throw v1
.end method

.method private jj_3R_10()Z
    .locals 1

    .prologue
    .line 778
    const/16 v0, 0x17

    invoke-direct {p0, v0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_scan_token(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 779
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private jj_3R_11()Z
    .locals 1

    .prologue
    .line 752
    const/16 v0, 0x19

    invoke-direct {p0, v0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_scan_token(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 753
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private jj_3R_12()Z
    .locals 1

    .prologue
    .line 747
    const/16 v0, 0x1a

    invoke-direct {p0, v0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_scan_token(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 748
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private jj_3R_4()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 826
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_scanpos:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 827
    .local v0, "xsp":Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    const/16 v2, 0xf

    invoke-direct {p0, v2}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_scan_token(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 828
    iput-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_scanpos:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 829
    const/16 v2, 0x10

    invoke-direct {p0, v2}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_scan_token(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 832
    :cond_0
    :goto_0
    return v1

    .line 831
    :cond_1
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_3R_6()Z

    move-result v2

    if-nez v2, :cond_0

    .line 832
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private jj_3R_5()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 802
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_scanpos:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 803
    .local v0, "xsp":Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    const/16 v2, 0x11

    invoke-direct {p0, v2}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_scan_token(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 804
    iput-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_scanpos:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 805
    const/16 v2, 0x12

    invoke-direct {p0, v2}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_scan_token(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 806
    iput-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_scanpos:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 807
    const/16 v2, 0x13

    invoke-direct {p0, v2}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_scan_token(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 808
    iput-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_scanpos:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 809
    const/16 v2, 0x14

    invoke-direct {p0, v2}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_scan_token(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 821
    :cond_0
    :goto_0
    return v1

    .line 813
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_scanpos:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 814
    const/16 v2, 0x17

    invoke-direct {p0, v2}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_scan_token(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 815
    iput-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_scanpos:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 816
    const/16 v2, 0x16

    invoke-direct {p0, v2}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_scan_token(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 817
    iput-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_scanpos:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 818
    const/16 v2, 0x1c

    invoke-direct {p0, v2}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_scan_token(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 821
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private jj_3R_6()Z
    .locals 2

    .prologue
    .line 837
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_scanpos:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 838
    .local v0, "xsp":Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_3R_7()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 839
    iput-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_scanpos:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 840
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_3R_8()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 841
    iput-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_scanpos:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 842
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_3R_9()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    .line 845
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private jj_3R_7()Z
    .locals 2

    .prologue
    .line 784
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_scanpos:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 785
    .local v0, "xsp":Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_3R_10()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 786
    iput-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_scanpos:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 787
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_3R_11()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 788
    iput-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_scanpos:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 789
    const/16 v1, 0x1c

    invoke-direct {p0, v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_scan_token(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    .line 792
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private jj_3R_8()Z
    .locals 2

    .prologue
    .line 769
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_scanpos:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 770
    .local v0, "xsp":Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_3R_12()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 771
    iput-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_scanpos:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 772
    const/16 v1, 0x1b

    invoke-direct {p0, v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_scan_token(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    .line 774
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private jj_3R_9()Z
    .locals 1

    .prologue
    .line 796
    const/16 v0, 0x16

    invoke-direct {p0, v0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_scan_token(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 797
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private jj_3_1()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 757
    const/16 v2, 0x17

    invoke-direct {p0, v2}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_scan_token(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 764
    :cond_0
    :goto_0
    return v1

    .line 759
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_scanpos:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 760
    .local v0, "xsp":Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    const/16 v2, 0xf

    invoke-direct {p0, v2}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_scan_token(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 761
    iput-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_scanpos:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 762
    const/16 v2, 0x10

    invoke-direct {p0, v2}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_scan_token(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 764
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private jj_3_2()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 736
    const/16 v2, 0x17

    invoke-direct {p0, v2}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_scan_token(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 743
    :cond_0
    :goto_0
    return v1

    .line 738
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_scanpos:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 739
    .local v0, "xsp":Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_3R_4()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 740
    iput-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_scanpos:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 741
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_3R_5()Z

    move-result v2

    if-nez v2, :cond_0

    .line 743
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private jj_add_error_token(II)V
    .locals 6
    .param p1, "kind"    # I
    .param p2, "pos"    # I

    .prologue
    .line 996
    const/16 v3, 0x64

    if-lt p2, v3, :cond_1

    .line 1018
    :cond_0
    :goto_0
    return-void

    .line 997
    :cond_1
    iget v3, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_endpos:I

    add-int/lit8 v3, v3, 0x1

    if-ne p2, v3, :cond_2

    .line 998
    iget-object v3, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_lasttokens:[I

    iget v4, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_endpos:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_endpos:I

    aput p1, v3, v4

    goto :goto_0

    .line 999
    :cond_2
    iget v3, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_endpos:I

    if-eqz v3, :cond_0

    .line 1000
    iget v3, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_endpos:I

    new-array v3, v3, [I

    iput-object v3, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_expentry:[I

    .line 1001
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget v3, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_endpos:I

    if-lt v0, v3, :cond_4

    .line 1004
    iget-object v3, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_expentries:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<*>;"
    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_5

    .line 1016
    :goto_2
    if-eqz p2, :cond_0

    iget-object v3, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_lasttokens:[I

    iput p2, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_endpos:I

    add-int/lit8 v4, p2, -0x1

    aput p1, v3, v4

    goto :goto_0

    .line 1002
    .end local v1    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<*>;"
    :cond_4
    iget-object v3, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_expentry:[I

    iget-object v4, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_lasttokens:[I

    aget v4, v4, v0

    aput v4, v3, v0

    .line 1001
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1005
    .restart local v1    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<*>;"
    :cond_5
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [I

    .line 1006
    .local v2, "oldentry":[I
    array-length v3, v2

    iget-object v4, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_expentry:[I

    array-length v4, v4

    if-ne v3, v4, :cond_3

    .line 1007
    const/4 v0, 0x0

    :goto_3
    iget-object v3, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_expentry:[I

    array-length v3, v3

    if-lt v0, v3, :cond_6

    .line 1012
    iget-object v3, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_expentries:Ljava/util/List;

    iget-object v4, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_expentry:[I

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1008
    :cond_6
    aget v3, v2, v0

    iget-object v4, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_expentry:[I

    aget v4, v4, v0

    if-ne v3, v4, :cond_3

    .line 1007
    add-int/lit8 v0, v0, 0x1

    goto :goto_3
.end method

.method private jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    .locals 5
    .param p1, "kind"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 917
    iget-object v2, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->token:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .local v2, "oldToken":Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    iget-object v3, v2, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->next:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->token:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    iget-object v3, v3, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->next:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    iput-object v3, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->token:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 919
    :goto_0
    const/4 v3, -0x1

    iput v3, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    .line 920
    iget-object v3, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->token:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    iget v3, v3, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->kind:I

    if-ne v3, p1, :cond_5

    .line 921
    iget v3, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_gen:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_gen:I

    .line 922
    iget v3, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_gc:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_gc:I

    const/16 v4, 0x64

    if-le v3, v4, :cond_0

    .line 923
    const/4 v3, 0x0

    iput v3, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_gc:I

    .line 924
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v3, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_2_rtns:[Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$JJCalls;

    array-length v3, v3

    if-lt v1, v3, :cond_2

    .line 932
    .end local v1    # "i":I
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->token:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    return-object v3

    .line 918
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->token:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    iget-object v4, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->token_source:Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;

    invoke-virtual {v4}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->getNextToken()Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    move-result-object v4

    iput-object v4, v3, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->next:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    iput-object v4, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->token:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    goto :goto_0

    .line 925
    .restart local v1    # "i":I
    :cond_2
    iget-object v3, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_2_rtns:[Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$JJCalls;

    aget-object v0, v3, v1

    .line 926
    .local v0, "c":Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$JJCalls;
    :goto_2
    if-nez v0, :cond_3

    .line 924
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 927
    :cond_3
    iget v3, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$JJCalls;->gen:I

    iget v4, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_gen:I

    if-ge v3, v4, :cond_4

    const/4 v3, 0x0

    iput-object v3, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$JJCalls;->first:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 928
    :cond_4
    iget-object v0, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$JJCalls;->next:Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$JJCalls;

    goto :goto_2

    .line 934
    .end local v0    # "c":Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$JJCalls;
    .end local v1    # "i":I
    :cond_5
    iput-object v2, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->token:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 935
    iput p1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_kind:I

    .line 936
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->generateParseException()Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException;

    move-result-object v3

    throw v3
.end method

.method private static jj_la1_init_0()V
    .locals 7

    .prologue
    const v6, 0x18000

    const/16 v5, 0x1c00

    const/16 v2, 0x300

    const/high16 v4, 0x1000000

    const/high16 v3, 0x200000

    .line 866
    const/16 v0, 0x1c

    new-array v0, v0, [I

    const/4 v1, 0x0

    aput v2, v0, v1

    const/4 v1, 0x1

    aput v2, v0, v1

    const/4 v1, 0x2

    aput v5, v0, v1

    const/4 v1, 0x3

    aput v5, v0, v1

    const/4 v1, 0x4

    const v2, 0x1ec03c00

    aput v2, v0, v1

    const/4 v1, 0x5

    const/16 v2, 0x200

    aput v2, v0, v1

    const/4 v1, 0x6

    const/16 v2, 0x100

    aput v2, v0, v1

    const/4 v1, 0x7

    aput v6, v0, v1

    const/16 v1, 0x8

    const/high16 v2, 0x1e0000

    aput v2, v0, v1

    const/16 v1, 0x9

    const/high16 v2, 0x10c00000

    aput v2, v0, v1

    const/16 v1, 0xa

    const v2, 0x1f8000

    aput v2, v0, v1

    const/16 v1, 0xb

    aput v6, v0, v1

    const/16 v1, 0xc

    aput v3, v0, v1

    const/16 v1, 0xd

    const v2, 0x1ec02000

    aput v2, v0, v1

    const/16 v1, 0xe

    const v2, 0x1ec02000

    aput v2, v0, v1

    const/16 v1, 0xf

    const/high16 v2, 0x12800000

    aput v2, v0, v1

    const/16 v1, 0x10

    aput v4, v0, v1

    const/16 v1, 0x11

    aput v4, v0, v1

    const/16 v1, 0x12

    aput v3, v0, v1

    const/16 v1, 0x13

    const/high16 v2, 0xc000000

    aput v2, v0, v1

    const/16 v1, 0x15

    const/high16 v2, 0x20000000

    aput v2, v0, v1

    const/16 v1, 0x17

    const/high16 v2, -0x40000000    # -2.0f

    aput v2, v0, v1

    const/16 v1, 0x18

    aput v3, v0, v1

    const/16 v1, 0x19

    aput v4, v0, v1

    const/16 v1, 0x1a

    aput v3, v0, v1

    const/16 v1, 0x1b

    const/high16 v2, 0x1ec00000

    aput v2, v0, v1

    sput-object v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_la1_0:[I

    .line 867
    return-void
.end method

.method private static jj_la1_init_1()V
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 869
    const/16 v0, 0x1c

    new-array v0, v0, [I

    const/16 v1, 0x14

    aput v2, v0, v1

    const/16 v1, 0x16

    aput v2, v0, v1

    sput-object v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_la1_1:[I

    .line 870
    return-void
.end method

.method private jj_ntk()I
    .locals 2

    .prologue
    .line 983
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->token:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    iget-object v0, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->next:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    iput-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_nt:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    if-nez v0, :cond_0

    .line 984
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->token:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->token_source:Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;

    invoke-virtual {v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->getNextToken()Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    move-result-object v1

    iput-object v1, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->next:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    iget v0, v1, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->kind:I

    iput v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    .line 986
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_nt:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    iget v0, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->kind:I

    iput v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    goto :goto_0
.end method

.method private jj_rescan_token()V
    .locals 4

    .prologue
    .line 1066
    const/4 v2, 0x1

    iput-boolean v2, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_rescan:Z

    .line 1067
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v2, 0x2

    if-lt v0, v2, :cond_0

    .line 1082
    const/4 v2, 0x0

    iput-boolean v2, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_rescan:Z

    .line 1083
    return-void

    .line 1069
    :cond_0
    :try_start_0
    iget-object v2, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_2_rtns:[Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$JJCalls;

    aget-object v1, v2, v0

    .line 1071
    .local v1, "p":Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$JJCalls;
    :cond_1
    iget v2, v1, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$JJCalls;->gen:I

    iget v3, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_gen:I

    if-le v2, v3, :cond_2

    .line 1072
    iget v2, v1, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$JJCalls;->arg:I

    iput v2, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_la:I

    iget-object v2, v1, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$JJCalls;->first:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    iput-object v2, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_scanpos:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    iput-object v2, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_lastpos:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 1073
    packed-switch v0, :pswitch_data_0

    .line 1078
    :cond_2
    :goto_1
    iget-object v1, v1, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$JJCalls;->next:Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$JJCalls;

    .line 1079
    if-nez v1, :cond_1

    .line 1067
    .end local v1    # "p":Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$JJCalls;
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1074
    .restart local v1    # "p":Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$JJCalls;
    :pswitch_0
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_3_1()Z

    goto :goto_1

    .line 1080
    .end local v1    # "p":Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$JJCalls;
    :catch_0
    move-exception v2

    goto :goto_2

    .line 1075
    .restart local v1    # "p":Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$JJCalls;
    :pswitch_1
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_3_2()Z
    :try_end_0
    .catch Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$LookaheadSuccess; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1073
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private jj_save(II)V
    .locals 4
    .param p1, "index"    # I
    .param p2, "xla"    # I

    .prologue
    .line 1086
    iget-object v2, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_2_rtns:[Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$JJCalls;

    aget-object v0, v2, p1

    .line 1087
    .local v0, "p":Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$JJCalls;
    :goto_0
    iget v2, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$JJCalls;->gen:I

    iget v3, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_gen:I

    if-gt v2, v3, :cond_0

    .line 1091
    :goto_1
    iget v2, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_gen:I

    add-int/2addr v2, p2

    iget v3, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_la:I

    sub-int/2addr v2, v3

    iput v2, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$JJCalls;->gen:I

    iget-object v2, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->token:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    iput-object v2, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$JJCalls;->first:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    iput p2, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$JJCalls;->arg:I

    .line 1092
    return-void

    .line 1088
    :cond_0
    iget-object v2, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$JJCalls;->next:Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$JJCalls;

    if-nez v2, :cond_1

    new-instance v1, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$JJCalls;

    invoke-direct {v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$JJCalls;-><init>()V

    iput-object v1, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$JJCalls;->next:Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$JJCalls;

    .end local v0    # "p":Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$JJCalls;
    .local v1, "p":Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$JJCalls;
    move-object v0, v1

    .end local v1    # "p":Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$JJCalls;
    .restart local v0    # "p":Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$JJCalls;
    goto :goto_1

    .line 1089
    :cond_1
    iget-object v0, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$JJCalls;->next:Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$JJCalls;

    goto :goto_0
.end method

.method private jj_scan_token(I)Z
    .locals 4
    .param p1, "kind"    # I

    .prologue
    .line 942
    iget-object v2, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_scanpos:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    iget-object v3, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_lastpos:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    if-ne v2, v3, :cond_3

    .line 943
    iget v2, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_la:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_la:I

    .line 944
    iget-object v2, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_scanpos:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    iget-object v2, v2, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->next:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    if-nez v2, :cond_2

    .line 945
    iget-object v2, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_scanpos:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    iget-object v3, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->token_source:Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;

    invoke-virtual {v3}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->getNextToken()Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    move-result-object v3

    iput-object v3, v2, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->next:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    iput-object v3, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_scanpos:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    iput-object v3, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_lastpos:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 952
    :goto_0
    iget-boolean v2, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_rescan:Z

    if-eqz v2, :cond_1

    .line 953
    const/4 v0, 0x0

    .local v0, "i":I
    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->token:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 954
    .local v1, "tok":Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    :goto_1
    if-eqz v1, :cond_0

    iget-object v2, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_scanpos:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    if-ne v1, v2, :cond_4

    .line 955
    :cond_0
    if-eqz v1, :cond_1

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_add_error_token(II)V

    .line 957
    .end local v0    # "i":I
    .end local v1    # "tok":Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_scanpos:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    iget v2, v2, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->kind:I

    if-eq v2, p1, :cond_5

    const/4 v2, 0x1

    .line 959
    :goto_2
    return v2

    .line 947
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_scanpos:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    iget-object v2, v2, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->next:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    iput-object v2, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_scanpos:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    iput-object v2, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_lastpos:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    goto :goto_0

    .line 950
    :cond_3
    iget-object v2, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_scanpos:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    iget-object v2, v2, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->next:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    iput-object v2, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_scanpos:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    goto :goto_0

    .line 954
    .restart local v0    # "i":I
    .restart local v1    # "tok":Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    :cond_4
    add-int/lit8 v0, v0, 0x1

    iget-object v1, v1, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->next:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    goto :goto_1

    .line 958
    .end local v0    # "i":I
    .end local v1    # "tok":Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    :cond_5
    iget v2, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_la:I

    if-nez v2, :cond_6

    iget-object v2, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_scanpos:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    iget-object v3, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_lastpos:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    if-ne v2, v3, :cond_6

    iget-object v2, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ls:Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$LookaheadSuccess;

    throw v2

    .line 959
    :cond_6
    const/4 v2, 0x0

    goto :goto_2
.end method


# virtual methods
.method public final Clause(Ljava/lang/CharSequence;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 16
    .param p1, "field"    # Ljava/lang/CharSequence;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 313
    const/4 v3, 0x0

    .local v3, "fieldToken":Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    const/4 v1, 0x0

    .local v1, "boost":Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    const/4 v6, 0x0

    .local v6, "operator":Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    const/4 v11, 0x0

    .line 317
    .local v11, "term":Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    const/4 v4, 0x0

    .line 318
    .local v4, "group":Z
    const/4 v13, 0x3

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_2_2(I)Z

    move-result v13

    if-eqz v13, :cond_6

    .line 319
    const/16 v13, 0x17

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    move-result-object v3

    .line 320
    move-object/from16 v0, p0

    iget v13, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    const/4 v14, -0x1

    if-ne v13, v14, :cond_0

    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk()I

    move-result v13

    :goto_0
    packed-switch v13, :pswitch_data_0

    .line 423
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_la1:[I

    const/16 v14, 0xa

    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_gen:I

    aput v15, v13, v14

    .line 424
    const/4 v13, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 425
    new-instance v13, Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException;

    invoke-direct {v13}, Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException;-><init>()V

    throw v13

    .line 320
    :cond_0
    move-object/from16 v0, p0

    iget v13, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    goto :goto_0

    .line 323
    :pswitch_0
    move-object/from16 v0, p0

    iget v13, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    const/4 v14, -0x1

    if-ne v13, v14, :cond_1

    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk()I

    move-result v13

    :goto_1
    packed-switch v13, :pswitch_data_1

    .line 331
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_la1:[I

    const/4 v14, 0x7

    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_gen:I

    aput v15, v13, v14

    .line 332
    const/4 v13, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 333
    new-instance v13, Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException;

    invoke-direct {v13}, Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException;-><init>()V

    throw v13

    .line 323
    :cond_1
    move-object/from16 v0, p0

    iget v13, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    goto :goto_1

    .line 325
    :pswitch_1
    const/16 v13, 0xf

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 335
    :goto_2
    iget-object v13, v3, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->image:Ljava/lang/String;

    invoke-static {v13}, Lorg/apache/lucene/queryparser/flexible/standard/parser/EscapeQuerySyntaxImpl;->discardEscapeChar(Ljava/lang/CharSequence;)Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;

    move-result-object p1

    .line 336
    invoke-virtual/range {p0 .. p1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->Term(Ljava/lang/CharSequence;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v7

    .local v7, "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    move-object v8, v7

    .line 490
    .end local v7    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .local v8, "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    :goto_3
    if-eqz v1, :cond_2

    .line 491
    const/high16 v2, 0x3f800000    # 1.0f

    .line 493
    .local v2, "f":F
    :try_start_0
    iget-object v13, v1, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->image:Ljava/lang/String;

    invoke-static {v13}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/Float;->floatValue()F

    move-result v2

    .line 495
    if-eqz v8, :cond_2

    .line 496
    new-instance v7, Lorg/apache/lucene/queryparser/flexible/core/nodes/BoostQueryNode;

    invoke-direct {v7, v8, v2}, Lorg/apache/lucene/queryparser/flexible/core/nodes/BoostQueryNode;-><init>(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;F)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .end local v8    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .restart local v7    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    move-object v8, v7

    .line 504
    .end local v2    # "f":F
    .end local v7    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .restart local v8    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    :cond_2
    :goto_4
    if-eqz v4, :cond_c

    new-instance v7, Lorg/apache/lucene/queryparser/flexible/core/nodes/GroupQueryNode;

    invoke-direct {v7, v8}, Lorg/apache/lucene/queryparser/flexible/core/nodes/GroupQueryNode;-><init>(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)V

    .line 505
    .end local v8    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .restart local v7    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    :goto_5
    return-object v7

    .line 328
    .end local v7    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    :pswitch_2
    const/16 v13, 0x10

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    goto :goto_2

    .line 342
    :pswitch_3
    move-object/from16 v0, p0

    iget v13, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    const/4 v14, -0x1

    if-ne v13, v14, :cond_3

    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk()I

    move-result v13

    :goto_6
    packed-switch v13, :pswitch_data_2

    .line 356
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_la1:[I

    const/16 v14, 0x8

    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_gen:I

    aput v15, v13, v14

    .line 357
    const/4 v13, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 358
    new-instance v13, Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException;

    invoke-direct {v13}, Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException;-><init>()V

    throw v13

    .line 342
    :cond_3
    move-object/from16 v0, p0

    iget v13, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    goto :goto_6

    .line 344
    :pswitch_4
    const/16 v13, 0x11

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    move-result-object v6

    .line 360
    :goto_7
    iget-object v13, v3, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->image:Ljava/lang/String;

    invoke-static {v13}, Lorg/apache/lucene/queryparser/flexible/standard/parser/EscapeQuerySyntaxImpl;->discardEscapeChar(Ljava/lang/CharSequence;)Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;

    move-result-object p1

    .line 361
    move-object/from16 v0, p0

    iget v13, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    const/4 v14, -0x1

    if-ne v13, v14, :cond_4

    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk()I

    move-result v13

    :goto_8
    sparse-switch v13, :sswitch_data_0

    .line 372
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_la1:[I

    const/16 v14, 0x9

    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_gen:I

    aput v15, v13, v14

    .line 373
    const/4 v13, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 374
    new-instance v13, Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException;

    invoke-direct {v13}, Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException;-><init>()V

    throw v13

    .line 347
    :pswitch_5
    const/16 v13, 0x12

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    move-result-object v6

    .line 348
    goto :goto_7

    .line 350
    :pswitch_6
    const/16 v13, 0x13

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    move-result-object v6

    .line 351
    goto :goto_7

    .line 353
    :pswitch_7
    const/16 v13, 0x14

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    move-result-object v6

    .line 354
    goto :goto_7

    .line 361
    :cond_4
    move-object/from16 v0, p0

    iget v13, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    goto :goto_8

    .line 363
    :sswitch_0
    const/16 v13, 0x17

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    move-result-object v11

    .line 376
    :goto_9
    iget v13, v11, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->kind:I

    const/16 v14, 0x16

    if-ne v13, v14, :cond_5

    .line 377
    iget-object v13, v11, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->image:Ljava/lang/String;

    const/4 v14, 0x1

    iget-object v15, v11, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->image:Ljava/lang/String;

    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v15

    add-int/lit8 v15, v15, -0x1

    invoke-virtual {v13, v14, v15}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    iput-object v13, v11, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->image:Ljava/lang/String;

    .line 379
    :cond_5
    iget v13, v6, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->kind:I

    packed-switch v13, :pswitch_data_3

    .line 418
    new-instance v13, Ljava/lang/Error;

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "Unhandled case: operator="

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    throw v13

    .line 366
    :sswitch_1
    const/16 v13, 0x16

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    move-result-object v11

    .line 367
    goto :goto_9

    .line 369
    :sswitch_2
    const/16 v13, 0x1c

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    move-result-object v11

    .line 370
    goto :goto_9

    .line 381
    :pswitch_8
    const/4 v5, 0x1

    .line 382
    .local v5, "lowerInclusive":Z
    const/4 v12, 0x0

    .line 384
    .local v12, "upperInclusive":Z
    new-instance v9, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;

    .line 385
    const-string v13, "*"

    iget v14, v11, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->beginColumn:I

    iget v15, v11, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->endColumn:I

    .line 384
    move-object/from16 v0, p1

    invoke-direct {v9, v0, v13, v14, v15}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;II)V

    .line 386
    .local v9, "qLower":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    new-instance v10, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;

    .line 387
    iget-object v13, v11, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->image:Ljava/lang/String;

    invoke-static {v13}, Lorg/apache/lucene/queryparser/flexible/standard/parser/EscapeQuerySyntaxImpl;->discardEscapeChar(Ljava/lang/CharSequence;)Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;

    move-result-object v13

    iget v14, v11, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->beginColumn:I

    iget v15, v11, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->endColumn:I

    .line 386
    move-object/from16 v0, p1

    invoke-direct {v10, v0, v13, v14, v15}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;II)V

    .line 420
    .local v10, "qUpper":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    :goto_a
    new-instance v7, Lorg/apache/lucene/queryparser/flexible/standard/nodes/TermRangeQueryNode;

    invoke-direct {v7, v9, v10, v5, v12}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/TermRangeQueryNode;-><init>(Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;ZZ)V

    .restart local v7    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    move-object v8, v7

    .line 421
    .end local v7    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .restart local v8    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    goto/16 :goto_3

    .line 391
    .end local v5    # "lowerInclusive":Z
    .end local v8    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .end local v9    # "qLower":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    .end local v10    # "qUpper":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    .end local v12    # "upperInclusive":Z
    :pswitch_9
    const/4 v5, 0x1

    .line 392
    .restart local v5    # "lowerInclusive":Z
    const/4 v12, 0x1

    .line 394
    .restart local v12    # "upperInclusive":Z
    new-instance v9, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;

    .line 395
    const-string v13, "*"

    iget v14, v11, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->beginColumn:I

    iget v15, v11, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->endColumn:I

    .line 394
    move-object/from16 v0, p1

    invoke-direct {v9, v0, v13, v14, v15}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;II)V

    .line 396
    .restart local v9    # "qLower":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    new-instance v10, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;

    .line 397
    iget-object v13, v11, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->image:Ljava/lang/String;

    invoke-static {v13}, Lorg/apache/lucene/queryparser/flexible/standard/parser/EscapeQuerySyntaxImpl;->discardEscapeChar(Ljava/lang/CharSequence;)Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;

    move-result-object v13

    iget v14, v11, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->beginColumn:I

    iget v15, v11, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->endColumn:I

    .line 396
    move-object/from16 v0, p1

    invoke-direct {v10, v0, v13, v14, v15}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;II)V

    .line 398
    .restart local v10    # "qUpper":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    goto :goto_a

    .line 400
    .end local v5    # "lowerInclusive":Z
    .end local v9    # "qLower":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    .end local v10    # "qUpper":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    .end local v12    # "upperInclusive":Z
    :pswitch_a
    const/4 v5, 0x0

    .line 401
    .restart local v5    # "lowerInclusive":Z
    const/4 v12, 0x1

    .line 403
    .restart local v12    # "upperInclusive":Z
    new-instance v9, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;

    .line 404
    iget-object v13, v11, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->image:Ljava/lang/String;

    invoke-static {v13}, Lorg/apache/lucene/queryparser/flexible/standard/parser/EscapeQuerySyntaxImpl;->discardEscapeChar(Ljava/lang/CharSequence;)Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;

    move-result-object v13

    iget v14, v11, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->beginColumn:I

    iget v15, v11, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->endColumn:I

    .line 403
    move-object/from16 v0, p1

    invoke-direct {v9, v0, v13, v14, v15}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;II)V

    .line 405
    .restart local v9    # "qLower":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    new-instance v10, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;

    .line 406
    const-string v13, "*"

    iget v14, v11, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->beginColumn:I

    iget v15, v11, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->endColumn:I

    .line 405
    move-object/from16 v0, p1

    invoke-direct {v10, v0, v13, v14, v15}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;II)V

    .line 407
    .restart local v10    # "qUpper":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    goto :goto_a

    .line 409
    .end local v5    # "lowerInclusive":Z
    .end local v9    # "qLower":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    .end local v10    # "qUpper":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    .end local v12    # "upperInclusive":Z
    :pswitch_b
    const/4 v5, 0x1

    .line 410
    .restart local v5    # "lowerInclusive":Z
    const/4 v12, 0x1

    .line 412
    .restart local v12    # "upperInclusive":Z
    new-instance v9, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;

    .line 413
    iget-object v13, v11, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->image:Ljava/lang/String;

    invoke-static {v13}, Lorg/apache/lucene/queryparser/flexible/standard/parser/EscapeQuerySyntaxImpl;->discardEscapeChar(Ljava/lang/CharSequence;)Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;

    move-result-object v13

    iget v14, v11, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->beginColumn:I

    iget v15, v11, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->endColumn:I

    .line 412
    move-object/from16 v0, p1

    invoke-direct {v9, v0, v13, v14, v15}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;II)V

    .line 414
    .restart local v9    # "qLower":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    new-instance v10, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;

    .line 415
    const-string v13, "*"

    iget v14, v11, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->beginColumn:I

    iget v15, v11, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->endColumn:I

    .line 414
    move-object/from16 v0, p1

    invoke-direct {v10, v0, v13, v14, v15}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;II)V

    .line 416
    .restart local v10    # "qUpper":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    goto :goto_a

    .line 428
    .end local v5    # "lowerInclusive":Z
    .end local v9    # "qLower":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    .end local v10    # "qUpper":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    .end local v12    # "upperInclusive":Z
    :cond_6
    move-object/from16 v0, p0

    iget v13, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    const/4 v14, -0x1

    if-ne v13, v14, :cond_7

    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk()I

    move-result v13

    :goto_b
    packed-switch v13, :pswitch_data_4

    .line 485
    :pswitch_c
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_la1:[I

    const/16 v14, 0xe

    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_gen:I

    aput v15, v13, v14

    .line 486
    const/4 v13, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 487
    new-instance v13, Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException;

    invoke-direct {v13}, Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException;-><init>()V

    throw v13

    .line 428
    :cond_7
    move-object/from16 v0, p0

    iget v13, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    goto :goto_b

    .line 436
    :pswitch_d
    const/4 v13, 0x2

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_2_1(I)Z

    move-result v13

    if-eqz v13, :cond_9

    .line 437
    const/16 v13, 0x17

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    move-result-object v3

    .line 438
    move-object/from16 v0, p0

    iget v13, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    const/4 v14, -0x1

    if-ne v13, v14, :cond_8

    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk()I

    move-result v13

    :goto_c
    packed-switch v13, :pswitch_data_5

    .line 446
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_la1:[I

    const/16 v14, 0xb

    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_gen:I

    aput v15, v13, v14

    .line 447
    const/4 v13, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 448
    new-instance v13, Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException;

    invoke-direct {v13}, Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException;-><init>()V

    throw v13

    .line 438
    :cond_8
    move-object/from16 v0, p0

    iget v13, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    goto :goto_c

    .line 440
    :pswitch_e
    const/16 v13, 0xf

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 450
    :goto_d
    iget-object v13, v3, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->image:Ljava/lang/String;

    invoke-static {v13}, Lorg/apache/lucene/queryparser/flexible/standard/parser/EscapeQuerySyntaxImpl;->discardEscapeChar(Ljava/lang/CharSequence;)Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;

    move-result-object p1

    .line 454
    :cond_9
    move-object/from16 v0, p0

    iget v13, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    const/4 v14, -0x1

    if-ne v13, v14, :cond_a

    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk()I

    move-result v13

    :goto_e
    packed-switch v13, :pswitch_data_6

    .line 479
    :pswitch_f
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_la1:[I

    const/16 v14, 0xd

    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_gen:I

    aput v15, v13, v14

    .line 480
    const/4 v13, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 481
    new-instance v13, Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException;

    invoke-direct {v13}, Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException;-><init>()V

    throw v13

    .line 443
    :pswitch_10
    const/16 v13, 0x10

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    goto :goto_d

    .line 454
    :cond_a
    move-object/from16 v0, p0

    iget v13, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    goto :goto_e

    .line 461
    :pswitch_11
    invoke-virtual/range {p0 .. p1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->Term(Ljava/lang/CharSequence;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v7

    .restart local v7    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    move-object v8, v7

    .line 462
    .end local v7    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .restart local v8    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    goto/16 :goto_3

    .line 464
    .end local v8    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    :pswitch_12
    const/16 v13, 0xd

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 465
    invoke-virtual/range {p0 .. p1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->Query(Ljava/lang/CharSequence;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v7

    .line 466
    .restart local v7    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    const/16 v13, 0xe

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 467
    move-object/from16 v0, p0

    iget v13, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    const/4 v14, -0x1

    if-ne v13, v14, :cond_b

    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk()I

    move-result v13

    :goto_f
    packed-switch v13, :pswitch_data_7

    .line 473
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_la1:[I

    const/16 v14, 0xc

    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_gen:I

    aput v15, v13, v14

    .line 476
    :goto_10
    const/4 v4, 0x1

    move-object v8, v7

    .line 477
    .end local v7    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .restart local v8    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    goto/16 :goto_3

    .line 467
    .end local v8    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .restart local v7    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    :cond_b
    move-object/from16 v0, p0

    iget v13, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    goto :goto_f

    .line 469
    :pswitch_13
    const/16 v13, 0x15

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 470
    const/16 v13, 0x1c

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    move-result-object v1

    .line 471
    goto :goto_10

    .line 498
    .end local v7    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .restart local v2    # "f":F
    .restart local v8    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    :catch_0
    move-exception v13

    goto/16 :goto_4

    .end local v2    # "f":F
    :cond_c
    move-object v7, v8

    .end local v8    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .restart local v7    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    goto/16 :goto_5

    .line 320
    nop

    :pswitch_data_0
    .packed-switch 0xf
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch

    .line 323
    :pswitch_data_1
    .packed-switch 0xf
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 342
    :pswitch_data_2
    .packed-switch 0x11
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    .line 361
    :sswitch_data_0
    .sparse-switch
        0x16 -> :sswitch_1
        0x17 -> :sswitch_0
        0x1c -> :sswitch_2
    .end sparse-switch

    .line 379
    :pswitch_data_3
    .packed-switch 0x11
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch

    .line 428
    :pswitch_data_4
    .packed-switch 0xd
        :pswitch_d
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_d
        :pswitch_d
        :pswitch_c
        :pswitch_d
        :pswitch_d
        :pswitch_d
        :pswitch_d
    .end packed-switch

    .line 438
    :pswitch_data_5
    .packed-switch 0xf
        :pswitch_e
        :pswitch_10
    .end packed-switch

    .line 454
    :pswitch_data_6
    .packed-switch 0xd
        :pswitch_12
        :pswitch_f
        :pswitch_f
        :pswitch_f
        :pswitch_f
        :pswitch_f
        :pswitch_f
        :pswitch_f
        :pswitch_f
        :pswitch_11
        :pswitch_11
        :pswitch_f
        :pswitch_11
        :pswitch_11
        :pswitch_11
        :pswitch_11
    .end packed-switch

    .line 467
    :pswitch_data_7
    .packed-switch 0x15
        :pswitch_13
    .end packed-switch
.end method

.method public final ConjQuery(Ljava/lang/CharSequence;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 6
    .param p1, "field"    # Ljava/lang/CharSequence;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 233
    const/4 v1, 0x0

    .line 234
    .local v1, "clauses":Ljava/util/Vector;, "Ljava/util/Vector<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->ModClause(Ljava/lang/CharSequence;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v2

    .line 237
    .local v2, "first":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    :goto_0
    iget v3, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_1

    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk()I

    move-result v3

    :goto_1
    packed-switch v3, :pswitch_data_0

    .line 242
    iget-object v3, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_la1:[I

    const/4 v4, 0x6

    iget v5, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_gen:I

    aput v5, v3, v4

    .line 253
    if-eqz v1, :cond_0

    .line 254
    new-instance v2, Lorg/apache/lucene/queryparser/flexible/core/nodes/AndQueryNode;

    .end local v2    # "first":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    invoke-direct {v2, v1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/AndQueryNode;-><init>(Ljava/util/List;)V

    .line 256
    :cond_0
    return-object v2

    .line 237
    .restart local v2    # "first":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    :cond_1
    iget v3, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    goto :goto_1

    .line 245
    :pswitch_0
    const/16 v3, 0x8

    invoke-direct {p0, v3}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 246
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->ModClause(Ljava/lang/CharSequence;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v0

    .line 247
    .local v0, "c":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    if-nez v1, :cond_2

    .line 248
    new-instance v1, Ljava/util/Vector;

    .end local v1    # "clauses":Ljava/util/Vector;, "Ljava/util/Vector<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    .line 249
    .restart local v1    # "clauses":Ljava/util/Vector;, "Ljava/util/Vector<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    invoke-virtual {v1, v2}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 251
    :cond_2
    invoke-virtual {v1, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_0

    .line 237
    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_0
    .end packed-switch
.end method

.method public final Conjunction()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException;
        }
    .end annotation

    .prologue
    const/4 v4, -0x1

    .line 84
    const/4 v0, 0x0

    .line 85
    .local v0, "ret":I
    iget v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    if-ne v1, v4, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk()I

    move-result v1

    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 104
    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_la1:[I

    const/4 v2, 0x1

    iget v3, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_gen:I

    aput v3, v1, v2

    .line 107
    :goto_1
    return v0

    .line 85
    :cond_0
    iget v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    goto :goto_0

    .line 88
    :pswitch_0
    iget v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    if-ne v1, v4, :cond_1

    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk()I

    move-result v1

    :goto_2
    packed-switch v1, :pswitch_data_1

    .line 98
    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_la1:[I

    const/4 v2, 0x0

    iget v3, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_gen:I

    aput v3, v1, v2

    .line 99
    invoke-direct {p0, v4}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 100
    new-instance v1, Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException;

    invoke-direct {v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException;-><init>()V

    throw v1

    .line 88
    :cond_1
    iget v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    goto :goto_2

    .line 90
    :pswitch_1
    const/16 v1, 0x8

    invoke-direct {p0, v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 91
    const/4 v0, 0x2

    .line 92
    goto :goto_1

    .line 94
    :pswitch_2
    const/16 v1, 0x9

    invoke-direct {p0, v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 95
    const/4 v0, 0x2

    .line 96
    goto :goto_1

    .line 85
    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 88
    :pswitch_data_1
    .packed-switch 0x8
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final DisjQuery(Ljava/lang/CharSequence;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 6
    .param p1, "field"    # Ljava/lang/CharSequence;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 203
    const/4 v1, 0x0

    .line 204
    .local v1, "clauses":Ljava/util/Vector;, "Ljava/util/Vector<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->ConjQuery(Ljava/lang/CharSequence;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v2

    .line 207
    .local v2, "first":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    :goto_0
    iget v3, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_1

    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk()I

    move-result v3

    :goto_1
    packed-switch v3, :pswitch_data_0

    .line 212
    iget-object v3, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_la1:[I

    const/4 v4, 0x5

    iget v5, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_gen:I

    aput v5, v3, v4

    .line 223
    if-eqz v1, :cond_0

    .line 224
    new-instance v2, Lorg/apache/lucene/queryparser/flexible/core/nodes/OrQueryNode;

    .end local v2    # "first":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    invoke-direct {v2, v1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/OrQueryNode;-><init>(Ljava/util/List;)V

    .line 226
    :cond_0
    return-object v2

    .line 207
    .restart local v2    # "first":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    :cond_1
    iget v3, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    goto :goto_1

    .line 215
    :pswitch_0
    const/16 v3, 0x9

    invoke-direct {p0, v3}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 216
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->ConjQuery(Ljava/lang/CharSequence;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v0

    .line 217
    .local v0, "c":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    if-nez v1, :cond_2

    .line 218
    new-instance v1, Ljava/util/Vector;

    .end local v1    # "clauses":Ljava/util/Vector;, "Ljava/util/Vector<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    .line 219
    .restart local v1    # "clauses":Ljava/util/Vector;, "Ljava/util/Vector<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    invoke-virtual {v1, v2}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 221
    :cond_2
    invoke-virtual {v1, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_0

    .line 207
    :pswitch_data_0
    .packed-switch 0x9
        :pswitch_0
    .end packed-switch
.end method

.method public final ModClause(Ljava/lang/CharSequence;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 4
    .param p1, "field"    # Ljava/lang/CharSequence;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 302
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->Modifiers()Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    move-result-object v0

    .line 303
    .local v0, "mods":Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->Clause(Ljava/lang/CharSequence;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v1

    .line 304
    .local v1, "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    sget-object v3, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;->MOD_NONE:Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    if-eq v0, v3, :cond_0

    .line 305
    new-instance v2, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;

    invoke-direct {v2, v1, v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;-><init>(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;)V

    .end local v1    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .local v2, "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    move-object v1, v2

    .line 307
    .end local v2    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .restart local v1    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    :cond_0
    return-object v1
.end method

.method public final Modifiers()Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException;
        }
    .end annotation

    .prologue
    const/4 v4, -0x1

    .line 112
    sget-object v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;->MOD_NONE:Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    .line 113
    .local v0, "ret":Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;
    iget v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    if-ne v1, v4, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk()I

    move-result v1

    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 137
    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_la1:[I

    const/4 v2, 0x3

    iget v3, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_gen:I

    aput v3, v1, v2

    .line 140
    :goto_1
    return-object v0

    .line 113
    :cond_0
    iget v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    goto :goto_0

    .line 117
    :pswitch_0
    iget v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    if-ne v1, v4, :cond_1

    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk()I

    move-result v1

    :goto_2
    packed-switch v1, :pswitch_data_1

    .line 131
    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_la1:[I

    const/4 v2, 0x2

    iget v3, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_gen:I

    aput v3, v1, v2

    .line 132
    invoke-direct {p0, v4}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 133
    new-instance v1, Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException;

    invoke-direct {v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException;-><init>()V

    throw v1

    .line 117
    :cond_1
    iget v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    goto :goto_2

    .line 119
    :pswitch_1
    const/16 v1, 0xb

    invoke-direct {p0, v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 120
    sget-object v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;->MOD_REQ:Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    .line 121
    goto :goto_1

    .line 123
    :pswitch_2
    const/16 v1, 0xc

    invoke-direct {p0, v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 124
    sget-object v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;->MOD_NOT:Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    .line 125
    goto :goto_1

    .line 127
    :pswitch_3
    const/16 v1, 0xa

    invoke-direct {p0, v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 128
    sget-object v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;->MOD_NOT:Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    .line 129
    goto :goto_1

    .line 113
    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 117
    :pswitch_data_1
    .packed-switch 0xa
        :pswitch_3
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final Query(Ljava/lang/CharSequence;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 6
    .param p1, "field"    # Ljava/lang/CharSequence;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 164
    const/4 v1, 0x0

    .line 165
    .local v1, "clauses":Ljava/util/Vector;, "Ljava/util/Vector<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    const/4 v2, 0x0

    .line 166
    .local v2, "first":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->DisjQuery(Ljava/lang/CharSequence;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v2

    .line 169
    :goto_0
    iget v3, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_1

    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk()I

    move-result v3

    :goto_1
    packed-switch v3, :pswitch_data_0

    .line 183
    :pswitch_0
    iget-object v3, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_la1:[I

    const/4 v4, 0x4

    iget v5, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_gen:I

    aput v5, v3, v4

    .line 193
    if-eqz v1, :cond_0

    .line 194
    new-instance v2, Lorg/apache/lucene/queryparser/flexible/core/nodes/BooleanQueryNode;

    .end local v2    # "first":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    invoke-direct {v2, v1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/BooleanQueryNode;-><init>(Ljava/util/List;)V

    .line 196
    :cond_0
    return-object v2

    .line 169
    .restart local v2    # "first":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    :cond_1
    iget v3, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    goto :goto_1

    .line 186
    :pswitch_1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->DisjQuery(Ljava/lang/CharSequence;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v0

    .line 187
    .local v0, "c":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    if-nez v1, :cond_2

    .line 188
    new-instance v1, Ljava/util/Vector;

    .end local v1    # "clauses":Ljava/util/Vector;, "Ljava/util/Vector<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    .line 189
    .restart local v1    # "clauses":Ljava/util/Vector;, "Ljava/util/Vector<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    invoke-virtual {v1, v2}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 191
    :cond_2
    invoke-virtual {v1, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_0

    .line 169
    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public ReInit(Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;)V
    .locals 3
    .param p1, "stream"    # Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;

    .prologue
    const/4 v2, -0x1

    .line 887
    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->token_source:Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->ReInit(Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;)V

    .line 888
    new-instance v1, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    invoke-direct {v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->token:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 889
    iput v2, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    .line 890
    const/4 v1, 0x0

    iput v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_gen:I

    .line 891
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v1, 0x1c

    if-lt v0, v1, :cond_0

    .line 892
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_2_rtns:[Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$JJCalls;

    array-length v1, v1

    if-lt v0, v1, :cond_1

    .line 893
    return-void

    .line 891
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_la1:[I

    aput v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 892
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_2_rtns:[Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$JJCalls;

    new-instance v2, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$JJCalls;

    invoke-direct {v2}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$JJCalls;-><init>()V

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public ReInit(Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;)V
    .locals 3
    .param p1, "tm"    # Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;

    .prologue
    const/4 v2, -0x1

    .line 907
    iput-object p1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->token_source:Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;

    .line 908
    new-instance v1, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    invoke-direct {v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->token:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 909
    iput v2, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    .line 910
    const/4 v1, 0x0

    iput v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_gen:I

    .line 911
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v1, 0x1c

    if-lt v0, v1, :cond_0

    .line 912
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_2_rtns:[Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$JJCalls;

    array-length v1, v1

    if-lt v0, v1, :cond_1

    .line 913
    return-void

    .line 911
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_la1:[I

    aput v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 912
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_2_rtns:[Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$JJCalls;

    new-instance v2, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$JJCalls;

    invoke-direct {v2}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser$JJCalls;-><init>()V

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public final Term(Ljava/lang/CharSequence;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 24
    .param p1, "field"    # Ljava/lang/CharSequence;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 510
    const/4 v8, 0x0

    .local v8, "boost":Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    const/4 v13, 0x0

    .line 511
    .local v13, "fuzzySlop":Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    const/4 v12, 0x0

    .line 512
    .local v12, "fuzzy":Z
    const/16 v21, 0x0

    .line 513
    .local v21, "regexp":Z
    const/16 v22, 0x0

    .line 514
    .local v22, "startInc":Z
    const/4 v10, 0x0

    .line 515
    .local v10, "endInc":Z
    const/4 v2, 0x0

    .line 517
    .local v2, "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    const/high16 v9, 0x40000000    # 2.0f

    .line 518
    .local v9, "defaultMinSimilarity":F
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_0

    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk()I

    move-result v3

    :goto_0
    packed-switch v3, :pswitch_data_0

    .line 699
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_la1:[I

    const/16 v4, 0x1b

    move-object/from16 v0, p0

    iget v6, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_gen:I

    aput v6, v3, v4

    .line 700
    const/4 v3, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 701
    new-instance v3, Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException;

    invoke-direct {v3}, Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException;-><init>()V

    throw v3

    .line 518
    :cond_0
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    goto :goto_0

    .line 522
    :pswitch_1
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_1

    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk()I

    move-result v3

    :goto_1
    packed-switch v3, :pswitch_data_1

    .line 535
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_la1:[I

    const/16 v4, 0xf

    move-object/from16 v0, p0

    iget v6, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_gen:I

    aput v6, v3, v4

    .line 536
    const/4 v3, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 537
    new-instance v3, Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException;

    invoke-direct {v3}, Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException;-><init>()V

    throw v3

    .line 522
    :cond_1
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    goto :goto_1

    .line 524
    :pswitch_3
    const/16 v3, 0x17

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    move-result-object v23

    .line 525
    .local v23, "term":Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    new-instance v2, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;

    .end local v2    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    move-object/from16 v0, v23

    iget-object v3, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->image:Ljava/lang/String;

    invoke-static {v3}, Lorg/apache/lucene/queryparser/flexible/standard/parser/EscapeQuerySyntaxImpl;->discardEscapeChar(Ljava/lang/CharSequence;)Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;

    move-result-object v3

    move-object/from16 v0, v23

    iget v4, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->beginColumn:I

    move-object/from16 v0, v23

    iget v6, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->endColumn:I

    move-object/from16 v0, p1

    invoke-direct {v2, v0, v3, v4, v6}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;II)V

    .line 539
    .restart local v2    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    :goto_2
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_2

    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk()I

    move-result v3

    :goto_3
    packed-switch v3, :pswitch_data_2

    .line 545
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_la1:[I

    const/16 v4, 0x10

    move-object/from16 v0, p0

    iget v6, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_gen:I

    aput v6, v3, v4

    .line 548
    :goto_4
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_3

    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk()I

    move-result v3

    :goto_5
    packed-switch v3, :pswitch_data_3

    .line 563
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_la1:[I

    const/16 v4, 0x12

    move-object/from16 v0, p0

    iget v6, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_gen:I

    aput v6, v3, v4

    .line 566
    :goto_6
    if-eqz v12, :cond_7

    .line 567
    move v5, v9

    .line 569
    .local v5, "fms":F
    :try_start_0
    iget-object v3, v13, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->image:Ljava/lang/String;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    move-result v5

    .line 571
    :goto_7
    const/4 v3, 0x0

    cmpg-float v3, v5, v3

    if-gez v3, :cond_5

    .line 572
    new-instance v3, Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException;

    new-instance v4, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;

    sget-object v6, Lorg/apache/lucene/queryparser/flexible/core/messages/QueryParserMessages;->INVALID_SYNTAX_FUZZY_LIMITS:Ljava/lang/String;

    invoke-direct {v4, v6}, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;-><init>(Ljava/lang/String;)V

    invoke-direct {v3, v4}, Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException;-><init>(Lorg/apache/lucene/queryparser/flexible/messages/Message;)V

    throw v3

    .line 528
    .end local v5    # "fms":F
    .end local v23    # "term":Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    :pswitch_4
    const/16 v3, 0x19

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    move-result-object v23

    .line 529
    .restart local v23    # "term":Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    const/16 v21, 0x1

    .line 530
    goto :goto_2

    .line 532
    .end local v23    # "term":Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    :pswitch_5
    const/16 v3, 0x1c

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    move-result-object v23

    .line 533
    .restart local v23    # "term":Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    goto :goto_2

    .line 539
    :cond_2
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    goto :goto_3

    .line 541
    :pswitch_6
    const/16 v3, 0x18

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    move-result-object v13

    .line 542
    const/4 v12, 0x1

    .line 543
    goto :goto_4

    .line 548
    :cond_3
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    goto :goto_5

    .line 550
    :pswitch_7
    const/16 v3, 0x15

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 551
    const/16 v3, 0x1c

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    move-result-object v8

    .line 552
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_4

    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk()I

    move-result v3

    :goto_8
    packed-switch v3, :pswitch_data_4

    .line 558
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_la1:[I

    const/16 v4, 0x11

    move-object/from16 v0, p0

    iget v6, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_gen:I

    aput v6, v3, v4

    goto :goto_6

    .line 552
    :cond_4
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    goto :goto_8

    .line 554
    :pswitch_8
    const/16 v3, 0x18

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    move-result-object v13

    .line 555
    const/4 v12, 0x1

    .line 556
    goto/16 :goto_6

    .line 573
    .restart local v5    # "fms":F
    :cond_5
    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v3, v5, v3

    if-ltz v3, :cond_6

    float-to-int v3, v5

    int-to-float v3, v3

    cmpl-float v3, v5, v3

    if-eqz v3, :cond_6

    .line 574
    new-instance v3, Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException;

    new-instance v4, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;

    sget-object v6, Lorg/apache/lucene/queryparser/flexible/core/messages/QueryParserMessages;->INVALID_SYNTAX_FUZZY_EDITS:Ljava/lang/String;

    invoke-direct {v4, v6}, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;-><init>(Ljava/lang/String;)V

    invoke-direct {v3, v4}, Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException;-><init>(Lorg/apache/lucene/queryparser/flexible/messages/Message;)V

    throw v3

    .line 576
    :cond_6
    new-instance v2, Lorg/apache/lucene/queryparser/flexible/core/nodes/FuzzyQueryNode;

    .end local v2    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    move-object/from16 v0, v23

    iget-object v3, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->image:Ljava/lang/String;

    invoke-static {v3}, Lorg/apache/lucene/queryparser/flexible/standard/parser/EscapeQuerySyntaxImpl;->discardEscapeChar(Ljava/lang/CharSequence;)Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;

    move-result-object v4

    move-object/from16 v0, v23

    iget v6, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->beginColumn:I

    move-object/from16 v0, v23

    iget v7, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->endColumn:I

    move-object/from16 v3, p1

    invoke-direct/range {v2 .. v7}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FuzzyQueryNode;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;FII)V

    .restart local v2    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    move-object/from16 v17, v2

    .line 703
    .end local v2    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .end local v5    # "fms":F
    .end local v23    # "term":Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    .local v17, "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    :goto_9
    if-eqz v8, :cond_14

    .line 704
    const/high16 v11, 0x3f800000    # 1.0f

    .line 706
    .local v11, "f":F
    :try_start_1
    iget-object v3, v8, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->image:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v11

    .line 708
    if-eqz v17, :cond_14

    .line 709
    new-instance v2, Lorg/apache/lucene/queryparser/flexible/core/nodes/BoostQueryNode;

    move-object/from16 v0, v17

    invoke-direct {v2, v0, v11}, Lorg/apache/lucene/queryparser/flexible/core/nodes/BoostQueryNode;-><init>(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;F)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 717
    .end local v11    # "f":F
    .end local v17    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .restart local v2    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    :goto_a
    return-object v2

    .line 577
    .restart local v23    # "term":Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    :cond_7
    if-eqz v21, :cond_15

    .line 578
    move-object/from16 v0, v23

    iget-object v3, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->image:Ljava/lang/String;

    const/4 v4, 0x1

    move-object/from16 v0, v23

    iget-object v6, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->image:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v3, v4, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v20

    .line 579
    .local v20, "re":Ljava/lang/String;
    new-instance v2, Lorg/apache/lucene/queryparser/flexible/standard/nodes/RegexpQueryNode;

    .end local v2    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    const/4 v3, 0x0

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    move-result v4

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-direct {v2, v0, v1, v3, v4}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/RegexpQueryNode;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;II)V

    .restart local v2    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    move-object/from16 v17, v2

    .line 581
    .end local v2    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .restart local v17    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    goto :goto_9

    .line 584
    .end local v17    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .end local v20    # "re":Ljava/lang/String;
    .end local v23    # "term":Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    .restart local v2    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    :pswitch_9
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_8

    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk()I

    move-result v3

    :goto_b
    packed-switch v3, :pswitch_data_5

    .line 593
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_la1:[I

    const/16 v4, 0x13

    move-object/from16 v0, p0

    iget v6, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_gen:I

    aput v6, v3, v4

    .line 594
    const/4 v3, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 595
    new-instance v3, Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException;

    invoke-direct {v3}, Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException;-><init>()V

    throw v3

    .line 584
    :cond_8
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    goto :goto_b

    .line 586
    :pswitch_a
    const/16 v3, 0x1a

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 587
    const/16 v22, 0x1

    .line 597
    :goto_c
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_9

    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk()I

    move-result v3

    :goto_d
    packed-switch v3, :pswitch_data_6

    .line 605
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_la1:[I

    const/16 v4, 0x14

    move-object/from16 v0, p0

    iget v6, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_gen:I

    aput v6, v3, v4

    .line 606
    const/4 v3, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 607
    new-instance v3, Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException;

    invoke-direct {v3}, Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException;-><init>()V

    throw v3

    .line 590
    :pswitch_b
    const/16 v3, 0x1b

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    goto :goto_c

    .line 597
    :cond_9
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    goto :goto_d

    .line 599
    :pswitch_c
    const/16 v3, 0x21

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    move-result-object v14

    .line 609
    .local v14, "goop1":Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    :goto_e
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_a

    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk()I

    move-result v3

    :goto_f
    packed-switch v3, :pswitch_data_7

    .line 614
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_la1:[I

    const/16 v4, 0x15

    move-object/from16 v0, p0

    iget v6, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_gen:I

    aput v6, v3, v4

    .line 617
    :goto_10
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_b

    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk()I

    move-result v3

    :goto_11
    packed-switch v3, :pswitch_data_8

    .line 625
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_la1:[I

    const/16 v4, 0x16

    move-object/from16 v0, p0

    iget v6, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_gen:I

    aput v6, v3, v4

    .line 626
    const/4 v3, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 627
    new-instance v3, Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException;

    invoke-direct {v3}, Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException;-><init>()V

    throw v3

    .line 602
    .end local v14    # "goop1":Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    :pswitch_d
    const/16 v3, 0x20

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    move-result-object v14

    .line 603
    .restart local v14    # "goop1":Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    goto :goto_e

    .line 609
    :cond_a
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    goto :goto_f

    .line 611
    :pswitch_e
    const/16 v3, 0x1d

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    goto :goto_10

    .line 617
    :cond_b
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    goto :goto_11

    .line 619
    :pswitch_f
    const/16 v3, 0x21

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    move-result-object v15

    .line 629
    .local v15, "goop2":Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    :goto_12
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_c

    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk()I

    move-result v3

    :goto_13
    packed-switch v3, :pswitch_data_9

    .line 638
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_la1:[I

    const/16 v4, 0x17

    move-object/from16 v0, p0

    iget v6, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_gen:I

    aput v6, v3, v4

    .line 639
    const/4 v3, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 640
    new-instance v3, Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException;

    invoke-direct {v3}, Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException;-><init>()V

    throw v3

    .line 622
    .end local v15    # "goop2":Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    :pswitch_10
    const/16 v3, 0x20

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    move-result-object v15

    .line 623
    .restart local v15    # "goop2":Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    goto :goto_12

    .line 629
    :cond_c
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    goto :goto_13

    .line 631
    :pswitch_11
    const/16 v3, 0x1e

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 632
    const/4 v10, 0x1

    .line 642
    :goto_14
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_f

    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk()I

    move-result v3

    :goto_15
    packed-switch v3, :pswitch_data_a

    .line 648
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_la1:[I

    const/16 v4, 0x18

    move-object/from16 v0, p0

    iget v6, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_gen:I

    aput v6, v3, v4

    .line 651
    :goto_16
    iget v3, v14, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->kind:I

    const/16 v4, 0x20

    if-ne v3, v4, :cond_d

    .line 652
    iget-object v3, v14, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->image:Ljava/lang/String;

    const/4 v4, 0x1

    iget-object v6, v14, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->image:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v3, v4, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v14, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->image:Ljava/lang/String;

    .line 654
    :cond_d
    iget v3, v15, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->kind:I

    const/16 v4, 0x20

    if-ne v3, v4, :cond_e

    .line 655
    iget-object v3, v15, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->image:Ljava/lang/String;

    const/4 v4, 0x1

    iget-object v6, v15, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->image:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v3, v4, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v15, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->image:Ljava/lang/String;

    .line 658
    :cond_e
    new-instance v18, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;

    .line 659
    iget-object v3, v14, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->image:Ljava/lang/String;

    invoke-static {v3}, Lorg/apache/lucene/queryparser/flexible/standard/parser/EscapeQuerySyntaxImpl;->discardEscapeChar(Ljava/lang/CharSequence;)Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;

    move-result-object v3

    iget v4, v14, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->beginColumn:I

    iget v6, v14, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->endColumn:I

    .line 658
    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3, v4, v6}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;II)V

    .line 660
    .local v18, "qLower":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    new-instance v19, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;

    .line 661
    iget-object v3, v15, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->image:Ljava/lang/String;

    invoke-static {v3}, Lorg/apache/lucene/queryparser/flexible/standard/parser/EscapeQuerySyntaxImpl;->discardEscapeChar(Ljava/lang/CharSequence;)Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;

    move-result-object v3

    iget v4, v15, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->beginColumn:I

    iget v6, v15, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->endColumn:I

    .line 660
    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3, v4, v6}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;II)V

    .line 662
    .local v19, "qUpper":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    new-instance v2, Lorg/apache/lucene/queryparser/flexible/standard/nodes/TermRangeQueryNode;

    .end local v2    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    if-eqz v22, :cond_10

    const/4 v3, 0x1

    move v4, v3

    :goto_17
    if-eqz v10, :cond_11

    const/4 v3, 0x1

    :goto_18
    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-direct {v2, v0, v1, v4, v3}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/TermRangeQueryNode;-><init>(Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;ZZ)V

    .restart local v2    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    move-object/from16 v17, v2

    .line 663
    .end local v2    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .restart local v17    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    goto/16 :goto_9

    .line 635
    .end local v17    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .end local v18    # "qLower":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    .end local v19    # "qUpper":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    .restart local v2    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    :pswitch_12
    const/16 v3, 0x1f

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    goto/16 :goto_14

    .line 642
    :cond_f
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    goto/16 :goto_15

    .line 644
    :pswitch_13
    const/16 v3, 0x15

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 645
    const/16 v3, 0x1c

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    move-result-object v8

    .line 646
    goto/16 :goto_16

    .line 662
    .end local v2    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .restart local v18    # "qLower":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    .restart local v19    # "qUpper":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    :cond_10
    const/4 v3, 0x0

    move v4, v3

    goto :goto_17

    :cond_11
    const/4 v3, 0x0

    goto :goto_18

    .line 665
    .end local v14    # "goop1":Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    .end local v15    # "goop2":Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    .end local v18    # "qLower":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    .end local v19    # "qUpper":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    .restart local v2    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    :pswitch_14
    const/16 v3, 0x16

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    move-result-object v23

    .line 666
    .restart local v23    # "term":Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    new-instance v2, Lorg/apache/lucene/queryparser/flexible/core/nodes/QuotedFieldQueryNode;

    .end local v2    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    move-object/from16 v0, v23

    iget-object v3, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->image:Ljava/lang/String;

    const/4 v4, 0x1

    move-object/from16 v0, v23

    iget-object v6, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->image:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v3, v4, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lorg/apache/lucene/queryparser/flexible/standard/parser/EscapeQuerySyntaxImpl;->discardEscapeChar(Ljava/lang/CharSequence;)Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;

    move-result-object v3

    move-object/from16 v0, v23

    iget v4, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->beginColumn:I

    add-int/lit8 v4, v4, 0x1

    move-object/from16 v0, v23

    iget v6, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->endColumn:I

    add-int/lit8 v6, v6, -0x1

    move-object/from16 v0, p1

    invoke-direct {v2, v0, v3, v4, v6}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QuotedFieldQueryNode;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;II)V

    .line 667
    .restart local v2    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_12

    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk()I

    move-result v3

    :goto_19
    packed-switch v3, :pswitch_data_b

    .line 672
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_la1:[I

    const/16 v4, 0x19

    move-object/from16 v0, p0

    iget v6, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_gen:I

    aput v6, v3, v4

    .line 675
    :goto_1a
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_13

    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk()I

    move-result v3

    :goto_1b
    packed-switch v3, :pswitch_data_c

    .line 681
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_la1:[I

    const/16 v4, 0x1a

    move-object/from16 v0, p0

    iget v6, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_gen:I

    aput v6, v3, v4

    .line 684
    :goto_1c
    const/16 v16, 0x0

    .line 686
    .local v16, "phraseSlop":I
    if-eqz v13, :cond_15

    .line 688
    :try_start_2
    iget-object v3, v13, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->image:Ljava/lang/String;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Float;->intValue()I

    move-result v16

    .line 689
    new-instance v17, Lorg/apache/lucene/queryparser/flexible/core/nodes/SlopQueryNode;

    move-object/from16 v0, v17

    move/from16 v1, v16

    invoke-direct {v0, v2, v1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/SlopQueryNode;-><init>(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;I)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .end local v2    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .restart local v17    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    goto/16 :goto_9

    .line 667
    .end local v16    # "phraseSlop":I
    .end local v17    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .restart local v2    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    :cond_12
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    goto :goto_19

    .line 669
    :pswitch_15
    const/16 v3, 0x18

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    move-result-object v13

    .line 670
    goto :goto_1a

    .line 675
    :cond_13
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    goto :goto_1b

    .line 677
    :pswitch_16
    const/16 v3, 0x15

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 678
    const/16 v3, 0x1c

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    move-result-object v8

    .line 679
    goto :goto_1c

    .line 691
    .restart local v16    # "phraseSlop":I
    :catch_0
    move-exception v3

    move-object/from16 v17, v2

    .line 697
    .end local v2    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .restart local v17    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    goto/16 :goto_9

    .line 711
    .end local v16    # "phraseSlop":I
    .end local v23    # "term":Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    .restart local v11    # "f":F
    :catch_1
    move-exception v3

    move-object/from16 v2, v17

    .end local v17    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .restart local v2    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    goto/16 :goto_a

    .line 570
    .end local v11    # "f":F
    .restart local v5    # "fms":F
    .restart local v23    # "term":Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    :catch_2
    move-exception v3

    goto/16 :goto_7

    .end local v2    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .end local v5    # "fms":F
    .end local v23    # "term":Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    .restart local v17    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    :cond_14
    move-object/from16 v2, v17

    .end local v17    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .restart local v2    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    goto/16 :goto_a

    .restart local v23    # "term":Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    :cond_15
    move-object/from16 v17, v2

    .end local v2    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .restart local v17    # "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    goto/16 :goto_9

    .line 518
    nop

    :pswitch_data_0
    .packed-switch 0x16
        :pswitch_14
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_9
        :pswitch_9
        :pswitch_1
    .end packed-switch

    .line 522
    :pswitch_data_1
    .packed-switch 0x17
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_2
        :pswitch_2
        :pswitch_5
    .end packed-switch

    .line 539
    :pswitch_data_2
    .packed-switch 0x18
        :pswitch_6
    .end packed-switch

    .line 548
    :pswitch_data_3
    .packed-switch 0x15
        :pswitch_7
    .end packed-switch

    .line 552
    :pswitch_data_4
    .packed-switch 0x18
        :pswitch_8
    .end packed-switch

    .line 584
    :pswitch_data_5
    .packed-switch 0x1a
        :pswitch_a
        :pswitch_b
    .end packed-switch

    .line 597
    :pswitch_data_6
    .packed-switch 0x20
        :pswitch_d
        :pswitch_c
    .end packed-switch

    .line 609
    :pswitch_data_7
    .packed-switch 0x1d
        :pswitch_e
    .end packed-switch

    .line 617
    :pswitch_data_8
    .packed-switch 0x20
        :pswitch_10
        :pswitch_f
    .end packed-switch

    .line 629
    :pswitch_data_9
    .packed-switch 0x1e
        :pswitch_11
        :pswitch_12
    .end packed-switch

    .line 642
    :pswitch_data_a
    .packed-switch 0x15
        :pswitch_13
    .end packed-switch

    .line 667
    :pswitch_data_b
    .packed-switch 0x18
        :pswitch_15
    .end packed-switch

    .line 675
    :pswitch_data_c
    .packed-switch 0x15
        :pswitch_16
    .end packed-switch
.end method

.method public final TopLevelQuery(Ljava/lang/CharSequence;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 2
    .param p1, "field"    # Ljava/lang/CharSequence;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 147
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->Query(Ljava/lang/CharSequence;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v0

    .line 148
    .local v0, "q":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 149
    return-object v0
.end method

.method public final disable_tracing()V
    .locals 0

    .prologue
    .line 1063
    return-void
.end method

.method public final enable_tracing()V
    .locals 0

    .prologue
    .line 1059
    return-void
.end method

.method public generateParseException()Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException;
    .locals 9

    .prologue
    const/16 v8, 0x22

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 1022
    iget-object v4, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_expentries:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 1023
    new-array v3, v8, [Z

    .line 1024
    .local v3, "la1tokens":[Z
    iget v4, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_kind:I

    if-ltz v4, :cond_0

    .line 1025
    iget v4, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_kind:I

    aput-boolean v6, v3, v4

    .line 1026
    const/4 v4, -0x1

    iput v4, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_kind:I

    .line 1028
    :cond_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/16 v4, 0x1c

    if-lt v1, v4, :cond_1

    .line 1040
    const/4 v1, 0x0

    :goto_1
    if-lt v1, v8, :cond_6

    .line 1047
    iput v7, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_endpos:I

    .line 1048
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_rescan_token()V

    .line 1049
    invoke-direct {p0, v7, v7}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_add_error_token(II)V

    .line 1050
    iget-object v4, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_expentries:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    new-array v0, v4, [[I

    .line 1051
    .local v0, "exptokseq":[[I
    const/4 v1, 0x0

    :goto_2
    iget-object v4, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_expentries:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lt v1, v4, :cond_8

    .line 1054
    new-instance v4, Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException;

    iget-object v5, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->token:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    sget-object v6, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->tokenImage:[Ljava/lang/String;

    invoke-direct {v4, v5, v0, v6}, Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException;-><init>(Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;[[I[Ljava/lang/String;)V

    return-object v4

    .line 1029
    .end local v0    # "exptokseq":[[I
    :cond_1
    iget-object v4, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_la1:[I

    aget v4, v4, v1

    iget v5, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_gen:I

    if-ne v4, v5, :cond_2

    .line 1030
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_3
    const/16 v4, 0x20

    if-lt v2, v4, :cond_3

    .line 1028
    .end local v2    # "j":I
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1031
    .restart local v2    # "j":I
    :cond_3
    sget-object v4, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_la1_0:[I

    aget v4, v4, v1

    shl-int v5, v6, v2

    and-int/2addr v4, v5

    if-eqz v4, :cond_4

    .line 1032
    aput-boolean v6, v3, v2

    .line 1034
    :cond_4
    sget-object v4, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_la1_1:[I

    aget v4, v4, v1

    shl-int v5, v6, v2

    and-int/2addr v4, v5

    if-eqz v4, :cond_5

    .line 1035
    add-int/lit8 v4, v2, 0x20

    aput-boolean v6, v3, v4

    .line 1030
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 1041
    .end local v2    # "j":I
    :cond_6
    aget-boolean v4, v3, v1

    if-eqz v4, :cond_7

    .line 1042
    new-array v4, v6, [I

    iput-object v4, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_expentry:[I

    .line 1043
    iget-object v4, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_expentry:[I

    aput v1, v4, v7

    .line 1044
    iget-object v4, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_expentries:Ljava/util/List;

    iget-object v5, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_expentry:[I

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1040
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1052
    .restart local v0    # "exptokseq":[[I
    :cond_8
    iget-object v4, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_expentries:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [I

    aput-object v4, v0, v1

    .line 1051
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method

.method public final getNextToken()Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    .locals 2

    .prologue
    .line 965
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->token:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    iget-object v0, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->next:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->token:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    iget-object v0, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->next:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    iput-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->token:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 967
    :goto_0
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_ntk:I

    .line 968
    iget v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_gen:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->jj_gen:I

    .line 969
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->token:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    return-object v0

    .line 966
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->token:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->token_source:Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;

    invoke-virtual {v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->getNextToken()Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    move-result-object v1

    iput-object v1, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->next:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    iput-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->token:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    goto :goto_0
.end method

.method public final getToken(I)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    .locals 4
    .param p1, "index"    # I

    .prologue
    .line 974
    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->token:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 975
    .local v1, "t":Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    const/4 v0, 0x0

    .local v0, "i":I
    move-object v2, v1

    .end local v1    # "t":Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    .local v2, "t":Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    :goto_0
    if-lt v0, p1, :cond_0

    .line 979
    return-object v2

    .line 976
    :cond_0
    iget-object v3, v2, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->next:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    if-eqz v3, :cond_1

    iget-object v1, v2, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->next:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .line 975
    .end local v2    # "t":Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    .restart local v1    # "t":Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    :goto_1
    add-int/lit8 v0, v0, 0x1

    move-object v2, v1

    .end local v1    # "t":Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    .restart local v2    # "t":Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    goto :goto_0

    .line 977
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->token_source:Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;

    invoke-virtual {v3}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->getNextToken()Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    move-result-object v1

    iput-object v1, v2, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->next:Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    .end local v2    # "t":Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    .restart local v1    # "t":Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    goto :goto_1
.end method

.method public parse(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 8
    .param p1, "query"    # Ljava/lang/CharSequence;
    .param p2, "field"    # Ljava/lang/CharSequence;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeParseException;
        }
    .end annotation

    .prologue
    .line 62
    new-instance v4, Lorg/apache/lucene/queryparser/flexible/standard/parser/FastCharStream;

    new-instance v5, Ljava/io/StringReader;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v4, v5}, Lorg/apache/lucene/queryparser/flexible/standard/parser/FastCharStream;-><init>(Ljava/io/Reader;)V

    invoke-virtual {p0, v4}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->ReInit(Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;)V

    .line 65
    :try_start_0
    invoke-virtual {p0, p2}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;->TopLevelQuery(Ljava/lang/CharSequence;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    :try_end_0
    .catch Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 66
    .local v2, "querynode":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    return-object v2

    .line 68
    .end local v2    # "querynode":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    :catch_0
    move-exception v3

    .line 69
    .local v3, "tme":Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException;
    invoke-virtual {v3, p1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException;->setQuery(Ljava/lang/CharSequence;)V

    .line 70
    throw v3

    .line 72
    .end local v3    # "tme":Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException;
    :catch_1
    move-exception v3

    .line 73
    .local v3, "tme":Ljava/lang/Error;
    new-instance v1, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;

    sget-object v4, Lorg/apache/lucene/queryparser/flexible/core/messages/QueryParserMessages;->INVALID_SYNTAX_CANNOT_PARSE:Ljava/lang/String;

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    const/4 v6, 0x1

    invoke-virtual {v3}, Ljava/lang/Error;->getMessage()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-direct {v1, v4, v5}, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 74
    .local v1, "message":Lorg/apache/lucene/queryparser/flexible/messages/Message;
    new-instance v0, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeParseException;

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeParseException;-><init>(Ljava/lang/Throwable;)V

    .line 75
    .local v0, "e":Lorg/apache/lucene/queryparser/flexible/core/QueryNodeParseException;
    invoke-virtual {v0, p1}, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeParseException;->setQuery(Ljava/lang/CharSequence;)V

    .line 76
    invoke-virtual {v0, v1}, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeParseException;->setNonLocalizedMessage(Lorg/apache/lucene/queryparser/flexible/messages/Message;)V

    .line 77
    throw v0
.end method
