.class public Lorg/apache/lucene/queryparser/flexible/standard/config/FieldDateResolutionFCListener;
.super Ljava/lang/Object;
.source "FieldDateResolutionFCListener.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/flexible/core/config/FieldConfigListener;


# instance fields
.field private config:Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;)V
    .locals 1
    .param p1, "config"    # Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/config/FieldDateResolutionFCListener;->config:Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    .line 45
    iput-object p1, p0, Lorg/apache/lucene/queryparser/flexible/standard/config/FieldDateResolutionFCListener;->config:Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    .line 46
    return-void
.end method


# virtual methods
.method public buildFieldConfig(Lorg/apache/lucene/queryparser/flexible/core/config/FieldConfig;)V
    .locals 4
    .param p1, "fieldConfig"    # Lorg/apache/lucene/queryparser/flexible/core/config/FieldConfig;

    .prologue
    .line 50
    const/4 v0, 0x0

    .line 51
    .local v0, "dateRes":Lorg/apache/lucene/document/DateTools$Resolution;
    iget-object v2, p0, Lorg/apache/lucene/queryparser/flexible/standard/config/FieldDateResolutionFCListener;->config:Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    sget-object v3, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->FIELD_DATE_RESOLUTION_MAP:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->get(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    .line 53
    .local v1, "dateResMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/CharSequence;Lorg/apache/lucene/document/DateTools$Resolution;>;"
    if-eqz v1, :cond_0

    .line 55
    invoke-virtual {p1}, Lorg/apache/lucene/queryparser/flexible/core/config/FieldConfig;->getField()Ljava/lang/String;

    move-result-object v2

    .line 54
    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "dateRes":Lorg/apache/lucene/document/DateTools$Resolution;
    check-cast v0, Lorg/apache/lucene/document/DateTools$Resolution;

    .line 58
    .restart local v0    # "dateRes":Lorg/apache/lucene/document/DateTools$Resolution;
    :cond_0
    if-nez v0, :cond_1

    .line 59
    iget-object v2, p0, Lorg/apache/lucene/queryparser/flexible/standard/config/FieldDateResolutionFCListener;->config:Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    sget-object v3, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->DATE_RESOLUTION:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->get(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "dateRes":Lorg/apache/lucene/document/DateTools$Resolution;
    check-cast v0, Lorg/apache/lucene/document/DateTools$Resolution;

    .line 62
    .restart local v0    # "dateRes":Lorg/apache/lucene/document/DateTools$Resolution;
    :cond_1
    if-eqz v0, :cond_2

    .line 63
    sget-object v2, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->DATE_RESOLUTION:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-virtual {p1, v2, v0}, Lorg/apache/lucene/queryparser/flexible/core/config/FieldConfig;->set(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;Ljava/lang/Object;)V

    .line 66
    :cond_2
    return-void
.end method
