.class Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser$ComplexPhraseQuery;
.super Lorg/apache/lucene/search/Query;
.source "ComplexPhraseQueryParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ComplexPhraseQuery"
.end annotation


# instance fields
.field private contents:Lorg/apache/lucene/search/Query;

.field field:Ljava/lang/String;

.field phrasedQueryStringContents:Ljava/lang/String;

.field slopFactor:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "phrasedQueryStringContents"    # Ljava/lang/String;
    .param p3, "slopFactor"    # I

    .prologue
    .line 209
    invoke-direct {p0}, Lorg/apache/lucene/search/Query;-><init>()V

    .line 210
    iput-object p1, p0, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser$ComplexPhraseQuery;->field:Ljava/lang/String;

    .line 211
    iput-object p2, p0, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser$ComplexPhraseQuery;->phrasedQueryStringContents:Ljava/lang/String;

    .line 212
    iput p3, p0, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser$ComplexPhraseQuery;->slopFactor:I

    .line 213
    return-void
.end method

.method private addComplexPhraseClause(Ljava/util/List;Lorg/apache/lucene/search/BooleanQuery;)V
    .locals 16
    .param p2, "qc"    # Lorg/apache/lucene/search/BooleanQuery;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/search/spans/SpanQuery;",
            ">;",
            "Lorg/apache/lucene/search/BooleanQuery;",
            ")V"
        }
    .end annotation

    .prologue
    .line 315
    .local p1, "spanClauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/spans/SpanQuery;>;"
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 316
    .local v7, "ors":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/search/spans/SpanQuery;>;"
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 317
    .local v6, "nots":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/search/spans/SpanQuery;>;"
    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/search/BooleanQuery;->getClauses()[Lorg/apache/lucene/search/BooleanClause;

    move-result-object v1

    .line 320
    .local v1, "bclauses":[Lorg/apache/lucene/search/BooleanClause;
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    array-length v13, v1

    if-lt v5, v13, :cond_0

    .line 343
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v13

    if-nez v13, :cond_4

    .line 356
    :goto_1
    return-void

    .line 321
    :cond_0
    aget-object v13, v1, v5

    invoke-virtual {v13}, Lorg/apache/lucene/search/BooleanClause;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v3

    .line 324
    .local v3, "childQuery":Lorg/apache/lucene/search/Query;
    move-object v4, v7

    .line 325
    .local v4, "chosenList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/search/spans/SpanQuery;>;"
    aget-object v13, v1, v5

    invoke-virtual {v13}, Lorg/apache/lucene/search/BooleanClause;->getOccur()Lorg/apache/lucene/search/BooleanClause$Occur;

    move-result-object v13

    sget-object v14, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST_NOT:Lorg/apache/lucene/search/BooleanClause$Occur;

    if-ne v13, v14, :cond_1

    .line 326
    move-object v4, v6

    .line 329
    :cond_1
    instance-of v13, v3, Lorg/apache/lucene/search/TermQuery;

    if-eqz v13, :cond_2

    move-object v12, v3

    .line 330
    check-cast v12, Lorg/apache/lucene/search/TermQuery;

    .line 331
    .local v12, "tq":Lorg/apache/lucene/search/TermQuery;
    new-instance v11, Lorg/apache/lucene/search/spans/SpanTermQuery;

    invoke-virtual {v12}, Lorg/apache/lucene/search/TermQuery;->getTerm()Lorg/apache/lucene/index/Term;

    move-result-object v13

    invoke-direct {v11, v13}, Lorg/apache/lucene/search/spans/SpanTermQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    .line 332
    .local v11, "stq":Lorg/apache/lucene/search/spans/SpanTermQuery;
    invoke-virtual {v12}, Lorg/apache/lucene/search/TermQuery;->getBoost()F

    move-result v13

    invoke-virtual {v11, v13}, Lorg/apache/lucene/search/spans/SpanTermQuery;->setBoost(F)V

    .line 333
    invoke-virtual {v4, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 320
    .end local v11    # "stq":Lorg/apache/lucene/search/spans/SpanTermQuery;
    .end local v12    # "tq":Lorg/apache/lucene/search/TermQuery;
    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 334
    :cond_2
    instance-of v13, v3, Lorg/apache/lucene/search/BooleanQuery;

    if-eqz v13, :cond_3

    move-object v2, v3

    .line 335
    check-cast v2, Lorg/apache/lucene/search/BooleanQuery;

    .line 336
    .local v2, "cbq":Lorg/apache/lucene/search/BooleanQuery;
    move-object/from16 v0, p0

    invoke-direct {v0, v4, v2}, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser$ComplexPhraseQuery;->addComplexPhraseClause(Ljava/util/List;Lorg/apache/lucene/search/BooleanQuery;)V

    goto :goto_2

    .line 339
    .end local v2    # "cbq":Lorg/apache/lucene/search/BooleanQuery;
    :cond_3
    new-instance v13, Ljava/lang/IllegalArgumentException;

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "Unknown query type:"

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 340
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 339
    invoke-direct {v13, v14}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 346
    .end local v3    # "childQuery":Lorg/apache/lucene/search/Query;
    .end local v4    # "chosenList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/search/spans/SpanQuery;>;"
    :cond_4
    new-instance v10, Lorg/apache/lucene/search/spans/SpanOrQuery;

    .line 347
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v13

    new-array v13, v13, [Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v7, v13}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v13

    check-cast v13, [Lorg/apache/lucene/search/spans/SpanQuery;

    .line 346
    invoke-direct {v10, v13}, Lorg/apache/lucene/search/spans/SpanOrQuery;-><init>([Lorg/apache/lucene/search/spans/SpanQuery;)V

    .line 348
    .local v10, "soq":Lorg/apache/lucene/search/spans/SpanOrQuery;
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v13

    if-nez v13, :cond_5

    .line 349
    move-object/from16 v0, p1

    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 351
    :cond_5
    new-instance v9, Lorg/apache/lucene/search/spans/SpanOrQuery;

    .line 352
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v13

    new-array v13, v13, [Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v6, v13}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v13

    check-cast v13, [Lorg/apache/lucene/search/spans/SpanQuery;

    .line 351
    invoke-direct {v9, v13}, Lorg/apache/lucene/search/spans/SpanOrQuery;-><init>([Lorg/apache/lucene/search/spans/SpanQuery;)V

    .line 353
    .local v9, "snqs":Lorg/apache/lucene/search/spans/SpanOrQuery;
    new-instance v8, Lorg/apache/lucene/search/spans/SpanNotQuery;

    invoke-direct {v8, v10, v9}, Lorg/apache/lucene/search/spans/SpanNotQuery;-><init>(Lorg/apache/lucene/search/spans/SpanQuery;Lorg/apache/lucene/search/spans/SpanQuery;)V

    .line 354
    .local v8, "snq":Lorg/apache/lucene/search/spans/SpanNotQuery;
    move-object/from16 v0, p1

    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 378
    if-ne p0, p1, :cond_1

    .line 401
    :cond_0
    :goto_0
    return v1

    .line 380
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 381
    goto :goto_0

    .line 382
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 383
    goto :goto_0

    .line 384
    :cond_3
    invoke-super {p0, p1}, Lorg/apache/lucene/search/Query;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    .line 385
    goto :goto_0

    :cond_4
    move-object v0, p1

    .line 387
    check-cast v0, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser$ComplexPhraseQuery;

    .line 388
    .local v0, "other":Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser$ComplexPhraseQuery;
    iget-object v3, p0, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser$ComplexPhraseQuery;->field:Ljava/lang/String;

    if-nez v3, :cond_5

    .line 389
    iget-object v3, v0, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser$ComplexPhraseQuery;->field:Ljava/lang/String;

    if-eqz v3, :cond_6

    move v1, v2

    .line 390
    goto :goto_0

    .line 391
    :cond_5
    iget-object v3, p0, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser$ComplexPhraseQuery;->field:Ljava/lang/String;

    iget-object v4, v0, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser$ComplexPhraseQuery;->field:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    move v1, v2

    .line 392
    goto :goto_0

    .line 393
    :cond_6
    iget-object v3, p0, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser$ComplexPhraseQuery;->phrasedQueryStringContents:Ljava/lang/String;

    if-nez v3, :cond_7

    .line 394
    iget-object v3, v0, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser$ComplexPhraseQuery;->phrasedQueryStringContents:Ljava/lang/String;

    if-eqz v3, :cond_8

    move v1, v2

    .line 395
    goto :goto_0

    .line 396
    :cond_7
    iget-object v3, p0, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser$ComplexPhraseQuery;->phrasedQueryStringContents:Ljava/lang/String;

    .line 397
    iget-object v4, v0, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser$ComplexPhraseQuery;->phrasedQueryStringContents:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    move v1, v2

    .line 398
    goto :goto_0

    .line 399
    :cond_8
    iget v3, p0, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser$ComplexPhraseQuery;->slopFactor:I

    iget v4, v0, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser$ComplexPhraseQuery;->slopFactor:I

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 400
    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 365
    const/16 v0, 0x1f

    .line 366
    .local v0, "prime":I
    invoke-super {p0}, Lorg/apache/lucene/search/Query;->hashCode()I

    move-result v1

    .line 367
    .local v1, "result":I
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser$ComplexPhraseQuery;->field:Ljava/lang/String;

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    add-int v1, v4, v2

    .line 368
    mul-int/lit8 v2, v1, 0x1f

    .line 370
    iget-object v4, p0, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser$ComplexPhraseQuery;->phrasedQueryStringContents:Ljava/lang/String;

    if-nez v4, :cond_1

    .line 368
    :goto_1
    add-int v1, v2, v3

    .line 372
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser$ComplexPhraseQuery;->slopFactor:I

    add-int v1, v2, v3

    .line 373
    return v1

    .line 367
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser$ComplexPhraseQuery;->field:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_0

    .line 371
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser$ComplexPhraseQuery;->phrasedQueryStringContents:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    goto :goto_1
.end method

.method protected parsePhraseElements(Lorg/apache/lucene/queryparser/classic/QueryParser;)V
    .locals 1
    .param p1, "qp"    # Lorg/apache/lucene/queryparser/classic/QueryParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/classic/ParseException;
        }
    .end annotation

    .prologue
    .line 224
    iget-object v0, p0, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser$ComplexPhraseQuery;->phrasedQueryStringContents:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/apache/lucene/queryparser/classic/QueryParser;->parse(Ljava/lang/String;)Lorg/apache/lucene/search/Query;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser$ComplexPhraseQuery;->contents:Lorg/apache/lucene/search/Query;

    .line 225
    return-void
.end method

.method public rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;
    .locals 19
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 230
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser$ComplexPhraseQuery;->contents:Lorg/apache/lucene/search/Query;

    instance-of v15, v15, Lorg/apache/lucene/search/TermQuery;

    if-eqz v15, :cond_0

    .line 231
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser$ComplexPhraseQuery;->contents:Lorg/apache/lucene/search/Query;

    .line 311
    :goto_0
    return-object v13

    .line 236
    :cond_0
    const/4 v9, 0x0

    .line 237
    .local v9, "numNegatives":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser$ComplexPhraseQuery;->contents:Lorg/apache/lucene/search/Query;

    instance-of v15, v15, Lorg/apache/lucene/search/BooleanQuery;

    if-nez v15, :cond_1

    .line 238
    new-instance v15, Ljava/lang/IllegalArgumentException;

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "Unknown query type \""

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 239
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser$ComplexPhraseQuery;->contents:Lorg/apache/lucene/search/Query;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    .line 240
    const-string v17, "\" found in phrase query string \""

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser$ComplexPhraseQuery;->phrasedQueryStringContents:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    .line 241
    const-string v17, "\""

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 238
    invoke-direct/range {v15 .. v16}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v15

    .line 243
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser$ComplexPhraseQuery;->contents:Lorg/apache/lucene/search/Query;

    check-cast v3, Lorg/apache/lucene/search/BooleanQuery;

    .line 244
    .local v3, "bq":Lorg/apache/lucene/search/BooleanQuery;
    invoke-virtual {v3}, Lorg/apache/lucene/search/BooleanQuery;->getClauses()[Lorg/apache/lucene/search/BooleanClause;

    move-result-object v2

    .line 245
    .local v2, "bclauses":[Lorg/apache/lucene/search/BooleanClause;
    array-length v15, v2

    new-array v1, v15, [Lorg/apache/lucene/search/spans/SpanQuery;

    .line 247
    .local v1, "allSpanClauses":[Lorg/apache/lucene/search/spans/SpanQuery;
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    array-length v15, v2

    if-lt v5, v15, :cond_2

    .line 281
    if-nez v9, :cond_7

    .line 283
    new-instance v13, Lorg/apache/lucene/search/spans/SpanNearQuery;

    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser$ComplexPhraseQuery;->slopFactor:I

    const/16 v16, 0x1

    move/from16 v0, v16

    invoke-direct {v13, v1, v15, v0}, Lorg/apache/lucene/search/spans/SpanNearQuery;-><init>([Lorg/apache/lucene/search/spans/SpanQuery;IZ)V

    goto :goto_0

    .line 249
    :cond_2
    aget-object v15, v2, v5

    invoke-virtual {v15}, Lorg/apache/lucene/search/BooleanClause;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v11

    .line 251
    .local v11, "qc":Lorg/apache/lucene/search/Query;
    move-object/from16 v0, p1

    invoke-virtual {v11, v0}, Lorg/apache/lucene/search/Query;->rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;

    move-result-object v11

    .line 252
    aget-object v15, v2, v5

    invoke-virtual {v15}, Lorg/apache/lucene/search/BooleanClause;->getOccur()Lorg/apache/lucene/search/BooleanClause$Occur;

    move-result-object v15

    sget-object v16, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST_NOT:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual/range {v15 .. v16}, Lorg/apache/lucene/search/BooleanClause$Occur;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 253
    add-int/lit8 v9, v9, 0x1

    .line 256
    :cond_3
    instance-of v15, v11, Lorg/apache/lucene/search/BooleanQuery;

    if-eqz v15, :cond_5

    .line 257
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 258
    .local v12, "sc":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/search/spans/SpanQuery;>;"
    check-cast v11, Lorg/apache/lucene/search/BooleanQuery;

    .end local v11    # "qc":Lorg/apache/lucene/search/Query;
    move-object/from16 v0, p0

    invoke-direct {v0, v12, v11}, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser$ComplexPhraseQuery;->addComplexPhraseClause(Ljava/util/List;Lorg/apache/lucene/search/BooleanQuery;)V

    .line 259
    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v15

    if-lez v15, :cond_4

    .line 260
    const/4 v15, 0x0

    invoke-virtual {v12, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lorg/apache/lucene/search/spans/SpanQuery;

    aput-object v15, v1, v5

    .line 247
    .end local v12    # "sc":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/search/spans/SpanQuery;>;"
    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 265
    .restart local v12    # "sc":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/search/spans/SpanQuery;>;"
    :cond_4
    new-instance v15, Lorg/apache/lucene/search/spans/SpanTermQuery;

    new-instance v16, Lorg/apache/lucene/index/Term;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser$ComplexPhraseQuery;->field:Ljava/lang/String;

    move-object/from16 v17, v0

    .line 266
    const-string v18, "Dummy clause because no terms found - must match nothing"

    invoke-direct/range {v16 .. v18}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct/range {v15 .. v16}, Lorg/apache/lucene/search/spans/SpanTermQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    .line 265
    aput-object v15, v1, v5

    goto :goto_2

    .line 269
    .end local v12    # "sc":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/search/spans/SpanQuery;>;"
    .restart local v11    # "qc":Lorg/apache/lucene/search/Query;
    :cond_5
    instance-of v15, v11, Lorg/apache/lucene/search/TermQuery;

    if-eqz v15, :cond_6

    move-object v14, v11

    .line 270
    check-cast v14, Lorg/apache/lucene/search/TermQuery;

    .line 271
    .local v14, "tq":Lorg/apache/lucene/search/TermQuery;
    new-instance v15, Lorg/apache/lucene/search/spans/SpanTermQuery;

    invoke-virtual {v14}, Lorg/apache/lucene/search/TermQuery;->getTerm()Lorg/apache/lucene/index/Term;

    move-result-object v16

    invoke-direct/range {v15 .. v16}, Lorg/apache/lucene/search/spans/SpanTermQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    aput-object v15, v1, v5

    goto :goto_2

    .line 273
    .end local v14    # "tq":Lorg/apache/lucene/search/TermQuery;
    :cond_6
    new-instance v15, Ljava/lang/IllegalArgumentException;

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "Unknown query type \""

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 274
    invoke-virtual {v11}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    .line 275
    const-string v17, "\" found in phrase query string \""

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    .line 276
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser$ComplexPhraseQuery;->phrasedQueryStringContents:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "\""

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 273
    invoke-direct/range {v15 .. v16}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v15

    .line 288
    .end local v11    # "qc":Lorg/apache/lucene/search/Query;
    :cond_7
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 289
    .local v10, "positiveClauses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/search/spans/SpanQuery;>;"
    const/4 v8, 0x0

    .local v8, "j":I
    :goto_3
    array-length v15, v1

    if-lt v8, v15, :cond_8

    .line 296
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v15

    new-array v15, v15, [Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v10, v15}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Lorg/apache/lucene/search/spans/SpanQuery;

    .line 298
    .local v7, "includeClauses":[Lorg/apache/lucene/search/spans/SpanQuery;
    const/4 v6, 0x0

    .line 299
    .local v6, "include":Lorg/apache/lucene/search/spans/SpanQuery;
    array-length v15, v7

    const/16 v16, 0x1

    move/from16 v0, v16

    if-ne v15, v0, :cond_a

    .line 300
    const/4 v15, 0x0

    aget-object v6, v7, v15

    .line 308
    :goto_4
    new-instance v4, Lorg/apache/lucene/search/spans/SpanNearQuery;

    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser$ComplexPhraseQuery;->slopFactor:I

    .line 309
    const/16 v16, 0x1

    .line 308
    move/from16 v0, v16

    invoke-direct {v4, v1, v15, v0}, Lorg/apache/lucene/search/spans/SpanNearQuery;-><init>([Lorg/apache/lucene/search/spans/SpanQuery;IZ)V

    .line 310
    .local v4, "exclude":Lorg/apache/lucene/search/spans/SpanNearQuery;
    new-instance v13, Lorg/apache/lucene/search/spans/SpanNotQuery;

    invoke-direct {v13, v6, v4}, Lorg/apache/lucene/search/spans/SpanNotQuery;-><init>(Lorg/apache/lucene/search/spans/SpanQuery;Lorg/apache/lucene/search/spans/SpanQuery;)V

    .line 311
    .local v13, "snot":Lorg/apache/lucene/search/spans/SpanNotQuery;
    goto/16 :goto_0

    .line 290
    .end local v4    # "exclude":Lorg/apache/lucene/search/spans/SpanNearQuery;
    .end local v6    # "include":Lorg/apache/lucene/search/spans/SpanQuery;
    .end local v7    # "includeClauses":[Lorg/apache/lucene/search/spans/SpanQuery;
    .end local v13    # "snot":Lorg/apache/lucene/search/spans/SpanNotQuery;
    :cond_8
    aget-object v15, v2, v8

    invoke-virtual {v15}, Lorg/apache/lucene/search/BooleanClause;->getOccur()Lorg/apache/lucene/search/BooleanClause$Occur;

    move-result-object v15

    sget-object v16, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST_NOT:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual/range {v15 .. v16}, Lorg/apache/lucene/search/BooleanClause$Occur;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_9

    .line 291
    aget-object v15, v1, v8

    invoke-virtual {v10, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 289
    :cond_9
    add-int/lit8 v8, v8, 0x1

    goto :goto_3

    .line 304
    .restart local v6    # "include":Lorg/apache/lucene/search/spans/SpanQuery;
    .restart local v7    # "includeClauses":[Lorg/apache/lucene/search/spans/SpanQuery;
    :cond_a
    new-instance v6, Lorg/apache/lucene/search/spans/SpanNearQuery;

    .end local v6    # "include":Lorg/apache/lucene/search/spans/SpanQuery;
    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser$ComplexPhraseQuery;->slopFactor:I

    add-int/2addr v15, v9

    .line 305
    const/16 v16, 0x1

    .line 304
    move/from16 v0, v16

    invoke-direct {v6, v7, v15, v0}, Lorg/apache/lucene/search/spans/SpanNearQuery;-><init>([Lorg/apache/lucene/search/spans/SpanQuery;IZ)V

    .restart local v6    # "include":Lorg/apache/lucene/search/spans/SpanQuery;
    goto :goto_4
.end method

.method public toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 360
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "\""

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/queryparser/complexPhrase/ComplexPhraseQueryParser$ComplexPhraseQuery;->phrasedQueryStringContents:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
