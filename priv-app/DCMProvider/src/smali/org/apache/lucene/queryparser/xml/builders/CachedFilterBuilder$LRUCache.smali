.class Lorg/apache/lucene/queryparser/xml/builders/CachedFilterBuilder$LRUCache;
.super Ljava/util/LinkedHashMap;
.source "CachedFilterBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/queryparser/xml/builders/CachedFilterBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "LRUCache"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/LinkedHashMap",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field protected maxsize:I


# direct methods
.method public constructor <init>(I)V
    .locals 3
    .param p1, "maxsize"    # I

    .prologue
    .line 103
    .local p0, "this":Lorg/apache/lucene/queryparser/xml/builders/CachedFilterBuilder$LRUCache;, "Lorg/apache/lucene/queryparser/xml/builders/CachedFilterBuilder$LRUCache<TK;TV;>;"
    mul-int/lit8 v0, p1, 0x4

    div-int/lit8 v0, v0, 0x3

    add-int/lit8 v0, v0, 0x1

    const/high16 v1, 0x3f400000    # 0.75f

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Ljava/util/LinkedHashMap;-><init>(IFZ)V

    .line 104
    iput p1, p0, Lorg/apache/lucene/queryparser/xml/builders/CachedFilterBuilder$LRUCache;->maxsize:I

    .line 105
    return-void
.end method


# virtual methods
.method protected removeEldestEntry(Ljava/util/Map$Entry;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;)Z"
        }
    .end annotation

    .prologue
    .line 111
    .local p0, "this":Lorg/apache/lucene/queryparser/xml/builders/CachedFilterBuilder$LRUCache;, "Lorg/apache/lucene/queryparser/xml/builders/CachedFilterBuilder$LRUCache<TK;TV;>;"
    .local p1, "eldest":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<TK;TV;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/xml/builders/CachedFilterBuilder$LRUCache;->size()I

    move-result v0

    iget v1, p0, Lorg/apache/lucene/queryparser/xml/builders/CachedFilterBuilder$LRUCache;->maxsize:I

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
