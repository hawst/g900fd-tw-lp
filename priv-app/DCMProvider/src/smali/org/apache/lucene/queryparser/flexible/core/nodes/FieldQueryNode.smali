.class public Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
.super Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;
.source "FieldQueryNode.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldValuePairQueryNode;
.implements Lorg/apache/lucene/queryparser/flexible/core/nodes/TextableQueryNode;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;",
        "Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldValuePairQueryNode",
        "<",
        "Ljava/lang/CharSequence;",
        ">;",
        "Lorg/apache/lucene/queryparser/flexible/core/nodes/TextableQueryNode;"
    }
.end annotation


# instance fields
.field protected begin:I

.field protected end:I

.field protected field:Ljava/lang/CharSequence;

.field protected positionIncrement:I

.field protected text:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;II)V
    .locals 1
    .param p1, "field"    # Ljava/lang/CharSequence;
    .param p2, "text"    # Ljava/lang/CharSequence;
    .param p3, "begin"    # I
    .param p4, "end"    # I

    .prologue
    .line 65
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;-><init>()V

    .line 67
    iput-object p1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->field:Ljava/lang/CharSequence;

    .line 68
    iput-object p2, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->text:Ljava/lang/CharSequence;

    .line 69
    iput p3, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->begin:I

    .line 70
    iput p4, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->end:I

    .line 71
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->setLeaf(Z)V

    .line 73
    return-void
.end method


# virtual methods
.method public cloneTree()Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 175
    invoke-super {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;->cloneTree()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;

    .line 176
    .local v0, "fqn":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    iget v1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->begin:I

    iput v1, v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->begin:I

    .line 177
    iget v1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->end:I

    iput v1, v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->end:I

    .line 178
    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->field:Ljava/lang/CharSequence;

    iput-object v1, v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->field:Ljava/lang/CharSequence;

    .line 179
    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->text:Ljava/lang/CharSequence;

    iput-object v1, v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->text:Ljava/lang/CharSequence;

    .line 180
    iget v1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->positionIncrement:I

    iput v1, v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->positionIncrement:I

    .line 181
    iget-boolean v1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->toQueryStringIgnoreFields:Z

    iput-boolean v1, v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->toQueryStringIgnoreFields:Z

    .line 183
    return-object v0
.end method

.method public bridge synthetic cloneTree()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->cloneTree()Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;

    move-result-object v0

    return-object v0
.end method

.method public getBegin()I
    .locals 1

    .prologue
    .line 121
    iget v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->begin:I

    return v0
.end method

.method public getEnd()I
    .locals 1

    .prologue
    .line 129
    iget v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->end:I

    return v0
.end method

.method public getField()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->field:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getFieldAsString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->field:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    .line 115
    const/4 v0, 0x0

    .line 117
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->field:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getPositionIncrement()I
    .locals 1

    .prologue
    .line 147
    iget v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->positionIncrement:I

    return v0
.end method

.method protected getTermEscapeQuoted(Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;)Ljava/lang/CharSequence;
    .locals 3
    .param p1, "escaper"    # Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;

    .prologue
    .line 80
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->text:Ljava/lang/CharSequence;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    sget-object v2, Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax$Type;->STRING:Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax$Type;

    invoke-interface {p1, v0, v1, v2}, Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;->escape(Ljava/lang/CharSequence;Ljava/util/Locale;Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax$Type;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method protected getTermEscaped(Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;)Ljava/lang/CharSequence;
    .locals 3
    .param p1, "escaper"    # Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;

    .prologue
    .line 76
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->text:Ljava/lang/CharSequence;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    sget-object v2, Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax$Type;->NORMAL:Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax$Type;

    invoke-interface {p1, v0, v1, v2}, Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;->escape(Ljava/lang/CharSequence;Ljava/util/Locale;Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax$Type;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getText()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->text:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getTextAsString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->text:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    .line 103
    const/4 v0, 0x0

    .line 105
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->text:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getValue()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 189
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getValue()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->getValue()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public setBegin(I)V
    .locals 0
    .param p1, "begin"    # I

    .prologue
    .line 125
    iput p1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->begin:I

    .line 126
    return-void
.end method

.method public setEnd(I)V
    .locals 0
    .param p1, "end"    # I

    .prologue
    .line 133
    iput p1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->end:I

    .line 134
    return-void
.end method

.method public setField(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "field"    # Ljava/lang/CharSequence;

    .prologue
    .line 143
    iput-object p1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->field:Ljava/lang/CharSequence;

    .line 144
    return-void
.end method

.method public setPositionIncrement(I)V
    .locals 0
    .param p1, "pi"    # I

    .prologue
    .line 151
    iput p1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->positionIncrement:I

    .line 152
    return-void
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 170
    iput-object p1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->text:Ljava/lang/CharSequence;

    .line 171
    return-void
.end method

.method public setValue(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/CharSequence;

    .prologue
    .line 194
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->setText(Ljava/lang/CharSequence;)V

    .line 195
    return-void
.end method

.method public bridge synthetic setValue(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->setValue(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public toQueryString(Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;)Ljava/lang/CharSequence;
    .locals 2
    .param p1, "escaper"    # Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;

    .prologue
    .line 85
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->field:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->isDefaultField(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 86
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->getTermEscaped(Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 88
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->field:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->getTermEscaped(Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 94
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "<field start=\'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->begin:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\' end=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->end:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\' field=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 95
    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->field:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\' text=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->text:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'/>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 94
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
