.class public Lorg/apache/lucene/queryparser/flexible/standard/processors/AnalyzerQueryNodeProcessor;
.super Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl;
.source "AnalyzerQueryNodeProcessor.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private analyzer:Lorg/apache/lucene/analysis/Analyzer;

.field private positionIncrementsEnabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 71
    const-class v0, Lorg/apache/lucene/queryparser/flexible/standard/processors/AnalyzerQueryNodeProcessor;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/queryparser/flexible/standard/processors/AnalyzerQueryNodeProcessor;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl;-><init>()V

    .line 79
    return-void
.end method


# virtual methods
.method protected postProcessNode(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 30
    .param p1, "node"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 107
    move-object/from16 v0, p1

    instance-of v0, v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/TextableQueryNode;

    move/from16 v27, v0

    if-eqz v27, :cond_1b

    .line 108
    move-object/from16 v0, p1

    instance-of v0, v0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/WildcardQueryNode;

    move/from16 v27, v0

    if-nez v27, :cond_1b

    .line 109
    move-object/from16 v0, p1

    instance-of v0, v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FuzzyQueryNode;

    move/from16 v27, v0

    if-nez v27, :cond_1b

    .line 110
    move-object/from16 v0, p1

    instance-of v0, v0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/RegexpQueryNode;

    move/from16 v27, v0

    if-nez v27, :cond_1b

    .line 111
    invoke-interface/range {p1 .. p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->getParent()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v27

    move-object/from16 v0, v27

    instance-of v0, v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/RangeQueryNode;

    move/from16 v27, v0

    if-nez v27, :cond_1b

    move-object/from16 v8, p1

    .line 113
    check-cast v8, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;

    .line 114
    .local v8, "fieldNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    invoke-virtual {v8}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->getTextAsString()Ljava/lang/String;

    move-result-object v26

    .line 115
    .local v26, "text":Ljava/lang/String;
    invoke-virtual {v8}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->getFieldAsString()Ljava/lang/String;

    move-result-object v7

    .line 119
    .local v7, "field":Ljava/lang/String;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/queryparser/flexible/standard/processors/AnalyzerQueryNodeProcessor;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    move-object/from16 v27, v0

    new-instance v28, Ljava/io/StringReader;

    move-object/from16 v0, v28

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    invoke-virtual {v0, v7, v1}, Lorg/apache/lucene/analysis/Analyzer;->tokenStream(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/TokenStream;

    move-result-object v21

    .line 120
    .local v21, "source":Lorg/apache/lucene/analysis/TokenStream;
    invoke-virtual/range {v21 .. v21}, Lorg/apache/lucene/analysis/TokenStream;->reset()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 124
    new-instance v4, Lorg/apache/lucene/analysis/CachingTokenFilter;

    move-object/from16 v0, v21

    invoke-direct {v4, v0}, Lorg/apache/lucene/analysis/CachingTokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 126
    .local v4, "buffer":Lorg/apache/lucene/analysis/CachingTokenFilter;
    const/4 v15, 0x0

    .line 127
    .local v15, "posIncrAtt":Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;
    const/4 v14, 0x0

    .line 128
    .local v14, "numTokens":I
    const/16 v17, 0x0

    .line 129
    .local v17, "positionCount":I
    const/16 v20, 0x0

    .line 131
    .local v20, "severalTokensAtSamePosition":Z
    const-class v27, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    move-object/from16 v0, v27

    invoke-virtual {v4, v0}, Lorg/apache/lucene/analysis/CachingTokenFilter;->hasAttribute(Ljava/lang/Class;)Z

    move-result v27

    if-eqz v27, :cond_0

    .line 132
    const-class v27, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    move-object/from16 v0, v27

    invoke-virtual {v4, v0}, Lorg/apache/lucene/analysis/CachingTokenFilter;->getAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v15

    .end local v15    # "posIncrAtt":Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;
    check-cast v15, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    .line 137
    .restart local v15    # "posIncrAtt":Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;
    :cond_0
    :goto_0
    :try_start_1
    invoke-virtual {v4}, Lorg/apache/lucene/analysis/CachingTokenFilter;->incrementToken()Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6

    move-result v27

    if-nez v27, :cond_1

    .line 156
    :goto_1
    :try_start_2
    invoke-virtual {v4}, Lorg/apache/lucene/analysis/CachingTokenFilter;->reset()V

    .line 159
    invoke-virtual/range {v21 .. v21}, Lorg/apache/lucene/analysis/TokenStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5

    .line 164
    :goto_2
    const-class v27, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    move-object/from16 v0, v27

    invoke-virtual {v4, v0}, Lorg/apache/lucene/analysis/CachingTokenFilter;->hasAttribute(Ljava/lang/Class;)Z

    move-result v27

    if-nez v27, :cond_4

    .line 165
    new-instance v8, Lorg/apache/lucene/queryparser/flexible/core/nodes/NoTokenFoundQueryNode;

    .end local v8    # "fieldNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    invoke-direct {v8}, Lorg/apache/lucene/queryparser/flexible/core/nodes/NoTokenFoundQueryNode;-><init>()V

    .line 320
    .end local v4    # "buffer":Lorg/apache/lucene/analysis/CachingTokenFilter;
    .end local v7    # "field":Ljava/lang/String;
    .end local v14    # "numTokens":I
    .end local v15    # "posIncrAtt":Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;
    .end local v17    # "positionCount":I
    .end local v20    # "severalTokensAtSamePosition":Z
    .end local v21    # "source":Lorg/apache/lucene/analysis/TokenStream;
    .end local v26    # "text":Ljava/lang/String;
    :goto_3
    return-object v8

    .line 121
    .restart local v7    # "field":Ljava/lang/String;
    .restart local v8    # "fieldNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    .restart local v26    # "text":Ljava/lang/String;
    :catch_0
    move-exception v6

    .line 122
    .local v6, "e1":Ljava/io/IOException;
    new-instance v27, Ljava/lang/RuntimeException;

    move-object/from16 v0, v27

    invoke-direct {v0, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v27

    .line 138
    .end local v6    # "e1":Ljava/io/IOException;
    .restart local v4    # "buffer":Lorg/apache/lucene/analysis/CachingTokenFilter;
    .restart local v14    # "numTokens":I
    .restart local v15    # "posIncrAtt":Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;
    .restart local v17    # "positionCount":I
    .restart local v20    # "severalTokensAtSamePosition":Z
    .restart local v21    # "source":Lorg/apache/lucene/analysis/TokenStream;
    :cond_1
    add-int/lit8 v14, v14, 0x1

    .line 139
    if-eqz v15, :cond_2

    .line 140
    :try_start_3
    invoke-interface {v15}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->getPositionIncrement()I
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_6

    move-result v18

    .line 141
    .local v18, "positionIncrement":I
    :goto_4
    if-eqz v18, :cond_3

    .line 142
    add-int v17, v17, v18

    .line 144
    goto :goto_0

    .line 140
    .end local v18    # "positionIncrement":I
    :cond_2
    const/16 v18, 0x1

    goto :goto_4

    .line 145
    .restart local v18    # "positionIncrement":I
    :cond_3
    const/16 v20, 0x1

    goto :goto_0

    .line 168
    .end local v18    # "positionIncrement":I
    :cond_4
    const-class v27, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    move-object/from16 v0, v27

    invoke-virtual {v4, v0}, Lorg/apache/lucene/analysis/CachingTokenFilter;->getAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v23

    check-cast v23, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 170
    .local v23, "termAtt":Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
    if-nez v14, :cond_5

    .line 171
    new-instance v8, Lorg/apache/lucene/queryparser/flexible/core/nodes/NoTokenFoundQueryNode;

    .end local v8    # "fieldNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    invoke-direct {v8}, Lorg/apache/lucene/queryparser/flexible/core/nodes/NoTokenFoundQueryNode;-><init>()V

    goto :goto_3

    .line 173
    .restart local v8    # "fieldNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    :cond_5
    const/16 v27, 0x1

    move/from16 v0, v27

    if-ne v14, v0, :cond_7

    .line 174
    const/16 v22, 0x0

    .line 177
    .local v22, "term":Ljava/lang/String;
    :try_start_4
    invoke-virtual {v4}, Lorg/apache/lucene/analysis/CachingTokenFilter;->incrementToken()Z

    move-result v9

    .line 178
    .local v9, "hasNext":Z
    sget-boolean v27, Lorg/apache/lucene/queryparser/flexible/standard/processors/AnalyzerQueryNodeProcessor;->$assertionsDisabled:Z

    if-nez v27, :cond_6

    if-nez v9, :cond_6

    new-instance v27, Ljava/lang/AssertionError;

    invoke-direct/range {v27 .. v27}, Ljava/lang/AssertionError;-><init>()V

    throw v27
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    .line 181
    .end local v9    # "hasNext":Z
    :catch_1
    move-exception v27

    .line 185
    :goto_5
    move-object/from16 v0, v22

    invoke-virtual {v8, v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 179
    .restart local v9    # "hasNext":Z
    :cond_6
    :try_start_5
    invoke-interface/range {v23 .. v23}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->toString()Ljava/lang/String;
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    move-result-object v22

    goto :goto_5

    .line 189
    .end local v9    # "hasNext":Z
    .end local v22    # "term":Ljava/lang/String;
    :cond_7
    if-nez v20, :cond_8

    move-object/from16 v0, p1

    instance-of v0, v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/QuotedFieldQueryNode;

    move/from16 v27, v0

    if-nez v27, :cond_16

    .line 190
    :cond_8
    const/16 v27, 0x1

    move/from16 v0, v17

    move/from16 v1, v27

    if-eq v0, v1, :cond_9

    move-object/from16 v0, p1

    instance-of v0, v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/QuotedFieldQueryNode;

    move/from16 v27, v0

    if-nez v27, :cond_d

    .line 192
    :cond_9
    new-instance v5, Ljava/util/LinkedList;

    invoke-direct {v5}, Ljava/util/LinkedList;-><init>()V

    .line 194
    .local v5, "children":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_6
    if-lt v10, v14, :cond_a

    .line 208
    new-instance v8, Lorg/apache/lucene/queryparser/flexible/core/nodes/GroupQueryNode;

    .line 209
    .end local v8    # "fieldNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    new-instance v28, Lorg/apache/lucene/queryparser/flexible/standard/nodes/StandardBooleanQueryNode;

    const/16 v27, 0x1

    move/from16 v0, v17

    move/from16 v1, v27

    if-ne v0, v1, :cond_c

    const/16 v27, 0x1

    :goto_7
    move-object/from16 v0, v28

    move/from16 v1, v27

    invoke-direct {v0, v5, v1}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/StandardBooleanQueryNode;-><init>(Ljava/util/List;Z)V

    .line 208
    move-object/from16 v0, v28

    invoke-direct {v8, v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/GroupQueryNode;-><init>(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)V

    goto/16 :goto_3

    .line 195
    .restart local v8    # "fieldNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    :cond_a
    const/16 v22, 0x0

    .line 197
    .restart local v22    # "term":Ljava/lang/String;
    :try_start_6
    invoke-virtual {v4}, Lorg/apache/lucene/analysis/CachingTokenFilter;->incrementToken()Z

    move-result v9

    .line 198
    .restart local v9    # "hasNext":Z
    sget-boolean v27, Lorg/apache/lucene/queryparser/flexible/standard/processors/AnalyzerQueryNodeProcessor;->$assertionsDisabled:Z

    if-nez v27, :cond_b

    if-nez v9, :cond_b

    new-instance v27, Ljava/lang/AssertionError;

    invoke-direct/range {v27 .. v27}, Ljava/lang/AssertionError;-><init>()V

    throw v27
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    .line 201
    .end local v9    # "hasNext":Z
    :catch_2
    move-exception v27

    .line 205
    :goto_8
    new-instance v27, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;

    const/16 v28, -0x1

    const/16 v29, -0x1

    move-object/from16 v0, v27

    move-object/from16 v1, v22

    move/from16 v2, v28

    move/from16 v3, v29

    invoke-direct {v0, v7, v1, v2, v3}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;II)V

    move-object/from16 v0, v27

    invoke-virtual {v5, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 194
    add-int/lit8 v10, v10, 0x1

    goto :goto_6

    .line 199
    .restart local v9    # "hasNext":Z
    :cond_b
    :try_start_7
    invoke-interface/range {v23 .. v23}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->toString()Ljava/lang/String;
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    move-result-object v22

    goto :goto_8

    .line 209
    .end local v8    # "fieldNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    .end local v9    # "hasNext":Z
    .end local v22    # "term":Ljava/lang/String;
    :cond_c
    const/16 v27, 0x0

    goto :goto_7

    .line 212
    .end local v5    # "children":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    .end local v10    # "i":I
    .restart local v8    # "fieldNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    :cond_d
    new-instance v11, Lorg/apache/lucene/queryparser/flexible/standard/nodes/MultiPhraseQueryNode;

    invoke-direct {v11}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/MultiPhraseQueryNode;-><init>()V

    .line 214
    .local v11, "mpq":Lorg/apache/lucene/queryparser/flexible/standard/nodes/MultiPhraseQueryNode;
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 215
    .local v12, "multiTerms":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;>;"
    const/16 v16, -0x1

    .line 216
    .local v16, "position":I
    const/4 v10, 0x0

    .line 217
    .restart local v10    # "i":I
    const/16 v24, 0x0

    .line 218
    .local v24, "termGroupCount":I
    :goto_9
    if-lt v10, v14, :cond_e

    .line 260
    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v27

    :goto_a
    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->hasNext()Z

    move-result v28

    if-nez v28, :cond_14

    move-object v8, v11

    .line 273
    goto/16 :goto_3

    .line 219
    :cond_e
    const/16 v22, 0x0

    .line 220
    .restart local v22    # "term":Ljava/lang/String;
    const/16 v18, 0x1

    .line 222
    .restart local v18    # "positionIncrement":I
    :try_start_8
    invoke-virtual {v4}, Lorg/apache/lucene/analysis/CachingTokenFilter;->incrementToken()Z

    move-result v9

    .line 223
    .restart local v9    # "hasNext":Z
    sget-boolean v27, Lorg/apache/lucene/queryparser/flexible/standard/processors/AnalyzerQueryNodeProcessor;->$assertionsDisabled:Z

    if-nez v27, :cond_11

    if-nez v9, :cond_11

    new-instance v27, Ljava/lang/AssertionError;

    invoke-direct/range {v27 .. v27}, Ljava/lang/AssertionError;-><init>()V

    throw v27
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3

    .line 229
    .end local v9    # "hasNext":Z
    :catch_3
    move-exception v27

    .line 233
    :cond_f
    :goto_b
    if-lez v18, :cond_10

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v27

    if-lez v27, :cond_10

    .line 235
    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v27

    :goto_c
    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->hasNext()Z

    move-result v28

    if-nez v28, :cond_12

    .line 249
    add-int/lit8 v24, v24, 0x1

    .line 251
    invoke-interface {v12}, Ljava/util/List;->clear()V

    .line 255
    :cond_10
    add-int v16, v16, v18

    .line 256
    new-instance v27, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;

    const/16 v28, -0x1

    const/16 v29, -0x1

    move-object/from16 v0, v27

    move-object/from16 v1, v22

    move/from16 v2, v28

    move/from16 v3, v29

    invoke-direct {v0, v7, v1, v2, v3}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;II)V

    move-object/from16 v0, v27

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 218
    add-int/lit8 v10, v10, 0x1

    goto :goto_9

    .line 224
    .restart local v9    # "hasNext":Z
    :cond_11
    :try_start_9
    invoke-interface/range {v23 .. v23}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->toString()Ljava/lang/String;

    move-result-object v22

    .line 225
    if-eqz v15, :cond_f

    .line 226
    invoke-interface {v15}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->getPositionIncrement()I
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3

    move-result v18

    goto :goto_b

    .line 235
    .end local v9    # "hasNext":Z
    :cond_12
    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;

    .line 237
    .local v25, "termNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/lucene/queryparser/flexible/standard/processors/AnalyzerQueryNodeProcessor;->positionIncrementsEnabled:Z

    move/from16 v28, v0

    if-eqz v28, :cond_13

    .line 238
    move-object/from16 v0, v25

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->setPositionIncrement(I)V

    .line 243
    :goto_d
    move-object/from16 v0, v25

    invoke-virtual {v11, v0}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/MultiPhraseQueryNode;->add(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)V

    goto :goto_c

    .line 240
    :cond_13
    move-object/from16 v0, v25

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->setPositionIncrement(I)V

    goto :goto_d

    .line 260
    .end local v18    # "positionIncrement":I
    .end local v22    # "term":Ljava/lang/String;
    .end local v25    # "termNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    :cond_14
    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;

    .line 262
    .restart local v25    # "termNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/lucene/queryparser/flexible/standard/processors/AnalyzerQueryNodeProcessor;->positionIncrementsEnabled:Z

    move/from16 v28, v0

    if-eqz v28, :cond_15

    .line 263
    move-object/from16 v0, v25

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->setPositionIncrement(I)V

    .line 269
    :goto_e
    move-object/from16 v0, v25

    invoke-virtual {v11, v0}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/MultiPhraseQueryNode;->add(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)V

    goto/16 :goto_a

    .line 266
    :cond_15
    move-object/from16 v0, v25

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->setPositionIncrement(I)V

    goto :goto_e

    .line 279
    .end local v10    # "i":I
    .end local v11    # "mpq":Lorg/apache/lucene/queryparser/flexible/standard/nodes/MultiPhraseQueryNode;
    .end local v12    # "multiTerms":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;>;"
    .end local v16    # "position":I
    .end local v24    # "termGroupCount":I
    .end local v25    # "termNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    :cond_16
    new-instance v19, Lorg/apache/lucene/queryparser/flexible/core/nodes/TokenizedPhraseQueryNode;

    invoke-direct/range {v19 .. v19}, Lorg/apache/lucene/queryparser/flexible/core/nodes/TokenizedPhraseQueryNode;-><init>()V

    .line 281
    .local v19, "pq":Lorg/apache/lucene/queryparser/flexible/core/nodes/TokenizedPhraseQueryNode;
    const/16 v16, -0x1

    .line 283
    .restart local v16    # "position":I
    const/4 v10, 0x0

    .restart local v10    # "i":I
    :goto_f
    if-lt v10, v14, :cond_17

    move-object/from16 v8, v19

    .line 314
    goto/16 :goto_3

    .line 284
    :cond_17
    const/16 v22, 0x0

    .line 285
    .restart local v22    # "term":Ljava/lang/String;
    const/16 v18, 0x1

    .line 288
    .restart local v18    # "positionIncrement":I
    :try_start_a
    invoke-virtual {v4}, Lorg/apache/lucene/analysis/CachingTokenFilter;->incrementToken()Z

    move-result v9

    .line 289
    .restart local v9    # "hasNext":Z
    sget-boolean v27, Lorg/apache/lucene/queryparser/flexible/standard/processors/AnalyzerQueryNodeProcessor;->$assertionsDisabled:Z

    if-nez v27, :cond_19

    if-nez v9, :cond_19

    new-instance v27, Ljava/lang/AssertionError;

    invoke-direct/range {v27 .. v27}, Ljava/lang/AssertionError;-><init>()V

    throw v27
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_4

    .line 296
    .end local v9    # "hasNext":Z
    :catch_4
    move-exception v27

    .line 300
    :cond_18
    :goto_10
    new-instance v13, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;

    const/16 v27, -0x1

    const/16 v28, -0x1

    move-object/from16 v0, v22

    move/from16 v1, v27

    move/from16 v2, v28

    invoke-direct {v13, v7, v0, v1, v2}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;II)V

    .line 302
    .local v13, "newFieldNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/lucene/queryparser/flexible/standard/processors/AnalyzerQueryNodeProcessor;->positionIncrementsEnabled:Z

    move/from16 v27, v0

    if-eqz v27, :cond_1a

    .line 303
    add-int v16, v16, v18

    .line 304
    move/from16 v0, v16

    invoke-virtual {v13, v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->setPositionIncrement(I)V

    .line 310
    :goto_11
    move-object/from16 v0, v19

    invoke-virtual {v0, v13}, Lorg/apache/lucene/queryparser/flexible/core/nodes/TokenizedPhraseQueryNode;->add(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)V

    .line 283
    add-int/lit8 v10, v10, 0x1

    goto :goto_f

    .line 290
    .end local v13    # "newFieldNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    .restart local v9    # "hasNext":Z
    :cond_19
    :try_start_b
    invoke-interface/range {v23 .. v23}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->toString()Ljava/lang/String;

    move-result-object v22

    .line 292
    if-eqz v15, :cond_18

    .line 293
    invoke-interface {v15}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->getPositionIncrement()I
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_4

    move-result v18

    goto :goto_10

    .line 307
    .end local v9    # "hasNext":Z
    .restart local v13    # "newFieldNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    :cond_1a
    invoke-virtual {v13, v10}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->setPositionIncrement(I)V

    goto :goto_11

    .end local v4    # "buffer":Lorg/apache/lucene/analysis/CachingTokenFilter;
    .end local v7    # "field":Ljava/lang/String;
    .end local v8    # "fieldNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    .end local v10    # "i":I
    .end local v13    # "newFieldNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    .end local v14    # "numTokens":I
    .end local v15    # "posIncrAtt":Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;
    .end local v16    # "position":I
    .end local v17    # "positionCount":I
    .end local v18    # "positionIncrement":I
    .end local v19    # "pq":Lorg/apache/lucene/queryparser/flexible/core/nodes/TokenizedPhraseQueryNode;
    .end local v20    # "severalTokensAtSamePosition":Z
    .end local v21    # "source":Lorg/apache/lucene/analysis/TokenStream;
    .end local v22    # "term":Ljava/lang/String;
    .end local v23    # "termAtt":Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
    .end local v26    # "text":Ljava/lang/String;
    :cond_1b
    move-object/from16 v8, p1

    .line 320
    goto/16 :goto_3

    .line 160
    .restart local v4    # "buffer":Lorg/apache/lucene/analysis/CachingTokenFilter;
    .restart local v7    # "field":Ljava/lang/String;
    .restart local v8    # "fieldNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    .restart local v14    # "numTokens":I
    .restart local v15    # "posIncrAtt":Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;
    .restart local v17    # "positionCount":I
    .restart local v20    # "severalTokensAtSamePosition":Z
    .restart local v21    # "source":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v26    # "text":Ljava/lang/String;
    :catch_5
    move-exception v27

    goto/16 :goto_2

    .line 150
    :catch_6
    move-exception v27

    goto/16 :goto_1
.end method

.method protected preProcessNode(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 0
    .param p1, "node"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 327
    return-object p1
.end method

.method public process(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 4
    .param p1, "queryTree"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 83
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/processors/AnalyzerQueryNodeProcessor;->getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    move-result-object v2

    sget-object v3, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->ANALYZER:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->get(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/Analyzer;

    .line 85
    .local v0, "analyzer":Lorg/apache/lucene/analysis/Analyzer;
    if-eqz v0, :cond_1

    .line 86
    iput-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/processors/AnalyzerQueryNodeProcessor;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    .line 87
    const/4 v2, 0x0

    iput-boolean v2, p0, Lorg/apache/lucene/queryparser/flexible/standard/processors/AnalyzerQueryNodeProcessor;->positionIncrementsEnabled:Z

    .line 88
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/processors/AnalyzerQueryNodeProcessor;->getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    move-result-object v2

    sget-object v3, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->ENABLE_POSITION_INCREMENTS:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->get(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    .line 90
    .local v1, "positionIncrementsEnabled":Ljava/lang/Boolean;
    if-eqz v1, :cond_0

    .line 91
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    iput-boolean v2, p0, Lorg/apache/lucene/queryparser/flexible/standard/processors/AnalyzerQueryNodeProcessor;->positionIncrementsEnabled:Z

    .line 94
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/queryparser/flexible/standard/processors/AnalyzerQueryNodeProcessor;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    if-eqz v2, :cond_1

    .line 95
    invoke-super {p0, p1}, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl;->process(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object p1

    .line 100
    .end local v1    # "positionIncrementsEnabled":Ljava/lang/Boolean;
    .end local p1    # "queryTree":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    :cond_1
    return-object p1
.end method

.method protected setChildrenOrder(Ljava/util/List;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 335
    .local p1, "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    return-object p1
.end method
