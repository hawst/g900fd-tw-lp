.class public Lorg/apache/lucene/queryparser/flexible/core/builders/QueryTreeBuilder;
.super Ljava/lang/Object;
.source "QueryTreeBuilder.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/flexible/core/builders/QueryBuilder;


# static fields
.field public static final QUERY_TREE_BUILDER_TAGID:Ljava/lang/String;


# instance fields
.field private fieldNameBuilders:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/queryparser/flexible/core/builders/QueryBuilder;",
            ">;"
        }
    .end annotation
.end field

.field private queryNodeBuilders:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;",
            ">;",
            "Lorg/apache/lucene/queryparser/flexible/core/builders/QueryBuilder;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    const-class v0, Lorg/apache/lucene/queryparser/flexible/core/builders/QueryTreeBuilder;

    .line 60
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    .line 59
    sput-object v0, Lorg/apache/lucene/queryparser/flexible/core/builders/QueryTreeBuilder;->QUERY_TREE_BUILDER_TAGID:Ljava/lang/String;

    .line 60
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    return-void
.end method

.method private getBuilder(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/builders/QueryBuilder;
    .locals 7
    .param p1, "node"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    .prologue
    .line 132
    const/4 v1, 0x0

    .line 134
    .local v1, "builder":Lorg/apache/lucene/queryparser/flexible/core/builders/QueryBuilder;
    iget-object v5, p0, Lorg/apache/lucene/queryparser/flexible/core/builders/QueryTreeBuilder;->fieldNameBuilders:Ljava/util/HashMap;

    if-eqz v5, :cond_1

    instance-of v5, p1, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldableNode;

    if-eqz v5, :cond_1

    move-object v5, p1

    .line 135
    check-cast v5, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldableNode;

    invoke-interface {v5}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldableNode;->getField()Ljava/lang/CharSequence;

    move-result-object v4

    .line 137
    .local v4, "field":Ljava/lang/CharSequence;
    if-eqz v4, :cond_0

    .line 138
    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    .line 141
    :cond_0
    iget-object v5, p0, Lorg/apache/lucene/queryparser/flexible/core/builders/QueryTreeBuilder;->fieldNameBuilders:Ljava/util/HashMap;

    invoke-virtual {v5, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "builder":Lorg/apache/lucene/queryparser/flexible/core/builders/QueryBuilder;
    check-cast v1, Lorg/apache/lucene/queryparser/flexible/core/builders/QueryBuilder;

    .line 145
    .end local v4    # "field":Ljava/lang/CharSequence;
    .restart local v1    # "builder":Lorg/apache/lucene/queryparser/flexible/core/builders/QueryBuilder;
    :cond_1
    if-nez v1, :cond_4

    iget-object v5, p0, Lorg/apache/lucene/queryparser/flexible/core/builders/QueryTreeBuilder;->queryNodeBuilders:Ljava/util/HashMap;

    if-eqz v5, :cond_4

    .line 147
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    .line 150
    .local v3, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_2
    invoke-direct {p0, v3}, Lorg/apache/lucene/queryparser/flexible/core/builders/QueryTreeBuilder;->getQueryBuilder(Ljava/lang/Class;)Lorg/apache/lucene/queryparser/flexible/core/builders/QueryBuilder;

    move-result-object v1

    .line 152
    if-nez v1, :cond_3

    .line 153
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getInterfaces()[Ljava/lang/Class;

    move-result-object v2

    .line 155
    .local v2, "classes":[Ljava/lang/Class;
    array-length v6, v2

    const/4 v5, 0x0

    :goto_0
    if-lt v5, v6, :cond_5

    .line 166
    .end local v2    # "classes":[Ljava/lang/Class;
    :cond_3
    if-nez v1, :cond_4

    invoke-virtual {v3}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v3

    if-nez v3, :cond_2

    .line 170
    .end local v3    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_4
    return-object v1

    .line 155
    .restart local v2    # "classes":[Ljava/lang/Class;
    .restart local v3    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_5
    aget-object v0, v2, v5

    .line 156
    .local v0, "actualClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-direct {p0, v0}, Lorg/apache/lucene/queryparser/flexible/core/builders/QueryTreeBuilder;->getQueryBuilder(Ljava/lang/Class;)Lorg/apache/lucene/queryparser/flexible/core/builders/QueryBuilder;

    move-result-object v1

    .line 158
    if-nez v1, :cond_3

    .line 155
    add-int/lit8 v5, v5, 0x1

    goto :goto_0
.end method

.method private getQueryBuilder(Ljava/lang/Class;)Lorg/apache/lucene/queryparser/flexible/core/builders/QueryBuilder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Lorg/apache/lucene/queryparser/flexible/core/builders/QueryBuilder;"
        }
    .end annotation

    .prologue
    .line 196
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-class v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    invoke-virtual {v0, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 197
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/builders/QueryTreeBuilder;->queryNodeBuilders:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/builders/QueryBuilder;

    .line 200
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private process(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)V
    .locals 5
    .param p1, "node"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 109
    if-eqz p1, :cond_1

    .line 110
    invoke-direct {p0, p1}, Lorg/apache/lucene/queryparser/flexible/core/builders/QueryTreeBuilder;->getBuilder(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/builders/QueryBuilder;

    move-result-object v0

    .line 112
    .local v0, "builder":Lorg/apache/lucene/queryparser/flexible/core/builders/QueryBuilder;
    instance-of v3, v0, Lorg/apache/lucene/queryparser/flexible/core/builders/QueryTreeBuilder;

    if-nez v3, :cond_0

    .line 113
    invoke-interface {p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->getChildren()Ljava/util/List;

    move-result-object v2

    .line 115
    .local v2, "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    if-eqz v2, :cond_0

    .line 117
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_2

    .line 125
    .end local v2    # "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    :cond_0
    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/queryparser/flexible/core/builders/QueryTreeBuilder;->processNode(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;Lorg/apache/lucene/queryparser/flexible/core/builders/QueryBuilder;)V

    .line 129
    .end local v0    # "builder":Lorg/apache/lucene/queryparser/flexible/core/builders/QueryBuilder;
    :cond_1
    return-void

    .line 117
    .restart local v0    # "builder":Lorg/apache/lucene/queryparser/flexible/core/builders/QueryBuilder;
    .restart local v2    # "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    .line 118
    .local v1, "child":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    invoke-direct {p0, v1}, Lorg/apache/lucene/queryparser/flexible/core/builders/QueryTreeBuilder;->process(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)V

    goto :goto_0
.end method

.method private processNode(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;Lorg/apache/lucene/queryparser/flexible/core/builders/QueryBuilder;)V
    .locals 7
    .param p1, "node"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .param p2, "builder"    # Lorg/apache/lucene/queryparser/flexible/core/builders/QueryBuilder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 177
    if-nez p2, :cond_0

    .line 179
    new-instance v1, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;

    new-instance v2, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;

    .line 180
    sget-object v3, Lorg/apache/lucene/queryparser/flexible/core/messages/QueryParserMessages;->LUCENE_QUERY_CONVERSION_ERROR:Ljava/lang/String;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 181
    new-instance v6, Lorg/apache/lucene/queryparser/flexible/standard/parser/EscapeQuerySyntaxImpl;

    invoke-direct {v6}, Lorg/apache/lucene/queryparser/flexible/standard/parser/EscapeQuerySyntaxImpl;-><init>()V

    invoke-interface {p1, v6}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->toQueryString(Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;)Ljava/lang/CharSequence;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    .line 182
    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-direct {v2, v3, v4}, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 179
    invoke-direct {v1, v2}, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;-><init>(Lorg/apache/lucene/queryparser/flexible/messages/Message;)V

    throw v1

    .line 186
    :cond_0
    invoke-interface {p2, p1}, Lorg/apache/lucene/queryparser/flexible/core/builders/QueryBuilder;->build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Ljava/lang/Object;

    move-result-object v0

    .line 188
    .local v0, "obj":Ljava/lang/Object;
    if-eqz v0, :cond_1

    .line 189
    sget-object v1, Lorg/apache/lucene/queryparser/flexible/core/builders/QueryTreeBuilder;->QUERY_TREE_BUILDER_TAGID:Ljava/lang/String;

    invoke-interface {p1, v1, v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->setTag(Ljava/lang/String;Ljava/lang/Object;)V

    .line 192
    :cond_1
    return-void
.end method


# virtual methods
.method public build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Ljava/lang/Object;
    .locals 1
    .param p1, "queryNode"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 218
    invoke-direct {p0, p1}, Lorg/apache/lucene/queryparser/flexible/core/builders/QueryTreeBuilder;->process(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)V

    .line 220
    sget-object v0, Lorg/apache/lucene/queryparser/flexible/core/builders/QueryTreeBuilder;->QUERY_TREE_BUILDER_TAGID:Ljava/lang/String;

    invoke-interface {p1, v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->getTag(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public setBuilder(Ljava/lang/CharSequence;Lorg/apache/lucene/queryparser/flexible/core/builders/QueryBuilder;)V
    .locals 2
    .param p1, "fieldName"    # Ljava/lang/CharSequence;
    .param p2, "builder"    # Lorg/apache/lucene/queryparser/flexible/core/builders/QueryBuilder;

    .prologue
    .line 81
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/builders/QueryTreeBuilder;->fieldNameBuilders:Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 82
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/builders/QueryTreeBuilder;->fieldNameBuilders:Ljava/util/HashMap;

    .line 85
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/builders/QueryTreeBuilder;->fieldNameBuilders:Ljava/util/HashMap;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    return-void
.end method

.method public setBuilder(Ljava/lang/Class;Lorg/apache/lucene/queryparser/flexible/core/builders/QueryBuilder;)V
    .locals 1
    .param p2, "builder"    # Lorg/apache/lucene/queryparser/flexible/core/builders/QueryBuilder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;",
            ">;",
            "Lorg/apache/lucene/queryparser/flexible/core/builders/QueryBuilder;",
            ")V"
        }
    .end annotation

    .prologue
    .line 99
    .local p1, "queryNodeClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/builders/QueryTreeBuilder;->queryNodeBuilders:Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 100
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/builders/QueryTreeBuilder;->queryNodeBuilders:Ljava/util/HashMap;

    .line 103
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/builders/QueryTreeBuilder;->queryNodeBuilders:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    return-void
.end method
