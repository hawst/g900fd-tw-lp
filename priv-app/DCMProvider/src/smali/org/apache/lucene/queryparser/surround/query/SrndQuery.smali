.class public abstract Lorg/apache/lucene/queryparser/surround/query/SrndQuery;
.super Ljava/lang/Object;
.source "SrndQuery.java"

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field public static final theEmptyLcnQuery:Lorg/apache/lucene/search/Query;


# instance fields
.field private weight:F

.field private weighted:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 99
    new-instance v0, Lorg/apache/lucene/queryparser/surround/query/SrndQuery$1;

    invoke-direct {v0}, Lorg/apache/lucene/queryparser/surround/query/SrndQuery$1;-><init>()V

    sput-object v0, Lorg/apache/lucene/queryparser/surround/query/SrndQuery;->theEmptyLcnQuery:Lorg/apache/lucene/search/Query;

    .line 112
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lorg/apache/lucene/queryparser/surround/query/SrndQuery;->weight:F

    .line 28
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/queryparser/surround/query/SrndQuery;->weighted:Z

    .line 25
    return-void
.end method


# virtual methods
.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/query/SrndQuery;->clone()Lorg/apache/lucene/queryparser/surround/query/SrndQuery;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lorg/apache/lucene/queryparser/surround/query/SrndQuery;
    .locals 2

    .prologue
    .line 68
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/queryparser/surround/query/SrndQuery;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 69
    :catch_0
    move-exception v0

    .line 70
    .local v0, "cns":Ljava/lang/CloneNotSupportedException;
    new-instance v1, Ljava/lang/Error;

    invoke-direct {v1, v0}, Ljava/lang/Error;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x0

    .line 91
    if-nez p1, :cond_1

    .line 95
    :cond_0
    :goto_0
    return v0

    .line 93
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 95
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/query/SrndQuery;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public getWeight()F
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lorg/apache/lucene/queryparser/surround/query/SrndQuery;->weight:F

    return v0
.end method

.method public getWeightOperator()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    const-string v0, "^"

    return-object v0
.end method

.method public getWeightString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/query/SrndQuery;->getWeight()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 81
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/query/SrndQuery;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public isFieldsSubQueryAcceptable()Z
    .locals 1

    .prologue
    .line 63
    const/4 v0, 0x1

    return v0
.end method

.method public isWeighted()Z
    .locals 1

    .prologue
    .line 34
    iget-boolean v0, p0, Lorg/apache/lucene/queryparser/surround/query/SrndQuery;->weighted:Z

    return v0
.end method

.method public makeLuceneQueryField(Ljava/lang/String;Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;)Lorg/apache/lucene/search/Query;
    .locals 3
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "qf"    # Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;

    .prologue
    .line 48
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/queryparser/surround/query/SrndQuery;->makeLuceneQueryFieldNoBoost(Ljava/lang/String;Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;)Lorg/apache/lucene/search/Query;

    move-result-object v0

    .line 49
    .local v0, "q":Lorg/apache/lucene/search/Query;
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/query/SrndQuery;->isWeighted()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 50
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/query/SrndQuery;->getWeight()F

    move-result v1

    invoke-virtual {v0}, Lorg/apache/lucene/search/Query;->getBoost()F

    move-result v2

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/Query;->setBoost(F)V

    .line 52
    :cond_0
    return-object v0
.end method

.method public abstract makeLuceneQueryFieldNoBoost(Ljava/lang/String;Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;)Lorg/apache/lucene/search/Query;
.end method

.method public setWeight(F)V
    .locals 1
    .param p1, "w"    # F

    .prologue
    .line 31
    iput p1, p0, Lorg/apache/lucene/queryparser/surround/query/SrndQuery;->weight:F

    .line 32
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/queryparser/surround/query/SrndQuery;->weighted:Z

    .line 33
    return-void
.end method

.method public abstract toString()Ljava/lang/String;
.end method

.method protected weightToString(Ljava/lang/StringBuilder;)V
    .locals 1
    .param p1, "r"    # Ljava/lang/StringBuilder;

    .prologue
    .line 41
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/query/SrndQuery;->isWeighted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 42
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/query/SrndQuery;->getWeightOperator()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 43
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/query/SrndQuery;->getWeightString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 45
    :cond_0
    return-void
.end method
