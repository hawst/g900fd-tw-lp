.class public Lorg/apache/lucene/queryparser/flexible/standard/builders/RegexpQueryNodeBuilder;
.super Ljava/lang/Object;
.source "RegexpQueryNodeBuilder.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/flexible/standard/builders/StandardQueryBuilder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    return-void
.end method


# virtual methods
.method public bridge synthetic build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/RegexpQueryNodeBuilder;->build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/search/RegexpQuery;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/search/Query;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/RegexpQueryNodeBuilder;->build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/search/RegexpQuery;

    move-result-object v0

    return-object v0
.end method

.method public build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/search/RegexpQuery;
    .locals 6
    .param p1, "queryNode"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 39
    move-object v2, p1

    check-cast v2, Lorg/apache/lucene/queryparser/flexible/standard/nodes/RegexpQueryNode;

    .line 41
    .local v2, "regexpNode":Lorg/apache/lucene/queryparser/flexible/standard/nodes/RegexpQueryNode;
    new-instance v1, Lorg/apache/lucene/search/RegexpQuery;

    new-instance v3, Lorg/apache/lucene/index/Term;

    invoke-virtual {v2}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/RegexpQueryNode;->getFieldAsString()Ljava/lang/String;

    move-result-object v4

    .line 42
    invoke-virtual {v2}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/RegexpQueryNode;->textToBytesRef()Lorg/apache/lucene/util/BytesRef;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 41
    invoke-direct {v1, v3}, Lorg/apache/lucene/search/RegexpQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    .line 45
    .local v1, "q":Lorg/apache/lucene/search/RegexpQuery;
    const-string v3, "MultiTermRewriteMethodConfiguration"

    invoke-interface {p1, v3}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->getTag(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 44
    check-cast v0, Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    .line 46
    .local v0, "method":Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;
    if-eqz v0, :cond_0

    .line 47
    invoke-virtual {v1, v0}, Lorg/apache/lucene/search/RegexpQuery;->setRewriteMethod(Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;)V

    .line 50
    :cond_0
    return-object v1
.end method
