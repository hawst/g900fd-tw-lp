.class public Lorg/apache/lucene/queryparser/flexible/standard/builders/SlopQueryNodeBuilder;
.super Ljava/lang/Object;
.source "SlopQueryNodeBuilder.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/flexible/standard/builders/StandardQueryBuilder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    return-void
.end method


# virtual methods
.method public bridge synthetic build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/SlopQueryNodeBuilder;->build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/search/Query;
    .locals 4
    .param p1, "queryNode"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 42
    move-object v0, p1

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/SlopQueryNode;

    .line 44
    .local v0, "phraseSlopNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/SlopQueryNode;
    invoke-virtual {v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/SlopQueryNode;->getChild()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v2

    .line 45
    sget-object v3, Lorg/apache/lucene/queryparser/flexible/core/builders/QueryTreeBuilder;->QUERY_TREE_BUILDER_TAGID:Ljava/lang/String;

    .line 44
    invoke-interface {v2, v3}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->getTag(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/search/Query;

    .line 47
    .local v1, "query":Lorg/apache/lucene/search/Query;
    instance-of v2, v1, Lorg/apache/lucene/search/PhraseQuery;

    if-eqz v2, :cond_0

    move-object v2, v1

    .line 48
    check-cast v2, Lorg/apache/lucene/search/PhraseQuery;

    invoke-virtual {v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/SlopQueryNode;->getValue()I

    move-result v3

    invoke-virtual {v2, v3}, Lorg/apache/lucene/search/PhraseQuery;->setSlop(I)V

    .line 54
    :goto_0
    return-object v1

    :cond_0
    move-object v2, v1

    .line 51
    check-cast v2, Lorg/apache/lucene/search/MultiPhraseQuery;

    invoke-virtual {v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/SlopQueryNode;->getValue()I

    move-result v3

    invoke-virtual {v2, v3}, Lorg/apache/lucene/search/MultiPhraseQuery;->setSlop(I)V

    goto :goto_0
.end method
