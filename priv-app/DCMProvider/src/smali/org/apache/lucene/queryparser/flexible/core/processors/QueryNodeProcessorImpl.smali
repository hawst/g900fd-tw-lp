.class public abstract Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl;
.super Ljava/lang/Object;
.source "QueryNodeProcessorImpl.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl$ChildrenList;
    }
.end annotation


# instance fields
.field private childrenListPool:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl$ChildrenList;",
            ">;"
        }
    .end annotation
.end field

.field private queryConfig:Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl;->childrenListPool:Ljava/util/ArrayList;

    .line 82
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;)V
    .locals 1
    .param p1, "queryConfigHandler"    # Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl;->childrenListPool:Ljava/util/ArrayList;

    .line 85
    iput-object p1, p0, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl;->queryConfig:Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    .line 86
    return-void
.end method

.method private allocateChildrenList()Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl$ChildrenList;
    .locals 4

    .prologue
    .line 149
    const/4 v1, 0x0

    .line 151
    .local v1, "list":Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl$ChildrenList;
    iget-object v2, p0, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl;->childrenListPool:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 163
    :goto_0
    if-nez v1, :cond_1

    .line 164
    new-instance v1, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl$ChildrenList;

    .end local v1    # "list":Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl$ChildrenList;
    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl$ChildrenList;-><init>(Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl$ChildrenList;)V

    .line 165
    .restart local v1    # "list":Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl$ChildrenList;
    iget-object v2, p0, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl;->childrenListPool:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 169
    :cond_1
    const/4 v2, 0x1

    iput-boolean v2, v1, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl$ChildrenList;->beingUsed:Z

    .line 171
    return-object v1

    .line 151
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl$ChildrenList;

    .line 153
    .local v0, "auxList":Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl$ChildrenList;
    iget-boolean v3, v0, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl$ChildrenList;->beingUsed:Z

    if-nez v3, :cond_0

    .line 154
    move-object v1, v0

    .line 155
    invoke-virtual {v1}, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl$ChildrenList;->clear()V

    goto :goto_0
.end method

.method private processIteration(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 0
    .param p1, "queryTree"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 95
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl;->preProcessNode(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object p1

    .line 97
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl;->processChildren(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)V

    .line 99
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl;->postProcessNode(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object p1

    .line 101
    return-object p1
.end method


# virtual methods
.method public getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl;->queryConfig:Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    return-object v0
.end method

.method protected abstract postProcessNode(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation
.end method

.method protected abstract preProcessNode(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation
.end method

.method public process(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 1
    .param p1, "queryTree"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl;->processIteration(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v0

    return-object v0
.end method

.method protected processChildren(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)V
    .locals 7
    .param p1, "queryTree"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 115
    invoke-interface {p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->getChildren()Ljava/util/List;

    move-result-object v1

    .line 118
    .local v1, "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_0

    .line 120
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl;->allocateChildrenList()Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl$ChildrenList;

    move-result-object v2

    .line 124
    .local v2, "newChildren":Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl$ChildrenList;
    :try_start_0
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_1

    .line 136
    invoke-virtual {p0, v2}, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl;->setChildrenOrder(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    .line 138
    .local v3, "orderedChildrenList":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    invoke-interface {p1, v3}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->set(Ljava/util/List;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 141
    iput-boolean v6, v2, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl$ChildrenList;->beingUsed:Z

    .line 146
    .end local v2    # "newChildren":Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl$ChildrenList;
    .end local v3    # "orderedChildrenList":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    :cond_0
    return-void

    .line 124
    .restart local v2    # "newChildren":Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl$ChildrenList;
    :cond_1
    :try_start_1
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    .line 125
    .local v0, "child":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    invoke-direct {p0, v0}, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl;->processIteration(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v0

    .line 127
    if-nez v0, :cond_2

    .line 128
    new-instance v4, Ljava/lang/NullPointerException;

    invoke-direct {v4}, Ljava/lang/NullPointerException;-><init>()V

    throw v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 140
    .end local v0    # "child":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    :catchall_0
    move-exception v4

    .line 141
    iput-boolean v6, v2, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl$ChildrenList;->beingUsed:Z

    .line 142
    throw v4

    .line 132
    .restart local v0    # "child":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    :cond_2
    :try_start_2
    invoke-virtual {v2, v0}, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl$ChildrenList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method protected abstract setChildrenOrder(Ljava/util/List;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation
.end method

.method public setQueryConfigHandler(Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;)V
    .locals 0
    .param p1, "queryConfigHandler"    # Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    .prologue
    .line 187
    iput-object p1, p0, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl;->queryConfig:Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    .line 188
    return-void
.end method
