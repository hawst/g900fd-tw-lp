.class public interface abstract Lorg/apache/lucene/queryparser/surround/parser/QueryParserConstants;
.super Ljava/lang/Object;
.source "QueryParserConstants.java"


# static fields
.field public static final AND:I = 0x9

.field public static final Boost:I = 0x0

.field public static final CARAT:I = 0x11

.field public static final COLON:I = 0x10

.field public static final COMMA:I = 0xf

.field public static final DEFAULT:I = 0x1

.field public static final EOF:I = 0x0

.field public static final LPAREN:I = 0xd

.field public static final N:I = 0xc

.field public static final NOT:I = 0xa

.field public static final NUMBER:I = 0x17

.field public static final OR:I = 0x8

.field public static final QUOTED:I = 0x13

.field public static final RPAREN:I = 0xe

.field public static final SUFFIXTERM:I = 0x14

.field public static final TERM:I = 0x16

.field public static final TRUNCQUOTED:I = 0x12

.field public static final TRUNCTERM:I = 0x15

.field public static final W:I = 0xb

.field public static final _DISTOP_NUM:I = 0x6

.field public static final _NUM_CHAR:I = 0x1

.field public static final _ONE_CHAR:I = 0x5

.field public static final _STAR:I = 0x4

.field public static final _TERM_CHAR:I = 0x2

.field public static final _WHITESPACE:I = 0x3

.field public static final tokenImage:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 64
    const/16 v0, 0x18

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    .line 65
    const-string v2, "<EOF>"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 66
    const-string v2, "<_NUM_CHAR>"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 67
    const-string v2, "<_TERM_CHAR>"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    .line 68
    const-string v2, "<_WHITESPACE>"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    .line 69
    const-string v2, "\"*\""

    aput-object v2, v0, v1

    const/4 v1, 0x5

    .line 70
    const-string v2, "\"?\""

    aput-object v2, v0, v1

    const/4 v1, 0x6

    .line 71
    const-string v2, "<_DISTOP_NUM>"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 72
    const-string v2, "<token of kind 7>"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    .line 73
    const-string v2, "<OR>"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    .line 74
    const-string v2, "<AND>"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    .line 75
    const-string v2, "<NOT>"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    .line 76
    const-string v2, "<W>"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    .line 77
    const-string v2, "<N>"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    .line 78
    const-string v2, "\"(\""

    aput-object v2, v0, v1

    const/16 v1, 0xe

    .line 79
    const-string v2, "\")\""

    aput-object v2, v0, v1

    const/16 v1, 0xf

    .line 80
    const-string v2, "\",\""

    aput-object v2, v0, v1

    const/16 v1, 0x10

    .line 81
    const-string v2, "\":\""

    aput-object v2, v0, v1

    const/16 v1, 0x11

    .line 82
    const-string v2, "\"^\""

    aput-object v2, v0, v1

    const/16 v1, 0x12

    .line 83
    const-string v2, "<TRUNCQUOTED>"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    .line 84
    const-string v2, "<QUOTED>"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    .line 85
    const-string v2, "<SUFFIXTERM>"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    .line 86
    const-string v2, "<TRUNCTERM>"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    .line 87
    const-string v2, "<TERM>"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    .line 88
    const-string v2, "<NUMBER>"

    aput-object v2, v0, v1

    .line 64
    sput-object v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserConstants;->tokenImage:[Ljava/lang/String;

    .line 89
    return-void
.end method
