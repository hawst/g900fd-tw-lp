.class public abstract Lorg/apache/lucene/queryparser/flexible/core/config/AbstractQueryConfig;
.super Ljava/lang/Object;
.source "AbstractQueryConfig.java"


# instance fields
.field private final configMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey",
            "<*>;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/config/AbstractQueryConfig;->configMap:Ljava/util/HashMap;

    .line 40
    return-void
.end method


# virtual methods
.method public get(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 54
    .local p1, "key":Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;, "Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey<TT;>;"
    if-nez p1, :cond_0

    .line 55
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "key cannot be null!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 58
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/config/AbstractQueryConfig;->configMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public has(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey",
            "<TT;>;)Z"
        }
    .end annotation

    .prologue
    .line 71
    .local p1, "key":Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;, "Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey<TT;>;"
    if-nez p1, :cond_0

    .line 72
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "key cannot be null!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 75
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/config/AbstractQueryConfig;->configMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public set(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey",
            "<TT;>;TT;)V"
        }
    .end annotation

    .prologue
    .line 88
    .local p1, "key":Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;, "Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey<TT;>;"
    .local p2, "value":Ljava/lang/Object;, "TT;"
    if-nez p1, :cond_0

    .line 89
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "key cannot be null!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 92
    :cond_0
    if-nez p2, :cond_1

    .line 93
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/core/config/AbstractQueryConfig;->unset(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;)Z

    .line 99
    :goto_0
    return-void

    .line 96
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/config/AbstractQueryConfig;->configMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public unset(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey",
            "<TT;>;)Z"
        }
    .end annotation

    .prologue
    .line 110
    .local p1, "key":Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;, "Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey<TT;>;"
    if-nez p1, :cond_0

    .line 111
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "key cannot be null!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 114
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/config/AbstractQueryConfig;->configMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
