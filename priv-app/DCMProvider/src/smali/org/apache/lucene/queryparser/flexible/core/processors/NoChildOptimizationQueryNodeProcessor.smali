.class public Lorg/apache/lucene/queryparser/flexible/core/processors/NoChildOptimizationQueryNodeProcessor;
.super Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl;
.source "NoChildOptimizationQueryNodeProcessor.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl;-><init>()V

    .line 41
    return-void
.end method


# virtual methods
.method protected postProcessNode(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 4
    .param p1, "node"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 46
    instance-of v2, p1, Lorg/apache/lucene/queryparser/flexible/core/nodes/BooleanQueryNode;

    if-nez v2, :cond_0

    instance-of v2, p1, Lorg/apache/lucene/queryparser/flexible/core/nodes/BoostQueryNode;

    if-nez v2, :cond_0

    .line 47
    instance-of v2, p1, Lorg/apache/lucene/queryparser/flexible/core/nodes/TokenizedPhraseQueryNode;

    if-nez v2, :cond_0

    .line 48
    instance-of v2, p1, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;

    if-eqz v2, :cond_3

    .line 50
    :cond_0
    invoke-interface {p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->getChildren()Ljava/util/List;

    move-result-object v1

    .line 52
    .local v1, "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    if-eqz v1, :cond_2

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_2

    .line 54
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_4

    .line 64
    :cond_2
    new-instance p1, Lorg/apache/lucene/queryparser/flexible/core/nodes/MatchNoDocsQueryNode;

    .end local p1    # "node":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    invoke-direct {p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/MatchNoDocsQueryNode;-><init>()V

    .line 68
    .end local v1    # "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    :cond_3
    :goto_0
    return-object p1

    .line 54
    .restart local v1    # "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    .restart local p1    # "node":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    :cond_4
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    .line 56
    .local v0, "child":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    instance-of v3, v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/DeletedQueryNode;

    if-nez v3, :cond_1

    goto :goto_0
.end method

.method protected preProcessNode(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 0
    .param p1, "node"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 75
    return-object p1
.end method

.method protected setChildrenOrder(Ljava/util/List;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 83
    .local p1, "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    return-object p1
.end method
