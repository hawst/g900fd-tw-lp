.class public Lorg/apache/lucene/queryparser/flexible/standard/nodes/PrefixWildcardQueryNode;
.super Lorg/apache/lucene/queryparser/flexible/standard/nodes/WildcardQueryNode;
.source "PrefixWildcardQueryNode.java"


# direct methods
.method public constructor <init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;II)V
    .locals 0
    .param p1, "field"    # Ljava/lang/CharSequence;
    .param p2, "text"    # Ljava/lang/CharSequence;
    .param p3, "begin"    # I
    .param p4, "end"    # I

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3, p4}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/WildcardQueryNode;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;II)V

    .line 43
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;)V
    .locals 4
    .param p1, "fqn"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;

    .prologue
    .line 46
    invoke-virtual {p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->getField()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->getBegin()I

    move-result v2

    invoke-virtual {p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->getEnd()I

    move-result v3

    invoke-direct {p0, v0, v1, v2, v3}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/PrefixWildcardQueryNode;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;II)V

    .line 47
    return-void
.end method


# virtual methods
.method public bridge synthetic cloneTree()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/PrefixWildcardQueryNode;->cloneTree()Lorg/apache/lucene/queryparser/flexible/standard/nodes/PrefixWildcardQueryNode;

    move-result-object v0

    return-object v0
.end method

.method public cloneTree()Lorg/apache/lucene/queryparser/flexible/standard/nodes/PrefixWildcardQueryNode;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 57
    invoke-super {p0}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/WildcardQueryNode;->cloneTree()Lorg/apache/lucene/queryparser/flexible/standard/nodes/WildcardQueryNode;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/PrefixWildcardQueryNode;

    .line 61
    .local v0, "clone":Lorg/apache/lucene/queryparser/flexible/standard/nodes/PrefixWildcardQueryNode;
    return-object v0
.end method

.method public bridge synthetic cloneTree()Lorg/apache/lucene/queryparser/flexible/standard/nodes/WildcardQueryNode;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/PrefixWildcardQueryNode;->cloneTree()Lorg/apache/lucene/queryparser/flexible/standard/nodes/PrefixWildcardQueryNode;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 51
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "<prefixWildcard field=\'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/PrefixWildcardQueryNode;->field:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\' term=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/PrefixWildcardQueryNode;->text:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 52
    const-string v1, "\'/>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 51
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
