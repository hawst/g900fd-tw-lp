.class public Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
.super Ljava/lang/Exception;
.source "QueryNodeException.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/flexible/messages/NLSException;


# instance fields
.field protected message:Lorg/apache/lucene/queryparser/flexible/messages/Message;


# direct methods
.method public constructor <init>(Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "throwable"    # Ljava/lang/Throwable;

    .prologue
    .line 55
    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/Throwable;)V

    .line 45
    new-instance v0, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;

    sget-object v1, Lorg/apache/lucene/queryparser/flexible/core/messages/QueryParserMessages;->EMPTY_MESSAGE:Ljava/lang/String;

    invoke-direct {v0, v1}, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;->message:Lorg/apache/lucene/queryparser/flexible/messages/Message;

    .line 56
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/queryparser/flexible/messages/Message;)V
    .locals 2
    .param p1, "message"    # Lorg/apache/lucene/queryparser/flexible/messages/Message;

    .prologue
    .line 48
    invoke-interface {p1}, Lorg/apache/lucene/queryparser/flexible/messages/Message;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 45
    new-instance v0, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;

    sget-object v1, Lorg/apache/lucene/queryparser/flexible/core/messages/QueryParserMessages;->EMPTY_MESSAGE:Ljava/lang/String;

    invoke-direct {v0, v1}, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;->message:Lorg/apache/lucene/queryparser/flexible/messages/Message;

    .line 50
    iput-object p1, p0, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;->message:Lorg/apache/lucene/queryparser/flexible/messages/Message;

    .line 52
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/queryparser/flexible/messages/Message;Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "message"    # Lorg/apache/lucene/queryparser/flexible/messages/Message;
    .param p2, "throwable"    # Ljava/lang/Throwable;

    .prologue
    .line 59
    invoke-interface {p1}, Lorg/apache/lucene/queryparser/flexible/messages/Message;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 45
    new-instance v0, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;

    sget-object v1, Lorg/apache/lucene/queryparser/flexible/core/messages/QueryParserMessages;->EMPTY_MESSAGE:Ljava/lang/String;

    invoke-direct {v0, v1}, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;->message:Lorg/apache/lucene/queryparser/flexible/messages/Message;

    .line 61
    iput-object p1, p0, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;->message:Lorg/apache/lucene/queryparser/flexible/messages/Message;

    .line 63
    return-void
.end method


# virtual methods
.method public getLocalizedMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;->getLocalizedMessage(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLocalizedMessage(Ljava/util/Locale;)Ljava/lang/String;
    .locals 1
    .param p1, "locale"    # Ljava/util/Locale;

    .prologue
    .line 81
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;->message:Lorg/apache/lucene/queryparser/flexible/messages/Message;

    invoke-interface {v0, p1}, Lorg/apache/lucene/queryparser/flexible/messages/Message;->getLocalizedMessage(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMessageObject()Lorg/apache/lucene/queryparser/flexible/messages/Message;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;->message:Lorg/apache/lucene/queryparser/flexible/messages/Message;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 86
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;->message:Lorg/apache/lucene/queryparser/flexible/messages/Message;

    invoke-interface {v1}, Lorg/apache/lucene/queryparser/flexible/messages/Message;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
