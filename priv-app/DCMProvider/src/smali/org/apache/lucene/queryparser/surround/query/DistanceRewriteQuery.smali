.class Lorg/apache/lucene/queryparser/surround/query/DistanceRewriteQuery;
.super Lorg/apache/lucene/queryparser/surround/query/RewriteQuery;
.source "DistanceRewriteQuery.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/queryparser/surround/query/RewriteQuery",
        "<",
        "Lorg/apache/lucene/queryparser/surround/query/DistanceQuery;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Lorg/apache/lucene/queryparser/surround/query/DistanceQuery;Ljava/lang/String;Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;)V
    .locals 0
    .param p1, "srndQuery"    # Lorg/apache/lucene/queryparser/surround/query/DistanceQuery;
    .param p2, "fieldName"    # Ljava/lang/String;
    .param p3, "qf"    # Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;

    .prologue
    .line 30
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/queryparser/surround/query/RewriteQuery;-><init>(Lorg/apache/lucene/queryparser/surround/query/SrndQuery;Ljava/lang/String;Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;)V

    .line 31
    return-void
.end method


# virtual methods
.method public rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;
    .locals 4
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 35
    iget-object v0, p0, Lorg/apache/lucene/queryparser/surround/query/DistanceRewriteQuery;->srndQuery:Lorg/apache/lucene/queryparser/surround/query/SrndQuery;

    check-cast v0, Lorg/apache/lucene/queryparser/surround/query/DistanceQuery;

    iget-object v1, p0, Lorg/apache/lucene/queryparser/surround/query/DistanceRewriteQuery;->fieldName:Ljava/lang/String;

    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/query/DistanceRewriteQuery;->getBoost()F

    move-result v2

    iget-object v3, p0, Lorg/apache/lucene/queryparser/surround/query/DistanceRewriteQuery;->qf:Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;

    invoke-virtual {v0, p1, v1, v2, v3}, Lorg/apache/lucene/queryparser/surround/query/DistanceQuery;->getSpanNearQuery(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;FLorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;)Lorg/apache/lucene/search/Query;

    move-result-object v0

    return-object v0
.end method
