.class public Lorg/apache/lucene/queryparser/xml/builders/TermsQueryBuilder;
.super Ljava/lang/Object;
.source "TermsQueryBuilder.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/xml/QueryBuilder;


# instance fields
.field private final analyzer:Lorg/apache/lucene/analysis/Analyzer;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/Analyzer;)V
    .locals 0
    .param p1, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lorg/apache/lucene/queryparser/xml/builders/TermsQueryBuilder;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    .line 46
    return-void
.end method


# virtual methods
.method public getQuery(Lorg/w3c/dom/Element;)Lorg/apache/lucene/search/Query;
    .locals 11
    .param p1, "e"    # Lorg/w3c/dom/Element;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/xml/ParserException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 50
    const-string v8, "fieldName"

    invoke-static {p1, v8}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttributeWithInheritanceOrFail(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 51
    .local v2, "fieldName":Ljava/lang/String;
    invoke-static {p1}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getNonBlankTextOrFail(Lorg/w3c/dom/Element;)Ljava/lang/String;

    move-result-object v6

    .line 53
    .local v6, "text":Ljava/lang/String;
    new-instance v0, Lorg/apache/lucene/search/BooleanQuery;

    const-string v8, "disableCoord"

    invoke-static {p1, v8, v9}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttribute(Lorg/w3c/dom/Element;Ljava/lang/String;Z)Z

    move-result v8

    invoke-direct {v0, v8}, Lorg/apache/lucene/search/BooleanQuery;-><init>(Z)V

    .line 54
    .local v0, "bq":Lorg/apache/lucene/search/BooleanQuery;
    const-string v8, "minimumNumberShouldMatch"

    invoke-static {p1, v8, v9}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttribute(Lorg/w3c/dom/Element;Ljava/lang/String;I)I

    move-result v8

    invoke-virtual {v0, v8}, Lorg/apache/lucene/search/BooleanQuery;->setMinimumNumberShouldMatch(I)V

    .line 56
    :try_start_0
    iget-object v8, p0, Lorg/apache/lucene/queryparser/xml/builders/TermsQueryBuilder;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    new-instance v9, Ljava/io/StringReader;

    invoke-direct {v9, v6}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v2, v9}, Lorg/apache/lucene/analysis/Analyzer;->tokenStream(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/TokenStream;

    move-result-object v7

    .line 57
    .local v7, "ts":Lorg/apache/lucene/analysis/TokenStream;
    const-class v8, Lorg/apache/lucene/analysis/tokenattributes/TermToBytesRefAttribute;

    invoke-virtual {v7, v8}, Lorg/apache/lucene/analysis/TokenStream;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/analysis/tokenattributes/TermToBytesRefAttribute;

    .line 58
    .local v5, "termAtt":Lorg/apache/lucene/analysis/tokenattributes/TermToBytesRefAttribute;
    const/4 v4, 0x0

    .line 59
    .local v4, "term":Lorg/apache/lucene/index/Term;
    invoke-interface {v5}, Lorg/apache/lucene/analysis/tokenattributes/TermToBytesRefAttribute;->getBytesRef()Lorg/apache/lucene/util/BytesRef;

    move-result-object v1

    .line 60
    .local v1, "bytes":Lorg/apache/lucene/util/BytesRef;
    invoke-virtual {v7}, Lorg/apache/lucene/analysis/TokenStream;->reset()V

    .line 61
    :goto_0
    invoke-virtual {v7}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v8

    if-nez v8, :cond_0

    .line 66
    invoke-virtual {v7}, Lorg/apache/lucene/analysis/TokenStream;->end()V

    .line 67
    invoke-virtual {v7}, Lorg/apache/lucene/analysis/TokenStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 73
    const-string v8, "boost"

    const/high16 v9, 0x3f800000    # 1.0f

    invoke-static {p1, v8, v9}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttribute(Lorg/w3c/dom/Element;Ljava/lang/String;F)F

    move-result v8

    invoke-virtual {v0, v8}, Lorg/apache/lucene/search/BooleanQuery;->setBoost(F)V

    .line 74
    return-object v0

    .line 62
    :cond_0
    :try_start_1
    invoke-interface {v5}, Lorg/apache/lucene/analysis/tokenattributes/TermToBytesRefAttribute;->fillBytesRef()I

    .line 63
    new-instance v4, Lorg/apache/lucene/index/Term;

    .end local v4    # "term":Lorg/apache/lucene/index/Term;
    invoke-static {v1}, Lorg/apache/lucene/util/BytesRef;->deepCopyOf(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/BytesRef;

    move-result-object v8

    invoke-direct {v4, v2, v8}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 64
    .restart local v4    # "term":Lorg/apache/lucene/index/Term;
    new-instance v8, Lorg/apache/lucene/search/BooleanClause;

    new-instance v9, Lorg/apache/lucene/search/TermQuery;

    invoke-direct {v9, v4}, Lorg/apache/lucene/search/TermQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    sget-object v10, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-direct {v8, v9, v10}, Lorg/apache/lucene/search/BooleanClause;-><init>(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    invoke-virtual {v0, v8}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/BooleanClause;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 69
    .end local v1    # "bytes":Lorg/apache/lucene/util/BytesRef;
    .end local v4    # "term":Lorg/apache/lucene/index/Term;
    .end local v5    # "termAtt":Lorg/apache/lucene/analysis/tokenattributes/TermToBytesRefAttribute;
    .end local v7    # "ts":Lorg/apache/lucene/analysis/TokenStream;
    :catch_0
    move-exception v3

    .line 70
    .local v3, "ioe":Ljava/io/IOException;
    new-instance v8, Ljava/lang/RuntimeException;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Error constructing terms from index:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v8
.end method
