.class Lorg/apache/lucene/queryparser/surround/query/SimpleTermRewriteQuery;
.super Lorg/apache/lucene/queryparser/surround/query/RewriteQuery;
.source "SimpleTermRewriteQuery.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/queryparser/surround/query/RewriteQuery",
        "<",
        "Lorg/apache/lucene/queryparser/surround/query/SimpleTerm;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Lorg/apache/lucene/queryparser/surround/query/SimpleTerm;Ljava/lang/String;Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;)V
    .locals 0
    .param p1, "srndQuery"    # Lorg/apache/lucene/queryparser/surround/query/SimpleTerm;
    .param p2, "fieldName"    # Ljava/lang/String;
    .param p3, "qf"    # Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;

    .prologue
    .line 33
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/queryparser/surround/query/RewriteQuery;-><init>(Lorg/apache/lucene/queryparser/surround/query/SrndQuery;Ljava/lang/String;Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;)V

    .line 34
    return-void
.end method


# virtual methods
.method public rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;
    .locals 4
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 38
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 39
    .local v0, "luceneSubQueries":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/Query;>;"
    iget-object v1, p0, Lorg/apache/lucene/queryparser/surround/query/SimpleTermRewriteQuery;->srndQuery:Lorg/apache/lucene/queryparser/surround/query/SrndQuery;

    check-cast v1, Lorg/apache/lucene/queryparser/surround/query/SimpleTerm;

    iget-object v2, p0, Lorg/apache/lucene/queryparser/surround/query/SimpleTermRewriteQuery;->fieldName:Ljava/lang/String;

    .line 40
    new-instance v3, Lorg/apache/lucene/queryparser/surround/query/SimpleTermRewriteQuery$1;

    invoke-direct {v3, p0, v0}, Lorg/apache/lucene/queryparser/surround/query/SimpleTermRewriteQuery$1;-><init>(Lorg/apache/lucene/queryparser/surround/query/SimpleTermRewriteQuery;Ljava/util/List;)V

    .line 39
    invoke-virtual {v1, p1, v2, v3}, Lorg/apache/lucene/queryparser/surround/query/SimpleTerm;->visitMatchingTerms(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/queryparser/surround/query/SimpleTerm$MatchingTermVisitor;)V

    .line 46
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lorg/apache/lucene/queryparser/surround/query/SrndQuery;->theEmptyLcnQuery:Lorg/apache/lucene/search/Query;

    :goto_0
    return-object v1

    .line 47
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/search/Query;

    goto :goto_0

    .line 50
    :cond_1
    sget-object v1, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    .line 48
    invoke-static {v0, v1}, Lorg/apache/lucene/queryparser/surround/query/SrndBooleanQuery;->makeBooleanQuery(Ljava/util/List;Lorg/apache/lucene/search/BooleanClause$Occur;)Lorg/apache/lucene/search/Query;

    move-result-object v1

    goto :goto_0
.end method
