.class public Lorg/apache/lucene/queryparser/flexible/standard/nodes/RegexpQueryNode;
.super Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;
.source "RegexpQueryNode.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldableNode;
.implements Lorg/apache/lucene/queryparser/flexible/core/nodes/TextableQueryNode;


# instance fields
.field private field:Ljava/lang/CharSequence;

.field private text:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;II)V
    .locals 1
    .param p1, "field"    # Ljava/lang/CharSequence;
    .param p2, "text"    # Ljava/lang/CharSequence;
    .param p3, "begin"    # I
    .param p4, "end"    # I

    .prologue
    .line 44
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;-><init>()V

    .line 46
    iput-object p1, p0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/RegexpQueryNode;->field:Ljava/lang/CharSequence;

    .line 47
    invoke-interface {p2, p3, p4}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/RegexpQueryNode;->text:Ljava/lang/CharSequence;

    .line 48
    return-void
.end method


# virtual methods
.method public bridge synthetic cloneTree()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/RegexpQueryNode;->cloneTree()Lorg/apache/lucene/queryparser/flexible/standard/nodes/RegexpQueryNode;

    move-result-object v0

    return-object v0
.end method

.method public cloneTree()Lorg/apache/lucene/queryparser/flexible/standard/nodes/RegexpQueryNode;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 61
    invoke-super {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;->cloneTree()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/RegexpQueryNode;

    .line 62
    .local v0, "clone":Lorg/apache/lucene/queryparser/flexible/standard/nodes/RegexpQueryNode;
    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/RegexpQueryNode;->field:Ljava/lang/CharSequence;

    iput-object v1, v0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/RegexpQueryNode;->field:Ljava/lang/CharSequence;

    .line 63
    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/RegexpQueryNode;->text:Ljava/lang/CharSequence;

    iput-object v1, v0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/RegexpQueryNode;->text:Ljava/lang/CharSequence;

    .line 64
    return-object v0
.end method

.method public getField()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/RegexpQueryNode;->field:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getFieldAsString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/RegexpQueryNode;->field:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getText()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/RegexpQueryNode;->text:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public setField(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "field"    # Ljava/lang/CharSequence;

    .prologue
    .line 88
    iput-object p1, p0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/RegexpQueryNode;->field:Ljava/lang/CharSequence;

    .line 89
    return-void
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 74
    iput-object p1, p0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/RegexpQueryNode;->text:Ljava/lang/CharSequence;

    .line 75
    return-void
.end method

.method public textToBytesRef()Lorg/apache/lucene/util/BytesRef;
    .locals 2

    .prologue
    .line 51
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/RegexpQueryNode;->text:Ljava/lang/CharSequence;

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    return-object v0
.end method

.method public toQueryString(Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;)Ljava/lang/CharSequence;
    .locals 2
    .param p1, "escapeSyntaxParser"    # Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;

    .prologue
    .line 93
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/RegexpQueryNode;->field:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/RegexpQueryNode;->isDefaultField(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/RegexpQueryNode;->text:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/RegexpQueryNode;->field:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/RegexpQueryNode;->text:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 56
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "<regexp field=\'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/RegexpQueryNode;->field:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\' term=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/RegexpQueryNode;->text:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'/>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
