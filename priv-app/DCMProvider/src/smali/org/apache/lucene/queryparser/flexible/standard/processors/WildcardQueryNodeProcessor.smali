.class public Lorg/apache/lucene/queryparser/flexible/standard/processors/WildcardQueryNodeProcessor;
.super Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl;
.source "WildcardQueryNodeProcessor.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl;-><init>()V

    .line 49
    return-void
.end method

.method private isPrefixWildcard(Ljava/lang/CharSequence;)Z
    .locals 6
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    const/16 v5, 0x2a

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 101
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-lez v3, :cond_0

    invoke-direct {p0, p1}, Lorg/apache/lucene/queryparser/flexible/standard/processors/WildcardQueryNodeProcessor;->isWildcard(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 121
    :cond_0
    :goto_0
    return v1

    .line 105
    :cond_1
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {p1, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    if-ne v3, v5, :cond_0

    .line 106
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-static {p1, v3}, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;->wasEscaped(Ljava/lang/CharSequence;I)Z

    move-result v3

    if-nez v3, :cond_0

    .line 107
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-eq v3, v2, :cond_0

    .line 111
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 112
    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    const/16 v4, 0x3f

    if-eq v3, v4, :cond_0

    .line 113
    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    if-ne v3, v5, :cond_2

    invoke-static {p1, v0}, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;->wasEscaped(Ljava/lang/CharSequence;I)Z

    move-result v3

    if-nez v3, :cond_2

    .line 114
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ne v0, v3, :cond_0

    move v1, v2

    .line 115
    goto :goto_0

    .line 111
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private isWildcard(Ljava/lang/CharSequence;)Z
    .locals 4
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    const/4 v1, 0x0

    .line 87
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-gtz v2, :cond_1

    .line 97
    :cond_0
    :goto_0
    return v1

    .line 91
    :cond_1
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    .local v0, "i":I
    :goto_1
    if-ltz v0, :cond_0

    .line 92
    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    const/16 v3, 0x2a

    if-eq v2, v3, :cond_2

    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    const/16 v3, 0x3f

    if-ne v2, v3, :cond_3

    :cond_2
    invoke-static {p1, v0}, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;->wasEscaped(Ljava/lang/CharSequence;I)Z

    move-result v2

    if-nez v2, :cond_3

    .line 93
    const/4 v1, 0x1

    goto :goto_0

    .line 91
    :cond_3
    add-int/lit8 v0, v0, -0x1

    goto :goto_1
.end method


# virtual methods
.method protected postProcessNode(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 5
    .param p1, "node"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 56
    instance-of v4, p1, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;

    if-nez v4, :cond_0

    instance-of v4, p1, Lorg/apache/lucene/queryparser/flexible/core/nodes/FuzzyQueryNode;

    if-eqz v4, :cond_1

    :cond_0
    move-object v0, p1

    .line 57
    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;

    .line 58
    .local v0, "fqn":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    invoke-virtual {v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    .line 62
    .local v2, "text":Ljava/lang/CharSequence;
    invoke-virtual {v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->getParent()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v4

    instance-of v4, v4, Lorg/apache/lucene/queryparser/flexible/standard/nodes/TermRangeQueryNode;

    if-nez v4, :cond_1

    .line 63
    instance-of v4, v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/QuotedFieldQueryNode;

    if-nez v4, :cond_1

    .line 64
    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v4

    if-gtz v4, :cond_2

    .line 82
    .end local v0    # "fqn":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    .end local v2    # "text":Ljava/lang/CharSequence;
    .end local p1    # "node":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    :cond_1
    :goto_0
    return-object p1

    .line 71
    .restart local v0    # "fqn":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    .restart local v2    # "text":Ljava/lang/CharSequence;
    .restart local p1    # "node":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    :cond_2
    invoke-direct {p0, v2}, Lorg/apache/lucene/queryparser/flexible/standard/processors/WildcardQueryNodeProcessor;->isPrefixWildcard(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 72
    new-instance v1, Lorg/apache/lucene/queryparser/flexible/standard/nodes/PrefixWildcardQueryNode;

    invoke-direct {v1, v0}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/PrefixWildcardQueryNode;-><init>(Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;)V

    .local v1, "prefixWildcardQN":Lorg/apache/lucene/queryparser/flexible/standard/nodes/PrefixWildcardQueryNode;
    move-object p1, v1

    .line 73
    goto :goto_0

    .line 75
    .end local v1    # "prefixWildcardQN":Lorg/apache/lucene/queryparser/flexible/standard/nodes/PrefixWildcardQueryNode;
    :cond_3
    invoke-direct {p0, v2}, Lorg/apache/lucene/queryparser/flexible/standard/processors/WildcardQueryNodeProcessor;->isWildcard(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 76
    new-instance v3, Lorg/apache/lucene/queryparser/flexible/standard/nodes/WildcardQueryNode;

    invoke-direct {v3, v0}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/WildcardQueryNode;-><init>(Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;)V

    .local v3, "wildcardQN":Lorg/apache/lucene/queryparser/flexible/standard/nodes/WildcardQueryNode;
    move-object p1, v3

    .line 77
    goto :goto_0
.end method

.method protected preProcessNode(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 0
    .param p1, "node"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 127
    return-object p1
.end method

.method protected setChildrenOrder(Ljava/util/List;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 135
    .local p1, "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    return-object p1
.end method
