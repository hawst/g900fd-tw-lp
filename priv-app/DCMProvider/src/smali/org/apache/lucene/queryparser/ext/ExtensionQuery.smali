.class public Lorg/apache/lucene/queryparser/ext/ExtensionQuery;
.super Ljava/lang/Object;
.source "ExtensionQuery.java"


# instance fields
.field private final field:Ljava/lang/String;

.field private final rawQueryString:Ljava/lang/String;

.field private final topLevelParser:Lorg/apache/lucene/queryparser/classic/QueryParser;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/queryparser/classic/QueryParser;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "topLevelParser"    # Lorg/apache/lucene/queryparser/classic/QueryParser;
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "rawQueryString"    # Ljava/lang/String;

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p2, p0, Lorg/apache/lucene/queryparser/ext/ExtensionQuery;->field:Ljava/lang/String;

    .line 46
    iput-object p3, p0, Lorg/apache/lucene/queryparser/ext/ExtensionQuery;->rawQueryString:Ljava/lang/String;

    .line 47
    iput-object p1, p0, Lorg/apache/lucene/queryparser/ext/ExtensionQuery;->topLevelParser:Lorg/apache/lucene/queryparser/classic/QueryParser;

    .line 48
    return-void
.end method


# virtual methods
.method public getField()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lorg/apache/lucene/queryparser/ext/ExtensionQuery;->field:Ljava/lang/String;

    return-object v0
.end method

.method public getRawQueryString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lorg/apache/lucene/queryparser/ext/ExtensionQuery;->rawQueryString:Ljava/lang/String;

    return-object v0
.end method

.method public getTopLevelParser()Lorg/apache/lucene/queryparser/classic/QueryParser;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lorg/apache/lucene/queryparser/ext/ExtensionQuery;->topLevelParser:Lorg/apache/lucene/queryparser/classic/QueryParser;

    return-object v0
.end method
