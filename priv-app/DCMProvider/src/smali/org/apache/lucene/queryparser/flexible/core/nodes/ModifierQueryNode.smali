.class public Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;
.super Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;
.source "ModifierQueryNode.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;
    }
.end annotation


# instance fields
.field private modifier:Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;)V
    .locals 6
    .param p1, "query"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .param p2, "mod"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    .prologue
    const/4 v5, 0x0

    .line 96
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;-><init>()V

    .line 86
    sget-object v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;->MOD_NONE:Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    iput-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;->modifier:Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    .line 97
    if-nez p1, :cond_0

    .line 98
    new-instance v0, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeError;

    new-instance v1, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;

    .line 99
    sget-object v2, Lorg/apache/lucene/queryparser/flexible/core/messages/QueryParserMessages;->PARAMETER_VALUE_NOT_SUPPORTED:Ljava/lang/String;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "query"

    aput-object v4, v3, v5

    const/4 v4, 0x1

    const-string v5, "null"

    aput-object v5, v3, v4

    invoke-direct {v1, v2, v3}, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 98
    invoke-direct {v0, v1}, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeError;-><init>(Lorg/apache/lucene/queryparser/flexible/messages/Message;)V

    throw v0

    .line 102
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;->allocate()V

    .line 103
    invoke-virtual {p0, v5}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;->setLeaf(Z)V

    .line 104
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;->add(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)V

    .line 105
    iput-object p2, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;->modifier:Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    .line 106
    return-void
.end method


# virtual methods
.method public cloneTree()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 146
    invoke-super {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;->cloneTree()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;

    .line 148
    .local v0, "clone":Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;
    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;->modifier:Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    iput-object v1, v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;->modifier:Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    .line 150
    return-object v0
.end method

.method public getChild()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 2

    .prologue
    .line 109
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;->getChildren()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    return-object v0
.end method

.method public getModifier()Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;->modifier:Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    return-object v0
.end method

.method public setChild(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)V
    .locals 1
    .param p1, "child"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    .prologue
    .line 154
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 155
    .local v0, "list":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 156
    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;->set(Ljava/util/List;)V

    .line 157
    return-void
.end method

.method public toQueryString(Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;)Ljava/lang/CharSequence;
    .locals 4
    .param p1, "escapeSyntaxParser"    # Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;

    .prologue
    .line 124
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;->getChild()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v2

    if-nez v2, :cond_0

    .line 125
    const-string v2, ""

    .line 139
    :goto_0
    return-object v2

    .line 127
    :cond_0
    const-string v0, ""

    .line 128
    .local v0, "leftParenthensis":Ljava/lang/String;
    const-string v1, ""

    .line 130
    .local v1, "rightParenthensis":Ljava/lang/String;
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;->getChild()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;->getChild()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v2

    instance-of v2, v2, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;

    if-eqz v2, :cond_1

    .line 131
    const-string v0, "("

    .line 132
    const-string v1, ")"

    .line 135
    :cond_1
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;->getChild()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v2

    instance-of v2, v2, Lorg/apache/lucene/queryparser/flexible/core/nodes/BooleanQueryNode;

    if-eqz v2, :cond_2

    .line 136
    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;->modifier:Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    invoke-virtual {v3}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;->toLargeString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 137
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;->getChild()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v3

    invoke-interface {v3, p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->toQueryString(Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 136
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 139
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;->modifier:Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    invoke-virtual {v3}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;->toDigitString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 140
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;->getChild()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v3

    invoke-interface {v3, p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->toQueryString(Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 139
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 118
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "<modifier operation=\'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;->modifier:Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    invoke-virtual {v1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 119
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;->getChild()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n</modifier>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 118
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
