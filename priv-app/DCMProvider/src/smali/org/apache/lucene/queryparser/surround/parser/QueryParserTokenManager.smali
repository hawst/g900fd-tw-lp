.class public Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;
.super Ljava/lang/Object;
.source "QueryParserTokenManager.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/surround/parser/QueryParserConstants;


# static fields
.field static final jjbitVec0:[J

.field static final jjbitVec2:[J

.field public static final jjnewLexState:[I

.field static final jjnextStates:[I

.field public static final jjstrLiteralImages:[Ljava/lang/String;

.field static final jjtoSkip:[J

.field static final jjtoToken:[J

.field public static final lexStateNames:[Ljava/lang/String;


# instance fields
.field protected curChar:C

.field curLexState:I

.field public debugStream:Ljava/io/PrintStream;

.field defaultLexState:I

.field protected input_stream:Lorg/apache/lucene/queryparser/surround/parser/CharStream;

.field jjmatchedKind:I

.field jjmatchedPos:I

.field jjnewStateCnt:I

.field jjround:I

.field private final jjrounds:[I

.field private final jjstateSet:[I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, -0x1

    .line 61
    new-array v0, v8, [J

    fill-array-data v0, :array_0

    sput-object v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjbitVec0:[J

    .line 64
    new-array v0, v8, [J

    .line 65
    const-wide/16 v2, -0x1

    aput-wide v2, v0, v7

    const/4 v1, 0x3

    const-wide/16 v2, -0x1

    aput-wide v2, v0, v1

    .line 64
    sput-object v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjbitVec2:[J

    .line 497
    const/16 v0, 0x13

    new-array v0, v0, [I

    .line 498
    const/16 v1, 0x20

    aput v1, v0, v6

    const/16 v1, 0x21

    aput v1, v0, v5

    const/16 v1, 0x22

    aput v1, v0, v7

    const/4 v1, 0x3

    const/16 v2, 0x23

    aput v2, v0, v1

    const/16 v1, 0x25

    aput v1, v0, v8

    const/4 v1, 0x5

    const/16 v2, 0x18

    aput v2, v0, v1

    const/4 v1, 0x6

    const/16 v2, 0x1b

    aput v2, v0, v1

    const/4 v1, 0x7

    const/16 v2, 0x1c

    aput v2, v0, v1

    const/16 v1, 0x8

    const/16 v2, 0x14

    aput v2, v0, v1

    const/16 v1, 0x9

    const/16 v2, 0x11

    aput v2, v0, v1

    const/16 v1, 0xa

    const/16 v2, 0x15

    aput v2, v0, v1

    const/16 v1, 0xb

    const/16 v2, 0x12

    aput v2, v0, v1

    const/16 v1, 0xc

    const/16 v2, 0x1b

    aput v2, v0, v1

    const/16 v1, 0xd

    const/16 v2, 0x1c

    aput v2, v0, v1

    const/16 v1, 0xe

    const/16 v2, 0x1e

    aput v2, v0, v1

    const/16 v1, 0xf

    const/16 v2, 0x18

    aput v2, v0, v1

    const/16 v1, 0x10

    .line 499
    const/16 v2, 0x19

    aput v2, v0, v1

    const/16 v1, 0x12

    aput v5, v0, v1

    .line 497
    sput-object v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjnextStates:[I

    .line 515
    const/16 v0, 0x18

    new-array v0, v0, [Ljava/lang/String;

    .line 516
    const-string v1, ""

    aput-object v1, v0, v6

    const/16 v1, 0xd

    .line 517
    const-string v2, "("

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, ":"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "^"

    aput-object v2, v0, v1

    .line 515
    sput-object v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjstrLiteralImages:[Ljava/lang/String;

    .line 520
    new-array v0, v7, [Ljava/lang/String;

    .line 521
    const-string v1, "Boost"

    aput-object v1, v0, v6

    .line 522
    const-string v1, "DEFAULT"

    aput-object v1, v0, v5

    .line 520
    sput-object v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->lexStateNames:[Ljava/lang/String;

    .line 526
    const/16 v0, 0x18

    new-array v0, v0, [I

    .line 527
    aput v4, v0, v6

    aput v4, v0, v5

    aput v4, v0, v7

    const/4 v1, 0x3

    aput v4, v0, v1

    aput v4, v0, v8

    const/4 v1, 0x5

    aput v4, v0, v1

    const/4 v1, 0x6

    aput v4, v0, v1

    const/4 v1, 0x7

    aput v4, v0, v1

    const/16 v1, 0x8

    aput v4, v0, v1

    const/16 v1, 0x9

    aput v4, v0, v1

    const/16 v1, 0xa

    aput v4, v0, v1

    const/16 v1, 0xb

    aput v4, v0, v1

    const/16 v1, 0xc

    aput v4, v0, v1

    const/16 v1, 0xd

    aput v4, v0, v1

    const/16 v1, 0xe

    aput v4, v0, v1

    const/16 v1, 0xf

    aput v4, v0, v1

    const/16 v1, 0x10

    aput v4, v0, v1

    const/16 v1, 0x12

    aput v4, v0, v1

    const/16 v1, 0x13

    aput v4, v0, v1

    const/16 v1, 0x14

    aput v4, v0, v1

    const/16 v1, 0x15

    aput v4, v0, v1

    const/16 v1, 0x16

    aput v4, v0, v1

    const/16 v1, 0x17

    aput v5, v0, v1

    .line 526
    sput-object v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjnewLexState:[I

    .line 529
    new-array v0, v5, [J

    .line 530
    const-wide/32 v2, 0xffff01

    aput-wide v2, v0, v6

    .line 529
    sput-object v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjtoToken:[J

    .line 532
    new-array v0, v5, [J

    .line 533
    const-wide/16 v2, 0x80

    aput-wide v2, v0, v6

    .line 532
    sput-object v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjtoSkip:[J

    .line 534
    return-void

    .line 61
    nop

    :array_0
    .array-data 8
        -0x2
        -0x1
        -0x1
        -0x1
    .end array-data
.end method

.method public constructor <init>(Lorg/apache/lucene/queryparser/surround/parser/CharStream;)V
    .locals 2
    .param p1, "stream"    # Lorg/apache/lucene/queryparser/surround/parser/CharStream;

    .prologue
    const/4 v1, 0x1

    .line 540
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    iput-object v0, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->debugStream:Ljava/io/PrintStream;

    .line 536
    const/16 v0, 0x26

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjrounds:[I

    .line 537
    const/16 v0, 0x4c

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjstateSet:[I

    .line 606
    iput v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curLexState:I

    .line 607
    iput v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->defaultLexState:I

    .line 541
    iput-object p1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/surround/parser/CharStream;

    .line 542
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/queryparser/surround/parser/CharStream;I)V
    .locals 0
    .param p1, "stream"    # Lorg/apache/lucene/queryparser/surround/parser/CharStream;
    .param p2, "lexState"    # I

    .prologue
    .line 546
    invoke-direct {p0, p1}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;-><init>(Lorg/apache/lucene/queryparser/surround/parser/CharStream;)V

    .line 547
    invoke-virtual {p0, p2}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->SwitchTo(I)V

    .line 548
    return-void
.end method

.method private ReInitRounds()V
    .locals 4

    .prologue
    .line 561
    const v2, -0x7fffffff

    iput v2, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjround:I

    .line 562
    const/16 v0, 0x26

    .local v0, "i":I
    move v1, v0

    .end local v0    # "i":I
    .local v1, "i":I
    :goto_0
    add-int/lit8 v0, v1, -0x1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    if-gtz v1, :cond_0

    .line 564
    return-void

    .line 563
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjrounds:[I

    const/high16 v3, -0x80000000

    aput v3, v2, v0

    move v1, v0

    .end local v0    # "i":I
    .restart local v1    # "i":I
    goto :goto_0
.end method

.method private jjAddStates(II)V
    .locals 4
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 698
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjstateSet:[I

    iget v2, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjnewStateCnt:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjnewStateCnt:I

    sget-object v3, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjnextStates:[I

    aget v3, v3, p1

    aput v3, v1, v2

    .line 699
    add-int/lit8 v0, p1, 0x1

    .end local p1    # "start":I
    .local v0, "start":I
    if-ne p1, p2, :cond_0

    .line 700
    return-void

    :cond_0
    move p1, v0

    .end local v0    # "start":I
    .restart local p1    # "start":I
    goto :goto_0
.end method

.method private static final jjCanMove_0(IIIJJ)Z
    .locals 7
    .param p0, "hiByte"    # I
    .param p1, "i1"    # I
    .param p2, "i2"    # I
    .param p3, "l1"    # J
    .param p5, "l2"    # J

    .prologue
    const-wide/16 v4, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 503
    packed-switch p0, :pswitch_data_0

    .line 508
    sget-object v2, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjbitVec0:[J

    aget-wide v2, v2, p1

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 510
    :cond_0
    :goto_0
    return v0

    .line 506
    :pswitch_0
    sget-object v2, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjbitVec2:[J

    aget-wide v2, v2, p2

    and-long/2addr v2, p5

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 510
    goto :goto_0

    .line 503
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method private jjCheckNAdd(I)V
    .locals 3
    .param p1, "state"    # I

    .prologue
    .line 689
    iget-object v0, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjrounds:[I

    aget v0, v0, p1

    iget v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjround:I

    if-eq v0, v1, :cond_0

    .line 691
    iget-object v0, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjstateSet:[I

    iget v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjnewStateCnt:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjnewStateCnt:I

    aput p1, v0, v1

    .line 692
    iget-object v0, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjrounds:[I

    iget v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjround:I

    aput v1, v0, p1

    .line 694
    :cond_0
    return-void
.end method

.method private jjCheckNAddStates(II)V
    .locals 2
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 710
    :goto_0
    sget-object v1, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjnextStates:[I

    aget v1, v1, p1

    invoke-direct {p0, v1}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjCheckNAdd(I)V

    .line 711
    add-int/lit8 v0, p1, 0x1

    .end local p1    # "start":I
    .local v0, "start":I
    if-ne p1, p2, :cond_0

    .line 712
    return-void

    :cond_0
    move p1, v0

    .end local v0    # "start":I
    .restart local p1    # "start":I
    goto :goto_0
.end method

.method private jjCheckNAddTwoStates(II)V
    .locals 0
    .param p1, "state1"    # I
    .param p2, "state2"    # I

    .prologue
    .line 703
    invoke-direct {p0, p1}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjCheckNAdd(I)V

    .line 704
    invoke-direct {p0, p2}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjCheckNAdd(I)V

    .line 705
    return-void
.end method

.method private jjMoveNfa_0(II)I
    .locals 20
    .param p1, "startState"    # I
    .param p2, "curPos"    # I

    .prologue
    .line 420
    const/4 v14, 0x0

    .line 421
    .local v14, "startsAt":I
    const/4 v15, 0x3

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjnewStateCnt:I

    .line 422
    const/4 v4, 0x1

    .line 423
    .local v4, "i":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjstateSet:[I

    const/16 v16, 0x0

    aput p1, v15, v16

    .line 424
    const v7, 0x7fffffff

    .line 427
    .local v7, "kind":I
    :goto_0
    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjround:I

    add-int/lit8 v15, v15, 0x1

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjround:I

    const v16, 0x7fffffff

    move/from16 v0, v16

    if-ne v15, v0, :cond_0

    .line 428
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->ReInitRounds()V

    .line 429
    :cond_0
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x40

    move/from16 v0, v16

    if-ge v15, v0, :cond_6

    .line 431
    const-wide/16 v16, 0x1

    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curChar:C

    shl-long v8, v16, v15

    .line 434
    .local v8, "l":J
    :cond_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjstateSet:[I

    add-int/lit8 v4, v4, -0x1

    aget v15, v15, v4

    packed-switch v15, :pswitch_data_0

    .line 456
    :cond_2
    :goto_1
    if-ne v4, v14, :cond_1

    .line 484
    .end local v8    # "l":J
    :goto_2
    const v15, 0x7fffffff

    if-eq v7, v15, :cond_3

    .line 486
    move-object/from16 v0, p0

    iput v7, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjmatchedKind:I

    .line 487
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjmatchedPos:I

    .line 488
    const v7, 0x7fffffff

    .line 490
    :cond_3
    add-int/lit8 p2, p2, 0x1

    .line 491
    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjnewStateCnt:I

    move-object/from16 v0, p0

    iput v14, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjnewStateCnt:I

    rsub-int/lit8 v14, v14, 0x3

    if-ne v4, v14, :cond_a

    .line 494
    :goto_3
    return p2

    .line 437
    .restart local v8    # "l":J
    :pswitch_0
    const-wide/high16 v16, 0x3ff000000000000L

    and-long v16, v16, v8

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_2

    .line 439
    const/16 v15, 0x17

    if-le v7, v15, :cond_4

    .line 440
    const/16 v7, 0x17

    .line 441
    :cond_4
    const/16 v15, 0x11

    const/16 v16, 0x12

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjAddStates(II)V

    goto :goto_1

    .line 444
    :pswitch_1
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x2e

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 445
    const/4 v15, 0x2

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjCheckNAdd(I)V

    goto :goto_1

    .line 448
    :pswitch_2
    const-wide/high16 v16, 0x3ff000000000000L

    and-long v16, v16, v8

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_2

    .line 450
    const/16 v15, 0x17

    if-le v7, v15, :cond_5

    .line 451
    const/16 v7, 0x17

    .line 452
    :cond_5
    const/4 v15, 0x2

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjCheckNAdd(I)V

    goto :goto_1

    .line 458
    .end local v8    # "l":J
    :cond_6
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x80

    move/from16 v0, v16

    if-ge v15, v0, :cond_8

    .line 460
    const-wide/16 v16, 0x1

    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curChar:C

    and-int/lit8 v15, v15, 0x3f

    shl-long v8, v16, v15

    .line 463
    .restart local v8    # "l":J
    :cond_7
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjstateSet:[I

    add-int/lit8 v4, v4, -0x1

    aget v15, v15, v4

    .line 467
    if-ne v4, v14, :cond_7

    goto/16 :goto_2

    .line 471
    .end local v8    # "l":J
    :cond_8
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curChar:C

    shr-int/lit8 v3, v15, 0x8

    .line 472
    .local v3, "hiByte":I
    shr-int/lit8 v5, v3, 0x6

    .line 473
    .local v5, "i1":I
    const-wide/16 v16, 0x1

    and-int/lit8 v15, v3, 0x3f

    shl-long v10, v16, v15

    .line 474
    .local v10, "l1":J
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curChar:C

    and-int/lit16 v15, v15, 0xff

    shr-int/lit8 v6, v15, 0x6

    .line 475
    .local v6, "i2":I
    const-wide/16 v16, 0x1

    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curChar:C

    and-int/lit8 v15, v15, 0x3f

    shl-long v12, v16, v15

    .line 478
    .local v12, "l2":J
    :cond_9
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjstateSet:[I

    add-int/lit8 v4, v4, -0x1

    aget v15, v15, v4

    .line 482
    if-ne v4, v14, :cond_9

    goto/16 :goto_2

    .line 493
    .end local v3    # "hiByte":I
    .end local v5    # "i1":I
    .end local v6    # "i2":I
    .end local v10    # "l1":J
    .end local v12    # "l2":J
    :cond_a
    :try_start_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/surround/parser/CharStream;

    invoke-interface {v15}, Lorg/apache/lucene/queryparser/surround/parser/CharStream;->readChar()C

    move-result v15

    move-object/from16 v0, p0

    iput-char v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 494
    :catch_0
    move-exception v2

    .local v2, "e":Ljava/io/IOException;
    goto/16 :goto_3

    .line 434
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private jjMoveNfa_1(II)I
    .locals 20
    .param p1, "startState"    # I
    .param p2, "curPos"    # I

    .prologue
    .line 69
    const/4 v14, 0x0

    .line 70
    .local v14, "startsAt":I
    const/16 v15, 0x26

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjnewStateCnt:I

    .line 71
    const/4 v10, 0x1

    .line 72
    .local v10, "i":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjstateSet:[I

    const/16 v16, 0x0

    aput p1, v15, v16

    .line 73
    const v11, 0x7fffffff

    .line 76
    .local v11, "kind":I
    :goto_0
    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjround:I

    add-int/lit8 v15, v15, 0x1

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjround:I

    const v16, 0x7fffffff

    move/from16 v0, v16

    if-ne v15, v0, :cond_0

    .line 77
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->ReInitRounds()V

    .line 78
    :cond_0
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x40

    move/from16 v0, v16

    if-ge v15, v0, :cond_d

    .line 80
    const-wide/16 v16, 0x1

    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curChar:C

    shl-long v12, v16, v15

    .line 83
    .local v12, "l":J
    :cond_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjstateSet:[I

    add-int/lit8 v10, v10, -0x1

    aget v15, v15, v10

    packed-switch v15, :pswitch_data_0

    .line 190
    :cond_2
    :goto_1
    :pswitch_0
    if-ne v10, v14, :cond_1

    .line 401
    .end local v12    # "l":J
    :goto_2
    const v15, 0x7fffffff

    if-eq v11, v15, :cond_3

    .line 403
    move-object/from16 v0, p0

    iput v11, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjmatchedKind:I

    .line 404
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjmatchedPos:I

    .line 405
    const v11, 0x7fffffff

    .line 407
    :cond_3
    add-int/lit8 p2, p2, 0x1

    .line 408
    move-object/from16 v0, p0

    iget v10, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjnewStateCnt:I

    move-object/from16 v0, p0

    iput v14, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjnewStateCnt:I

    rsub-int/lit8 v14, v14, 0x26

    if-ne v10, v14, :cond_22

    .line 411
    :goto_3
    return p2

    .line 86
    .restart local v12    # "l":J
    :pswitch_1
    const-wide v16, 0x7bffe8faffffd9ffL    # 1.9435859331268573E289

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_6

    .line 88
    const/16 v15, 0x16

    if-le v11, v15, :cond_4

    .line 89
    const/16 v11, 0x16

    .line 90
    :cond_4
    const/4 v15, 0x0

    const/16 v16, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjCheckNAddStates(II)V

    .line 99
    :cond_5
    :goto_4
    const-wide/high16 v16, 0x3fc000000000000L

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_8

    .line 100
    const/16 v15, 0x8

    const/16 v16, 0xb

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto :goto_1

    .line 92
    :cond_6
    const-wide v16, 0x100002600L    # 2.122000597E-314

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_7

    .line 94
    const/4 v15, 0x7

    if-le v11, v15, :cond_5

    .line 95
    const/4 v11, 0x7

    .line 96
    goto :goto_4

    .line 97
    :cond_7
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x22

    move/from16 v0, v16

    if-ne v15, v0, :cond_5

    .line 98
    const/4 v15, 0x5

    const/16 v16, 0x7

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto :goto_4

    .line 101
    :cond_8
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x31

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 102
    const/16 v15, 0x14

    const/16 v16, 0x15

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 105
    :pswitch_2
    const-wide/high16 v16, 0x3fc000000000000L

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_2

    .line 106
    const/16 v15, 0x8

    const/16 v16, 0xb

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 109
    :pswitch_3
    const-wide/high16 v16, 0x3ff000000000000L

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_2

    .line 110
    const/16 v15, 0x11

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_1

    .line 113
    :pswitch_4
    const-wide/high16 v16, 0x3ff000000000000L

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_2

    .line 114
    const/16 v15, 0x12

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_1

    .line 117
    :pswitch_5
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x31

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 118
    const/16 v15, 0x14

    const/16 v16, 0x15

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 121
    :pswitch_6
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x22

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 122
    const/4 v15, 0x5

    const/16 v16, 0x7

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 125
    :pswitch_7
    const-wide v16, -0x400000001L

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_2

    .line 126
    const/16 v15, 0x18

    const/16 v16, 0x19

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 129
    :pswitch_8
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x22

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 130
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x1a

    aput v17, v15, v16

    goto/16 :goto_1

    .line 133
    :pswitch_9
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x2a

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x12

    if-le v11, v15, :cond_2

    .line 134
    const/16 v11, 0x12

    .line 135
    goto/16 :goto_1

    .line 137
    :pswitch_a
    const-wide v16, -0x400000001L

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_2

    .line 138
    const/16 v15, 0xc

    const/16 v16, 0xe

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 141
    :pswitch_b
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x22

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 142
    const/16 v15, 0xc

    const/16 v16, 0xe

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 145
    :pswitch_c
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x22

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x13

    if-le v11, v15, :cond_2

    .line 146
    const/16 v11, 0x13

    .line 147
    goto/16 :goto_1

    .line 149
    :pswitch_d
    const-wide v16, 0x7bffe8faffffd9ffL    # 1.9435859331268573E289

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_2

    .line 151
    const/16 v15, 0x16

    if-le v11, v15, :cond_9

    .line 152
    const/16 v11, 0x16

    .line 153
    :cond_9
    const/4 v15, 0x0

    const/16 v16, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 156
    :pswitch_e
    const-wide v16, 0x7bffe8faffffd9ffL    # 1.9435859331268573E289

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_2

    .line 157
    const/16 v15, 0x20

    const/16 v16, 0x21

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 160
    :pswitch_f
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x2a

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x14

    if-le v11, v15, :cond_2

    .line 161
    const/16 v11, 0x14

    .line 162
    goto/16 :goto_1

    .line 164
    :pswitch_10
    const-wide v16, 0x7bffe8faffffd9ffL    # 1.9435859331268573E289

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_2

    .line 165
    const/16 v15, 0x22

    const/16 v16, 0x23

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 168
    :pswitch_11
    const-wide v16, -0x7ffffc0000000000L    # -2.1729236899484E-311

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_2

    .line 170
    const/16 v15, 0x15

    if-le v11, v15, :cond_a

    .line 171
    const/16 v11, 0x15

    .line 172
    :cond_a
    const/16 v15, 0x23

    const/16 v16, 0x24

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 175
    :pswitch_12
    const-wide v16, -0x400130500002601L

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_2

    .line 177
    const/16 v15, 0x15

    if-le v11, v15, :cond_b

    .line 178
    const/16 v11, 0x15

    .line 179
    :cond_b
    const/16 v15, 0x24

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_1

    .line 182
    :pswitch_13
    const-wide v16, 0x7bffe8faffffd9ffL    # 1.9435859331268573E289

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_2

    .line 184
    const/16 v15, 0x16

    if-le v11, v15, :cond_c

    .line 185
    const/16 v11, 0x16

    .line 186
    :cond_c
    const/16 v15, 0x25

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_1

    .line 192
    .end local v12    # "l":J
    :cond_d
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x80

    move/from16 v0, v16

    if-ge v15, v0, :cond_1c

    .line 194
    const-wide/16 v16, 0x1

    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curChar:C

    and-int/lit8 v15, v15, 0x3f

    shl-long v12, v16, v15

    .line 197
    .restart local v12    # "l":J
    :cond_e
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjstateSet:[I

    add-int/lit8 v10, v10, -0x1

    aget v15, v15, v10

    packed-switch v15, :pswitch_data_1

    .line 347
    :cond_f
    :goto_5
    :pswitch_14
    if-ne v10, v14, :cond_e

    goto/16 :goto_2

    .line 200
    :pswitch_15
    const-wide/32 v16, -0x40000001

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_11

    .line 202
    const/16 v15, 0x16

    if-le v11, v15, :cond_10

    .line 203
    const/16 v11, 0x16

    .line 204
    :cond_10
    const/4 v15, 0x0

    const/16 v16, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjCheckNAddStates(II)V

    .line 206
    :cond_11
    const-wide v16, 0x400000004000L

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_13

    .line 208
    const/16 v15, 0xc

    if-le v11, v15, :cond_12

    .line 209
    const/16 v11, 0xc

    .line 224
    :cond_12
    :goto_6
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x6e

    move/from16 v0, v16

    if-ne v15, v0, :cond_18

    .line 225
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0xf

    aput v17, v15, v16

    goto :goto_5

    .line 211
    :cond_13
    const-wide v16, 0x80000000800000L

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_14

    .line 213
    const/16 v15, 0xb

    if-le v11, v15, :cond_12

    .line 214
    const/16 v11, 0xb

    .line 215
    goto :goto_6

    .line 216
    :cond_14
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x61

    move/from16 v0, v16

    if-ne v15, v0, :cond_15

    .line 217
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x9

    aput v17, v15, v16

    goto :goto_6

    .line 218
    :cond_15
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x41

    move/from16 v0, v16

    if-ne v15, v0, :cond_16

    .line 219
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x6

    aput v17, v15, v16

    goto :goto_6

    .line 220
    :cond_16
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x6f

    move/from16 v0, v16

    if-ne v15, v0, :cond_17

    .line 221
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x3

    aput v17, v15, v16

    goto/16 :goto_6

    .line 222
    :cond_17
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x4f

    move/from16 v0, v16

    if-ne v15, v0, :cond_12

    .line 223
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x1

    aput v17, v15, v16

    goto/16 :goto_6

    .line 226
    :cond_18
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x4e

    move/from16 v0, v16

    if-ne v15, v0, :cond_f

    .line 227
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0xc

    aput v17, v15, v16

    goto/16 :goto_5

    .line 230
    :pswitch_16
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x52

    move/from16 v0, v16

    if-ne v15, v0, :cond_f

    const/16 v15, 0x8

    if-le v11, v15, :cond_f

    .line 231
    const/16 v11, 0x8

    .line 232
    goto/16 :goto_5

    .line 234
    :pswitch_17
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x4f

    move/from16 v0, v16

    if-ne v15, v0, :cond_f

    .line 235
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x1

    aput v17, v15, v16

    goto/16 :goto_5

    .line 238
    :pswitch_18
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x72

    move/from16 v0, v16

    if-ne v15, v0, :cond_f

    const/16 v15, 0x8

    if-le v11, v15, :cond_f

    .line 239
    const/16 v11, 0x8

    .line 240
    goto/16 :goto_5

    .line 242
    :pswitch_19
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x6f

    move/from16 v0, v16

    if-ne v15, v0, :cond_f

    .line 243
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x3

    aput v17, v15, v16

    goto/16 :goto_5

    .line 246
    :pswitch_1a
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x44

    move/from16 v0, v16

    if-ne v15, v0, :cond_f

    const/16 v15, 0x9

    if-le v11, v15, :cond_f

    .line 247
    const/16 v11, 0x9

    .line 248
    goto/16 :goto_5

    .line 250
    :pswitch_1b
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x4e

    move/from16 v0, v16

    if-ne v15, v0, :cond_f

    .line 251
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x5

    aput v17, v15, v16

    goto/16 :goto_5

    .line 254
    :pswitch_1c
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x41

    move/from16 v0, v16

    if-ne v15, v0, :cond_f

    .line 255
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x6

    aput v17, v15, v16

    goto/16 :goto_5

    .line 258
    :pswitch_1d
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x64

    move/from16 v0, v16

    if-ne v15, v0, :cond_f

    const/16 v15, 0x9

    if-le v11, v15, :cond_f

    .line 259
    const/16 v11, 0x9

    .line 260
    goto/16 :goto_5

    .line 262
    :pswitch_1e
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x6e

    move/from16 v0, v16

    if-ne v15, v0, :cond_f

    .line 263
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x8

    aput v17, v15, v16

    goto/16 :goto_5

    .line 266
    :pswitch_1f
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x61

    move/from16 v0, v16

    if-ne v15, v0, :cond_f

    .line 267
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x9

    aput v17, v15, v16

    goto/16 :goto_5

    .line 270
    :pswitch_20
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x54

    move/from16 v0, v16

    if-ne v15, v0, :cond_f

    const/16 v15, 0xa

    if-le v11, v15, :cond_f

    .line 271
    const/16 v11, 0xa

    .line 272
    goto/16 :goto_5

    .line 274
    :pswitch_21
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x4f

    move/from16 v0, v16

    if-ne v15, v0, :cond_f

    .line 275
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0xb

    aput v17, v15, v16

    goto/16 :goto_5

    .line 278
    :pswitch_22
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x4e

    move/from16 v0, v16

    if-ne v15, v0, :cond_f

    .line 279
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0xc

    aput v17, v15, v16

    goto/16 :goto_5

    .line 282
    :pswitch_23
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x74

    move/from16 v0, v16

    if-ne v15, v0, :cond_f

    const/16 v15, 0xa

    if-le v11, v15, :cond_f

    .line 283
    const/16 v11, 0xa

    .line 284
    goto/16 :goto_5

    .line 286
    :pswitch_24
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x6f

    move/from16 v0, v16

    if-ne v15, v0, :cond_f

    .line 287
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0xe

    aput v17, v15, v16

    goto/16 :goto_5

    .line 290
    :pswitch_25
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x6e

    move/from16 v0, v16

    if-ne v15, v0, :cond_f

    .line 291
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0xf

    aput v17, v15, v16

    goto/16 :goto_5

    .line 294
    :pswitch_26
    const-wide v16, 0x80000000800000L

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_f

    const/16 v15, 0xb

    if-le v11, v15, :cond_f

    .line 295
    const/16 v11, 0xb

    .line 296
    goto/16 :goto_5

    .line 298
    :pswitch_27
    const-wide v16, 0x400000004000L

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_f

    const/16 v15, 0xc

    if-le v11, v15, :cond_f

    .line 299
    const/16 v11, 0xc

    .line 300
    goto/16 :goto_5

    .line 302
    :pswitch_28
    const/16 v15, 0xf

    const/16 v16, 0x10

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjAddStates(II)V

    goto/16 :goto_5

    .line 305
    :pswitch_29
    const-wide/32 v16, -0x10000001

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_f

    .line 306
    const/16 v15, 0xc

    const/16 v16, 0xe

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_5

    .line 309
    :pswitch_2a
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x5c

    move/from16 v0, v16

    if-ne v15, v0, :cond_f

    .line 310
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x1d

    aput v17, v15, v16

    goto/16 :goto_5

    .line 313
    :pswitch_2b
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x5c

    move/from16 v0, v16

    if-ne v15, v0, :cond_f

    .line 314
    const/16 v15, 0xc

    const/16 v16, 0xe

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_5

    .line 317
    :pswitch_2c
    const-wide/32 v16, -0x40000001

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_f

    .line 319
    const/16 v15, 0x16

    if-le v11, v15, :cond_19

    .line 320
    const/16 v11, 0x16

    .line 321
    :cond_19
    const/4 v15, 0x0

    const/16 v16, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_5

    .line 324
    :pswitch_2d
    const-wide/32 v16, -0x40000001

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_f

    .line 325
    const/16 v15, 0x20

    const/16 v16, 0x21

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_5

    .line 328
    :pswitch_2e
    const-wide/32 v16, -0x40000001

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_f

    .line 329
    const/16 v15, 0x22

    const/16 v16, 0x23

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_5

    .line 332
    :pswitch_2f
    const-wide/32 v16, -0x40000001

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_f

    .line 334
    const/16 v15, 0x15

    if-le v11, v15, :cond_1a

    .line 335
    const/16 v11, 0x15

    .line 336
    :cond_1a
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x24

    aput v17, v15, v16

    goto/16 :goto_5

    .line 339
    :pswitch_30
    const-wide/32 v16, -0x40000001

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_f

    .line 341
    const/16 v15, 0x16

    if-le v11, v15, :cond_1b

    .line 342
    const/16 v11, 0x16

    .line 343
    :cond_1b
    const/16 v15, 0x25

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_5

    .line 351
    .end local v12    # "l":J
    :cond_1c
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curChar:C

    shr-int/lit8 v3, v15, 0x8

    .line 352
    .local v3, "hiByte":I
    shr-int/lit8 v4, v3, 0x6

    .line 353
    .local v4, "i1":I
    const-wide/16 v16, 0x1

    and-int/lit8 v15, v3, 0x3f

    shl-long v6, v16, v15

    .line 354
    .local v6, "l1":J
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curChar:C

    and-int/lit16 v15, v15, 0xff

    shr-int/lit8 v5, v15, 0x6

    .line 355
    .local v5, "i2":I
    const-wide/16 v16, 0x1

    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curChar:C

    and-int/lit8 v15, v15, 0x3f

    shl-long v8, v16, v15

    .line 358
    .local v8, "l2":J
    :cond_1d
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjstateSet:[I

    add-int/lit8 v10, v10, -0x1

    aget v15, v15, v10

    sparse-switch v15, :sswitch_data_0

    .line 399
    :cond_1e
    :goto_7
    if-ne v10, v14, :cond_1d

    goto/16 :goto_2

    .line 361
    :sswitch_0
    invoke-static/range {v3 .. v9}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjCanMove_0(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_1e

    .line 363
    const/16 v15, 0x16

    if-le v11, v15, :cond_1f

    .line 364
    const/16 v11, 0x16

    .line 365
    :cond_1f
    const/4 v15, 0x0

    const/16 v16, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto :goto_7

    .line 368
    :sswitch_1
    invoke-static/range {v3 .. v9}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjCanMove_0(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_1e

    .line 369
    const/16 v15, 0xf

    const/16 v16, 0x10

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjAddStates(II)V

    goto :goto_7

    .line 372
    :sswitch_2
    invoke-static/range {v3 .. v9}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjCanMove_0(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_1e

    .line 373
    const/16 v15, 0xc

    const/16 v16, 0xe

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjAddStates(II)V

    goto :goto_7

    .line 376
    :sswitch_3
    invoke-static/range {v3 .. v9}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjCanMove_0(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_1e

    .line 377
    const/16 v15, 0x20

    const/16 v16, 0x21

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto :goto_7

    .line 380
    :sswitch_4
    invoke-static/range {v3 .. v9}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjCanMove_0(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_1e

    .line 381
    const/16 v15, 0x22

    const/16 v16, 0x23

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto :goto_7

    .line 384
    :sswitch_5
    invoke-static/range {v3 .. v9}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjCanMove_0(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_1e

    .line 386
    const/16 v15, 0x15

    if-le v11, v15, :cond_20

    .line 387
    const/16 v11, 0x15

    .line 388
    :cond_20
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x24

    aput v17, v15, v16

    goto/16 :goto_7

    .line 391
    :sswitch_6
    invoke-static/range {v3 .. v9}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjCanMove_0(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_1e

    .line 393
    const/16 v15, 0x16

    if-le v11, v15, :cond_21

    .line 394
    const/16 v11, 0x16

    .line 395
    :cond_21
    const/16 v15, 0x25

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_7

    .line 410
    .end local v3    # "hiByte":I
    .end local v4    # "i1":I
    .end local v5    # "i2":I
    .end local v6    # "l1":J
    .end local v8    # "l2":J
    :cond_22
    :try_start_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/surround/parser/CharStream;

    invoke-interface {v15}, Lorg/apache/lucene/queryparser/surround/parser/CharStream;->readChar()C

    move-result v15

    move-object/from16 v0, p0

    iput-char v15, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 411
    :catch_0
    move-exception v2

    .local v2, "e":Ljava/io/IOException;
    goto/16 :goto_3

    .line 83
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_0
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
    .end packed-switch

    .line 197
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_14
        :pswitch_14
        :pswitch_14
        :pswitch_14
        :pswitch_14
        :pswitch_28
        :pswitch_14
        :pswitch_14
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_14
        :pswitch_2c
        :pswitch_2d
        :pswitch_14
        :pswitch_2e
        :pswitch_14
        :pswitch_2f
        :pswitch_30
    .end packed-switch

    .line 358
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x18 -> :sswitch_1
        0x1b -> :sswitch_2
        0x20 -> :sswitch_3
        0x22 -> :sswitch_4
        0x24 -> :sswitch_5
        0x25 -> :sswitch_6
    .end sparse-switch
.end method

.method private jjMoveStringLiteralDfa0_0()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 416
    invoke-direct {p0, v0, v0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjMoveNfa_0(II)I

    move-result v0

    return v0
.end method

.method private jjMoveStringLiteralDfa0_1()I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 45
    iget-char v0, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curChar:C

    sparse-switch v0, :sswitch_data_0

    .line 58
    invoke-direct {p0, v1, v1}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjMoveNfa_1(II)I

    move-result v0

    :goto_0
    return v0

    .line 48
    :sswitch_0
    const/16 v0, 0xd

    invoke-direct {p0, v1, v0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 50
    :sswitch_1
    const/16 v0, 0xe

    invoke-direct {p0, v1, v0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 52
    :sswitch_2
    const/16 v0, 0xf

    invoke-direct {p0, v1, v0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 54
    :sswitch_3
    const/16 v0, 0x10

    invoke-direct {p0, v1, v0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 56
    :sswitch_4
    const/16 v0, 0x11

    invoke-direct {p0, v1, v0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 45
    :sswitch_data_0
    .sparse-switch
        0x28 -> :sswitch_0
        0x29 -> :sswitch_1
        0x2c -> :sswitch_2
        0x3a -> :sswitch_3
        0x5e -> :sswitch_4
    .end sparse-switch
.end method

.method private final jjStartNfa_1(IJ)I
    .locals 2
    .param p1, "pos"    # I
    .param p2, "active0"    # J

    .prologue
    .line 35
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjStopStringLiteralDfa_1(IJ)I

    move-result v0

    add-int/lit8 v1, p1, 0x1

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjMoveNfa_1(II)I

    move-result v0

    return v0
.end method

.method private jjStopAtPos(II)I
    .locals 1
    .param p1, "pos"    # I
    .param p2, "kind"    # I

    .prologue
    .line 39
    iput p2, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjmatchedKind:I

    .line 40
    iput p1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjmatchedPos:I

    .line 41
    add-int/lit8 v0, p1, 0x1

    return v0
.end method

.method private final jjStopStringLiteralDfa_1(IJ)I
    .locals 1
    .param p1, "pos"    # I
    .param p2, "active0"    # J

    .prologue
    .line 27
    .line 30
    const/4 v0, -0x1

    return v0
.end method


# virtual methods
.method public ReInit(Lorg/apache/lucene/queryparser/surround/parser/CharStream;)V
    .locals 1
    .param p1, "stream"    # Lorg/apache/lucene/queryparser/surround/parser/CharStream;

    .prologue
    .line 553
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjnewStateCnt:I

    iput v0, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjmatchedPos:I

    .line 554
    iget v0, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->defaultLexState:I

    iput v0, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curLexState:I

    .line 555
    iput-object p1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/surround/parser/CharStream;

    .line 556
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->ReInitRounds()V

    .line 557
    return-void
.end method

.method public ReInit(Lorg/apache/lucene/queryparser/surround/parser/CharStream;I)V
    .locals 0
    .param p1, "stream"    # Lorg/apache/lucene/queryparser/surround/parser/CharStream;
    .param p2, "lexState"    # I

    .prologue
    .line 569
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->ReInit(Lorg/apache/lucene/queryparser/surround/parser/CharStream;)V

    .line 570
    invoke-virtual {p0, p2}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->SwitchTo(I)V

    .line 571
    return-void
.end method

.method public SwitchTo(I)V
    .locals 4
    .param p1, "lexState"    # I

    .prologue
    const/4 v3, 0x2

    .line 576
    if-ge p1, v3, :cond_0

    if-gez p1, :cond_1

    .line 577
    :cond_0
    new-instance v0, Lorg/apache/lucene/queryparser/surround/parser/TokenMgrError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Error: Ignoring invalid lexical state : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". State unchanged."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v3}, Lorg/apache/lucene/queryparser/surround/parser/TokenMgrError;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 579
    :cond_1
    iput p1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curLexState:I

    .line 580
    return-void
.end method

.method public getNextToken()Lorg/apache/lucene/queryparser/surround/parser/Token;
    .locals 18

    .prologue
    .line 617
    const/4 v10, 0x0

    .line 624
    .local v10, "curPos":I
    :cond_0
    :goto_0
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/surround/parser/CharStream;

    invoke-interface {v2}, Lorg/apache/lucene/queryparser/surround/parser/CharStream;->BeginToken()C

    move-result v2

    move-object/from16 v0, p0

    iput-char v2, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 633
    move-object/from16 v0, p0

    iget v2, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curLexState:I

    packed-switch v2, :pswitch_data_0

    .line 646
    :goto_1
    move-object/from16 v0, p0

    iget v2, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjmatchedKind:I

    const v4, 0x7fffffff

    if-eq v2, v4, :cond_4

    .line 648
    move-object/from16 v0, p0

    iget v2, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjmatchedPos:I

    add-int/lit8 v2, v2, 0x1

    if-ge v2, v10, :cond_1

    .line 649
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/surround/parser/CharStream;

    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjmatchedPos:I

    sub-int v4, v10, v4

    add-int/lit8 v4, v4, -0x1

    invoke-interface {v2, v4}, Lorg/apache/lucene/queryparser/surround/parser/CharStream;->backup(I)V

    .line 650
    :cond_1
    sget-object v2, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjtoToken:[J

    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjmatchedKind:I

    shr-int/lit8 v4, v4, 0x6

    aget-wide v8, v2, v4

    const-wide/16 v16, 0x1

    move-object/from16 v0, p0

    iget v2, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjmatchedKind:I

    and-int/lit8 v2, v2, 0x3f

    shl-long v16, v16, v2

    and-long v8, v8, v16

    const-wide/16 v16, 0x0

    cmp-long v2, v8, v16

    if-eqz v2, :cond_3

    .line 652
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjFillToken()Lorg/apache/lucene/queryparser/surround/parser/Token;

    move-result-object v13

    .line 653
    .local v13, "matchedToken":Lorg/apache/lucene/queryparser/surround/parser/Token;
    sget-object v2, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjnewLexState:[I

    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjmatchedKind:I

    aget v2, v2, v4

    const/4 v4, -0x1

    if-eq v2, v4, :cond_2

    .line 654
    sget-object v2, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjnewLexState:[I

    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjmatchedKind:I

    aget v2, v2, v4

    move-object/from16 v0, p0

    iput v2, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curLexState:I

    :cond_2
    move-object v14, v13

    .line 655
    .end local v13    # "matchedToken":Lorg/apache/lucene/queryparser/surround/parser/Token;
    .local v14, "matchedToken":Lorg/apache/lucene/queryparser/surround/parser/Token;
    :goto_2
    return-object v14

    .line 626
    .end local v14    # "matchedToken":Lorg/apache/lucene/queryparser/surround/parser/Token;
    :catch_0
    move-exception v11

    .line 628
    .local v11, "e":Ljava/io/IOException;
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjmatchedKind:I

    .line 629
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjFillToken()Lorg/apache/lucene/queryparser/surround/parser/Token;

    move-result-object v13

    .restart local v13    # "matchedToken":Lorg/apache/lucene/queryparser/surround/parser/Token;
    move-object v14, v13

    .line 630
    .end local v13    # "matchedToken":Lorg/apache/lucene/queryparser/surround/parser/Token;
    .restart local v14    # "matchedToken":Lorg/apache/lucene/queryparser/surround/parser/Token;
    goto :goto_2

    .line 636
    .end local v11    # "e":Ljava/io/IOException;
    .end local v14    # "matchedToken":Lorg/apache/lucene/queryparser/surround/parser/Token;
    :pswitch_0
    const v2, 0x7fffffff

    move-object/from16 v0, p0

    iput v2, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjmatchedKind:I

    .line 637
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjmatchedPos:I

    .line 638
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjMoveStringLiteralDfa0_0()I

    move-result v10

    .line 639
    goto :goto_1

    .line 641
    :pswitch_1
    const v2, 0x7fffffff

    move-object/from16 v0, p0

    iput v2, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjmatchedKind:I

    .line 642
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjmatchedPos:I

    .line 643
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjMoveStringLiteralDfa0_1()I

    move-result v10

    goto/16 :goto_1

    .line 659
    :cond_3
    sget-object v2, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjnewLexState:[I

    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjmatchedKind:I

    aget v2, v2, v4

    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    .line 660
    sget-object v2, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjnewLexState:[I

    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjmatchedKind:I

    aget v2, v2, v4

    move-object/from16 v0, p0

    iput v2, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curLexState:I

    goto/16 :goto_0

    .line 664
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/surround/parser/CharStream;

    invoke-interface {v2}, Lorg/apache/lucene/queryparser/surround/parser/CharStream;->getEndLine()I

    move-result v5

    .line 665
    .local v5, "error_line":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/surround/parser/CharStream;

    invoke-interface {v2}, Lorg/apache/lucene/queryparser/surround/parser/CharStream;->getEndColumn()I

    move-result v6

    .line 666
    .local v6, "error_column":I
    const/4 v7, 0x0

    .line 667
    .local v7, "error_after":Ljava/lang/String;
    const/4 v3, 0x0

    .line 668
    .local v3, "EOFSeen":Z
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/surround/parser/CharStream;

    invoke-interface {v2}, Lorg/apache/lucene/queryparser/surround/parser/CharStream;->readChar()C

    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/surround/parser/CharStream;

    const/4 v4, 0x1

    invoke-interface {v2, v4}, Lorg/apache/lucene/queryparser/surround/parser/CharStream;->backup(I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 679
    :goto_3
    if-nez v3, :cond_5

    .line 680
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/surround/parser/CharStream;

    const/4 v4, 0x1

    invoke-interface {v2, v4}, Lorg/apache/lucene/queryparser/surround/parser/CharStream;->backup(I)V

    .line 681
    const/4 v2, 0x1

    if-gt v10, v2, :cond_9

    const-string v7, ""

    .line 683
    :cond_5
    :goto_4
    new-instance v2, Lorg/apache/lucene/queryparser/surround/parser/TokenMgrError;

    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curLexState:I

    move-object/from16 v0, p0

    iget-char v8, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curChar:C

    const/4 v9, 0x0

    invoke-direct/range {v2 .. v9}, Lorg/apache/lucene/queryparser/surround/parser/TokenMgrError;-><init>(ZIIILjava/lang/String;CI)V

    throw v2

    .line 669
    :catch_1
    move-exception v12

    .line 670
    .local v12, "e1":Ljava/io/IOException;
    const/4 v3, 0x1

    .line 671
    const/4 v2, 0x1

    if-gt v10, v2, :cond_7

    const-string v7, ""

    .line 672
    :goto_5
    move-object/from16 v0, p0

    iget-char v2, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curChar:C

    const/16 v4, 0xa

    if-eq v2, v4, :cond_6

    move-object/from16 v0, p0

    iget-char v2, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->curChar:C

    const/16 v4, 0xd

    if-ne v2, v4, :cond_8

    .line 673
    :cond_6
    add-int/lit8 v5, v5, 0x1

    .line 674
    const/4 v6, 0x0

    .line 675
    goto :goto_3

    .line 671
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/surround/parser/CharStream;

    invoke-interface {v2}, Lorg/apache/lucene/queryparser/surround/parser/CharStream;->GetImage()Ljava/lang/String;

    move-result-object v7

    goto :goto_5

    .line 677
    :cond_8
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 681
    .end local v12    # "e1":Ljava/io/IOException;
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/surround/parser/CharStream;

    invoke-interface {v2}, Lorg/apache/lucene/queryparser/surround/parser/CharStream;->GetImage()Ljava/lang/String;

    move-result-object v7

    goto :goto_4

    .line 633
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected jjFillToken()Lorg/apache/lucene/queryparser/surround/parser/Token;
    .locals 9

    .prologue
    .line 590
    sget-object v7, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjstrLiteralImages:[Ljava/lang/String;

    iget v8, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjmatchedKind:I

    aget-object v5, v7, v8

    .line 591
    .local v5, "im":Ljava/lang/String;
    if-nez v5, :cond_0

    iget-object v7, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/surround/parser/CharStream;

    invoke-interface {v7}, Lorg/apache/lucene/queryparser/surround/parser/CharStream;->GetImage()Ljava/lang/String;

    move-result-object v2

    .line 592
    .local v2, "curTokenImage":Ljava/lang/String;
    :goto_0
    iget-object v7, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/surround/parser/CharStream;

    invoke-interface {v7}, Lorg/apache/lucene/queryparser/surround/parser/CharStream;->getBeginLine()I

    move-result v1

    .line 593
    .local v1, "beginLine":I
    iget-object v7, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/surround/parser/CharStream;

    invoke-interface {v7}, Lorg/apache/lucene/queryparser/surround/parser/CharStream;->getBeginColumn()I

    move-result v0

    .line 594
    .local v0, "beginColumn":I
    iget-object v7, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/surround/parser/CharStream;

    invoke-interface {v7}, Lorg/apache/lucene/queryparser/surround/parser/CharStream;->getEndLine()I

    move-result v4

    .line 595
    .local v4, "endLine":I
    iget-object v7, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/surround/parser/CharStream;

    invoke-interface {v7}, Lorg/apache/lucene/queryparser/surround/parser/CharStream;->getEndColumn()I

    move-result v3

    .line 596
    .local v3, "endColumn":I
    iget v7, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->jjmatchedKind:I

    invoke-static {v7, v2}, Lorg/apache/lucene/queryparser/surround/parser/Token;->newToken(ILjava/lang/String;)Lorg/apache/lucene/queryparser/surround/parser/Token;

    move-result-object v6

    .line 598
    .local v6, "t":Lorg/apache/lucene/queryparser/surround/parser/Token;
    iput v1, v6, Lorg/apache/lucene/queryparser/surround/parser/Token;->beginLine:I

    .line 599
    iput v4, v6, Lorg/apache/lucene/queryparser/surround/parser/Token;->endLine:I

    .line 600
    iput v0, v6, Lorg/apache/lucene/queryparser/surround/parser/Token;->beginColumn:I

    .line 601
    iput v3, v6, Lorg/apache/lucene/queryparser/surround/parser/Token;->endColumn:I

    .line 603
    return-object v6

    .end local v0    # "beginColumn":I
    .end local v1    # "beginLine":I
    .end local v2    # "curTokenImage":Ljava/lang/String;
    .end local v3    # "endColumn":I
    .end local v4    # "endLine":I
    .end local v6    # "t":Lorg/apache/lucene/queryparser/surround/parser/Token;
    :cond_0
    move-object v2, v5

    .line 591
    goto :goto_0
.end method

.method public setDebugStream(Ljava/io/PrintStream;)V
    .locals 0
    .param p1, "ds"    # Ljava/io/PrintStream;

    .prologue
    .line 24
    iput-object p1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->debugStream:Ljava/io/PrintStream;

    return-void
.end method
