.class public Lorg/apache/lucene/queryparser/surround/parser/QueryParser;
.super Ljava/lang/Object;
.source "QueryParser.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/surround/parser/QueryParserConstants;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/queryparser/surround/parser/QueryParser$JJCalls;,
        Lorg/apache/lucene/queryparser/surround/parser/QueryParser$LookaheadSuccess;
    }
.end annotation


# static fields
.field private static jj_la1_0:[I


# instance fields
.field final anyChar:C

.field final boostErrorMessage:Ljava/lang/String;

.field final carat:C

.field final comma:C

.field final fieldOperator:C

.field private final jj_2_rtns:[Lorg/apache/lucene/queryparser/surround/parser/QueryParser$JJCalls;

.field private jj_endpos:I

.field private jj_expentries:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<[I>;"
        }
    .end annotation
.end field

.field private jj_expentry:[I

.field private jj_gc:I

.field private jj_gen:I

.field private jj_kind:I

.field private jj_la:I

.field private final jj_la1:[I

.field private jj_lastpos:Lorg/apache/lucene/queryparser/surround/parser/Token;

.field private jj_lasttokens:[I

.field private final jj_ls:Lorg/apache/lucene/queryparser/surround/parser/QueryParser$LookaheadSuccess;

.field public jj_nt:Lorg/apache/lucene/queryparser/surround/parser/Token;

.field private jj_ntk:I

.field private jj_rescan:Z

.field private jj_scanpos:Lorg/apache/lucene/queryparser/surround/parser/Token;

.field final minimumCharsInTrunc:I

.field final minimumPrefixLength:I

.field final quote:C

.field public token:Lorg/apache/lucene/queryparser/surround/parser/Token;

.field public token_source:Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;

.field final truncationErrorMessage:Ljava/lang/String;

.field final truncator:C


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .prologue
    .line 524
    invoke-static {}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_la1_init_0()V

    .line 525
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 66
    new-instance v0, Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;

    new-instance v1, Ljava/io/StringReader;

    const-string v2, ""

    invoke-direct {v1, v2}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;-><init>(Ljava/io/Reader;)V

    invoke-direct {p0, v0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;-><init>(Lorg/apache/lucene/queryparser/surround/parser/CharStream;)V

    .line 67
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/queryparser/surround/parser/CharStream;)V
    .locals 6
    .param p1, "stream"    # Lorg/apache/lucene/queryparser/surround/parser/CharStream;

    .prologue
    const/16 v5, 0xa

    const/4 v1, 0x3

    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 534
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->minimumPrefixLength:I

    .line 48
    iput v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->minimumCharsInTrunc:I

    .line 49
    const-string v1, "Too unrestrictive truncation: "

    iput-object v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->truncationErrorMessage:Ljava/lang/String;

    .line 50
    const-string v1, "Cannot handle boost value: "

    iput-object v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->boostErrorMessage:Ljava/lang/String;

    .line 53
    const/16 v1, 0x2a

    iput-char v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->truncator:C

    .line 54
    const/16 v1, 0x3f

    iput-char v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->anyChar:C

    .line 55
    const/16 v1, 0x22

    iput-char v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->quote:C

    .line 56
    const/16 v1, 0x3a

    iput-char v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->fieldOperator:C

    .line 57
    const/16 v1, 0x2c

    iput-char v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->comma:C

    .line 58
    const/16 v1, 0x5e

    iput-char v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->carat:C

    .line 521
    new-array v1, v5, [I

    iput-object v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_la1:[I

    .line 529
    const/4 v1, 0x1

    new-array v1, v1, [Lorg/apache/lucene/queryparser/surround/parser/QueryParser$JJCalls;

    iput-object v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_2_rtns:[Lorg/apache/lucene/queryparser/surround/parser/QueryParser$JJCalls;

    .line 530
    iput-boolean v4, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_rescan:Z

    .line 531
    iput v4, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_gc:I

    .line 598
    new-instance v1, Lorg/apache/lucene/queryparser/surround/parser/QueryParser$LookaheadSuccess;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser$LookaheadSuccess;-><init>(Lorg/apache/lucene/queryparser/surround/parser/QueryParser$LookaheadSuccess;)V

    iput-object v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_ls:Lorg/apache/lucene/queryparser/surround/parser/QueryParser$LookaheadSuccess;

    .line 647
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_expentries:Ljava/util/List;

    .line 649
    iput v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_kind:I

    .line 650
    const/16 v1, 0x64

    new-array v1, v1, [I

    iput-object v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_lasttokens:[I

    .line 535
    new-instance v1, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;

    invoke-direct {v1, p1}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;-><init>(Lorg/apache/lucene/queryparser/surround/parser/CharStream;)V

    iput-object v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->token_source:Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;

    .line 536
    new-instance v1, Lorg/apache/lucene/queryparser/surround/parser/Token;

    invoke-direct {v1}, Lorg/apache/lucene/queryparser/surround/parser/Token;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->token:Lorg/apache/lucene/queryparser/surround/parser/Token;

    .line 537
    iput v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_ntk:I

    .line 538
    iput v4, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_gen:I

    .line 539
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v5, :cond_0

    .line 540
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_2_rtns:[Lorg/apache/lucene/queryparser/surround/parser/QueryParser$JJCalls;

    array-length v1, v1

    if-lt v0, v1, :cond_1

    .line 541
    return-void

    .line 539
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_la1:[I

    aput v3, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 540
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_2_rtns:[Lorg/apache/lucene/queryparser/surround/parser/QueryParser$JJCalls;

    new-instance v2, Lorg/apache/lucene/queryparser/surround/parser/QueryParser$JJCalls;

    invoke-direct {v2}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser$JJCalls;-><init>()V

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public constructor <init>(Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;)V
    .locals 6
    .param p1, "tm"    # Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;

    .prologue
    const/16 v5, 0xa

    const/4 v1, 0x3

    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 554
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->minimumPrefixLength:I

    .line 48
    iput v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->minimumCharsInTrunc:I

    .line 49
    const-string v1, "Too unrestrictive truncation: "

    iput-object v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->truncationErrorMessage:Ljava/lang/String;

    .line 50
    const-string v1, "Cannot handle boost value: "

    iput-object v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->boostErrorMessage:Ljava/lang/String;

    .line 53
    const/16 v1, 0x2a

    iput-char v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->truncator:C

    .line 54
    const/16 v1, 0x3f

    iput-char v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->anyChar:C

    .line 55
    const/16 v1, 0x22

    iput-char v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->quote:C

    .line 56
    const/16 v1, 0x3a

    iput-char v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->fieldOperator:C

    .line 57
    const/16 v1, 0x2c

    iput-char v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->comma:C

    .line 58
    const/16 v1, 0x5e

    iput-char v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->carat:C

    .line 521
    new-array v1, v5, [I

    iput-object v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_la1:[I

    .line 529
    const/4 v1, 0x1

    new-array v1, v1, [Lorg/apache/lucene/queryparser/surround/parser/QueryParser$JJCalls;

    iput-object v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_2_rtns:[Lorg/apache/lucene/queryparser/surround/parser/QueryParser$JJCalls;

    .line 530
    iput-boolean v4, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_rescan:Z

    .line 531
    iput v4, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_gc:I

    .line 598
    new-instance v1, Lorg/apache/lucene/queryparser/surround/parser/QueryParser$LookaheadSuccess;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser$LookaheadSuccess;-><init>(Lorg/apache/lucene/queryparser/surround/parser/QueryParser$LookaheadSuccess;)V

    iput-object v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_ls:Lorg/apache/lucene/queryparser/surround/parser/QueryParser$LookaheadSuccess;

    .line 647
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_expentries:Ljava/util/List;

    .line 649
    iput v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_kind:I

    .line 650
    const/16 v1, 0x64

    new-array v1, v1, [I

    iput-object v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_lasttokens:[I

    .line 555
    iput-object p1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->token_source:Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;

    .line 556
    new-instance v1, Lorg/apache/lucene/queryparser/surround/parser/Token;

    invoke-direct {v1}, Lorg/apache/lucene/queryparser/surround/parser/Token;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->token:Lorg/apache/lucene/queryparser/surround/parser/Token;

    .line 557
    iput v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_ntk:I

    .line 558
    iput v4, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_gen:I

    .line 559
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v5, :cond_0

    .line 560
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_2_rtns:[Lorg/apache/lucene/queryparser/surround/parser/QueryParser$JJCalls;

    array-length v1, v1

    if-lt v0, v1, :cond_1

    .line 561
    return-void

    .line 559
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_la1:[I

    aput v3, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 560
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_2_rtns:[Lorg/apache/lucene/queryparser/surround/parser/QueryParser$JJCalls;

    new-instance v2, Lorg/apache/lucene/queryparser/surround/parser/QueryParser$JJCalls;

    invoke-direct {v2}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser$JJCalls;-><init>()V

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method protected static checkDistanceSubQueries(Lorg/apache/lucene/queryparser/surround/query/DistanceQuery;Ljava/lang/String;)V
    .locals 4
    .param p0, "distq"    # Lorg/apache/lucene/queryparser/surround/query/DistanceQuery;
    .param p1, "opName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/surround/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 107
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/query/DistanceQuery;->distanceSubQueryNotAllowed()Ljava/lang/String;

    move-result-object v0

    .line 108
    .local v0, "m":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 109
    new-instance v1, Lorg/apache/lucene/queryparser/surround/parser/ParseException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Operator "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/lucene/queryparser/surround/parser/ParseException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 111
    :cond_0
    return-void
.end method

.method protected static getOpDistance(Ljava/lang/String;)I
    .locals 2
    .param p0, "distanceOp"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    .line 100
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    .line 102
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method private jj_2_1(I)Z
    .locals 4
    .param p1, "xla"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 499
    iput p1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_la:I

    iget-object v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->token:Lorg/apache/lucene/queryparser/surround/parser/Token;

    iput-object v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_scanpos:Lorg/apache/lucene/queryparser/surround/parser/Token;

    iput-object v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_lastpos:Lorg/apache/lucene/queryparser/surround/parser/Token;

    .line 500
    :try_start_0
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_3_1()Z
    :try_end_0
    .catch Lorg/apache/lucene/queryparser/surround/parser/QueryParser$LookaheadSuccess; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-eqz v3, :cond_0

    move v1, v2

    .line 502
    :cond_0
    invoke-direct {p0, v2, p1}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_save(II)V

    .line 501
    :goto_0
    return v1

    :catch_0
    move-exception v0

    .line 502
    .local v0, "ls":Lorg/apache/lucene/queryparser/surround/parser/QueryParser$LookaheadSuccess;
    invoke-direct {p0, v2, p1}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_save(II)V

    goto :goto_0

    .end local v0    # "ls":Lorg/apache/lucene/queryparser/surround/parser/QueryParser$LookaheadSuccess;
    :catchall_0
    move-exception v1

    invoke-direct {p0, v2, p1}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_save(II)V

    throw v1
.end method

.method private jj_3_1()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 506
    const/16 v1, 0x16

    invoke-direct {p0, v1}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_scan_token(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 508
    :cond_0
    :goto_0
    return v0

    .line 507
    :cond_1
    const/16 v1, 0x10

    invoke-direct {p0, v1}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_scan_token(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 508
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private jj_add_error_token(II)V
    .locals 6
    .param p1, "kind"    # I
    .param p2, "pos"    # I

    .prologue
    .line 654
    const/16 v3, 0x64

    if-lt p2, v3, :cond_1

    .line 676
    :cond_0
    :goto_0
    return-void

    .line 655
    :cond_1
    iget v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_endpos:I

    add-int/lit8 v3, v3, 0x1

    if-ne p2, v3, :cond_2

    .line 656
    iget-object v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_lasttokens:[I

    iget v4, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_endpos:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_endpos:I

    aput p1, v3, v4

    goto :goto_0

    .line 657
    :cond_2
    iget v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_endpos:I

    if-eqz v3, :cond_0

    .line 658
    iget v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_endpos:I

    new-array v3, v3, [I

    iput-object v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_expentry:[I

    .line 659
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_endpos:I

    if-lt v0, v3, :cond_4

    .line 662
    iget-object v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_expentries:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<*>;"
    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_5

    .line 674
    :goto_2
    if-eqz p2, :cond_0

    iget-object v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_lasttokens:[I

    iput p2, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_endpos:I

    add-int/lit8 v4, p2, -0x1

    aput p1, v3, v4

    goto :goto_0

    .line 660
    .end local v1    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<*>;"
    :cond_4
    iget-object v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_expentry:[I

    iget-object v4, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_lasttokens:[I

    aget v4, v4, v0

    aput v4, v3, v0

    .line 659
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 663
    .restart local v1    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<*>;"
    :cond_5
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [I

    .line 664
    .local v2, "oldentry":[I
    array-length v3, v2

    iget-object v4, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_expentry:[I

    array-length v4, v4

    if-ne v3, v4, :cond_3

    .line 665
    const/4 v0, 0x0

    :goto_3
    iget-object v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_expentry:[I

    array-length v3, v3

    if-lt v0, v3, :cond_6

    .line 670
    iget-object v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_expentries:Ljava/util/List;

    iget-object v4, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_expentry:[I

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 666
    :cond_6
    aget v3, v2, v0

    iget-object v4, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_expentry:[I

    aget v4, v4, v0

    if-ne v3, v4, :cond_3

    .line 665
    add-int/lit8 v0, v0, 0x1

    goto :goto_3
.end method

.method private jj_consume_token(I)Lorg/apache/lucene/queryparser/surround/parser/Token;
    .locals 5
    .param p1, "kind"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/surround/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 575
    iget-object v2, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->token:Lorg/apache/lucene/queryparser/surround/parser/Token;

    .local v2, "oldToken":Lorg/apache/lucene/queryparser/surround/parser/Token;
    iget-object v3, v2, Lorg/apache/lucene/queryparser/surround/parser/Token;->next:Lorg/apache/lucene/queryparser/surround/parser/Token;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->token:Lorg/apache/lucene/queryparser/surround/parser/Token;

    iget-object v3, v3, Lorg/apache/lucene/queryparser/surround/parser/Token;->next:Lorg/apache/lucene/queryparser/surround/parser/Token;

    iput-object v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->token:Lorg/apache/lucene/queryparser/surround/parser/Token;

    .line 577
    :goto_0
    const/4 v3, -0x1

    iput v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_ntk:I

    .line 578
    iget-object v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->token:Lorg/apache/lucene/queryparser/surround/parser/Token;

    iget v3, v3, Lorg/apache/lucene/queryparser/surround/parser/Token;->kind:I

    if-ne v3, p1, :cond_5

    .line 579
    iget v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_gen:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_gen:I

    .line 580
    iget v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_gc:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_gc:I

    const/16 v4, 0x64

    if-le v3, v4, :cond_0

    .line 581
    const/4 v3, 0x0

    iput v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_gc:I

    .line 582
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_2_rtns:[Lorg/apache/lucene/queryparser/surround/parser/QueryParser$JJCalls;

    array-length v3, v3

    if-lt v1, v3, :cond_2

    .line 590
    .end local v1    # "i":I
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->token:Lorg/apache/lucene/queryparser/surround/parser/Token;

    return-object v3

    .line 576
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->token:Lorg/apache/lucene/queryparser/surround/parser/Token;

    iget-object v4, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->token_source:Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;

    invoke-virtual {v4}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->getNextToken()Lorg/apache/lucene/queryparser/surround/parser/Token;

    move-result-object v4

    iput-object v4, v3, Lorg/apache/lucene/queryparser/surround/parser/Token;->next:Lorg/apache/lucene/queryparser/surround/parser/Token;

    iput-object v4, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->token:Lorg/apache/lucene/queryparser/surround/parser/Token;

    goto :goto_0

    .line 583
    .restart local v1    # "i":I
    :cond_2
    iget-object v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_2_rtns:[Lorg/apache/lucene/queryparser/surround/parser/QueryParser$JJCalls;

    aget-object v0, v3, v1

    .line 584
    .local v0, "c":Lorg/apache/lucene/queryparser/surround/parser/QueryParser$JJCalls;
    :goto_2
    if-nez v0, :cond_3

    .line 582
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 585
    :cond_3
    iget v3, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser$JJCalls;->gen:I

    iget v4, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_gen:I

    if-ge v3, v4, :cond_4

    const/4 v3, 0x0

    iput-object v3, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser$JJCalls;->first:Lorg/apache/lucene/queryparser/surround/parser/Token;

    .line 586
    :cond_4
    iget-object v0, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser$JJCalls;->next:Lorg/apache/lucene/queryparser/surround/parser/QueryParser$JJCalls;

    goto :goto_2

    .line 592
    .end local v0    # "c":Lorg/apache/lucene/queryparser/surround/parser/QueryParser$JJCalls;
    .end local v1    # "i":I
    :cond_5
    iput-object v2, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->token:Lorg/apache/lucene/queryparser/surround/parser/Token;

    .line 593
    iput p1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_kind:I

    .line 594
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->generateParseException()Lorg/apache/lucene/queryparser/surround/parser/ParseException;

    move-result-object v3

    throw v3
.end method

.method private static jj_la1_init_0()V
    .locals 1

    .prologue
    .line 527
    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_la1_0:[I

    .line 528
    return-void

    .line 527
    :array_0
    .array-data 4
        0x100
        0x200
        0x400
        0x1000
        0x800
        0x7c3b00
        0x1b00
        0x8000
        0x7c0000
        0x20000
    .end array-data
.end method

.method private jj_ntk()I
    .locals 2

    .prologue
    .line 641
    iget-object v0, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->token:Lorg/apache/lucene/queryparser/surround/parser/Token;

    iget-object v0, v0, Lorg/apache/lucene/queryparser/surround/parser/Token;->next:Lorg/apache/lucene/queryparser/surround/parser/Token;

    iput-object v0, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_nt:Lorg/apache/lucene/queryparser/surround/parser/Token;

    if-nez v0, :cond_0

    .line 642
    iget-object v0, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->token:Lorg/apache/lucene/queryparser/surround/parser/Token;

    iget-object v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->token_source:Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;

    invoke-virtual {v1}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->getNextToken()Lorg/apache/lucene/queryparser/surround/parser/Token;

    move-result-object v1

    iput-object v1, v0, Lorg/apache/lucene/queryparser/surround/parser/Token;->next:Lorg/apache/lucene/queryparser/surround/parser/Token;

    iget v0, v1, Lorg/apache/lucene/queryparser/surround/parser/Token;->kind:I

    iput v0, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_ntk:I

    .line 644
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_nt:Lorg/apache/lucene/queryparser/surround/parser/Token;

    iget v0, v0, Lorg/apache/lucene/queryparser/surround/parser/Token;->kind:I

    iput v0, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_ntk:I

    goto :goto_0
.end method

.method private jj_rescan_token()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 721
    iput-boolean v4, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_rescan:Z

    .line 722
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v4, :cond_0

    .line 736
    const/4 v2, 0x0

    iput-boolean v2, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_rescan:Z

    .line 737
    return-void

    .line 724
    :cond_0
    :try_start_0
    iget-object v2, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_2_rtns:[Lorg/apache/lucene/queryparser/surround/parser/QueryParser$JJCalls;

    aget-object v1, v2, v0

    .line 726
    .local v1, "p":Lorg/apache/lucene/queryparser/surround/parser/QueryParser$JJCalls;
    :cond_1
    iget v2, v1, Lorg/apache/lucene/queryparser/surround/parser/QueryParser$JJCalls;->gen:I

    iget v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_gen:I

    if-le v2, v3, :cond_2

    .line 727
    iget v2, v1, Lorg/apache/lucene/queryparser/surround/parser/QueryParser$JJCalls;->arg:I

    iput v2, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_la:I

    iget-object v2, v1, Lorg/apache/lucene/queryparser/surround/parser/QueryParser$JJCalls;->first:Lorg/apache/lucene/queryparser/surround/parser/Token;

    iput-object v2, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_scanpos:Lorg/apache/lucene/queryparser/surround/parser/Token;

    iput-object v2, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_lastpos:Lorg/apache/lucene/queryparser/surround/parser/Token;

    .line 728
    packed-switch v0, :pswitch_data_0

    .line 732
    :cond_2
    :goto_1
    iget-object v1, v1, Lorg/apache/lucene/queryparser/surround/parser/QueryParser$JJCalls;->next:Lorg/apache/lucene/queryparser/surround/parser/QueryParser$JJCalls;

    .line 733
    if-nez v1, :cond_1

    .line 722
    .end local v1    # "p":Lorg/apache/lucene/queryparser/surround/parser/QueryParser$JJCalls;
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 729
    .restart local v1    # "p":Lorg/apache/lucene/queryparser/surround/parser/QueryParser$JJCalls;
    :pswitch_0
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_3_1()Z
    :try_end_0
    .catch Lorg/apache/lucene/queryparser/surround/parser/QueryParser$LookaheadSuccess; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 734
    .end local v1    # "p":Lorg/apache/lucene/queryparser/surround/parser/QueryParser$JJCalls;
    :catch_0
    move-exception v2

    goto :goto_2

    .line 728
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method private jj_save(II)V
    .locals 4
    .param p1, "index"    # I
    .param p2, "xla"    # I

    .prologue
    .line 740
    iget-object v2, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_2_rtns:[Lorg/apache/lucene/queryparser/surround/parser/QueryParser$JJCalls;

    aget-object v0, v2, p1

    .line 741
    .local v0, "p":Lorg/apache/lucene/queryparser/surround/parser/QueryParser$JJCalls;
    :goto_0
    iget v2, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser$JJCalls;->gen:I

    iget v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_gen:I

    if-gt v2, v3, :cond_0

    .line 745
    :goto_1
    iget v2, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_gen:I

    add-int/2addr v2, p2

    iget v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_la:I

    sub-int/2addr v2, v3

    iput v2, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser$JJCalls;->gen:I

    iget-object v2, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->token:Lorg/apache/lucene/queryparser/surround/parser/Token;

    iput-object v2, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser$JJCalls;->first:Lorg/apache/lucene/queryparser/surround/parser/Token;

    iput p2, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser$JJCalls;->arg:I

    .line 746
    return-void

    .line 742
    :cond_0
    iget-object v2, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser$JJCalls;->next:Lorg/apache/lucene/queryparser/surround/parser/QueryParser$JJCalls;

    if-nez v2, :cond_1

    new-instance v1, Lorg/apache/lucene/queryparser/surround/parser/QueryParser$JJCalls;

    invoke-direct {v1}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser$JJCalls;-><init>()V

    iput-object v1, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser$JJCalls;->next:Lorg/apache/lucene/queryparser/surround/parser/QueryParser$JJCalls;

    .end local v0    # "p":Lorg/apache/lucene/queryparser/surround/parser/QueryParser$JJCalls;
    .local v1, "p":Lorg/apache/lucene/queryparser/surround/parser/QueryParser$JJCalls;
    move-object v0, v1

    .end local v1    # "p":Lorg/apache/lucene/queryparser/surround/parser/QueryParser$JJCalls;
    .restart local v0    # "p":Lorg/apache/lucene/queryparser/surround/parser/QueryParser$JJCalls;
    goto :goto_1

    .line 743
    :cond_1
    iget-object v0, v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser$JJCalls;->next:Lorg/apache/lucene/queryparser/surround/parser/QueryParser$JJCalls;

    goto :goto_0
.end method

.method private jj_scan_token(I)Z
    .locals 4
    .param p1, "kind"    # I

    .prologue
    .line 600
    iget-object v2, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_scanpos:Lorg/apache/lucene/queryparser/surround/parser/Token;

    iget-object v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_lastpos:Lorg/apache/lucene/queryparser/surround/parser/Token;

    if-ne v2, v3, :cond_3

    .line 601
    iget v2, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_la:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_la:I

    .line 602
    iget-object v2, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_scanpos:Lorg/apache/lucene/queryparser/surround/parser/Token;

    iget-object v2, v2, Lorg/apache/lucene/queryparser/surround/parser/Token;->next:Lorg/apache/lucene/queryparser/surround/parser/Token;

    if-nez v2, :cond_2

    .line 603
    iget-object v2, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_scanpos:Lorg/apache/lucene/queryparser/surround/parser/Token;

    iget-object v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->token_source:Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;

    invoke-virtual {v3}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->getNextToken()Lorg/apache/lucene/queryparser/surround/parser/Token;

    move-result-object v3

    iput-object v3, v2, Lorg/apache/lucene/queryparser/surround/parser/Token;->next:Lorg/apache/lucene/queryparser/surround/parser/Token;

    iput-object v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_scanpos:Lorg/apache/lucene/queryparser/surround/parser/Token;

    iput-object v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_lastpos:Lorg/apache/lucene/queryparser/surround/parser/Token;

    .line 610
    :goto_0
    iget-boolean v2, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_rescan:Z

    if-eqz v2, :cond_1

    .line 611
    const/4 v0, 0x0

    .local v0, "i":I
    iget-object v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->token:Lorg/apache/lucene/queryparser/surround/parser/Token;

    .line 612
    .local v1, "tok":Lorg/apache/lucene/queryparser/surround/parser/Token;
    :goto_1
    if-eqz v1, :cond_0

    iget-object v2, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_scanpos:Lorg/apache/lucene/queryparser/surround/parser/Token;

    if-ne v1, v2, :cond_4

    .line 613
    :cond_0
    if-eqz v1, :cond_1

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_add_error_token(II)V

    .line 615
    .end local v0    # "i":I
    .end local v1    # "tok":Lorg/apache/lucene/queryparser/surround/parser/Token;
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_scanpos:Lorg/apache/lucene/queryparser/surround/parser/Token;

    iget v2, v2, Lorg/apache/lucene/queryparser/surround/parser/Token;->kind:I

    if-eq v2, p1, :cond_5

    const/4 v2, 0x1

    .line 617
    :goto_2
    return v2

    .line 605
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_scanpos:Lorg/apache/lucene/queryparser/surround/parser/Token;

    iget-object v2, v2, Lorg/apache/lucene/queryparser/surround/parser/Token;->next:Lorg/apache/lucene/queryparser/surround/parser/Token;

    iput-object v2, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_scanpos:Lorg/apache/lucene/queryparser/surround/parser/Token;

    iput-object v2, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_lastpos:Lorg/apache/lucene/queryparser/surround/parser/Token;

    goto :goto_0

    .line 608
    :cond_3
    iget-object v2, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_scanpos:Lorg/apache/lucene/queryparser/surround/parser/Token;

    iget-object v2, v2, Lorg/apache/lucene/queryparser/surround/parser/Token;->next:Lorg/apache/lucene/queryparser/surround/parser/Token;

    iput-object v2, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_scanpos:Lorg/apache/lucene/queryparser/surround/parser/Token;

    goto :goto_0

    .line 612
    .restart local v0    # "i":I
    .restart local v1    # "tok":Lorg/apache/lucene/queryparser/surround/parser/Token;
    :cond_4
    add-int/lit8 v0, v0, 0x1

    iget-object v1, v1, Lorg/apache/lucene/queryparser/surround/parser/Token;->next:Lorg/apache/lucene/queryparser/surround/parser/Token;

    goto :goto_1

    .line 616
    .end local v0    # "i":I
    .end local v1    # "tok":Lorg/apache/lucene/queryparser/surround/parser/Token;
    :cond_5
    iget v2, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_la:I

    if-nez v2, :cond_6

    iget-object v2, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_scanpos:Lorg/apache/lucene/queryparser/surround/parser/Token;

    iget-object v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_lastpos:Lorg/apache/lucene/queryparser/surround/parser/Token;

    if-ne v2, v3, :cond_6

    iget-object v2, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_ls:Lorg/apache/lucene/queryparser/surround/parser/QueryParser$LookaheadSuccess;

    throw v2

    .line 617
    :cond_6
    const/4 v2, 0x0

    goto :goto_2
.end method

.method public static parse(Ljava/lang/String;)Lorg/apache/lucene/queryparser/surround/query/SrndQuery;
    .locals 2
    .param p0, "query"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/surround/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 61
    new-instance v0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;

    invoke-direct {v0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;-><init>()V

    .line 62
    .local v0, "parser":Lorg/apache/lucene/queryparser/surround/parser/QueryParser;
    invoke-virtual {v0, p0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->parse2(Ljava/lang/String;)Lorg/apache/lucene/queryparser/surround/query/SrndQuery;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public final AndQuery()Lorg/apache/lucene/queryparser/surround/query/SrndQuery;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/surround/parser/ParseException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 226
    const/4 v2, 0x0

    .line 227
    .local v2, "queries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/queryparser/surround/query/SrndQuery;>;"
    const/4 v0, 0x0

    .line 228
    .local v0, "oprt":Lorg/apache/lucene/queryparser/surround/parser/Token;
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->NotQuery()Lorg/apache/lucene/queryparser/surround/query/SrndQuery;

    move-result-object v1

    .line 231
    .local v1, "q":Lorg/apache/lucene/queryparser/surround/query/SrndQuery;
    :goto_0
    iget v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_ntk:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_ntk()I

    move-result v3

    :goto_1
    packed-switch v3, :pswitch_data_0

    .line 236
    iget-object v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_la1:[I

    iget v4, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_gen:I

    aput v4, v3, v5

    .line 248
    if-nez v2, :cond_2

    .end local v1    # "q":Lorg/apache/lucene/queryparser/surround/query/SrndQuery;
    :goto_2
    return-object v1

    .line 231
    .restart local v1    # "q":Lorg/apache/lucene/queryparser/surround/query/SrndQuery;
    :cond_0
    iget v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_ntk:I

    goto :goto_1

    .line 239
    :pswitch_0
    const/16 v3, 0x9

    invoke-direct {p0, v3}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/surround/parser/Token;

    move-result-object v0

    .line 241
    if-nez v2, :cond_1

    .line 242
    new-instance v2, Ljava/util/ArrayList;

    .end local v2    # "queries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/queryparser/surround/query/SrndQuery;>;"
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 243
    .restart local v2    # "queries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/queryparser/surround/query/SrndQuery;>;"
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 245
    :cond_1
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->NotQuery()Lorg/apache/lucene/queryparser/surround/query/SrndQuery;

    move-result-object v1

    .line 246
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 248
    :cond_2
    invoke-virtual {p0, v2, v5, v0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->getAndQuery(Ljava/util/List;ZLorg/apache/lucene/queryparser/surround/parser/Token;)Lorg/apache/lucene/queryparser/surround/query/SrndQuery;

    move-result-object v1

    goto :goto_2

    .line 231
    :pswitch_data_0
    .packed-switch 0x9
        :pswitch_0
    .end packed-switch
.end method

.method public final FieldsQuery()Lorg/apache/lucene/queryparser/surround/query/SrndQuery;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/surround/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 168
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->OptionalFields()Ljava/util/ArrayList;

    move-result-object v0

    .line 169
    .local v0, "fieldNames":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->OrQuery()Lorg/apache/lucene/queryparser/surround/query/SrndQuery;

    move-result-object v1

    .line 170
    .local v1, "q":Lorg/apache/lucene/queryparser/surround/query/SrndQuery;
    if-nez v0, :cond_0

    .end local v1    # "q":Lorg/apache/lucene/queryparser/surround/query/SrndQuery;
    :goto_0
    return-object v1

    .restart local v1    # "q":Lorg/apache/lucene/queryparser/surround/query/SrndQuery;
    :cond_0
    invoke-virtual {p0, v1, v0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->getFieldsQuery(Lorg/apache/lucene/queryparser/surround/query/SrndQuery;Ljava/util/ArrayList;)Lorg/apache/lucene/queryparser/surround/query/SrndQuery;

    move-result-object v1

    goto :goto_0
.end method

.method public final FieldsQueryList()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/surround/query/SrndQuery;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/surround/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 404
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 405
    .local v1, "queries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/queryparser/surround/query/SrndQuery;>;"
    const/16 v2, 0xd

    invoke-direct {p0, v2}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/surround/parser/Token;

    .line 406
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->FieldsQuery()Lorg/apache/lucene/queryparser/surround/query/SrndQuery;

    move-result-object v0

    .line 407
    .local v0, "q":Lorg/apache/lucene/queryparser/surround/query/SrndQuery;
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 410
    :pswitch_0
    const/16 v2, 0xf

    invoke-direct {p0, v2}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/surround/parser/Token;

    .line 411
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->FieldsQuery()Lorg/apache/lucene/queryparser/surround/query/SrndQuery;

    move-result-object v0

    .line 412
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 413
    iget v2, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_ntk:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_ntk()I

    move-result v2

    :goto_0
    packed-switch v2, :pswitch_data_0

    .line 418
    iget-object v2, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_la1:[I

    const/4 v3, 0x7

    iget v4, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_gen:I

    aput v4, v2, v3

    .line 422
    const/16 v2, 0xe

    invoke-direct {p0, v2}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/surround/parser/Token;

    .line 423
    return-object v1

    .line 413
    :cond_0
    iget v2, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_ntk:I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0xf
        :pswitch_0
    .end packed-switch
.end method

.method public final NQuery()Lorg/apache/lucene/queryparser/surround/query/SrndQuery;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/surround/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 284
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->WQuery()Lorg/apache/lucene/queryparser/surround/query/SrndQuery;

    move-result-object v1

    .line 287
    .local v1, "q":Lorg/apache/lucene/queryparser/surround/query/SrndQuery;
    :goto_0
    iget v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_ntk:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_ntk()I

    move-result v3

    :goto_1
    packed-switch v3, :pswitch_data_0

    .line 292
    iget-object v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_la1:[I

    const/4 v4, 0x3

    iget v5, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_gen:I

    aput v5, v3, v4

    .line 303
    return-object v1

    .line 287
    :cond_0
    iget v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_ntk:I

    goto :goto_1

    .line 295
    :pswitch_0
    const/16 v3, 0xc

    invoke-direct {p0, v3}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/surround/parser/Token;

    move-result-object v0

    .line 296
    .local v0, "dt":Lorg/apache/lucene/queryparser/surround/parser/Token;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 297
    .local v2, "queries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/queryparser/surround/query/SrndQuery;>;"
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 299
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->WQuery()Lorg/apache/lucene/queryparser/surround/query/SrndQuery;

    move-result-object v1

    .line 300
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 301
    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {p0, v2, v3, v0, v4}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->getDistanceQuery(Ljava/util/List;ZLorg/apache/lucene/queryparser/surround/parser/Token;Z)Lorg/apache/lucene/queryparser/surround/query/SrndQuery;

    move-result-object v1

    .line 286
    goto :goto_0

    .line 287
    nop

    :pswitch_data_0
    .packed-switch 0xc
        :pswitch_0
    .end packed-switch
.end method

.method public final NotQuery()Lorg/apache/lucene/queryparser/surround/query/SrndQuery;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/surround/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 254
    const/4 v2, 0x0

    .line 255
    .local v2, "queries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/queryparser/surround/query/SrndQuery;>;"
    const/4 v0, 0x0

    .line 256
    .local v0, "oprt":Lorg/apache/lucene/queryparser/surround/parser/Token;
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->NQuery()Lorg/apache/lucene/queryparser/surround/query/SrndQuery;

    move-result-object v1

    .line 259
    .local v1, "q":Lorg/apache/lucene/queryparser/surround/query/SrndQuery;
    :goto_0
    iget v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_ntk:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_ntk()I

    move-result v3

    :goto_1
    packed-switch v3, :pswitch_data_0

    .line 264
    iget-object v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_la1:[I

    const/4 v4, 0x2

    iget v5, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_gen:I

    aput v5, v3, v4

    .line 276
    if-nez v2, :cond_2

    .end local v1    # "q":Lorg/apache/lucene/queryparser/surround/query/SrndQuery;
    :goto_2
    return-object v1

    .line 259
    .restart local v1    # "q":Lorg/apache/lucene/queryparser/surround/query/SrndQuery;
    :cond_0
    iget v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_ntk:I

    goto :goto_1

    .line 267
    :pswitch_0
    const/16 v3, 0xa

    invoke-direct {p0, v3}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/surround/parser/Token;

    move-result-object v0

    .line 269
    if-nez v2, :cond_1

    .line 270
    new-instance v2, Ljava/util/ArrayList;

    .end local v2    # "queries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/queryparser/surround/query/SrndQuery;>;"
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 271
    .restart local v2    # "queries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/queryparser/surround/query/SrndQuery;>;"
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 273
    :cond_1
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->NQuery()Lorg/apache/lucene/queryparser/surround/query/SrndQuery;

    move-result-object v1

    .line 274
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 276
    :cond_2
    invoke-virtual {p0, v2, v0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->getNotQuery(Ljava/util/List;Lorg/apache/lucene/queryparser/surround/parser/Token;)Lorg/apache/lucene/queryparser/surround/query/SrndQuery;

    move-result-object v1

    goto :goto_2

    .line 259
    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
    .end packed-switch
.end method

.method public final OptionalFields()Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/surround/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 176
    const/4 v1, 0x0

    .line 179
    .local v1, "fieldNames":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :goto_0
    const/4 v2, 0x2

    invoke-direct {p0, v2}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_2_1(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 185
    const/16 v2, 0x16

    invoke-direct {p0, v2}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/surround/parser/Token;

    move-result-object v0

    .line 186
    .local v0, "fieldName":Lorg/apache/lucene/queryparser/surround/parser/Token;
    const/16 v2, 0x10

    invoke-direct {p0, v2}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/surround/parser/Token;

    .line 187
    if-nez v1, :cond_0

    .line 188
    new-instance v1, Ljava/util/ArrayList;

    .end local v1    # "fieldNames":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 190
    .restart local v1    # "fieldNames":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_0
    iget-object v2, v0, Lorg/apache/lucene/queryparser/surround/parser/Token;->image:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 192
    .end local v0    # "fieldName":Lorg/apache/lucene/queryparser/surround/parser/Token;
    :cond_1
    return-object v1
.end method

.method public final OptionalWeights(Lorg/apache/lucene/queryparser/surround/query/SrndQuery;)V
    .locals 8
    .param p1, "q"    # Lorg/apache/lucene/queryparser/surround/query/SrndQuery;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/surround/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 471
    const/4 v2, 0x0

    .line 474
    .local v2, "weight":Lorg/apache/lucene/queryparser/surround/parser/Token;
    :goto_0
    iget v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_ntk:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_ntk()I

    move-result v3

    :goto_1
    packed-switch v3, :pswitch_data_0

    .line 479
    iget-object v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_la1:[I

    const/16 v4, 0x9

    iget v5, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_gen:I

    aput v5, v3, v4

    .line 496
    return-void

    .line 474
    :cond_0
    iget v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_ntk:I

    goto :goto_1

    .line 482
    :pswitch_0
    const/16 v3, 0x11

    invoke-direct {p0, v3}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/surround/parser/Token;

    .line 483
    const/16 v3, 0x17

    invoke-direct {p0, v3}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/surround/parser/Token;

    move-result-object v2

    .line 486
    :try_start_0
    iget-object v3, v2, Lorg/apache/lucene/queryparser/surround/parser/Token;->image:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 490
    .local v0, "f":F
    float-to-double v4, v0

    const-wide/16 v6, 0x0

    cmpg-double v3, v4, v6

    if-gtz v3, :cond_1

    .line 491
    new-instance v3, Lorg/apache/lucene/queryparser/surround/parser/ParseException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Cannot handle boost value: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v2, Lorg/apache/lucene/queryparser/surround/parser/Token;->image:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/apache/lucene/queryparser/surround/parser/ParseException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 487
    .end local v0    # "f":F
    :catch_0
    move-exception v1

    .line 488
    .local v1, "floatExc":Ljava/lang/Exception;
    new-instance v3, Lorg/apache/lucene/queryparser/surround/parser/ParseException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Cannot handle boost value: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v2, Lorg/apache/lucene/queryparser/surround/parser/Token;->image:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/apache/lucene/queryparser/surround/parser/ParseException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 493
    .end local v1    # "floatExc":Ljava/lang/Exception;
    .restart local v0    # "f":F
    :cond_1
    invoke-virtual {p1}, Lorg/apache/lucene/queryparser/surround/query/SrndQuery;->getWeight()F

    move-result v3

    mul-float/2addr v3, v0

    invoke-virtual {p1, v3}, Lorg/apache/lucene/queryparser/surround/query/SrndQuery;->setWeight(F)V

    goto :goto_0

    .line 474
    nop

    :pswitch_data_0
    .packed-switch 0x11
        :pswitch_0
    .end packed-switch
.end method

.method public final OrQuery()Lorg/apache/lucene/queryparser/surround/query/SrndQuery;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/surround/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 198
    const/4 v2, 0x0

    .line 199
    .local v2, "queries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/queryparser/surround/query/SrndQuery;>;"
    const/4 v0, 0x0

    .line 200
    .local v0, "oprt":Lorg/apache/lucene/queryparser/surround/parser/Token;
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->AndQuery()Lorg/apache/lucene/queryparser/surround/query/SrndQuery;

    move-result-object v1

    .line 203
    .local v1, "q":Lorg/apache/lucene/queryparser/surround/query/SrndQuery;
    :goto_0
    iget v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_ntk:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_ntk()I

    move-result v3

    :goto_1
    packed-switch v3, :pswitch_data_0

    .line 208
    iget-object v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_la1:[I

    const/4 v4, 0x0

    iget v5, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_gen:I

    aput v5, v3, v4

    .line 220
    if-nez v2, :cond_2

    .end local v1    # "q":Lorg/apache/lucene/queryparser/surround/query/SrndQuery;
    :goto_2
    return-object v1

    .line 203
    .restart local v1    # "q":Lorg/apache/lucene/queryparser/surround/query/SrndQuery;
    :cond_0
    iget v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_ntk:I

    goto :goto_1

    .line 211
    :pswitch_0
    const/16 v3, 0x8

    invoke-direct {p0, v3}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/surround/parser/Token;

    move-result-object v0

    .line 213
    if-nez v2, :cond_1

    .line 214
    new-instance v2, Ljava/util/ArrayList;

    .end local v2    # "queries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/queryparser/surround/query/SrndQuery;>;"
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 215
    .restart local v2    # "queries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/queryparser/surround/query/SrndQuery;>;"
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 217
    :cond_1
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->AndQuery()Lorg/apache/lucene/queryparser/surround/query/SrndQuery;

    move-result-object v1

    .line 218
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 220
    :cond_2
    const/4 v3, 0x1

    invoke-virtual {p0, v2, v3, v0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->getOrQuery(Ljava/util/List;ZLorg/apache/lucene/queryparser/surround/parser/Token;)Lorg/apache/lucene/queryparser/surround/query/SrndQuery;

    move-result-object v1

    goto :goto_2

    .line 203
    nop

    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_0
    .end packed-switch
.end method

.method public final PrefixOperatorQuery()Lorg/apache/lucene/queryparser/surround/query/SrndQuery;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/surround/parser/ParseException;
        }
    .end annotation

    .prologue
    const/4 v5, -0x1

    const/4 v3, 0x0

    .line 369
    iget v2, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_ntk:I

    if-ne v2, v5, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_ntk()I

    move-result v2

    :goto_0
    packed-switch v2, :pswitch_data_0

    .line 395
    :pswitch_0
    iget-object v2, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_la1:[I

    const/4 v3, 0x6

    iget v4, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_gen:I

    aput v4, v2, v3

    .line 396
    invoke-direct {p0, v5}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/surround/parser/Token;

    .line 397
    new-instance v2, Lorg/apache/lucene/queryparser/surround/parser/ParseException;

    invoke-direct {v2}, Lorg/apache/lucene/queryparser/surround/parser/ParseException;-><init>()V

    throw v2

    .line 369
    :cond_0
    iget v2, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_ntk:I

    goto :goto_0

    .line 371
    :pswitch_1
    const/16 v2, 0x8

    invoke-direct {p0, v2}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/surround/parser/Token;

    move-result-object v0

    .line 373
    .local v0, "oprt":Lorg/apache/lucene/queryparser/surround/parser/Token;
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->FieldsQueryList()Ljava/util/List;

    move-result-object v1

    .line 374
    .local v1, "queries":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/surround/query/SrndQuery;>;"
    invoke-virtual {p0, v1, v3, v0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->getOrQuery(Ljava/util/List;ZLorg/apache/lucene/queryparser/surround/parser/Token;)Lorg/apache/lucene/queryparser/surround/query/SrndQuery;

    move-result-object v2

    .line 392
    :goto_1
    return-object v2

    .line 377
    .end local v0    # "oprt":Lorg/apache/lucene/queryparser/surround/parser/Token;
    .end local v1    # "queries":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/surround/query/SrndQuery;>;"
    :pswitch_2
    const/16 v2, 0x9

    invoke-direct {p0, v2}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/surround/parser/Token;

    move-result-object v0

    .line 379
    .restart local v0    # "oprt":Lorg/apache/lucene/queryparser/surround/parser/Token;
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->FieldsQueryList()Ljava/util/List;

    move-result-object v1

    .line 380
    .restart local v1    # "queries":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/surround/query/SrndQuery;>;"
    invoke-virtual {p0, v1, v3, v0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->getAndQuery(Ljava/util/List;ZLorg/apache/lucene/queryparser/surround/parser/Token;)Lorg/apache/lucene/queryparser/surround/query/SrndQuery;

    move-result-object v2

    goto :goto_1

    .line 383
    .end local v0    # "oprt":Lorg/apache/lucene/queryparser/surround/parser/Token;
    .end local v1    # "queries":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/surround/query/SrndQuery;>;"
    :pswitch_3
    const/16 v2, 0xc

    invoke-direct {p0, v2}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/surround/parser/Token;

    move-result-object v0

    .line 385
    .restart local v0    # "oprt":Lorg/apache/lucene/queryparser/surround/parser/Token;
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->FieldsQueryList()Ljava/util/List;

    move-result-object v1

    .line 386
    .restart local v1    # "queries":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/surround/query/SrndQuery;>;"
    invoke-virtual {p0, v1, v3, v0, v3}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->getDistanceQuery(Ljava/util/List;ZLorg/apache/lucene/queryparser/surround/parser/Token;Z)Lorg/apache/lucene/queryparser/surround/query/SrndQuery;

    move-result-object v2

    goto :goto_1

    .line 389
    .end local v0    # "oprt":Lorg/apache/lucene/queryparser/surround/parser/Token;
    .end local v1    # "queries":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/surround/query/SrndQuery;>;"
    :pswitch_4
    const/16 v2, 0xb

    invoke-direct {p0, v2}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/surround/parser/Token;

    move-result-object v0

    .line 391
    .restart local v0    # "oprt":Lorg/apache/lucene/queryparser/surround/parser/Token;
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->FieldsQueryList()Ljava/util/List;

    move-result-object v1

    .line 392
    .restart local v1    # "queries":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/surround/query/SrndQuery;>;"
    const/4 v2, 0x1

    invoke-virtual {p0, v1, v3, v0, v2}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->getDistanceQuery(Ljava/util/List;ZLorg/apache/lucene/queryparser/surround/parser/Token;Z)Lorg/apache/lucene/queryparser/surround/query/SrndQuery;

    move-result-object v2

    goto :goto_1

    .line 369
    nop

    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method public final PrimaryQuery()Lorg/apache/lucene/queryparser/surround/query/SrndQuery;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/surround/parser/ParseException;
        }
    .end annotation

    .prologue
    const/4 v4, -0x1

    .line 337
    iget v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_ntk:I

    if-ne v1, v4, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_ntk()I

    move-result v1

    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 357
    :pswitch_0
    iget-object v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_la1:[I

    const/4 v2, 0x5

    iget v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_gen:I

    aput v3, v1, v2

    .line 358
    invoke-direct {p0, v4}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/surround/parser/Token;

    .line 359
    new-instance v1, Lorg/apache/lucene/queryparser/surround/parser/ParseException;

    invoke-direct {v1}, Lorg/apache/lucene/queryparser/surround/parser/ParseException;-><init>()V

    throw v1

    .line 337
    :cond_0
    iget v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_ntk:I

    goto :goto_0

    .line 339
    :pswitch_1
    const/16 v1, 0xd

    invoke-direct {p0, v1}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/surround/parser/Token;

    .line 340
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->FieldsQuery()Lorg/apache/lucene/queryparser/surround/query/SrndQuery;

    move-result-object v0

    .line 341
    .local v0, "q":Lorg/apache/lucene/queryparser/surround/query/SrndQuery;
    const/16 v1, 0xe

    invoke-direct {p0, v1}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/surround/parser/Token;

    .line 361
    :goto_1
    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->OptionalWeights(Lorg/apache/lucene/queryparser/surround/query/SrndQuery;)V

    .line 362
    return-object v0

    .line 347
    .end local v0    # "q":Lorg/apache/lucene/queryparser/surround/query/SrndQuery;
    :pswitch_2
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->PrefixOperatorQuery()Lorg/apache/lucene/queryparser/surround/query/SrndQuery;

    move-result-object v0

    .line 348
    .restart local v0    # "q":Lorg/apache/lucene/queryparser/surround/query/SrndQuery;
    goto :goto_1

    .line 354
    .end local v0    # "q":Lorg/apache/lucene/queryparser/surround/query/SrndQuery;
    :pswitch_3
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->SimpleTerm()Lorg/apache/lucene/queryparser/surround/query/SrndQuery;

    move-result-object v0

    .line 355
    .restart local v0    # "q":Lorg/apache/lucene/queryparser/surround/query/SrndQuery;
    goto :goto_1

    .line 337
    nop

    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public ReInit(Lorg/apache/lucene/queryparser/surround/parser/CharStream;)V
    .locals 3
    .param p1, "stream"    # Lorg/apache/lucene/queryparser/surround/parser/CharStream;

    .prologue
    const/4 v2, -0x1

    .line 545
    iget-object v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->token_source:Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->ReInit(Lorg/apache/lucene/queryparser/surround/parser/CharStream;)V

    .line 546
    new-instance v1, Lorg/apache/lucene/queryparser/surround/parser/Token;

    invoke-direct {v1}, Lorg/apache/lucene/queryparser/surround/parser/Token;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->token:Lorg/apache/lucene/queryparser/surround/parser/Token;

    .line 547
    iput v2, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_ntk:I

    .line 548
    const/4 v1, 0x0

    iput v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_gen:I

    .line 549
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v1, 0xa

    if-lt v0, v1, :cond_0

    .line 550
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_2_rtns:[Lorg/apache/lucene/queryparser/surround/parser/QueryParser$JJCalls;

    array-length v1, v1

    if-lt v0, v1, :cond_1

    .line 551
    return-void

    .line 549
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_la1:[I

    aput v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 550
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_2_rtns:[Lorg/apache/lucene/queryparser/surround/parser/QueryParser$JJCalls;

    new-instance v2, Lorg/apache/lucene/queryparser/surround/parser/QueryParser$JJCalls;

    invoke-direct {v2}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser$JJCalls;-><init>()V

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public ReInit(Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;)V
    .locals 3
    .param p1, "tm"    # Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;

    .prologue
    const/4 v2, -0x1

    .line 565
    iput-object p1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->token_source:Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;

    .line 566
    new-instance v1, Lorg/apache/lucene/queryparser/surround/parser/Token;

    invoke-direct {v1}, Lorg/apache/lucene/queryparser/surround/parser/Token;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->token:Lorg/apache/lucene/queryparser/surround/parser/Token;

    .line 567
    iput v2, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_ntk:I

    .line 568
    const/4 v1, 0x0

    iput v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_gen:I

    .line 569
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v1, 0xa

    if-lt v0, v1, :cond_0

    .line 570
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_2_rtns:[Lorg/apache/lucene/queryparser/surround/parser/QueryParser$JJCalls;

    array-length v1, v1

    if-lt v0, v1, :cond_1

    .line 571
    return-void

    .line 569
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_la1:[I

    aput v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 570
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_2_rtns:[Lorg/apache/lucene/queryparser/surround/parser/QueryParser$JJCalls;

    new-instance v2, Lorg/apache/lucene/queryparser/surround/parser/QueryParser$JJCalls;

    invoke-direct {v2}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser$JJCalls;-><init>()V

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public final SimpleTerm()Lorg/apache/lucene/queryparser/surround/query/SrndQuery;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/surround/parser/ParseException;
        }
    .end annotation

    .prologue
    const/4 v5, -0x1

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 429
    iget v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_ntk:I

    if-ne v1, v5, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_ntk()I

    move-result v1

    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 463
    iget-object v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_la1:[I

    const/16 v2, 0x8

    iget v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_gen:I

    aput v3, v1, v2

    .line 464
    invoke-direct {p0, v5}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/surround/parser/Token;

    .line 465
    new-instance v1, Lorg/apache/lucene/queryparser/surround/parser/ParseException;

    invoke-direct {v1}, Lorg/apache/lucene/queryparser/surround/parser/ParseException;-><init>()V

    throw v1

    .line 429
    :cond_0
    iget v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_ntk:I

    goto :goto_0

    .line 431
    :pswitch_0
    const/16 v1, 0x16

    invoke-direct {p0, v1}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/surround/parser/Token;

    move-result-object v0

    .line 432
    .local v0, "term":Lorg/apache/lucene/queryparser/surround/parser/Token;
    iget-object v1, v0, Lorg/apache/lucene/queryparser/surround/parser/Token;->image:Ljava/lang/String;

    invoke-virtual {p0, v1, v4}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->getTermQuery(Ljava/lang/String;Z)Lorg/apache/lucene/queryparser/surround/query/SrndQuery;

    move-result-object v1

    .line 460
    :goto_1
    return-object v1

    .line 435
    .end local v0    # "term":Lorg/apache/lucene/queryparser/surround/parser/Token;
    :pswitch_1
    const/16 v1, 0x13

    invoke-direct {p0, v1}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/surround/parser/Token;

    move-result-object v0

    .line 436
    .restart local v0    # "term":Lorg/apache/lucene/queryparser/surround/parser/Token;
    iget-object v1, v0, Lorg/apache/lucene/queryparser/surround/parser/Token;->image:Ljava/lang/String;

    iget-object v2, v0, Lorg/apache/lucene/queryparser/surround/parser/Token;->image:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v3}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->getTermQuery(Ljava/lang/String;Z)Lorg/apache/lucene/queryparser/surround/query/SrndQuery;

    move-result-object v1

    goto :goto_1

    .line 439
    .end local v0    # "term":Lorg/apache/lucene/queryparser/surround/parser/Token;
    :pswitch_2
    const/16 v1, 0x14

    invoke-direct {p0, v1}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/surround/parser/Token;

    move-result-object v0

    .line 441
    .restart local v0    # "term":Lorg/apache/lucene/queryparser/surround/parser/Token;
    iget-object v1, v0, Lorg/apache/lucene/queryparser/surround/parser/Token;->image:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->allowedSuffix(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 442
    new-instance v1, Lorg/apache/lucene/queryparser/surround/parser/ParseException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Too unrestrictive truncation: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v0, Lorg/apache/lucene/queryparser/surround/parser/Token;->image:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/lucene/queryparser/surround/parser/ParseException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 444
    :cond_1
    iget-object v1, v0, Lorg/apache/lucene/queryparser/surround/parser/Token;->image:Ljava/lang/String;

    iget-object v2, v0, Lorg/apache/lucene/queryparser/surround/parser/Token;->image:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v4}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->getPrefixQuery(Ljava/lang/String;Z)Lorg/apache/lucene/queryparser/surround/query/SrndQuery;

    move-result-object v1

    goto :goto_1

    .line 447
    .end local v0    # "term":Lorg/apache/lucene/queryparser/surround/parser/Token;
    :pswitch_3
    const/16 v1, 0x15

    invoke-direct {p0, v1}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/surround/parser/Token;

    move-result-object v0

    .line 449
    .restart local v0    # "term":Lorg/apache/lucene/queryparser/surround/parser/Token;
    iget-object v1, v0, Lorg/apache/lucene/queryparser/surround/parser/Token;->image:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->allowedTruncation(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 450
    new-instance v1, Lorg/apache/lucene/queryparser/surround/parser/ParseException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Too unrestrictive truncation: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v0, Lorg/apache/lucene/queryparser/surround/parser/Token;->image:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/lucene/queryparser/surround/parser/ParseException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 452
    :cond_2
    iget-object v1, v0, Lorg/apache/lucene/queryparser/surround/parser/Token;->image:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->getTruncQuery(Ljava/lang/String;)Lorg/apache/lucene/queryparser/surround/query/SrndQuery;

    move-result-object v1

    goto :goto_1

    .line 455
    .end local v0    # "term":Lorg/apache/lucene/queryparser/surround/parser/Token;
    :pswitch_4
    const/16 v1, 0x12

    invoke-direct {p0, v1}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/surround/parser/Token;

    move-result-object v0

    .line 457
    .restart local v0    # "term":Lorg/apache/lucene/queryparser/surround/parser/Token;
    iget-object v1, v0, Lorg/apache/lucene/queryparser/surround/parser/Token;->image:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x3

    const/4 v2, 0x3

    if-ge v1, v2, :cond_3

    .line 458
    new-instance v1, Lorg/apache/lucene/queryparser/surround/parser/ParseException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Too unrestrictive truncation: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v0, Lorg/apache/lucene/queryparser/surround/parser/Token;->image:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/lucene/queryparser/surround/parser/ParseException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 460
    :cond_3
    iget-object v1, v0, Lorg/apache/lucene/queryparser/surround/parser/Token;->image:Ljava/lang/String;

    iget-object v2, v0, Lorg/apache/lucene/queryparser/surround/parser/Token;->image:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    invoke-virtual {v1, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v3}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->getPrefixQuery(Ljava/lang/String;Z)Lorg/apache/lucene/queryparser/surround/query/SrndQuery;

    move-result-object v1

    goto/16 :goto_1

    .line 429
    :pswitch_data_0
    .packed-switch 0x12
        :pswitch_4
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method

.method public final TopSrndQuery()Lorg/apache/lucene/queryparser/surround/query/SrndQuery;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/surround/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 159
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->FieldsQuery()Lorg/apache/lucene/queryparser/surround/query/SrndQuery;

    move-result-object v0

    .line 160
    .local v0, "q":Lorg/apache/lucene/queryparser/surround/query/SrndQuery;
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/surround/parser/Token;

    .line 161
    return-object v0
.end method

.method public final WQuery()Lorg/apache/lucene/queryparser/surround/query/SrndQuery;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/surround/parser/ParseException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 311
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->PrimaryQuery()Lorg/apache/lucene/queryparser/surround/query/SrndQuery;

    move-result-object v0

    .line 314
    .local v0, "q":Lorg/apache/lucene/queryparser/surround/query/SrndQuery;
    :goto_0
    iget v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_ntk:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_ntk()I

    move-result v3

    :goto_1
    packed-switch v3, :pswitch_data_0

    .line 319
    iget-object v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_la1:[I

    const/4 v4, 0x4

    iget v5, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_gen:I

    aput v5, v3, v4

    .line 330
    return-object v0

    .line 314
    :cond_0
    iget v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_ntk:I

    goto :goto_1

    .line 322
    :pswitch_0
    const/16 v3, 0xb

    invoke-direct {p0, v3}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/surround/parser/Token;

    move-result-object v2

    .line 323
    .local v2, "wt":Lorg/apache/lucene/queryparser/surround/parser/Token;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 324
    .local v1, "queries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/queryparser/surround/query/SrndQuery;>;"
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 326
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->PrimaryQuery()Lorg/apache/lucene/queryparser/surround/query/SrndQuery;

    move-result-object v0

    .line 327
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 328
    invoke-virtual {p0, v1, v5, v2, v5}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->getDistanceQuery(Ljava/util/List;ZLorg/apache/lucene/queryparser/surround/parser/Token;Z)Lorg/apache/lucene/queryparser/surround/query/SrndQuery;

    move-result-object v0

    .line 313
    goto :goto_0

    .line 314
    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_0
    .end packed-switch
.end method

.method protected allowedSuffix(Ljava/lang/String;)Z
    .locals 2
    .param p1, "suffixed"    # Ljava/lang/String;

    .prologue
    .line 133
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    const/4 v1, 0x3

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected allowedTruncation(Ljava/lang/String;)Z
    .locals 4
    .param p1, "truncated"    # Ljava/lang/String;

    .prologue
    .line 143
    const/4 v2, 0x0

    .line 144
    .local v2, "nrNormalChars":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-lt v1, v3, :cond_0

    .line 150
    const/4 v3, 0x3

    if-lt v2, v3, :cond_2

    const/4 v3, 0x1

    :goto_1
    return v3

    .line 145
    :cond_0
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 146
    .local v0, "c":C
    const/16 v3, 0x2a

    if-eq v0, v3, :cond_1

    const/16 v3, 0x3f

    if-eq v0, v3, :cond_1

    .line 147
    add-int/lit8 v2, v2, 0x1

    .line 144
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 150
    .end local v0    # "c":C
    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public final disable_tracing()V
    .locals 0

    .prologue
    .line 718
    return-void
.end method

.method public final enable_tracing()V
    .locals 0

    .prologue
    .line 714
    return-void
.end method

.method public generateParseException()Lorg/apache/lucene/queryparser/surround/parser/ParseException;
    .locals 9

    .prologue
    const/16 v8, 0x18

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 680
    iget-object v4, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_expentries:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 681
    new-array v3, v8, [Z

    .line 682
    .local v3, "la1tokens":[Z
    iget v4, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_kind:I

    if-ltz v4, :cond_0

    .line 683
    iget v4, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_kind:I

    aput-boolean v7, v3, v4

    .line 684
    const/4 v4, -0x1

    iput v4, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_kind:I

    .line 686
    :cond_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/16 v4, 0xa

    if-lt v1, v4, :cond_1

    .line 695
    const/4 v1, 0x0

    :goto_1
    if-lt v1, v8, :cond_5

    .line 702
    iput v6, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_endpos:I

    .line 703
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_rescan_token()V

    .line 704
    invoke-direct {p0, v6, v6}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_add_error_token(II)V

    .line 705
    iget-object v4, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_expentries:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    new-array v0, v4, [[I

    .line 706
    .local v0, "exptokseq":[[I
    const/4 v1, 0x0

    :goto_2
    iget-object v4, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_expentries:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lt v1, v4, :cond_7

    .line 709
    new-instance v4, Lorg/apache/lucene/queryparser/surround/parser/ParseException;

    iget-object v5, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->token:Lorg/apache/lucene/queryparser/surround/parser/Token;

    sget-object v6, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->tokenImage:[Ljava/lang/String;

    invoke-direct {v4, v5, v0, v6}, Lorg/apache/lucene/queryparser/surround/parser/ParseException;-><init>(Lorg/apache/lucene/queryparser/surround/parser/Token;[[I[Ljava/lang/String;)V

    return-object v4

    .line 687
    .end local v0    # "exptokseq":[[I
    :cond_1
    iget-object v4, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_la1:[I

    aget v4, v4, v1

    iget v5, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_gen:I

    if-ne v4, v5, :cond_2

    .line 688
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_3
    const/16 v4, 0x20

    if-lt v2, v4, :cond_3

    .line 686
    .end local v2    # "j":I
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 689
    .restart local v2    # "j":I
    :cond_3
    sget-object v4, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_la1_0:[I

    aget v4, v4, v1

    shl-int v5, v7, v2

    and-int/2addr v4, v5

    if-eqz v4, :cond_4

    .line 690
    aput-boolean v7, v3, v2

    .line 688
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 696
    .end local v2    # "j":I
    :cond_5
    aget-boolean v4, v3, v1

    if-eqz v4, :cond_6

    .line 697
    new-array v4, v7, [I

    iput-object v4, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_expentry:[I

    .line 698
    iget-object v4, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_expentry:[I

    aput v1, v4, v6

    .line 699
    iget-object v4, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_expentries:Ljava/util/List;

    iget-object v5, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_expentry:[I

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 695
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 707
    .restart local v0    # "exptokseq":[[I
    :cond_7
    iget-object v4, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_expentries:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [I

    aput-object v4, v0, v1

    .line 706
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method

.method protected getAndQuery(Ljava/util/List;ZLorg/apache/lucene/queryparser/surround/parser/Token;)Lorg/apache/lucene/queryparser/surround/query/SrndQuery;
    .locals 2
    .param p2, "infix"    # Z
    .param p3, "andToken"    # Lorg/apache/lucene/queryparser/surround/parser/Token;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/surround/query/SrndQuery;",
            ">;Z",
            "Lorg/apache/lucene/queryparser/surround/parser/Token;",
            ")",
            "Lorg/apache/lucene/queryparser/surround/query/SrndQuery;"
        }
    .end annotation

    .prologue
    .line 91
    .local p1, "queries":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/surround/query/SrndQuery;>;"
    new-instance v0, Lorg/apache/lucene/queryparser/surround/query/AndQuery;

    iget-object v1, p3, Lorg/apache/lucene/queryparser/surround/parser/Token;->image:Ljava/lang/String;

    invoke-direct {v0, p1, p2, v1}, Lorg/apache/lucene/queryparser/surround/query/AndQuery;-><init>(Ljava/util/List;ZLjava/lang/String;)V

    return-object v0
.end method

.method protected getDistanceQuery(Ljava/util/List;ZLorg/apache/lucene/queryparser/surround/parser/Token;Z)Lorg/apache/lucene/queryparser/surround/query/SrndQuery;
    .locals 6
    .param p2, "infix"    # Z
    .param p3, "dToken"    # Lorg/apache/lucene/queryparser/surround/parser/Token;
    .param p4, "ordered"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/surround/query/SrndQuery;",
            ">;Z",
            "Lorg/apache/lucene/queryparser/surround/parser/Token;",
            "Z)",
            "Lorg/apache/lucene/queryparser/surround/query/SrndQuery;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/surround/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 118
    .local p1, "queries":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/surround/query/SrndQuery;>;"
    new-instance v0, Lorg/apache/lucene/queryparser/surround/query/DistanceQuery;

    .line 120
    iget-object v1, p3, Lorg/apache/lucene/queryparser/surround/parser/Token;->image:Ljava/lang/String;

    invoke-static {v1}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->getOpDistance(Ljava/lang/String;)I

    move-result v3

    .line 121
    iget-object v4, p3, Lorg/apache/lucene/queryparser/surround/parser/Token;->image:Ljava/lang/String;

    move-object v1, p1

    move v2, p2

    move v5, p4

    .line 118
    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/queryparser/surround/query/DistanceQuery;-><init>(Ljava/util/List;ZILjava/lang/String;Z)V

    .line 123
    .local v0, "dq":Lorg/apache/lucene/queryparser/surround/query/DistanceQuery;
    iget-object v1, p3, Lorg/apache/lucene/queryparser/surround/parser/Token;->image:Ljava/lang/String;

    invoke-static {v0, v1}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->checkDistanceSubQueries(Lorg/apache/lucene/queryparser/surround/query/DistanceQuery;Ljava/lang/String;)V

    .line 124
    return-object v0
.end method

.method protected getFieldsQuery(Lorg/apache/lucene/queryparser/surround/query/SrndQuery;Ljava/util/ArrayList;)Lorg/apache/lucene/queryparser/surround/query/SrndQuery;
    .locals 2
    .param p1, "q"    # Lorg/apache/lucene/queryparser/surround/query/SrndQuery;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/queryparser/surround/query/SrndQuery;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lorg/apache/lucene/queryparser/surround/query/SrndQuery;"
        }
    .end annotation

    .prologue
    .line 83
    .local p2, "fieldNames":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v0, Lorg/apache/lucene/queryparser/surround/query/FieldsQuery;

    const/16 v1, 0x3a

    invoke-direct {v0, p1, p2, v1}, Lorg/apache/lucene/queryparser/surround/query/FieldsQuery;-><init>(Lorg/apache/lucene/queryparser/surround/query/SrndQuery;Ljava/util/List;C)V

    return-object v0
.end method

.method public final getNextToken()Lorg/apache/lucene/queryparser/surround/parser/Token;
    .locals 2

    .prologue
    .line 623
    iget-object v0, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->token:Lorg/apache/lucene/queryparser/surround/parser/Token;

    iget-object v0, v0, Lorg/apache/lucene/queryparser/surround/parser/Token;->next:Lorg/apache/lucene/queryparser/surround/parser/Token;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->token:Lorg/apache/lucene/queryparser/surround/parser/Token;

    iget-object v0, v0, Lorg/apache/lucene/queryparser/surround/parser/Token;->next:Lorg/apache/lucene/queryparser/surround/parser/Token;

    iput-object v0, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->token:Lorg/apache/lucene/queryparser/surround/parser/Token;

    .line 625
    :goto_0
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_ntk:I

    .line 626
    iget v0, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_gen:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->jj_gen:I

    .line 627
    iget-object v0, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->token:Lorg/apache/lucene/queryparser/surround/parser/Token;

    return-object v0

    .line 624
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->token:Lorg/apache/lucene/queryparser/surround/parser/Token;

    iget-object v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->token_source:Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;

    invoke-virtual {v1}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->getNextToken()Lorg/apache/lucene/queryparser/surround/parser/Token;

    move-result-object v1

    iput-object v1, v0, Lorg/apache/lucene/queryparser/surround/parser/Token;->next:Lorg/apache/lucene/queryparser/surround/parser/Token;

    iput-object v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->token:Lorg/apache/lucene/queryparser/surround/parser/Token;

    goto :goto_0
.end method

.method protected getNotQuery(Ljava/util/List;Lorg/apache/lucene/queryparser/surround/parser/Token;)Lorg/apache/lucene/queryparser/surround/query/SrndQuery;
    .locals 2
    .param p2, "notToken"    # Lorg/apache/lucene/queryparser/surround/parser/Token;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/surround/query/SrndQuery;",
            ">;",
            "Lorg/apache/lucene/queryparser/surround/parser/Token;",
            ")",
            "Lorg/apache/lucene/queryparser/surround/query/SrndQuery;"
        }
    .end annotation

    .prologue
    .line 95
    .local p1, "queries":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/surround/query/SrndQuery;>;"
    new-instance v0, Lorg/apache/lucene/queryparser/surround/query/NotQuery;

    iget-object v1, p2, Lorg/apache/lucene/queryparser/surround/parser/Token;->image:Ljava/lang/String;

    invoke-direct {v0, p1, v1}, Lorg/apache/lucene/queryparser/surround/query/NotQuery;-><init>(Ljava/util/List;Ljava/lang/String;)V

    return-object v0
.end method

.method protected getOrQuery(Ljava/util/List;ZLorg/apache/lucene/queryparser/surround/parser/Token;)Lorg/apache/lucene/queryparser/surround/query/SrndQuery;
    .locals 2
    .param p2, "infix"    # Z
    .param p3, "orToken"    # Lorg/apache/lucene/queryparser/surround/parser/Token;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/surround/query/SrndQuery;",
            ">;Z",
            "Lorg/apache/lucene/queryparser/surround/parser/Token;",
            ")",
            "Lorg/apache/lucene/queryparser/surround/query/SrndQuery;"
        }
    .end annotation

    .prologue
    .line 87
    .local p1, "queries":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/surround/query/SrndQuery;>;"
    new-instance v0, Lorg/apache/lucene/queryparser/surround/query/OrQuery;

    iget-object v1, p3, Lorg/apache/lucene/queryparser/surround/parser/Token;->image:Ljava/lang/String;

    invoke-direct {v0, p1, p2, v1}, Lorg/apache/lucene/queryparser/surround/query/OrQuery;-><init>(Ljava/util/List;ZLjava/lang/String;)V

    return-object v0
.end method

.method protected getPrefixQuery(Ljava/lang/String;Z)Lorg/apache/lucene/queryparser/surround/query/SrndQuery;
    .locals 2
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "quoted"    # Z

    .prologue
    .line 138
    new-instance v0, Lorg/apache/lucene/queryparser/surround/query/SrndPrefixQuery;

    const/16 v1, 0x2a

    invoke-direct {v0, p1, p2, v1}, Lorg/apache/lucene/queryparser/surround/query/SrndPrefixQuery;-><init>(Ljava/lang/String;ZC)V

    return-object v0
.end method

.method protected getTermQuery(Ljava/lang/String;Z)Lorg/apache/lucene/queryparser/surround/query/SrndQuery;
    .locals 1
    .param p1, "term"    # Ljava/lang/String;
    .param p2, "quoted"    # Z

    .prologue
    .line 129
    new-instance v0, Lorg/apache/lucene/queryparser/surround/query/SrndTermQuery;

    invoke-direct {v0, p1, p2}, Lorg/apache/lucene/queryparser/surround/query/SrndTermQuery;-><init>(Ljava/lang/String;Z)V

    return-object v0
.end method

.method public final getToken(I)Lorg/apache/lucene/queryparser/surround/parser/Token;
    .locals 4
    .param p1, "index"    # I

    .prologue
    .line 632
    iget-object v1, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->token:Lorg/apache/lucene/queryparser/surround/parser/Token;

    .line 633
    .local v1, "t":Lorg/apache/lucene/queryparser/surround/parser/Token;
    const/4 v0, 0x0

    .local v0, "i":I
    move-object v2, v1

    .end local v1    # "t":Lorg/apache/lucene/queryparser/surround/parser/Token;
    .local v2, "t":Lorg/apache/lucene/queryparser/surround/parser/Token;
    :goto_0
    if-lt v0, p1, :cond_0

    .line 637
    return-object v2

    .line 634
    :cond_0
    iget-object v3, v2, Lorg/apache/lucene/queryparser/surround/parser/Token;->next:Lorg/apache/lucene/queryparser/surround/parser/Token;

    if-eqz v3, :cond_1

    iget-object v1, v2, Lorg/apache/lucene/queryparser/surround/parser/Token;->next:Lorg/apache/lucene/queryparser/surround/parser/Token;

    .line 633
    .end local v2    # "t":Lorg/apache/lucene/queryparser/surround/parser/Token;
    .restart local v1    # "t":Lorg/apache/lucene/queryparser/surround/parser/Token;
    :goto_1
    add-int/lit8 v0, v0, 0x1

    move-object v2, v1

    .end local v1    # "t":Lorg/apache/lucene/queryparser/surround/parser/Token;
    .restart local v2    # "t":Lorg/apache/lucene/queryparser/surround/parser/Token;
    goto :goto_0

    .line 635
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->token_source:Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;

    invoke-virtual {v3}, Lorg/apache/lucene/queryparser/surround/parser/QueryParserTokenManager;->getNextToken()Lorg/apache/lucene/queryparser/surround/parser/Token;

    move-result-object v1

    iput-object v1, v2, Lorg/apache/lucene/queryparser/surround/parser/Token;->next:Lorg/apache/lucene/queryparser/surround/parser/Token;

    .end local v2    # "t":Lorg/apache/lucene/queryparser/surround/parser/Token;
    .restart local v1    # "t":Lorg/apache/lucene/queryparser/surround/parser/Token;
    goto :goto_1
.end method

.method protected getTruncQuery(Ljava/lang/String;)Lorg/apache/lucene/queryparser/surround/query/SrndQuery;
    .locals 3
    .param p1, "truncated"    # Ljava/lang/String;

    .prologue
    .line 154
    new-instance v0, Lorg/apache/lucene/queryparser/surround/query/SrndTruncQuery;

    const/16 v1, 0x2a

    const/16 v2, 0x3f

    invoke-direct {v0, p1, v1, v2}, Lorg/apache/lucene/queryparser/surround/query/SrndTruncQuery;-><init>(Ljava/lang/String;CC)V

    return-object v0
.end method

.method public parse2(Ljava/lang/String;)Lorg/apache/lucene/queryparser/surround/query/SrndQuery;
    .locals 3
    .param p1, "query"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/surround/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 70
    new-instance v1, Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;

    new-instance v2, Ljava/io/StringReader;

    invoke-direct {v2, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;-><init>(Ljava/io/Reader;)V

    invoke-virtual {p0, v1}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->ReInit(Lorg/apache/lucene/queryparser/surround/parser/CharStream;)V

    .line 72
    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/parser/QueryParser;->TopSrndQuery()Lorg/apache/lucene/queryparser/surround/query/SrndQuery;
    :try_end_0
    .catch Lorg/apache/lucene/queryparser/surround/parser/TokenMgrError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 73
    :catch_0
    move-exception v0

    .line 74
    .local v0, "tme":Lorg/apache/lucene/queryparser/surround/parser/TokenMgrError;
    new-instance v1, Lorg/apache/lucene/queryparser/surround/parser/ParseException;

    invoke-virtual {v0}, Lorg/apache/lucene/queryparser/surround/parser/TokenMgrError;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/lucene/queryparser/surround/parser/ParseException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
