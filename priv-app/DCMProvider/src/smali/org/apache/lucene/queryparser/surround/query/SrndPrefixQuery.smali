.class public Lorg/apache/lucene/queryparser/surround/query/SrndPrefixQuery;
.super Lorg/apache/lucene/queryparser/surround/query/SimpleTerm;
.source "SrndPrefixQuery.java"


# instance fields
.field private final prefix:Ljava/lang/String;

.field private final prefixRef:Lorg/apache/lucene/util/BytesRef;

.field private final truncator:C


# direct methods
.method public constructor <init>(Ljava/lang/String;ZC)V
    .locals 1
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "quoted"    # Z
    .param p3, "truncator"    # C

    .prologue
    .line 36
    invoke-direct {p0, p2}, Lorg/apache/lucene/queryparser/surround/query/SimpleTerm;-><init>(Z)V

    .line 37
    iput-object p1, p0, Lorg/apache/lucene/queryparser/surround/query/SrndPrefixQuery;->prefix:Ljava/lang/String;

    .line 38
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0, p1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    iput-object v0, p0, Lorg/apache/lucene/queryparser/surround/query/SrndPrefixQuery;->prefixRef:Lorg/apache/lucene/util/BytesRef;

    .line 39
    iput-char p3, p0, Lorg/apache/lucene/queryparser/surround/query/SrndPrefixQuery;->truncator:C

    .line 40
    return-void
.end method


# virtual methods
.method public getLucenePrefixTerm(Ljava/lang/String;)Lorg/apache/lucene/index/Term;
    .locals 2
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 49
    new-instance v0, Lorg/apache/lucene/index/Term;

    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/query/SrndPrefixQuery;->getPrefix()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public getPrefix()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lorg/apache/lucene/queryparser/surround/query/SrndPrefixQuery;->prefix:Ljava/lang/String;

    return-object v0
.end method

.method public getSuffixOperator()C
    .locals 1

    .prologue
    .line 46
    iget-char v0, p0, Lorg/apache/lucene/queryparser/surround/query/SrndPrefixQuery;->truncator:C

    return v0
.end method

.method protected suffixToString(Ljava/lang/StringBuilder;)V
    .locals 1
    .param p1, "r"    # Ljava/lang/StringBuilder;

    .prologue
    .line 56
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/query/SrndPrefixQuery;->getSuffixOperator()C

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    return-void
.end method

.method public toStringUnquoted()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/query/SrndPrefixQuery;->getPrefix()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public visitMatchingTerms(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/queryparser/surround/query/SimpleTerm$MatchingTermVisitor;)V
    .locals 7
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "fieldName"    # Ljava/lang/String;
    .param p3, "mtv"    # Lorg/apache/lucene/queryparser/surround/query/SimpleTerm$MatchingTermVisitor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 65
    invoke-static {p1, p2}, Lorg/apache/lucene/index/MultiFields;->getTerms(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v2

    .line 66
    .local v2, "terms":Lorg/apache/lucene/index/Terms;
    if-eqz v2, :cond_3

    .line 67
    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v3

    .line 69
    .local v3, "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    const/4 v0, 0x0

    .line 70
    .local v0, "skip":Z
    new-instance v5, Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/query/SrndPrefixQuery;->getPrefix()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v3, v5}, Lorg/apache/lucene/index/TermsEnum;->seekCeil(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    move-result-object v1

    .line 71
    .local v1, "status":Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    sget-object v5, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    if-ne v1, v5, :cond_0

    .line 72
    invoke-virtual {p0, p2}, Lorg/apache/lucene/queryparser/surround/query/SrndPrefixQuery;->getLucenePrefixTerm(Ljava/lang/String;)Lorg/apache/lucene/index/Term;

    move-result-object v5

    invoke-interface {p3, v5}, Lorg/apache/lucene/queryparser/surround/query/SimpleTerm$MatchingTermVisitor;->visitMatchingTerm(Lorg/apache/lucene/index/Term;)V

    .line 84
    :goto_0
    if-nez v0, :cond_3

    .line 86
    :goto_1
    invoke-virtual {v3}, Lorg/apache/lucene/index/TermsEnum;->next()Lorg/apache/lucene/util/BytesRef;

    move-result-object v4

    .line 87
    .local v4, "text":Lorg/apache/lucene/util/BytesRef;
    if-eqz v4, :cond_3

    iget-object v5, p0, Lorg/apache/lucene/queryparser/surround/query/SrndPrefixQuery;->prefixRef:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v4, v5}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 88
    new-instance v5, Lorg/apache/lucene/index/Term;

    invoke-virtual {v4}, Lorg/apache/lucene/util/BytesRef;->utf8ToString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p3, v5}, Lorg/apache/lucene/queryparser/surround/query/SimpleTerm$MatchingTermVisitor;->visitMatchingTerm(Lorg/apache/lucene/index/Term;)V

    goto :goto_1

    .line 73
    .end local v4    # "text":Lorg/apache/lucene/util/BytesRef;
    :cond_0
    sget-object v5, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->NOT_FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    if-ne v1, v5, :cond_2

    .line 74
    invoke-virtual {v3}, Lorg/apache/lucene/index/TermsEnum;->term()Lorg/apache/lucene/util/BytesRef;

    move-result-object v5

    iget-object v6, p0, Lorg/apache/lucene/queryparser/surround/query/SrndPrefixQuery;->prefixRef:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v5, v6}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 75
    new-instance v5, Lorg/apache/lucene/index/Term;

    invoke-virtual {v3}, Lorg/apache/lucene/index/TermsEnum;->term()Lorg/apache/lucene/util/BytesRef;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/lucene/util/BytesRef;->utf8ToString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p3, v5}, Lorg/apache/lucene/queryparser/surround/query/SimpleTerm$MatchingTermVisitor;->visitMatchingTerm(Lorg/apache/lucene/index/Term;)V

    goto :goto_0

    .line 77
    :cond_1
    const/4 v0, 0x1

    .line 79
    goto :goto_0

    .line 81
    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    .line 95
    .end local v0    # "skip":Z
    .end local v1    # "status":Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    .end local v3    # "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    :cond_3
    return-void
.end method
