.class public Lorg/apache/lucene/queryparser/xml/builders/TermQueryBuilder;
.super Ljava/lang/Object;
.source "TermQueryBuilder.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/xml/QueryBuilder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getQuery(Lorg/w3c/dom/Element;)Lorg/apache/lucene/search/Query;
    .locals 5
    .param p1, "e"    # Lorg/w3c/dom/Element;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/xml/ParserException;
        }
    .end annotation

    .prologue
    .line 34
    const-string v3, "fieldName"

    invoke-static {p1, v3}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttributeWithInheritanceOrFail(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 35
    .local v0, "field":Ljava/lang/String;
    invoke-static {p1}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getNonBlankTextOrFail(Lorg/w3c/dom/Element;)Ljava/lang/String;

    move-result-object v2

    .line 36
    .local v2, "value":Ljava/lang/String;
    new-instance v1, Lorg/apache/lucene/search/TermQuery;

    new-instance v3, Lorg/apache/lucene/index/Term;

    invoke-direct {v3, v0, v2}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v3}, Lorg/apache/lucene/search/TermQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    .line 37
    .local v1, "tq":Lorg/apache/lucene/search/TermQuery;
    const-string v3, "boost"

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-static {p1, v3, v4}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttribute(Lorg/w3c/dom/Element;Ljava/lang/String;F)F

    move-result v3

    invoke-virtual {v1, v3}, Lorg/apache/lucene/search/TermQuery;->setBoost(F)V

    .line 38
    return-object v1
.end method
