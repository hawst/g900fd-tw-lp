.class public Lorg/apache/lucene/queryparser/flexible/standard/builders/WildcardQueryNodeBuilder;
.super Ljava/lang/Object;
.source "WildcardQueryNodeBuilder.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/flexible/standard/builders/StandardQueryBuilder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    return-void
.end method


# virtual methods
.method public bridge synthetic build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/WildcardQueryNodeBuilder;->build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/search/WildcardQuery;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/search/Query;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/WildcardQueryNodeBuilder;->build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/search/WildcardQuery;

    move-result-object v0

    return-object v0
.end method

.method public build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/search/WildcardQuery;
    .locals 6
    .param p1, "queryNode"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 40
    move-object v2, p1

    check-cast v2, Lorg/apache/lucene/queryparser/flexible/standard/nodes/WildcardQueryNode;

    .line 42
    .local v2, "wildcardNode":Lorg/apache/lucene/queryparser/flexible/standard/nodes/WildcardQueryNode;
    new-instance v1, Lorg/apache/lucene/search/WildcardQuery;

    new-instance v3, Lorg/apache/lucene/index/Term;

    invoke-virtual {v2}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/WildcardQueryNode;->getFieldAsString()Ljava/lang/String;

    move-result-object v4

    .line 43
    invoke-virtual {v2}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/WildcardQueryNode;->getTextAsString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    invoke-direct {v1, v3}, Lorg/apache/lucene/search/WildcardQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    .line 45
    .local v1, "q":Lorg/apache/lucene/search/WildcardQuery;
    const-string v3, "MultiTermRewriteMethodConfiguration"

    invoke-interface {p1, v3}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->getTag(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    .line 46
    .local v0, "method":Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;
    if-eqz v0, :cond_0

    .line 47
    invoke-virtual {v1, v0}, Lorg/apache/lucene/search/WildcardQuery;->setRewriteMethod(Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;)V

    .line 50
    :cond_0
    return-object v1
.end method
