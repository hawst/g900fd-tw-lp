.class public Lorg/apache/lucene/queryparser/flexible/standard/builders/StandardBooleanQueryNodeBuilder;
.super Ljava/lang/Object;
.source "StandardBooleanQueryNodeBuilder.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/flexible/standard/builders/StandardQueryBuilder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    return-void
.end method

.method private static getModifierValue(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/search/BooleanClause$Occur;
    .locals 3
    .param p0, "node"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    .prologue
    .line 90
    instance-of v2, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;

    if-eqz v2, :cond_2

    move-object v0, p0

    .line 91
    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;

    .line 92
    .local v0, "mNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;
    invoke-virtual {v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;->getModifier()Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    move-result-object v1

    .line 94
    .local v1, "modifier":Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;
    sget-object v2, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;->MOD_NONE:Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    invoke-virtual {v2, v1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 95
    sget-object v2, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    .line 105
    .end local v0    # "mNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;
    .end local v1    # "modifier":Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;
    :goto_0
    return-object v2

    .line 97
    .restart local v0    # "mNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;
    .restart local v1    # "modifier":Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;
    :cond_0
    sget-object v2, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;->MOD_NOT:Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    invoke-virtual {v2, v1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 98
    sget-object v2, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST_NOT:Lorg/apache/lucene/search/BooleanClause$Occur;

    goto :goto_0

    .line 101
    :cond_1
    sget-object v2, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    goto :goto_0

    .line 105
    .end local v0    # "mNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;
    .end local v1    # "modifier":Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;
    :cond_2
    sget-object v2, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/StandardBooleanQueryNodeBuilder;->build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/search/BooleanQuery;

    move-result-object v0

    return-object v0
.end method

.method public build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/search/BooleanQuery;
    .locals 13
    .param p1, "queryNode"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 54
    move-object v1, p1

    check-cast v1, Lorg/apache/lucene/queryparser/flexible/standard/nodes/StandardBooleanQueryNode;

    .line 56
    .local v1, "booleanNode":Lorg/apache/lucene/queryparser/flexible/standard/nodes/StandardBooleanQueryNode;
    new-instance v0, Lorg/apache/lucene/search/BooleanQuery;

    invoke-virtual {v1}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/StandardBooleanQueryNode;->isDisableCoord()Z

    move-result v7

    invoke-direct {v0, v7}, Lorg/apache/lucene/search/BooleanQuery;-><init>(Z)V

    .line 57
    .local v0, "bQuery":Lorg/apache/lucene/search/BooleanQuery;
    invoke-virtual {v1}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/StandardBooleanQueryNode;->getChildren()Ljava/util/List;

    move-result-object v3

    .line 59
    .local v3, "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    if-eqz v3, :cond_1

    .line 61
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_2

    .line 84
    :cond_1
    return-object v0

    .line 61
    :cond_2
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    .line 62
    .local v2, "child":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    sget-object v8, Lorg/apache/lucene/queryparser/flexible/core/builders/QueryTreeBuilder;->QUERY_TREE_BUILDER_TAGID:Ljava/lang/String;

    invoke-interface {v2, v8}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->getTag(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    .line 64
    .local v5, "obj":Ljava/lang/Object;
    if-eqz v5, :cond_0

    move-object v6, v5

    .line 65
    check-cast v6, Lorg/apache/lucene/search/Query;

    .line 68
    .local v6, "query":Lorg/apache/lucene/search/Query;
    :try_start_0
    invoke-static {v2}, Lorg/apache/lucene/queryparser/flexible/standard/builders/StandardBooleanQueryNodeBuilder;->getModifierValue(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/search/BooleanClause$Occur;

    move-result-object v8

    invoke-virtual {v0, v6, v8}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V
    :try_end_0
    .catch Lorg/apache/lucene/search/BooleanQuery$TooManyClauses; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 69
    :catch_0
    move-exception v4

    .line 71
    .local v4, "ex":Lorg/apache/lucene/search/BooleanQuery$TooManyClauses;
    new-instance v7, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;

    new-instance v8, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;

    .line 72
    sget-object v9, Lorg/apache/lucene/queryparser/flexible/core/messages/QueryParserMessages;->TOO_MANY_BOOLEAN_CLAUSES:Ljava/lang/String;

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    .line 73
    invoke-static {}, Lorg/apache/lucene/search/BooleanQuery;->getMaxClauseCount()I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    .line 74
    new-instance v12, Lorg/apache/lucene/queryparser/flexible/standard/parser/EscapeQuerySyntaxImpl;

    invoke-direct {v12}, Lorg/apache/lucene/queryparser/flexible/standard/parser/EscapeQuerySyntaxImpl;-><init>()V

    invoke-interface {p1, v12}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->toQueryString(Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;)Ljava/lang/CharSequence;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-direct {v8, v9, v10}, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 71
    invoke-direct {v7, v8, v4}, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;-><init>(Lorg/apache/lucene/queryparser/flexible/messages/Message;Ljava/lang/Throwable;)V

    throw v7
.end method

.method public bridge synthetic build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/search/Query;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/StandardBooleanQueryNodeBuilder;->build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/search/BooleanQuery;

    move-result-object v0

    return-object v0
.end method
