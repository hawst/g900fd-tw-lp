.class public Lorg/apache/lucene/queryparser/flexible/core/nodes/PhraseSlopQueryNode;
.super Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;
.source "PhraseSlopQueryNode.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldableNode;


# instance fields
.field private value:I


# direct methods
.method public constructor <init>(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;I)V
    .locals 6
    .param p1, "query"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .param p2, "value"    # I

    .prologue
    const/4 v5, 0x0

    .line 36
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;-><init>()V

    .line 31
    iput v5, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/PhraseSlopQueryNode;->value:I

    .line 37
    if-nez p1, :cond_0

    .line 38
    new-instance v0, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeError;

    new-instance v1, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;

    .line 39
    sget-object v2, Lorg/apache/lucene/queryparser/flexible/core/messages/QueryParserMessages;->NODE_ACTION_NOT_SUPPORTED:Ljava/lang/String;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "query"

    aput-object v4, v3, v5

    const/4 v4, 0x1

    const-string v5, "null"

    aput-object v5, v3, v4

    invoke-direct {v1, v2, v3}, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 38
    invoke-direct {v0, v1}, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeError;-><init>(Lorg/apache/lucene/queryparser/flexible/messages/Message;)V

    throw v0

    .line 42
    :cond_0
    iput p2, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/PhraseSlopQueryNode;->value:I

    .line 43
    invoke-virtual {p0, v5}, Lorg/apache/lucene/queryparser/flexible/core/nodes/PhraseSlopQueryNode;->setLeaf(Z)V

    .line 44
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/PhraseSlopQueryNode;->allocate()V

    .line 45
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/PhraseSlopQueryNode;->add(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)V

    .line 46
    return-void
.end method

.method private getValueString()Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 57
    iget v1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/PhraseSlopQueryNode;->value:I

    int-to-float v1, v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    .line 58
    .local v0, "f":Ljava/lang/Float;
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {v0}, Ljava/lang/Float;->longValue()J

    move-result-wide v2

    long-to-float v2, v2

    cmpl-float v1, v1, v2

    if-nez v1, :cond_0

    .line 59
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Float;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 61
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method public cloneTree()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 81
    invoke-super {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;->cloneTree()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/PhraseSlopQueryNode;

    .line 83
    .local v0, "clone":Lorg/apache/lucene/queryparser/flexible/core/nodes/PhraseSlopQueryNode;
    iget v1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/PhraseSlopQueryNode;->value:I

    iput v1, v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/PhraseSlopQueryNode;->value:I

    .line 85
    return-object v0
.end method

.method public getChild()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 2

    .prologue
    .line 49
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/PhraseSlopQueryNode;->getChildren()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    return-object v0
.end method

.method public getField()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 90
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/PhraseSlopQueryNode;->getChild()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v0

    .line 92
    .local v0, "child":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    instance-of v1, v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldableNode;

    if-eqz v1, :cond_0

    .line 93
    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldableNode;

    .end local v0    # "child":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    invoke-interface {v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldableNode;->getField()Ljava/lang/CharSequence;

    move-result-object v1

    .line 96
    :goto_0
    return-object v1

    .restart local v0    # "child":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getValue()I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/PhraseSlopQueryNode;->value:I

    return v0
.end method

.method public setField(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1, "fieldName"    # Ljava/lang/CharSequence;

    .prologue
    .line 102
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/PhraseSlopQueryNode;->getChild()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v0

    .line 104
    .local v0, "child":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    instance-of v1, v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldableNode;

    if-eqz v1, :cond_0

    .line 105
    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldableNode;

    .end local v0    # "child":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    invoke-interface {v0, p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldableNode;->setField(Ljava/lang/CharSequence;)V

    .line 108
    :cond_0
    return-void
.end method

.method public toQueryString(Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;)Ljava/lang/CharSequence;
    .locals 2
    .param p1, "escapeSyntaxParser"    # Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;

    .prologue
    .line 73
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/PhraseSlopQueryNode;->getChild()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v0

    if-nez v0, :cond_0

    .line 74
    const-string v0, ""

    .line 75
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/PhraseSlopQueryNode;->getChild()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v1

    invoke-interface {v1, p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->toQueryString(Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "~"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 76
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/PhraseSlopQueryNode;->getValueString()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 75
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 67
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "<phraseslop value=\'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/PhraseSlopQueryNode;->getValueString()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 68
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/PhraseSlopQueryNode;->getChild()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n</phraseslop>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 67
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
