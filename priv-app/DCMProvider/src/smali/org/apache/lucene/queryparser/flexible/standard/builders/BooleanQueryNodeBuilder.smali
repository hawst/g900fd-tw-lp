.class public Lorg/apache/lucene/queryparser/flexible/standard/builders/BooleanQueryNodeBuilder;
.super Ljava/lang/Object;
.source "BooleanQueryNodeBuilder.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/flexible/standard/builders/StandardQueryBuilder;


# static fields
.field private static synthetic $SWITCH_TABLE$org$apache$lucene$queryparser$flexible$core$nodes$ModifierQueryNode$Modifier:[I


# direct methods
.method static synthetic $SWITCH_TABLE$org$apache$lucene$queryparser$flexible$core$nodes$ModifierQueryNode$Modifier()[I
    .locals 3

    .prologue
    .line 44
    sget-object v0, Lorg/apache/lucene/queryparser/flexible/standard/builders/BooleanQueryNodeBuilder;->$SWITCH_TABLE$org$apache$lucene$queryparser$flexible$core$nodes$ModifierQueryNode$Modifier:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;->values()[Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;->MOD_NONE:Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    invoke-virtual {v1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;->MOD_NOT:Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    invoke-virtual {v1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;->MOD_REQ:Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    invoke-virtual {v1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lorg/apache/lucene/queryparser/flexible/standard/builders/BooleanQueryNodeBuilder;->$SWITCH_TABLE$org$apache$lucene$queryparser$flexible$core$nodes$ModifierQueryNode$Modifier:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    return-void
.end method

.method private static getModifierValue(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/search/BooleanClause$Occur;
    .locals 3
    .param p0, "node"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    .prologue
    .line 89
    instance-of v1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;

    if-eqz v1, :cond_0

    move-object v0, p0

    .line 90
    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;

    .line 91
    .local v0, "mNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;
    invoke-static {}, Lorg/apache/lucene/queryparser/flexible/standard/builders/BooleanQueryNodeBuilder;->$SWITCH_TABLE$org$apache$lucene$queryparser$flexible$core$nodes$ModifierQueryNode$Modifier()[I

    move-result-object v1

    invoke-virtual {v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;->getModifier()Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 106
    .end local v0    # "mNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;
    :cond_0
    sget-object v1, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    :goto_0
    return-object v1

    .line 94
    .restart local v0    # "mNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;
    :pswitch_0
    sget-object v1, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    goto :goto_0

    .line 97
    :pswitch_1
    sget-object v1, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST_NOT:Lorg/apache/lucene/search/BooleanClause$Occur;

    goto :goto_0

    .line 100
    :pswitch_2
    sget-object v1, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    goto :goto_0

    .line 91
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public bridge synthetic build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/BooleanQueryNodeBuilder;->build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/search/BooleanQuery;

    move-result-object v0

    return-object v0
.end method

.method public build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/search/BooleanQuery;
    .locals 13
    .param p1, "queryNode"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 52
    move-object v1, p1

    check-cast v1, Lorg/apache/lucene/queryparser/flexible/core/nodes/BooleanQueryNode;

    .line 54
    .local v1, "booleanNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/BooleanQueryNode;
    new-instance v0, Lorg/apache/lucene/search/BooleanQuery;

    invoke-direct {v0}, Lorg/apache/lucene/search/BooleanQuery;-><init>()V

    .line 55
    .local v0, "bQuery":Lorg/apache/lucene/search/BooleanQuery;
    invoke-virtual {v1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/BooleanQueryNode;->getChildren()Ljava/util/List;

    move-result-object v3

    .line 57
    .local v3, "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    if-eqz v3, :cond_1

    .line 59
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_2

    .line 83
    :cond_1
    return-object v0

    .line 59
    :cond_2
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    .line 60
    .local v2, "child":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    sget-object v8, Lorg/apache/lucene/queryparser/flexible/core/builders/QueryTreeBuilder;->QUERY_TREE_BUILDER_TAGID:Ljava/lang/String;

    invoke-interface {v2, v8}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->getTag(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    .line 62
    .local v5, "obj":Ljava/lang/Object;
    if-eqz v5, :cond_0

    move-object v6, v5

    .line 63
    check-cast v6, Lorg/apache/lucene/search/Query;

    .line 66
    .local v6, "query":Lorg/apache/lucene/search/Query;
    :try_start_0
    invoke-static {v2}, Lorg/apache/lucene/queryparser/flexible/standard/builders/BooleanQueryNodeBuilder;->getModifierValue(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/search/BooleanClause$Occur;

    move-result-object v8

    invoke-virtual {v0, v6, v8}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V
    :try_end_0
    .catch Lorg/apache/lucene/search/BooleanQuery$TooManyClauses; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 68
    :catch_0
    move-exception v4

    .line 70
    .local v4, "ex":Lorg/apache/lucene/search/BooleanQuery$TooManyClauses;
    new-instance v7, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;

    new-instance v8, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;

    .line 71
    sget-object v9, Lorg/apache/lucene/queryparser/flexible/core/messages/QueryParserMessages;->TOO_MANY_BOOLEAN_CLAUSES:Ljava/lang/String;

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    .line 72
    invoke-static {}, Lorg/apache/lucene/search/BooleanQuery;->getMaxClauseCount()I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    .line 73
    new-instance v12, Lorg/apache/lucene/queryparser/flexible/standard/parser/EscapeQuerySyntaxImpl;

    invoke-direct {v12}, Lorg/apache/lucene/queryparser/flexible/standard/parser/EscapeQuerySyntaxImpl;-><init>()V

    invoke-interface {p1, v12}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->toQueryString(Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;)Ljava/lang/CharSequence;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-direct {v8, v9, v10}, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 70
    invoke-direct {v7, v8, v4}, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;-><init>(Lorg/apache/lucene/queryparser/flexible/messages/Message;Ljava/lang/Throwable;)V

    throw v7
.end method

.method public bridge synthetic build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/search/Query;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/BooleanQueryNodeBuilder;->build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/search/BooleanQuery;

    move-result-object v0

    return-object v0
.end method
