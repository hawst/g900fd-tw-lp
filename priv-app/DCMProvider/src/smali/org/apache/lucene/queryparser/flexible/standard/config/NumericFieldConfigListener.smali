.class public Lorg/apache/lucene/queryparser/flexible/standard/config/NumericFieldConfigListener;
.super Ljava/lang/Object;
.source "NumericFieldConfigListener.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/flexible/core/config/FieldConfigListener;


# instance fields
.field private final config:Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;)V
    .locals 2
    .param p1, "config"    # Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    if-nez p1, :cond_0

    .line 50
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "config cannot be null!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 53
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/queryparser/flexible/standard/config/NumericFieldConfigListener;->config:Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    .line 55
    return-void
.end method


# virtual methods
.method public buildFieldConfig(Lorg/apache/lucene/queryparser/flexible/core/config/FieldConfig;)V
    .locals 4
    .param p1, "fieldConfig"    # Lorg/apache/lucene/queryparser/flexible/core/config/FieldConfig;

    .prologue
    .line 59
    iget-object v2, p0, Lorg/apache/lucene/queryparser/flexible/standard/config/NumericFieldConfigListener;->config:Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    .line 60
    sget-object v3, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->NUMERIC_CONFIG_MAP:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->get(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    .line 62
    .local v1, "numericConfigMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;>;"
    if-eqz v1, :cond_0

    .line 64
    invoke-virtual {p1}, Lorg/apache/lucene/queryparser/flexible/core/config/FieldConfig;->getField()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;

    .line 66
    .local v0, "numericConfig":Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;
    if-eqz v0, :cond_0

    .line 67
    sget-object v2, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->NUMERIC_CONFIG:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-virtual {p1, v2, v0}, Lorg/apache/lucene/queryparser/flexible/core/config/FieldConfig;->set(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;Ljava/lang/Object;)V

    .line 72
    .end local v0    # "numericConfig":Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;
    :cond_0
    return-void
.end method
