.class public interface abstract Lorg/apache/lucene/queryparser/flexible/core/nodes/RangeQueryNode;
.super Ljava/lang/Object;
.source "RangeQueryNode.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldableNode;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldValuePairQueryNode",
        "<*>;>",
        "Ljava/lang/Object;",
        "Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldableNode;"
    }
.end annotation


# virtual methods
.method public abstract getLowerBound()Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldValuePairQueryNode;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public abstract getUpperBound()Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldValuePairQueryNode;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public abstract isLowerInclusive()Z
.end method

.method public abstract isUpperInclusive()Z
.end method
