.class public Lorg/apache/lucene/queryparser/analyzing/AnalyzingQueryParser;
.super Lorg/apache/lucene/queryparser/classic/QueryParser;
.source "AnalyzingQueryParser.java"


# direct methods
.method public constructor <init>(Lorg/apache/lucene/util/Version;Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;

    .prologue
    .line 52
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/queryparser/classic/QueryParser;-><init>(Lorg/apache/lucene/util/Version;Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer;)V

    .line 53
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/analyzing/AnalyzingQueryParser;->setAnalyzeRangeTerms(Z)V

    .line 54
    return-void
.end method


# virtual methods
.method protected getFuzzyQuery(Ljava/lang/String;Ljava/lang/String;F)Lorg/apache/lucene/search/Query;
    .locals 8
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "termStr"    # Ljava/lang/String;
    .param p3, "minSimilarity"    # F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/classic/ParseException;
        }
    .end annotation

    .prologue
    .line 249
    const/4 v3, 0x0

    .line 250
    .local v3, "source":Lorg/apache/lucene/analysis/TokenStream;
    const/4 v2, 0x0

    .line 251
    .local v2, "nextToken":Ljava/lang/String;
    const/4 v1, 0x0

    .line 254
    .local v1, "multipleTokens":Z
    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/analyzing/AnalyzingQueryParser;->getAnalyzer()Lorg/apache/lucene/analysis/Analyzer;

    move-result-object v5

    new-instance v6, Ljava/io/StringReader;

    invoke-direct {v6, p2}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1, v6}, Lorg/apache/lucene/analysis/Analyzer;->tokenStream(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/TokenStream;

    move-result-object v3

    .line 255
    const-class v5, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {v3, v5}, Lorg/apache/lucene/analysis/TokenStream;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 256
    .local v4, "termAtt":Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
    invoke-virtual {v3}, Lorg/apache/lucene/analysis/TokenStream;->reset()V

    .line 257
    invoke-virtual {v3}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 258
    invoke-interface {v4}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->toString()Ljava/lang/String;

    move-result-object v2

    .line 260
    :cond_0
    invoke-virtual {v3}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 266
    .end local v4    # "termAtt":Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
    :goto_0
    :try_start_1
    invoke-virtual {v3}, Lorg/apache/lucene/analysis/TokenStream;->end()V

    .line 267
    invoke-virtual {v3}, Lorg/apache/lucene/analysis/TokenStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 272
    :goto_1
    if-eqz v1, :cond_1

    .line 273
    new-instance v5, Lorg/apache/lucene/queryparser/classic/ParseException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Cannot build FuzzyQuery with analyzer "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/analyzing/AnalyzingQueryParser;->getAnalyzer()Lorg/apache/lucene/analysis/Analyzer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 274
    const-string v7, " - tokens were added"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 273
    invoke-direct {v5, v6}, Lorg/apache/lucene/queryparser/classic/ParseException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 261
    :catch_0
    move-exception v0

    .line 262
    .local v0, "e":Ljava/io/IOException;
    const/4 v2, 0x0

    goto :goto_0

    .line 277
    .end local v0    # "e":Ljava/io/IOException;
    :cond_1
    if-nez v2, :cond_2

    const/4 v5, 0x0

    :goto_2
    return-object v5

    :cond_2
    invoke-super {p0, p1, v2, p3}, Lorg/apache/lucene/queryparser/classic/QueryParser;->getFuzzyQuery(Ljava/lang/String;Ljava/lang/String;F)Lorg/apache/lucene/search/Query;

    move-result-object v5

    goto :goto_2

    .line 268
    :catch_1
    move-exception v5

    goto :goto_1
.end method

.method protected getPrefixQuery(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/lucene/search/Query;
    .locals 9
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "termStr"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/classic/ParseException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    .line 197
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 199
    .local v4, "tlist":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/analyzing/AnalyzingQueryParser;->getAnalyzer()Lorg/apache/lucene/analysis/Analyzer;

    move-result-object v5

    new-instance v6, Ljava/io/StringReader;

    invoke-direct {v6, p2}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1, v6}, Lorg/apache/lucene/analysis/Analyzer;->tokenStream(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/TokenStream;

    move-result-object v2

    .line 200
    .local v2, "source":Lorg/apache/lucene/analysis/TokenStream;
    invoke-virtual {v2}, Lorg/apache/lucene/analysis/TokenStream;->reset()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 204
    const-class v5, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {v2, v5}, Lorg/apache/lucene/analysis/TokenStream;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 207
    .local v3, "termAtt":Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
    :goto_0
    :try_start_1
    invoke-virtual {v2}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v5

    if-nez v5, :cond_0

    .line 215
    :goto_1
    :try_start_2
    invoke-virtual {v2}, Lorg/apache/lucene/analysis/TokenStream;->end()V

    .line 216
    invoke-virtual {v2}, Lorg/apache/lucene/analysis/TokenStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 221
    :goto_2
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    if-ne v5, v8, :cond_1

    .line 222
    const/4 v5, 0x0

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-super {p0, p1, v5}, Lorg/apache/lucene/queryparser/classic/QueryParser;->getPrefixQuery(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/lucene/search/Query;

    move-result-object v5

    return-object v5

    .line 201
    .end local v2    # "source":Lorg/apache/lucene/analysis/TokenStream;
    .end local v3    # "termAtt":Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
    :catch_0
    move-exception v1

    .line 202
    .local v1, "e1":Ljava/io/IOException;
    new-instance v5, Ljava/lang/RuntimeException;

    invoke-direct {v5, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v5

    .line 208
    .end local v1    # "e1":Ljava/io/IOException;
    .restart local v2    # "source":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v3    # "termAtt":Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
    :catch_1
    move-exception v0

    .line 209
    .local v0, "e":Ljava/io/IOException;
    goto :goto_1

    .line 211
    .end local v0    # "e":Ljava/io/IOException;
    :cond_0
    invoke-interface {v3}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 226
    :cond_1
    new-instance v6, Lorg/apache/lucene/queryparser/classic/ParseException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v7, "Cannot build PrefixQuery with analyzer "

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 227
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/analyzing/AnalyzingQueryParser;->getAnalyzer()Lorg/apache/lucene/analysis/Analyzer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 228
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    if-le v5, v8, :cond_2

    const-string v5, " - token(s) added"

    :goto_3
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 226
    invoke-direct {v6, v5}, Lorg/apache/lucene/queryparser/classic/ParseException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 228
    :cond_2
    const-string v5, " - token consumed"

    goto :goto_3

    .line 217
    :catch_2
    move-exception v5

    goto :goto_2
.end method

.method protected getWildcardQuery(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/lucene/search/Query;
    .locals 21
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "termStr"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/classic/ParseException;
        }
    .end annotation

    .prologue
    .line 78
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 79
    .local v15, "tlist":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 82
    .local v17, "wlist":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v18, "?"

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v18

    if-nez v18, :cond_1

    const-string v18, "*"

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v18

    if-nez v18, :cond_1

    const/4 v10, 0x1

    .line 83
    .local v10, "isWithinToken":Z
    :goto_0
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    .line 84
    .local v16, "tmpBuffer":Ljava/lang/StringBuilder;
    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v3

    .line 85
    .local v3, "chars":[C
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_1
    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v18

    move/from16 v0, v18

    if-lt v8, v0, :cond_2

    .line 101
    if-eqz v10, :cond_7

    .line 102
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-interface {v15, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 110
    :goto_2
    const/4 v4, 0x0

    .line 112
    .local v4, "countTokens":I
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/queryparser/analyzing/AnalyzingQueryParser;->getAnalyzer()Lorg/apache/lucene/analysis/Analyzer;

    move-result-object v18

    new-instance v19, Ljava/io/StringReader;

    move-object/from16 v0, v19

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/analysis/Analyzer;->tokenStream(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/TokenStream;

    move-result-object v12

    .line 113
    .local v12, "source":Lorg/apache/lucene/analysis/TokenStream;
    invoke-virtual {v12}, Lorg/apache/lucene/analysis/TokenStream;->reset()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 117
    const-class v18, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    move-object/from16 v0, v18

    invoke-virtual {v12, v0}, Lorg/apache/lucene/analysis/TokenStream;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v14

    check-cast v14, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .local v14, "termAtt":Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
    move v5, v4

    .line 120
    .end local v4    # "countTokens":I
    .local v5, "countTokens":I
    :cond_0
    :goto_3
    :try_start_1
    invoke-virtual {v12}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v18

    if-nez v18, :cond_8

    .line 134
    :goto_4
    :try_start_2
    invoke-virtual {v12}, Lorg/apache/lucene/analysis/TokenStream;->end()V

    .line 135
    invoke-virtual {v12}, Lorg/apache/lucene/analysis/TokenStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    .line 140
    :goto_5
    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v18

    move/from16 v0, v18

    if-eq v5, v0, :cond_9

    .line 143
    new-instance v18, Lorg/apache/lucene/queryparser/classic/ParseException;

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "Cannot build WildcardQuery with analyzer "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 144
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/queryparser/analyzing/AnalyzingQueryParser;->getAnalyzer()Lorg/apache/lucene/analysis/Analyzer;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " - tokens added or lost"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 143
    invoke-direct/range {v18 .. v19}, Lorg/apache/lucene/queryparser/classic/ParseException;-><init>(Ljava/lang/String;)V

    throw v18

    .line 82
    .end local v3    # "chars":[C
    .end local v5    # "countTokens":I
    .end local v8    # "i":I
    .end local v10    # "isWithinToken":Z
    .end local v12    # "source":Lorg/apache/lucene/analysis/TokenStream;
    .end local v14    # "termAtt":Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
    .end local v16    # "tmpBuffer":Ljava/lang/StringBuilder;
    :cond_1
    const/4 v10, 0x0

    goto :goto_0

    .line 86
    .restart local v3    # "chars":[C
    .restart local v8    # "i":I
    .restart local v10    # "isWithinToken":Z
    .restart local v16    # "tmpBuffer":Ljava/lang/StringBuilder;
    :cond_2
    aget-char v18, v3, v8

    const/16 v19, 0x3f

    move/from16 v0, v18

    move/from16 v1, v19

    if-eq v0, v1, :cond_3

    aget-char v18, v3, v8

    const/16 v19, 0x2a

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_5

    .line 87
    :cond_3
    if-eqz v10, :cond_4

    .line 88
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-interface {v15, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 89
    const/16 v18, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 91
    :cond_4
    const/4 v10, 0x0

    .line 99
    :goto_6
    aget-char v18, v3, v8

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 85
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_1

    .line 93
    :cond_5
    if-nez v10, :cond_6

    .line 94
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-interface/range {v17 .. v18}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 95
    const/16 v18, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 97
    :cond_6
    const/4 v10, 0x1

    goto :goto_6

    .line 104
    :cond_7
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-interface/range {v17 .. v18}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 114
    .restart local v4    # "countTokens":I
    :catch_0
    move-exception v7

    .line 115
    .local v7, "e1":Ljava/io/IOException;
    new-instance v18, Ljava/lang/RuntimeException;

    move-object/from16 v0, v18

    invoke-direct {v0, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v18

    .line 121
    .end local v4    # "countTokens":I
    .end local v7    # "e1":Ljava/io/IOException;
    .restart local v5    # "countTokens":I
    .restart local v12    # "source":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v14    # "termAtt":Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
    :catch_1
    move-exception v6

    .line 122
    .local v6, "e":Ljava/io/IOException;
    goto/16 :goto_4

    .line 124
    .end local v6    # "e":Ljava/io/IOException;
    :cond_8
    invoke-interface {v14}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->toString()Ljava/lang/String;

    move-result-object v13

    .line 125
    .local v13, "term":Ljava/lang/String;
    const-string v18, ""

    move-object/from16 v0, v18

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_0

    .line 127
    add-int/lit8 v4, v5, 0x1

    .end local v5    # "countTokens":I
    .restart local v4    # "countTokens":I
    :try_start_3
    invoke-interface {v15, v5, v13}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_3 .. :try_end_3} :catch_2

    move v5, v4

    .line 128
    .end local v4    # "countTokens":I
    .restart local v5    # "countTokens":I
    goto/16 :goto_3

    .end local v5    # "countTokens":I
    .restart local v4    # "countTokens":I
    :catch_2
    move-exception v9

    .line 129
    .local v9, "ioobe":Ljava/lang/IndexOutOfBoundsException;
    const/4 v4, -0x1

    move v5, v4

    .line 118
    .end local v4    # "countTokens":I
    .restart local v5    # "countTokens":I
    goto/16 :goto_3

    .line 147
    .end local v9    # "ioobe":Ljava/lang/IndexOutOfBoundsException;
    .end local v13    # "term":Ljava/lang/String;
    :cond_9
    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v18

    if-nez v18, :cond_a

    .line 148
    const/16 v18, 0x0

    .line 171
    :goto_7
    return-object v18

    .line 149
    :cond_a
    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_c

    .line 150
    if-eqz v17, :cond_b

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_b

    .line 154
    new-instance v19, Ljava/lang/StringBuilder;

    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-interface {v15, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/String;

    invoke-static/range {v18 .. v18}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 155
    const/16 v18, 0x0

    invoke-interface/range {v17 .. v18}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/String;

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    .line 154
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    invoke-super {v0, v1, v2}, Lorg/apache/lucene/queryparser/classic/QueryParser;->getWildcardQuery(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/lucene/search/Query;

    move-result-object v18

    goto :goto_7

    .line 159
    :cond_b
    new-instance v18, Ljava/lang/IllegalArgumentException;

    const-string v19, "getWildcardQuery called without wildcard"

    invoke-direct/range {v18 .. v19}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v18

    .line 164
    :cond_c
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 165
    .local v11, "sb":Ljava/lang/StringBuilder;
    const/4 v8, 0x0

    :goto_8
    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v18

    move/from16 v0, v18

    if-lt v8, v0, :cond_d

    .line 171
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    invoke-super {v0, v1, v2}, Lorg/apache/lucene/queryparser/classic/QueryParser;->getWildcardQuery(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/lucene/search/Query;

    move-result-object v18

    goto :goto_7

    .line 166
    :cond_d
    invoke-interface {v15, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 167
    if-eqz v17, :cond_e

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v18

    move/from16 v0, v18

    if-le v0, v8, :cond_e

    .line 168
    move-object/from16 v0, v17

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 165
    :cond_e
    add-int/lit8 v8, v8, 0x1

    goto :goto_8

    .line 136
    .end local v11    # "sb":Ljava/lang/StringBuilder;
    :catch_3
    move-exception v18

    goto/16 :goto_5
.end method
