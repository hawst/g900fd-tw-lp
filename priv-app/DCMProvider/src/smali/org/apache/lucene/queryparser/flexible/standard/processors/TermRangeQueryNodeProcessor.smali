.class public Lorg/apache/lucene/queryparser/flexible/standard/processors/TermRangeQueryNodeProcessor;
.super Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl;
.source "TermRangeQueryNodeProcessor.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl;-><init>()V

    .line 62
    return-void
.end method


# virtual methods
.method protected postProcessNode(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 20
    .param p1, "node"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 67
    move-object/from16 v0, p1

    instance-of v0, v0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/TermRangeQueryNode;

    move/from16 v18, v0

    if-eqz v18, :cond_7

    move-object/from16 v15, p1

    .line 68
    check-cast v15, Lorg/apache/lucene/queryparser/flexible/standard/nodes/TermRangeQueryNode;

    .line 69
    .local v15, "termRangeNode":Lorg/apache/lucene/queryparser/flexible/standard/nodes/TermRangeQueryNode;
    invoke-virtual {v15}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/TermRangeQueryNode;->getUpperBound()Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldValuePairQueryNode;

    move-result-object v17

    check-cast v17, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;

    .line 70
    .local v17, "upper":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    invoke-virtual {v15}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/TermRangeQueryNode;->getLowerBound()Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldValuePairQueryNode;

    move-result-object v12

    check-cast v12, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;

    .line 72
    .local v12, "lower":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    const/4 v5, 0x0

    .line 73
    .local v5, "dateRes":Lorg/apache/lucene/document/DateTools$Resolution;
    const/4 v10, 0x0

    .line 74
    .local v10, "inclusive":Z
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/queryparser/flexible/standard/processors/TermRangeQueryNodeProcessor;->getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    move-result-object v18

    sget-object v19, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->LOCALE:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-virtual/range {v18 .. v19}, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->get(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/util/Locale;

    .line 76
    .local v11, "locale":Ljava/util/Locale;
    if-nez v11, :cond_0

    .line 77
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v11

    .line 80
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/queryparser/flexible/standard/processors/TermRangeQueryNodeProcessor;->getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    move-result-object v18

    sget-object v19, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->TIMEZONE:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-virtual/range {v18 .. v19}, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->get(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/util/TimeZone;

    .line 82
    .local v16, "timeZone":Ljava/util/TimeZone;
    if-nez v16, :cond_1

    .line 83
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v16

    .line 86
    :cond_1
    invoke-virtual {v15}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/TermRangeQueryNode;->getField()Ljava/lang/CharSequence;

    move-result-object v7

    .line 87
    .local v7, "field":Ljava/lang/CharSequence;
    const/4 v9, 0x0

    .line 89
    .local v9, "fieldStr":Ljava/lang/String;
    if-eqz v7, :cond_2

    .line 90
    invoke-interface {v7}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v9

    .line 93
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/queryparser/flexible/standard/processors/TermRangeQueryNodeProcessor;->getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    move-result-object v18

    .line 94
    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->getFieldConfig(Ljava/lang/String;)Lorg/apache/lucene/queryparser/flexible/core/config/FieldConfig;

    move-result-object v8

    .line 96
    .local v8, "fieldConfig":Lorg/apache/lucene/queryparser/flexible/core/config/FieldConfig;
    if-eqz v8, :cond_3

    .line 97
    sget-object v18, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->DATE_RESOLUTION:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    move-object/from16 v0, v18

    invoke-virtual {v8, v0}, Lorg/apache/lucene/queryparser/flexible/core/config/FieldConfig;->get(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;)Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "dateRes":Lorg/apache/lucene/document/DateTools$Resolution;
    check-cast v5, Lorg/apache/lucene/document/DateTools$Resolution;

    .line 100
    .restart local v5    # "dateRes":Lorg/apache/lucene/document/DateTools$Resolution;
    :cond_3
    invoke-virtual {v15}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/TermRangeQueryNode;->isUpperInclusive()Z

    move-result v18

    if-eqz v18, :cond_4

    .line 101
    const/4 v10, 0x1

    .line 104
    :cond_4
    invoke-virtual {v12}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->getTextAsString()Ljava/lang/String;

    move-result-object v13

    .line 105
    .local v13, "part1":Ljava/lang/String;
    invoke-virtual/range {v17 .. v17}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->getTextAsString()Ljava/lang/String;

    move-result-object v14

    .line 108
    .local v14, "part2":Ljava/lang/String;
    const/16 v18, 0x3

    :try_start_0
    move/from16 v0, v18

    invoke-static {v0, v11}, Ljava/text/DateFormat;->getDateInstance(ILjava/util/Locale;)Ljava/text/DateFormat;

    move-result-object v6

    .line 109
    .local v6, "df":Ljava/text/DateFormat;
    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-virtual {v6, v0}, Ljava/text/DateFormat;->setLenient(Z)V

    .line 111
    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v18

    if-lez v18, :cond_5

    .line 112
    invoke-virtual {v6, v13}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v3

    .line 113
    .local v3, "d1":Ljava/util/Date;
    invoke-static {v3, v5}, Lorg/apache/lucene/document/DateTools;->dateToString(Ljava/util/Date;Lorg/apache/lucene/document/DateTools$Resolution;)Ljava/lang/String;

    move-result-object v13

    .line 114
    invoke-virtual {v12, v13}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->setText(Ljava/lang/CharSequence;)V

    .line 117
    .end local v3    # "d1":Ljava/util/Date;
    :cond_5
    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v18

    if-lez v18, :cond_7

    .line 118
    invoke-virtual {v6, v14}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v4

    .line 119
    .local v4, "d2":Ljava/util/Date;
    if-eqz v10, :cond_6

    .line 124
    move-object/from16 v0, v16

    invoke-static {v0, v11}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v2

    .line 125
    .local v2, "cal":Ljava/util/Calendar;
    invoke-virtual {v2, v4}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 126
    const/16 v18, 0xb

    const/16 v19, 0x17

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 127
    const/16 v18, 0xc

    const/16 v19, 0x3b

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 128
    const/16 v18, 0xd

    const/16 v19, 0x3b

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 129
    const/16 v18, 0xe

    const/16 v19, 0x3e7

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 130
    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v4

    .line 133
    .end local v2    # "cal":Ljava/util/Calendar;
    :cond_6
    invoke-static {v4, v5}, Lorg/apache/lucene/document/DateTools;->dateToString(Ljava/util/Date;Lorg/apache/lucene/document/DateTools$Resolution;)Ljava/lang/String;

    move-result-object v14

    .line 134
    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 144
    .end local v4    # "d2":Ljava/util/Date;
    .end local v5    # "dateRes":Lorg/apache/lucene/document/DateTools$Resolution;
    .end local v6    # "df":Ljava/text/DateFormat;
    .end local v7    # "field":Ljava/lang/CharSequence;
    .end local v8    # "fieldConfig":Lorg/apache/lucene/queryparser/flexible/core/config/FieldConfig;
    .end local v9    # "fieldStr":Ljava/lang/String;
    .end local v10    # "inclusive":Z
    .end local v11    # "locale":Ljava/util/Locale;
    .end local v12    # "lower":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    .end local v13    # "part1":Ljava/lang/String;
    .end local v14    # "part2":Ljava/lang/String;
    .end local v15    # "termRangeNode":Lorg/apache/lucene/queryparser/flexible/standard/nodes/TermRangeQueryNode;
    .end local v16    # "timeZone":Ljava/util/TimeZone;
    .end local v17    # "upper":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    :cond_7
    :goto_0
    return-object p1

    .line 138
    .restart local v5    # "dateRes":Lorg/apache/lucene/document/DateTools$Resolution;
    .restart local v7    # "field":Ljava/lang/CharSequence;
    .restart local v8    # "fieldConfig":Lorg/apache/lucene/queryparser/flexible/core/config/FieldConfig;
    .restart local v9    # "fieldStr":Ljava/lang/String;
    .restart local v10    # "inclusive":Z
    .restart local v11    # "locale":Ljava/util/Locale;
    .restart local v12    # "lower":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    .restart local v13    # "part1":Ljava/lang/String;
    .restart local v14    # "part2":Ljava/lang/String;
    .restart local v15    # "termRangeNode":Lorg/apache/lucene/queryparser/flexible/standard/nodes/TermRangeQueryNode;
    .restart local v16    # "timeZone":Ljava/util/TimeZone;
    .restart local v17    # "upper":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    :catch_0
    move-exception v18

    goto :goto_0
.end method

.method protected preProcessNode(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 0
    .param p1, "node"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 151
    return-object p1
.end method

.method protected setChildrenOrder(Ljava/util/List;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 159
    .local p1, "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    return-object p1
.end method
