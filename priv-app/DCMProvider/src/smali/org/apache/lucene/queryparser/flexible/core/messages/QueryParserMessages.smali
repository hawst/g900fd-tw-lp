.class public Lorg/apache/lucene/queryparser/flexible/core/messages/QueryParserMessages;
.super Lorg/apache/lucene/queryparser/flexible/messages/NLS;
.source "QueryParserMessages.java"


# static fields
.field private static final BUNDLE_NAME:Ljava/lang/String;

.field public static COULD_NOT_PARSE_NUMBER:Ljava/lang/String;

.field public static EMPTY_MESSAGE:Ljava/lang/String;

.field public static INVALID_SYNTAX:Ljava/lang/String;

.field public static INVALID_SYNTAX_CANNOT_PARSE:Ljava/lang/String;

.field public static INVALID_SYNTAX_ESCAPE_CHARACTER:Ljava/lang/String;

.field public static INVALID_SYNTAX_ESCAPE_NONE_HEX_UNICODE:Ljava/lang/String;

.field public static INVALID_SYNTAX_ESCAPE_UNICODE_TRUNCATION:Ljava/lang/String;

.field public static INVALID_SYNTAX_FUZZY_EDITS:Ljava/lang/String;

.field public static INVALID_SYNTAX_FUZZY_LIMITS:Ljava/lang/String;

.field public static LEADING_WILDCARD_NOT_ALLOWED:Ljava/lang/String;

.field public static LUCENE_QUERY_CONVERSION_ERROR:Ljava/lang/String;

.field public static NODE_ACTION_NOT_SUPPORTED:Ljava/lang/String;

.field public static NUMBER_CLASS_NOT_SUPPORTED_BY_NUMERIC_RANGE_QUERY:Ljava/lang/String;

.field public static NUMERIC_CANNOT_BE_EMPTY:Ljava/lang/String;

.field public static PARAMETER_VALUE_NOT_SUPPORTED:Ljava/lang/String;

.field public static TOO_MANY_BOOLEAN_CLAUSES:Ljava/lang/String;

.field public static UNSUPPORTED_NUMERIC_DATA_TYPE:Ljava/lang/String;

.field public static WILDCARD_NOT_SUPPORTED:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 27
    const-class v0, Lorg/apache/lucene/queryparser/flexible/core/messages/QueryParserMessages;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/queryparser/flexible/core/messages/QueryParserMessages;->BUNDLE_NAME:Ljava/lang/String;

    .line 36
    sget-object v0, Lorg/apache/lucene/queryparser/flexible/core/messages/QueryParserMessages;->BUNDLE_NAME:Ljava/lang/String;

    const-class v1, Lorg/apache/lucene/queryparser/flexible/core/messages/QueryParserMessages;

    invoke-static {v0, v1}, Lorg/apache/lucene/queryparser/flexible/messages/NLS;->initializeMessages(Ljava/lang/String;Ljava/lang/Class;)V

    .line 57
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/messages/NLS;-><init>()V

    .line 31
    return-void
.end method
