.class public interface abstract Lorg/apache/lucene/queryparser/classic/QueryParserConstants;
.super Ljava/lang/Object;
.source "QueryParserConstants.java"


# static fields
.field public static final AND:I = 0x8

.field public static final BAREOPER:I = 0xd

.field public static final Boost:I = 0x0

.field public static final CARAT:I = 0x12

.field public static final COLON:I = 0x10

.field public static final DEFAULT:I = 0x2

.field public static final EOF:I = 0x0

.field public static final FUZZY_SLOP:I = 0x15

.field public static final LPAREN:I = 0xe

.field public static final MINUS:I = 0xc

.field public static final NOT:I = 0xa

.field public static final NUMBER:I = 0x1b

.field public static final OR:I = 0x9

.field public static final PLUS:I = 0xb

.field public static final PREFIXTERM:I = 0x16

.field public static final QUOTED:I = 0x13

.field public static final RANGEEX_END:I = 0x1e

.field public static final RANGEEX_START:I = 0x1a

.field public static final RANGEIN_END:I = 0x1d

.field public static final RANGEIN_START:I = 0x19

.field public static final RANGE_GOOP:I = 0x20

.field public static final RANGE_QUOTED:I = 0x1f

.field public static final RANGE_TO:I = 0x1c

.field public static final REGEXPTERM:I = 0x18

.field public static final RPAREN:I = 0xf

.field public static final Range:I = 0x1

.field public static final STAR:I = 0x11

.field public static final TERM:I = 0x14

.field public static final WILDTERM:I = 0x17

.field public static final _ESCAPED_CHAR:I = 0x2

.field public static final _NUM_CHAR:I = 0x1

.field public static final _QUOTED_CHAR:I = 0x6

.field public static final _TERM_CHAR:I = 0x4

.field public static final _TERM_START_CHAR:I = 0x3

.field public static final _WHITESPACE:I = 0x5

.field public static final tokenImage:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 84
    const/16 v0, 0x21

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    .line 85
    const-string v2, "<EOF>"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 86
    const-string v2, "<_NUM_CHAR>"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 87
    const-string v2, "<_ESCAPED_CHAR>"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    .line 88
    const-string v2, "<_TERM_START_CHAR>"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    .line 89
    const-string v2, "<_TERM_CHAR>"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    .line 90
    const-string v2, "<_WHITESPACE>"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    .line 91
    const-string v2, "<_QUOTED_CHAR>"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 92
    const-string v2, "<token of kind 7>"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    .line 93
    const-string v2, "<AND>"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    .line 94
    const-string v2, "<OR>"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    .line 95
    const-string v2, "<NOT>"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    .line 96
    const-string v2, "\"+\""

    aput-object v2, v0, v1

    const/16 v1, 0xc

    .line 97
    const-string v2, "\"-\""

    aput-object v2, v0, v1

    const/16 v1, 0xd

    .line 98
    const-string v2, "<BAREOPER>"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    .line 99
    const-string v2, "\"(\""

    aput-object v2, v0, v1

    const/16 v1, 0xf

    .line 100
    const-string v2, "\")\""

    aput-object v2, v0, v1

    const/16 v1, 0x10

    .line 101
    const-string v2, "\":\""

    aput-object v2, v0, v1

    const/16 v1, 0x11

    .line 102
    const-string v2, "\"*\""

    aput-object v2, v0, v1

    const/16 v1, 0x12

    .line 103
    const-string v2, "\"^\""

    aput-object v2, v0, v1

    const/16 v1, 0x13

    .line 104
    const-string v2, "<QUOTED>"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    .line 105
    const-string v2, "<TERM>"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    .line 106
    const-string v2, "<FUZZY_SLOP>"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    .line 107
    const-string v2, "<PREFIXTERM>"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    .line 108
    const-string v2, "<WILDTERM>"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    .line 109
    const-string v2, "<REGEXPTERM>"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    .line 110
    const-string v2, "\"[\""

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    .line 111
    const-string v2, "\"{\""

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    .line 112
    const-string v2, "<NUMBER>"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    .line 113
    const-string v2, "\"TO\""

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    .line 114
    const-string v2, "\"]\""

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    .line 115
    const-string v2, "\"}\""

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    .line 116
    const-string v2, "<RANGE_QUOTED>"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    .line 117
    const-string v2, "<RANGE_GOOP>"

    aput-object v2, v0, v1

    .line 84
    sput-object v0, Lorg/apache/lucene/queryparser/classic/QueryParserConstants;->tokenImage:[Ljava/lang/String;

    .line 118
    return-void
.end method
