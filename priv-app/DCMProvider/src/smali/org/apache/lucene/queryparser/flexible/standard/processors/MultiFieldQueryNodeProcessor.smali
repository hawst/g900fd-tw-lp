.class public Lorg/apache/lucene/queryparser/flexible/standard/processors/MultiFieldQueryNodeProcessor;
.super Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl;
.source "MultiFieldQueryNodeProcessor.java"


# instance fields
.field private processChildren:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl;-><init>()V

    .line 49
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/processors/MultiFieldQueryNodeProcessor;->processChildren:Z

    .line 53
    return-void
.end method


# virtual methods
.method protected postProcessNode(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 0
    .param p1, "node"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 58
    return-object p1
.end method

.method protected preProcessNode(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 8
    .param p1, "node"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 77
    instance-of v5, p1, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldableNode;

    if-eqz v5, :cond_3

    .line 78
    iput-boolean v7, p0, Lorg/apache/lucene/queryparser/flexible/standard/processors/MultiFieldQueryNodeProcessor;->processChildren:Z

    move-object v2, p1

    .line 79
    check-cast v2, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldableNode;

    .line 81
    .local v2, "fieldNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldableNode;
    invoke-interface {v2}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldableNode;->getField()Ljava/lang/CharSequence;

    move-result-object v5

    if-nez v5, :cond_3

    .line 82
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/processors/MultiFieldQueryNodeProcessor;->getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    move-result-object v5

    sget-object v6, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->MULTI_FIELDS:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-virtual {v5, v6}, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->get(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/CharSequence;

    .line 84
    .local v3, "fields":[Ljava/lang/CharSequence;
    if-nez v3, :cond_0

    .line 85
    new-instance v5, Ljava/lang/IllegalArgumentException;

    .line 86
    const-string v6, "StandardQueryConfigHandler.ConfigurationKeys.MULTI_FIELDS should be set on the QueryConfigHandler"

    .line 85
    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 89
    :cond_0
    if-eqz v3, :cond_3

    array-length v5, v3

    if-lez v5, :cond_3

    .line 90
    aget-object v5, v3, v7

    invoke-interface {v2, v5}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldableNode;->setField(Ljava/lang/CharSequence;)V

    .line 92
    array-length v5, v3

    const/4 v6, 0x1

    if-ne v5, v6, :cond_1

    .line 122
    .end local v2    # "fieldNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldableNode;
    .end local v3    # "fields":[Ljava/lang/CharSequence;
    :goto_0
    return-object v2

    .line 96
    .restart local v2    # "fieldNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldableNode;
    .restart local v3    # "fields":[Ljava/lang/CharSequence;
    :cond_1
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 97
    .local v1, "children":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    invoke-virtual {v1, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 99
    const/4 v4, 0x1

    .local v4, "i":I
    :goto_1
    array-length v5, v3

    if-lt v4, v5, :cond_2

    .line 112
    new-instance v2, Lorg/apache/lucene/queryparser/flexible/core/nodes/GroupQueryNode;

    .end local v2    # "fieldNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldableNode;
    new-instance v5, Lorg/apache/lucene/queryparser/flexible/core/nodes/OrQueryNode;

    invoke-direct {v5, v1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/OrQueryNode;-><init>(Ljava/util/List;)V

    invoke-direct {v2, v5}, Lorg/apache/lucene/queryparser/flexible/core/nodes/GroupQueryNode;-><init>(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)V

    goto :goto_0

    .line 101
    .restart local v2    # "fieldNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldableNode;
    :cond_2
    :try_start_0
    invoke-interface {v2}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldableNode;->cloneTree()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldableNode;

    move-object v2, v0

    .line 102
    aget-object v5, v3, v4

    invoke-interface {v2, v5}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldableNode;->setField(Ljava/lang/CharSequence;)V

    .line 104
    invoke-virtual {v1, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 99
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .end local v1    # "children":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    .end local v2    # "fieldNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldableNode;
    .end local v3    # "fields":[Ljava/lang/CharSequence;
    .end local v4    # "i":I
    :cond_3
    move-object v2, p1

    .line 122
    goto :goto_0

    .line 106
    .restart local v1    # "children":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    .restart local v2    # "fieldNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldableNode;
    .restart local v3    # "fields":[Ljava/lang/CharSequence;
    .restart local v4    # "i":I
    :catch_0
    move-exception v5

    goto :goto_2
.end method

.method protected processChildren(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)V
    .locals 1
    .param p1, "queryTree"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 65
    iget-boolean v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/processors/MultiFieldQueryNodeProcessor;->processChildren:Z

    if-eqz v0, :cond_0

    .line 66
    invoke-super {p0, p1}, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl;->processChildren(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)V

    .line 72
    :goto_0
    return-void

    .line 69
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/processors/MultiFieldQueryNodeProcessor;->processChildren:Z

    goto :goto_0
.end method

.method protected setChildrenOrder(Ljava/util/List;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 130
    .local p1, "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    return-object p1
.end method
