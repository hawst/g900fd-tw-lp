.class public Lorg/apache/lucene/queryparser/flexible/standard/builders/FuzzyQueryNodeBuilder;
.super Ljava/lang/Object;
.source "FuzzyQueryNodeBuilder.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/flexible/standard/builders/StandardQueryBuilder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    return-void
.end method


# virtual methods
.method public bridge synthetic build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/FuzzyQueryNodeBuilder;->build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/search/FuzzyQuery;

    move-result-object v0

    return-object v0
.end method

.method public build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/search/FuzzyQuery;
    .locals 7
    .param p1, "queryNode"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 37
    move-object v0, p1

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FuzzyQueryNode;

    .line 38
    .local v0, "fuzzyNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/FuzzyQueryNode;
    invoke-virtual {v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FuzzyQueryNode;->getTextAsString()Ljava/lang/String;

    move-result-object v2

    .line 40
    .local v2, "text":Ljava/lang/String;
    invoke-virtual {v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FuzzyQueryNode;->getSimilarity()F

    move-result v3

    .line 41
    const/4 v4, 0x0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->codePointCount(II)I

    move-result v4

    .line 40
    invoke-static {v3, v4}, Lorg/apache/lucene/search/FuzzyQuery;->floatToEdits(FI)I

    move-result v1

    .line 43
    .local v1, "numEdits":I
    new-instance v3, Lorg/apache/lucene/search/FuzzyQuery;

    new-instance v4, Lorg/apache/lucene/index/Term;

    invoke-virtual {v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FuzzyQueryNode;->getFieldAsString()Ljava/lang/String;

    move-result-object v5

    .line 44
    invoke-virtual {v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FuzzyQueryNode;->getTextAsString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    invoke-virtual {v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FuzzyQueryNode;->getPrefixLength()I

    move-result v5

    .line 43
    invoke-direct {v3, v4, v1, v5}, Lorg/apache/lucene/search/FuzzyQuery;-><init>(Lorg/apache/lucene/index/Term;II)V

    return-object v3
.end method

.method public bridge synthetic build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/search/Query;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/FuzzyQueryNodeBuilder;->build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/search/FuzzyQuery;

    move-result-object v0

    return-object v0
.end method
