.class public Lorg/apache/lucene/queryparser/ext/ExtendableQueryParser;
.super Lorg/apache/lucene/queryparser/classic/QueryParser;
.source "ExtendableQueryParser.java"


# static fields
.field private static final DEFAULT_EXTENSION:Lorg/apache/lucene/queryparser/ext/Extensions;


# instance fields
.field private final defaultField:Ljava/lang/String;

.field private final extensions:Lorg/apache/lucene/queryparser/ext/Extensions;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 82
    new-instance v0, Lorg/apache/lucene/queryparser/ext/Extensions;

    invoke-direct {v0}, Lorg/apache/lucene/queryparser/ext/Extensions;-><init>()V

    sput-object v0, Lorg/apache/lucene/queryparser/ext/ExtendableQueryParser;->DEFAULT_EXTENSION:Lorg/apache/lucene/queryparser/ext/Extensions;

    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "f"    # Ljava/lang/String;
    .param p3, "a"    # Lorg/apache/lucene/analysis/Analyzer;

    .prologue
    .line 96
    sget-object v0, Lorg/apache/lucene/queryparser/ext/ExtendableQueryParser;->DEFAULT_EXTENSION:Lorg/apache/lucene/queryparser/ext/Extensions;

    invoke-direct {p0, p1, p2, p3, v0}, Lorg/apache/lucene/queryparser/ext/ExtendableQueryParser;-><init>(Lorg/apache/lucene/util/Version;Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer;Lorg/apache/lucene/queryparser/ext/Extensions;)V

    .line 98
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer;Lorg/apache/lucene/queryparser/ext/Extensions;)V
    .locals 0
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "f"    # Ljava/lang/String;
    .param p3, "a"    # Lorg/apache/lucene/analysis/Analyzer;
    .param p4, "ext"    # Lorg/apache/lucene/queryparser/ext/Extensions;

    .prologue
    .line 114
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/queryparser/classic/QueryParser;-><init>(Lorg/apache/lucene/util/Version;Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer;)V

    .line 115
    iput-object p2, p0, Lorg/apache/lucene/queryparser/ext/ExtendableQueryParser;->defaultField:Ljava/lang/String;

    .line 116
    iput-object p4, p0, Lorg/apache/lucene/queryparser/ext/ExtendableQueryParser;->extensions:Lorg/apache/lucene/queryparser/ext/Extensions;

    .line 117
    return-void
.end method


# virtual methods
.method public getExtensionFieldDelimiter()C
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lorg/apache/lucene/queryparser/ext/ExtendableQueryParser;->extensions:Lorg/apache/lucene/queryparser/ext/Extensions;

    invoke-virtual {v0}, Lorg/apache/lucene/queryparser/ext/Extensions;->getExtensionFieldDelimiter()C

    move-result v0

    return v0
.end method

.method protected getFieldQuery(Ljava/lang/String;Ljava/lang/String;Z)Lorg/apache/lucene/search/Query;
    .locals 4
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "queryText"    # Ljava/lang/String;
    .param p3, "quoted"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/classic/ParseException;
        }
    .end annotation

    .prologue
    .line 131
    iget-object v2, p0, Lorg/apache/lucene/queryparser/ext/ExtendableQueryParser;->extensions:Lorg/apache/lucene/queryparser/ext/Extensions;

    .line 132
    iget-object v3, p0, Lorg/apache/lucene/queryparser/ext/ExtendableQueryParser;->defaultField:Ljava/lang/String;

    invoke-virtual {v2, v3, p1}, Lorg/apache/lucene/queryparser/ext/Extensions;->splitExtensionField(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/lucene/queryparser/ext/Extensions$Pair;

    move-result-object v1

    .line 133
    .local v1, "splitExtensionField":Lorg/apache/lucene/queryparser/ext/Extensions$Pair;, "Lorg/apache/lucene/queryparser/ext/Extensions$Pair<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v3, p0, Lorg/apache/lucene/queryparser/ext/ExtendableQueryParser;->extensions:Lorg/apache/lucene/queryparser/ext/Extensions;

    .line 134
    iget-object v2, v1, Lorg/apache/lucene/queryparser/ext/Extensions$Pair;->cud:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v3, v2}, Lorg/apache/lucene/queryparser/ext/Extensions;->getExtension(Ljava/lang/String;)Lorg/apache/lucene/queryparser/ext/ParserExtension;

    move-result-object v0

    .line 135
    .local v0, "extension":Lorg/apache/lucene/queryparser/ext/ParserExtension;
    if-eqz v0, :cond_0

    .line 136
    new-instance v3, Lorg/apache/lucene/queryparser/ext/ExtensionQuery;

    iget-object v2, v1, Lorg/apache/lucene/queryparser/ext/Extensions$Pair;->cur:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    .line 137
    invoke-direct {v3, p0, v2, p2}, Lorg/apache/lucene/queryparser/ext/ExtensionQuery;-><init>(Lorg/apache/lucene/queryparser/classic/QueryParser;Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    invoke-virtual {v0, v3}, Lorg/apache/lucene/queryparser/ext/ParserExtension;->parse(Lorg/apache/lucene/queryparser/ext/ExtensionQuery;)Lorg/apache/lucene/search/Query;

    move-result-object v2

    .line 139
    :goto_0
    return-object v2

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lorg/apache/lucene/queryparser/classic/QueryParser;->getFieldQuery(Ljava/lang/String;Ljava/lang/String;Z)Lorg/apache/lucene/search/Query;

    move-result-object v2

    goto :goto_0
.end method
