.class public Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericRangeQueryNode;
.super Lorg/apache/lucene/queryparser/flexible/standard/nodes/AbstractRangeQueryNode;
.source "NumericRangeQueryNode.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/queryparser/flexible/standard/nodes/AbstractRangeQueryNode",
        "<",
        "Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;",
        ">;"
    }
.end annotation


# instance fields
.field public numericConfig:Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;ZZLorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;)V
    .locals 0
    .param p1, "lower"    # Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;
    .param p2, "upper"    # Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;
    .param p3, "lowerInclusive"    # Z
    .param p4, "upperInclusive"    # Z
    .param p5, "numericConfig"    # Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 51
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/AbstractRangeQueryNode;-><init>()V

    .line 53
    invoke-virtual/range {p0 .. p5}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericRangeQueryNode;->setBounds(Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;ZZLorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;)V

    .line 54
    return-void
.end method

.method private static getNumericDataType(Ljava/lang/Number;)Lorg/apache/lucene/document/FieldType$NumericType;
    .locals 6
    .param p0, "number"    # Ljava/lang/Number;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 58
    instance-of v0, p0, Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 59
    sget-object v0, Lorg/apache/lucene/document/FieldType$NumericType;->LONG:Lorg/apache/lucene/document/FieldType$NumericType;

    .line 65
    :goto_0
    return-object v0

    .line 60
    :cond_0
    instance-of v0, p0, Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 61
    sget-object v0, Lorg/apache/lucene/document/FieldType$NumericType;->INT:Lorg/apache/lucene/document/FieldType$NumericType;

    goto :goto_0

    .line 62
    :cond_1
    instance-of v0, p0, Ljava/lang/Double;

    if-eqz v0, :cond_2

    .line 63
    sget-object v0, Lorg/apache/lucene/document/FieldType$NumericType;->DOUBLE:Lorg/apache/lucene/document/FieldType$NumericType;

    goto :goto_0

    .line 64
    :cond_2
    instance-of v0, p0, Ljava/lang/Float;

    if-eqz v0, :cond_3

    .line 65
    sget-object v0, Lorg/apache/lucene/document/FieldType$NumericType;->FLOAT:Lorg/apache/lucene/document/FieldType$NumericType;

    goto :goto_0

    .line 67
    :cond_3
    new-instance v0, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;

    .line 68
    new-instance v1, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;

    .line 69
    sget-object v2, Lorg/apache/lucene/queryparser/flexible/core/messages/QueryParserMessages;->NUMBER_CLASS_NOT_SUPPORTED_BY_NUMERIC_RANGE_QUERY:Ljava/lang/String;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 70
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    .line 68
    aput-object v5, v3, v4

    invoke-direct {v1, v2, v3}, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 67
    invoke-direct {v0, v1}, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;-><init>(Lorg/apache/lucene/queryparser/flexible/messages/Message;)V

    throw v0
.end method


# virtual methods
.method public getNumericConfig()Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericRangeQueryNode;->numericConfig:Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;

    return-object v0
.end method

.method public setBounds(Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;ZZLorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;)V
    .locals 5
    .param p1, "lower"    # Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;
    .param p2, "upper"    # Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;
    .param p3, "lowerInclusive"    # Z
    .param p4, "upperInclusive"    # Z
    .param p5, "numericConfig"    # Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 89
    if-nez p5, :cond_0

    .line 90
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "numericConfig cannot be null!"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 95
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;->getValue()Ljava/lang/Number;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 96
    invoke-virtual {p1}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;->getValue()Ljava/lang/Number;

    move-result-object v2

    invoke-static {v2}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericRangeQueryNode;->getNumericDataType(Ljava/lang/Number;)Lorg/apache/lucene/document/FieldType$NumericType;

    move-result-object v0

    .line 101
    .local v0, "lowerNumberType":Lorg/apache/lucene/document/FieldType$NumericType;
    :goto_0
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;->getValue()Ljava/lang/Number;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 102
    invoke-virtual {p2}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;->getValue()Ljava/lang/Number;

    move-result-object v2

    invoke-static {v2}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericRangeQueryNode;->getNumericDataType(Ljava/lang/Number;)Lorg/apache/lucene/document/FieldType$NumericType;

    move-result-object v1

    .line 107
    .local v1, "upperNumberType":Lorg/apache/lucene/document/FieldType$NumericType;
    :goto_1
    if-eqz v0, :cond_3

    .line 108
    invoke-virtual {p5}, Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;->getType()Lorg/apache/lucene/document/FieldType$NumericType;

    move-result-object v2

    invoke-virtual {v0, v2}, Lorg/apache/lucene/document/FieldType$NumericType;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 109
    new-instance v2, Ljava/lang/IllegalArgumentException;

    .line 110
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "lower value\'s type should be the same as numericConfig type: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 111
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " != "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p5}, Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;->getType()Lorg/apache/lucene/document/FieldType$NumericType;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 110
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 109
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 98
    .end local v0    # "lowerNumberType":Lorg/apache/lucene/document/FieldType$NumericType;
    .end local v1    # "upperNumberType":Lorg/apache/lucene/document/FieldType$NumericType;
    :cond_1
    const/4 v0, 0x0

    .restart local v0    # "lowerNumberType":Lorg/apache/lucene/document/FieldType$NumericType;
    goto :goto_0

    .line 104
    :cond_2
    const/4 v1, 0x0

    .restart local v1    # "upperNumberType":Lorg/apache/lucene/document/FieldType$NumericType;
    goto :goto_1

    .line 114
    :cond_3
    if-eqz v1, :cond_4

    .line 115
    invoke-virtual {p5}, Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;->getType()Lorg/apache/lucene/document/FieldType$NumericType;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/lucene/document/FieldType$NumericType;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 116
    new-instance v2, Ljava/lang/IllegalArgumentException;

    .line 117
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "upper value\'s type should be the same as numericConfig type: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 118
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " != "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p5}, Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;->getType()Lorg/apache/lucene/document/FieldType$NumericType;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 117
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 116
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 121
    :cond_4
    invoke-super {p0, p1, p2, p3, p4}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/AbstractRangeQueryNode;->setBounds(Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldValuePairQueryNode;Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldValuePairQueryNode;ZZ)V

    .line 122
    iput-object p5, p0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericRangeQueryNode;->numericConfig:Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;

    .line 124
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    const/16 v4, 0xa

    .line 137
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "<numericRange lowerInclusive=\'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 139
    .local v0, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericRangeQueryNode;->isLowerInclusive()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' upperInclusive=\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 140
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericRangeQueryNode;->isUpperInclusive()Z

    move-result v2

    .line 139
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 141
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "\' precisionStep=\'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericRangeQueryNode;->numericConfig:Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;

    invoke-virtual {v3}, Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;->getPrecisionStep()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 140
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 142
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "\' type=\'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericRangeQueryNode;->numericConfig:Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;

    invoke-virtual {v3}, Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;->getType()Lorg/apache/lucene/document/FieldType$NumericType;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 141
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 142
    const-string v2, "\'>\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 144
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericRangeQueryNode;->getLowerBound()Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldValuePairQueryNode;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 145
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericRangeQueryNode;->getUpperBound()Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldValuePairQueryNode;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 146
    const-string v1, "</numericRange>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 148
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
