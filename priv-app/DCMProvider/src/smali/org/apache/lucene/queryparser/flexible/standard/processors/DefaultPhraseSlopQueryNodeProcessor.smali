.class public Lorg/apache/lucene/queryparser/flexible/standard/processors/DefaultPhraseSlopQueryNodeProcessor;
.super Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl;
.source "DefaultPhraseSlopQueryNodeProcessor.java"


# instance fields
.field private defaultPhraseSlop:I

.field private processChildren:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl;-><init>()V

    .line 44
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/processors/DefaultPhraseSlopQueryNodeProcessor;->processChildren:Z

    .line 50
    return-void
.end method


# virtual methods
.method protected postProcessNode(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 2
    .param p1, "node"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 75
    instance-of v0, p1, Lorg/apache/lucene/queryparser/flexible/core/nodes/TokenizedPhraseQueryNode;

    if-nez v0, :cond_0

    .line 76
    instance-of v0, p1, Lorg/apache/lucene/queryparser/flexible/standard/nodes/MultiPhraseQueryNode;

    if-eqz v0, :cond_1

    .line 78
    :cond_0
    new-instance v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/SlopQueryNode;

    iget v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/processors/DefaultPhraseSlopQueryNodeProcessor;->defaultPhraseSlop:I

    invoke-direct {v0, p1, v1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/SlopQueryNode;-><init>(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;I)V

    move-object p1, v0

    .line 82
    .end local p1    # "node":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    :cond_1
    return-object p1
.end method

.method protected preProcessNode(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 1
    .param p1, "node"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 89
    instance-of v0, p1, Lorg/apache/lucene/queryparser/flexible/core/nodes/SlopQueryNode;

    if-eqz v0, :cond_0

    .line 90
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/processors/DefaultPhraseSlopQueryNodeProcessor;->processChildren:Z

    .line 94
    :cond_0
    return-object p1
.end method

.method public process(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 3
    .param p1, "queryTree"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 54
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/processors/DefaultPhraseSlopQueryNodeProcessor;->getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    move-result-object v1

    .line 56
    .local v1, "queryConfig":Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;
    if-eqz v1, :cond_0

    .line 57
    sget-object v2, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->PHRASE_SLOP:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->get(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 59
    .local v0, "defaultPhraseSlop":Ljava/lang/Integer;
    if-eqz v0, :cond_0

    .line 60
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, p0, Lorg/apache/lucene/queryparser/flexible/standard/processors/DefaultPhraseSlopQueryNodeProcessor;->defaultPhraseSlop:I

    .line 62
    invoke-super {p0, p1}, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl;->process(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object p1

    .line 68
    .end local v0    # "defaultPhraseSlop":Ljava/lang/Integer;
    .end local p1    # "queryTree":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    :cond_0
    return-object p1
.end method

.method protected processChildren(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)V
    .locals 1
    .param p1, "queryTree"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 101
    iget-boolean v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/processors/DefaultPhraseSlopQueryNodeProcessor;->processChildren:Z

    if-eqz v0, :cond_0

    .line 102
    invoke-super {p0, p1}, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl;->processChildren(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)V

    .line 108
    :goto_0
    return-void

    .line 105
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/processors/DefaultPhraseSlopQueryNodeProcessor;->processChildren:Z

    goto :goto_0
.end method

.method protected setChildrenOrder(Ljava/util/List;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 114
    .local p1, "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    return-object p1
.end method
