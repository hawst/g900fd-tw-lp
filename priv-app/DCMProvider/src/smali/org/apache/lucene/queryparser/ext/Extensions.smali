.class public Lorg/apache/lucene/queryparser/ext/Extensions;
.super Ljava/lang/Object;
.source "Extensions.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/queryparser/ext/Extensions$Pair;
    }
.end annotation


# static fields
.field public static final DEFAULT_EXTENSION_FIELD_DELIMITER:C = ':'


# instance fields
.field private final extensionFieldDelimiter:C

.field private final extensions:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/queryparser/ext/ParserExtension;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 58
    const/16 v0, 0x3a

    invoke-direct {p0, v0}, Lorg/apache/lucene/queryparser/ext/Extensions;-><init>(C)V

    .line 59
    return-void
.end method

.method public constructor <init>(C)V
    .locals 1
    .param p1, "extensionFieldDelimiter"    # C

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/queryparser/ext/Extensions;->extensions:Ljava/util/Map;

    .line 68
    iput-char p1, p0, Lorg/apache/lucene/queryparser/ext/Extensions;->extensionFieldDelimiter:C

    .line 69
    return-void
.end method


# virtual methods
.method public add(Ljava/lang/String;Lorg/apache/lucene/queryparser/ext/ParserExtension;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "extension"    # Lorg/apache/lucene/queryparser/ext/ParserExtension;

    .prologue
    .line 80
    iget-object v0, p0, Lorg/apache/lucene/queryparser/ext/Extensions;->extensions:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    return-void
.end method

.method public buildExtensionField(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "extensionKey"    # Ljava/lang/String;

    .prologue
    .line 161
    const-string v0, ""

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/queryparser/ext/Extensions;->buildExtensionField(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public buildExtensionField(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "extensionKey"    # Ljava/lang/String;
    .param p2, "field"    # Ljava/lang/String;

    .prologue
    .line 186
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, p2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 187
    .local v0, "builder":Ljava/lang/StringBuilder;
    iget-char v1, p0, Lorg/apache/lucene/queryparser/ext/Extensions;->extensionFieldDelimiter:C

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 188
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 189
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/apache/lucene/queryparser/ext/Extensions;->escapeExtensionField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public escapeExtensionField(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "extfield"    # Ljava/lang/String;

    .prologue
    .line 143
    invoke-static {p1}, Lorg/apache/lucene/queryparser/classic/QueryParserBase;->escape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getExtension(Ljava/lang/String;)Lorg/apache/lucene/queryparser/ext/ParserExtension;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 93
    iget-object v0, p0, Lorg/apache/lucene/queryparser/ext/Extensions;->extensions:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/ext/ParserExtension;

    return-object v0
.end method

.method public getExtensionFieldDelimiter()C
    .locals 1

    .prologue
    .line 102
    iget-char v0, p0, Lorg/apache/lucene/queryparser/ext/Extensions;->extensionFieldDelimiter:C

    return v0
.end method

.method public splitExtensionField(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/lucene/queryparser/ext/Extensions$Pair;
    .locals 5
    .param p1, "defaultField"    # Ljava/lang/String;
    .param p2, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lorg/apache/lucene/queryparser/ext/Extensions$Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 123
    iget-char v3, p0, Lorg/apache/lucene/queryparser/ext/Extensions;->extensionFieldDelimiter:C

    invoke-virtual {p2, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    .line 124
    .local v2, "indexOf":I
    if-gez v2, :cond_0

    .line 125
    new-instance v3, Lorg/apache/lucene/queryparser/ext/Extensions$Pair;

    const/4 v4, 0x0

    invoke-direct {v3, p2, v4}, Lorg/apache/lucene/queryparser/ext/Extensions$Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 129
    :goto_0
    return-object v3

    .line 126
    :cond_0
    if-nez v2, :cond_1

    move-object v1, p1

    .line 128
    .local v1, "indexField":Ljava/lang/String;
    :goto_1
    add-int/lit8 v3, v2, 0x1

    invoke-virtual {p2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 129
    .local v0, "extensionKey":Ljava/lang/String;
    new-instance v3, Lorg/apache/lucene/queryparser/ext/Extensions$Pair;

    invoke-direct {v3, v1, v0}, Lorg/apache/lucene/queryparser/ext/Extensions$Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 126
    .end local v0    # "extensionKey":Ljava/lang/String;
    .end local v1    # "indexField":Ljava/lang/String;
    :cond_1
    const/4 v3, 0x0

    invoke-virtual {p2, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method
