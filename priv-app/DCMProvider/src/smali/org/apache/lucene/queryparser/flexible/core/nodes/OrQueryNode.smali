.class public Lorg/apache/lucene/queryparser/flexible/core/nodes/OrQueryNode;
.super Lorg/apache/lucene/queryparser/flexible/core/nodes/BooleanQueryNode;
.source "OrQueryNode.java"


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 37
    .local p1, "clauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/BooleanQueryNode;-><init>(Ljava/util/List;)V

    .line 38
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 39
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 40
    const-string v1, "OR query must have at least one clause"

    .line 39
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 42
    :cond_1
    return-void
.end method


# virtual methods
.method public toQueryString(Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;)Ljava/lang/CharSequence;
    .locals 5
    .param p1, "escapeSyntaxParser"    # Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;

    .prologue
    .line 61
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/OrQueryNode;->getChildren()Ljava/util/List;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/OrQueryNode;->getChildren()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_1

    .line 62
    :cond_0
    const-string v3, ""

    .line 76
    :goto_0
    return-object v3

    .line 64
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 65
    .local v2, "sb":Ljava/lang/StringBuilder;
    const-string v0, ""

    .line 66
    .local v0, "filler":Ljava/lang/String;
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/OrQueryNode;->getChildren()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_4

    .line 72
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/OrQueryNode;->getParent()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/OrQueryNode;->getParent()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v3

    instance-of v3, v3, Lorg/apache/lucene/queryparser/flexible/core/nodes/GroupQueryNode;

    if-nez v3, :cond_3

    .line 73
    :cond_2
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/OrQueryNode;->isRoot()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 74
    :cond_3
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 67
    :cond_4
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    invoke-interface {v3, p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->toQueryString(Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 68
    const-string v0, " OR "

    goto :goto_1

    .line 76
    :cond_5
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "( "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " )"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 46
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/OrQueryNode;->getChildren()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/OrQueryNode;->getChildren()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_1

    .line 47
    :cond_0
    const-string v2, "<boolean operation=\'or\'/>"

    .line 56
    :goto_0
    return-object v2

    .line 48
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 49
    .local v1, "sb":Ljava/lang/StringBuilder;
    const-string v2, "<boolean operation=\'or\'>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 50
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/OrQueryNode;->getChildren()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 55
    const-string v2, "\n</boolean>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 56
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 50
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    .line 51
    .local v0, "child":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    const-string v3, "\n"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 52
    invoke-interface {v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method
