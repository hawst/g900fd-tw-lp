.class public Lorg/apache/lucene/queryparser/flexible/messages/NLS;
.super Ljava/lang/Object;
.source "NLS.java"


# static fields
.field private static bundles:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<+",
            "Lorg/apache/lucene/queryparser/flexible/messages/NLS;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 49
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 48
    sput-object v0, Lorg/apache/lucene/queryparser/flexible/messages/NLS;->bundles:Ljava/util/Map;

    .line 49
    return-void
.end method

.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    return-void
.end method

.method public static getLocalizedMessage(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "key"    # Ljava/lang/String;

    .prologue
    .line 56
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {p0, v0}, Lorg/apache/lucene/queryparser/flexible/messages/NLS;->getLocalizedMessage(Ljava/lang/String;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getLocalizedMessage(Ljava/lang/String;Ljava/util/Locale;)Ljava/lang/String;
    .locals 3
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "locale"    # Ljava/util/Locale;

    .prologue
    .line 60
    invoke-static {p0, p1}, Lorg/apache/lucene/queryparser/flexible/messages/NLS;->getResourceBundleObject(Ljava/lang/String;Ljava/util/Locale;)Ljava/lang/Object;

    move-result-object v0

    .line 61
    .local v0, "message":Ljava/lang/Object;
    if-nez v0, :cond_0

    .line 62
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Message with key:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and locale: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 63
    const-string v2, " not found."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 62
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 65
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static varargs getLocalizedMessage(Ljava/lang/String;Ljava/util/Locale;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 2
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "locale"    # Ljava/util/Locale;
    .param p2, "args"    # [Ljava/lang/Object;

    .prologue
    .line 70
    invoke-static {p0, p1}, Lorg/apache/lucene/queryparser/flexible/messages/NLS;->getLocalizedMessage(Ljava/lang/String;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 72
    .local v0, "str":Ljava/lang/String;
    array-length v1, p2

    if-lez v1, :cond_0

    .line 73
    invoke-static {v0, p2}, Ljava/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 76
    :cond_0
    return-object v0
.end method

.method public static varargs getLocalizedMessage(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "args"    # [Ljava/lang/Object;

    .prologue
    .line 80
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {p0, v0, p1}, Lorg/apache/lucene/queryparser/flexible/messages/NLS;->getLocalizedMessage(Ljava/lang/String;Ljava/util/Locale;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getResourceBundleObject(Ljava/lang/String;Ljava/util/Locale;)Ljava/lang/Object;
    .locals 6
    .param p0, "messageKey"    # Ljava/lang/String;
    .param p1, "locale"    # Ljava/util/Locale;

    .prologue
    .line 107
    sget-object v4, Lorg/apache/lucene/queryparser/flexible/messages/NLS;->bundles:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 122
    const/4 v2, 0x0

    :goto_1
    return-object v2

    .line 108
    :cond_1
    sget-object v4, Lorg/apache/lucene/queryparser/flexible/messages/NLS;->bundles:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 109
    .local v0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/apache/lucene/queryparser/flexible/messages/NLS;>;"
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, p1}, Ljava/util/ResourceBundle;->getBundle(Ljava/lang/String;Ljava/util/Locale;)Ljava/util/ResourceBundle;

    move-result-object v3

    .line 111
    .local v3, "resourceBundle":Ljava/util/ResourceBundle;
    if-eqz v3, :cond_0

    .line 113
    :try_start_0
    invoke-virtual {v3, p0}, Ljava/util/ResourceBundle;->getObject(Ljava/lang/String;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/MissingResourceException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 114
    .local v2, "obj":Ljava/lang/Object;
    if-eqz v2, :cond_0

    goto :goto_1

    .line 116
    .end local v2    # "obj":Ljava/lang/Object;
    :catch_0
    move-exception v4

    goto :goto_0
.end method

.method protected static initializeMessages(Ljava/lang/String;Ljava/lang/Class;)V
    .locals 1
    .param p0, "bundleName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<+",
            "Lorg/apache/lucene/queryparser/flexible/messages/NLS;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 94
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/apache/lucene/queryparser/flexible/messages/NLS;>;"
    :try_start_0
    invoke-static {p1}, Lorg/apache/lucene/queryparser/flexible/messages/NLS;->load(Ljava/lang/Class;)V

    .line 95
    sget-object v0, Lorg/apache/lucene/queryparser/flexible/messages/NLS;->bundles:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 96
    sget-object v0, Lorg/apache/lucene/queryparser/flexible/messages/NLS;->bundles:Ljava/util/Map;

    invoke-interface {v0, p0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 101
    :cond_0
    :goto_0
    return-void

    .line 97
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private static load(Ljava/lang/Class;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lorg/apache/lucene/queryparser/flexible/messages/NLS;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 126
    .local p0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/apache/lucene/queryparser/flexible/messages/NLS;>;"
    invoke-virtual {p0}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v0

    .line 128
    .local v0, "fieldArray":[Ljava/lang/reflect/Field;
    invoke-virtual {p0}, Ljava/lang/Class;->getModifiers()I

    move-result v5

    and-int/lit8 v5, v5, 0x1

    if-eqz v5, :cond_0

    const/4 v3, 0x1

    .line 131
    .local v3, "isFieldAccessible":Z
    :goto_0
    array-length v4, v0

    .line 132
    .local v4, "len":I
    new-instance v1, Ljava/util/HashMap;

    mul-int/lit8 v5, v4, 0x2

    invoke-direct {v1, v5}, Ljava/util/HashMap;-><init>(I)V

    .line 133
    .local v1, "fields":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/reflect/Field;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-lt v2, v4, :cond_1

    .line 137
    return-void

    .line 128
    .end local v1    # "fields":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/reflect/Field;>;"
    .end local v2    # "i":I
    .end local v3    # "isFieldAccessible":Z
    .end local v4    # "len":I
    :cond_0
    const/4 v3, 0x0

    goto :goto_0

    .line 134
    .restart local v1    # "fields":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/reflect/Field;>;"
    .restart local v2    # "i":I
    .restart local v3    # "isFieldAccessible":Z
    .restart local v4    # "len":I
    :cond_1
    aget-object v5, v0, v2

    invoke-virtual {v5}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v5

    aget-object v6, v0, v2

    invoke-interface {v1, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    aget-object v5, v0, v2

    invoke-static {v5, v3, p0}, Lorg/apache/lucene/queryparser/flexible/messages/NLS;->loadfieldValue(Ljava/lang/reflect/Field;ZLjava/lang/Class;)V

    .line 133
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method private static loadfieldValue(Ljava/lang/reflect/Field;ZLjava/lang/Class;)V
    .locals 4
    .param p0, "field"    # Ljava/lang/reflect/Field;
    .param p1, "isFieldAccessible"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Field;",
            "Z",
            "Ljava/lang/Class",
            "<+",
            "Lorg/apache/lucene/queryparser/flexible/messages/NLS;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 141
    .local p2, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/apache/lucene/queryparser/flexible/messages/NLS;>;"
    const/16 v0, 0x9

    .line 142
    .local v0, "MOD_EXPECTED":I
    or-int/lit8 v1, v0, 0x10

    .line 143
    .local v1, "MOD_MASK":I
    invoke-virtual {p0}, Ljava/lang/reflect/Field;->getModifiers()I

    move-result v2

    and-int/lit8 v2, v2, 0x19

    if-eq v2, v0, :cond_0

    .line 157
    :goto_0
    return-void

    .line 147
    :cond_0
    if-nez p1, :cond_1

    .line 148
    invoke-static {p0}, Lorg/apache/lucene/queryparser/flexible/messages/NLS;->makeAccessible(Ljava/lang/reflect/Field;)V

    .line 150
    :cond_1
    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 151
    invoke-virtual {p0}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p2}, Lorg/apache/lucene/queryparser/flexible/messages/NLS;->validateMessage(Ljava/lang/String;Ljava/lang/Class;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 152
    :catch_0
    move-exception v2

    goto :goto_0

    .line 154
    :catch_1
    move-exception v2

    goto :goto_0
.end method

.method private static makeAccessible(Ljava/lang/reflect/Field;)V
    .locals 1
    .param p0, "field"    # Ljava/lang/reflect/Field;

    .prologue
    .line 188
    invoke-static {}, Ljava/lang/System;->getSecurityManager()Ljava/lang/SecurityManager;

    move-result-object v0

    if-nez v0, :cond_0

    .line 189
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 199
    :goto_0
    return-void

    .line 191
    :cond_0
    new-instance v0, Lorg/apache/lucene/queryparser/flexible/messages/NLS$1;

    invoke-direct {v0, p0}, Lorg/apache/lucene/queryparser/flexible/messages/NLS$1;-><init>(Ljava/lang/reflect/Field;)V

    invoke-static {v0}, Ljava/security/AccessController;->doPrivileged(Ljava/security/PrivilegedAction;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private static validateMessage(Ljava/lang/String;Ljava/lang/Class;)V
    .locals 3
    .param p0, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<+",
            "Lorg/apache/lucene/queryparser/flexible/messages/NLS;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 166
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/apache/lucene/queryparser/flexible/messages/NLS;>;"
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    .line 167
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    .line 166
    invoke-static {v1, v2}, Ljava/util/ResourceBundle;->getBundle(Ljava/lang/String;Ljava/util/Locale;)Ljava/util/ResourceBundle;

    move-result-object v0

    .line 168
    .local v0, "resourceBundle":Ljava/util/ResourceBundle;
    if-eqz v0, :cond_0

    .line 169
    invoke-virtual {v0, p0}, Ljava/util/ResourceBundle;->getObject(Ljava/lang/String;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/MissingResourceException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 182
    .end local v0    # "resourceBundle":Ljava/util/ResourceBundle;
    :cond_0
    :goto_0
    return-void

    .line 177
    :catch_0
    move-exception v1

    goto :goto_0

    .line 174
    :catch_1
    move-exception v1

    goto :goto_0
.end method
