.class public Lorg/apache/lucene/queryparser/xml/builders/SpanNearBuilder;
.super Lorg/apache/lucene/queryparser/xml/builders/SpanBuilderBase;
.source "SpanNearBuilder.java"


# instance fields
.field private final factory:Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilder;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilder;)V
    .locals 0
    .param p1, "factory"    # Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilder;

    .prologue
    .line 36
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/xml/builders/SpanBuilderBase;-><init>()V

    .line 37
    iput-object p1, p0, Lorg/apache/lucene/queryparser/xml/builders/SpanNearBuilder;->factory:Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilder;

    .line 38
    return-void
.end method


# virtual methods
.method public getSpanQuery(Lorg/w3c/dom/Element;)Lorg/apache/lucene/search/spans/SpanQuery;
    .locals 8
    .param p1, "e"    # Lorg/w3c/dom/Element;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/xml/ParserException;
        }
    .end annotation

    .prologue
    .line 42
    const-string v6, "slop"

    invoke-static {p1, v6}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttributeOrFail(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 43
    .local v3, "slopString":Ljava/lang/String;
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 44
    .local v2, "slop":I
    const-string v6, "inOrder"

    const/4 v7, 0x0

    invoke-static {p1, v6, v7}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttribute(Lorg/w3c/dom/Element;Ljava/lang/String;Z)Z

    move-result v0

    .line 45
    .local v0, "inOrder":Z
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 46
    .local v5, "spans":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/spans/SpanQuery;>;"
    invoke-interface {p1}, Lorg/w3c/dom/Element;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v1

    .local v1, "kid":Lorg/w3c/dom/Node;
    :goto_0
    if-nez v1, :cond_0

    .line 51
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    new-array v6, v6, [Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-interface {v5, v6}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Lorg/apache/lucene/search/spans/SpanQuery;

    .line 52
    .local v4, "spanQueries":[Lorg/apache/lucene/search/spans/SpanQuery;
    new-instance v6, Lorg/apache/lucene/search/spans/SpanNearQuery;

    invoke-direct {v6, v4, v2, v0}, Lorg/apache/lucene/search/spans/SpanNearQuery;-><init>([Lorg/apache/lucene/search/spans/SpanQuery;IZ)V

    return-object v6

    .line 47
    .end local v4    # "spanQueries":[Lorg/apache/lucene/search/spans/SpanQuery;
    :cond_0
    invoke-interface {v1}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_1

    .line 48
    iget-object v7, p0, Lorg/apache/lucene/queryparser/xml/builders/SpanNearBuilder;->factory:Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilder;

    move-object v6, v1

    check-cast v6, Lorg/w3c/dom/Element;

    invoke-interface {v7, v6}, Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilder;->getSpanQuery(Lorg/w3c/dom/Element;)Lorg/apache/lucene/search/spans/SpanQuery;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 46
    :cond_1
    invoke-interface {v1}, Lorg/w3c/dom/Node;->getNextSibling()Lorg/w3c/dom/Node;

    move-result-object v1

    goto :goto_0
.end method
