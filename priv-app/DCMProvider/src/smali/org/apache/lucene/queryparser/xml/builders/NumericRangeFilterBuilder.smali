.class public Lorg/apache/lucene/queryparser/xml/builders/NumericRangeFilterBuilder;
.super Ljava/lang/Object;
.source "NumericRangeFilterBuilder.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/xml/FilterBuilder;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/queryparser/xml/builders/NumericRangeFilterBuilder$NoMatchFilter;
    }
.end annotation


# static fields
.field private static final NO_MATCH_FILTER:Lorg/apache/lucene/queryparser/xml/builders/NumericRangeFilterBuilder$NoMatchFilter;


# instance fields
.field private strictMode:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 96
    new-instance v0, Lorg/apache/lucene/queryparser/xml/builders/NumericRangeFilterBuilder$NoMatchFilter;

    invoke-direct {v0}, Lorg/apache/lucene/queryparser/xml/builders/NumericRangeFilterBuilder$NoMatchFilter;-><init>()V

    sput-object v0, Lorg/apache/lucene/queryparser/xml/builders/NumericRangeFilterBuilder;->NO_MATCH_FILTER:Lorg/apache/lucene/queryparser/xml/builders/NumericRangeFilterBuilder$NoMatchFilter;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/queryparser/xml/builders/NumericRangeFilterBuilder;->strictMode:Z

    .line 94
    return-void
.end method


# virtual methods
.method public getFilter(Lorg/w3c/dom/Element;)Lorg/apache/lucene/search/Filter;
    .locals 11
    .param p1, "e"    # Lorg/w3c/dom/Element;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/xml/ParserException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 117
    const-string v2, "fieldName"

    invoke-static {p1, v2}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttributeWithInheritanceOrFail(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 118
    .local v0, "field":Ljava/lang/String;
    const-string v2, "lowerTerm"

    invoke-static {p1, v2}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttributeOrFail(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 119
    .local v7, "lowerTerm":Ljava/lang/String;
    const-string v2, "upperTerm"

    invoke-static {p1, v2}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttributeOrFail(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 120
    .local v10, "upperTerm":Ljava/lang/String;
    const-string v2, "includeLower"

    invoke-static {p1, v2, v3}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttribute(Lorg/w3c/dom/Element;Ljava/lang/String;Z)Z

    move-result v4

    .line 121
    .local v4, "lowerInclusive":Z
    const-string v2, "includeUpper"

    invoke-static {p1, v2, v3}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttribute(Lorg/w3c/dom/Element;Ljava/lang/String;Z)Z

    move-result v5

    .line 122
    .local v5, "upperInclusive":Z
    const-string v2, "precisionStep"

    const/4 v3, 0x4

    invoke-static {p1, v2, v3}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttribute(Lorg/w3c/dom/Element;Ljava/lang/String;I)I

    move-result v1

    .line 124
    .local v1, "precisionStep":I
    const-string v2, "type"

    const-string v3, "int"

    invoke-static {p1, v2, v3}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttribute(Lorg/w3c/dom/Element;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 127
    .local v9, "type":Ljava/lang/String;
    :try_start_0
    const-string v2, "int"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 129
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    .line 128
    invoke-static/range {v0 .. v5}, Lorg/apache/lucene/search/NumericRangeFilter;->newIntRange(Ljava/lang/String;ILjava/lang/Integer;Ljava/lang/Integer;ZZ)Lorg/apache/lucene/search/NumericRangeFilter;

    move-result-object v6

    .line 151
    :goto_0
    return-object v6

    .line 131
    :cond_0
    const-string v2, "long"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 133
    invoke-static {v7}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v10}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    .line 132
    invoke-static/range {v0 .. v5}, Lorg/apache/lucene/search/NumericRangeFilter;->newLongRange(Ljava/lang/String;ILjava/lang/Long;Ljava/lang/Long;ZZ)Lorg/apache/lucene/search/NumericRangeFilter;

    move-result-object v6

    .line 135
    .local v6, "filter":Lorg/apache/lucene/search/Filter;
    goto :goto_0

    .end local v6    # "filter":Lorg/apache/lucene/search/Filter;
    :cond_1
    const-string v2, "double"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 137
    invoke-static {v7}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v2

    invoke-static {v10}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v3

    .line 136
    invoke-static/range {v0 .. v5}, Lorg/apache/lucene/search/NumericRangeFilter;->newDoubleRange(Ljava/lang/String;ILjava/lang/Double;Ljava/lang/Double;ZZ)Lorg/apache/lucene/search/NumericRangeFilter;

    move-result-object v6

    .line 139
    .restart local v6    # "filter":Lorg/apache/lucene/search/Filter;
    goto :goto_0

    .end local v6    # "filter":Lorg/apache/lucene/search/Filter;
    :cond_2
    const-string v2, "float"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 141
    invoke-static {v7}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v2

    invoke-static {v10}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v3

    .line 140
    invoke-static/range {v0 .. v5}, Lorg/apache/lucene/search/NumericRangeFilter;->newFloatRange(Ljava/lang/String;ILjava/lang/Float;Ljava/lang/Float;ZZ)Lorg/apache/lucene/search/NumericRangeFilter;

    move-result-object v6

    .line 143
    .restart local v6    # "filter":Lorg/apache/lucene/search/Filter;
    goto :goto_0

    .line 144
    .end local v6    # "filter":Lorg/apache/lucene/search/Filter;
    :cond_3
    new-instance v2, Lorg/apache/lucene/queryparser/xml/ParserException;

    const-string v3, "type attribute must be one of: [long, int, double, float]"

    invoke-direct {v2, v3}, Lorg/apache/lucene/queryparser/xml/ParserException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 147
    :catch_0
    move-exception v8

    .line 148
    .local v8, "nfe":Ljava/lang/NumberFormatException;
    iget-boolean v2, p0, Lorg/apache/lucene/queryparser/xml/builders/NumericRangeFilterBuilder;->strictMode:Z

    if-eqz v2, :cond_4

    .line 149
    new-instance v2, Lorg/apache/lucene/queryparser/xml/ParserException;

    const-string v3, "Could not parse lowerTerm or upperTerm into a number"

    invoke-direct {v2, v3, v8}, Lorg/apache/lucene/queryparser/xml/ParserException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    .line 151
    :cond_4
    sget-object v6, Lorg/apache/lucene/queryparser/xml/builders/NumericRangeFilterBuilder;->NO_MATCH_FILTER:Lorg/apache/lucene/queryparser/xml/builders/NumericRangeFilterBuilder$NoMatchFilter;

    goto :goto_0
.end method

.method public setStrictMode(Z)V
    .locals 0
    .param p1, "strictMode"    # Z

    .prologue
    .line 112
    iput-boolean p1, p0, Lorg/apache/lucene/queryparser/xml/builders/NumericRangeFilterBuilder;->strictMode:Z

    .line 113
    return-void
.end method
