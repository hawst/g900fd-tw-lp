.class public Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;
.super Ljava/lang/Object;
.source "NumericConfig.java"


# instance fields
.field private format:Ljava/text/NumberFormat;

.field private precisionStep:I

.field private type:Lorg/apache/lucene/document/FieldType$NumericType;


# direct methods
.method public constructor <init>(ILjava/text/NumberFormat;Lorg/apache/lucene/document/FieldType$NumericType;)V
    .locals 0
    .param p1, "precisionStep"    # I
    .param p2, "format"    # Ljava/text/NumberFormat;
    .param p3, "type"    # Lorg/apache/lucene/document/FieldType$NumericType;

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;->setPrecisionStep(I)V

    .line 58
    invoke-virtual {p0, p2}, Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;->setNumberFormat(Ljava/text/NumberFormat;)V

    .line 59
    invoke-virtual {p0, p3}, Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;->setType(Lorg/apache/lucene/document/FieldType$NumericType;)V

    .line 61
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    .line 142
    if-ne p1, p0, :cond_1

    .line 155
    :cond_0
    :goto_0
    return v1

    .line 144
    :cond_1
    instance-of v2, p1, Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;

    if-eqz v2, :cond_2

    move-object v0, p1

    .line 145
    check-cast v0, Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;

    .line 147
    .local v0, "other":Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;
    iget v2, p0, Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;->precisionStep:I

    iget v3, v0, Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;->precisionStep:I

    if-ne v2, v3, :cond_2

    .line 148
    iget-object v2, p0, Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;->type:Lorg/apache/lucene/document/FieldType$NumericType;

    iget-object v3, v0, Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;->type:Lorg/apache/lucene/document/FieldType$NumericType;

    if-ne v2, v3, :cond_2

    .line 149
    iget-object v2, p0, Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;->format:Ljava/text/NumberFormat;

    iget-object v3, v0, Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;->format:Ljava/text/NumberFormat;

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;->format:Ljava/text/NumberFormat;

    iget-object v3, v0, Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;->format:Ljava/text/NumberFormat;

    invoke-virtual {v2, v3}, Ljava/text/NumberFormat;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 155
    .end local v0    # "other":Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getNumberFormat()Ljava/text/NumberFormat;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;->format:Ljava/text/NumberFormat;

    return-object v0
.end method

.method public getPrecisionStep()I
    .locals 1

    .prologue
    .line 71
    iget v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;->precisionStep:I

    return v0
.end method

.method public getType()Lorg/apache/lucene/document/FieldType$NumericType;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;->type:Lorg/apache/lucene/document/FieldType$NumericType;

    return-object v0
.end method

.method public setNumberFormat(Ljava/text/NumberFormat;)V
    .locals 2
    .param p1, "format"    # Ljava/text/NumberFormat;

    .prologue
    .line 131
    if-nez p1, :cond_0

    .line 132
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "format cannot be null!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 135
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;->format:Ljava/text/NumberFormat;

    .line 137
    return-void
.end method

.method public setPrecisionStep(I)V
    .locals 0
    .param p1, "precisionStep"    # I

    .prologue
    .line 83
    iput p1, p0, Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;->precisionStep:I

    .line 84
    return-void
.end method

.method public setType(Lorg/apache/lucene/document/FieldType$NumericType;)V
    .locals 2
    .param p1, "type"    # Lorg/apache/lucene/document/FieldType$NumericType;

    .prologue
    .line 113
    if-nez p1, :cond_0

    .line 114
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "type cannot be null!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 117
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;->type:Lorg/apache/lucene/document/FieldType$NumericType;

    .line 119
    return-void
.end method
