.class public Lorg/apache/lucene/queryparser/flexible/standard/nodes/TermRangeQueryNode;
.super Lorg/apache/lucene/queryparser/flexible/standard/nodes/AbstractRangeQueryNode;
.source "TermRangeQueryNode.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/queryparser/flexible/standard/nodes/AbstractRangeQueryNode",
        "<",
        "Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;ZZ)V
    .locals 0
    .param p1, "lower"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    .param p2, "upper"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    .param p3, "lowerInclusive"    # Z
    .param p4, "upperInclusive"    # Z

    .prologue
    .line 40
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/AbstractRangeQueryNode;-><init>()V

    .line 42
    invoke-virtual {p0, p1, p2, p3, p4}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/TermRangeQueryNode;->setBounds(Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldValuePairQueryNode;Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldValuePairQueryNode;ZZ)V

    .line 43
    return-void
.end method
