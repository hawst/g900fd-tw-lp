.class public Lorg/apache/lucene/queryparser/flexible/core/nodes/NoTokenFoundQueryNode;
.super Lorg/apache/lucene/queryparser/flexible/core/nodes/DeletedQueryNode;
.source "NoTokenFoundQueryNode.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/DeletedQueryNode;-><init>()V

    .line 30
    return-void
.end method


# virtual methods
.method public cloneTree()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 44
    invoke-super {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/DeletedQueryNode;->cloneTree()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/NoTokenFoundQueryNode;

    .line 48
    .local v0, "clone":Lorg/apache/lucene/queryparser/flexible/core/nodes/NoTokenFoundQueryNode;
    return-object v0
.end method

.method public toQueryString(Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;)Ljava/lang/CharSequence;
    .locals 1
    .param p1, "escaper"    # Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;

    .prologue
    .line 34
    const-string v0, "[NTF]"

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    const-string v0, "<notokenfound/>"

    return-object v0
.end method
