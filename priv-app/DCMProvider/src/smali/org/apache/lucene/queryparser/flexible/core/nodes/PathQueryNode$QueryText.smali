.class public Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode$QueryText;
.super Ljava/lang/Object;
.source "PathQueryNode.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "QueryText"
.end annotation


# instance fields
.field begin:I

.field end:I

.field value:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Ljava/lang/CharSequence;II)V
    .locals 1
    .param p1, "value"    # Ljava/lang/CharSequence;
    .param p2, "begin"    # I
    .param p3, "end"    # I

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode$QueryText;->value:Ljava/lang/CharSequence;

    .line 69
    iput-object p1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode$QueryText;->value:Ljava/lang/CharSequence;

    .line 70
    iput p2, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode$QueryText;->begin:I

    .line 71
    iput p3, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode$QueryText;->end:I

    .line 72
    return-void
.end method


# virtual methods
.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode$QueryText;->clone()Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode$QueryText;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode$QueryText;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 76
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode$QueryText;

    .line 77
    .local v0, "clone":Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode$QueryText;
    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode$QueryText;->value:Ljava/lang/CharSequence;

    iput-object v1, v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode$QueryText;->value:Ljava/lang/CharSequence;

    .line 78
    iget v1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode$QueryText;->begin:I

    iput v1, v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode$QueryText;->begin:I

    .line 79
    iget v1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode$QueryText;->end:I

    iput v1, v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode$QueryText;->end:I

    .line 80
    return-object v0
.end method

.method public getBegin()I
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode$QueryText;->begin:I

    return v0
.end method

.method public getEnd()I
    .locals 1

    .prologue
    .line 101
    iget v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode$QueryText;->end:I

    return v0
.end method

.method public getValue()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode$QueryText;->value:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 106
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode$QueryText;->value:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode$QueryText;->begin:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/PathQueryNode$QueryText;->end:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
