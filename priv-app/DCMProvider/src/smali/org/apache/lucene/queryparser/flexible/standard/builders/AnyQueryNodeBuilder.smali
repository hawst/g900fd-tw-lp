.class public Lorg/apache/lucene/queryparser/flexible/standard/builders/AnyQueryNodeBuilder;
.super Ljava/lang/Object;
.source "AnyQueryNodeBuilder.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/flexible/standard/builders/StandardQueryBuilder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    return-void
.end method


# virtual methods
.method public bridge synthetic build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/AnyQueryNodeBuilder;->build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/search/BooleanQuery;

    move-result-object v0

    return-object v0
.end method

.method public build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/search/BooleanQuery;
    .locals 10
    .param p1, "queryNode"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 45
    move-object v0, p1

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/AnyQueryNode;

    .line 47
    .local v0, "andNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/AnyQueryNode;
    new-instance v1, Lorg/apache/lucene/search/BooleanQuery;

    invoke-direct {v1}, Lorg/apache/lucene/search/BooleanQuery;-><init>()V

    .line 48
    .local v1, "bQuery":Lorg/apache/lucene/search/BooleanQuery;
    invoke-virtual {v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/AnyQueryNode;->getChildren()Ljava/util/List;

    move-result-object v3

    .line 50
    .local v3, "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    if-eqz v3, :cond_1

    .line 52
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_2

    .line 76
    :cond_1
    invoke-virtual {v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/AnyQueryNode;->getMinimumMatchingElements()I

    move-result v7

    invoke-virtual {v1, v7}, Lorg/apache/lucene/search/BooleanQuery;->setMinimumNumberShouldMatch(I)V

    .line 78
    return-object v1

    .line 52
    :cond_2
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    .line 53
    .local v2, "child":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    sget-object v8, Lorg/apache/lucene/queryparser/flexible/core/builders/QueryTreeBuilder;->QUERY_TREE_BUILDER_TAGID:Ljava/lang/String;

    invoke-interface {v2, v8}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->getTag(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    .line 55
    .local v5, "obj":Ljava/lang/Object;
    if-eqz v5, :cond_0

    move-object v6, v5

    .line 56
    check-cast v6, Lorg/apache/lucene/search/Query;

    .line 59
    .local v6, "query":Lorg/apache/lucene/search/Query;
    :try_start_0
    sget-object v8, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v1, v6, v8}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V
    :try_end_0
    .catch Lorg/apache/lucene/search/BooleanQuery$TooManyClauses; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 60
    :catch_0
    move-exception v4

    .line 62
    .local v4, "ex":Lorg/apache/lucene/search/BooleanQuery$TooManyClauses;
    new-instance v7, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;

    new-instance v8, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;

    .line 66
    sget-object v9, Lorg/apache/lucene/queryparser/flexible/core/messages/QueryParserMessages;->EMPTY_MESSAGE:Ljava/lang/String;

    invoke-direct {v8, v9}, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;-><init>(Ljava/lang/String;)V

    .line 62
    invoke-direct {v7, v8, v4}, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;-><init>(Lorg/apache/lucene/queryparser/flexible/messages/Message;Ljava/lang/Throwable;)V

    throw v7
.end method

.method public bridge synthetic build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/search/Query;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/AnyQueryNodeBuilder;->build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/search/BooleanQuery;

    move-result-object v0

    return-object v0
.end method
