.class public Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilderFactory;
.super Ljava/lang/Object;
.source "SpanQueryBuilderFactory.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilder;


# instance fields
.field private final builders:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilder;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilderFactory;->builders:Ljava/util/Map;

    .line 30
    return-void
.end method


# virtual methods
.method public addBuilder(Ljava/lang/String;Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilder;)V
    .locals 1
    .param p1, "nodeName"    # Ljava/lang/String;
    .param p2, "builder"    # Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilder;

    .prologue
    .line 40
    iget-object v0, p0, Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilderFactory;->builders:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    return-void
.end method

.method public getQuery(Lorg/w3c/dom/Element;)Lorg/apache/lucene/search/Query;
    .locals 1
    .param p1, "e"    # Lorg/w3c/dom/Element;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/xml/ParserException;
        }
    .end annotation

    .prologue
    .line 36
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilderFactory;->getSpanQuery(Lorg/w3c/dom/Element;)Lorg/apache/lucene/search/spans/SpanQuery;

    move-result-object v0

    return-object v0
.end method

.method public getSpanQuery(Lorg/w3c/dom/Element;)Lorg/apache/lucene/search/spans/SpanQuery;
    .locals 4
    .param p1, "e"    # Lorg/w3c/dom/Element;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/xml/ParserException;
        }
    .end annotation

    .prologue
    .line 45
    iget-object v1, p0, Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilderFactory;->builders:Ljava/util/Map;

    invoke-interface {p1}, Lorg/w3c/dom/Element;->getNodeName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilder;

    .line 46
    .local v0, "builder":Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilder;
    if-nez v0, :cond_0

    .line 47
    new-instance v1, Lorg/apache/lucene/queryparser/xml/ParserException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No SpanQueryObjectBuilder defined for node "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lorg/w3c/dom/Element;->getNodeName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/lucene/queryparser/xml/ParserException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 49
    :cond_0
    invoke-interface {v0, p1}, Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilder;->getSpanQuery(Lorg/w3c/dom/Element;)Lorg/apache/lucene/search/spans/SpanQuery;

    move-result-object v1

    return-object v1
.end method
