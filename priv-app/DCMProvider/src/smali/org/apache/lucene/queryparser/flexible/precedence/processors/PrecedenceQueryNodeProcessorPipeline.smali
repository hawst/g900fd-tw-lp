.class public Lorg/apache/lucene/queryparser/flexible/precedence/processors/PrecedenceQueryNodeProcessorPipeline;
.super Lorg/apache/lucene/queryparser/flexible/standard/processors/StandardQueryNodeProcessorPipeline;
.source "PrecedenceQueryNodeProcessorPipeline.java"


# direct methods
.method public constructor <init>(Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;)V
    .locals 4
    .param p1, "queryConfig"    # Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lorg/apache/lucene/queryparser/flexible/standard/processors/StandardQueryNodeProcessorPipeline;-><init>(Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;)V

    .line 48
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/precedence/processors/PrecedenceQueryNodeProcessorPipeline;->size()I

    move-result v2

    if-lt v0, v2, :cond_0

    .line 56
    new-instance v2, Lorg/apache/lucene/queryparser/flexible/precedence/processors/BooleanModifiersQueryNodeProcessor;

    invoke-direct {v2}, Lorg/apache/lucene/queryparser/flexible/precedence/processors/BooleanModifiersQueryNodeProcessor;-><init>()V

    invoke-virtual {p0, v2}, Lorg/apache/lucene/queryparser/flexible/precedence/processors/PrecedenceQueryNodeProcessorPipeline;->add(Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;)Z

    .line 58
    return-void

    .line 50
    :cond_0
    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/flexible/precedence/processors/PrecedenceQueryNodeProcessorPipeline;->get(I)Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-class v3, Lorg/apache/lucene/queryparser/flexible/standard/processors/BooleanQuery2ModifierNodeProcessor;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 51
    add-int/lit8 v1, v0, -0x1

    .end local v0    # "i":I
    .local v1, "i":I
    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/flexible/precedence/processors/PrecedenceQueryNodeProcessorPipeline;->remove(I)Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;

    move v0, v1

    .line 48
    .end local v1    # "i":I
    .restart local v0    # "i":I
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
