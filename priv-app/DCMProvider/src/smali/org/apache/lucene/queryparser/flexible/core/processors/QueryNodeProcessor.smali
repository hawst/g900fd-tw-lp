.class public interface abstract Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;
.super Ljava/lang/Object;
.source "QueryNodeProcessor.java"


# virtual methods
.method public abstract getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;
.end method

.method public abstract process(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation
.end method

.method public abstract setQueryConfigHandler(Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;)V
.end method
