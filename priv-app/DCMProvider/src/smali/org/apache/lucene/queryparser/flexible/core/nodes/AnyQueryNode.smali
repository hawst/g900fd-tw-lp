.class public Lorg/apache/lucene/queryparser/flexible/core/nodes/AnyQueryNode;
.super Lorg/apache/lucene/queryparser/flexible/core/nodes/AndQueryNode;
.source "AnyQueryNode.java"


# instance fields
.field private field:Ljava/lang/CharSequence;

.field private minimumMatchingmElements:I


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/lang/CharSequence;I)V
    .locals 4
    .param p2, "field"    # Ljava/lang/CharSequence;
    .param p3, "minimumMatchingElements"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;",
            ">;",
            "Ljava/lang/CharSequence;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 39
    .local p1, "clauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/AndQueryNode;-><init>(Ljava/util/List;)V

    .line 30
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/AnyQueryNode;->field:Ljava/lang/CharSequence;

    .line 31
    const/4 v1, 0x0

    iput v1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/AnyQueryNode;->minimumMatchingmElements:I

    .line 40
    iput-object p2, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/AnyQueryNode;->field:Ljava/lang/CharSequence;

    .line 41
    iput p3, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/AnyQueryNode;->minimumMatchingmElements:I

    .line 43
    if-eqz p1, :cond_1

    .line 45
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_2

    .line 62
    :cond_1
    return-void

    .line 45
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    .line 47
    .local v0, "clause":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    instance-of v1, v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;

    if-eqz v1, :cond_0

    .line 49
    instance-of v1, v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;

    if-eqz v1, :cond_3

    move-object v1, v0

    .line 50
    check-cast v1, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;

    const/4 v3, 0x1

    iput-boolean v3, v1, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;->toQueryStringIgnoreFields:Z

    .line 53
    :cond_3
    instance-of v1, v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldableNode;

    if-eqz v1, :cond_0

    .line 54
    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldableNode;

    .end local v0    # "clause":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    invoke-interface {v0, p2}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldableNode;->setField(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public cloneTree()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 99
    invoke-super {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/AndQueryNode;->cloneTree()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/AnyQueryNode;

    .line 101
    .local v0, "clone":Lorg/apache/lucene/queryparser/flexible/core/nodes/AnyQueryNode;
    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/AnyQueryNode;->field:Ljava/lang/CharSequence;

    iput-object v1, v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/AnyQueryNode;->field:Ljava/lang/CharSequence;

    .line 102
    iget v1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/AnyQueryNode;->minimumMatchingmElements:I

    iput v1, v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/AnyQueryNode;->minimumMatchingmElements:I

    .line 104
    return-object v0
.end method

.method public getField()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/AnyQueryNode;->field:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getFieldAsString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/AnyQueryNode;->field:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    .line 84
    const/4 v0, 0x0

    .line 86
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/AnyQueryNode;->field:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getMinimumMatchingElements()I
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/AnyQueryNode;->minimumMatchingmElements:I

    return v0
.end method

.method public setField(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "field"    # Ljava/lang/CharSequence;

    .prologue
    .line 94
    iput-object p1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/AnyQueryNode;->field:Ljava/lang/CharSequence;

    .line 95
    return-void
.end method

.method public toQueryString(Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;)Ljava/lang/CharSequence;
    .locals 7
    .param p1, "escapeSyntaxParser"    # Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;

    .prologue
    .line 125
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "ANY "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/AnyQueryNode;->minimumMatchingmElements:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 127
    .local v0, "anySTR":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 128
    .local v3, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/AnyQueryNode;->getChildren()Ljava/util/List;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/AnyQueryNode;->getChildren()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-eqz v4, :cond_0

    .line 131
    const-string v2, ""

    .line 132
    .local v2, "filler":Ljava/lang/String;
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/AnyQueryNode;->getChildren()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_1

    .line 138
    .end local v2    # "filler":Ljava/lang/String;
    :cond_0
    iget-object v4, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/AnyQueryNode;->field:Ljava/lang/CharSequence;

    invoke-virtual {p0, v4}, Lorg/apache/lucene/queryparser/flexible/core/nodes/AnyQueryNode;->isDefaultField(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 139
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "( "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ) "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 141
    :goto_1
    return-object v4

    .line 132
    .restart local v2    # "filler":Ljava/lang/String;
    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    .line 133
    .local v1, "clause":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v1, p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->toQueryString(Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 134
    const-string v2, " "

    goto :goto_0

    .line 141
    .end local v1    # "clause":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .end local v2    # "filler":Ljava/lang/String;
    :cond_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/AnyQueryNode;->field:Ljava/lang/CharSequence;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":(( "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ) "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 109
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/AnyQueryNode;->getChildren()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/AnyQueryNode;->getChildren()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_1

    .line 110
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "<any field=\'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/AnyQueryNode;->field:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'  matchelements="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 111
    iget v3, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/AnyQueryNode;->minimumMatchingmElements:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 110
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 120
    :goto_0
    return-object v2

    .line 112
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 113
    .local v1, "sb":Ljava/lang/StringBuilder;
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "<any field=\'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/AnyQueryNode;->field:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'  matchelements="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 114
    iget v3, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/AnyQueryNode;->minimumMatchingmElements:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ">"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 113
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 115
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/AnyQueryNode;->getChildren()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 119
    const-string v2, "\n</any>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 120
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 115
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    .line 116
    .local v0, "clause":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    const-string v3, "\n"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 117
    invoke-interface {v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method
