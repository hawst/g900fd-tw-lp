.class public Lorg/apache/lucene/queryparser/flexible/standard/processors/BooleanQuery2ModifierNodeProcessor;
.super Ljava/lang/Object;
.source "BooleanQuery2ModifierNodeProcessor.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;


# static fields
.field static final TAG_BOOLEAN_ROOT:Ljava/lang/String; = "booleanRoot"

.field static final TAG_MODIFIER:Ljava/lang/String; = "wrapWithModifier"

.field static final TAG_REMOVE:Ljava/lang/String; = "remove"


# instance fields
.field private final childrenBuffer:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;",
            ">;"
        }
    .end annotation
.end field

.field queryConfigHandler:Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

.field private usingAnd:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/processors/BooleanQuery2ModifierNodeProcessor;->childrenBuffer:Ljava/util/ArrayList;

    .line 73
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/processors/BooleanQuery2ModifierNodeProcessor;->usingAnd:Ljava/lang/Boolean;

    .line 77
    return-void
.end method

.method private applyModifier(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 3
    .param p1, "node"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .param p2, "mod"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    .prologue
    .line 163
    instance-of v1, p1, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;

    if-nez v1, :cond_1

    .line 164
    new-instance v1, Lorg/apache/lucene/queryparser/flexible/standard/nodes/BooleanModifierNode;

    invoke-direct {v1, p1, p2}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/BooleanModifierNode;-><init>(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;)V

    move-object p1, v1

    .line 175
    .end local p1    # "node":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    :cond_0
    :goto_0
    return-object p1

    .restart local p1    # "node":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    :cond_1
    move-object v0, p1

    .line 167
    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;

    .line 169
    .local v0, "modNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;
    invoke-virtual {v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;->getModifier()Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    move-result-object v1

    sget-object v2, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;->MOD_NONE:Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    if-ne v1, v2, :cond_0

    .line 170
    new-instance p1, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;

    .end local p1    # "node":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    invoke-virtual {v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;->getChild()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v1

    invoke-direct {p1, v1, p2}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;-><init>(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;)V

    goto :goto_0
.end method

.method private processIteration(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 0
    .param p1, "queryTree"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 106
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/standard/processors/BooleanQuery2ModifierNodeProcessor;->preProcessNode(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object p1

    .line 108
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/standard/processors/BooleanQuery2ModifierNodeProcessor;->processChildren(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)V

    .line 110
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/standard/processors/BooleanQuery2ModifierNodeProcessor;->postProcessNode(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object p1

    .line 112
    return-object p1
.end method


# virtual methods
.method protected fillChildrenBufferAndApplyModifiery(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)V
    .locals 4
    .param p1, "parent"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    .prologue
    .line 117
    invoke-interface {p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->getChildren()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 127
    return-void

    .line 117
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    .line 118
    .local v0, "node":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    const-string v1, "remove"

    invoke-interface {v0, v1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->containsTag(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 119
    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/flexible/standard/processors/BooleanQuery2ModifierNodeProcessor;->fillChildrenBufferAndApplyModifiery(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)V

    goto :goto_0

    .line 120
    :cond_1
    const-string/jumbo v1, "wrapWithModifier"

    invoke-interface {v0, v1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->containsTag(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 121
    iget-object v3, p0, Lorg/apache/lucene/queryparser/flexible/standard/processors/BooleanQuery2ModifierNodeProcessor;->childrenBuffer:Ljava/util/ArrayList;

    .line 122
    const-string/jumbo v1, "wrapWithModifier"

    invoke-interface {v0, v1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->getTag(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    .line 121
    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/queryparser/flexible/standard/processors/BooleanQuery2ModifierNodeProcessor;->applyModifier(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 124
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/processors/BooleanQuery2ModifierNodeProcessor;->childrenBuffer:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/processors/BooleanQuery2ModifierNodeProcessor;->queryConfigHandler:Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    return-object v0
.end method

.method protected isDefaultBooleanQueryNode(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Z
    .locals 2
    .param p1, "toTest"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    .prologue
    .line 157
    if-eqz p1, :cond_0

    const-class v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/BooleanQueryNode;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected postProcessNode(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 1
    .param p1, "node"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 130
    const-string v0, "booleanRoot"

    invoke-interface {p1, v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->containsTag(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 131
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/processors/BooleanQuery2ModifierNodeProcessor;->childrenBuffer:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 132
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/standard/processors/BooleanQuery2ModifierNodeProcessor;->fillChildrenBufferAndApplyModifiery(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)V

    .line 133
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/processors/BooleanQuery2ModifierNodeProcessor;->childrenBuffer:Ljava/util/ArrayList;

    invoke-interface {p1, v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->set(Ljava/util/List;)V

    .line 135
    :cond_0
    return-object p1
.end method

.method protected preProcessNode(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 3
    .param p1, "node"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 140
    invoke-interface {p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->getParent()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v0

    .line 141
    .local v0, "parent":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    instance-of v1, p1, Lorg/apache/lucene/queryparser/flexible/core/nodes/BooleanQueryNode;

    if-eqz v1, :cond_2

    .line 142
    instance-of v1, v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/BooleanQueryNode;

    if-eqz v1, :cond_1

    .line 143
    const-string v1, "remove"

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-interface {p1, v1, v2}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->setTag(Ljava/lang/String;Ljava/lang/Object;)V

    .line 153
    :cond_0
    :goto_0
    return-object p1

    .line 145
    :cond_1
    const-string v1, "booleanRoot"

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-interface {p1, v1, v2}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->setTag(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 147
    :cond_2
    instance-of v1, v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/BooleanQueryNode;

    if-eqz v1, :cond_0

    .line 148
    instance-of v1, v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/AndQueryNode;

    if-nez v1, :cond_3

    .line 149
    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/processors/BooleanQuery2ModifierNodeProcessor;->usingAnd:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/flexible/standard/processors/BooleanQuery2ModifierNodeProcessor;->isDefaultBooleanQueryNode(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 150
    :cond_3
    sget-object v1, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;->MOD_REQ:Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    invoke-virtual {p0, p1, v1}, Lorg/apache/lucene/queryparser/flexible/standard/processors/BooleanQuery2ModifierNodeProcessor;->tagModifierButDoNotOverride(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;)V

    goto :goto_0
.end method

.method public process(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 3
    .param p1, "queryTree"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 81
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/processors/BooleanQuery2ModifierNodeProcessor;->getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    move-result-object v1

    .line 82
    sget-object v2, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->DEFAULT_OPERATOR:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    .line 81
    invoke-virtual {v1, v2}, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->get(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$Operator;

    .line 84
    .local v0, "op":Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$Operator;
    if-nez v0, :cond_0

    .line 85
    new-instance v1, Ljava/lang/IllegalArgumentException;

    .line 86
    const-string v2, "StandardQueryConfigHandler.ConfigurationKeys.DEFAULT_OPERATOR should be set on the QueryConfigHandler"

    .line 85
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 89
    :cond_0
    sget-object v1, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$Operator;->AND:Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$Operator;

    if-ne v1, v0, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/processors/BooleanQuery2ModifierNodeProcessor;->usingAnd:Ljava/lang/Boolean;

    .line 91
    invoke-direct {p0, p1}, Lorg/apache/lucene/queryparser/flexible/standard/processors/BooleanQuery2ModifierNodeProcessor;->processIteration(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v1

    return-object v1

    .line 89
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected processChildren(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)V
    .locals 4
    .param p1, "queryTree"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 96
    invoke-interface {p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->getChildren()Ljava/util/List;

    move-result-object v1

    .line 97
    .local v1, "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 98
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 102
    :cond_0
    return-void

    .line 98
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    .line 99
    .local v0, "child":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    invoke-direct {p0, v0}, Lorg/apache/lucene/queryparser/flexible/standard/processors/BooleanQuery2ModifierNodeProcessor;->processIteration(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    goto :goto_0
.end method

.method public setQueryConfigHandler(Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;)V
    .locals 0
    .param p1, "queryConfigHandler"    # Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    .prologue
    .line 192
    iput-object p1, p0, Lorg/apache/lucene/queryparser/flexible/standard/processors/BooleanQuery2ModifierNodeProcessor;->queryConfigHandler:Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    .line 194
    return-void
.end method

.method protected tagModifierButDoNotOverride(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;)V
    .locals 3
    .param p1, "node"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .param p2, "mod"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    .prologue
    .line 180
    instance-of v1, p1, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;

    if-eqz v1, :cond_1

    move-object v0, p1

    .line 181
    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;

    .line 182
    .local v0, "modNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;
    invoke-virtual {v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;->getModifier()Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    move-result-object v1

    sget-object v2, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;->MOD_NONE:Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    if-ne v1, v2, :cond_0

    .line 183
    const-string/jumbo v1, "wrapWithModifier"

    invoke-interface {p1, v1, p2}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->setTag(Ljava/lang/String;Ljava/lang/Object;)V

    .line 188
    .end local v0    # "modNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;
    :cond_0
    :goto_0
    return-void

    .line 186
    :cond_1
    const-string/jumbo v1, "wrapWithModifier"

    sget-object v2, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;->MOD_REQ:Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    invoke-interface {p1, v1, v2}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->setTag(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method
