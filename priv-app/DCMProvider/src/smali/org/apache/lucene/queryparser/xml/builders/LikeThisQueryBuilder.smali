.class public Lorg/apache/lucene/queryparser/xml/builders/LikeThisQueryBuilder;
.super Ljava/lang/Object;
.source "LikeThisQueryBuilder.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/xml/QueryBuilder;


# static fields
.field private static final DEFAULT_MAX_QUERY_TERMS:I = 0x14

.field private static final DEFAULT_MIN_TERM_FREQUENCY:I = 0x1

.field private static final DEFAULT_PERCENT_TERMS_TO_MATCH:F = 30.0f


# instance fields
.field private final analyzer:Lorg/apache/lucene/analysis/Analyzer;

.field private final defaultFieldNames:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/Analyzer;[Ljava/lang/String;)V
    .locals 0
    .param p1, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;
    .param p2, "defaultFieldNames"    # [Ljava/lang/String;

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lorg/apache/lucene/queryparser/xml/builders/LikeThisQueryBuilder;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    .line 51
    iput-object p2, p0, Lorg/apache/lucene/queryparser/xml/builders/LikeThisQueryBuilder;->defaultFieldNames:[Ljava/lang/String;

    .line 52
    return-void
.end method


# virtual methods
.method public getQuery(Lorg/w3c/dom/Element;)Lorg/apache/lucene/search/Query;
    .locals 16
    .param p1, "e"    # Lorg/w3c/dom/Element;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/xml/ParserException;
        }
    .end annotation

    .prologue
    .line 59
    const-string v12, "fieldNames"

    move-object/from16 v0, p1

    invoke-interface {v0, v12}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 60
    .local v3, "fieldsList":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/queryparser/xml/builders/LikeThisQueryBuilder;->defaultFieldNames:[Ljava/lang/String;

    .line 61
    .local v2, "fields":[Ljava/lang/String;
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    if-lez v12, :cond_0

    .line 62
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v12

    const-string v13, ","

    invoke-virtual {v12, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 64
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    array-length v12, v2

    if-lt v4, v12, :cond_3

    .line 72
    .end local v4    # "i":I
    :cond_0
    const-string v12, "stopWords"

    move-object/from16 v0, p1

    invoke-interface {v0, v12}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 73
    .local v8, "stopWords":Ljava/lang/String;
    const/4 v9, 0x0

    .line 74
    .local v9, "stopWordsSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz v8, :cond_1

    if-eqz v2, :cond_1

    .line 75
    new-instance v9, Ljava/util/HashSet;

    .end local v9    # "stopWordsSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-direct {v9}, Ljava/util/HashSet;-><init>()V

    .line 76
    .restart local v9    # "stopWordsSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    array-length v13, v2

    const/4 v12, 0x0

    :goto_1
    if-lt v12, v13, :cond_4

    .line 94
    :cond_1
    new-instance v7, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;

    invoke-static/range {p1 .. p1}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getText(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/queryparser/xml/builders/LikeThisQueryBuilder;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    const/4 v14, 0x0

    aget-object v14, v2, v14

    invoke-direct {v7, v12, v2, v13, v14}, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;-><init>(Ljava/lang/String;[Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer;Ljava/lang/String;)V

    .line 95
    .local v7, "mlt":Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;
    const-string v12, "maxQueryTerms"

    const/16 v13, 0x14

    move-object/from16 v0, p1

    invoke-static {v0, v12, v13}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttribute(Lorg/w3c/dom/Element;Ljava/lang/String;I)I

    move-result v12

    invoke-virtual {v7, v12}, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->setMaxQueryTerms(I)V

    .line 96
    const-string v12, "minTermFrequency"

    const/4 v13, 0x1

    move-object/from16 v0, p1

    invoke-static {v0, v12, v13}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttribute(Lorg/w3c/dom/Element;Ljava/lang/String;I)I

    move-result v12

    invoke-virtual {v7, v12}, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->setMinTermFrequency(I)V

    .line 97
    const-string v12, "percentTermsToMatch"

    const/high16 v13, 0x41f00000    # 30.0f

    move-object/from16 v0, p1

    invoke-static {v0, v12, v13}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttribute(Lorg/w3c/dom/Element;Ljava/lang/String;F)F

    move-result v12

    const/high16 v13, 0x42c80000    # 100.0f

    div-float/2addr v12, v13

    invoke-virtual {v7, v12}, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->setPercentTermsToMatch(F)V

    .line 98
    invoke-virtual {v7, v9}, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->setStopWords(Ljava/util/Set;)V

    .line 99
    const-string v12, "minDocFreq"

    const/4 v13, -0x1

    move-object/from16 v0, p1

    invoke-static {v0, v12, v13}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttribute(Lorg/w3c/dom/Element;Ljava/lang/String;I)I

    move-result v6

    .line 100
    .local v6, "minDocFreq":I
    if-ltz v6, :cond_2

    .line 101
    invoke-virtual {v7, v6}, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->setMinDocFreq(I)V

    .line 104
    :cond_2
    const-string v12, "boost"

    const/high16 v13, 0x3f800000    # 1.0f

    move-object/from16 v0, p1

    invoke-static {v0, v12, v13}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttribute(Lorg/w3c/dom/Element;Ljava/lang/String;F)F

    move-result v12

    invoke-virtual {v7, v12}, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->setBoost(F)V

    .line 106
    return-object v7

    .line 65
    .end local v6    # "minDocFreq":I
    .end local v7    # "mlt":Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;
    .end local v8    # "stopWords":Ljava/lang/String;
    .end local v9    # "stopWordsSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .restart local v4    # "i":I
    :cond_3
    aget-object v12, v2, v4

    invoke-virtual {v12}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v2, v4

    .line 64
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 76
    .end local v4    # "i":I
    .restart local v8    # "stopWords":Ljava/lang/String;
    .restart local v9    # "stopWordsSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_4
    aget-object v1, v2, v12

    .line 78
    .local v1, "field":Ljava/lang/String;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/queryparser/xml/builders/LikeThisQueryBuilder;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    new-instance v15, Ljava/io/StringReader;

    invoke-direct {v15, v8}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-virtual {v14, v1, v15}, Lorg/apache/lucene/analysis/Analyzer;->tokenStream(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/TokenStream;

    move-result-object v11

    .line 79
    .local v11, "ts":Lorg/apache/lucene/analysis/TokenStream;
    const-class v14, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {v11, v14}, Lorg/apache/lucene/analysis/TokenStream;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v10

    check-cast v10, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 80
    .local v10, "termAtt":Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
    invoke-virtual {v11}, Lorg/apache/lucene/analysis/TokenStream;->reset()V

    .line 81
    :goto_2
    invoke-virtual {v11}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v14

    if-nez v14, :cond_5

    .line 84
    invoke-virtual {v11}, Lorg/apache/lucene/analysis/TokenStream;->end()V

    .line 85
    invoke-virtual {v11}, Lorg/apache/lucene/analysis/TokenStream;->close()V

    .line 76
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_1

    .line 82
    :cond_5
    invoke-interface {v10}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-interface {v9, v14}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 86
    .end local v10    # "termAtt":Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
    .end local v11    # "ts":Lorg/apache/lucene/analysis/TokenStream;
    :catch_0
    move-exception v5

    .line 87
    .local v5, "ioe":Ljava/io/IOException;
    new-instance v12, Lorg/apache/lucene/queryparser/xml/ParserException;

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "IoException parsing stop words list in "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 88
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ":"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v5}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 87
    invoke-direct {v12, v13}, Lorg/apache/lucene/queryparser/xml/ParserException;-><init>(Ljava/lang/String;)V

    throw v12
.end method
