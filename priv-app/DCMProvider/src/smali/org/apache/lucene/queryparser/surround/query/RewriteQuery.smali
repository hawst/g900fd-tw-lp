.class abstract Lorg/apache/lucene/queryparser/surround/query/RewriteQuery;
.super Lorg/apache/lucene/search/Query;
.source "RewriteQuery.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<SQ:",
        "Lorg/apache/lucene/queryparser/surround/query/SrndQuery;",
        ">",
        "Lorg/apache/lucene/search/Query;"
    }
.end annotation


# instance fields
.field protected final fieldName:Ljava/lang/String;

.field protected final qf:Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;

.field protected final srndQuery:Lorg/apache/lucene/queryparser/surround/query/SrndQuery;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TSQ;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lorg/apache/lucene/queryparser/surround/query/SrndQuery;Ljava/lang/String;Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;)V
    .locals 0
    .param p2, "fieldName"    # Ljava/lang/String;
    .param p3, "qf"    # Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TSQ;",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;",
            ")V"
        }
    .end annotation

    .prologue
    .line 28
    .local p0, "this":Lorg/apache/lucene/queryparser/surround/query/RewriteQuery;, "Lorg/apache/lucene/queryparser/surround/query/RewriteQuery<TSQ;>;"
    .local p1, "srndQuery":Lorg/apache/lucene/queryparser/surround/query/SrndQuery;, "TSQ;"
    invoke-direct {p0}, Lorg/apache/lucene/search/Query;-><init>()V

    .line 32
    iput-object p1, p0, Lorg/apache/lucene/queryparser/surround/query/RewriteQuery;->srndQuery:Lorg/apache/lucene/queryparser/surround/query/SrndQuery;

    .line 33
    iput-object p2, p0, Lorg/apache/lucene/queryparser/surround/query/RewriteQuery;->fieldName:Ljava/lang/String;

    .line 34
    iput-object p3, p0, Lorg/apache/lucene/queryparser/surround/query/RewriteQuery;->qf:Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;

    .line 35
    return-void
.end method


# virtual methods
.method public clone()Lorg/apache/lucene/queryparser/surround/query/RewriteQuery;
    .locals 1

    .prologue
    .line 80
    .local p0, "this":Lorg/apache/lucene/queryparser/surround/query/RewriteQuery;, "Lorg/apache/lucene/queryparser/surround/query/RewriteQuery<TSQ;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public bridge synthetic clone()Lorg/apache/lucene/search/Query;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/query/RewriteQuery;->clone()Lorg/apache/lucene/queryparser/surround/query/RewriteQuery;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .local p0, "this":Lorg/apache/lucene/queryparser/surround/query/RewriteQuery;, "Lorg/apache/lucene/queryparser/surround/query/RewriteQuery<TSQ;>;"
    const/4 v1, 0x0

    .line 65
    if-nez p1, :cond_1

    .line 70
    :cond_0
    :goto_0
    return v1

    .line 67
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 69
    check-cast v0, Lorg/apache/lucene/queryparser/surround/query/RewriteQuery;

    .line 70
    .local v0, "other":Lorg/apache/lucene/queryparser/surround/query/RewriteQuery;
    iget-object v2, p0, Lorg/apache/lucene/queryparser/surround/query/RewriteQuery;->fieldName:Ljava/lang/String;

    iget-object v3, v0, Lorg/apache/lucene/queryparser/surround/query/RewriteQuery;->fieldName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 71
    iget-object v2, p0, Lorg/apache/lucene/queryparser/surround/query/RewriteQuery;->qf:Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;

    iget-object v3, v0, Lorg/apache/lucene/queryparser/surround/query/RewriteQuery;->qf:Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 72
    iget-object v2, p0, Lorg/apache/lucene/queryparser/surround/query/RewriteQuery;->srndQuery:Lorg/apache/lucene/queryparser/surround/query/SrndQuery;

    iget-object v3, v0, Lorg/apache/lucene/queryparser/surround/query/RewriteQuery;->srndQuery:Lorg/apache/lucene/queryparser/surround/query/SrndQuery;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/queryparser/surround/query/SrndQuery;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 70
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 57
    .local p0, "this":Lorg/apache/lucene/queryparser/surround/query/RewriteQuery;, "Lorg/apache/lucene/queryparser/surround/query/RewriteQuery<TSQ;>;"
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    .line 58
    iget-object v1, p0, Lorg/apache/lucene/queryparser/surround/query/RewriteQuery;->fieldName:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    .line 57
    xor-int/2addr v0, v1

    .line 59
    iget-object v1, p0, Lorg/apache/lucene/queryparser/surround/query/RewriteQuery;->qf:Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;

    invoke-virtual {v1}, Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;->hashCode()I

    move-result v1

    .line 57
    xor-int/2addr v0, v1

    .line 60
    iget-object v1, p0, Lorg/apache/lucene/queryparser/surround/query/RewriteQuery;->srndQuery:Lorg/apache/lucene/queryparser/surround/query/SrndQuery;

    invoke-virtual {v1}, Lorg/apache/lucene/queryparser/surround/query/SrndQuery;->hashCode()I

    move-result v1

    .line 57
    xor-int/2addr v0, v1

    return v0
.end method

.method public abstract rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    .local p0, "this":Lorg/apache/lucene/queryparser/surround/query/RewriteQuery;, "Lorg/apache/lucene/queryparser/surround/query/RewriteQuery<TSQ;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/surround/query/RewriteQuery;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 47
    .local p0, "this":Lorg/apache/lucene/queryparser/surround/query/RewriteQuery;, "Lorg/apache/lucene/queryparser/surround/query/RewriteQuery<TSQ;>;"
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 48
    if-nez p1, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 49
    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/queryparser/surround/query/RewriteQuery;->fieldName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 50
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/queryparser/surround/query/RewriteQuery;->srndQuery:Lorg/apache/lucene/queryparser/surround/query/SrndQuery;

    invoke-virtual {v1}, Lorg/apache/lucene/queryparser/surround/query/SrndQuery;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 51
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/queryparser/surround/query/RewriteQuery;->qf:Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;

    invoke-virtual {v1}, Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 52
    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 47
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 48
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "(unused: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
