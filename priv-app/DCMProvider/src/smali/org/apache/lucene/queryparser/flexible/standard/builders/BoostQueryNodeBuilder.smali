.class public Lorg/apache/lucene/queryparser/flexible/standard/builders/BoostQueryNodeBuilder;
.super Ljava/lang/Object;
.source "BoostQueryNodeBuilder.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/flexible/standard/builders/StandardQueryBuilder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    return-void
.end method


# virtual methods
.method public bridge synthetic build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/BoostQueryNodeBuilder;->build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/search/Query;
    .locals 4
    .param p1, "queryNode"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 40
    move-object v0, p1

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/BoostQueryNode;

    .line 41
    .local v0, "boostNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/BoostQueryNode;
    invoke-virtual {v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/BoostQueryNode;->getChild()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v1

    .line 43
    .local v1, "child":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    if-nez v1, :cond_0

    .line 44
    const/4 v2, 0x0

    .line 51
    :goto_0
    return-object v2

    .line 48
    :cond_0
    sget-object v3, Lorg/apache/lucene/queryparser/flexible/core/builders/QueryTreeBuilder;->QUERY_TREE_BUILDER_TAGID:Ljava/lang/String;

    invoke-interface {v1, v3}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->getTag(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .line 47
    check-cast v2, Lorg/apache/lucene/search/Query;

    .line 49
    .local v2, "query":Lorg/apache/lucene/search/Query;
    invoke-virtual {v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/BoostQueryNode;->getValue()F

    move-result v3

    invoke-virtual {v2, v3}, Lorg/apache/lucene/search/Query;->setBoost(F)V

    goto :goto_0
.end method
