.class public Lorg/apache/lucene/queryparser/flexible/standard/processors/LowercaseExpandedTermsQueryNodeProcessor;
.super Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl;
.source "LowercaseExpandedTermsQueryNodeProcessor.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl;-><init>()V

    .line 50
    return-void
.end method


# virtual methods
.method protected postProcessNode(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 5
    .param p1, "node"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 67
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/processors/LowercaseExpandedTermsQueryNodeProcessor;->getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    move-result-object v3

    sget-object v4, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->LOCALE:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->get(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    .line 68
    .local v0, "locale":Ljava/util/Locale;
    if-nez v0, :cond_0

    .line 69
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    .line 72
    :cond_0
    instance-of v3, p1, Lorg/apache/lucene/queryparser/flexible/standard/nodes/WildcardQueryNode;

    if-nez v3, :cond_2

    .line 73
    instance-of v3, p1, Lorg/apache/lucene/queryparser/flexible/core/nodes/FuzzyQueryNode;

    if-nez v3, :cond_2

    .line 74
    instance-of v3, p1, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;

    if-eqz v3, :cond_1

    invoke-interface {p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->getParent()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v3

    instance-of v3, v3, Lorg/apache/lucene/queryparser/flexible/core/nodes/RangeQueryNode;

    if-nez v3, :cond_2

    .line 75
    :cond_1
    instance-of v3, p1, Lorg/apache/lucene/queryparser/flexible/standard/nodes/RegexpQueryNode;

    if-eqz v3, :cond_3

    :cond_2
    move-object v2, p1

    .line 77
    check-cast v2, Lorg/apache/lucene/queryparser/flexible/core/nodes/TextableQueryNode;

    .line 78
    .local v2, "txtNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/TextableQueryNode;
    invoke-interface {v2}, Lorg/apache/lucene/queryparser/flexible/core/nodes/TextableQueryNode;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    .line 79
    .local v1, "text":Ljava/lang/CharSequence;
    if-eqz v1, :cond_4

    invoke-static {v1, v0}, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;->toLowerCase(Ljava/lang/CharSequence;Ljava/util/Locale;)Ljava/lang/CharSequence;

    move-result-object v3

    :goto_0
    invoke-interface {v2, v3}, Lorg/apache/lucene/queryparser/flexible/core/nodes/TextableQueryNode;->setText(Ljava/lang/CharSequence;)V

    .line 82
    .end local v1    # "text":Ljava/lang/CharSequence;
    .end local v2    # "txtNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/TextableQueryNode;
    :cond_3
    return-object p1

    .line 79
    .restart local v1    # "text":Ljava/lang/CharSequence;
    .restart local v2    # "txtNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/TextableQueryNode;
    :cond_4
    const/4 v3, 0x0

    goto :goto_0
.end method

.method protected preProcessNode(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 0
    .param p1, "node"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 89
    return-object p1
.end method

.method public process(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 3
    .param p1, "queryTree"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 54
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/processors/LowercaseExpandedTermsQueryNodeProcessor;->getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    move-result-object v1

    sget-object v2, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->LOWERCASE_EXPANDED_TERMS:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->get(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 56
    .local v0, "lowercaseExpandedTerms":Ljava/lang/Boolean;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 57
    invoke-super {p0, p1}, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl;->process(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object p1

    .line 60
    .end local p1    # "queryTree":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    :cond_0
    return-object p1
.end method

.method protected setChildrenOrder(Ljava/util/List;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 97
    .local p1, "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    return-object p1
.end method
