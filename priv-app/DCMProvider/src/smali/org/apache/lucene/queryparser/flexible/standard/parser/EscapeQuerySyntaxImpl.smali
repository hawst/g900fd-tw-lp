.class public Lorg/apache/lucene/queryparser/flexible/standard/parser/EscapeQuerySyntaxImpl;
.super Ljava/lang/Object;
.source "EscapeQuerySyntaxImpl.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;


# static fields
.field private static final escapableQuotedChars:[Ljava/lang/String;

.field private static final escapableTermChars:[Ljava/lang/String;

.field private static final escapableTermExtraFirstChars:[Ljava/lang/String;

.field private static final escapableWhiteChars:[Ljava/lang/String;

.field private static final escapableWordTokens:[Ljava/lang/String;

.field private static final wildcardChars:[C


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 33
    new-array v0, v5, [C

    fill-array-data v0, :array_0

    sput-object v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/EscapeQuerySyntaxImpl;->wildcardChars:[C

    .line 35
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "+"

    aput-object v1, v0, v3

    const-string v1, "-"

    aput-object v1, v0, v4

    const-string v1, "@"

    aput-object v1, v0, v5

    sput-object v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/EscapeQuerySyntaxImpl;->escapableTermExtraFirstChars:[Ljava/lang/String;

    .line 37
    const/16 v0, 0xf

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "\""

    aput-object v1, v0, v3

    const-string v1, "<"

    aput-object v1, v0, v4

    const-string v1, ">"

    aput-object v1, v0, v5

    const-string v1, "="

    aput-object v1, v0, v6

    .line 38
    const-string v1, "!"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "("

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, ")"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "^"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "["

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "{"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, ":"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "]"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "}"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "~"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "/"

    aput-object v2, v0, v1

    .line 37
    sput-object v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/EscapeQuerySyntaxImpl;->escapableTermChars:[Ljava/lang/String;

    .line 41
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "\""

    aput-object v1, v0, v3

    sput-object v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/EscapeQuerySyntaxImpl;->escapableQuotedChars:[Ljava/lang/String;

    .line 42
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, " "

    aput-object v1, v0, v3

    const-string v1, "\t"

    aput-object v1, v0, v4

    const-string v1, "\n"

    aput-object v1, v0, v5

    const-string v1, "\r"

    aput-object v1, v0, v6

    .line 43
    const-string v1, "\u000c"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "\u0008"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "\u3000"

    aput-object v2, v0, v1

    .line 42
    sput-object v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/EscapeQuerySyntaxImpl;->escapableWhiteChars:[Ljava/lang/String;

    .line 44
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "AND"

    aput-object v1, v0, v3

    const-string v1, "OR"

    aput-object v1, v0, v4

    const-string v1, "NOT"

    aput-object v1, v0, v5

    .line 45
    const-string v1, "TO"

    aput-object v1, v0, v6

    const-string v1, "WITHIN"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "SENTENCE"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "PARAGRAPH"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "INORDER"

    aput-object v2, v0, v1

    .line 44
    sput-object v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/EscapeQuerySyntaxImpl;->escapableWordTokens:[Ljava/lang/String;

    .line 45
    return-void

    .line 33
    :array_0
    .array-data 2
        0x2as
        0x3fs
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static discardEscapeChar(Ljava/lang/CharSequence;)Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;
    .locals 12
    .param p0, "input"    # Ljava/lang/CharSequence;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 222
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v9

    new-array v7, v9, [C

    .line 223
    .local v7, "output":[C
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v9

    new-array v8, v9, [Z

    .line 228
    .local v8, "wasEscaped":[Z
    const/4 v5, 0x0

    .line 232
    .local v5, "length":I
    const/4 v4, 0x0

    .line 237
    .local v4, "lastCharWasEscapeChar":Z
    const/4 v1, 0x0

    .line 240
    .local v1, "codePointMultiplier":I
    const/4 v0, 0x0

    .line 242
    .local v0, "codePoint":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v9

    if-lt v3, v9, :cond_0

    .line 272
    if-lez v1, :cond_6

    .line 273
    new-instance v9, Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException;

    new-instance v10, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;

    .line 274
    sget-object v11, Lorg/apache/lucene/queryparser/flexible/core/messages/QueryParserMessages;->INVALID_SYNTAX_ESCAPE_UNICODE_TRUNCATION:Ljava/lang/String;

    invoke-direct {v10, v11}, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;-><init>(Ljava/lang/String;)V

    .line 273
    invoke-direct {v9, v10}, Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException;-><init>(Lorg/apache/lucene/queryparser/flexible/messages/Message;)V

    throw v9

    .line 243
    :cond_0
    invoke-interface {p0, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    .line 244
    .local v2, "curChar":C
    if-lez v1, :cond_2

    .line 245
    invoke-static {v2}, Lorg/apache/lucene/queryparser/flexible/standard/parser/EscapeQuerySyntaxImpl;->hexToInt(C)I

    move-result v9

    mul-int/2addr v9, v1

    add-int/2addr v0, v9

    .line 246
    ushr-int/lit8 v1, v1, 0x4

    .line 247
    if-nez v1, :cond_1

    .line 248
    add-int/lit8 v6, v5, 0x1

    .end local v5    # "length":I
    .local v6, "length":I
    int-to-char v9, v0

    aput-char v9, v7, v5

    .line 249
    const/4 v0, 0x0

    move v5, v6

    .line 242
    .end local v6    # "length":I
    .restart local v5    # "length":I
    :cond_1
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 251
    :cond_2
    if-eqz v4, :cond_4

    .line 252
    const/16 v9, 0x75

    if-ne v2, v9, :cond_3

    .line 254
    const/16 v1, 0x1000

    .line 261
    :goto_2
    const/4 v4, 0x0

    .line 262
    goto :goto_1

    .line 257
    :cond_3
    aput-char v2, v7, v5

    .line 258
    const/4 v9, 0x1

    aput-boolean v9, v8, v5

    .line 259
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 263
    :cond_4
    const/16 v9, 0x5c

    if-ne v2, v9, :cond_5

    .line 264
    const/4 v4, 0x1

    .line 265
    goto :goto_1

    .line 266
    :cond_5
    aput-char v2, v7, v5

    .line 267
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 277
    .end local v2    # "curChar":C
    :cond_6
    if-eqz v4, :cond_7

    .line 278
    new-instance v9, Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException;

    new-instance v10, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;

    .line 279
    sget-object v11, Lorg/apache/lucene/queryparser/flexible/core/messages/QueryParserMessages;->INVALID_SYNTAX_ESCAPE_CHARACTER:Ljava/lang/String;

    invoke-direct {v10, v11}, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;-><init>(Ljava/lang/String;)V

    .line 278
    invoke-direct {v9, v10}, Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException;-><init>(Lorg/apache/lucene/queryparser/flexible/messages/Message;)V

    throw v9

    .line 282
    :cond_7
    new-instance v9, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;

    const/4 v10, 0x0

    invoke-direct {v9, v7, v8, v10, v5}, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;-><init>([C[ZII)V

    return-object v9
.end method

.method private static final escapeChar(Ljava/lang/CharSequence;Ljava/util/Locale;)Ljava/lang/CharSequence;
    .locals 5
    .param p0, "str"    # Ljava/lang/CharSequence;
    .param p1, "locale"    # Ljava/util/Locale;

    .prologue
    const/4 v4, 0x0

    .line 48
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-nez v2, :cond_2

    :cond_0
    move-object v0, p0

    .line 68
    :cond_1
    :goto_0
    return-object v0

    .line 51
    :cond_2
    move-object v0, p0

    .line 54
    .local v0, "buffer":Ljava/lang/CharSequence;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    sget-object v2, Lorg/apache/lucene/queryparser/flexible/standard/parser/EscapeQuerySyntaxImpl;->escapableTermChars:[Ljava/lang/String;

    array-length v2, v2

    if-lt v1, v2, :cond_3

    .line 60
    const/4 v1, 0x0

    :goto_2
    sget-object v2, Lorg/apache/lucene/queryparser/flexible/standard/parser/EscapeQuerySyntaxImpl;->escapableTermExtraFirstChars:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 61
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    sget-object v3, Lorg/apache/lucene/queryparser/flexible/standard/parser/EscapeQuerySyntaxImpl;->escapableTermExtraFirstChars:[Ljava/lang/String;

    aget-object v3, v3, v1

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-ne v2, v3, :cond_4

    .line 62
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "\\"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 63
    const/4 v3, 0x1

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v4

    invoke-interface {v0, v3, v4}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 62
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 64
    goto :goto_0

    .line 55
    :cond_3
    sget-object v2, Lorg/apache/lucene/queryparser/flexible/standard/parser/EscapeQuerySyntaxImpl;->escapableTermChars:[Ljava/lang/String;

    aget-object v2, v2, v1

    invoke-virtual {v2, p1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    .line 56
    const-string v3, "\\"

    .line 55
    invoke-static {v0, v2, v3, p1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/EscapeQuerySyntaxImpl;->replaceIgnoreCase(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/util/Locale;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 54
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 60
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method

.method private final escapeQuoted(Ljava/lang/CharSequence;Ljava/util/Locale;)Ljava/lang/CharSequence;
    .locals 4
    .param p1, "str"    # Ljava/lang/CharSequence;
    .param p2, "locale"    # Ljava/util/Locale;

    .prologue
    .line 72
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-nez v2, :cond_2

    :cond_0
    move-object v0, p1

    .line 81
    :cond_1
    return-object v0

    .line 75
    :cond_2
    move-object v0, p1

    .line 77
    .local v0, "buffer":Ljava/lang/CharSequence;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget-object v2, Lorg/apache/lucene/queryparser/flexible/standard/parser/EscapeQuerySyntaxImpl;->escapableQuotedChars:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 78
    sget-object v2, Lorg/apache/lucene/queryparser/flexible/standard/parser/EscapeQuerySyntaxImpl;->escapableTermChars:[Ljava/lang/String;

    aget-object v2, v2, v1

    invoke-virtual {v2, p2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    .line 79
    const-string v3, "\\"

    .line 78
    invoke-static {v0, v2, v3, p2}, Lorg/apache/lucene/queryparser/flexible/standard/parser/EscapeQuerySyntaxImpl;->replaceIgnoreCase(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/util/Locale;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 77
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private static final escapeTerm(Ljava/lang/CharSequence;Ljava/util/Locale;)Ljava/lang/CharSequence;
    .locals 3
    .param p0, "term"    # Ljava/lang/CharSequence;
    .param p1, "locale"    # Ljava/util/Locale;

    .prologue
    .line 85
    if-nez p0, :cond_1

    .line 97
    .end local p0    # "term":Ljava/lang/CharSequence;
    :cond_0
    :goto_0
    return-object p0

    .line 89
    .restart local p0    # "term":Ljava/lang/CharSequence;
    :cond_1
    invoke-static {p0, p1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/EscapeQuerySyntaxImpl;->escapeChar(Ljava/lang/CharSequence;Ljava/util/Locale;)Ljava/lang/CharSequence;

    move-result-object p0

    .line 90
    invoke-static {p0, p1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/EscapeQuerySyntaxImpl;->escapeWhiteChar(Ljava/lang/CharSequence;Ljava/util/Locale;)Ljava/lang/CharSequence;

    move-result-object p0

    .line 93
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    sget-object v1, Lorg/apache/lucene/queryparser/flexible/standard/parser/EscapeQuerySyntaxImpl;->escapableWordTokens:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 94
    sget-object v1, Lorg/apache/lucene/queryparser/flexible/standard/parser/EscapeQuerySyntaxImpl;->escapableWordTokens:[Ljava/lang/String;

    aget-object v1, v1, v0

    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 95
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "\\"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 93
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private static final escapeWhiteChar(Ljava/lang/CharSequence;Ljava/util/Locale;)Ljava/lang/CharSequence;
    .locals 4
    .param p0, "str"    # Ljava/lang/CharSequence;
    .param p1, "locale"    # Ljava/util/Locale;

    .prologue
    .line 177
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-nez v2, :cond_2

    :cond_0
    move-object v0, p0

    .line 186
    :cond_1
    return-object v0

    .line 180
    :cond_2
    move-object v0, p0

    .line 182
    .local v0, "buffer":Ljava/lang/CharSequence;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget-object v2, Lorg/apache/lucene/queryparser/flexible/standard/parser/EscapeQuerySyntaxImpl;->escapableWhiteChars:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 183
    sget-object v2, Lorg/apache/lucene/queryparser/flexible/standard/parser/EscapeQuerySyntaxImpl;->escapableWhiteChars:[Ljava/lang/String;

    aget-object v2, v2, v1

    invoke-virtual {v2, p1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    .line 184
    const-string v3, "\\"

    .line 183
    invoke-static {v0, v2, v3, p1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/EscapeQuerySyntaxImpl;->replaceIgnoreCase(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/util/Locale;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 182
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private static final hexToInt(C)I
    .locals 6
    .param p0, "c"    # C
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 287
    const/16 v0, 0x30

    if-gt v0, p0, :cond_0

    const/16 v0, 0x39

    if-gt p0, v0, :cond_0

    .line 288
    add-int/lit8 v0, p0, -0x30

    .line 292
    :goto_0
    return v0

    .line 289
    :cond_0
    const/16 v0, 0x61

    if-gt v0, p0, :cond_1

    const/16 v0, 0x66

    if-gt p0, v0, :cond_1

    .line 290
    add-int/lit8 v0, p0, -0x61

    add-int/lit8 v0, v0, 0xa

    goto :goto_0

    .line 291
    :cond_1
    const/16 v0, 0x41

    if-gt v0, p0, :cond_2

    const/16 v0, 0x46

    if-gt p0, v0, :cond_2

    .line 292
    add-int/lit8 v0, p0, -0x41

    add-int/lit8 v0, v0, 0xa

    goto :goto_0

    .line 294
    :cond_2
    new-instance v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException;

    new-instance v1, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;

    .line 295
    sget-object v2, Lorg/apache/lucene/queryparser/flexible/core/messages/QueryParserMessages;->INVALID_SYNTAX_ESCAPE_NONE_HEX_UNICODE:Ljava/lang/String;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-direct {v1, v2, v3}, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 294
    invoke-direct {v0, v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/ParseException;-><init>(Lorg/apache/lucene/queryparser/flexible/messages/Message;)V

    throw v0
.end method

.method private static replaceIgnoreCase(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/util/Locale;)Ljava/lang/CharSequence;
    .locals 11
    .param p0, "string"    # Ljava/lang/CharSequence;
    .param p1, "sequence1"    # Ljava/lang/CharSequence;
    .param p2, "escapeChar"    # Ljava/lang/CharSequence;
    .param p3, "locale"    # Ljava/util/Locale;

    .prologue
    .line 113
    if-eqz p2, :cond_0

    if-eqz p1, :cond_0

    if-nez p0, :cond_1

    .line 114
    :cond_0
    new-instance v9, Ljava/lang/NullPointerException;

    invoke-direct {v9}, Ljava/lang/NullPointerException;-><init>()V

    throw v9

    .line 117
    :cond_1
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    .line 118
    .local v1, "count":I
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v7

    .line 119
    .local v7, "sequence1Length":I
    if-nez v7, :cond_4

    .line 120
    new-instance v6, Ljava/lang/StringBuilder;

    add-int/lit8 v9, v1, 0x1

    .line 121
    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v10

    mul-int/2addr v9, v10

    .line 120
    invoke-direct {v6, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 122
    .local v6, "result":Ljava/lang/StringBuilder;
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 123
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    if-lt v5, v1, :cond_3

    .line 127
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 163
    .end local v5    # "i":I
    .end local p0    # "string":Ljava/lang/CharSequence;
    :cond_2
    :goto_1
    return-object p0

    .line 124
    .restart local v5    # "i":I
    .restart local p0    # "string":Ljava/lang/CharSequence;
    :cond_3
    invoke-interface {p0, v5}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v9

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 125
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 123
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 131
    .end local v5    # "i":I
    .end local v6    # "result":Ljava/lang/StringBuilder;
    :cond_4
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 132
    .restart local v6    # "result":Ljava/lang/StringBuilder;
    const/4 v9, 0x0

    invoke-interface {p1, v9}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    .line 133
    .local v2, "first":C
    const/4 v8, 0x0

    .local v8, "start":I
    const/4 v0, 0x0

    .line 134
    .local v0, "copyStart":I
    :goto_2
    if-lt v8, v1, :cond_7

    .line 160
    :cond_5
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v9

    if-nez v9, :cond_6

    if-eqz v0, :cond_2

    .line 162
    :cond_6
    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 163
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_1

    .line 135
    :cond_7
    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9, p3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9, v2, v8}, Ljava/lang/String;->indexOf(II)I

    move-result v3

    .line 136
    .local v3, "firstIndex":I
    const/4 v9, -0x1

    .line 135
    if-eq v3, v9, :cond_5

    .line 138
    const/4 v4, 0x1

    .line 139
    .local v4, "found":Z
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v9

    const/4 v10, 0x1

    if-le v9, v10, :cond_8

    .line 140
    add-int v9, v3, v7

    if-gt v9, v1, :cond_5

    .line 142
    const/4 v5, 0x1

    .restart local v5    # "i":I
    :goto_3
    if-lt v5, v7, :cond_9

    .line 150
    .end local v5    # "i":I
    :cond_8
    :goto_4
    if-eqz v4, :cond_b

    .line 151
    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9, v0, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 152
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 153
    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v9

    .line 154
    add-int v10, v3, v7

    .line 153
    invoke-virtual {v9, v3, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 155
    add-int v8, v3, v7

    move v0, v8

    .line 156
    goto :goto_2

    .line 143
    .restart local v5    # "i":I
    :cond_9
    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9, p3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v9

    add-int v10, v3, v5

    invoke-virtual {v9, v10}, Ljava/lang/String;->charAt(I)C

    move-result v9

    .line 144
    invoke-interface {p1, v5}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v10

    .line 143
    if-eq v9, v10, :cond_a

    .line 145
    const/4 v4, 0x0

    .line 146
    goto :goto_4

    .line 142
    :cond_a
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 157
    .end local v5    # "i":I
    :cond_b
    add-int/lit8 v8, v3, 0x1

    goto :goto_2
.end method


# virtual methods
.method public escape(Ljava/lang/CharSequence;Ljava/util/Locale;Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax$Type;)Ljava/lang/CharSequence;
    .locals 2
    .param p1, "text"    # Ljava/lang/CharSequence;
    .param p2, "locale"    # Ljava/util/Locale;
    .param p3, "type"    # Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax$Type;

    .prologue
    .line 191
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move-object v0, p1

    .line 207
    :goto_0
    return-object v0

    .line 198
    :cond_1
    instance-of v0, p1, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;

    if-eqz v0, :cond_2

    .line 199
    check-cast p1, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;

    .end local p1    # "text":Ljava/lang/CharSequence;
    sget-object v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/EscapeQuerySyntaxImpl;->wildcardChars:[C

    invoke-virtual {p1, v0}, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;->toStringEscaped([C)Ljava/lang/String;

    move-result-object p1

    .line 204
    .restart local p1    # "text":Ljava/lang/CharSequence;
    :goto_1
    sget-object v0, Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax$Type;->STRING:Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax$Type;

    if-ne p3, v0, :cond_3

    .line 205
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/queryparser/flexible/standard/parser/EscapeQuerySyntaxImpl;->escapeQuoted(Ljava/lang/CharSequence;Ljava/util/Locale;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 201
    :cond_2
    new-instance v0, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;

    invoke-direct {v0, p1}, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;-><init>(Ljava/lang/CharSequence;)V

    sget-object v1, Lorg/apache/lucene/queryparser/flexible/standard/parser/EscapeQuerySyntaxImpl;->wildcardChars:[C

    invoke-virtual {v0, v1}, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;->toStringEscaped([C)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    .line 207
    :cond_3
    invoke-static {p1, p2}, Lorg/apache/lucene/queryparser/flexible/standard/parser/EscapeQuerySyntaxImpl;->escapeTerm(Ljava/lang/CharSequence;Ljava/util/Locale;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method
