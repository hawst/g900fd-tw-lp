.class public Lorg/apache/lucene/queryparser/flexible/standard/builders/NumericRangeQueryNodeBuilder;
.super Ljava/lang/Object;
.source "NumericRangeQueryNodeBuilder.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/flexible/standard/builders/StandardQueryBuilder;


# static fields
.field private static synthetic $SWITCH_TABLE$org$apache$lucene$document$FieldType$NumericType:[I


# direct methods
.method static synthetic $SWITCH_TABLE$org$apache$lucene$document$FieldType$NumericType()[I
    .locals 3

    .prologue
    .line 37
    sget-object v0, Lorg/apache/lucene/queryparser/flexible/standard/builders/NumericRangeQueryNodeBuilder;->$SWITCH_TABLE$org$apache$lucene$document$FieldType$NumericType:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lorg/apache/lucene/document/FieldType$NumericType;->values()[Lorg/apache/lucene/document/FieldType$NumericType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lorg/apache/lucene/document/FieldType$NumericType;->DOUBLE:Lorg/apache/lucene/document/FieldType$NumericType;

    invoke-virtual {v1}, Lorg/apache/lucene/document/FieldType$NumericType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_3

    :goto_1
    :try_start_1
    sget-object v1, Lorg/apache/lucene/document/FieldType$NumericType;->FLOAT:Lorg/apache/lucene/document/FieldType$NumericType;

    invoke-virtual {v1}, Lorg/apache/lucene/document/FieldType$NumericType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_2

    :goto_2
    :try_start_2
    sget-object v1, Lorg/apache/lucene/document/FieldType$NumericType;->INT:Lorg/apache/lucene/document/FieldType$NumericType;

    invoke-virtual {v1}, Lorg/apache/lucene/document/FieldType$NumericType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_1

    :goto_3
    :try_start_3
    sget-object v1, Lorg/apache/lucene/document/FieldType$NumericType;->LONG:Lorg/apache/lucene/document/FieldType$NumericType;

    invoke-virtual {v1}, Lorg/apache/lucene/document/FieldType$NumericType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_0

    :goto_4
    sput-object v0, Lorg/apache/lucene/queryparser/flexible/standard/builders/NumericRangeQueryNodeBuilder;->$SWITCH_TABLE$org$apache$lucene$document$FieldType$NumericType:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_4

    :catch_1
    move-exception v1

    goto :goto_3

    :catch_2
    move-exception v1

    goto :goto_2

    :catch_3
    move-exception v1

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    return-void
.end method


# virtual methods
.method public bridge synthetic build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/NumericRangeQueryNodeBuilder;->build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v0

    return-object v0
.end method

.method public build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/search/NumericRangeQuery;
    .locals 16
    .param p1, "queryNode"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;",
            ")",
            "Lorg/apache/lucene/search/NumericRangeQuery",
            "<+",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 49
    move-object/from16 v10, p1

    check-cast v10, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericRangeQueryNode;

    .line 51
    .local v10, "numericRangeNode":Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericRangeQueryNode;
    invoke-virtual {v10}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericRangeQueryNode;->getLowerBound()Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldValuePairQueryNode;

    move-result-object v7

    check-cast v7, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;

    .line 52
    .local v7, "lowerNumericNode":Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;
    invoke-virtual {v10}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericRangeQueryNode;->getUpperBound()Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldValuePairQueryNode;

    move-result-object v12

    check-cast v12, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;

    .line 54
    .local v12, "upperNumericNode":Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;
    invoke-virtual {v7}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;->getValue()Ljava/lang/Number;

    move-result-object v6

    .line 55
    .local v6, "lowerNumber":Ljava/lang/Number;
    invoke-virtual {v12}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;->getValue()Ljava/lang/Number;

    move-result-object v11

    .line 57
    .local v11, "upperNumber":Ljava/lang/Number;
    invoke-virtual {v10}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericRangeQueryNode;->getNumericConfig()Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;

    move-result-object v9

    .line 58
    .local v9, "numericConfig":Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;
    invoke-virtual {v9}, Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;->getType()Lorg/apache/lucene/document/FieldType$NumericType;

    move-result-object v8

    .line 59
    .local v8, "numberType":Lorg/apache/lucene/document/FieldType$NumericType;
    invoke-virtual {v10}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericRangeQueryNode;->getField()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Lorg/apache/lucene/queryparser/flexible/core/util/StringUtils;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 60
    .local v0, "field":Ljava/lang/String;
    invoke-virtual {v10}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericRangeQueryNode;->isLowerInclusive()Z

    move-result v4

    .line 61
    .local v4, "minInclusive":Z
    invoke-virtual {v10}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericRangeQueryNode;->isUpperInclusive()Z

    move-result v5

    .line 62
    .local v5, "maxInclusive":Z
    invoke-virtual {v9}, Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;->getPrecisionStep()I

    move-result v1

    .line 64
    .local v1, "precisionStep":I
    invoke-static {}, Lorg/apache/lucene/queryparser/flexible/standard/builders/NumericRangeQueryNodeBuilder;->$SWITCH_TABLE$org$apache$lucene$document$FieldType$NumericType()[I

    move-result-object v2

    invoke-virtual {v8}, Lorg/apache/lucene/document/FieldType$NumericType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 86
    new-instance v2, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;

    new-instance v3, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;

    .line 87
    sget-object v13, Lorg/apache/lucene/queryparser/flexible/core/messages/QueryParserMessages;->UNSUPPORTED_NUMERIC_DATA_TYPE:Ljava/lang/String;

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object v8, v14, v15

    invoke-direct {v3, v13, v14}, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 86
    invoke-direct {v2, v3}, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;-><init>(Lorg/apache/lucene/queryparser/flexible/messages/Message;)V

    throw v2

    :pswitch_0
    move-object v2, v6

    .line 68
    check-cast v2, Ljava/lang/Long;

    move-object v3, v11

    check-cast v3, Ljava/lang/Long;

    .line 67
    invoke-static/range {v0 .. v5}, Lorg/apache/lucene/search/NumericRangeQuery;->newLongRange(Ljava/lang/String;ILjava/lang/Long;Ljava/lang/Long;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v2

    .line 81
    :goto_0
    return-object v2

    :pswitch_1
    move-object v2, v6

    .line 72
    check-cast v2, Ljava/lang/Integer;

    move-object v3, v11

    check-cast v3, Ljava/lang/Integer;

    .line 71
    invoke-static/range {v0 .. v5}, Lorg/apache/lucene/search/NumericRangeQuery;->newIntRange(Ljava/lang/String;ILjava/lang/Integer;Ljava/lang/Integer;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v2

    goto :goto_0

    :pswitch_2
    move-object v2, v6

    .line 77
    check-cast v2, Ljava/lang/Float;

    move-object v3, v11

    check-cast v3, Ljava/lang/Float;

    .line 76
    invoke-static/range {v0 .. v5}, Lorg/apache/lucene/search/NumericRangeQuery;->newFloatRange(Ljava/lang/String;ILjava/lang/Float;Ljava/lang/Float;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v2

    goto :goto_0

    :pswitch_3
    move-object v2, v6

    .line 82
    check-cast v2, Ljava/lang/Double;

    move-object v3, v11

    check-cast v3, Ljava/lang/Double;

    .line 81
    invoke-static/range {v0 .. v5}, Lorg/apache/lucene/search/NumericRangeQuery;->newDoubleRange(Ljava/lang/String;ILjava/lang/Double;Ljava/lang/Double;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v2

    goto :goto_0

    .line 64
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public bridge synthetic build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/search/Query;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/NumericRangeQueryNodeBuilder;->build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v0

    return-object v0
.end method
