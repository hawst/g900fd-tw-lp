.class public abstract Lorg/apache/lucene/queryparser/surround/query/SimpleTerm;
.super Lorg/apache/lucene/queryparser/surround/query/SrndQuery;
.source "SimpleTerm.java"

# interfaces
.implements Ljava/lang/Comparable;
.implements Lorg/apache/lucene/queryparser/surround/query/DistanceSubQuery;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/queryparser/surround/query/SimpleTerm$MatchingTermVisitor;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/queryparser/surround/query/SrndQuery;",
        "Lorg/apache/lucene/queryparser/surround/query/DistanceSubQuery;",
        "Ljava/lang/Comparable",
        "<",
        "Lorg/apache/lucene/queryparser/surround/query/SimpleTerm;",
        ">;"
    }
.end annotation


# instance fields
.field private quoted:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 0
    .param p1, "q"    # Z

    .prologue
    .line 32
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/surround/query/SrndQuery;-><init>()V

    iput-boolean p1, p0, Lorg/apache/lucene/queryparser/surround/query/SimpleTerm;->quoted:Z

    return-void
.end method


# virtual methods
.method public addSpanQueries(Lorg/apache/lucene/queryparser/surround/query/SpanNearClauseFactory;)V
    .locals 3
    .param p1, "sncf"    # Lorg/apache/lucene/queryparser/surround/query/SpanNearClauseFactory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 87
    .line 88
    invoke-virtual {p1}, Lorg/apache/lucene/queryparser/surround/query/SpanNearClauseFactory;->getIndexReader()Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    .line 89
    invoke-virtual {p1}, Lorg/apache/lucene/queryparser/surround/query/SpanNearClauseFactory;->getFieldName()Ljava/lang/String;

    move-result-object v1

    .line 90
    new-instance v2, Lorg/apache/lucene/queryparser/surround/query/SimpleTerm$1;

    invoke-direct {v2, p0, p1}, Lorg/apache/lucene/queryparser/surround/query/SimpleTerm$1;-><init>(Lorg/apache/lucene/queryparser/surround/query/SimpleTerm;Lorg/apache/lucene/queryparser/surround/query/SpanNearClauseFactory;)V

    .line 87
    invoke-virtual {p0, v0, v1, v2}, Lorg/apache/lucene/queryparser/surround/query/SimpleTerm;->visitMatchingTerms(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/queryparser/surround/query/SimpleTerm$MatchingTermVisitor;)V

    .line 96
    return-void
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/queryparser/surround/query/SimpleTerm;

    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/surround/query/SimpleTerm;->compareTo(Lorg/apache/lucene/queryparser/surround/query/SimpleTerm;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lorg/apache/lucene/queryparser/surround/query/SimpleTerm;)I
    .locals 2
    .param p1, "ost"    # Lorg/apache/lucene/queryparser/surround/query/SimpleTerm;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 49
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/query/SimpleTerm;->toStringUnquoted()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lorg/apache/lucene/queryparser/surround/query/SimpleTerm;->toStringUnquoted()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public distanceSubQueryNotAllowed()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x0

    return-object v0
.end method

.method public getFieldOperator()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    const-string v0, "/"

    return-object v0
.end method

.method public getQuote()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    const-string v0, "\""

    return-object v0
.end method

.method isQuoted()Z
    .locals 1

    .prologue
    .line 35
    iget-boolean v0, p0, Lorg/apache/lucene/queryparser/surround/query/SimpleTerm;->quoted:Z

    return v0
.end method

.method public makeLuceneQueryFieldNoBoost(Ljava/lang/String;Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;)Lorg/apache/lucene/search/Query;
    .locals 1
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "qf"    # Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;

    .prologue
    .line 100
    new-instance v0, Lorg/apache/lucene/queryparser/surround/query/SimpleTermRewriteQuery;

    invoke-direct {v0, p0, p1, p2}, Lorg/apache/lucene/queryparser/surround/query/SimpleTermRewriteQuery;-><init>(Lorg/apache/lucene/queryparser/surround/query/SimpleTerm;Ljava/lang/String;Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;)V

    return-object v0
.end method

.method protected suffixToString(Ljava/lang/StringBuilder;)V
    .locals 0
    .param p1, "r"    # Ljava/lang/StringBuilder;

    .prologue
    .line 52
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 56
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 57
    .local v0, "r":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/query/SimpleTerm;->isQuoted()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 58
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/query/SimpleTerm;->getQuote()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 60
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/query/SimpleTerm;->toStringUnquoted()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 61
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/query/SimpleTerm;->isQuoted()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 62
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/query/SimpleTerm;->getQuote()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64
    :cond_1
    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/surround/query/SimpleTerm;->suffixToString(Ljava/lang/StringBuilder;)V

    .line 65
    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/surround/query/SimpleTerm;->weightToString(Ljava/lang/StringBuilder;)V

    .line 66
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public abstract toStringUnquoted()Ljava/lang/String;
.end method

.method public abstract visitMatchingTerms(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/queryparser/surround/query/SimpleTerm$MatchingTermVisitor;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
