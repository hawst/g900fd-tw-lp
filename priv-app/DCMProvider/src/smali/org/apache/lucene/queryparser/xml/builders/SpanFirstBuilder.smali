.class public Lorg/apache/lucene/queryparser/xml/builders/SpanFirstBuilder;
.super Lorg/apache/lucene/queryparser/xml/builders/SpanBuilderBase;
.source "SpanFirstBuilder.java"


# instance fields
.field private final factory:Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilder;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilder;)V
    .locals 0
    .param p1, "factory"    # Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilder;

    .prologue
    .line 32
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/xml/builders/SpanBuilderBase;-><init>()V

    .line 33
    iput-object p1, p0, Lorg/apache/lucene/queryparser/xml/builders/SpanFirstBuilder;->factory:Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilder;

    .line 34
    return-void
.end method


# virtual methods
.method public getSpanQuery(Lorg/w3c/dom/Element;)Lorg/apache/lucene/search/spans/SpanQuery;
    .locals 6
    .param p1, "e"    # Lorg/w3c/dom/Element;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/xml/ParserException;
        }
    .end annotation

    .prologue
    .line 38
    const-string v4, "end"

    const/4 v5, 0x1

    invoke-static {p1, v4, v5}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttribute(Lorg/w3c/dom/Element;Ljava/lang/String;I)I

    move-result v1

    .line 39
    .local v1, "end":I
    invoke-static {p1}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getFirstChildElement(Lorg/w3c/dom/Element;)Lorg/w3c/dom/Element;

    move-result-object v0

    .line 40
    .local v0, "child":Lorg/w3c/dom/Element;
    iget-object v4, p0, Lorg/apache/lucene/queryparser/xml/builders/SpanFirstBuilder;->factory:Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilder;

    invoke-interface {v4, v0}, Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilder;->getSpanQuery(Lorg/w3c/dom/Element;)Lorg/apache/lucene/search/spans/SpanQuery;

    move-result-object v2

    .line 42
    .local v2, "q":Lorg/apache/lucene/search/spans/SpanQuery;
    new-instance v3, Lorg/apache/lucene/search/spans/SpanFirstQuery;

    invoke-direct {v3, v2, v1}, Lorg/apache/lucene/search/spans/SpanFirstQuery;-><init>(Lorg/apache/lucene/search/spans/SpanQuery;I)V

    .line 44
    .local v3, "sfq":Lorg/apache/lucene/search/spans/SpanFirstQuery;
    const-string v4, "boost"

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-static {p1, v4, v5}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttribute(Lorg/w3c/dom/Element;Ljava/lang/String;F)F

    move-result v4

    invoke-virtual {v3, v4}, Lorg/apache/lucene/search/spans/SpanFirstQuery;->setBoost(F)V

    .line 45
    return-object v3
.end method
