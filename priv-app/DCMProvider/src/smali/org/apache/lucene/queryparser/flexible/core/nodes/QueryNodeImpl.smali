.class public abstract Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;
.super Ljava/lang/Object;
.source "QueryNodeImpl.java"

# interfaces
.implements Ljava/lang/Cloneable;
.implements Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;


# static fields
.field public static final PLAINTEXT_FIELD_NAME:Ljava/lang/String; = "_plain"


# instance fields
.field private clauses:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;",
            ">;"
        }
    .end annotation
.end field

.field private isLeaf:Z

.field private parent:Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

.field private tags:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field protected toQueryStringIgnoreFields:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;->isLeaf:Z

    .line 43
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;->tags:Ljava/util/Hashtable;

    .line 45
    iput-object v1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;->clauses:Ljava/util/List;

    .line 181
    iput-object v1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;->parent:Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    .line 199
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;->toQueryStringIgnoreFields:Z

    .line 35
    return-void
.end method

.method private setParent(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)V
    .locals 0
    .param p1, "parent"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    .prologue
    .line 184
    iput-object p1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;->parent:Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    .line 185
    return-void
.end method


# virtual methods
.method public final add(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 74
    .local p1, "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;->isLeaf()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;->clauses:Ljava/util/List;

    if-nez v1, :cond_1

    .line 75
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    .line 76
    sget-object v2, Lorg/apache/lucene/queryparser/flexible/core/messages/QueryParserMessages;->NODE_ACTION_NOT_SUPPORTED:Ljava/lang/String;

    invoke-static {v2}, Lorg/apache/lucene/queryparser/flexible/messages/NLS;->getLocalizedMessage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 75
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 79
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_2

    .line 83
    return-void

    .line 79
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    .line 80
    .local v0, "child":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;->add(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)V

    goto :goto_0
.end method

.method public final add(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)V
    .locals 2
    .param p1, "child"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    .prologue
    .line 61
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;->isLeaf()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;->clauses:Ljava/util/List;

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 62
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 63
    sget-object v1, Lorg/apache/lucene/queryparser/flexible/core/messages/QueryParserMessages;->NODE_ACTION_NOT_SUPPORTED:Ljava/lang/String;

    invoke-static {v1}, Lorg/apache/lucene/queryparser/flexible/messages/NLS;->getLocalizedMessage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 62
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 66
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;->clauses:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 67
    check-cast p1, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;

    .end local p1    # "child":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    invoke-direct {p1, p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;->setParent(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)V

    .line 69
    return-void
.end method

.method protected allocate()V
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;->clauses:Ljava/util/List;

    if-nez v0, :cond_0

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;->clauses:Ljava/util/List;

    .line 56
    :goto_0
    return-void

    .line 53
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;->clauses:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto :goto_0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;->clone()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 141
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;->cloneTree()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v0

    return-object v0
.end method

.method public cloneTree()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 121
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;

    .line 122
    .local v1, "clone":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;
    iget-boolean v3, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;->isLeaf:Z

    iput-boolean v3, v1, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;->isLeaf:Z

    .line 125
    new-instance v3, Ljava/util/Hashtable;

    invoke-direct {v3}, Ljava/util/Hashtable;-><init>()V

    iput-object v3, v1, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;->tags:Ljava/util/Hashtable;

    .line 128
    iget-object v3, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;->clauses:Ljava/util/List;

    if-eqz v3, :cond_0

    .line 129
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 130
    .local v2, "localClauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    iget-object v3, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;->clauses:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 133
    iput-object v2, v1, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;->clauses:Ljava/util/List;

    .line 136
    .end local v2    # "localClauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    :cond_0
    return-object v1

    .line 130
    .restart local v2    # "localClauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    .line 131
    .local v0, "clause":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    invoke-interface {v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->cloneTree()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public containsTag(Ljava/lang/String;)Z
    .locals 2
    .param p1, "tagName"    # Ljava/lang/String;

    .prologue
    .line 173
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;->tags:Ljava/util/Hashtable;

    sget-object v1, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-virtual {p1, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final getChildren()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 154
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;->isLeaf()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;->clauses:Ljava/util/List;

    if-nez v0, :cond_1

    .line 155
    :cond_0
    const/4 v0, 0x0

    .line 157
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;->clauses:Ljava/util/List;

    goto :goto_0
.end method

.method public getParent()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;->parent:Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    return-object v0
.end method

.method public getTag(Ljava/lang/String;)Ljava/lang/Object;
    .locals 2
    .param p1, "tagName"    # Ljava/lang/String;

    .prologue
    .line 178
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;->tags:Ljava/util/Hashtable;

    sget-object v1, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-virtual {p1, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getTagMap()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 242
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;->tags:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    return-object v0
.end method

.method protected isDefaultField(Ljava/lang/CharSequence;)Z
    .locals 3
    .param p1, "fld"    # Ljava/lang/CharSequence;

    .prologue
    const/4 v0, 0x1

    .line 213
    iget-boolean v1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;->toQueryStringIgnoreFields:Z

    if-eqz v1, :cond_1

    .line 219
    :cond_0
    :goto_0
    return v0

    .line 215
    :cond_1
    if-eqz p1, :cond_0

    .line 217
    const-string v1, "_plain"

    invoke-static {p1}, Lorg/apache/lucene/queryparser/flexible/core/util/StringUtils;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 219
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLeaf()Z
    .locals 1

    .prologue
    .line 87
    iget-boolean v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;->isLeaf:Z

    return v0
.end method

.method protected isRoot()Z
    .locals 1

    .prologue
    .line 193
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;->getParent()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final set(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 93
    .local p1, "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;->isLeaf()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;->clauses:Ljava/util/List;

    if-nez v3, :cond_1

    .line 95
    :cond_0
    const-string v3, "org.apache.lucene.queryParser.messages.QueryParserMessages"

    invoke-static {v3}, Ljava/util/ResourceBundle;->getBundle(Ljava/lang/String;)Ljava/util/ResourceBundle;

    move-result-object v0

    .line 96
    .local v0, "bundle":Ljava/util/ResourceBundle;
    const-string v3, "Q0008E.NODE_ACTION_NOT_SUPPORTED"

    invoke-virtual {v0, v3}, Ljava/util/ResourceBundle;->getObject(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    .line 97
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 99
    .local v2, "message":Ljava/lang/String;
    new-instance v3, Ljava/lang/IllegalArgumentException;

    invoke-direct {v3, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 104
    .end local v0    # "bundle":Ljava/util/ResourceBundle;
    .end local v2    # "message":Ljava/lang/String;
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_2

    .line 111
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;->allocate()V

    .line 114
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_3

    .line 117
    return-void

    .line 104
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    .line 106
    .local v1, "child":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    check-cast v1, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;

    .end local v1    # "child":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    const/4 v4, 0x0

    invoke-direct {v1, v4}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;->setParent(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)V

    goto :goto_0

    .line 114
    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    .line 115
    .restart local v1    # "child":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    invoke-virtual {p0, v1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;->add(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)V

    goto :goto_1
.end method

.method protected setLeaf(Z)V
    .locals 0
    .param p1, "isLeaf"    # Z

    .prologue
    .line 145
    iput-boolean p1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;->isLeaf:Z

    .line 146
    return-void
.end method

.method public setTag(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2
    .param p1, "tagName"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    .line 162
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;->tags:Ljava/util/Hashtable;

    sget-object v1, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-virtual {p1, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 163
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 231
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public unsetTag(Ljava/lang/String;)V
    .locals 2
    .param p1, "tagName"    # Ljava/lang/String;

    .prologue
    .line 167
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;->tags:Ljava/util/Hashtable;

    sget-object v1, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-virtual {p1, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 168
    return-void
.end method
