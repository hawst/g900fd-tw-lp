.class public final enum Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax$Type;
.super Ljava/lang/Enum;
.source "EscapeQuerySyntax.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax$Type;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax$Type;

.field public static final enum NORMAL:Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax$Type;

.field public static final enum STRING:Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 32
    new-instance v0, Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax$Type;

    const-string v1, "STRING"

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax$Type;->STRING:Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax$Type;

    new-instance v0, Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax$Type;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v3}, Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax$Type;->NORMAL:Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax$Type;

    .line 31
    const/4 v0, 0x2

    new-array v0, v0, [Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax$Type;

    sget-object v1, Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax$Type;->STRING:Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax$Type;

    aput-object v1, v0, v2

    sget-object v1, Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax$Type;->NORMAL:Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax$Type;

    aput-object v1, v0, v3

    sput-object v0, Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax$Type;->ENUM$VALUES:[Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax$Type;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax$Type;

    return-object v0
.end method

.method public static values()[Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax$Type;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax$Type;->ENUM$VALUES:[Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax$Type;

    array-length v1, v0

    new-array v2, v1, [Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax$Type;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
