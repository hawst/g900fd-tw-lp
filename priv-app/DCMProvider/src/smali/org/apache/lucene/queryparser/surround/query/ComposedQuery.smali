.class public abstract Lorg/apache/lucene/queryparser/surround/query/ComposedQuery;
.super Lorg/apache/lucene/queryparser/surround/query/SrndQuery;
.source "ComposedQuery.java"


# instance fields
.field protected opName:Ljava/lang/String;

.field private operatorInfix:Z

.field protected queries:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/surround/query/SrndQuery;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;ZLjava/lang/String;)V
    .locals 0
    .param p2, "operatorInfix"    # Z
    .param p3, "opName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/surround/query/SrndQuery;",
            ">;Z",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 28
    .local p1, "qs":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/surround/query/SrndQuery;>;"
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/surround/query/SrndQuery;-><init>()V

    .line 29
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/surround/query/ComposedQuery;->recompose(Ljava/util/List;)V

    .line 30
    iput-boolean p2, p0, Lorg/apache/lucene/queryparser/surround/query/ComposedQuery;->operatorInfix:Z

    .line 31
    iput-object p3, p0, Lorg/apache/lucene/queryparser/surround/query/ComposedQuery;->opName:Ljava/lang/String;

    .line 32
    return-void
.end method


# virtual methods
.method protected getBracketClose()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    const-string v0, ")"

    return-object v0
.end method

.method protected getBracketOpen()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    const-string v0, "("

    return-object v0
.end method

.method public getNrSubQueries()I
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lorg/apache/lucene/queryparser/surround/query/ComposedQuery;->queries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getOperatorName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lorg/apache/lucene/queryparser/surround/query/ComposedQuery;->opName:Ljava/lang/String;

    return-object v0
.end method

.method protected getPrefixSeparator()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    const-string v0, ", "

    return-object v0
.end method

.method public getSubQueriesIterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lorg/apache/lucene/queryparser/surround/query/SrndQuery;",
            ">;"
        }
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lorg/apache/lucene/queryparser/surround/query/ComposedQuery;->queries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v0

    return-object v0
.end method

.method public getSubQuery(I)Lorg/apache/lucene/queryparser/surround/query/SrndQuery;
    .locals 1
    .param p1, "qn"    # I

    .prologue
    .line 48
    iget-object v0, p0, Lorg/apache/lucene/queryparser/surround/query/ComposedQuery;->queries:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/surround/query/SrndQuery;

    return-object v0
.end method

.method protected infixToString(Ljava/lang/StringBuilder;)V
    .locals 2
    .param p1, "r"    # Ljava/lang/StringBuilder;

    .prologue
    .line 81
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/query/ComposedQuery;->getSubQueriesIterator()Ljava/util/Iterator;

    move-result-object v0

    .line 82
    .local v0, "sqi":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/queryparser/surround/query/SrndQuery;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/query/ComposedQuery;->getBracketOpen()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 84
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/queryparser/surround/query/SrndQuery;

    invoke-virtual {v1}, Lorg/apache/lucene/queryparser/surround/query/SrndQuery;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 85
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_1

    .line 92
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/query/ComposedQuery;->getBracketClose()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 93
    return-void

    .line 86
    :cond_1
    const-string v1, " "

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 87
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/query/ComposedQuery;->getOperatorName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 88
    const-string v1, " "

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/queryparser/surround/query/SrndQuery;

    invoke-virtual {v1}, Lorg/apache/lucene/queryparser/surround/query/SrndQuery;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public isFieldsSubQueryAcceptable()Z
    .locals 2

    .prologue
    .line 113
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/query/ComposedQuery;->getSubQueriesIterator()Ljava/util/Iterator;

    move-result-object v0

    .line 114
    .local v0, "sqi":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/queryparser/surround/query/SrndQuery;>;"
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_1

    .line 119
    const/4 v1, 0x0

    :goto_0
    return v1

    .line 115
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/queryparser/surround/query/SrndQuery;

    invoke-virtual {v1}, Lorg/apache/lucene/queryparser/surround/query/SrndQuery;->isFieldsSubQueryAcceptable()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 116
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isOperatorInfix()Z
    .locals 1

    .prologue
    .line 51
    iget-boolean v0, p0, Lorg/apache/lucene/queryparser/surround/query/ComposedQuery;->operatorInfix:Z

    return v0
.end method

.method public makeLuceneSubQueriesField(Ljava/lang/String;Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;)Ljava/util/List;
    .locals 3
    .param p1, "fn"    # Ljava/lang/String;
    .param p2, "qf"    # Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;",
            ")",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/search/Query;",
            ">;"
        }
    .end annotation

    .prologue
    .line 54
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 55
    .local v0, "luceneSubQueries":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/Query;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/query/ComposedQuery;->getSubQueriesIterator()Ljava/util/Iterator;

    move-result-object v1

    .line 56
    .local v1, "sqi":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/queryparser/surround/query/SrndQuery;>;"
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 59
    return-object v0

    .line 57
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/queryparser/surround/query/SrndQuery;

    invoke-virtual {v2, p1, p2}, Lorg/apache/lucene/queryparser/surround/query/SrndQuery;->makeLuceneQueryField(Ljava/lang/String;Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;)Lorg/apache/lucene/search/Query;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method protected prefixToString(Ljava/lang/StringBuilder;)V
    .locals 2
    .param p1, "r"    # Ljava/lang/StringBuilder;

    .prologue
    .line 96
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/query/ComposedQuery;->getSubQueriesIterator()Ljava/util/Iterator;

    move-result-object v0

    .line 97
    .local v0, "sqi":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/queryparser/surround/query/SrndQuery;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/query/ComposedQuery;->getOperatorName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 98
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/query/ComposedQuery;->getBracketOpen()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 99
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 100
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/queryparser/surround/query/SrndQuery;

    invoke-virtual {v1}, Lorg/apache/lucene/queryparser/surround/query/SrndQuery;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 101
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_1

    .line 106
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/query/ComposedQuery;->getBracketClose()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 107
    return-void

    .line 102
    :cond_1
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/query/ComposedQuery;->getPrefixSeparator()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 103
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/queryparser/surround/query/SrndQuery;

    invoke-virtual {v1}, Lorg/apache/lucene/queryparser/surround/query/SrndQuery;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method protected recompose(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/surround/query/SrndQuery;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 35
    .local p1, "queries":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/surround/query/SrndQuery;>;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Too few subqueries"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 36
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/queryparser/surround/query/ComposedQuery;->queries:Ljava/util/List;

    .line 37
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 64
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 65
    .local v0, "r":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/query/ComposedQuery;->isOperatorInfix()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 66
    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/surround/query/ComposedQuery;->infixToString(Ljava/lang/StringBuilder;)V

    .line 70
    :goto_0
    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/surround/query/ComposedQuery;->weightToString(Ljava/lang/StringBuilder;)V

    .line 71
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 68
    :cond_0
    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/surround/query/ComposedQuery;->prefixToString(Ljava/lang/StringBuilder;)V

    goto :goto_0
.end method
