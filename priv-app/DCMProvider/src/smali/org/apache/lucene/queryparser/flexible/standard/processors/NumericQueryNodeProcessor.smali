.class public Lorg/apache/lucene/queryparser/flexible/standard/processors/NumericQueryNodeProcessor;
.super Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl;
.source "NumericQueryNodeProcessor.java"


# static fields
.field private static synthetic $SWITCH_TABLE$org$apache$lucene$document$FieldType$NumericType:[I


# direct methods
.method static synthetic $SWITCH_TABLE$org$apache$lucene$document$FieldType$NumericType()[I
    .locals 3

    .prologue
    .line 59
    sget-object v0, Lorg/apache/lucene/queryparser/flexible/standard/processors/NumericQueryNodeProcessor;->$SWITCH_TABLE$org$apache$lucene$document$FieldType$NumericType:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lorg/apache/lucene/document/FieldType$NumericType;->values()[Lorg/apache/lucene/document/FieldType$NumericType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lorg/apache/lucene/document/FieldType$NumericType;->DOUBLE:Lorg/apache/lucene/document/FieldType$NumericType;

    invoke-virtual {v1}, Lorg/apache/lucene/document/FieldType$NumericType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_3

    :goto_1
    :try_start_1
    sget-object v1, Lorg/apache/lucene/document/FieldType$NumericType;->FLOAT:Lorg/apache/lucene/document/FieldType$NumericType;

    invoke-virtual {v1}, Lorg/apache/lucene/document/FieldType$NumericType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_2

    :goto_2
    :try_start_2
    sget-object v1, Lorg/apache/lucene/document/FieldType$NumericType;->INT:Lorg/apache/lucene/document/FieldType$NumericType;

    invoke-virtual {v1}, Lorg/apache/lucene/document/FieldType$NumericType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_1

    :goto_3
    :try_start_3
    sget-object v1, Lorg/apache/lucene/document/FieldType$NumericType;->LONG:Lorg/apache/lucene/document/FieldType$NumericType;

    invoke-virtual {v1}, Lorg/apache/lucene/document/FieldType$NumericType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_0

    :goto_4
    sput-object v0, Lorg/apache/lucene/queryparser/flexible/standard/processors/NumericQueryNodeProcessor;->$SWITCH_TABLE$org$apache$lucene$document$FieldType$NumericType:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_4

    :catch_1
    move-exception v1

    goto :goto_3

    :catch_2
    move-exception v1

    goto :goto_2

    :catch_3
    move-exception v1

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl;-><init>()V

    .line 66
    return-void
.end method


# virtual methods
.method protected postProcessNode(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 18
    .param p1, "node"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 71
    move-object/from16 v0, p1

    instance-of v2, v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;

    if-eqz v2, :cond_1

    .line 72
    invoke-interface/range {p1 .. p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->getParent()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v2

    instance-of v2, v2, Lorg/apache/lucene/queryparser/flexible/core/nodes/RangeQueryNode;

    if-nez v2, :cond_1

    .line 74
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/queryparser/flexible/standard/processors/NumericQueryNodeProcessor;->getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    move-result-object v8

    .line 76
    .local v8, "config":Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;
    if-eqz v8, :cond_1

    move-object/from16 v11, p1

    .line 77
    check-cast v11, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;

    .line 79
    .local v11, "fieldNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    invoke-virtual {v11}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->getFieldAsString()Ljava/lang/String;

    move-result-object v2

    .line 78
    invoke-virtual {v8, v2}, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->getFieldConfig(Ljava/lang/String;)Lorg/apache/lucene/queryparser/flexible/core/config/FieldConfig;

    move-result-object v10

    .line 81
    .local v10, "fieldConfig":Lorg/apache/lucene/queryparser/flexible/core/config/FieldConfig;
    if-eqz v10, :cond_1

    .line 83
    sget-object v2, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->NUMERIC_CONFIG:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-virtual {v10, v2}, Lorg/apache/lucene/queryparser/flexible/core/config/FieldConfig;->get(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;

    .line 85
    .local v7, "numericConfig":Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;
    if-eqz v7, :cond_1

    .line 87
    invoke-virtual {v7}, Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;->getNumberFormat()Ljava/text/NumberFormat;

    move-result-object v13

    .line 88
    .local v13, "numberFormat":Ljava/text/NumberFormat;
    invoke-virtual {v11}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->getTextAsString()Ljava/lang/String;

    move-result-object v14

    .line 89
    .local v14, "text":Ljava/lang/String;
    const/4 v12, 0x0

    .line 91
    .local v12, "number":Ljava/lang/Number;
    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    .line 94
    :try_start_0
    invoke-virtual {v13, v14}, Ljava/text/NumberFormat;->parse(Ljava/lang/String;)Ljava/lang/Number;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v12

    .line 103
    invoke-static {}, Lorg/apache/lucene/queryparser/flexible/standard/processors/NumericQueryNodeProcessor;->$SWITCH_TABLE$org$apache$lucene$document$FieldType$NumericType()[I

    move-result-object v2

    invoke-virtual {v7}, Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;->getType()Lorg/apache/lucene/document/FieldType$NumericType;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/lucene/document/FieldType$NumericType;->ordinal()I

    move-result v5

    aget v2, v2, v5

    packed-switch v2, :pswitch_data_0

    .line 122
    :goto_0
    new-instance v3, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;

    .line 123
    invoke-virtual {v11}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->getField()Ljava/lang/CharSequence;

    move-result-object v2

    .line 122
    invoke-direct {v3, v2, v12, v13}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;-><init>(Ljava/lang/CharSequence;Ljava/lang/Number;Ljava/text/NumberFormat;)V

    .line 124
    .local v3, "lowerNode":Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;
    new-instance v4, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;

    .line 125
    invoke-virtual {v11}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->getField()Ljava/lang/CharSequence;

    move-result-object v2

    .line 124
    invoke-direct {v4, v2, v12, v13}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;-><init>(Ljava/lang/CharSequence;Ljava/lang/Number;Ljava/text/NumberFormat;)V

    .line 127
    .local v4, "upperNode":Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;
    new-instance v2, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericRangeQueryNode;

    const/4 v5, 0x1

    const/4 v6, 0x1

    invoke-direct/range {v2 .. v7}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericRangeQueryNode;-><init>(Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;ZZLorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;)V

    .line 138
    .end local v3    # "lowerNode":Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;
    .end local v4    # "upperNode":Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;
    .end local v7    # "numericConfig":Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;
    .end local v8    # "config":Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;
    .end local v10    # "fieldConfig":Lorg/apache/lucene/queryparser/flexible/core/config/FieldConfig;
    .end local v11    # "fieldNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    .end local v12    # "number":Ljava/lang/Number;
    .end local v13    # "numberFormat":Ljava/text/NumberFormat;
    .end local v14    # "text":Ljava/lang/String;
    :goto_1
    return-object v2

    .line 96
    .restart local v7    # "numericConfig":Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;
    .restart local v8    # "config":Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;
    .restart local v10    # "fieldConfig":Lorg/apache/lucene/queryparser/flexible/core/config/FieldConfig;
    .restart local v11    # "fieldNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    .restart local v12    # "number":Ljava/lang/Number;
    .restart local v13    # "numberFormat":Ljava/text/NumberFormat;
    .restart local v14    # "text":Ljava/lang/String;
    :catch_0
    move-exception v9

    .line 97
    .local v9, "e":Ljava/text/ParseException;
    new-instance v2, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeParseException;

    new-instance v5, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;

    .line 98
    sget-object v6, Lorg/apache/lucene/queryparser/flexible/core/messages/QueryParserMessages;->COULD_NOT_PARSE_NUMBER:Ljava/lang/String;

    const/4 v15, 0x2

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    .line 99
    invoke-virtual {v11}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->getTextAsString()Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x1

    invoke-virtual {v13}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v17

    .line 100
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-direct {v5, v6, v15}, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 97
    invoke-direct {v2, v5, v9}, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeParseException;-><init>(Lorg/apache/lucene/queryparser/flexible/messages/Message;Ljava/lang/Throwable;)V

    throw v2

    .line 105
    .end local v9    # "e":Ljava/text/ParseException;
    :pswitch_0
    invoke-virtual {v12}, Ljava/lang/Number;->longValue()J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    .line 106
    goto :goto_0

    .line 108
    :pswitch_1
    invoke-virtual {v12}, Ljava/lang/Number;->intValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    .line 109
    goto :goto_0

    .line 111
    :pswitch_2
    invoke-virtual {v12}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v12

    .line 112
    goto :goto_0

    .line 114
    :pswitch_3
    invoke-virtual {v12}, Ljava/lang/Number;->floatValue()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v12

    goto :goto_0

    .line 118
    :cond_0
    new-instance v2, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeParseException;

    new-instance v5, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;

    .line 119
    sget-object v6, Lorg/apache/lucene/queryparser/flexible/core/messages/QueryParserMessages;->NUMERIC_CANNOT_BE_EMPTY:Ljava/lang/String;

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    invoke-virtual {v11}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->getFieldAsString()Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-direct {v5, v6, v15}, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 118
    invoke-direct {v2, v5}, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeParseException;-><init>(Lorg/apache/lucene/queryparser/flexible/messages/Message;)V

    throw v2

    .end local v7    # "numericConfig":Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;
    .end local v8    # "config":Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;
    .end local v10    # "fieldConfig":Lorg/apache/lucene/queryparser/flexible/core/config/FieldConfig;
    .end local v11    # "fieldNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    .end local v12    # "number":Ljava/lang/Number;
    .end local v13    # "numberFormat":Ljava/text/NumberFormat;
    .end local v14    # "text":Ljava/lang/String;
    :cond_1
    move-object/from16 v2, p1

    .line 138
    goto :goto_1

    .line 103
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method protected preProcessNode(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 0
    .param p1, "node"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 144
    return-object p1
.end method

.method protected setChildrenOrder(Ljava/util/List;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 150
    .local p1, "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    return-object p1
.end method
