.class public Lorg/apache/lucene/queryparser/xml/builders/UserInputQueryBuilder;
.super Ljava/lang/Object;
.source "UserInputQueryBuilder.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/xml/QueryBuilder;


# instance fields
.field private analyzer:Lorg/apache/lucene/analysis/Analyzer;

.field private defaultField:Ljava/lang/String;

.field private unSafeParser:Lorg/apache/lucene/queryparser/classic/QueryParser;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer;)V
    .locals 0
    .param p1, "defaultField"    # Ljava/lang/String;
    .param p2, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p2, p0, Lorg/apache/lucene/queryparser/xml/builders/UserInputQueryBuilder;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    .line 53
    iput-object p1, p0, Lorg/apache/lucene/queryparser/xml/builders/UserInputQueryBuilder;->defaultField:Ljava/lang/String;

    .line 54
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/queryparser/classic/QueryParser;)V
    .locals 0
    .param p1, "parser"    # Lorg/apache/lucene/queryparser/classic/QueryParser;

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lorg/apache/lucene/queryparser/xml/builders/UserInputQueryBuilder;->unSafeParser:Lorg/apache/lucene/queryparser/classic/QueryParser;

    .line 49
    return-void
.end method


# virtual methods
.method protected createQueryParser(Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer;)Lorg/apache/lucene/queryparser/classic/QueryParser;
    .locals 2
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;

    .prologue
    .line 89
    new-instance v0, Lorg/apache/lucene/queryparser/classic/QueryParser;

    sget-object v1, Lorg/apache/lucene/util/Version;->LUCENE_CURRENT:Lorg/apache/lucene/util/Version;

    invoke-direct {v0, v1, p1, p2}, Lorg/apache/lucene/queryparser/classic/QueryParser;-><init>(Lorg/apache/lucene/util/Version;Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer;)V

    return-object v0
.end method

.method public getQuery(Lorg/w3c/dom/Element;)Lorg/apache/lucene/search/Query;
    .locals 7
    .param p1, "e"    # Lorg/w3c/dom/Element;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/xml/ParserException;
        }
    .end annotation

    .prologue
    .line 62
    invoke-static {p1}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getText(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v4

    .line 64
    .local v4, "text":Ljava/lang/String;
    const/4 v3, 0x0

    .line 65
    .local v3, "q":Lorg/apache/lucene/search/Query;
    :try_start_0
    iget-object v5, p0, Lorg/apache/lucene/queryparser/xml/builders/UserInputQueryBuilder;->unSafeParser:Lorg/apache/lucene/queryparser/classic/QueryParser;

    if-eqz v5, :cond_0

    .line 67
    iget-object v6, p0, Lorg/apache/lucene/queryparser/xml/builders/UserInputQueryBuilder;->unSafeParser:Lorg/apache/lucene/queryparser/classic/QueryParser;

    monitor-enter v6
    :try_end_0
    .catch Lorg/apache/lucene/queryparser/classic/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 68
    :try_start_1
    iget-object v5, p0, Lorg/apache/lucene/queryparser/xml/builders/UserInputQueryBuilder;->unSafeParser:Lorg/apache/lucene/queryparser/classic/QueryParser;

    invoke-virtual {v5, v4}, Lorg/apache/lucene/queryparser/classic/QueryParser;->parse(Ljava/lang/String;)Lorg/apache/lucene/search/Query;

    move-result-object v3

    .line 67
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 76
    :goto_0
    :try_start_2
    const-string v5, "boost"

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-static {p1, v5, v6}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttribute(Lorg/w3c/dom/Element;Ljava/lang/String;F)F

    move-result v5

    invoke-virtual {v3, v5}, Lorg/apache/lucene/search/Query;->setBoost(F)V
    :try_end_2
    .catch Lorg/apache/lucene/queryparser/classic/ParseException; {:try_start_2 .. :try_end_2} :catch_0

    .line 77
    return-object v3

    .line 67
    :catchall_0
    move-exception v5

    :try_start_3
    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v5
    :try_end_4
    .catch Lorg/apache/lucene/queryparser/classic/ParseException; {:try_start_4 .. :try_end_4} :catch_0

    .line 78
    :catch_0
    move-exception v0

    .line 79
    .local v0, "e1":Lorg/apache/lucene/queryparser/classic/ParseException;
    new-instance v5, Lorg/apache/lucene/queryparser/xml/ParserException;

    invoke-virtual {v0}, Lorg/apache/lucene/queryparser/classic/ParseException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lorg/apache/lucene/queryparser/xml/ParserException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 71
    .end local v0    # "e1":Lorg/apache/lucene/queryparser/classic/ParseException;
    :cond_0
    :try_start_5
    const-string v5, "fieldName"

    iget-object v6, p0, Lorg/apache/lucene/queryparser/xml/builders/UserInputQueryBuilder;->defaultField:Ljava/lang/String;

    invoke-static {p1, v5, v6}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttribute(Lorg/w3c/dom/Element;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 73
    .local v1, "fieldName":Ljava/lang/String;
    iget-object v5, p0, Lorg/apache/lucene/queryparser/xml/builders/UserInputQueryBuilder;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    invoke-virtual {p0, v1, v5}, Lorg/apache/lucene/queryparser/xml/builders/UserInputQueryBuilder;->createQueryParser(Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer;)Lorg/apache/lucene/queryparser/classic/QueryParser;

    move-result-object v2

    .line 74
    .local v2, "parser":Lorg/apache/lucene/queryparser/classic/QueryParser;
    invoke-virtual {v2, v4}, Lorg/apache/lucene/queryparser/classic/QueryParser;->parse(Ljava/lang/String;)Lorg/apache/lucene/search/Query;
    :try_end_5
    .catch Lorg/apache/lucene/queryparser/classic/ParseException; {:try_start_5 .. :try_end_5} :catch_0

    move-result-object v3

    goto :goto_0
.end method
