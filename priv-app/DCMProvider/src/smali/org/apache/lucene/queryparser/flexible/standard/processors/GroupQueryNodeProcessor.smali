.class public Lorg/apache/lucene/queryparser/flexible/standard/processors/GroupQueryNodeProcessor;
.super Ljava/lang/Object;
.source "GroupQueryNodeProcessor.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;


# instance fields
.field private latestNodeVerified:Z

.field private queryConfig:Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

.field private queryNodeList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;",
            ">;"
        }
    .end annotation
.end field

.field private usingAnd:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/processors/GroupQueryNodeProcessor;->usingAnd:Ljava/lang/Boolean;

    .line 66
    return-void
.end method

.method private applyModifier(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 3
    .param p1, "node"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .param p2, "parent"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    .prologue
    .line 115
    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/processors/GroupQueryNodeProcessor;->usingAnd:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 117
    instance-of v1, p2, Lorg/apache/lucene/queryparser/flexible/core/nodes/OrQueryNode;

    if-eqz v1, :cond_1

    .line 119
    instance-of v1, p1, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 121
    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;

    .line 123
    .local v0, "modNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;
    invoke-virtual {v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;->getModifier()Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    move-result-object v1

    sget-object v2, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;->MOD_REQ:Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    if-ne v1, v2, :cond_0

    .line 124
    invoke-virtual {v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;->getChild()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object p1

    .line 165
    .end local v0    # "modNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;
    .end local p1    # "node":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    :cond_0
    :goto_0
    return-object p1

    .line 131
    .restart local p1    # "node":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    :cond_1
    instance-of v1, p1, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;

    if-eqz v1, :cond_2

    move-object v0, p1

    .line 133
    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;

    .line 135
    .restart local v0    # "modNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;
    invoke-virtual {v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;->getModifier()Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    move-result-object v1

    sget-object v2, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;->MOD_NONE:Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    if-ne v1, v2, :cond_0

    .line 136
    new-instance p1, Lorg/apache/lucene/queryparser/flexible/standard/nodes/BooleanModifierNode;

    .end local p1    # "node":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    invoke-virtual {v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;->getChild()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v1

    sget-object v2, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;->MOD_REQ:Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    invoke-direct {p1, v1, v2}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/BooleanModifierNode;-><init>(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;)V

    goto :goto_0

    .line 140
    .end local v0    # "modNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;
    .restart local p1    # "node":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    :cond_2
    new-instance v1, Lorg/apache/lucene/queryparser/flexible/standard/nodes/BooleanModifierNode;

    sget-object v2, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;->MOD_REQ:Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    invoke-direct {v1, p1, v2}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/BooleanModifierNode;-><init>(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;)V

    move-object p1, v1

    goto :goto_0

    .line 147
    :cond_3
    invoke-interface {p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->getParent()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v1

    instance-of v1, v1, Lorg/apache/lucene/queryparser/flexible/core/nodes/AndQueryNode;

    if-eqz v1, :cond_0

    .line 149
    instance-of v1, p1, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;

    if-eqz v1, :cond_4

    move-object v0, p1

    .line 151
    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;

    .line 153
    .restart local v0    # "modNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;
    invoke-virtual {v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;->getModifier()Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    move-result-object v1

    sget-object v2, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;->MOD_NONE:Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    if-ne v1, v2, :cond_0

    .line 154
    new-instance p1, Lorg/apache/lucene/queryparser/flexible/standard/nodes/BooleanModifierNode;

    .end local p1    # "node":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    invoke-virtual {v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;->getChild()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v1

    sget-object v2, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;->MOD_REQ:Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    invoke-direct {p1, v1, v2}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/BooleanModifierNode;-><init>(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;)V

    goto :goto_0

    .line 158
    .end local v0    # "modNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;
    .restart local p1    # "node":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    :cond_4
    new-instance v1, Lorg/apache/lucene/queryparser/flexible/standard/nodes/BooleanModifierNode;

    sget-object v2, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;->MOD_REQ:Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    invoke-direct {v1, p1, v2}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/BooleanModifierNode;-><init>(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;)V

    move-object p1, v1

    goto :goto_0
.end method

.method private processNode(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)V
    .locals 3
    .param p1, "node"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    .prologue
    .line 195
    instance-of v0, p1, Lorg/apache/lucene/queryparser/flexible/core/nodes/AndQueryNode;

    if-nez v0, :cond_0

    instance-of v0, p1, Lorg/apache/lucene/queryparser/flexible/core/nodes/OrQueryNode;

    if-eqz v0, :cond_2

    .line 197
    :cond_0
    iget-boolean v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/processors/GroupQueryNodeProcessor;->latestNodeVerified:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/processors/GroupQueryNodeProcessor;->queryNodeList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 198
    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/processors/GroupQueryNodeProcessor;->queryNodeList:Ljava/util/ArrayList;

    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/processors/GroupQueryNodeProcessor;->queryNodeList:Ljava/util/ArrayList;

    .line 199
    iget-object v2, p0, Lorg/apache/lucene/queryparser/flexible/standard/processors/GroupQueryNodeProcessor;->queryNodeList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    .line 198
    invoke-direct {p0, v0, p1}, Lorg/apache/lucene/queryparser/flexible/standard/processors/GroupQueryNodeProcessor;->applyModifier(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 200
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/processors/GroupQueryNodeProcessor;->latestNodeVerified:Z

    .line 210
    :cond_1
    :goto_0
    return-void

    .line 204
    :cond_2
    instance-of v0, p1, Lorg/apache/lucene/queryparser/flexible/core/nodes/BooleanQueryNode;

    if-nez v0, :cond_1

    .line 205
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/processors/GroupQueryNodeProcessor;->queryNodeList:Ljava/util/ArrayList;

    invoke-interface {p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->getParent()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v1

    invoke-direct {p0, p1, v1}, Lorg/apache/lucene/queryparser/flexible/standard/processors/GroupQueryNodeProcessor;->applyModifier(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 206
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/processors/GroupQueryNodeProcessor;->latestNodeVerified:Z

    goto :goto_0
.end method

.method private readTree(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)V
    .locals 3
    .param p1, "node"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    .prologue
    .line 171
    instance-of v2, p1, Lorg/apache/lucene/queryparser/flexible/core/nodes/BooleanQueryNode;

    if-eqz v2, :cond_2

    .line 172
    invoke-interface {p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->getChildren()Ljava/util/List;

    move-result-object v0

    .line 174
    .local v0, "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 176
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-lt v1, v2, :cond_0

    .line 180
    invoke-direct {p0, p1}, Lorg/apache/lucene/queryparser/flexible/standard/processors/GroupQueryNodeProcessor;->processNode(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)V

    .line 181
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    invoke-direct {p0, v2}, Lorg/apache/lucene/queryparser/flexible/standard/processors/GroupQueryNodeProcessor;->readTree(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)V

    .line 191
    .end local v0    # "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    .end local v1    # "i":I
    :goto_1
    return-void

    .line 177
    .restart local v0    # "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    .restart local v1    # "i":I
    :cond_0
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    invoke-direct {p0, v2}, Lorg/apache/lucene/queryparser/flexible/standard/processors/GroupQueryNodeProcessor;->readTree(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)V

    .line 176
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 184
    .end local v1    # "i":I
    :cond_1
    invoke-direct {p0, p1}, Lorg/apache/lucene/queryparser/flexible/standard/processors/GroupQueryNodeProcessor;->processNode(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)V

    goto :goto_1

    .line 188
    .end local v0    # "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    :cond_2
    invoke-direct {p0, p1}, Lorg/apache/lucene/queryparser/flexible/standard/processors/GroupQueryNodeProcessor;->processNode(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)V

    goto :goto_1
.end method


# virtual methods
.method public getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/processors/GroupQueryNodeProcessor;->queryConfig:Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    return-object v0
.end method

.method public process(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 7
    .param p1, "queryTree"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 70
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/processors/GroupQueryNodeProcessor;->getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    move-result-object v4

    sget-object v6, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->DEFAULT_OPERATOR:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-virtual {v4, v6}, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->get(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$Operator;

    .line 72
    .local v1, "defaultOperator":Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$Operator;
    if-nez v1, :cond_0

    .line 73
    new-instance v4, Ljava/lang/IllegalArgumentException;

    .line 74
    const-string v5, "DEFAULT_OPERATOR should be set on the QueryConfigHandler"

    .line 73
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 77
    :cond_0
    sget-object v4, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$Operator;->AND:Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$Operator;

    if-ne v4, v1, :cond_2

    const/4 v4, 0x1

    :goto_0
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iput-object v4, p0, Lorg/apache/lucene/queryparser/flexible/standard/processors/GroupQueryNodeProcessor;->usingAnd:Ljava/lang/Boolean;

    .line 79
    instance-of v4, p1, Lorg/apache/lucene/queryparser/flexible/core/nodes/GroupQueryNode;

    if-eqz v4, :cond_1

    .line 80
    check-cast p1, Lorg/apache/lucene/queryparser/flexible/core/nodes/GroupQueryNode;

    .end local p1    # "queryTree":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    invoke-virtual {p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/GroupQueryNode;->getChild()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object p1

    .line 83
    .restart local p1    # "queryTree":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    :cond_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lorg/apache/lucene/queryparser/flexible/standard/processors/GroupQueryNodeProcessor;->queryNodeList:Ljava/util/ArrayList;

    .line 84
    iput-boolean v5, p0, Lorg/apache/lucene/queryparser/flexible/standard/processors/GroupQueryNodeProcessor;->latestNodeVerified:Z

    .line 85
    invoke-direct {p0, p1}, Lorg/apache/lucene/queryparser/flexible/standard/processors/GroupQueryNodeProcessor;->readTree(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)V

    .line 87
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/processors/GroupQueryNodeProcessor;->queryNodeList:Ljava/util/ArrayList;

    .line 89
    .local v0, "actualQueryNodeList":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-lt v2, v4, :cond_3

    .line 98
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iput-object v4, p0, Lorg/apache/lucene/queryparser/flexible/standard/processors/GroupQueryNodeProcessor;->usingAnd:Ljava/lang/Boolean;

    .line 100
    instance-of v4, p1, Lorg/apache/lucene/queryparser/flexible/core/nodes/BooleanQueryNode;

    if-eqz v4, :cond_5

    .line 101
    invoke-interface {p1, v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->set(Ljava/util/List;)V

    .line 106
    .end local p1    # "queryTree":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    :goto_2
    return-object p1

    .end local v0    # "actualQueryNodeList":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    .end local v2    # "i":I
    .restart local p1    # "queryTree":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    :cond_2
    move v4, v5

    .line 77
    goto :goto_0

    .line 90
    .restart local v0    # "actualQueryNodeList":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    .restart local v2    # "i":I
    :cond_3
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    .line 92
    .local v3, "node":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    instance-of v4, v3, Lorg/apache/lucene/queryparser/flexible/core/nodes/GroupQueryNode;

    if-eqz v4, :cond_4

    .line 93
    invoke-virtual {p0, v3}, Lorg/apache/lucene/queryparser/flexible/standard/processors/GroupQueryNodeProcessor;->process(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v4

    invoke-interface {v0, v2, v4}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 89
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 106
    .end local v3    # "node":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    :cond_5
    new-instance p1, Lorg/apache/lucene/queryparser/flexible/core/nodes/BooleanQueryNode;

    .end local p1    # "queryTree":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    invoke-direct {p1, v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/BooleanQueryNode;-><init>(Ljava/util/List;)V

    goto :goto_2
.end method

.method public setQueryConfigHandler(Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;)V
    .locals 0
    .param p1, "queryConfigHandler"    # Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    .prologue
    .line 219
    iput-object p1, p0, Lorg/apache/lucene/queryparser/flexible/standard/processors/GroupQueryNodeProcessor;->queryConfig:Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    .line 220
    return-void
.end method
