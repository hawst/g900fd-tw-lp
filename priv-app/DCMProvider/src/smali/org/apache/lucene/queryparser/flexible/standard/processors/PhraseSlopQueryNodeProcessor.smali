.class public Lorg/apache/lucene/queryparser/flexible/standard/processors/PhraseSlopQueryNodeProcessor;
.super Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl;
.source "PhraseSlopQueryNodeProcessor.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl;-><init>()V

    .line 40
    return-void
.end method


# virtual methods
.method protected postProcessNode(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 2
    .param p1, "node"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 45
    instance-of v1, p1, Lorg/apache/lucene/queryparser/flexible/core/nodes/SlopQueryNode;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 46
    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/SlopQueryNode;

    .line 48
    .local v0, "phraseSlopNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/SlopQueryNode;
    invoke-virtual {v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/SlopQueryNode;->getChild()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v1

    instance-of v1, v1, Lorg/apache/lucene/queryparser/flexible/core/nodes/TokenizedPhraseQueryNode;

    if-nez v1, :cond_0

    .line 49
    invoke-virtual {v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/SlopQueryNode;->getChild()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v1

    instance-of v1, v1, Lorg/apache/lucene/queryparser/flexible/standard/nodes/MultiPhraseQueryNode;

    if-nez v1, :cond_0

    .line 50
    invoke-virtual {v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/SlopQueryNode;->getChild()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object p1

    .line 55
    .end local v0    # "phraseSlopNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/SlopQueryNode;
    .end local p1    # "node":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    :cond_0
    return-object p1
.end method

.method protected preProcessNode(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 0
    .param p1, "node"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 62
    return-object p1
.end method

.method protected setChildrenOrder(Ljava/util/List;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 70
    .local p1, "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    return-object p1
.end method
