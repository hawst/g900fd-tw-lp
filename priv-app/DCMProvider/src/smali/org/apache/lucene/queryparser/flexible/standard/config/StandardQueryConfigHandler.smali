.class public Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler;
.super Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;
.source "StandardQueryConfigHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;,
        Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$Operator;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 195
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;-><init>()V

    .line 197
    new-instance v0, Lorg/apache/lucene/queryparser/flexible/standard/config/FieldBoostMapFCListener;

    invoke-direct {v0, p0}, Lorg/apache/lucene/queryparser/flexible/standard/config/FieldBoostMapFCListener;-><init>(Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;)V

    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler;->addFieldConfigListener(Lorg/apache/lucene/queryparser/flexible/core/config/FieldConfigListener;)V

    .line 198
    new-instance v0, Lorg/apache/lucene/queryparser/flexible/standard/config/FieldDateResolutionFCListener;

    invoke-direct {v0, p0}, Lorg/apache/lucene/queryparser/flexible/standard/config/FieldDateResolutionFCListener;-><init>(Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;)V

    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler;->addFieldConfigListener(Lorg/apache/lucene/queryparser/flexible/core/config/FieldConfigListener;)V

    .line 199
    new-instance v0, Lorg/apache/lucene/queryparser/flexible/standard/config/NumericFieldConfigListener;

    invoke-direct {v0, p0}, Lorg/apache/lucene/queryparser/flexible/standard/config/NumericFieldConfigListener;-><init>(Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;)V

    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler;->addFieldConfigListener(Lorg/apache/lucene/queryparser/flexible/core/config/FieldConfigListener;)V

    .line 202
    sget-object v0, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->ALLOW_LEADING_WILDCARD:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler;->set(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;Ljava/lang/Object;)V

    .line 203
    sget-object v0, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->ANALYZER:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler;->set(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;Ljava/lang/Object;)V

    .line 204
    sget-object v0, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->DEFAULT_OPERATOR:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    sget-object v1, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$Operator;->OR:Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$Operator;

    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler;->set(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;Ljava/lang/Object;)V

    .line 205
    sget-object v0, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->PHRASE_SLOP:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler;->set(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;Ljava/lang/Object;)V

    .line 206
    sget-object v0, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->LOWERCASE_EXPANDED_TERMS:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler;->set(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;Ljava/lang/Object;)V

    .line 207
    sget-object v0, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->ENABLE_POSITION_INCREMENTS:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler;->set(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;Ljava/lang/Object;)V

    .line 208
    sget-object v0, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->FIELD_BOOST_MAP:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler;->set(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;Ljava/lang/Object;)V

    .line 209
    sget-object v0, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->FUZZY_CONFIG:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    new-instance v1, Lorg/apache/lucene/queryparser/flexible/standard/config/FuzzyConfig;

    invoke-direct {v1}, Lorg/apache/lucene/queryparser/flexible/standard/config/FuzzyConfig;-><init>()V

    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler;->set(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;Ljava/lang/Object;)V

    .line 210
    sget-object v0, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->LOCALE:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler;->set(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;Ljava/lang/Object;)V

    .line 211
    sget-object v0, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->MULTI_TERM_REWRITE_METHOD:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    sget-object v1, Lorg/apache/lucene/search/MultiTermQuery;->CONSTANT_SCORE_AUTO_REWRITE_DEFAULT:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler;->set(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;Ljava/lang/Object;)V

    .line 212
    sget-object v0, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->FIELD_DATE_RESOLUTION_MAP:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler;->set(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;Ljava/lang/Object;)V

    .line 214
    return-void
.end method
