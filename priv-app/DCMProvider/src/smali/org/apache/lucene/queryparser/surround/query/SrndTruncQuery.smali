.class public Lorg/apache/lucene/queryparser/surround/query/SrndTruncQuery;
.super Lorg/apache/lucene/queryparser/surround/query/SimpleTerm;
.source "SrndTruncQuery.java"


# instance fields
.field private final mask:C

.field private pattern:Ljava/util/regex/Pattern;

.field private prefix:Ljava/lang/String;

.field private prefixRef:Lorg/apache/lucene/util/BytesRef;

.field private final truncated:Ljava/lang/String;

.field private final unlimited:C


# direct methods
.method public constructor <init>(Ljava/lang/String;CC)V
    .locals 1
    .param p1, "truncated"    # Ljava/lang/String;
    .param p2, "unlimited"    # C
    .param p3, "mask"    # C

    .prologue
    .line 37
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/lucene/queryparser/surround/query/SimpleTerm;-><init>(Z)V

    .line 38
    iput-object p1, p0, Lorg/apache/lucene/queryparser/surround/query/SrndTruncQuery;->truncated:Ljava/lang/String;

    .line 39
    iput-char p2, p0, Lorg/apache/lucene/queryparser/surround/query/SrndTruncQuery;->unlimited:C

    .line 40
    iput-char p3, p0, Lorg/apache/lucene/queryparser/surround/query/SrndTruncQuery;->mask:C

    .line 41
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/query/SrndTruncQuery;->truncatedToPrefixAndPattern()V

    .line 42
    return-void
.end method


# virtual methods
.method protected appendRegExpForChar(CLjava/lang/StringBuilder;)V
    .locals 1
    .param p1, "c"    # C
    .param p2, "re"    # Ljava/lang/StringBuilder;

    .prologue
    .line 64
    iget-char v0, p0, Lorg/apache/lucene/queryparser/surround/query/SrndTruncQuery;->unlimited:C

    if-ne p1, v0, :cond_0

    .line 65
    const-string v0, ".*"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 70
    :goto_0
    return-void

    .line 66
    :cond_0
    iget-char v0, p0, Lorg/apache/lucene/queryparser/surround/query/SrndTruncQuery;->mask:C

    if-ne p1, v0, :cond_1

    .line 67
    const-string v0, "."

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 69
    :cond_1
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public getTruncated()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lorg/apache/lucene/queryparser/surround/query/SrndTruncQuery;->truncated:Ljava/lang/String;

    return-object v0
.end method

.method protected matchingChar(C)Z
    .locals 1
    .param p1, "c"    # C

    .prologue
    .line 60
    iget-char v0, p0, Lorg/apache/lucene/queryparser/surround/query/SrndTruncQuery;->unlimited:C

    if-eq p1, v0, :cond_0

    iget-char v0, p0, Lorg/apache/lucene/queryparser/surround/query/SrndTruncQuery;->mask:C

    if-eq p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toStringUnquoted()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/query/SrndTruncQuery;->getTruncated()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected truncatedToPrefixAndPattern()V
    .locals 4

    .prologue
    .line 73
    const/4 v0, 0x0

    .line 74
    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/queryparser/surround/query/SrndTruncQuery;->truncated:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lorg/apache/lucene/queryparser/surround/query/SrndTruncQuery;->truncated:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-virtual {p0, v2}, Lorg/apache/lucene/queryparser/surround/query/SrndTruncQuery;->matchingChar(C)Z

    move-result v2

    if-nez v2, :cond_1

    .line 77
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/queryparser/surround/query/SrndTruncQuery;->truncated:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/lucene/queryparser/surround/query/SrndTruncQuery;->prefix:Ljava/lang/String;

    .line 78
    new-instance v2, Lorg/apache/lucene/util/BytesRef;

    iget-object v3, p0, Lorg/apache/lucene/queryparser/surround/query/SrndTruncQuery;->prefix:Ljava/lang/String;

    invoke-direct {v2, v3}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    iput-object v2, p0, Lorg/apache/lucene/queryparser/surround/query/SrndTruncQuery;->prefixRef:Lorg/apache/lucene/util/BytesRef;

    .line 80
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 81
    .local v1, "re":Ljava/lang/StringBuilder;
    :goto_1
    iget-object v2, p0, Lorg/apache/lucene/queryparser/surround/query/SrndTruncQuery;->truncated:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lt v0, v2, :cond_2

    .line 85
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/lucene/queryparser/surround/query/SrndTruncQuery;->pattern:Ljava/util/regex/Pattern;

    .line 86
    return-void

    .line 75
    .end local v1    # "re":Ljava/lang/StringBuilder;
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 82
    .restart local v1    # "re":Ljava/lang/StringBuilder;
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/queryparser/surround/query/SrndTruncQuery;->truncated:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-virtual {p0, v2, v1}, Lorg/apache/lucene/queryparser/surround/query/SrndTruncQuery;->appendRegExpForChar(CLjava/lang/StringBuilder;)V

    .line 83
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public visitMatchingTerms(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/queryparser/surround/query/SimpleTerm$MatchingTermVisitor;)V
    .locals 9
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "fieldName"    # Ljava/lang/String;
    .param p3, "mtv"    # Lorg/apache/lucene/queryparser/surround/query/SimpleTerm$MatchingTermVisitor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 94
    iget-object v7, p0, Lorg/apache/lucene/queryparser/surround/query/SrndTruncQuery;->prefix:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v1

    .line 95
    .local v1, "prefixLength":I
    invoke-static {p1, p2}, Lorg/apache/lucene/index/MultiFields;->getTerms(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v3

    .line 96
    .local v3, "terms":Lorg/apache/lucene/index/Terms;
    if-eqz v3, :cond_1

    .line 97
    iget-object v7, p0, Lorg/apache/lucene/queryparser/surround/query/SrndTruncQuery;->pattern:Ljava/util/regex/Pattern;

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 99
    .local v0, "matcher":Ljava/util/regex/Matcher;
    const/4 v7, 0x0

    :try_start_0
    invoke-virtual {v3, v7}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v4

    .line 101
    .local v4, "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    iget-object v7, p0, Lorg/apache/lucene/queryparser/surround/query/SrndTruncQuery;->prefixRef:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v4, v7}, Lorg/apache/lucene/index/TermsEnum;->seekCeil(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    move-result-object v2

    .line 103
    .local v2, "status":Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    sget-object v7, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    if-ne v2, v7, :cond_2

    .line 104
    iget-object v5, p0, Lorg/apache/lucene/queryparser/surround/query/SrndTruncQuery;->prefixRef:Lorg/apache/lucene/util/BytesRef;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 111
    .local v5, "text":Lorg/apache/lucene/util/BytesRef;
    :goto_0
    if-nez v5, :cond_4

    .line 124
    :cond_0
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->reset()Ljava/util/regex/Matcher;

    .line 127
    .end local v0    # "matcher":Ljava/util/regex/Matcher;
    .end local v2    # "status":Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    .end local v4    # "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    .end local v5    # "text":Lorg/apache/lucene/util/BytesRef;
    :cond_1
    return-void

    .line 105
    .restart local v0    # "matcher":Ljava/util/regex/Matcher;
    .restart local v2    # "status":Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    .restart local v4    # "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    :cond_2
    :try_start_1
    sget-object v7, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->NOT_FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    if-ne v2, v7, :cond_3

    .line 106
    invoke-virtual {v4}, Lorg/apache/lucene/index/TermsEnum;->term()Lorg/apache/lucene/util/BytesRef;

    move-result-object v5

    .line 107
    .restart local v5    # "text":Lorg/apache/lucene/util/BytesRef;
    goto :goto_0

    .line 108
    .end local v5    # "text":Lorg/apache/lucene/util/BytesRef;
    :cond_3
    const/4 v5, 0x0

    .line 111
    .restart local v5    # "text":Lorg/apache/lucene/util/BytesRef;
    goto :goto_0

    .line 112
    :cond_4
    if-eqz v5, :cond_0

    iget-object v7, p0, Lorg/apache/lucene/queryparser/surround/query/SrndTruncQuery;->prefixRef:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v5, v7}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 113
    invoke-virtual {v5}, Lorg/apache/lucene/util/BytesRef;->utf8ToString()Ljava/lang/String;

    move-result-object v6

    .line 114
    .local v6, "textString":Ljava/lang/String;
    invoke-virtual {v6, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/util/regex/Matcher;->reset(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    .line 115
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 116
    new-instance v7, Lorg/apache/lucene/index/Term;

    invoke-direct {v7, p2, v6}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p3, v7}, Lorg/apache/lucene/queryparser/surround/query/SimpleTerm$MatchingTermVisitor;->visitMatchingTerm(Lorg/apache/lucene/index/Term;)V

    .line 121
    :cond_5
    invoke-virtual {v4}, Lorg/apache/lucene/index/TermsEnum;->next()Lorg/apache/lucene/util/BytesRef;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v5

    goto :goto_0

    .line 123
    .end local v2    # "status":Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    .end local v4    # "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    .end local v5    # "text":Lorg/apache/lucene/util/BytesRef;
    .end local v6    # "textString":Ljava/lang/String;
    :catchall_0
    move-exception v7

    .line 124
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->reset()Ljava/util/regex/Matcher;

    .line 125
    throw v7
.end method
