.class public Lorg/apache/lucene/queryparser/xml/builders/FilteredQueryBuilder;
.super Ljava/lang/Object;
.source "FilteredQueryBuilder.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/xml/QueryBuilder;


# instance fields
.field private final filterFactory:Lorg/apache/lucene/queryparser/xml/FilterBuilder;

.field private final queryFactory:Lorg/apache/lucene/queryparser/xml/QueryBuilder;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/queryparser/xml/FilterBuilder;Lorg/apache/lucene/queryparser/xml/QueryBuilder;)V
    .locals 0
    .param p1, "filterFactory"    # Lorg/apache/lucene/queryparser/xml/FilterBuilder;
    .param p2, "queryFactory"    # Lorg/apache/lucene/queryparser/xml/QueryBuilder;

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lorg/apache/lucene/queryparser/xml/builders/FilteredQueryBuilder;->filterFactory:Lorg/apache/lucene/queryparser/xml/FilterBuilder;

    .line 42
    iput-object p2, p0, Lorg/apache/lucene/queryparser/xml/builders/FilteredQueryBuilder;->queryFactory:Lorg/apache/lucene/queryparser/xml/QueryBuilder;

    .line 44
    return-void
.end method


# virtual methods
.method public getQuery(Lorg/w3c/dom/Element;)Lorg/apache/lucene/search/Query;
    .locals 7
    .param p1, "e"    # Lorg/w3c/dom/Element;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/xml/ParserException;
        }
    .end annotation

    .prologue
    .line 51
    const-string v5, "Filter"

    invoke-static {p1, v5}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getChildByTagOrFail(Lorg/w3c/dom/Element;Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v1

    .line 52
    .local v1, "filterElement":Lorg/w3c/dom/Element;
    invoke-static {v1}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getFirstChildOrFail(Lorg/w3c/dom/Element;)Lorg/w3c/dom/Element;

    move-result-object v1

    .line 53
    iget-object v5, p0, Lorg/apache/lucene/queryparser/xml/builders/FilteredQueryBuilder;->filterFactory:Lorg/apache/lucene/queryparser/xml/FilterBuilder;

    invoke-interface {v5, v1}, Lorg/apache/lucene/queryparser/xml/FilterBuilder;->getFilter(Lorg/w3c/dom/Element;)Lorg/apache/lucene/search/Filter;

    move-result-object v0

    .line 55
    .local v0, "f":Lorg/apache/lucene/search/Filter;
    const-string v5, "Query"

    invoke-static {p1, v5}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getChildByTagOrFail(Lorg/w3c/dom/Element;Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v4

    .line 56
    .local v4, "queryElement":Lorg/w3c/dom/Element;
    invoke-static {v4}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getFirstChildOrFail(Lorg/w3c/dom/Element;)Lorg/w3c/dom/Element;

    move-result-object v4

    .line 57
    iget-object v5, p0, Lorg/apache/lucene/queryparser/xml/builders/FilteredQueryBuilder;->queryFactory:Lorg/apache/lucene/queryparser/xml/QueryBuilder;

    invoke-interface {v5, v4}, Lorg/apache/lucene/queryparser/xml/QueryBuilder;->getQuery(Lorg/w3c/dom/Element;)Lorg/apache/lucene/search/Query;

    move-result-object v3

    .line 59
    .local v3, "q":Lorg/apache/lucene/search/Query;
    new-instance v2, Lorg/apache/lucene/search/FilteredQuery;

    invoke-direct {v2, v3, v0}, Lorg/apache/lucene/search/FilteredQuery;-><init>(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;)V

    .line 60
    .local v2, "fq":Lorg/apache/lucene/search/FilteredQuery;
    const-string v5, "boost"

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-static {p1, v5, v6}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttribute(Lorg/w3c/dom/Element;Ljava/lang/String;F)F

    move-result v5

    invoke-virtual {v2, v5}, Lorg/apache/lucene/search/FilteredQuery;->setBoost(F)V

    .line 61
    return-object v2
.end method
