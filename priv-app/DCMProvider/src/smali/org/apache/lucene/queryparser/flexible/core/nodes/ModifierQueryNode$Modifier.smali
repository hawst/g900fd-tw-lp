.class public final enum Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;
.super Ljava/lang/Enum;
.source "ModifierQueryNode.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Modifier"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;",
        ">;"
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$org$apache$lucene$queryparser$flexible$core$nodes$ModifierQueryNode$Modifier:[I

.field private static final synthetic ENUM$VALUES:[Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

.field public static final enum MOD_NONE:Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

.field public static final enum MOD_NOT:Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

.field public static final enum MOD_REQ:Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;


# direct methods
.method static synthetic $SWITCH_TABLE$org$apache$lucene$queryparser$flexible$core$nodes$ModifierQueryNode$Modifier()[I
    .locals 3

    .prologue
    .line 42
    sget-object v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;->$SWITCH_TABLE$org$apache$lucene$queryparser$flexible$core$nodes$ModifierQueryNode$Modifier:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;->values()[Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;->MOD_NONE:Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    invoke-virtual {v1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;->MOD_NOT:Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    invoke-virtual {v1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;->MOD_REQ:Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    invoke-virtual {v1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;->$SWITCH_TABLE$org$apache$lucene$queryparser$flexible$core$nodes$ModifierQueryNode$Modifier:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 43
    new-instance v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    const-string v1, "MOD_NONE"

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;->MOD_NONE:Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    new-instance v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    const-string v1, "MOD_NOT"

    invoke-direct {v0, v1, v3}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;->MOD_NOT:Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    new-instance v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    const-string v1, "MOD_REQ"

    invoke-direct {v0, v1, v4}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;->MOD_REQ:Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    .line 42
    const/4 v0, 0x3

    new-array v0, v0, [Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    sget-object v1, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;->MOD_NONE:Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    aput-object v1, v0, v2

    sget-object v1, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;->MOD_NOT:Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    aput-object v1, v0, v3

    sget-object v1, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;->MOD_REQ:Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    aput-object v1, v0, v4

    sput-object v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;->ENUM$VALUES:[Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    return-object v0
.end method

.method public static values()[Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;->ENUM$VALUES:[Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    array-length v1, v0

    new-array v2, v1, [Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public toDigitString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 60
    invoke-static {}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;->$SWITCH_TABLE$org$apache$lucene$queryparser$flexible$core$nodes$ModifierQueryNode$Modifier()[I

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 69
    const-string v0, ""

    :goto_0
    return-object v0

    .line 62
    :pswitch_0
    const-string v0, ""

    goto :goto_0

    .line 64
    :pswitch_1
    const-string v0, "-"

    goto :goto_0

    .line 66
    :pswitch_2
    const-string v0, "+"

    goto :goto_0

    .line 60
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public toLargeString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 73
    invoke-static {}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;->$SWITCH_TABLE$org$apache$lucene$queryparser$flexible$core$nodes$ModifierQueryNode$Modifier()[I

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 82
    const-string v0, ""

    :goto_0
    return-object v0

    .line 75
    :pswitch_0
    const-string v0, ""

    goto :goto_0

    .line 77
    :pswitch_1
    const-string v0, "NOT "

    goto :goto_0

    .line 79
    :pswitch_2
    const-string v0, "+"

    goto :goto_0

    .line 73
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 47
    invoke-static {}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;->$SWITCH_TABLE$org$apache$lucene$queryparser$flexible$core$nodes$ModifierQueryNode$Modifier()[I

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 56
    const-string v0, "MOD_DEFAULT"

    :goto_0
    return-object v0

    .line 49
    :pswitch_0
    const-string v0, "MOD_NONE"

    goto :goto_0

    .line 51
    :pswitch_1
    const-string v0, "MOD_NOT"

    goto :goto_0

    .line 53
    :pswitch_2
    const-string v0, "MOD_REQ"

    goto :goto_0

    .line 47
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
