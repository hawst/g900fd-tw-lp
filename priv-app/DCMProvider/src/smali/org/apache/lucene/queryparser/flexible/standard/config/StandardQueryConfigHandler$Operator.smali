.class public final enum Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$Operator;
.super Ljava/lang/Enum;
.source "StandardQueryConfigHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Operator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$Operator;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum AND:Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$Operator;

.field private static final synthetic ENUM$VALUES:[Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$Operator;

.field public static final enum OR:Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$Operator;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 192
    new-instance v0, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$Operator;

    const-string v1, "AND"

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$Operator;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$Operator;->AND:Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$Operator;

    new-instance v0, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$Operator;

    const-string v1, "OR"

    invoke-direct {v0, v1, v3}, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$Operator;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$Operator;->OR:Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$Operator;

    .line 191
    const/4 v0, 0x2

    new-array v0, v0, [Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$Operator;

    sget-object v1, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$Operator;->AND:Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$Operator;

    aput-object v1, v0, v2

    sget-object v1, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$Operator;->OR:Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$Operator;

    aput-object v1, v0, v3

    sput-object v0, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$Operator;->ENUM$VALUES:[Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$Operator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 191
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$Operator;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$Operator;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$Operator;

    return-object v0
.end method

.method public static values()[Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$Operator;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$Operator;->ENUM$VALUES:[Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$Operator;

    array-length v1, v0

    new-array v2, v1, [Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$Operator;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
