.class public Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;
.super Ljava/lang/Object;
.source "StandardSyntaxParserTokenManager.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserConstants;


# static fields
.field static final jjbitVec0:[J

.field static final jjbitVec1:[J

.field static final jjbitVec3:[J

.field static final jjbitVec4:[J

.field public static final jjnewLexState:[I

.field static final jjnextStates:[I

.field public static final jjstrLiteralImages:[Ljava/lang/String;

.field static final jjtoSkip:[J

.field static final jjtoToken:[J

.field public static final lexStateNames:[Ljava/lang/String;


# instance fields
.field protected curChar:C

.field curLexState:I

.field public debugStream:Ljava/io/PrintStream;

.field defaultLexState:I

.field protected input_stream:Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;

.field jjmatchedKind:I

.field jjmatchedPos:I

.field jjnewStateCnt:I

.field jjround:I

.field private final jjrounds:[I

.field private final jjstateSet:[I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x0

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, -0x1

    .line 119
    new-array v0, v8, [J

    .line 120
    const-wide/16 v2, 0x1

    aput-wide v2, v0, v7

    .line 119
    sput-object v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjbitVec0:[J

    .line 122
    new-array v0, v8, [J

    fill-array-data v0, :array_0

    sput-object v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjbitVec1:[J

    .line 125
    new-array v0, v8, [J

    .line 126
    const-wide/16 v2, -0x1

    aput-wide v2, v0, v6

    const/4 v1, 0x3

    const-wide/16 v2, -0x1

    aput-wide v2, v0, v1

    .line 125
    sput-object v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjbitVec3:[J

    .line 128
    new-array v0, v8, [J

    fill-array-data v0, :array_1

    sput-object v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjbitVec4:[J

    .line 720
    const/16 v0, 0xd

    new-array v0, v0, [I

    .line 721
    const/16 v1, 0x1d

    aput v1, v0, v7

    const/16 v1, 0x1f

    aput v1, v0, v5

    const/16 v1, 0x20

    aput v1, v0, v6

    const/4 v1, 0x3

    const/16 v2, 0xf

    aput v2, v0, v1

    const/16 v1, 0x10

    aput v1, v0, v8

    const/4 v1, 0x5

    const/16 v2, 0x12

    aput v2, v0, v1

    const/4 v1, 0x6

    const/16 v2, 0x19

    aput v2, v0, v1

    const/4 v1, 0x7

    const/16 v2, 0x1a

    aput v2, v0, v1

    const/16 v1, 0x9

    aput v5, v0, v1

    const/16 v1, 0xa

    aput v6, v0, v1

    const/16 v1, 0xb

    aput v8, v0, v1

    const/16 v1, 0xc

    const/4 v2, 0x5

    aput v2, v0, v1

    .line 720
    sput-object v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjnextStates:[I

    .line 761
    const/16 v0, 0x22

    new-array v0, v0, [Ljava/lang/String;

    .line 762
    const-string v1, ""

    aput-object v1, v0, v7

    const/16 v1, 0xb

    const-string v2, "+"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "-"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    .line 763
    const-string v2, "("

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, ":"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "="

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "<"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "<="

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, ">"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, ">="

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "^"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    .line 764
    const-string v2, "["

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string/jumbo v2, "{"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "TO"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "]"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string/jumbo v2, "}"

    aput-object v2, v0, v1

    .line 761
    sput-object v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjstrLiteralImages:[Ljava/lang/String;

    .line 767
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    .line 768
    const-string v1, "Boost"

    aput-object v1, v0, v7

    .line 769
    const-string v1, "Range"

    aput-object v1, v0, v5

    .line 770
    const-string v1, "DEFAULT"

    aput-object v1, v0, v6

    .line 767
    sput-object v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->lexStateNames:[Ljava/lang/String;

    .line 774
    const/16 v0, 0x22

    new-array v0, v0, [I

    .line 775
    aput v4, v0, v7

    aput v4, v0, v5

    aput v4, v0, v6

    const/4 v1, 0x3

    aput v4, v0, v1

    aput v4, v0, v8

    const/4 v1, 0x5

    aput v4, v0, v1

    const/4 v1, 0x6

    aput v4, v0, v1

    const/4 v1, 0x7

    aput v4, v0, v1

    const/16 v1, 0x8

    aput v4, v0, v1

    const/16 v1, 0x9

    aput v4, v0, v1

    const/16 v1, 0xa

    aput v4, v0, v1

    const/16 v1, 0xb

    aput v4, v0, v1

    const/16 v1, 0xc

    aput v4, v0, v1

    const/16 v1, 0xd

    aput v4, v0, v1

    const/16 v1, 0xe

    aput v4, v0, v1

    const/16 v1, 0xf

    aput v4, v0, v1

    const/16 v1, 0x10

    aput v4, v0, v1

    const/16 v1, 0x11

    aput v4, v0, v1

    const/16 v1, 0x12

    aput v4, v0, v1

    const/16 v1, 0x13

    aput v4, v0, v1

    const/16 v1, 0x14

    aput v4, v0, v1

    const/16 v1, 0x16

    aput v4, v0, v1

    const/16 v1, 0x17

    aput v4, v0, v1

    const/16 v1, 0x18

    aput v4, v0, v1

    const/16 v1, 0x19

    .line 776
    aput v4, v0, v1

    const/16 v1, 0x1a

    aput v5, v0, v1

    const/16 v1, 0x1b

    aput v5, v0, v1

    const/16 v1, 0x1c

    aput v6, v0, v1

    const/16 v1, 0x1d

    aput v4, v0, v1

    const/16 v1, 0x1e

    aput v6, v0, v1

    const/16 v1, 0x1f

    aput v6, v0, v1

    const/16 v1, 0x20

    aput v4, v0, v1

    const/16 v1, 0x21

    aput v4, v0, v1

    .line 774
    sput-object v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjnewLexState:[I

    .line 778
    new-array v0, v5, [J

    .line 779
    const-wide v2, 0x3ffffff01L

    aput-wide v2, v0, v7

    .line 778
    sput-object v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjtoToken:[J

    .line 781
    new-array v0, v5, [J

    .line 782
    const-wide/16 v2, 0x80

    aput-wide v2, v0, v7

    .line 781
    sput-object v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjtoSkip:[J

    .line 783
    return-void

    .line 122
    :array_0
    .array-data 8
        -0x2
        -0x1
        -0x1
        -0x1
    .end array-data

    .line 128
    :array_1
    .array-data 8
        -0x1000000000002L
        -0x1
        -0x1
        -0x1
    .end array-data
.end method

.method public constructor <init>(Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;)V
    .locals 2
    .param p1, "stream"    # Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;

    .prologue
    const/4 v1, 0x2

    .line 789
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    iput-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->debugStream:Ljava/io/PrintStream;

    .line 785
    const/16 v0, 0x21

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjrounds:[I

    .line 786
    const/16 v0, 0x42

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjstateSet:[I

    .line 855
    iput v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curLexState:I

    .line 856
    iput v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->defaultLexState:I

    .line 790
    iput-object p1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;

    .line 791
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;I)V
    .locals 0
    .param p1, "stream"    # Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;
    .param p2, "lexState"    # I

    .prologue
    .line 795
    invoke-direct {p0, p1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;-><init>(Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;)V

    .line 796
    invoke-virtual {p0, p2}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->SwitchTo(I)V

    .line 797
    return-void
.end method

.method private ReInitRounds()V
    .locals 4

    .prologue
    .line 810
    const v2, -0x7fffffff

    iput v2, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjround:I

    .line 811
    const/16 v0, 0x21

    .local v0, "i":I
    move v1, v0

    .end local v0    # "i":I
    .local v1, "i":I
    :goto_0
    add-int/lit8 v0, v1, -0x1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    if-gtz v1, :cond_0

    .line 813
    return-void

    .line 812
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjrounds:[I

    const/high16 v3, -0x80000000

    aput v3, v2, v0

    move v1, v0

    .end local v0    # "i":I
    .restart local v1    # "i":I
    goto :goto_0
.end method

.method private jjAddStates(II)V
    .locals 4
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 952
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjstateSet:[I

    iget v2, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjnewStateCnt:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjnewStateCnt:I

    sget-object v3, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjnextStates:[I

    aget v3, v3, p1

    aput v3, v1, v2

    .line 953
    add-int/lit8 v0, p1, 0x1

    .end local p1    # "start":I
    .local v0, "start":I
    if-ne p1, p2, :cond_0

    .line 954
    return-void

    :cond_0
    move p1, v0

    .end local v0    # "start":I
    .restart local p1    # "start":I
    goto :goto_0
.end method

.method private static final jjCanMove_0(IIIJJ)Z
    .locals 7
    .param p0, "hiByte"    # I
    .param p1, "i1"    # I
    .param p2, "i2"    # I
    .param p3, "l1"    # J
    .param p5, "l2"    # J

    .prologue
    const/4 v0, 0x0

    .line 725
    packed-switch p0, :pswitch_data_0

    .line 730
    :cond_0
    :goto_0
    return v0

    .line 728
    :pswitch_0
    sget-object v1, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjbitVec0:[J

    aget-wide v2, v1, p2

    and-long/2addr v2, p5

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    .line 725
    :pswitch_data_0
    .packed-switch 0x30
        :pswitch_0
    .end packed-switch
.end method

.method private static final jjCanMove_1(IIIJJ)Z
    .locals 7
    .param p0, "hiByte"    # I
    .param p1, "i1"    # I
    .param p2, "i2"    # I
    .param p3, "l1"    # J
    .param p5, "l2"    # J

    .prologue
    const-wide/16 v4, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 735
    packed-switch p0, :pswitch_data_0

    .line 740
    sget-object v2, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjbitVec1:[J

    aget-wide v2, v2, p1

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 742
    :cond_0
    :goto_0
    return v0

    .line 738
    :pswitch_0
    sget-object v2, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjbitVec3:[J

    aget-wide v2, v2, p2

    and-long/2addr v2, p5

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 742
    goto :goto_0

    .line 735
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method private static final jjCanMove_2(IIIJJ)Z
    .locals 7
    .param p0, "hiByte"    # I
    .param p1, "i1"    # I
    .param p2, "i2"    # I
    .param p3, "l1"    # J
    .param p5, "l2"    # J

    .prologue
    const-wide/16 v4, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 747
    sparse-switch p0, :sswitch_data_0

    .line 754
    sget-object v2, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjbitVec4:[J

    aget-wide v2, v2, p1

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 756
    :cond_0
    :goto_0
    return v0

    .line 750
    :sswitch_0
    sget-object v2, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjbitVec3:[J

    aget-wide v2, v2, p2

    and-long/2addr v2, p5

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 752
    :sswitch_1
    sget-object v2, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjbitVec1:[J

    aget-wide v2, v2, p2

    and-long/2addr v2, p5

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 756
    goto :goto_0

    .line 747
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x30 -> :sswitch_1
    .end sparse-switch
.end method

.method private jjCheckNAdd(I)V
    .locals 3
    .param p1, "state"    # I

    .prologue
    .line 943
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjrounds:[I

    aget v0, v0, p1

    iget v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjround:I

    if-eq v0, v1, :cond_0

    .line 945
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjstateSet:[I

    iget v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjnewStateCnt:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjnewStateCnt:I

    aput p1, v0, v1

    .line 946
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjrounds:[I

    iget v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjround:I

    aput v1, v0, p1

    .line 948
    :cond_0
    return-void
.end method

.method private jjCheckNAddStates(II)V
    .locals 2
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 964
    :goto_0
    sget-object v1, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjnextStates:[I

    aget v1, v1, p1

    invoke-direct {p0, v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjCheckNAdd(I)V

    .line 965
    add-int/lit8 v0, p1, 0x1

    .end local p1    # "start":I
    .local v0, "start":I
    if-ne p1, p2, :cond_0

    .line 966
    return-void

    :cond_0
    move p1, v0

    .end local v0    # "start":I
    .restart local p1    # "start":I
    goto :goto_0
.end method

.method private jjCheckNAddTwoStates(II)V
    .locals 0
    .param p1, "state1"    # I
    .param p2, "state2"    # I

    .prologue
    .line 957
    invoke-direct {p0, p1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjCheckNAdd(I)V

    .line 958
    invoke-direct {p0, p2}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjCheckNAdd(I)V

    .line 959
    return-void
.end method

.method private jjMoveNfa_0(II)I
    .locals 20
    .param p1, "startState"    # I
    .param p2, "curPos"    # I

    .prologue
    .line 446
    const/4 v14, 0x0

    .line 447
    .local v14, "startsAt":I
    const/4 v15, 0x3

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjnewStateCnt:I

    .line 448
    const/4 v4, 0x1

    .line 449
    .local v4, "i":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjstateSet:[I

    const/16 v16, 0x0

    aput p1, v15, v16

    .line 450
    const v7, 0x7fffffff

    .line 453
    .local v7, "kind":I
    :goto_0
    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjround:I

    add-int/lit8 v15, v15, 0x1

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjround:I

    const v16, 0x7fffffff

    move/from16 v0, v16

    if-ne v15, v0, :cond_0

    .line 454
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->ReInitRounds()V

    .line 455
    :cond_0
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    const/16 v16, 0x40

    move/from16 v0, v16

    if-ge v15, v0, :cond_6

    .line 457
    const-wide/16 v16, 0x1

    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    shl-long v8, v16, v15

    .line 460
    .local v8, "l":J
    :cond_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjstateSet:[I

    add-int/lit8 v4, v4, -0x1

    aget v15, v15, v4

    packed-switch v15, :pswitch_data_0

    .line 482
    :cond_2
    :goto_1
    if-ne v4, v14, :cond_1

    .line 510
    .end local v8    # "l":J
    :goto_2
    const v15, 0x7fffffff

    if-eq v7, v15, :cond_3

    .line 512
    move-object/from16 v0, p0

    iput v7, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjmatchedKind:I

    .line 513
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjmatchedPos:I

    .line 514
    const v7, 0x7fffffff

    .line 516
    :cond_3
    add-int/lit8 p2, p2, 0x1

    .line 517
    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjnewStateCnt:I

    move-object/from16 v0, p0

    iput v14, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjnewStateCnt:I

    rsub-int/lit8 v14, v14, 0x3

    if-ne v4, v14, :cond_a

    .line 520
    :goto_3
    return p2

    .line 463
    .restart local v8    # "l":J
    :pswitch_0
    const-wide/high16 v16, 0x3ff000000000000L

    and-long v16, v16, v8

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_2

    .line 465
    const/16 v15, 0x1c

    if-le v7, v15, :cond_4

    .line 466
    const/16 v7, 0x1c

    .line 467
    :cond_4
    const/16 v15, 0x8

    const/16 v16, 0x9

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjAddStates(II)V

    goto :goto_1

    .line 470
    :pswitch_1
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    const/16 v16, 0x2e

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 471
    const/4 v15, 0x2

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjCheckNAdd(I)V

    goto :goto_1

    .line 474
    :pswitch_2
    const-wide/high16 v16, 0x3ff000000000000L

    and-long v16, v16, v8

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_2

    .line 476
    const/16 v15, 0x1c

    if-le v7, v15, :cond_5

    .line 477
    const/16 v7, 0x1c

    .line 478
    :cond_5
    const/4 v15, 0x2

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjCheckNAdd(I)V

    goto :goto_1

    .line 484
    .end local v8    # "l":J
    :cond_6
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    const/16 v16, 0x80

    move/from16 v0, v16

    if-ge v15, v0, :cond_8

    .line 486
    const-wide/16 v16, 0x1

    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    and-int/lit8 v15, v15, 0x3f

    shl-long v8, v16, v15

    .line 489
    .restart local v8    # "l":J
    :cond_7
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjstateSet:[I

    add-int/lit8 v4, v4, -0x1

    aget v15, v15, v4

    .line 493
    if-ne v4, v14, :cond_7

    goto/16 :goto_2

    .line 497
    .end local v8    # "l":J
    :cond_8
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    shr-int/lit8 v3, v15, 0x8

    .line 498
    .local v3, "hiByte":I
    shr-int/lit8 v5, v3, 0x6

    .line 499
    .local v5, "i1":I
    const-wide/16 v16, 0x1

    and-int/lit8 v15, v3, 0x3f

    shl-long v10, v16, v15

    .line 500
    .local v10, "l1":J
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    and-int/lit16 v15, v15, 0xff

    shr-int/lit8 v6, v15, 0x6

    .line 501
    .local v6, "i2":I
    const-wide/16 v16, 0x1

    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    and-int/lit8 v15, v15, 0x3f

    shl-long v12, v16, v15

    .line 504
    .local v12, "l2":J
    :cond_9
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjstateSet:[I

    add-int/lit8 v4, v4, -0x1

    aget v15, v15, v4

    .line 508
    if-ne v4, v14, :cond_9

    goto/16 :goto_2

    .line 519
    .end local v3    # "hiByte":I
    .end local v5    # "i1":I
    .end local v6    # "i2":I
    .end local v10    # "l1":J
    .end local v12    # "l2":J
    :cond_a
    :try_start_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;

    invoke-interface {v15}, Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;->readChar()C

    move-result v15

    move-object/from16 v0, p0

    iput-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 520
    :catch_0
    move-exception v2

    .local v2, "e":Ljava/io/IOException;
    goto/16 :goto_3

    .line 460
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private jjMoveNfa_1(II)I
    .locals 20
    .param p1, "startState"    # I
    .param p2, "curPos"    # I

    .prologue
    .line 584
    const/4 v14, 0x0

    .line 585
    .local v14, "startsAt":I
    const/4 v15, 0x7

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjnewStateCnt:I

    .line 586
    const/4 v10, 0x1

    .line 587
    .local v10, "i":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjstateSet:[I

    const/16 v16, 0x0

    aput p1, v15, v16

    .line 588
    const v11, 0x7fffffff

    .line 591
    .local v11, "kind":I
    :goto_0
    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjround:I

    add-int/lit8 v15, v15, 0x1

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjround:I

    const v16, 0x7fffffff

    move/from16 v0, v16

    if-ne v15, v0, :cond_0

    .line 592
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->ReInitRounds()V

    .line 593
    :cond_0
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    const/16 v16, 0x40

    move/from16 v0, v16

    if-ge v15, v0, :cond_8

    .line 595
    const-wide/16 v16, 0x1

    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    shl-long v12, v16, v15

    .line 598
    .local v12, "l":J
    :cond_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjstateSet:[I

    add-int/lit8 v10, v10, -0x1

    aget v15, v15, v10

    packed-switch v15, :pswitch_data_0

    .line 640
    :cond_2
    :goto_1
    :pswitch_0
    if-ne v10, v14, :cond_1

    .line 707
    .end local v12    # "l":J
    :goto_2
    const v15, 0x7fffffff

    if-eq v11, v15, :cond_3

    .line 709
    move-object/from16 v0, p0

    iput v11, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjmatchedKind:I

    .line 710
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjmatchedPos:I

    .line 711
    const v11, 0x7fffffff

    .line 713
    :cond_3
    add-int/lit8 p2, p2, 0x1

    .line 714
    move-object/from16 v0, p0

    iget v10, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjnewStateCnt:I

    move-object/from16 v0, p0

    iput v14, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjnewStateCnt:I

    rsub-int/lit8 v14, v14, 0x7

    if-ne v10, v14, :cond_12

    .line 717
    :goto_3
    return p2

    .line 601
    .restart local v12    # "l":J
    :pswitch_1
    const-wide v16, -0x100000001L

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_5

    .line 603
    const/16 v15, 0x21

    if-le v11, v15, :cond_4

    .line 604
    const/16 v11, 0x21

    .line 605
    :cond_4
    const/4 v15, 0x6

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjCheckNAdd(I)V

    .line 607
    :cond_5
    const-wide v16, 0x100002600L    # 2.122000597E-314

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_6

    .line 609
    const/4 v15, 0x7

    if-le v11, v15, :cond_2

    .line 610
    const/4 v11, 0x7

    .line 611
    goto :goto_1

    .line 612
    :cond_6
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    const/16 v16, 0x22

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 613
    const/4 v15, 0x2

    const/16 v16, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto :goto_1

    .line 616
    :pswitch_2
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    const/16 v16, 0x22

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 617
    const/4 v15, 0x2

    const/16 v16, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto :goto_1

    .line 620
    :pswitch_3
    const-wide v16, -0x400000001L

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_2

    .line 621
    const/16 v15, 0xa

    const/16 v16, 0xc

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 624
    :pswitch_4
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    const/16 v16, 0x22

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 625
    const/16 v15, 0xa

    const/16 v16, 0xc

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 628
    :pswitch_5
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    const/16 v16, 0x22

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x20

    if-le v11, v15, :cond_2

    .line 629
    const/16 v11, 0x20

    .line 630
    goto/16 :goto_1

    .line 632
    :pswitch_6
    const-wide v16, -0x100000001L

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_2

    .line 634
    const/16 v15, 0x21

    if-le v11, v15, :cond_7

    .line 635
    const/16 v11, 0x21

    .line 636
    :cond_7
    const/4 v15, 0x6

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_1

    .line 642
    .end local v12    # "l":J
    :cond_8
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    const/16 v16, 0x80

    move/from16 v0, v16

    if-ge v15, v0, :cond_c

    .line 644
    const-wide/16 v16, 0x1

    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    and-int/lit8 v15, v15, 0x3f

    shl-long v12, v16, v15

    .line 647
    .restart local v12    # "l":J
    :cond_9
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjstateSet:[I

    add-int/lit8 v10, v10, -0x1

    aget v15, v15, v10

    packed-switch v15, :pswitch_data_1

    .line 666
    :cond_a
    :goto_4
    :pswitch_7
    if-ne v10, v14, :cond_9

    goto/16 :goto_2

    .line 651
    :pswitch_8
    const-wide v16, -0x2000000020000001L    # -2.6815614261549933E154

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_a

    .line 653
    const/16 v15, 0x21

    if-le v11, v15, :cond_b

    .line 654
    const/16 v11, 0x21

    .line 655
    :cond_b
    const/4 v15, 0x6

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjCheckNAdd(I)V

    goto :goto_4

    .line 658
    :pswitch_9
    const/16 v15, 0xa

    const/16 v16, 0xc

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjAddStates(II)V

    goto :goto_4

    .line 661
    :pswitch_a
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    const/16 v16, 0x5c

    move/from16 v0, v16

    if-ne v15, v0, :cond_a

    .line 662
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x3

    aput v17, v15, v16

    goto :goto_4

    .line 670
    .end local v12    # "l":J
    :cond_c
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    shr-int/lit8 v3, v15, 0x8

    .line 671
    .local v3, "hiByte":I
    shr-int/lit8 v4, v3, 0x6

    .line 672
    .local v4, "i1":I
    const-wide/16 v16, 0x1

    and-int/lit8 v15, v3, 0x3f

    shl-long v6, v16, v15

    .line 673
    .local v6, "l1":J
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    and-int/lit16 v15, v15, 0xff

    shr-int/lit8 v5, v15, 0x6

    .line 674
    .local v5, "i2":I
    const-wide/16 v16, 0x1

    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    and-int/lit8 v15, v15, 0x3f

    shl-long v8, v16, v15

    .line 677
    .local v8, "l2":J
    :cond_d
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjstateSet:[I

    add-int/lit8 v10, v10, -0x1

    aget v15, v15, v10

    sparse-switch v15, :sswitch_data_0

    .line 705
    :cond_e
    :goto_5
    if-ne v10, v14, :cond_d

    goto/16 :goto_2

    .line 680
    :sswitch_0
    invoke-static/range {v3 .. v9}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjCanMove_0(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_f

    .line 682
    const/4 v15, 0x7

    if-le v11, v15, :cond_f

    .line 683
    const/4 v11, 0x7

    .line 685
    :cond_f
    invoke-static/range {v3 .. v9}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjCanMove_1(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_e

    .line 687
    const/16 v15, 0x21

    if-le v11, v15, :cond_10

    .line 688
    const/16 v11, 0x21

    .line 689
    :cond_10
    const/4 v15, 0x6

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjCheckNAdd(I)V

    goto :goto_5

    .line 693
    :sswitch_1
    invoke-static/range {v3 .. v9}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjCanMove_1(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_e

    .line 694
    const/16 v15, 0xa

    const/16 v16, 0xc

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjAddStates(II)V

    goto :goto_5

    .line 697
    :sswitch_2
    invoke-static/range {v3 .. v9}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjCanMove_1(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_e

    .line 699
    const/16 v15, 0x21

    if-le v11, v15, :cond_11

    .line 700
    const/16 v11, 0x21

    .line 701
    :cond_11
    const/4 v15, 0x6

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjCheckNAdd(I)V

    goto :goto_5

    .line 716
    .end local v3    # "hiByte":I
    .end local v4    # "i1":I
    .end local v5    # "i2":I
    .end local v6    # "l1":J
    .end local v8    # "l2":J
    :cond_12
    :try_start_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;

    invoke-interface {v15}, Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;->readChar()C

    move-result v15

    move-object/from16 v0, p0

    iput-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 717
    :catch_0
    move-exception v2

    .local v2, "e":Ljava/io/IOException;
    goto/16 :goto_3

    .line 598
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 647
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_8
        :pswitch_7
        :pswitch_9
        :pswitch_7
        :pswitch_a
        :pswitch_7
        :pswitch_8
    .end packed-switch

    .line 677
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x2 -> :sswitch_1
        0x6 -> :sswitch_2
    .end sparse-switch
.end method

.method private jjMoveNfa_2(II)I
    .locals 20
    .param p1, "startState"    # I
    .param p2, "curPos"    # I

    .prologue
    .line 133
    const/4 v14, 0x0

    .line 134
    .local v14, "startsAt":I
    const/16 v15, 0x21

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjnewStateCnt:I

    .line 135
    const/4 v10, 0x1

    .line 136
    .local v10, "i":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjstateSet:[I

    const/16 v16, 0x0

    aput p1, v15, v16

    .line 137
    const v11, 0x7fffffff

    .line 140
    .local v11, "kind":I
    :goto_0
    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjround:I

    add-int/lit8 v15, v15, 0x1

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjround:I

    const v16, 0x7fffffff

    move/from16 v0, v16

    if-ne v15, v0, :cond_0

    .line 141
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->ReInitRounds()V

    .line 142
    :cond_0
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    const/16 v16, 0x40

    move/from16 v0, v16

    if-ge v15, v0, :cond_f

    .line 144
    const-wide/16 v16, 0x1

    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    shl-long v12, v16, v15

    .line 147
    .local v12, "l":J
    :cond_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjstateSet:[I

    add-int/lit8 v10, v10, -0x1

    aget v15, v15, v10

    packed-switch v15, :pswitch_data_0

    .line 252
    :cond_2
    :goto_1
    :pswitch_0
    if-ne v10, v14, :cond_1

    .line 427
    .end local v12    # "l":J
    :goto_2
    const v15, 0x7fffffff

    if-eq v11, v15, :cond_3

    .line 429
    move-object/from16 v0, p0

    iput v11, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjmatchedKind:I

    .line 430
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjmatchedPos:I

    .line 431
    const v11, 0x7fffffff

    .line 433
    :cond_3
    add-int/lit8 p2, p2, 0x1

    .line 434
    move-object/from16 v0, p0

    iget v10, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjnewStateCnt:I

    move-object/from16 v0, p0

    iput v14, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjnewStateCnt:I

    rsub-int/lit8 v14, v14, 0x21

    if-ne v10, v14, :cond_24

    .line 437
    :goto_3
    return p2

    .line 150
    .restart local v12    # "l":J
    :pswitch_1
    const-wide v16, -0x7400ab0700002601L    # -6.837710280313349E-251

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_6

    .line 152
    const/16 v15, 0x17

    if-le v11, v15, :cond_4

    .line 153
    const/16 v11, 0x17

    .line 154
    :cond_4
    const/16 v15, 0x14

    const/16 v16, 0x15

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjCheckNAddTwoStates(II)V

    .line 170
    :cond_5
    :goto_4
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    const/16 v16, 0x26

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 171
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x4

    aput v17, v15, v16

    goto :goto_1

    .line 156
    :cond_6
    const-wide v16, 0x100002600L    # 2.122000597E-314

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_7

    .line 158
    const/4 v15, 0x7

    if-le v11, v15, :cond_5

    .line 159
    const/4 v11, 0x7

    .line 160
    goto :goto_4

    .line 161
    :cond_7
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    const/16 v16, 0x2f

    move/from16 v0, v16

    if-ne v15, v0, :cond_8

    .line 162
    const/4 v15, 0x0

    const/16 v16, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjCheckNAddStates(II)V

    goto :goto_4

    .line 163
    :cond_8
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    const/16 v16, 0x22

    move/from16 v0, v16

    if-ne v15, v0, :cond_9

    .line 164
    const/4 v15, 0x3

    const/16 v16, 0x5

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjCheckNAddStates(II)V

    goto :goto_4

    .line 165
    :cond_9
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    const/16 v16, 0x21

    move/from16 v0, v16

    if-ne v15, v0, :cond_5

    .line 167
    const/16 v15, 0xa

    if-le v11, v15, :cond_5

    .line 168
    const/16 v11, 0xa

    goto :goto_4

    .line 174
    :pswitch_2
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    const/16 v16, 0x26

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x8

    if-le v11, v15, :cond_2

    .line 175
    const/16 v11, 0x8

    .line 176
    goto/16 :goto_1

    .line 178
    :pswitch_3
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    const/16 v16, 0x26

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 179
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x4

    aput v17, v15, v16

    goto/16 :goto_1

    .line 182
    :pswitch_4
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    const/16 v16, 0x21

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0xa

    if-le v11, v15, :cond_2

    .line 183
    const/16 v11, 0xa

    .line 184
    goto/16 :goto_1

    .line 186
    :pswitch_5
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    const/16 v16, 0x22

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 187
    const/4 v15, 0x3

    const/16 v16, 0x5

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 190
    :pswitch_6
    const-wide v16, -0x400000001L

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_2

    .line 191
    const/4 v15, 0x3

    const/16 v16, 0x5

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 194
    :pswitch_7
    const/4 v15, 0x3

    const/16 v16, 0x5

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 197
    :pswitch_8
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    const/16 v16, 0x22

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x16

    if-le v11, v15, :cond_2

    .line 198
    const/16 v11, 0x16

    .line 199
    goto/16 :goto_1

    .line 201
    :pswitch_9
    const-wide v16, -0x7400ab0700002601L    # -6.837710280313349E-251

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_2

    .line 203
    const/16 v15, 0x17

    if-le v11, v15, :cond_a

    .line 204
    const/16 v11, 0x17

    .line 205
    :cond_a
    const/16 v15, 0x14

    const/16 v16, 0x15

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 208
    :pswitch_a
    const-wide v16, -0x7400830700002601L    # -6.871809437975944E-251

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_2

    .line 210
    const/16 v15, 0x17

    if-le v11, v15, :cond_b

    .line 211
    const/16 v11, 0x17

    .line 212
    :cond_b
    const/16 v15, 0x14

    const/16 v16, 0x15

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 215
    :pswitch_b
    const/16 v15, 0x17

    if-le v11, v15, :cond_c

    .line 216
    const/16 v11, 0x17

    .line 217
    :cond_c
    const/16 v15, 0x14

    const/16 v16, 0x15

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 220
    :pswitch_c
    const-wide/high16 v16, 0x3ff000000000000L

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_2

    .line 222
    const/16 v15, 0x18

    if-le v11, v15, :cond_d

    .line 223
    const/16 v11, 0x18

    .line 224
    :cond_d
    const/4 v15, 0x6

    const/16 v16, 0x7

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjAddStates(II)V

    goto/16 :goto_1

    .line 227
    :pswitch_d
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    const/16 v16, 0x2e

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 228
    const/16 v15, 0x1b

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_1

    .line 231
    :pswitch_e
    const-wide/high16 v16, 0x3ff000000000000L

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_2

    .line 233
    const/16 v15, 0x18

    if-le v11, v15, :cond_e

    .line 234
    const/16 v11, 0x18

    .line 235
    :cond_e
    const/16 v15, 0x1b

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_1

    .line 239
    :pswitch_f
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    const/16 v16, 0x2f

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 240
    const/4 v15, 0x0

    const/16 v16, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 243
    :pswitch_10
    const-wide v16, -0x800000000001L

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_2

    .line 244
    const/4 v15, 0x0

    const/16 v16, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 247
    :pswitch_11
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    const/16 v16, 0x2f

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x19

    if-le v11, v15, :cond_2

    .line 248
    const/16 v11, 0x19

    .line 249
    goto/16 :goto_1

    .line 254
    .end local v12    # "l":J
    :cond_f
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    const/16 v16, 0x80

    move/from16 v0, v16

    if-ge v15, v0, :cond_1d

    .line 256
    const-wide/16 v16, 0x1

    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    and-int/lit8 v15, v15, 0x3f

    shl-long v12, v16, v15

    .line 259
    .restart local v12    # "l":J
    :cond_10
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjstateSet:[I

    add-int/lit8 v10, v10, -0x1

    aget v15, v15, v10

    packed-switch v15, :pswitch_data_1

    .line 373
    :cond_11
    :goto_5
    :pswitch_12
    if-ne v10, v14, :cond_10

    goto/16 :goto_2

    .line 262
    :pswitch_13
    const-wide v16, -0x6800000078000001L    # -4.383617718201673E-193

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_14

    .line 264
    const/16 v15, 0x17

    if-le v11, v15, :cond_12

    .line 265
    const/16 v11, 0x17

    .line 266
    :cond_12
    const/16 v15, 0x14

    const/16 v16, 0x15

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjCheckNAddTwoStates(II)V

    .line 276
    :cond_13
    :goto_6
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    const/16 v16, 0x4e

    move/from16 v0, v16

    if-ne v15, v0, :cond_17

    .line 277
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0xb

    aput v17, v15, v16

    goto :goto_5

    .line 268
    :cond_14
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    const/16 v16, 0x7e

    move/from16 v0, v16

    if-ne v15, v0, :cond_16

    .line 270
    const/16 v15, 0x18

    if-le v11, v15, :cond_15

    .line 271
    const/16 v11, 0x18

    .line 272
    :cond_15
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x19

    aput v17, v15, v16

    goto :goto_6

    .line 274
    :cond_16
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    const/16 v16, 0x5c

    move/from16 v0, v16

    if-ne v15, v0, :cond_13

    .line 275
    const/16 v15, 0x16

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjCheckNAdd(I)V

    goto :goto_6

    .line 278
    :cond_17
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    const/16 v16, 0x7c

    move/from16 v0, v16

    if-ne v15, v0, :cond_18

    .line 279
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x8

    aput v17, v15, v16

    goto/16 :goto_5

    .line 280
    :cond_18
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    const/16 v16, 0x4f

    move/from16 v0, v16

    if-ne v15, v0, :cond_19

    .line 281
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x6

    aput v17, v15, v16

    goto/16 :goto_5

    .line 282
    :cond_19
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    const/16 v16, 0x41

    move/from16 v0, v16

    if-ne v15, v0, :cond_11

    .line 283
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x2

    aput v17, v15, v16

    goto/16 :goto_5

    .line 286
    :pswitch_14
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    const/16 v16, 0x44

    move/from16 v0, v16

    if-ne v15, v0, :cond_11

    const/16 v15, 0x8

    if-le v11, v15, :cond_11

    .line 287
    const/16 v11, 0x8

    .line 288
    goto/16 :goto_5

    .line 290
    :pswitch_15
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    const/16 v16, 0x4e

    move/from16 v0, v16

    if-ne v15, v0, :cond_11

    .line 291
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x1

    aput v17, v15, v16

    goto/16 :goto_5

    .line 294
    :pswitch_16
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    const/16 v16, 0x41

    move/from16 v0, v16

    if-ne v15, v0, :cond_11

    .line 295
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x2

    aput v17, v15, v16

    goto/16 :goto_5

    .line 298
    :pswitch_17
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    const/16 v16, 0x52

    move/from16 v0, v16

    if-ne v15, v0, :cond_11

    const/16 v15, 0x9

    if-le v11, v15, :cond_11

    .line 299
    const/16 v11, 0x9

    .line 300
    goto/16 :goto_5

    .line 302
    :pswitch_18
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    const/16 v16, 0x4f

    move/from16 v0, v16

    if-ne v15, v0, :cond_11

    .line 303
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x6

    aput v17, v15, v16

    goto/16 :goto_5

    .line 306
    :pswitch_19
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    const/16 v16, 0x7c

    move/from16 v0, v16

    if-ne v15, v0, :cond_11

    const/16 v15, 0x9

    if-le v11, v15, :cond_11

    .line 307
    const/16 v11, 0x9

    .line 308
    goto/16 :goto_5

    .line 310
    :pswitch_1a
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    const/16 v16, 0x7c

    move/from16 v0, v16

    if-ne v15, v0, :cond_11

    .line 311
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x8

    aput v17, v15, v16

    goto/16 :goto_5

    .line 314
    :pswitch_1b
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    const/16 v16, 0x54

    move/from16 v0, v16

    if-ne v15, v0, :cond_11

    const/16 v15, 0xa

    if-le v11, v15, :cond_11

    .line 315
    const/16 v11, 0xa

    .line 316
    goto/16 :goto_5

    .line 318
    :pswitch_1c
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    const/16 v16, 0x4f

    move/from16 v0, v16

    if-ne v15, v0, :cond_11

    .line 319
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0xa

    aput v17, v15, v16

    goto/16 :goto_5

    .line 322
    :pswitch_1d
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    const/16 v16, 0x4e

    move/from16 v0, v16

    if-ne v15, v0, :cond_11

    .line 323
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0xb

    aput v17, v15, v16

    goto/16 :goto_5

    .line 326
    :pswitch_1e
    const-wide/32 v16, -0x10000001

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_11

    .line 327
    const/4 v15, 0x3

    const/16 v16, 0x5

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_5

    .line 330
    :pswitch_1f
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    const/16 v16, 0x5c

    move/from16 v0, v16

    if-ne v15, v0, :cond_11

    .line 331
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x11

    aput v17, v15, v16

    goto/16 :goto_5

    .line 334
    :pswitch_20
    const/4 v15, 0x3

    const/16 v16, 0x5

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_5

    .line 338
    :pswitch_21
    const-wide v16, -0x6800000078000001L    # -4.383617718201673E-193

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_11

    .line 340
    const/16 v15, 0x17

    if-le v11, v15, :cond_1a

    .line 341
    const/16 v11, 0x17

    .line 342
    :cond_1a
    const/16 v15, 0x14

    const/16 v16, 0x15

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_5

    .line 345
    :pswitch_22
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    const/16 v16, 0x5c

    move/from16 v0, v16

    if-ne v15, v0, :cond_11

    .line 346
    const/16 v15, 0x16

    const/16 v16, 0x16

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_5

    .line 349
    :pswitch_23
    const/16 v15, 0x17

    if-le v11, v15, :cond_1b

    .line 350
    const/16 v11, 0x17

    .line 351
    :cond_1b
    const/16 v15, 0x14

    const/16 v16, 0x15

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_5

    .line 354
    :pswitch_24
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    const/16 v16, 0x5c

    move/from16 v0, v16

    if-ne v15, v0, :cond_11

    .line 355
    const/16 v15, 0x16

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_5

    .line 358
    :pswitch_25
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    const/16 v16, 0x7e

    move/from16 v0, v16

    if-ne v15, v0, :cond_11

    .line 360
    const/16 v15, 0x18

    if-le v11, v15, :cond_1c

    .line 361
    const/16 v11, 0x18

    .line 362
    :cond_1c
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x19

    aput v17, v15, v16

    goto/16 :goto_5

    .line 365
    :pswitch_26
    const/4 v15, 0x0

    const/16 v16, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjAddStates(II)V

    goto/16 :goto_5

    .line 368
    :pswitch_27
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    const/16 v16, 0x5c

    move/from16 v0, v16

    if-ne v15, v0, :cond_11

    .line 369
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x1e

    aput v17, v15, v16

    goto/16 :goto_5

    .line 377
    .end local v12    # "l":J
    :cond_1d
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    shr-int/lit8 v3, v15, 0x8

    .line 378
    .local v3, "hiByte":I
    shr-int/lit8 v4, v3, 0x6

    .line 379
    .local v4, "i1":I
    const-wide/16 v16, 0x1

    and-int/lit8 v15, v3, 0x3f

    shl-long v6, v16, v15

    .line 380
    .local v6, "l1":J
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    and-int/lit16 v15, v15, 0xff

    shr-int/lit8 v5, v15, 0x6

    .line 381
    .local v5, "i2":I
    const-wide/16 v16, 0x1

    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    and-int/lit8 v15, v15, 0x3f

    shl-long v8, v16, v15

    .line 384
    .local v8, "l2":J
    :cond_1e
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjstateSet:[I

    add-int/lit8 v10, v10, -0x1

    aget v15, v15, v10

    sparse-switch v15, :sswitch_data_0

    .line 425
    :cond_1f
    :goto_7
    if-ne v10, v14, :cond_1e

    goto/16 :goto_2

    .line 387
    :sswitch_0
    invoke-static/range {v3 .. v9}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjCanMove_0(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_20

    .line 389
    const/4 v15, 0x7

    if-le v11, v15, :cond_20

    .line 390
    const/4 v11, 0x7

    .line 392
    :cond_20
    invoke-static/range {v3 .. v9}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjCanMove_2(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_1f

    .line 394
    const/16 v15, 0x17

    if-le v11, v15, :cond_21

    .line 395
    const/16 v11, 0x17

    .line 396
    :cond_21
    const/16 v15, 0x14

    const/16 v16, 0x15

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto :goto_7

    .line 401
    :sswitch_1
    invoke-static/range {v3 .. v9}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjCanMove_1(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_1f

    .line 402
    const/4 v15, 0x3

    const/16 v16, 0x5

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjCheckNAddStates(II)V

    goto :goto_7

    .line 406
    :sswitch_2
    invoke-static/range {v3 .. v9}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjCanMove_2(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_1f

    .line 408
    const/16 v15, 0x17

    if-le v11, v15, :cond_22

    .line 409
    const/16 v11, 0x17

    .line 410
    :cond_22
    const/16 v15, 0x14

    const/16 v16, 0x15

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto :goto_7

    .line 413
    :sswitch_3
    invoke-static/range {v3 .. v9}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjCanMove_1(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_1f

    .line 415
    const/16 v15, 0x17

    if-le v11, v15, :cond_23

    .line 416
    const/16 v11, 0x17

    .line 417
    :cond_23
    const/16 v15, 0x14

    const/16 v16, 0x15

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto :goto_7

    .line 420
    :sswitch_4
    invoke-static/range {v3 .. v9}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjCanMove_1(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_1f

    .line 421
    const/4 v15, 0x0

    const/16 v16, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjAddStates(II)V

    goto :goto_7

    .line 436
    .end local v3    # "hiByte":I
    .end local v4    # "i1":I
    .end local v5    # "i2":I
    .end local v6    # "l1":J
    .end local v8    # "l2":J
    :cond_24
    :try_start_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;

    invoke-interface {v15}, Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;->readChar()C

    move-result v15

    move-object/from16 v0, p0

    iput-char v15, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 437
    :catch_0
    move-exception v2

    .local v2, "e":Ljava/io/IOException;
    goto/16 :goto_3

    .line 147
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_0
        :pswitch_b
        :pswitch_0
        :pswitch_0
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_f
        :pswitch_0
        :pswitch_11
    .end packed-switch

    .line 259
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_12
        :pswitch_12
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_12
        :pswitch_12
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_12
        :pswitch_21
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_26
        :pswitch_12
        :pswitch_27
    .end packed-switch

    .line 384
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xf -> :sswitch_1
        0x11 -> :sswitch_1
        0x13 -> :sswitch_2
        0x14 -> :sswitch_2
        0x16 -> :sswitch_3
        0x1d -> :sswitch_4
    .end sparse-switch
.end method

.method private jjMoveStringLiteralDfa0_0()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 442
    invoke-direct {p0, v0, v0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjMoveNfa_0(II)I

    move-result v0

    return v0
.end method

.method private jjMoveStringLiteralDfa0_1()I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 544
    iget-char v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    sparse-switch v0, :sswitch_data_0

    .line 553
    invoke-direct {p0, v1, v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjMoveNfa_1(II)I

    move-result v0

    :goto_0
    return v0

    .line 547
    :sswitch_0
    const-wide/32 v0, 0x20000000

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjMoveStringLiteralDfa1_1(J)I

    move-result v0

    goto :goto_0

    .line 549
    :sswitch_1
    const/16 v0, 0x1e

    invoke-direct {p0, v1, v0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 551
    :sswitch_2
    const/16 v0, 0x1f

    invoke-direct {p0, v1, v0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 544
    nop

    :sswitch_data_0
    .sparse-switch
        0x54 -> :sswitch_0
        0x5d -> :sswitch_1
        0x7d -> :sswitch_2
    .end sparse-switch
.end method

.method private jjMoveStringLiteralDfa0_2()I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 69
    iget-char v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    sparse-switch v0, :sswitch_data_0

    .line 96
    invoke-direct {p0, v1, v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjMoveNfa_2(II)I

    move-result v0

    :goto_0
    return v0

    .line 72
    :sswitch_0
    const/16 v0, 0xd

    invoke-direct {p0, v1, v0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 74
    :sswitch_1
    const/16 v0, 0xe

    invoke-direct {p0, v1, v0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 76
    :sswitch_2
    const/16 v0, 0xb

    invoke-direct {p0, v1, v0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 78
    :sswitch_3
    const/16 v0, 0xc

    invoke-direct {p0, v1, v0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 80
    :sswitch_4
    const/16 v0, 0xf

    invoke-direct {p0, v1, v0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 82
    :sswitch_5
    const/16 v0, 0x11

    iput v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjmatchedKind:I

    .line 83
    const-wide/32 v0, 0x40000

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjMoveStringLiteralDfa1_2(J)I

    move-result v0

    goto :goto_0

    .line 85
    :sswitch_6
    const/16 v0, 0x10

    invoke-direct {p0, v1, v0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 87
    :sswitch_7
    const/16 v0, 0x13

    iput v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjmatchedKind:I

    .line 88
    const-wide/32 v0, 0x100000

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjMoveStringLiteralDfa1_2(J)I

    move-result v0

    goto :goto_0

    .line 90
    :sswitch_8
    const/16 v0, 0x1a

    invoke-direct {p0, v1, v0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 92
    :sswitch_9
    const/16 v0, 0x15

    invoke-direct {p0, v1, v0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 94
    :sswitch_a
    const/16 v0, 0x1b

    invoke-direct {p0, v1, v0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 69
    :sswitch_data_0
    .sparse-switch
        0x28 -> :sswitch_0
        0x29 -> :sswitch_1
        0x2b -> :sswitch_2
        0x2d -> :sswitch_3
        0x3a -> :sswitch_4
        0x3c -> :sswitch_5
        0x3d -> :sswitch_6
        0x3e -> :sswitch_7
        0x5b -> :sswitch_8
        0x5e -> :sswitch_9
        0x7b -> :sswitch_a
    .end sparse-switch
.end method

.method private jjMoveStringLiteralDfa1_1(J)I
    .locals 7
    .param p1, "active0"    # J

    .prologue
    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 558
    :try_start_0
    iget-object v2, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;

    invoke-interface {v2}, Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;->readChar()C

    move-result v2

    iput-char v2, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 563
    iget-char v2, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    packed-switch v2, :pswitch_data_0

    .line 572
    :cond_0
    invoke-direct {p0, v6, p1, p2}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjStartNfa_1(IJ)I

    move-result v1

    :goto_0
    return v1

    .line 559
    :catch_0
    move-exception v0

    .line 560
    .local v0, "e":Ljava/io/IOException;
    invoke-direct {p0, v6, p1, p2}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjStopStringLiteralDfa_1(IJ)I

    goto :goto_0

    .line 566
    .end local v0    # "e":Ljava/io/IOException;
    :pswitch_0
    const-wide/32 v2, 0x20000000

    and-long/2addr v2, p1

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 567
    const/16 v2, 0x1d

    const/4 v3, 0x6

    invoke-direct {p0, v1, v2, v3}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjStartNfaWithStates_1(III)I

    move-result v1

    goto :goto_0

    .line 563
    nop

    :pswitch_data_0
    .packed-switch 0x4f
        :pswitch_0
    .end packed-switch
.end method

.method private jjMoveStringLiteralDfa1_2(J)I
    .locals 9
    .param p1, "active0"    # J

    .prologue
    const-wide/16 v6, 0x0

    const/4 v4, 0x0

    const/4 v1, 0x1

    .line 101
    :try_start_0
    iget-object v2, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;

    invoke-interface {v2}, Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;->readChar()C

    move-result v2

    iput-char v2, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 106
    iget-char v2, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    packed-switch v2, :pswitch_data_0

    .line 117
    :cond_0
    invoke-direct {p0, v4, p1, p2}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjStartNfa_2(IJ)I

    move-result v1

    :goto_0
    return v1

    .line 102
    :catch_0
    move-exception v0

    .line 103
    .local v0, "e":Ljava/io/IOException;
    invoke-direct {p0, v4, p1, p2}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjStopStringLiteralDfa_2(IJ)I

    goto :goto_0

    .line 109
    .end local v0    # "e":Ljava/io/IOException;
    :pswitch_0
    const-wide/32 v2, 0x40000

    and-long/2addr v2, p1

    cmp-long v2, v2, v6

    if-eqz v2, :cond_1

    .line 110
    const/16 v2, 0x12

    invoke-direct {p0, v1, v2}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjStopAtPos(II)I

    move-result v1

    goto :goto_0

    .line 111
    :cond_1
    const-wide/32 v2, 0x100000

    and-long/2addr v2, p1

    cmp-long v2, v2, v6

    if-eqz v2, :cond_0

    .line 112
    const/16 v2, 0x14

    invoke-direct {p0, v1, v2}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjStopAtPos(II)I

    move-result v1

    goto :goto_0

    .line 106
    nop

    :pswitch_data_0
    .packed-switch 0x3d
        :pswitch_0
    .end packed-switch
.end method

.method private jjStartNfaWithStates_1(III)I
    .locals 2
    .param p1, "pos"    # I
    .param p2, "kind"    # I
    .param p3, "state"    # I

    .prologue
    .line 576
    iput p2, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjmatchedKind:I

    .line 577
    iput p1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjmatchedPos:I

    .line 578
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;

    invoke-interface {v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;->readChar()C

    move-result v1

    iput-char v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 580
    add-int/lit8 v1, p1, 0x1

    invoke-direct {p0, p3, v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjMoveNfa_1(II)I

    move-result v1

    :goto_0
    return v1

    .line 579
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/io/IOException;
    add-int/lit8 v1, p1, 0x1

    goto :goto_0
.end method

.method private final jjStartNfa_1(IJ)I
    .locals 2
    .param p1, "pos"    # I
    .param p2, "active0"    # J

    .prologue
    .line 540
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjStopStringLiteralDfa_1(IJ)I

    move-result v0

    add-int/lit8 v1, p1, 0x1

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjMoveNfa_1(II)I

    move-result v0

    return v0
.end method

.method private final jjStartNfa_2(IJ)I
    .locals 2
    .param p1, "pos"    # I
    .param p2, "active0"    # J

    .prologue
    .line 59
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjStopStringLiteralDfa_2(IJ)I

    move-result v0

    add-int/lit8 v1, p1, 0x1

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjMoveNfa_2(II)I

    move-result v0

    return v0
.end method

.method private jjStopAtPos(II)I
    .locals 1
    .param p1, "pos"    # I
    .param p2, "kind"    # I

    .prologue
    .line 63
    iput p2, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjmatchedKind:I

    .line 64
    iput p1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjmatchedPos:I

    .line 65
    add-int/lit8 v0, p1, 0x1

    return v0
.end method

.method private final jjStopStringLiteralDfa_1(IJ)I
    .locals 6
    .param p1, "pos"    # I
    .param p2, "active0"    # J

    .prologue
    const/4 v0, -0x1

    .line 525
    packed-switch p1, :pswitch_data_0

    .line 535
    :cond_0
    :goto_0
    return v0

    .line 528
    :pswitch_0
    const-wide/32 v2, 0x20000000

    and-long/2addr v2, p2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 530
    const/16 v0, 0x21

    iput v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjmatchedKind:I

    .line 531
    const/4 v0, 0x6

    goto :goto_0

    .line 525
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method private final jjStopStringLiteralDfa_2(IJ)I
    .locals 1
    .param p1, "pos"    # I
    .param p2, "active0"    # J

    .prologue
    .line 51
    .line 54
    const/4 v0, -0x1

    return v0
.end method


# virtual methods
.method public ReInit(Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;)V
    .locals 1
    .param p1, "stream"    # Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;

    .prologue
    .line 802
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjnewStateCnt:I

    iput v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjmatchedPos:I

    .line 803
    iget v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->defaultLexState:I

    iput v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curLexState:I

    .line 804
    iput-object p1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;

    .line 805
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->ReInitRounds()V

    .line 806
    return-void
.end method

.method public ReInit(Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;I)V
    .locals 0
    .param p1, "stream"    # Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;
    .param p2, "lexState"    # I

    .prologue
    .line 818
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->ReInit(Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;)V

    .line 819
    invoke-virtual {p0, p2}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->SwitchTo(I)V

    .line 820
    return-void
.end method

.method public SwitchTo(I)V
    .locals 3
    .param p1, "lexState"    # I

    .prologue
    .line 825
    const/4 v0, 0x3

    if-ge p1, v0, :cond_0

    if-gez p1, :cond_1

    .line 826
    :cond_0
    new-instance v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/TokenMgrError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Error: Ignoring invalid lexical state : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". State unchanged."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/queryparser/flexible/standard/parser/TokenMgrError;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 828
    :cond_1
    iput p1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curLexState:I

    .line 829
    return-void
.end method

.method public getNextToken()Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    .locals 18

    .prologue
    .line 866
    const/4 v10, 0x0

    .line 873
    .local v10, "curPos":I
    :cond_0
    :goto_0
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;

    invoke-interface {v2}, Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;->BeginToken()C

    move-result v2

    move-object/from16 v0, p0

    iput-char v2, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 882
    move-object/from16 v0, p0

    iget v2, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curLexState:I

    packed-switch v2, :pswitch_data_0

    .line 900
    :goto_1
    move-object/from16 v0, p0

    iget v2, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjmatchedKind:I

    const v4, 0x7fffffff

    if-eq v2, v4, :cond_4

    .line 902
    move-object/from16 v0, p0

    iget v2, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjmatchedPos:I

    add-int/lit8 v2, v2, 0x1

    if-ge v2, v10, :cond_1

    .line 903
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;

    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjmatchedPos:I

    sub-int v4, v10, v4

    add-int/lit8 v4, v4, -0x1

    invoke-interface {v2, v4}, Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;->backup(I)V

    .line 904
    :cond_1
    sget-object v2, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjtoToken:[J

    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjmatchedKind:I

    shr-int/lit8 v4, v4, 0x6

    aget-wide v8, v2, v4

    const-wide/16 v16, 0x1

    move-object/from16 v0, p0

    iget v2, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjmatchedKind:I

    and-int/lit8 v2, v2, 0x3f

    shl-long v16, v16, v2

    and-long v8, v8, v16

    const-wide/16 v16, 0x0

    cmp-long v2, v8, v16

    if-eqz v2, :cond_3

    .line 906
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjFillToken()Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    move-result-object v13

    .line 907
    .local v13, "matchedToken":Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    sget-object v2, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjnewLexState:[I

    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjmatchedKind:I

    aget v2, v2, v4

    const/4 v4, -0x1

    if-eq v2, v4, :cond_2

    .line 908
    sget-object v2, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjnewLexState:[I

    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjmatchedKind:I

    aget v2, v2, v4

    move-object/from16 v0, p0

    iput v2, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curLexState:I

    :cond_2
    move-object v14, v13

    .line 909
    .end local v13    # "matchedToken":Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    .local v14, "matchedToken":Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    :goto_2
    return-object v14

    .line 875
    .end local v14    # "matchedToken":Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    :catch_0
    move-exception v11

    .line 877
    .local v11, "e":Ljava/io/IOException;
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjmatchedKind:I

    .line 878
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjFillToken()Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    move-result-object v13

    .restart local v13    # "matchedToken":Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    move-object v14, v13

    .line 879
    .end local v13    # "matchedToken":Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    .restart local v14    # "matchedToken":Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    goto :goto_2

    .line 885
    .end local v11    # "e":Ljava/io/IOException;
    .end local v14    # "matchedToken":Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    :pswitch_0
    const v2, 0x7fffffff

    move-object/from16 v0, p0

    iput v2, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjmatchedKind:I

    .line 886
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjmatchedPos:I

    .line 887
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjMoveStringLiteralDfa0_0()I

    move-result v10

    .line 888
    goto :goto_1

    .line 890
    :pswitch_1
    const v2, 0x7fffffff

    move-object/from16 v0, p0

    iput v2, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjmatchedKind:I

    .line 891
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjmatchedPos:I

    .line 892
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjMoveStringLiteralDfa0_1()I

    move-result v10

    .line 893
    goto/16 :goto_1

    .line 895
    :pswitch_2
    const v2, 0x7fffffff

    move-object/from16 v0, p0

    iput v2, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjmatchedKind:I

    .line 896
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjmatchedPos:I

    .line 897
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjMoveStringLiteralDfa0_2()I

    move-result v10

    goto/16 :goto_1

    .line 913
    :cond_3
    sget-object v2, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjnewLexState:[I

    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjmatchedKind:I

    aget v2, v2, v4

    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    .line 914
    sget-object v2, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjnewLexState:[I

    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjmatchedKind:I

    aget v2, v2, v4

    move-object/from16 v0, p0

    iput v2, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curLexState:I

    goto/16 :goto_0

    .line 918
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;

    invoke-interface {v2}, Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;->getEndLine()I

    move-result v5

    .line 919
    .local v5, "error_line":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;

    invoke-interface {v2}, Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;->getEndColumn()I

    move-result v6

    .line 920
    .local v6, "error_column":I
    const/4 v7, 0x0

    .line 921
    .local v7, "error_after":Ljava/lang/String;
    const/4 v3, 0x0

    .line 922
    .local v3, "EOFSeen":Z
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;

    invoke-interface {v2}, Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;->readChar()C

    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;

    const/4 v4, 0x1

    invoke-interface {v2, v4}, Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;->backup(I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 933
    :goto_3
    if-nez v3, :cond_5

    .line 934
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;

    const/4 v4, 0x1

    invoke-interface {v2, v4}, Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;->backup(I)V

    .line 935
    const/4 v2, 0x1

    if-gt v10, v2, :cond_9

    const-string v7, ""

    .line 937
    :cond_5
    :goto_4
    new-instance v2, Lorg/apache/lucene/queryparser/flexible/standard/parser/TokenMgrError;

    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curLexState:I

    move-object/from16 v0, p0

    iget-char v8, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    const/4 v9, 0x0

    invoke-direct/range {v2 .. v9}, Lorg/apache/lucene/queryparser/flexible/standard/parser/TokenMgrError;-><init>(ZIIILjava/lang/String;CI)V

    throw v2

    .line 923
    :catch_1
    move-exception v12

    .line 924
    .local v12, "e1":Ljava/io/IOException;
    const/4 v3, 0x1

    .line 925
    const/4 v2, 0x1

    if-gt v10, v2, :cond_7

    const-string v7, ""

    .line 926
    :goto_5
    move-object/from16 v0, p0

    iget-char v2, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    const/16 v4, 0xa

    if-eq v2, v4, :cond_6

    move-object/from16 v0, p0

    iget-char v2, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->curChar:C

    const/16 v4, 0xd

    if-ne v2, v4, :cond_8

    .line 927
    :cond_6
    add-int/lit8 v5, v5, 0x1

    .line 928
    const/4 v6, 0x0

    .line 929
    goto :goto_3

    .line 925
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;

    invoke-interface {v2}, Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;->GetImage()Ljava/lang/String;

    move-result-object v7

    goto :goto_5

    .line 931
    :cond_8
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 935
    .end local v12    # "e1":Ljava/io/IOException;
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;

    invoke-interface {v2}, Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;->GetImage()Ljava/lang/String;

    move-result-object v7

    goto :goto_4

    .line 882
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected jjFillToken()Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    .locals 9

    .prologue
    .line 839
    sget-object v7, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjstrLiteralImages:[Ljava/lang/String;

    iget v8, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjmatchedKind:I

    aget-object v5, v7, v8

    .line 840
    .local v5, "im":Ljava/lang/String;
    if-nez v5, :cond_0

    iget-object v7, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;

    invoke-interface {v7}, Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;->GetImage()Ljava/lang/String;

    move-result-object v2

    .line 841
    .local v2, "curTokenImage":Ljava/lang/String;
    :goto_0
    iget-object v7, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;

    invoke-interface {v7}, Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;->getBeginLine()I

    move-result v1

    .line 842
    .local v1, "beginLine":I
    iget-object v7, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;

    invoke-interface {v7}, Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;->getBeginColumn()I

    move-result v0

    .line 843
    .local v0, "beginColumn":I
    iget-object v7, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;

    invoke-interface {v7}, Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;->getEndLine()I

    move-result v4

    .line 844
    .local v4, "endLine":I
    iget-object v7, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;

    invoke-interface {v7}, Lorg/apache/lucene/queryparser/flexible/standard/parser/CharStream;->getEndColumn()I

    move-result v3

    .line 845
    .local v3, "endColumn":I
    iget v7, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->jjmatchedKind:I

    invoke-static {v7, v2}, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->newToken(ILjava/lang/String;)Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;

    move-result-object v6

    .line 847
    .local v6, "t":Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    iput v1, v6, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->beginLine:I

    .line 848
    iput v4, v6, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->endLine:I

    .line 849
    iput v0, v6, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->beginColumn:I

    .line 850
    iput v3, v6, Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;->endColumn:I

    .line 852
    return-object v6

    .end local v0    # "beginColumn":I
    .end local v1    # "beginLine":I
    .end local v2    # "curTokenImage":Ljava/lang/String;
    .end local v3    # "endColumn":I
    .end local v4    # "endLine":I
    .end local v6    # "t":Lorg/apache/lucene/queryparser/flexible/standard/parser/Token;
    :cond_0
    move-object v2, v5

    .line 840
    goto :goto_0
.end method

.method public setDebugStream(Ljava/io/PrintStream;)V
    .locals 0
    .param p1, "ds"    # Ljava/io/PrintStream;

    .prologue
    .line 48
    iput-object p1, p0, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParserTokenManager;->debugStream:Ljava/io/PrintStream;

    return-void
.end method
