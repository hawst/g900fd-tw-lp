.class public final Lorg/apache/lucene/queryparser/flexible/standard/QueryParserUtil;
.super Ljava/lang/Object;
.source "QueryParserUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static escape(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    const/16 v4, 0x5c

    .line 191
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 192
    .local v2, "sb":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-lt v1, v3, :cond_0

    .line 203
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 193
    :cond_0
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 195
    .local v0, "c":C
    if-eq v0, v4, :cond_1

    const/16 v3, 0x2b

    if-eq v0, v3, :cond_1

    const/16 v3, 0x2d

    if-eq v0, v3, :cond_1

    const/16 v3, 0x21

    if-eq v0, v3, :cond_1

    const/16 v3, 0x28

    if-eq v0, v3, :cond_1

    const/16 v3, 0x29

    if-eq v0, v3, :cond_1

    .line 196
    const/16 v3, 0x3a

    if-eq v0, v3, :cond_1

    const/16 v3, 0x5e

    if-eq v0, v3, :cond_1

    const/16 v3, 0x5b

    if-eq v0, v3, :cond_1

    const/16 v3, 0x5d

    if-eq v0, v3, :cond_1

    const/16 v3, 0x22

    if-eq v0, v3, :cond_1

    .line 197
    const/16 v3, 0x7b

    if-eq v0, v3, :cond_1

    const/16 v3, 0x7d

    if-eq v0, v3, :cond_1

    const/16 v3, 0x7e

    if-eq v0, v3, :cond_1

    const/16 v3, 0x2a

    if-eq v0, v3, :cond_1

    const/16 v3, 0x3f

    if-eq v0, v3, :cond_1

    .line 198
    const/16 v3, 0x7c

    if-eq v0, v3, :cond_1

    const/16 v3, 0x26

    if-ne v0, v3, :cond_2

    .line 199
    :cond_1
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 201
    :cond_2
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 192
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static parse(Ljava/lang/String;[Ljava/lang/String;[Lorg/apache/lucene/search/BooleanClause$Occur;Lorg/apache/lucene/analysis/Analyzer;)Lorg/apache/lucene/search/Query;
    .locals 6
    .param p0, "query"    # Ljava/lang/String;
    .param p1, "fields"    # [Ljava/lang/String;
    .param p2, "flags"    # [Lorg/apache/lucene/search/BooleanClause$Occur;
    .param p3, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 111
    array-length v4, p1

    array-length v5, p2

    if-eq v4, v5, :cond_0

    .line 112
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "fields.length != flags.length"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 113
    :cond_0
    new-instance v0, Lorg/apache/lucene/search/BooleanQuery;

    invoke-direct {v0}, Lorg/apache/lucene/search/BooleanQuery;-><init>()V

    .line 115
    .local v0, "bQuery":Lorg/apache/lucene/search/BooleanQuery;
    new-instance v3, Lorg/apache/lucene/queryparser/flexible/standard/StandardQueryParser;

    invoke-direct {v3}, Lorg/apache/lucene/queryparser/flexible/standard/StandardQueryParser;-><init>()V

    .line 116
    .local v3, "qp":Lorg/apache/lucene/queryparser/flexible/standard/StandardQueryParser;
    invoke-virtual {v3, p3}, Lorg/apache/lucene/queryparser/flexible/standard/StandardQueryParser;->setAnalyzer(Lorg/apache/lucene/analysis/Analyzer;)V

    .line 118
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v4, p1

    if-lt v1, v4, :cond_1

    .line 126
    return-object v0

    .line 119
    :cond_1
    aget-object v4, p1, v1

    invoke-virtual {v3, p0, v4}, Lorg/apache/lucene/queryparser/flexible/standard/StandardQueryParser;->parse(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/lucene/search/Query;

    move-result-object v2

    .line 121
    .local v2, "q":Lorg/apache/lucene/search/Query;
    if-eqz v2, :cond_3

    .line 122
    instance-of v4, v2, Lorg/apache/lucene/search/BooleanQuery;

    if-eqz v4, :cond_2

    move-object v4, v2

    check-cast v4, Lorg/apache/lucene/search/BooleanQuery;

    invoke-virtual {v4}, Lorg/apache/lucene/search/BooleanQuery;->getClauses()[Lorg/apache/lucene/search/BooleanClause;

    move-result-object v4

    array-length v4, v4

    if-lez v4, :cond_3

    .line 123
    :cond_2
    aget-object v4, p2, v1

    invoke-virtual {v0, v2, v4}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 118
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static parse([Ljava/lang/String;[Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer;)Lorg/apache/lucene/search/Query;
    .locals 6
    .param p0, "queries"    # [Ljava/lang/String;
    .param p1, "fields"    # [Ljava/lang/String;
    .param p2, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 55
    array-length v4, p0

    array-length v5, p1

    if-eq v4, v5, :cond_0

    .line 56
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "queries.length != fields.length"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 57
    :cond_0
    new-instance v0, Lorg/apache/lucene/search/BooleanQuery;

    invoke-direct {v0}, Lorg/apache/lucene/search/BooleanQuery;-><init>()V

    .line 59
    .local v0, "bQuery":Lorg/apache/lucene/search/BooleanQuery;
    new-instance v3, Lorg/apache/lucene/queryparser/flexible/standard/StandardQueryParser;

    invoke-direct {v3}, Lorg/apache/lucene/queryparser/flexible/standard/StandardQueryParser;-><init>()V

    .line 60
    .local v3, "qp":Lorg/apache/lucene/queryparser/flexible/standard/StandardQueryParser;
    invoke-virtual {v3, p2}, Lorg/apache/lucene/queryparser/flexible/standard/StandardQueryParser;->setAnalyzer(Lorg/apache/lucene/analysis/Analyzer;)V

    .line 62
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v4, p1

    if-lt v1, v4, :cond_1

    .line 70
    return-object v0

    .line 63
    :cond_1
    aget-object v4, p0, v1

    aget-object v5, p1, v1

    invoke-virtual {v3, v4, v5}, Lorg/apache/lucene/queryparser/flexible/standard/StandardQueryParser;->parse(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/lucene/search/Query;

    move-result-object v2

    .line 65
    .local v2, "q":Lorg/apache/lucene/search/Query;
    if-eqz v2, :cond_3

    .line 66
    instance-of v4, v2, Lorg/apache/lucene/search/BooleanQuery;

    if-eqz v4, :cond_2

    move-object v4, v2

    check-cast v4, Lorg/apache/lucene/search/BooleanQuery;

    invoke-virtual {v4}, Lorg/apache/lucene/search/BooleanQuery;->getClauses()[Lorg/apache/lucene/search/BooleanClause;

    move-result-object v4

    array-length v4, v4

    if-lez v4, :cond_3

    .line 67
    :cond_2
    sget-object v4, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v0, v2, v4}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 62
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static parse([Ljava/lang/String;[Ljava/lang/String;[Lorg/apache/lucene/search/BooleanClause$Occur;Lorg/apache/lucene/analysis/Analyzer;)Lorg/apache/lucene/search/Query;
    .locals 6
    .param p0, "queries"    # [Ljava/lang/String;
    .param p1, "fields"    # [Ljava/lang/String;
    .param p2, "flags"    # [Lorg/apache/lucene/search/BooleanClause$Occur;
    .param p3, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 167
    array-length v4, p0

    array-length v5, p1

    if-ne v4, v5, :cond_0

    array-length v4, p0

    array-length v5, p2

    if-eq v4, v5, :cond_1

    .line 168
    :cond_0
    new-instance v4, Ljava/lang/IllegalArgumentException;

    .line 169
    const-string v5, "queries, fields, and flags array have have different length"

    .line 168
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 170
    :cond_1
    new-instance v0, Lorg/apache/lucene/search/BooleanQuery;

    invoke-direct {v0}, Lorg/apache/lucene/search/BooleanQuery;-><init>()V

    .line 172
    .local v0, "bQuery":Lorg/apache/lucene/search/BooleanQuery;
    new-instance v3, Lorg/apache/lucene/queryparser/flexible/standard/StandardQueryParser;

    invoke-direct {v3}, Lorg/apache/lucene/queryparser/flexible/standard/StandardQueryParser;-><init>()V

    .line 173
    .local v3, "qp":Lorg/apache/lucene/queryparser/flexible/standard/StandardQueryParser;
    invoke-virtual {v3, p3}, Lorg/apache/lucene/queryparser/flexible/standard/StandardQueryParser;->setAnalyzer(Lorg/apache/lucene/analysis/Analyzer;)V

    .line 175
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v4, p1

    if-lt v1, v4, :cond_2

    .line 183
    return-object v0

    .line 176
    :cond_2
    aget-object v4, p0, v1

    aget-object v5, p1, v1

    invoke-virtual {v3, v4, v5}, Lorg/apache/lucene/queryparser/flexible/standard/StandardQueryParser;->parse(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/lucene/search/Query;

    move-result-object v2

    .line 178
    .local v2, "q":Lorg/apache/lucene/search/Query;
    if-eqz v2, :cond_4

    .line 179
    instance-of v4, v2, Lorg/apache/lucene/search/BooleanQuery;

    if-eqz v4, :cond_3

    move-object v4, v2

    check-cast v4, Lorg/apache/lucene/search/BooleanQuery;

    invoke-virtual {v4}, Lorg/apache/lucene/search/BooleanQuery;->getClauses()[Lorg/apache/lucene/search/BooleanClause;

    move-result-object v4

    array-length v4, v4

    if-lez v4, :cond_4

    .line 180
    :cond_3
    aget-object v4, p2, v1

    invoke-virtual {v0, v2, v4}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 175
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
