.class public Lorg/apache/lucene/queryparser/flexible/standard/processors/BoostQueryNodeProcessor;
.super Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl;
.source "BoostQueryNodeProcessor.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl;-><init>()V

    return-void
.end method


# virtual methods
.method protected postProcessNode(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 7
    .param p1, "node"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 46
    instance-of v5, p1, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldableNode;

    if-eqz v5, :cond_1

    .line 47
    invoke-interface {p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->getParent()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-interface {p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->getParent()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v5

    instance-of v5, v5, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldableNode;

    if-nez v5, :cond_1

    :cond_0
    move-object v4, p1

    .line 49
    check-cast v4, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldableNode;

    .line 50
    .local v4, "fieldNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldableNode;
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/processors/BoostQueryNodeProcessor;->getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    move-result-object v1

    .line 52
    .local v1, "config":Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;
    if-eqz v1, :cond_1

    .line 53
    invoke-interface {v4}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldableNode;->getField()Ljava/lang/CharSequence;

    move-result-object v2

    .line 54
    .local v2, "field":Ljava/lang/CharSequence;
    invoke-static {v2}, Lorg/apache/lucene/queryparser/flexible/core/util/StringUtils;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->getFieldConfig(Ljava/lang/String;)Lorg/apache/lucene/queryparser/flexible/core/config/FieldConfig;

    move-result-object v3

    .line 56
    .local v3, "fieldConfig":Lorg/apache/lucene/queryparser/flexible/core/config/FieldConfig;
    if-eqz v3, :cond_1

    .line 57
    sget-object v5, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->BOOST:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-virtual {v3, v5}, Lorg/apache/lucene/queryparser/flexible/core/config/FieldConfig;->get(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    .line 59
    .local v0, "boost":Ljava/lang/Float;
    if-eqz v0, :cond_1

    .line 60
    new-instance v5, Lorg/apache/lucene/queryparser/flexible/core/nodes/BoostQueryNode;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v6

    invoke-direct {v5, p1, v6}, Lorg/apache/lucene/queryparser/flexible/core/nodes/BoostQueryNode;-><init>(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;F)V

    move-object p1, v5

    .line 69
    .end local v0    # "boost":Ljava/lang/Float;
    .end local v1    # "config":Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;
    .end local v2    # "field":Ljava/lang/CharSequence;
    .end local v3    # "fieldConfig":Lorg/apache/lucene/queryparser/flexible/core/config/FieldConfig;
    .end local v4    # "fieldNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldableNode;
    .end local p1    # "node":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    :cond_1
    return-object p1
.end method

.method protected preProcessNode(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 0
    .param p1, "node"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 76
    return-object p1
.end method

.method protected setChildrenOrder(Ljava/util/List;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 84
    .local p1, "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    return-object p1
.end method
