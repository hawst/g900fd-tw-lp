.class public Lorg/apache/lucene/queryparser/flexible/core/nodes/DeletedQueryNode;
.super Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;
.source "DeletedQueryNode.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;-><init>()V

    .line 33
    return-void
.end method


# virtual methods
.method public cloneTree()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 47
    invoke-super {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;->cloneTree()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/DeletedQueryNode;

    .line 49
    .local v0, "clone":Lorg/apache/lucene/queryparser/flexible/core/nodes/DeletedQueryNode;
    return-object v0
.end method

.method public toQueryString(Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;)Ljava/lang/CharSequence;
    .locals 1
    .param p1, "escaper"    # Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;

    .prologue
    .line 37
    const-string v0, "[DELETEDCHILD]"

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    const-string v0, "<deleted/>"

    return-object v0
.end method
