.class public Lorg/apache/lucene/queryparser/flexible/standard/StandardQueryParser;
.super Lorg/apache/lucene/queryparser/flexible/core/QueryParserHelper;
.source "StandardQueryParser.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/flexible/standard/CommonQueryParserConfiguration;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 119
    new-instance v0, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler;

    invoke-direct {v0}, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler;-><init>()V

    new-instance v1, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;

    invoke-direct {v1}, Lorg/apache/lucene/queryparser/flexible/standard/parser/StandardSyntaxParser;-><init>()V

    .line 120
    new-instance v2, Lorg/apache/lucene/queryparser/flexible/standard/processors/StandardQueryNodeProcessorPipeline;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lorg/apache/lucene/queryparser/flexible/standard/processors/StandardQueryNodeProcessorPipeline;-><init>(Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;)V

    .line 121
    new-instance v3, Lorg/apache/lucene/queryparser/flexible/standard/builders/StandardQueryTreeBuilder;

    invoke-direct {v3}, Lorg/apache/lucene/queryparser/flexible/standard/builders/StandardQueryTreeBuilder;-><init>()V

    invoke-direct {p0, v0, v1, v2, v3}, Lorg/apache/lucene/queryparser/flexible/core/QueryParserHelper;-><init>(Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;Lorg/apache/lucene/queryparser/flexible/core/parser/SyntaxParser;Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;Lorg/apache/lucene/queryparser/flexible/core/builders/QueryBuilder;)V

    .line 122
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/flexible/standard/StandardQueryParser;->setEnablePositionIncrements(Z)V

    .line 123
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/analysis/Analyzer;)V
    .locals 0
    .param p1, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;

    .prologue
    .line 138
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/standard/StandardQueryParser;-><init>()V

    .line 140
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/standard/StandardQueryParser;->setAnalyzer(Lorg/apache/lucene/analysis/Analyzer;)V

    .line 141
    return-void
.end method


# virtual methods
.method public getAllowLeadingWildcard()Z
    .locals 3

    .prologue
    .line 403
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/StandardQueryParser;->getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    move-result-object v1

    sget-object v2, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->ALLOW_LEADING_WILDCARD:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->get(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 405
    .local v0, "allowLeadingWildcard":Ljava/lang/Boolean;
    if-nez v0, :cond_0

    .line 406
    const/4 v1, 0x0

    .line 409
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    goto :goto_0
.end method

.method public getAnalyzer()Lorg/apache/lucene/analysis/Analyzer;
    .locals 2

    .prologue
    .line 395
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/StandardQueryParser;->getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    move-result-object v0

    sget-object v1, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->ANALYZER:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->get(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/Analyzer;

    return-object v0
.end method

.method public getDateResolution()Lorg/apache/lucene/document/DateTools$Resolution;
    .locals 2

    .prologue
    .line 511
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/StandardQueryParser;->getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    move-result-object v0

    sget-object v1, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->DATE_RESOLUTION:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->get(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/document/DateTools$Resolution;

    return-object v0
.end method

.method public getDateResolutionMap()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/CharSequence;",
            "Lorg/apache/lucene/document/DateTools$Resolution;",
            ">;"
        }
    .end annotation

    .prologue
    .line 532
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/StandardQueryParser;->getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    move-result-object v0

    sget-object v1, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->FIELD_DATE_RESOLUTION_MAP:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->get(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    return-object v0
.end method

.method public getDefaultOperator()Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$Operator;
    .locals 2

    .prologue
    .line 177
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/StandardQueryParser;->getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    move-result-object v0

    sget-object v1, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->DEFAULT_OPERATOR:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->get(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$Operator;

    return-object v0
.end method

.method public getEnablePositionIncrements()Z
    .locals 3

    .prologue
    .line 255
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/StandardQueryParser;->getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    move-result-object v1

    sget-object v2, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->ENABLE_POSITION_INCREMENTS:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->get(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 257
    .local v0, "enablePositionsIncrements":Ljava/lang/Boolean;
    if-nez v0, :cond_0

    .line 258
    const/4 v1, 0x0

    .line 261
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    goto :goto_0
.end method

.method public getFieldsBoost()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 490
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/StandardQueryParser;->getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    move-result-object v0

    sget-object v1, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->FIELD_BOOST_MAP:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->get(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    return-object v0
.end method

.method public getFuzzyMinSim()F
    .locals 3

    .prologue
    .line 418
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/StandardQueryParser;->getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    move-result-object v1

    sget-object v2, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->FUZZY_CONFIG:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->get(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/standard/config/FuzzyConfig;

    .line 420
    .local v0, "fuzzyConfig":Lorg/apache/lucene/queryparser/flexible/standard/config/FuzzyConfig;
    if-nez v0, :cond_0

    .line 421
    const/high16 v1, 0x40000000    # 2.0f

    .line 423
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Lorg/apache/lucene/queryparser/flexible/standard/config/FuzzyConfig;->getMinSimilarity()F

    move-result v1

    goto :goto_0
.end method

.method public getFuzzyPrefixLength()I
    .locals 3

    .prologue
    .line 434
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/StandardQueryParser;->getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    move-result-object v1

    sget-object v2, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->FUZZY_CONFIG:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->get(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/standard/config/FuzzyConfig;

    .line 436
    .local v0, "fuzzyConfig":Lorg/apache/lucene/queryparser/flexible/standard/config/FuzzyConfig;
    if-nez v0, :cond_0

    .line 437
    const/4 v1, 0x0

    .line 439
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Lorg/apache/lucene/queryparser/flexible/standard/config/FuzzyConfig;->getPrefixLength()I

    move-result v1

    goto :goto_0
.end method

.method public getLocale()Ljava/util/Locale;
    .locals 2

    .prologue
    .line 356
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/StandardQueryParser;->getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    move-result-object v0

    sget-object v1, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->LOCALE:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->get(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    return-object v0
.end method

.method public getLowercaseExpandedTerms()Z
    .locals 3

    .prologue
    .line 211
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/StandardQueryParser;->getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    move-result-object v1

    sget-object v2, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->LOWERCASE_EXPANDED_TERMS:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->get(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 213
    .local v0, "lowercaseExpandedTerms":Ljava/lang/Boolean;
    if-nez v0, :cond_0

    .line 214
    const/4 v1, 0x1

    .line 217
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    goto :goto_0
.end method

.method public getMultiFields([Ljava/lang/CharSequence;)V
    .locals 2
    .param p1, "fields"    # [Ljava/lang/CharSequence;

    .prologue
    .line 312
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/StandardQueryParser;->getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    move-result-object v0

    sget-object v1, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->MULTI_FIELDS:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->get(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;)Ljava/lang/Object;

    .line 313
    return-void
.end method

.method public getMultiTermRewriteMethod()Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;
    .locals 2

    .prologue
    .line 286
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/StandardQueryParser;->getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    move-result-object v0

    sget-object v1, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->MULTI_TERM_REWRITE_METHOD:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->get(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    return-object v0
.end method

.method public getNumericConfigMap()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;",
            ">;"
        }
    .end annotation

    .prologue
    .line 340
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/StandardQueryParser;->getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    move-result-object v0

    sget-object v1, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->NUMERIC_CONFIG_MAP:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->get(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    return-object v0
.end method

.method public getPhraseSlop()I
    .locals 3

    .prologue
    .line 448
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/StandardQueryParser;->getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    move-result-object v1

    sget-object v2, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->PHRASE_SLOP:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->get(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 450
    .local v0, "phraseSlop":Ljava/lang/Integer;
    if-nez v0, :cond_0

    .line 451
    const/4 v1, 0x0

    .line 454
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0
.end method

.method public getTimeZone()Ljava/util/TimeZone;
    .locals 2

    .prologue
    .line 366
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/StandardQueryParser;->getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    move-result-object v0

    sget-object v1, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->TIMEZONE:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->get(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/TimeZone;

    return-object v0
.end method

.method public bridge synthetic parse(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/queryparser/flexible/standard/StandardQueryParser;->parse(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/lucene/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public parse(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/lucene/search/Query;
    .locals 1
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "defaultField"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 168
    invoke-super {p0, p1, p2}, Lorg/apache/lucene/queryparser/flexible/core/QueryParserHelper;->parse(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/Query;

    return-object v0
.end method

.method public setAllowLeadingWildcard(Z)V
    .locals 3
    .param p1, "allowLeadingWildcard"    # Z

    .prologue
    .line 233
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/StandardQueryParser;->getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    move-result-object v0

    sget-object v1, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->ALLOW_LEADING_WILDCARD:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->set(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;Ljava/lang/Object;)V

    .line 234
    return-void
.end method

.method public setAnalyzer(Lorg/apache/lucene/analysis/Analyzer;)V
    .locals 2
    .param p1, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;

    .prologue
    .line 390
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/StandardQueryParser;->getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    move-result-object v0

    sget-object v1, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->ANALYZER:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-virtual {v0, v1, p1}, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->set(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;Ljava/lang/Object;)V

    .line 391
    return-void
.end method

.method public setDateResolution(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/CharSequence;",
            "Lorg/apache/lucene/document/DateTools$Resolution;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 523
    .local p1, "dateRes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/CharSequence;Lorg/apache/lucene/document/DateTools$Resolution;>;"
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/standard/StandardQueryParser;->setDateResolutionMap(Ljava/util/Map;)V

    .line 524
    return-void
.end method

.method public setDateResolution(Lorg/apache/lucene/document/DateTools$Resolution;)V
    .locals 2
    .param p1, "dateResolution"    # Lorg/apache/lucene/document/DateTools$Resolution;

    .prologue
    .line 501
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/StandardQueryParser;->getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    move-result-object v0

    sget-object v1, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->DATE_RESOLUTION:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-virtual {v0, v1, p1}, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->set(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;Ljava/lang/Object;)V

    .line 502
    return-void
.end method

.method public setDateResolutionMap(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/CharSequence;",
            "Lorg/apache/lucene/document/DateTools$Resolution;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 541
    .local p1, "dateRes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/CharSequence;Lorg/apache/lucene/document/DateTools$Resolution;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/StandardQueryParser;->getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    move-result-object v0

    sget-object v1, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->FIELD_DATE_RESOLUTION_MAP:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-virtual {v0, v1, p1}, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->set(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;Ljava/lang/Object;)V

    .line 542
    return-void
.end method

.method public setDefaultOperator(Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$Operator;)V
    .locals 2
    .param p1, "operator"    # Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$Operator;

    .prologue
    .line 189
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/StandardQueryParser;->getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    move-result-object v0

    sget-object v1, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->DEFAULT_OPERATOR:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-virtual {v0, v1, p1}, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->set(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;Ljava/lang/Object;)V

    .line 190
    return-void
.end method

.method public setDefaultPhraseSlop(I)V
    .locals 3
    .param p1, "defaultPhraseSlop"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 377
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/StandardQueryParser;->getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    move-result-object v0

    sget-object v1, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->PHRASE_SLOP:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->set(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;Ljava/lang/Object;)V

    .line 378
    return-void
.end method

.method public setEnablePositionIncrements(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    .line 247
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/StandardQueryParser;->getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    move-result-object v0

    sget-object v1, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->ENABLE_POSITION_INCREMENTS:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->set(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;Ljava/lang/Object;)V

    .line 248
    return-void
.end method

.method public setFieldsBoost(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Float;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 481
    .local p1, "boosts":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Float;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/StandardQueryParser;->getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    move-result-object v0

    sget-object v1, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->FIELD_BOOST_MAP:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-virtual {v0, v1, p1}, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->set(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;Ljava/lang/Object;)V

    .line 482
    return-void
.end method

.method public setFuzzyMinSim(F)V
    .locals 3
    .param p1, "fuzzyMinSim"    # F

    .prologue
    .line 464
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/StandardQueryParser;->getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    move-result-object v0

    .line 465
    .local v0, "config":Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;
    sget-object v2, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->FUZZY_CONFIG:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-virtual {v0, v2}, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->get(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/queryparser/flexible/standard/config/FuzzyConfig;

    .line 467
    .local v1, "fuzzyConfig":Lorg/apache/lucene/queryparser/flexible/standard/config/FuzzyConfig;
    if-nez v1, :cond_0

    .line 468
    new-instance v1, Lorg/apache/lucene/queryparser/flexible/standard/config/FuzzyConfig;

    .end local v1    # "fuzzyConfig":Lorg/apache/lucene/queryparser/flexible/standard/config/FuzzyConfig;
    invoke-direct {v1}, Lorg/apache/lucene/queryparser/flexible/standard/config/FuzzyConfig;-><init>()V

    .line 469
    .restart local v1    # "fuzzyConfig":Lorg/apache/lucene/queryparser/flexible/standard/config/FuzzyConfig;
    sget-object v2, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->FUZZY_CONFIG:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-virtual {v0, v2, v1}, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->set(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;Ljava/lang/Object;)V

    .line 472
    :cond_0
    invoke-virtual {v1, p1}, Lorg/apache/lucene/queryparser/flexible/standard/config/FuzzyConfig;->setMinSimilarity(F)V

    .line 473
    return-void
.end method

.method public setFuzzyPrefixLength(I)V
    .locals 3
    .param p1, "fuzzyPrefixLength"    # I

    .prologue
    .line 323
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/StandardQueryParser;->getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    move-result-object v0

    .line 324
    .local v0, "config":Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;
    sget-object v2, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->FUZZY_CONFIG:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-virtual {v0, v2}, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->get(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/queryparser/flexible/standard/config/FuzzyConfig;

    .line 326
    .local v1, "fuzzyConfig":Lorg/apache/lucene/queryparser/flexible/standard/config/FuzzyConfig;
    if-nez v1, :cond_0

    .line 327
    new-instance v1, Lorg/apache/lucene/queryparser/flexible/standard/config/FuzzyConfig;

    .end local v1    # "fuzzyConfig":Lorg/apache/lucene/queryparser/flexible/standard/config/FuzzyConfig;
    invoke-direct {v1}, Lorg/apache/lucene/queryparser/flexible/standard/config/FuzzyConfig;-><init>()V

    .line 328
    .restart local v1    # "fuzzyConfig":Lorg/apache/lucene/queryparser/flexible/standard/config/FuzzyConfig;
    sget-object v2, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->FUZZY_CONFIG:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-virtual {v0, v2, v1}, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->set(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;Ljava/lang/Object;)V

    .line 331
    :cond_0
    invoke-virtual {v1, p1}, Lorg/apache/lucene/queryparser/flexible/standard/config/FuzzyConfig;->setPrefixLength(I)V

    .line 333
    return-void
.end method

.method public setLocale(Ljava/util/Locale;)V
    .locals 2
    .param p1, "locale"    # Ljava/util/Locale;

    .prologue
    .line 348
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/StandardQueryParser;->getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    move-result-object v0

    sget-object v1, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->LOCALE:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-virtual {v0, v1, p1}, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->set(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;Ljava/lang/Object;)V

    .line 349
    return-void
.end method

.method public setLowercaseExpandedTerms(Z)V
    .locals 3
    .param p1, "lowercaseExpandedTerms"    # Z

    .prologue
    .line 203
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/StandardQueryParser;->getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    move-result-object v0

    sget-object v1, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->LOWERCASE_EXPANDED_TERMS:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->set(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;Ljava/lang/Object;)V

    .line 204
    return-void
.end method

.method public setMultiFields([Ljava/lang/CharSequence;)V
    .locals 2
    .param p1, "fields"    # [Ljava/lang/CharSequence;

    .prologue
    .line 297
    if-nez p1, :cond_0

    .line 298
    const/4 v0, 0x0

    new-array p1, v0, [Ljava/lang/CharSequence;

    .line 301
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/StandardQueryParser;->getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    move-result-object v0

    sget-object v1, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->MULTI_FIELDS:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-virtual {v0, v1, p1}, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->set(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;Ljava/lang/Object;)V

    .line 303
    return-void
.end method

.method public setMultiTermRewriteMethod(Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;)V
    .locals 2
    .param p1, "method"    # Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    .prologue
    .line 278
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/StandardQueryParser;->getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    move-result-object v0

    sget-object v1, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->MULTI_TERM_REWRITE_METHOD:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-virtual {v0, v1, p1}, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->set(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;Ljava/lang/Object;)V

    .line 279
    return-void
.end method

.method public setNumericConfigMap(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 336
    .local p1, "numericConfigMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/StandardQueryParser;->getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    move-result-object v0

    sget-object v1, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->NUMERIC_CONFIG_MAP:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-virtual {v0, v1, p1}, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->set(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;Ljava/lang/Object;)V

    .line 337
    return-void
.end method

.method public setPhraseSlop(I)V
    .locals 3
    .param p1, "defaultPhraseSlop"    # I

    .prologue
    .line 386
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/StandardQueryParser;->getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    move-result-object v0

    sget-object v1, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->PHRASE_SLOP:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->set(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;Ljava/lang/Object;)V

    .line 387
    return-void
.end method

.method public setTimeZone(Ljava/util/TimeZone;)V
    .locals 2
    .param p1, "timeZone"    # Ljava/util/TimeZone;

    .prologue
    .line 361
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/StandardQueryParser;->getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    move-result-object v0

    sget-object v1, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->TIMEZONE:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-virtual {v0, v1, p1}, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->set(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;Ljava/lang/Object;)V

    .line 362
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 145
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "<StandardQueryParser config=\""

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/StandardQueryParser;->getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 146
    const-string v1, "\"/>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 145
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
