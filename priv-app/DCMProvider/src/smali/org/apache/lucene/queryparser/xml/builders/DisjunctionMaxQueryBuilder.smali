.class public Lorg/apache/lucene/queryparser/xml/builders/DisjunctionMaxQueryBuilder;
.super Ljava/lang/Object;
.source "DisjunctionMaxQueryBuilder.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/xml/QueryBuilder;


# instance fields
.field private final factory:Lorg/apache/lucene/queryparser/xml/QueryBuilder;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/queryparser/xml/QueryBuilder;)V
    .locals 0
    .param p1, "factory"    # Lorg/apache/lucene/queryparser/xml/QueryBuilder;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lorg/apache/lucene/queryparser/xml/builders/DisjunctionMaxQueryBuilder;->factory:Lorg/apache/lucene/queryparser/xml/QueryBuilder;

    .line 37
    return-void
.end method


# virtual methods
.method public getQuery(Lorg/w3c/dom/Element;)Lorg/apache/lucene/search/Query;
    .locals 9
    .param p1, "e"    # Lorg/w3c/dom/Element;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/xml/ParserException;
        }
    .end annotation

    .prologue
    .line 45
    const-string v7, "tieBreaker"

    const/4 v8, 0x0

    invoke-static {p1, v7, v8}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttribute(Lorg/w3c/dom/Element;Ljava/lang/String;F)F

    move-result v6

    .line 46
    .local v6, "tieBreaker":F
    new-instance v0, Lorg/apache/lucene/search/DisjunctionMaxQuery;

    invoke-direct {v0, v6}, Lorg/apache/lucene/search/DisjunctionMaxQuery;-><init>(F)V

    .line 47
    .local v0, "dq":Lorg/apache/lucene/search/DisjunctionMaxQuery;
    const-string v7, "boost"

    const/high16 v8, 0x3f800000    # 1.0f

    invoke-static {p1, v7, v8}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttribute(Lorg/w3c/dom/Element;Ljava/lang/String;F)F

    move-result v7

    invoke-virtual {v0, v7}, Lorg/apache/lucene/search/DisjunctionMaxQuery;->setBoost(F)V

    .line 49
    invoke-interface {p1}, Lorg/w3c/dom/Element;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v2

    .line 50
    .local v2, "nl":Lorg/w3c/dom/NodeList;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {v2}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v7

    if-lt v1, v7, :cond_0

    .line 59
    return-object v0

    .line 51
    :cond_0
    invoke-interface {v2, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v3

    .line 52
    .local v3, "node":Lorg/w3c/dom/Node;
    instance-of v7, v3, Lorg/w3c/dom/Element;

    if-eqz v7, :cond_1

    move-object v5, v3

    .line 53
    check-cast v5, Lorg/w3c/dom/Element;

    .line 54
    .local v5, "queryElem":Lorg/w3c/dom/Element;
    iget-object v7, p0, Lorg/apache/lucene/queryparser/xml/builders/DisjunctionMaxQueryBuilder;->factory:Lorg/apache/lucene/queryparser/xml/QueryBuilder;

    invoke-interface {v7, v5}, Lorg/apache/lucene/queryparser/xml/QueryBuilder;->getQuery(Lorg/w3c/dom/Element;)Lorg/apache/lucene/search/Query;

    move-result-object v4

    .line 55
    .local v4, "q":Lorg/apache/lucene/search/Query;
    invoke-virtual {v0, v4}, Lorg/apache/lucene/search/DisjunctionMaxQuery;->add(Lorg/apache/lucene/search/Query;)V

    .line 50
    .end local v4    # "q":Lorg/apache/lucene/search/Query;
    .end local v5    # "queryElem":Lorg/w3c/dom/Element;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
