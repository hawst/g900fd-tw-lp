.class public Lorg/apache/lucene/queryparser/flexible/standard/config/FuzzyConfig;
.super Ljava/lang/Object;
.source "FuzzyConfig.java"


# instance fields
.field private minSimilarity:F

.field private prefixLength:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/config/FuzzyConfig;->prefixLength:I

    .line 29
    const/high16 v0, 0x40000000    # 2.0f

    iput v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/config/FuzzyConfig;->minSimilarity:F

    .line 31
    return-void
.end method


# virtual methods
.method public getMinSimilarity()F
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/config/FuzzyConfig;->minSimilarity:F

    return v0
.end method

.method public getPrefixLength()I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/config/FuzzyConfig;->prefixLength:I

    return v0
.end method

.method public setMinSimilarity(F)V
    .locals 0
    .param p1, "minSimilarity"    # F

    .prologue
    .line 46
    iput p1, p0, Lorg/apache/lucene/queryparser/flexible/standard/config/FuzzyConfig;->minSimilarity:F

    .line 47
    return-void
.end method

.method public setPrefixLength(I)V
    .locals 0
    .param p1, "prefixLength"    # I

    .prologue
    .line 38
    iput p1, p0, Lorg/apache/lucene/queryparser/flexible/standard/config/FuzzyConfig;->prefixLength:I

    .line 39
    return-void
.end method
