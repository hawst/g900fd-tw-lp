.class public Lorg/apache/lucene/queryparser/xml/builders/DuplicateFilterBuilder;
.super Ljava/lang/Object;
.source "DuplicateFilterBuilder.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/xml/FilterBuilder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getFilter(Lorg/w3c/dom/Element;)Lorg/apache/lucene/search/Filter;
    .locals 7
    .param p1, "e"    # Lorg/w3c/dom/Element;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/xml/ParserException;
        }
    .end annotation

    .prologue
    .line 37
    const-string v4, "fieldName"

    invoke-static {p1, v4}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttributeWithInheritanceOrFail(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 38
    .local v1, "fieldName":Ljava/lang/String;
    new-instance v0, Lorg/apache/lucene/sandbox/queries/DuplicateFilter;

    invoke-direct {v0, v1}, Lorg/apache/lucene/sandbox/queries/DuplicateFilter;-><init>(Ljava/lang/String;)V

    .line 40
    .local v0, "df":Lorg/apache/lucene/sandbox/queries/DuplicateFilter;
    const-string v4, "keepMode"

    const-string v5, "first"

    invoke-static {p1, v4, v5}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttribute(Lorg/w3c/dom/Element;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 41
    .local v2, "keepMode":Ljava/lang/String;
    const-string v4, "first"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 42
    sget-object v4, Lorg/apache/lucene/sandbox/queries/DuplicateFilter$KeepMode;->KM_USE_FIRST_OCCURRENCE:Lorg/apache/lucene/sandbox/queries/DuplicateFilter$KeepMode;

    invoke-virtual {v0, v4}, Lorg/apache/lucene/sandbox/queries/DuplicateFilter;->setKeepMode(Lorg/apache/lucene/sandbox/queries/DuplicateFilter$KeepMode;)V

    .line 49
    :goto_0
    const-string v4, "processingMode"

    const-string v5, "full"

    invoke-static {p1, v4, v5}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttribute(Lorg/w3c/dom/Element;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 50
    .local v3, "processingMode":Ljava/lang/String;
    const-string v4, "full"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 51
    sget-object v4, Lorg/apache/lucene/sandbox/queries/DuplicateFilter$ProcessingMode;->PM_FULL_VALIDATION:Lorg/apache/lucene/sandbox/queries/DuplicateFilter$ProcessingMode;

    invoke-virtual {v0, v4}, Lorg/apache/lucene/sandbox/queries/DuplicateFilter;->setProcessingMode(Lorg/apache/lucene/sandbox/queries/DuplicateFilter$ProcessingMode;)V

    .line 58
    :goto_1
    return-object v0

    .line 43
    .end local v3    # "processingMode":Ljava/lang/String;
    :cond_0
    const-string v4, "last"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 44
    sget-object v4, Lorg/apache/lucene/sandbox/queries/DuplicateFilter$KeepMode;->KM_USE_LAST_OCCURRENCE:Lorg/apache/lucene/sandbox/queries/DuplicateFilter$KeepMode;

    invoke-virtual {v0, v4}, Lorg/apache/lucene/sandbox/queries/DuplicateFilter;->setKeepMode(Lorg/apache/lucene/sandbox/queries/DuplicateFilter$KeepMode;)V

    goto :goto_0

    .line 46
    :cond_1
    new-instance v4, Lorg/apache/lucene/queryparser/xml/ParserException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Illegal keepMode attribute in DuplicateFilter:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/apache/lucene/queryparser/xml/ParserException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 52
    .restart local v3    # "processingMode":Ljava/lang/String;
    :cond_2
    const-string v4, "fast"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 53
    sget-object v4, Lorg/apache/lucene/sandbox/queries/DuplicateFilter$ProcessingMode;->PM_FAST_INVALIDATION:Lorg/apache/lucene/sandbox/queries/DuplicateFilter$ProcessingMode;

    invoke-virtual {v0, v4}, Lorg/apache/lucene/sandbox/queries/DuplicateFilter;->setProcessingMode(Lorg/apache/lucene/sandbox/queries/DuplicateFilter$ProcessingMode;)V

    goto :goto_1

    .line 55
    :cond_3
    new-instance v4, Lorg/apache/lucene/queryparser/xml/ParserException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Illegal processingMode attribute in DuplicateFilter:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/apache/lucene/queryparser/xml/ParserException;-><init>(Ljava/lang/String;)V

    throw v4
.end method
