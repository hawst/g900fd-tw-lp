.class public abstract Lorg/apache/lucene/queryparser/xml/builders/SpanBuilderBase;
.super Ljava/lang/Object;
.source "SpanBuilderBase.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getQuery(Lorg/w3c/dom/Element;)Lorg/apache/lucene/search/Query;
    .locals 1
    .param p1, "e"    # Lorg/w3c/dom/Element;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/xml/ParserException;
        }
    .end annotation

    .prologue
    .line 31
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/xml/builders/SpanBuilderBase;->getSpanQuery(Lorg/w3c/dom/Element;)Lorg/apache/lucene/search/spans/SpanQuery;

    move-result-object v0

    return-object v0
.end method
