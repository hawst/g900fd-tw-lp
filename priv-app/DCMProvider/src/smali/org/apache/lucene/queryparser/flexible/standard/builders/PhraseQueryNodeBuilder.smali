.class public Lorg/apache/lucene/queryparser/flexible/standard/builders/PhraseQueryNodeBuilder;
.super Ljava/lang/Object;
.source "PhraseQueryNodeBuilder.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/flexible/standard/builders/StandardQueryBuilder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    return-void
.end method


# virtual methods
.method public bridge synthetic build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/PhraseQueryNodeBuilder;->build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/search/PhraseQuery;

    move-result-object v0

    return-object v0
.end method

.method public build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/search/PhraseQuery;
    .locals 9
    .param p1, "queryNode"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 42
    move-object v2, p1

    check-cast v2, Lorg/apache/lucene/queryparser/flexible/core/nodes/TokenizedPhraseQueryNode;

    .line 44
    .local v2, "phraseNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/TokenizedPhraseQueryNode;
    new-instance v3, Lorg/apache/lucene/search/PhraseQuery;

    invoke-direct {v3}, Lorg/apache/lucene/search/PhraseQuery;-><init>()V

    .line 46
    .local v3, "phraseQuery":Lorg/apache/lucene/search/PhraseQuery;
    invoke-virtual {v2}, Lorg/apache/lucene/queryparser/flexible/core/nodes/TokenizedPhraseQueryNode;->getChildren()Ljava/util/List;

    move-result-object v1

    .line 48
    .local v1, "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    if-eqz v1, :cond_0

    .line 50
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_1

    .line 61
    :cond_0
    return-object v3

    .line 50
    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    .line 52
    .local v0, "child":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    sget-object v7, Lorg/apache/lucene/queryparser/flexible/core/builders/QueryTreeBuilder;->QUERY_TREE_BUILDER_TAGID:Ljava/lang/String;

    invoke-interface {v0, v7}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->getTag(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    .line 51
    check-cast v5, Lorg/apache/lucene/search/TermQuery;

    .local v5, "termQuery":Lorg/apache/lucene/search/TermQuery;
    move-object v4, v0

    .line 53
    check-cast v4, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;

    .line 55
    .local v4, "termNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    invoke-virtual {v5}, Lorg/apache/lucene/search/TermQuery;->getTerm()Lorg/apache/lucene/index/Term;

    move-result-object v7

    invoke-virtual {v4}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->getPositionIncrement()I

    move-result v8

    invoke-virtual {v3, v7, v8}, Lorg/apache/lucene/search/PhraseQuery;->add(Lorg/apache/lucene/index/Term;I)V

    goto :goto_0
.end method

.method public bridge synthetic build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/search/Query;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/PhraseQueryNodeBuilder;->build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/search/PhraseQuery;

    move-result-object v0

    return-object v0
.end method
