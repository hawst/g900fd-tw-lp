.class public Lorg/apache/lucene/queryparser/flexible/standard/builders/PrefixWildcardQueryNodeBuilder;
.super Ljava/lang/Object;
.source "PrefixWildcardQueryNodeBuilder.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/flexible/standard/builders/StandardQueryBuilder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    return-void
.end method


# virtual methods
.method public bridge synthetic build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/PrefixWildcardQueryNodeBuilder;->build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/search/PrefixQuery;

    move-result-object v0

    return-object v0
.end method

.method public build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/search/PrefixQuery;
    .locals 7
    .param p1, "queryNode"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 41
    move-object v3, p1

    check-cast v3, Lorg/apache/lucene/queryparser/flexible/standard/nodes/PrefixWildcardQueryNode;

    .line 43
    .local v3, "wildcardNode":Lorg/apache/lucene/queryparser/flexible/standard/nodes/PrefixWildcardQueryNode;
    invoke-virtual {v3}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/PrefixWildcardQueryNode;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v3}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/PrefixWildcardQueryNode;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->length()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-interface {v4, v5, v6}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    .line 44
    .local v2, "text":Ljava/lang/String;
    new-instance v1, Lorg/apache/lucene/search/PrefixQuery;

    new-instance v4, Lorg/apache/lucene/index/Term;

    invoke-virtual {v3}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/PrefixWildcardQueryNode;->getFieldAsString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5, v2}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v4}, Lorg/apache/lucene/search/PrefixQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    .line 46
    .local v1, "q":Lorg/apache/lucene/search/PrefixQuery;
    const-string v4, "MultiTermRewriteMethodConfiguration"

    invoke-interface {p1, v4}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->getTag(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    .line 47
    .local v0, "method":Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;
    if-eqz v0, :cond_0

    .line 48
    invoke-virtual {v1, v0}, Lorg/apache/lucene/search/PrefixQuery;->setRewriteMethod(Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;)V

    .line 51
    :cond_0
    return-object v1
.end method

.method public bridge synthetic build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/search/Query;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/PrefixWildcardQueryNodeBuilder;->build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/search/PrefixQuery;

    move-result-object v0

    return-object v0
.end method
