.class public Lorg/apache/lucene/queryparser/flexible/standard/nodes/AbstractRangeQueryNode;
.super Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;
.source "AbstractRangeQueryNode.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/flexible/core/nodes/RangeQueryNode;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldValuePairQueryNode",
        "<*>;>",
        "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;",
        "Lorg/apache/lucene/queryparser/flexible/core/nodes/RangeQueryNode",
        "<",
        "Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldValuePairQueryNode",
        "<*>;>;"
    }
.end annotation


# instance fields
.field private lowerInclusive:Z

.field private upperInclusive:Z


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 45
    .local p0, "this":Lorg/apache/lucene/queryparser/flexible/standard/nodes/AbstractRangeQueryNode;, "Lorg/apache/lucene/queryparser/flexible/standard/nodes/AbstractRangeQueryNode<TT;>;"
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;-><init>()V

    .line 46
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/AbstractRangeQueryNode;->setLeaf(Z)V

    .line 47
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/AbstractRangeQueryNode;->allocate()V

    .line 48
    return-void
.end method


# virtual methods
.method public getField()Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 59
    .local p0, "this":Lorg/apache/lucene/queryparser/flexible/standard/nodes/AbstractRangeQueryNode;, "Lorg/apache/lucene/queryparser/flexible/standard/nodes/AbstractRangeQueryNode<TT;>;"
    const/4 v0, 0x0

    .line 60
    .local v0, "field":Ljava/lang/CharSequence;
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/AbstractRangeQueryNode;->getLowerBound()Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldValuePairQueryNode;

    move-result-object v1

    .line 61
    .local v1, "lower":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldValuePairQueryNode;, "TT;"
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/AbstractRangeQueryNode;->getUpperBound()Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldValuePairQueryNode;

    move-result-object v2

    .line 63
    .local v2, "upper":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldValuePairQueryNode;, "TT;"
    if-eqz v1, :cond_1

    .line 64
    invoke-interface {v1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldValuePairQueryNode;->getField()Ljava/lang/CharSequence;

    move-result-object v0

    .line 70
    :cond_0
    :goto_0
    return-object v0

    .line 66
    :cond_1
    if-eqz v2, :cond_0

    .line 67
    invoke-interface {v2}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldValuePairQueryNode;->getField()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method public getLowerBound()Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldValuePairQueryNode;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 102
    .local p0, "this":Lorg/apache/lucene/queryparser/flexible/standard/nodes/AbstractRangeQueryNode;, "Lorg/apache/lucene/queryparser/flexible/standard/nodes/AbstractRangeQueryNode<TT;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/AbstractRangeQueryNode;->getChildren()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldValuePairQueryNode;

    return-object v0
.end method

.method public getUpperBound()Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldValuePairQueryNode;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 113
    .local p0, "this":Lorg/apache/lucene/queryparser/flexible/standard/nodes/AbstractRangeQueryNode;, "Lorg/apache/lucene/queryparser/flexible/standard/nodes/AbstractRangeQueryNode<TT;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/AbstractRangeQueryNode;->getChildren()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldValuePairQueryNode;

    return-object v0
.end method

.method public isLowerInclusive()Z
    .locals 1

    .prologue
    .line 123
    .local p0, "this":Lorg/apache/lucene/queryparser/flexible/standard/nodes/AbstractRangeQueryNode;, "Lorg/apache/lucene/queryparser/flexible/standard/nodes/AbstractRangeQueryNode<TT;>;"
    iget-boolean v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/AbstractRangeQueryNode;->lowerInclusive:Z

    return v0
.end method

.method public isUpperInclusive()Z
    .locals 1

    .prologue
    .line 133
    .local p0, "this":Lorg/apache/lucene/queryparser/flexible/standard/nodes/AbstractRangeQueryNode;, "Lorg/apache/lucene/queryparser/flexible/standard/nodes/AbstractRangeQueryNode<TT;>;"
    iget-boolean v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/AbstractRangeQueryNode;->upperInclusive:Z

    return v0
.end method

.method public setBounds(Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldValuePairQueryNode;Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldValuePairQueryNode;ZZ)V
    .locals 5
    .param p3, "lowerInclusive"    # Z
    .param p4, "upperInclusive"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TT;ZZ)V"
        }
    .end annotation

    .prologue
    .line 152
    .local p0, "this":Lorg/apache/lucene/queryparser/flexible/standard/nodes/AbstractRangeQueryNode;, "Lorg/apache/lucene/queryparser/flexible/standard/nodes/AbstractRangeQueryNode<TT;>;"
    .local p1, "lower":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldValuePairQueryNode;, "TT;"
    .local p2, "upper":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldValuePairQueryNode;, "TT;"
    if-eqz p1, :cond_4

    if-eqz p2, :cond_4

    .line 153
    invoke-interface {p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldValuePairQueryNode;->getField()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v3}, Lorg/apache/lucene/queryparser/flexible/core/util/StringUtils;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 154
    .local v1, "lowerField":Ljava/lang/String;
    invoke-interface {p2}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldValuePairQueryNode;->getField()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v3}, Lorg/apache/lucene/queryparser/flexible/core/util/StringUtils;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 156
    .local v2, "upperField":Ljava/lang/String;
    if-nez v2, :cond_0

    if-eqz v1, :cond_3

    .line 157
    :cond_0
    if-eqz v2, :cond_1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 158
    :cond_1
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 159
    :cond_2
    new-instance v3, Ljava/lang/IllegalArgumentException;

    .line 160
    const-string v4, "lower and upper bounds should have the same field name!"

    .line 159
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 163
    :cond_3
    iput-boolean p3, p0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/AbstractRangeQueryNode;->lowerInclusive:Z

    .line 164
    iput-boolean p4, p0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/AbstractRangeQueryNode;->upperInclusive:Z

    .line 166
    new-instance v0, Ljava/util/ArrayList;

    const/4 v3, 0x2

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 167
    .local v0, "children":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 168
    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 170
    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/AbstractRangeQueryNode;->set(Ljava/util/List;)V

    .line 174
    .end local v0    # "children":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    .end local v1    # "lowerField":Ljava/lang/String;
    .end local v2    # "upperField":Ljava/lang/String;
    :cond_4
    return-void
.end method

.method public setField(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1, "fieldName"    # Ljava/lang/CharSequence;

    .prologue
    .line 81
    .local p0, "this":Lorg/apache/lucene/queryparser/flexible/standard/nodes/AbstractRangeQueryNode;, "Lorg/apache/lucene/queryparser/flexible/standard/nodes/AbstractRangeQueryNode<TT;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/AbstractRangeQueryNode;->getLowerBound()Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldValuePairQueryNode;

    move-result-object v0

    .line 82
    .local v0, "lower":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldValuePairQueryNode;, "TT;"
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/AbstractRangeQueryNode;->getUpperBound()Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldValuePairQueryNode;

    move-result-object v1

    .line 84
    .local v1, "upper":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldValuePairQueryNode;, "TT;"
    if-eqz v0, :cond_0

    .line 85
    invoke-interface {v0, p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldValuePairQueryNode;->setField(Ljava/lang/CharSequence;)V

    .line 88
    :cond_0
    if-eqz v1, :cond_1

    .line 89
    invoke-interface {v1, p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldValuePairQueryNode;->setField(Ljava/lang/CharSequence;)V

    .line 92
    :cond_1
    return-void
.end method

.method public toQueryString(Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;)Ljava/lang/CharSequence;
    .locals 4
    .param p1, "escapeSyntaxParser"    # Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;

    .prologue
    .line 178
    .local p0, "this":Lorg/apache/lucene/queryparser/flexible/standard/nodes/AbstractRangeQueryNode;, "Lorg/apache/lucene/queryparser/flexible/standard/nodes/AbstractRangeQueryNode<TT;>;"
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 180
    .local v1, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/AbstractRangeQueryNode;->getLowerBound()Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldValuePairQueryNode;

    move-result-object v0

    .line 181
    .local v0, "lower":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldValuePairQueryNode;, "TT;"
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/AbstractRangeQueryNode;->getUpperBound()Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldValuePairQueryNode;

    move-result-object v2

    .line 183
    .local v2, "upper":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldValuePairQueryNode;, "TT;"
    iget-boolean v3, p0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/AbstractRangeQueryNode;->lowerInclusive:Z

    if-eqz v3, :cond_0

    .line 184
    const/16 v3, 0x5b

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 190
    :goto_0
    if-eqz v0, :cond_1

    .line 191
    invoke-interface {v0, p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldValuePairQueryNode;->toQueryString(Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 197
    :goto_1
    const/16 v3, 0x20

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 199
    if-eqz v2, :cond_2

    .line 200
    invoke-interface {v2, p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldValuePairQueryNode;->toQueryString(Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 206
    :goto_2
    iget-boolean v3, p0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/AbstractRangeQueryNode;->upperInclusive:Z

    if-eqz v3, :cond_3

    .line 207
    const/16 v3, 0x5d

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 213
    :goto_3
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 187
    :cond_0
    const/16 v3, 0x7b

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 194
    :cond_1
    const-string v3, "..."

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 203
    :cond_2
    const-string v3, "..."

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 210
    :cond_3
    const/16 v3, 0x7d

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_3
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 219
    .local p0, "this":Lorg/apache/lucene/queryparser/flexible/standard/nodes/AbstractRangeQueryNode;, "Lorg/apache/lucene/queryparser/flexible/standard/nodes/AbstractRangeQueryNode<TT;>;"
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "<"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 220
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v1, " lowerInclusive="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/AbstractRangeQueryNode;->isLowerInclusive()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 221
    const-string v1, " upperInclusive="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/AbstractRangeQueryNode;->isUpperInclusive()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 222
    const-string v1, ">\n\t"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 223
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/AbstractRangeQueryNode;->getUpperBound()Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldValuePairQueryNode;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n\t"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 224
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/AbstractRangeQueryNode;->getLowerBound()Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldValuePairQueryNode;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 225
    const-string v1, "</"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ">\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 227
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
