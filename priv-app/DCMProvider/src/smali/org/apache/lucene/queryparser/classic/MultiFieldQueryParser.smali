.class public Lorg/apache/lucene/queryparser/classic/MultiFieldQueryParser;
.super Lorg/apache/lucene/queryparser/classic/QueryParser;
.source "MultiFieldQueryParser.java"


# instance fields
.field protected boosts:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field protected fields:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/util/Version;[Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "fields"    # [Ljava/lang/String;
    .param p3, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;

    .prologue
    .line 94
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p3}, Lorg/apache/lucene/queryparser/classic/QueryParser;-><init>(Lorg/apache/lucene/util/Version;Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer;)V

    .line 95
    iput-object p2, p0, Lorg/apache/lucene/queryparser/classic/MultiFieldQueryParser;->fields:[Ljava/lang/String;

    .line 96
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;[Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer;Ljava/util/Map;)V
    .locals 0
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "fields"    # [Ljava/lang/String;
    .param p3, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/Version;",
            "[",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/analysis/Analyzer;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Float;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 69
    .local p4, "boosts":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Float;>;"
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/queryparser/classic/MultiFieldQueryParser;-><init>(Lorg/apache/lucene/util/Version;[Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer;)V

    .line 70
    iput-object p4, p0, Lorg/apache/lucene/queryparser/classic/MultiFieldQueryParser;->boosts:Ljava/util/Map;

    .line 71
    return-void
.end method

.method private applySlop(Lorg/apache/lucene/search/Query;I)V
    .locals 1
    .param p1, "q"    # Lorg/apache/lucene/search/Query;
    .param p2, "slop"    # I

    .prologue
    .line 127
    instance-of v0, p1, Lorg/apache/lucene/search/PhraseQuery;

    if-eqz v0, :cond_1

    .line 128
    check-cast p1, Lorg/apache/lucene/search/PhraseQuery;

    .end local p1    # "q":Lorg/apache/lucene/search/Query;
    invoke-virtual {p1, p2}, Lorg/apache/lucene/search/PhraseQuery;->setSlop(I)V

    .line 132
    :cond_0
    :goto_0
    return-void

    .line 129
    .restart local p1    # "q":Lorg/apache/lucene/search/Query;
    :cond_1
    instance-of v0, p1, Lorg/apache/lucene/search/MultiPhraseQuery;

    if-eqz v0, :cond_0

    .line 130
    check-cast p1, Lorg/apache/lucene/search/MultiPhraseQuery;

    .end local p1    # "q":Lorg/apache/lucene/search/Query;
    invoke-virtual {p1, p2}, Lorg/apache/lucene/search/MultiPhraseQuery;->setSlop(I)V

    goto :goto_0
.end method

.method public static parse(Lorg/apache/lucene/util/Version;Ljava/lang/String;[Ljava/lang/String;[Lorg/apache/lucene/search/BooleanClause$Occur;Lorg/apache/lucene/analysis/Analyzer;)Lorg/apache/lucene/search/Query;
    .locals 6
    .param p0, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "fields"    # [Ljava/lang/String;
    .param p3, "flags"    # [Lorg/apache/lucene/search/BooleanClause$Occur;
    .param p4, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/classic/ParseException;
        }
    .end annotation

    .prologue
    .line 302
    array-length v4, p2

    array-length v5, p3

    if-eq v4, v5, :cond_0

    .line 303
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "fields.length != flags.length"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 304
    :cond_0
    new-instance v0, Lorg/apache/lucene/search/BooleanQuery;

    invoke-direct {v0}, Lorg/apache/lucene/search/BooleanQuery;-><init>()V

    .line 305
    .local v0, "bQuery":Lorg/apache/lucene/search/BooleanQuery;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v4, p2

    if-lt v1, v4, :cond_1

    .line 313
    return-object v0

    .line 306
    :cond_1
    new-instance v3, Lorg/apache/lucene/queryparser/classic/QueryParser;

    aget-object v4, p2, v1

    invoke-direct {v3, p0, v4, p4}, Lorg/apache/lucene/queryparser/classic/QueryParser;-><init>(Lorg/apache/lucene/util/Version;Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer;)V

    .line 307
    .local v3, "qp":Lorg/apache/lucene/queryparser/classic/QueryParser;
    invoke-virtual {v3, p1}, Lorg/apache/lucene/queryparser/classic/QueryParser;->parse(Ljava/lang/String;)Lorg/apache/lucene/search/Query;

    move-result-object v2

    .line 308
    .local v2, "q":Lorg/apache/lucene/search/Query;
    if-eqz v2, :cond_3

    .line 309
    instance-of v4, v2, Lorg/apache/lucene/search/BooleanQuery;

    if-eqz v4, :cond_2

    move-object v4, v2

    check-cast v4, Lorg/apache/lucene/search/BooleanQuery;

    invoke-virtual {v4}, Lorg/apache/lucene/search/BooleanQuery;->getClauses()[Lorg/apache/lucene/search/BooleanClause;

    move-result-object v4

    array-length v4, v4

    if-lez v4, :cond_3

    .line 310
    :cond_2
    aget-object v4, p3, v1

    invoke-virtual {v0, v2, v4}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 305
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static parse(Lorg/apache/lucene/util/Version;[Ljava/lang/String;[Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer;)Lorg/apache/lucene/search/Query;
    .locals 6
    .param p0, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p1, "queries"    # [Ljava/lang/String;
    .param p2, "fields"    # [Ljava/lang/String;
    .param p3, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/classic/ParseException;
        }
    .end annotation

    .prologue
    .line 253
    array-length v4, p1

    array-length v5, p2

    if-eq v4, v5, :cond_0

    .line 254
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "queries.length != fields.length"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 255
    :cond_0
    new-instance v0, Lorg/apache/lucene/search/BooleanQuery;

    invoke-direct {v0}, Lorg/apache/lucene/search/BooleanQuery;-><init>()V

    .line 256
    .local v0, "bQuery":Lorg/apache/lucene/search/BooleanQuery;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v4, p2

    if-lt v1, v4, :cond_1

    .line 265
    return-object v0

    .line 258
    :cond_1
    new-instance v3, Lorg/apache/lucene/queryparser/classic/QueryParser;

    aget-object v4, p2, v1

    invoke-direct {v3, p0, v4, p3}, Lorg/apache/lucene/queryparser/classic/QueryParser;-><init>(Lorg/apache/lucene/util/Version;Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer;)V

    .line 259
    .local v3, "qp":Lorg/apache/lucene/queryparser/classic/QueryParser;
    aget-object v4, p1, v1

    invoke-virtual {v3, v4}, Lorg/apache/lucene/queryparser/classic/QueryParser;->parse(Ljava/lang/String;)Lorg/apache/lucene/search/Query;

    move-result-object v2

    .line 260
    .local v2, "q":Lorg/apache/lucene/search/Query;
    if-eqz v2, :cond_3

    .line 261
    instance-of v4, v2, Lorg/apache/lucene/search/BooleanQuery;

    if-eqz v4, :cond_2

    move-object v4, v2

    check-cast v4, Lorg/apache/lucene/search/BooleanQuery;

    invoke-virtual {v4}, Lorg/apache/lucene/search/BooleanQuery;->getClauses()[Lorg/apache/lucene/search/BooleanClause;

    move-result-object v4

    array-length v4, v4

    if-lez v4, :cond_3

    .line 262
    :cond_2
    sget-object v4, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v0, v2, v4}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 256
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static parse(Lorg/apache/lucene/util/Version;[Ljava/lang/String;[Ljava/lang/String;[Lorg/apache/lucene/search/BooleanClause$Occur;Lorg/apache/lucene/analysis/Analyzer;)Lorg/apache/lucene/search/Query;
    .locals 6
    .param p0, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p1, "queries"    # [Ljava/lang/String;
    .param p2, "fields"    # [Ljava/lang/String;
    .param p3, "flags"    # [Lorg/apache/lucene/search/BooleanClause$Occur;
    .param p4, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/classic/ParseException;
        }
    .end annotation

    .prologue
    .line 352
    array-length v4, p1

    array-length v5, p2

    if-ne v4, v5, :cond_0

    array-length v4, p1

    array-length v5, p3

    if-eq v4, v5, :cond_1

    .line 353
    :cond_0
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "queries, fields, and flags array have have different length"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 354
    :cond_1
    new-instance v0, Lorg/apache/lucene/search/BooleanQuery;

    invoke-direct {v0}, Lorg/apache/lucene/search/BooleanQuery;-><init>()V

    .line 355
    .local v0, "bQuery":Lorg/apache/lucene/search/BooleanQuery;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v4, p2

    if-lt v1, v4, :cond_2

    .line 364
    return-object v0

    .line 357
    :cond_2
    new-instance v3, Lorg/apache/lucene/queryparser/classic/QueryParser;

    aget-object v4, p2, v1

    invoke-direct {v3, p0, v4, p4}, Lorg/apache/lucene/queryparser/classic/QueryParser;-><init>(Lorg/apache/lucene/util/Version;Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer;)V

    .line 358
    .local v3, "qp":Lorg/apache/lucene/queryparser/classic/QueryParser;
    aget-object v4, p1, v1

    invoke-virtual {v3, v4}, Lorg/apache/lucene/queryparser/classic/QueryParser;->parse(Ljava/lang/String;)Lorg/apache/lucene/search/Query;

    move-result-object v2

    .line 359
    .local v2, "q":Lorg/apache/lucene/search/Query;
    if-eqz v2, :cond_4

    .line 360
    instance-of v4, v2, Lorg/apache/lucene/search/BooleanQuery;

    if-eqz v4, :cond_3

    move-object v4, v2

    check-cast v4, Lorg/apache/lucene/search/BooleanQuery;

    invoke-virtual {v4}, Lorg/apache/lucene/search/BooleanQuery;->getClauses()[Lorg/apache/lucene/search/BooleanClause;

    move-result-object v4

    array-length v4, v4

    if-lez v4, :cond_4

    .line 361
    :cond_3
    aget-object v4, p3, v1

    invoke-virtual {v0, v2, v4}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 355
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method protected getFieldQuery(Ljava/lang/String;Ljava/lang/String;I)Lorg/apache/lucene/search/Query;
    .locals 7
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "queryText"    # Ljava/lang/String;
    .param p3, "slop"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/classic/ParseException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 100
    if-nez p1, :cond_4

    .line 101
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 102
    .local v1, "clauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/BooleanClause;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v4, p0, Lorg/apache/lucene/queryparser/classic/MultiFieldQueryParser;->fields:[Ljava/lang/String;

    array-length v4, v4

    if-lt v2, v4, :cond_0

    .line 117
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_3

    .line 118
    const/4 v3, 0x0

    .line 123
    .end local v1    # "clauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/BooleanClause;>;"
    .end local v2    # "i":I
    :goto_1
    return-object v3

    .line 103
    .restart local v1    # "clauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/BooleanClause;>;"
    .restart local v2    # "i":I
    :cond_0
    iget-object v4, p0, Lorg/apache/lucene/queryparser/classic/MultiFieldQueryParser;->fields:[Ljava/lang/String;

    aget-object v4, v4, v2

    invoke-super {p0, v4, p2, v6}, Lorg/apache/lucene/queryparser/classic/QueryParser;->getFieldQuery(Ljava/lang/String;Ljava/lang/String;Z)Lorg/apache/lucene/search/Query;

    move-result-object v3

    .line 104
    .local v3, "q":Lorg/apache/lucene/search/Query;
    if-eqz v3, :cond_2

    .line 106
    iget-object v4, p0, Lorg/apache/lucene/queryparser/classic/MultiFieldQueryParser;->boosts:Ljava/util/Map;

    if-eqz v4, :cond_1

    .line 108
    iget-object v4, p0, Lorg/apache/lucene/queryparser/classic/MultiFieldQueryParser;->boosts:Ljava/util/Map;

    iget-object v5, p0, Lorg/apache/lucene/queryparser/classic/MultiFieldQueryParser;->fields:[Ljava/lang/String;

    aget-object v5, v5, v2

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    .line 109
    .local v0, "boost":Ljava/lang/Float;
    if-eqz v0, :cond_1

    .line 110
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v4

    invoke-virtual {v3, v4}, Lorg/apache/lucene/search/Query;->setBoost(F)V

    .line 113
    .end local v0    # "boost":Ljava/lang/Float;
    :cond_1
    invoke-direct {p0, v3, p3}, Lorg/apache/lucene/queryparser/classic/MultiFieldQueryParser;->applySlop(Lorg/apache/lucene/search/Query;I)V

    .line 114
    new-instance v4, Lorg/apache/lucene/search/BooleanClause;

    sget-object v5, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-direct {v4, v3, v5}, Lorg/apache/lucene/search/BooleanClause;-><init>(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 102
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 119
    .end local v3    # "q":Lorg/apache/lucene/search/Query;
    :cond_3
    invoke-virtual {p0, v1, v6}, Lorg/apache/lucene/queryparser/classic/MultiFieldQueryParser;->getBooleanQuery(Ljava/util/List;Z)Lorg/apache/lucene/search/Query;

    move-result-object v3

    goto :goto_1

    .line 121
    .end local v1    # "clauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/BooleanClause;>;"
    .end local v2    # "i":I
    :cond_4
    invoke-super {p0, p1, p2, v6}, Lorg/apache/lucene/queryparser/classic/QueryParser;->getFieldQuery(Ljava/lang/String;Ljava/lang/String;Z)Lorg/apache/lucene/search/Query;

    move-result-object v3

    .line 122
    .restart local v3    # "q":Lorg/apache/lucene/search/Query;
    invoke-direct {p0, v3, p3}, Lorg/apache/lucene/queryparser/classic/MultiFieldQueryParser;->applySlop(Lorg/apache/lucene/search/Query;I)V

    goto :goto_1
.end method

.method protected getFieldQuery(Ljava/lang/String;Ljava/lang/String;Z)Lorg/apache/lucene/search/Query;
    .locals 6
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "queryText"    # Ljava/lang/String;
    .param p3, "quoted"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/classic/ParseException;
        }
    .end annotation

    .prologue
    .line 137
    if-nez p1, :cond_4

    .line 138
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 139
    .local v1, "clauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/BooleanClause;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v4, p0, Lorg/apache/lucene/queryparser/classic/MultiFieldQueryParser;->fields:[Ljava/lang/String;

    array-length v4, v4

    if-lt v2, v4, :cond_0

    .line 153
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_3

    .line 154
    const/4 v3, 0x0

    .line 158
    .end local v1    # "clauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/BooleanClause;>;"
    .end local v2    # "i":I
    :goto_1
    return-object v3

    .line 140
    .restart local v1    # "clauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/BooleanClause;>;"
    .restart local v2    # "i":I
    :cond_0
    iget-object v4, p0, Lorg/apache/lucene/queryparser/classic/MultiFieldQueryParser;->fields:[Ljava/lang/String;

    aget-object v4, v4, v2

    invoke-super {p0, v4, p2, p3}, Lorg/apache/lucene/queryparser/classic/QueryParser;->getFieldQuery(Ljava/lang/String;Ljava/lang/String;Z)Lorg/apache/lucene/search/Query;

    move-result-object v3

    .line 141
    .local v3, "q":Lorg/apache/lucene/search/Query;
    if-eqz v3, :cond_2

    .line 143
    iget-object v4, p0, Lorg/apache/lucene/queryparser/classic/MultiFieldQueryParser;->boosts:Ljava/util/Map;

    if-eqz v4, :cond_1

    .line 145
    iget-object v4, p0, Lorg/apache/lucene/queryparser/classic/MultiFieldQueryParser;->boosts:Ljava/util/Map;

    iget-object v5, p0, Lorg/apache/lucene/queryparser/classic/MultiFieldQueryParser;->fields:[Ljava/lang/String;

    aget-object v5, v5, v2

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    .line 146
    .local v0, "boost":Ljava/lang/Float;
    if-eqz v0, :cond_1

    .line 147
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v4

    invoke-virtual {v3, v4}, Lorg/apache/lucene/search/Query;->setBoost(F)V

    .line 150
    .end local v0    # "boost":Ljava/lang/Float;
    :cond_1
    new-instance v4, Lorg/apache/lucene/search/BooleanClause;

    sget-object v5, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-direct {v4, v3, v5}, Lorg/apache/lucene/search/BooleanClause;-><init>(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 139
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 155
    .end local v3    # "q":Lorg/apache/lucene/search/Query;
    :cond_3
    const/4 v4, 0x1

    invoke-virtual {p0, v1, v4}, Lorg/apache/lucene/queryparser/classic/MultiFieldQueryParser;->getBooleanQuery(Ljava/util/List;Z)Lorg/apache/lucene/search/Query;

    move-result-object v3

    goto :goto_1

    .line 157
    .end local v1    # "clauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/BooleanClause;>;"
    .end local v2    # "i":I
    :cond_4
    invoke-super {p0, p1, p2, p3}, Lorg/apache/lucene/queryparser/classic/QueryParser;->getFieldQuery(Ljava/lang/String;Ljava/lang/String;Z)Lorg/apache/lucene/search/Query;

    move-result-object v3

    .line 158
    .restart local v3    # "q":Lorg/apache/lucene/search/Query;
    goto :goto_1
.end method

.method protected getFuzzyQuery(Ljava/lang/String;Ljava/lang/String;F)Lorg/apache/lucene/search/Query;
    .locals 5
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "termStr"    # Ljava/lang/String;
    .param p3, "minSimilarity"    # F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/classic/ParseException;
        }
    .end annotation

    .prologue
    .line 165
    if-nez p1, :cond_1

    .line 166
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 167
    .local v0, "clauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/BooleanClause;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/queryparser/classic/MultiFieldQueryParser;->fields:[Ljava/lang/String;

    array-length v2, v2

    if-lt v1, v2, :cond_0

    .line 171
    const/4 v2, 0x1

    invoke-virtual {p0, v0, v2}, Lorg/apache/lucene/queryparser/classic/MultiFieldQueryParser;->getBooleanQuery(Ljava/util/List;Z)Lorg/apache/lucene/search/Query;

    move-result-object v2

    .line 173
    .end local v0    # "clauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/BooleanClause;>;"
    .end local v1    # "i":I
    :goto_1
    return-object v2

    .line 168
    .restart local v0    # "clauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/BooleanClause;>;"
    .restart local v1    # "i":I
    :cond_0
    new-instance v2, Lorg/apache/lucene/search/BooleanClause;

    iget-object v3, p0, Lorg/apache/lucene/queryparser/classic/MultiFieldQueryParser;->fields:[Ljava/lang/String;

    aget-object v3, v3, v1

    invoke-virtual {p0, v3, p2, p3}, Lorg/apache/lucene/queryparser/classic/MultiFieldQueryParser;->getFuzzyQuery(Ljava/lang/String;Ljava/lang/String;F)Lorg/apache/lucene/search/Query;

    move-result-object v3

    .line 169
    sget-object v4, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-direct {v2, v3, v4}, Lorg/apache/lucene/search/BooleanClause;-><init>(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 168
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 167
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 173
    .end local v0    # "clauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/BooleanClause;>;"
    .end local v1    # "i":I
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lorg/apache/lucene/queryparser/classic/QueryParser;->getFuzzyQuery(Ljava/lang/String;Ljava/lang/String;F)Lorg/apache/lucene/search/Query;

    move-result-object v2

    goto :goto_1
.end method

.method protected getPrefixQuery(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/lucene/search/Query;
    .locals 5
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "termStr"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/classic/ParseException;
        }
    .end annotation

    .prologue
    .line 179
    if-nez p1, :cond_1

    .line 180
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 181
    .local v0, "clauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/BooleanClause;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/queryparser/classic/MultiFieldQueryParser;->fields:[Ljava/lang/String;

    array-length v2, v2

    if-lt v1, v2, :cond_0

    .line 185
    const/4 v2, 0x1

    invoke-virtual {p0, v0, v2}, Lorg/apache/lucene/queryparser/classic/MultiFieldQueryParser;->getBooleanQuery(Ljava/util/List;Z)Lorg/apache/lucene/search/Query;

    move-result-object v2

    .line 187
    .end local v0    # "clauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/BooleanClause;>;"
    .end local v1    # "i":I
    :goto_1
    return-object v2

    .line 182
    .restart local v0    # "clauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/BooleanClause;>;"
    .restart local v1    # "i":I
    :cond_0
    new-instance v2, Lorg/apache/lucene/search/BooleanClause;

    iget-object v3, p0, Lorg/apache/lucene/queryparser/classic/MultiFieldQueryParser;->fields:[Ljava/lang/String;

    aget-object v3, v3, v1

    invoke-virtual {p0, v3, p2}, Lorg/apache/lucene/queryparser/classic/MultiFieldQueryParser;->getPrefixQuery(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/lucene/search/Query;

    move-result-object v3

    .line 183
    sget-object v4, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-direct {v2, v3, v4}, Lorg/apache/lucene/search/BooleanClause;-><init>(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 182
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 181
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 187
    .end local v0    # "clauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/BooleanClause;>;"
    .end local v1    # "i":I
    :cond_1
    invoke-super {p0, p1, p2}, Lorg/apache/lucene/queryparser/classic/QueryParser;->getPrefixQuery(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/lucene/search/Query;

    move-result-object v2

    goto :goto_1
.end method

.method protected getRangeQuery(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Lorg/apache/lucene/search/Query;
    .locals 9
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "part1"    # Ljava/lang/String;
    .param p3, "part2"    # Ljava/lang/String;
    .param p4, "startInclusive"    # Z
    .param p5, "endInclusive"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/classic/ParseException;
        }
    .end annotation

    .prologue
    .line 206
    if-nez p1, :cond_1

    .line 207
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 208
    .local v6, "clauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/BooleanClause;>;"
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    iget-object v0, p0, Lorg/apache/lucene/queryparser/classic/MultiFieldQueryParser;->fields:[Ljava/lang/String;

    array-length v0, v0

    if-lt v7, v0, :cond_0

    .line 212
    const/4 v0, 0x1

    invoke-virtual {p0, v6, v0}, Lorg/apache/lucene/queryparser/classic/MultiFieldQueryParser;->getBooleanQuery(Ljava/util/List;Z)Lorg/apache/lucene/search/Query;

    move-result-object v0

    .line 214
    .end local v6    # "clauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/BooleanClause;>;"
    .end local v7    # "i":I
    :goto_1
    return-object v0

    .line 209
    .restart local v6    # "clauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/BooleanClause;>;"
    .restart local v7    # "i":I
    :cond_0
    new-instance v8, Lorg/apache/lucene/search/BooleanClause;

    iget-object v0, p0, Lorg/apache/lucene/queryparser/classic/MultiFieldQueryParser;->fields:[Ljava/lang/String;

    aget-object v1, v0, v7

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lorg/apache/lucene/queryparser/classic/MultiFieldQueryParser;->getRangeQuery(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Lorg/apache/lucene/search/Query;

    move-result-object v0

    .line 210
    sget-object v1, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-direct {v8, v0, v1}, Lorg/apache/lucene/search/BooleanClause;-><init>(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 209
    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 208
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 214
    .end local v6    # "clauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/BooleanClause;>;"
    .end local v7    # "i":I
    :cond_1
    invoke-super/range {p0 .. p5}, Lorg/apache/lucene/queryparser/classic/QueryParser;->getRangeQuery(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Lorg/apache/lucene/search/Query;

    move-result-object v0

    goto :goto_1
.end method

.method protected getRegexpQuery(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/lucene/search/Query;
    .locals 5
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "termStr"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/classic/ParseException;
        }
    .end annotation

    .prologue
    .line 222
    if-nez p1, :cond_1

    .line 223
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 224
    .local v0, "clauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/BooleanClause;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/queryparser/classic/MultiFieldQueryParser;->fields:[Ljava/lang/String;

    array-length v2, v2

    if-lt v1, v2, :cond_0

    .line 228
    const/4 v2, 0x1

    invoke-virtual {p0, v0, v2}, Lorg/apache/lucene/queryparser/classic/MultiFieldQueryParser;->getBooleanQuery(Ljava/util/List;Z)Lorg/apache/lucene/search/Query;

    move-result-object v2

    .line 230
    .end local v0    # "clauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/BooleanClause;>;"
    .end local v1    # "i":I
    :goto_1
    return-object v2

    .line 225
    .restart local v0    # "clauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/BooleanClause;>;"
    .restart local v1    # "i":I
    :cond_0
    new-instance v2, Lorg/apache/lucene/search/BooleanClause;

    iget-object v3, p0, Lorg/apache/lucene/queryparser/classic/MultiFieldQueryParser;->fields:[Ljava/lang/String;

    aget-object v3, v3, v1

    invoke-virtual {p0, v3, p2}, Lorg/apache/lucene/queryparser/classic/MultiFieldQueryParser;->getRegexpQuery(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/lucene/search/Query;

    move-result-object v3

    .line 226
    sget-object v4, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-direct {v2, v3, v4}, Lorg/apache/lucene/search/BooleanClause;-><init>(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 225
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 224
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 230
    .end local v0    # "clauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/BooleanClause;>;"
    .end local v1    # "i":I
    :cond_1
    invoke-super {p0, p1, p2}, Lorg/apache/lucene/queryparser/classic/QueryParser;->getRegexpQuery(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/lucene/search/Query;

    move-result-object v2

    goto :goto_1
.end method

.method protected getWildcardQuery(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/lucene/search/Query;
    .locals 5
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "termStr"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/classic/ParseException;
        }
    .end annotation

    .prologue
    .line 192
    if-nez p1, :cond_1

    .line 193
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 194
    .local v0, "clauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/BooleanClause;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/queryparser/classic/MultiFieldQueryParser;->fields:[Ljava/lang/String;

    array-length v2, v2

    if-lt v1, v2, :cond_0

    .line 198
    const/4 v2, 0x1

    invoke-virtual {p0, v0, v2}, Lorg/apache/lucene/queryparser/classic/MultiFieldQueryParser;->getBooleanQuery(Ljava/util/List;Z)Lorg/apache/lucene/search/Query;

    move-result-object v2

    .line 200
    .end local v0    # "clauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/BooleanClause;>;"
    .end local v1    # "i":I
    :goto_1
    return-object v2

    .line 195
    .restart local v0    # "clauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/BooleanClause;>;"
    .restart local v1    # "i":I
    :cond_0
    new-instance v2, Lorg/apache/lucene/search/BooleanClause;

    iget-object v3, p0, Lorg/apache/lucene/queryparser/classic/MultiFieldQueryParser;->fields:[Ljava/lang/String;

    aget-object v3, v3, v1

    invoke-virtual {p0, v3, p2}, Lorg/apache/lucene/queryparser/classic/MultiFieldQueryParser;->getWildcardQuery(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/lucene/search/Query;

    move-result-object v3

    .line 196
    sget-object v4, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-direct {v2, v3, v4}, Lorg/apache/lucene/search/BooleanClause;-><init>(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 195
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 194
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 200
    .end local v0    # "clauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/BooleanClause;>;"
    .end local v1    # "i":I
    :cond_1
    invoke-super {p0, p1, p2}, Lorg/apache/lucene/queryparser/classic/QueryParser;->getWildcardQuery(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/lucene/search/Query;

    move-result-object v2

    goto :goto_1
.end method
