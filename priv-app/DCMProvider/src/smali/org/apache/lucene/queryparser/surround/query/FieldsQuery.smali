.class public Lorg/apache/lucene/queryparser/surround/query/FieldsQuery;
.super Lorg/apache/lucene/queryparser/surround/query/SrndQuery;
.source "FieldsQuery.java"


# instance fields
.field private final OrOperatorName:Ljava/lang/String;

.field private fieldNames:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final fieldOp:C

.field private q:Lorg/apache/lucene/queryparser/surround/query/SrndQuery;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/queryparser/surround/query/SrndQuery;Ljava/lang/String;C)V
    .locals 1
    .param p1, "q"    # Lorg/apache/lucene/queryparser/surround/query/SrndQuery;
    .param p2, "fieldName"    # Ljava/lang/String;
    .param p3, "fieldOp"    # C

    .prologue
    .line 40
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/surround/query/SrndQuery;-><init>()V

    .line 32
    const-string v0, "OR"

    iput-object v0, p0, Lorg/apache/lucene/queryparser/surround/query/FieldsQuery;->OrOperatorName:Ljava/lang/String;

    .line 41
    iput-object p1, p0, Lorg/apache/lucene/queryparser/surround/query/FieldsQuery;->q:Lorg/apache/lucene/queryparser/surround/query/SrndQuery;

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/queryparser/surround/query/FieldsQuery;->fieldNames:Ljava/util/List;

    .line 43
    iget-object v0, p0, Lorg/apache/lucene/queryparser/surround/query/FieldsQuery;->fieldNames:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 44
    iput-char p3, p0, Lorg/apache/lucene/queryparser/surround/query/FieldsQuery;->fieldOp:C

    .line 45
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/queryparser/surround/query/SrndQuery;Ljava/util/List;C)V
    .locals 1
    .param p1, "q"    # Lorg/apache/lucene/queryparser/surround/query/SrndQuery;
    .param p3, "fieldOp"    # C
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/queryparser/surround/query/SrndQuery;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;C)V"
        }
    .end annotation

    .prologue
    .line 34
    .local p2, "fieldNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/surround/query/SrndQuery;-><init>()V

    .line 32
    const-string v0, "OR"

    iput-object v0, p0, Lorg/apache/lucene/queryparser/surround/query/FieldsQuery;->OrOperatorName:Ljava/lang/String;

    .line 35
    iput-object p1, p0, Lorg/apache/lucene/queryparser/surround/query/FieldsQuery;->q:Lorg/apache/lucene/queryparser/surround/query/SrndQuery;

    .line 36
    iput-object p2, p0, Lorg/apache/lucene/queryparser/surround/query/FieldsQuery;->fieldNames:Ljava/util/List;

    .line 37
    iput-char p3, p0, Lorg/apache/lucene/queryparser/surround/query/FieldsQuery;->fieldOp:C

    .line 38
    return-void
.end method


# virtual methods
.method protected fieldNamesToString(Ljava/lang/StringBuilder;)V
    .locals 2
    .param p1, "r"    # Ljava/lang/StringBuilder;

    .prologue
    .line 92
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/query/FieldsQuery;->getFieldNames()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v0

    .line 93
    .local v0, "fni":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 97
    return-void

    .line 94
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/query/FieldsQuery;->getFieldOperator()C

    move-result v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public getFieldNames()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 77
    iget-object v0, p0, Lorg/apache/lucene/queryparser/surround/query/FieldsQuery;->fieldNames:Ljava/util/List;

    return-object v0
.end method

.method public getFieldOperator()C
    .locals 1

    .prologue
    .line 79
    iget-char v0, p0, Lorg/apache/lucene/queryparser/surround/query/FieldsQuery;->fieldOp:C

    return v0
.end method

.method public isFieldsSubQueryAcceptable()Z
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x0

    return v0
.end method

.method public makeLuceneQueryFieldNoBoost(Ljava/lang/String;Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;)Lorg/apache/lucene/search/Query;
    .locals 1
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "qf"    # Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;

    .prologue
    .line 73
    invoke-virtual {p0, p2}, Lorg/apache/lucene/queryparser/surround/query/FieldsQuery;->makeLuceneQueryNoBoost(Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;)Lorg/apache/lucene/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public makeLuceneQueryNoBoost(Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;)Lorg/apache/lucene/search/Query;
    .locals 8
    .param p1, "qf"    # Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;

    .prologue
    const/4 v7, 0x1

    .line 53
    iget-object v4, p0, Lorg/apache/lucene/queryparser/surround/query/FieldsQuery;->fieldNames:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ne v4, v7, :cond_0

    .line 54
    iget-object v5, p0, Lorg/apache/lucene/queryparser/surround/query/FieldsQuery;->q:Lorg/apache/lucene/queryparser/surround/query/SrndQuery;

    iget-object v4, p0, Lorg/apache/lucene/queryparser/surround/query/FieldsQuery;->fieldNames:Ljava/util/List;

    const/4 v6, 0x0

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v5, v4, p1}, Lorg/apache/lucene/queryparser/surround/query/SrndQuery;->makeLuceneQueryFieldNoBoost(Ljava/lang/String;Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;)Lorg/apache/lucene/search/Query;

    move-result-object v4

    .line 67
    :goto_0
    return-object v4

    .line 56
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 57
    .local v3, "queries":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/surround/query/SrndQuery;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/query/FieldsQuery;->getFieldNames()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v0

    .line 59
    .local v0, "fni":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 63
    new-instance v1, Lorg/apache/lucene/queryparser/surround/query/OrQuery;

    .line 65
    const-string v4, "OR"

    .line 63
    invoke-direct {v1, v3, v7, v4}, Lorg/apache/lucene/queryparser/surround/query/OrQuery;-><init>(Ljava/util/List;ZLjava/lang/String;)V

    .line 67
    .local v1, "oq":Lorg/apache/lucene/queryparser/surround/query/OrQuery;
    const/4 v4, 0x0

    invoke-virtual {v1, v4, p1}, Lorg/apache/lucene/queryparser/surround/query/OrQuery;->makeLuceneQueryField(Ljava/lang/String;Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;)Lorg/apache/lucene/search/Query;

    move-result-object v4

    goto :goto_0

    .line 60
    .end local v1    # "oq":Lorg/apache/lucene/queryparser/surround/query/OrQuery;
    :cond_1
    iget-object v4, p0, Lorg/apache/lucene/queryparser/surround/query/FieldsQuery;->q:Lorg/apache/lucene/queryparser/surround/query/SrndQuery;

    invoke-virtual {v4}, Lorg/apache/lucene/queryparser/surround/query/SrndQuery;->clone()Lorg/apache/lucene/queryparser/surround/query/SrndQuery;

    move-result-object v2

    .line 61
    .local v2, "qc":Lorg/apache/lucene/queryparser/surround/query/SrndQuery;
    new-instance v5, Lorg/apache/lucene/queryparser/surround/query/FieldsQuery;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    iget-char v6, p0, Lorg/apache/lucene/queryparser/surround/query/FieldsQuery;->fieldOp:C

    invoke-direct {v5, v2, v4, v6}, Lorg/apache/lucene/queryparser/surround/query/FieldsQuery;-><init>(Lorg/apache/lucene/queryparser/surround/query/SrndQuery;Ljava/lang/String;C)V

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 83
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 84
    .local v0, "r":Ljava/lang/StringBuilder;
    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 85
    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/surround/query/FieldsQuery;->fieldNamesToString(Ljava/lang/StringBuilder;)V

    .line 86
    iget-object v1, p0, Lorg/apache/lucene/queryparser/surround/query/FieldsQuery;->q:Lorg/apache/lucene/queryparser/surround/query/SrndQuery;

    invoke-virtual {v1}, Lorg/apache/lucene/queryparser/surround/query/SrndQuery;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 87
    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 88
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
