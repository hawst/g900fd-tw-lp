.class public Lorg/apache/lucene/queryparser/flexible/core/nodes/TokenizedPhraseQueryNode;
.super Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;
.source "TokenizedPhraseQueryNode.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldableNode;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;-><init>()V

    .line 32
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/TokenizedPhraseQueryNode;->setLeaf(Z)V

    .line 33
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/TokenizedPhraseQueryNode;->allocate()V

    .line 34
    return-void
.end method


# virtual methods
.method public cloneTree()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 68
    .line 69
    invoke-super {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;->cloneTree()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v0

    .line 68
    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/TokenizedPhraseQueryNode;

    .line 73
    .local v0, "clone":Lorg/apache/lucene/queryparser/flexible/core/nodes/TokenizedPhraseQueryNode;
    return-object v0
.end method

.method public getField()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 78
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/TokenizedPhraseQueryNode;->getChildren()Ljava/util/List;

    move-result-object v0

    .line 80
    .local v0, "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 81
    :cond_0
    const/4 v1, 0x0

    .line 84
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldableNode;

    invoke-interface {v1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldableNode;->getField()Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_0
.end method

.method public setField(Ljava/lang/CharSequence;)V
    .locals 4
    .param p1, "fieldName"    # Ljava/lang/CharSequence;

    .prologue
    .line 91
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/TokenizedPhraseQueryNode;->getChildren()Ljava/util/List;

    move-result-object v1

    .line 93
    .local v1, "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    if-eqz v1, :cond_1

    .line 95
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/TokenizedPhraseQueryNode;->getChildren()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 105
    :cond_1
    return-void

    .line 95
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    .line 97
    .local v0, "child":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    instance-of v3, v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldableNode;

    if-eqz v3, :cond_0

    .line 98
    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldableNode;

    .end local v0    # "child":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    invoke-interface {v0, p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldableNode;->setField(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public toQueryString(Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;)Ljava/lang/CharSequence;
    .locals 6
    .param p1, "escapeSyntaxParser"    # Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;

    .prologue
    .line 53
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/TokenizedPhraseQueryNode;->getChildren()Ljava/util/List;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/TokenizedPhraseQueryNode;->getChildren()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_1

    .line 54
    :cond_0
    const-string v3, ""

    .line 63
    :goto_0
    return-object v3

    .line 56
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 57
    .local v2, "sb":Ljava/lang/StringBuilder;
    const-string v1, ""

    .line 58
    .local v1, "filler":Ljava/lang/String;
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/TokenizedPhraseQueryNode;->getChildren()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_2

    .line 63
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[TP["

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 58
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    .line 59
    .local v0, "child":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v0, p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->toQueryString(Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 60
    const-string v1, ","

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 38
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/TokenizedPhraseQueryNode;->getChildren()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/TokenizedPhraseQueryNode;->getChildren()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_1

    .line 39
    :cond_0
    const-string v2, "<tokenizedphrase/>"

    .line 47
    :goto_0
    return-object v2

    .line 40
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 41
    .local v1, "sb":Ljava/lang/StringBuilder;
    const-string v2, "<tokenizedtphrase>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 42
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/TokenizedPhraseQueryNode;->getChildren()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 46
    const-string v2, "\n</tokenizedphrase>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 47
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 42
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    .line 43
    .local v0, "child":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    const-string v3, "\n"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 44
    invoke-interface {v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method
