.class public Lorg/apache/lucene/queryparser/xml/builders/FuzzyLikeThisQueryBuilder;
.super Ljava/lang/Object;
.source "FuzzyLikeThisQueryBuilder.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/xml/QueryBuilder;


# static fields
.field private static final DEFAULT_IGNORE_TF:Z = false

.field private static final DEFAULT_MAX_NUM_TERMS:I = 0x32

.field private static final DEFAULT_MIN_SIMILARITY:F = 2.0f

.field private static final DEFAULT_PREFIX_LENGTH:I = 0x1


# instance fields
.field private final analyzer:Lorg/apache/lucene/analysis/Analyzer;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/Analyzer;)V
    .locals 0
    .param p1, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lorg/apache/lucene/queryparser/xml/builders/FuzzyLikeThisQueryBuilder;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    .line 44
    return-void
.end method


# virtual methods
.method public getQuery(Lorg/w3c/dom/Element;)Lorg/apache/lucene/search/Query;
    .locals 11
    .param p1, "e"    # Lorg/w3c/dom/Element;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/xml/ParserException;
        }
    .end annotation

    .prologue
    .line 48
    const-string v9, "Field"

    invoke-interface {p1, v9}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v6

    .line 49
    .local v6, "nl":Lorg/w3c/dom/NodeList;
    const-string v9, "maxNumTerms"

    const/16 v10, 0x32

    invoke-static {p1, v9, v10}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttribute(Lorg/w3c/dom/Element;Ljava/lang/String;I)I

    move-result v4

    .line 50
    .local v4, "maxNumTerms":I
    new-instance v0, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;

    iget-object v9, p0, Lorg/apache/lucene/queryparser/xml/builders/FuzzyLikeThisQueryBuilder;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    invoke-direct {v0, v4, v9}, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;-><init>(ILorg/apache/lucene/analysis/Analyzer;)V

    .line 51
    .local v0, "fbq":Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;
    const-string v9, "ignoreTF"

    const/4 v10, 0x0

    invoke-static {p1, v9, v10}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttribute(Lorg/w3c/dom/Element;Ljava/lang/String;Z)Z

    move-result v9

    invoke-virtual {v0, v9}, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;->setIgnoreTF(Z)V

    .line 53
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-interface {v6}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v9

    if-lt v3, v9, :cond_0

    .line 63
    const-string v9, "boost"

    const/high16 v10, 0x3f800000    # 1.0f

    invoke-static {p1, v9, v10}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttribute(Lorg/w3c/dom/Element;Ljava/lang/String;F)F

    move-result v9

    invoke-virtual {v0, v9}, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;->setBoost(F)V

    .line 64
    return-object v0

    .line 54
    :cond_0
    invoke-interface {v6, v3}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v1

    check-cast v1, Lorg/w3c/dom/Element;

    .line 55
    .local v1, "fieldElem":Lorg/w3c/dom/Element;
    const-string v9, "minSimilarity"

    const/high16 v10, 0x40000000    # 2.0f

    invoke-static {v1, v9, v10}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttribute(Lorg/w3c/dom/Element;Ljava/lang/String;F)F

    move-result v5

    .line 56
    .local v5, "minSimilarity":F
    const-string v9, "prefixLength"

    const/4 v10, 0x1

    invoke-static {v1, v9, v10}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttribute(Lorg/w3c/dom/Element;Ljava/lang/String;I)I

    move-result v7

    .line 57
    .local v7, "prefixLength":I
    const-string v9, "fieldName"

    invoke-static {v1, v9}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttributeWithInheritance(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 59
    .local v2, "fieldName":Ljava/lang/String;
    invoke-static {v1}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getText(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v8

    .line 60
    .local v8, "value":Ljava/lang/String;
    invoke-virtual {v0, v8, v2, v5, v7}, Lorg/apache/lucene/sandbox/queries/FuzzyLikeThisQuery;->addTerms(Ljava/lang/String;Ljava/lang/String;FI)V

    .line 53
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method
