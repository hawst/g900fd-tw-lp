.class public Lorg/apache/lucene/queryparser/flexible/core/nodes/BoostQueryNode;
.super Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;
.source "BoostQueryNode.java"


# instance fields
.field private value:F


# direct methods
.method public constructor <init>(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;F)V
    .locals 6
    .param p1, "query"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .param p2, "value"    # F

    .prologue
    const/4 v5, 0x0

    .line 46
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;-><init>()V

    .line 36
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/BoostQueryNode;->value:F

    .line 47
    if-nez p1, :cond_0

    .line 48
    new-instance v0, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeError;

    new-instance v1, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;

    .line 49
    sget-object v2, Lorg/apache/lucene/queryparser/flexible/core/messages/QueryParserMessages;->NODE_ACTION_NOT_SUPPORTED:Ljava/lang/String;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "query"

    aput-object v4, v3, v5

    const/4 v4, 0x1

    const-string v5, "null"

    aput-object v5, v3, v4

    invoke-direct {v1, v2, v3}, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 48
    invoke-direct {v0, v1}, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeError;-><init>(Lorg/apache/lucene/queryparser/flexible/messages/Message;)V

    throw v0

    .line 52
    :cond_0
    iput p2, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/BoostQueryNode;->value:F

    .line 53
    invoke-virtual {p0, v5}, Lorg/apache/lucene/queryparser/flexible/core/nodes/BoostQueryNode;->setLeaf(Z)V

    .line 54
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/BoostQueryNode;->allocate()V

    .line 55
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/BoostQueryNode;->add(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)V

    .line 56
    return-void
.end method

.method private getValueString()Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 89
    iget v1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/BoostQueryNode;->value:F

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    .line 90
    .local v0, "f":Ljava/lang/Float;
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {v0}, Ljava/lang/Float;->longValue()J

    move-result-wide v2

    long-to-float v2, v2

    cmpl-float v1, v1, v2

    if-nez v1, :cond_0

    .line 91
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Float;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 93
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method public cloneTree()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 113
    invoke-super {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;->cloneTree()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/BoostQueryNode;

    .line 115
    .local v0, "clone":Lorg/apache/lucene/queryparser/flexible/core/nodes/BoostQueryNode;
    iget v1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/BoostQueryNode;->value:F

    iput v1, v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/BoostQueryNode;->value:F

    .line 117
    return-object v0
.end method

.method public getChild()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 2

    .prologue
    .line 64
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/BoostQueryNode;->getChildren()Ljava/util/List;

    move-result-object v0

    .line 66
    .local v0, "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 67
    :cond_0
    const/4 v1, 0x0

    .line 70
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    goto :goto_0
.end method

.method public getValue()F
    .locals 1

    .prologue
    .line 80
    iget v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/BoostQueryNode;->value:F

    return v0
.end method

.method public toQueryString(Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;)Ljava/lang/CharSequence;
    .locals 2
    .param p1, "escapeSyntaxParser"    # Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;

    .prologue
    .line 105
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/BoostQueryNode;->getChild()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v0

    if-nez v0, :cond_0

    .line 106
    const-string v0, ""

    .line 107
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/BoostQueryNode;->getChild()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v1

    invoke-interface {v1, p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->toQueryString(Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "^"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 108
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/BoostQueryNode;->getValueString()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 107
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 99
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "<boost value=\'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/BoostQueryNode;->getValueString()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 100
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/BoostQueryNode;->getChild()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n</boost>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 99
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
