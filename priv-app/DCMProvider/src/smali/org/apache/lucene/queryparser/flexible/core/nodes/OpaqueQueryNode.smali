.class public Lorg/apache/lucene/queryparser/flexible/core/nodes/OpaqueQueryNode;
.super Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;
.source "OpaqueQueryNode.java"


# instance fields
.field private schema:Ljava/lang/CharSequence;

.field private value:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "schema"    # Ljava/lang/CharSequence;
    .param p2, "value"    # Ljava/lang/CharSequence;

    .prologue
    const/4 v0, 0x0

    .line 39
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;-><init>()V

    .line 29
    iput-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/OpaqueQueryNode;->schema:Ljava/lang/CharSequence;

    .line 31
    iput-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/OpaqueQueryNode;->value:Ljava/lang/CharSequence;

    .line 40
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/OpaqueQueryNode;->setLeaf(Z)V

    .line 42
    iput-object p1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/OpaqueQueryNode;->schema:Ljava/lang/CharSequence;

    .line 43
    iput-object p2, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/OpaqueQueryNode;->value:Ljava/lang/CharSequence;

    .line 45
    return-void
.end method


# virtual methods
.method public cloneTree()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 59
    invoke-super {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;->cloneTree()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/OpaqueQueryNode;

    .line 61
    .local v0, "clone":Lorg/apache/lucene/queryparser/flexible/core/nodes/OpaqueQueryNode;
    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/OpaqueQueryNode;->schema:Ljava/lang/CharSequence;

    iput-object v1, v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/OpaqueQueryNode;->schema:Ljava/lang/CharSequence;

    .line 62
    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/OpaqueQueryNode;->value:Ljava/lang/CharSequence;

    iput-object v1, v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/OpaqueQueryNode;->value:Ljava/lang/CharSequence;

    .line 64
    return-object v0
.end method

.method public getSchema()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/OpaqueQueryNode;->schema:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getValue()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/OpaqueQueryNode;->value:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public toQueryString(Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;)Ljava/lang/CharSequence;
    .locals 2
    .param p1, "escapeSyntaxParser"    # Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;

    .prologue
    .line 54
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "@"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/OpaqueQueryNode;->schema:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/OpaqueQueryNode;->value:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 49
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "<opaque schema=\'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/OpaqueQueryNode;->schema:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\' value=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/OpaqueQueryNode;->value:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'/>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
