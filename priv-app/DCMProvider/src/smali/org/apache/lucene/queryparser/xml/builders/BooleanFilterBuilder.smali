.class public Lorg/apache/lucene/queryparser/xml/builders/BooleanFilterBuilder;
.super Ljava/lang/Object;
.source "BooleanFilterBuilder.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/xml/FilterBuilder;


# instance fields
.field private final factory:Lorg/apache/lucene/queryparser/xml/FilterBuilder;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/queryparser/xml/FilterBuilder;)V
    .locals 0
    .param p1, "factory"    # Lorg/apache/lucene/queryparser/xml/FilterBuilder;

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lorg/apache/lucene/queryparser/xml/builders/BooleanFilterBuilder;->factory:Lorg/apache/lucene/queryparser/xml/FilterBuilder;

    .line 43
    return-void
.end method


# virtual methods
.method public getFilter(Lorg/w3c/dom/Element;)Lorg/apache/lucene/search/Filter;
    .locals 10
    .param p1, "e"    # Lorg/w3c/dom/Element;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/xml/ParserException;
        }
    .end annotation

    .prologue
    .line 47
    new-instance v0, Lorg/apache/lucene/queries/BooleanFilter;

    invoke-direct {v0}, Lorg/apache/lucene/queries/BooleanFilter;-><init>()V

    .line 48
    .local v0, "bf":Lorg/apache/lucene/queries/BooleanFilter;
    invoke-interface {p1}, Lorg/w3c/dom/Element;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v5

    .line 50
    .local v5, "nl":Lorg/w3c/dom/NodeList;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-interface {v5}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v8

    if-lt v4, v8, :cond_0

    .line 62
    return-object v0

    .line 51
    :cond_0
    invoke-interface {v5, v4}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v6

    .line 52
    .local v6, "node":Lorg/w3c/dom/Node;
    invoke-interface {v6}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Clause"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    move-object v1, v6

    .line 53
    check-cast v1, Lorg/w3c/dom/Element;

    .line 54
    .local v1, "clauseElem":Lorg/w3c/dom/Element;
    invoke-static {v1}, Lorg/apache/lucene/queryparser/xml/builders/BooleanQueryBuilder;->getOccursValue(Lorg/w3c/dom/Element;)Lorg/apache/lucene/search/BooleanClause$Occur;

    move-result-object v7

    .line 56
    .local v7, "occurs":Lorg/apache/lucene/search/BooleanClause$Occur;
    invoke-static {v1}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getFirstChildOrFail(Lorg/w3c/dom/Element;)Lorg/w3c/dom/Element;

    move-result-object v2

    .line 57
    .local v2, "clauseFilter":Lorg/w3c/dom/Element;
    iget-object v8, p0, Lorg/apache/lucene/queryparser/xml/builders/BooleanFilterBuilder;->factory:Lorg/apache/lucene/queryparser/xml/FilterBuilder;

    invoke-interface {v8, v2}, Lorg/apache/lucene/queryparser/xml/FilterBuilder;->getFilter(Lorg/w3c/dom/Element;)Lorg/apache/lucene/search/Filter;

    move-result-object v3

    .line 58
    .local v3, "f":Lorg/apache/lucene/search/Filter;
    new-instance v8, Lorg/apache/lucene/queries/FilterClause;

    invoke-direct {v8, v3, v7}, Lorg/apache/lucene/queries/FilterClause;-><init>(Lorg/apache/lucene/search/Filter;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    invoke-virtual {v0, v8}, Lorg/apache/lucene/queries/BooleanFilter;->add(Lorg/apache/lucene/queries/FilterClause;)V

    .line 50
    .end local v1    # "clauseElem":Lorg/w3c/dom/Element;
    .end local v2    # "clauseFilter":Lorg/w3c/dom/Element;
    .end local v3    # "f":Lorg/apache/lucene/search/Filter;
    .end local v7    # "occurs":Lorg/apache/lucene/search/BooleanClause$Occur;
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method
