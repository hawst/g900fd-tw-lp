.class public Lorg/apache/lucene/queryparser/flexible/core/QueryNodeError;
.super Ljava/lang/Error;
.source "QueryNodeError.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/flexible/messages/NLSException;


# instance fields
.field private message:Lorg/apache/lucene/queryparser/flexible/messages/Message;


# direct methods
.method public constructor <init>(Ljava/lang/Throwable;)V
    .locals 0
    .param p1, "throwable"    # Ljava/lang/Throwable;

    .prologue
    .line 48
    invoke-direct {p0, p1}, Ljava/lang/Error;-><init>(Ljava/lang/Throwable;)V

    .line 49
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/queryparser/flexible/messages/Message;)V
    .locals 1
    .param p1, "message"    # Lorg/apache/lucene/queryparser/flexible/messages/Message;

    .prologue
    .line 37
    invoke-interface {p1}, Lorg/apache/lucene/queryparser/flexible/messages/Message;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    .line 39
    iput-object p1, p0, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeError;->message:Lorg/apache/lucene/queryparser/flexible/messages/Message;

    .line 41
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/queryparser/flexible/messages/Message;Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "message"    # Lorg/apache/lucene/queryparser/flexible/messages/Message;
    .param p2, "throwable"    # Ljava/lang/Throwable;

    .prologue
    .line 58
    invoke-interface {p1}, Lorg/apache/lucene/queryparser/flexible/messages/Message;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Ljava/lang/Error;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 60
    iput-object p1, p0, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeError;->message:Lorg/apache/lucene/queryparser/flexible/messages/Message;

    .line 62
    return-void
.end method


# virtual methods
.method public getMessageObject()Lorg/apache/lucene/queryparser/flexible/messages/Message;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeError;->message:Lorg/apache/lucene/queryparser/flexible/messages/Message;

    return-object v0
.end method
