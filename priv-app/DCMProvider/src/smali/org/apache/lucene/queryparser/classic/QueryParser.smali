.class public Lorg/apache/lucene/queryparser/classic/QueryParser;
.super Lorg/apache/lucene/queryparser/classic/QueryParserBase;
.source "QueryParser.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/classic/QueryParserConstants;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/queryparser/classic/QueryParser$JJCalls;,
        Lorg/apache/lucene/queryparser/classic/QueryParser$LookaheadSuccess;,
        Lorg/apache/lucene/queryparser/classic/QueryParser$Operator;
    }
.end annotation


# static fields
.field private static jj_la1_0:[I

.field private static jj_la1_1:[I


# instance fields
.field private final jj_2_rtns:[Lorg/apache/lucene/queryparser/classic/QueryParser$JJCalls;

.field private jj_endpos:I

.field private jj_expentries:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<[I>;"
        }
    .end annotation
.end field

.field private jj_expentry:[I

.field private jj_gc:I

.field private jj_gen:I

.field private jj_kind:I

.field private jj_la:I

.field private final jj_la1:[I

.field private jj_lastpos:Lorg/apache/lucene/queryparser/classic/Token;

.field private jj_lasttokens:[I

.field private final jj_ls:Lorg/apache/lucene/queryparser/classic/QueryParser$LookaheadSuccess;

.field public jj_nt:Lorg/apache/lucene/queryparser/classic/Token;

.field private jj_ntk:I

.field private jj_rescan:Z

.field private jj_scanpos:Lorg/apache/lucene/queryparser/classic/Token;

.field public token:Lorg/apache/lucene/queryparser/classic/Token;

.field public token_source:Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .prologue
    .line 516
    invoke-static {}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_la1_init_0()V

    .line 517
    invoke-static {}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_la1_init_1()V

    .line 518
    return-void
.end method

.method protected constructor <init>(Lorg/apache/lucene/queryparser/classic/CharStream;)V
    .locals 6
    .param p1, "stream"    # Lorg/apache/lucene/queryparser/classic/CharStream;

    .prologue
    const/16 v5, 0x15

    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 530
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/classic/QueryParserBase;-><init>()V

    .line 512
    new-array v1, v5, [I

    iput-object v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_la1:[I

    .line 525
    const/4 v1, 0x1

    new-array v1, v1, [Lorg/apache/lucene/queryparser/classic/QueryParser$JJCalls;

    iput-object v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_2_rtns:[Lorg/apache/lucene/queryparser/classic/QueryParser$JJCalls;

    .line 526
    iput-boolean v4, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_rescan:Z

    .line 527
    iput v4, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_gc:I

    .line 594
    new-instance v1, Lorg/apache/lucene/queryparser/classic/QueryParser$LookaheadSuccess;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lorg/apache/lucene/queryparser/classic/QueryParser$LookaheadSuccess;-><init>(Lorg/apache/lucene/queryparser/classic/QueryParser$LookaheadSuccess;)V

    iput-object v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ls:Lorg/apache/lucene/queryparser/classic/QueryParser$LookaheadSuccess;

    .line 643
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_expentries:Ljava/util/List;

    .line 645
    iput v3, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_kind:I

    .line 646
    const/16 v1, 0x64

    new-array v1, v1, [I

    iput-object v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_lasttokens:[I

    .line 531
    new-instance v1, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;

    invoke-direct {v1, p1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;-><init>(Lorg/apache/lucene/queryparser/classic/CharStream;)V

    iput-object v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->token_source:Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;

    .line 532
    new-instance v1, Lorg/apache/lucene/queryparser/classic/Token;

    invoke-direct {v1}, Lorg/apache/lucene/queryparser/classic/Token;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->token:Lorg/apache/lucene/queryparser/classic/Token;

    .line 533
    iput v3, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk:I

    .line 534
    iput v4, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_gen:I

    .line 535
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v5, :cond_0

    .line 536
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_2_rtns:[Lorg/apache/lucene/queryparser/classic/QueryParser$JJCalls;

    array-length v1, v1

    if-lt v0, v1, :cond_1

    .line 537
    return-void

    .line 535
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_la1:[I

    aput v3, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 536
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_2_rtns:[Lorg/apache/lucene/queryparser/classic/QueryParser$JJCalls;

    new-instance v2, Lorg/apache/lucene/queryparser/classic/QueryParser$JJCalls;

    invoke-direct {v2}, Lorg/apache/lucene/queryparser/classic/QueryParser$JJCalls;-><init>()V

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method protected constructor <init>(Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;)V
    .locals 6
    .param p1, "tm"    # Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;

    .prologue
    const/16 v5, 0x15

    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 550
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/classic/QueryParserBase;-><init>()V

    .line 512
    new-array v1, v5, [I

    iput-object v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_la1:[I

    .line 525
    const/4 v1, 0x1

    new-array v1, v1, [Lorg/apache/lucene/queryparser/classic/QueryParser$JJCalls;

    iput-object v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_2_rtns:[Lorg/apache/lucene/queryparser/classic/QueryParser$JJCalls;

    .line 526
    iput-boolean v4, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_rescan:Z

    .line 527
    iput v4, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_gc:I

    .line 594
    new-instance v1, Lorg/apache/lucene/queryparser/classic/QueryParser$LookaheadSuccess;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lorg/apache/lucene/queryparser/classic/QueryParser$LookaheadSuccess;-><init>(Lorg/apache/lucene/queryparser/classic/QueryParser$LookaheadSuccess;)V

    iput-object v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ls:Lorg/apache/lucene/queryparser/classic/QueryParser$LookaheadSuccess;

    .line 643
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_expentries:Ljava/util/List;

    .line 645
    iput v3, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_kind:I

    .line 646
    const/16 v1, 0x64

    new-array v1, v1, [I

    iput-object v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_lasttokens:[I

    .line 551
    iput-object p1, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->token_source:Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;

    .line 552
    new-instance v1, Lorg/apache/lucene/queryparser/classic/Token;

    invoke-direct {v1}, Lorg/apache/lucene/queryparser/classic/Token;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->token:Lorg/apache/lucene/queryparser/classic/Token;

    .line 553
    iput v3, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk:I

    .line 554
    iput v4, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_gen:I

    .line 555
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v5, :cond_0

    .line 556
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_2_rtns:[Lorg/apache/lucene/queryparser/classic/QueryParser$JJCalls;

    array-length v1, v1

    if-lt v0, v1, :cond_1

    .line 557
    return-void

    .line 555
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_la1:[I

    aput v3, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 556
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_2_rtns:[Lorg/apache/lucene/queryparser/classic/QueryParser$JJCalls;

    new-instance v2, Lorg/apache/lucene/queryparser/classic/QueryParser$JJCalls;

    invoke-direct {v2}, Lorg/apache/lucene/queryparser/classic/QueryParser$JJCalls;-><init>()V

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer;)V
    .locals 3
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "f"    # Ljava/lang/String;
    .param p3, "a"    # Lorg/apache/lucene/analysis/Analyzer;

    .prologue
    .line 100
    new-instance v0, Lorg/apache/lucene/queryparser/classic/FastCharStream;

    new-instance v1, Ljava/io/StringReader;

    const-string v2, ""

    invoke-direct {v1, v2}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lorg/apache/lucene/queryparser/classic/FastCharStream;-><init>(Ljava/io/Reader;)V

    invoke-direct {p0, v0}, Lorg/apache/lucene/queryparser/classic/QueryParser;-><init>(Lorg/apache/lucene/queryparser/classic/CharStream;)V

    .line 101
    invoke-virtual {p0, p1, p2, p3}, Lorg/apache/lucene/queryparser/classic/QueryParser;->init(Lorg/apache/lucene/util/Version;Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer;)V

    .line 102
    return-void
.end method

.method private jj_2_1(I)Z
    .locals 4
    .param p1, "xla"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 474
    iput p1, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_la:I

    iget-object v3, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->token:Lorg/apache/lucene/queryparser/classic/Token;

    iput-object v3, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_scanpos:Lorg/apache/lucene/queryparser/classic/Token;

    iput-object v3, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_lastpos:Lorg/apache/lucene/queryparser/classic/Token;

    .line 475
    :try_start_0
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_3_1()Z
    :try_end_0
    .catch Lorg/apache/lucene/queryparser/classic/QueryParser$LookaheadSuccess; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-eqz v3, :cond_0

    move v1, v2

    .line 477
    :cond_0
    invoke-direct {p0, v2, p1}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_save(II)V

    .line 476
    :goto_0
    return v1

    :catch_0
    move-exception v0

    .line 477
    .local v0, "ls":Lorg/apache/lucene/queryparser/classic/QueryParser$LookaheadSuccess;
    invoke-direct {p0, v2, p1}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_save(II)V

    goto :goto_0

    .end local v0    # "ls":Lorg/apache/lucene/queryparser/classic/QueryParser$LookaheadSuccess;
    :catchall_0
    move-exception v1

    invoke-direct {p0, v2, p1}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_save(II)V

    throw v1
.end method

.method private jj_3R_2()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 481
    const/16 v1, 0x14

    invoke-direct {p0, v1}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_scan_token(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 483
    :cond_0
    :goto_0
    return v0

    .line 482
    :cond_1
    const/16 v1, 0x10

    invoke-direct {p0, v1}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_scan_token(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 483
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private jj_3R_3()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 497
    const/16 v1, 0x11

    invoke-direct {p0, v1}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_scan_token(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 499
    :cond_0
    :goto_0
    return v0

    .line 498
    :cond_1
    const/16 v1, 0x10

    invoke-direct {p0, v1}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_scan_token(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 499
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private jj_3_1()Z
    .locals 2

    .prologue
    .line 488
    iget-object v0, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_scanpos:Lorg/apache/lucene/queryparser/classic/Token;

    .line 489
    .local v0, "xsp":Lorg/apache/lucene/queryparser/classic/Token;
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_3R_2()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 490
    iput-object v0, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_scanpos:Lorg/apache/lucene/queryparser/classic/Token;

    .line 491
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_3R_3()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    .line 493
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private jj_add_error_token(II)V
    .locals 6
    .param p1, "kind"    # I
    .param p2, "pos"    # I

    .prologue
    .line 650
    const/16 v3, 0x64

    if-lt p2, v3, :cond_1

    .line 672
    :cond_0
    :goto_0
    return-void

    .line 651
    :cond_1
    iget v3, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_endpos:I

    add-int/lit8 v3, v3, 0x1

    if-ne p2, v3, :cond_2

    .line 652
    iget-object v3, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_lasttokens:[I

    iget v4, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_endpos:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_endpos:I

    aput p1, v3, v4

    goto :goto_0

    .line 653
    :cond_2
    iget v3, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_endpos:I

    if-eqz v3, :cond_0

    .line 654
    iget v3, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_endpos:I

    new-array v3, v3, [I

    iput-object v3, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_expentry:[I

    .line 655
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget v3, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_endpos:I

    if-lt v0, v3, :cond_4

    .line 658
    iget-object v3, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_expentries:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<*>;"
    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_5

    .line 670
    :goto_2
    if-eqz p2, :cond_0

    iget-object v3, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_lasttokens:[I

    iput p2, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_endpos:I

    add-int/lit8 v4, p2, -0x1

    aput p1, v3, v4

    goto :goto_0

    .line 656
    .end local v1    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<*>;"
    :cond_4
    iget-object v3, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_expentry:[I

    iget-object v4, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_lasttokens:[I

    aget v4, v4, v0

    aput v4, v3, v0

    .line 655
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 659
    .restart local v1    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<*>;"
    :cond_5
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [I

    .line 660
    .local v2, "oldentry":[I
    array-length v3, v2

    iget-object v4, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_expentry:[I

    array-length v4, v4

    if-ne v3, v4, :cond_3

    .line 661
    const/4 v0, 0x0

    :goto_3
    iget-object v3, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_expentry:[I

    array-length v3, v3

    if-lt v0, v3, :cond_6

    .line 666
    iget-object v3, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_expentries:Ljava/util/List;

    iget-object v4, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_expentry:[I

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 662
    :cond_6
    aget v3, v2, v0

    iget-object v4, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_expentry:[I

    aget v4, v4, v0

    if-ne v3, v4, :cond_3

    .line 661
    add-int/lit8 v0, v0, 0x1

    goto :goto_3
.end method

.method private jj_consume_token(I)Lorg/apache/lucene/queryparser/classic/Token;
    .locals 5
    .param p1, "kind"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/classic/ParseException;
        }
    .end annotation

    .prologue
    .line 571
    iget-object v2, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->token:Lorg/apache/lucene/queryparser/classic/Token;

    .local v2, "oldToken":Lorg/apache/lucene/queryparser/classic/Token;
    iget-object v3, v2, Lorg/apache/lucene/queryparser/classic/Token;->next:Lorg/apache/lucene/queryparser/classic/Token;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->token:Lorg/apache/lucene/queryparser/classic/Token;

    iget-object v3, v3, Lorg/apache/lucene/queryparser/classic/Token;->next:Lorg/apache/lucene/queryparser/classic/Token;

    iput-object v3, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->token:Lorg/apache/lucene/queryparser/classic/Token;

    .line 573
    :goto_0
    const/4 v3, -0x1

    iput v3, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk:I

    .line 574
    iget-object v3, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->token:Lorg/apache/lucene/queryparser/classic/Token;

    iget v3, v3, Lorg/apache/lucene/queryparser/classic/Token;->kind:I

    if-ne v3, p1, :cond_5

    .line 575
    iget v3, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_gen:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_gen:I

    .line 576
    iget v3, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_gc:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_gc:I

    const/16 v4, 0x64

    if-le v3, v4, :cond_0

    .line 577
    const/4 v3, 0x0

    iput v3, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_gc:I

    .line 578
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v3, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_2_rtns:[Lorg/apache/lucene/queryparser/classic/QueryParser$JJCalls;

    array-length v3, v3

    if-lt v1, v3, :cond_2

    .line 586
    .end local v1    # "i":I
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->token:Lorg/apache/lucene/queryparser/classic/Token;

    return-object v3

    .line 572
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->token:Lorg/apache/lucene/queryparser/classic/Token;

    iget-object v4, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->token_source:Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;

    invoke-virtual {v4}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->getNextToken()Lorg/apache/lucene/queryparser/classic/Token;

    move-result-object v4

    iput-object v4, v3, Lorg/apache/lucene/queryparser/classic/Token;->next:Lorg/apache/lucene/queryparser/classic/Token;

    iput-object v4, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->token:Lorg/apache/lucene/queryparser/classic/Token;

    goto :goto_0

    .line 579
    .restart local v1    # "i":I
    :cond_2
    iget-object v3, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_2_rtns:[Lorg/apache/lucene/queryparser/classic/QueryParser$JJCalls;

    aget-object v0, v3, v1

    .line 580
    .local v0, "c":Lorg/apache/lucene/queryparser/classic/QueryParser$JJCalls;
    :goto_2
    if-nez v0, :cond_3

    .line 578
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 581
    :cond_3
    iget v3, v0, Lorg/apache/lucene/queryparser/classic/QueryParser$JJCalls;->gen:I

    iget v4, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_gen:I

    if-ge v3, v4, :cond_4

    const/4 v3, 0x0

    iput-object v3, v0, Lorg/apache/lucene/queryparser/classic/QueryParser$JJCalls;->first:Lorg/apache/lucene/queryparser/classic/Token;

    .line 582
    :cond_4
    iget-object v0, v0, Lorg/apache/lucene/queryparser/classic/QueryParser$JJCalls;->next:Lorg/apache/lucene/queryparser/classic/QueryParser$JJCalls;

    goto :goto_2

    .line 588
    .end local v0    # "c":Lorg/apache/lucene/queryparser/classic/QueryParser$JJCalls;
    .end local v1    # "i":I
    :cond_5
    iput-object v2, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->token:Lorg/apache/lucene/queryparser/classic/Token;

    .line 589
    iput p1, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_kind:I

    .line 590
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/classic/QueryParser;->generateParseException()Lorg/apache/lucene/queryparser/classic/ParseException;

    move-result-object v3

    throw v3
.end method

.method private static jj_la1_init_0()V
    .locals 1

    .prologue
    .line 520
    const/16 v0, 0x15

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_la1_0:[I

    .line 521
    return-void

    .line 520
    :array_0
    .array-data 4
        0x300
        0x300
        0x1c00
        0x1c00
        0xfda7f00
        0x120000
        0x40000
        0xfda6000
        0x9d22000
        0x200000
        0x200000
        0x40000
        0x6000000
        -0x80000000
        0x10000000
        -0x80000000
        0x60000000
        0x40000
        0x200000
        0x40000
        0xfda2000
    .end array-data
.end method

.method private static jj_la1_init_1()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 523
    const/16 v0, 0x15

    new-array v0, v0, [I

    const/16 v1, 0xd

    aput v2, v0, v1

    const/16 v1, 0xf

    aput v2, v0, v1

    sput-object v0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_la1_1:[I

    .line 524
    return-void
.end method

.method private jj_ntk()I
    .locals 2

    .prologue
    .line 637
    iget-object v0, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->token:Lorg/apache/lucene/queryparser/classic/Token;

    iget-object v0, v0, Lorg/apache/lucene/queryparser/classic/Token;->next:Lorg/apache/lucene/queryparser/classic/Token;

    iput-object v0, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_nt:Lorg/apache/lucene/queryparser/classic/Token;

    if-nez v0, :cond_0

    .line 638
    iget-object v0, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->token:Lorg/apache/lucene/queryparser/classic/Token;

    iget-object v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->token_source:Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;

    invoke-virtual {v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->getNextToken()Lorg/apache/lucene/queryparser/classic/Token;

    move-result-object v1

    iput-object v1, v0, Lorg/apache/lucene/queryparser/classic/Token;->next:Lorg/apache/lucene/queryparser/classic/Token;

    iget v0, v1, Lorg/apache/lucene/queryparser/classic/Token;->kind:I

    iput v0, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk:I

    .line 640
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_nt:Lorg/apache/lucene/queryparser/classic/Token;

    iget v0, v0, Lorg/apache/lucene/queryparser/classic/Token;->kind:I

    iput v0, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk:I

    goto :goto_0
.end method

.method private jj_rescan_token()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 720
    iput-boolean v4, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_rescan:Z

    .line 721
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v4, :cond_0

    .line 735
    const/4 v2, 0x0

    iput-boolean v2, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_rescan:Z

    .line 736
    return-void

    .line 723
    :cond_0
    :try_start_0
    iget-object v2, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_2_rtns:[Lorg/apache/lucene/queryparser/classic/QueryParser$JJCalls;

    aget-object v1, v2, v0

    .line 725
    .local v1, "p":Lorg/apache/lucene/queryparser/classic/QueryParser$JJCalls;
    :cond_1
    iget v2, v1, Lorg/apache/lucene/queryparser/classic/QueryParser$JJCalls;->gen:I

    iget v3, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_gen:I

    if-le v2, v3, :cond_2

    .line 726
    iget v2, v1, Lorg/apache/lucene/queryparser/classic/QueryParser$JJCalls;->arg:I

    iput v2, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_la:I

    iget-object v2, v1, Lorg/apache/lucene/queryparser/classic/QueryParser$JJCalls;->first:Lorg/apache/lucene/queryparser/classic/Token;

    iput-object v2, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_scanpos:Lorg/apache/lucene/queryparser/classic/Token;

    iput-object v2, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_lastpos:Lorg/apache/lucene/queryparser/classic/Token;

    .line 727
    packed-switch v0, :pswitch_data_0

    .line 731
    :cond_2
    :goto_1
    iget-object v1, v1, Lorg/apache/lucene/queryparser/classic/QueryParser$JJCalls;->next:Lorg/apache/lucene/queryparser/classic/QueryParser$JJCalls;

    .line 732
    if-nez v1, :cond_1

    .line 721
    .end local v1    # "p":Lorg/apache/lucene/queryparser/classic/QueryParser$JJCalls;
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 728
    .restart local v1    # "p":Lorg/apache/lucene/queryparser/classic/QueryParser$JJCalls;
    :pswitch_0
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_3_1()Z
    :try_end_0
    .catch Lorg/apache/lucene/queryparser/classic/QueryParser$LookaheadSuccess; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 733
    .end local v1    # "p":Lorg/apache/lucene/queryparser/classic/QueryParser$JJCalls;
    :catch_0
    move-exception v2

    goto :goto_2

    .line 727
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method private jj_save(II)V
    .locals 4
    .param p1, "index"    # I
    .param p2, "xla"    # I

    .prologue
    .line 739
    iget-object v2, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_2_rtns:[Lorg/apache/lucene/queryparser/classic/QueryParser$JJCalls;

    aget-object v0, v2, p1

    .line 740
    .local v0, "p":Lorg/apache/lucene/queryparser/classic/QueryParser$JJCalls;
    :goto_0
    iget v2, v0, Lorg/apache/lucene/queryparser/classic/QueryParser$JJCalls;->gen:I

    iget v3, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_gen:I

    if-gt v2, v3, :cond_0

    .line 744
    :goto_1
    iget v2, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_gen:I

    add-int/2addr v2, p2

    iget v3, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_la:I

    sub-int/2addr v2, v3

    iput v2, v0, Lorg/apache/lucene/queryparser/classic/QueryParser$JJCalls;->gen:I

    iget-object v2, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->token:Lorg/apache/lucene/queryparser/classic/Token;

    iput-object v2, v0, Lorg/apache/lucene/queryparser/classic/QueryParser$JJCalls;->first:Lorg/apache/lucene/queryparser/classic/Token;

    iput p2, v0, Lorg/apache/lucene/queryparser/classic/QueryParser$JJCalls;->arg:I

    .line 745
    return-void

    .line 741
    :cond_0
    iget-object v2, v0, Lorg/apache/lucene/queryparser/classic/QueryParser$JJCalls;->next:Lorg/apache/lucene/queryparser/classic/QueryParser$JJCalls;

    if-nez v2, :cond_1

    new-instance v1, Lorg/apache/lucene/queryparser/classic/QueryParser$JJCalls;

    invoke-direct {v1}, Lorg/apache/lucene/queryparser/classic/QueryParser$JJCalls;-><init>()V

    iput-object v1, v0, Lorg/apache/lucene/queryparser/classic/QueryParser$JJCalls;->next:Lorg/apache/lucene/queryparser/classic/QueryParser$JJCalls;

    .end local v0    # "p":Lorg/apache/lucene/queryparser/classic/QueryParser$JJCalls;
    .local v1, "p":Lorg/apache/lucene/queryparser/classic/QueryParser$JJCalls;
    move-object v0, v1

    .end local v1    # "p":Lorg/apache/lucene/queryparser/classic/QueryParser$JJCalls;
    .restart local v0    # "p":Lorg/apache/lucene/queryparser/classic/QueryParser$JJCalls;
    goto :goto_1

    .line 742
    :cond_1
    iget-object v0, v0, Lorg/apache/lucene/queryparser/classic/QueryParser$JJCalls;->next:Lorg/apache/lucene/queryparser/classic/QueryParser$JJCalls;

    goto :goto_0
.end method

.method private jj_scan_token(I)Z
    .locals 4
    .param p1, "kind"    # I

    .prologue
    .line 596
    iget-object v2, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_scanpos:Lorg/apache/lucene/queryparser/classic/Token;

    iget-object v3, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_lastpos:Lorg/apache/lucene/queryparser/classic/Token;

    if-ne v2, v3, :cond_3

    .line 597
    iget v2, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_la:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_la:I

    .line 598
    iget-object v2, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_scanpos:Lorg/apache/lucene/queryparser/classic/Token;

    iget-object v2, v2, Lorg/apache/lucene/queryparser/classic/Token;->next:Lorg/apache/lucene/queryparser/classic/Token;

    if-nez v2, :cond_2

    .line 599
    iget-object v2, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_scanpos:Lorg/apache/lucene/queryparser/classic/Token;

    iget-object v3, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->token_source:Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;

    invoke-virtual {v3}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->getNextToken()Lorg/apache/lucene/queryparser/classic/Token;

    move-result-object v3

    iput-object v3, v2, Lorg/apache/lucene/queryparser/classic/Token;->next:Lorg/apache/lucene/queryparser/classic/Token;

    iput-object v3, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_scanpos:Lorg/apache/lucene/queryparser/classic/Token;

    iput-object v3, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_lastpos:Lorg/apache/lucene/queryparser/classic/Token;

    .line 606
    :goto_0
    iget-boolean v2, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_rescan:Z

    if-eqz v2, :cond_1

    .line 607
    const/4 v0, 0x0

    .local v0, "i":I
    iget-object v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->token:Lorg/apache/lucene/queryparser/classic/Token;

    .line 608
    .local v1, "tok":Lorg/apache/lucene/queryparser/classic/Token;
    :goto_1
    if-eqz v1, :cond_0

    iget-object v2, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_scanpos:Lorg/apache/lucene/queryparser/classic/Token;

    if-ne v1, v2, :cond_4

    .line 609
    :cond_0
    if-eqz v1, :cond_1

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_add_error_token(II)V

    .line 611
    .end local v0    # "i":I
    .end local v1    # "tok":Lorg/apache/lucene/queryparser/classic/Token;
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_scanpos:Lorg/apache/lucene/queryparser/classic/Token;

    iget v2, v2, Lorg/apache/lucene/queryparser/classic/Token;->kind:I

    if-eq v2, p1, :cond_5

    const/4 v2, 0x1

    .line 613
    :goto_2
    return v2

    .line 601
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_scanpos:Lorg/apache/lucene/queryparser/classic/Token;

    iget-object v2, v2, Lorg/apache/lucene/queryparser/classic/Token;->next:Lorg/apache/lucene/queryparser/classic/Token;

    iput-object v2, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_scanpos:Lorg/apache/lucene/queryparser/classic/Token;

    iput-object v2, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_lastpos:Lorg/apache/lucene/queryparser/classic/Token;

    goto :goto_0

    .line 604
    :cond_3
    iget-object v2, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_scanpos:Lorg/apache/lucene/queryparser/classic/Token;

    iget-object v2, v2, Lorg/apache/lucene/queryparser/classic/Token;->next:Lorg/apache/lucene/queryparser/classic/Token;

    iput-object v2, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_scanpos:Lorg/apache/lucene/queryparser/classic/Token;

    goto :goto_0

    .line 608
    .restart local v0    # "i":I
    .restart local v1    # "tok":Lorg/apache/lucene/queryparser/classic/Token;
    :cond_4
    add-int/lit8 v0, v0, 0x1

    iget-object v1, v1, Lorg/apache/lucene/queryparser/classic/Token;->next:Lorg/apache/lucene/queryparser/classic/Token;

    goto :goto_1

    .line 612
    .end local v0    # "i":I
    .end local v1    # "tok":Lorg/apache/lucene/queryparser/classic/Token;
    :cond_5
    iget v2, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_la:I

    if-nez v2, :cond_6

    iget-object v2, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_scanpos:Lorg/apache/lucene/queryparser/classic/Token;

    iget-object v3, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_lastpos:Lorg/apache/lucene/queryparser/classic/Token;

    if-ne v2, v3, :cond_6

    iget-object v2, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ls:Lorg/apache/lucene/queryparser/classic/QueryParser$LookaheadSuccess;

    throw v2

    .line 613
    :cond_6
    const/4 v2, 0x0

    goto :goto_2
.end method


# virtual methods
.method public final Clause(Ljava/lang/String;)Lorg/apache/lucene/search/Query;
    .locals 7
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/classic/ParseException;
        }
    .end annotation

    .prologue
    const/16 v4, 0x10

    const/4 v6, -0x1

    .line 225
    const/4 v1, 0x0

    .local v1, "fieldToken":Lorg/apache/lucene/queryparser/classic/Token;
    const/4 v0, 0x0

    .line 226
    .local v0, "boost":Lorg/apache/lucene/queryparser/classic/Token;
    const/4 v3, 0x2

    invoke-direct {p0, v3}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_2_1(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 227
    iget v3, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk:I

    if-ne v3, v6, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk()I

    move-result v3

    :goto_0
    packed-switch v3, :pswitch_data_0

    .line 239
    :pswitch_0
    iget-object v3, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_la1:[I

    const/4 v4, 0x5

    iget v5, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_gen:I

    aput v5, v3, v4

    .line 240
    invoke-direct {p0, v6}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/classic/Token;

    .line 241
    new-instance v3, Lorg/apache/lucene/queryparser/classic/ParseException;

    invoke-direct {v3}, Lorg/apache/lucene/queryparser/classic/ParseException;-><init>()V

    throw v3

    .line 227
    :cond_0
    iget v3, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk:I

    goto :goto_0

    .line 229
    :pswitch_1
    const/16 v3, 0x14

    invoke-direct {p0, v3}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/classic/Token;

    move-result-object v1

    .line 230
    invoke-direct {p0, v4}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/classic/Token;

    .line 231
    iget-object v3, v1, Lorg/apache/lucene/queryparser/classic/Token;->image:Ljava/lang/String;

    invoke-virtual {p0, v3}, Lorg/apache/lucene/queryparser/classic/QueryParser;->discardEscapeChar(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 246
    :cond_1
    :goto_1
    iget v3, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk:I

    if-ne v3, v6, :cond_2

    invoke-direct {p0}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk()I

    move-result v3

    :goto_2
    packed-switch v3, :pswitch_data_1

    .line 274
    :pswitch_2
    iget-object v3, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_la1:[I

    const/4 v4, 0x7

    iget v5, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_gen:I

    aput v5, v3, v4

    .line 275
    invoke-direct {p0, v6}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/classic/Token;

    .line 276
    new-instance v3, Lorg/apache/lucene/queryparser/classic/ParseException;

    invoke-direct {v3}, Lorg/apache/lucene/queryparser/classic/ParseException;-><init>()V

    throw v3

    .line 234
    :pswitch_3
    const/16 v3, 0x11

    invoke-direct {p0, v3}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/classic/Token;

    .line 235
    invoke-direct {p0, v4}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/classic/Token;

    .line 236
    const-string p1, "*"

    .line 237
    goto :goto_1

    .line 246
    :cond_2
    iget v3, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk:I

    goto :goto_2

    .line 257
    :pswitch_4
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/classic/QueryParser;->Term(Ljava/lang/String;)Lorg/apache/lucene/search/Query;

    move-result-object v2

    .line 278
    .local v2, "q":Lorg/apache/lucene/search/Query;
    :goto_3
    invoke-virtual {p0, v2, v0}, Lorg/apache/lucene/queryparser/classic/QueryParser;->handleBoost(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/queryparser/classic/Token;)Lorg/apache/lucene/search/Query;

    move-result-object v3

    return-object v3

    .line 260
    .end local v2    # "q":Lorg/apache/lucene/search/Query;
    :pswitch_5
    const/16 v3, 0xe

    invoke-direct {p0, v3}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/classic/Token;

    .line 261
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/classic/QueryParser;->Query(Ljava/lang/String;)Lorg/apache/lucene/search/Query;

    move-result-object v2

    .line 262
    .restart local v2    # "q":Lorg/apache/lucene/search/Query;
    const/16 v3, 0xf

    invoke-direct {p0, v3}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/classic/Token;

    .line 263
    iget v3, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk:I

    if-ne v3, v6, :cond_3

    invoke-direct {p0}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk()I

    move-result v3

    :goto_4
    packed-switch v3, :pswitch_data_2

    .line 269
    iget-object v3, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_la1:[I

    const/4 v4, 0x6

    iget v5, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_gen:I

    aput v5, v3, v4

    goto :goto_3

    .line 263
    :cond_3
    iget v3, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk:I

    goto :goto_4

    .line 265
    :pswitch_6
    const/16 v3, 0x12

    invoke-direct {p0, v3}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/classic/Token;

    .line 266
    const/16 v3, 0x1b

    invoke-direct {p0, v3}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/classic/Token;

    move-result-object v0

    .line 267
    goto :goto_3

    .line 227
    nop

    :pswitch_data_0
    .packed-switch 0x11
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 246
    :pswitch_data_1
    .packed-switch 0xd
        :pswitch_4
        :pswitch_5
        :pswitch_2
        :pswitch_2
        :pswitch_4
        :pswitch_2
        :pswitch_4
        :pswitch_4
        :pswitch_2
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
    .end packed-switch

    .line 263
    :pswitch_data_2
    .packed-switch 0x12
        :pswitch_6
    .end packed-switch
.end method

.method public final Conjunction()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/classic/ParseException;
        }
    .end annotation

    .prologue
    const/4 v4, -0x1

    .line 107
    const/4 v0, 0x0

    .line 108
    .local v0, "ret":I
    iget v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk:I

    if-ne v1, v4, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk()I

    move-result v1

    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 127
    iget-object v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_la1:[I

    const/4 v2, 0x1

    iget v3, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_gen:I

    aput v3, v1, v2

    .line 130
    :goto_1
    return v0

    .line 108
    :cond_0
    iget v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk:I

    goto :goto_0

    .line 111
    :pswitch_0
    iget v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk:I

    if-ne v1, v4, :cond_1

    invoke-direct {p0}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk()I

    move-result v1

    :goto_2
    packed-switch v1, :pswitch_data_1

    .line 121
    iget-object v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_la1:[I

    const/4 v2, 0x0

    iget v3, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_gen:I

    aput v3, v1, v2

    .line 122
    invoke-direct {p0, v4}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/classic/Token;

    .line 123
    new-instance v1, Lorg/apache/lucene/queryparser/classic/ParseException;

    invoke-direct {v1}, Lorg/apache/lucene/queryparser/classic/ParseException;-><init>()V

    throw v1

    .line 111
    :cond_1
    iget v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk:I

    goto :goto_2

    .line 113
    :pswitch_1
    const/16 v1, 0x8

    invoke-direct {p0, v1}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/classic/Token;

    .line 114
    const/4 v0, 0x1

    .line 115
    goto :goto_1

    .line 117
    :pswitch_2
    const/16 v1, 0x9

    invoke-direct {p0, v1}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/classic/Token;

    .line 118
    const/4 v0, 0x2

    .line 119
    goto :goto_1

    .line 108
    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 111
    :pswitch_data_1
    .packed-switch 0x8
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final Modifiers()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/classic/ParseException;
        }
    .end annotation

    .prologue
    const/4 v4, -0x1

    .line 135
    const/4 v0, 0x0

    .line 136
    .local v0, "ret":I
    iget v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk:I

    if-ne v1, v4, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk()I

    move-result v1

    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 160
    iget-object v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_la1:[I

    const/4 v2, 0x3

    iget v3, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_gen:I

    aput v3, v1, v2

    .line 163
    :goto_1
    return v0

    .line 136
    :cond_0
    iget v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk:I

    goto :goto_0

    .line 140
    :pswitch_0
    iget v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk:I

    if-ne v1, v4, :cond_1

    invoke-direct {p0}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk()I

    move-result v1

    :goto_2
    packed-switch v1, :pswitch_data_1

    .line 154
    iget-object v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_la1:[I

    const/4 v2, 0x2

    iget v3, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_gen:I

    aput v3, v1, v2

    .line 155
    invoke-direct {p0, v4}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/classic/Token;

    .line 156
    new-instance v1, Lorg/apache/lucene/queryparser/classic/ParseException;

    invoke-direct {v1}, Lorg/apache/lucene/queryparser/classic/ParseException;-><init>()V

    throw v1

    .line 140
    :cond_1
    iget v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk:I

    goto :goto_2

    .line 142
    :pswitch_1
    const/16 v1, 0xb

    invoke-direct {p0, v1}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/classic/Token;

    .line 143
    const/16 v0, 0xb

    .line 144
    goto :goto_1

    .line 146
    :pswitch_2
    const/16 v1, 0xc

    invoke-direct {p0, v1}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/classic/Token;

    .line 147
    const/16 v0, 0xa

    .line 148
    goto :goto_1

    .line 150
    :pswitch_3
    const/16 v1, 0xa

    invoke-direct {p0, v1}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/classic/Token;

    .line 151
    const/16 v0, 0xa

    .line 152
    goto :goto_1

    .line 136
    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 140
    :pswitch_data_1
    .packed-switch 0xa
        :pswitch_3
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final Query(Ljava/lang/String;)Lorg/apache/lucene/search/Query;
    .locals 8
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/classic/ParseException;
        }
    .end annotation

    .prologue
    .line 177
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 178
    .local v0, "clauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/BooleanClause;>;"
    const/4 v2, 0x0

    .line 180
    .local v2, "firstQuery":Lorg/apache/lucene/search/Query;
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/classic/QueryParser;->Modifiers()I

    move-result v3

    .line 181
    .local v3, "mods":I
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/classic/QueryParser;->Clause(Ljava/lang/String;)Lorg/apache/lucene/search/Query;

    move-result-object v4

    .line 182
    .local v4, "q":Lorg/apache/lucene/search/Query;
    const/4 v5, 0x0

    invoke-virtual {p0, v0, v5, v3, v4}, Lorg/apache/lucene/queryparser/classic/QueryParser;->addClause(Ljava/util/List;IILorg/apache/lucene/search/Query;)V

    .line 183
    if-nez v3, :cond_0

    .line 184
    move-object v2, v4

    .line 187
    :cond_0
    :goto_0
    iget v5, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk:I

    const/4 v6, -0x1

    if-ne v5, v6, :cond_1

    invoke-direct {p0}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk()I

    move-result v5

    :goto_1
    packed-switch v5, :pswitch_data_0

    .line 207
    :pswitch_0
    iget-object v5, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_la1:[I

    const/4 v6, 0x4

    iget v7, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_gen:I

    aput v7, v5, v6

    .line 215
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_2

    if-eqz v2, :cond_2

    .line 218
    .end local v2    # "firstQuery":Lorg/apache/lucene/search/Query;
    :goto_2
    return-object v2

    .line 187
    .restart local v2    # "firstQuery":Lorg/apache/lucene/search/Query;
    :cond_1
    iget v5, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk:I

    goto :goto_1

    .line 210
    :pswitch_1
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/classic/QueryParser;->Conjunction()I

    move-result v1

    .line 211
    .local v1, "conj":I
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/classic/QueryParser;->Modifiers()I

    move-result v3

    .line 212
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/classic/QueryParser;->Clause(Ljava/lang/String;)Lorg/apache/lucene/search/Query;

    move-result-object v4

    .line 213
    invoke-virtual {p0, v0, v1, v3, v4}, Lorg/apache/lucene/queryparser/classic/QueryParser;->addClause(Ljava/util/List;IILorg/apache/lucene/search/Query;)V

    goto :goto_0

    .line 218
    .end local v1    # "conj":I
    :cond_2
    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/classic/QueryParser;->getBooleanQuery(Ljava/util/List;)Lorg/apache/lucene/search/Query;

    move-result-object v2

    goto :goto_2

    .line 187
    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public ReInit(Lorg/apache/lucene/queryparser/classic/CharStream;)V
    .locals 3
    .param p1, "stream"    # Lorg/apache/lucene/queryparser/classic/CharStream;

    .prologue
    const/4 v2, -0x1

    .line 541
    iget-object v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->token_source:Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->ReInit(Lorg/apache/lucene/queryparser/classic/CharStream;)V

    .line 542
    new-instance v1, Lorg/apache/lucene/queryparser/classic/Token;

    invoke-direct {v1}, Lorg/apache/lucene/queryparser/classic/Token;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->token:Lorg/apache/lucene/queryparser/classic/Token;

    .line 543
    iput v2, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk:I

    .line 544
    const/4 v1, 0x0

    iput v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_gen:I

    .line 545
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 546
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_2_rtns:[Lorg/apache/lucene/queryparser/classic/QueryParser$JJCalls;

    array-length v1, v1

    if-lt v0, v1, :cond_1

    .line 547
    return-void

    .line 545
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_la1:[I

    aput v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 546
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_2_rtns:[Lorg/apache/lucene/queryparser/classic/QueryParser$JJCalls;

    new-instance v2, Lorg/apache/lucene/queryparser/classic/QueryParser$JJCalls;

    invoke-direct {v2}, Lorg/apache/lucene/queryparser/classic/QueryParser$JJCalls;-><init>()V

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public ReInit(Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;)V
    .locals 3
    .param p1, "tm"    # Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;

    .prologue
    const/4 v2, -0x1

    .line 561
    iput-object p1, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->token_source:Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;

    .line 562
    new-instance v1, Lorg/apache/lucene/queryparser/classic/Token;

    invoke-direct {v1}, Lorg/apache/lucene/queryparser/classic/Token;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->token:Lorg/apache/lucene/queryparser/classic/Token;

    .line 563
    iput v2, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk:I

    .line 564
    const/4 v1, 0x0

    iput v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_gen:I

    .line 565
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 566
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_2_rtns:[Lorg/apache/lucene/queryparser/classic/QueryParser$JJCalls;

    array-length v1, v1

    if-lt v0, v1, :cond_1

    .line 567
    return-void

    .line 565
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_la1:[I

    aput v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 566
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_2_rtns:[Lorg/apache/lucene/queryparser/classic/QueryParser$JJCalls;

    new-instance v2, Lorg/apache/lucene/queryparser/classic/QueryParser$JJCalls;

    invoke-direct {v2}, Lorg/apache/lucene/queryparser/classic/QueryParser$JJCalls;-><init>()V

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public final Term(Ljava/lang/String;)Lorg/apache/lucene/search/Query;
    .locals 23
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/classic/ParseException;
        }
    .end annotation

    .prologue
    .line 283
    const/16 v17, 0x0

    .local v17, "boost":Lorg/apache/lucene/queryparser/classic/Token;
    const/4 v6, 0x0

    .line 284
    .local v6, "fuzzySlop":Lorg/apache/lucene/queryparser/classic/Token;
    const/4 v7, 0x0

    .line 285
    .local v7, "prefix":Z
    const/4 v8, 0x0

    .line 286
    .local v8, "wildcard":Z
    const/4 v9, 0x0

    .line 287
    .local v9, "fuzzy":Z
    const/4 v10, 0x0

    .line 288
    .local v10, "regexp":Z
    const/4 v15, 0x0

    .line 289
    .local v15, "startInc":Z
    const/16 v16, 0x0

    .line 291
    .local v16, "endInc":Z
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_0

    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk()I

    move-result v3

    :goto_0
    packed-switch v3, :pswitch_data_0

    .line 465
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_la1:[I

    const/16 v4, 0x14

    move-object/from16 v0, p0

    iget v11, v0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_gen:I

    aput v11, v3, v4

    .line 466
    const/4 v3, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/classic/Token;

    .line 467
    new-instance v3, Lorg/apache/lucene/queryparser/classic/ParseException;

    invoke-direct {v3}, Lorg/apache/lucene/queryparser/classic/ParseException;-><init>()V

    throw v3

    .line 291
    :cond_0
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk:I

    goto :goto_0

    .line 299
    :pswitch_1
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_1

    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk()I

    move-result v3

    :goto_1
    packed-switch v3, :pswitch_data_1

    .line 327
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_la1:[I

    const/16 v4, 0x8

    move-object/from16 v0, p0

    iget v11, v0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_gen:I

    aput v11, v3, v4

    .line 328
    const/4 v3, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/classic/Token;

    .line 329
    new-instance v3, Lorg/apache/lucene/queryparser/classic/ParseException;

    invoke-direct {v3}, Lorg/apache/lucene/queryparser/classic/ParseException;-><init>()V

    throw v3

    .line 299
    :cond_1
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk:I

    goto :goto_1

    .line 301
    :pswitch_3
    const/16 v3, 0x14

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/classic/Token;

    move-result-object v5

    .line 331
    .local v5, "term":Lorg/apache/lucene/queryparser/classic/Token;
    :goto_2
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_2

    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk()I

    move-result v3

    :goto_3
    packed-switch v3, :pswitch_data_2

    .line 337
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_la1:[I

    const/16 v4, 0x9

    move-object/from16 v0, p0

    iget v11, v0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_gen:I

    aput v11, v3, v4

    .line 340
    :goto_4
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_3

    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk()I

    move-result v3

    :goto_5
    packed-switch v3, :pswitch_data_3

    .line 355
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_la1:[I

    const/16 v4, 0xb

    move-object/from16 v0, p0

    iget v11, v0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_gen:I

    aput v11, v3, v4

    :goto_6
    move-object/from16 v3, p0

    move-object/from16 v4, p1

    .line 358
    invoke-virtual/range {v3 .. v10}, Lorg/apache/lucene/queryparser/classic/QueryParser;->handleBareTokenQuery(Ljava/lang/String;Lorg/apache/lucene/queryparser/classic/Token;Lorg/apache/lucene/queryparser/classic/Token;ZZZZ)Lorg/apache/lucene/search/Query;

    move-result-object v21

    .line 469
    .end local v5    # "term":Lorg/apache/lucene/queryparser/classic/Token;
    .local v21, "q":Lorg/apache/lucene/search/Query;
    :goto_7
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/queryparser/classic/QueryParser;->handleBoost(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/queryparser/classic/Token;)Lorg/apache/lucene/search/Query;

    move-result-object v3

    return-object v3

    .line 304
    .end local v21    # "q":Lorg/apache/lucene/search/Query;
    :pswitch_4
    const/16 v3, 0x11

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/classic/Token;

    move-result-object v5

    .line 305
    .restart local v5    # "term":Lorg/apache/lucene/queryparser/classic/Token;
    const/4 v8, 0x1

    .line 306
    goto :goto_2

    .line 308
    .end local v5    # "term":Lorg/apache/lucene/queryparser/classic/Token;
    :pswitch_5
    const/16 v3, 0x16

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/classic/Token;

    move-result-object v5

    .line 309
    .restart local v5    # "term":Lorg/apache/lucene/queryparser/classic/Token;
    const/4 v7, 0x1

    .line 310
    goto :goto_2

    .line 312
    .end local v5    # "term":Lorg/apache/lucene/queryparser/classic/Token;
    :pswitch_6
    const/16 v3, 0x17

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/classic/Token;

    move-result-object v5

    .line 313
    .restart local v5    # "term":Lorg/apache/lucene/queryparser/classic/Token;
    const/4 v8, 0x1

    .line 314
    goto :goto_2

    .line 316
    .end local v5    # "term":Lorg/apache/lucene/queryparser/classic/Token;
    :pswitch_7
    const/16 v3, 0x18

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/classic/Token;

    move-result-object v5

    .line 317
    .restart local v5    # "term":Lorg/apache/lucene/queryparser/classic/Token;
    const/4 v10, 0x1

    .line 318
    goto :goto_2

    .line 320
    .end local v5    # "term":Lorg/apache/lucene/queryparser/classic/Token;
    :pswitch_8
    const/16 v3, 0x1b

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/classic/Token;

    move-result-object v5

    .line 321
    .restart local v5    # "term":Lorg/apache/lucene/queryparser/classic/Token;
    goto :goto_2

    .line 323
    .end local v5    # "term":Lorg/apache/lucene/queryparser/classic/Token;
    :pswitch_9
    const/16 v3, 0xd

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/classic/Token;

    move-result-object v5

    .line 324
    .restart local v5    # "term":Lorg/apache/lucene/queryparser/classic/Token;
    iget-object v3, v5, Lorg/apache/lucene/queryparser/classic/Token;->image:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v11, 0x1

    invoke-virtual {v3, v4, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v5, Lorg/apache/lucene/queryparser/classic/Token;->image:Ljava/lang/String;

    goto/16 :goto_2

    .line 331
    :cond_2
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk:I

    goto/16 :goto_3

    .line 333
    :pswitch_a
    const/16 v3, 0x15

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/classic/Token;

    move-result-object v6

    .line 334
    const/4 v9, 0x1

    .line 335
    goto/16 :goto_4

    .line 340
    :cond_3
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk:I

    goto :goto_5

    .line 342
    :pswitch_b
    const/16 v3, 0x12

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/classic/Token;

    .line 343
    const/16 v3, 0x1b

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/classic/Token;

    move-result-object v17

    .line 344
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_4

    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk()I

    move-result v3

    :goto_8
    packed-switch v3, :pswitch_data_4

    .line 350
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_la1:[I

    const/16 v4, 0xa

    move-object/from16 v0, p0

    iget v11, v0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_gen:I

    aput v11, v3, v4

    goto/16 :goto_6

    .line 344
    :cond_4
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk:I

    goto :goto_8

    .line 346
    :pswitch_c
    const/16 v3, 0x15

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/classic/Token;

    move-result-object v6

    .line 347
    const/4 v9, 0x1

    .line 348
    goto/16 :goto_6

    .line 362
    .end local v5    # "term":Lorg/apache/lucene/queryparser/classic/Token;
    :pswitch_d
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_5

    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk()I

    move-result v3

    :goto_9
    packed-switch v3, :pswitch_data_5

    .line 371
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_la1:[I

    const/16 v4, 0xc

    move-object/from16 v0, p0

    iget v11, v0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_gen:I

    aput v11, v3, v4

    .line 372
    const/4 v3, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/classic/Token;

    .line 373
    new-instance v3, Lorg/apache/lucene/queryparser/classic/ParseException;

    invoke-direct {v3}, Lorg/apache/lucene/queryparser/classic/ParseException;-><init>()V

    throw v3

    .line 362
    :cond_5
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk:I

    goto :goto_9

    .line 364
    :pswitch_e
    const/16 v3, 0x19

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/classic/Token;

    .line 365
    const/4 v15, 0x1

    .line 375
    :goto_a
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_6

    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk()I

    move-result v3

    :goto_b
    packed-switch v3, :pswitch_data_6

    .line 383
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_la1:[I

    const/16 v4, 0xd

    move-object/from16 v0, p0

    iget v11, v0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_gen:I

    aput v11, v3, v4

    .line 384
    const/4 v3, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/classic/Token;

    .line 385
    new-instance v3, Lorg/apache/lucene/queryparser/classic/ParseException;

    invoke-direct {v3}, Lorg/apache/lucene/queryparser/classic/ParseException;-><init>()V

    throw v3

    .line 368
    :pswitch_f
    const/16 v3, 0x1a

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/classic/Token;

    goto :goto_a

    .line 375
    :cond_6
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk:I

    goto :goto_b

    .line 377
    :pswitch_10
    const/16 v3, 0x20

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/classic/Token;

    move-result-object v19

    .line 387
    .local v19, "goop1":Lorg/apache/lucene/queryparser/classic/Token;
    :goto_c
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_7

    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk()I

    move-result v3

    :goto_d
    packed-switch v3, :pswitch_data_7

    .line 392
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_la1:[I

    const/16 v4, 0xe

    move-object/from16 v0, p0

    iget v11, v0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_gen:I

    aput v11, v3, v4

    .line 395
    :goto_e
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_8

    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk()I

    move-result v3

    :goto_f
    packed-switch v3, :pswitch_data_8

    .line 403
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_la1:[I

    const/16 v4, 0xf

    move-object/from16 v0, p0

    iget v11, v0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_gen:I

    aput v11, v3, v4

    .line 404
    const/4 v3, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/classic/Token;

    .line 405
    new-instance v3, Lorg/apache/lucene/queryparser/classic/ParseException;

    invoke-direct {v3}, Lorg/apache/lucene/queryparser/classic/ParseException;-><init>()V

    throw v3

    .line 380
    .end local v19    # "goop1":Lorg/apache/lucene/queryparser/classic/Token;
    :pswitch_11
    const/16 v3, 0x1f

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/classic/Token;

    move-result-object v19

    .line 381
    .restart local v19    # "goop1":Lorg/apache/lucene/queryparser/classic/Token;
    goto :goto_c

    .line 387
    :cond_7
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk:I

    goto :goto_d

    .line 389
    :pswitch_12
    const/16 v3, 0x1c

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/classic/Token;

    goto :goto_e

    .line 395
    :cond_8
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk:I

    goto :goto_f

    .line 397
    :pswitch_13
    const/16 v3, 0x20

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/classic/Token;

    move-result-object v20

    .line 407
    .local v20, "goop2":Lorg/apache/lucene/queryparser/classic/Token;
    :goto_10
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_9

    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk()I

    move-result v3

    :goto_11
    packed-switch v3, :pswitch_data_9

    .line 416
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_la1:[I

    const/16 v4, 0x10

    move-object/from16 v0, p0

    iget v11, v0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_gen:I

    aput v11, v3, v4

    .line 417
    const/4 v3, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/classic/Token;

    .line 418
    new-instance v3, Lorg/apache/lucene/queryparser/classic/ParseException;

    invoke-direct {v3}, Lorg/apache/lucene/queryparser/classic/ParseException;-><init>()V

    throw v3

    .line 400
    .end local v20    # "goop2":Lorg/apache/lucene/queryparser/classic/Token;
    :pswitch_14
    const/16 v3, 0x1f

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/classic/Token;

    move-result-object v20

    .line 401
    .restart local v20    # "goop2":Lorg/apache/lucene/queryparser/classic/Token;
    goto :goto_10

    .line 407
    :cond_9
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk:I

    goto :goto_11

    .line 409
    :pswitch_15
    const/16 v3, 0x1d

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/classic/Token;

    .line 410
    const/16 v16, 0x1

    .line 420
    :goto_12
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_c

    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk()I

    move-result v3

    :goto_13
    packed-switch v3, :pswitch_data_a

    .line 426
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_la1:[I

    const/16 v4, 0x11

    move-object/from16 v0, p0

    iget v11, v0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_gen:I

    aput v11, v3, v4

    .line 429
    :goto_14
    const/16 v22, 0x0

    .line 430
    .local v22, "startOpen":Z
    const/16 v18, 0x0

    .line 431
    .local v18, "endOpen":Z
    move-object/from16 v0, v19

    iget v3, v0, Lorg/apache/lucene/queryparser/classic/Token;->kind:I

    const/16 v4, 0x1f

    if-ne v3, v4, :cond_d

    .line 432
    move-object/from16 v0, v19

    iget-object v3, v0, Lorg/apache/lucene/queryparser/classic/Token;->image:Ljava/lang/String;

    const/4 v4, 0x1

    move-object/from16 v0, v19

    iget-object v11, v0, Lorg/apache/lucene/queryparser/classic/Token;->image:Ljava/lang/String;

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/lit8 v11, v11, -0x1

    invoke-virtual {v3, v4, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v19

    iput-object v3, v0, Lorg/apache/lucene/queryparser/classic/Token;->image:Ljava/lang/String;

    .line 436
    :cond_a
    :goto_15
    move-object/from16 v0, v20

    iget v3, v0, Lorg/apache/lucene/queryparser/classic/Token;->kind:I

    const/16 v4, 0x1f

    if-ne v3, v4, :cond_e

    .line 437
    move-object/from16 v0, v20

    iget-object v3, v0, Lorg/apache/lucene/queryparser/classic/Token;->image:Ljava/lang/String;

    const/4 v4, 0x1

    move-object/from16 v0, v20

    iget-object v11, v0, Lorg/apache/lucene/queryparser/classic/Token;->image:Ljava/lang/String;

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/lit8 v11, v11, -0x1

    invoke-virtual {v3, v4, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v20

    iput-object v3, v0, Lorg/apache/lucene/queryparser/classic/Token;->image:Ljava/lang/String;

    .line 441
    :cond_b
    :goto_16
    if-eqz v22, :cond_f

    const/4 v13, 0x0

    :goto_17
    if-eqz v18, :cond_10

    const/4 v14, 0x0

    :goto_18
    move-object/from16 v11, p0

    move-object/from16 v12, p1

    invoke-virtual/range {v11 .. v16}, Lorg/apache/lucene/queryparser/classic/QueryParser;->getRangeQuery(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Lorg/apache/lucene/search/Query;

    move-result-object v21

    .line 442
    .restart local v21    # "q":Lorg/apache/lucene/search/Query;
    goto/16 :goto_7

    .line 413
    .end local v18    # "endOpen":Z
    .end local v21    # "q":Lorg/apache/lucene/search/Query;
    .end local v22    # "startOpen":Z
    :pswitch_16
    const/16 v3, 0x1e

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/classic/Token;

    goto :goto_12

    .line 420
    :cond_c
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk:I

    goto :goto_13

    .line 422
    :pswitch_17
    const/16 v3, 0x12

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/classic/Token;

    .line 423
    const/16 v3, 0x1b

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/classic/Token;

    move-result-object v17

    .line 424
    goto :goto_14

    .line 433
    .restart local v18    # "endOpen":Z
    .restart local v22    # "startOpen":Z
    :cond_d
    const-string v3, "*"

    move-object/from16 v0, v19

    iget-object v4, v0, Lorg/apache/lucene/queryparser/classic/Token;->image:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 434
    const/16 v22, 0x1

    goto :goto_15

    .line 438
    :cond_e
    const-string v3, "*"

    move-object/from16 v0, v20

    iget-object v4, v0, Lorg/apache/lucene/queryparser/classic/Token;->image:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 439
    const/16 v18, 0x1

    goto :goto_16

    .line 441
    :cond_f
    move-object/from16 v0, v19

    iget-object v3, v0, Lorg/apache/lucene/queryparser/classic/Token;->image:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lorg/apache/lucene/queryparser/classic/QueryParser;->discardEscapeChar(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    goto :goto_17

    :cond_10
    move-object/from16 v0, v20

    iget-object v3, v0, Lorg/apache/lucene/queryparser/classic/Token;->image:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lorg/apache/lucene/queryparser/classic/QueryParser;->discardEscapeChar(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    goto :goto_18

    .line 444
    .end local v18    # "endOpen":Z
    .end local v19    # "goop1":Lorg/apache/lucene/queryparser/classic/Token;
    .end local v20    # "goop2":Lorg/apache/lucene/queryparser/classic/Token;
    .end local v22    # "startOpen":Z
    :pswitch_18
    const/16 v3, 0x13

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/classic/Token;

    move-result-object v5

    .line 445
    .restart local v5    # "term":Lorg/apache/lucene/queryparser/classic/Token;
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_11

    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk()I

    move-result v3

    :goto_19
    packed-switch v3, :pswitch_data_b

    .line 450
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_la1:[I

    const/16 v4, 0x12

    move-object/from16 v0, p0

    iget v11, v0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_gen:I

    aput v11, v3, v4

    .line 453
    :goto_1a
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_12

    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk()I

    move-result v3

    :goto_1b
    packed-switch v3, :pswitch_data_c

    .line 459
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_la1:[I

    const/16 v4, 0x13

    move-object/from16 v0, p0

    iget v11, v0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_gen:I

    aput v11, v3, v4

    .line 462
    :goto_1c
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v5, v6}, Lorg/apache/lucene/queryparser/classic/QueryParser;->handleQuotedTerm(Ljava/lang/String;Lorg/apache/lucene/queryparser/classic/Token;Lorg/apache/lucene/queryparser/classic/Token;)Lorg/apache/lucene/search/Query;

    move-result-object v21

    .line 463
    .restart local v21    # "q":Lorg/apache/lucene/search/Query;
    goto/16 :goto_7

    .line 445
    .end local v21    # "q":Lorg/apache/lucene/search/Query;
    :cond_11
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk:I

    goto :goto_19

    .line 447
    :pswitch_19
    const/16 v3, 0x15

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/classic/Token;

    move-result-object v6

    .line 448
    goto :goto_1a

    .line 453
    :cond_12
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk:I

    goto :goto_1b

    .line 455
    :pswitch_1a
    const/16 v3, 0x12

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/classic/Token;

    .line 456
    const/16 v3, 0x1b

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/classic/Token;

    move-result-object v17

    .line 457
    goto :goto_1c

    .line 291
    nop

    :pswitch_data_0
    .packed-switch 0xd
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_18
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_d
        :pswitch_d
        :pswitch_1
    .end packed-switch

    .line 299
    :pswitch_data_1
    .packed-switch 0xd
        :pswitch_9
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_4
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_2
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_2
        :pswitch_2
        :pswitch_8
    .end packed-switch

    .line 331
    :pswitch_data_2
    .packed-switch 0x15
        :pswitch_a
    .end packed-switch

    .line 340
    :pswitch_data_3
    .packed-switch 0x12
        :pswitch_b
    .end packed-switch

    .line 344
    :pswitch_data_4
    .packed-switch 0x15
        :pswitch_c
    .end packed-switch

    .line 362
    :pswitch_data_5
    .packed-switch 0x19
        :pswitch_e
        :pswitch_f
    .end packed-switch

    .line 375
    :pswitch_data_6
    .packed-switch 0x1f
        :pswitch_11
        :pswitch_10
    .end packed-switch

    .line 387
    :pswitch_data_7
    .packed-switch 0x1c
        :pswitch_12
    .end packed-switch

    .line 395
    :pswitch_data_8
    .packed-switch 0x1f
        :pswitch_14
        :pswitch_13
    .end packed-switch

    .line 407
    :pswitch_data_9
    .packed-switch 0x1d
        :pswitch_15
        :pswitch_16
    .end packed-switch

    .line 420
    :pswitch_data_a
    .packed-switch 0x12
        :pswitch_17
    .end packed-switch

    .line 445
    :pswitch_data_b
    .packed-switch 0x15
        :pswitch_19
    .end packed-switch

    .line 453
    :pswitch_data_c
    .packed-switch 0x12
        :pswitch_1a
    .end packed-switch
.end method

.method public final TopLevelQuery(Ljava/lang/String;)Lorg/apache/lucene/search/Query;
    .locals 2
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/classic/ParseException;
        }
    .end annotation

    .prologue
    .line 170
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/classic/QueryParser;->Query(Ljava/lang/String;)Lorg/apache/lucene/search/Query;

    move-result-object v0

    .line 171
    .local v0, "q":Lorg/apache/lucene/search/Query;
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryparser/classic/Token;

    .line 172
    return-object v0
.end method

.method public final disable_tracing()V
    .locals 0

    .prologue
    .line 717
    return-void
.end method

.method public final enable_tracing()V
    .locals 0

    .prologue
    .line 713
    return-void
.end method

.method public generateParseException()Lorg/apache/lucene/queryparser/classic/ParseException;
    .locals 9

    .prologue
    const/16 v8, 0x21

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 676
    iget-object v4, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_expentries:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 677
    new-array v3, v8, [Z

    .line 678
    .local v3, "la1tokens":[Z
    iget v4, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_kind:I

    if-ltz v4, :cond_0

    .line 679
    iget v4, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_kind:I

    aput-boolean v6, v3, v4

    .line 680
    const/4 v4, -0x1

    iput v4, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_kind:I

    .line 682
    :cond_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/16 v4, 0x15

    if-lt v1, v4, :cond_1

    .line 694
    const/4 v1, 0x0

    :goto_1
    if-lt v1, v8, :cond_6

    .line 701
    iput v7, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_endpos:I

    .line 702
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_rescan_token()V

    .line 703
    invoke-direct {p0, v7, v7}, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_add_error_token(II)V

    .line 704
    iget-object v4, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_expentries:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    new-array v0, v4, [[I

    .line 705
    .local v0, "exptokseq":[[I
    const/4 v1, 0x0

    :goto_2
    iget-object v4, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_expentries:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lt v1, v4, :cond_8

    .line 708
    new-instance v4, Lorg/apache/lucene/queryparser/classic/ParseException;

    iget-object v5, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->token:Lorg/apache/lucene/queryparser/classic/Token;

    sget-object v6, Lorg/apache/lucene/queryparser/classic/QueryParser;->tokenImage:[Ljava/lang/String;

    invoke-direct {v4, v5, v0, v6}, Lorg/apache/lucene/queryparser/classic/ParseException;-><init>(Lorg/apache/lucene/queryparser/classic/Token;[[I[Ljava/lang/String;)V

    return-object v4

    .line 683
    .end local v0    # "exptokseq":[[I
    :cond_1
    iget-object v4, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_la1:[I

    aget v4, v4, v1

    iget v5, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_gen:I

    if-ne v4, v5, :cond_2

    .line 684
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_3
    const/16 v4, 0x20

    if-lt v2, v4, :cond_3

    .line 682
    .end local v2    # "j":I
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 685
    .restart local v2    # "j":I
    :cond_3
    sget-object v4, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_la1_0:[I

    aget v4, v4, v1

    shl-int v5, v6, v2

    and-int/2addr v4, v5

    if-eqz v4, :cond_4

    .line 686
    aput-boolean v6, v3, v2

    .line 688
    :cond_4
    sget-object v4, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_la1_1:[I

    aget v4, v4, v1

    shl-int v5, v6, v2

    and-int/2addr v4, v5

    if-eqz v4, :cond_5

    .line 689
    add-int/lit8 v4, v2, 0x20

    aput-boolean v6, v3, v4

    .line 684
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 695
    .end local v2    # "j":I
    :cond_6
    aget-boolean v4, v3, v1

    if-eqz v4, :cond_7

    .line 696
    new-array v4, v6, [I

    iput-object v4, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_expentry:[I

    .line 697
    iget-object v4, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_expentry:[I

    aput v1, v4, v7

    .line 698
    iget-object v4, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_expentries:Ljava/util/List;

    iget-object v5, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_expentry:[I

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 694
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 706
    .restart local v0    # "exptokseq":[[I
    :cond_8
    iget-object v4, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_expentries:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [I

    aput-object v4, v0, v1

    .line 705
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method

.method public final getNextToken()Lorg/apache/lucene/queryparser/classic/Token;
    .locals 2

    .prologue
    .line 619
    iget-object v0, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->token:Lorg/apache/lucene/queryparser/classic/Token;

    iget-object v0, v0, Lorg/apache/lucene/queryparser/classic/Token;->next:Lorg/apache/lucene/queryparser/classic/Token;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->token:Lorg/apache/lucene/queryparser/classic/Token;

    iget-object v0, v0, Lorg/apache/lucene/queryparser/classic/Token;->next:Lorg/apache/lucene/queryparser/classic/Token;

    iput-object v0, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->token:Lorg/apache/lucene/queryparser/classic/Token;

    .line 621
    :goto_0
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_ntk:I

    .line 622
    iget v0, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_gen:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->jj_gen:I

    .line 623
    iget-object v0, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->token:Lorg/apache/lucene/queryparser/classic/Token;

    return-object v0

    .line 620
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->token:Lorg/apache/lucene/queryparser/classic/Token;

    iget-object v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->token_source:Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;

    invoke-virtual {v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->getNextToken()Lorg/apache/lucene/queryparser/classic/Token;

    move-result-object v1

    iput-object v1, v0, Lorg/apache/lucene/queryparser/classic/Token;->next:Lorg/apache/lucene/queryparser/classic/Token;

    iput-object v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->token:Lorg/apache/lucene/queryparser/classic/Token;

    goto :goto_0
.end method

.method public final getToken(I)Lorg/apache/lucene/queryparser/classic/Token;
    .locals 4
    .param p1, "index"    # I

    .prologue
    .line 628
    iget-object v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->token:Lorg/apache/lucene/queryparser/classic/Token;

    .line 629
    .local v1, "t":Lorg/apache/lucene/queryparser/classic/Token;
    const/4 v0, 0x0

    .local v0, "i":I
    move-object v2, v1

    .end local v1    # "t":Lorg/apache/lucene/queryparser/classic/Token;
    .local v2, "t":Lorg/apache/lucene/queryparser/classic/Token;
    :goto_0
    if-lt v0, p1, :cond_0

    .line 633
    return-object v2

    .line 630
    :cond_0
    iget-object v3, v2, Lorg/apache/lucene/queryparser/classic/Token;->next:Lorg/apache/lucene/queryparser/classic/Token;

    if-eqz v3, :cond_1

    iget-object v1, v2, Lorg/apache/lucene/queryparser/classic/Token;->next:Lorg/apache/lucene/queryparser/classic/Token;

    .line 629
    .end local v2    # "t":Lorg/apache/lucene/queryparser/classic/Token;
    .restart local v1    # "t":Lorg/apache/lucene/queryparser/classic/Token;
    :goto_1
    add-int/lit8 v0, v0, 0x1

    move-object v2, v1

    .end local v1    # "t":Lorg/apache/lucene/queryparser/classic/Token;
    .restart local v2    # "t":Lorg/apache/lucene/queryparser/classic/Token;
    goto :goto_0

    .line 631
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/queryparser/classic/QueryParser;->token_source:Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;

    invoke-virtual {v3}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->getNextToken()Lorg/apache/lucene/queryparser/classic/Token;

    move-result-object v1

    iput-object v1, v2, Lorg/apache/lucene/queryparser/classic/Token;->next:Lorg/apache/lucene/queryparser/classic/Token;

    .end local v2    # "t":Lorg/apache/lucene/queryparser/classic/Token;
    .restart local v1    # "t":Lorg/apache/lucene/queryparser/classic/Token;
    goto :goto_1
.end method
