.class public Lorg/apache/lucene/queryparser/xml/builders/BooleanQueryBuilder;
.super Ljava/lang/Object;
.source "BooleanQueryBuilder.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/xml/QueryBuilder;


# instance fields
.field private final factory:Lorg/apache/lucene/queryparser/xml/QueryBuilder;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/queryparser/xml/QueryBuilder;)V
    .locals 0
    .param p1, "factory"    # Lorg/apache/lucene/queryparser/xml/QueryBuilder;

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lorg/apache/lucene/queryparser/xml/builders/BooleanQueryBuilder;->factory:Lorg/apache/lucene/queryparser/xml/QueryBuilder;

    .line 41
    return-void
.end method

.method static getOccursValue(Lorg/w3c/dom/Element;)Lorg/apache/lucene/search/BooleanClause$Occur;
    .locals 5
    .param p0, "clauseElem"    # Lorg/w3c/dom/Element;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/xml/ParserException;
        }
    .end annotation

    .prologue
    .line 70
    const-string v2, "occurs"

    invoke-interface {p0, v2}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 71
    .local v0, "occs":Ljava/lang/String;
    sget-object v1, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    .line 72
    .local v1, "occurs":Lorg/apache/lucene/search/BooleanClause$Occur;
    const-string v2, "must"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 73
    sget-object v1, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    .line 87
    :cond_0
    :goto_0
    return-object v1

    .line 75
    :cond_1
    const-string v2, "mustNot"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 76
    sget-object v1, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST_NOT:Lorg/apache/lucene/search/BooleanClause$Occur;

    .line 77
    goto :goto_0

    .line 78
    :cond_2
    const-string v2, "should"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, ""

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 79
    :cond_3
    sget-object v1, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    .line 80
    goto :goto_0

    .line 81
    :cond_4
    if-eqz v0, :cond_0

    .line 82
    new-instance v2, Lorg/apache/lucene/queryparser/xml/ParserException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Invalid value for \"occurs\" attribute of clause:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/apache/lucene/queryparser/xml/ParserException;-><init>(Ljava/lang/String;)V

    throw v2
.end method


# virtual methods
.method public getQuery(Lorg/w3c/dom/Element;)Lorg/apache/lucene/search/Query;
    .locals 10
    .param p1, "e"    # Lorg/w3c/dom/Element;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/xml/ParserException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 49
    new-instance v0, Lorg/apache/lucene/search/BooleanQuery;

    const-string v8, "disableCoord"

    invoke-static {p1, v8, v9}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttribute(Lorg/w3c/dom/Element;Ljava/lang/String;Z)Z

    move-result v8

    invoke-direct {v0, v8}, Lorg/apache/lucene/search/BooleanQuery;-><init>(Z)V

    .line 50
    .local v0, "bq":Lorg/apache/lucene/search/BooleanQuery;
    const-string v8, "minimumNumberShouldMatch"

    invoke-static {p1, v8, v9}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttribute(Lorg/w3c/dom/Element;Ljava/lang/String;I)I

    move-result v8

    invoke-virtual {v0, v8}, Lorg/apache/lucene/search/BooleanQuery;->setMinimumNumberShouldMatch(I)V

    .line 51
    const-string v8, "boost"

    const/high16 v9, 0x3f800000    # 1.0f

    invoke-static {p1, v8, v9}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttribute(Lorg/w3c/dom/Element;Ljava/lang/String;F)F

    move-result v8

    invoke-virtual {v0, v8}, Lorg/apache/lucene/search/BooleanQuery;->setBoost(F)V

    .line 53
    invoke-interface {p1}, Lorg/w3c/dom/Element;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v4

    .line 54
    .local v4, "nl":Lorg/w3c/dom/NodeList;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-interface {v4}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v8

    if-lt v3, v8, :cond_0

    .line 66
    return-object v0

    .line 55
    :cond_0
    invoke-interface {v4, v3}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v5

    .line 56
    .local v5, "node":Lorg/w3c/dom/Node;
    invoke-interface {v5}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Clause"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    move-object v1, v5

    .line 57
    check-cast v1, Lorg/w3c/dom/Element;

    .line 58
    .local v1, "clauseElem":Lorg/w3c/dom/Element;
    invoke-static {v1}, Lorg/apache/lucene/queryparser/xml/builders/BooleanQueryBuilder;->getOccursValue(Lorg/w3c/dom/Element;)Lorg/apache/lucene/search/BooleanClause$Occur;

    move-result-object v6

    .line 60
    .local v6, "occurs":Lorg/apache/lucene/search/BooleanClause$Occur;
    invoke-static {v1}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getFirstChildOrFail(Lorg/w3c/dom/Element;)Lorg/w3c/dom/Element;

    move-result-object v2

    .line 61
    .local v2, "clauseQuery":Lorg/w3c/dom/Element;
    iget-object v8, p0, Lorg/apache/lucene/queryparser/xml/builders/BooleanQueryBuilder;->factory:Lorg/apache/lucene/queryparser/xml/QueryBuilder;

    invoke-interface {v8, v2}, Lorg/apache/lucene/queryparser/xml/QueryBuilder;->getQuery(Lorg/w3c/dom/Element;)Lorg/apache/lucene/search/Query;

    move-result-object v7

    .line 62
    .local v7, "q":Lorg/apache/lucene/search/Query;
    new-instance v8, Lorg/apache/lucene/search/BooleanClause;

    invoke-direct {v8, v7, v6}, Lorg/apache/lucene/search/BooleanClause;-><init>(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    invoke-virtual {v0, v8}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/BooleanClause;)V

    .line 54
    .end local v1    # "clauseElem":Lorg/w3c/dom/Element;
    .end local v2    # "clauseQuery":Lorg/w3c/dom/Element;
    .end local v6    # "occurs":Lorg/apache/lucene/search/BooleanClause$Occur;
    .end local v7    # "q":Lorg/apache/lucene/search/Query;
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method
