.class public Lorg/apache/lucene/queryparser/flexible/core/nodes/SlopQueryNode;
.super Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;
.source "SlopQueryNode.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldableNode;


# instance fields
.field private value:I


# direct methods
.method public constructor <init>(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;I)V
    .locals 6
    .param p1, "query"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .param p2, "value"    # I

    .prologue
    const/4 v5, 0x0

    .line 45
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;-><init>()V

    .line 37
    iput v5, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/SlopQueryNode;->value:I

    .line 46
    if-nez p1, :cond_0

    .line 47
    new-instance v0, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeError;

    new-instance v1, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;

    .line 48
    sget-object v2, Lorg/apache/lucene/queryparser/flexible/core/messages/QueryParserMessages;->NODE_ACTION_NOT_SUPPORTED:Ljava/lang/String;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "query"

    aput-object v4, v3, v5

    const/4 v4, 0x1

    const-string v5, "null"

    aput-object v5, v3, v4

    invoke-direct {v1, v2, v3}, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 47
    invoke-direct {v0, v1}, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeError;-><init>(Lorg/apache/lucene/queryparser/flexible/messages/Message;)V

    throw v0

    .line 51
    :cond_0
    iput p2, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/SlopQueryNode;->value:I

    .line 52
    invoke-virtual {p0, v5}, Lorg/apache/lucene/queryparser/flexible/core/nodes/SlopQueryNode;->setLeaf(Z)V

    .line 53
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/SlopQueryNode;->allocate()V

    .line 54
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/SlopQueryNode;->add(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)V

    .line 55
    return-void
.end method

.method private getValueString()Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 66
    iget v1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/SlopQueryNode;->value:I

    int-to-float v1, v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    .line 67
    .local v0, "f":Ljava/lang/Float;
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {v0}, Ljava/lang/Float;->longValue()J

    move-result-wide v2

    long-to-float v2, v2

    cmpl-float v1, v1, v2

    if-nez v1, :cond_0

    .line 68
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Float;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 70
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method public cloneTree()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 90
    invoke-super {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;->cloneTree()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/SlopQueryNode;

    .line 92
    .local v0, "clone":Lorg/apache/lucene/queryparser/flexible/core/nodes/SlopQueryNode;
    iget v1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/SlopQueryNode;->value:I

    iput v1, v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/SlopQueryNode;->value:I

    .line 94
    return-object v0
.end method

.method public getChild()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 2

    .prologue
    .line 58
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/SlopQueryNode;->getChildren()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    return-object v0
.end method

.method public getField()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 99
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/SlopQueryNode;->getChild()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v0

    .line 101
    .local v0, "child":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    instance-of v1, v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldableNode;

    if-eqz v1, :cond_0

    .line 102
    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldableNode;

    .end local v0    # "child":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    invoke-interface {v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldableNode;->getField()Ljava/lang/CharSequence;

    move-result-object v1

    .line 105
    :goto_0
    return-object v1

    .restart local v0    # "child":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getValue()I
    .locals 1

    .prologue
    .line 62
    iget v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/SlopQueryNode;->value:I

    return v0
.end method

.method public setField(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1, "fieldName"    # Ljava/lang/CharSequence;

    .prologue
    .line 111
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/SlopQueryNode;->getChild()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v0

    .line 113
    .local v0, "child":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    instance-of v1, v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldableNode;

    if-eqz v1, :cond_0

    .line 114
    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldableNode;

    .end local v0    # "child":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    invoke-interface {v0, p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldableNode;->setField(Ljava/lang/CharSequence;)V

    .line 117
    :cond_0
    return-void
.end method

.method public toQueryString(Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;)Ljava/lang/CharSequence;
    .locals 2
    .param p1, "escapeSyntaxParser"    # Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;

    .prologue
    .line 82
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/SlopQueryNode;->getChild()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v0

    if-nez v0, :cond_0

    .line 83
    const-string v0, ""

    .line 84
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/SlopQueryNode;->getChild()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v1

    invoke-interface {v1, p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->toQueryString(Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "~"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 85
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/SlopQueryNode;->getValueString()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 84
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 76
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "<slop value=\'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/SlopQueryNode;->getValueString()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 77
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/SlopQueryNode;->getChild()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n</slop>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 76
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
