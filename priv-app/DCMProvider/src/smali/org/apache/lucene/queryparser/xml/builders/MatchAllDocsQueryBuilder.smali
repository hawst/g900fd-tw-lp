.class public Lorg/apache/lucene/queryparser/xml/builders/MatchAllDocsQueryBuilder;
.super Ljava/lang/Object;
.source "MatchAllDocsQueryBuilder.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/xml/QueryBuilder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getQuery(Lorg/w3c/dom/Element;)Lorg/apache/lucene/search/Query;
    .locals 1
    .param p1, "e"    # Lorg/w3c/dom/Element;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/xml/ParserException;
        }
    .end annotation

    .prologue
    .line 32
    new-instance v0, Lorg/apache/lucene/search/MatchAllDocsQuery;

    invoke-direct {v0}, Lorg/apache/lucene/search/MatchAllDocsQuery;-><init>()V

    return-object v0
.end method
