.class public final Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation;
.super Ljava/lang/Object;
.source "QueryNodeOperation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$org$apache$lucene$queryparser$flexible$core$util$QueryNodeOperation$ANDOperation:[I


# direct methods
.method static synthetic $SWITCH_TABLE$org$apache$lucene$queryparser$flexible$core$util$QueryNodeOperation$ANDOperation()[I
    .locals 3

    .prologue
    .line 31
    sget-object v0, Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation;->$SWITCH_TABLE$org$apache$lucene$queryparser$flexible$core$util$QueryNodeOperation$ANDOperation:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;->values()[Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;->BOTH:Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;

    invoke-virtual {v1}, Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_3

    :goto_1
    :try_start_1
    sget-object v1, Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;->NONE:Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;

    invoke-virtual {v1}, Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_2

    :goto_2
    :try_start_2
    sget-object v1, Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;->Q1:Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;

    invoke-virtual {v1}, Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_1

    :goto_3
    :try_start_3
    sget-object v1, Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;->Q2:Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;

    invoke-virtual {v1}, Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_0

    :goto_4
    sput-object v0, Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation;->$SWITCH_TABLE$org$apache$lucene$queryparser$flexible$core$util$QueryNodeOperation$ANDOperation:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_4

    :catch_1
    move-exception v1

    goto :goto_3

    :catch_2
    move-exception v1

    goto :goto_2

    :catch_3
    move-exception v1

    goto :goto_1
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    return-void
.end method

.method public static final logicalAnd(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 7
    .param p0, "q1"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .param p1, "q2"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    .prologue
    .line 50
    if-nez p0, :cond_0

    move-object v3, p1

    .line 91
    :goto_0
    return-object v3

    .line 52
    :cond_0
    if-nez p1, :cond_1

    move-object v3, p0

    .line 53
    goto :goto_0

    .line 55
    :cond_1
    const/4 v2, 0x0

    .line 56
    .local v2, "op":Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;
    instance-of v5, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/AndQueryNode;

    if-eqz v5, :cond_2

    instance-of v5, p1, Lorg/apache/lucene/queryparser/flexible/core/nodes/AndQueryNode;

    if-eqz v5, :cond_2

    .line 57
    sget-object v2, Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;->BOTH:Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;

    .line 66
    :goto_1
    const/4 v3, 0x0

    .line 67
    .local v3, "result":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    :try_start_0
    invoke-static {}, Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation;->$SWITCH_TABLE$org$apache$lucene$queryparser$flexible$core$util$QueryNodeOperation$ANDOperation()[I

    move-result-object v5

    invoke-virtual {v2}, Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;->ordinal()I

    move-result v6

    aget v5, v5, v6
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    packed-switch v5, :pswitch_data_0

    .line 91
    const/4 v3, 0x0

    goto :goto_0

    .line 58
    .end local v3    # "result":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    :cond_2
    instance-of v5, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/AndQueryNode;

    if-eqz v5, :cond_3

    .line 59
    sget-object v2, Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;->Q1:Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;

    goto :goto_1

    .line 60
    :cond_3
    instance-of v5, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/AndQueryNode;

    if-eqz v5, :cond_4

    .line 61
    sget-object v2, Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;->Q2:Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;

    goto :goto_1

    .line 63
    :cond_4
    sget-object v2, Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;->NONE:Lorg/apache/lucene/queryparser/flexible/core/util/QueryNodeOperation$ANDOperation;

    goto :goto_1

    .line 69
    .restart local v3    # "result":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    :pswitch_0
    :try_start_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 70
    .local v0, "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    invoke-interface {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->cloneTree()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 71
    invoke-interface {p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->cloneTree()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 72
    new-instance v4, Lorg/apache/lucene/queryparser/flexible/core/nodes/AndQueryNode;

    invoke-direct {v4, v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/AndQueryNode;-><init>(Ljava/util/List;)V

    .end local v3    # "result":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .local v4, "result":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    move-object v3, v4

    .line 73
    goto :goto_0

    .line 75
    .end local v0    # "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    .end local v4    # "result":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .restart local v3    # "result":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    :pswitch_1
    invoke-interface {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->cloneTree()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v3

    .line 76
    invoke-interface {p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->cloneTree()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v5

    invoke-interface {v3, v5}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->add(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)V
    :try_end_1
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 87
    :catch_0
    move-exception v1

    .line 88
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v5, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeError;

    invoke-direct {v5, v1}, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeError;-><init>(Ljava/lang/Throwable;)V

    throw v5

    .line 79
    .end local v1    # "e":Ljava/lang/CloneNotSupportedException;
    :pswitch_2
    :try_start_2
    invoke-interface {p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->cloneTree()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v3

    .line 80
    invoke-interface {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->cloneTree()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v5

    invoke-interface {v3, v5}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->add(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)V

    goto :goto_0

    .line 83
    :pswitch_3
    invoke-interface {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->cloneTree()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v3

    .line 84
    invoke-interface {p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->cloneTree()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v5

    invoke-interface {v5}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->getChildren()Ljava/util/List;

    move-result-object v5

    invoke-interface {v3, v5}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->add(Ljava/util/List;)V
    :try_end_2
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 67
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method
