.class public Lorg/apache/lucene/queryparser/surround/query/DistanceQuery;
.super Lorg/apache/lucene/queryparser/surround/query/ComposedQuery;
.source "DistanceQuery.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/surround/query/DistanceSubQuery;


# instance fields
.field private opDistance:I

.field private ordered:Z


# direct methods
.method public constructor <init>(Ljava/util/List;ZILjava/lang/String;Z)V
    .locals 0
    .param p2, "infix"    # Z
    .param p3, "opDistance"    # I
    .param p4, "opName"    # Ljava/lang/String;
    .param p5, "ordered"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/surround/query/SrndQuery;",
            ">;ZI",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 37
    .local p1, "queries":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/surround/query/SrndQuery;>;"
    invoke-direct {p0, p1, p2, p4}, Lorg/apache/lucene/queryparser/surround/query/ComposedQuery;-><init>(Ljava/util/List;ZLjava/lang/String;)V

    .line 38
    iput p3, p0, Lorg/apache/lucene/queryparser/surround/query/DistanceQuery;->opDistance:I

    .line 39
    iput-boolean p5, p0, Lorg/apache/lucene/queryparser/surround/query/DistanceQuery;->ordered:Z

    .line 40
    return-void
.end method


# virtual methods
.method public addSpanQueries(Lorg/apache/lucene/queryparser/surround/query/SpanNearClauseFactory;)V
    .locals 5
    .param p1, "sncf"    # Lorg/apache/lucene/queryparser/surround/query/SpanNearClauseFactory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 69
    invoke-virtual {p1}, Lorg/apache/lucene/queryparser/surround/query/SpanNearClauseFactory;->getIndexReader()Lorg/apache/lucene/index/IndexReader;

    move-result-object v1

    .line 70
    invoke-virtual {p1}, Lorg/apache/lucene/queryparser/surround/query/SpanNearClauseFactory;->getFieldName()Ljava/lang/String;

    move-result-object v2

    .line 71
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/query/DistanceQuery;->getWeight()F

    move-result v3

    .line 72
    invoke-virtual {p1}, Lorg/apache/lucene/queryparser/surround/query/SpanNearClauseFactory;->getBasicQueryFactory()Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;

    move-result-object v4

    .line 69
    invoke-virtual {p0, v1, v2, v3, v4}, Lorg/apache/lucene/queryparser/surround/query/DistanceQuery;->getSpanNearQuery(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;FLorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;)Lorg/apache/lucene/search/Query;

    move-result-object v0

    .line 73
    .local v0, "snq":Lorg/apache/lucene/search/Query;
    invoke-virtual {p1, v0}, Lorg/apache/lucene/queryparser/surround/query/SpanNearClauseFactory;->addSpanQuery(Lorg/apache/lucene/search/Query;)V

    .line 74
    return-void
.end method

.method public distanceSubQueryNotAllowed()Ljava/lang/String;
    .locals 6

    .prologue
    .line 51
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/query/DistanceQuery;->getSubQueriesIterator()Ljava/util/Iterator;

    move-result-object v3

    .line 52
    .local v3, "sqi":Ljava/util/Iterator;, "Ljava/util/Iterator<*>;"
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 64
    const/4 v2, 0x0

    :goto_0
    return-object v2

    .line 53
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 54
    .local v1, "leq":Ljava/lang/Object;
    instance-of v4, v1, Lorg/apache/lucene/queryparser/surround/query/DistanceSubQuery;

    if-eqz v4, :cond_2

    move-object v0, v1

    .line 55
    check-cast v0, Lorg/apache/lucene/queryparser/surround/query/DistanceSubQuery;

    .line 56
    .local v0, "dsq":Lorg/apache/lucene/queryparser/surround/query/DistanceSubQuery;
    invoke-interface {v0}, Lorg/apache/lucene/queryparser/surround/query/DistanceSubQuery;->distanceSubQueryNotAllowed()Ljava/lang/String;

    move-result-object v2

    .line 57
    .local v2, "m":Ljava/lang/String;
    if-eqz v2, :cond_0

    goto :goto_0

    .line 61
    .end local v0    # "dsq":Lorg/apache/lucene/queryparser/surround/query/DistanceSubQuery;
    .end local v2    # "m":Ljava/lang/String;
    :cond_2
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Operator "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/query/DistanceQuery;->getOperatorName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " does not allow subquery "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public getOpDistance()I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lorg/apache/lucene/queryparser/surround/query/DistanceQuery;->opDistance:I

    return v0
.end method

.method public getSpanNearQuery(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;FLorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;)Lorg/apache/lucene/search/Query;
    .locals 7
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "fieldName"    # Ljava/lang/String;
    .param p3, "boost"    # F
    .param p4, "qf"    # Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 81
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/query/DistanceQuery;->getNrSubQueries()I

    move-result v5

    new-array v3, v5, [Lorg/apache/lucene/search/spans/SpanQuery;

    .line 82
    .local v3, "spanClauses":[Lorg/apache/lucene/search/spans/SpanQuery;
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/query/DistanceQuery;->getSubQueriesIterator()Ljava/util/Iterator;

    move-result-object v4

    .line 83
    .local v4, "sqi":Ljava/util/Iterator;, "Ljava/util/Iterator<*>;"
    const/4 v0, 0x0

    .line 84
    .local v0, "qi":I
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_0

    .line 100
    new-instance v1, Lorg/apache/lucene/search/spans/SpanNearQuery;

    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/query/DistanceQuery;->getOpDistance()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/query/DistanceQuery;->subQueriesOrdered()Z

    move-result v6

    invoke-direct {v1, v3, v5, v6}, Lorg/apache/lucene/search/spans/SpanNearQuery;-><init>([Lorg/apache/lucene/search/spans/SpanQuery;IZ)V

    .line 101
    .local v1, "r":Lorg/apache/lucene/search/spans/SpanNearQuery;
    invoke-virtual {v1, p3}, Lorg/apache/lucene/search/spans/SpanNearQuery;->setBoost(F)V

    .line 102
    .end local v1    # "r":Lorg/apache/lucene/search/spans/SpanNearQuery;
    :goto_1
    return-object v1

    .line 85
    :cond_0
    new-instance v2, Lorg/apache/lucene/queryparser/surround/query/SpanNearClauseFactory;

    invoke-direct {v2, p1, p2, p4}, Lorg/apache/lucene/queryparser/surround/query/SpanNearClauseFactory;-><init>(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;)V

    .line 87
    .local v2, "sncf":Lorg/apache/lucene/queryparser/surround/query/SpanNearClauseFactory;
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/queryparser/surround/query/DistanceSubQuery;

    invoke-interface {v5, v2}, Lorg/apache/lucene/queryparser/surround/query/DistanceSubQuery;->addSpanQueries(Lorg/apache/lucene/queryparser/surround/query/SpanNearClauseFactory;)V

    .line 88
    invoke-virtual {v2}, Lorg/apache/lucene/queryparser/surround/query/SpanNearClauseFactory;->size()I

    move-result v5

    if-nez v5, :cond_2

    .line 89
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_1

    .line 93
    sget-object v1, Lorg/apache/lucene/queryparser/surround/query/SrndQuery;->theEmptyLcnQuery:Lorg/apache/lucene/search/Query;

    goto :goto_1

    .line 90
    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/queryparser/surround/query/DistanceSubQuery;

    invoke-interface {v5, v2}, Lorg/apache/lucene/queryparser/surround/query/DistanceSubQuery;->addSpanQueries(Lorg/apache/lucene/queryparser/surround/query/SpanNearClauseFactory;)V

    .line 91
    invoke-virtual {v2}, Lorg/apache/lucene/queryparser/surround/query/SpanNearClauseFactory;->clear()V

    goto :goto_2

    .line 96
    :cond_2
    invoke-virtual {v2}, Lorg/apache/lucene/queryparser/surround/query/SpanNearClauseFactory;->makeSpanClause()Lorg/apache/lucene/search/spans/SpanQuery;

    move-result-object v5

    aput-object v5, v3, v0

    .line 97
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public makeLuceneQueryFieldNoBoost(Ljava/lang/String;Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;)Lorg/apache/lucene/search/Query;
    .locals 1
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "qf"    # Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;

    .prologue
    .line 107
    new-instance v0, Lorg/apache/lucene/queryparser/surround/query/DistanceRewriteQuery;

    invoke-direct {v0, p0, p1, p2}, Lorg/apache/lucene/queryparser/surround/query/DistanceRewriteQuery;-><init>(Lorg/apache/lucene/queryparser/surround/query/DistanceQuery;Ljava/lang/String;Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;)V

    return-object v0
.end method

.method public subQueriesOrdered()Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lorg/apache/lucene/queryparser/surround/query/DistanceQuery;->ordered:Z

    return v0
.end method
