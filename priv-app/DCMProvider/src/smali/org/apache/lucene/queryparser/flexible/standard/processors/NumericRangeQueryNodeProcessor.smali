.class public Lorg/apache/lucene/queryparser/flexible/standard/processors/NumericRangeQueryNodeProcessor;
.super Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl;
.source "NumericRangeQueryNodeProcessor.java"


# static fields
.field private static synthetic $SWITCH_TABLE$org$apache$lucene$document$FieldType$NumericType:[I


# direct methods
.method static synthetic $SWITCH_TABLE$org$apache$lucene$document$FieldType$NumericType()[I
    .locals 3

    .prologue
    .line 54
    sget-object v0, Lorg/apache/lucene/queryparser/flexible/standard/processors/NumericRangeQueryNodeProcessor;->$SWITCH_TABLE$org$apache$lucene$document$FieldType$NumericType:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lorg/apache/lucene/document/FieldType$NumericType;->values()[Lorg/apache/lucene/document/FieldType$NumericType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lorg/apache/lucene/document/FieldType$NumericType;->DOUBLE:Lorg/apache/lucene/document/FieldType$NumericType;

    invoke-virtual {v1}, Lorg/apache/lucene/document/FieldType$NumericType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_3

    :goto_1
    :try_start_1
    sget-object v1, Lorg/apache/lucene/document/FieldType$NumericType;->FLOAT:Lorg/apache/lucene/document/FieldType$NumericType;

    invoke-virtual {v1}, Lorg/apache/lucene/document/FieldType$NumericType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_2

    :goto_2
    :try_start_2
    sget-object v1, Lorg/apache/lucene/document/FieldType$NumericType;->INT:Lorg/apache/lucene/document/FieldType$NumericType;

    invoke-virtual {v1}, Lorg/apache/lucene/document/FieldType$NumericType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_1

    :goto_3
    :try_start_3
    sget-object v1, Lorg/apache/lucene/document/FieldType$NumericType;->LONG:Lorg/apache/lucene/document/FieldType$NumericType;

    invoke-virtual {v1}, Lorg/apache/lucene/document/FieldType$NumericType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_0

    :goto_4
    sput-object v0, Lorg/apache/lucene/queryparser/flexible/standard/processors/NumericRangeQueryNodeProcessor;->$SWITCH_TABLE$org$apache$lucene$document$FieldType$NumericType:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_4

    :catch_1
    move-exception v1

    goto :goto_3

    :catch_2
    move-exception v1

    goto :goto_2

    :catch_3
    move-exception v1

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl;-><init>()V

    .line 61
    return-void
.end method


# virtual methods
.method protected postProcessNode(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 24
    .param p1, "node"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 66
    move-object/from16 v0, p1

    instance-of v2, v0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/TermRangeQueryNode;

    if-eqz v2, :cond_7

    .line 67
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/queryparser/flexible/standard/processors/NumericRangeQueryNodeProcessor;->getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    move-result-object v8

    .line 69
    .local v8, "config":Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;
    if-eqz v8, :cond_7

    move-object/from16 v15, p1

    .line 70
    check-cast v15, Lorg/apache/lucene/queryparser/flexible/standard/nodes/TermRangeQueryNode;

    .line 72
    .local v15, "termRangeNode":Lorg/apache/lucene/queryparser/flexible/standard/nodes/TermRangeQueryNode;
    invoke-virtual {v15}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/TermRangeQueryNode;->getField()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Lorg/apache/lucene/queryparser/flexible/core/util/StringUtils;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 71
    invoke-virtual {v8, v2}, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->getFieldConfig(Ljava/lang/String;)Lorg/apache/lucene/queryparser/flexible/core/config/FieldConfig;

    move-result-object v10

    .line 74
    .local v10, "fieldConfig":Lorg/apache/lucene/queryparser/flexible/core/config/FieldConfig;
    if-eqz v10, :cond_7

    .line 77
    sget-object v2, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->NUMERIC_CONFIG:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-virtual {v10, v2}, Lorg/apache/lucene/queryparser/flexible/core/config/FieldConfig;->get(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;

    .line 79
    .local v7, "numericConfig":Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;
    if-eqz v7, :cond_7

    .line 81
    invoke-virtual {v15}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/TermRangeQueryNode;->getLowerBound()Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldValuePairQueryNode;

    move-result-object v11

    check-cast v11, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;

    .line 82
    .local v11, "lower":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    invoke-virtual {v15}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/TermRangeQueryNode;->getUpperBound()Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldValuePairQueryNode;

    move-result-object v16

    check-cast v16, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;

    .line 84
    .local v16, "upper":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    invoke-virtual {v11}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->getTextAsString()Ljava/lang/String;

    move-result-object v13

    .line 85
    .local v13, "lowerText":Ljava/lang/String;
    invoke-virtual/range {v16 .. v16}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->getTextAsString()Ljava/lang/String;

    move-result-object v18

    .line 86
    .local v18, "upperText":Ljava/lang/String;
    invoke-virtual {v7}, Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;->getNumberFormat()Ljava/text/NumberFormat;

    move-result-object v14

    .line 87
    .local v14, "numberFormat":Ljava/text/NumberFormat;
    const/4 v12, 0x0

    .local v12, "lowerNumber":Ljava/lang/Number;
    const/16 v17, 0x0

    .line 89
    .local v17, "upperNumber":Ljava/lang/Number;
    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    .line 92
    :try_start_0
    invoke-virtual {v14, v13}, Ljava/text/NumberFormat;->parse(Ljava/lang/String;)Ljava/lang/Number;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v12

    .line 103
    :cond_0
    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_1

    .line 106
    :try_start_1
    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Ljava/text/NumberFormat;->parse(Ljava/lang/String;)Ljava/lang/Number;
    :try_end_1
    .catch Ljava/text/ParseException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v17

    .line 117
    :cond_1
    invoke-static {}, Lorg/apache/lucene/queryparser/flexible/standard/processors/NumericRangeQueryNodeProcessor;->$SWITCH_TABLE$org$apache$lucene$document$FieldType$NumericType()[I

    move-result-object v2

    invoke-virtual {v7}, Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;->getType()Lorg/apache/lucene/document/FieldType$NumericType;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/document/FieldType$NumericType;->ordinal()I

    move-result v19

    aget v2, v2, v19

    packed-switch v2, :pswitch_data_0

    .line 135
    :cond_2
    :goto_0
    new-instance v3, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;

    .line 136
    invoke-virtual {v15}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/TermRangeQueryNode;->getField()Ljava/lang/CharSequence;

    move-result-object v2

    .line 135
    invoke-direct {v3, v2, v12, v14}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;-><init>(Ljava/lang/CharSequence;Ljava/lang/Number;Ljava/text/NumberFormat;)V

    .line 137
    .local v3, "lowerNode":Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;
    new-instance v4, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;

    .line 138
    invoke-virtual {v15}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/TermRangeQueryNode;->getField()Ljava/lang/CharSequence;

    move-result-object v2

    .line 137
    move-object/from16 v0, v17

    invoke-direct {v4, v2, v0, v14}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;-><init>(Ljava/lang/CharSequence;Ljava/lang/Number;Ljava/text/NumberFormat;)V

    .line 140
    .local v4, "upperNode":Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;
    invoke-virtual {v15}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/TermRangeQueryNode;->isLowerInclusive()Z

    move-result v5

    .line 141
    .local v5, "lowerInclusive":Z
    invoke-virtual {v15}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/TermRangeQueryNode;->isUpperInclusive()Z

    move-result v6

    .line 143
    .local v6, "upperInclusive":Z
    new-instance v2, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericRangeQueryNode;

    invoke-direct/range {v2 .. v7}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericRangeQueryNode;-><init>(Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;ZZLorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;)V

    .line 154
    .end local v3    # "lowerNode":Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;
    .end local v4    # "upperNode":Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;
    .end local v5    # "lowerInclusive":Z
    .end local v6    # "upperInclusive":Z
    .end local v7    # "numericConfig":Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;
    .end local v8    # "config":Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;
    .end local v10    # "fieldConfig":Lorg/apache/lucene/queryparser/flexible/core/config/FieldConfig;
    .end local v11    # "lower":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    .end local v12    # "lowerNumber":Ljava/lang/Number;
    .end local v13    # "lowerText":Ljava/lang/String;
    .end local v14    # "numberFormat":Ljava/text/NumberFormat;
    .end local v15    # "termRangeNode":Lorg/apache/lucene/queryparser/flexible/standard/nodes/TermRangeQueryNode;
    .end local v16    # "upper":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    .end local v17    # "upperNumber":Ljava/lang/Number;
    .end local v18    # "upperText":Ljava/lang/String;
    :goto_1
    return-object v2

    .line 94
    .restart local v7    # "numericConfig":Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;
    .restart local v8    # "config":Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;
    .restart local v10    # "fieldConfig":Lorg/apache/lucene/queryparser/flexible/core/config/FieldConfig;
    .restart local v11    # "lower":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    .restart local v12    # "lowerNumber":Ljava/lang/Number;
    .restart local v13    # "lowerText":Ljava/lang/String;
    .restart local v14    # "numberFormat":Ljava/text/NumberFormat;
    .restart local v15    # "termRangeNode":Lorg/apache/lucene/queryparser/flexible/standard/nodes/TermRangeQueryNode;
    .restart local v16    # "upper":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    .restart local v17    # "upperNumber":Ljava/lang/Number;
    .restart local v18    # "upperText":Ljava/lang/String;
    :catch_0
    move-exception v9

    .line 95
    .local v9, "e":Ljava/text/ParseException;
    new-instance v2, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeParseException;

    new-instance v19, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;

    .line 96
    sget-object v20, Lorg/apache/lucene/queryparser/flexible/core/messages/QueryParserMessages;->COULD_NOT_PARSE_NUMBER:Ljava/lang/String;

    const/16 v21, 0x2

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    .line 97
    invoke-virtual {v11}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->getTextAsString()Ljava/lang/String;

    move-result-object v23

    aput-object v23, v21, v22

    const/16 v22, 0x1

    invoke-virtual {v14}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v23

    .line 98
    invoke-virtual/range {v23 .. v23}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v23

    aput-object v23, v21, v22

    invoke-direct/range {v19 .. v21}, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 95
    move-object/from16 v0, v19

    invoke-direct {v2, v0, v9}, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeParseException;-><init>(Lorg/apache/lucene/queryparser/flexible/messages/Message;Ljava/lang/Throwable;)V

    throw v2

    .line 108
    .end local v9    # "e":Ljava/text/ParseException;
    :catch_1
    move-exception v9

    .line 109
    .restart local v9    # "e":Ljava/text/ParseException;
    new-instance v2, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeParseException;

    new-instance v19, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;

    .line 110
    sget-object v20, Lorg/apache/lucene/queryparser/flexible/core/messages/QueryParserMessages;->COULD_NOT_PARSE_NUMBER:Ljava/lang/String;

    const/16 v21, 0x2

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    .line 111
    invoke-virtual/range {v16 .. v16}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->getTextAsString()Ljava/lang/String;

    move-result-object v23

    aput-object v23, v21, v22

    const/16 v22, 0x1

    invoke-virtual {v14}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v23

    .line 112
    invoke-virtual/range {v23 .. v23}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v23

    aput-object v23, v21, v22

    invoke-direct/range {v19 .. v21}, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 109
    move-object/from16 v0, v19

    invoke-direct {v2, v0, v9}, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeParseException;-><init>(Lorg/apache/lucene/queryparser/flexible/messages/Message;Ljava/lang/Throwable;)V

    throw v2

    .line 119
    .end local v9    # "e":Ljava/text/ParseException;
    :pswitch_0
    if-eqz v17, :cond_3

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Number;->longValue()J

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    .line 120
    :cond_3
    if-eqz v12, :cond_2

    invoke-virtual {v12}, Ljava/lang/Number;->longValue()J

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    .line 121
    goto/16 :goto_0

    .line 123
    :pswitch_1
    if-eqz v17, :cond_4

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Number;->intValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    .line 124
    :cond_4
    if-eqz v12, :cond_2

    invoke-virtual {v12}, Ljava/lang/Number;->intValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    .line 125
    goto/16 :goto_0

    .line 127
    :pswitch_2
    if-eqz v17, :cond_5

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v17

    .line 128
    :cond_5
    if-eqz v12, :cond_2

    invoke-virtual {v12}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v12

    .line 129
    goto/16 :goto_0

    .line 131
    :pswitch_3
    if-eqz v17, :cond_6

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Number;->floatValue()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v17

    .line 132
    :cond_6
    if-eqz v12, :cond_2

    invoke-virtual {v12}, Ljava/lang/Number;->floatValue()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v12

    goto/16 :goto_0

    .end local v7    # "numericConfig":Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;
    .end local v8    # "config":Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;
    .end local v10    # "fieldConfig":Lorg/apache/lucene/queryparser/flexible/core/config/FieldConfig;
    .end local v11    # "lower":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    .end local v12    # "lowerNumber":Ljava/lang/Number;
    .end local v13    # "lowerText":Ljava/lang/String;
    .end local v14    # "numberFormat":Ljava/text/NumberFormat;
    .end local v15    # "termRangeNode":Lorg/apache/lucene/queryparser/flexible/standard/nodes/TermRangeQueryNode;
    .end local v16    # "upper":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    .end local v17    # "upperNumber":Ljava/lang/Number;
    .end local v18    # "upperText":Ljava/lang/String;
    :cond_7
    move-object/from16 v2, p1

    .line 154
    goto/16 :goto_1

    .line 117
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method protected preProcessNode(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 0
    .param p1, "node"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 160
    return-object p1
.end method

.method protected setChildrenOrder(Ljava/util/List;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 166
    .local p1, "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    return-object p1
.end method
