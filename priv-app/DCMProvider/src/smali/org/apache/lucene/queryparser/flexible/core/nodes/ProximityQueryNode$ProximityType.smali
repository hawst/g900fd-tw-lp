.class public Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode$ProximityType;
.super Ljava/lang/Object;
.source "ProximityQueryNode.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ProximityType"
.end annotation


# instance fields
.field pDistance:I

.field pType:Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode$Type;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode$Type;)V
    .locals 1
    .param p1, "type"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode$Type;

    .prologue
    .line 63
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode$ProximityType;-><init>(Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode$Type;I)V

    .line 64
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode$Type;I)V
    .locals 1
    .param p1, "type"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode$Type;
    .param p2, "distance"    # I

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode$ProximityType;->pDistance:I

    .line 60
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode$ProximityType;->pType:Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode$Type;

    .line 67
    iput-object p1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode$ProximityType;->pType:Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode$Type;

    .line 68
    iput p2, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode$ProximityType;->pDistance:I

    .line 69
    return-void
.end method
