.class public Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;
.super Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;
.source "NumericQueryNode.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldValuePairQueryNode;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;",
        "Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldValuePairQueryNode",
        "<",
        "Ljava/lang/Number;",
        ">;"
    }
.end annotation


# instance fields
.field private field:Ljava/lang/CharSequence;

.field private numberFormat:Ljava/text/NumberFormat;

.field private value:Ljava/lang/Number;


# direct methods
.method public constructor <init>(Ljava/lang/CharSequence;Ljava/lang/Number;Ljava/text/NumberFormat;)V
    .locals 0
    .param p1, "field"    # Ljava/lang/CharSequence;
    .param p2, "value"    # Ljava/lang/Number;
    .param p3, "numberFormat"    # Ljava/text/NumberFormat;

    .prologue
    .line 58
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNodeImpl;-><init>()V

    .line 60
    invoke-virtual {p0, p3}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;->setNumberFormat(Ljava/text/NumberFormat;)V

    .line 61
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;->setField(Ljava/lang/CharSequence;)V

    .line 62
    invoke-virtual {p0, p2}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;->setValue(Ljava/lang/Number;)V

    .line 64
    return-void
.end method


# virtual methods
.method public getField()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;->field:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getNumberFormat()Ljava/text/NumberFormat;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;->numberFormat:Ljava/text/NumberFormat;

    return-object v0
.end method

.method protected getTermEscaped(Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;)Ljava/lang/CharSequence;
    .locals 3
    .param p1, "escaper"    # Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;

    .prologue
    .line 95
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;->numberFormat:Ljava/text/NumberFormat;

    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;->value:Ljava/lang/Number;

    invoke-virtual {v0, v1}, Ljava/text/NumberFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 96
    sget-object v1, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    sget-object v2, Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax$Type;->NORMAL:Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax$Type;

    .line 95
    invoke-interface {p1, v0, v1, v2}, Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;->escape(Ljava/lang/CharSequence;Ljava/util/Locale;Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax$Type;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getValue()Ljava/lang/Number;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;->value:Ljava/lang/Number;

    return-object v0
.end method

.method public bridge synthetic getValue()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;->getValue()Ljava/lang/Number;

    move-result-object v0

    return-object v0
.end method

.method public setField(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "fieldName"    # Ljava/lang/CharSequence;

    .prologue
    .line 83
    iput-object p1, p0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;->field:Ljava/lang/CharSequence;

    .line 84
    return-void
.end method

.method public setNumberFormat(Ljava/text/NumberFormat;)V
    .locals 0
    .param p1, "format"    # Ljava/text/NumberFormat;

    .prologue
    .line 114
    iput-object p1, p0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;->numberFormat:Ljava/text/NumberFormat;

    .line 115
    return-void
.end method

.method public setValue(Ljava/lang/Number;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/Number;

    .prologue
    .line 143
    iput-object p1, p0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;->value:Ljava/lang/Number;

    .line 144
    return-void
.end method

.method public bridge synthetic setValue(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;->setValue(Ljava/lang/Number;)V

    return-void
.end method

.method public toQueryString(Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;)Ljava/lang/CharSequence;
    .locals 2
    .param p1, "escapeSyntaxParser"    # Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;

    .prologue
    .line 101
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;->field:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;->isDefaultField(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;->getTermEscaped(Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 104
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;->field:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;->getTermEscaped(Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 148
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "<numeric field=\'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;->field:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\' number=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 149
    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;->numberFormat:Ljava/text/NumberFormat;

    iget-object v2, p0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;->value:Ljava/lang/Number;

    invoke-virtual {v1, v2}, Ljava/text/NumberFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'/>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 148
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
