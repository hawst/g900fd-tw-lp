.class public final Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;
.super Ljava/lang/Object;
.source "FastCharStream.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/surround/parser/CharStream;


# instance fields
.field buffer:[C

.field bufferLength:I

.field bufferPosition:I

.field bufferStart:I

.field input:Ljava/io/Reader;

.field tokenStart:I


# direct methods
.method public constructor <init>(Ljava/io/Reader;)V
    .locals 2
    .param p1, "r"    # Ljava/io/Reader;

    .prologue
    const/4 v1, 0x0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;->buffer:[C

    .line 29
    iput v1, p0, Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;->bufferLength:I

    .line 30
    iput v1, p0, Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;->bufferPosition:I

    .line 32
    iput v1, p0, Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;->tokenStart:I

    .line 33
    iput v1, p0, Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;->bufferStart:I

    .line 39
    iput-object p1, p0, Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;->input:Ljava/io/Reader;

    .line 40
    return-void
.end method

.method private final refill()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 50
    iget v3, p0, Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;->bufferLength:I

    iget v4, p0, Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;->tokenStart:I

    sub-int v2, v3, v4

    .line 52
    .local v2, "newPosition":I
    iget v3, p0, Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;->tokenStart:I

    if-nez v3, :cond_2

    .line 53
    iget-object v3, p0, Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;->buffer:[C

    if-nez v3, :cond_1

    .line 54
    const/16 v3, 0x800

    new-array v3, v3, [C

    iput-object v3, p0, Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;->buffer:[C

    .line 64
    :cond_0
    :goto_0
    iput v2, p0, Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;->bufferLength:I

    .line 65
    iput v2, p0, Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;->bufferPosition:I

    .line 66
    iget v3, p0, Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;->bufferStart:I

    iget v4, p0, Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;->tokenStart:I

    add-int/2addr v3, v4

    iput v3, p0, Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;->bufferStart:I

    .line 67
    iput v6, p0, Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;->tokenStart:I

    .line 70
    iget-object v3, p0, Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;->input:Ljava/io/Reader;

    iget-object v4, p0, Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;->buffer:[C

    iget-object v5, p0, Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;->buffer:[C

    array-length v5, v5

    sub-int/2addr v5, v2

    invoke-virtual {v3, v4, v2, v5}, Ljava/io/Reader;->read([CII)I

    move-result v0

    .line 71
    .local v0, "charsRead":I
    const/4 v3, -0x1

    if-ne v0, v3, :cond_3

    .line 72
    new-instance v3, Ljava/io/IOException;

    const-string v4, "read past eof"

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 55
    .end local v0    # "charsRead":I
    :cond_1
    iget v3, p0, Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;->bufferLength:I

    iget-object v4, p0, Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;->buffer:[C

    array-length v4, v4

    if-ne v3, v4, :cond_0

    .line 56
    iget-object v3, p0, Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;->buffer:[C

    array-length v3, v3

    mul-int/lit8 v3, v3, 0x2

    new-array v1, v3, [C

    .line 57
    .local v1, "newBuffer":[C
    iget-object v3, p0, Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;->buffer:[C

    iget v4, p0, Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;->bufferLength:I

    invoke-static {v3, v6, v1, v6, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 58
    iput-object v1, p0, Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;->buffer:[C

    goto :goto_0

    .line 61
    .end local v1    # "newBuffer":[C
    :cond_2
    iget-object v3, p0, Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;->buffer:[C

    iget v4, p0, Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;->tokenStart:I

    iget-object v5, p0, Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;->buffer:[C

    invoke-static {v3, v4, v5, v6, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0

    .line 74
    .restart local v0    # "charsRead":I
    :cond_3
    iget v3, p0, Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;->bufferLength:I

    add-int/2addr v3, v0

    iput v3, p0, Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;->bufferLength:I

    .line 75
    return-void
.end method


# virtual methods
.method public final BeginToken()C
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 79
    iget v0, p0, Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;->bufferPosition:I

    iput v0, p0, Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;->tokenStart:I

    .line 80
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;->readChar()C

    move-result v0

    return v0
.end method

.method public final Done()V
    .locals 1

    .prologue
    .line 103
    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;->input:Ljava/io/Reader;

    invoke-virtual {v0}, Ljava/io/Reader;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 106
    :goto_0
    return-void

    .line 104
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final GetImage()Ljava/lang/String;
    .locals 5

    .prologue
    .line 90
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;->buffer:[C

    iget v2, p0, Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;->tokenStart:I

    iget v3, p0, Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;->bufferPosition:I

    iget v4, p0, Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;->tokenStart:I

    sub-int/2addr v3, v4

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([CII)V

    return-object v0
.end method

.method public final GetSuffix(I)[C
    .locals 4
    .param p1, "len"    # I

    .prologue
    .line 95
    new-array v0, p1, [C

    .line 96
    .local v0, "value":[C
    iget-object v1, p0, Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;->buffer:[C

    iget v2, p0, Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;->bufferPosition:I

    sub-int/2addr v2, p1

    const/4 v3, 0x0

    invoke-static {v1, v2, v0, v3, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 97
    return-object v0
.end method

.method public final backup(I)V
    .locals 1
    .param p1, "amount"    # I

    .prologue
    .line 85
    iget v0, p0, Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;->bufferPosition:I

    sub-int/2addr v0, p1

    iput v0, p0, Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;->bufferPosition:I

    .line 86
    return-void
.end method

.method public final getBeginColumn()I
    .locals 2

    .prologue
    .line 126
    iget v0, p0, Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;->bufferStart:I

    iget v1, p0, Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;->tokenStart:I

    add-int/2addr v0, v1

    return v0
.end method

.method public final getBeginLine()I
    .locals 1

    .prologue
    .line 130
    const/4 v0, 0x1

    return v0
.end method

.method public final getColumn()I
    .locals 2

    .prologue
    .line 110
    iget v0, p0, Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;->bufferStart:I

    iget v1, p0, Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;->bufferPosition:I

    add-int/2addr v0, v1

    return v0
.end method

.method public final getEndColumn()I
    .locals 2

    .prologue
    .line 118
    iget v0, p0, Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;->bufferStart:I

    iget v1, p0, Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;->bufferPosition:I

    add-int/2addr v0, v1

    return v0
.end method

.method public final getEndLine()I
    .locals 1

    .prologue
    .line 122
    const/4 v0, 0x1

    return v0
.end method

.method public final getLine()I
    .locals 1

    .prologue
    .line 114
    const/4 v0, 0x1

    return v0
.end method

.method public final readChar()C
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 44
    iget v0, p0, Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;->bufferPosition:I

    iget v1, p0, Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;->bufferLength:I

    if-lt v0, v1, :cond_0

    .line 45
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;->refill()V

    .line 46
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;->buffer:[C

    iget v1, p0, Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;->bufferPosition:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/queryparser/surround/parser/FastCharStream;->bufferPosition:I

    aget-char v0, v0, v1

    return v0
.end method
