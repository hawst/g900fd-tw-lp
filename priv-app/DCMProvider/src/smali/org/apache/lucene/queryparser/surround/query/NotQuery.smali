.class public Lorg/apache/lucene/queryparser/surround/query/NotQuery;
.super Lorg/apache/lucene/queryparser/surround/query/ComposedQuery;
.source "NotQuery.java"


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/lang/String;)V
    .locals 1
    .param p2, "opName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/surround/query/SrndQuery;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 28
    .local p1, "queries":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/surround/query/SrndQuery;>;"
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0, p2}, Lorg/apache/lucene/queryparser/surround/query/ComposedQuery;-><init>(Ljava/util/List;ZLjava/lang/String;)V

    return-void
.end method


# virtual methods
.method public makeLuceneQueryFieldNoBoost(Ljava/lang/String;Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;)Lorg/apache/lucene/search/Query;
    .locals 4
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "qf"    # Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;

    .prologue
    .line 32
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/queryparser/surround/query/NotQuery;->makeLuceneSubQueriesField(Ljava/lang/String;Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;)Ljava/util/List;

    move-result-object v1

    .line 33
    .local v1, "luceneSubQueries":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/Query;>;"
    new-instance v0, Lorg/apache/lucene/search/BooleanQuery;

    invoke-direct {v0}, Lorg/apache/lucene/search/BooleanQuery;-><init>()V

    .line 34
    .local v0, "bq":Lorg/apache/lucene/search/BooleanQuery;
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/search/Query;

    sget-object v3, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 37
    const/4 v2, 0x1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    invoke-interface {v1, v2, v3}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v2

    .line 39
    sget-object v3, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST_NOT:Lorg/apache/lucene/search/BooleanClause$Occur;

    .line 35
    invoke-static {v0, v2, v3}, Lorg/apache/lucene/queryparser/surround/query/SrndBooleanQuery;->addQueriesToBoolean(Lorg/apache/lucene/search/BooleanQuery;Ljava/util/List;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 40
    return-object v0
.end method
