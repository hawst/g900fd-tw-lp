.class public Lorg/apache/lucene/queryparser/xml/CoreParser;
.super Ljava/lang/Object;
.source "CoreParser.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/xml/QueryBuilder;


# static fields
.field public static maxNumCachedFilters:I


# instance fields
.field protected analyzer:Lorg/apache/lucene/analysis/Analyzer;

.field protected filterFactory:Lorg/apache/lucene/queryparser/xml/FilterBuilderFactory;

.field protected parser:Lorg/apache/lucene/queryparser/classic/QueryParser;

.field protected queryFactory:Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const/16 v0, 0x14

    sput v0, Lorg/apache/lucene/queryparser/xml/CoreParser;->maxNumCachedFilters:I

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer;)V
    .locals 1
    .param p1, "defaultField"    # Ljava/lang/String;
    .param p2, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;

    .prologue
    .line 60
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/queryparser/xml/CoreParser;-><init>(Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer;Lorg/apache/lucene/queryparser/classic/QueryParser;)V

    .line 61
    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer;Lorg/apache/lucene/queryparser/classic/QueryParser;)V
    .locals 16
    .param p1, "defaultField"    # Ljava/lang/String;
    .param p2, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;
    .param p3, "parser"    # Lorg/apache/lucene/queryparser/classic/QueryParser;

    .prologue
    .line 63
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 64
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/queryparser/xml/CoreParser;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    .line 65
    move-object/from16 v0, p3

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/queryparser/xml/CoreParser;->parser:Lorg/apache/lucene/queryparser/classic/QueryParser;

    .line 66
    new-instance v10, Lorg/apache/lucene/queryparser/xml/FilterBuilderFactory;

    invoke-direct {v10}, Lorg/apache/lucene/queryparser/xml/FilterBuilderFactory;-><init>()V

    move-object/from16 v0, p0

    iput-object v10, v0, Lorg/apache/lucene/queryparser/xml/CoreParser;->filterFactory:Lorg/apache/lucene/queryparser/xml/FilterBuilderFactory;

    .line 67
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/lucene/queryparser/xml/CoreParser;->filterFactory:Lorg/apache/lucene/queryparser/xml/FilterBuilderFactory;

    const-string v11, "RangeFilter"

    new-instance v12, Lorg/apache/lucene/queryparser/xml/builders/RangeFilterBuilder;

    invoke-direct {v12}, Lorg/apache/lucene/queryparser/xml/builders/RangeFilterBuilder;-><init>()V

    invoke-virtual {v10, v11, v12}, Lorg/apache/lucene/queryparser/xml/FilterBuilderFactory;->addBuilder(Ljava/lang/String;Lorg/apache/lucene/queryparser/xml/FilterBuilder;)V

    .line 68
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/lucene/queryparser/xml/CoreParser;->filterFactory:Lorg/apache/lucene/queryparser/xml/FilterBuilderFactory;

    const-string v11, "NumericRangeFilter"

    new-instance v12, Lorg/apache/lucene/queryparser/xml/builders/NumericRangeFilterBuilder;

    invoke-direct {v12}, Lorg/apache/lucene/queryparser/xml/builders/NumericRangeFilterBuilder;-><init>()V

    invoke-virtual {v10, v11, v12}, Lorg/apache/lucene/queryparser/xml/FilterBuilderFactory;->addBuilder(Ljava/lang/String;Lorg/apache/lucene/queryparser/xml/FilterBuilder;)V

    .line 70
    new-instance v10, Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;

    invoke-direct {v10}, Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;-><init>()V

    move-object/from16 v0, p0

    iput-object v10, v0, Lorg/apache/lucene/queryparser/xml/CoreParser;->queryFactory:Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;

    .line 71
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/lucene/queryparser/xml/CoreParser;->queryFactory:Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;

    const-string v11, "TermQuery"

    new-instance v12, Lorg/apache/lucene/queryparser/xml/builders/TermQueryBuilder;

    invoke-direct {v12}, Lorg/apache/lucene/queryparser/xml/builders/TermQueryBuilder;-><init>()V

    invoke-virtual {v10, v11, v12}, Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;->addBuilder(Ljava/lang/String;Lorg/apache/lucene/queryparser/xml/QueryBuilder;)V

    .line 72
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/lucene/queryparser/xml/CoreParser;->queryFactory:Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;

    const-string v11, "TermsQuery"

    new-instance v12, Lorg/apache/lucene/queryparser/xml/builders/TermsQueryBuilder;

    move-object/from16 v0, p2

    invoke-direct {v12, v0}, Lorg/apache/lucene/queryparser/xml/builders/TermsQueryBuilder;-><init>(Lorg/apache/lucene/analysis/Analyzer;)V

    invoke-virtual {v10, v11, v12}, Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;->addBuilder(Ljava/lang/String;Lorg/apache/lucene/queryparser/xml/QueryBuilder;)V

    .line 73
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/lucene/queryparser/xml/CoreParser;->queryFactory:Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;

    const-string v11, "MatchAllDocsQuery"

    new-instance v12, Lorg/apache/lucene/queryparser/xml/builders/MatchAllDocsQueryBuilder;

    invoke-direct {v12}, Lorg/apache/lucene/queryparser/xml/builders/MatchAllDocsQueryBuilder;-><init>()V

    invoke-virtual {v10, v11, v12}, Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;->addBuilder(Ljava/lang/String;Lorg/apache/lucene/queryparser/xml/QueryBuilder;)V

    .line 74
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/lucene/queryparser/xml/CoreParser;->queryFactory:Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;

    const-string v11, "BooleanQuery"

    new-instance v12, Lorg/apache/lucene/queryparser/xml/builders/BooleanQueryBuilder;

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/queryparser/xml/CoreParser;->queryFactory:Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;

    invoke-direct {v12, v13}, Lorg/apache/lucene/queryparser/xml/builders/BooleanQueryBuilder;-><init>(Lorg/apache/lucene/queryparser/xml/QueryBuilder;)V

    invoke-virtual {v10, v11, v12}, Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;->addBuilder(Ljava/lang/String;Lorg/apache/lucene/queryparser/xml/QueryBuilder;)V

    .line 75
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/lucene/queryparser/xml/CoreParser;->queryFactory:Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;

    const-string v11, "NumericRangeQuery"

    new-instance v12, Lorg/apache/lucene/queryparser/xml/builders/NumericRangeQueryBuilder;

    invoke-direct {v12}, Lorg/apache/lucene/queryparser/xml/builders/NumericRangeQueryBuilder;-><init>()V

    invoke-virtual {v10, v11, v12}, Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;->addBuilder(Ljava/lang/String;Lorg/apache/lucene/queryparser/xml/QueryBuilder;)V

    .line 76
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/lucene/queryparser/xml/CoreParser;->queryFactory:Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;

    const-string v11, "DisjunctionMaxQuery"

    new-instance v12, Lorg/apache/lucene/queryparser/xml/builders/DisjunctionMaxQueryBuilder;

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/queryparser/xml/CoreParser;->queryFactory:Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;

    invoke-direct {v12, v13}, Lorg/apache/lucene/queryparser/xml/builders/DisjunctionMaxQueryBuilder;-><init>(Lorg/apache/lucene/queryparser/xml/QueryBuilder;)V

    invoke-virtual {v10, v11, v12}, Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;->addBuilder(Ljava/lang/String;Lorg/apache/lucene/queryparser/xml/QueryBuilder;)V

    .line 77
    if-eqz p3, :cond_0

    .line 78
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/lucene/queryparser/xml/CoreParser;->queryFactory:Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;

    const-string v11, "UserQuery"

    new-instance v12, Lorg/apache/lucene/queryparser/xml/builders/UserInputQueryBuilder;

    move-object/from16 v0, p3

    invoke-direct {v12, v0}, Lorg/apache/lucene/queryparser/xml/builders/UserInputQueryBuilder;-><init>(Lorg/apache/lucene/queryparser/classic/QueryParser;)V

    invoke-virtual {v10, v11, v12}, Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;->addBuilder(Ljava/lang/String;Lorg/apache/lucene/queryparser/xml/QueryBuilder;)V

    .line 82
    :goto_0
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/lucene/queryparser/xml/CoreParser;->queryFactory:Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;

    const-string v11, "FilteredQuery"

    new-instance v12, Lorg/apache/lucene/queryparser/xml/builders/FilteredQueryBuilder;

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/queryparser/xml/CoreParser;->filterFactory:Lorg/apache/lucene/queryparser/xml/FilterBuilderFactory;

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/queryparser/xml/CoreParser;->queryFactory:Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;

    invoke-direct {v12, v13, v14}, Lorg/apache/lucene/queryparser/xml/builders/FilteredQueryBuilder;-><init>(Lorg/apache/lucene/queryparser/xml/FilterBuilder;Lorg/apache/lucene/queryparser/xml/QueryBuilder;)V

    invoke-virtual {v10, v11, v12}, Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;->addBuilder(Ljava/lang/String;Lorg/apache/lucene/queryparser/xml/QueryBuilder;)V

    .line 83
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/lucene/queryparser/xml/CoreParser;->queryFactory:Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;

    const-string v11, "ConstantScoreQuery"

    new-instance v12, Lorg/apache/lucene/queryparser/xml/builders/ConstantScoreQueryBuilder;

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/queryparser/xml/CoreParser;->filterFactory:Lorg/apache/lucene/queryparser/xml/FilterBuilderFactory;

    invoke-direct {v12, v13}, Lorg/apache/lucene/queryparser/xml/builders/ConstantScoreQueryBuilder;-><init>(Lorg/apache/lucene/queryparser/xml/FilterBuilderFactory;)V

    invoke-virtual {v10, v11, v12}, Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;->addBuilder(Ljava/lang/String;Lorg/apache/lucene/queryparser/xml/QueryBuilder;)V

    .line 85
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/lucene/queryparser/xml/CoreParser;->filterFactory:Lorg/apache/lucene/queryparser/xml/FilterBuilderFactory;

    const-string v11, "CachedFilter"

    new-instance v12, Lorg/apache/lucene/queryparser/xml/builders/CachedFilterBuilder;

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/queryparser/xml/CoreParser;->queryFactory:Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;

    .line 86
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/queryparser/xml/CoreParser;->filterFactory:Lorg/apache/lucene/queryparser/xml/FilterBuilderFactory;

    sget v15, Lorg/apache/lucene/queryparser/xml/CoreParser;->maxNumCachedFilters:I

    invoke-direct {v12, v13, v14, v15}, Lorg/apache/lucene/queryparser/xml/builders/CachedFilterBuilder;-><init>(Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;Lorg/apache/lucene/queryparser/xml/FilterBuilderFactory;I)V

    .line 85
    invoke-virtual {v10, v11, v12}, Lorg/apache/lucene/queryparser/xml/FilterBuilderFactory;->addBuilder(Ljava/lang/String;Lorg/apache/lucene/queryparser/xml/FilterBuilder;)V

    .line 89
    new-instance v9, Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilderFactory;

    invoke-direct {v9}, Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilderFactory;-><init>()V

    .line 91
    .local v9, "sqof":Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilderFactory;
    new-instance v4, Lorg/apache/lucene/queryparser/xml/builders/SpanNearBuilder;

    invoke-direct {v4, v9}, Lorg/apache/lucene/queryparser/xml/builders/SpanNearBuilder;-><init>(Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilder;)V

    .line 92
    .local v4, "snb":Lorg/apache/lucene/queryparser/xml/builders/SpanNearBuilder;
    const-string v10, "SpanNear"

    invoke-virtual {v9, v10, v4}, Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilderFactory;->addBuilder(Ljava/lang/String;Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilder;)V

    .line 93
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/lucene/queryparser/xml/CoreParser;->queryFactory:Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;

    const-string v11, "SpanNear"

    invoke-virtual {v10, v11, v4}, Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;->addBuilder(Ljava/lang/String;Lorg/apache/lucene/queryparser/xml/QueryBuilder;)V

    .line 95
    new-instance v2, Lorg/apache/lucene/queryparser/xml/builders/BoostingTermBuilder;

    invoke-direct {v2}, Lorg/apache/lucene/queryparser/xml/builders/BoostingTermBuilder;-><init>()V

    .line 96
    .local v2, "btb":Lorg/apache/lucene/queryparser/xml/builders/BoostingTermBuilder;
    const-string v10, "BoostingTermQuery"

    invoke-virtual {v9, v10, v2}, Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilderFactory;->addBuilder(Ljava/lang/String;Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilder;)V

    .line 97
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/lucene/queryparser/xml/CoreParser;->queryFactory:Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;

    const-string v11, "BoostingTermQuery"

    invoke-virtual {v10, v11, v2}, Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;->addBuilder(Ljava/lang/String;Lorg/apache/lucene/queryparser/xml/QueryBuilder;)V

    .line 99
    new-instance v6, Lorg/apache/lucene/queryparser/xml/builders/SpanTermBuilder;

    invoke-direct {v6}, Lorg/apache/lucene/queryparser/xml/builders/SpanTermBuilder;-><init>()V

    .line 100
    .local v6, "snt":Lorg/apache/lucene/queryparser/xml/builders/SpanTermBuilder;
    const-string v10, "SpanTerm"

    invoke-virtual {v9, v10, v6}, Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilderFactory;->addBuilder(Ljava/lang/String;Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilder;)V

    .line 101
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/lucene/queryparser/xml/CoreParser;->queryFactory:Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;

    const-string v11, "SpanTerm"

    invoke-virtual {v10, v11, v6}, Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;->addBuilder(Ljava/lang/String;Lorg/apache/lucene/queryparser/xml/QueryBuilder;)V

    .line 103
    new-instance v7, Lorg/apache/lucene/queryparser/xml/builders/SpanOrBuilder;

    invoke-direct {v7, v9}, Lorg/apache/lucene/queryparser/xml/builders/SpanOrBuilder;-><init>(Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilder;)V

    .line 104
    .local v7, "sot":Lorg/apache/lucene/queryparser/xml/builders/SpanOrBuilder;
    const-string v10, "SpanOr"

    invoke-virtual {v9, v10, v7}, Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilderFactory;->addBuilder(Ljava/lang/String;Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilder;)V

    .line 105
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/lucene/queryparser/xml/CoreParser;->queryFactory:Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;

    const-string v11, "SpanOr"

    invoke-virtual {v10, v11, v7}, Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;->addBuilder(Ljava/lang/String;Lorg/apache/lucene/queryparser/xml/QueryBuilder;)V

    .line 107
    new-instance v8, Lorg/apache/lucene/queryparser/xml/builders/SpanOrTermsBuilder;

    move-object/from16 v0, p2

    invoke-direct {v8, v0}, Lorg/apache/lucene/queryparser/xml/builders/SpanOrTermsBuilder;-><init>(Lorg/apache/lucene/analysis/Analyzer;)V

    .line 108
    .local v8, "sots":Lorg/apache/lucene/queryparser/xml/builders/SpanOrTermsBuilder;
    const-string v10, "SpanOrTerms"

    invoke-virtual {v9, v10, v8}, Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilderFactory;->addBuilder(Ljava/lang/String;Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilder;)V

    .line 109
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/lucene/queryparser/xml/CoreParser;->queryFactory:Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;

    const-string v11, "SpanOrTerms"

    invoke-virtual {v10, v11, v8}, Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;->addBuilder(Ljava/lang/String;Lorg/apache/lucene/queryparser/xml/QueryBuilder;)V

    .line 111
    new-instance v3, Lorg/apache/lucene/queryparser/xml/builders/SpanFirstBuilder;

    invoke-direct {v3, v9}, Lorg/apache/lucene/queryparser/xml/builders/SpanFirstBuilder;-><init>(Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilder;)V

    .line 112
    .local v3, "sft":Lorg/apache/lucene/queryparser/xml/builders/SpanFirstBuilder;
    const-string v10, "SpanFirst"

    invoke-virtual {v9, v10, v3}, Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilderFactory;->addBuilder(Ljava/lang/String;Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilder;)V

    .line 113
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/lucene/queryparser/xml/CoreParser;->queryFactory:Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;

    const-string v11, "SpanFirst"

    invoke-virtual {v10, v11, v3}, Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;->addBuilder(Ljava/lang/String;Lorg/apache/lucene/queryparser/xml/QueryBuilder;)V

    .line 115
    new-instance v5, Lorg/apache/lucene/queryparser/xml/builders/SpanNotBuilder;

    invoke-direct {v5, v9}, Lorg/apache/lucene/queryparser/xml/builders/SpanNotBuilder;-><init>(Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilder;)V

    .line 116
    .local v5, "snot":Lorg/apache/lucene/queryparser/xml/builders/SpanNotBuilder;
    const-string v10, "SpanNot"

    invoke-virtual {v9, v10, v5}, Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilderFactory;->addBuilder(Ljava/lang/String;Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilder;)V

    .line 117
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/lucene/queryparser/xml/CoreParser;->queryFactory:Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;

    const-string v11, "SpanNot"

    invoke-virtual {v10, v11, v5}, Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;->addBuilder(Ljava/lang/String;Lorg/apache/lucene/queryparser/xml/QueryBuilder;)V

    .line 118
    return-void

    .line 80
    .end local v2    # "btb":Lorg/apache/lucene/queryparser/xml/builders/BoostingTermBuilder;
    .end local v3    # "sft":Lorg/apache/lucene/queryparser/xml/builders/SpanFirstBuilder;
    .end local v4    # "snb":Lorg/apache/lucene/queryparser/xml/builders/SpanNearBuilder;
    .end local v5    # "snot":Lorg/apache/lucene/queryparser/xml/builders/SpanNotBuilder;
    .end local v6    # "snt":Lorg/apache/lucene/queryparser/xml/builders/SpanTermBuilder;
    .end local v7    # "sot":Lorg/apache/lucene/queryparser/xml/builders/SpanOrBuilder;
    .end local v8    # "sots":Lorg/apache/lucene/queryparser/xml/builders/SpanOrTermsBuilder;
    .end local v9    # "sqof":Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilderFactory;
    :cond_0
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/lucene/queryparser/xml/CoreParser;->queryFactory:Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;

    const-string v11, "UserQuery"

    new-instance v12, Lorg/apache/lucene/queryparser/xml/builders/UserInputQueryBuilder;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-direct {v12, v0, v1}, Lorg/apache/lucene/queryparser/xml/builders/UserInputQueryBuilder;-><init>(Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer;)V

    invoke-virtual {v10, v11, v12}, Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;->addBuilder(Ljava/lang/String;Lorg/apache/lucene/queryparser/xml/QueryBuilder;)V

    goto/16 :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/analysis/Analyzer;Lorg/apache/lucene/queryparser/classic/QueryParser;)V
    .locals 1
    .param p1, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;
    .param p2, "parser"    # Lorg/apache/lucene/queryparser/classic/QueryParser;

    .prologue
    .line 51
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1, p2}, Lorg/apache/lucene/queryparser/xml/CoreParser;-><init>(Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer;Lorg/apache/lucene/queryparser/classic/QueryParser;)V

    .line 52
    return-void
.end method

.method private static parseXML(Ljava/io/InputStream;)Lorg/w3c/dom/Document;
    .locals 7
    .param p0, "pXmlFile"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/xml/ParserException;
        }
    .end annotation

    .prologue
    .line 133
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v1

    .line 134
    .local v1, "dbf":Ljavax/xml/parsers/DocumentBuilderFactory;
    const/4 v0, 0x0

    .line 136
    .local v0, "db":Ljavax/xml/parsers/DocumentBuilder;
    :try_start_0
    invoke-virtual {v1}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 141
    const/4 v2, 0x0

    .line 143
    .local v2, "doc":Lorg/w3c/dom/Document;
    :try_start_1
    invoke-virtual {v0, p0}, Ljavax/xml/parsers/DocumentBuilder;->parse(Ljava/io/InputStream;)Lorg/w3c/dom/Document;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v2

    .line 148
    return-object v2

    .line 138
    .end local v2    # "doc":Lorg/w3c/dom/Document;
    :catch_0
    move-exception v3

    .line 139
    .local v3, "se":Ljava/lang/Exception;
    new-instance v4, Lorg/apache/lucene/queryparser/xml/ParserException;

    const-string v5, "XML Parser configuration error"

    invoke-direct {v4, v5, v3}, Lorg/apache/lucene/queryparser/xml/ParserException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4

    .line 145
    .end local v3    # "se":Ljava/lang/Exception;
    .restart local v2    # "doc":Lorg/w3c/dom/Document;
    :catch_1
    move-exception v3

    .line 146
    .restart local v3    # "se":Ljava/lang/Exception;
    new-instance v4, Lorg/apache/lucene/queryparser/xml/ParserException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Error parsing XML stream:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5, v3}, Lorg/apache/lucene/queryparser/xml/ParserException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
.end method


# virtual methods
.method public addFilterBuilder(Ljava/lang/String;Lorg/apache/lucene/queryparser/xml/FilterBuilder;)V
    .locals 1
    .param p1, "nodeName"    # Ljava/lang/String;
    .param p2, "builder"    # Lorg/apache/lucene/queryparser/xml/FilterBuilder;

    .prologue
    .line 129
    iget-object v0, p0, Lorg/apache/lucene/queryparser/xml/CoreParser;->filterFactory:Lorg/apache/lucene/queryparser/xml/FilterBuilderFactory;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/queryparser/xml/FilterBuilderFactory;->addBuilder(Ljava/lang/String;Lorg/apache/lucene/queryparser/xml/FilterBuilder;)V

    .line 130
    return-void
.end method

.method public addQueryBuilder(Ljava/lang/String;Lorg/apache/lucene/queryparser/xml/QueryBuilder;)V
    .locals 1
    .param p1, "nodeName"    # Ljava/lang/String;
    .param p2, "builder"    # Lorg/apache/lucene/queryparser/xml/QueryBuilder;

    .prologue
    .line 125
    iget-object v0, p0, Lorg/apache/lucene/queryparser/xml/CoreParser;->queryFactory:Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;->addBuilder(Ljava/lang/String;Lorg/apache/lucene/queryparser/xml/QueryBuilder;)V

    .line 126
    return-void
.end method

.method public getQuery(Lorg/w3c/dom/Element;)Lorg/apache/lucene/search/Query;
    .locals 1
    .param p1, "e"    # Lorg/w3c/dom/Element;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/xml/ParserException;
        }
    .end annotation

    .prologue
    .line 154
    iget-object v0, p0, Lorg/apache/lucene/queryparser/xml/CoreParser;->queryFactory:Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/queryparser/xml/QueryBuilderFactory;->getQuery(Lorg/w3c/dom/Element;)Lorg/apache/lucene/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public parse(Ljava/io/InputStream;)Lorg/apache/lucene/search/Query;
    .locals 1
    .param p1, "xmlStream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/xml/ParserException;
        }
    .end annotation

    .prologue
    .line 121
    invoke-static {p1}, Lorg/apache/lucene/queryparser/xml/CoreParser;->parseXML(Ljava/io/InputStream;)Lorg/w3c/dom/Document;

    move-result-object v0

    invoke-interface {v0}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/xml/CoreParser;->getQuery(Lorg/w3c/dom/Element;)Lorg/apache/lucene/search/Query;

    move-result-object v0

    return-object v0
.end method
