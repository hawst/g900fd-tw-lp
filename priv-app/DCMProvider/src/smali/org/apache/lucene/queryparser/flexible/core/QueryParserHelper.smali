.class public Lorg/apache/lucene/queryparser/flexible/core/QueryParserHelper;
.super Ljava/lang/Object;
.source "QueryParserHelper.java"


# instance fields
.field private builder:Lorg/apache/lucene/queryparser/flexible/core/builders/QueryBuilder;

.field private config:Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

.field private processor:Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;

.field private syntaxParser:Lorg/apache/lucene/queryparser/flexible/core/parser/SyntaxParser;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;Lorg/apache/lucene/queryparser/flexible/core/parser/SyntaxParser;Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;Lorg/apache/lucene/queryparser/flexible/core/builders/QueryBuilder;)V
    .locals 0
    .param p1, "queryConfigHandler"    # Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;
    .param p2, "syntaxParser"    # Lorg/apache/lucene/queryparser/flexible/core/parser/SyntaxParser;
    .param p3, "processor"    # Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;
    .param p4, "builder"    # Lorg/apache/lucene/queryparser/flexible/core/builders/QueryBuilder;

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iput-object p2, p0, Lorg/apache/lucene/queryparser/flexible/core/QueryParserHelper;->syntaxParser:Lorg/apache/lucene/queryparser/flexible/core/parser/SyntaxParser;

    .line 75
    iput-object p1, p0, Lorg/apache/lucene/queryparser/flexible/core/QueryParserHelper;->config:Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    .line 76
    iput-object p3, p0, Lorg/apache/lucene/queryparser/flexible/core/QueryParserHelper;->processor:Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;

    .line 77
    iput-object p4, p0, Lorg/apache/lucene/queryparser/flexible/core/QueryParserHelper;->builder:Lorg/apache/lucene/queryparser/flexible/core/builders/QueryBuilder;

    .line 79
    if-eqz p3, :cond_0

    .line 80
    invoke-interface {p3, p1}, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;->setQueryConfigHandler(Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;)V

    .line 83
    :cond_0
    return-void
.end method


# virtual methods
.method public getQueryBuilder()Lorg/apache/lucene/queryparser/flexible/core/builders/QueryBuilder;
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/QueryParserHelper;->builder:Lorg/apache/lucene/queryparser/flexible/core/builders/QueryBuilder;

    return-object v0
.end method

.method public getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/QueryParserHelper;->config:Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    return-object v0
.end method

.method public getQueryNodeProcessor()Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/QueryParserHelper;->processor:Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;

    return-object v0
.end method

.method public getSyntaxParser()Lorg/apache/lucene/queryparser/flexible/core/parser/SyntaxParser;
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/QueryParserHelper;->syntaxParser:Lorg/apache/lucene/queryparser/flexible/core/parser/SyntaxParser;

    return-object v0
.end method

.method public parse(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;
    .locals 3
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "defaultField"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 250
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/QueryParserHelper;->getSyntaxParser()Lorg/apache/lucene/queryparser/flexible/core/parser/SyntaxParser;

    move-result-object v2

    invoke-interface {v2, p1, p2}, Lorg/apache/lucene/queryparser/flexible/core/parser/SyntaxParser;->parse(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v1

    .line 252
    .local v1, "queryTree":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/QueryParserHelper;->getQueryNodeProcessor()Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;

    move-result-object v0

    .line 254
    .local v0, "processor":Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;
    if-eqz v0, :cond_0

    .line 255
    invoke-interface {v0, v1}, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;->process(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v1

    .line 258
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/QueryParserHelper;->getQueryBuilder()Lorg/apache/lucene/queryparser/flexible/core/builders/QueryBuilder;

    move-result-object v2

    invoke-interface {v2, v1}, Lorg/apache/lucene/queryparser/flexible/core/builders/QueryBuilder;->build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Ljava/lang/Object;

    move-result-object v2

    return-object v2
.end method

.method public setQueryBuilder(Lorg/apache/lucene/queryparser/flexible/core/builders/QueryBuilder;)V
    .locals 2
    .param p1, "queryBuilder"    # Lorg/apache/lucene/queryparser/flexible/core/builders/QueryBuilder;

    .prologue
    .line 151
    if-nez p1, :cond_0

    .line 152
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "queryBuilder should not be null!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 155
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/queryparser/flexible/core/QueryParserHelper;->builder:Lorg/apache/lucene/queryparser/flexible/core/builders/QueryBuilder;

    .line 157
    return-void
.end method

.method public setQueryConfigHandler(Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;)V
    .locals 1
    .param p1, "config"    # Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    .prologue
    .line 214
    iput-object p1, p0, Lorg/apache/lucene/queryparser/flexible/core/QueryParserHelper;->config:Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    .line 215
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/QueryParserHelper;->getQueryNodeProcessor()Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;

    move-result-object v0

    .line 217
    .local v0, "processor":Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;
    if-eqz v0, :cond_0

    .line 218
    invoke-interface {v0, p1}, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;->setQueryConfigHandler(Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;)V

    .line 221
    :cond_0
    return-void
.end method

.method public setQueryNodeProcessor(Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;)V
    .locals 2
    .param p1, "processor"    # Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;

    .prologue
    .line 114
    iput-object p1, p0, Lorg/apache/lucene/queryparser/flexible/core/QueryParserHelper;->processor:Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;

    .line 115
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/QueryParserHelper;->processor:Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;

    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/QueryParserHelper;->getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;->setQueryConfigHandler(Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;)V

    .line 117
    return-void
.end method

.method public setSyntaxParser(Lorg/apache/lucene/queryparser/flexible/core/parser/SyntaxParser;)V
    .locals 2
    .param p1, "syntaxParser"    # Lorg/apache/lucene/queryparser/flexible/core/parser/SyntaxParser;

    .prologue
    .line 131
    if-nez p1, :cond_0

    .line 132
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "textParser should not be null!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 135
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/queryparser/flexible/core/QueryParserHelper;->syntaxParser:Lorg/apache/lucene/queryparser/flexible/core/parser/SyntaxParser;

    .line 137
    return-void
.end method
