.class public Lorg/apache/lucene/queryparser/xml/builders/SpanNotBuilder;
.super Lorg/apache/lucene/queryparser/xml/builders/SpanBuilderBase;
.source "SpanNotBuilder.java"


# instance fields
.field private final factory:Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilder;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilder;)V
    .locals 0
    .param p1, "factory"    # Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilder;

    .prologue
    .line 32
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/xml/builders/SpanBuilderBase;-><init>()V

    .line 33
    iput-object p1, p0, Lorg/apache/lucene/queryparser/xml/builders/SpanNotBuilder;->factory:Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilder;

    .line 34
    return-void
.end method


# virtual methods
.method public getSpanQuery(Lorg/w3c/dom/Element;)Lorg/apache/lucene/search/spans/SpanQuery;
    .locals 7
    .param p1, "e"    # Lorg/w3c/dom/Element;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/xml/ParserException;
        }
    .end annotation

    .prologue
    .line 38
    const-string v5, "Include"

    invoke-static {p1, v5}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getChildByTagOrFail(Lorg/w3c/dom/Element;Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v3

    .line 39
    .local v3, "includeElem":Lorg/w3c/dom/Element;
    invoke-static {v3}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getFirstChildOrFail(Lorg/w3c/dom/Element;)Lorg/w3c/dom/Element;

    move-result-object v3

    .line 41
    const-string v5, "Exclude"

    invoke-static {p1, v5}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getChildByTagOrFail(Lorg/w3c/dom/Element;Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v1

    .line 42
    .local v1, "excludeElem":Lorg/w3c/dom/Element;
    invoke-static {v1}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getFirstChildOrFail(Lorg/w3c/dom/Element;)Lorg/w3c/dom/Element;

    move-result-object v1

    .line 44
    iget-object v5, p0, Lorg/apache/lucene/queryparser/xml/builders/SpanNotBuilder;->factory:Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilder;

    invoke-interface {v5, v3}, Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilder;->getSpanQuery(Lorg/w3c/dom/Element;)Lorg/apache/lucene/search/spans/SpanQuery;

    move-result-object v2

    .line 45
    .local v2, "include":Lorg/apache/lucene/search/spans/SpanQuery;
    iget-object v5, p0, Lorg/apache/lucene/queryparser/xml/builders/SpanNotBuilder;->factory:Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilder;

    invoke-interface {v5, v1}, Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilder;->getSpanQuery(Lorg/w3c/dom/Element;)Lorg/apache/lucene/search/spans/SpanQuery;

    move-result-object v0

    .line 47
    .local v0, "exclude":Lorg/apache/lucene/search/spans/SpanQuery;
    new-instance v4, Lorg/apache/lucene/search/spans/SpanNotQuery;

    invoke-direct {v4, v2, v0}, Lorg/apache/lucene/search/spans/SpanNotQuery;-><init>(Lorg/apache/lucene/search/spans/SpanQuery;Lorg/apache/lucene/search/spans/SpanQuery;)V

    .line 49
    .local v4, "snq":Lorg/apache/lucene/search/spans/SpanNotQuery;
    const-string v5, "boost"

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-static {p1, v5, v6}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttribute(Lorg/w3c/dom/Element;Ljava/lang/String;F)F

    move-result v5

    invoke-virtual {v4, v5}, Lorg/apache/lucene/search/spans/SpanNotQuery;->setBoost(F)V

    .line 50
    return-object v4
.end method
