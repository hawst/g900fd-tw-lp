.class public Lorg/apache/lucene/queryparser/xml/builders/NumericRangeQueryBuilder;
.super Ljava/lang/Object;
.source "NumericRangeQueryBuilder.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/xml/QueryBuilder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getQuery(Lorg/w3c/dom/Element;)Lorg/apache/lucene/search/Query;
    .locals 11
    .param p1, "e"    # Lorg/w3c/dom/Element;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/xml/ParserException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 92
    const-string v2, "fieldName"

    invoke-static {p1, v2}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttributeWithInheritanceOrFail(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 93
    .local v0, "field":Ljava/lang/String;
    const-string v2, "lowerTerm"

    invoke-static {p1, v2}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttributeOrFail(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 94
    .local v7, "lowerTerm":Ljava/lang/String;
    const-string v2, "upperTerm"

    invoke-static {p1, v2}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttributeOrFail(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 95
    .local v10, "upperTerm":Ljava/lang/String;
    const-string v2, "includeLower"

    invoke-static {p1, v2, v3}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttribute(Lorg/w3c/dom/Element;Ljava/lang/String;Z)Z

    move-result v4

    .line 96
    .local v4, "lowerInclusive":Z
    const-string v2, "includeUpper"

    invoke-static {p1, v2, v3}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttribute(Lorg/w3c/dom/Element;Ljava/lang/String;Z)Z

    move-result v5

    .line 97
    .local v5, "upperInclusive":Z
    const-string v2, "precisionStep"

    const/4 v3, 0x4

    invoke-static {p1, v2, v3}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttribute(Lorg/w3c/dom/Element;Ljava/lang/String;I)I

    move-result v1

    .line 99
    .local v1, "precisionStep":I
    const-string v2, "type"

    const-string v3, "int"

    invoke-static {p1, v2, v3}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttribute(Lorg/w3c/dom/Element;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 102
    .local v9, "type":Ljava/lang/String;
    :try_start_0
    const-string v2, "int"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 104
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    .line 103
    invoke-static/range {v0 .. v5}, Lorg/apache/lucene/search/NumericRangeQuery;->newIntRange(Ljava/lang/String;ILjava/lang/Integer;Ljava/lang/Integer;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v6

    .line 121
    .local v6, "filter":Lorg/apache/lucene/search/Query;
    :goto_0
    return-object v6

    .line 106
    .end local v6    # "filter":Lorg/apache/lucene/search/Query;
    :cond_0
    const-string v2, "long"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 108
    invoke-static {v7}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v10}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    .line 107
    invoke-static/range {v0 .. v5}, Lorg/apache/lucene/search/NumericRangeQuery;->newLongRange(Ljava/lang/String;ILjava/lang/Long;Ljava/lang/Long;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v6

    .line 110
    .restart local v6    # "filter":Lorg/apache/lucene/search/Query;
    goto :goto_0

    .end local v6    # "filter":Lorg/apache/lucene/search/Query;
    :cond_1
    const-string v2, "double"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 112
    invoke-static {v7}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v2

    invoke-static {v10}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v3

    .line 111
    invoke-static/range {v0 .. v5}, Lorg/apache/lucene/search/NumericRangeQuery;->newDoubleRange(Ljava/lang/String;ILjava/lang/Double;Ljava/lang/Double;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v6

    .line 114
    .restart local v6    # "filter":Lorg/apache/lucene/search/Query;
    goto :goto_0

    .end local v6    # "filter":Lorg/apache/lucene/search/Query;
    :cond_2
    const-string v2, "float"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 116
    invoke-static {v7}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v2

    invoke-static {v10}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v3

    .line 115
    invoke-static/range {v0 .. v5}, Lorg/apache/lucene/search/NumericRangeQuery;->newFloatRange(Ljava/lang/String;ILjava/lang/Float;Ljava/lang/Float;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v6

    .line 118
    .restart local v6    # "filter":Lorg/apache/lucene/search/Query;
    goto :goto_0

    .line 119
    .end local v6    # "filter":Lorg/apache/lucene/search/Query;
    :cond_3
    new-instance v2, Lorg/apache/lucene/queryparser/xml/ParserException;

    const-string v3, "type attribute must be one of: [long, int, double, float]"

    invoke-direct {v2, v3}, Lorg/apache/lucene/queryparser/xml/ParserException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 122
    :catch_0
    move-exception v8

    .line 123
    .local v8, "nfe":Ljava/lang/NumberFormatException;
    new-instance v2, Lorg/apache/lucene/queryparser/xml/ParserException;

    const-string v3, "Could not parse lowerTerm or upperTerm into a number"

    invoke-direct {v2, v3, v8}, Lorg/apache/lucene/queryparser/xml/ParserException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method
