.class public Lorg/apache/lucene/queryparser/flexible/core/nodes/FuzzyQueryNode;
.super Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
.source "FuzzyQueryNode.java"


# instance fields
.field private prefixLength:I

.field private similarity:F


# direct methods
.method public constructor <init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;FII)V
    .locals 1
    .param p1, "field"    # Ljava/lang/CharSequence;
    .param p2, "term"    # Ljava/lang/CharSequence;
    .param p3, "minSimilarity"    # F
    .param p4, "begin"    # I
    .param p5, "end"    # I

    .prologue
    .line 52
    invoke-direct {p0, p1, p2, p4, p5}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;II)V

    .line 53
    iput p3, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FuzzyQueryNode;->similarity:F

    .line 54
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FuzzyQueryNode;->setLeaf(Z)V

    .line 55
    return-void
.end method


# virtual methods
.method public bridge synthetic cloneTree()Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FuzzyQueryNode;->cloneTree()Lorg/apache/lucene/queryparser/flexible/core/nodes/FuzzyQueryNode;

    move-result-object v0

    return-object v0
.end method

.method public cloneTree()Lorg/apache/lucene/queryparser/flexible/core/nodes/FuzzyQueryNode;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 86
    invoke-super {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->cloneTree()Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FuzzyQueryNode;

    .line 88
    .local v0, "clone":Lorg/apache/lucene/queryparser/flexible/core/nodes/FuzzyQueryNode;
    iget v1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FuzzyQueryNode;->similarity:F

    iput v1, v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FuzzyQueryNode;->similarity:F

    .line 90
    return-object v0
.end method

.method public bridge synthetic cloneTree()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FuzzyQueryNode;->cloneTree()Lorg/apache/lucene/queryparser/flexible/core/nodes/FuzzyQueryNode;

    move-result-object v0

    return-object v0
.end method

.method public getPrefixLength()I
    .locals 1

    .prologue
    .line 62
    iget v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FuzzyQueryNode;->prefixLength:I

    return v0
.end method

.method public getSimilarity()F
    .locals 1

    .prologue
    .line 97
    iget v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FuzzyQueryNode;->similarity:F

    return v0
.end method

.method public setPrefixLength(I)V
    .locals 0
    .param p1, "prefixLength"    # I

    .prologue
    .line 58
    iput p1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FuzzyQueryNode;->prefixLength:I

    .line 59
    return-void
.end method

.method public setSimilarity(F)V
    .locals 0
    .param p1, "similarity"    # F

    .prologue
    .line 81
    iput p1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FuzzyQueryNode;->similarity:F

    .line 82
    return-void
.end method

.method public toQueryString(Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;)Ljava/lang/CharSequence;
    .locals 2
    .param p1, "escaper"    # Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;

    .prologue
    .line 67
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FuzzyQueryNode;->field:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FuzzyQueryNode;->isDefaultField(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 68
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FuzzyQueryNode;->getTermEscaped(Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "~"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FuzzyQueryNode;->similarity:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 70
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FuzzyQueryNode;->field:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FuzzyQueryNode;->getTermEscaped(Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "~"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FuzzyQueryNode;->similarity:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 76
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "<fuzzy field=\'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FuzzyQueryNode;->field:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\' similarity=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FuzzyQueryNode;->similarity:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 77
    const-string v1, "\' term=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FuzzyQueryNode;->text:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'/>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 76
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
