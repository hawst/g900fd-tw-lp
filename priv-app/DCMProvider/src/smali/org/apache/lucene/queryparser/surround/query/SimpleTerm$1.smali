.class Lorg/apache/lucene/queryparser/surround/query/SimpleTerm$1;
.super Ljava/lang/Object;
.source "SimpleTerm.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/surround/query/SimpleTerm$MatchingTermVisitor;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/queryparser/surround/query/SimpleTerm;->addSpanQueries(Lorg/apache/lucene/queryparser/surround/query/SpanNearClauseFactory;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/queryparser/surround/query/SimpleTerm;

.field private final synthetic val$sncf:Lorg/apache/lucene/queryparser/surround/query/SpanNearClauseFactory;


# direct methods
.method constructor <init>(Lorg/apache/lucene/queryparser/surround/query/SimpleTerm;Lorg/apache/lucene/queryparser/surround/query/SpanNearClauseFactory;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/queryparser/surround/query/SimpleTerm$1;->this$0:Lorg/apache/lucene/queryparser/surround/query/SimpleTerm;

    iput-object p2, p0, Lorg/apache/lucene/queryparser/surround/query/SimpleTerm$1;->val$sncf:Lorg/apache/lucene/queryparser/surround/query/SpanNearClauseFactory;

    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public visitMatchingTerm(Lorg/apache/lucene/index/Term;)V
    .locals 2
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 93
    iget-object v0, p0, Lorg/apache/lucene/queryparser/surround/query/SimpleTerm$1;->val$sncf:Lorg/apache/lucene/queryparser/surround/query/SpanNearClauseFactory;

    iget-object v1, p0, Lorg/apache/lucene/queryparser/surround/query/SimpleTerm$1;->this$0:Lorg/apache/lucene/queryparser/surround/query/SimpleTerm;

    invoke-virtual {v1}, Lorg/apache/lucene/queryparser/surround/query/SimpleTerm;->getWeight()F

    move-result v1

    invoke-virtual {v0, p1, v1}, Lorg/apache/lucene/queryparser/surround/query/SpanNearClauseFactory;->addTermWeighted(Lorg/apache/lucene/index/Term;F)V

    .line 94
    return-void
.end method
