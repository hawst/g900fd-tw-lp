.class Lorg/apache/lucene/queryparser/surround/query/SrndBooleanQuery;
.super Ljava/lang/Object;
.source "SrndBooleanQuery.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addQueriesToBoolean(Lorg/apache/lucene/search/BooleanQuery;Ljava/util/List;Lorg/apache/lucene/search/BooleanClause$Occur;)V
    .locals 2
    .param p0, "bq"    # Lorg/apache/lucene/search/BooleanQuery;
    .param p2, "occur"    # Lorg/apache/lucene/search/BooleanClause$Occur;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/search/BooleanQuery;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/search/Query;",
            ">;",
            "Lorg/apache/lucene/search/BooleanClause$Occur;",
            ")V"
        }
    .end annotation

    .prologue
    .line 30
    .local p1, "queries":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/Query;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 33
    return-void

    .line 31
    :cond_0
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/search/Query;

    invoke-virtual {p0, v1, p2}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 30
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static makeBooleanQuery(Ljava/util/List;Lorg/apache/lucene/search/BooleanClause$Occur;)Lorg/apache/lucene/search/Query;
    .locals 4
    .param p1, "occur"    # Lorg/apache/lucene/search/BooleanClause$Occur;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/search/Query;",
            ">;",
            "Lorg/apache/lucene/search/BooleanClause$Occur;",
            ")",
            "Lorg/apache/lucene/search/Query;"
        }
    .end annotation

    .prologue
    .line 38
    .local p0, "queries":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/Query;>;"
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-gt v1, v2, :cond_0

    .line 39
    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Too few subqueries: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 41
    :cond_0
    new-instance v0, Lorg/apache/lucene/search/BooleanQuery;

    invoke-direct {v0}, Lorg/apache/lucene/search/BooleanQuery;-><init>()V

    .line 42
    .local v0, "bq":Lorg/apache/lucene/search/BooleanQuery;
    const/4 v1, 0x0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    invoke-interface {p0, v1, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1, p1}, Lorg/apache/lucene/queryparser/surround/query/SrndBooleanQuery;->addQueriesToBoolean(Lorg/apache/lucene/search/BooleanQuery;Ljava/util/List;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 43
    return-object v0
.end method
