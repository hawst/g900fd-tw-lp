.class public Lorg/apache/lucene/queryparser/flexible/standard/processors/StandardQueryNodeProcessorPipeline;
.super Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorPipeline;
.source "StandardQueryNodeProcessorPipeline.java"


# direct methods
.method public constructor <init>(Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;)V
    .locals 1
    .param p1, "queryConfig"    # Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorPipeline;-><init>(Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;)V

    .line 54
    new-instance v0, Lorg/apache/lucene/queryparser/flexible/standard/processors/WildcardQueryNodeProcessor;

    invoke-direct {v0}, Lorg/apache/lucene/queryparser/flexible/standard/processors/WildcardQueryNodeProcessor;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/flexible/standard/processors/StandardQueryNodeProcessorPipeline;->add(Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;)Z

    .line 55
    new-instance v0, Lorg/apache/lucene/queryparser/flexible/standard/processors/MultiFieldQueryNodeProcessor;

    invoke-direct {v0}, Lorg/apache/lucene/queryparser/flexible/standard/processors/MultiFieldQueryNodeProcessor;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/flexible/standard/processors/StandardQueryNodeProcessorPipeline;->add(Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;)Z

    .line 56
    new-instance v0, Lorg/apache/lucene/queryparser/flexible/standard/processors/FuzzyQueryNodeProcessor;

    invoke-direct {v0}, Lorg/apache/lucene/queryparser/flexible/standard/processors/FuzzyQueryNodeProcessor;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/flexible/standard/processors/StandardQueryNodeProcessorPipeline;->add(Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;)Z

    .line 57
    new-instance v0, Lorg/apache/lucene/queryparser/flexible/standard/processors/MatchAllDocsQueryNodeProcessor;

    invoke-direct {v0}, Lorg/apache/lucene/queryparser/flexible/standard/processors/MatchAllDocsQueryNodeProcessor;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/flexible/standard/processors/StandardQueryNodeProcessorPipeline;->add(Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;)Z

    .line 58
    new-instance v0, Lorg/apache/lucene/queryparser/flexible/standard/processors/OpenRangeQueryNodeProcessor;

    invoke-direct {v0}, Lorg/apache/lucene/queryparser/flexible/standard/processors/OpenRangeQueryNodeProcessor;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/flexible/standard/processors/StandardQueryNodeProcessorPipeline;->add(Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;)Z

    .line 59
    new-instance v0, Lorg/apache/lucene/queryparser/flexible/standard/processors/NumericQueryNodeProcessor;

    invoke-direct {v0}, Lorg/apache/lucene/queryparser/flexible/standard/processors/NumericQueryNodeProcessor;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/flexible/standard/processors/StandardQueryNodeProcessorPipeline;->add(Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;)Z

    .line 60
    new-instance v0, Lorg/apache/lucene/queryparser/flexible/standard/processors/NumericRangeQueryNodeProcessor;

    invoke-direct {v0}, Lorg/apache/lucene/queryparser/flexible/standard/processors/NumericRangeQueryNodeProcessor;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/flexible/standard/processors/StandardQueryNodeProcessorPipeline;->add(Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;)Z

    .line 61
    new-instance v0, Lorg/apache/lucene/queryparser/flexible/standard/processors/LowercaseExpandedTermsQueryNodeProcessor;

    invoke-direct {v0}, Lorg/apache/lucene/queryparser/flexible/standard/processors/LowercaseExpandedTermsQueryNodeProcessor;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/flexible/standard/processors/StandardQueryNodeProcessorPipeline;->add(Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;)Z

    .line 62
    new-instance v0, Lorg/apache/lucene/queryparser/flexible/standard/processors/TermRangeQueryNodeProcessor;

    invoke-direct {v0}, Lorg/apache/lucene/queryparser/flexible/standard/processors/TermRangeQueryNodeProcessor;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/flexible/standard/processors/StandardQueryNodeProcessorPipeline;->add(Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;)Z

    .line 63
    new-instance v0, Lorg/apache/lucene/queryparser/flexible/standard/processors/AllowLeadingWildcardProcessor;

    invoke-direct {v0}, Lorg/apache/lucene/queryparser/flexible/standard/processors/AllowLeadingWildcardProcessor;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/flexible/standard/processors/StandardQueryNodeProcessorPipeline;->add(Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;)Z

    .line 64
    new-instance v0, Lorg/apache/lucene/queryparser/flexible/standard/processors/AnalyzerQueryNodeProcessor;

    invoke-direct {v0}, Lorg/apache/lucene/queryparser/flexible/standard/processors/AnalyzerQueryNodeProcessor;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/flexible/standard/processors/StandardQueryNodeProcessorPipeline;->add(Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;)Z

    .line 65
    new-instance v0, Lorg/apache/lucene/queryparser/flexible/standard/processors/PhraseSlopQueryNodeProcessor;

    invoke-direct {v0}, Lorg/apache/lucene/queryparser/flexible/standard/processors/PhraseSlopQueryNodeProcessor;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/flexible/standard/processors/StandardQueryNodeProcessorPipeline;->add(Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;)Z

    .line 67
    new-instance v0, Lorg/apache/lucene/queryparser/flexible/standard/processors/BooleanQuery2ModifierNodeProcessor;

    invoke-direct {v0}, Lorg/apache/lucene/queryparser/flexible/standard/processors/BooleanQuery2ModifierNodeProcessor;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/flexible/standard/processors/StandardQueryNodeProcessorPipeline;->add(Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;)Z

    .line 68
    new-instance v0, Lorg/apache/lucene/queryparser/flexible/core/processors/NoChildOptimizationQueryNodeProcessor;

    invoke-direct {v0}, Lorg/apache/lucene/queryparser/flexible/core/processors/NoChildOptimizationQueryNodeProcessor;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/flexible/standard/processors/StandardQueryNodeProcessorPipeline;->add(Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;)Z

    .line 69
    new-instance v0, Lorg/apache/lucene/queryparser/flexible/core/processors/RemoveDeletedQueryNodesProcessor;

    invoke-direct {v0}, Lorg/apache/lucene/queryparser/flexible/core/processors/RemoveDeletedQueryNodesProcessor;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/flexible/standard/processors/StandardQueryNodeProcessorPipeline;->add(Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;)Z

    .line 70
    new-instance v0, Lorg/apache/lucene/queryparser/flexible/standard/processors/RemoveEmptyNonLeafQueryNodeProcessor;

    invoke-direct {v0}, Lorg/apache/lucene/queryparser/flexible/standard/processors/RemoveEmptyNonLeafQueryNodeProcessor;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/flexible/standard/processors/StandardQueryNodeProcessorPipeline;->add(Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;)Z

    .line 71
    new-instance v0, Lorg/apache/lucene/queryparser/flexible/standard/processors/BooleanSingleChildOptimizationQueryNodeProcessor;

    invoke-direct {v0}, Lorg/apache/lucene/queryparser/flexible/standard/processors/BooleanSingleChildOptimizationQueryNodeProcessor;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/flexible/standard/processors/StandardQueryNodeProcessorPipeline;->add(Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;)Z

    .line 72
    new-instance v0, Lorg/apache/lucene/queryparser/flexible/standard/processors/DefaultPhraseSlopQueryNodeProcessor;

    invoke-direct {v0}, Lorg/apache/lucene/queryparser/flexible/standard/processors/DefaultPhraseSlopQueryNodeProcessor;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/flexible/standard/processors/StandardQueryNodeProcessorPipeline;->add(Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;)Z

    .line 73
    new-instance v0, Lorg/apache/lucene/queryparser/flexible/standard/processors/BoostQueryNodeProcessor;

    invoke-direct {v0}, Lorg/apache/lucene/queryparser/flexible/standard/processors/BoostQueryNodeProcessor;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/flexible/standard/processors/StandardQueryNodeProcessorPipeline;->add(Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;)Z

    .line 74
    new-instance v0, Lorg/apache/lucene/queryparser/flexible/standard/processors/MultiTermRewriteMethodProcessor;

    invoke-direct {v0}, Lorg/apache/lucene/queryparser/flexible/standard/processors/MultiTermRewriteMethodProcessor;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/flexible/standard/processors/StandardQueryNodeProcessorPipeline;->add(Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;)Z

    .line 75
    return-void
.end method
