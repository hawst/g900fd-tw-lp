.class public Lorg/apache/lucene/queryparser/flexible/standard/config/FieldBoostMapFCListener;
.super Ljava/lang/Object;
.source "FieldBoostMapFCListener.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/flexible/core/config/FieldConfigListener;


# instance fields
.field private config:Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;)V
    .locals 1
    .param p1, "config"    # Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/config/FieldBoostMapFCListener;->config:Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    .line 43
    iput-object p1, p0, Lorg/apache/lucene/queryparser/flexible/standard/config/FieldBoostMapFCListener;->config:Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    .line 44
    return-void
.end method


# virtual methods
.method public buildFieldConfig(Lorg/apache/lucene/queryparser/flexible/core/config/FieldConfig;)V
    .locals 4
    .param p1, "fieldConfig"    # Lorg/apache/lucene/queryparser/flexible/core/config/FieldConfig;

    .prologue
    .line 48
    iget-object v2, p0, Lorg/apache/lucene/queryparser/flexible/standard/config/FieldBoostMapFCListener;->config:Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    sget-object v3, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->FIELD_BOOST_MAP:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->get(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    .line 50
    .local v1, "fieldBoostMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Float;>;"
    if-eqz v1, :cond_0

    .line 51
    invoke-virtual {p1}, Lorg/apache/lucene/queryparser/flexible/core/config/FieldConfig;->getField()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    .line 53
    .local v0, "boost":Ljava/lang/Float;
    if-eqz v0, :cond_0

    .line 54
    sget-object v2, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->BOOST:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-virtual {p1, v2, v0}, Lorg/apache/lucene/queryparser/flexible/core/config/FieldConfig;->set(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;Ljava/lang/Object;)V

    .line 58
    .end local v0    # "boost":Ljava/lang/Float;
    :cond_0
    return-void
.end method
