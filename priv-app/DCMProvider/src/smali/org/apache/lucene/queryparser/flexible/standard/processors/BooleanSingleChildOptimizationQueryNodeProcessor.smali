.class public Lorg/apache/lucene/queryparser/flexible/standard/processors/BooleanSingleChildOptimizationQueryNodeProcessor;
.super Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl;
.source "BooleanSingleChildOptimizationQueryNodeProcessor.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl;-><init>()V

    .line 43
    return-void
.end method


# virtual methods
.method protected postProcessNode(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 5
    .param p1, "node"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 48
    instance-of v3, p1, Lorg/apache/lucene/queryparser/flexible/core/nodes/BooleanQueryNode;

    if-eqz v3, :cond_1

    .line 49
    invoke-interface {p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->getChildren()Ljava/util/List;

    move-result-object v1

    .line 51
    .local v1, "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    .line 52
    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    .line 54
    .local v0, "child":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    instance-of v3, v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;

    if-eqz v3, :cond_0

    move-object v2, v0

    .line 55
    check-cast v2, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;

    .line 57
    .local v2, "modNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;
    instance-of v3, v2, Lorg/apache/lucene/queryparser/flexible/standard/nodes/BooleanModifierNode;

    if-nez v3, :cond_0

    .line 58
    invoke-virtual {v2}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;->getModifier()Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    move-result-object v3

    sget-object v4, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;->MOD_NONE:Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    if-ne v3, v4, :cond_1

    .line 72
    .end local v0    # "child":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .end local v1    # "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    .end local v2    # "modNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    move-object v0, p1

    goto :goto_0
.end method

.method protected preProcessNode(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 0
    .param p1, "node"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 79
    return-object p1
.end method

.method protected setChildrenOrder(Ljava/util/List;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 87
    .local p1, "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    return-object p1
.end method
