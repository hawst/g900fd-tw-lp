.class public Lorg/apache/lucene/queryparser/xml/builders/BoostingTermBuilder;
.super Lorg/apache/lucene/queryparser/xml/builders/SpanBuilderBase;
.source "BoostingTermBuilder.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/xml/builders/SpanBuilderBase;-><init>()V

    return-void
.end method


# virtual methods
.method public getSpanQuery(Lorg/w3c/dom/Element;)Lorg/apache/lucene/search/spans/SpanQuery;
    .locals 5
    .param p1, "e"    # Lorg/w3c/dom/Element;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/xml/ParserException;
        }
    .end annotation

    .prologue
    .line 35
    const-string v3, "fieldName"

    invoke-static {p1, v3}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttributeWithInheritanceOrFail(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 36
    .local v1, "fieldName":Ljava/lang/String;
    invoke-static {p1}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getNonBlankTextOrFail(Lorg/w3c/dom/Element;)Ljava/lang/String;

    move-result-object v2

    .line 38
    .local v2, "value":Ljava/lang/String;
    new-instance v0, Lorg/apache/lucene/search/payloads/PayloadTermQuery;

    new-instance v3, Lorg/apache/lucene/index/Term;

    invoke-direct {v3, v1, v2}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v4, Lorg/apache/lucene/search/payloads/AveragePayloadFunction;

    invoke-direct {v4}, Lorg/apache/lucene/search/payloads/AveragePayloadFunction;-><init>()V

    invoke-direct {v0, v3, v4}, Lorg/apache/lucene/search/payloads/PayloadTermQuery;-><init>(Lorg/apache/lucene/index/Term;Lorg/apache/lucene/search/payloads/PayloadFunction;)V

    .line 39
    .local v0, "btq":Lorg/apache/lucene/search/payloads/PayloadTermQuery;
    const-string v3, "boost"

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-static {p1, v3, v4}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttribute(Lorg/w3c/dom/Element;Ljava/lang/String;F)F

    move-result v3

    invoke-virtual {v0, v3}, Lorg/apache/lucene/search/payloads/PayloadTermQuery;->setBoost(F)V

    .line 40
    return-object v0
.end method
