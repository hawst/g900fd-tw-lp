.class public abstract Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;
.super Lorg/apache/lucene/queryparser/flexible/core/config/AbstractQueryConfig;
.source "QueryConfigHandler.java"


# instance fields
.field private final listeners:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/config/FieldConfigListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/core/config/AbstractQueryConfig;-><init>()V

    .line 43
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->listeners:Ljava/util/LinkedList;

    .line 41
    return-void
.end method


# virtual methods
.method public addFieldConfigListener(Lorg/apache/lucene/queryparser/flexible/core/config/FieldConfigListener;)V
    .locals 1
    .param p1, "listener"    # Lorg/apache/lucene/queryparser/flexible/core/config/FieldConfigListener;

    .prologue
    .line 77
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->listeners:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 78
    return-void
.end method

.method public getFieldConfig(Ljava/lang/String;)Lorg/apache/lucene/queryparser/flexible/core/config/FieldConfig;
    .locals 4
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 59
    new-instance v0, Lorg/apache/lucene/queryparser/flexible/core/config/FieldConfig;

    invoke-static {p1}, Lorg/apache/lucene/queryparser/flexible/core/util/StringUtils;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Lorg/apache/lucene/queryparser/flexible/core/config/FieldConfig;-><init>(Ljava/lang/String;)V

    .line 61
    .local v0, "fieldConfig":Lorg/apache/lucene/queryparser/flexible/core/config/FieldConfig;
    iget-object v2, p0, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->listeners:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 65
    return-object v0

    .line 61
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/queryparser/flexible/core/config/FieldConfigListener;

    .line 62
    .local v1, "listener":Lorg/apache/lucene/queryparser/flexible/core/config/FieldConfigListener;
    invoke-interface {v1, v0}, Lorg/apache/lucene/queryparser/flexible/core/config/FieldConfigListener;->buildFieldConfig(Lorg/apache/lucene/queryparser/flexible/core/config/FieldConfig;)V

    goto :goto_0
.end method
