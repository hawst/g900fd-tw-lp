.class public final Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;
.super Ljava/lang/Object;
.source "StandardQueryConfigHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ConfigurationKeys"
.end annotation


# static fields
.field public static final ALLOW_LEADING_WILDCARD:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final ANALYZER:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey",
            "<",
            "Lorg/apache/lucene/analysis/Analyzer;",
            ">;"
        }
    .end annotation
.end field

.field public static final BOOST:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public static final DATE_RESOLUTION:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey",
            "<",
            "Lorg/apache/lucene/document/DateTools$Resolution;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_OPERATOR:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$Operator;",
            ">;"
        }
    .end annotation
.end field

.field public static final ENABLE_POSITION_INCREMENTS:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final FIELD_BOOST_MAP:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Float;",
            ">;>;"
        }
    .end annotation
.end field

.field public static final FIELD_DATE_RESOLUTION_MAP:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/CharSequence;",
            "Lorg/apache/lucene/document/DateTools$Resolution;",
            ">;>;"
        }
    .end annotation
.end field

.field public static final FUZZY_CONFIG:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/standard/config/FuzzyConfig;",
            ">;"
        }
    .end annotation
.end field

.field public static final LOCALE:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey",
            "<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field public static final LOWERCASE_EXPANDED_TERMS:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final MULTI_FIELDS:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey",
            "<[",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field public static final MULTI_TERM_REWRITE_METHOD:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey",
            "<",
            "Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;",
            ">;"
        }
    .end annotation
.end field

.field public static final NUMERIC_CONFIG:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;",
            ">;"
        }
    .end annotation
.end field

.field public static final NUMERIC_CONFIG_MAP:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/queryparser/flexible/standard/config/NumericConfig;",
            ">;>;"
        }
    .end annotation
.end field

.field public static final PHRASE_SLOP:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final TIMEZONE:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey",
            "<",
            "Ljava/util/TimeZone;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    invoke-static {}, Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;->newInstance()Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->ENABLE_POSITION_INCREMENTS:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    .line 66
    invoke-static {}, Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;->newInstance()Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->LOWERCASE_EXPANDED_TERMS:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    .line 74
    invoke-static {}, Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;->newInstance()Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->ALLOW_LEADING_WILDCARD:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    .line 82
    invoke-static {}, Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;->newInstance()Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->ANALYZER:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    .line 90
    invoke-static {}, Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;->newInstance()Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->DEFAULT_OPERATOR:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    .line 98
    invoke-static {}, Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;->newInstance()Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->PHRASE_SLOP:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    .line 106
    invoke-static {}, Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;->newInstance()Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->LOCALE:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    .line 108
    invoke-static {}, Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;->newInstance()Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->TIMEZONE:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    .line 116
    invoke-static {}, Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;->newInstance()Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->MULTI_TERM_REWRITE_METHOD:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    .line 125
    invoke-static {}, Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;->newInstance()Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->MULTI_FIELDS:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    .line 133
    invoke-static {}, Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;->newInstance()Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->FIELD_BOOST_MAP:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    .line 142
    invoke-static {}, Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;->newInstance()Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->FIELD_DATE_RESOLUTION_MAP:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    .line 152
    invoke-static {}, Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;->newInstance()Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->FUZZY_CONFIG:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    .line 160
    invoke-static {}, Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;->newInstance()Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->DATE_RESOLUTION:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    .line 168
    invoke-static {}, Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;->newInstance()Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->BOOST:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    .line 176
    invoke-static {}, Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;->newInstance()Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->NUMERIC_CONFIG:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    .line 184
    invoke-static {}, Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;->newInstance()Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->NUMERIC_CONFIG_MAP:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
