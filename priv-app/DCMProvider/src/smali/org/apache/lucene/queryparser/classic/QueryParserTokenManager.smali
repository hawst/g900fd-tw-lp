.class public Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;
.super Ljava/lang/Object;
.source "QueryParserTokenManager.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/classic/QueryParserConstants;


# static fields
.field static final jjbitVec0:[J

.field static final jjbitVec1:[J

.field static final jjbitVec3:[J

.field static final jjbitVec4:[J

.field public static final jjnewLexState:[I

.field static final jjnextStates:[I

.field public static final jjstrLiteralImages:[Ljava/lang/String;

.field static final jjtoSkip:[J

.field static final jjtoToken:[J

.field public static final lexStateNames:[Ljava/lang/String;


# instance fields
.field protected curChar:C

.field curLexState:I

.field public debugStream:Ljava/io/PrintStream;

.field defaultLexState:I

.field protected input_stream:Lorg/apache/lucene/queryparser/classic/CharStream;

.field jjmatchedKind:I

.field jjmatchedPos:I

.field jjnewStateCnt:I

.field jjround:I

.field private final jjrounds:[I

.field private final jjstateSet:[I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x0

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, -0x1

    .line 75
    new-array v0, v8, [J

    .line 76
    const-wide/16 v2, 0x1

    aput-wide v2, v0, v7

    .line 75
    sput-object v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjbitVec0:[J

    .line 78
    new-array v0, v8, [J

    fill-array-data v0, :array_0

    sput-object v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjbitVec1:[J

    .line 81
    new-array v0, v8, [J

    .line 82
    const-wide/16 v2, -0x1

    aput-wide v2, v0, v6

    const/4 v1, 0x3

    const-wide/16 v2, -0x1

    aput-wide v2, v0, v1

    .line 81
    sput-object v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjbitVec3:[J

    .line 84
    new-array v0, v8, [J

    fill-array-data v0, :array_1

    sput-object v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjbitVec4:[J

    .line 914
    const/16 v0, 0x24

    new-array v0, v0, [I

    .line 915
    const/16 v1, 0x25

    aput v1, v0, v7

    const/16 v1, 0x27

    aput v1, v0, v5

    const/16 v1, 0x28

    aput v1, v0, v6

    const/4 v1, 0x3

    const/16 v2, 0x11

    aput v2, v0, v1

    const/16 v1, 0x12

    aput v1, v0, v8

    const/4 v1, 0x5

    const/16 v2, 0x14

    aput v2, v0, v1

    const/4 v1, 0x6

    const/16 v2, 0x2a

    aput v2, v0, v1

    const/4 v1, 0x7

    const/16 v2, 0x2d

    aput v2, v0, v1

    const/16 v1, 0x8

    const/16 v2, 0x1f

    aput v2, v0, v1

    const/16 v1, 0x9

    const/16 v2, 0x2e

    aput v2, v0, v1

    const/16 v1, 0xa

    const/16 v2, 0x2b

    aput v2, v0, v1

    const/16 v1, 0xb

    const/16 v2, 0x16

    aput v2, v0, v1

    const/16 v1, 0xc

    const/16 v2, 0x17

    aput v2, v0, v1

    const/16 v1, 0xd

    const/16 v2, 0x19

    aput v2, v0, v1

    const/16 v1, 0xe

    const/16 v2, 0x1a

    aput v2, v0, v1

    const/16 v1, 0xf

    const/16 v2, 0x18

    aput v2, v0, v1

    const/16 v1, 0x10

    .line 916
    const/16 v2, 0x19

    aput v2, v0, v1

    const/16 v1, 0x11

    const/16 v2, 0x1a

    aput v2, v0, v1

    const/16 v1, 0x12

    const/16 v2, 0x2d

    aput v2, v0, v1

    const/16 v1, 0x13

    const/16 v2, 0x1f

    aput v2, v0, v1

    const/16 v1, 0x14

    const/16 v2, 0x2e

    aput v2, v0, v1

    const/16 v1, 0x15

    const/16 v2, 0x2c

    aput v2, v0, v1

    const/16 v1, 0x16

    const/16 v2, 0x2f

    aput v2, v0, v1

    const/16 v1, 0x17

    const/16 v2, 0x23

    aput v2, v0, v1

    const/16 v1, 0x18

    const/16 v2, 0x16

    aput v2, v0, v1

    const/16 v1, 0x19

    const/16 v2, 0x1c

    aput v2, v0, v1

    const/16 v1, 0x1a

    const/16 v2, 0x1d

    aput v2, v0, v1

    const/16 v1, 0x1b

    const/16 v2, 0x1b

    aput v2, v0, v1

    const/16 v1, 0x1c

    const/16 v2, 0x1b

    aput v2, v0, v1

    const/16 v1, 0x1d

    const/16 v2, 0x1e

    aput v2, v0, v1

    const/16 v1, 0x1e

    const/16 v2, 0x1e

    aput v2, v0, v1

    const/16 v1, 0x20

    .line 917
    aput v5, v0, v1

    const/16 v1, 0x21

    aput v6, v0, v1

    const/16 v1, 0x22

    aput v8, v0, v1

    const/16 v1, 0x23

    const/4 v2, 0x5

    aput v2, v0, v1

    .line 914
    sput-object v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjnextStates:[I

    .line 957
    const/16 v0, 0x21

    new-array v0, v0, [Ljava/lang/String;

    .line 958
    const-string v1, ""

    aput-object v1, v0, v7

    const/16 v1, 0xb

    const-string v2, "+"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "-"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    .line 959
    const-string v2, "("

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, ":"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "*"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "^"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    .line 960
    const-string v2, "["

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string/jumbo v2, "{"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "TO"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "]"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string/jumbo v2, "}"

    aput-object v2, v0, v1

    .line 957
    sput-object v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjstrLiteralImages:[Ljava/lang/String;

    .line 963
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    .line 964
    const-string v1, "Boost"

    aput-object v1, v0, v7

    .line 965
    const-string v1, "Range"

    aput-object v1, v0, v5

    .line 966
    const-string v1, "DEFAULT"

    aput-object v1, v0, v6

    .line 963
    sput-object v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->lexStateNames:[Ljava/lang/String;

    .line 970
    const/16 v0, 0x21

    new-array v0, v0, [I

    .line 971
    aput v4, v0, v7

    aput v4, v0, v5

    aput v4, v0, v6

    const/4 v1, 0x3

    aput v4, v0, v1

    aput v4, v0, v8

    const/4 v1, 0x5

    aput v4, v0, v1

    const/4 v1, 0x6

    aput v4, v0, v1

    const/4 v1, 0x7

    aput v4, v0, v1

    const/16 v1, 0x8

    aput v4, v0, v1

    const/16 v1, 0x9

    aput v4, v0, v1

    const/16 v1, 0xa

    aput v4, v0, v1

    const/16 v1, 0xb

    aput v4, v0, v1

    const/16 v1, 0xc

    aput v4, v0, v1

    const/16 v1, 0xd

    aput v4, v0, v1

    const/16 v1, 0xe

    aput v4, v0, v1

    const/16 v1, 0xf

    aput v4, v0, v1

    const/16 v1, 0x10

    aput v4, v0, v1

    const/16 v1, 0x11

    aput v4, v0, v1

    const/16 v1, 0x13

    aput v4, v0, v1

    const/16 v1, 0x14

    aput v4, v0, v1

    const/16 v1, 0x15

    aput v4, v0, v1

    const/16 v1, 0x16

    aput v4, v0, v1

    const/16 v1, 0x17

    aput v4, v0, v1

    const/16 v1, 0x18

    aput v4, v0, v1

    const/16 v1, 0x19

    .line 972
    aput v5, v0, v1

    const/16 v1, 0x1a

    aput v5, v0, v1

    const/16 v1, 0x1b

    aput v6, v0, v1

    const/16 v1, 0x1c

    aput v4, v0, v1

    const/16 v1, 0x1d

    aput v6, v0, v1

    const/16 v1, 0x1e

    aput v6, v0, v1

    const/16 v1, 0x1f

    aput v4, v0, v1

    const/16 v1, 0x20

    aput v4, v0, v1

    .line 970
    sput-object v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjnewLexState:[I

    .line 974
    new-array v0, v5, [J

    .line 975
    const-wide v2, 0x1ffffff01L

    aput-wide v2, v0, v7

    .line 974
    sput-object v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjtoToken:[J

    .line 977
    new-array v0, v5, [J

    .line 978
    const-wide/16 v2, 0x80

    aput-wide v2, v0, v7

    .line 977
    sput-object v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjtoSkip:[J

    .line 979
    return-void

    .line 78
    :array_0
    .array-data 8
        -0x2
        -0x1
        -0x1
        -0x1
    .end array-data

    .line 84
    :array_1
    .array-data 8
        -0x1000000000002L
        -0x1
        -0x1
        -0x1
    .end array-data
.end method

.method public constructor <init>(Lorg/apache/lucene/queryparser/classic/CharStream;)V
    .locals 2
    .param p1, "stream"    # Lorg/apache/lucene/queryparser/classic/CharStream;

    .prologue
    const/4 v1, 0x2

    .line 985
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    iput-object v0, p0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->debugStream:Ljava/io/PrintStream;

    .line 981
    const/16 v0, 0x31

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjrounds:[I

    .line 982
    const/16 v0, 0x62

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjstateSet:[I

    .line 1051
    iput v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curLexState:I

    .line 1052
    iput v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->defaultLexState:I

    .line 986
    iput-object p1, p0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/classic/CharStream;

    .line 987
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/queryparser/classic/CharStream;I)V
    .locals 0
    .param p1, "stream"    # Lorg/apache/lucene/queryparser/classic/CharStream;
    .param p2, "lexState"    # I

    .prologue
    .line 991
    invoke-direct {p0, p1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;-><init>(Lorg/apache/lucene/queryparser/classic/CharStream;)V

    .line 992
    invoke-virtual {p0, p2}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->SwitchTo(I)V

    .line 993
    return-void
.end method

.method private ReInitRounds()V
    .locals 4

    .prologue
    .line 1006
    const v2, -0x7fffffff

    iput v2, p0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjround:I

    .line 1007
    const/16 v0, 0x31

    .local v0, "i":I
    move v1, v0

    .end local v0    # "i":I
    .local v1, "i":I
    :goto_0
    add-int/lit8 v0, v1, -0x1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    if-gtz v1, :cond_0

    .line 1009
    return-void

    .line 1008
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjrounds:[I

    const/high16 v3, -0x80000000

    aput v3, v2, v0

    move v1, v0

    .end local v0    # "i":I
    .restart local v1    # "i":I
    goto :goto_0
.end method

.method private jjAddStates(II)V
    .locals 4
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 1148
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjstateSet:[I

    iget v2, p0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjnewStateCnt:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjnewStateCnt:I

    sget-object v3, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjnextStates:[I

    aget v3, v3, p1

    aput v3, v1, v2

    .line 1149
    add-int/lit8 v0, p1, 0x1

    .end local p1    # "start":I
    .local v0, "start":I
    if-ne p1, p2, :cond_0

    .line 1150
    return-void

    :cond_0
    move p1, v0

    .end local v0    # "start":I
    .restart local p1    # "start":I
    goto :goto_0
.end method

.method private static final jjCanMove_0(IIIJJ)Z
    .locals 7
    .param p0, "hiByte"    # I
    .param p1, "i1"    # I
    .param p2, "i2"    # I
    .param p3, "l1"    # J
    .param p5, "l2"    # J

    .prologue
    const/4 v0, 0x0

    .line 921
    packed-switch p0, :pswitch_data_0

    .line 926
    :cond_0
    :goto_0
    return v0

    .line 924
    :pswitch_0
    sget-object v1, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjbitVec0:[J

    aget-wide v2, v1, p2

    and-long/2addr v2, p5

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    .line 921
    :pswitch_data_0
    .packed-switch 0x30
        :pswitch_0
    .end packed-switch
.end method

.method private static final jjCanMove_1(IIIJJ)Z
    .locals 7
    .param p0, "hiByte"    # I
    .param p1, "i1"    # I
    .param p2, "i2"    # I
    .param p3, "l1"    # J
    .param p5, "l2"    # J

    .prologue
    const-wide/16 v4, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 931
    packed-switch p0, :pswitch_data_0

    .line 936
    sget-object v2, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjbitVec1:[J

    aget-wide v2, v2, p1

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 938
    :cond_0
    :goto_0
    return v0

    .line 934
    :pswitch_0
    sget-object v2, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjbitVec3:[J

    aget-wide v2, v2, p2

    and-long/2addr v2, p5

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 938
    goto :goto_0

    .line 931
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method private static final jjCanMove_2(IIIJJ)Z
    .locals 7
    .param p0, "hiByte"    # I
    .param p1, "i1"    # I
    .param p2, "i2"    # I
    .param p3, "l1"    # J
    .param p5, "l2"    # J

    .prologue
    const-wide/16 v4, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 943
    sparse-switch p0, :sswitch_data_0

    .line 950
    sget-object v2, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjbitVec4:[J

    aget-wide v2, v2, p1

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 952
    :cond_0
    :goto_0
    return v0

    .line 946
    :sswitch_0
    sget-object v2, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjbitVec3:[J

    aget-wide v2, v2, p2

    and-long/2addr v2, p5

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 948
    :sswitch_1
    sget-object v2, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjbitVec1:[J

    aget-wide v2, v2, p2

    and-long/2addr v2, p5

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 952
    goto :goto_0

    .line 943
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x30 -> :sswitch_1
    .end sparse-switch
.end method

.method private jjCheckNAdd(I)V
    .locals 3
    .param p1, "state"    # I

    .prologue
    .line 1139
    iget-object v0, p0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjrounds:[I

    aget v0, v0, p1

    iget v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjround:I

    if-eq v0, v1, :cond_0

    .line 1141
    iget-object v0, p0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjstateSet:[I

    iget v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjnewStateCnt:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjnewStateCnt:I

    aput p1, v0, v1

    .line 1142
    iget-object v0, p0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjrounds:[I

    iget v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjround:I

    aput v1, v0, p1

    .line 1144
    :cond_0
    return-void
.end method

.method private jjCheckNAddStates(II)V
    .locals 2
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 1160
    :goto_0
    sget-object v1, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjnextStates:[I

    aget v1, v1, p1

    invoke-direct {p0, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAdd(I)V

    .line 1161
    add-int/lit8 v0, p1, 0x1

    .end local p1    # "start":I
    .local v0, "start":I
    if-ne p1, p2, :cond_0

    .line 1162
    return-void

    :cond_0
    move p1, v0

    .end local v0    # "start":I
    .restart local p1    # "start":I
    goto :goto_0
.end method

.method private jjCheckNAddTwoStates(II)V
    .locals 0
    .param p1, "state1"    # I
    .param p2, "state2"    # I

    .prologue
    .line 1153
    invoke-direct {p0, p1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAdd(I)V

    .line 1154
    invoke-direct {p0, p2}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAdd(I)V

    .line 1155
    return-void
.end method

.method private jjMoveNfa_0(II)I
    .locals 20
    .param p1, "startState"    # I
    .param p2, "curPos"    # I

    .prologue
    .line 640
    const/4 v14, 0x0

    .line 641
    .local v14, "startsAt":I
    const/4 v15, 0x3

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjnewStateCnt:I

    .line 642
    const/4 v4, 0x1

    .line 643
    .local v4, "i":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjstateSet:[I

    const/16 v16, 0x0

    aput p1, v15, v16

    .line 644
    const v7, 0x7fffffff

    .line 647
    .local v7, "kind":I
    :goto_0
    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjround:I

    add-int/lit8 v15, v15, 0x1

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjround:I

    const v16, 0x7fffffff

    move/from16 v0, v16

    if-ne v15, v0, :cond_0

    .line 648
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->ReInitRounds()V

    .line 649
    :cond_0
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x40

    move/from16 v0, v16

    if-ge v15, v0, :cond_6

    .line 651
    const-wide/16 v16, 0x1

    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    shl-long v8, v16, v15

    .line 654
    .local v8, "l":J
    :cond_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjstateSet:[I

    add-int/lit8 v4, v4, -0x1

    aget v15, v15, v4

    packed-switch v15, :pswitch_data_0

    .line 676
    :cond_2
    :goto_1
    if-ne v4, v14, :cond_1

    .line 704
    .end local v8    # "l":J
    :goto_2
    const v15, 0x7fffffff

    if-eq v7, v15, :cond_3

    .line 706
    move-object/from16 v0, p0

    iput v7, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjmatchedKind:I

    .line 707
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjmatchedPos:I

    .line 708
    const v7, 0x7fffffff

    .line 710
    :cond_3
    add-int/lit8 p2, p2, 0x1

    .line 711
    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjnewStateCnt:I

    move-object/from16 v0, p0

    iput v14, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjnewStateCnt:I

    rsub-int/lit8 v14, v14, 0x3

    if-ne v4, v14, :cond_a

    .line 714
    :goto_3
    return p2

    .line 657
    .restart local v8    # "l":J
    :pswitch_0
    const-wide/high16 v16, 0x3ff000000000000L

    and-long v16, v16, v8

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_2

    .line 659
    const/16 v15, 0x1b

    if-le v7, v15, :cond_4

    .line 660
    const/16 v7, 0x1b

    .line 661
    :cond_4
    const/16 v15, 0x1f

    const/16 v16, 0x20

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjAddStates(II)V

    goto :goto_1

    .line 664
    :pswitch_1
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x2e

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 665
    const/4 v15, 0x2

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAdd(I)V

    goto :goto_1

    .line 668
    :pswitch_2
    const-wide/high16 v16, 0x3ff000000000000L

    and-long v16, v16, v8

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_2

    .line 670
    const/16 v15, 0x1b

    if-le v7, v15, :cond_5

    .line 671
    const/16 v7, 0x1b

    .line 672
    :cond_5
    const/4 v15, 0x2

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAdd(I)V

    goto :goto_1

    .line 678
    .end local v8    # "l":J
    :cond_6
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x80

    move/from16 v0, v16

    if-ge v15, v0, :cond_8

    .line 680
    const-wide/16 v16, 0x1

    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    and-int/lit8 v15, v15, 0x3f

    shl-long v8, v16, v15

    .line 683
    .restart local v8    # "l":J
    :cond_7
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjstateSet:[I

    add-int/lit8 v4, v4, -0x1

    aget v15, v15, v4

    .line 687
    if-ne v4, v14, :cond_7

    goto/16 :goto_2

    .line 691
    .end local v8    # "l":J
    :cond_8
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    shr-int/lit8 v3, v15, 0x8

    .line 692
    .local v3, "hiByte":I
    shr-int/lit8 v5, v3, 0x6

    .line 693
    .local v5, "i1":I
    const-wide/16 v16, 0x1

    and-int/lit8 v15, v3, 0x3f

    shl-long v10, v16, v15

    .line 694
    .local v10, "l1":J
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    and-int/lit16 v15, v15, 0xff

    shr-int/lit8 v6, v15, 0x6

    .line 695
    .local v6, "i2":I
    const-wide/16 v16, 0x1

    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    and-int/lit8 v15, v15, 0x3f

    shl-long v12, v16, v15

    .line 698
    .local v12, "l2":J
    :cond_9
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjstateSet:[I

    add-int/lit8 v4, v4, -0x1

    aget v15, v15, v4

    .line 702
    if-ne v4, v14, :cond_9

    goto/16 :goto_2

    .line 713
    .end local v3    # "hiByte":I
    .end local v5    # "i1":I
    .end local v6    # "i2":I
    .end local v10    # "l1":J
    .end local v12    # "l2":J
    :cond_a
    :try_start_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/classic/CharStream;

    invoke-interface {v15}, Lorg/apache/lucene/queryparser/classic/CharStream;->readChar()C

    move-result v15

    move-object/from16 v0, p0

    iput-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 714
    :catch_0
    move-exception v2

    .local v2, "e":Ljava/io/IOException;
    goto/16 :goto_3

    .line 654
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private jjMoveNfa_1(II)I
    .locals 20
    .param p1, "startState"    # I
    .param p2, "curPos"    # I

    .prologue
    .line 778
    const/4 v14, 0x0

    .line 779
    .local v14, "startsAt":I
    const/4 v15, 0x7

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjnewStateCnt:I

    .line 780
    const/4 v10, 0x1

    .line 781
    .local v10, "i":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjstateSet:[I

    const/16 v16, 0x0

    aput p1, v15, v16

    .line 782
    const v11, 0x7fffffff

    .line 785
    .local v11, "kind":I
    :goto_0
    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjround:I

    add-int/lit8 v15, v15, 0x1

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjround:I

    const v16, 0x7fffffff

    move/from16 v0, v16

    if-ne v15, v0, :cond_0

    .line 786
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->ReInitRounds()V

    .line 787
    :cond_0
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x40

    move/from16 v0, v16

    if-ge v15, v0, :cond_8

    .line 789
    const-wide/16 v16, 0x1

    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    shl-long v12, v16, v15

    .line 792
    .local v12, "l":J
    :cond_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjstateSet:[I

    add-int/lit8 v10, v10, -0x1

    aget v15, v15, v10

    packed-switch v15, :pswitch_data_0

    .line 834
    :cond_2
    :goto_1
    :pswitch_0
    if-ne v10, v14, :cond_1

    .line 901
    .end local v12    # "l":J
    :goto_2
    const v15, 0x7fffffff

    if-eq v11, v15, :cond_3

    .line 903
    move-object/from16 v0, p0

    iput v11, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjmatchedKind:I

    .line 904
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjmatchedPos:I

    .line 905
    const v11, 0x7fffffff

    .line 907
    :cond_3
    add-int/lit8 p2, p2, 0x1

    .line 908
    move-object/from16 v0, p0

    iget v10, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjnewStateCnt:I

    move-object/from16 v0, p0

    iput v14, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjnewStateCnt:I

    rsub-int/lit8 v14, v14, 0x7

    if-ne v10, v14, :cond_12

    .line 911
    :goto_3
    return p2

    .line 795
    .restart local v12    # "l":J
    :pswitch_1
    const-wide v16, -0x100000001L

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_5

    .line 797
    const/16 v15, 0x20

    if-le v11, v15, :cond_4

    .line 798
    const/16 v11, 0x20

    .line 799
    :cond_4
    const/4 v15, 0x6

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAdd(I)V

    .line 801
    :cond_5
    const-wide v16, 0x100002600L    # 2.122000597E-314

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_6

    .line 803
    const/4 v15, 0x7

    if-le v11, v15, :cond_2

    .line 804
    const/4 v11, 0x7

    .line 805
    goto :goto_1

    .line 806
    :cond_6
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x22

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 807
    const/4 v15, 0x2

    const/16 v16, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto :goto_1

    .line 810
    :pswitch_2
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x22

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 811
    const/4 v15, 0x2

    const/16 v16, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto :goto_1

    .line 814
    :pswitch_3
    const-wide v16, -0x400000001L

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_2

    .line 815
    const/16 v15, 0x21

    const/16 v16, 0x23

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 818
    :pswitch_4
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x22

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 819
    const/16 v15, 0x21

    const/16 v16, 0x23

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 822
    :pswitch_5
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x22

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x1f

    if-le v11, v15, :cond_2

    .line 823
    const/16 v11, 0x1f

    .line 824
    goto/16 :goto_1

    .line 826
    :pswitch_6
    const-wide v16, -0x100000001L

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_2

    .line 828
    const/16 v15, 0x20

    if-le v11, v15, :cond_7

    .line 829
    const/16 v11, 0x20

    .line 830
    :cond_7
    const/4 v15, 0x6

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_1

    .line 836
    .end local v12    # "l":J
    :cond_8
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x80

    move/from16 v0, v16

    if-ge v15, v0, :cond_c

    .line 838
    const-wide/16 v16, 0x1

    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    and-int/lit8 v15, v15, 0x3f

    shl-long v12, v16, v15

    .line 841
    .restart local v12    # "l":J
    :cond_9
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjstateSet:[I

    add-int/lit8 v10, v10, -0x1

    aget v15, v15, v10

    packed-switch v15, :pswitch_data_1

    .line 860
    :cond_a
    :goto_4
    :pswitch_7
    if-ne v10, v14, :cond_9

    goto/16 :goto_2

    .line 845
    :pswitch_8
    const-wide v16, -0x2000000020000001L    # -2.6815614261549933E154

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_a

    .line 847
    const/16 v15, 0x20

    if-le v11, v15, :cond_b

    .line 848
    const/16 v11, 0x20

    .line 849
    :cond_b
    const/4 v15, 0x6

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAdd(I)V

    goto :goto_4

    .line 852
    :pswitch_9
    const/16 v15, 0x21

    const/16 v16, 0x23

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjAddStates(II)V

    goto :goto_4

    .line 855
    :pswitch_a
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x5c

    move/from16 v0, v16

    if-ne v15, v0, :cond_a

    .line 856
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x3

    aput v17, v15, v16

    goto :goto_4

    .line 864
    .end local v12    # "l":J
    :cond_c
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    shr-int/lit8 v3, v15, 0x8

    .line 865
    .local v3, "hiByte":I
    shr-int/lit8 v4, v3, 0x6

    .line 866
    .local v4, "i1":I
    const-wide/16 v16, 0x1

    and-int/lit8 v15, v3, 0x3f

    shl-long v6, v16, v15

    .line 867
    .local v6, "l1":J
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    and-int/lit16 v15, v15, 0xff

    shr-int/lit8 v5, v15, 0x6

    .line 868
    .local v5, "i2":I
    const-wide/16 v16, 0x1

    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    and-int/lit8 v15, v15, 0x3f

    shl-long v8, v16, v15

    .line 871
    .local v8, "l2":J
    :cond_d
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjstateSet:[I

    add-int/lit8 v10, v10, -0x1

    aget v15, v15, v10

    sparse-switch v15, :sswitch_data_0

    .line 899
    :cond_e
    :goto_5
    if-ne v10, v14, :cond_d

    goto/16 :goto_2

    .line 874
    :sswitch_0
    invoke-static/range {v3 .. v9}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCanMove_0(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_f

    .line 876
    const/4 v15, 0x7

    if-le v11, v15, :cond_f

    .line 877
    const/4 v11, 0x7

    .line 879
    :cond_f
    invoke-static/range {v3 .. v9}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCanMove_1(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_e

    .line 881
    const/16 v15, 0x20

    if-le v11, v15, :cond_10

    .line 882
    const/16 v11, 0x20

    .line 883
    :cond_10
    const/4 v15, 0x6

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAdd(I)V

    goto :goto_5

    .line 887
    :sswitch_1
    invoke-static/range {v3 .. v9}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCanMove_1(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_e

    .line 888
    const/16 v15, 0x21

    const/16 v16, 0x23

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjAddStates(II)V

    goto :goto_5

    .line 891
    :sswitch_2
    invoke-static/range {v3 .. v9}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCanMove_1(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_e

    .line 893
    const/16 v15, 0x20

    if-le v11, v15, :cond_11

    .line 894
    const/16 v11, 0x20

    .line 895
    :cond_11
    const/4 v15, 0x6

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAdd(I)V

    goto :goto_5

    .line 910
    .end local v3    # "hiByte":I
    .end local v4    # "i1":I
    .end local v5    # "i2":I
    .end local v6    # "l1":J
    .end local v8    # "l2":J
    :cond_12
    :try_start_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/classic/CharStream;

    invoke-interface {v15}, Lorg/apache/lucene/queryparser/classic/CharStream;->readChar()C

    move-result v15

    move-object/from16 v0, p0

    iput-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 911
    :catch_0
    move-exception v2

    .local v2, "e":Ljava/io/IOException;
    goto/16 :goto_3

    .line 792
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 841
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_8
        :pswitch_7
        :pswitch_9
        :pswitch_7
        :pswitch_a
        :pswitch_7
        :pswitch_8
    .end packed-switch

    .line 871
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x2 -> :sswitch_1
        0x6 -> :sswitch_2
    .end sparse-switch
.end method

.method private jjMoveNfa_2(II)I
    .locals 20
    .param p1, "startState"    # I
    .param p2, "curPos"    # I

    .prologue
    .line 89
    const/4 v14, 0x0

    .line 90
    .local v14, "startsAt":I
    const/16 v15, 0x31

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjnewStateCnt:I

    .line 91
    const/4 v10, 0x1

    .line 92
    .local v10, "i":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjstateSet:[I

    const/16 v16, 0x0

    aput p1, v15, v16

    .line 93
    const v11, 0x7fffffff

    .line 96
    .local v11, "kind":I
    :goto_0
    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjround:I

    add-int/lit8 v15, v15, 0x1

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjround:I

    const v16, 0x7fffffff

    move/from16 v0, v16

    if-ne v15, v0, :cond_0

    .line 97
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->ReInitRounds()V

    .line 98
    :cond_0
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x40

    move/from16 v0, v16

    if-ge v15, v0, :cond_1a

    .line 100
    const-wide/16 v16, 0x1

    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    shl-long v12, v16, v15

    .line 103
    .local v12, "l":J
    :cond_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjstateSet:[I

    add-int/lit8 v10, v10, -0x1

    aget v15, v15, v10

    packed-switch v15, :pswitch_data_0

    .line 284
    :cond_2
    :goto_1
    :pswitch_0
    if-ne v10, v14, :cond_1

    .line 621
    .end local v12    # "l":J
    :goto_2
    const v15, 0x7fffffff

    if-eq v11, v15, :cond_3

    .line 623
    move-object/from16 v0, p0

    iput v11, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjmatchedKind:I

    .line 624
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjmatchedPos:I

    .line 625
    const v11, 0x7fffffff

    .line 627
    :cond_3
    add-int/lit8 p2, p2, 0x1

    .line 628
    move-object/from16 v0, p0

    iget v10, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjnewStateCnt:I

    move-object/from16 v0, p0

    iput v14, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjnewStateCnt:I

    rsub-int/lit8 v14, v14, 0x31

    if-ne v10, v14, :cond_45

    .line 631
    :goto_3
    return p2

    .line 107
    .restart local v12    # "l":J
    :pswitch_1
    const-wide v16, -0x400830700002601L

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_2

    .line 109
    const/16 v15, 0x17

    if-le v11, v15, :cond_4

    .line 110
    const/16 v11, 0x17

    .line 111
    :cond_4
    const/16 v15, 0x21

    const/16 v16, 0x22

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto :goto_1

    .line 114
    :pswitch_2
    const-wide v16, -0x400ab0700002601L

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_9

    .line 116
    const/16 v15, 0x17

    if-le v11, v15, :cond_5

    .line 117
    const/16 v11, 0x17

    .line 118
    :cond_5
    const/16 v15, 0x21

    const/16 v16, 0x22

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    .line 131
    :cond_6
    :goto_4
    const-wide v16, 0x7bff50f8ffffd9ffL    # 1.907419823141542E289

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_d

    .line 133
    const/16 v15, 0x14

    if-le v11, v15, :cond_7

    .line 134
    const/16 v11, 0x14

    .line 135
    :cond_7
    const/4 v15, 0x6

    const/16 v16, 0xa

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddStates(II)V

    .line 147
    :cond_8
    :goto_5
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x26

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 148
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x4

    aput v17, v15, v16

    goto/16 :goto_1

    .line 120
    :cond_9
    const-wide v16, 0x100002600L    # 2.122000597E-314

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_a

    .line 122
    const/4 v15, 0x7

    if-le v11, v15, :cond_6

    .line 123
    const/4 v11, 0x7

    .line 124
    goto :goto_4

    .line 125
    :cond_a
    const-wide v16, 0x280200000000L

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_b

    .line 126
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0xf

    aput v17, v15, v16

    goto :goto_4

    .line 127
    :cond_b
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x2f

    move/from16 v0, v16

    if-ne v15, v0, :cond_c

    .line 128
    const/4 v15, 0x0

    const/16 v16, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_4

    .line 129
    :cond_c
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x22

    move/from16 v0, v16

    if-ne v15, v0, :cond_6

    .line 130
    const/4 v15, 0x3

    const/16 v16, 0x5

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_4

    .line 137
    :cond_d
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x2a

    move/from16 v0, v16

    if-ne v15, v0, :cond_e

    .line 139
    const/16 v15, 0x16

    if-le v11, v15, :cond_8

    .line 140
    const/16 v11, 0x16

    .line 141
    goto/16 :goto_5

    .line 142
    :cond_e
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x21

    move/from16 v0, v16

    if-ne v15, v0, :cond_8

    .line 144
    const/16 v15, 0xa

    if-le v11, v15, :cond_8

    .line 145
    const/16 v11, 0xa

    goto/16 :goto_5

    .line 151
    :pswitch_3
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x26

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x8

    if-le v11, v15, :cond_2

    .line 152
    const/16 v11, 0x8

    .line 153
    goto/16 :goto_1

    .line 155
    :pswitch_4
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x26

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 156
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x4

    aput v17, v15, v16

    goto/16 :goto_1

    .line 159
    :pswitch_5
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x21

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0xa

    if-le v11, v15, :cond_2

    .line 160
    const/16 v11, 0xa

    .line 161
    goto/16 :goto_1

    .line 163
    :pswitch_6
    const-wide v16, 0x280200000000L

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_2

    .line 164
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0xf

    aput v17, v15, v16

    goto/16 :goto_1

    .line 167
    :pswitch_7
    const-wide v16, 0x100002600L    # 2.122000597E-314

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_2

    const/16 v15, 0xd

    if-le v11, v15, :cond_2

    .line 168
    const/16 v11, 0xd

    .line 169
    goto/16 :goto_1

    .line 171
    :pswitch_8
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x22

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 172
    const/4 v15, 0x3

    const/16 v16, 0x5

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 175
    :pswitch_9
    const-wide v16, -0x400000001L

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_2

    .line 176
    const/4 v15, 0x3

    const/16 v16, 0x5

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 179
    :pswitch_a
    const/4 v15, 0x3

    const/16 v16, 0x5

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 182
    :pswitch_b
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x22

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x13

    if-le v11, v15, :cond_2

    .line 183
    const/16 v11, 0x13

    .line 184
    goto/16 :goto_1

    .line 186
    :pswitch_c
    const-wide/high16 v16, 0x3ff000000000000L

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_2

    .line 188
    const/16 v15, 0x15

    if-le v11, v15, :cond_f

    .line 189
    const/16 v11, 0x15

    .line 190
    :cond_f
    const/16 v15, 0xb

    const/16 v16, 0xe

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 193
    :pswitch_d
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x2e

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 194
    const/16 v15, 0x18

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_1

    .line 197
    :pswitch_e
    const-wide/high16 v16, 0x3ff000000000000L

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_2

    .line 199
    const/16 v15, 0x15

    if-le v11, v15, :cond_10

    .line 200
    const/16 v11, 0x15

    .line 201
    :cond_10
    const/16 v15, 0xf

    const/16 v16, 0x11

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 204
    :pswitch_f
    const-wide v16, 0x7bff78f8ffffd9ffL    # 1.9169367313557999E289

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_2

    .line 206
    const/16 v15, 0x15

    if-le v11, v15, :cond_11

    .line 207
    const/16 v11, 0x15

    .line 208
    :cond_11
    const/16 v15, 0x19

    const/16 v16, 0x1a

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 211
    :pswitch_10
    const/16 v15, 0x15

    if-le v11, v15, :cond_12

    .line 212
    const/16 v11, 0x15

    .line 213
    :cond_12
    const/16 v15, 0x19

    const/16 v16, 0x1a

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 216
    :pswitch_11
    const-wide v16, 0x7bff78f8ffffd9ffL    # 1.9169367313557999E289

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_2

    .line 218
    const/16 v15, 0x15

    if-le v11, v15, :cond_13

    .line 219
    const/16 v11, 0x15

    .line 220
    :cond_13
    const/16 v15, 0x1c

    const/16 v16, 0x1d

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 223
    :pswitch_12
    const/16 v15, 0x15

    if-le v11, v15, :cond_14

    .line 224
    const/16 v11, 0x15

    .line 225
    :cond_14
    const/16 v15, 0x1c

    const/16 v16, 0x1d

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 228
    :pswitch_13
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x2a

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x16

    if-le v11, v15, :cond_2

    .line 229
    const/16 v11, 0x16

    .line 230
    goto/16 :goto_1

    .line 232
    :pswitch_14
    const-wide v16, -0x400ab0700002601L

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_2

    .line 234
    const/16 v15, 0x17

    if-le v11, v15, :cond_15

    .line 235
    const/16 v11, 0x17

    .line 236
    :cond_15
    const/16 v15, 0x21

    const/16 v16, 0x22

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 239
    :pswitch_15
    const/16 v15, 0x17

    if-le v11, v15, :cond_16

    .line 240
    const/16 v11, 0x17

    .line 241
    :cond_16
    const/16 v15, 0x21

    const/16 v16, 0x22

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 245
    :pswitch_16
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x2f

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 246
    const/4 v15, 0x0

    const/16 v16, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 249
    :pswitch_17
    const-wide v16, -0x800000000001L

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_2

    .line 250
    const/4 v15, 0x0

    const/16 v16, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 253
    :pswitch_18
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x2f

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x18

    if-le v11, v15, :cond_2

    .line 254
    const/16 v11, 0x18

    .line 255
    goto/16 :goto_1

    .line 257
    :pswitch_19
    const-wide v16, 0x7bff50f8ffffd9ffL    # 1.907419823141542E289

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_2

    .line 259
    const/16 v15, 0x14

    if-le v11, v15, :cond_17

    .line 260
    const/16 v11, 0x14

    .line 261
    :cond_17
    const/4 v15, 0x6

    const/16 v16, 0xa

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 264
    :pswitch_1a
    const-wide v16, 0x7bff78f8ffffd9ffL    # 1.9169367313557999E289

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_2

    .line 266
    const/16 v15, 0x14

    if-le v11, v15, :cond_18

    .line 267
    const/16 v11, 0x14

    .line 268
    :cond_18
    const/16 v15, 0x2a

    const/16 v16, 0x2b

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 271
    :pswitch_1b
    const/16 v15, 0x14

    if-le v11, v15, :cond_19

    .line 272
    const/16 v11, 0x14

    .line 273
    :cond_19
    const/16 v15, 0x2a

    const/16 v16, 0x2b

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 276
    :pswitch_1c
    const-wide v16, 0x7bff78f8ffffd9ffL    # 1.9169367313557999E289

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_2

    .line 277
    const/16 v15, 0x12

    const/16 v16, 0x14

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 280
    :pswitch_1d
    const/16 v15, 0x12

    const/16 v16, 0x14

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 286
    .end local v12    # "l":J
    :cond_1a
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x80

    move/from16 v0, v16

    if-ge v15, v0, :cond_34

    .line 288
    const-wide/16 v16, 0x1

    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    and-int/lit8 v15, v15, 0x3f

    shl-long v12, v16, v15

    .line 291
    .restart local v12    # "l":J
    :cond_1b
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjstateSet:[I

    add-int/lit8 v10, v10, -0x1

    aget v15, v15, v10

    packed-switch v15, :pswitch_data_1

    .line 493
    :cond_1c
    :goto_6
    :pswitch_1e
    if-ne v10, v14, :cond_1b

    goto/16 :goto_2

    .line 294
    :pswitch_1f
    const-wide v16, -0x6800000078000001L    # -4.383617718201673E-193

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_1e

    .line 296
    const/16 v15, 0x17

    if-le v11, v15, :cond_1d

    .line 297
    const/16 v11, 0x17

    .line 298
    :cond_1d
    const/16 v15, 0x21

    const/16 v16, 0x22

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto :goto_6

    .line 300
    :cond_1e
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x5c

    move/from16 v0, v16

    if-ne v15, v0, :cond_1c

    .line 301
    const/16 v15, 0x23

    const/16 v16, 0x23

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto :goto_6

    .line 304
    :pswitch_20
    const-wide v16, -0x6800000078000001L    # -4.383617718201673E-193

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_23

    .line 306
    const/16 v15, 0x14

    if-le v11, v15, :cond_1f

    .line 307
    const/16 v11, 0x14

    .line 308
    :cond_1f
    const/4 v15, 0x6

    const/16 v16, 0xa

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddStates(II)V

    .line 318
    :cond_20
    :goto_7
    const-wide v16, -0x6800000078000001L    # -4.383617718201673E-193

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_22

    .line 320
    const/16 v15, 0x17

    if-le v11, v15, :cond_21

    .line 321
    const/16 v11, 0x17

    .line 322
    :cond_21
    const/16 v15, 0x21

    const/16 v16, 0x22

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    .line 324
    :cond_22
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x4e

    move/from16 v0, v16

    if-ne v15, v0, :cond_26

    .line 325
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0xb

    aput v17, v15, v16

    goto/16 :goto_6

    .line 310
    :cond_23
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x5c

    move/from16 v0, v16

    if-ne v15, v0, :cond_24

    .line 311
    const/16 v15, 0x15

    const/16 v16, 0x17

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto :goto_7

    .line 312
    :cond_24
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x7e

    move/from16 v0, v16

    if-ne v15, v0, :cond_20

    .line 314
    const/16 v15, 0x15

    if-le v11, v15, :cond_25

    .line 315
    const/16 v11, 0x15

    .line 316
    :cond_25
    const/16 v15, 0x18

    const/16 v16, 0x1a

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto :goto_7

    .line 326
    :cond_26
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x7c

    move/from16 v0, v16

    if-ne v15, v0, :cond_27

    .line 327
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x8

    aput v17, v15, v16

    goto/16 :goto_6

    .line 328
    :cond_27
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x4f

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    .line 329
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x6

    aput v17, v15, v16

    goto/16 :goto_6

    .line 330
    :cond_28
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x41

    move/from16 v0, v16

    if-ne v15, v0, :cond_1c

    .line 331
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x2

    aput v17, v15, v16

    goto/16 :goto_6

    .line 334
    :pswitch_21
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x44

    move/from16 v0, v16

    if-ne v15, v0, :cond_1c

    const/16 v15, 0x8

    if-le v11, v15, :cond_1c

    .line 335
    const/16 v11, 0x8

    .line 336
    goto/16 :goto_6

    .line 338
    :pswitch_22
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x4e

    move/from16 v0, v16

    if-ne v15, v0, :cond_1c

    .line 339
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x1

    aput v17, v15, v16

    goto/16 :goto_6

    .line 342
    :pswitch_23
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x41

    move/from16 v0, v16

    if-ne v15, v0, :cond_1c

    .line 343
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x2

    aput v17, v15, v16

    goto/16 :goto_6

    .line 346
    :pswitch_24
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x52

    move/from16 v0, v16

    if-ne v15, v0, :cond_1c

    const/16 v15, 0x9

    if-le v11, v15, :cond_1c

    .line 347
    const/16 v11, 0x9

    .line 348
    goto/16 :goto_6

    .line 350
    :pswitch_25
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x4f

    move/from16 v0, v16

    if-ne v15, v0, :cond_1c

    .line 351
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x6

    aput v17, v15, v16

    goto/16 :goto_6

    .line 354
    :pswitch_26
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x7c

    move/from16 v0, v16

    if-ne v15, v0, :cond_1c

    const/16 v15, 0x9

    if-le v11, v15, :cond_1c

    .line 355
    const/16 v11, 0x9

    .line 356
    goto/16 :goto_6

    .line 358
    :pswitch_27
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x7c

    move/from16 v0, v16

    if-ne v15, v0, :cond_1c

    .line 359
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x8

    aput v17, v15, v16

    goto/16 :goto_6

    .line 362
    :pswitch_28
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x54

    move/from16 v0, v16

    if-ne v15, v0, :cond_1c

    const/16 v15, 0xa

    if-le v11, v15, :cond_1c

    .line 363
    const/16 v11, 0xa

    .line 364
    goto/16 :goto_6

    .line 366
    :pswitch_29
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x4f

    move/from16 v0, v16

    if-ne v15, v0, :cond_1c

    .line 367
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0xa

    aput v17, v15, v16

    goto/16 :goto_6

    .line 370
    :pswitch_2a
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x4e

    move/from16 v0, v16

    if-ne v15, v0, :cond_1c

    .line 371
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0xb

    aput v17, v15, v16

    goto/16 :goto_6

    .line 374
    :pswitch_2b
    const-wide/32 v16, -0x10000001

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_1c

    .line 375
    const/4 v15, 0x3

    const/16 v16, 0x5

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_6

    .line 378
    :pswitch_2c
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x5c

    move/from16 v0, v16

    if-ne v15, v0, :cond_1c

    .line 379
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x13

    aput v17, v15, v16

    goto/16 :goto_6

    .line 382
    :pswitch_2d
    const/4 v15, 0x3

    const/16 v16, 0x5

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_6

    .line 385
    :pswitch_2e
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x7e

    move/from16 v0, v16

    if-ne v15, v0, :cond_1c

    .line 387
    const/16 v15, 0x15

    if-le v11, v15, :cond_29

    .line 388
    const/16 v11, 0x15

    .line 389
    :cond_29
    const/16 v15, 0x18

    const/16 v16, 0x1a

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_6

    .line 392
    :pswitch_2f
    const-wide v16, -0x6800000078000001L    # -4.383617718201673E-193

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_1c

    .line 394
    const/16 v15, 0x15

    if-le v11, v15, :cond_2a

    .line 395
    const/16 v11, 0x15

    .line 396
    :cond_2a
    const/16 v15, 0x19

    const/16 v16, 0x1a

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_6

    .line 399
    :pswitch_30
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x5c

    move/from16 v0, v16

    if-ne v15, v0, :cond_1c

    .line 400
    const/16 v15, 0x1b

    const/16 v16, 0x1c

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjAddStates(II)V

    goto/16 :goto_6

    .line 403
    :pswitch_31
    const/16 v15, 0x15

    if-le v11, v15, :cond_2b

    .line 404
    const/16 v11, 0x15

    .line 405
    :cond_2b
    const/16 v15, 0x19

    const/16 v16, 0x1a

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_6

    .line 408
    :pswitch_32
    const-wide v16, -0x6800000078000001L    # -4.383617718201673E-193

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_1c

    .line 410
    const/16 v15, 0x15

    if-le v11, v15, :cond_2c

    .line 411
    const/16 v11, 0x15

    .line 412
    :cond_2c
    const/16 v15, 0x1c

    const/16 v16, 0x1d

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_6

    .line 415
    :pswitch_33
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x5c

    move/from16 v0, v16

    if-ne v15, v0, :cond_1c

    .line 416
    const/16 v15, 0x1d

    const/16 v16, 0x1e

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjAddStates(II)V

    goto/16 :goto_6

    .line 419
    :pswitch_34
    const/16 v15, 0x15

    if-le v11, v15, :cond_2d

    .line 420
    const/16 v11, 0x15

    .line 421
    :cond_2d
    const/16 v15, 0x1c

    const/16 v16, 0x1d

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_6

    .line 424
    :pswitch_35
    const-wide v16, -0x6800000078000001L    # -4.383617718201673E-193

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_1c

    .line 426
    const/16 v15, 0x17

    if-le v11, v15, :cond_2e

    .line 427
    const/16 v11, 0x17

    .line 428
    :cond_2e
    const/16 v15, 0x21

    const/16 v16, 0x22

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_6

    .line 431
    :pswitch_36
    const-wide v16, -0x6800000078000001L    # -4.383617718201673E-193

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_1c

    .line 433
    const/16 v15, 0x17

    if-le v11, v15, :cond_2f

    .line 434
    const/16 v11, 0x17

    .line 435
    :cond_2f
    const/16 v15, 0x21

    const/16 v16, 0x22

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_6

    .line 438
    :pswitch_37
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x5c

    move/from16 v0, v16

    if-ne v15, v0, :cond_1c

    .line 439
    const/16 v15, 0x23

    const/16 v16, 0x23

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_6

    .line 442
    :pswitch_38
    const/16 v15, 0x17

    if-le v11, v15, :cond_30

    .line 443
    const/16 v11, 0x17

    .line 444
    :cond_30
    const/16 v15, 0x21

    const/16 v16, 0x22

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_6

    .line 447
    :pswitch_39
    const/4 v15, 0x0

    const/16 v16, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjAddStates(II)V

    goto/16 :goto_6

    .line 450
    :pswitch_3a
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x5c

    move/from16 v0, v16

    if-ne v15, v0, :cond_1c

    .line 451
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x26

    aput v17, v15, v16

    goto/16 :goto_6

    .line 454
    :pswitch_3b
    const-wide v16, -0x6800000078000001L    # -4.383617718201673E-193

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_1c

    .line 456
    const/16 v15, 0x14

    if-le v11, v15, :cond_31

    .line 457
    const/16 v11, 0x14

    .line 458
    :cond_31
    const/4 v15, 0x6

    const/16 v16, 0xa

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_6

    .line 461
    :pswitch_3c
    const-wide v16, -0x6800000078000001L    # -4.383617718201673E-193

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_1c

    .line 463
    const/16 v15, 0x14

    if-le v11, v15, :cond_32

    .line 464
    const/16 v11, 0x14

    .line 465
    :cond_32
    const/16 v15, 0x2a

    const/16 v16, 0x2b

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_6

    .line 468
    :pswitch_3d
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x5c

    move/from16 v0, v16

    if-ne v15, v0, :cond_1c

    .line 469
    const/16 v15, 0x2c

    const/16 v16, 0x2c

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_6

    .line 472
    :pswitch_3e
    const/16 v15, 0x14

    if-le v11, v15, :cond_33

    .line 473
    const/16 v11, 0x14

    .line 474
    :cond_33
    const/16 v15, 0x2a

    const/16 v16, 0x2b

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_6

    .line 477
    :pswitch_3f
    const-wide v16, -0x6800000078000001L    # -4.383617718201673E-193

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_1c

    .line 478
    const/16 v15, 0x12

    const/16 v16, 0x14

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_6

    .line 481
    :pswitch_40
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x5c

    move/from16 v0, v16

    if-ne v15, v0, :cond_1c

    .line 482
    const/16 v15, 0x2f

    const/16 v16, 0x2f

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_6

    .line 485
    :pswitch_41
    const/16 v15, 0x12

    const/16 v16, 0x14

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_6

    .line 488
    :pswitch_42
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x5c

    move/from16 v0, v16

    if-ne v15, v0, :cond_1c

    .line 489
    const/16 v15, 0x15

    const/16 v16, 0x17

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_6

    .line 497
    .end local v12    # "l":J
    :cond_34
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    shr-int/lit8 v3, v15, 0x8

    .line 498
    .local v3, "hiByte":I
    shr-int/lit8 v4, v3, 0x6

    .line 499
    .local v4, "i1":I
    const-wide/16 v16, 0x1

    and-int/lit8 v15, v3, 0x3f

    shl-long v6, v16, v15

    .line 500
    .local v6, "l1":J
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    and-int/lit16 v15, v15, 0xff

    shr-int/lit8 v5, v15, 0x6

    .line 501
    .local v5, "i2":I
    const-wide/16 v16, 0x1

    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    and-int/lit8 v15, v15, 0x3f

    shl-long v8, v16, v15

    .line 504
    .local v8, "l2":J
    :cond_35
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjstateSet:[I

    add-int/lit8 v10, v10, -0x1

    aget v15, v15, v10

    sparse-switch v15, :sswitch_data_0

    .line 619
    :cond_36
    :goto_8
    if-ne v10, v14, :cond_35

    goto/16 :goto_2

    .line 508
    :sswitch_0
    invoke-static/range {v3 .. v9}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCanMove_2(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_36

    .line 510
    const/16 v15, 0x17

    if-le v11, v15, :cond_37

    .line 511
    const/16 v11, 0x17

    .line 512
    :cond_37
    const/16 v15, 0x21

    const/16 v16, 0x22

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto :goto_8

    .line 515
    :sswitch_1
    invoke-static/range {v3 .. v9}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCanMove_0(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_38

    .line 517
    const/4 v15, 0x7

    if-le v11, v15, :cond_38

    .line 518
    const/4 v11, 0x7

    .line 520
    :cond_38
    invoke-static/range {v3 .. v9}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCanMove_2(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_3a

    .line 522
    const/16 v15, 0x17

    if-le v11, v15, :cond_39

    .line 523
    const/16 v11, 0x17

    .line 524
    :cond_39
    const/16 v15, 0x21

    const/16 v16, 0x22

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    .line 526
    :cond_3a
    invoke-static/range {v3 .. v9}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCanMove_2(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_36

    .line 528
    const/16 v15, 0x14

    if-le v11, v15, :cond_3b

    .line 529
    const/16 v11, 0x14

    .line 530
    :cond_3b
    const/4 v15, 0x6

    const/16 v16, 0xa

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto :goto_8

    .line 534
    :sswitch_2
    invoke-static/range {v3 .. v9}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCanMove_0(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_36

    const/16 v15, 0xd

    if-le v11, v15, :cond_36

    .line 535
    const/16 v11, 0xd

    .line 536
    goto :goto_8

    .line 539
    :sswitch_3
    invoke-static/range {v3 .. v9}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCanMove_1(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_36

    .line 540
    const/4 v15, 0x3

    const/16 v16, 0x5

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto :goto_8

    .line 543
    :sswitch_4
    invoke-static/range {v3 .. v9}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCanMove_2(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_36

    .line 545
    const/16 v15, 0x15

    if-le v11, v15, :cond_3c

    .line 546
    const/16 v11, 0x15

    .line 547
    :cond_3c
    const/16 v15, 0x19

    const/16 v16, 0x1a

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_8

    .line 550
    :sswitch_5
    invoke-static/range {v3 .. v9}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCanMove_1(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_36

    .line 552
    const/16 v15, 0x15

    if-le v11, v15, :cond_3d

    .line 553
    const/16 v11, 0x15

    .line 554
    :cond_3d
    const/16 v15, 0x19

    const/16 v16, 0x1a

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_8

    .line 557
    :sswitch_6
    invoke-static/range {v3 .. v9}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCanMove_2(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_36

    .line 559
    const/16 v15, 0x15

    if-le v11, v15, :cond_3e

    .line 560
    const/16 v11, 0x15

    .line 561
    :cond_3e
    const/16 v15, 0x1c

    const/16 v16, 0x1d

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_8

    .line 564
    :sswitch_7
    invoke-static/range {v3 .. v9}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCanMove_1(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_36

    .line 566
    const/16 v15, 0x15

    if-le v11, v15, :cond_3f

    .line 567
    const/16 v11, 0x15

    .line 568
    :cond_3f
    const/16 v15, 0x1c

    const/16 v16, 0x1d

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_8

    .line 571
    :sswitch_8
    invoke-static/range {v3 .. v9}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCanMove_2(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_36

    .line 573
    const/16 v15, 0x17

    if-le v11, v15, :cond_40

    .line 574
    const/16 v11, 0x17

    .line 575
    :cond_40
    const/16 v15, 0x21

    const/16 v16, 0x22

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_8

    .line 578
    :sswitch_9
    invoke-static/range {v3 .. v9}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCanMove_1(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_36

    .line 580
    const/16 v15, 0x17

    if-le v11, v15, :cond_41

    .line 581
    const/16 v11, 0x17

    .line 582
    :cond_41
    const/16 v15, 0x21

    const/16 v16, 0x22

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_8

    .line 585
    :sswitch_a
    invoke-static/range {v3 .. v9}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCanMove_1(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_36

    .line 586
    const/4 v15, 0x0

    const/16 v16, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjAddStates(II)V

    goto/16 :goto_8

    .line 589
    :sswitch_b
    invoke-static/range {v3 .. v9}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCanMove_2(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_36

    .line 591
    const/16 v15, 0x14

    if-le v11, v15, :cond_42

    .line 592
    const/16 v11, 0x14

    .line 593
    :cond_42
    const/4 v15, 0x6

    const/16 v16, 0xa

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_8

    .line 596
    :sswitch_c
    invoke-static/range {v3 .. v9}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCanMove_2(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_36

    .line 598
    const/16 v15, 0x14

    if-le v11, v15, :cond_43

    .line 599
    const/16 v11, 0x14

    .line 600
    :cond_43
    const/16 v15, 0x2a

    const/16 v16, 0x2b

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_8

    .line 603
    :sswitch_d
    invoke-static/range {v3 .. v9}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCanMove_1(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_36

    .line 605
    const/16 v15, 0x14

    if-le v11, v15, :cond_44

    .line 606
    const/16 v11, 0x14

    .line 607
    :cond_44
    const/16 v15, 0x2a

    const/16 v16, 0x2b

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_8

    .line 610
    :sswitch_e
    invoke-static/range {v3 .. v9}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCanMove_2(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_36

    .line 611
    const/16 v15, 0x12

    const/16 v16, 0x14

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_8

    .line 614
    :sswitch_f
    invoke-static/range {v3 .. v9}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCanMove_1(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_36

    .line 615
    const/16 v15, 0x12

    const/16 v16, 0x14

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_8

    .line 630
    .end local v3    # "hiByte":I
    .end local v4    # "i1":I
    .end local v5    # "i2":I
    .end local v6    # "l1":J
    .end local v8    # "l2":J
    :cond_45
    :try_start_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/classic/CharStream;

    invoke-interface {v15}, Lorg/apache/lucene/queryparser/classic/CharStream;->readChar()C

    move-result v15

    move-object/from16 v0, p0

    iput-char v15, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 631
    :catch_0
    move-exception v2

    .local v2, "e":Ljava/io/IOException;
    goto/16 :goto_3

    .line 103
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_a
        :pswitch_b
        :pswitch_0
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_0
        :pswitch_10
        :pswitch_11
        :pswitch_0
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_1
        :pswitch_0
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_16
        :pswitch_0
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_0
        :pswitch_1b
        :pswitch_1c
        :pswitch_0
        :pswitch_1d
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 291
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_1e
        :pswitch_1e
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_1e
        :pswitch_1e
        :pswitch_1e
        :pswitch_1e
        :pswitch_2b
        :pswitch_2c
        :pswitch_2d
        :pswitch_1e
        :pswitch_2e
        :pswitch_1e
        :pswitch_1e
        :pswitch_1e
        :pswitch_2f
        :pswitch_30
        :pswitch_31
        :pswitch_32
        :pswitch_33
        :pswitch_34
        :pswitch_1e
        :pswitch_35
        :pswitch_36
        :pswitch_37
        :pswitch_38
        :pswitch_1e
        :pswitch_39
        :pswitch_1e
        :pswitch_3a
        :pswitch_1e
        :pswitch_3b
        :pswitch_3c
        :pswitch_3d
        :pswitch_3e
        :pswitch_3f
        :pswitch_40
        :pswitch_41
        :pswitch_42
        :pswitch_1f
    .end packed-switch

    .line 504
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0xf -> :sswitch_2
        0x11 -> :sswitch_3
        0x13 -> :sswitch_3
        0x19 -> :sswitch_4
        0x1b -> :sswitch_5
        0x1c -> :sswitch_6
        0x1e -> :sswitch_7
        0x20 -> :sswitch_8
        0x21 -> :sswitch_0
        0x23 -> :sswitch_9
        0x25 -> :sswitch_a
        0x29 -> :sswitch_b
        0x2a -> :sswitch_c
        0x2c -> :sswitch_d
        0x2d -> :sswitch_e
        0x2f -> :sswitch_f
        0x31 -> :sswitch_0
    .end sparse-switch
.end method

.method private jjMoveStringLiteralDfa0_0()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 636
    invoke-direct {p0, v0, v0}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjMoveNfa_0(II)I

    move-result v0

    return v0
.end method

.method private jjMoveStringLiteralDfa0_1()I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 738
    iget-char v0, p0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    sparse-switch v0, :sswitch_data_0

    .line 747
    invoke-direct {p0, v1, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjMoveNfa_1(II)I

    move-result v0

    :goto_0
    return v0

    .line 741
    :sswitch_0
    const-wide/32 v0, 0x10000000

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjMoveStringLiteralDfa1_1(J)I

    move-result v0

    goto :goto_0

    .line 743
    :sswitch_1
    const/16 v0, 0x1d

    invoke-direct {p0, v1, v0}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 745
    :sswitch_2
    const/16 v0, 0x1e

    invoke-direct {p0, v1, v0}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 738
    nop

    :sswitch_data_0
    .sparse-switch
        0x54 -> :sswitch_0
        0x5d -> :sswitch_1
        0x7d -> :sswitch_2
    .end sparse-switch
.end method

.method private jjMoveStringLiteralDfa0_2()I
    .locals 3

    .prologue
    const/16 v1, 0xf

    const/4 v2, 0x0

    .line 43
    iget-char v0, p0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    sparse-switch v0, :sswitch_data_0

    .line 64
    invoke-direct {p0, v2, v2}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjMoveNfa_2(II)I

    move-result v0

    :goto_0
    return v0

    .line 46
    :sswitch_0
    const/16 v0, 0xe

    invoke-direct {p0, v2, v0}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 48
    :sswitch_1
    invoke-direct {p0, v2, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 50
    :sswitch_2
    const/16 v0, 0x11

    const/16 v1, 0x31

    invoke-direct {p0, v2, v0, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjStartNfaWithStates_2(III)I

    move-result v0

    goto :goto_0

    .line 52
    :sswitch_3
    const/16 v0, 0xb

    invoke-direct {p0, v2, v0, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjStartNfaWithStates_2(III)I

    move-result v0

    goto :goto_0

    .line 54
    :sswitch_4
    const/16 v0, 0xc

    invoke-direct {p0, v2, v0, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjStartNfaWithStates_2(III)I

    move-result v0

    goto :goto_0

    .line 56
    :sswitch_5
    const/16 v0, 0x10

    invoke-direct {p0, v2, v0}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 58
    :sswitch_6
    const/16 v0, 0x19

    invoke-direct {p0, v2, v0}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 60
    :sswitch_7
    const/16 v0, 0x12

    invoke-direct {p0, v2, v0}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 62
    :sswitch_8
    const/16 v0, 0x1a

    invoke-direct {p0, v2, v0}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 43
    :sswitch_data_0
    .sparse-switch
        0x28 -> :sswitch_0
        0x29 -> :sswitch_1
        0x2a -> :sswitch_2
        0x2b -> :sswitch_3
        0x2d -> :sswitch_4
        0x3a -> :sswitch_5
        0x5b -> :sswitch_6
        0x5e -> :sswitch_7
        0x7b -> :sswitch_8
    .end sparse-switch
.end method

.method private jjMoveStringLiteralDfa1_1(J)I
    .locals 7
    .param p1, "active0"    # J

    .prologue
    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 752
    :try_start_0
    iget-object v2, p0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/classic/CharStream;

    invoke-interface {v2}, Lorg/apache/lucene/queryparser/classic/CharStream;->readChar()C

    move-result v2

    iput-char v2, p0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 757
    iget-char v2, p0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    packed-switch v2, :pswitch_data_0

    .line 766
    :cond_0
    invoke-direct {p0, v6, p1, p2}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjStartNfa_1(IJ)I

    move-result v1

    :goto_0
    return v1

    .line 753
    :catch_0
    move-exception v0

    .line 754
    .local v0, "e":Ljava/io/IOException;
    invoke-direct {p0, v6, p1, p2}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjStopStringLiteralDfa_1(IJ)I

    goto :goto_0

    .line 760
    .end local v0    # "e":Ljava/io/IOException;
    :pswitch_0
    const-wide/32 v2, 0x10000000

    and-long/2addr v2, p1

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 761
    const/16 v2, 0x1c

    const/4 v3, 0x6

    invoke-direct {p0, v1, v2, v3}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjStartNfaWithStates_1(III)I

    move-result v1

    goto :goto_0

    .line 757
    nop

    :pswitch_data_0
    .packed-switch 0x4f
        :pswitch_0
    .end packed-switch
.end method

.method private jjStartNfaWithStates_1(III)I
    .locals 2
    .param p1, "pos"    # I
    .param p2, "kind"    # I
    .param p3, "state"    # I

    .prologue
    .line 770
    iput p2, p0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjmatchedKind:I

    .line 771
    iput p1, p0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjmatchedPos:I

    .line 772
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/classic/CharStream;

    invoke-interface {v1}, Lorg/apache/lucene/queryparser/classic/CharStream;->readChar()C

    move-result v1

    iput-char v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 774
    add-int/lit8 v1, p1, 0x1

    invoke-direct {p0, p3, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjMoveNfa_1(II)I

    move-result v1

    :goto_0
    return v1

    .line 773
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/io/IOException;
    add-int/lit8 v1, p1, 0x1

    goto :goto_0
.end method

.method private jjStartNfaWithStates_2(III)I
    .locals 2
    .param p1, "pos"    # I
    .param p2, "kind"    # I
    .param p3, "state"    # I

    .prologue
    .line 69
    iput p2, p0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjmatchedKind:I

    .line 70
    iput p1, p0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjmatchedPos:I

    .line 71
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/classic/CharStream;

    invoke-interface {v1}, Lorg/apache/lucene/queryparser/classic/CharStream;->readChar()C

    move-result v1

    iput-char v1, p0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 73
    add-int/lit8 v1, p1, 0x1

    invoke-direct {p0, p3, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjMoveNfa_2(II)I

    move-result v1

    :goto_0
    return v1

    .line 72
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/io/IOException;
    add-int/lit8 v1, p1, 0x1

    goto :goto_0
.end method

.method private final jjStartNfa_1(IJ)I
    .locals 2
    .param p1, "pos"    # I
    .param p2, "active0"    # J

    .prologue
    .line 734
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjStopStringLiteralDfa_1(IJ)I

    move-result v0

    add-int/lit8 v1, p1, 0x1

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjMoveNfa_1(II)I

    move-result v0

    return v0
.end method

.method private final jjStartNfa_2(IJ)I
    .locals 2
    .param p1, "pos"    # I
    .param p2, "active0"    # J

    .prologue
    .line 33
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjStopStringLiteralDfa_2(IJ)I

    move-result v0

    add-int/lit8 v1, p1, 0x1

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjMoveNfa_2(II)I

    move-result v0

    return v0
.end method

.method private jjStopAtPos(II)I
    .locals 1
    .param p1, "pos"    # I
    .param p2, "kind"    # I

    .prologue
    .line 37
    iput p2, p0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjmatchedKind:I

    .line 38
    iput p1, p0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjmatchedPos:I

    .line 39
    add-int/lit8 v0, p1, 0x1

    return v0
.end method

.method private final jjStopStringLiteralDfa_1(IJ)I
    .locals 6
    .param p1, "pos"    # I
    .param p2, "active0"    # J

    .prologue
    const/4 v0, -0x1

    .line 719
    packed-switch p1, :pswitch_data_0

    .line 729
    :cond_0
    :goto_0
    return v0

    .line 722
    :pswitch_0
    const-wide/32 v2, 0x10000000

    and-long/2addr v2, p2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 724
    const/16 v0, 0x20

    iput v0, p0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjmatchedKind:I

    .line 725
    const/4 v0, 0x6

    goto :goto_0

    .line 719
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method private final jjStopStringLiteralDfa_2(IJ)I
    .locals 1
    .param p1, "pos"    # I
    .param p2, "active0"    # J

    .prologue
    .line 25
    .line 28
    const/4 v0, -0x1

    return v0
.end method


# virtual methods
.method public ReInit(Lorg/apache/lucene/queryparser/classic/CharStream;)V
    .locals 1
    .param p1, "stream"    # Lorg/apache/lucene/queryparser/classic/CharStream;

    .prologue
    .line 998
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjnewStateCnt:I

    iput v0, p0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjmatchedPos:I

    .line 999
    iget v0, p0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->defaultLexState:I

    iput v0, p0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curLexState:I

    .line 1000
    iput-object p1, p0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/classic/CharStream;

    .line 1001
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->ReInitRounds()V

    .line 1002
    return-void
.end method

.method public ReInit(Lorg/apache/lucene/queryparser/classic/CharStream;I)V
    .locals 0
    .param p1, "stream"    # Lorg/apache/lucene/queryparser/classic/CharStream;
    .param p2, "lexState"    # I

    .prologue
    .line 1014
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->ReInit(Lorg/apache/lucene/queryparser/classic/CharStream;)V

    .line 1015
    invoke-virtual {p0, p2}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->SwitchTo(I)V

    .line 1016
    return-void
.end method

.method public SwitchTo(I)V
    .locals 3
    .param p1, "lexState"    # I

    .prologue
    .line 1021
    const/4 v0, 0x3

    if-ge p1, v0, :cond_0

    if-gez p1, :cond_1

    .line 1022
    :cond_0
    new-instance v0, Lorg/apache/lucene/queryparser/classic/TokenMgrError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Error: Ignoring invalid lexical state : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". State unchanged."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/queryparser/classic/TokenMgrError;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 1024
    :cond_1
    iput p1, p0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curLexState:I

    .line 1025
    return-void
.end method

.method public getNextToken()Lorg/apache/lucene/queryparser/classic/Token;
    .locals 18

    .prologue
    .line 1062
    const/4 v10, 0x0

    .line 1069
    .local v10, "curPos":I
    :cond_0
    :goto_0
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/classic/CharStream;

    invoke-interface {v2}, Lorg/apache/lucene/queryparser/classic/CharStream;->BeginToken()C

    move-result v2

    move-object/from16 v0, p0

    iput-char v2, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1078
    move-object/from16 v0, p0

    iget v2, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curLexState:I

    packed-switch v2, :pswitch_data_0

    .line 1096
    :goto_1
    move-object/from16 v0, p0

    iget v2, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjmatchedKind:I

    const v4, 0x7fffffff

    if-eq v2, v4, :cond_4

    .line 1098
    move-object/from16 v0, p0

    iget v2, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjmatchedPos:I

    add-int/lit8 v2, v2, 0x1

    if-ge v2, v10, :cond_1

    .line 1099
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/classic/CharStream;

    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjmatchedPos:I

    sub-int v4, v10, v4

    add-int/lit8 v4, v4, -0x1

    invoke-interface {v2, v4}, Lorg/apache/lucene/queryparser/classic/CharStream;->backup(I)V

    .line 1100
    :cond_1
    sget-object v2, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjtoToken:[J

    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjmatchedKind:I

    shr-int/lit8 v4, v4, 0x6

    aget-wide v8, v2, v4

    const-wide/16 v16, 0x1

    move-object/from16 v0, p0

    iget v2, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjmatchedKind:I

    and-int/lit8 v2, v2, 0x3f

    shl-long v16, v16, v2

    and-long v8, v8, v16

    const-wide/16 v16, 0x0

    cmp-long v2, v8, v16

    if-eqz v2, :cond_3

    .line 1102
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjFillToken()Lorg/apache/lucene/queryparser/classic/Token;

    move-result-object v13

    .line 1103
    .local v13, "matchedToken":Lorg/apache/lucene/queryparser/classic/Token;
    sget-object v2, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjnewLexState:[I

    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjmatchedKind:I

    aget v2, v2, v4

    const/4 v4, -0x1

    if-eq v2, v4, :cond_2

    .line 1104
    sget-object v2, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjnewLexState:[I

    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjmatchedKind:I

    aget v2, v2, v4

    move-object/from16 v0, p0

    iput v2, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curLexState:I

    :cond_2
    move-object v14, v13

    .line 1105
    .end local v13    # "matchedToken":Lorg/apache/lucene/queryparser/classic/Token;
    .local v14, "matchedToken":Lorg/apache/lucene/queryparser/classic/Token;
    :goto_2
    return-object v14

    .line 1071
    .end local v14    # "matchedToken":Lorg/apache/lucene/queryparser/classic/Token;
    :catch_0
    move-exception v11

    .line 1073
    .local v11, "e":Ljava/io/IOException;
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjmatchedKind:I

    .line 1074
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjFillToken()Lorg/apache/lucene/queryparser/classic/Token;

    move-result-object v13

    .restart local v13    # "matchedToken":Lorg/apache/lucene/queryparser/classic/Token;
    move-object v14, v13

    .line 1075
    .end local v13    # "matchedToken":Lorg/apache/lucene/queryparser/classic/Token;
    .restart local v14    # "matchedToken":Lorg/apache/lucene/queryparser/classic/Token;
    goto :goto_2

    .line 1081
    .end local v11    # "e":Ljava/io/IOException;
    .end local v14    # "matchedToken":Lorg/apache/lucene/queryparser/classic/Token;
    :pswitch_0
    const v2, 0x7fffffff

    move-object/from16 v0, p0

    iput v2, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjmatchedKind:I

    .line 1082
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjmatchedPos:I

    .line 1083
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjMoveStringLiteralDfa0_0()I

    move-result v10

    .line 1084
    goto :goto_1

    .line 1086
    :pswitch_1
    const v2, 0x7fffffff

    move-object/from16 v0, p0

    iput v2, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjmatchedKind:I

    .line 1087
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjmatchedPos:I

    .line 1088
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjMoveStringLiteralDfa0_1()I

    move-result v10

    .line 1089
    goto/16 :goto_1

    .line 1091
    :pswitch_2
    const v2, 0x7fffffff

    move-object/from16 v0, p0

    iput v2, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjmatchedKind:I

    .line 1092
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjmatchedPos:I

    .line 1093
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjMoveStringLiteralDfa0_2()I

    move-result v10

    goto/16 :goto_1

    .line 1109
    :cond_3
    sget-object v2, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjnewLexState:[I

    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjmatchedKind:I

    aget v2, v2, v4

    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    .line 1110
    sget-object v2, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjnewLexState:[I

    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjmatchedKind:I

    aget v2, v2, v4

    move-object/from16 v0, p0

    iput v2, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curLexState:I

    goto/16 :goto_0

    .line 1114
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/classic/CharStream;

    invoke-interface {v2}, Lorg/apache/lucene/queryparser/classic/CharStream;->getEndLine()I

    move-result v5

    .line 1115
    .local v5, "error_line":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/classic/CharStream;

    invoke-interface {v2}, Lorg/apache/lucene/queryparser/classic/CharStream;->getEndColumn()I

    move-result v6

    .line 1116
    .local v6, "error_column":I
    const/4 v7, 0x0

    .line 1117
    .local v7, "error_after":Ljava/lang/String;
    const/4 v3, 0x0

    .line 1118
    .local v3, "EOFSeen":Z
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/classic/CharStream;

    invoke-interface {v2}, Lorg/apache/lucene/queryparser/classic/CharStream;->readChar()C

    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/classic/CharStream;

    const/4 v4, 0x1

    invoke-interface {v2, v4}, Lorg/apache/lucene/queryparser/classic/CharStream;->backup(I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1129
    :goto_3
    if-nez v3, :cond_5

    .line 1130
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/classic/CharStream;

    const/4 v4, 0x1

    invoke-interface {v2, v4}, Lorg/apache/lucene/queryparser/classic/CharStream;->backup(I)V

    .line 1131
    const/4 v2, 0x1

    if-gt v10, v2, :cond_9

    const-string v7, ""

    .line 1133
    :cond_5
    :goto_4
    new-instance v2, Lorg/apache/lucene/queryparser/classic/TokenMgrError;

    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curLexState:I

    move-object/from16 v0, p0

    iget-char v8, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    const/4 v9, 0x0

    invoke-direct/range {v2 .. v9}, Lorg/apache/lucene/queryparser/classic/TokenMgrError;-><init>(ZIIILjava/lang/String;CI)V

    throw v2

    .line 1119
    :catch_1
    move-exception v12

    .line 1120
    .local v12, "e1":Ljava/io/IOException;
    const/4 v3, 0x1

    .line 1121
    const/4 v2, 0x1

    if-gt v10, v2, :cond_7

    const-string v7, ""

    .line 1122
    :goto_5
    move-object/from16 v0, p0

    iget-char v2, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    const/16 v4, 0xa

    if-eq v2, v4, :cond_6

    move-object/from16 v0, p0

    iget-char v2, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->curChar:C

    const/16 v4, 0xd

    if-ne v2, v4, :cond_8

    .line 1123
    :cond_6
    add-int/lit8 v5, v5, 0x1

    .line 1124
    const/4 v6, 0x0

    .line 1125
    goto :goto_3

    .line 1121
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/classic/CharStream;

    invoke-interface {v2}, Lorg/apache/lucene/queryparser/classic/CharStream;->GetImage()Ljava/lang/String;

    move-result-object v7

    goto :goto_5

    .line 1127
    :cond_8
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 1131
    .end local v12    # "e1":Ljava/io/IOException;
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/classic/CharStream;

    invoke-interface {v2}, Lorg/apache/lucene/queryparser/classic/CharStream;->GetImage()Ljava/lang/String;

    move-result-object v7

    goto :goto_4

    .line 1078
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected jjFillToken()Lorg/apache/lucene/queryparser/classic/Token;
    .locals 9

    .prologue
    .line 1035
    sget-object v7, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjstrLiteralImages:[Ljava/lang/String;

    iget v8, p0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjmatchedKind:I

    aget-object v5, v7, v8

    .line 1036
    .local v5, "im":Ljava/lang/String;
    if-nez v5, :cond_0

    iget-object v7, p0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/classic/CharStream;

    invoke-interface {v7}, Lorg/apache/lucene/queryparser/classic/CharStream;->GetImage()Ljava/lang/String;

    move-result-object v2

    .line 1037
    .local v2, "curTokenImage":Ljava/lang/String;
    :goto_0
    iget-object v7, p0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/classic/CharStream;

    invoke-interface {v7}, Lorg/apache/lucene/queryparser/classic/CharStream;->getBeginLine()I

    move-result v1

    .line 1038
    .local v1, "beginLine":I
    iget-object v7, p0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/classic/CharStream;

    invoke-interface {v7}, Lorg/apache/lucene/queryparser/classic/CharStream;->getBeginColumn()I

    move-result v0

    .line 1039
    .local v0, "beginColumn":I
    iget-object v7, p0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/classic/CharStream;

    invoke-interface {v7}, Lorg/apache/lucene/queryparser/classic/CharStream;->getEndLine()I

    move-result v4

    .line 1040
    .local v4, "endLine":I
    iget-object v7, p0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryparser/classic/CharStream;

    invoke-interface {v7}, Lorg/apache/lucene/queryparser/classic/CharStream;->getEndColumn()I

    move-result v3

    .line 1041
    .local v3, "endColumn":I
    iget v7, p0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->jjmatchedKind:I

    invoke-static {v7, v2}, Lorg/apache/lucene/queryparser/classic/Token;->newToken(ILjava/lang/String;)Lorg/apache/lucene/queryparser/classic/Token;

    move-result-object v6

    .line 1043
    .local v6, "t":Lorg/apache/lucene/queryparser/classic/Token;
    iput v1, v6, Lorg/apache/lucene/queryparser/classic/Token;->beginLine:I

    .line 1044
    iput v4, v6, Lorg/apache/lucene/queryparser/classic/Token;->endLine:I

    .line 1045
    iput v0, v6, Lorg/apache/lucene/queryparser/classic/Token;->beginColumn:I

    .line 1046
    iput v3, v6, Lorg/apache/lucene/queryparser/classic/Token;->endColumn:I

    .line 1048
    return-object v6

    .end local v0    # "beginColumn":I
    .end local v1    # "beginLine":I
    .end local v2    # "curTokenImage":Ljava/lang/String;
    .end local v3    # "endColumn":I
    .end local v4    # "endLine":I
    .end local v6    # "t":Lorg/apache/lucene/queryparser/classic/Token;
    :cond_0
    move-object v2, v5

    .line 1036
    goto :goto_0
.end method

.method public setDebugStream(Ljava/io/PrintStream;)V
    .locals 0
    .param p1, "ds"    # Ljava/io/PrintStream;

    .prologue
    .line 22
    iput-object p1, p0, Lorg/apache/lucene/queryparser/classic/QueryParserTokenManager;->debugStream:Ljava/io/PrintStream;

    return-void
.end method
