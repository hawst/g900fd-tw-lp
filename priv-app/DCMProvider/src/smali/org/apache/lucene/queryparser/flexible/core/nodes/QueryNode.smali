.class public interface abstract Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
.super Ljava/lang/Object;
.source "QueryNode.java"


# virtual methods
.method public abstract add(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract add(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)V
.end method

.method public abstract cloneTree()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation
.end method

.method public abstract containsTag(Ljava/lang/String;)Z
.end method

.method public abstract getChildren()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getParent()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
.end method

.method public abstract getTag(Ljava/lang/String;)Ljava/lang/Object;
.end method

.method public abstract getTagMap()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method public abstract isLeaf()Z
.end method

.method public abstract set(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract setTag(Ljava/lang/String;Ljava/lang/Object;)V
.end method

.method public abstract toQueryString(Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;)Ljava/lang/CharSequence;
.end method

.method public abstract toString()Ljava/lang/String;
.end method

.method public abstract unsetTag(Ljava/lang/String;)V
.end method
