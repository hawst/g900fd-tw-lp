.class public Lorg/apache/lucene/queryparser/flexible/precedence/PrecedenceQueryParser;
.super Lorg/apache/lucene/queryparser/flexible/standard/StandardQueryParser;
.source "PrecedenceQueryParser.java"


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 43
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/standard/StandardQueryParser;-><init>()V

    .line 44
    new-instance v0, Lorg/apache/lucene/queryparser/flexible/precedence/processors/PrecedenceQueryNodeProcessorPipeline;

    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/precedence/PrecedenceQueryParser;->getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/lucene/queryparser/flexible/precedence/processors/PrecedenceQueryNodeProcessorPipeline;-><init>(Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;)V

    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/flexible/precedence/PrecedenceQueryParser;->setQueryNodeProcessor(Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;)V

    .line 45
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/analysis/Analyzer;)V
    .locals 2
    .param p1, "analyer"    # Lorg/apache/lucene/analysis/Analyzer;

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lorg/apache/lucene/queryparser/flexible/standard/StandardQueryParser;-><init>(Lorg/apache/lucene/analysis/Analyzer;)V

    .line 53
    new-instance v0, Lorg/apache/lucene/queryparser/flexible/precedence/processors/PrecedenceQueryNodeProcessorPipeline;

    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/precedence/PrecedenceQueryParser;->getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/lucene/queryparser/flexible/precedence/processors/PrecedenceQueryNodeProcessorPipeline;-><init>(Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;)V

    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryparser/flexible/precedence/PrecedenceQueryParser;->setQueryNodeProcessor(Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;)V

    .line 55
    return-void
.end method
