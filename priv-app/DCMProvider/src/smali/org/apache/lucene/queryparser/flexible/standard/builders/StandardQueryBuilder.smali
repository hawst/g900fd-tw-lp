.class public interface abstract Lorg/apache/lucene/queryparser/flexible/standard/builders/StandardQueryBuilder;
.super Ljava/lang/Object;
.source "StandardQueryBuilder.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/flexible/core/builders/QueryBuilder;


# virtual methods
.method public abstract build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/search/Query;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation
.end method
