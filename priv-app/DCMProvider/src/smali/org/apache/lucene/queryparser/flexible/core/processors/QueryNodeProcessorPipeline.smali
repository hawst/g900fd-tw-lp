.class public Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorPipeline;
.super Ljava/lang/Object;
.source "QueryNodeProcessorPipeline.java"

# interfaces
.implements Ljava/util/List;
.implements Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;",
        "Ljava/util/List",
        "<",
        "Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;",
        ">;"
    }
.end annotation


# instance fields
.field private processors:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;",
            ">;"
        }
    .end annotation
.end field

.field private queryConfig:Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorPipeline;->processors:Ljava/util/LinkedList;

    .line 52
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;)V
    .locals 1
    .param p1, "queryConfigHandler"    # Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorPipeline;->processors:Ljava/util/LinkedList;

    .line 58
    iput-object p1, p0, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorPipeline;->queryConfig:Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    .line 59
    return-void
.end method


# virtual methods
.method public bridge synthetic add(ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p2, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;

    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorPipeline;->add(ILorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;)V

    return-void
.end method

.method public add(ILorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;)V
    .locals 1
    .param p1, "index"    # I
    .param p2, "processor"    # Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;

    .prologue
    .line 136
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorPipeline;->processors:Ljava/util/LinkedList;

    invoke-virtual {v0, p1, p2}, Ljava/util/LinkedList;->add(ILjava/lang/Object;)V

    .line 137
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorPipeline;->queryConfig:Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    invoke-interface {p2, v0}, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;->setQueryConfigHandler(Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;)V

    .line 139
    return-void
.end method

.method public bridge synthetic add(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;

    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorPipeline;->add(Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;)Z

    move-result v0

    return v0
.end method

.method public add(Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;)Z
    .locals 2
    .param p1, "processor"    # Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;

    .prologue
    .line 121
    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorPipeline;->processors:Ljava/util/LinkedList;

    invoke-virtual {v1, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    move-result v0

    .line 123
    .local v0, "added":Z
    if-eqz v0, :cond_0

    .line 124
    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorPipeline;->queryConfig:Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    invoke-interface {p1, v1}, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;->setQueryConfigHandler(Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;)V

    .line 127
    :cond_0
    return v0
.end method

.method public addAll(ILjava/util/Collection;)Z
    .locals 4
    .param p1, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Collection",
            "<+",
            "Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 161
    .local p2, "c":Ljava/util/Collection;, "Ljava/util/Collection<+Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;>;"
    iget-object v2, p0, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorPipeline;->processors:Ljava/util/LinkedList;

    invoke-virtual {v2, p1, p2}, Ljava/util/LinkedList;->addAll(ILjava/util/Collection;)Z

    move-result v0

    .line 163
    .local v0, "anyAdded":Z
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 167
    return v0

    .line 163
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;

    .line 164
    .local v1, "processor":Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;
    iget-object v3, p0, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorPipeline;->queryConfig:Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    invoke-interface {v1, v3}, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;->setQueryConfigHandler(Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;)V

    goto :goto_0
.end method

.method public addAll(Ljava/util/Collection;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+",
            "Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 146
    .local p1, "c":Ljava/util/Collection;, "Ljava/util/Collection<+Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;>;"
    iget-object v2, p0, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorPipeline;->processors:Ljava/util/LinkedList;

    invoke-virtual {v2, p1}, Ljava/util/LinkedList;->addAll(Ljava/util/Collection;)Z

    move-result v0

    .line 148
    .local v0, "anyAdded":Z
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 152
    return v0

    .line 148
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;

    .line 149
    .local v1, "processor":Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;
    iget-object v3, p0, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorPipeline;->queryConfig:Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    invoke-interface {v1, v3}, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;->setQueryConfigHandler(Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;)V

    goto :goto_0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorPipeline;->processors:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 177
    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 184
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorPipeline;->processors:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public containsAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 192
    .local p1, "c":Ljava/util/Collection;, "Ljava/util/Collection<*>;"
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorPipeline;->processors:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic get(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorPipeline;->get(I)Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;

    move-result-object v0

    return-object v0
.end method

.method public get(I)Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 200
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorPipeline;->processors:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;

    return-object v0
.end method

.method public getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorPipeline;->queryConfig:Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    return-object v0
.end method

.method public indexOf(Ljava/lang/Object;)I
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 208
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorPipeline;->processors:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorPipeline;->processors:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 224
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorPipeline;->processors:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public lastIndexOf(Ljava/lang/Object;)I
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 232
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorPipeline;->processors:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->lastIndexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public listIterator()Ljava/util/ListIterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ListIterator",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 240
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorPipeline;->processors:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->listIterator()Ljava/util/ListIterator;

    move-result-object v0

    return-object v0
.end method

.method public listIterator(I)Ljava/util/ListIterator;
    .locals 1
    .param p1, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ListIterator",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 248
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorPipeline;->processors:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v0

    return-object v0
.end method

.method public process(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 3
    .param p1, "queryTree"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 89
    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorPipeline;->processors:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 93
    return-object p1

    .line 89
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;

    .line 90
    .local v0, "processor":Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;
    invoke-interface {v0, p1}, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;->process(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object p1

    goto :goto_0
.end method

.method public bridge synthetic remove(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorPipeline;->remove(I)Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;

    move-result-object v0

    return-object v0
.end method

.method public remove(I)Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 264
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorPipeline;->processors:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 256
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorPipeline;->processors:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public removeAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 272
    .local p1, "c":Ljava/util/Collection;, "Ljava/util/Collection<*>;"
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorPipeline;->processors:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->removeAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public retainAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 280
    .local p1, "c":Ljava/util/Collection;, "Ljava/util/Collection<*>;"
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorPipeline;->processors:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->retainAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic set(ILjava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p2, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;

    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorPipeline;->set(ILorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;)Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;

    move-result-object v0

    return-object v0
.end method

.method public set(ILorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;)Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;
    .locals 2
    .param p1, "index"    # I
    .param p2, "processor"    # Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;

    .prologue
    .line 288
    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorPipeline;->processors:Ljava/util/LinkedList;

    invoke-virtual {v1, p1, p2}, Ljava/util/LinkedList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;

    .line 290
    .local v0, "oldProcessor":Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;
    if-eq v0, p2, :cond_0

    .line 291
    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorPipeline;->queryConfig:Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    invoke-interface {p2, v1}, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;->setQueryConfigHandler(Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;)V

    .line 294
    :cond_0
    return-object v0
.end method

.method public setQueryConfigHandler(Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;)V
    .locals 3
    .param p1, "queryConfigHandler"    # Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    .prologue
    .line 108
    iput-object p1, p0, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorPipeline;->queryConfig:Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    .line 110
    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorPipeline;->processors:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 114
    return-void

    .line 110
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;

    .line 111
    .local v0, "processor":Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;
    iget-object v2, p0, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorPipeline;->queryConfig:Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    invoke-interface {v0, v2}, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;->setQueryConfigHandler(Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;)V

    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 303
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorPipeline;->processors:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    return v0
.end method

.method public subList(II)Ljava/util/List;
    .locals 1
    .param p1, "fromIndex"    # I
    .param p2, "toIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 311
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorPipeline;->processors:Ljava/util/LinkedList;

    invoke-virtual {v0, p1, p2}, Ljava/util/LinkedList;->subList(II)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public toArray()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 327
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorPipeline;->processors:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->toArray()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1
    .param p1, "array"    # [Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;)[TT;"
        }
    .end annotation

    .prologue
    .line 319
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorPipeline;->processors:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
