.class public Lorg/apache/lucene/queryparser/flexible/standard/builders/StandardQueryTreeBuilder;
.super Lorg/apache/lucene/queryparser/flexible/core/builders/QueryTreeBuilder;
.source "StandardQueryTreeBuilder.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/flexible/standard/builders/StandardQueryBuilder;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 56
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/core/builders/QueryTreeBuilder;-><init>()V

    .line 57
    const-class v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/GroupQueryNode;

    new-instance v1, Lorg/apache/lucene/queryparser/flexible/standard/builders/GroupQueryNodeBuilder;

    invoke-direct {v1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/GroupQueryNodeBuilder;-><init>()V

    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/StandardQueryTreeBuilder;->setBuilder(Ljava/lang/Class;Lorg/apache/lucene/queryparser/flexible/core/builders/QueryBuilder;)V

    .line 58
    const-class v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;

    new-instance v1, Lorg/apache/lucene/queryparser/flexible/standard/builders/FieldQueryNodeBuilder;

    invoke-direct {v1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/FieldQueryNodeBuilder;-><init>()V

    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/StandardQueryTreeBuilder;->setBuilder(Ljava/lang/Class;Lorg/apache/lucene/queryparser/flexible/core/builders/QueryBuilder;)V

    .line 59
    const-class v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/BooleanQueryNode;

    new-instance v1, Lorg/apache/lucene/queryparser/flexible/standard/builders/BooleanQueryNodeBuilder;

    invoke-direct {v1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/BooleanQueryNodeBuilder;-><init>()V

    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/StandardQueryTreeBuilder;->setBuilder(Ljava/lang/Class;Lorg/apache/lucene/queryparser/flexible/core/builders/QueryBuilder;)V

    .line 60
    const-class v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FuzzyQueryNode;

    new-instance v1, Lorg/apache/lucene/queryparser/flexible/standard/builders/FuzzyQueryNodeBuilder;

    invoke-direct {v1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/FuzzyQueryNodeBuilder;-><init>()V

    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/StandardQueryTreeBuilder;->setBuilder(Ljava/lang/Class;Lorg/apache/lucene/queryparser/flexible/core/builders/QueryBuilder;)V

    .line 61
    const-class v0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericQueryNode;

    new-instance v1, Lorg/apache/lucene/queryparser/flexible/standard/builders/DummyQueryNodeBuilder;

    invoke-direct {v1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/DummyQueryNodeBuilder;-><init>()V

    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/StandardQueryTreeBuilder;->setBuilder(Ljava/lang/Class;Lorg/apache/lucene/queryparser/flexible/core/builders/QueryBuilder;)V

    .line 62
    const-class v0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/NumericRangeQueryNode;

    new-instance v1, Lorg/apache/lucene/queryparser/flexible/standard/builders/NumericRangeQueryNodeBuilder;

    invoke-direct {v1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/NumericRangeQueryNodeBuilder;-><init>()V

    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/StandardQueryTreeBuilder;->setBuilder(Ljava/lang/Class;Lorg/apache/lucene/queryparser/flexible/core/builders/QueryBuilder;)V

    .line 63
    const-class v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/BoostQueryNode;

    new-instance v1, Lorg/apache/lucene/queryparser/flexible/standard/builders/BoostQueryNodeBuilder;

    invoke-direct {v1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/BoostQueryNodeBuilder;-><init>()V

    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/StandardQueryTreeBuilder;->setBuilder(Ljava/lang/Class;Lorg/apache/lucene/queryparser/flexible/core/builders/QueryBuilder;)V

    .line 64
    const-class v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;

    new-instance v1, Lorg/apache/lucene/queryparser/flexible/standard/builders/ModifierQueryNodeBuilder;

    invoke-direct {v1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/ModifierQueryNodeBuilder;-><init>()V

    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/StandardQueryTreeBuilder;->setBuilder(Ljava/lang/Class;Lorg/apache/lucene/queryparser/flexible/core/builders/QueryBuilder;)V

    .line 65
    const-class v0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/WildcardQueryNode;

    new-instance v1, Lorg/apache/lucene/queryparser/flexible/standard/builders/WildcardQueryNodeBuilder;

    invoke-direct {v1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/WildcardQueryNodeBuilder;-><init>()V

    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/StandardQueryTreeBuilder;->setBuilder(Ljava/lang/Class;Lorg/apache/lucene/queryparser/flexible/core/builders/QueryBuilder;)V

    .line 66
    const-class v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/TokenizedPhraseQueryNode;

    new-instance v1, Lorg/apache/lucene/queryparser/flexible/standard/builders/PhraseQueryNodeBuilder;

    invoke-direct {v1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/PhraseQueryNodeBuilder;-><init>()V

    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/StandardQueryTreeBuilder;->setBuilder(Ljava/lang/Class;Lorg/apache/lucene/queryparser/flexible/core/builders/QueryBuilder;)V

    .line 67
    const-class v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/MatchNoDocsQueryNode;

    new-instance v1, Lorg/apache/lucene/queryparser/flexible/standard/builders/MatchNoDocsQueryNodeBuilder;

    invoke-direct {v1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/MatchNoDocsQueryNodeBuilder;-><init>()V

    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/StandardQueryTreeBuilder;->setBuilder(Ljava/lang/Class;Lorg/apache/lucene/queryparser/flexible/core/builders/QueryBuilder;)V

    .line 68
    const-class v0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/PrefixWildcardQueryNode;

    .line 69
    new-instance v1, Lorg/apache/lucene/queryparser/flexible/standard/builders/PrefixWildcardQueryNodeBuilder;

    invoke-direct {v1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/PrefixWildcardQueryNodeBuilder;-><init>()V

    .line 68
    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/StandardQueryTreeBuilder;->setBuilder(Ljava/lang/Class;Lorg/apache/lucene/queryparser/flexible/core/builders/QueryBuilder;)V

    .line 70
    const-class v0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/TermRangeQueryNode;

    new-instance v1, Lorg/apache/lucene/queryparser/flexible/standard/builders/TermRangeQueryNodeBuilder;

    invoke-direct {v1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/TermRangeQueryNodeBuilder;-><init>()V

    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/StandardQueryTreeBuilder;->setBuilder(Ljava/lang/Class;Lorg/apache/lucene/queryparser/flexible/core/builders/QueryBuilder;)V

    .line 71
    const-class v0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/RegexpQueryNode;

    new-instance v1, Lorg/apache/lucene/queryparser/flexible/standard/builders/RegexpQueryNodeBuilder;

    invoke-direct {v1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/RegexpQueryNodeBuilder;-><init>()V

    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/StandardQueryTreeBuilder;->setBuilder(Ljava/lang/Class;Lorg/apache/lucene/queryparser/flexible/core/builders/QueryBuilder;)V

    .line 72
    const-class v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/SlopQueryNode;

    new-instance v1, Lorg/apache/lucene/queryparser/flexible/standard/builders/SlopQueryNodeBuilder;

    invoke-direct {v1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/SlopQueryNodeBuilder;-><init>()V

    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/StandardQueryTreeBuilder;->setBuilder(Ljava/lang/Class;Lorg/apache/lucene/queryparser/flexible/core/builders/QueryBuilder;)V

    .line 73
    const-class v0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/StandardBooleanQueryNode;

    .line 74
    new-instance v1, Lorg/apache/lucene/queryparser/flexible/standard/builders/StandardBooleanQueryNodeBuilder;

    invoke-direct {v1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/StandardBooleanQueryNodeBuilder;-><init>()V

    .line 73
    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/StandardQueryTreeBuilder;->setBuilder(Ljava/lang/Class;Lorg/apache/lucene/queryparser/flexible/core/builders/QueryBuilder;)V

    .line 75
    const-class v0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/MultiPhraseQueryNode;

    new-instance v1, Lorg/apache/lucene/queryparser/flexible/standard/builders/MultiPhraseQueryNodeBuilder;

    invoke-direct {v1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/MultiPhraseQueryNodeBuilder;-><init>()V

    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/StandardQueryTreeBuilder;->setBuilder(Ljava/lang/Class;Lorg/apache/lucene/queryparser/flexible/core/builders/QueryBuilder;)V

    .line 76
    const-class v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/MatchAllDocsQueryNode;

    new-instance v1, Lorg/apache/lucene/queryparser/flexible/standard/builders/MatchAllDocsQueryNodeBuilder;

    invoke-direct {v1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/MatchAllDocsQueryNodeBuilder;-><init>()V

    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/StandardQueryTreeBuilder;->setBuilder(Ljava/lang/Class;Lorg/apache/lucene/queryparser/flexible/core/builders/QueryBuilder;)V

    .line 78
    return-void
.end method


# virtual methods
.method public bridge synthetic build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/StandardQueryTreeBuilder;->build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/search/Query;
    .locals 1
    .param p1, "queryNode"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 82
    invoke-super {p0, p1}, Lorg/apache/lucene/queryparser/flexible/core/builders/QueryTreeBuilder;->build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/Query;

    return-object v0
.end method
