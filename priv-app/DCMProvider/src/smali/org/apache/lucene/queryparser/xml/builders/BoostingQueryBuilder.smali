.class public Lorg/apache/lucene/queryparser/xml/builders/BoostingQueryBuilder;
.super Ljava/lang/Object;
.source "BoostingQueryBuilder.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/xml/QueryBuilder;


# static fields
.field private static DEFAULT_BOOST:F


# instance fields
.field private final factory:Lorg/apache/lucene/queryparser/xml/QueryBuilder;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const v0, 0x3c23d70a    # 0.01f

    sput v0, Lorg/apache/lucene/queryparser/xml/builders/BoostingQueryBuilder;->DEFAULT_BOOST:F

    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/queryparser/xml/QueryBuilder;)V
    .locals 0
    .param p1, "factory"    # Lorg/apache/lucene/queryparser/xml/QueryBuilder;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lorg/apache/lucene/queryparser/xml/builders/BoostingQueryBuilder;->factory:Lorg/apache/lucene/queryparser/xml/QueryBuilder;

    .line 37
    return-void
.end method


# virtual methods
.method public getQuery(Lorg/w3c/dom/Element;)Lorg/apache/lucene/search/Query;
    .locals 8
    .param p1, "e"    # Lorg/w3c/dom/Element;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/xml/ParserException;
        }
    .end annotation

    .prologue
    .line 41
    const-string v6, "Query"

    invoke-static {p1, v6}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getChildByTagOrFail(Lorg/w3c/dom/Element;Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v5

    .line 42
    .local v5, "mainQueryElem":Lorg/w3c/dom/Element;
    invoke-static {v5}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getFirstChildOrFail(Lorg/w3c/dom/Element;)Lorg/w3c/dom/Element;

    move-result-object v5

    .line 43
    iget-object v6, p0, Lorg/apache/lucene/queryparser/xml/builders/BoostingQueryBuilder;->factory:Lorg/apache/lucene/queryparser/xml/QueryBuilder;

    invoke-interface {v6, v5}, Lorg/apache/lucene/queryparser/xml/QueryBuilder;->getQuery(Lorg/w3c/dom/Element;)Lorg/apache/lucene/search/Query;

    move-result-object v4

    .line 45
    .local v4, "mainQuery":Lorg/apache/lucene/search/Query;
    const-string v6, "BoostQuery"

    invoke-static {p1, v6}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getChildByTagOrFail(Lorg/w3c/dom/Element;Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v2

    .line 46
    .local v2, "boostQueryElem":Lorg/w3c/dom/Element;
    const-string v6, "boost"

    sget v7, Lorg/apache/lucene/queryparser/xml/builders/BoostingQueryBuilder;->DEFAULT_BOOST:F

    invoke-static {v2, v6, v7}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttribute(Lorg/w3c/dom/Element;Ljava/lang/String;F)F

    move-result v0

    .line 47
    .local v0, "boost":F
    invoke-static {v2}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getFirstChildOrFail(Lorg/w3c/dom/Element;)Lorg/w3c/dom/Element;

    move-result-object v2

    .line 48
    iget-object v6, p0, Lorg/apache/lucene/queryparser/xml/builders/BoostingQueryBuilder;->factory:Lorg/apache/lucene/queryparser/xml/QueryBuilder;

    invoke-interface {v6, v2}, Lorg/apache/lucene/queryparser/xml/QueryBuilder;->getQuery(Lorg/w3c/dom/Element;)Lorg/apache/lucene/search/Query;

    move-result-object v1

    .line 50
    .local v1, "boostQuery":Lorg/apache/lucene/search/Query;
    new-instance v3, Lorg/apache/lucene/queries/BoostingQuery;

    invoke-direct {v3, v4, v1, v0}, Lorg/apache/lucene/queries/BoostingQuery;-><init>(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Query;F)V

    .line 52
    .local v3, "bq":Lorg/apache/lucene/queries/BoostingQuery;
    const-string v6, "boost"

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-static {p1, v6, v7}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttribute(Lorg/w3c/dom/Element;Ljava/lang/String;F)F

    move-result v6

    invoke-virtual {v3, v6}, Lorg/apache/lucene/queries/BoostingQuery;->setBoost(F)V

    .line 53
    return-object v3
.end method
