.class public Lorg/apache/lucene/queryparser/flexible/standard/nodes/StandardBooleanQueryNode;
.super Lorg/apache/lucene/queryparser/flexible/core/nodes/BooleanQueryNode;
.source "StandardBooleanQueryNode.java"


# instance fields
.field private disableCoord:Z


# direct methods
.method public constructor <init>(Ljava/util/List;Z)V
    .locals 0
    .param p2, "disableCoord"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 40
    .local p1, "clauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/BooleanQueryNode;-><init>(Ljava/util/List;)V

    .line 42
    iput-boolean p2, p0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/StandardBooleanQueryNode;->disableCoord:Z

    .line 44
    return-void
.end method


# virtual methods
.method public isDisableCoord()Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/StandardBooleanQueryNode;->disableCoord:Z

    return v0
.end method
