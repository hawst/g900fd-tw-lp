.class public Lorg/apache/lucene/queryparser/surround/query/OrQuery;
.super Lorg/apache/lucene/queryparser/surround/query/ComposedQuery;
.source "OrQuery.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/surround/query/DistanceSubQuery;


# direct methods
.method public constructor <init>(Ljava/util/List;ZLjava/lang/String;)V
    .locals 0
    .param p2, "infix"    # Z
    .param p3, "opName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/surround/query/SrndQuery;",
            ">;Z",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 31
    .local p1, "queries":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/surround/query/SrndQuery;>;"
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/queryparser/surround/query/ComposedQuery;-><init>(Ljava/util/List;ZLjava/lang/String;)V

    .line 32
    return-void
.end method


# virtual methods
.method public addSpanQueries(Lorg/apache/lucene/queryparser/surround/query/SpanNearClauseFactory;)V
    .locals 2
    .param p1, "sncf"    # Lorg/apache/lucene/queryparser/surround/query/SpanNearClauseFactory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 60
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/query/OrQuery;->getSubQueriesIterator()Ljava/util/Iterator;

    move-result-object v0

    .line 61
    .local v0, "sqi":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 64
    return-void

    .line 62
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/queryparser/surround/query/DistanceSubQuery;

    invoke-interface {v1, p1}, Lorg/apache/lucene/queryparser/surround/query/DistanceSubQuery;->addSpanQueries(Lorg/apache/lucene/queryparser/surround/query/SpanNearClauseFactory;)V

    goto :goto_0
.end method

.method public distanceSubQueryNotAllowed()Ljava/lang/String;
    .locals 5

    .prologue
    .line 43
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/query/OrQuery;->getSubQueriesIterator()Ljava/util/Iterator;

    move-result-object v2

    .line 44
    .local v2, "sqi":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 55
    const/4 v1, 0x0

    :goto_0
    return-object v1

    .line 45
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/surround/query/SrndQuery;

    .line 46
    .local v0, "leq":Lorg/apache/lucene/queryparser/surround/query/SrndQuery;
    instance-of v3, v0, Lorg/apache/lucene/queryparser/surround/query/DistanceSubQuery;

    if-eqz v3, :cond_2

    .line 47
    check-cast v0, Lorg/apache/lucene/queryparser/surround/query/DistanceSubQuery;

    .end local v0    # "leq":Lorg/apache/lucene/queryparser/surround/query/SrndQuery;
    invoke-interface {v0}, Lorg/apache/lucene/queryparser/surround/query/DistanceSubQuery;->distanceSubQueryNotAllowed()Ljava/lang/String;

    move-result-object v1

    .line 48
    .local v1, "m":Ljava/lang/String;
    if-eqz v1, :cond_0

    goto :goto_0

    .line 52
    .end local v1    # "m":Ljava/lang/String;
    .restart local v0    # "leq":Lorg/apache/lucene/queryparser/surround/query/SrndQuery;
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "subquery not allowed: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/apache/lucene/queryparser/surround/query/SrndQuery;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public makeLuceneQueryFieldNoBoost(Ljava/lang/String;Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;)Lorg/apache/lucene/search/Query;
    .locals 2
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "qf"    # Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;

    .prologue
    .line 38
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/queryparser/surround/query/OrQuery;->makeLuceneSubQueriesField(Ljava/lang/String;Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;)Ljava/util/List;

    move-result-object v0

    sget-object v1, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    .line 36
    invoke-static {v0, v1}, Lorg/apache/lucene/queryparser/surround/query/SrndBooleanQuery;->makeBooleanQuery(Ljava/util/List;Lorg/apache/lucene/search/BooleanClause$Occur;)Lorg/apache/lucene/search/Query;

    move-result-object v0

    return-object v0
.end method
