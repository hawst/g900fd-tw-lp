.class public Lorg/apache/lucene/queryparser/flexible/precedence/processors/BooleanModifiersQueryNodeProcessor;
.super Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl;
.source "BooleanModifiersQueryNodeProcessor.java"


# instance fields
.field private childrenBuffer:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;",
            ">;"
        }
    .end annotation
.end field

.field private usingAnd:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl;-><init>()V

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/queryparser/flexible/precedence/processors/BooleanModifiersQueryNodeProcessor;->childrenBuffer:Ljava/util/ArrayList;

    .line 51
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/queryparser/flexible/precedence/processors/BooleanModifiersQueryNodeProcessor;->usingAnd:Ljava/lang/Boolean;

    .line 55
    return-void
.end method

.method private applyModifier(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 3
    .param p1, "node"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .param p2, "mod"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    .prologue
    .line 106
    instance-of v1, p1, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;

    if-nez v1, :cond_1

    .line 107
    new-instance v1, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;

    invoke-direct {v1, p1, p2}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;-><init>(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;)V

    move-object p1, v1

    .line 118
    .end local p1    # "node":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    :cond_0
    :goto_0
    return-object p1

    .restart local p1    # "node":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    :cond_1
    move-object v0, p1

    .line 110
    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;

    .line 112
    .local v0, "modNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;
    invoke-virtual {v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;->getModifier()Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    move-result-object v1

    sget-object v2, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;->MOD_NONE:Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    if-ne v1, v2, :cond_0

    .line 113
    new-instance p1, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;

    .end local p1    # "node":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    invoke-virtual {v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;->getChild()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v1

    invoke-direct {p1, v1, p2}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode;-><init>(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;)V

    goto :goto_0
.end method


# virtual methods
.method protected postProcessNode(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 5
    .param p1, "node"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 75
    instance-of v2, p1, Lorg/apache/lucene/queryparser/flexible/core/nodes/AndQueryNode;

    if-eqz v2, :cond_2

    .line 76
    iget-object v2, p0, Lorg/apache/lucene/queryparser/flexible/precedence/processors/BooleanModifiersQueryNodeProcessor;->childrenBuffer:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 77
    invoke-interface {p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->getChildren()Ljava/util/List;

    move-result-object v1

    .line 79
    .local v1, "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 83
    iget-object v2, p0, Lorg/apache/lucene/queryparser/flexible/precedence/processors/BooleanModifiersQueryNodeProcessor;->childrenBuffer:Ljava/util/ArrayList;

    invoke-interface {p1, v2}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->set(Ljava/util/List;)V

    .line 99
    .end local v1    # "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    :cond_0
    :goto_1
    return-object p1

    .line 79
    .restart local v1    # "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    .line 80
    .local v0, "child":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    iget-object v3, p0, Lorg/apache/lucene/queryparser/flexible/precedence/processors/BooleanModifiersQueryNodeProcessor;->childrenBuffer:Ljava/util/ArrayList;

    sget-object v4, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;->MOD_REQ:Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    invoke-direct {p0, v0, v4}, Lorg/apache/lucene/queryparser/flexible/precedence/processors/BooleanModifiersQueryNodeProcessor;->applyModifier(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 85
    .end local v0    # "child":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .end local v1    # "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/queryparser/flexible/precedence/processors/BooleanModifiersQueryNodeProcessor;->usingAnd:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    instance-of v2, p1, Lorg/apache/lucene/queryparser/flexible/core/nodes/BooleanQueryNode;

    if-eqz v2, :cond_0

    .line 86
    instance-of v2, p1, Lorg/apache/lucene/queryparser/flexible/core/nodes/OrQueryNode;

    if-nez v2, :cond_0

    .line 88
    iget-object v2, p0, Lorg/apache/lucene/queryparser/flexible/precedence/processors/BooleanModifiersQueryNodeProcessor;->childrenBuffer:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 89
    invoke-interface {p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->getChildren()Ljava/util/List;

    move-result-object v1

    .line 91
    .restart local v1    # "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_3

    .line 95
    iget-object v2, p0, Lorg/apache/lucene/queryparser/flexible/precedence/processors/BooleanModifiersQueryNodeProcessor;->childrenBuffer:Ljava/util/ArrayList;

    invoke-interface {p1, v2}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->set(Ljava/util/List;)V

    goto :goto_1

    .line 91
    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    .line 92
    .restart local v0    # "child":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    iget-object v3, p0, Lorg/apache/lucene/queryparser/flexible/precedence/processors/BooleanModifiersQueryNodeProcessor;->childrenBuffer:Ljava/util/ArrayList;

    sget-object v4, Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;->MOD_REQ:Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;

    invoke-direct {p0, v0, v4}, Lorg/apache/lucene/queryparser/flexible/precedence/processors/BooleanModifiersQueryNodeProcessor;->applyModifier(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;Lorg/apache/lucene/queryparser/flexible/core/nodes/ModifierQueryNode$Modifier;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method

.method protected preProcessNode(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 0
    .param p1, "node"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 124
    return-object p1
.end method

.method public process(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 3
    .param p1, "queryTree"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 59
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/precedence/processors/BooleanModifiersQueryNodeProcessor;->getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    move-result-object v1

    sget-object v2, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->DEFAULT_OPERATOR:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->get(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$Operator;

    .line 61
    .local v0, "op":Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$Operator;
    if-nez v0, :cond_0

    .line 62
    new-instance v1, Ljava/lang/IllegalArgumentException;

    .line 63
    const-string v2, "StandardQueryConfigHandler.ConfigurationKeys.DEFAULT_OPERATOR should be set on the QueryConfigHandler"

    .line 62
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 66
    :cond_0
    sget-object v1, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$Operator;->AND:Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$Operator;

    if-ne v1, v0, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/queryparser/flexible/precedence/processors/BooleanModifiersQueryNodeProcessor;->usingAnd:Ljava/lang/Boolean;

    .line 68
    invoke-super {p0, p1}, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl;->process(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v1

    return-object v1

    .line 66
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected setChildrenOrder(Ljava/util/List;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 131
    .local p1, "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    return-object p1
.end method
