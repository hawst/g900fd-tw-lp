.class public Lorg/apache/lucene/queryparser/xml/QueryTemplateManager;
.super Ljava/lang/Object;
.source "QueryTemplateManager.java"


# static fields
.field static final dbf:Ljavax/xml/parsers/DocumentBuilderFactory;

.field static final tFactory:Ljavax/xml/transform/TransformerFactory;


# instance fields
.field compiledTemplatesCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljavax/xml/transform/Templates;",
            ">;"
        }
    .end annotation
.end field

.field defaultCompiledTemplates:Ljavax/xml/transform/Templates;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/queryparser/xml/QueryTemplateManager;->dbf:Ljavax/xml/parsers/DocumentBuilderFactory;

    .line 49
    invoke-static {}, Ljavax/xml/transform/TransformerFactory;->newInstance()Ljavax/xml/transform/TransformerFactory;

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/queryparser/xml/QueryTemplateManager;->tFactory:Ljavax/xml/transform/TransformerFactory;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/queryparser/xml/QueryTemplateManager;->compiledTemplatesCache:Ljava/util/HashMap;

    .line 52
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/queryparser/xml/QueryTemplateManager;->defaultCompiledTemplates:Ljavax/xml/transform/Templates;

    .line 57
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "xslIs"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/xml/transform/TransformerConfigurationException;,
            Ljavax/xml/parsers/ParserConfigurationException;,
            Lorg/xml/sax/SAXException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/queryparser/xml/QueryTemplateManager;->compiledTemplatesCache:Ljava/util/HashMap;

    .line 52
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/queryparser/xml/QueryTemplateManager;->defaultCompiledTemplates:Ljavax/xml/transform/Templates;

    .line 61
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/xml/QueryTemplateManager;->addDefaultQueryTemplate(Ljava/io/InputStream;)V

    .line 62
    return-void
.end method

.method public static getQueryAsDOM(Ljava/util/Properties;Ljava/io/InputStream;)Lorg/w3c/dom/Document;
    .locals 2
    .param p0, "formProperties"    # Ljava/util/Properties;
    .param p1, "xslIs"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;,
            Ljava/io/IOException;,
            Ljavax/xml/parsers/ParserConfigurationException;,
            Ljavax/xml/transform/TransformerException;
        }
    .end annotation

    .prologue
    .line 137
    new-instance v0, Ljavax/xml/transform/dom/DOMResult;

    invoke-direct {v0}, Ljavax/xml/transform/dom/DOMResult;-><init>()V

    .line 138
    .local v0, "result":Ljavax/xml/transform/dom/DOMResult;
    invoke-static {p0, p1, v0}, Lorg/apache/lucene/queryparser/xml/QueryTemplateManager;->transformCriteria(Ljava/util/Properties;Ljava/io/InputStream;Ljavax/xml/transform/Result;)V

    .line 139
    invoke-virtual {v0}, Ljavax/xml/transform/dom/DOMResult;->getNode()Lorg/w3c/dom/Node;

    move-result-object v1

    check-cast v1, Lorg/w3c/dom/Document;

    return-object v1
.end method

.method public static getQueryAsDOM(Ljava/util/Properties;Ljavax/xml/transform/Templates;)Lorg/w3c/dom/Document;
    .locals 2
    .param p0, "formProperties"    # Ljava/util/Properties;
    .param p1, "template"    # Ljavax/xml/transform/Templates;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/xml/parsers/ParserConfigurationException;,
            Ljavax/xml/transform/TransformerException;
        }
    .end annotation

    .prologue
    .line 126
    new-instance v0, Ljavax/xml/transform/dom/DOMResult;

    invoke-direct {v0}, Ljavax/xml/transform/dom/DOMResult;-><init>()V

    .line 127
    .local v0, "result":Ljavax/xml/transform/dom/DOMResult;
    invoke-static {p0, p1, v0}, Lorg/apache/lucene/queryparser/xml/QueryTemplateManager;->transformCriteria(Ljava/util/Properties;Ljavax/xml/transform/Templates;Ljavax/xml/transform/Result;)V

    .line 128
    invoke-virtual {v0}, Ljavax/xml/transform/dom/DOMResult;->getNode()Lorg/w3c/dom/Node;

    move-result-object v1

    check-cast v1, Lorg/w3c/dom/Document;

    return-object v1
.end method

.method public static getQueryAsXmlString(Ljava/util/Properties;Ljava/io/InputStream;)Ljava/lang/String;
    .locals 3
    .param p0, "formProperties"    # Ljava/util/Properties;
    .param p1, "xslIs"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;,
            Ljava/io/IOException;,
            Ljavax/xml/parsers/ParserConfigurationException;,
            Ljavax/xml/transform/TransformerException;
        }
    .end annotation

    .prologue
    .line 114
    new-instance v1, Ljava/io/StringWriter;

    invoke-direct {v1}, Ljava/io/StringWriter;-><init>()V

    .line 115
    .local v1, "writer":Ljava/io/StringWriter;
    new-instance v0, Ljavax/xml/transform/stream/StreamResult;

    invoke-direct {v0, v1}, Ljavax/xml/transform/stream/StreamResult;-><init>(Ljava/io/Writer;)V

    .line 116
    .local v0, "result":Ljavax/xml/transform/stream/StreamResult;
    invoke-static {p0, p1, v0}, Lorg/apache/lucene/queryparser/xml/QueryTemplateManager;->transformCriteria(Ljava/util/Properties;Ljava/io/InputStream;Ljavax/xml/transform/Result;)V

    .line 117
    invoke-virtual {v1}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static getQueryAsXmlString(Ljava/util/Properties;Ljavax/xml/transform/Templates;)Ljava/lang/String;
    .locals 3
    .param p0, "formProperties"    # Ljava/util/Properties;
    .param p1, "template"    # Ljavax/xml/transform/Templates;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/xml/parsers/ParserConfigurationException;,
            Ljavax/xml/transform/TransformerException;
        }
    .end annotation

    .prologue
    .line 102
    new-instance v1, Ljava/io/StringWriter;

    invoke-direct {v1}, Ljava/io/StringWriter;-><init>()V

    .line 103
    .local v1, "writer":Ljava/io/StringWriter;
    new-instance v0, Ljavax/xml/transform/stream/StreamResult;

    invoke-direct {v0, v1}, Ljavax/xml/transform/stream/StreamResult;-><init>(Ljava/io/Writer;)V

    .line 104
    .local v0, "result":Ljavax/xml/transform/stream/StreamResult;
    invoke-static {p0, p1, v0}, Lorg/apache/lucene/queryparser/xml/QueryTemplateManager;->transformCriteria(Ljava/util/Properties;Ljavax/xml/transform/Templates;Ljavax/xml/transform/Result;)V

    .line 105
    invoke-virtual {v1}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static getTemplates(Ljava/io/InputStream;)Ljavax/xml/transform/Templates;
    .locals 5
    .param p0, "xslIs"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/xml/parsers/ParserConfigurationException;,
            Lorg/xml/sax/SAXException;,
            Ljava/io/IOException;,
            Ljavax/xml/transform/TransformerConfigurationException;
        }
    .end annotation

    .prologue
    .line 197
    sget-object v3, Lorg/apache/lucene/queryparser/xml/QueryTemplateManager;->dbf:Ljavax/xml/parsers/DocumentBuilderFactory;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljavax/xml/parsers/DocumentBuilderFactory;->setNamespaceAware(Z)V

    .line 198
    sget-object v3, Lorg/apache/lucene/queryparser/xml/QueryTemplateManager;->dbf:Ljavax/xml/parsers/DocumentBuilderFactory;

    invoke-virtual {v3}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v0

    .line 199
    .local v0, "builder":Ljavax/xml/parsers/DocumentBuilder;
    invoke-virtual {v0, p0}, Ljavax/xml/parsers/DocumentBuilder;->parse(Ljava/io/InputStream;)Lorg/w3c/dom/Document;

    move-result-object v2

    .line 200
    .local v2, "xslDoc":Lorg/w3c/dom/Document;
    new-instance v1, Ljavax/xml/transform/dom/DOMSource;

    invoke-direct {v1, v2}, Ljavax/xml/transform/dom/DOMSource;-><init>(Lorg/w3c/dom/Node;)V

    .line 201
    .local v1, "ds":Ljavax/xml/transform/dom/DOMSource;
    sget-object v3, Lorg/apache/lucene/queryparser/xml/QueryTemplateManager;->tFactory:Ljavax/xml/transform/TransformerFactory;

    invoke-virtual {v3, v1}, Ljavax/xml/transform/TransformerFactory;->newTemplates(Ljavax/xml/transform/Source;)Ljavax/xml/transform/Templates;

    move-result-object v3

    return-object v3
.end method

.method public static transformCriteria(Ljava/util/Properties;Ljava/io/InputStream;Ljavax/xml/transform/Result;)V
    .locals 6
    .param p0, "formProperties"    # Ljava/util/Properties;
    .param p1, "xslIs"    # Ljava/io/InputStream;
    .param p2, "result"    # Ljavax/xml/transform/Result;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;,
            Ljava/io/IOException;,
            Ljavax/xml/parsers/ParserConfigurationException;,
            Ljavax/xml/transform/TransformerException;
        }
    .end annotation

    .prologue
    .line 148
    sget-object v4, Lorg/apache/lucene/queryparser/xml/QueryTemplateManager;->dbf:Ljavax/xml/parsers/DocumentBuilderFactory;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Ljavax/xml/parsers/DocumentBuilderFactory;->setNamespaceAware(Z)V

    .line 149
    sget-object v4, Lorg/apache/lucene/queryparser/xml/QueryTemplateManager;->dbf:Ljavax/xml/parsers/DocumentBuilderFactory;

    invoke-virtual {v4}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v0

    .line 150
    .local v0, "builder":Ljavax/xml/parsers/DocumentBuilder;
    invoke-virtual {v0, p1}, Ljavax/xml/parsers/DocumentBuilder;->parse(Ljava/io/InputStream;)Lorg/w3c/dom/Document;

    move-result-object v3

    .line 151
    .local v3, "xslDoc":Lorg/w3c/dom/Document;
    new-instance v1, Ljavax/xml/transform/dom/DOMSource;

    invoke-direct {v1, v3}, Ljavax/xml/transform/dom/DOMSource;-><init>(Lorg/w3c/dom/Node;)V

    .line 153
    .local v1, "ds":Ljavax/xml/transform/dom/DOMSource;
    const/4 v2, 0x0

    .line 154
    .local v2, "transformer":Ljavax/xml/transform/Transformer;
    sget-object v5, Lorg/apache/lucene/queryparser/xml/QueryTemplateManager;->tFactory:Ljavax/xml/transform/TransformerFactory;

    monitor-enter v5

    .line 155
    :try_start_0
    sget-object v4, Lorg/apache/lucene/queryparser/xml/QueryTemplateManager;->tFactory:Ljavax/xml/transform/TransformerFactory;

    invoke-virtual {v4, v1}, Ljavax/xml/transform/TransformerFactory;->newTransformer(Ljavax/xml/transform/Source;)Ljavax/xml/transform/Transformer;

    move-result-object v2

    .line 154
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 157
    invoke-static {p0, v2, p2}, Lorg/apache/lucene/queryparser/xml/QueryTemplateManager;->transformCriteria(Ljava/util/Properties;Ljavax/xml/transform/Transformer;Ljavax/xml/transform/Result;)V

    .line 158
    return-void

    .line 154
    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4
.end method

.method public static transformCriteria(Ljava/util/Properties;Ljavax/xml/transform/Templates;Ljavax/xml/transform/Result;)V
    .locals 1
    .param p0, "formProperties"    # Ljava/util/Properties;
    .param p1, "template"    # Ljavax/xml/transform/Templates;
    .param p2, "result"    # Ljavax/xml/transform/Result;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/xml/parsers/ParserConfigurationException;,
            Ljavax/xml/transform/TransformerException;
        }
    .end annotation

    .prologue
    .line 165
    invoke-interface {p1}, Ljavax/xml/transform/Templates;->newTransformer()Ljavax/xml/transform/Transformer;

    move-result-object v0

    invoke-static {p0, v0, p2}, Lorg/apache/lucene/queryparser/xml/QueryTemplateManager;->transformCriteria(Ljava/util/Properties;Ljavax/xml/transform/Transformer;Ljavax/xml/transform/Result;)V

    .line 166
    return-void
.end method

.method public static transformCriteria(Ljava/util/Properties;Ljavax/xml/transform/Transformer;Ljavax/xml/transform/Result;)V
    .locals 9
    .param p0, "formProperties"    # Ljava/util/Properties;
    .param p1, "transformer"    # Ljavax/xml/transform/Transformer;
    .param p2, "result"    # Ljavax/xml/transform/Result;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/xml/parsers/ParserConfigurationException;,
            Ljavax/xml/transform/TransformerException;
        }
    .end annotation

    .prologue
    .line 171
    sget-object v7, Lorg/apache/lucene/queryparser/xml/QueryTemplateManager;->dbf:Ljavax/xml/parsers/DocumentBuilderFactory;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Ljavax/xml/parsers/DocumentBuilderFactory;->setNamespaceAware(Z)V

    .line 174
    sget-object v7, Lorg/apache/lucene/queryparser/xml/QueryTemplateManager;->dbf:Ljavax/xml/parsers/DocumentBuilderFactory;

    invoke-virtual {v7}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v0

    .line 175
    .local v0, "db":Ljavax/xml/parsers/DocumentBuilder;
    invoke-virtual {v0}, Ljavax/xml/parsers/DocumentBuilder;->newDocument()Lorg/w3c/dom/Document;

    move-result-object v1

    .line 176
    .local v1, "doc":Lorg/w3c/dom/Document;
    const-string v7, "Document"

    invoke-interface {v1, v7}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v4

    .line 177
    .local v4, "root":Lorg/w3c/dom/Element;
    invoke-interface {v1, v4}, Lorg/w3c/dom/Document;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 179
    invoke-virtual {p0}, Ljava/util/Properties;->keys()Ljava/util/Enumeration;

    move-result-object v2

    .line 180
    .local v2, "keysEnum":Ljava/util/Enumeration;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v7

    if-nez v7, :cond_1

    .line 188
    new-instance v6, Ljavax/xml/transform/dom/DOMSource;

    invoke-direct {v6, v1}, Ljavax/xml/transform/dom/DOMSource;-><init>(Lorg/w3c/dom/Node;)V

    .line 189
    .local v6, "xml":Ljavax/xml/transform/dom/DOMSource;
    invoke-virtual {p1, v6, p2}, Ljavax/xml/transform/Transformer;->transform(Ljavax/xml/transform/Source;Ljavax/xml/transform/Result;)V

    .line 190
    return-void

    .line 181
    .end local v6    # "xml":Ljavax/xml/transform/dom/DOMSource;
    :cond_1
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 182
    .local v3, "propName":Ljava/lang/String;
    invoke-virtual {p0, v3}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 183
    .local v5, "value":Ljava/lang/String;
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_0

    .line 184
    invoke-static {v4, v3, v5}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->insertChild(Lorg/w3c/dom/Element;Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Element;

    goto :goto_0
.end method


# virtual methods
.method public addDefaultQueryTemplate(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "xslIs"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/xml/transform/TransformerConfigurationException;,
            Ljavax/xml/parsers/ParserConfigurationException;,
            Lorg/xml/sax/SAXException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 66
    invoke-static {p1}, Lorg/apache/lucene/queryparser/xml/QueryTemplateManager;->getTemplates(Ljava/io/InputStream;)Ljavax/xml/transform/Templates;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/queryparser/xml/QueryTemplateManager;->defaultCompiledTemplates:Ljavax/xml/transform/Templates;

    .line 67
    return-void
.end method

.method public addQueryTemplate(Ljava/lang/String;Ljava/io/InputStream;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "xslIs"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/xml/transform/TransformerConfigurationException;,
            Ljavax/xml/parsers/ParserConfigurationException;,
            Lorg/xml/sax/SAXException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 71
    iget-object v0, p0, Lorg/apache/lucene/queryparser/xml/QueryTemplateManager;->compiledTemplatesCache:Ljava/util/HashMap;

    invoke-static {p2}, Lorg/apache/lucene/queryparser/xml/QueryTemplateManager;->getTemplates(Ljava/io/InputStream;)Ljavax/xml/transform/Templates;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    return-void
.end method

.method public getQueryAsDOM(Ljava/util/Properties;)Lorg/w3c/dom/Document;
    .locals 1
    .param p1, "formProperties"    # Ljava/util/Properties;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;,
            Ljava/io/IOException;,
            Ljavax/xml/parsers/ParserConfigurationException;,
            Ljavax/xml/transform/TransformerException;
        }
    .end annotation

    .prologue
    .line 93
    iget-object v0, p0, Lorg/apache/lucene/queryparser/xml/QueryTemplateManager;->defaultCompiledTemplates:Ljavax/xml/transform/Templates;

    invoke-static {p1, v0}, Lorg/apache/lucene/queryparser/xml/QueryTemplateManager;->getQueryAsDOM(Ljava/util/Properties;Ljavax/xml/transform/Templates;)Lorg/w3c/dom/Document;

    move-result-object v0

    return-object v0
.end method

.method public getQueryAsDOM(Ljava/util/Properties;Ljava/lang/String;)Lorg/w3c/dom/Document;
    .locals 2
    .param p1, "formProperties"    # Ljava/util/Properties;
    .param p2, "queryTemplateName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;,
            Ljava/io/IOException;,
            Ljavax/xml/parsers/ParserConfigurationException;,
            Ljavax/xml/transform/TransformerException;
        }
    .end annotation

    .prologue
    .line 82
    iget-object v1, p0, Lorg/apache/lucene/queryparser/xml/QueryTemplateManager;->compiledTemplatesCache:Ljava/util/HashMap;

    invoke-virtual {v1, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljavax/xml/transform/Templates;

    .line 83
    .local v0, "ts":Ljavax/xml/transform/Templates;
    invoke-static {p1, v0}, Lorg/apache/lucene/queryparser/xml/QueryTemplateManager;->getQueryAsDOM(Ljava/util/Properties;Ljavax/xml/transform/Templates;)Lorg/w3c/dom/Document;

    move-result-object v1

    return-object v1
.end method

.method public getQueryAsXmlString(Ljava/util/Properties;)Ljava/lang/String;
    .locals 1
    .param p1, "formProperties"    # Ljava/util/Properties;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;,
            Ljava/io/IOException;,
            Ljavax/xml/parsers/ParserConfigurationException;,
            Ljavax/xml/transform/TransformerException;
        }
    .end annotation

    .prologue
    .line 88
    iget-object v0, p0, Lorg/apache/lucene/queryparser/xml/QueryTemplateManager;->defaultCompiledTemplates:Ljavax/xml/transform/Templates;

    invoke-static {p1, v0}, Lorg/apache/lucene/queryparser/xml/QueryTemplateManager;->getQueryAsXmlString(Ljava/util/Properties;Ljavax/xml/transform/Templates;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getQueryAsXmlString(Ljava/util/Properties;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "formProperties"    # Ljava/util/Properties;
    .param p2, "queryTemplateName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;,
            Ljava/io/IOException;,
            Ljavax/xml/parsers/ParserConfigurationException;,
            Ljavax/xml/transform/TransformerException;
        }
    .end annotation

    .prologue
    .line 76
    iget-object v1, p0, Lorg/apache/lucene/queryparser/xml/QueryTemplateManager;->compiledTemplatesCache:Ljava/util/HashMap;

    invoke-virtual {v1, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljavax/xml/transform/Templates;

    .line 77
    .local v0, "ts":Ljavax/xml/transform/Templates;
    invoke-static {p1, v0}, Lorg/apache/lucene/queryparser/xml/QueryTemplateManager;->getQueryAsXmlString(Ljava/util/Properties;Ljavax/xml/transform/Templates;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
