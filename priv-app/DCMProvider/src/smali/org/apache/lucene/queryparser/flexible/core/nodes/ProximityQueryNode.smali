.class public Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode;
.super Lorg/apache/lucene/queryparser/flexible/core/nodes/BooleanQueryNode;
.source "ProximityQueryNode.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode$ProximityType;,
        Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode$Type;
    }
.end annotation


# instance fields
.field private distance:I

.field private field:Ljava/lang/CharSequence;

.field private inorder:Z

.field private proximityType:Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode$Type;


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/lang/CharSequence;Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode$Type;IZ)V
    .locals 6
    .param p2, "field"    # Ljava/lang/CharSequence;
    .param p3, "type"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode$Type;
    .param p4, "distance"    # I
    .param p5, "inorder"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;",
            ">;",
            "Ljava/lang/CharSequence;",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode$Type;",
            "IZ)V"
        }
    .end annotation

    .prologue
    .local p1, "clauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    const/4 v5, 0x0

    .line 92
    invoke-direct {p0, p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/BooleanQueryNode;-><init>(Ljava/util/List;)V

    .line 72
    sget-object v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode$Type;->SENTENCE:Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode$Type;

    iput-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode;->proximityType:Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode$Type;

    .line 73
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode;->distance:I

    .line 74
    iput-boolean v5, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode;->inorder:Z

    .line 75
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode;->field:Ljava/lang/CharSequence;

    .line 93
    invoke-virtual {p0, v5}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode;->setLeaf(Z)V

    .line 94
    iput-object p3, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode;->proximityType:Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode$Type;

    .line 95
    iput-boolean p5, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode;->inorder:Z

    .line 96
    iput-object p2, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode;->field:Ljava/lang/CharSequence;

    .line 97
    sget-object v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode$Type;->NUMBER:Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode$Type;

    if-ne p3, v0, :cond_1

    .line 98
    if-gtz p4, :cond_0

    .line 99
    new-instance v0, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeError;

    new-instance v1, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;

    .line 100
    sget-object v2, Lorg/apache/lucene/queryparser/flexible/core/messages/QueryParserMessages;->PARAMETER_VALUE_NOT_SUPPORTED:Ljava/lang/String;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "distance"

    aput-object v4, v3, v5

    const/4 v4, 0x1

    .line 101
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-direct {v1, v2, v3}, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 99
    invoke-direct {v0, v1}, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeError;-><init>(Lorg/apache/lucene/queryparser/flexible/messages/Message;)V

    throw v0

    .line 104
    :cond_0
    iput p4, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode;->distance:I

    .line 108
    :cond_1
    invoke-static {p1, p2}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode;->clearFields(Ljava/util/List;Ljava/lang/CharSequence;)V

    .line 109
    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/lang/CharSequence;Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode$Type;Z)V
    .locals 6
    .param p2, "field"    # Ljava/lang/CharSequence;
    .param p3, "type"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode$Type;
    .param p4, "inorder"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;",
            ">;",
            "Ljava/lang/CharSequence;",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode$Type;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 124
    .local p1, "clauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    const/4 v4, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode;-><init>(Ljava/util/List;Ljava/lang/CharSequence;Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode$Type;IZ)V

    .line 125
    return-void
.end method

.method private static clearFields(Ljava/util/List;Ljava/lang/CharSequence;)V
    .locals 4
    .param p1, "field"    # Ljava/lang/CharSequence;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;",
            ">;",
            "Ljava/lang/CharSequence;",
            ")V"
        }
    .end annotation

    .prologue
    .line 128
    .local p0, "nodes":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 138
    :cond_0
    return-void

    .line 131
    :cond_1
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    .line 133
    .local v0, "clause":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    instance-of v1, v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;

    if-eqz v1, :cond_2

    move-object v1, v0

    .line 134
    check-cast v1, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;

    const/4 v3, 0x1

    iput-boolean v3, v1, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->toQueryStringIgnoreFields:Z

    .line 135
    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;

    .end local v0    # "clause":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    invoke-virtual {v0, p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->setField(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public cloneTree()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 190
    invoke-super {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/BooleanQueryNode;->cloneTree()Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode;

    .line 192
    .local v0, "clone":Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode;
    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode;->proximityType:Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode$Type;

    iput-object v1, v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode;->proximityType:Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode$Type;

    .line 193
    iget v1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode;->distance:I

    iput v1, v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode;->distance:I

    .line 194
    iget-object v1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode;->field:Ljava/lang/CharSequence;

    iput-object v1, v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode;->field:Ljava/lang/CharSequence;

    .line 196
    return-object v0
.end method

.method public getDistance()I
    .locals 1

    .prologue
    .line 203
    iget v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode;->distance:I

    return v0
.end method

.method public getField()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode;->field:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getFieldAsString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode;->field:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    .line 222
    const/4 v0, 0x0

    .line 224
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode;->field:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getProximityType()Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode$Type;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode;->proximityType:Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode$Type;

    return-object v0
.end method

.method public isInOrder()Z
    .locals 1

    .prologue
    .line 239
    iget-boolean v0, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode;->inorder:Z

    return v0
.end method

.method public setField(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "field"    # Ljava/lang/CharSequence;

    .prologue
    .line 232
    iput-object p1, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode;->field:Ljava/lang/CharSequence;

    .line 233
    return-void
.end method

.method public toQueryString(Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;)Ljava/lang/CharSequence;
    .locals 7
    .param p1, "escapeSyntaxParser"    # Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;

    .prologue
    .line 166
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode;->proximityType:Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode$Type;

    invoke-virtual {v5}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode$Type;->toQueryString()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 167
    iget v4, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode;->distance:I

    const/4 v6, -0x1

    if-ne v4, v6, :cond_1

    const-string v4, ""

    :goto_0
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 168
    iget-boolean v4, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode;->inorder:Z

    if-eqz v4, :cond_2

    const-string v4, " INORDER"

    :goto_1
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 166
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 170
    .local v3, "withinSTR":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 171
    .local v2, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode;->getChildren()Ljava/util/List;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode;->getChildren()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-eqz v4, :cond_0

    .line 174
    const-string v1, ""

    .line 175
    .local v1, "filler":Ljava/lang/String;
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode;->getChildren()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_3

    .line 181
    .end local v1    # "filler":Ljava/lang/String;
    :cond_0
    iget-object v4, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode;->field:Ljava/lang/CharSequence;

    invoke-virtual {p0, v4}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode;->isDefaultField(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 182
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "( "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ) "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 184
    :goto_3
    return-object v4

    .line 167
    .end local v2    # "sb":Ljava/lang/StringBuilder;
    .end local v3    # "withinSTR":Ljava/lang/String;
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, " "

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode;->distance:I

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 168
    :cond_2
    const-string v4, ""

    goto :goto_1

    .line 175
    .restart local v1    # "filler":Ljava/lang/String;
    .restart local v2    # "sb":Ljava/lang/StringBuilder;
    .restart local v3    # "withinSTR":Ljava/lang/String;
    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    .line 176
    .local v0, "child":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v0, p1}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->toQueryString(Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 177
    const-string v1, " "

    goto :goto_2

    .line 184
    .end local v0    # "child":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .end local v1    # "filler":Ljava/lang/String;
    :cond_4
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode;->field:Ljava/lang/CharSequence;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":(( "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ) "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_3
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 146
    iget v3, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode;->distance:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_1

    const-string v1, ""

    .line 149
    .local v1, "distanceSTR":Ljava/lang/String;
    :goto_0
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode;->getChildren()Ljava/util/List;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode;->getChildren()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_2

    .line 150
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "<proximity field=\'"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode;->field:Ljava/lang/CharSequence;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\' inorder=\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode;->inorder:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 151
    const-string v4, "\' type=\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode;->proximityType:Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode$Type;

    invoke-virtual {v4}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode$Type;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 152
    const-string v4, "/>"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 150
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 161
    :goto_1
    return-object v3

    .line 147
    .end local v1    # "distanceSTR":Ljava/lang/String;
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, " distance=\'"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode;->distance:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 153
    .restart local v1    # "distanceSTR":Ljava/lang/String;
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 154
    .local v2, "sb":Ljava/lang/StringBuilder;
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "<proximity field=\'"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode;->field:Ljava/lang/CharSequence;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\' inorder=\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode;->inorder:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 155
    const-string v4, "\' type=\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode;->proximityType:Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode$Type;

    invoke-virtual {v4}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode$Type;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ">"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 154
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 156
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/ProximityQueryNode;->getChildren()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_3

    .line 160
    const-string v3, "\n</proximity>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 161
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 156
    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    .line 157
    .local v0, "child":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    const-string v4, "\n"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 158
    invoke-interface {v0}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2
.end method
