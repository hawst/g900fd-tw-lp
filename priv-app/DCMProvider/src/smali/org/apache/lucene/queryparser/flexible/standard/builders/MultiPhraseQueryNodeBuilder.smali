.class public Lorg/apache/lucene/queryparser/flexible/standard/builders/MultiPhraseQueryNodeBuilder;
.super Ljava/lang/Object;
.source "MultiPhraseQueryNodeBuilder.java"

# interfaces
.implements Lorg/apache/lucene/queryparser/flexible/standard/builders/StandardQueryBuilder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    return-void
.end method


# virtual methods
.method public bridge synthetic build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/MultiPhraseQueryNodeBuilder;->build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/search/MultiPhraseQuery;

    move-result-object v0

    return-object v0
.end method

.method public build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/search/MultiPhraseQuery;
    .locals 11
    .param p1, "queryNode"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 45
    move-object v2, p1

    check-cast v2, Lorg/apache/lucene/queryparser/flexible/standard/nodes/MultiPhraseQueryNode;

    .line 47
    .local v2, "phraseNode":Lorg/apache/lucene/queryparser/flexible/standard/nodes/MultiPhraseQueryNode;
    new-instance v3, Lorg/apache/lucene/search/MultiPhraseQuery;

    invoke-direct {v3}, Lorg/apache/lucene/search/MultiPhraseQuery;-><init>()V

    .line 49
    .local v3, "phraseQuery":Lorg/apache/lucene/search/MultiPhraseQuery;
    invoke-virtual {v2}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/MultiPhraseQueryNode;->getChildren()Ljava/util/List;

    move-result-object v1

    .line 51
    .local v1, "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    if-eqz v1, :cond_0

    .line 52
    new-instance v5, Ljava/util/TreeMap;

    invoke-direct {v5}, Ljava/util/TreeMap;-><init>()V

    .line 54
    .local v5, "positionTermMap":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Integer;Ljava/util/List<Lorg/apache/lucene/index/Term;>;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-nez v10, :cond_1

    .line 71
    invoke-virtual {v5}, Ljava/util/TreeMap;->keySet()Ljava/util/Set;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-nez v9, :cond_3

    .line 81
    .end local v5    # "positionTermMap":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Integer;Ljava/util/List<Lorg/apache/lucene/index/Term;>;>;"
    :cond_0
    return-object v3

    .line 54
    .restart local v5    # "positionTermMap":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Integer;Ljava/util/List<Lorg/apache/lucene/index/Term;>;>;"
    :cond_1
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    .local v0, "child":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    move-object v7, v0

    .line 55
    check-cast v7, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;

    .line 57
    .local v7, "termNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    sget-object v10, Lorg/apache/lucene/queryparser/flexible/core/builders/QueryTreeBuilder;->QUERY_TREE_BUILDER_TAGID:Ljava/lang/String;

    invoke-virtual {v7, v10}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->getTag(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    .line 56
    check-cast v8, Lorg/apache/lucene/search/TermQuery;

    .line 59
    .local v8, "termQuery":Lorg/apache/lucene/search/TermQuery;
    invoke-virtual {v7}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->getPositionIncrement()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    .line 58
    invoke-virtual {v5, v10}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/List;

    .line 61
    .local v6, "termList":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/Term;>;"
    if-nez v6, :cond_2

    .line 62
    new-instance v6, Ljava/util/LinkedList;

    .end local v6    # "termList":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/Term;>;"
    invoke-direct {v6}, Ljava/util/LinkedList;-><init>()V

    .line 63
    .restart local v6    # "termList":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/Term;>;"
    invoke-virtual {v7}, Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;->getPositionIncrement()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v5, v10, v6}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    :cond_2
    invoke-virtual {v8}, Lorg/apache/lucene/search/TermQuery;->getTerm()Lorg/apache/lucene/index/Term;

    move-result-object v10

    invoke-interface {v6, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 71
    .end local v0    # "child":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .end local v6    # "termList":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/Term;>;"
    .end local v7    # "termNode":Lorg/apache/lucene/queryparser/flexible/core/nodes/FieldQueryNode;
    .end local v8    # "termQuery":Lorg/apache/lucene/search/TermQuery;
    :cond_3
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 72
    .local v4, "positionIncrement":I
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/List;

    .line 74
    .restart local v6    # "termList":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/Term;>;"
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v9

    new-array v9, v9, [Lorg/apache/lucene/index/Term;

    invoke-interface {v6, v9}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Lorg/apache/lucene/index/Term;

    invoke-virtual {v3, v9, v4}, Lorg/apache/lucene/search/MultiPhraseQuery;->add([Lorg/apache/lucene/index/Term;I)V

    goto :goto_1
.end method

.method public bridge synthetic build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/search/Query;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryparser/flexible/standard/builders/MultiPhraseQueryNodeBuilder;->build(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/search/MultiPhraseQuery;

    move-result-object v0

    return-object v0
.end method
