.class public Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;
.super Ljava/lang/Object;
.source "BasicQueryFactory.java"


# instance fields
.field private maxBasicQueries:I

.field private queriesMade:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    const/16 v0, 0x400

    invoke-direct {p0, v0}, Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;-><init>(I)V

    .line 41
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "maxBasicQueries"    # I

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput p1, p0, Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;->maxBasicQueries:I

    .line 36
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;->queriesMade:I

    .line 37
    return-void
.end method

.method private atMax()Z
    .locals 2

    .prologue
    .line 58
    iget v0, p0, Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;->queriesMade:I

    iget v1, p0, Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;->maxBasicQueries:I

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected declared-synchronized checkMax()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/surround/query/TooManyBasicQueries;
        }
    .end annotation

    .prologue
    .line 62
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;->atMax()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    new-instance v0, Lorg/apache/lucene/queryparser/surround/query/TooManyBasicQueries;

    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;->getMaxBasicQueries()I

    move-result v1

    invoke-direct {v0, v1}, Lorg/apache/lucene/queryparser/surround/query/TooManyBasicQueries;-><init>(I)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 62
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 64
    :cond_0
    :try_start_1
    iget v0, p0, Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;->queriesMade:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;->queriesMade:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 65
    monitor-exit p0

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 87
    instance-of v2, p1, Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;

    if-nez v2, :cond_1

    .line 90
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 89
    check-cast v0, Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;

    .line 90
    .local v0, "other":Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;->atMax()Z

    move-result v2

    invoke-direct {v0}, Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;->atMax()Z

    move-result v3

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getMaxBasicQueries()I
    .locals 1

    .prologue
    .line 47
    iget v0, p0, Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;->maxBasicQueries:I

    return v0
.end method

.method public getNrQueriesMade()I
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;->queriesMade:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 79
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-direct {p0}, Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;->atMax()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x7

    :goto_0
    xor-int/2addr v0, v1

    return v0

    :cond_0
    const/16 v0, 0x3e0

    goto :goto_0
.end method

.method public newSpanTermQuery(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/search/spans/SpanTermQuery;
    .locals 1
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/surround/query/TooManyBasicQueries;
        }
    .end annotation

    .prologue
    .line 73
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;->checkMax()V

    .line 74
    new-instance v0, Lorg/apache/lucene/search/spans/SpanTermQuery;

    invoke-direct {v0, p1}, Lorg/apache/lucene/search/spans/SpanTermQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    return-object v0
.end method

.method public newTermQuery(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/search/TermQuery;
    .locals 1
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/surround/query/TooManyBasicQueries;
        }
    .end annotation

    .prologue
    .line 68
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;->checkMax()V

    .line 69
    new-instance v0, Lorg/apache/lucene/search/TermQuery;

    invoke-direct {v0, p1}, Lorg/apache/lucene/search/TermQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 51
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 52
    const-string v1, "(maxBasicQueries: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;->maxBasicQueries:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 53
    const-string v1, ", queriesMade: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;->queriesMade:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 54
    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 51
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
