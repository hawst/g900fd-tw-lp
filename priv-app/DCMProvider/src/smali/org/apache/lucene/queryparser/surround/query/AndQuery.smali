.class public Lorg/apache/lucene/queryparser/surround/query/AndQuery;
.super Lorg/apache/lucene/queryparser/surround/query/ComposedQuery;
.source "AndQuery.java"


# direct methods
.method public constructor <init>(Ljava/util/List;ZLjava/lang/String;)V
    .locals 0
    .param p2, "inf"    # Z
    .param p3, "opName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/surround/query/SrndQuery;",
            ">;Z",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 29
    .local p1, "queries":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/surround/query/SrndQuery;>;"
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/queryparser/surround/query/ComposedQuery;-><init>(Ljava/util/List;ZLjava/lang/String;)V

    .line 30
    return-void
.end method


# virtual methods
.method public makeLuceneQueryFieldNoBoost(Ljava/lang/String;Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;)Lorg/apache/lucene/search/Query;
    .locals 2
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "qf"    # Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;

    .prologue
    .line 35
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/queryparser/surround/query/AndQuery;->makeLuceneSubQueriesField(Ljava/lang/String;Lorg/apache/lucene/queryparser/surround/query/BasicQueryFactory;)Ljava/util/List;

    move-result-object v0

    sget-object v1, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    .line 34
    invoke-static {v0, v1}, Lorg/apache/lucene/queryparser/surround/query/SrndBooleanQuery;->makeBooleanQuery(Ljava/util/List;Lorg/apache/lucene/search/BooleanClause$Occur;)Lorg/apache/lucene/search/Query;

    move-result-object v0

    return-object v0
.end method
