.class Lorg/apache/lucene/queryparser/surround/query/SrndQuery$1;
.super Lorg/apache/lucene/search/BooleanQuery;
.source "SrndQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/queryparser/surround/query/SrndQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 99
    invoke-direct {p0}, Lorg/apache/lucene/search/BooleanQuery;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public add(Lorg/apache/lucene/search/BooleanClause;)V
    .locals 1
    .param p1, "clause"    # Lorg/apache/lucene/search/BooleanClause;

    .prologue
    .line 106
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V
    .locals 1
    .param p1, "query"    # Lorg/apache/lucene/search/Query;
    .param p2, "occur"    # Lorg/apache/lucene/search/BooleanClause$Occur;

    .prologue
    .line 110
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setBoost(F)V
    .locals 1
    .param p1, "boost"    # F

    .prologue
    .line 102
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
