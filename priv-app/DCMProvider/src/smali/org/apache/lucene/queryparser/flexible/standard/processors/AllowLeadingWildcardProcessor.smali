.class public Lorg/apache/lucene/queryparser/flexible/standard/processors/AllowLeadingWildcardProcessor;
.super Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl;
.source "AllowLeadingWildcardProcessor.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl;-><init>()V

    .line 46
    return-void
.end method


# virtual methods
.method protected postProcessNode(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 7
    .param p1, "node"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 66
    instance-of v1, p1, Lorg/apache/lucene/queryparser/flexible/standard/nodes/WildcardQueryNode;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 67
    check-cast v0, Lorg/apache/lucene/queryparser/flexible/standard/nodes/WildcardQueryNode;

    .line 69
    .local v0, "wildcardNode":Lorg/apache/lucene/queryparser/flexible/standard/nodes/WildcardQueryNode;
    invoke-virtual {v0}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/WildcardQueryNode;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 72
    invoke-virtual {v0}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/WildcardQueryNode;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1, v6}, Lorg/apache/lucene/queryparser/flexible/core/util/UnescapedCharSequence;->wasEscaped(Ljava/lang/CharSequence;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 86
    .end local v0    # "wildcardNode":Lorg/apache/lucene/queryparser/flexible/standard/nodes/WildcardQueryNode;
    :cond_0
    :goto_0
    return-object p1

    .line 75
    .restart local v0    # "wildcardNode":Lorg/apache/lucene/queryparser/flexible/standard/nodes/WildcardQueryNode;
    :cond_1
    invoke-virtual {v0}, Lorg/apache/lucene/queryparser/flexible/standard/nodes/WildcardQueryNode;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1, v6}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    sparse-switch v1, :sswitch_data_0

    goto :goto_0

    .line 78
    :sswitch_0
    new-instance v1, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;

    new-instance v2, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;

    .line 79
    sget-object v3, Lorg/apache/lucene/queryparser/flexible/core/messages/QueryParserMessages;->LEADING_WILDCARD_NOT_ALLOWED:Ljava/lang/String;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    .line 80
    new-instance v5, Lorg/apache/lucene/queryparser/flexible/standard/parser/EscapeQuerySyntaxImpl;

    invoke-direct {v5}, Lorg/apache/lucene/queryparser/flexible/standard/parser/EscapeQuerySyntaxImpl;-><init>()V

    invoke-interface {p1, v5}, Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;->toQueryString(Lorg/apache/lucene/queryparser/flexible/core/parser/EscapeQuerySyntax;)Ljava/lang/CharSequence;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-direct {v2, v3, v4}, Lorg/apache/lucene/queryparser/flexible/messages/MessageImpl;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 78
    invoke-direct {v1, v2}, Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;-><init>(Lorg/apache/lucene/queryparser/flexible/messages/Message;)V

    throw v1

    .line 75
    :sswitch_data_0
    .sparse-switch
        0x2a -> :sswitch_0
        0x3f -> :sswitch_0
    .end sparse-switch
.end method

.method protected preProcessNode(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 0
    .param p1, "node"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 93
    return-object p1
.end method

.method public process(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .locals 3
    .param p1, "queryTree"    # Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 50
    invoke-virtual {p0}, Lorg/apache/lucene/queryparser/flexible/standard/processors/AllowLeadingWildcardProcessor;->getQueryConfigHandler()Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;

    move-result-object v1

    sget-object v2, Lorg/apache/lucene/queryparser/flexible/standard/config/StandardQueryConfigHandler$ConfigurationKeys;->ALLOW_LEADING_WILDCARD:Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/queryparser/flexible/core/config/QueryConfigHandler;->get(Lorg/apache/lucene/queryparser/flexible/core/config/ConfigurationKey;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 52
    .local v0, "allowsLeadingWildcard":Ljava/lang/Boolean;
    if-eqz v0, :cond_0

    .line 54
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_0

    .line 55
    invoke-super {p0, p1}, Lorg/apache/lucene/queryparser/flexible/core/processors/QueryNodeProcessorImpl;->process(Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;)Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;

    move-result-object p1

    .line 60
    .end local p1    # "queryTree":Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;
    :cond_0
    return-object p1
.end method

.method protected setChildrenOrder(Ljava/util/List;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/flexible/core/QueryNodeException;
        }
    .end annotation

    .prologue
    .line 101
    .local p1, "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queryparser/flexible/core/nodes/QueryNode;>;"
    return-object p1
.end method
