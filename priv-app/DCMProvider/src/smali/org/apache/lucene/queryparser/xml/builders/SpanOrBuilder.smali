.class public Lorg/apache/lucene/queryparser/xml/builders/SpanOrBuilder;
.super Lorg/apache/lucene/queryparser/xml/builders/SpanBuilderBase;
.source "SpanOrBuilder.java"


# instance fields
.field private final factory:Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilder;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilder;)V
    .locals 0
    .param p1, "factory"    # Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilder;

    .prologue
    .line 36
    invoke-direct {p0}, Lorg/apache/lucene/queryparser/xml/builders/SpanBuilderBase;-><init>()V

    .line 37
    iput-object p1, p0, Lorg/apache/lucene/queryparser/xml/builders/SpanOrBuilder;->factory:Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilder;

    .line 38
    return-void
.end method


# virtual methods
.method public getSpanQuery(Lorg/w3c/dom/Element;)Lorg/apache/lucene/search/spans/SpanQuery;
    .locals 7
    .param p1, "e"    # Lorg/w3c/dom/Element;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryparser/xml/ParserException;
        }
    .end annotation

    .prologue
    .line 42
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 43
    .local v2, "clausesList":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/spans/SpanQuery;>;"
    invoke-interface {p1}, Lorg/w3c/dom/Element;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v3

    .local v3, "kid":Lorg/w3c/dom/Node;
    :goto_0
    if-nez v3, :cond_0

    .line 49
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    new-array v5, v5, [Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-interface {v2, v5}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lorg/apache/lucene/search/spans/SpanQuery;

    .line 50
    .local v1, "clauses":[Lorg/apache/lucene/search/spans/SpanQuery;
    new-instance v4, Lorg/apache/lucene/search/spans/SpanOrQuery;

    invoke-direct {v4, v1}, Lorg/apache/lucene/search/spans/SpanOrQuery;-><init>([Lorg/apache/lucene/search/spans/SpanQuery;)V

    .line 51
    .local v4, "soq":Lorg/apache/lucene/search/spans/SpanOrQuery;
    const-string v5, "boost"

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-static {p1, v5, v6}, Lorg/apache/lucene/queryparser/xml/DOMUtils;->getAttribute(Lorg/w3c/dom/Element;Ljava/lang/String;F)F

    move-result v5

    invoke-virtual {v4, v5}, Lorg/apache/lucene/search/spans/SpanOrQuery;->setBoost(F)V

    .line 52
    return-object v4

    .line 44
    .end local v1    # "clauses":[Lorg/apache/lucene/search/spans/SpanQuery;
    .end local v4    # "soq":Lorg/apache/lucene/search/spans/SpanOrQuery;
    :cond_0
    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_1

    .line 45
    iget-object v6, p0, Lorg/apache/lucene/queryparser/xml/builders/SpanOrBuilder;->factory:Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilder;

    move-object v5, v3

    check-cast v5, Lorg/w3c/dom/Element;

    invoke-interface {v6, v5}, Lorg/apache/lucene/queryparser/xml/builders/SpanQueryBuilder;->getSpanQuery(Lorg/w3c/dom/Element;)Lorg/apache/lucene/search/spans/SpanQuery;

    move-result-object v0

    .line 46
    .local v0, "clause":Lorg/apache/lucene/search/spans/SpanQuery;
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 43
    .end local v0    # "clause":Lorg/apache/lucene/search/spans/SpanQuery;
    :cond_1
    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNextSibling()Lorg/w3c/dom/Node;

    move-result-object v3

    goto :goto_0
.end method
