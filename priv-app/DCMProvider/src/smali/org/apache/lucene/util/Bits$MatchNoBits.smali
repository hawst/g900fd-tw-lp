.class public Lorg/apache/lucene/util/Bits$MatchNoBits;
.super Ljava/lang/Object;
.source "Bits.java"

# interfaces
.implements Lorg/apache/lucene/util/Bits;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/Bits;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MatchNoBits"
.end annotation


# instance fields
.field final len:I


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1, "len"    # I

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    iput p1, p0, Lorg/apache/lucene/util/Bits$MatchNoBits;->len:I

    .line 69
    return-void
.end method


# virtual methods
.method public get(I)Z
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 73
    const/4 v0, 0x0

    return v0
.end method

.method public length()I
    .locals 1

    .prologue
    .line 78
    iget v0, p0, Lorg/apache/lucene/util/Bits$MatchNoBits;->len:I

    return v0
.end method
