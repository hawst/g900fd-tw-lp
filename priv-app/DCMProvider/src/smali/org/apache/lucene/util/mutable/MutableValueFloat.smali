.class public Lorg/apache/lucene/util/mutable/MutableValueFloat;
.super Lorg/apache/lucene/util/mutable/MutableValue;
.source "MutableValueFloat.java"


# instance fields
.field public value:F


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lorg/apache/lucene/util/mutable/MutableValue;-><init>()V

    return-void
.end method


# virtual methods
.method public compareSameType(Ljava/lang/Object;)I
    .locals 4
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 54
    move-object v0, p1

    check-cast v0, Lorg/apache/lucene/util/mutable/MutableValueFloat;

    .line 55
    .local v0, "b":Lorg/apache/lucene/util/mutable/MutableValueFloat;
    iget v2, p0, Lorg/apache/lucene/util/mutable/MutableValueFloat;->value:F

    iget v3, v0, Lorg/apache/lucene/util/mutable/MutableValueFloat;->value:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v1

    .line 56
    .local v1, "c":I
    if-eqz v1, :cond_0

    .line 58
    .end local v1    # "c":I
    :goto_0
    return v1

    .line 57
    .restart local v1    # "c":I
    :cond_0
    iget-boolean v2, p0, Lorg/apache/lucene/util/mutable/MutableValueFloat;->exists:Z

    iget-boolean v3, v0, Lorg/apache/lucene/util/mutable/MutableValueFloat;->exists:Z

    if-ne v2, v3, :cond_1

    const/4 v1, 0x0

    goto :goto_0

    .line 58
    :cond_1
    iget-boolean v2, p0, Lorg/apache/lucene/util/mutable/MutableValueFloat;->exists:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :goto_1
    move v1, v2

    goto :goto_0

    :cond_2
    const/4 v2, -0x1

    goto :goto_1
.end method

.method public copy(Lorg/apache/lucene/util/mutable/MutableValue;)V
    .locals 2
    .param p1, "source"    # Lorg/apache/lucene/util/mutable/MutableValue;

    .prologue
    .line 33
    move-object v0, p1

    check-cast v0, Lorg/apache/lucene/util/mutable/MutableValueFloat;

    .line 34
    .local v0, "s":Lorg/apache/lucene/util/mutable/MutableValueFloat;
    iget v1, v0, Lorg/apache/lucene/util/mutable/MutableValueFloat;->value:F

    iput v1, p0, Lorg/apache/lucene/util/mutable/MutableValueFloat;->value:F

    .line 35
    iget-boolean v1, v0, Lorg/apache/lucene/util/mutable/MutableValueFloat;->exists:Z

    iput-boolean v1, p0, Lorg/apache/lucene/util/mutable/MutableValueFloat;->exists:Z

    .line 36
    return-void
.end method

.method public duplicate()Lorg/apache/lucene/util/mutable/MutableValue;
    .locals 2

    .prologue
    .line 40
    new-instance v0, Lorg/apache/lucene/util/mutable/MutableValueFloat;

    invoke-direct {v0}, Lorg/apache/lucene/util/mutable/MutableValueFloat;-><init>()V

    .line 41
    .local v0, "v":Lorg/apache/lucene/util/mutable/MutableValueFloat;
    iget v1, p0, Lorg/apache/lucene/util/mutable/MutableValueFloat;->value:F

    iput v1, v0, Lorg/apache/lucene/util/mutable/MutableValueFloat;->value:F

    .line 42
    iget-boolean v1, p0, Lorg/apache/lucene/util/mutable/MutableValueFloat;->exists:Z

    iput-boolean v1, v0, Lorg/apache/lucene/util/mutable/MutableValueFloat;->exists:Z

    .line 43
    return-object v0
.end method

.method public equalsSameType(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 48
    move-object v0, p1

    check-cast v0, Lorg/apache/lucene/util/mutable/MutableValueFloat;

    .line 49
    .local v0, "b":Lorg/apache/lucene/util/mutable/MutableValueFloat;
    iget v1, p0, Lorg/apache/lucene/util/mutable/MutableValueFloat;->value:F

    iget v2, v0, Lorg/apache/lucene/util/mutable/MutableValueFloat;->value:F

    cmpl-float v1, v1, v2

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lorg/apache/lucene/util/mutable/MutableValueFloat;->exists:Z

    iget-boolean v2, v0, Lorg/apache/lucene/util/mutable/MutableValueFloat;->exists:Z

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 63
    iget v0, p0, Lorg/apache/lucene/util/mutable/MutableValueFloat;->value:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    return v0
.end method

.method public toObject()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 28
    iget-boolean v0, p0, Lorg/apache/lucene/util/mutable/MutableValueFloat;->exists:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/util/mutable/MutableValueFloat;->value:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
