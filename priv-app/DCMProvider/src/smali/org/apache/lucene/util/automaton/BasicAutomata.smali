.class public final Lorg/apache/lucene/util/automaton/BasicAutomata;
.super Ljava/lang/Object;
.source "BasicAutomata.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static anyOfRightLength(Ljava/lang/String;I)Lorg/apache/lucene/util/automaton/State;
    .locals 5
    .param p0, "x"    # Ljava/lang/String;
    .param p1, "n"    # I

    .prologue
    .line 119
    new-instance v0, Lorg/apache/lucene/util/automaton/State;

    invoke-direct {v0}, Lorg/apache/lucene/util/automaton/State;-><init>()V

    .line 120
    .local v0, "s":Lorg/apache/lucene/util/automaton/State;
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-ne v1, p1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/automaton/State;->setAccept(Z)V

    .line 122
    :goto_0
    return-object v0

    .line 121
    :cond_0
    new-instance v1, Lorg/apache/lucene/util/automaton/Transition;

    const/16 v2, 0x30

    const/16 v3, 0x39

    add-int/lit8 v4, p1, 0x1

    invoke-static {p0, v4}, Lorg/apache/lucene/util/automaton/BasicAutomata;->anyOfRightLength(Ljava/lang/String;I)Lorg/apache/lucene/util/automaton/State;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lorg/apache/lucene/util/automaton/Transition;-><init>(IILorg/apache/lucene/util/automaton/State;)V

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/automaton/State;->addTransition(Lorg/apache/lucene/util/automaton/Transition;)V

    goto :goto_0
.end method

.method private static atLeast(Ljava/lang/String;ILjava/util/Collection;Z)Lorg/apache/lucene/util/automaton/State;
    .locals 7
    .param p0, "x"    # Ljava/lang/String;
    .param p1, "n"    # I
    .param p3, "zeros"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/util/automaton/State;",
            ">;Z)",
            "Lorg/apache/lucene/util/automaton/State;"
        }
    .end annotation

    .prologue
    .local p2, "initials":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/util/automaton/State;>;"
    const/16 v6, 0x39

    const/4 v2, 0x1

    .line 131
    new-instance v1, Lorg/apache/lucene/util/automaton/State;

    invoke-direct {v1}, Lorg/apache/lucene/util/automaton/State;-><init>()V

    .line 132
    .local v1, "s":Lorg/apache/lucene/util/automaton/State;
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-ne v3, p1, :cond_1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/automaton/State;->setAccept(Z)V

    .line 141
    :cond_0
    :goto_0
    return-object v1

    .line 134
    :cond_1
    if-eqz p3, :cond_2

    invoke-interface {p2, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 135
    :cond_2
    invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 136
    .local v0, "c":C
    new-instance v3, Lorg/apache/lucene/util/automaton/Transition;

    add-int/lit8 v4, p1, 0x1

    if-eqz p3, :cond_3

    .line 137
    const/16 v5, 0x30

    if-ne v0, v5, :cond_3

    .line 136
    :goto_1
    invoke-static {p0, v4, p2, v2}, Lorg/apache/lucene/util/automaton/BasicAutomata;->atLeast(Ljava/lang/String;ILjava/util/Collection;Z)Lorg/apache/lucene/util/automaton/State;

    move-result-object v2

    invoke-direct {v3, v0, v2}, Lorg/apache/lucene/util/automaton/Transition;-><init>(ILorg/apache/lucene/util/automaton/State;)V

    invoke-virtual {v1, v3}, Lorg/apache/lucene/util/automaton/State;->addTransition(Lorg/apache/lucene/util/automaton/Transition;)V

    .line 138
    if-ge v0, v6, :cond_0

    new-instance v2, Lorg/apache/lucene/util/automaton/Transition;

    add-int/lit8 v3, v0, 0x1

    int-to-char v3, v3

    .line 139
    add-int/lit8 v4, p1, 0x1

    invoke-static {p0, v4}, Lorg/apache/lucene/util/automaton/BasicAutomata;->anyOfRightLength(Ljava/lang/String;I)Lorg/apache/lucene/util/automaton/State;

    move-result-object v4

    invoke-direct {v2, v3, v6, v4}, Lorg/apache/lucene/util/automaton/Transition;-><init>(IILorg/apache/lucene/util/automaton/State;)V

    .line 138
    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/automaton/State;->addTransition(Lorg/apache/lucene/util/automaton/Transition;)V

    goto :goto_0

    .line 137
    :cond_3
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private static atMost(Ljava/lang/String;I)Lorg/apache/lucene/util/automaton/State;
    .locals 6
    .param p0, "x"    # Ljava/lang/String;
    .param p1, "n"    # I

    .prologue
    const/16 v5, 0x30

    .line 149
    new-instance v1, Lorg/apache/lucene/util/automaton/State;

    invoke-direct {v1}, Lorg/apache/lucene/util/automaton/State;-><init>()V

    .line 150
    .local v1, "s":Lorg/apache/lucene/util/automaton/State;
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-ne v2, p1, :cond_1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/automaton/State;->setAccept(Z)V

    .line 157
    :cond_0
    :goto_0
    return-object v1

    .line 152
    :cond_1
    invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 153
    .local v0, "c":C
    new-instance v2, Lorg/apache/lucene/util/automaton/Transition;

    int-to-char v3, p1

    add-int/lit8 v3, v3, 0x1

    invoke-static {p0, v3}, Lorg/apache/lucene/util/automaton/BasicAutomata;->atMost(Ljava/lang/String;I)Lorg/apache/lucene/util/automaton/State;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Lorg/apache/lucene/util/automaton/Transition;-><init>(ILorg/apache/lucene/util/automaton/State;)V

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/automaton/State;->addTransition(Lorg/apache/lucene/util/automaton/Transition;)V

    .line 154
    if-le v0, v5, :cond_0

    new-instance v2, Lorg/apache/lucene/util/automaton/Transition;

    add-int/lit8 v3, v0, -0x1

    int-to-char v3, v3

    .line 155
    add-int/lit8 v4, p1, 0x1

    invoke-static {p0, v4}, Lorg/apache/lucene/util/automaton/BasicAutomata;->anyOfRightLength(Ljava/lang/String;I)Lorg/apache/lucene/util/automaton/State;

    move-result-object v4

    invoke-direct {v2, v5, v3, v4}, Lorg/apache/lucene/util/automaton/Transition;-><init>(IILorg/apache/lucene/util/automaton/State;)V

    .line 154
    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/automaton/State;->addTransition(Lorg/apache/lucene/util/automaton/Transition;)V

    goto :goto_0
.end method

.method private static between(Ljava/lang/String;Ljava/lang/String;ILjava/util/Collection;Z)Lorg/apache/lucene/util/automaton/State;
    .locals 8
    .param p0, "x"    # Ljava/lang/String;
    .param p1, "y"    # Ljava/lang/String;
    .param p2, "n"    # I
    .param p4, "zeros"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/util/automaton/State;",
            ">;Z)",
            "Lorg/apache/lucene/util/automaton/State;"
        }
    .end annotation

    .prologue
    .local p3, "initials":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/util/automaton/State;>;"
    const/16 v7, 0x30

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 167
    new-instance v2, Lorg/apache/lucene/util/automaton/State;

    invoke-direct {v2}, Lorg/apache/lucene/util/automaton/State;-><init>()V

    .line 168
    .local v2, "s":Lorg/apache/lucene/util/automaton/State;
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    if-ne v5, p2, :cond_1

    invoke-virtual {v2, v3}, Lorg/apache/lucene/util/automaton/State;->setAccept(Z)V

    .line 183
    :cond_0
    :goto_0
    return-object v2

    .line 170
    :cond_1
    if-eqz p4, :cond_2

    invoke-interface {p3, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 171
    :cond_2
    invoke-virtual {p0, p2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 172
    .local v0, "cx":C
    invoke-virtual {p1, p2}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 173
    .local v1, "cy":C
    if-ne v0, v1, :cond_4

    new-instance v5, Lorg/apache/lucene/util/automaton/Transition;

    add-int/lit8 v6, p2, 0x1

    .line 174
    if-eqz p4, :cond_3

    if-ne v0, v7, :cond_3

    .line 173
    :goto_1
    invoke-static {p0, p1, v6, p3, v3}, Lorg/apache/lucene/util/automaton/BasicAutomata;->between(Ljava/lang/String;Ljava/lang/String;ILjava/util/Collection;Z)Lorg/apache/lucene/util/automaton/State;

    move-result-object v3

    invoke-direct {v5, v0, v3}, Lorg/apache/lucene/util/automaton/Transition;-><init>(ILorg/apache/lucene/util/automaton/State;)V

    invoke-virtual {v2, v5}, Lorg/apache/lucene/util/automaton/State;->addTransition(Lorg/apache/lucene/util/automaton/Transition;)V

    goto :goto_0

    :cond_3
    move v3, v4

    .line 174
    goto :goto_1

    .line 176
    :cond_4
    new-instance v5, Lorg/apache/lucene/util/automaton/Transition;

    add-int/lit8 v6, p2, 0x1

    if-eqz p4, :cond_5

    .line 177
    if-ne v0, v7, :cond_5

    .line 176
    :goto_2
    invoke-static {p0, v6, p3, v3}, Lorg/apache/lucene/util/automaton/BasicAutomata;->atLeast(Ljava/lang/String;ILjava/util/Collection;Z)Lorg/apache/lucene/util/automaton/State;

    move-result-object v3

    invoke-direct {v5, v0, v3}, Lorg/apache/lucene/util/automaton/Transition;-><init>(ILorg/apache/lucene/util/automaton/State;)V

    invoke-virtual {v2, v5}, Lorg/apache/lucene/util/automaton/State;->addTransition(Lorg/apache/lucene/util/automaton/Transition;)V

    .line 178
    new-instance v3, Lorg/apache/lucene/util/automaton/Transition;

    add-int/lit8 v4, p2, 0x1

    invoke-static {p1, v4}, Lorg/apache/lucene/util/automaton/BasicAutomata;->atMost(Ljava/lang/String;I)Lorg/apache/lucene/util/automaton/State;

    move-result-object v4

    invoke-direct {v3, v1, v4}, Lorg/apache/lucene/util/automaton/Transition;-><init>(ILorg/apache/lucene/util/automaton/State;)V

    invoke-virtual {v2, v3}, Lorg/apache/lucene/util/automaton/State;->addTransition(Lorg/apache/lucene/util/automaton/Transition;)V

    .line 179
    add-int/lit8 v3, v0, 0x1

    if-ge v3, v1, :cond_0

    new-instance v3, Lorg/apache/lucene/util/automaton/Transition;

    add-int/lit8 v4, v0, 0x1

    int-to-char v4, v4

    .line 180
    add-int/lit8 v5, v1, -0x1

    int-to-char v5, v5

    add-int/lit8 v6, p2, 0x1

    invoke-static {p0, v6}, Lorg/apache/lucene/util/automaton/BasicAutomata;->anyOfRightLength(Ljava/lang/String;I)Lorg/apache/lucene/util/automaton/State;

    move-result-object v6

    invoke-direct {v3, v4, v5, v6}, Lorg/apache/lucene/util/automaton/Transition;-><init>(IILorg/apache/lucene/util/automaton/State;)V

    .line 179
    invoke-virtual {v2, v3}, Lorg/apache/lucene/util/automaton/State;->addTransition(Lorg/apache/lucene/util/automaton/Transition;)V

    goto :goto_0

    :cond_5
    move v3, v4

    .line 177
    goto :goto_2
.end method

.method public static makeAnyChar()Lorg/apache/lucene/util/automaton/Automaton;
    .locals 2

    .prologue
    .line 84
    const/4 v0, 0x0

    const v1, 0x10ffff

    invoke-static {v0, v1}, Lorg/apache/lucene/util/automaton/BasicAutomata;->makeCharRange(II)Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v0

    return-object v0
.end method

.method public static makeAnyString()Lorg/apache/lucene/util/automaton/Automaton;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 70
    new-instance v0, Lorg/apache/lucene/util/automaton/Automaton;

    invoke-direct {v0}, Lorg/apache/lucene/util/automaton/Automaton;-><init>()V

    .line 71
    .local v0, "a":Lorg/apache/lucene/util/automaton/Automaton;
    new-instance v1, Lorg/apache/lucene/util/automaton/State;

    invoke-direct {v1}, Lorg/apache/lucene/util/automaton/State;-><init>()V

    .line 72
    .local v1, "s":Lorg/apache/lucene/util/automaton/State;
    iput-object v1, v0, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    .line 73
    iput-boolean v5, v1, Lorg/apache/lucene/util/automaton/State;->accept:Z

    .line 74
    new-instance v2, Lorg/apache/lucene/util/automaton/Transition;

    const/4 v3, 0x0

    const v4, 0x10ffff

    .line 75
    invoke-direct {v2, v3, v4, v1}, Lorg/apache/lucene/util/automaton/Transition;-><init>(IILorg/apache/lucene/util/automaton/State;)V

    .line 74
    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/automaton/State;->addTransition(Lorg/apache/lucene/util/automaton/Transition;)V

    .line 76
    iput-boolean v5, v0, Lorg/apache/lucene/util/automaton/Automaton;->deterministic:Z

    .line 77
    return-object v0
.end method

.method public static makeChar(I)Lorg/apache/lucene/util/automaton/Automaton;
    .locals 3
    .param p0, "c"    # I

    .prologue
    .line 92
    new-instance v0, Lorg/apache/lucene/util/automaton/Automaton;

    invoke-direct {v0}, Lorg/apache/lucene/util/automaton/Automaton;-><init>()V

    .line 93
    .local v0, "a":Lorg/apache/lucene/util/automaton/Automaton;
    new-instance v1, Ljava/lang/String;

    invoke-static {p0}, Ljava/lang/Character;->toChars(I)[C

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>([C)V

    iput-object v1, v0, Lorg/apache/lucene/util/automaton/Automaton;->singleton:Ljava/lang/String;

    .line 94
    const/4 v1, 0x1

    iput-boolean v1, v0, Lorg/apache/lucene/util/automaton/Automaton;->deterministic:Z

    .line 95
    return-object v0
.end method

.method public static makeCharRange(II)Lorg/apache/lucene/util/automaton/Automaton;
    .locals 5
    .param p0, "min"    # I
    .param p1, "max"    # I

    .prologue
    const/4 v4, 0x1

    .line 103
    if-ne p0, p1, :cond_0

    invoke-static {p0}, Lorg/apache/lucene/util/automaton/BasicAutomata;->makeChar(I)Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v0

    .line 111
    :goto_0
    return-object v0

    .line 104
    :cond_0
    new-instance v0, Lorg/apache/lucene/util/automaton/Automaton;

    invoke-direct {v0}, Lorg/apache/lucene/util/automaton/Automaton;-><init>()V

    .line 105
    .local v0, "a":Lorg/apache/lucene/util/automaton/Automaton;
    new-instance v1, Lorg/apache/lucene/util/automaton/State;

    invoke-direct {v1}, Lorg/apache/lucene/util/automaton/State;-><init>()V

    .line 106
    .local v1, "s1":Lorg/apache/lucene/util/automaton/State;
    new-instance v2, Lorg/apache/lucene/util/automaton/State;

    invoke-direct {v2}, Lorg/apache/lucene/util/automaton/State;-><init>()V

    .line 107
    .local v2, "s2":Lorg/apache/lucene/util/automaton/State;
    iput-object v1, v0, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    .line 108
    iput-boolean v4, v2, Lorg/apache/lucene/util/automaton/State;->accept:Z

    .line 109
    if-gt p0, p1, :cond_1

    new-instance v3, Lorg/apache/lucene/util/automaton/Transition;

    invoke-direct {v3, p0, p1, v2}, Lorg/apache/lucene/util/automaton/Transition;-><init>(IILorg/apache/lucene/util/automaton/State;)V

    invoke-virtual {v1, v3}, Lorg/apache/lucene/util/automaton/State;->addTransition(Lorg/apache/lucene/util/automaton/Transition;)V

    .line 110
    :cond_1
    iput-boolean v4, v0, Lorg/apache/lucene/util/automaton/Automaton;->deterministic:Z

    goto :goto_0
.end method

.method public static makeEmpty()Lorg/apache/lucene/util/automaton/Automaton;
    .locals 3

    .prologue
    .line 49
    new-instance v0, Lorg/apache/lucene/util/automaton/Automaton;

    invoke-direct {v0}, Lorg/apache/lucene/util/automaton/Automaton;-><init>()V

    .line 50
    .local v0, "a":Lorg/apache/lucene/util/automaton/Automaton;
    new-instance v1, Lorg/apache/lucene/util/automaton/State;

    invoke-direct {v1}, Lorg/apache/lucene/util/automaton/State;-><init>()V

    .line 51
    .local v1, "s":Lorg/apache/lucene/util/automaton/State;
    iput-object v1, v0, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    .line 52
    const/4 v2, 0x1

    iput-boolean v2, v0, Lorg/apache/lucene/util/automaton/Automaton;->deterministic:Z

    .line 53
    return-object v0
.end method

.method public static makeEmptyString()Lorg/apache/lucene/util/automaton/Automaton;
    .locals 2

    .prologue
    .line 60
    new-instance v0, Lorg/apache/lucene/util/automaton/Automaton;

    invoke-direct {v0}, Lorg/apache/lucene/util/automaton/Automaton;-><init>()V

    .line 61
    .local v0, "a":Lorg/apache/lucene/util/automaton/Automaton;
    const-string v1, ""

    iput-object v1, v0, Lorg/apache/lucene/util/automaton/Automaton;->singleton:Ljava/lang/String;

    .line 62
    const/4 v1, 0x1

    iput-boolean v1, v0, Lorg/apache/lucene/util/automaton/Automaton;->deterministic:Z

    .line 63
    return-object v0
.end method

.method public static makeInterval(III)Lorg/apache/lucene/util/automaton/Automaton;
    .locals 15
    .param p0, "min"    # I
    .param p1, "max"    # I
    .param p2, "digits"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 202
    new-instance v1, Lorg/apache/lucene/util/automaton/Automaton;

    invoke-direct {v1}, Lorg/apache/lucene/util/automaton/Automaton;-><init>()V

    .line 203
    .local v1, "a":Lorg/apache/lucene/util/automaton/Automaton;
    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    .line 204
    .local v9, "x":Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v10

    .line 205
    .local v10, "y":Ljava/lang/String;
    move/from16 v0, p1

    if-gt p0, v0, :cond_0

    if-lez p2, :cond_1

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v11

    move/from16 v0, p2

    if-le v11, v0, :cond_1

    :cond_0
    new-instance v11, Ljava/lang/IllegalArgumentException;

    invoke-direct {v11}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v11

    .line 207
    :cond_1
    if-lez p2, :cond_3

    move/from16 v4, p2

    .line 209
    .local v4, "d":I
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 210
    .local v2, "bx":Ljava/lang/StringBuilder;
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v5

    .local v5, "i":I
    :goto_1
    if-lt v5, v4, :cond_4

    .line 212
    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 213
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 214
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 215
    .local v3, "by":Ljava/lang/StringBuilder;
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v5

    :goto_2
    if-lt v5, v4, :cond_5

    .line 217
    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 218
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 219
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 220
    .local v6, "initials":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/util/automaton/State;>;"
    const/4 v12, 0x0

    if-gtz p2, :cond_6

    const/4 v11, 0x1

    :goto_3
    invoke-static {v9, v10, v12, v6, v11}, Lorg/apache/lucene/util/automaton/BasicAutomata;->between(Ljava/lang/String;Ljava/lang/String;ILjava/util/Collection;Z)Lorg/apache/lucene/util/automaton/State;

    move-result-object v11

    iput-object v11, v1, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    .line 221
    if-gtz p2, :cond_8

    .line 222
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 223
    .local v8, "pairs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/util/automaton/StatePair;>;"
    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_2
    :goto_4
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_7

    .line 225
    invoke-static {v1, v8}, Lorg/apache/lucene/util/automaton/BasicOperations;->addEpsilons(Lorg/apache/lucene/util/automaton/Automaton;Ljava/util/Collection;)V

    .line 226
    iget-object v11, v1, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    new-instance v12, Lorg/apache/lucene/util/automaton/Transition;

    const/16 v13, 0x30

    iget-object v14, v1, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    invoke-direct {v12, v13, v14}, Lorg/apache/lucene/util/automaton/Transition;-><init>(ILorg/apache/lucene/util/automaton/State;)V

    invoke-virtual {v11, v12}, Lorg/apache/lucene/util/automaton/State;->addTransition(Lorg/apache/lucene/util/automaton/Transition;)V

    .line 227
    const/4 v11, 0x0

    iput-boolean v11, v1, Lorg/apache/lucene/util/automaton/Automaton;->deterministic:Z

    .line 229
    .end local v8    # "pairs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/util/automaton/StatePair;>;"
    :goto_5
    invoke-virtual {v1}, Lorg/apache/lucene/util/automaton/Automaton;->checkMinimizeAlways()V

    .line 230
    return-object v1

    .line 208
    .end local v2    # "bx":Ljava/lang/StringBuilder;
    .end local v3    # "by":Ljava/lang/StringBuilder;
    .end local v4    # "d":I
    .end local v5    # "i":I
    .end local v6    # "initials":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/util/automaton/State;>;"
    :cond_3
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v4

    .restart local v4    # "d":I
    goto :goto_0

    .line 211
    .restart local v2    # "bx":Ljava/lang/StringBuilder;
    .restart local v5    # "i":I
    :cond_4
    const/16 v11, 0x30

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 210
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 216
    .restart local v3    # "by":Ljava/lang/StringBuilder;
    :cond_5
    const/16 v11, 0x30

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 215
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 220
    .restart local v6    # "initials":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/util/automaton/State;>;"
    :cond_6
    const/4 v11, 0x0

    goto :goto_3

    .line 223
    .restart local v8    # "pairs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/util/automaton/StatePair;>;"
    :cond_7
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/lucene/util/automaton/State;

    .line 224
    .local v7, "p":Lorg/apache/lucene/util/automaton/State;
    iget-object v12, v1, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    if-eq v12, v7, :cond_2

    new-instance v12, Lorg/apache/lucene/util/automaton/StatePair;

    iget-object v13, v1, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    invoke-direct {v12, v13, v7}, Lorg/apache/lucene/util/automaton/StatePair;-><init>(Lorg/apache/lucene/util/automaton/State;Lorg/apache/lucene/util/automaton/State;)V

    invoke-virtual {v8, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 228
    .end local v7    # "p":Lorg/apache/lucene/util/automaton/State;
    .end local v8    # "pairs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/util/automaton/StatePair;>;"
    :cond_8
    const/4 v11, 0x1

    iput-boolean v11, v1, Lorg/apache/lucene/util/automaton/Automaton;->deterministic:Z

    goto :goto_5
.end method

.method public static makeString(Ljava/lang/String;)Lorg/apache/lucene/util/automaton/Automaton;
    .locals 2
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 238
    new-instance v0, Lorg/apache/lucene/util/automaton/Automaton;

    invoke-direct {v0}, Lorg/apache/lucene/util/automaton/Automaton;-><init>()V

    .line 239
    .local v0, "a":Lorg/apache/lucene/util/automaton/Automaton;
    iput-object p0, v0, Lorg/apache/lucene/util/automaton/Automaton;->singleton:Ljava/lang/String;

    .line 240
    const/4 v1, 0x1

    iput-boolean v1, v0, Lorg/apache/lucene/util/automaton/Automaton;->deterministic:Z

    .line 241
    return-object v0
.end method

.method public static makeString([III)Lorg/apache/lucene/util/automaton/Automaton;
    .locals 7
    .param p0, "word"    # [I
    .param p1, "offset"    # I
    .param p2, "length"    # I

    .prologue
    const/4 v6, 0x1

    .line 245
    new-instance v0, Lorg/apache/lucene/util/automaton/Automaton;

    invoke-direct {v0}, Lorg/apache/lucene/util/automaton/Automaton;-><init>()V

    .line 246
    .local v0, "a":Lorg/apache/lucene/util/automaton/Automaton;
    invoke-virtual {v0, v6}, Lorg/apache/lucene/util/automaton/Automaton;->setDeterministic(Z)V

    .line 247
    new-instance v2, Lorg/apache/lucene/util/automaton/State;

    invoke-direct {v2}, Lorg/apache/lucene/util/automaton/State;-><init>()V

    .line 248
    .local v2, "s":Lorg/apache/lucene/util/automaton/State;
    iput-object v2, v0, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    .line 249
    move v1, p1

    .local v1, "i":I
    :goto_0
    add-int v4, p1, p2

    if-lt v1, v4, :cond_0

    .line 254
    iput-boolean v6, v2, Lorg/apache/lucene/util/automaton/State;->accept:Z

    .line 255
    return-object v0

    .line 250
    :cond_0
    new-instance v3, Lorg/apache/lucene/util/automaton/State;

    invoke-direct {v3}, Lorg/apache/lucene/util/automaton/State;-><init>()V

    .line 251
    .local v3, "s2":Lorg/apache/lucene/util/automaton/State;
    new-instance v4, Lorg/apache/lucene/util/automaton/Transition;

    aget v5, p0, v1

    invoke-direct {v4, v5, v3}, Lorg/apache/lucene/util/automaton/Transition;-><init>(ILorg/apache/lucene/util/automaton/State;)V

    invoke-virtual {v2, v4}, Lorg/apache/lucene/util/automaton/State;->addTransition(Lorg/apache/lucene/util/automaton/Transition;)V

    .line 252
    move-object v2, v3

    .line 249
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static makeStringUnion(Ljava/util/Collection;)Lorg/apache/lucene/util/automaton/Automaton;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;)",
            "Lorg/apache/lucene/util/automaton/Automaton;"
        }
    .end annotation

    .prologue
    .line 272
    .local p0, "utf8Strings":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/util/BytesRef;>;"
    invoke-interface {p0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 273
    invoke-static {}, Lorg/apache/lucene/util/automaton/BasicAutomata;->makeEmpty()Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v0

    .line 275
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder;->build(Ljava/util/Collection;)Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v0

    goto :goto_0
.end method
