.class public abstract Lorg/apache/lucene/util/InfoStream;
.super Ljava/lang/Object;
.source "InfoStream.java"

# interfaces
.implements Ljava/io/Closeable;
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/InfoStream$NoOutput;
    }
.end annotation


# static fields
.field public static final NO_OUTPUT:Lorg/apache/lucene/util/InfoStream;

.field private static defaultInfoStream:Lorg/apache/lucene/util/InfoStream;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 36
    new-instance v0, Lorg/apache/lucene/util/InfoStream$NoOutput;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/InfoStream$NoOutput;-><init>(Lorg/apache/lucene/util/InfoStream$NoOutput;)V

    sput-object v0, Lorg/apache/lucene/util/InfoStream;->NO_OUTPUT:Lorg/apache/lucene/util/InfoStream;

    .line 58
    sget-object v0, Lorg/apache/lucene/util/InfoStream;->NO_OUTPUT:Lorg/apache/lucene/util/InfoStream;

    sput-object v0, Lorg/apache/lucene/util/InfoStream;->defaultInfoStream:Lorg/apache/lucene/util/InfoStream;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized getDefault()Lorg/apache/lucene/util/InfoStream;
    .locals 2

    .prologue
    .line 63
    const-class v0, Lorg/apache/lucene/util/InfoStream;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lorg/apache/lucene/util/InfoStream;->defaultInfoStream:Lorg/apache/lucene/util/InfoStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized setDefault(Lorg/apache/lucene/util/InfoStream;)V
    .locals 3
    .param p0, "infoStream"    # Lorg/apache/lucene/util/InfoStream;

    .prologue
    .line 71
    const-class v1, Lorg/apache/lucene/util/InfoStream;

    monitor-enter v1

    if-nez p0, :cond_0

    .line 72
    :try_start_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v2, "Cannot set InfoStream default implementation to null. To disable logging use InfoStream.NO_OUTPUT"

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 71
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 75
    :cond_0
    :try_start_1
    sput-object p0, Lorg/apache/lucene/util/InfoStream;->defaultInfoStream:Lorg/apache/lucene/util/InfoStream;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 76
    monitor-exit v1

    return-void
.end method


# virtual methods
.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/util/InfoStream;->clone()Lorg/apache/lucene/util/InfoStream;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lorg/apache/lucene/util/InfoStream;
    .locals 2

    .prologue
    .line 81
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/util/InfoStream;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 82
    :catch_0
    move-exception v0

    .line 83
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v1, Ljava/lang/Error;

    invoke-direct {v1, v0}, Ljava/lang/Error;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public abstract isEnabled(Ljava/lang/String;)Z
.end method

.method public abstract message(Ljava/lang/String;Ljava/lang/String;)V
.end method
