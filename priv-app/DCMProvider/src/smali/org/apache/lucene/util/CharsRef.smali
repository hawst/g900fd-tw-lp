.class public final Lorg/apache/lucene/util/CharsRef;
.super Ljava/lang/Object;
.source "CharsRef.java"

# interfaces
.implements Ljava/lang/CharSequence;
.implements Ljava/lang/Cloneable;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/CharsRef$UTF16SortedAsUTF8Comparator;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lorg/apache/lucene/util/CharsRef;",
        ">;",
        "Ljava/lang/CharSequence;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final EMPTY_CHARS:[C

.field private static final utf16SortedAsUTF8SortOrder:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/CharsRef;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# instance fields
.field public chars:[C

.field public length:I

.field public offset:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 28
    const-class v0, Lorg/apache/lucene/util/CharsRef;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/CharsRef;->$assertionsDisabled:Z

    .line 30
    new-array v0, v1, [C

    sput-object v0, Lorg/apache/lucene/util/CharsRef;->EMPTY_CHARS:[C

    .line 223
    new-instance v0, Lorg/apache/lucene/util/CharsRef$UTF16SortedAsUTF8Comparator;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/CharsRef$UTF16SortedAsUTF8Comparator;-><init>(Lorg/apache/lucene/util/CharsRef$UTF16SortedAsUTF8Comparator;)V

    sput-object v0, Lorg/apache/lucene/util/CharsRef;->utf16SortedAsUTF8SortOrder:Ljava/util/Comparator;

    return-void

    :cond_0
    move v0, v1

    .line 28
    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 42
    sget-object v0, Lorg/apache/lucene/util/CharsRef;->EMPTY_CHARS:[C

    invoke-direct {p0, v0, v1, v1}, Lorg/apache/lucene/util/CharsRef;-><init>([CII)V

    .line 43
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "capacity"    # I

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    new-array v0, p1, [C

    iput-object v0, p0, Lorg/apache/lucene/util/CharsRef;->chars:[C

    .line 51
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/util/CharsRef;->chars:[C

    .line 70
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/util/CharsRef;->offset:I

    .line 71
    iget-object v0, p0, Lorg/apache/lucene/util/CharsRef;->chars:[C

    array-length v0, v0

    iput v0, p0, Lorg/apache/lucene/util/CharsRef;->length:I

    .line 72
    return-void
.end method

.method public constructor <init>([CII)V
    .locals 1
    .param p1, "chars"    # [C
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p1, p0, Lorg/apache/lucene/util/CharsRef;->chars:[C

    .line 59
    iput p2, p0, Lorg/apache/lucene/util/CharsRef;->offset:I

    .line 60
    iput p3, p0, Lorg/apache/lucene/util/CharsRef;->length:I

    .line 61
    sget-boolean v0, Lorg/apache/lucene/util/CharsRef;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/util/CharsRef;->isValid()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 62
    :cond_0
    return-void
.end method

.method public static deepCopyOf(Lorg/apache/lucene/util/CharsRef;)Lorg/apache/lucene/util/CharsRef;
    .locals 1
    .param p0, "other"    # Lorg/apache/lucene/util/CharsRef;

    .prologue
    .line 288
    new-instance v0, Lorg/apache/lucene/util/CharsRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/CharsRef;-><init>()V

    .line 289
    .local v0, "clone":Lorg/apache/lucene/util/CharsRef;
    invoke-virtual {v0, p0}, Lorg/apache/lucene/util/CharsRef;->copyChars(Lorg/apache/lucene/util/CharsRef;)V

    .line 290
    return-object v0
.end method

.method public static getUTF16SortedAsUTF8Comparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/CharsRef;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 228
    sget-object v0, Lorg/apache/lucene/util/CharsRef;->utf16SortedAsUTF8SortOrder:Ljava/util/Comparator;

    return-object v0
.end method


# virtual methods
.method public append([CII)V
    .locals 6
    .param p1, "otherChars"    # [C
    .param p2, "otherOffset"    # I
    .param p3, "otherLength"    # I

    .prologue
    const/4 v5, 0x0

    .line 182
    iget v2, p0, Lorg/apache/lucene/util/CharsRef;->length:I

    add-int v1, v2, p3

    .line 183
    .local v1, "newLen":I
    iget-object v2, p0, Lorg/apache/lucene/util/CharsRef;->chars:[C

    array-length v2, v2

    iget v3, p0, Lorg/apache/lucene/util/CharsRef;->offset:I

    sub-int/2addr v2, v3

    if-ge v2, v1, :cond_0

    .line 184
    new-array v0, v1, [C

    .line 185
    .local v0, "newChars":[C
    iget-object v2, p0, Lorg/apache/lucene/util/CharsRef;->chars:[C

    iget v3, p0, Lorg/apache/lucene/util/CharsRef;->offset:I

    iget v4, p0, Lorg/apache/lucene/util/CharsRef;->length:I

    invoke-static {v2, v3, v0, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 186
    iput v5, p0, Lorg/apache/lucene/util/CharsRef;->offset:I

    .line 187
    iput-object v0, p0, Lorg/apache/lucene/util/CharsRef;->chars:[C

    .line 189
    .end local v0    # "newChars":[C
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/util/CharsRef;->chars:[C

    iget v3, p0, Lorg/apache/lucene/util/CharsRef;->length:I

    iget v4, p0, Lorg/apache/lucene/util/CharsRef;->offset:I

    add-int/2addr v3, v4

    invoke-static {p1, p2, v2, v3, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 190
    iput v1, p0, Lorg/apache/lucene/util/CharsRef;->length:I

    .line 191
    return-void
.end method

.method public charAt(I)C
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 206
    if-ltz p1, :cond_0

    iget v0, p0, Lorg/apache/lucene/util/CharsRef;->length:I

    if-lt p1, v0, :cond_1

    .line 207
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0

    .line 209
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/util/CharsRef;->chars:[C

    iget v1, p0, Lorg/apache/lucene/util/CharsRef;->offset:I

    add-int/2addr v1, p1

    aget-char v0, v0, v1

    return v0
.end method

.method public charsEquals(Lorg/apache/lucene/util/CharsRef;)Z
    .locals 7
    .param p1, "other"    # Lorg/apache/lucene/util/CharsRef;

    .prologue
    const/4 v4, 0x0

    .line 102
    iget v5, p0, Lorg/apache/lucene/util/CharsRef;->length:I

    iget v6, p1, Lorg/apache/lucene/util/CharsRef;->length:I

    if-ne v5, v6, :cond_0

    .line 103
    iget v2, p1, Lorg/apache/lucene/util/CharsRef;->offset:I

    .line 104
    .local v2, "otherUpto":I
    iget-object v1, p1, Lorg/apache/lucene/util/CharsRef;->chars:[C

    .line 105
    .local v1, "otherChars":[C
    iget v5, p0, Lorg/apache/lucene/util/CharsRef;->offset:I

    iget v6, p0, Lorg/apache/lucene/util/CharsRef;->length:I

    add-int v0, v5, v6

    .line 106
    .local v0, "end":I
    iget v3, p0, Lorg/apache/lucene/util/CharsRef;->offset:I

    .local v3, "upto":I
    :goto_0
    if-lt v3, v0, :cond_1

    .line 111
    const/4 v4, 0x1

    .line 113
    .end local v0    # "end":I
    .end local v1    # "otherChars":[C
    .end local v2    # "otherUpto":I
    .end local v3    # "upto":I
    :cond_0
    return v4

    .line 107
    .restart local v0    # "end":I
    .restart local v1    # "otherChars":[C
    .restart local v2    # "otherUpto":I
    .restart local v3    # "upto":I
    :cond_1
    iget-object v5, p0, Lorg/apache/lucene/util/CharsRef;->chars:[C

    aget-char v5, v5, v3

    aget-char v6, v1, v2

    if-ne v5, v6, :cond_0

    .line 106
    add-int/lit8 v3, v3, 0x1

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/util/CharsRef;->clone()Lorg/apache/lucene/util/CharsRef;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lorg/apache/lucene/util/CharsRef;
    .locals 4

    .prologue
    .line 76
    new-instance v0, Lorg/apache/lucene/util/CharsRef;

    iget-object v1, p0, Lorg/apache/lucene/util/CharsRef;->chars:[C

    iget v2, p0, Lorg/apache/lucene/util/CharsRef;->offset:I

    iget v3, p0, Lorg/apache/lucene/util/CharsRef;->length:I

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/util/CharsRef;-><init>([CII)V

    return-object v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/util/CharsRef;

    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/CharsRef;->compareTo(Lorg/apache/lucene/util/CharsRef;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lorg/apache/lucene/util/CharsRef;)I
    .locals 11
    .param p1, "other"    # Lorg/apache/lucene/util/CharsRef;

    .prologue
    .line 120
    if-ne p0, p1, :cond_0

    .line 121
    const/4 v9, 0x0

    .line 141
    :goto_0
    return v9

    .line 123
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/util/CharsRef;->chars:[C

    .line 124
    .local v0, "aChars":[C
    iget v3, p0, Lorg/apache/lucene/util/CharsRef;->offset:I

    .line 125
    .local v3, "aUpto":I
    iget-object v5, p1, Lorg/apache/lucene/util/CharsRef;->chars:[C

    .line 126
    .local v5, "bChars":[C
    iget v7, p1, Lorg/apache/lucene/util/CharsRef;->offset:I

    .line 128
    .local v7, "bUpto":I
    iget v9, p0, Lorg/apache/lucene/util/CharsRef;->length:I

    iget v10, p1, Lorg/apache/lucene/util/CharsRef;->length:I

    invoke-static {v9, v10}, Ljava/lang/Math;->min(II)I

    move-result v9

    add-int v2, v3, v9

    .local v2, "aStop":I
    move v8, v7

    .end local v7    # "bUpto":I
    .local v8, "bUpto":I
    move v4, v3

    .line 130
    .end local v3    # "aUpto":I
    .local v4, "aUpto":I
    :goto_1
    if-lt v4, v2, :cond_1

    .line 141
    iget v9, p0, Lorg/apache/lucene/util/CharsRef;->length:I

    iget v10, p1, Lorg/apache/lucene/util/CharsRef;->length:I

    sub-int/2addr v9, v10

    goto :goto_0

    .line 131
    :cond_1
    add-int/lit8 v3, v4, 0x1

    .end local v4    # "aUpto":I
    .restart local v3    # "aUpto":I
    aget-char v1, v0, v4

    .line 132
    .local v1, "aInt":I
    add-int/lit8 v7, v8, 0x1

    .end local v8    # "bUpto":I
    .restart local v7    # "bUpto":I
    aget-char v6, v5, v8

    .line 133
    .local v6, "bInt":I
    if-le v1, v6, :cond_2

    .line 134
    const/4 v9, 0x1

    goto :goto_0

    .line 135
    :cond_2
    if-ge v1, v6, :cond_3

    .line 136
    const/4 v9, -0x1

    goto :goto_0

    :cond_3
    move v8, v7

    .end local v7    # "bUpto":I
    .restart local v8    # "bUpto":I
    move v4, v3

    .end local v3    # "aUpto":I
    .restart local v4    # "aUpto":I
    goto :goto_1
.end method

.method public copyChars(Lorg/apache/lucene/util/CharsRef;)V
    .locals 3
    .param p1, "other"    # Lorg/apache/lucene/util/CharsRef;

    .prologue
    .line 151
    iget-object v0, p1, Lorg/apache/lucene/util/CharsRef;->chars:[C

    iget v1, p1, Lorg/apache/lucene/util/CharsRef;->offset:I

    iget v2, p1, Lorg/apache/lucene/util/CharsRef;->length:I

    invoke-virtual {p0, v0, v1, v2}, Lorg/apache/lucene/util/CharsRef;->copyChars([CII)V

    .line 152
    return-void
.end method

.method public copyChars([CII)V
    .locals 2
    .param p1, "otherChars"    # [C
    .param p2, "otherOffset"    # I
    .param p3, "otherLength"    # I

    .prologue
    .line 170
    iget-object v0, p0, Lorg/apache/lucene/util/CharsRef;->chars:[C

    array-length v0, v0

    iget v1, p0, Lorg/apache/lucene/util/CharsRef;->offset:I

    sub-int/2addr v0, v1

    if-ge v0, p3, :cond_0

    .line 171
    new-array v0, p3, [C

    iput-object v0, p0, Lorg/apache/lucene/util/CharsRef;->chars:[C

    .line 172
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/util/CharsRef;->offset:I

    .line 174
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/util/CharsRef;->chars:[C

    iget v1, p0, Lorg/apache/lucene/util/CharsRef;->offset:I

    invoke-static {p1, p2, v0, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 175
    iput p3, p0, Lorg/apache/lucene/util/CharsRef;->length:I

    .line 176
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x0

    .line 92
    if-nez p1, :cond_1

    .line 98
    .end local p1    # "other":Ljava/lang/Object;
    :cond_0
    :goto_0
    return v0

    .line 95
    .restart local p1    # "other":Ljava/lang/Object;
    :cond_1
    instance-of v1, p1, Lorg/apache/lucene/util/CharsRef;

    if-eqz v1, :cond_0

    .line 96
    check-cast p1, Lorg/apache/lucene/util/CharsRef;

    .end local p1    # "other":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/CharsRef;->charsEquals(Lorg/apache/lucene/util/CharsRef;)Z

    move-result v0

    goto :goto_0
.end method

.method public grow(I)V
    .locals 1
    .param p1, "newLength"    # I

    .prologue
    .line 160
    sget-boolean v0, Lorg/apache/lucene/util/CharsRef;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/util/CharsRef;->offset:I

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 161
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/util/CharsRef;->chars:[C

    array-length v0, v0

    if-ge v0, p1, :cond_1

    .line 162
    iget-object v0, p0, Lorg/apache/lucene/util/CharsRef;->chars:[C

    invoke-static {v0, p1}, Lorg/apache/lucene/util/ArrayUtil;->grow([CI)[C

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/util/CharsRef;->chars:[C

    .line 164
    :cond_1
    return-void
.end method

.method public hashCode()I
    .locals 6

    .prologue
    .line 81
    const/16 v2, 0x1f

    .line 82
    .local v2, "prime":I
    const/4 v3, 0x0

    .line 83
    .local v3, "result":I
    iget v4, p0, Lorg/apache/lucene/util/CharsRef;->offset:I

    iget v5, p0, Lorg/apache/lucene/util/CharsRef;->length:I

    add-int v0, v4, v5

    .line 84
    .local v0, "end":I
    iget v1, p0, Lorg/apache/lucene/util/CharsRef;->offset:I

    .local v1, "i":I
    :goto_0
    if-lt v1, v0, :cond_0

    .line 87
    return v3

    .line 85
    :cond_0
    mul-int/lit8 v4, v3, 0x1f

    iget-object v5, p0, Lorg/apache/lucene/util/CharsRef;->chars:[C

    aget-char v5, v5, v1

    add-int v3, v4, v5

    .line 84
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public isValid()Z
    .locals 3

    .prologue
    .line 298
    iget-object v0, p0, Lorg/apache/lucene/util/CharsRef;->chars:[C

    if-nez v0, :cond_0

    .line 299
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "chars is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 301
    :cond_0
    iget v0, p0, Lorg/apache/lucene/util/CharsRef;->length:I

    if-gez v0, :cond_1

    .line 302
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "length is negative: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/lucene/util/CharsRef;->length:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 304
    :cond_1
    iget v0, p0, Lorg/apache/lucene/util/CharsRef;->length:I

    iget-object v1, p0, Lorg/apache/lucene/util/CharsRef;->chars:[C

    array-length v1, v1

    if-le v0, v1, :cond_2

    .line 305
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "length is out of bounds: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/lucene/util/CharsRef;->length:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",chars.length="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/util/CharsRef;->chars:[C

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 307
    :cond_2
    iget v0, p0, Lorg/apache/lucene/util/CharsRef;->offset:I

    if-gez v0, :cond_3

    .line 308
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "offset is negative: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/lucene/util/CharsRef;->offset:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 310
    :cond_3
    iget v0, p0, Lorg/apache/lucene/util/CharsRef;->offset:I

    iget-object v1, p0, Lorg/apache/lucene/util/CharsRef;->chars:[C

    array-length v1, v1

    if-le v0, v1, :cond_4

    .line 311
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "offset out of bounds: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/lucene/util/CharsRef;->offset:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",chars.length="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/util/CharsRef;->chars:[C

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 313
    :cond_4
    iget v0, p0, Lorg/apache/lucene/util/CharsRef;->offset:I

    iget v1, p0, Lorg/apache/lucene/util/CharsRef;->length:I

    add-int/2addr v0, v1

    if-gez v0, :cond_5

    .line 314
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "offset+length is negative: offset="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/lucene/util/CharsRef;->offset:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",length="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/util/CharsRef;->length:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 316
    :cond_5
    iget v0, p0, Lorg/apache/lucene/util/CharsRef;->offset:I

    iget v1, p0, Lorg/apache/lucene/util/CharsRef;->length:I

    add-int/2addr v0, v1

    iget-object v1, p0, Lorg/apache/lucene/util/CharsRef;->chars:[C

    array-length v1, v1

    if-le v0, v1, :cond_6

    .line 317
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "offset+length out of bounds: offset="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/lucene/util/CharsRef;->offset:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",length="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/util/CharsRef;->length:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",chars.length="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/util/CharsRef;->chars:[C

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 319
    :cond_6
    const/4 v0, 0x1

    return v0
.end method

.method public length()I
    .locals 1

    .prologue
    .line 200
    iget v0, p0, Lorg/apache/lucene/util/CharsRef;->length:I

    return v0
.end method

.method public subSequence(II)Ljava/lang/CharSequence;
    .locals 4
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 215
    if-ltz p1, :cond_0

    iget v0, p0, Lorg/apache/lucene/util/CharsRef;->length:I

    if-gt p2, v0, :cond_0

    if-le p1, p2, :cond_1

    .line 216
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0

    .line 218
    :cond_1
    new-instance v0, Lorg/apache/lucene/util/CharsRef;

    iget-object v1, p0, Lorg/apache/lucene/util/CharsRef;->chars:[C

    iget v2, p0, Lorg/apache/lucene/util/CharsRef;->offset:I

    add-int/2addr v2, p1

    sub-int v3, p2, p1

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/util/CharsRef;-><init>([CII)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 195
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/lucene/util/CharsRef;->chars:[C

    iget v2, p0, Lorg/apache/lucene/util/CharsRef;->offset:I

    iget v3, p0, Lorg/apache/lucene/util/CharsRef;->length:I

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([CII)V

    return-object v0
.end method
