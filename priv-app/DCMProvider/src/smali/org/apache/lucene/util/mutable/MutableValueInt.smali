.class public Lorg/apache/lucene/util/mutable/MutableValueInt;
.super Lorg/apache/lucene/util/mutable/MutableValue;
.source "MutableValueInt.java"


# instance fields
.field public value:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lorg/apache/lucene/util/mutable/MutableValue;-><init>()V

    return-void
.end method


# virtual methods
.method public compareSameType(Ljava/lang/Object;)I
    .locals 7
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v3, 0x1

    const/4 v4, -0x1

    .line 54
    move-object v1, p1

    check-cast v1, Lorg/apache/lucene/util/mutable/MutableValueInt;

    .line 55
    .local v1, "b":Lorg/apache/lucene/util/mutable/MutableValueInt;
    iget v0, p0, Lorg/apache/lucene/util/mutable/MutableValueInt;->value:I

    .line 56
    .local v0, "ai":I
    iget v2, v1, Lorg/apache/lucene/util/mutable/MutableValueInt;->value:I

    .line 57
    .local v2, "bi":I
    if-ge v0, v2, :cond_1

    move v3, v4

    .line 61
    :cond_0
    :goto_0
    return v3

    .line 58
    :cond_1
    if-gt v0, v2, :cond_0

    .line 60
    iget-boolean v5, p0, Lorg/apache/lucene/util/mutable/MutableValueInt;->exists:Z

    iget-boolean v6, v1, Lorg/apache/lucene/util/mutable/MutableValueInt;->exists:Z

    if-ne v5, v6, :cond_2

    const/4 v3, 0x0

    goto :goto_0

    .line 61
    :cond_2
    iget-boolean v5, p0, Lorg/apache/lucene/util/mutable/MutableValueInt;->exists:Z

    if-nez v5, :cond_0

    move v3, v4

    goto :goto_0
.end method

.method public copy(Lorg/apache/lucene/util/mutable/MutableValue;)V
    .locals 2
    .param p1, "source"    # Lorg/apache/lucene/util/mutable/MutableValue;

    .prologue
    .line 33
    move-object v0, p1

    check-cast v0, Lorg/apache/lucene/util/mutable/MutableValueInt;

    .line 34
    .local v0, "s":Lorg/apache/lucene/util/mutable/MutableValueInt;
    iget v1, v0, Lorg/apache/lucene/util/mutable/MutableValueInt;->value:I

    iput v1, p0, Lorg/apache/lucene/util/mutable/MutableValueInt;->value:I

    .line 35
    iget-boolean v1, v0, Lorg/apache/lucene/util/mutable/MutableValueInt;->exists:Z

    iput-boolean v1, p0, Lorg/apache/lucene/util/mutable/MutableValueInt;->exists:Z

    .line 36
    return-void
.end method

.method public duplicate()Lorg/apache/lucene/util/mutable/MutableValue;
    .locals 2

    .prologue
    .line 40
    new-instance v0, Lorg/apache/lucene/util/mutable/MutableValueInt;

    invoke-direct {v0}, Lorg/apache/lucene/util/mutable/MutableValueInt;-><init>()V

    .line 41
    .local v0, "v":Lorg/apache/lucene/util/mutable/MutableValueInt;
    iget v1, p0, Lorg/apache/lucene/util/mutable/MutableValueInt;->value:I

    iput v1, v0, Lorg/apache/lucene/util/mutable/MutableValueInt;->value:I

    .line 42
    iget-boolean v1, p0, Lorg/apache/lucene/util/mutable/MutableValueInt;->exists:Z

    iput-boolean v1, v0, Lorg/apache/lucene/util/mutable/MutableValueInt;->exists:Z

    .line 43
    return-object v0
.end method

.method public equalsSameType(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 48
    move-object v0, p1

    check-cast v0, Lorg/apache/lucene/util/mutable/MutableValueInt;

    .line 49
    .local v0, "b":Lorg/apache/lucene/util/mutable/MutableValueInt;
    iget v1, p0, Lorg/apache/lucene/util/mutable/MutableValueInt;->value:I

    iget v2, v0, Lorg/apache/lucene/util/mutable/MutableValueInt;->value:I

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lorg/apache/lucene/util/mutable/MutableValueInt;->exists:Z

    iget-boolean v2, v0, Lorg/apache/lucene/util/mutable/MutableValueInt;->exists:Z

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 68
    iget v0, p0, Lorg/apache/lucene/util/mutable/MutableValueInt;->value:I

    shr-int/lit8 v0, v0, 0x8

    iget v1, p0, Lorg/apache/lucene/util/mutable/MutableValueInt;->value:I

    shr-int/lit8 v1, v1, 0x10

    add-int/2addr v0, v1

    return v0
.end method

.method public toObject()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 28
    iget-boolean v0, p0, Lorg/apache/lucene/util/mutable/MutableValueInt;->exists:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/util/mutable/MutableValueInt;->value:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
