.class public abstract Lorg/apache/lucene/util/mutable/MutableValue;
.super Ljava/lang/Object;
.source "MutableValue.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lorg/apache/lucene/util/mutable/MutableValue;",
        ">;"
    }
.end annotation


# instance fields
.field public exists:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/util/mutable/MutableValue;->exists:Z

    .line 24
    return-void
.end method


# virtual methods
.method public abstract compareSameType(Ljava/lang/Object;)I
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/util/mutable/MutableValue;

    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/mutable/MutableValue;->compareTo(Lorg/apache/lucene/util/mutable/MutableValue;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lorg/apache/lucene/util/mutable/MutableValue;)I
    .locals 5
    .param p1, "other"    # Lorg/apache/lucene/util/mutable/MutableValue;

    .prologue
    .line 39
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    .line 40
    .local v1, "c1":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/apache/lucene/util/mutable/MutableValue;>;"
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    .line 41
    .local v2, "c2":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/apache/lucene/util/mutable/MutableValue;>;"
    if-eq v1, v2, :cond_1

    .line 42
    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v4

    sub-int v0, v3, v4

    .line 43
    .local v0, "c":I
    if-nez v0, :cond_0

    .line 44
    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    .line 48
    .end local v0    # "c":I
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/mutable/MutableValue;->compareSameType(Ljava/lang/Object;)I

    move-result v0

    goto :goto_0
.end method

.method public abstract copy(Lorg/apache/lucene/util/mutable/MutableValue;)V
.end method

.method public abstract duplicate()Lorg/apache/lucene/util/mutable/MutableValue;
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 53
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/mutable/MutableValue;->equalsSameType(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract equalsSameType(Ljava/lang/Object;)Z
.end method

.method public exists()Z
    .locals 1

    .prologue
    .line 34
    iget-boolean v0, p0, Lorg/apache/lucene/util/mutable/MutableValue;->exists:Z

    return v0
.end method

.method public abstract hashCode()I
.end method

.method public abstract toObject()Ljava/lang/Object;
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    invoke-virtual {p0}, Lorg/apache/lucene/util/mutable/MutableValue;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/util/mutable/MutableValue;->toObject()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "(null)"

    goto :goto_0
.end method
