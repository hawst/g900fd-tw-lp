.class public Lorg/apache/lucene/util/mutable/MutableValueBool;
.super Lorg/apache/lucene/util/mutable/MutableValue;
.source "MutableValueBool.java"


# instance fields
.field public value:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lorg/apache/lucene/util/mutable/MutableValue;-><init>()V

    return-void
.end method


# virtual methods
.method public compareSameType(Ljava/lang/Object;)I
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 54
    move-object v0, p1

    check-cast v0, Lorg/apache/lucene/util/mutable/MutableValueBool;

    .line 55
    .local v0, "b":Lorg/apache/lucene/util/mutable/MutableValueBool;
    iget-boolean v3, p0, Lorg/apache/lucene/util/mutable/MutableValueBool;->value:Z

    iget-boolean v4, v0, Lorg/apache/lucene/util/mutable/MutableValueBool;->value:Z

    if-eq v3, v4, :cond_2

    iget-boolean v3, p0, Lorg/apache/lucene/util/mutable/MutableValueBool;->value:Z

    if-eqz v3, :cond_1

    .line 57
    :cond_0
    :goto_0
    return v1

    :cond_1
    move v1, v2

    .line 55
    goto :goto_0

    .line 56
    :cond_2
    iget-boolean v3, p0, Lorg/apache/lucene/util/mutable/MutableValueBool;->exists:Z

    iget-boolean v4, v0, Lorg/apache/lucene/util/mutable/MutableValueBool;->exists:Z

    if-ne v3, v4, :cond_3

    move v1, v2

    goto :goto_0

    .line 57
    :cond_3
    iget-boolean v2, p0, Lorg/apache/lucene/util/mutable/MutableValueBool;->exists:Z

    if-nez v2, :cond_0

    const/4 v1, -0x1

    goto :goto_0
.end method

.method public copy(Lorg/apache/lucene/util/mutable/MutableValue;)V
    .locals 2
    .param p1, "source"    # Lorg/apache/lucene/util/mutable/MutableValue;

    .prologue
    .line 33
    move-object v0, p1

    check-cast v0, Lorg/apache/lucene/util/mutable/MutableValueBool;

    .line 34
    .local v0, "s":Lorg/apache/lucene/util/mutable/MutableValueBool;
    iget-boolean v1, v0, Lorg/apache/lucene/util/mutable/MutableValueBool;->value:Z

    iput-boolean v1, p0, Lorg/apache/lucene/util/mutable/MutableValueBool;->value:Z

    .line 35
    iget-boolean v1, v0, Lorg/apache/lucene/util/mutable/MutableValueBool;->exists:Z

    iput-boolean v1, p0, Lorg/apache/lucene/util/mutable/MutableValueBool;->exists:Z

    .line 36
    return-void
.end method

.method public duplicate()Lorg/apache/lucene/util/mutable/MutableValue;
    .locals 2

    .prologue
    .line 40
    new-instance v0, Lorg/apache/lucene/util/mutable/MutableValueBool;

    invoke-direct {v0}, Lorg/apache/lucene/util/mutable/MutableValueBool;-><init>()V

    .line 41
    .local v0, "v":Lorg/apache/lucene/util/mutable/MutableValueBool;
    iget-boolean v1, p0, Lorg/apache/lucene/util/mutable/MutableValueBool;->value:Z

    iput-boolean v1, v0, Lorg/apache/lucene/util/mutable/MutableValueBool;->value:Z

    .line 42
    iget-boolean v1, p0, Lorg/apache/lucene/util/mutable/MutableValueBool;->exists:Z

    iput-boolean v1, v0, Lorg/apache/lucene/util/mutable/MutableValueBool;->exists:Z

    .line 43
    return-object v0
.end method

.method public equalsSameType(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 48
    move-object v0, p1

    check-cast v0, Lorg/apache/lucene/util/mutable/MutableValueBool;

    .line 49
    .local v0, "b":Lorg/apache/lucene/util/mutable/MutableValueBool;
    iget-boolean v1, p0, Lorg/apache/lucene/util/mutable/MutableValueBool;->value:Z

    iget-boolean v2, v0, Lorg/apache/lucene/util/mutable/MutableValueBool;->value:Z

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lorg/apache/lucene/util/mutable/MutableValueBool;->exists:Z

    iget-boolean v2, v0, Lorg/apache/lucene/util/mutable/MutableValueBool;->exists:Z

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 62
    iget-boolean v0, p0, Lorg/apache/lucene/util/mutable/MutableValueBool;->value:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, Lorg/apache/lucene/util/mutable/MutableValueBool;->exists:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toObject()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 28
    iget-boolean v0, p0, Lorg/apache/lucene/util/mutable/MutableValueBool;->exists:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lorg/apache/lucene/util/mutable/MutableValueBool;->value:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
