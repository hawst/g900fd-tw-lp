.class public Lorg/apache/lucene/util/OpenBitSetDISI;
.super Lorg/apache/lucene/util/OpenBitSet;
.source "OpenBitSetDISI.java"


# direct methods
.method public constructor <init>(I)V
    .locals 2
    .param p1, "maxSize"    # I

    .prologue
    .line 42
    int-to-long v0, p1

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/util/OpenBitSet;-><init>(J)V

    .line 43
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/search/DocIdSetIterator;I)V
    .locals 2
    .param p1, "disi"    # Lorg/apache/lucene/search/DocIdSetIterator;
    .param p2, "maxSize"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 33
    int-to-long v0, p2

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/util/OpenBitSet;-><init>(J)V

    .line 34
    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/OpenBitSetDISI;->inPlaceOr(Lorg/apache/lucene/search/DocIdSetIterator;)V

    .line 35
    return-void
.end method


# virtual methods
.method public inPlaceAnd(Lorg/apache/lucene/search/DocIdSetIterator;)V
    .locals 6
    .param p1, "disi"    # Lorg/apache/lucene/search/DocIdSetIterator;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, -0x1

    .line 66
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lorg/apache/lucene/util/OpenBitSetDISI;->nextSetBit(I)I

    move-result v0

    .line 68
    .local v0, "bitSetDoc":I
    :goto_0
    if-eq v0, v3, :cond_0

    invoke-virtual {p1, v0}, Lorg/apache/lucene/search/DocIdSetIterator;->advance(I)I

    move-result v1

    .local v1, "disiDoc":I
    const v2, 0x7fffffff

    if-ne v1, v2, :cond_2

    .line 72
    .end local v1    # "disiDoc":I
    :cond_0
    if-eq v0, v3, :cond_1

    .line 73
    int-to-long v2, v0

    invoke-virtual {p0}, Lorg/apache/lucene/util/OpenBitSetDISI;->size()J

    move-result-wide v4

    invoke-virtual {p0, v2, v3, v4, v5}, Lorg/apache/lucene/util/OpenBitSetDISI;->clear(JJ)V

    .line 75
    :cond_1
    return-void

    .line 69
    .restart local v1    # "disiDoc":I
    :cond_2
    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/util/OpenBitSetDISI;->clear(II)V

    .line 70
    add-int/lit8 v2, v1, 0x1

    invoke-virtual {p0, v2}, Lorg/apache/lucene/util/OpenBitSetDISI;->nextSetBit(I)I

    move-result v0

    goto :goto_0
.end method

.method public inPlaceNot(Lorg/apache/lucene/search/DocIdSetIterator;)V
    .locals 6
    .param p1, "disi"    # Lorg/apache/lucene/search/DocIdSetIterator;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 85
    invoke-virtual {p0}, Lorg/apache/lucene/util/OpenBitSetDISI;->size()J

    move-result-wide v2

    .line 86
    .local v2, "size":J
    :goto_0
    invoke-virtual {p1}, Lorg/apache/lucene/search/DocIdSetIterator;->nextDoc()I

    move-result v0

    .local v0, "doc":I
    int-to-long v4, v0

    cmp-long v1, v4, v2

    if-ltz v1, :cond_0

    .line 89
    return-void

    .line 87
    :cond_0
    invoke-virtual {p0, v0}, Lorg/apache/lucene/util/OpenBitSetDISI;->fastClear(I)V

    goto :goto_0
.end method

.method public inPlaceOr(Lorg/apache/lucene/search/DocIdSetIterator;)V
    .locals 6
    .param p1, "disi"    # Lorg/apache/lucene/search/DocIdSetIterator;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53
    invoke-virtual {p0}, Lorg/apache/lucene/util/OpenBitSetDISI;->size()J

    move-result-wide v2

    .line 54
    .local v2, "size":J
    :goto_0
    invoke-virtual {p1}, Lorg/apache/lucene/search/DocIdSetIterator;->nextDoc()I

    move-result v0

    .local v0, "doc":I
    int-to-long v4, v0

    cmp-long v1, v4, v2

    if-ltz v1, :cond_0

    .line 57
    return-void

    .line 55
    :cond_0
    invoke-virtual {p0, v0}, Lorg/apache/lucene/util/OpenBitSetDISI;->fastSet(I)V

    goto :goto_0
.end method

.method public inPlaceXor(Lorg/apache/lucene/search/DocIdSetIterator;)V
    .locals 6
    .param p1, "disi"    # Lorg/apache/lucene/search/DocIdSetIterator;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 99
    invoke-virtual {p0}, Lorg/apache/lucene/util/OpenBitSetDISI;->size()J

    move-result-wide v2

    .line 100
    .local v2, "size":J
    :goto_0
    invoke-virtual {p1}, Lorg/apache/lucene/search/DocIdSetIterator;->nextDoc()I

    move-result v0

    .local v0, "doc":I
    int-to-long v4, v0

    cmp-long v1, v4, v2

    if-ltz v1, :cond_0

    .line 103
    return-void

    .line 101
    :cond_0
    invoke-virtual {p0, v0}, Lorg/apache/lucene/util/OpenBitSetDISI;->fastFlip(I)V

    goto :goto_0
.end method
