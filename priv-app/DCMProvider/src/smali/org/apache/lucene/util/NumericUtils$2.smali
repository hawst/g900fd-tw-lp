.class Lorg/apache/lucene/util/NumericUtils$2;
.super Lorg/apache/lucene/index/FilteredTermsEnum;
.source "NumericUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/util/NumericUtils;->filterPrefixCodedInts(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# direct methods
.method constructor <init>(Lorg/apache/lucene/index/TermsEnum;Z)V
    .locals 0
    .param p1, "$anonymous0"    # Lorg/apache/lucene/index/TermsEnum;
    .param p2, "$anonymous1"    # Z

    .prologue
    .line 489
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/index/FilteredTermsEnum;-><init>(Lorg/apache/lucene/index/TermsEnum;Z)V

    .line 1
    return-void
.end method


# virtual methods
.method protected accept(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;
    .locals 1
    .param p1, "term"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 493
    invoke-static {p1}, Lorg/apache/lucene/util/NumericUtils;->getPrefixCodedIntShift(Lorg/apache/lucene/util/BytesRef;)I

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;->YES:Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;->END:Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

    goto :goto_0
.end method
