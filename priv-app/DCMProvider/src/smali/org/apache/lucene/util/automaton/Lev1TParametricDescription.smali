.class Lorg/apache/lucene/util/automaton/Lev1TParametricDescription;
.super Lorg/apache/lucene/util/automaton/LevenshteinAutomata$ParametricDescription;
.source "Lev1TParametricDescription.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final offsetIncrs0:[J

.field private static final offsetIncrs1:[J

.field private static final offsetIncrs2:[J

.field private static final offsetIncrs3:[J

.field private static final toStates0:[J

.field private static final toStates1:[J

.field private static final toStates2:[J

.field private static final toStates3:[J


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 28
    const-class v0, Lorg/apache/lucene/util/automaton/Lev1TParametricDescription;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/automaton/Lev1TParametricDescription;->$assertionsDisabled:Z

    .line 76
    new-array v0, v1, [J

    .line 77
    const-wide/16 v4, 0x2

    aput-wide v4, v0, v2

    .line 76
    sput-object v0, Lorg/apache/lucene/util/automaton/Lev1TParametricDescription;->toStates0:[J

    .line 79
    new-array v0, v1, [J

    sput-object v0, Lorg/apache/lucene/util/automaton/Lev1TParametricDescription;->offsetIncrs0:[J

    .line 84
    new-array v0, v1, [J

    .line 85
    const-wide/16 v4, 0xa43

    aput-wide v4, v0, v2

    .line 84
    sput-object v0, Lorg/apache/lucene/util/automaton/Lev1TParametricDescription;->toStates1:[J

    .line 87
    new-array v0, v1, [J

    .line 88
    const-wide/16 v4, 0x38

    aput-wide v4, v0, v2

    .line 87
    sput-object v0, Lorg/apache/lucene/util/automaton/Lev1TParametricDescription;->offsetIncrs1:[J

    .line 92
    new-array v0, v3, [J

    fill-array-data v0, :array_0

    sput-object v0, Lorg/apache/lucene/util/automaton/Lev1TParametricDescription;->toStates2:[J

    .line 95
    new-array v0, v1, [J

    .line 96
    const-wide v4, 0x555555a20000L

    aput-wide v4, v0, v2

    .line 95
    sput-object v0, Lorg/apache/lucene/util/automaton/Lev1TParametricDescription;->offsetIncrs2:[J

    .line 100
    const/4 v0, 0x3

    new-array v0, v0, [J

    fill-array-data v0, :array_1

    sput-object v0, Lorg/apache/lucene/util/automaton/Lev1TParametricDescription;->toStates3:[J

    .line 103
    new-array v0, v3, [J

    fill-array-data v0, :array_2

    sput-object v0, Lorg/apache/lucene/util/automaton/Lev1TParametricDescription;->offsetIncrs3:[J

    .line 105
    return-void

    :cond_0
    move v0, v2

    .line 28
    goto :goto_0

    .line 92
    :array_0
    .array-data 8
        0x3453491482140003L    # 1.2289409980610215E-56
        0x6d
    .end array-data

    .line 100
    :array_1
    .array-data 8
        0x21520854900c0003L    # 3.525648842400044E-148
        0x5b4d19a24534916dL    # 6.4548043935804775E131
        0xda34
    .end array-data

    .line 103
    :array_2
    .array-data 8
        0x5555ae0a20fc0000L    # 1.2139328062873987E103
        0x55555555
    .end array-data
.end method

.method public constructor <init>(I)V
    .locals 4
    .param p1, "w"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    .line 117
    const/4 v0, 0x6

    new-array v0, v0, [I

    aput v3, v0, v3

    const/4 v1, 0x3

    aput v2, v0, v1

    const/4 v1, 0x4

    aput v2, v0, v1

    const/4 v1, 0x5

    aput v2, v0, v1

    invoke-direct {p0, p1, v3, v0}, Lorg/apache/lucene/util/automaton/LevenshteinAutomata$ParametricDescription;-><init>(II[I)V

    .line 118
    return-void
.end method


# virtual methods
.method transition(III)I
    .locals 9
    .param p1, "absState"    # I
    .param p2, "position"    # I
    .param p3, "vector"    # I

    .prologue
    const/4 v8, 0x6

    const/4 v7, 0x1

    const/4 v6, 0x3

    const/4 v3, -0x1

    const/4 v5, 0x2

    .line 33
    sget-boolean v4, Lorg/apache/lucene/util/automaton/Lev1TParametricDescription;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    if-ne p1, v3, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 36
    :cond_0
    iget v4, p0, Lorg/apache/lucene/util/automaton/Lev1TParametricDescription;->w:I

    add-int/lit8 v4, v4, 0x1

    div-int v2, p1, v4

    .line 37
    .local v2, "state":I
    iget v4, p0, Lorg/apache/lucene/util/automaton/Lev1TParametricDescription;->w:I

    add-int/lit8 v4, v4, 0x1

    rem-int v1, p1, v4

    .line 38
    .local v1, "offset":I
    sget-boolean v4, Lorg/apache/lucene/util/automaton/Lev1TParametricDescription;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    if-gez v1, :cond_1

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 40
    :cond_1
    iget v4, p0, Lorg/apache/lucene/util/automaton/Lev1TParametricDescription;->w:I

    if-ne p2, v4, :cond_3

    .line 41
    if-ge v2, v5, :cond_2

    .line 42
    mul-int/lit8 v4, p3, 0x2

    add-int v0, v4, v2

    .line 43
    .local v0, "loc":I
    sget-object v4, Lorg/apache/lucene/util/automaton/Lev1TParametricDescription;->offsetIncrs0:[J

    invoke-virtual {p0, v4, v0, v7}, Lorg/apache/lucene/util/automaton/Lev1TParametricDescription;->unpack([JII)I

    move-result v4

    add-int/2addr v1, v4

    .line 44
    sget-object v4, Lorg/apache/lucene/util/automaton/Lev1TParametricDescription;->toStates0:[J

    invoke-virtual {p0, v4, v0, v5}, Lorg/apache/lucene/util/automaton/Lev1TParametricDescription;->unpack([JII)I

    move-result v4

    add-int/lit8 v2, v4, -0x1

    .line 66
    .end local v0    # "loc":I
    :cond_2
    :goto_0
    if-ne v2, v3, :cond_6

    .line 71
    :goto_1
    return v3

    .line 46
    :cond_3
    iget v4, p0, Lorg/apache/lucene/util/automaton/Lev1TParametricDescription;->w:I

    add-int/lit8 v4, v4, -0x1

    if-ne p2, v4, :cond_4

    .line 47
    if-ge v2, v6, :cond_2

    .line 48
    mul-int/lit8 v4, p3, 0x3

    add-int v0, v4, v2

    .line 49
    .restart local v0    # "loc":I
    sget-object v4, Lorg/apache/lucene/util/automaton/Lev1TParametricDescription;->offsetIncrs1:[J

    invoke-virtual {p0, v4, v0, v7}, Lorg/apache/lucene/util/automaton/Lev1TParametricDescription;->unpack([JII)I

    move-result v4

    add-int/2addr v1, v4

    .line 50
    sget-object v4, Lorg/apache/lucene/util/automaton/Lev1TParametricDescription;->toStates1:[J

    invoke-virtual {p0, v4, v0, v5}, Lorg/apache/lucene/util/automaton/Lev1TParametricDescription;->unpack([JII)I

    move-result v4

    add-int/lit8 v2, v4, -0x1

    .line 52
    goto :goto_0

    .end local v0    # "loc":I
    :cond_4
    iget v4, p0, Lorg/apache/lucene/util/automaton/Lev1TParametricDescription;->w:I

    add-int/lit8 v4, v4, -0x2

    if-ne p2, v4, :cond_5

    .line 53
    if-ge v2, v8, :cond_2

    .line 54
    mul-int/lit8 v4, p3, 0x6

    add-int v0, v4, v2

    .line 55
    .restart local v0    # "loc":I
    sget-object v4, Lorg/apache/lucene/util/automaton/Lev1TParametricDescription;->offsetIncrs2:[J

    invoke-virtual {p0, v4, v0, v5}, Lorg/apache/lucene/util/automaton/Lev1TParametricDescription;->unpack([JII)I

    move-result v4

    add-int/2addr v1, v4

    .line 56
    sget-object v4, Lorg/apache/lucene/util/automaton/Lev1TParametricDescription;->toStates2:[J

    invoke-virtual {p0, v4, v0, v6}, Lorg/apache/lucene/util/automaton/Lev1TParametricDescription;->unpack([JII)I

    move-result v4

    add-int/lit8 v2, v4, -0x1

    .line 58
    goto :goto_0

    .line 59
    .end local v0    # "loc":I
    :cond_5
    if-ge v2, v8, :cond_2

    .line 60
    mul-int/lit8 v4, p3, 0x6

    add-int v0, v4, v2

    .line 61
    .restart local v0    # "loc":I
    sget-object v4, Lorg/apache/lucene/util/automaton/Lev1TParametricDescription;->offsetIncrs3:[J

    invoke-virtual {p0, v4, v0, v5}, Lorg/apache/lucene/util/automaton/Lev1TParametricDescription;->unpack([JII)I

    move-result v4

    add-int/2addr v1, v4

    .line 62
    sget-object v4, Lorg/apache/lucene/util/automaton/Lev1TParametricDescription;->toStates3:[J

    invoke-virtual {p0, v4, v0, v6}, Lorg/apache/lucene/util/automaton/Lev1TParametricDescription;->unpack([JII)I

    move-result v4

    add-int/lit8 v2, v4, -0x1

    goto :goto_0

    .line 71
    .end local v0    # "loc":I
    :cond_6
    iget v3, p0, Lorg/apache/lucene/util/automaton/Lev1TParametricDescription;->w:I

    add-int/lit8 v3, v3, 0x1

    mul-int/2addr v3, v2

    add-int/2addr v3, v1

    goto :goto_1
.end method
