.class public final enum Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;
.super Ljava/lang/Enum;
.source "CompiledAutomaton.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/automaton/CompiledAutomaton;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AUTOMATON_TYPE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum ALL:Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;

.field private static final synthetic ENUM$VALUES:[Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;

.field public static final enum NONE:Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;

.field public static final enum NORMAL:Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;

.field public static final enum PREFIX:Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;

.field public static final enum SINGLE:Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 43
    new-instance v0, Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;-><init>(Ljava/lang/String;I)V

    .line 44
    sput-object v0, Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;->NONE:Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;

    .line 45
    new-instance v0, Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;

    const-string v1, "ALL"

    invoke-direct {v0, v1, v3}, Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;-><init>(Ljava/lang/String;I)V

    .line 46
    sput-object v0, Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;->ALL:Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;

    .line 47
    new-instance v0, Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;

    const-string v1, "SINGLE"

    invoke-direct {v0, v1, v4}, Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;-><init>(Ljava/lang/String;I)V

    .line 48
    sput-object v0, Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;->SINGLE:Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;

    .line 49
    new-instance v0, Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;

    const-string v1, "PREFIX"

    invoke-direct {v0, v1, v5}, Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;-><init>(Ljava/lang/String;I)V

    .line 50
    sput-object v0, Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;->PREFIX:Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;

    .line 51
    new-instance v0, Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v6}, Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;-><init>(Ljava/lang/String;I)V

    .line 52
    sput-object v0, Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;->NORMAL:Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;

    .line 42
    const/4 v0, 0x5

    new-array v0, v0, [Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;

    sget-object v1, Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;->NONE:Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;

    aput-object v1, v0, v2

    sget-object v1, Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;->ALL:Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;

    aput-object v1, v0, v3

    sget-object v1, Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;->SINGLE:Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;

    aput-object v1, v0, v4

    sget-object v1, Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;->PREFIX:Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;

    aput-object v1, v0, v5

    sget-object v1, Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;->NORMAL:Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;

    aput-object v1, v0, v6

    sput-object v0, Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;->ENUM$VALUES:[Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;

    return-object v0
.end method

.method public static values()[Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;->ENUM$VALUES:[Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;

    array-length v1, v0

    new-array v2, v1, [Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
