.class public abstract Lorg/apache/lucene/util/IntBlockPool$Allocator;
.super Ljava/lang/Object;
.source "IntBlockPool.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/IntBlockPool;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Allocator"
.end annotation


# instance fields
.field protected final blockSize:I


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1, "blockSize"    # I

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput p1, p0, Lorg/apache/lucene/util/IntBlockPool$Allocator;->blockSize:I

    .line 38
    return-void
.end method


# virtual methods
.method public getIntBlock()[I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lorg/apache/lucene/util/IntBlockPool$Allocator;->blockSize:I

    new-array v0, v0, [I

    return-object v0
.end method

.method public abstract recycleIntBlocks([[III)V
.end method
