.class final Lorg/apache/lucene/util/packed/Direct64;
.super Lorg/apache/lucene/util/packed/PackedInts$MutableImpl;
.source "Direct64.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final values:[J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lorg/apache/lucene/util/packed/Direct64;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/packed/Direct64;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(I)V
    .locals 1
    .param p1, "valueCount"    # I

    .prologue
    .line 36
    const/16 v0, 0x40

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/util/packed/PackedInts$MutableImpl;-><init>(II)V

    .line 37
    new-array v0, p1, [J

    iput-object v0, p0, Lorg/apache/lucene/util/packed/Direct64;->values:[J

    .line 38
    return-void
.end method

.method constructor <init>(ILorg/apache/lucene/store/DataInput;I)V
    .locals 4
    .param p1, "packedIntsVersion"    # I
    .param p2, "in"    # Lorg/apache/lucene/store/DataInput;
    .param p3, "valueCount"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0, p3}, Lorg/apache/lucene/util/packed/Direct64;-><init>(I)V

    .line 42
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, p3, :cond_0

    .line 45
    return-void

    .line 43
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/util/packed/Direct64;->values:[J

    invoke-virtual {p2}, Lorg/apache/lucene/store/DataInput;->readLong()J

    move-result-wide v2

    aput-wide v2, v1, v0

    .line 42
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public clear()V
    .locals 4

    .prologue
    .line 68
    iget-object v0, p0, Lorg/apache/lucene/util/packed/Direct64;->values:[J

    const-wide/16 v2, 0x0

    invoke-static {v0, v2, v3}, Ljava/util/Arrays;->fill([JJ)V

    .line 69
    return-void
.end method

.method public fill(IIJ)V
    .locals 1
    .param p1, "fromIndex"    # I
    .param p2, "toIndex"    # I
    .param p3, "val"    # J

    .prologue
    .line 105
    iget-object v0, p0, Lorg/apache/lucene/util/packed/Direct64;->values:[J

    invoke-static {v0, p1, p2, p3, p4}, Ljava/util/Arrays;->fill([JIIJ)V

    .line 106
    return-void
.end method

.method public get(I[JII)I
    .locals 4
    .param p1, "index"    # I
    .param p2, "arr"    # [J
    .param p3, "off"    # I
    .param p4, "len"    # I

    .prologue
    .line 83
    sget-boolean v1, Lorg/apache/lucene/util/packed/Direct64;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-gtz p4, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "len must be > 0 (got "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 84
    :cond_0
    sget-boolean v1, Lorg/apache/lucene/util/packed/Direct64;->$assertionsDisabled:Z

    if-nez v1, :cond_2

    if-ltz p1, :cond_1

    iget v1, p0, Lorg/apache/lucene/util/packed/Direct64;->valueCount:I

    if-lt p1, v1, :cond_2

    :cond_1
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 85
    :cond_2
    sget-boolean v1, Lorg/apache/lucene/util/packed/Direct64;->$assertionsDisabled:Z

    if-nez v1, :cond_3

    add-int v1, p3, p4

    array-length v2, p2

    if-le v1, v2, :cond_3

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 87
    :cond_3
    iget v1, p0, Lorg/apache/lucene/util/packed/Direct64;->valueCount:I

    sub-int/2addr v1, p1

    invoke-static {v1, p4}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 88
    .local v0, "gets":I
    iget-object v1, p0, Lorg/apache/lucene/util/packed/Direct64;->values:[J

    invoke-static {v1, p1, p2, p3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 89
    return v0
.end method

.method public get(I)J
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 49
    iget-object v0, p0, Lorg/apache/lucene/util/packed/Direct64;->values:[J

    aget-wide v0, v0, p1

    return-wide v0
.end method

.method public getArray()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lorg/apache/lucene/util/packed/Direct64;->values:[J

    return-object v0
.end method

.method public hasArray()Z
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x1

    return v0
.end method

.method public ramBytesUsed()J
    .locals 4

    .prologue
    .line 60
    sget v0, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_HEADER:I

    add-int/lit8 v0, v0, 0x8

    .line 62
    sget v1, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_REF:I

    .line 60
    add-int/2addr v0, v1

    int-to-long v0, v0

    .line 59
    invoke-static {v0, v1}, Lorg/apache/lucene/util/RamUsageEstimator;->alignObjectSize(J)J

    move-result-wide v0

    .line 63
    iget-object v2, p0, Lorg/apache/lucene/util/packed/Direct64;->values:[J

    invoke-static {v2}, Lorg/apache/lucene/util/RamUsageEstimator;->sizeOf([J)J

    move-result-wide v2

    .line 59
    add-long/2addr v0, v2

    return-wide v0
.end method

.method public set(I[JII)I
    .locals 4
    .param p1, "index"    # I
    .param p2, "arr"    # [J
    .param p3, "off"    # I
    .param p4, "len"    # I

    .prologue
    .line 94
    sget-boolean v1, Lorg/apache/lucene/util/packed/Direct64;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-gtz p4, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "len must be > 0 (got "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 95
    :cond_0
    sget-boolean v1, Lorg/apache/lucene/util/packed/Direct64;->$assertionsDisabled:Z

    if-nez v1, :cond_2

    if-ltz p1, :cond_1

    iget v1, p0, Lorg/apache/lucene/util/packed/Direct64;->valueCount:I

    if-lt p1, v1, :cond_2

    :cond_1
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 96
    :cond_2
    sget-boolean v1, Lorg/apache/lucene/util/packed/Direct64;->$assertionsDisabled:Z

    if-nez v1, :cond_3

    add-int v1, p3, p4

    array-length v2, p2

    if-le v1, v2, :cond_3

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 98
    :cond_3
    iget v1, p0, Lorg/apache/lucene/util/packed/Direct64;->valueCount:I

    sub-int/2addr v1, p1

    invoke-static {v1, p4}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 99
    .local v0, "sets":I
    iget-object v1, p0, Lorg/apache/lucene/util/packed/Direct64;->values:[J

    invoke-static {p2, p3, v1, p1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 100
    return v0
.end method

.method public set(IJ)V
    .locals 2
    .param p1, "index"    # I
    .param p2, "value"    # J

    .prologue
    .line 54
    iget-object v0, p0, Lorg/apache/lucene/util/packed/Direct64;->values:[J

    aput-wide p2, v0, p1

    .line 55
    return-void
.end method
