.class Lorg/apache/lucene/util/automaton/Lev2ParametricDescription;
.super Lorg/apache/lucene/util/automaton/LevenshteinAutomata$ParametricDescription;
.source "Lev2ParametricDescription.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final offsetIncrs0:[J

.field private static final offsetIncrs1:[J

.field private static final offsetIncrs2:[J

.field private static final offsetIncrs3:[J

.field private static final offsetIncrs4:[J

.field private static final offsetIncrs5:[J

.field private static final toStates0:[J

.field private static final toStates1:[J

.field private static final toStates2:[J

.field private static final toStates3:[J

.field private static final toStates4:[J

.field private static final toStates5:[J


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 27
    const-class v0, Lorg/apache/lucene/util/automaton/Lev2ParametricDescription;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/automaton/Lev2ParametricDescription;->$assertionsDisabled:Z

    .line 87
    new-array v0, v1, [J

    .line 88
    const-wide/16 v4, 0x23

    aput-wide v4, v0, v2

    .line 87
    sput-object v0, Lorg/apache/lucene/util/automaton/Lev2ParametricDescription;->toStates0:[J

    .line 90
    new-array v0, v1, [J

    sput-object v0, Lorg/apache/lucene/util/automaton/Lev2ParametricDescription;->offsetIncrs0:[J

    .line 95
    new-array v0, v1, [J

    .line 96
    const-wide/32 v4, 0x13688b44

    aput-wide v4, v0, v2

    .line 95
    sput-object v0, Lorg/apache/lucene/util/automaton/Lev2ParametricDescription;->toStates1:[J

    .line 98
    new-array v0, v1, [J

    .line 99
    const-wide/16 v4, 0x3e0

    aput-wide v4, v0, v2

    .line 98
    sput-object v0, Lorg/apache/lucene/util/automaton/Lev2ParametricDescription;->offsetIncrs1:[J

    .line 103
    const/4 v0, 0x3

    new-array v0, v0, [J

    fill-array-data v0, :array_0

    sput-object v0, Lorg/apache/lucene/util/automaton/Lev2ParametricDescription;->toStates2:[J

    .line 106
    const/4 v0, 0x2

    new-array v0, v0, [J

    fill-array-data v0, :array_1

    sput-object v0, Lorg/apache/lucene/util/automaton/Lev2ParametricDescription;->offsetIncrs2:[J

    .line 111
    const/16 v0, 0xe

    new-array v0, v0, [J

    fill-array-data v0, :array_2

    sput-object v0, Lorg/apache/lucene/util/automaton/Lev2ParametricDescription;->toStates3:[J

    .line 117
    const/4 v0, 0x6

    new-array v0, v0, [J

    fill-array-data v0, :array_3

    sput-object v0, Lorg/apache/lucene/util/automaton/Lev2ParametricDescription;->offsetIncrs3:[J

    .line 123
    const/16 v0, 0x26

    new-array v0, v0, [J

    fill-array-data v0, :array_4

    sput-object v0, Lorg/apache/lucene/util/automaton/Lev2ParametricDescription;->toStates4:[J

    .line 135
    const/16 v0, 0x17

    new-array v0, v0, [J

    fill-array-data v0, :array_5

    sput-object v0, Lorg/apache/lucene/util/automaton/Lev2ParametricDescription;->offsetIncrs4:[J

    .line 145
    const/16 v0, 0x4b

    new-array v0, v0, [J

    fill-array-data v0, :array_6

    sput-object v0, Lorg/apache/lucene/util/automaton/Lev2ParametricDescription;->toStates5:[J

    .line 166
    const/16 v0, 0x2d

    new-array v0, v0, [J

    fill-array-data v0, :array_7

    sput-object v0, Lorg/apache/lucene/util/automaton/Lev2ParametricDescription;->offsetIncrs5:[J

    .line 179
    return-void

    :cond_0
    move v0, v2

    .line 27
    goto :goto_0

    .line 103
    :array_0
    .array-data 8
        0x26a09a0a0520a504L    # 1.255696656358472E-122
        0x2323523321a260a2L
        0x354235543213L
    .end array-data

    .line 106
    :array_1
    .array-data 8
        0x5555520280000800L
        0x555555
    .end array-data

    .line 111
    :array_2
    .array-data 8
        0x380e014a051404L
        0xe28245009451140L
        -0x75d977ff6759d974L    # -9.158813432927618E-260
        0x180a288ca0246213L    # 7.166777052025494E-193
        0x494053284a1080e1L    # 7.281119053041306E44
        0x510265a89c311940L    # 1.7450918807810956E82
        0x4218c41188a6509cL    # 2.659217463357872E10
        0x6340c4211c4710dL
        -0x5e97c67b8e77d5eeL    # -9.472620441935283E-148
        0x104c841c683a0425L    # 3.67352349707258E-230
        0x3294472904351483L    # 4.813770854723305E-65
        -0x19d6f9df57b5df30L
        0x1441a0ea2896a4a0L    # 4.189191426287958E-211
        0x32
    .end array-data

    .line 117
    :array_3
    .array-data 8
        0x33300230c0000800L    # 3.89146448601398E-62
        0x220ca080a00fc330L
        0x555555f832823380L    # 1.19466967928939E103
        0x5555555555555555L    # 1.1945305291614955E103
        0x5555555555555555L    # 1.1945305291614955E103
        0x5555
    .end array-data

    .line 123
    :array_4
    .array-data 8
        0x380e014a051404L
        0xaa015452940L
        0x55014501000000L
        0x1843ddc771085c07L    # 8.70863914789903E-192
        0x7141200040108405L    # 3.484795307164455E237
        0x52b44004c5313460L    # 2.5781329246268844E90
        0x401080200063115cL    # 4.1251220760790055
        -0x7aceb3b2e7e3afb8L
        0x1440190a3e5c7828L    # 3.825426960492064E-211
        0x28a232809100a21L
        -0x5fd735d57bdfc7baL    # -9.24450565446071E-154
        -0x35fdbffef7ffef76L    # -3.334053484869306E48
        -0x384bdfa3ea7f5af8L    # -2.6752581100459972E37
        0x1021090251846b6L
        0x4cb513862328090L
        0x210863128ca2b8a2L
        0x4e188ca024402940L    # 1.6546207982000544E68
        0xa6b6c7c520532d4L
        -0x73beefebaeeafde7L
        -0x5f3bdee3b8ef2bdfL    # -7.686730726309189E-151
        0x2108421094e15063L
        -0x70ec3bc8f79cefbcL    # -4.856824493262651E-236
        0x18274d908c611631L    # 2.553785225485831E-192
        0x1cc238c411098263L    # 3.772092599359222E-170
        0x450e3a1d0212d0b4L    # 4.5677758686793327E24
        0x31050242048108c6L    # 1.486321331041106E-72
        0xfa318b42d07308eL
        -0x5779ae7dca96f83aL
        0x1ca410d4520c4140L
        0x2954e13883a0ca51L
        0x3714831044229442L    # 2.299471023186194E-43
        -0x6c6b9ee94a70d37cL    # -2.364674411572029E-214
        -0x3beef65a9ce5a8b3L    # -7.857242998383649E19
        0x1d4512d4941cc520L
        0x52848294c643883aL    # 3.2640450133528006E89
        -0x4adaf8ceb7cefafeL    # -1.0977178163522993E-52
        -0x5aca96c6b9f08ca8L
        0x409ca651
    .end array-data

    .line 135
    :array_5
    .array-data 8
        0x20c0600000010000L    # 6.253072869127164E-151
        0x2000040000000001L
        0x209204a40209L
        0x301b6c0618018618L    # 5.920518440469461E-77
        0x207206186000186cL    # 2.15084339240922E-152
        0x1200061b8e06dc0L
        0x480492080612010L
        -0x5dfdfb5fbffb8000L    # -7.214734759855644E-145
        0x1061a0000129124L
        0x1848349b680612L
        -0x2d925fdfb5fbe798L    # -1.1786756725032803E89
        0x2492492492496128L
        -0x6db6db6db6db6db7L
        0x4924924924924924L    # 2.2937909347060067E44
        0x2492492492492492L
        -0x6db6db6db6db6db7L
        0x4924924924924924L    # 2.2937909347060067E44
        0x2492492492492492L
        -0x6db6db6db6db6db7L
        0x4924924924924924L    # 2.2937909347060067E44
        0x2492492492492492L
        -0x6db6db6db6db6db7L
        0x24924924
    .end array-data

    .line 145
    :array_6
    .array-data 8
        0x380e014a051404L
        0xaa015452940L
        -0x7fad7ebaff000000L    # -4.117490089312098E-307
        -0x47f5aeabaffff1fdL    # -9.667593488167818E-39
        0x5140410842108426L    # 2.4668908469512104E83
        0x71dc421701c01540L
        0x100421014610f7L
        -0x7a3f8ffaafebaff0L    # -5.659681109438749E-281
        -0x6b5d8e7bc22388f0L
        0x1346071412108a22L    # 7.987323693291531E-216
        0x3115c52b44004c53L    # 3.080372923200003E-72
        -0x3afb7bfef7fdfffaL    # -3.1002359739748973E24
        0x54d1001314c4d181L    # 3.718389915010007E100
        0x9081204239c4a71L
        0x14c5313460714124L    # 1.289226338001381E-208
        0x51006428f971e0a2L    # 1.554822846990703E82
        0x4d181c5048402884L    # 2.4796313872213307E63
        0xa3e5c782885314cL
        0x2809409482a8a239L    # 8.011097824175922E-116
        0x2a84203846028a23L    # 7.020142423383457E-104
        0x10800108aa028caL
        0xe1180a288ca0240L
        -0x673947f1cd6b5ef8L
        0x2942328091098c10L
        0x11adb1ed08170560L
        -0x5fdbffbf7bdbf6baL    # -7.458707845164171E-154
        0x7b4205c1580a508cL    # 5.359946927961089E285
        -0x573d69738e7b9494L
        0x4cb5138623280910L
        0x10863128ca2b8a20L    # 4.574149057460839E-229
        -0x1e7735fdbbfd6bfeL    # -6.970250673019751E161
        0x4e3294e288132d44L    # 5.009584488470955E68
        -0x7f6bf652ede73c64L    # -7.133692561185E-306
        -0xeb7eb34aec79dceL    # -4.900328726191816E237
        0x514454086429adb1L    # 3.085239438674652E83
        0x32d44e188ca02440L    # 7.712323707389636E-64
        -0x73c6f5949383adfbL    # -8.74359637783594E-250
        -0x2bde73bebf632d56L    # -1.8741012132237844E97
        0x5063a0c4211c4710L    # 1.818213025461615E79
        0x10442108421094e1L
        0x31084711c4350863L    # 1.717580381477247E-72
        -0x421084220fa6e70eL    # -2.290751964628156E-10
        -0x3b0ef23de73be109L    # -1.2885443844163562E24
        0x9d3642318458c63L
        0x70863104426098c6L    # 1.1024851391130456E234
        0x8c6116318f13c43L
        0x41ef75dd6b5de4d9L    # 4.2225447309341855E9
        -0x2fded2f4be33dc74L    # -9.9441974232771E77
        0x2048108c6450e3a1L
        0x42d07308e3105024L    # 7.234502552812856E13
        -0x24a6e6c70d8bf7b5L    # -1.1134387227395404E132
        -0x3dc73be088211045L    # -1.0636961174293645E11
        0x1f183e8c62d0b41cL    # 6.897829860986723E-159
        0x502a2194608d5a4L
        -0x5ce74bd2f8cf71cfL
        -0x1298a24a96f839f1L    # -1.031032023865972E219
        -0x5bef2badf3be088dL    # -5.78855266951674E-135
        0x54e13883a0ca511cL    # 7.53322329562034E100
        0x1483104422944229L    # 7.24830689379023E-210
        0x20f2329447290435L    # 5.559243372411571E-150
        0x1ef6f7ef6f7df05cL
        -0x529c34def23adf3cL    # -4.858400475187962E-90
        0x58c695d364e51845L    # 4.556310687087604E119
        -0x37bc8eb7cefbbd97L    # -1.3231885149141754E40
        -0x1b26c6b9ee94a70eL
        0x520c41ef717d6b17L    # 1.7566426406451008E87
        -0x7c5e2baed2b6be34L
        0x50252848294c6438L    # 1.2249269429733726E78
        0x144b525073148310L    # 6.492593673861305E-211
        -0x105084a6e3df0d8bL    # -9.54611489473386E229
        -0x6be33adf3be08885L    # -8.546009955696334E-212
        -0x2a5b1ae7c2329d2cL
        0x4831050272994694L    # 5.791459086567042E39
        0x460f7358b5250731L    # 3.1147138187189455E29
        -0x8864298e84a96c7L    # -3.319878971838167E267
    .end array-data

    .line 166
    :array_7
    .array-data 8
        0x20c0600000010000L    # 6.253072869127164E-151
        0x40000000001L
        0xb6db6d4830180L
        0x4812900824800010L    # 1.5791334286631896E39
        0x2092000040000082L    # 8.592010343226826E-152
        0x618000b659254a40L    # 4.499696159336966E161
        -0x793cfe493f9e7fe8L    # -4.288847323293568E-276
        -0x24fe79ff9e79ffffL    # -2.429336010509142E130
        -0x7e79e7fff8a4512aL    # -2.577411685076928E-301
        0x186e381b70081cL
        -0x1a923fdf8df9e7a0L    # -3.8578169533254054E180
        0x61201001200075b8L    # 7.057019536553266E159
        0x480000480492080L
        0x52b5248201848040L    # 2.6917659139220016E90
        -0x77f7ed7effedfff5L    # -5.695752906109094E-270
        0x4004800004a4492L
        0xb529124a20204aL
        0x49b68061201061a0L    # 1.2846075314608206E47
        -0x7b7fbe797ffe7b7dL    # -5.337854411165375E-287
        0x1a000752ad26da01L
        0x4a349b6808128106L    # 3.011724629679127E49
        -0x5fdfb5fbe797ffe8L    # -6.074492219382477E-154
        0x492492497528d26dL    # 2.29379147030535E44
        0x2492492492492492L
        -0x6db6db6db6db6db7L
        0x4924924924924924L    # 2.2937909347060067E44
        0x2492492492492492L
        -0x6db6db6db6db6db7L
        0x4924924924924924L    # 2.2937909347060067E44
        0x2492492492492492L
        -0x6db6db6db6db6db7L
        0x4924924924924924L    # 2.2937909347060067E44
        0x2492492492492492L
        -0x6db6db6db6db6db7L
        0x4924924924924924L    # 2.2937909347060067E44
        0x2492492492492492L
        -0x6db6db6db6db6db7L
        0x4924924924924924L    # 2.2937909347060067E44
        0x2492492492492492L
        -0x6db6db6db6db6db7L
        0x4924924924924924L    # 2.2937909347060067E44
        0x2492492492492492L
        -0x6db6db6db6db6db7L
        0x4924924924924924L    # 2.2937909347060067E44
        0x2492492492492492L
    .end array-data
.end method

.method public constructor <init>(I)V
    .locals 6
    .param p1, "w"    # I

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, -0x1

    const/4 v2, -0x2

    .line 215
    const/16 v0, 0x1e

    new-array v0, v0, [I

    aput v5, v0, v4

    aput v4, v0, v5

    const/4 v1, 0x4

    aput v4, v0, v1

    const/4 v1, 0x5

    aput v3, v0, v1

    const/16 v1, 0x8

    aput v3, v0, v1

    const/16 v1, 0xa

    aput v3, v0, v1

    const/16 v1, 0xb

    aput v3, v0, v1

    const/16 v1, 0xc

    aput v3, v0, v1

    const/16 v1, 0xd

    aput v3, v0, v1

    const/16 v1, 0xe

    aput v3, v0, v1

    const/16 v1, 0xf

    aput v2, v0, v1

    const/16 v1, 0x10

    aput v3, v0, v1

    const/16 v1, 0x11

    aput v3, v0, v1

    const/16 v1, 0x12

    aput v2, v0, v1

    const/16 v1, 0x13

    aput v3, v0, v1

    const/16 v1, 0x14

    aput v2, v0, v1

    const/16 v1, 0x15

    aput v2, v0, v1

    const/16 v1, 0x16

    aput v2, v0, v1

    const/16 v1, 0x17

    aput v2, v0, v1

    const/16 v1, 0x18

    aput v2, v0, v1

    const/16 v1, 0x19

    aput v2, v0, v1

    const/16 v1, 0x1a

    aput v2, v0, v1

    const/16 v1, 0x1b

    aput v2, v0, v1

    const/16 v1, 0x1c

    aput v2, v0, v1

    const/16 v1, 0x1d

    aput v2, v0, v1

    invoke-direct {p0, p1, v5, v0}, Lorg/apache/lucene/util/automaton/LevenshteinAutomata$ParametricDescription;-><init>(II[I)V

    .line 216
    return-void
.end method


# virtual methods
.method transition(III)I
    .locals 9
    .param p1, "absState"    # I
    .param p2, "position"    # I
    .param p3, "vector"    # I

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x2

    const/4 v3, -0x1

    const/4 v6, 0x5

    const/4 v5, 0x3

    .line 32
    sget-boolean v4, Lorg/apache/lucene/util/automaton/Lev2ParametricDescription;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    if-ne p1, v3, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 35
    :cond_0
    iget v4, p0, Lorg/apache/lucene/util/automaton/Lev2ParametricDescription;->w:I

    add-int/lit8 v4, v4, 0x1

    div-int v2, p1, v4

    .line 36
    .local v2, "state":I
    iget v4, p0, Lorg/apache/lucene/util/automaton/Lev2ParametricDescription;->w:I

    add-int/lit8 v4, v4, 0x1

    rem-int v1, p1, v4

    .line 37
    .local v1, "offset":I
    sget-boolean v4, Lorg/apache/lucene/util/automaton/Lev2ParametricDescription;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    if-gez v1, :cond_1

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 39
    :cond_1
    iget v4, p0, Lorg/apache/lucene/util/automaton/Lev2ParametricDescription;->w:I

    if-ne p2, v4, :cond_3

    .line 40
    if-ge v2, v5, :cond_2

    .line 41
    mul-int/lit8 v4, p3, 0x3

    add-int v0, v4, v2

    .line 42
    .local v0, "loc":I
    sget-object v4, Lorg/apache/lucene/util/automaton/Lev2ParametricDescription;->offsetIncrs0:[J

    invoke-virtual {p0, v4, v0, v8}, Lorg/apache/lucene/util/automaton/Lev2ParametricDescription;->unpack([JII)I

    move-result v4

    add-int/2addr v1, v4

    .line 43
    sget-object v4, Lorg/apache/lucene/util/automaton/Lev2ParametricDescription;->toStates0:[J

    invoke-virtual {p0, v4, v0, v7}, Lorg/apache/lucene/util/automaton/Lev2ParametricDescription;->unpack([JII)I

    move-result v4

    add-int/lit8 v2, v4, -0x1

    .line 77
    .end local v0    # "loc":I
    :cond_2
    :goto_0
    if-ne v2, v3, :cond_8

    .line 82
    :goto_1
    return v3

    .line 45
    :cond_3
    iget v4, p0, Lorg/apache/lucene/util/automaton/Lev2ParametricDescription;->w:I

    add-int/lit8 v4, v4, -0x1

    if-ne p2, v4, :cond_4

    .line 46
    if-ge v2, v6, :cond_2

    .line 47
    mul-int/lit8 v4, p3, 0x5

    add-int v0, v4, v2

    .line 48
    .restart local v0    # "loc":I
    sget-object v4, Lorg/apache/lucene/util/automaton/Lev2ParametricDescription;->offsetIncrs1:[J

    invoke-virtual {p0, v4, v0, v8}, Lorg/apache/lucene/util/automaton/Lev2ParametricDescription;->unpack([JII)I

    move-result v4

    add-int/2addr v1, v4

    .line 49
    sget-object v4, Lorg/apache/lucene/util/automaton/Lev2ParametricDescription;->toStates1:[J

    invoke-virtual {p0, v4, v0, v5}, Lorg/apache/lucene/util/automaton/Lev2ParametricDescription;->unpack([JII)I

    move-result v4

    add-int/lit8 v2, v4, -0x1

    .line 51
    goto :goto_0

    .end local v0    # "loc":I
    :cond_4
    iget v4, p0, Lorg/apache/lucene/util/automaton/Lev2ParametricDescription;->w:I

    add-int/lit8 v4, v4, -0x2

    if-ne p2, v4, :cond_5

    .line 52
    const/16 v4, 0xb

    if-ge v2, v4, :cond_2

    .line 53
    mul-int/lit8 v4, p3, 0xb

    add-int v0, v4, v2

    .line 54
    .restart local v0    # "loc":I
    sget-object v4, Lorg/apache/lucene/util/automaton/Lev2ParametricDescription;->offsetIncrs2:[J

    invoke-virtual {p0, v4, v0, v7}, Lorg/apache/lucene/util/automaton/Lev2ParametricDescription;->unpack([JII)I

    move-result v4

    add-int/2addr v1, v4

    .line 55
    sget-object v4, Lorg/apache/lucene/util/automaton/Lev2ParametricDescription;->toStates2:[J

    const/4 v5, 0x4

    invoke-virtual {p0, v4, v0, v5}, Lorg/apache/lucene/util/automaton/Lev2ParametricDescription;->unpack([JII)I

    move-result v4

    add-int/lit8 v2, v4, -0x1

    .line 57
    goto :goto_0

    .end local v0    # "loc":I
    :cond_5
    iget v4, p0, Lorg/apache/lucene/util/automaton/Lev2ParametricDescription;->w:I

    add-int/lit8 v4, v4, -0x3

    if-ne p2, v4, :cond_6

    .line 58
    const/16 v4, 0x15

    if-ge v2, v4, :cond_2

    .line 59
    mul-int/lit8 v4, p3, 0x15

    add-int v0, v4, v2

    .line 60
    .restart local v0    # "loc":I
    sget-object v4, Lorg/apache/lucene/util/automaton/Lev2ParametricDescription;->offsetIncrs3:[J

    invoke-virtual {p0, v4, v0, v7}, Lorg/apache/lucene/util/automaton/Lev2ParametricDescription;->unpack([JII)I

    move-result v4

    add-int/2addr v1, v4

    .line 61
    sget-object v4, Lorg/apache/lucene/util/automaton/Lev2ParametricDescription;->toStates3:[J

    invoke-virtual {p0, v4, v0, v6}, Lorg/apache/lucene/util/automaton/Lev2ParametricDescription;->unpack([JII)I

    move-result v4

    add-int/lit8 v2, v4, -0x1

    .line 63
    goto :goto_0

    .end local v0    # "loc":I
    :cond_6
    iget v4, p0, Lorg/apache/lucene/util/automaton/Lev2ParametricDescription;->w:I

    add-int/lit8 v4, v4, -0x4

    if-ne p2, v4, :cond_7

    .line 64
    const/16 v4, 0x1e

    if-ge v2, v4, :cond_2

    .line 65
    mul-int/lit8 v4, p3, 0x1e

    add-int v0, v4, v2

    .line 66
    .restart local v0    # "loc":I
    sget-object v4, Lorg/apache/lucene/util/automaton/Lev2ParametricDescription;->offsetIncrs4:[J

    invoke-virtual {p0, v4, v0, v5}, Lorg/apache/lucene/util/automaton/Lev2ParametricDescription;->unpack([JII)I

    move-result v4

    add-int/2addr v1, v4

    .line 67
    sget-object v4, Lorg/apache/lucene/util/automaton/Lev2ParametricDescription;->toStates4:[J

    invoke-virtual {p0, v4, v0, v6}, Lorg/apache/lucene/util/automaton/Lev2ParametricDescription;->unpack([JII)I

    move-result v4

    add-int/lit8 v2, v4, -0x1

    .line 69
    goto :goto_0

    .line 70
    .end local v0    # "loc":I
    :cond_7
    const/16 v4, 0x1e

    if-ge v2, v4, :cond_2

    .line 71
    mul-int/lit8 v4, p3, 0x1e

    add-int v0, v4, v2

    .line 72
    .restart local v0    # "loc":I
    sget-object v4, Lorg/apache/lucene/util/automaton/Lev2ParametricDescription;->offsetIncrs5:[J

    invoke-virtual {p0, v4, v0, v5}, Lorg/apache/lucene/util/automaton/Lev2ParametricDescription;->unpack([JII)I

    move-result v4

    add-int/2addr v1, v4

    .line 73
    sget-object v4, Lorg/apache/lucene/util/automaton/Lev2ParametricDescription;->toStates5:[J

    invoke-virtual {p0, v4, v0, v6}, Lorg/apache/lucene/util/automaton/Lev2ParametricDescription;->unpack([JII)I

    move-result v4

    add-int/lit8 v2, v4, -0x1

    goto/16 :goto_0

    .line 82
    .end local v0    # "loc":I
    :cond_8
    iget v3, p0, Lorg/apache/lucene/util/automaton/Lev2ParametricDescription;->w:I

    add-int/lit8 v3, v3, 0x1

    mul-int/2addr v3, v2

    add-int/2addr v3, v1

    goto/16 :goto_1
.end method
