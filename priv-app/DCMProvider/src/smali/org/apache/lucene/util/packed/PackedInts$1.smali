.class Lorg/apache/lucene/util/packed/PackedInts$1;
.super Lorg/apache/lucene/util/packed/DirectPackedReader;
.source "PackedInts.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/util/packed/PackedInts;->getDirectReaderNoHeader(Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/util/packed/PackedInts$Format;III)Lorg/apache/lucene/util/packed/PackedInts$Reader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic val$endPointer:J

.field private final synthetic val$in:Lorg/apache/lucene/store/IndexInput;


# direct methods
.method constructor <init>(IILorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/store/IndexInput;J)V
    .locals 1
    .param p1, "$anonymous0"    # I
    .param p2, "$anonymous1"    # I
    .param p3, "$anonymous2"    # Lorg/apache/lucene/store/IndexInput;

    .prologue
    .line 1
    iput-object p4, p0, Lorg/apache/lucene/util/packed/PackedInts$1;->val$in:Lorg/apache/lucene/store/IndexInput;

    iput-wide p5, p0, Lorg/apache/lucene/util/packed/PackedInts$1;->val$endPointer:J

    .line 955
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/util/packed/DirectPackedReader;-><init>(IILorg/apache/lucene/store/IndexInput;)V

    return-void
.end method


# virtual methods
.method public get(I)J
    .locals 6
    .param p1, "index"    # I

    .prologue
    .line 958
    invoke-super {p0, p1}, Lorg/apache/lucene/util/packed/DirectPackedReader;->get(I)J

    move-result-wide v2

    .line 959
    .local v2, "result":J
    iget v1, p0, Lorg/apache/lucene/util/packed/PackedInts$1;->valueCount:I

    add-int/lit8 v1, v1, -0x1

    if-ne p1, v1, :cond_0

    .line 961
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/util/packed/PackedInts$1;->val$in:Lorg/apache/lucene/store/IndexInput;

    iget-wide v4, p0, Lorg/apache/lucene/util/packed/PackedInts$1;->val$endPointer:J

    invoke-virtual {v1, v4, v5}, Lorg/apache/lucene/store/IndexInput;->seek(J)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 966
    :cond_0
    return-wide v2

    .line 962
    :catch_0
    move-exception v0

    .line 963
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v4, "failed"

    invoke-direct {v1, v4, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
