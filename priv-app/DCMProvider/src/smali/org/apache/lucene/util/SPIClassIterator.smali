.class public final Lorg/apache/lucene/util/SPIClassIterator;
.super Ljava/lang/Object;
.source "SPIClassIterator.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<S:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Ljava/lang/Class",
        "<+TS;>;>;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final clazz:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TS;>;"
        }
    .end annotation
.end field

.field private codecList:[Ljava/lang/Class;

.field private docValuesFormatList:[Ljava/lang/Class;

.field private linesIterator:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/Class;",
            ">;"
        }
    .end annotation
.end field

.field private final loader:Ljava/lang/ClassLoader;

.field private postingFormatList:[Ljava/lang/Class;

.field private profilesEnum:Ljava/util/Enumeration;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Enumeration",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-class v0, Lorg/apache/lucene/util/SPIClassIterator;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/SPIClassIterator;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/Class;Ljava/lang/ClassLoader;)V
    .locals 7
    .param p2, "loader"    # Ljava/lang/ClassLoader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<TS;>;",
            "Ljava/lang/ClassLoader;",
            ")V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/util/SPIClassIterator;, "Lorg/apache/lucene/util/SPIClassIterator<TS;>;"
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TS;>;"
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Class;

    .line 52
    const-class v1, Lorg/apache/lucene/codecs/lucene40/Lucene40Codec;

    aput-object v1, v0, v3

    .line 53
    const-class v1, Lorg/apache/lucene/codecs/lucene3x/Lucene3xCodec;

    aput-object v1, v0, v4

    .line 54
    const-class v1, Lorg/apache/lucene/codecs/lucene41/Lucene41Codec;

    aput-object v1, v0, v5

    .line 55
    const-class v1, Lorg/apache/lucene/codecs/lucene42/Lucene42Codec;

    aput-object v1, v0, v6

    const/4 v1, 0x4

    .line 56
    const-class v2, Lorg/apache/lucene/codecs/simpletext/SimpleTextCodec;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    .line 57
    const-class v2, Lcom/samsung/dcm/lucene/codecs/DCMCodec;

    aput-object v2, v0, v1

    iput-object v0, p0, Lorg/apache/lucene/util/SPIClassIterator;->codecList:[Ljava/lang/Class;

    .line 67
    new-array v0, v5, [Ljava/lang/Class;

    .line 69
    const-class v1, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesFormat;

    aput-object v1, v0, v3

    .line 70
    const-class v1, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesFormat;

    aput-object v1, v0, v4

    iput-object v0, p0, Lorg/apache/lucene/util/SPIClassIterator;->docValuesFormatList:[Ljava/lang/Class;

    .line 74
    new-array v0, v6, [Ljava/lang/Class;

    .line 75
    const-class v1, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsFormat;

    aput-object v1, v0, v3

    .line 76
    const-class v1, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsFormat;

    aput-object v1, v0, v4

    .line 77
    const-class v1, Lorg/apache/lucene/codecs/simpletext/SimpleTextPostingsFormat;

    aput-object v1, v0, v5

    iput-object v0, p0, Lorg/apache/lucene/util/SPIClassIterator;->postingFormatList:[Ljava/lang/Class;

    .line 117
    iput-object p1, p0, Lorg/apache/lucene/util/SPIClassIterator;->clazz:Ljava/lang/Class;

    .line 119
    new-array v0, v4, [Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->enumeration(Ljava/util/Collection;)Ljava/util/Enumeration;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/util/SPIClassIterator;->profilesEnum:Ljava/util/Enumeration;

    .line 121
    if-nez p2, :cond_0

    invoke-static {}, Ljava/lang/ClassLoader;->getSystemClassLoader()Ljava/lang/ClassLoader;

    move-result-object p2

    .end local p2    # "loader":Ljava/lang/ClassLoader;
    :cond_0
    iput-object p2, p0, Lorg/apache/lucene/util/SPIClassIterator;->loader:Ljava/lang/ClassLoader;

    .line 122
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/util/SPIClassIterator;->linesIterator:Ljava/util/Iterator;

    .line 124
    return-void
.end method

.method public static get(Ljava/lang/Class;)Lorg/apache/lucene/util/SPIClassIterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TS;>;)",
            "Lorg/apache/lucene/util/SPIClassIterator",
            "<TS;>;"
        }
    .end annotation

    .prologue
    .line 92
    .local p0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TS;>;"
    new-instance v0, Lorg/apache/lucene/util/SPIClassIterator;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    .line 93
    invoke-virtual {v1}, Ljava/lang/Thread;->getContextClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    .line 92
    invoke-direct {v0, p0, v1}, Lorg/apache/lucene/util/SPIClassIterator;-><init>(Ljava/lang/Class;Ljava/lang/ClassLoader;)V

    return-object v0
.end method

.method public static get(Ljava/lang/Class;Ljava/lang/ClassLoader;)Lorg/apache/lucene/util/SPIClassIterator;
    .locals 1
    .param p1, "loader"    # Ljava/lang/ClassLoader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TS;>;",
            "Ljava/lang/ClassLoader;",
            ")",
            "Lorg/apache/lucene/util/SPIClassIterator",
            "<TS;>;"
        }
    .end annotation

    .prologue
    .line 97
    .local p0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TS;>;"
    new-instance v0, Lorg/apache/lucene/util/SPIClassIterator;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/util/SPIClassIterator;-><init>(Ljava/lang/Class;Ljava/lang/ClassLoader;)V

    return-object v0
.end method

.method public static isParentClassLoader(Ljava/lang/ClassLoader;Ljava/lang/ClassLoader;)Z
    .locals 1
    .param p0, "parent"    # Ljava/lang/ClassLoader;
    .param p1, "child"    # Ljava/lang/ClassLoader;

    .prologue
    .line 107
    :goto_0
    if-nez p1, :cond_0

    .line 113
    const/4 v0, 0x0

    :goto_1
    return v0

    .line 108
    :cond_0
    if-ne p1, p0, :cond_1

    .line 109
    const/4 v0, 0x1

    goto :goto_1

    .line 111
    :cond_1
    invoke-virtual {p1}, Ljava/lang/ClassLoader;->getParent()Ljava/lang/ClassLoader;

    move-result-object p1

    goto :goto_0
.end method

.method private loadNextProfile()Z
    .locals 3

    .prologue
    .line 127
    .local p0, "this":Lorg/apache/lucene/util/SPIClassIterator;, "Lorg/apache/lucene/util/SPIClassIterator<TS;>;"
    const/4 v0, 0x0

    .line 128
    .local v0, "lines":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Class;>;"
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/util/SPIClassIterator;->profilesEnum:Ljava/util/Enumeration;

    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-nez v2, :cond_1

    .line 149
    const/4 v2, 0x0

    :goto_0
    return v2

    .line 129
    :cond_1
    if-eqz v0, :cond_3

    .line 130
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 134
    :goto_1
    iget-object v2, p0, Lorg/apache/lucene/util/SPIClassIterator;->profilesEnum:Ljava/util/Enumeration;

    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 136
    .local v1, "profile":Ljava/lang/String;
    const-string v2, "org.apache.lucene.codecs.Codec"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 137
    new-instance v0, Ljava/util/ArrayList;

    .end local v0    # "lines":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Class;>;"
    iget-object v2, p0, Lorg/apache/lucene/util/SPIClassIterator;->codecList:[Ljava/lang/Class;

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 144
    .restart local v0    # "lines":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Class;>;"
    :cond_2
    :goto_2
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 145
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/lucene/util/SPIClassIterator;->linesIterator:Ljava/util/Iterator;

    .line 146
    const/4 v2, 0x1

    goto :goto_0

    .line 132
    .end local v1    # "profile":Ljava/lang/String;
    :cond_3
    new-instance v0, Ljava/util/ArrayList;

    .end local v0    # "lines":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Class;>;"
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .restart local v0    # "lines":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Class;>;"
    goto :goto_1

    .line 138
    .restart local v1    # "profile":Ljava/lang/String;
    :cond_4
    const-string v2, "org.apache.lucene.codecs.DocValuesFormat"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 139
    new-instance v0, Ljava/util/ArrayList;

    .end local v0    # "lines":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Class;>;"
    iget-object v2, p0, Lorg/apache/lucene/util/SPIClassIterator;->docValuesFormatList:[Ljava/lang/Class;

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .restart local v0    # "lines":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Class;>;"
    goto :goto_2

    .line 140
    :cond_5
    const-string v2, "org.apache.lucene.codecs.PostingsFormat"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 141
    new-instance v0, Ljava/util/ArrayList;

    .end local v0    # "lines":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Class;>;"
    iget-object v2, p0, Lorg/apache/lucene/util/SPIClassIterator;->postingFormatList:[Ljava/lang/Class;

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .restart local v0    # "lines":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Class;>;"
    goto :goto_2
.end method


# virtual methods
.method public hasNext()Z
    .locals 1

    .prologue
    .line 155
    .local p0, "this":Lorg/apache/lucene/util/SPIClassIterator;, "Lorg/apache/lucene/util/SPIClassIterator<TS;>;"
    iget-object v0, p0, Lorg/apache/lucene/util/SPIClassIterator;->linesIterator:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/util/SPIClassIterator;->loadNextProfile()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public next()Ljava/lang/Class;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+TS;>;"
        }
    .end annotation

    .prologue
    .line 162
    .local p0, "this":Lorg/apache/lucene/util/SPIClassIterator;, "Lorg/apache/lucene/util/SPIClassIterator<TS;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/util/SPIClassIterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 163
    new-instance v1, Ljava/util/NoSuchElementException;

    invoke-direct {v1}, Ljava/util/NoSuchElementException;-><init>()V

    throw v1

    .line 165
    :cond_0
    sget-boolean v1, Lorg/apache/lucene/util/SPIClassIterator;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lorg/apache/lucene/util/SPIClassIterator;->linesIterator:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 166
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/util/SPIClassIterator;->linesIterator:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 170
    .local v0, "c":Ljava/lang/Class;
    iget-object v1, p0, Lorg/apache/lucene/util/SPIClassIterator;->clazz:Ljava/lang/Class;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->asSubclass(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/util/SPIClassIterator;->next()Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 183
    .local p0, "this":Lorg/apache/lucene/util/SPIClassIterator;, "Lorg/apache/lucene/util/SPIClassIterator<TS;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
