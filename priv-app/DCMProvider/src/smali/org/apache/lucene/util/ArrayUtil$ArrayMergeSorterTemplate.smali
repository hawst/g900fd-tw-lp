.class abstract Lorg/apache/lucene/util/ArrayUtil$ArrayMergeSorterTemplate;
.super Lorg/apache/lucene/util/ArrayUtil$ArraySorterTemplate;
.source "ArrayUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/ArrayUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "ArrayMergeSorterTemplate"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lorg/apache/lucene/util/ArrayUtil$ArraySorterTemplate",
        "<TT;>;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final threshold:I

.field private final tmp:[Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[TT;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 650
    const-class v0, Lorg/apache/lucene/util/ArrayUtil;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/ArrayUtil$ArrayMergeSorterTemplate;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>([Ljava/lang/Object;F)V
    .locals 2
    .param p1, "a"    # [Ljava/lang/Object;
    .param p2, "overheadRatio"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TT;F)V"
        }
    .end annotation

    .prologue
    .line 656
    .local p0, "this":Lorg/apache/lucene/util/ArrayUtil$ArrayMergeSorterTemplate;, "Lorg/apache/lucene/util/ArrayUtil$ArrayMergeSorterTemplate<TT;>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/util/ArrayUtil$ArraySorterTemplate;-><init>([Ljava/lang/Object;)V

    .line 657
    array-length v1, p1

    int-to-float v1, v1

    mul-float/2addr v1, p2

    float-to-int v1, v1

    iput v1, p0, Lorg/apache/lucene/util/ArrayUtil$ArrayMergeSorterTemplate;->threshold:I

    .line 659
    iget v1, p0, Lorg/apache/lucene/util/ArrayUtil$ArrayMergeSorterTemplate;->threshold:I

    new-array v0, v1, [Ljava/lang/Object;

    .line 660
    .local v0, "tmpBuf":[Ljava/lang/Object;
    iput-object v0, p0, Lorg/apache/lucene/util/ArrayUtil$ArrayMergeSorterTemplate;->tmp:[Ljava/lang/Object;

    .line 661
    return-void
.end method

.method private mergeWithExtraMemory(IIIII)V
    .locals 9
    .param p1, "lo"    # I
    .param p2, "pivot"    # I
    .param p3, "hi"    # I
    .param p4, "len1"    # I
    .param p5, "len2"    # I

    .prologue
    .line 664
    .local p0, "this":Lorg/apache/lucene/util/ArrayUtil$ArrayMergeSorterTemplate;, "Lorg/apache/lucene/util/ArrayUtil$ArrayMergeSorterTemplate<TT;>;"
    iget-object v6, p0, Lorg/apache/lucene/util/ArrayUtil$ArrayMergeSorterTemplate;->a:[Ljava/lang/Object;

    iget-object v7, p0, Lorg/apache/lucene/util/ArrayUtil$ArrayMergeSorterTemplate;->tmp:[Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v6, p1, v7, v8, p4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 665
    const/4 v2, 0x0

    .local v2, "i":I
    move v4, p2

    .local v4, "j":I
    move v0, p1

    .local v0, "dest":I
    move v1, v0

    .end local v0    # "dest":I
    .local v1, "dest":I
    move v5, v4

    .end local v4    # "j":I
    .local v5, "j":I
    move v3, v2

    .line 666
    .end local v2    # "i":I
    .local v3, "i":I
    :goto_0
    if-ge v3, p4, :cond_0

    if-lt v5, p3, :cond_1

    .line 673
    :cond_0
    :goto_1
    if-lt v3, p4, :cond_3

    .line 676
    sget-boolean v6, Lorg/apache/lucene/util/ArrayUtil$ArrayMergeSorterTemplate;->$assertionsDisabled:Z

    if-nez v6, :cond_4

    if-eq v5, v1, :cond_4

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 667
    :cond_1
    iget-object v6, p0, Lorg/apache/lucene/util/ArrayUtil$ArrayMergeSorterTemplate;->tmp:[Ljava/lang/Object;

    aget-object v6, v6, v3

    iget-object v7, p0, Lorg/apache/lucene/util/ArrayUtil$ArrayMergeSorterTemplate;->a:[Ljava/lang/Object;

    aget-object v7, v7, v5

    invoke-virtual {p0, v6, v7}, Lorg/apache/lucene/util/ArrayUtil$ArrayMergeSorterTemplate;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v6

    if-gtz v6, :cond_2

    .line 668
    iget-object v6, p0, Lorg/apache/lucene/util/ArrayUtil$ArrayMergeSorterTemplate;->a:[Ljava/lang/Object;

    add-int/lit8 v0, v1, 0x1

    .end local v1    # "dest":I
    .restart local v0    # "dest":I
    iget-object v7, p0, Lorg/apache/lucene/util/ArrayUtil$ArrayMergeSorterTemplate;->tmp:[Ljava/lang/Object;

    add-int/lit8 v2, v3, 0x1

    .end local v3    # "i":I
    .restart local v2    # "i":I
    aget-object v7, v7, v3

    aput-object v7, v6, v1

    move v1, v0

    .end local v0    # "dest":I
    .restart local v1    # "dest":I
    move v3, v2

    .line 669
    .end local v2    # "i":I
    .restart local v3    # "i":I
    goto :goto_0

    .line 670
    :cond_2
    iget-object v6, p0, Lorg/apache/lucene/util/ArrayUtil$ArrayMergeSorterTemplate;->a:[Ljava/lang/Object;

    add-int/lit8 v0, v1, 0x1

    .end local v1    # "dest":I
    .restart local v0    # "dest":I
    iget-object v7, p0, Lorg/apache/lucene/util/ArrayUtil$ArrayMergeSorterTemplate;->a:[Ljava/lang/Object;

    add-int/lit8 v4, v5, 0x1

    .end local v5    # "j":I
    .restart local v4    # "j":I
    aget-object v7, v7, v5

    aput-object v7, v6, v1

    move v1, v0

    .end local v0    # "dest":I
    .restart local v1    # "dest":I
    move v5, v4

    .end local v4    # "j":I
    .restart local v5    # "j":I
    goto :goto_0

    .line 674
    :cond_3
    iget-object v6, p0, Lorg/apache/lucene/util/ArrayUtil$ArrayMergeSorterTemplate;->a:[Ljava/lang/Object;

    add-int/lit8 v0, v1, 0x1

    .end local v1    # "dest":I
    .restart local v0    # "dest":I
    iget-object v7, p0, Lorg/apache/lucene/util/ArrayUtil$ArrayMergeSorterTemplate;->tmp:[Ljava/lang/Object;

    add-int/lit8 v2, v3, 0x1

    .end local v3    # "i":I
    .restart local v2    # "i":I
    aget-object v7, v7, v3

    aput-object v7, v6, v1

    move v1, v0

    .end local v0    # "dest":I
    .restart local v1    # "dest":I
    move v3, v2

    .end local v2    # "i":I
    .restart local v3    # "i":I
    goto :goto_1

    .line 677
    :cond_4
    return-void
.end method


# virtual methods
.method protected merge(IIIII)V
    .locals 1
    .param p1, "lo"    # I
    .param p2, "pivot"    # I
    .param p3, "hi"    # I
    .param p4, "len1"    # I
    .param p5, "len2"    # I

    .prologue
    .line 681
    .local p0, "this":Lorg/apache/lucene/util/ArrayUtil$ArrayMergeSorterTemplate;, "Lorg/apache/lucene/util/ArrayUtil$ArrayMergeSorterTemplate<TT;>;"
    iget v0, p0, Lorg/apache/lucene/util/ArrayUtil$ArrayMergeSorterTemplate;->threshold:I

    if-gt p4, v0, :cond_0

    .line 682
    invoke-direct/range {p0 .. p5}, Lorg/apache/lucene/util/ArrayUtil$ArrayMergeSorterTemplate;->mergeWithExtraMemory(IIIII)V

    .line 688
    :goto_0
    return-void

    .line 686
    :cond_0
    invoke-super/range {p0 .. p5}, Lorg/apache/lucene/util/ArrayUtil$ArraySorterTemplate;->merge(IIIII)V

    goto :goto_0
.end method
