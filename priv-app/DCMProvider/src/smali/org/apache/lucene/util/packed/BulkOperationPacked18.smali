.class final Lorg/apache/lucene/util/packed/BulkOperationPacked18;
.super Lorg/apache/lucene/util/packed/BulkOperationPacked;
.source "BulkOperationPacked18.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    const/16 v0, 0x12

    invoke-direct {p0, v0}, Lorg/apache/lucene/util/packed/BulkOperationPacked;-><init>(I)V

    .line 29
    return-void
.end method


# virtual methods
.method public decode([BI[III)V
    .locals 15
    .param p1, "blocks"    # [B
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [I
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 80
    const/4 v11, 0x0

    .local v11, "i":I
    move/from16 v12, p4

    .end local p4    # "valuesOffset":I
    .local v12, "valuesOffset":I
    move/from16 v1, p2

    .end local p2    # "blocksOffset":I
    .local v1, "blocksOffset":I
    :goto_0
    move/from16 v0, p5

    if-lt v11, v0, :cond_0

    .line 95
    return-void

    .line 81
    :cond_0
    add-int/lit8 p2, v1, 0x1

    .end local v1    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v13, p1, v1

    and-int/lit16 v2, v13, 0xff

    .line 82
    .local v2, "byte0":I
    add-int/lit8 v1, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v1    # "blocksOffset":I
    aget-byte v13, p1, p2

    and-int/lit16 v3, v13, 0xff

    .line 83
    .local v3, "byte1":I
    add-int/lit8 p2, v1, 0x1

    .end local v1    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v13, p1, v1

    and-int/lit16 v4, v13, 0xff

    .line 84
    .local v4, "byte2":I
    add-int/lit8 p4, v12, 0x1

    .end local v12    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    shl-int/lit8 v13, v2, 0xa

    shl-int/lit8 v14, v3, 0x2

    or-int/2addr v13, v14

    ushr-int/lit8 v14, v4, 0x6

    or-int/2addr v13, v14

    aput v13, p3, v12

    .line 85
    add-int/lit8 v1, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v1    # "blocksOffset":I
    aget-byte v13, p1, p2

    and-int/lit16 v5, v13, 0xff

    .line 86
    .local v5, "byte3":I
    add-int/lit8 p2, v1, 0x1

    .end local v1    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v13, p1, v1

    and-int/lit16 v6, v13, 0xff

    .line 87
    .local v6, "byte4":I
    add-int/lit8 v12, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v12    # "valuesOffset":I
    and-int/lit8 v13, v4, 0x3f

    shl-int/lit8 v13, v13, 0xc

    shl-int/lit8 v14, v5, 0x4

    or-int/2addr v13, v14

    ushr-int/lit8 v14, v6, 0x4

    or-int/2addr v13, v14

    aput v13, p3, p4

    .line 88
    add-int/lit8 v1, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v1    # "blocksOffset":I
    aget-byte v13, p1, p2

    and-int/lit16 v7, v13, 0xff

    .line 89
    .local v7, "byte5":I
    add-int/lit8 p2, v1, 0x1

    .end local v1    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v13, p1, v1

    and-int/lit16 v8, v13, 0xff

    .line 90
    .local v8, "byte6":I
    add-int/lit8 p4, v12, 0x1

    .end local v12    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    and-int/lit8 v13, v6, 0xf

    shl-int/lit8 v13, v13, 0xe

    shl-int/lit8 v14, v7, 0x6

    or-int/2addr v13, v14

    ushr-int/lit8 v14, v8, 0x2

    or-int/2addr v13, v14

    aput v13, p3, v12

    .line 91
    add-int/lit8 v1, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v1    # "blocksOffset":I
    aget-byte v13, p1, p2

    and-int/lit16 v9, v13, 0xff

    .line 92
    .local v9, "byte7":I
    add-int/lit8 p2, v1, 0x1

    .end local v1    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v13, p1, v1

    and-int/lit16 v10, v13, 0xff

    .line 93
    .local v10, "byte8":I
    add-int/lit8 v12, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v12    # "valuesOffset":I
    and-int/lit8 v13, v8, 0x3

    shl-int/lit8 v13, v13, 0x10

    shl-int/lit8 v14, v9, 0x8

    or-int/2addr v13, v14

    or-int/2addr v13, v10

    aput v13, p3, p4

    .line 80
    add-int/lit8 v11, v11, 0x1

    move/from16 v1, p2

    .end local p2    # "blocksOffset":I
    .restart local v1    # "blocksOffset":I
    goto :goto_0
.end method

.method public decode([BI[JII)V
    .locals 28
    .param p1, "blocks"    # [B
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [J
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 146
    const/4 v3, 0x0

    .local v3, "i":I
    move/from16 v22, p4

    .end local p4    # "valuesOffset":I
    .local v22, "valuesOffset":I
    move/from16 v2, p2

    .end local p2    # "blocksOffset":I
    .local v2, "blocksOffset":I
    :goto_0
    move/from16 v0, p5

    if-lt v3, v0, :cond_0

    .line 161
    return-void

    .line 147
    :cond_0
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v23, p1, v2

    move/from16 v0, v23

    and-int/lit16 v0, v0, 0xff

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-long v4, v0

    .line 148
    .local v4, "byte0":J
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v23, p1, p2

    move/from16 v0, v23

    and-int/lit16 v0, v0, 0xff

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-long v6, v0

    .line 149
    .local v6, "byte1":J
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v23, p1, v2

    move/from16 v0, v23

    and-int/lit16 v0, v0, 0xff

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-long v8, v0

    .line 150
    .local v8, "byte2":J
    add-int/lit8 p4, v22, 0x1

    .end local v22    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v23, 0xa

    shl-long v24, v4, v23

    const/16 v23, 0x2

    shl-long v26, v6, v23

    or-long v24, v24, v26

    const/16 v23, 0x6

    ushr-long v26, v8, v23

    or-long v24, v24, v26

    aput-wide v24, p3, v22

    .line 151
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v23, p1, p2

    move/from16 v0, v23

    and-int/lit16 v0, v0, 0xff

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-long v10, v0

    .line 152
    .local v10, "byte3":J
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v23, p1, v2

    move/from16 v0, v23

    and-int/lit16 v0, v0, 0xff

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-long v12, v0

    .line 153
    .local v12, "byte4":J
    add-int/lit8 v22, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v22    # "valuesOffset":I
    const-wide/16 v24, 0x3f

    and-long v24, v24, v8

    const/16 v23, 0xc

    shl-long v24, v24, v23

    const/16 v23, 0x4

    shl-long v26, v10, v23

    or-long v24, v24, v26

    const/16 v23, 0x4

    ushr-long v26, v12, v23

    or-long v24, v24, v26

    aput-wide v24, p3, p4

    .line 154
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v23, p1, p2

    move/from16 v0, v23

    and-int/lit16 v0, v0, 0xff

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-long v14, v0

    .line 155
    .local v14, "byte5":J
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v23, p1, v2

    move/from16 v0, v23

    and-int/lit16 v0, v0, 0xff

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-long v0, v0

    move-wide/from16 v16, v0

    .line 156
    .local v16, "byte6":J
    add-int/lit8 p4, v22, 0x1

    .end local v22    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v24, 0xf

    and-long v24, v24, v12

    const/16 v23, 0xe

    shl-long v24, v24, v23

    const/16 v23, 0x6

    shl-long v26, v14, v23

    or-long v24, v24, v26

    const/16 v23, 0x2

    ushr-long v26, v16, v23

    or-long v24, v24, v26

    aput-wide v24, p3, v22

    .line 157
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v23, p1, p2

    move/from16 v0, v23

    and-int/lit16 v0, v0, 0xff

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-long v0, v0

    move-wide/from16 v18, v0

    .line 158
    .local v18, "byte7":J
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v23, p1, v2

    move/from16 v0, v23

    and-int/lit16 v0, v0, 0xff

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-long v0, v0

    move-wide/from16 v20, v0

    .line 159
    .local v20, "byte8":J
    add-int/lit8 v22, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v22    # "valuesOffset":I
    const-wide/16 v24, 0x3

    and-long v24, v24, v16

    const/16 v23, 0x10

    shl-long v24, v24, v23

    const/16 v23, 0x8

    shl-long v26, v18, v23

    or-long v24, v24, v26

    or-long v24, v24, v20

    aput-wide v24, p3, p4

    .line 146
    add-int/lit8 v3, v3, 0x1

    move/from16 v2, p2

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    goto/16 :goto_0
.end method

.method public decode([JI[III)V
    .locals 28
    .param p1, "blocks"    # [J
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [I
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 33
    const/16 v21, 0x0

    .local v21, "i":I
    move/from16 v22, p4

    .end local p4    # "valuesOffset":I
    .local v22, "valuesOffset":I
    move/from16 v20, p2

    .end local p2    # "blocksOffset":I
    .local v20, "blocksOffset":I
    :goto_0
    move/from16 v0, v21

    move/from16 v1, p5

    if-lt v0, v1, :cond_0

    .line 76
    return-void

    .line 34
    :cond_0
    add-int/lit8 p2, v20, 0x1

    .end local v20    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v2, p1, v20

    .line 35
    .local v2, "block0":J
    add-int/lit8 p4, v22, 0x1

    .end local v22    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v23, 0x2e

    ushr-long v24, v2, v23

    move-wide/from16 v0, v24

    long-to-int v0, v0

    move/from16 v23, v0

    aput v23, p3, v22

    .line 36
    add-int/lit8 v22, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v22    # "valuesOffset":I
    const/16 v23, 0x1c

    ushr-long v24, v2, v23

    const-wide/32 v26, 0x3ffff

    and-long v24, v24, v26

    move-wide/from16 v0, v24

    long-to-int v0, v0

    move/from16 v23, v0

    aput v23, p3, p4

    .line 37
    add-int/lit8 p4, v22, 0x1

    .end local v22    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v23, 0xa

    ushr-long v24, v2, v23

    const-wide/32 v26, 0x3ffff

    and-long v24, v24, v26

    move-wide/from16 v0, v24

    long-to-int v0, v0

    move/from16 v23, v0

    aput v23, p3, v22

    .line 38
    add-int/lit8 v20, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v20    # "blocksOffset":I
    aget-wide v4, p1, p2

    .line 39
    .local v4, "block1":J
    add-int/lit8 v22, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v22    # "valuesOffset":I
    const-wide/16 v24, 0x3ff

    and-long v24, v24, v2

    const/16 v23, 0x8

    shl-long v24, v24, v23

    const/16 v23, 0x38

    ushr-long v26, v4, v23

    or-long v24, v24, v26

    move-wide/from16 v0, v24

    long-to-int v0, v0

    move/from16 v23, v0

    aput v23, p3, p4

    .line 40
    add-int/lit8 p4, v22, 0x1

    .end local v22    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v23, 0x26

    ushr-long v24, v4, v23

    const-wide/32 v26, 0x3ffff

    and-long v24, v24, v26

    move-wide/from16 v0, v24

    long-to-int v0, v0

    move/from16 v23, v0

    aput v23, p3, v22

    .line 41
    add-int/lit8 v22, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v22    # "valuesOffset":I
    const/16 v23, 0x14

    ushr-long v24, v4, v23

    const-wide/32 v26, 0x3ffff

    and-long v24, v24, v26

    move-wide/from16 v0, v24

    long-to-int v0, v0

    move/from16 v23, v0

    aput v23, p3, p4

    .line 42
    add-int/lit8 p4, v22, 0x1

    .end local v22    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v23, 0x2

    ushr-long v24, v4, v23

    const-wide/32 v26, 0x3ffff

    and-long v24, v24, v26

    move-wide/from16 v0, v24

    long-to-int v0, v0

    move/from16 v23, v0

    aput v23, p3, v22

    .line 43
    add-int/lit8 p2, v20, 0x1

    .end local v20    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v6, p1, v20

    .line 44
    .local v6, "block2":J
    add-int/lit8 v22, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v22    # "valuesOffset":I
    const-wide/16 v24, 0x3

    and-long v24, v24, v4

    const/16 v23, 0x10

    shl-long v24, v24, v23

    const/16 v23, 0x30

    ushr-long v26, v6, v23

    or-long v24, v24, v26

    move-wide/from16 v0, v24

    long-to-int v0, v0

    move/from16 v23, v0

    aput v23, p3, p4

    .line 45
    add-int/lit8 p4, v22, 0x1

    .end local v22    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v23, 0x1e

    ushr-long v24, v6, v23

    const-wide/32 v26, 0x3ffff

    and-long v24, v24, v26

    move-wide/from16 v0, v24

    long-to-int v0, v0

    move/from16 v23, v0

    aput v23, p3, v22

    .line 46
    add-int/lit8 v22, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v22    # "valuesOffset":I
    const/16 v23, 0xc

    ushr-long v24, v6, v23

    const-wide/32 v26, 0x3ffff

    and-long v24, v24, v26

    move-wide/from16 v0, v24

    long-to-int v0, v0

    move/from16 v23, v0

    aput v23, p3, p4

    .line 47
    add-int/lit8 v20, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v20    # "blocksOffset":I
    aget-wide v8, p1, p2

    .line 48
    .local v8, "block3":J
    add-int/lit8 p4, v22, 0x1

    .end local v22    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v24, 0xfff

    and-long v24, v24, v6

    const/16 v23, 0x6

    shl-long v24, v24, v23

    const/16 v23, 0x3a

    ushr-long v26, v8, v23

    or-long v24, v24, v26

    move-wide/from16 v0, v24

    long-to-int v0, v0

    move/from16 v23, v0

    aput v23, p3, v22

    .line 49
    add-int/lit8 v22, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v22    # "valuesOffset":I
    const/16 v23, 0x28

    ushr-long v24, v8, v23

    const-wide/32 v26, 0x3ffff

    and-long v24, v24, v26

    move-wide/from16 v0, v24

    long-to-int v0, v0

    move/from16 v23, v0

    aput v23, p3, p4

    .line 50
    add-int/lit8 p4, v22, 0x1

    .end local v22    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v23, 0x16

    ushr-long v24, v8, v23

    const-wide/32 v26, 0x3ffff

    and-long v24, v24, v26

    move-wide/from16 v0, v24

    long-to-int v0, v0

    move/from16 v23, v0

    aput v23, p3, v22

    .line 51
    add-int/lit8 v22, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v22    # "valuesOffset":I
    const/16 v23, 0x4

    ushr-long v24, v8, v23

    const-wide/32 v26, 0x3ffff

    and-long v24, v24, v26

    move-wide/from16 v0, v24

    long-to-int v0, v0

    move/from16 v23, v0

    aput v23, p3, p4

    .line 52
    add-int/lit8 p2, v20, 0x1

    .end local v20    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v10, p1, v20

    .line 53
    .local v10, "block4":J
    add-int/lit8 p4, v22, 0x1

    .end local v22    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v24, 0xf

    and-long v24, v24, v8

    const/16 v23, 0xe

    shl-long v24, v24, v23

    const/16 v23, 0x32

    ushr-long v26, v10, v23

    or-long v24, v24, v26

    move-wide/from16 v0, v24

    long-to-int v0, v0

    move/from16 v23, v0

    aput v23, p3, v22

    .line 54
    add-int/lit8 v22, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v22    # "valuesOffset":I
    const/16 v23, 0x20

    ushr-long v24, v10, v23

    const-wide/32 v26, 0x3ffff

    and-long v24, v24, v26

    move-wide/from16 v0, v24

    long-to-int v0, v0

    move/from16 v23, v0

    aput v23, p3, p4

    .line 55
    add-int/lit8 p4, v22, 0x1

    .end local v22    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v23, 0xe

    ushr-long v24, v10, v23

    const-wide/32 v26, 0x3ffff

    and-long v24, v24, v26

    move-wide/from16 v0, v24

    long-to-int v0, v0

    move/from16 v23, v0

    aput v23, p3, v22

    .line 56
    add-int/lit8 v20, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v20    # "blocksOffset":I
    aget-wide v12, p1, p2

    .line 57
    .local v12, "block5":J
    add-int/lit8 v22, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v22    # "valuesOffset":I
    const-wide/16 v24, 0x3fff

    and-long v24, v24, v10

    const/16 v23, 0x4

    shl-long v24, v24, v23

    const/16 v23, 0x3c

    ushr-long v26, v12, v23

    or-long v24, v24, v26

    move-wide/from16 v0, v24

    long-to-int v0, v0

    move/from16 v23, v0

    aput v23, p3, p4

    .line 58
    add-int/lit8 p4, v22, 0x1

    .end local v22    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v23, 0x2a

    ushr-long v24, v12, v23

    const-wide/32 v26, 0x3ffff

    and-long v24, v24, v26

    move-wide/from16 v0, v24

    long-to-int v0, v0

    move/from16 v23, v0

    aput v23, p3, v22

    .line 59
    add-int/lit8 v22, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v22    # "valuesOffset":I
    const/16 v23, 0x18

    ushr-long v24, v12, v23

    const-wide/32 v26, 0x3ffff

    and-long v24, v24, v26

    move-wide/from16 v0, v24

    long-to-int v0, v0

    move/from16 v23, v0

    aput v23, p3, p4

    .line 60
    add-int/lit8 p4, v22, 0x1

    .end local v22    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v23, 0x6

    ushr-long v24, v12, v23

    const-wide/32 v26, 0x3ffff

    and-long v24, v24, v26

    move-wide/from16 v0, v24

    long-to-int v0, v0

    move/from16 v23, v0

    aput v23, p3, v22

    .line 61
    add-int/lit8 p2, v20, 0x1

    .end local v20    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v14, p1, v20

    .line 62
    .local v14, "block6":J
    add-int/lit8 v22, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v22    # "valuesOffset":I
    const-wide/16 v24, 0x3f

    and-long v24, v24, v12

    const/16 v23, 0xc

    shl-long v24, v24, v23

    const/16 v23, 0x34

    ushr-long v26, v14, v23

    or-long v24, v24, v26

    move-wide/from16 v0, v24

    long-to-int v0, v0

    move/from16 v23, v0

    aput v23, p3, p4

    .line 63
    add-int/lit8 p4, v22, 0x1

    .end local v22    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v23, 0x22

    ushr-long v24, v14, v23

    const-wide/32 v26, 0x3ffff

    and-long v24, v24, v26

    move-wide/from16 v0, v24

    long-to-int v0, v0

    move/from16 v23, v0

    aput v23, p3, v22

    .line 64
    add-int/lit8 v22, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v22    # "valuesOffset":I
    const/16 v23, 0x10

    ushr-long v24, v14, v23

    const-wide/32 v26, 0x3ffff

    and-long v24, v24, v26

    move-wide/from16 v0, v24

    long-to-int v0, v0

    move/from16 v23, v0

    aput v23, p3, p4

    .line 65
    add-int/lit8 v20, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v20    # "blocksOffset":I
    aget-wide v16, p1, p2

    .line 66
    .local v16, "block7":J
    add-int/lit8 p4, v22, 0x1

    .end local v22    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/32 v24, 0xffff

    and-long v24, v24, v14

    const/16 v23, 0x2

    shl-long v24, v24, v23

    const/16 v23, 0x3e

    ushr-long v26, v16, v23

    or-long v24, v24, v26

    move-wide/from16 v0, v24

    long-to-int v0, v0

    move/from16 v23, v0

    aput v23, p3, v22

    .line 67
    add-int/lit8 v22, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v22    # "valuesOffset":I
    const/16 v23, 0x2c

    ushr-long v24, v16, v23

    const-wide/32 v26, 0x3ffff

    and-long v24, v24, v26

    move-wide/from16 v0, v24

    long-to-int v0, v0

    move/from16 v23, v0

    aput v23, p3, p4

    .line 68
    add-int/lit8 p4, v22, 0x1

    .end local v22    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v23, 0x1a

    ushr-long v24, v16, v23

    const-wide/32 v26, 0x3ffff

    and-long v24, v24, v26

    move-wide/from16 v0, v24

    long-to-int v0, v0

    move/from16 v23, v0

    aput v23, p3, v22

    .line 69
    add-int/lit8 v22, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v22    # "valuesOffset":I
    const/16 v23, 0x8

    ushr-long v24, v16, v23

    const-wide/32 v26, 0x3ffff

    and-long v24, v24, v26

    move-wide/from16 v0, v24

    long-to-int v0, v0

    move/from16 v23, v0

    aput v23, p3, p4

    .line 70
    add-int/lit8 p2, v20, 0x1

    .end local v20    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v18, p1, v20

    .line 71
    .local v18, "block8":J
    add-int/lit8 p4, v22, 0x1

    .end local v22    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v24, 0xff

    and-long v24, v24, v16

    const/16 v23, 0xa

    shl-long v24, v24, v23

    const/16 v23, 0x36

    ushr-long v26, v18, v23

    or-long v24, v24, v26

    move-wide/from16 v0, v24

    long-to-int v0, v0

    move/from16 v23, v0

    aput v23, p3, v22

    .line 72
    add-int/lit8 v22, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v22    # "valuesOffset":I
    const/16 v23, 0x24

    ushr-long v24, v18, v23

    const-wide/32 v26, 0x3ffff

    and-long v24, v24, v26

    move-wide/from16 v0, v24

    long-to-int v0, v0

    move/from16 v23, v0

    aput v23, p3, p4

    .line 73
    add-int/lit8 p4, v22, 0x1

    .end local v22    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v23, 0x12

    ushr-long v24, v18, v23

    const-wide/32 v26, 0x3ffff

    and-long v24, v24, v26

    move-wide/from16 v0, v24

    long-to-int v0, v0

    move/from16 v23, v0

    aput v23, p3, v22

    .line 74
    add-int/lit8 v22, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v22    # "valuesOffset":I
    const-wide/32 v24, 0x3ffff

    and-long v24, v24, v18

    move-wide/from16 v0, v24

    long-to-int v0, v0

    move/from16 v23, v0

    aput v23, p3, p4

    .line 33
    add-int/lit8 v21, v21, 0x1

    move/from16 v20, p2

    .end local p2    # "blocksOffset":I
    .restart local v20    # "blocksOffset":I
    goto/16 :goto_0
.end method

.method public decode([JI[JII)V
    .locals 28
    .param p1, "blocks"    # [J
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [J
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 99
    const/16 v21, 0x0

    .local v21, "i":I
    move/from16 v22, p4

    .end local p4    # "valuesOffset":I
    .local v22, "valuesOffset":I
    move/from16 v20, p2

    .end local p2    # "blocksOffset":I
    .local v20, "blocksOffset":I
    :goto_0
    move/from16 v0, v21

    move/from16 v1, p5

    if-lt v0, v1, :cond_0

    .line 142
    return-void

    .line 100
    :cond_0
    add-int/lit8 p2, v20, 0x1

    .end local v20    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v2, p1, v20

    .line 101
    .local v2, "block0":J
    add-int/lit8 p4, v22, 0x1

    .end local v22    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v23, 0x2e

    ushr-long v24, v2, v23

    aput-wide v24, p3, v22

    .line 102
    add-int/lit8 v22, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v22    # "valuesOffset":I
    const/16 v23, 0x1c

    ushr-long v24, v2, v23

    const-wide/32 v26, 0x3ffff

    and-long v24, v24, v26

    aput-wide v24, p3, p4

    .line 103
    add-int/lit8 p4, v22, 0x1

    .end local v22    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v23, 0xa

    ushr-long v24, v2, v23

    const-wide/32 v26, 0x3ffff

    and-long v24, v24, v26

    aput-wide v24, p3, v22

    .line 104
    add-int/lit8 v20, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v20    # "blocksOffset":I
    aget-wide v4, p1, p2

    .line 105
    .local v4, "block1":J
    add-int/lit8 v22, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v22    # "valuesOffset":I
    const-wide/16 v24, 0x3ff

    and-long v24, v24, v2

    const/16 v23, 0x8

    shl-long v24, v24, v23

    const/16 v23, 0x38

    ushr-long v26, v4, v23

    or-long v24, v24, v26

    aput-wide v24, p3, p4

    .line 106
    add-int/lit8 p4, v22, 0x1

    .end local v22    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v23, 0x26

    ushr-long v24, v4, v23

    const-wide/32 v26, 0x3ffff

    and-long v24, v24, v26

    aput-wide v24, p3, v22

    .line 107
    add-int/lit8 v22, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v22    # "valuesOffset":I
    const/16 v23, 0x14

    ushr-long v24, v4, v23

    const-wide/32 v26, 0x3ffff

    and-long v24, v24, v26

    aput-wide v24, p3, p4

    .line 108
    add-int/lit8 p4, v22, 0x1

    .end local v22    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v23, 0x2

    ushr-long v24, v4, v23

    const-wide/32 v26, 0x3ffff

    and-long v24, v24, v26

    aput-wide v24, p3, v22

    .line 109
    add-int/lit8 p2, v20, 0x1

    .end local v20    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v6, p1, v20

    .line 110
    .local v6, "block2":J
    add-int/lit8 v22, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v22    # "valuesOffset":I
    const-wide/16 v24, 0x3

    and-long v24, v24, v4

    const/16 v23, 0x10

    shl-long v24, v24, v23

    const/16 v23, 0x30

    ushr-long v26, v6, v23

    or-long v24, v24, v26

    aput-wide v24, p3, p4

    .line 111
    add-int/lit8 p4, v22, 0x1

    .end local v22    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v23, 0x1e

    ushr-long v24, v6, v23

    const-wide/32 v26, 0x3ffff

    and-long v24, v24, v26

    aput-wide v24, p3, v22

    .line 112
    add-int/lit8 v22, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v22    # "valuesOffset":I
    const/16 v23, 0xc

    ushr-long v24, v6, v23

    const-wide/32 v26, 0x3ffff

    and-long v24, v24, v26

    aput-wide v24, p3, p4

    .line 113
    add-int/lit8 v20, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v20    # "blocksOffset":I
    aget-wide v8, p1, p2

    .line 114
    .local v8, "block3":J
    add-int/lit8 p4, v22, 0x1

    .end local v22    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v24, 0xfff

    and-long v24, v24, v6

    const/16 v23, 0x6

    shl-long v24, v24, v23

    const/16 v23, 0x3a

    ushr-long v26, v8, v23

    or-long v24, v24, v26

    aput-wide v24, p3, v22

    .line 115
    add-int/lit8 v22, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v22    # "valuesOffset":I
    const/16 v23, 0x28

    ushr-long v24, v8, v23

    const-wide/32 v26, 0x3ffff

    and-long v24, v24, v26

    aput-wide v24, p3, p4

    .line 116
    add-int/lit8 p4, v22, 0x1

    .end local v22    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v23, 0x16

    ushr-long v24, v8, v23

    const-wide/32 v26, 0x3ffff

    and-long v24, v24, v26

    aput-wide v24, p3, v22

    .line 117
    add-int/lit8 v22, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v22    # "valuesOffset":I
    const/16 v23, 0x4

    ushr-long v24, v8, v23

    const-wide/32 v26, 0x3ffff

    and-long v24, v24, v26

    aput-wide v24, p3, p4

    .line 118
    add-int/lit8 p2, v20, 0x1

    .end local v20    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v10, p1, v20

    .line 119
    .local v10, "block4":J
    add-int/lit8 p4, v22, 0x1

    .end local v22    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v24, 0xf

    and-long v24, v24, v8

    const/16 v23, 0xe

    shl-long v24, v24, v23

    const/16 v23, 0x32

    ushr-long v26, v10, v23

    or-long v24, v24, v26

    aput-wide v24, p3, v22

    .line 120
    add-int/lit8 v22, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v22    # "valuesOffset":I
    const/16 v23, 0x20

    ushr-long v24, v10, v23

    const-wide/32 v26, 0x3ffff

    and-long v24, v24, v26

    aput-wide v24, p3, p4

    .line 121
    add-int/lit8 p4, v22, 0x1

    .end local v22    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v23, 0xe

    ushr-long v24, v10, v23

    const-wide/32 v26, 0x3ffff

    and-long v24, v24, v26

    aput-wide v24, p3, v22

    .line 122
    add-int/lit8 v20, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v20    # "blocksOffset":I
    aget-wide v12, p1, p2

    .line 123
    .local v12, "block5":J
    add-int/lit8 v22, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v22    # "valuesOffset":I
    const-wide/16 v24, 0x3fff

    and-long v24, v24, v10

    const/16 v23, 0x4

    shl-long v24, v24, v23

    const/16 v23, 0x3c

    ushr-long v26, v12, v23

    or-long v24, v24, v26

    aput-wide v24, p3, p4

    .line 124
    add-int/lit8 p4, v22, 0x1

    .end local v22    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v23, 0x2a

    ushr-long v24, v12, v23

    const-wide/32 v26, 0x3ffff

    and-long v24, v24, v26

    aput-wide v24, p3, v22

    .line 125
    add-int/lit8 v22, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v22    # "valuesOffset":I
    const/16 v23, 0x18

    ushr-long v24, v12, v23

    const-wide/32 v26, 0x3ffff

    and-long v24, v24, v26

    aput-wide v24, p3, p4

    .line 126
    add-int/lit8 p4, v22, 0x1

    .end local v22    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v23, 0x6

    ushr-long v24, v12, v23

    const-wide/32 v26, 0x3ffff

    and-long v24, v24, v26

    aput-wide v24, p3, v22

    .line 127
    add-int/lit8 p2, v20, 0x1

    .end local v20    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v14, p1, v20

    .line 128
    .local v14, "block6":J
    add-int/lit8 v22, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v22    # "valuesOffset":I
    const-wide/16 v24, 0x3f

    and-long v24, v24, v12

    const/16 v23, 0xc

    shl-long v24, v24, v23

    const/16 v23, 0x34

    ushr-long v26, v14, v23

    or-long v24, v24, v26

    aput-wide v24, p3, p4

    .line 129
    add-int/lit8 p4, v22, 0x1

    .end local v22    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v23, 0x22

    ushr-long v24, v14, v23

    const-wide/32 v26, 0x3ffff

    and-long v24, v24, v26

    aput-wide v24, p3, v22

    .line 130
    add-int/lit8 v22, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v22    # "valuesOffset":I
    const/16 v23, 0x10

    ushr-long v24, v14, v23

    const-wide/32 v26, 0x3ffff

    and-long v24, v24, v26

    aput-wide v24, p3, p4

    .line 131
    add-int/lit8 v20, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v20    # "blocksOffset":I
    aget-wide v16, p1, p2

    .line 132
    .local v16, "block7":J
    add-int/lit8 p4, v22, 0x1

    .end local v22    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/32 v24, 0xffff

    and-long v24, v24, v14

    const/16 v23, 0x2

    shl-long v24, v24, v23

    const/16 v23, 0x3e

    ushr-long v26, v16, v23

    or-long v24, v24, v26

    aput-wide v24, p3, v22

    .line 133
    add-int/lit8 v22, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v22    # "valuesOffset":I
    const/16 v23, 0x2c

    ushr-long v24, v16, v23

    const-wide/32 v26, 0x3ffff

    and-long v24, v24, v26

    aput-wide v24, p3, p4

    .line 134
    add-int/lit8 p4, v22, 0x1

    .end local v22    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v23, 0x1a

    ushr-long v24, v16, v23

    const-wide/32 v26, 0x3ffff

    and-long v24, v24, v26

    aput-wide v24, p3, v22

    .line 135
    add-int/lit8 v22, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v22    # "valuesOffset":I
    const/16 v23, 0x8

    ushr-long v24, v16, v23

    const-wide/32 v26, 0x3ffff

    and-long v24, v24, v26

    aput-wide v24, p3, p4

    .line 136
    add-int/lit8 p2, v20, 0x1

    .end local v20    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v18, p1, v20

    .line 137
    .local v18, "block8":J
    add-int/lit8 p4, v22, 0x1

    .end local v22    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v24, 0xff

    and-long v24, v24, v16

    const/16 v23, 0xa

    shl-long v24, v24, v23

    const/16 v23, 0x36

    ushr-long v26, v18, v23

    or-long v24, v24, v26

    aput-wide v24, p3, v22

    .line 138
    add-int/lit8 v22, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v22    # "valuesOffset":I
    const/16 v23, 0x24

    ushr-long v24, v18, v23

    const-wide/32 v26, 0x3ffff

    and-long v24, v24, v26

    aput-wide v24, p3, p4

    .line 139
    add-int/lit8 p4, v22, 0x1

    .end local v22    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v23, 0x12

    ushr-long v24, v18, v23

    const-wide/32 v26, 0x3ffff

    and-long v24, v24, v26

    aput-wide v24, p3, v22

    .line 140
    add-int/lit8 v22, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v22    # "valuesOffset":I
    const-wide/32 v24, 0x3ffff

    and-long v24, v24, v18

    aput-wide v24, p3, p4

    .line 99
    add-int/lit8 v21, v21, 0x1

    move/from16 v20, p2

    .end local p2    # "blocksOffset":I
    .restart local v20    # "blocksOffset":I
    goto/16 :goto_0
.end method
