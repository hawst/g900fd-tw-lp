.class Lorg/apache/lucene/util/fst/BytesStore$1;
.super Lorg/apache/lucene/util/fst/FST$BytesReader;
.source "BytesStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/util/fst/BytesStore;->getForwardReader()Lorg/apache/lucene/util/fst/FST$BytesReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/util/fst/FST$BytesReader;"
    }
.end annotation


# instance fields
.field private current:[B

.field private nextBuffer:I

.field private nextRead:I

.field final synthetic this$0:Lorg/apache/lucene/util/fst/BytesStore;


# direct methods
.method constructor <init>(Lorg/apache/lucene/util/fst/BytesStore;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/util/fst/BytesStore$1;->this$0:Lorg/apache/lucene/util/fst/BytesStore;

    .line 350
    invoke-direct {p0}, Lorg/apache/lucene/util/fst/FST$BytesReader;-><init>()V

    .line 353
    # getter for: Lorg/apache/lucene/util/fst/BytesStore;->blockSize:I
    invoke-static {p1}, Lorg/apache/lucene/util/fst/BytesStore;->access$0(Lorg/apache/lucene/util/fst/BytesStore;)I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/util/fst/BytesStore$1;->nextRead:I

    return-void
.end method


# virtual methods
.method public getPosition()J
    .locals 4

    .prologue
    .line 391
    iget v0, p0, Lorg/apache/lucene/util/fst/BytesStore$1;->nextBuffer:I

    int-to-long v0, v0

    const-wide/16 v2, 0x1

    sub-long/2addr v0, v2

    iget-object v2, p0, Lorg/apache/lucene/util/fst/BytesStore$1;->this$0:Lorg/apache/lucene/util/fst/BytesStore;

    # getter for: Lorg/apache/lucene/util/fst/BytesStore;->blockSize:I
    invoke-static {v2}, Lorg/apache/lucene/util/fst/BytesStore;->access$0(Lorg/apache/lucene/util/fst/BytesStore;)I

    move-result v2

    int-to-long v2, v2

    mul-long/2addr v0, v2

    iget v2, p0, Lorg/apache/lucene/util/fst/BytesStore$1;->nextRead:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public readByte()B
    .locals 3

    .prologue
    .line 357
    iget v0, p0, Lorg/apache/lucene/util/fst/BytesStore$1;->nextRead:I

    iget-object v1, p0, Lorg/apache/lucene/util/fst/BytesStore$1;->this$0:Lorg/apache/lucene/util/fst/BytesStore;

    # getter for: Lorg/apache/lucene/util/fst/BytesStore;->blockSize:I
    invoke-static {v1}, Lorg/apache/lucene/util/fst/BytesStore;->access$0(Lorg/apache/lucene/util/fst/BytesStore;)I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 358
    iget-object v0, p0, Lorg/apache/lucene/util/fst/BytesStore$1;->this$0:Lorg/apache/lucene/util/fst/BytesStore;

    # getter for: Lorg/apache/lucene/util/fst/BytesStore;->blocks:Ljava/util/List;
    invoke-static {v0}, Lorg/apache/lucene/util/fst/BytesStore;->access$1(Lorg/apache/lucene/util/fst/BytesStore;)Ljava/util/List;

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/util/fst/BytesStore$1;->nextBuffer:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/util/fst/BytesStore$1;->nextBuffer:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iput-object v0, p0, Lorg/apache/lucene/util/fst/BytesStore$1;->current:[B

    .line 359
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/util/fst/BytesStore$1;->nextRead:I

    .line 361
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/util/fst/BytesStore$1;->current:[B

    iget v1, p0, Lorg/apache/lucene/util/fst/BytesStore$1;->nextRead:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/util/fst/BytesStore$1;->nextRead:I

    aget-byte v0, v0, v1

    return v0
.end method

.method public readBytes([BII)V
    .locals 4
    .param p1, "b"    # [B
    .param p2, "offset"    # I
    .param p3, "len"    # I

    .prologue
    .line 371
    :goto_0
    if-gtz p3, :cond_0

    .line 387
    :goto_1
    return-void

    .line 372
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/util/fst/BytesStore$1;->this$0:Lorg/apache/lucene/util/fst/BytesStore;

    # getter for: Lorg/apache/lucene/util/fst/BytesStore;->blockSize:I
    invoke-static {v1}, Lorg/apache/lucene/util/fst/BytesStore;->access$0(Lorg/apache/lucene/util/fst/BytesStore;)I

    move-result v1

    iget v2, p0, Lorg/apache/lucene/util/fst/BytesStore$1;->nextRead:I

    sub-int v0, v1, v2

    .line 373
    .local v0, "chunkLeft":I
    if-gt p3, v0, :cond_1

    .line 374
    iget-object v1, p0, Lorg/apache/lucene/util/fst/BytesStore$1;->current:[B

    iget v2, p0, Lorg/apache/lucene/util/fst/BytesStore$1;->nextRead:I

    invoke-static {v1, v2, p1, p2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 375
    iget v1, p0, Lorg/apache/lucene/util/fst/BytesStore$1;->nextRead:I

    add-int/2addr v1, p3

    iput v1, p0, Lorg/apache/lucene/util/fst/BytesStore$1;->nextRead:I

    goto :goto_1

    .line 378
    :cond_1
    if-lez v0, :cond_2

    .line 379
    iget-object v1, p0, Lorg/apache/lucene/util/fst/BytesStore$1;->current:[B

    iget v2, p0, Lorg/apache/lucene/util/fst/BytesStore$1;->nextRead:I

    invoke-static {v1, v2, p1, p2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 380
    add-int/2addr p2, v0

    .line 381
    sub-int/2addr p3, v0

    .line 383
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/util/fst/BytesStore$1;->this$0:Lorg/apache/lucene/util/fst/BytesStore;

    # getter for: Lorg/apache/lucene/util/fst/BytesStore;->blocks:Ljava/util/List;
    invoke-static {v1}, Lorg/apache/lucene/util/fst/BytesStore;->access$1(Lorg/apache/lucene/util/fst/BytesStore;)Ljava/util/List;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/util/fst/BytesStore$1;->nextBuffer:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lorg/apache/lucene/util/fst/BytesStore$1;->nextBuffer:I

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    iput-object v1, p0, Lorg/apache/lucene/util/fst/BytesStore$1;->current:[B

    .line 384
    const/4 v1, 0x0

    iput v1, p0, Lorg/apache/lucene/util/fst/BytesStore$1;->nextRead:I

    goto :goto_0
.end method

.method public reversed()Z
    .locals 1

    .prologue
    .line 405
    const/4 v0, 0x0

    return v0
.end method

.method public setPosition(J)V
    .locals 5
    .param p1, "pos"    # J

    .prologue
    .line 396
    iget-object v1, p0, Lorg/apache/lucene/util/fst/BytesStore$1;->this$0:Lorg/apache/lucene/util/fst/BytesStore;

    # getter for: Lorg/apache/lucene/util/fst/BytesStore;->blockBits:I
    invoke-static {v1}, Lorg/apache/lucene/util/fst/BytesStore;->access$2(Lorg/apache/lucene/util/fst/BytesStore;)I

    move-result v1

    shr-long v2, p1, v1

    long-to-int v0, v2

    .line 397
    .local v0, "bufferIndex":I
    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lorg/apache/lucene/util/fst/BytesStore$1;->nextBuffer:I

    .line 398
    iget-object v1, p0, Lorg/apache/lucene/util/fst/BytesStore$1;->this$0:Lorg/apache/lucene/util/fst/BytesStore;

    # getter for: Lorg/apache/lucene/util/fst/BytesStore;->blocks:Ljava/util/List;
    invoke-static {v1}, Lorg/apache/lucene/util/fst/BytesStore;->access$1(Lorg/apache/lucene/util/fst/BytesStore;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    iput-object v1, p0, Lorg/apache/lucene/util/fst/BytesStore$1;->current:[B

    .line 399
    iget-object v1, p0, Lorg/apache/lucene/util/fst/BytesStore$1;->this$0:Lorg/apache/lucene/util/fst/BytesStore;

    # getter for: Lorg/apache/lucene/util/fst/BytesStore;->blockMask:I
    invoke-static {v1}, Lorg/apache/lucene/util/fst/BytesStore;->access$3(Lorg/apache/lucene/util/fst/BytesStore;)I

    move-result v1

    int-to-long v2, v1

    and-long/2addr v2, p1

    long-to-int v1, v2

    iput v1, p0, Lorg/apache/lucene/util/fst/BytesStore$1;->nextRead:I

    .line 400
    sget-boolean v1, Lorg/apache/lucene/util/fst/BytesStore;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/util/fst/BytesStore$1;->getPosition()J

    move-result-wide v2

    cmp-long v1, v2, p1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 401
    :cond_0
    return-void
.end method

.method public skipBytes(I)V
    .locals 4
    .param p1, "count"    # I

    .prologue
    .line 366
    invoke-virtual {p0}, Lorg/apache/lucene/util/fst/BytesStore$1;->getPosition()J

    move-result-wide v0

    int-to-long v2, p1

    add-long/2addr v0, v2

    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/util/fst/BytesStore$1;->setPosition(J)V

    .line 367
    return-void
.end method
