.class public Lorg/apache/lucene/util/packed/GrowableWriter;
.super Ljava/lang/Object;
.source "GrowableWriter.java"

# interfaces
.implements Lorg/apache/lucene/util/packed/PackedInts$Mutable;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final acceptableOverheadRatio:F

.field private current:Lorg/apache/lucene/util/packed/PackedInts$Mutable;

.field private currentMaxValue:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lorg/apache/lucene/util/packed/GrowableWriter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/packed/GrowableWriter;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(IIF)V
    .locals 2
    .param p1, "startBitsPerValue"    # I
    .param p2, "valueCount"    # I
    .param p3, "acceptableOverheadRatio"    # F

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput p3, p0, Lorg/apache/lucene/util/packed/GrowableWriter;->acceptableOverheadRatio:F

    .line 39
    iget v0, p0, Lorg/apache/lucene/util/packed/GrowableWriter;->acceptableOverheadRatio:F

    invoke-static {p2, p1, v0}, Lorg/apache/lucene/util/packed/PackedInts;->getMutable(IIF)Lorg/apache/lucene/util/packed/PackedInts$Mutable;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/util/packed/GrowableWriter;->current:Lorg/apache/lucene/util/packed/PackedInts$Mutable;

    .line 40
    iget-object v0, p0, Lorg/apache/lucene/util/packed/GrowableWriter;->current:Lorg/apache/lucene/util/packed/PackedInts$Mutable;

    invoke-interface {v0}, Lorg/apache/lucene/util/packed/PackedInts$Mutable;->getBitsPerValue()I

    move-result v0

    invoke-static {v0}, Lorg/apache/lucene/util/packed/PackedInts;->maxValue(I)J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/lucene/util/packed/GrowableWriter;->currentMaxValue:J

    .line 41
    return-void
.end method

.method private ensureCapacity(J)V
    .locals 11
    .param p1, "value"    # J

    .prologue
    const/4 v1, 0x0

    .line 73
    sget-boolean v0, Lorg/apache/lucene/util/packed/GrowableWriter;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    const-wide/16 v8, 0x0

    cmp-long v0, p1, v8

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 74
    :cond_0
    iget-wide v8, p0, Lorg/apache/lucene/util/packed/GrowableWriter;->currentMaxValue:J

    cmp-long v0, p1, v8

    if-gtz v0, :cond_1

    .line 83
    :goto_0
    return-void

    .line 77
    :cond_1
    invoke-static {p1, p2}, Lorg/apache/lucene/util/packed/PackedInts;->bitsRequired(J)I

    move-result v6

    .line 78
    .local v6, "bitsRequired":I
    invoke-virtual {p0}, Lorg/apache/lucene/util/packed/GrowableWriter;->size()I

    move-result v4

    .line 79
    .local v4, "valueCount":I
    iget v0, p0, Lorg/apache/lucene/util/packed/GrowableWriter;->acceptableOverheadRatio:F

    invoke-static {v4, v6, v0}, Lorg/apache/lucene/util/packed/PackedInts;->getMutable(IIF)Lorg/apache/lucene/util/packed/PackedInts$Mutable;

    move-result-object v2

    .line 80
    .local v2, "next":Lorg/apache/lucene/util/packed/PackedInts$Mutable;
    iget-object v0, p0, Lorg/apache/lucene/util/packed/GrowableWriter;->current:Lorg/apache/lucene/util/packed/PackedInts$Mutable;

    const/16 v5, 0x400

    move v3, v1

    invoke-static/range {v0 .. v5}, Lorg/apache/lucene/util/packed/PackedInts;->copy(Lorg/apache/lucene/util/packed/PackedInts$Reader;ILorg/apache/lucene/util/packed/PackedInts$Mutable;III)V

    .line 81
    iput-object v2, p0, Lorg/apache/lucene/util/packed/GrowableWriter;->current:Lorg/apache/lucene/util/packed/PackedInts$Mutable;

    .line 82
    iget-object v0, p0, Lorg/apache/lucene/util/packed/GrowableWriter;->current:Lorg/apache/lucene/util/packed/PackedInts$Mutable;

    invoke-interface {v0}, Lorg/apache/lucene/util/packed/PackedInts$Mutable;->getBitsPerValue()I

    move-result v0

    invoke-static {v0}, Lorg/apache/lucene/util/packed/PackedInts;->maxValue(I)J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/lucene/util/packed/GrowableWriter;->currentMaxValue:J

    goto :goto_0
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lorg/apache/lucene/util/packed/GrowableWriter;->current:Lorg/apache/lucene/util/packed/PackedInts$Mutable;

    invoke-interface {v0}, Lorg/apache/lucene/util/packed/PackedInts$Mutable;->clear()V

    .line 94
    return-void
.end method

.method public fill(IIJ)V
    .locals 1
    .param p1, "fromIndex"    # I
    .param p2, "toIndex"    # I
    .param p3, "val"    # J

    .prologue
    .line 120
    invoke-direct {p0, p3, p4}, Lorg/apache/lucene/util/packed/GrowableWriter;->ensureCapacity(J)V

    .line 121
    iget-object v0, p0, Lorg/apache/lucene/util/packed/GrowableWriter;->current:Lorg/apache/lucene/util/packed/PackedInts$Mutable;

    invoke-interface {v0, p1, p2, p3, p4}, Lorg/apache/lucene/util/packed/PackedInts$Mutable;->fill(IIJ)V

    .line 122
    return-void
.end method

.method public get(I[JII)I
    .locals 1
    .param p1, "index"    # I
    .param p2, "arr"    # [J
    .param p3, "off"    # I
    .param p4, "len"    # I

    .prologue
    .line 105
    iget-object v0, p0, Lorg/apache/lucene/util/packed/GrowableWriter;->current:Lorg/apache/lucene/util/packed/PackedInts$Mutable;

    invoke-interface {v0, p1, p2, p3, p4}, Lorg/apache/lucene/util/packed/PackedInts$Mutable;->get(I[JII)I

    move-result v0

    return v0
.end method

.method public get(I)J
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 45
    iget-object v0, p0, Lorg/apache/lucene/util/packed/GrowableWriter;->current:Lorg/apache/lucene/util/packed/PackedInts$Mutable;

    invoke-interface {v0, p1}, Lorg/apache/lucene/util/packed/PackedInts$Mutable;->get(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getArray()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lorg/apache/lucene/util/packed/GrowableWriter;->current:Lorg/apache/lucene/util/packed/PackedInts$Mutable;

    invoke-interface {v0}, Lorg/apache/lucene/util/packed/PackedInts$Mutable;->getArray()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getBitsPerValue()I
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lorg/apache/lucene/util/packed/GrowableWriter;->current:Lorg/apache/lucene/util/packed/PackedInts$Mutable;

    invoke-interface {v0}, Lorg/apache/lucene/util/packed/PackedInts$Mutable;->getBitsPerValue()I

    move-result v0

    return v0
.end method

.method public getMutable()Lorg/apache/lucene/util/packed/PackedInts$Mutable;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lorg/apache/lucene/util/packed/GrowableWriter;->current:Lorg/apache/lucene/util/packed/PackedInts$Mutable;

    return-object v0
.end method

.method public hasArray()Z
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lorg/apache/lucene/util/packed/GrowableWriter;->current:Lorg/apache/lucene/util/packed/PackedInts$Mutable;

    invoke-interface {v0}, Lorg/apache/lucene/util/packed/PackedInts$Mutable;->hasArray()Z

    move-result v0

    return v0
.end method

.method public ramBytesUsed()J
    .locals 2

    .prologue
    .line 126
    iget-object v0, p0, Lorg/apache/lucene/util/packed/GrowableWriter;->current:Lorg/apache/lucene/util/packed/PackedInts$Mutable;

    invoke-interface {v0}, Lorg/apache/lucene/util/packed/PackedInts$Mutable;->ramBytesUsed()J

    move-result-wide v0

    return-wide v0
.end method

.method public resize(I)Lorg/apache/lucene/util/packed/GrowableWriter;
    .locals 6
    .param p1, "newSize"    # I

    .prologue
    const/4 v1, 0x0

    .line 97
    new-instance v2, Lorg/apache/lucene/util/packed/GrowableWriter;

    invoke-virtual {p0}, Lorg/apache/lucene/util/packed/GrowableWriter;->getBitsPerValue()I

    move-result v0

    iget v3, p0, Lorg/apache/lucene/util/packed/GrowableWriter;->acceptableOverheadRatio:F

    invoke-direct {v2, v0, p1, v3}, Lorg/apache/lucene/util/packed/GrowableWriter;-><init>(IIF)V

    .line 98
    .local v2, "next":Lorg/apache/lucene/util/packed/GrowableWriter;
    invoke-virtual {p0}, Lorg/apache/lucene/util/packed/GrowableWriter;->size()I

    move-result v0

    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 99
    .local v4, "limit":I
    iget-object v0, p0, Lorg/apache/lucene/util/packed/GrowableWriter;->current:Lorg/apache/lucene/util/packed/PackedInts$Mutable;

    const/16 v5, 0x400

    move v3, v1

    invoke-static/range {v0 .. v5}, Lorg/apache/lucene/util/packed/PackedInts;->copy(Lorg/apache/lucene/util/packed/PackedInts$Reader;ILorg/apache/lucene/util/packed/PackedInts$Mutable;III)V

    .line 100
    return-object v2
.end method

.method public save(Lorg/apache/lucene/store/DataOutput;)V
    .locals 1
    .param p1, "out"    # Lorg/apache/lucene/store/DataOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 131
    iget-object v0, p0, Lorg/apache/lucene/util/packed/GrowableWriter;->current:Lorg/apache/lucene/util/packed/PackedInts$Mutable;

    invoke-interface {v0, p1}, Lorg/apache/lucene/util/packed/PackedInts$Mutable;->save(Lorg/apache/lucene/store/DataOutput;)V

    .line 132
    return-void
.end method

.method public set(I[JII)I
    .locals 6
    .param p1, "index"    # I
    .param p2, "arr"    # [J
    .param p3, "off"    # I
    .param p4, "len"    # I

    .prologue
    .line 110
    const-wide/16 v2, 0x0

    .line 111
    .local v2, "max":J
    move v1, p3

    .local v1, "i":I
    add-int v0, p3, p4

    .local v0, "end":I
    :goto_0
    if-lt v1, v0, :cond_0

    .line 114
    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/util/packed/GrowableWriter;->ensureCapacity(J)V

    .line 115
    iget-object v4, p0, Lorg/apache/lucene/util/packed/GrowableWriter;->current:Lorg/apache/lucene/util/packed/PackedInts$Mutable;

    invoke-interface {v4, p1, p2, p3, p4}, Lorg/apache/lucene/util/packed/PackedInts$Mutable;->set(I[JII)I

    move-result v4

    return v4

    .line 112
    :cond_0
    aget-wide v4, p2, v1

    or-long/2addr v2, v4

    .line 111
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public set(IJ)V
    .locals 2
    .param p1, "index"    # I
    .param p2, "value"    # J

    .prologue
    .line 87
    invoke-direct {p0, p2, p3}, Lorg/apache/lucene/util/packed/GrowableWriter;->ensureCapacity(J)V

    .line 88
    iget-object v0, p0, Lorg/apache/lucene/util/packed/GrowableWriter;->current:Lorg/apache/lucene/util/packed/PackedInts$Mutable;

    invoke-interface {v0, p1, p2, p3}, Lorg/apache/lucene/util/packed/PackedInts$Mutable;->set(IJ)V

    .line 89
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lorg/apache/lucene/util/packed/GrowableWriter;->current:Lorg/apache/lucene/util/packed/PackedInts$Mutable;

    invoke-interface {v0}, Lorg/apache/lucene/util/packed/PackedInts$Mutable;->size()I

    move-result v0

    return v0
.end method
