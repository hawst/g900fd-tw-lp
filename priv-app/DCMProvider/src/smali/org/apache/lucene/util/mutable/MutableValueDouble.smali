.class public Lorg/apache/lucene/util/mutable/MutableValueDouble;
.super Lorg/apache/lucene/util/mutable/MutableValue;
.source "MutableValueDouble.java"


# instance fields
.field public value:D


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lorg/apache/lucene/util/mutable/MutableValue;-><init>()V

    return-void
.end method


# virtual methods
.method public compareSameType(Ljava/lang/Object;)I
    .locals 6
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 54
    move-object v0, p1

    check-cast v0, Lorg/apache/lucene/util/mutable/MutableValueDouble;

    .line 55
    .local v0, "b":Lorg/apache/lucene/util/mutable/MutableValueDouble;
    iget-wide v2, p0, Lorg/apache/lucene/util/mutable/MutableValueDouble;->value:D

    iget-wide v4, v0, Lorg/apache/lucene/util/mutable/MutableValueDouble;->value:D

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Double;->compare(DD)I

    move-result v1

    .line 56
    .local v1, "c":I
    if-eqz v1, :cond_0

    .line 59
    .end local v1    # "c":I
    :goto_0
    return v1

    .line 57
    .restart local v1    # "c":I
    :cond_0
    iget-boolean v2, p0, Lorg/apache/lucene/util/mutable/MutableValueDouble;->exists:Z

    if-nez v2, :cond_1

    const/4 v1, -0x1

    goto :goto_0

    .line 58
    :cond_1
    iget-boolean v2, v0, Lorg/apache/lucene/util/mutable/MutableValueDouble;->exists:Z

    if-nez v2, :cond_2

    const/4 v1, 0x1

    goto :goto_0

    .line 59
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public copy(Lorg/apache/lucene/util/mutable/MutableValue;)V
    .locals 4
    .param p1, "source"    # Lorg/apache/lucene/util/mutable/MutableValue;

    .prologue
    .line 33
    move-object v0, p1

    check-cast v0, Lorg/apache/lucene/util/mutable/MutableValueDouble;

    .line 34
    .local v0, "s":Lorg/apache/lucene/util/mutable/MutableValueDouble;
    iget-wide v2, v0, Lorg/apache/lucene/util/mutable/MutableValueDouble;->value:D

    iput-wide v2, p0, Lorg/apache/lucene/util/mutable/MutableValueDouble;->value:D

    .line 35
    iget-boolean v1, v0, Lorg/apache/lucene/util/mutable/MutableValueDouble;->exists:Z

    iput-boolean v1, p0, Lorg/apache/lucene/util/mutable/MutableValueDouble;->exists:Z

    .line 36
    return-void
.end method

.method public duplicate()Lorg/apache/lucene/util/mutable/MutableValue;
    .locals 4

    .prologue
    .line 40
    new-instance v0, Lorg/apache/lucene/util/mutable/MutableValueDouble;

    invoke-direct {v0}, Lorg/apache/lucene/util/mutable/MutableValueDouble;-><init>()V

    .line 41
    .local v0, "v":Lorg/apache/lucene/util/mutable/MutableValueDouble;
    iget-wide v2, p0, Lorg/apache/lucene/util/mutable/MutableValueDouble;->value:D

    iput-wide v2, v0, Lorg/apache/lucene/util/mutable/MutableValueDouble;->value:D

    .line 42
    iget-boolean v1, p0, Lorg/apache/lucene/util/mutable/MutableValueDouble;->exists:Z

    iput-boolean v1, v0, Lorg/apache/lucene/util/mutable/MutableValueDouble;->exists:Z

    .line 43
    return-object v0
.end method

.method public equalsSameType(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 48
    move-object v0, p1

    check-cast v0, Lorg/apache/lucene/util/mutable/MutableValueDouble;

    .line 49
    .local v0, "b":Lorg/apache/lucene/util/mutable/MutableValueDouble;
    iget-wide v2, p0, Lorg/apache/lucene/util/mutable/MutableValueDouble;->value:D

    iget-wide v4, v0, Lorg/apache/lucene/util/mutable/MutableValueDouble;->value:D

    cmpl-double v1, v2, v4

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lorg/apache/lucene/util/mutable/MutableValueDouble;->exists:Z

    iget-boolean v2, v0, Lorg/apache/lucene/util/mutable/MutableValueDouble;->exists:Z

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    .line 64
    iget-wide v2, p0, Lorg/apache/lucene/util/mutable/MutableValueDouble;->value:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v0

    .line 65
    .local v0, "x":J
    long-to-int v2, v0

    const/16 v3, 0x20

    ushr-long v4, v0, v3

    long-to-int v3, v4

    add-int/2addr v2, v3

    return v2
.end method

.method public toObject()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 28
    iget-boolean v0, p0, Lorg/apache/lucene/util/mutable/MutableValueDouble;->exists:Z

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lorg/apache/lucene/util/mutable/MutableValueDouble;->value:D

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
