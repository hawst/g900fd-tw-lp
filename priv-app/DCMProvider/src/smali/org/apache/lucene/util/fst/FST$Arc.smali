.class public final Lorg/apache/lucene/util/fst/FST$Arc;
.super Ljava/lang/Object;
.source "FST.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/fst/FST;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Arc"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field arcIdx:I

.field bytesPerArc:I

.field flags:B

.field public label:I

.field nextArc:J

.field public nextFinalOutput:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field node:J

.field numArcs:I

.field public output:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field posArcsStart:J

.field public target:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 176
    .local p0, "this":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST<TT;>.Arc<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public copyFrom(Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;)",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 201
    .local p0, "this":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST<TT;>.Arc<TT;>;"
    .local p1, "other":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    iget-wide v0, p1, Lorg/apache/lucene/util/fst/FST$Arc;->node:J

    iput-wide v0, p0, Lorg/apache/lucene/util/fst/FST$Arc;->node:J

    .line 202
    iget v0, p1, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    iput v0, p0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    .line 203
    iget-wide v0, p1, Lorg/apache/lucene/util/fst/FST$Arc;->target:J

    iput-wide v0, p0, Lorg/apache/lucene/util/fst/FST$Arc;->target:J

    .line 204
    iget-byte v0, p1, Lorg/apache/lucene/util/fst/FST$Arc;->flags:B

    iput-byte v0, p0, Lorg/apache/lucene/util/fst/FST$Arc;->flags:B

    .line 205
    iget-object v0, p1, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    iput-object v0, p0, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    .line 206
    iget-object v0, p1, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    iput-object v0, p0, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    .line 207
    iget-wide v0, p1, Lorg/apache/lucene/util/fst/FST$Arc;->nextArc:J

    iput-wide v0, p0, Lorg/apache/lucene/util/fst/FST$Arc;->nextArc:J

    .line 208
    iget v0, p1, Lorg/apache/lucene/util/fst/FST$Arc;->bytesPerArc:I

    iput v0, p0, Lorg/apache/lucene/util/fst/FST$Arc;->bytesPerArc:I

    .line 209
    iget v0, p0, Lorg/apache/lucene/util/fst/FST$Arc;->bytesPerArc:I

    if-eqz v0, :cond_0

    .line 210
    iget-wide v0, p1, Lorg/apache/lucene/util/fst/FST$Arc;->posArcsStart:J

    iput-wide v0, p0, Lorg/apache/lucene/util/fst/FST$Arc;->posArcsStart:J

    .line 211
    iget v0, p1, Lorg/apache/lucene/util/fst/FST$Arc;->arcIdx:I

    iput v0, p0, Lorg/apache/lucene/util/fst/FST$Arc;->arcIdx:I

    .line 212
    iget v0, p1, Lorg/apache/lucene/util/fst/FST$Arc;->numArcs:I

    iput v0, p0, Lorg/apache/lucene/util/fst/FST$Arc;->numArcs:I

    .line 214
    :cond_0
    return-object p0
.end method

.method flag(I)Z
    .locals 1
    .param p1, "flag"    # I

    .prologue
    .line 218
    .local p0, "this":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST<TT;>.Arc<TT;>;"
    iget-byte v0, p0, Lorg/apache/lucene/util/fst/FST$Arc;->flags:B

    # invokes: Lorg/apache/lucene/util/fst/FST;->flag(II)Z
    invoke-static {v0, p1}, Lorg/apache/lucene/util/fst/FST;->access$0(II)Z

    move-result v0

    return v0
.end method

.method public isFinal()Z
    .locals 1

    .prologue
    .line 226
    .local p0, "this":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST<TT;>.Arc<TT;>;"
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lorg/apache/lucene/util/fst/FST$Arc;->flag(I)Z

    move-result v0

    return v0
.end method

.method public isLast()Z
    .locals 1

    .prologue
    .line 222
    .local p0, "this":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST<TT;>.Arc<TT;>;"
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lorg/apache/lucene/util/fst/FST$Arc;->flag(I)Z

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 231
    .local p0, "this":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST<TT;>.Arc<TT;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 232
    .local v0, "b":Ljava/lang/StringBuilder;
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "node="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lorg/apache/lucene/util/fst/FST$Arc;->node:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 233
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " target="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lorg/apache/lucene/util/fst/FST$Arc;->target:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 234
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " label="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 235
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lorg/apache/lucene/util/fst/FST$Arc;->flag(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 236
    const-string v1, " last"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 238
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lorg/apache/lucene/util/fst/FST$Arc;->flag(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 239
    const-string v1, " final"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 241
    :cond_1
    const/4 v1, 0x4

    invoke-virtual {p0, v1}, Lorg/apache/lucene/util/fst/FST$Arc;->flag(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 242
    const-string v1, " targetNext"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 244
    :cond_2
    const/16 v1, 0x10

    invoke-virtual {p0, v1}, Lorg/apache/lucene/util/fst/FST$Arc;->flag(I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 245
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " output="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 247
    :cond_3
    const/16 v1, 0x20

    invoke-virtual {p0, v1}, Lorg/apache/lucene/util/fst/FST$Arc;->flag(I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 248
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " nextFinalOutput="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 250
    :cond_4
    iget v1, p0, Lorg/apache/lucene/util/fst/FST$Arc;->bytesPerArc:I

    if-eqz v1, :cond_5

    .line 251
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " arcArray(idx="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/lucene/util/fst/FST$Arc;->arcIdx:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/util/fst/FST$Arc;->numArcs:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 253
    :cond_5
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
