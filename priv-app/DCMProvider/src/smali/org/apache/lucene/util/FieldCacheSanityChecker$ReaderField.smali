.class final Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;
.super Ljava/lang/Object;
.source "FieldCacheSanityChecker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/FieldCacheSanityChecker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ReaderField"
.end annotation


# instance fields
.field public final fieldName:Ljava/lang/String;

.field public final readerKey:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 0
    .param p1, "readerKey"    # Ljava/lang/Object;
    .param p2, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 310
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 311
    iput-object p1, p0, Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;->readerKey:Ljava/lang/Object;

    .line 312
    iput-object p2, p0, Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;->fieldName:Ljava/lang/String;

    .line 313
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "that"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 320
    instance-of v2, p1, Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;

    if-nez v2, :cond_1

    .line 323
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 322
    check-cast v0, Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;

    .line 323
    .local v0, "other":Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;
    iget-object v2, p0, Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;->readerKey:Ljava/lang/Object;

    iget-object v3, v0, Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;->readerKey:Ljava/lang/Object;

    if-ne v2, v3, :cond_0

    .line 324
    iget-object v2, p0, Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;->fieldName:Ljava/lang/String;

    iget-object v3, v0, Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;->fieldName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 323
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 316
    iget-object v0, p0, Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;->readerKey:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;->fieldName:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    mul-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 328
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;->readerKey:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "+"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;->fieldName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
