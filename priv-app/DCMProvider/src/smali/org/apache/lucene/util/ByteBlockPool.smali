.class public final Lorg/apache/lucene/util/ByteBlockPool;
.super Ljava/lang/Object;
.source "ByteBlockPool.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/ByteBlockPool$Allocator;,
        Lorg/apache/lucene/util/ByteBlockPool$DirectAllocator;,
        Lorg/apache/lucene/util/ByteBlockPool$DirectTrackingAllocator;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final BYTE_BLOCK_MASK:I = 0x7fff

.field public static final BYTE_BLOCK_SHIFT:I = 0xf

.field public static final BYTE_BLOCK_SIZE:I = 0x8000

.field public static final FIRST_LEVEL_SIZE:I

.field public static final LEVEL_SIZE_ARRAY:[I

.field public static final NEXT_LEVEL_ARRAY:[I


# instance fields
.field private final allocator:Lorg/apache/lucene/util/ByteBlockPool$Allocator;

.field public buffer:[B

.field private bufferUpto:I

.field public buffers:[[B

.field public byteOffset:I

.field public byteUpto:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/16 v2, 0xa

    const/4 v1, 0x0

    .line 44
    const-class v0, Lorg/apache/lucene/util/ByteBlockPool;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/ByteBlockPool;->$assertionsDisabled:Z

    .line 232
    new-array v0, v2, [I

    fill-array-data v0, :array_0

    sput-object v0, Lorg/apache/lucene/util/ByteBlockPool;->NEXT_LEVEL_ARRAY:[I

    .line 237
    new-array v0, v2, [I

    fill-array-data v0, :array_1

    sput-object v0, Lorg/apache/lucene/util/ByteBlockPool;->LEVEL_SIZE_ARRAY:[I

    .line 243
    sget-object v0, Lorg/apache/lucene/util/ByteBlockPool;->LEVEL_SIZE_ARRAY:[I

    aget v0, v0, v1

    sput v0, Lorg/apache/lucene/util/ByteBlockPool;->FIRST_LEVEL_SIZE:I

    return-void

    :cond_0
    move v0, v1

    .line 44
    goto :goto_0

    .line 232
    nop

    :array_0
    .array-data 4
        0x1
        0x2
        0x3
        0x4
        0x5
        0x6
        0x7
        0x8
        0x9
        0x9
    .end array-data

    .line 237
    :array_1
    .array-data 4
        0x5
        0xe
        0x14
        0x1e
        0x28
        0x28
        0x50
        0x50
        0x78
        0xc8
    .end array-data
.end method

.method public constructor <init>(Lorg/apache/lucene/util/ByteBlockPool$Allocator;)V
    .locals 1
    .param p1, "allocator"    # Lorg/apache/lucene/util/ByteBlockPool$Allocator;

    .prologue
    .line 133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 119
    const/16 v0, 0xa

    new-array v0, v0, [[B

    iput-object v0, p0, Lorg/apache/lucene/util/ByteBlockPool;->buffers:[[B

    .line 122
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/util/ByteBlockPool;->bufferUpto:I

    .line 124
    const v0, 0x8000

    iput v0, p0, Lorg/apache/lucene/util/ByteBlockPool;->byteUpto:I

    .line 129
    const/16 v0, -0x8000

    iput v0, p0, Lorg/apache/lucene/util/ByteBlockPool;->byteOffset:I

    .line 134
    iput-object p1, p0, Lorg/apache/lucene/util/ByteBlockPool;->allocator:Lorg/apache/lucene/util/ByteBlockPool$Allocator;

    .line 135
    return-void
.end method


# virtual methods
.method public allocSlice([BI)I
    .locals 8
    .param p1, "slice"    # [B
    .param p2, "upto"    # I

    .prologue
    .line 251
    aget-byte v5, p1, p2

    and-int/lit8 v0, v5, 0xf

    .line 252
    .local v0, "level":I
    sget-object v5, Lorg/apache/lucene/util/ByteBlockPool;->NEXT_LEVEL_ARRAY:[I

    aget v1, v5, v0

    .line 253
    .local v1, "newLevel":I
    sget-object v5, Lorg/apache/lucene/util/ByteBlockPool;->LEVEL_SIZE_ARRAY:[I

    aget v2, v5, v1

    .line 256
    .local v2, "newSize":I
    iget v5, p0, Lorg/apache/lucene/util/ByteBlockPool;->byteUpto:I

    const v6, 0x8000

    sub-int/2addr v6, v2

    if-le v5, v6, :cond_0

    .line 257
    invoke-virtual {p0}, Lorg/apache/lucene/util/ByteBlockPool;->nextBuffer()V

    .line 260
    :cond_0
    iget v3, p0, Lorg/apache/lucene/util/ByteBlockPool;->byteUpto:I

    .line 261
    .local v3, "newUpto":I
    iget v5, p0, Lorg/apache/lucene/util/ByteBlockPool;->byteOffset:I

    add-int v4, v3, v5

    .line 262
    .local v4, "offset":I
    iget v5, p0, Lorg/apache/lucene/util/ByteBlockPool;->byteUpto:I

    add-int/2addr v5, v2

    iput v5, p0, Lorg/apache/lucene/util/ByteBlockPool;->byteUpto:I

    .line 266
    iget-object v5, p0, Lorg/apache/lucene/util/ByteBlockPool;->buffer:[B

    add-int/lit8 v6, p2, -0x3

    aget-byte v6, p1, v6

    aput-byte v6, v5, v3

    .line 267
    iget-object v5, p0, Lorg/apache/lucene/util/ByteBlockPool;->buffer:[B

    add-int/lit8 v6, v3, 0x1

    add-int/lit8 v7, p2, -0x2

    aget-byte v7, p1, v7

    aput-byte v7, v5, v6

    .line 268
    iget-object v5, p0, Lorg/apache/lucene/util/ByteBlockPool;->buffer:[B

    add-int/lit8 v6, v3, 0x2

    add-int/lit8 v7, p2, -0x1

    aget-byte v7, p1, v7

    aput-byte v7, v5, v6

    .line 271
    add-int/lit8 v5, p2, -0x3

    ushr-int/lit8 v6, v4, 0x18

    int-to-byte v6, v6

    aput-byte v6, p1, v5

    .line 272
    add-int/lit8 v5, p2, -0x2

    ushr-int/lit8 v6, v4, 0x10

    int-to-byte v6, v6

    aput-byte v6, p1, v5

    .line 273
    add-int/lit8 v5, p2, -0x1

    ushr-int/lit8 v6, v4, 0x8

    int-to-byte v6, v6

    aput-byte v6, p1, v5

    .line 274
    int-to-byte v5, v4

    aput-byte v5, p1, p2

    .line 277
    iget-object v5, p0, Lorg/apache/lucene/util/ByteBlockPool;->buffer:[B

    iget v6, p0, Lorg/apache/lucene/util/ByteBlockPool;->byteUpto:I

    add-int/lit8 v6, v6, -0x1

    or-int/lit8 v7, v1, 0x10

    int-to-byte v7, v7

    aput-byte v7, v5, v6

    .line 279
    add-int/lit8 v5, v3, 0x3

    return v5
.end method

.method public append(Lorg/apache/lucene/util/BytesRef;)V
    .locals 7
    .param p1, "bytes"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 304
    iget v1, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 305
    .local v1, "length":I
    if-nez v1, :cond_0

    .line 326
    :goto_0
    return-void

    .line 308
    :cond_0
    iget v2, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 309
    .local v2, "offset":I
    iget v4, p0, Lorg/apache/lucene/util/ByteBlockPool;->byteUpto:I

    add-int/2addr v4, v1

    add-int/lit16 v3, v4, -0x8000

    .line 311
    .local v3, "overflow":I
    :goto_1
    if-gtz v3, :cond_1

    .line 312
    iget-object v4, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget-object v5, p0, Lorg/apache/lucene/util/ByteBlockPool;->buffer:[B

    iget v6, p0, Lorg/apache/lucene/util/ByteBlockPool;->byteUpto:I

    invoke-static {v4, v2, v5, v6, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 313
    iget v4, p0, Lorg/apache/lucene/util/ByteBlockPool;->byteUpto:I

    add-int/2addr v4, v1

    iput v4, p0, Lorg/apache/lucene/util/ByteBlockPool;->byteUpto:I

    goto :goto_0

    .line 316
    :cond_1
    sub-int v0, v1, v3

    .line 317
    .local v0, "bytesToCopy":I
    if-lez v0, :cond_2

    .line 318
    iget-object v4, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget-object v5, p0, Lorg/apache/lucene/util/ByteBlockPool;->buffer:[B

    iget v6, p0, Lorg/apache/lucene/util/ByteBlockPool;->byteUpto:I

    invoke-static {v4, v2, v5, v6, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 319
    add-int/2addr v2, v0

    .line 320
    sub-int/2addr v1, v0

    .line 322
    :cond_2
    invoke-virtual {p0}, Lorg/apache/lucene/util/ByteBlockPool;->nextBuffer()V

    .line 323
    add-int/lit16 v3, v3, -0x8000

    .line 325
    goto :goto_1
.end method

.method public newSlice(I)I
    .locals 4
    .param p1, "size"    # I

    .prologue
    .line 214
    iget v1, p0, Lorg/apache/lucene/util/ByteBlockPool;->byteUpto:I

    const v2, 0x8000

    sub-int/2addr v2, p1

    if-le v1, v2, :cond_0

    .line 215
    invoke-virtual {p0}, Lorg/apache/lucene/util/ByteBlockPool;->nextBuffer()V

    .line 216
    :cond_0
    iget v0, p0, Lorg/apache/lucene/util/ByteBlockPool;->byteUpto:I

    .line 217
    .local v0, "upto":I
    iget v1, p0, Lorg/apache/lucene/util/ByteBlockPool;->byteUpto:I

    add-int/2addr v1, p1

    iput v1, p0, Lorg/apache/lucene/util/ByteBlockPool;->byteUpto:I

    .line 218
    iget-object v1, p0, Lorg/apache/lucene/util/ByteBlockPool;->buffer:[B

    iget v2, p0, Lorg/apache/lucene/util/ByteBlockPool;->byteUpto:I

    add-int/lit8 v2, v2, -0x1

    const/16 v3, 0x10

    aput-byte v3, v1, v2

    .line 219
    return v0
.end method

.method public nextBuffer()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 196
    iget v1, p0, Lorg/apache/lucene/util/ByteBlockPool;->bufferUpto:I

    add-int/lit8 v1, v1, 0x1

    iget-object v2, p0, Lorg/apache/lucene/util/ByteBlockPool;->buffers:[[B

    array-length v2, v2

    if-ne v1, v2, :cond_0

    .line 197
    iget-object v1, p0, Lorg/apache/lucene/util/ByteBlockPool;->buffers:[[B

    array-length v1, v1

    add-int/lit8 v1, v1, 0x1

    .line 198
    sget v2, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_REF:I

    .line 197
    invoke-static {v1, v2}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v1

    new-array v0, v1, [[B

    .line 199
    .local v0, "newBuffers":[[B
    iget-object v1, p0, Lorg/apache/lucene/util/ByteBlockPool;->buffers:[[B

    iget-object v2, p0, Lorg/apache/lucene/util/ByteBlockPool;->buffers:[[B

    array-length v2, v2

    invoke-static {v1, v4, v0, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 200
    iput-object v0, p0, Lorg/apache/lucene/util/ByteBlockPool;->buffers:[[B

    .line 202
    .end local v0    # "newBuffers":[[B
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/util/ByteBlockPool;->buffers:[[B

    iget v2, p0, Lorg/apache/lucene/util/ByteBlockPool;->bufferUpto:I

    add-int/lit8 v2, v2, 0x1

    iget-object v3, p0, Lorg/apache/lucene/util/ByteBlockPool;->allocator:Lorg/apache/lucene/util/ByteBlockPool$Allocator;

    invoke-virtual {v3}, Lorg/apache/lucene/util/ByteBlockPool$Allocator;->getByteBlock()[B

    move-result-object v3

    aput-object v3, v1, v2

    iput-object v3, p0, Lorg/apache/lucene/util/ByteBlockPool;->buffer:[B

    .line 203
    iget v1, p0, Lorg/apache/lucene/util/ByteBlockPool;->bufferUpto:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/util/ByteBlockPool;->bufferUpto:I

    .line 205
    iput v4, p0, Lorg/apache/lucene/util/ByteBlockPool;->byteUpto:I

    .line 206
    iget v1, p0, Lorg/apache/lucene/util/ByteBlockPool;->byteOffset:I

    const v2, 0x8000

    add-int/2addr v1, v2

    iput v1, p0, Lorg/apache/lucene/util/ByteBlockPool;->byteOffset:I

    .line 207
    return-void
.end method

.method public readBytes(J[BII)V
    .locals 11
    .param p1, "offset"    # J
    .param p3, "bytes"    # [B
    .param p4, "off"    # I
    .param p5, "length"    # I

    .prologue
    .line 334
    if-nez p5, :cond_0

    .line 357
    :goto_0
    return-void

    .line 337
    :cond_0
    move v3, p4

    .line 338
    .local v3, "bytesOffset":I
    move/from16 v2, p5

    .line 339
    .local v2, "bytesLength":I
    const/16 v7, 0xf

    shr-long v8, p1, v7

    long-to-int v1, v8

    .line 340
    .local v1, "bufferIndex":I
    iget-object v7, p0, Lorg/apache/lucene/util/ByteBlockPool;->buffers:[[B

    aget-object v0, v7, v1

    .line 341
    .local v0, "buffer":[B
    const-wide/16 v8, 0x7fff

    and-long/2addr v8, p1

    long-to-int v6, v8

    .line 342
    .local v6, "pos":I
    add-int v7, v6, p5

    add-int/lit16 v5, v7, -0x8000

    .line 344
    .local v5, "overflow":I
    :goto_1
    if-gtz v5, :cond_1

    .line 345
    invoke-static {v0, v6, p3, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0

    .line 348
    :cond_1
    sub-int v4, p5, v5

    .line 349
    .local v4, "bytesToCopy":I
    invoke-static {v0, v6, p3, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 350
    const/4 v6, 0x0

    .line 351
    sub-int/2addr v2, v4

    .line 352
    add-int/2addr v3, v4

    .line 353
    iget-object v7, p0, Lorg/apache/lucene/util/ByteBlockPool;->buffers:[[B

    add-int/lit8 v1, v1, 0x1

    aget-object v0, v7, v1

    .line 354
    add-int/lit16 v5, v5, -0x8000

    .line 356
    goto :goto_1
.end method

.method public reset()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 144
    invoke-virtual {p0, v0, v0}, Lorg/apache/lucene/util/ByteBlockPool;->reset(ZZ)V

    .line 145
    return-void
.end method

.method public reset(ZZ)V
    .locals 8
    .param p1, "zeroFillBuffers"    # Z
    .param p2, "reuseFirst"    # Z

    .prologue
    const/4 v7, 0x0

    const/4 v6, -0x1

    const/4 v2, 0x0

    .line 157
    iget v3, p0, Lorg/apache/lucene/util/ByteBlockPool;->bufferUpto:I

    if-eq v3, v6, :cond_3

    .line 160
    if-eqz p1, :cond_0

    .line 161
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v3, p0, Lorg/apache/lucene/util/ByteBlockPool;->bufferUpto:I

    if-lt v0, v3, :cond_4

    .line 166
    iget-object v3, p0, Lorg/apache/lucene/util/ByteBlockPool;->buffers:[[B

    iget v4, p0, Lorg/apache/lucene/util/ByteBlockPool;->bufferUpto:I

    aget-object v3, v3, v4

    iget v4, p0, Lorg/apache/lucene/util/ByteBlockPool;->byteUpto:I

    invoke-static {v3, v2, v4, v2}, Ljava/util/Arrays;->fill([BIIB)V

    .line 169
    .end local v0    # "i":I
    :cond_0
    iget v3, p0, Lorg/apache/lucene/util/ByteBlockPool;->bufferUpto:I

    if-gtz v3, :cond_1

    if-nez p2, :cond_2

    .line 170
    :cond_1
    if-eqz p2, :cond_5

    const/4 v1, 0x1

    .line 172
    .local v1, "offset":I
    :goto_1
    iget-object v3, p0, Lorg/apache/lucene/util/ByteBlockPool;->allocator:Lorg/apache/lucene/util/ByteBlockPool$Allocator;

    iget-object v4, p0, Lorg/apache/lucene/util/ByteBlockPool;->buffers:[[B

    iget v5, p0, Lorg/apache/lucene/util/ByteBlockPool;->bufferUpto:I

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v3, v4, v1, v5}, Lorg/apache/lucene/util/ByteBlockPool$Allocator;->recycleByteBlocks([[BII)V

    .line 173
    iget-object v3, p0, Lorg/apache/lucene/util/ByteBlockPool;->buffers:[[B

    iget v4, p0, Lorg/apache/lucene/util/ByteBlockPool;->bufferUpto:I

    add-int/lit8 v4, v4, 0x1

    invoke-static {v3, v1, v4, v7}, Ljava/util/Arrays;->fill([Ljava/lang/Object;IILjava/lang/Object;)V

    .line 175
    .end local v1    # "offset":I
    :cond_2
    if-eqz p2, :cond_6

    .line 177
    iput v2, p0, Lorg/apache/lucene/util/ByteBlockPool;->bufferUpto:I

    .line 178
    iput v2, p0, Lorg/apache/lucene/util/ByteBlockPool;->byteUpto:I

    .line 179
    iput v2, p0, Lorg/apache/lucene/util/ByteBlockPool;->byteOffset:I

    .line 180
    iget-object v3, p0, Lorg/apache/lucene/util/ByteBlockPool;->buffers:[[B

    aget-object v2, v3, v2

    iput-object v2, p0, Lorg/apache/lucene/util/ByteBlockPool;->buffer:[B

    .line 188
    :cond_3
    :goto_2
    return-void

    .line 163
    .restart local v0    # "i":I
    :cond_4
    iget-object v3, p0, Lorg/apache/lucene/util/ByteBlockPool;->buffers:[[B

    aget-object v3, v3, v0

    invoke-static {v3, v2}, Ljava/util/Arrays;->fill([BB)V

    .line 161
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .end local v0    # "i":I
    :cond_5
    move v1, v2

    .line 170
    goto :goto_1

    .line 182
    :cond_6
    iput v6, p0, Lorg/apache/lucene/util/ByteBlockPool;->bufferUpto:I

    .line 183
    const v2, 0x8000

    iput v2, p0, Lorg/apache/lucene/util/ByteBlockPool;->byteUpto:I

    .line 184
    const/16 v2, -0x8000

    iput v2, p0, Lorg/apache/lucene/util/ByteBlockPool;->byteOffset:I

    .line 185
    iput-object v7, p0, Lorg/apache/lucene/util/ByteBlockPool;->buffer:[B

    goto :goto_2
.end method

.method public setBytesRef(Lorg/apache/lucene/util/BytesRef;I)V
    .locals 4
    .param p1, "term"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "textStart"    # I

    .prologue
    .line 285
    iget-object v2, p0, Lorg/apache/lucene/util/ByteBlockPool;->buffers:[[B

    shr-int/lit8 v3, p2, 0xf

    aget-object v0, v2, v3

    iput-object v0, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 286
    .local v0, "bytes":[B
    and-int/lit16 v1, p2, 0x7fff

    .line 287
    .local v1, "pos":I
    aget-byte v2, v0, v1

    and-int/lit16 v2, v2, 0x80

    if-nez v2, :cond_0

    .line 289
    aget-byte v2, v0, v1

    iput v2, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 290
    add-int/lit8 v2, v1, 0x1

    iput v2, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 296
    :goto_0
    sget-boolean v2, Lorg/apache/lucene/util/ByteBlockPool;->$assertionsDisabled:Z

    if-nez v2, :cond_1

    iget v2, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    if-gez v2, :cond_1

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 293
    :cond_0
    aget-byte v2, v0, v1

    and-int/lit8 v2, v2, 0x7f

    add-int/lit8 v3, v1, 0x1

    aget-byte v3, v0, v3

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x7

    add-int/2addr v2, v3

    iput v2, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 294
    add-int/lit8 v2, v1, 0x2

    iput v2, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    goto :goto_0

    .line 297
    :cond_1
    return-void
.end method
