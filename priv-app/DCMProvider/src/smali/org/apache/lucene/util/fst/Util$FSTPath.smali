.class Lorg/apache/lucene/util/fst/Util$FSTPath;
.super Ljava/lang/Object;
.source "Util.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/fst/Util;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FSTPath"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public arc:Lorg/apache/lucene/util/fst/FST$Arc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;"
        }
    .end annotation
.end field

.field public cost:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field public final input:Lorg/apache/lucene/util/IntsRef;


# direct methods
.method public constructor <init>(Ljava/lang/Object;Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/IntsRef;)V
    .locals 1
    .param p3, "input"    # Lorg/apache/lucene/util/IntsRef;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;",
            "Lorg/apache/lucene/util/IntsRef;",
            ")V"
        }
    .end annotation

    .prologue
    .line 248
    .local p0, "this":Lorg/apache/lucene/util/fst/Util$FSTPath;, "Lorg/apache/lucene/util/fst/Util$FSTPath<TT;>;"
    .local p1, "cost":Ljava/lang/Object;, "TT;"
    .local p2, "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 249
    new-instance v0, Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-direct {v0}, Lorg/apache/lucene/util/fst/FST$Arc;-><init>()V

    invoke-virtual {v0, p2}, Lorg/apache/lucene/util/fst/FST$Arc;->copyFrom(Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/util/fst/Util$FSTPath;->arc:Lorg/apache/lucene/util/fst/FST$Arc;

    .line 250
    iput-object p1, p0, Lorg/apache/lucene/util/fst/Util$FSTPath;->cost:Ljava/lang/Object;

    .line 251
    iput-object p3, p0, Lorg/apache/lucene/util/fst/Util$FSTPath;->input:Lorg/apache/lucene/util/IntsRef;

    .line 252
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 256
    .local p0, "this":Lorg/apache/lucene/util/fst/Util$FSTPath;, "Lorg/apache/lucene/util/fst/Util$FSTPath<TT;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "input="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/util/fst/Util$FSTPath;->input:Lorg/apache/lucene/util/IntsRef;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " cost="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/util/fst/Util$FSTPath;->cost:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
