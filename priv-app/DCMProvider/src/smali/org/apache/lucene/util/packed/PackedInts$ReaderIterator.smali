.class public interface abstract Lorg/apache/lucene/util/packed/PackedInts$ReaderIterator;
.super Ljava/lang/Object;
.source "PackedInts.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/packed/PackedInts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ReaderIterator"
.end annotation


# virtual methods
.method public abstract getBitsPerValue()I
.end method

.method public abstract next()J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract next(I)Lorg/apache/lucene/util/LongsRef;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract ord()I
.end method

.method public abstract size()I
.end method
