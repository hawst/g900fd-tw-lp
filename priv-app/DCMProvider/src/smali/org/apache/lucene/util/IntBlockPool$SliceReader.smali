.class public final Lorg/apache/lucene/util/IntBlockPool$SliceReader;
.super Ljava/lang/Object;
.source "IntBlockPool.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/IntBlockPool;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SliceReader"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private buffer:[I

.field private bufferOffset:I

.field private bufferUpto:I

.field private end:I

.field private level:I

.field private limit:I

.field private final pool:Lorg/apache/lucene/util/IntBlockPool;

.field private upto:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 293
    const-class v0, Lorg/apache/lucene/util/IntBlockPool;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/IntBlockPool$SliceReader;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/util/IntBlockPool;)V
    .locals 0
    .param p1, "pool"    # Lorg/apache/lucene/util/IntBlockPool;

    .prologue
    .line 307
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 308
    iput-object p1, p0, Lorg/apache/lucene/util/IntBlockPool$SliceReader;->pool:Lorg/apache/lucene/util/IntBlockPool;

    .line 309
    return-void
.end method

.method private nextSlice()V
    .locals 4

    .prologue
    .line 358
    iget-object v2, p0, Lorg/apache/lucene/util/IntBlockPool$SliceReader;->buffer:[I

    iget v3, p0, Lorg/apache/lucene/util/IntBlockPool$SliceReader;->limit:I

    aget v1, v2, v3

    .line 359
    .local v1, "nextIndex":I
    # getter for: Lorg/apache/lucene/util/IntBlockPool;->NEXT_LEVEL_ARRAY:[I
    invoke-static {}, Lorg/apache/lucene/util/IntBlockPool;->access$4()[I

    move-result-object v2

    iget v3, p0, Lorg/apache/lucene/util/IntBlockPool$SliceReader;->level:I

    add-int/lit8 v3, v3, -0x1

    aget v2, v2, v3

    iput v2, p0, Lorg/apache/lucene/util/IntBlockPool$SliceReader;->level:I

    .line 360
    # getter for: Lorg/apache/lucene/util/IntBlockPool;->LEVEL_SIZE_ARRAY:[I
    invoke-static {}, Lorg/apache/lucene/util/IntBlockPool;->access$3()[I

    move-result-object v2

    iget v3, p0, Lorg/apache/lucene/util/IntBlockPool$SliceReader;->level:I

    aget v0, v2, v3

    .line 362
    .local v0, "newSize":I
    div-int/lit16 v2, v1, 0x2000

    iput v2, p0, Lorg/apache/lucene/util/IntBlockPool$SliceReader;->bufferUpto:I

    .line 363
    iget v2, p0, Lorg/apache/lucene/util/IntBlockPool$SliceReader;->bufferUpto:I

    mul-int/lit16 v2, v2, 0x2000

    iput v2, p0, Lorg/apache/lucene/util/IntBlockPool$SliceReader;->bufferOffset:I

    .line 365
    iget-object v2, p0, Lorg/apache/lucene/util/IntBlockPool$SliceReader;->pool:Lorg/apache/lucene/util/IntBlockPool;

    iget-object v2, v2, Lorg/apache/lucene/util/IntBlockPool;->buffers:[[I

    iget v3, p0, Lorg/apache/lucene/util/IntBlockPool$SliceReader;->bufferUpto:I

    aget-object v2, v2, v3

    iput-object v2, p0, Lorg/apache/lucene/util/IntBlockPool$SliceReader;->buffer:[I

    .line 366
    and-int/lit16 v2, v1, 0x1fff

    iput v2, p0, Lorg/apache/lucene/util/IntBlockPool$SliceReader;->upto:I

    .line 368
    add-int v2, v1, v0

    iget v3, p0, Lorg/apache/lucene/util/IntBlockPool$SliceReader;->end:I

    if-lt v2, v3, :cond_1

    .line 370
    sget-boolean v2, Lorg/apache/lucene/util/IntBlockPool$SliceReader;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    iget v2, p0, Lorg/apache/lucene/util/IntBlockPool$SliceReader;->end:I

    sub-int/2addr v2, v1

    if-gtz v2, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 371
    :cond_0
    iget v2, p0, Lorg/apache/lucene/util/IntBlockPool$SliceReader;->end:I

    iget v3, p0, Lorg/apache/lucene/util/IntBlockPool$SliceReader;->bufferOffset:I

    sub-int/2addr v2, v3

    iput v2, p0, Lorg/apache/lucene/util/IntBlockPool$SliceReader;->limit:I

    .line 377
    :goto_0
    return-void

    .line 375
    :cond_1
    iget v2, p0, Lorg/apache/lucene/util/IntBlockPool$SliceReader;->upto:I

    add-int/2addr v2, v0

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lorg/apache/lucene/util/IntBlockPool$SliceReader;->limit:I

    goto :goto_0
.end method


# virtual methods
.method public endOfSlice()Z
    .locals 2

    .prologue
    .line 340
    sget-boolean v0, Lorg/apache/lucene/util/IntBlockPool$SliceReader;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/util/IntBlockPool$SliceReader;->upto:I

    iget v1, p0, Lorg/apache/lucene/util/IntBlockPool$SliceReader;->bufferOffset:I

    add-int/2addr v0, v1

    iget v1, p0, Lorg/apache/lucene/util/IntBlockPool$SliceReader;->end:I

    if-le v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 341
    :cond_0
    iget v0, p0, Lorg/apache/lucene/util/IntBlockPool$SliceReader;->upto:I

    iget v1, p0, Lorg/apache/lucene/util/IntBlockPool$SliceReader;->bufferOffset:I

    add-int/2addr v0, v1

    iget v1, p0, Lorg/apache/lucene/util/IntBlockPool$SliceReader;->end:I

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public readInt()I
    .locals 3

    .prologue
    .line 349
    sget-boolean v0, Lorg/apache/lucene/util/IntBlockPool$SliceReader;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/util/IntBlockPool$SliceReader;->endOfSlice()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 350
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/util/IntBlockPool$SliceReader;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget v0, p0, Lorg/apache/lucene/util/IntBlockPool$SliceReader;->upto:I

    iget v1, p0, Lorg/apache/lucene/util/IntBlockPool$SliceReader;->limit:I

    if-le v0, v1, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 351
    :cond_1
    iget v0, p0, Lorg/apache/lucene/util/IntBlockPool$SliceReader;->upto:I

    iget v1, p0, Lorg/apache/lucene/util/IntBlockPool$SliceReader;->limit:I

    if-ne v0, v1, :cond_2

    .line 352
    invoke-direct {p0}, Lorg/apache/lucene/util/IntBlockPool$SliceReader;->nextSlice()V

    .line 353
    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/util/IntBlockPool$SliceReader;->buffer:[I

    iget v1, p0, Lorg/apache/lucene/util/IntBlockPool$SliceReader;->upto:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/util/IntBlockPool$SliceReader;->upto:I

    aget v0, v0, v1

    return v0
.end method

.method public reset(II)V
    .locals 3
    .param p1, "startOffset"    # I
    .param p2, "endOffset"    # I

    .prologue
    .line 315
    div-int/lit16 v1, p1, 0x2000

    iput v1, p0, Lorg/apache/lucene/util/IntBlockPool$SliceReader;->bufferUpto:I

    .line 316
    iget v1, p0, Lorg/apache/lucene/util/IntBlockPool$SliceReader;->bufferUpto:I

    mul-int/lit16 v1, v1, 0x2000

    iput v1, p0, Lorg/apache/lucene/util/IntBlockPool$SliceReader;->bufferOffset:I

    .line 317
    iput p2, p0, Lorg/apache/lucene/util/IntBlockPool$SliceReader;->end:I

    .line 318
    iput p1, p0, Lorg/apache/lucene/util/IntBlockPool$SliceReader;->upto:I

    .line 319
    const/4 v1, 0x1

    iput v1, p0, Lorg/apache/lucene/util/IntBlockPool$SliceReader;->level:I

    .line 321
    iget-object v1, p0, Lorg/apache/lucene/util/IntBlockPool$SliceReader;->pool:Lorg/apache/lucene/util/IntBlockPool;

    iget-object v1, v1, Lorg/apache/lucene/util/IntBlockPool;->buffers:[[I

    iget v2, p0, Lorg/apache/lucene/util/IntBlockPool$SliceReader;->bufferUpto:I

    aget-object v1, v1, v2

    iput-object v1, p0, Lorg/apache/lucene/util/IntBlockPool$SliceReader;->buffer:[I

    .line 322
    and-int/lit16 v1, p1, 0x1fff

    iput v1, p0, Lorg/apache/lucene/util/IntBlockPool$SliceReader;->upto:I

    .line 324
    # getter for: Lorg/apache/lucene/util/IntBlockPool;->LEVEL_SIZE_ARRAY:[I
    invoke-static {}, Lorg/apache/lucene/util/IntBlockPool;->access$3()[I

    move-result-object v1

    const/4 v2, 0x0

    aget v0, v1, v2

    .line 325
    .local v0, "firstSize":I
    add-int v1, p1, v0

    if-lt v1, p2, :cond_0

    .line 327
    and-int/lit16 v1, p2, 0x1fff

    iput v1, p0, Lorg/apache/lucene/util/IntBlockPool$SliceReader;->limit:I

    .line 332
    :goto_0
    return-void

    .line 329
    :cond_0
    iget v1, p0, Lorg/apache/lucene/util/IntBlockPool$SliceReader;->upto:I

    add-int/2addr v1, v0

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/lucene/util/IntBlockPool$SliceReader;->limit:I

    goto :goto_0
.end method
