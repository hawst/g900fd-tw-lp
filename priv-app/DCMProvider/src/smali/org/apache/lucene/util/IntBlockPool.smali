.class public final Lorg/apache/lucene/util/IntBlockPool;
.super Ljava/lang/Object;
.source "IntBlockPool.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/IntBlockPool$Allocator;,
        Lorg/apache/lucene/util/IntBlockPool$DirectAllocator;,
        Lorg/apache/lucene/util/IntBlockPool$SliceReader;,
        Lorg/apache/lucene/util/IntBlockPool$SliceWriter;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final FIRST_LEVEL_SIZE:I

.field public static final INT_BLOCK_MASK:I = 0x1fff

.field public static final INT_BLOCK_SHIFT:I = 0xd

.field public static final INT_BLOCK_SIZE:I = 0x2000

.field private static final LEVEL_SIZE_ARRAY:[I

.field private static final NEXT_LEVEL_ARRAY:[I


# instance fields
.field private final allocator:Lorg/apache/lucene/util/IntBlockPool$Allocator;

.field public buffer:[I

.field private bufferUpto:I

.field public buffers:[[I

.field public intOffset:I

.field public intUpto:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/16 v2, 0xa

    const/4 v1, 0x0

    .line 26
    const-class v0, Lorg/apache/lucene/util/IntBlockPool;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/IntBlockPool;->$assertionsDisabled:Z

    .line 193
    new-array v0, v2, [I

    fill-array-data v0, :array_0

    sput-object v0, Lorg/apache/lucene/util/IntBlockPool;->NEXT_LEVEL_ARRAY:[I

    .line 198
    new-array v0, v2, [I

    fill-array-data v0, :array_1

    sput-object v0, Lorg/apache/lucene/util/IntBlockPool;->LEVEL_SIZE_ARRAY:[I

    .line 203
    sget-object v0, Lorg/apache/lucene/util/IntBlockPool;->LEVEL_SIZE_ARRAY:[I

    aget v0, v0, v1

    sput v0, Lorg/apache/lucene/util/IntBlockPool;->FIRST_LEVEL_SIZE:I

    return-void

    :cond_0
    move v0, v1

    .line 26
    goto :goto_0

    .line 193
    nop

    :array_0
    .array-data 4
        0x1
        0x2
        0x3
        0x4
        0x5
        0x6
        0x7
        0x8
        0x9
        0x9
    .end array-data

    .line 198
    :array_1
    .array-data 4
        0x2
        0x4
        0x8
        0x10
        0x20
        0x40
        0x80
        0x100
        0x200
        0x400
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 81
    new-instance v0, Lorg/apache/lucene/util/IntBlockPool$DirectAllocator;

    invoke-direct {v0}, Lorg/apache/lucene/util/IntBlockPool$DirectAllocator;-><init>()V

    invoke-direct {p0, v0}, Lorg/apache/lucene/util/IntBlockPool;-><init>(Lorg/apache/lucene/util/IntBlockPool$Allocator;)V

    .line 82
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/IntBlockPool$Allocator;)V
    .locals 1
    .param p1, "allocator"    # Lorg/apache/lucene/util/IntBlockPool$Allocator;

    .prologue
    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    const/16 v0, 0xa

    new-array v0, v0, [[I

    iput-object v0, p0, Lorg/apache/lucene/util/IntBlockPool;->buffers:[[I

    .line 66
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/util/IntBlockPool;->bufferUpto:I

    .line 68
    const/16 v0, 0x2000

    iput v0, p0, Lorg/apache/lucene/util/IntBlockPool;->intUpto:I

    .line 72
    const/16 v0, -0x2000

    iput v0, p0, Lorg/apache/lucene/util/IntBlockPool;->intOffset:I

    .line 89
    iput-object p1, p0, Lorg/apache/lucene/util/IntBlockPool;->allocator:Lorg/apache/lucene/util/IntBlockPool$Allocator;

    .line 90
    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/util/IntBlockPool;[II)I
    .locals 1

    .prologue
    .line 208
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/util/IntBlockPool;->allocSlice([II)I

    move-result v0

    return v0
.end method

.method static synthetic access$1()I
    .locals 1

    .prologue
    .line 203
    sget v0, Lorg/apache/lucene/util/IntBlockPool;->FIRST_LEVEL_SIZE:I

    return v0
.end method

.method static synthetic access$2(Lorg/apache/lucene/util/IntBlockPool;I)I
    .locals 1

    .prologue
    .line 166
    invoke-direct {p0, p1}, Lorg/apache/lucene/util/IntBlockPool;->newSlice(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$3()[I
    .locals 1

    .prologue
    .line 198
    sget-object v0, Lorg/apache/lucene/util/IntBlockPool;->LEVEL_SIZE_ARRAY:[I

    return-object v0
.end method

.method static synthetic access$4()[I
    .locals 1

    .prologue
    .line 193
    sget-object v0, Lorg/apache/lucene/util/IntBlockPool;->NEXT_LEVEL_ARRAY:[I

    return-object v0
.end method

.method private allocSlice([II)I
    .locals 7
    .param p1, "slice"    # [I
    .param p2, "sliceOffset"    # I

    .prologue
    .line 209
    aget v0, p1, p2

    .line 210
    .local v0, "level":I
    sget-object v5, Lorg/apache/lucene/util/IntBlockPool;->NEXT_LEVEL_ARRAY:[I

    add-int/lit8 v6, v0, -0x1

    aget v1, v5, v6

    .line 211
    .local v1, "newLevel":I
    sget-object v5, Lorg/apache/lucene/util/IntBlockPool;->LEVEL_SIZE_ARRAY:[I

    aget v2, v5, v1

    .line 213
    .local v2, "newSize":I
    iget v5, p0, Lorg/apache/lucene/util/IntBlockPool;->intUpto:I

    rsub-int v6, v2, 0x2000

    if-le v5, v6, :cond_0

    .line 214
    invoke-virtual {p0}, Lorg/apache/lucene/util/IntBlockPool;->nextBuffer()V

    .line 215
    sget-boolean v5, Lorg/apache/lucene/util/IntBlockPool;->$assertionsDisabled:Z

    if-nez v5, :cond_0

    iget-object v5, p0, Lorg/apache/lucene/util/IntBlockPool;->buffer:[I

    invoke-static {v5}, Lorg/apache/lucene/util/IntBlockPool;->assertSliceBuffer([I)Z

    move-result v5

    if-nez v5, :cond_0

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 218
    :cond_0
    iget v3, p0, Lorg/apache/lucene/util/IntBlockPool;->intUpto:I

    .line 219
    .local v3, "newUpto":I
    iget v5, p0, Lorg/apache/lucene/util/IntBlockPool;->intOffset:I

    add-int v4, v3, v5

    .line 220
    .local v4, "offset":I
    iget v5, p0, Lorg/apache/lucene/util/IntBlockPool;->intUpto:I

    add-int/2addr v5, v2

    iput v5, p0, Lorg/apache/lucene/util/IntBlockPool;->intUpto:I

    .line 222
    aput v4, p1, p2

    .line 225
    iget-object v5, p0, Lorg/apache/lucene/util/IntBlockPool;->buffer:[I

    iget v6, p0, Lorg/apache/lucene/util/IntBlockPool;->intUpto:I

    add-int/lit8 v6, v6, -0x1

    aput v1, v5, v6

    .line 227
    return v3
.end method

.method private static final assertSliceBuffer([I)Z
    .locals 3
    .param p0, "buffer"    # [I

    .prologue
    .line 179
    const/4 v0, 0x0

    .line 180
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, p0

    if-lt v1, v2, :cond_0

    .line 183
    if-nez v0, :cond_1

    const/4 v2, 0x1

    :goto_1
    return v2

    .line 181
    :cond_0
    aget v2, p0, v1

    add-int/2addr v0, v2

    .line 180
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 183
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private newSlice(I)I
    .locals 4
    .param p1, "size"    # I

    .prologue
    .line 167
    iget v1, p0, Lorg/apache/lucene/util/IntBlockPool;->intUpto:I

    rsub-int v2, p1, 0x2000

    if-le v1, v2, :cond_0

    .line 168
    invoke-virtual {p0}, Lorg/apache/lucene/util/IntBlockPool;->nextBuffer()V

    .line 169
    sget-boolean v1, Lorg/apache/lucene/util/IntBlockPool;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/util/IntBlockPool;->buffer:[I

    invoke-static {v1}, Lorg/apache/lucene/util/IntBlockPool;->assertSliceBuffer([I)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 172
    :cond_0
    iget v0, p0, Lorg/apache/lucene/util/IntBlockPool;->intUpto:I

    .line 173
    .local v0, "upto":I
    iget v1, p0, Lorg/apache/lucene/util/IntBlockPool;->intUpto:I

    add-int/2addr v1, p1

    iput v1, p0, Lorg/apache/lucene/util/IntBlockPool;->intUpto:I

    .line 174
    iget-object v1, p0, Lorg/apache/lucene/util/IntBlockPool;->buffer:[I

    iget v2, p0, Lorg/apache/lucene/util/IntBlockPool;->intUpto:I

    add-int/lit8 v2, v2, -0x1

    const/4 v3, 0x1

    aput v3, v1, v2

    .line 175
    return v0
.end method


# virtual methods
.method public nextBuffer()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 150
    iget v1, p0, Lorg/apache/lucene/util/IntBlockPool;->bufferUpto:I

    add-int/lit8 v1, v1, 0x1

    iget-object v2, p0, Lorg/apache/lucene/util/IntBlockPool;->buffers:[[I

    array-length v2, v2

    if-ne v1, v2, :cond_0

    .line 151
    iget-object v1, p0, Lorg/apache/lucene/util/IntBlockPool;->buffers:[[I

    array-length v1, v1

    int-to-double v2, v1

    const-wide/high16 v4, 0x3ff8000000000000L    # 1.5

    mul-double/2addr v2, v4

    double-to-int v1, v2

    new-array v0, v1, [[I

    .line 152
    .local v0, "newBuffers":[[I
    iget-object v1, p0, Lorg/apache/lucene/util/IntBlockPool;->buffers:[[I

    iget-object v2, p0, Lorg/apache/lucene/util/IntBlockPool;->buffers:[[I

    array-length v2, v2

    invoke-static {v1, v6, v0, v6, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 153
    iput-object v0, p0, Lorg/apache/lucene/util/IntBlockPool;->buffers:[[I

    .line 155
    .end local v0    # "newBuffers":[[I
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/util/IntBlockPool;->buffers:[[I

    iget v2, p0, Lorg/apache/lucene/util/IntBlockPool;->bufferUpto:I

    add-int/lit8 v2, v2, 0x1

    iget-object v3, p0, Lorg/apache/lucene/util/IntBlockPool;->allocator:Lorg/apache/lucene/util/IntBlockPool$Allocator;

    invoke-virtual {v3}, Lorg/apache/lucene/util/IntBlockPool$Allocator;->getIntBlock()[I

    move-result-object v3

    aput-object v3, v1, v2

    iput-object v3, p0, Lorg/apache/lucene/util/IntBlockPool;->buffer:[I

    .line 156
    iget v1, p0, Lorg/apache/lucene/util/IntBlockPool;->bufferUpto:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/util/IntBlockPool;->bufferUpto:I

    .line 158
    iput v6, p0, Lorg/apache/lucene/util/IntBlockPool;->intUpto:I

    .line 159
    iget v1, p0, Lorg/apache/lucene/util/IntBlockPool;->intOffset:I

    add-int/lit16 v1, v1, 0x2000

    iput v1, p0, Lorg/apache/lucene/util/IntBlockPool;->intOffset:I

    .line 160
    return-void
.end method

.method public reset()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 97
    invoke-virtual {p0, v0, v0}, Lorg/apache/lucene/util/IntBlockPool;->reset(ZZ)V

    .line 98
    return-void
.end method

.method public reset(ZZ)V
    .locals 8
    .param p1, "zeroFillBuffers"    # Z
    .param p2, "reuseFirst"    # Z

    .prologue
    const/4 v7, 0x0

    const/4 v6, -0x1

    const/4 v2, 0x0

    .line 110
    iget v3, p0, Lorg/apache/lucene/util/IntBlockPool;->bufferUpto:I

    if-eq v3, v6, :cond_3

    .line 113
    if-eqz p1, :cond_0

    .line 114
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v3, p0, Lorg/apache/lucene/util/IntBlockPool;->bufferUpto:I

    if-lt v0, v3, :cond_4

    .line 119
    iget-object v3, p0, Lorg/apache/lucene/util/IntBlockPool;->buffers:[[I

    iget v4, p0, Lorg/apache/lucene/util/IntBlockPool;->bufferUpto:I

    aget-object v3, v3, v4

    iget v4, p0, Lorg/apache/lucene/util/IntBlockPool;->intUpto:I

    invoke-static {v3, v2, v4, v2}, Ljava/util/Arrays;->fill([IIII)V

    .line 122
    .end local v0    # "i":I
    :cond_0
    iget v3, p0, Lorg/apache/lucene/util/IntBlockPool;->bufferUpto:I

    if-gtz v3, :cond_1

    if-nez p2, :cond_2

    .line 123
    :cond_1
    if-eqz p2, :cond_5

    const/4 v1, 0x1

    .line 125
    .local v1, "offset":I
    :goto_1
    iget-object v3, p0, Lorg/apache/lucene/util/IntBlockPool;->allocator:Lorg/apache/lucene/util/IntBlockPool$Allocator;

    iget-object v4, p0, Lorg/apache/lucene/util/IntBlockPool;->buffers:[[I

    iget v5, p0, Lorg/apache/lucene/util/IntBlockPool;->bufferUpto:I

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v3, v4, v1, v5}, Lorg/apache/lucene/util/IntBlockPool$Allocator;->recycleIntBlocks([[III)V

    .line 126
    iget-object v3, p0, Lorg/apache/lucene/util/IntBlockPool;->buffers:[[I

    iget v4, p0, Lorg/apache/lucene/util/IntBlockPool;->bufferUpto:I

    add-int/lit8 v4, v4, 0x1

    invoke-static {v3, v1, v4, v7}, Ljava/util/Arrays;->fill([Ljava/lang/Object;IILjava/lang/Object;)V

    .line 128
    .end local v1    # "offset":I
    :cond_2
    if-eqz p2, :cond_6

    .line 130
    iput v2, p0, Lorg/apache/lucene/util/IntBlockPool;->bufferUpto:I

    .line 131
    iput v2, p0, Lorg/apache/lucene/util/IntBlockPool;->intUpto:I

    .line 132
    iput v2, p0, Lorg/apache/lucene/util/IntBlockPool;->intOffset:I

    .line 133
    iget-object v3, p0, Lorg/apache/lucene/util/IntBlockPool;->buffers:[[I

    aget-object v2, v3, v2

    iput-object v2, p0, Lorg/apache/lucene/util/IntBlockPool;->buffer:[I

    .line 141
    :cond_3
    :goto_2
    return-void

    .line 116
    .restart local v0    # "i":I
    :cond_4
    iget-object v3, p0, Lorg/apache/lucene/util/IntBlockPool;->buffers:[[I

    aget-object v3, v3, v0

    invoke-static {v3, v2}, Ljava/util/Arrays;->fill([II)V

    .line 114
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .end local v0    # "i":I
    :cond_5
    move v1, v2

    .line 123
    goto :goto_1

    .line 135
    :cond_6
    iput v6, p0, Lorg/apache/lucene/util/IntBlockPool;->bufferUpto:I

    .line 136
    const/16 v2, 0x2000

    iput v2, p0, Lorg/apache/lucene/util/IntBlockPool;->intUpto:I

    .line 137
    const/16 v2, -0x2000

    iput v2, p0, Lorg/apache/lucene/util/IntBlockPool;->intOffset:I

    .line 138
    iput-object v7, p0, Lorg/apache/lucene/util/IntBlockPool;->buffer:[I

    goto :goto_2
.end method
