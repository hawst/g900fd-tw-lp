.class public final Lorg/apache/lucene/util/DoubleBarrelLRUCache;
.super Ljava/lang/Object;
.source "DoubleBarrelLRUCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/DoubleBarrelLRUCache$CloneableKey;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Lorg/apache/lucene/util/DoubleBarrelLRUCache$CloneableKey;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final cache1:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field private final cache2:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field private final countdown:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final maxSize:I

.field private volatile swapped:Z


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "maxSize"    # I

    .prologue
    .line 59
    .local p0, "this":Lorg/apache/lucene/util/DoubleBarrelLRUCache;, "Lorg/apache/lucene/util/DoubleBarrelLRUCache<TK;TV;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput p1, p0, Lorg/apache/lucene/util/DoubleBarrelLRUCache;->maxSize:I

    .line 61
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, p1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/util/DoubleBarrelLRUCache;->countdown:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 62
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/util/DoubleBarrelLRUCache;->cache1:Ljava/util/Map;

    .line 63
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/util/DoubleBarrelLRUCache;->cache2:Ljava/util/Map;

    .line 64
    return-void
.end method


# virtual methods
.method public get(Lorg/apache/lucene/util/DoubleBarrelLRUCache$CloneableKey;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TV;"
        }
    .end annotation

    .prologue
    .line 70
    .local p0, "this":Lorg/apache/lucene/util/DoubleBarrelLRUCache;, "Lorg/apache/lucene/util/DoubleBarrelLRUCache<TK;TV;>;"
    .local p1, "key":Lorg/apache/lucene/util/DoubleBarrelLRUCache$CloneableKey;, "TK;"
    iget-boolean v3, p0, Lorg/apache/lucene/util/DoubleBarrelLRUCache;->swapped:Z

    if-eqz v3, :cond_1

    .line 71
    iget-object v0, p0, Lorg/apache/lucene/util/DoubleBarrelLRUCache;->cache2:Ljava/util/Map;

    .line 72
    .local v0, "primary":Ljava/util/Map;, "Ljava/util/Map<TK;TV;>;"
    iget-object v2, p0, Lorg/apache/lucene/util/DoubleBarrelLRUCache;->cache1:Ljava/util/Map;

    .line 79
    .local v2, "secondary":Ljava/util/Map;, "Ljava/util/Map<TK;TV;>;"
    :goto_0
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 80
    .local v1, "result":Ljava/lang/Object;, "TV;"
    if-nez v1, :cond_0

    .line 82
    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 83
    if-eqz v1, :cond_0

    .line 85
    invoke-virtual {p1}, Lorg/apache/lucene/util/DoubleBarrelLRUCache$CloneableKey;->clone()Lorg/apache/lucene/util/DoubleBarrelLRUCache$CloneableKey;

    move-result-object v3

    invoke-virtual {p0, v3, v1}, Lorg/apache/lucene/util/DoubleBarrelLRUCache;->put(Lorg/apache/lucene/util/DoubleBarrelLRUCache$CloneableKey;Ljava/lang/Object;)V

    .line 88
    :cond_0
    return-object v1

    .line 74
    .end local v0    # "primary":Ljava/util/Map;, "Ljava/util/Map<TK;TV;>;"
    .end local v1    # "result":Ljava/lang/Object;, "TV;"
    .end local v2    # "secondary":Ljava/util/Map;, "Ljava/util/Map<TK;TV;>;"
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/util/DoubleBarrelLRUCache;->cache1:Ljava/util/Map;

    .line 75
    .restart local v0    # "primary":Ljava/util/Map;, "Ljava/util/Map<TK;TV;>;"
    iget-object v2, p0, Lorg/apache/lucene/util/DoubleBarrelLRUCache;->cache2:Ljava/util/Map;

    .restart local v2    # "secondary":Ljava/util/Map;, "Ljava/util/Map<TK;TV;>;"
    goto :goto_0
.end method

.method public put(Lorg/apache/lucene/util/DoubleBarrelLRUCache$CloneableKey;Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)V"
        }
    .end annotation

    .prologue
    .line 94
    .local p0, "this":Lorg/apache/lucene/util/DoubleBarrelLRUCache;, "Lorg/apache/lucene/util/DoubleBarrelLRUCache<TK;TV;>;"
    .local p1, "key":Lorg/apache/lucene/util/DoubleBarrelLRUCache$CloneableKey;, "TK;"
    .local p2, "value":Ljava/lang/Object;, "TV;"
    iget-boolean v2, p0, Lorg/apache/lucene/util/DoubleBarrelLRUCache;->swapped:Z

    if-eqz v2, :cond_1

    .line 95
    iget-object v0, p0, Lorg/apache/lucene/util/DoubleBarrelLRUCache;->cache2:Ljava/util/Map;

    .line 96
    .local v0, "primary":Ljava/util/Map;, "Ljava/util/Map<TK;TV;>;"
    iget-object v1, p0, Lorg/apache/lucene/util/DoubleBarrelLRUCache;->cache1:Ljava/util/Map;

    .line 101
    .local v1, "secondary":Ljava/util/Map;, "Ljava/util/Map<TK;TV;>;"
    :goto_0
    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    iget-object v2, p0, Lorg/apache/lucene/util/DoubleBarrelLRUCache;->countdown:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v2

    if-nez v2, :cond_0

    .line 113
    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 116
    iget-boolean v2, p0, Lorg/apache/lucene/util/DoubleBarrelLRUCache;->swapped:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x0

    :goto_1
    iput-boolean v2, p0, Lorg/apache/lucene/util/DoubleBarrelLRUCache;->swapped:Z

    .line 119
    iget-object v2, p0, Lorg/apache/lucene/util/DoubleBarrelLRUCache;->countdown:Ljava/util/concurrent/atomic/AtomicInteger;

    iget v3, p0, Lorg/apache/lucene/util/DoubleBarrelLRUCache;->maxSize:I

    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 121
    :cond_0
    return-void

    .line 98
    .end local v0    # "primary":Ljava/util/Map;, "Ljava/util/Map<TK;TV;>;"
    .end local v1    # "secondary":Ljava/util/Map;, "Ljava/util/Map<TK;TV;>;"
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/util/DoubleBarrelLRUCache;->cache1:Ljava/util/Map;

    .line 99
    .restart local v0    # "primary":Ljava/util/Map;, "Ljava/util/Map<TK;TV;>;"
    iget-object v1, p0, Lorg/apache/lucene/util/DoubleBarrelLRUCache;->cache2:Ljava/util/Map;

    .restart local v1    # "secondary":Ljava/util/Map;, "Ljava/util/Map<TK;TV;>;"
    goto :goto_0

    .line 116
    :cond_2
    const/4 v2, 0x1

    goto :goto_1
.end method
