.class public abstract Lorg/apache/lucene/util/AttributeSource$AttributeFactory;
.super Ljava/lang/Object;
.source "AttributeSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/AttributeSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "AttributeFactory"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/AttributeSource$AttributeFactory$DefaultAttributeFactory;
    }
.end annotation


# static fields
.field public static final DEFAULT_ATTRIBUTE_FACTORY:Lorg/apache/lucene/util/AttributeSource$AttributeFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    new-instance v0, Lorg/apache/lucene/util/AttributeSource$AttributeFactory$DefaultAttributeFactory;

    invoke-direct {v0}, Lorg/apache/lucene/util/AttributeSource$AttributeFactory$DefaultAttributeFactory;-><init>()V

    sput-object v0, Lorg/apache/lucene/util/AttributeSource$AttributeFactory;->DEFAULT_ATTRIBUTE_FACTORY:Lorg/apache/lucene/util/AttributeSource$AttributeFactory;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract createAttributeInstance(Ljava/lang/Class;)Lorg/apache/lucene/util/AttributeImpl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lorg/apache/lucene/util/Attribute;",
            ">;)",
            "Lorg/apache/lucene/util/AttributeImpl;"
        }
    .end annotation
.end method
