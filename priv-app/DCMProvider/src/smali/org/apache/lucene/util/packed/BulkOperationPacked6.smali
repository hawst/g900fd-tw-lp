.class final Lorg/apache/lucene/util/packed/BulkOperationPacked6;
.super Lorg/apache/lucene/util/packed/BulkOperationPacked;
.source "BulkOperationPacked6.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lorg/apache/lucene/util/packed/BulkOperationPacked;-><init>(I)V

    .line 29
    return-void
.end method


# virtual methods
.method public decode([BI[III)V
    .locals 8
    .param p1, "blocks"    # [B
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [I
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 74
    const/4 v4, 0x0

    .local v4, "i":I
    move v5, p4

    .end local p4    # "valuesOffset":I
    .local v5, "valuesOffset":I
    move v0, p2

    .end local p2    # "blocksOffset":I
    .local v0, "blocksOffset":I
    :goto_0
    if-lt v4, p5, :cond_0

    .line 83
    return-void

    .line 75
    :cond_0
    add-int/lit8 p2, v0, 0x1

    .end local v0    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v6, p1, v0

    and-int/lit16 v1, v6, 0xff

    .line 76
    .local v1, "byte0":I
    add-int/lit8 p4, v5, 0x1

    .end local v5    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    ushr-int/lit8 v6, v1, 0x2

    aput v6, p3, v5

    .line 77
    add-int/lit8 v0, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v0    # "blocksOffset":I
    aget-byte v6, p1, p2

    and-int/lit16 v2, v6, 0xff

    .line 78
    .local v2, "byte1":I
    add-int/lit8 v5, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v5    # "valuesOffset":I
    and-int/lit8 v6, v1, 0x3

    shl-int/lit8 v6, v6, 0x4

    ushr-int/lit8 v7, v2, 0x4

    or-int/2addr v6, v7

    aput v6, p3, p4

    .line 79
    add-int/lit8 p2, v0, 0x1

    .end local v0    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v6, p1, v0

    and-int/lit16 v3, v6, 0xff

    .line 80
    .local v3, "byte2":I
    add-int/lit8 p4, v5, 0x1

    .end local v5    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    and-int/lit8 v6, v2, 0xf

    shl-int/lit8 v6, v6, 0x2

    ushr-int/lit8 v7, v3, 0x6

    or-int/2addr v6, v7

    aput v6, p3, v5

    .line 81
    add-int/lit8 v5, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v5    # "valuesOffset":I
    and-int/lit8 v6, v3, 0x3f

    aput v6, p3, p4

    .line 74
    add-int/lit8 v4, v4, 0x1

    move v0, p2

    .end local p2    # "blocksOffset":I
    .restart local v0    # "blocksOffset":I
    goto :goto_0
.end method

.method public decode([BI[JII)V
    .locals 16
    .param p1, "blocks"    # [B
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [J
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 128
    const/4 v3, 0x0

    .local v3, "i":I
    move/from16 v10, p4

    .end local p4    # "valuesOffset":I
    .local v10, "valuesOffset":I
    move/from16 v2, p2

    .end local p2    # "blocksOffset":I
    .local v2, "blocksOffset":I
    :goto_0
    move/from16 v0, p5

    if-lt v3, v0, :cond_0

    .line 137
    return-void

    .line 129
    :cond_0
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v11, p1, v2

    and-int/lit16 v11, v11, 0xff

    int-to-long v4, v11

    .line 130
    .local v4, "byte0":J
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/4 v11, 0x2

    ushr-long v12, v4, v11

    aput-wide v12, p3, v10

    .line 131
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v11, p1, p2

    and-int/lit16 v11, v11, 0xff

    int-to-long v6, v11

    .line 132
    .local v6, "byte1":J
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const-wide/16 v12, 0x3

    and-long/2addr v12, v4

    const/4 v11, 0x4

    shl-long/2addr v12, v11

    const/4 v11, 0x4

    ushr-long v14, v6, v11

    or-long/2addr v12, v14

    aput-wide v12, p3, p4

    .line 133
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v11, p1, v2

    and-int/lit16 v11, v11, 0xff

    int-to-long v8, v11

    .line 134
    .local v8, "byte2":J
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v12, 0xf

    and-long/2addr v12, v6

    const/4 v11, 0x2

    shl-long/2addr v12, v11

    const/4 v11, 0x6

    ushr-long v14, v8, v11

    or-long/2addr v12, v14

    aput-wide v12, p3, v10

    .line 135
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const-wide/16 v12, 0x3f

    and-long/2addr v12, v8

    aput-wide v12, p3, p4

    .line 128
    add-int/lit8 v3, v3, 0x1

    move/from16 v2, p2

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    goto :goto_0
.end method

.method public decode([JI[III)V
    .locals 16
    .param p1, "blocks"    # [J
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [I
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 33
    const/4 v9, 0x0

    .local v9, "i":I
    move/from16 v10, p4

    .end local p4    # "valuesOffset":I
    .local v10, "valuesOffset":I
    move/from16 v8, p2

    .end local p2    # "blocksOffset":I
    .local v8, "blocksOffset":I
    :goto_0
    move/from16 v0, p5

    if-lt v9, v0, :cond_0

    .line 70
    return-void

    .line 34
    :cond_0
    add-int/lit8 p2, v8, 0x1

    .end local v8    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v2, p1, v8

    .line 35
    .local v2, "block0":J
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x3a

    ushr-long v12, v2, v11

    long-to-int v11, v12

    aput v11, p3, v10

    .line 36
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x34

    ushr-long v12, v2, v11

    const-wide/16 v14, 0x3f

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, p4

    .line 37
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x2e

    ushr-long v12, v2, v11

    const-wide/16 v14, 0x3f

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, v10

    .line 38
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x28

    ushr-long v12, v2, v11

    const-wide/16 v14, 0x3f

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, p4

    .line 39
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x22

    ushr-long v12, v2, v11

    const-wide/16 v14, 0x3f

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, v10

    .line 40
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x1c

    ushr-long v12, v2, v11

    const-wide/16 v14, 0x3f

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, p4

    .line 41
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x16

    ushr-long v12, v2, v11

    const-wide/16 v14, 0x3f

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, v10

    .line 42
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x10

    ushr-long v12, v2, v11

    const-wide/16 v14, 0x3f

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, p4

    .line 43
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0xa

    ushr-long v12, v2, v11

    const-wide/16 v14, 0x3f

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, v10

    .line 44
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/4 v11, 0x4

    ushr-long v12, v2, v11

    const-wide/16 v14, 0x3f

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, p4

    .line 45
    add-int/lit8 v8, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v8    # "blocksOffset":I
    aget-wide v4, p1, p2

    .line 46
    .local v4, "block1":J
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v12, 0xf

    and-long/2addr v12, v2

    const/4 v11, 0x2

    shl-long/2addr v12, v11

    const/16 v11, 0x3e

    ushr-long v14, v4, v11

    or-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, v10

    .line 47
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x38

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x3f

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, p4

    .line 48
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x32

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x3f

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, v10

    .line 49
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x2c

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x3f

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, p4

    .line 50
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x26

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x3f

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, v10

    .line 51
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x20

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x3f

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, p4

    .line 52
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x1a

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x3f

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, v10

    .line 53
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x14

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x3f

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, p4

    .line 54
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0xe

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x3f

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, v10

    .line 55
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x8

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x3f

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, p4

    .line 56
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/4 v11, 0x2

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x3f

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, v10

    .line 57
    add-int/lit8 p2, v8, 0x1

    .end local v8    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v6, p1, v8

    .line 58
    .local v6, "block2":J
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const-wide/16 v12, 0x3

    and-long/2addr v12, v4

    const/4 v11, 0x4

    shl-long/2addr v12, v11

    const/16 v11, 0x3c

    ushr-long v14, v6, v11

    or-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, p4

    .line 59
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x36

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x3f

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, v10

    .line 60
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x30

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x3f

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, p4

    .line 61
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x2a

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x3f

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, v10

    .line 62
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x24

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x3f

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, p4

    .line 63
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x1e

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x3f

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, v10

    .line 64
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x18

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x3f

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, p4

    .line 65
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x12

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x3f

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, v10

    .line 66
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0xc

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x3f

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, p4

    .line 67
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/4 v11, 0x6

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x3f

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, v10

    .line 68
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const-wide/16 v12, 0x3f

    and-long/2addr v12, v6

    long-to-int v11, v12

    aput v11, p3, p4

    .line 33
    add-int/lit8 v9, v9, 0x1

    move/from16 v8, p2

    .end local p2    # "blocksOffset":I
    .restart local v8    # "blocksOffset":I
    goto/16 :goto_0
.end method

.method public decode([JI[JII)V
    .locals 16
    .param p1, "blocks"    # [J
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [J
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 87
    const/4 v9, 0x0

    .local v9, "i":I
    move/from16 v10, p4

    .end local p4    # "valuesOffset":I
    .local v10, "valuesOffset":I
    move/from16 v8, p2

    .end local p2    # "blocksOffset":I
    .local v8, "blocksOffset":I
    :goto_0
    move/from16 v0, p5

    if-lt v9, v0, :cond_0

    .line 124
    return-void

    .line 88
    :cond_0
    add-int/lit8 p2, v8, 0x1

    .end local v8    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v2, p1, v8

    .line 89
    .local v2, "block0":J
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x3a

    ushr-long v12, v2, v11

    aput-wide v12, p3, v10

    .line 90
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x34

    ushr-long v12, v2, v11

    const-wide/16 v14, 0x3f

    and-long/2addr v12, v14

    aput-wide v12, p3, p4

    .line 91
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x2e

    ushr-long v12, v2, v11

    const-wide/16 v14, 0x3f

    and-long/2addr v12, v14

    aput-wide v12, p3, v10

    .line 92
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x28

    ushr-long v12, v2, v11

    const-wide/16 v14, 0x3f

    and-long/2addr v12, v14

    aput-wide v12, p3, p4

    .line 93
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x22

    ushr-long v12, v2, v11

    const-wide/16 v14, 0x3f

    and-long/2addr v12, v14

    aput-wide v12, p3, v10

    .line 94
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x1c

    ushr-long v12, v2, v11

    const-wide/16 v14, 0x3f

    and-long/2addr v12, v14

    aput-wide v12, p3, p4

    .line 95
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x16

    ushr-long v12, v2, v11

    const-wide/16 v14, 0x3f

    and-long/2addr v12, v14

    aput-wide v12, p3, v10

    .line 96
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x10

    ushr-long v12, v2, v11

    const-wide/16 v14, 0x3f

    and-long/2addr v12, v14

    aput-wide v12, p3, p4

    .line 97
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0xa

    ushr-long v12, v2, v11

    const-wide/16 v14, 0x3f

    and-long/2addr v12, v14

    aput-wide v12, p3, v10

    .line 98
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/4 v11, 0x4

    ushr-long v12, v2, v11

    const-wide/16 v14, 0x3f

    and-long/2addr v12, v14

    aput-wide v12, p3, p4

    .line 99
    add-int/lit8 v8, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v8    # "blocksOffset":I
    aget-wide v4, p1, p2

    .line 100
    .local v4, "block1":J
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v12, 0xf

    and-long/2addr v12, v2

    const/4 v11, 0x2

    shl-long/2addr v12, v11

    const/16 v11, 0x3e

    ushr-long v14, v4, v11

    or-long/2addr v12, v14

    aput-wide v12, p3, v10

    .line 101
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x38

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x3f

    and-long/2addr v12, v14

    aput-wide v12, p3, p4

    .line 102
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x32

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x3f

    and-long/2addr v12, v14

    aput-wide v12, p3, v10

    .line 103
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x2c

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x3f

    and-long/2addr v12, v14

    aput-wide v12, p3, p4

    .line 104
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x26

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x3f

    and-long/2addr v12, v14

    aput-wide v12, p3, v10

    .line 105
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x20

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x3f

    and-long/2addr v12, v14

    aput-wide v12, p3, p4

    .line 106
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x1a

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x3f

    and-long/2addr v12, v14

    aput-wide v12, p3, v10

    .line 107
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x14

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x3f

    and-long/2addr v12, v14

    aput-wide v12, p3, p4

    .line 108
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0xe

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x3f

    and-long/2addr v12, v14

    aput-wide v12, p3, v10

    .line 109
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x8

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x3f

    and-long/2addr v12, v14

    aput-wide v12, p3, p4

    .line 110
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/4 v11, 0x2

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x3f

    and-long/2addr v12, v14

    aput-wide v12, p3, v10

    .line 111
    add-int/lit8 p2, v8, 0x1

    .end local v8    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v6, p1, v8

    .line 112
    .local v6, "block2":J
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const-wide/16 v12, 0x3

    and-long/2addr v12, v4

    const/4 v11, 0x4

    shl-long/2addr v12, v11

    const/16 v11, 0x3c

    ushr-long v14, v6, v11

    or-long/2addr v12, v14

    aput-wide v12, p3, p4

    .line 113
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x36

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x3f

    and-long/2addr v12, v14

    aput-wide v12, p3, v10

    .line 114
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x30

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x3f

    and-long/2addr v12, v14

    aput-wide v12, p3, p4

    .line 115
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x2a

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x3f

    and-long/2addr v12, v14

    aput-wide v12, p3, v10

    .line 116
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x24

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x3f

    and-long/2addr v12, v14

    aput-wide v12, p3, p4

    .line 117
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x1e

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x3f

    and-long/2addr v12, v14

    aput-wide v12, p3, v10

    .line 118
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x18

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x3f

    and-long/2addr v12, v14

    aput-wide v12, p3, p4

    .line 119
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x12

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x3f

    and-long/2addr v12, v14

    aput-wide v12, p3, v10

    .line 120
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0xc

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x3f

    and-long/2addr v12, v14

    aput-wide v12, p3, p4

    .line 121
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/4 v11, 0x6

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x3f

    and-long/2addr v12, v14

    aput-wide v12, p3, v10

    .line 122
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const-wide/16 v12, 0x3f

    and-long/2addr v12, v6

    aput-wide v12, p3, p4

    .line 87
    add-int/lit8 v9, v9, 0x1

    move/from16 v8, p2

    .end local p2    # "blocksOffset":I
    .restart local v8    # "blocksOffset":I
    goto/16 :goto_0
.end method
