.class Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet$1;
.super Ljava/lang/Object;
.source "RamUsageEstimator.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;->iterator()Ljava/util/Iterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TKType;>;"
    }
.end annotation


# instance fields
.field nextElement:Ljava/lang/Object;

.field pos:I

.field final synthetic this$1:Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;


# direct methods
.method constructor <init>(Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet$1;->this$1:Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;

    .line 799
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 800
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet$1;->pos:I

    .line 801
    invoke-direct {p0}, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet$1;->fetchNext()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet$1;->nextElement:Ljava/lang/Object;

    return-void
.end method

.method private fetchNext()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 820
    iget v0, p0, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet$1;->pos:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet$1;->pos:I

    .line 821
    :goto_0
    iget v0, p0, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet$1;->pos:I

    iget-object v1, p0, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet$1;->this$1:Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;

    iget-object v1, v1, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;->keys:[Ljava/lang/Object;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet$1;->this$1:Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;

    iget-object v0, v0, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;->keys:[Ljava/lang/Object;

    iget v1, p0, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet$1;->pos:I

    aget-object v0, v0, v1

    if-eqz v0, :cond_1

    .line 825
    :cond_0
    iget v0, p0, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet$1;->pos:I

    iget-object v1, p0, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet$1;->this$1:Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;

    iget-object v1, v1, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;->keys:[Ljava/lang/Object;

    array-length v1, v1

    if-lt v0, v1, :cond_2

    const/4 v0, 0x0

    :goto_1
    return-object v0

    .line 822
    :cond_1
    iget v0, p0, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet$1;->pos:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet$1;->pos:I

    goto :goto_0

    .line 825
    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet$1;->this$1:Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;

    iget-object v0, v0, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;->keys:[Ljava/lang/Object;

    iget v1, p0, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet$1;->pos:I

    aget-object v0, v0, v1

    goto :goto_1
.end method


# virtual methods
.method public hasNext()Z
    .locals 1

    .prologue
    .line 805
    iget-object v0, p0, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet$1;->nextElement:Ljava/lang/Object;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TKType;"
        }
    .end annotation

    .prologue
    .line 811
    iget-object v0, p0, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet$1;->nextElement:Ljava/lang/Object;

    .line 812
    .local v0, "r":Ljava/lang/Object;
    if-nez v0, :cond_0

    .line 813
    new-instance v1, Ljava/util/NoSuchElementException;

    invoke-direct {v1}, Ljava/util/NoSuchElementException;-><init>()V

    throw v1

    .line 815
    :cond_0
    invoke-direct {p0}, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet$1;->fetchNext()Ljava/lang/Object;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet$1;->nextElement:Ljava/lang/Object;

    .line 816
    return-object v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 830
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
