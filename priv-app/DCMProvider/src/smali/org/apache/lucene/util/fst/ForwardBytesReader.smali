.class final Lorg/apache/lucene/util/fst/ForwardBytesReader;
.super Lorg/apache/lucene/util/fst/FST$BytesReader;
.source "ForwardBytesReader.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/util/fst/FST$BytesReader;"
    }
.end annotation


# instance fields
.field private final bytes:[B

.field private pos:I


# direct methods
.method public constructor <init>([B)V
    .locals 0
    .param p1, "bytes"    # [B

    .prologue
    .line 28
    invoke-direct {p0}, Lorg/apache/lucene/util/fst/FST$BytesReader;-><init>()V

    .line 29
    iput-object p1, p0, Lorg/apache/lucene/util/fst/ForwardBytesReader;->bytes:[B

    .line 30
    return-void
.end method


# virtual methods
.method public getPosition()J
    .locals 2

    .prologue
    .line 50
    iget v0, p0, Lorg/apache/lucene/util/fst/ForwardBytesReader;->pos:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public readByte()B
    .locals 3

    .prologue
    .line 34
    iget-object v0, p0, Lorg/apache/lucene/util/fst/ForwardBytesReader;->bytes:[B

    iget v1, p0, Lorg/apache/lucene/util/fst/ForwardBytesReader;->pos:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/util/fst/ForwardBytesReader;->pos:I

    aget-byte v0, v0, v1

    return v0
.end method

.method public readBytes([BII)V
    .locals 2
    .param p1, "b"    # [B
    .param p2, "offset"    # I
    .param p3, "len"    # I

    .prologue
    .line 39
    iget-object v0, p0, Lorg/apache/lucene/util/fst/ForwardBytesReader;->bytes:[B

    iget v1, p0, Lorg/apache/lucene/util/fst/ForwardBytesReader;->pos:I

    invoke-static {v0, v1, p1, p2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 40
    iget v0, p0, Lorg/apache/lucene/util/fst/ForwardBytesReader;->pos:I

    add-int/2addr v0, p3

    iput v0, p0, Lorg/apache/lucene/util/fst/ForwardBytesReader;->pos:I

    .line 41
    return-void
.end method

.method public reversed()Z
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    return v0
.end method

.method public setPosition(J)V
    .locals 1
    .param p1, "pos"    # J

    .prologue
    .line 55
    long-to-int v0, p1

    iput v0, p0, Lorg/apache/lucene/util/fst/ForwardBytesReader;->pos:I

    .line 56
    return-void
.end method

.method public skipBytes(I)V
    .locals 1
    .param p1, "count"    # I

    .prologue
    .line 45
    iget v0, p0, Lorg/apache/lucene/util/fst/ForwardBytesReader;->pos:I

    add-int/2addr v0, p1

    iput v0, p0, Lorg/apache/lucene/util/fst/ForwardBytesReader;->pos:I

    .line 46
    return-void
.end method
