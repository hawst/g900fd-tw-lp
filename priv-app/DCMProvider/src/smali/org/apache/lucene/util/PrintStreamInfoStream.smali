.class public Lorg/apache/lucene/util/PrintStreamInfoStream;
.super Lorg/apache/lucene/util/InfoStream;
.source "PrintStreamInfoStream.java"


# static fields
.field private static final MESSAGE_ID:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field protected final messageID:I

.field protected final stream:Ljava/io/PrintStream;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    sput-object v0, Lorg/apache/lucene/util/PrintStreamInfoStream;->MESSAGE_ID:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>(Ljava/io/PrintStream;)V
    .locals 1
    .param p1, "stream"    # Ljava/io/PrintStream;

    .prologue
    .line 39
    sget-object v0, Lorg/apache/lucene/util/PrintStreamInfoStream;->MESSAGE_ID:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/util/PrintStreamInfoStream;-><init>(Ljava/io/PrintStream;I)V

    .line 40
    return-void
.end method

.method public constructor <init>(Ljava/io/PrintStream;I)V
    .locals 0
    .param p1, "stream"    # Ljava/io/PrintStream;
    .param p2, "messageID"    # I

    .prologue
    .line 42
    invoke-direct {p0}, Lorg/apache/lucene/util/InfoStream;-><init>()V

    .line 43
    iput-object p1, p0, Lorg/apache/lucene/util/PrintStreamInfoStream;->stream:Ljava/io/PrintStream;

    .line 44
    iput p2, p0, Lorg/apache/lucene/util/PrintStreamInfoStream;->messageID:I

    .line 45
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 59
    invoke-virtual {p0}, Lorg/apache/lucene/util/PrintStreamInfoStream;->isSystemStream()Z

    move-result v0

    if-nez v0, :cond_0

    .line 60
    iget-object v0, p0, Lorg/apache/lucene/util/PrintStreamInfoStream;->stream:Ljava/io/PrintStream;

    invoke-virtual {v0}, Ljava/io/PrintStream;->close()V

    .line 62
    :cond_0
    return-void
.end method

.method public isEnabled(Ljava/lang/String;)Z
    .locals 1
    .param p1, "component"    # Ljava/lang/String;

    .prologue
    .line 54
    const/4 v0, 0x1

    return v0
.end method

.method public isSystemStream()Z
    .locals 2

    .prologue
    .line 65
    iget-object v0, p0, Lorg/apache/lucene/util/PrintStreamInfoStream;->stream:Ljava/io/PrintStream;

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/util/PrintStreamInfoStream;->stream:Ljava/io/PrintStream;

    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public message(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "component"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 49
    iget-object v0, p0, Lorg/apache/lucene/util/PrintStreamInfoStream;->stream:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/util/PrintStreamInfoStream;->messageID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 50
    return-void
.end method
