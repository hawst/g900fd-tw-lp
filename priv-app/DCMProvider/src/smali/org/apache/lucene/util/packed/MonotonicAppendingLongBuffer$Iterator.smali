.class public final Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer$Iterator;
.super Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer$Iterator;
.source "MonotonicAppendingLongBuffer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "Iterator"
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;


# direct methods
.method constructor <init>(Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;)V
    .locals 0

    .prologue
    .line 104
    iput-object p1, p0, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer$Iterator;->this$0:Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;

    .line 105
    invoke-direct {p0, p1}, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer$Iterator;-><init>(Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;)V

    .line 106
    return-void
.end method


# virtual methods
.method fillValues()V
    .locals 9

    .prologue
    const/16 v8, 0x400

    .line 109
    iget v1, p0, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer$Iterator;->vOff:I

    iget-object v2, p0, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer$Iterator;->this$0:Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;

    iget v2, v2, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;->valuesOff:I

    if-ne v1, v2, :cond_1

    .line 110
    iget-object v1, p0, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer$Iterator;->this$0:Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;

    iget-object v1, v1, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;->pending:[J

    iput-object v1, p0, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer$Iterator;->currentValues:[J

    .line 123
    :cond_0
    return-void

    .line 111
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer$Iterator;->this$0:Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;

    iget-object v1, v1, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;->deltas:[Lorg/apache/lucene/util/packed/PackedInts$Reader;

    iget v2, p0, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer$Iterator;->vOff:I

    aget-object v1, v1, v2

    if-nez v1, :cond_2

    .line 112
    const/4 v0, 0x0

    .local v0, "k":I
    :goto_0
    if-ge v0, v8, :cond_0

    .line 113
    iget-object v1, p0, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer$Iterator;->currentValues:[J

    iget-object v2, p0, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer$Iterator;->this$0:Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;

    iget-object v2, v2, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;->minValues:[J

    iget v3, p0, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer$Iterator;->vOff:I

    aget-wide v2, v2, v3

    iget-object v4, p0, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer$Iterator;->this$0:Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;

    # getter for: Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;->averages:[F
    invoke-static {v4}, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;->access$1(Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;)[F

    move-result-object v4

    iget v5, p0, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer$Iterator;->vOff:I

    aget v4, v4, v5

    int-to-long v6, v0

    long-to-float v5, v6

    mul-float/2addr v4, v5

    float-to-long v4, v4

    add-long/2addr v2, v4

    aput-wide v2, v1, v0

    .line 112
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 116
    .end local v0    # "k":I
    :cond_2
    const/4 v0, 0x0

    .restart local v0    # "k":I
    :goto_1
    if-lt v0, v8, :cond_3

    .line 119
    const/4 v0, 0x0

    :goto_2
    if-ge v0, v8, :cond_0

    .line 120
    iget-object v1, p0, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer$Iterator;->currentValues:[J

    iget-object v2, p0, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer$Iterator;->this$0:Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;

    iget-object v2, v2, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;->minValues:[J

    iget v3, p0, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer$Iterator;->vOff:I

    aget-wide v2, v2, v3

    iget-object v4, p0, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer$Iterator;->this$0:Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;

    # getter for: Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;->averages:[F
    invoke-static {v4}, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;->access$1(Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;)[F

    move-result-object v4

    iget v5, p0, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer$Iterator;->vOff:I

    aget v4, v4, v5

    int-to-long v6, v0

    long-to-float v5, v6

    mul-float/2addr v4, v5

    float-to-long v4, v4

    add-long/2addr v2, v4

    iget-object v4, p0, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer$Iterator;->currentValues:[J

    aget-wide v4, v4, v0

    invoke-static {v4, v5}, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;->zigZagDecode(J)J

    move-result-wide v4

    add-long/2addr v2, v4

    aput-wide v2, v1, v0

    .line 119
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 117
    :cond_3
    iget-object v1, p0, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer$Iterator;->this$0:Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;

    iget-object v1, v1, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;->deltas:[Lorg/apache/lucene/util/packed/PackedInts$Reader;

    iget v2, p0, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer$Iterator;->vOff:I

    aget-object v1, v1, v2

    iget-object v2, p0, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer$Iterator;->currentValues:[J

    rsub-int v3, v0, 0x400

    invoke-interface {v1, v0, v2, v0, v3}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I[JII)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_1
.end method
