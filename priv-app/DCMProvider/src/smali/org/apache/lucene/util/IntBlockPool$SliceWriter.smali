.class public Lorg/apache/lucene/util/IntBlockPool$SliceWriter;
.super Ljava/lang/Object;
.source "IntBlockPool.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/IntBlockPool;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SliceWriter"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private offset:I

.field private final pool:Lorg/apache/lucene/util/IntBlockPool;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 236
    const-class v0, Lorg/apache/lucene/util/IntBlockPool;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/IntBlockPool$SliceWriter;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/util/IntBlockPool;)V
    .locals 0
    .param p1, "pool"    # Lorg/apache/lucene/util/IntBlockPool;

    .prologue
    .line 242
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 243
    iput-object p1, p0, Lorg/apache/lucene/util/IntBlockPool$SliceWriter;->pool:Lorg/apache/lucene/util/IntBlockPool;

    .line 244
    return-void
.end method


# virtual methods
.method public getCurrentOffset()I
    .locals 1

    .prologue
    .line 285
    iget v0, p0, Lorg/apache/lucene/util/IntBlockPool$SliceWriter;->offset:I

    return v0
.end method

.method public reset(I)V
    .locals 0
    .param p1, "sliceOffset"    # I

    .prologue
    .line 249
    iput p1, p0, Lorg/apache/lucene/util/IntBlockPool$SliceWriter;->offset:I

    .line 250
    return-void
.end method

.method public startNewSlice()I
    .locals 2

    .prologue
    .line 274
    iget-object v0, p0, Lorg/apache/lucene/util/IntBlockPool$SliceWriter;->pool:Lorg/apache/lucene/util/IntBlockPool;

    # getter for: Lorg/apache/lucene/util/IntBlockPool;->FIRST_LEVEL_SIZE:I
    invoke-static {}, Lorg/apache/lucene/util/IntBlockPool;->access$1()I

    move-result v1

    # invokes: Lorg/apache/lucene/util/IntBlockPool;->newSlice(I)I
    invoke-static {v0, v1}, Lorg/apache/lucene/util/IntBlockPool;->access$2(Lorg/apache/lucene/util/IntBlockPool;I)I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/util/IntBlockPool$SliceWriter;->pool:Lorg/apache/lucene/util/IntBlockPool;

    iget v1, v1, Lorg/apache/lucene/util/IntBlockPool;->intOffset:I

    add-int/2addr v0, v1

    iput v0, p0, Lorg/apache/lucene/util/IntBlockPool$SliceWriter;->offset:I

    return v0
.end method

.method public writeInt(I)V
    .locals 4
    .param p1, "value"    # I

    .prologue
    .line 256
    iget-object v2, p0, Lorg/apache/lucene/util/IntBlockPool$SliceWriter;->pool:Lorg/apache/lucene/util/IntBlockPool;

    iget-object v2, v2, Lorg/apache/lucene/util/IntBlockPool;->buffers:[[I

    iget v3, p0, Lorg/apache/lucene/util/IntBlockPool$SliceWriter;->offset:I

    shr-int/lit8 v3, v3, 0xd

    aget-object v0, v2, v3

    .line 257
    .local v0, "ints":[I
    sget-boolean v2, Lorg/apache/lucene/util/IntBlockPool$SliceWriter;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    if-nez v0, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 258
    :cond_0
    iget v2, p0, Lorg/apache/lucene/util/IntBlockPool$SliceWriter;->offset:I

    and-int/lit16 v1, v2, 0x1fff

    .line 259
    .local v1, "relativeOffset":I
    aget v2, v0, v1

    if-eqz v2, :cond_1

    .line 261
    iget-object v2, p0, Lorg/apache/lucene/util/IntBlockPool$SliceWriter;->pool:Lorg/apache/lucene/util/IntBlockPool;

    # invokes: Lorg/apache/lucene/util/IntBlockPool;->allocSlice([II)I
    invoke-static {v2, v0, v1}, Lorg/apache/lucene/util/IntBlockPool;->access$0(Lorg/apache/lucene/util/IntBlockPool;[II)I

    move-result v1

    .line 262
    iget-object v2, p0, Lorg/apache/lucene/util/IntBlockPool$SliceWriter;->pool:Lorg/apache/lucene/util/IntBlockPool;

    iget-object v0, v2, Lorg/apache/lucene/util/IntBlockPool;->buffer:[I

    .line 263
    iget-object v2, p0, Lorg/apache/lucene/util/IntBlockPool$SliceWriter;->pool:Lorg/apache/lucene/util/IntBlockPool;

    iget v2, v2, Lorg/apache/lucene/util/IntBlockPool;->intOffset:I

    add-int/2addr v2, v1

    iput v2, p0, Lorg/apache/lucene/util/IntBlockPool$SliceWriter;->offset:I

    .line 265
    :cond_1
    aput p1, v0, v1

    .line 266
    iget v2, p0, Lorg/apache/lucene/util/IntBlockPool$SliceWriter;->offset:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/lucene/util/IntBlockPool$SliceWriter;->offset:I

    .line 267
    return-void
.end method
