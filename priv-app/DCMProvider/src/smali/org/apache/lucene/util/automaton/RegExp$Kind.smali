.class final enum Lorg/apache/lucene/util/automaton/RegExp$Kind;
.super Ljava/lang/Enum;
.source "RegExp.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/automaton/RegExp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "Kind"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/lucene/util/automaton/RegExp$Kind;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lorg/apache/lucene/util/automaton/RegExp$Kind;

.field public static final enum REGEXP_ANYCHAR:Lorg/apache/lucene/util/automaton/RegExp$Kind;

.field public static final enum REGEXP_ANYSTRING:Lorg/apache/lucene/util/automaton/RegExp$Kind;

.field public static final enum REGEXP_AUTOMATON:Lorg/apache/lucene/util/automaton/RegExp$Kind;

.field public static final enum REGEXP_CHAR:Lorg/apache/lucene/util/automaton/RegExp$Kind;

.field public static final enum REGEXP_CHAR_RANGE:Lorg/apache/lucene/util/automaton/RegExp$Kind;

.field public static final enum REGEXP_COMPLEMENT:Lorg/apache/lucene/util/automaton/RegExp$Kind;

.field public static final enum REGEXP_CONCATENATION:Lorg/apache/lucene/util/automaton/RegExp$Kind;

.field public static final enum REGEXP_EMPTY:Lorg/apache/lucene/util/automaton/RegExp$Kind;

.field public static final enum REGEXP_INTERSECTION:Lorg/apache/lucene/util/automaton/RegExp$Kind;

.field public static final enum REGEXP_INTERVAL:Lorg/apache/lucene/util/automaton/RegExp$Kind;

.field public static final enum REGEXP_OPTIONAL:Lorg/apache/lucene/util/automaton/RegExp$Kind;

.field public static final enum REGEXP_REPEAT:Lorg/apache/lucene/util/automaton/RegExp$Kind;

.field public static final enum REGEXP_REPEAT_MIN:Lorg/apache/lucene/util/automaton/RegExp$Kind;

.field public static final enum REGEXP_REPEAT_MINMAX:Lorg/apache/lucene/util/automaton/RegExp$Kind;

.field public static final enum REGEXP_STRING:Lorg/apache/lucene/util/automaton/RegExp$Kind;

.field public static final enum REGEXP_UNION:Lorg/apache/lucene/util/automaton/RegExp$Kind;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 320
    new-instance v0, Lorg/apache/lucene/util/automaton/RegExp$Kind;

    const-string v1, "REGEXP_UNION"

    invoke-direct {v0, v1, v3}, Lorg/apache/lucene/util/automaton/RegExp$Kind;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_UNION:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    new-instance v0, Lorg/apache/lucene/util/automaton/RegExp$Kind;

    const-string v1, "REGEXP_CONCATENATION"

    invoke-direct {v0, v1, v4}, Lorg/apache/lucene/util/automaton/RegExp$Kind;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_CONCATENATION:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    new-instance v0, Lorg/apache/lucene/util/automaton/RegExp$Kind;

    const-string v1, "REGEXP_INTERSECTION"

    invoke-direct {v0, v1, v5}, Lorg/apache/lucene/util/automaton/RegExp$Kind;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_INTERSECTION:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    new-instance v0, Lorg/apache/lucene/util/automaton/RegExp$Kind;

    const-string v1, "REGEXP_OPTIONAL"

    invoke-direct {v0, v1, v6}, Lorg/apache/lucene/util/automaton/RegExp$Kind;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_OPTIONAL:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    new-instance v0, Lorg/apache/lucene/util/automaton/RegExp$Kind;

    const-string v1, "REGEXP_REPEAT"

    invoke-direct {v0, v1, v7}, Lorg/apache/lucene/util/automaton/RegExp$Kind;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_REPEAT:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    new-instance v0, Lorg/apache/lucene/util/automaton/RegExp$Kind;

    const-string v1, "REGEXP_REPEAT_MIN"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/util/automaton/RegExp$Kind;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_REPEAT_MIN:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    new-instance v0, Lorg/apache/lucene/util/automaton/RegExp$Kind;

    const-string v1, "REGEXP_REPEAT_MINMAX"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/util/automaton/RegExp$Kind;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_REPEAT_MINMAX:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    new-instance v0, Lorg/apache/lucene/util/automaton/RegExp$Kind;

    const-string v1, "REGEXP_COMPLEMENT"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/util/automaton/RegExp$Kind;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_COMPLEMENT:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    new-instance v0, Lorg/apache/lucene/util/automaton/RegExp$Kind;

    const-string v1, "REGEXP_CHAR"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/util/automaton/RegExp$Kind;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_CHAR:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    new-instance v0, Lorg/apache/lucene/util/automaton/RegExp$Kind;

    const-string v1, "REGEXP_CHAR_RANGE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/util/automaton/RegExp$Kind;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_CHAR_RANGE:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    new-instance v0, Lorg/apache/lucene/util/automaton/RegExp$Kind;

    const-string v1, "REGEXP_ANYCHAR"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/util/automaton/RegExp$Kind;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_ANYCHAR:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    new-instance v0, Lorg/apache/lucene/util/automaton/RegExp$Kind;

    const-string v1, "REGEXP_EMPTY"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/util/automaton/RegExp$Kind;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_EMPTY:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    new-instance v0, Lorg/apache/lucene/util/automaton/RegExp$Kind;

    const-string v1, "REGEXP_STRING"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/util/automaton/RegExp$Kind;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_STRING:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    new-instance v0, Lorg/apache/lucene/util/automaton/RegExp$Kind;

    const-string v1, "REGEXP_ANYSTRING"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/util/automaton/RegExp$Kind;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_ANYSTRING:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    new-instance v0, Lorg/apache/lucene/util/automaton/RegExp$Kind;

    const-string v1, "REGEXP_AUTOMATON"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/util/automaton/RegExp$Kind;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_AUTOMATON:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    new-instance v0, Lorg/apache/lucene/util/automaton/RegExp$Kind;

    const-string v1, "REGEXP_INTERVAL"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/util/automaton/RegExp$Kind;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_INTERVAL:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    .line 319
    const/16 v0, 0x10

    new-array v0, v0, [Lorg/apache/lucene/util/automaton/RegExp$Kind;

    sget-object v1, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_UNION:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    aput-object v1, v0, v3

    sget-object v1, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_CONCATENATION:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    aput-object v1, v0, v4

    sget-object v1, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_INTERSECTION:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    aput-object v1, v0, v5

    sget-object v1, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_OPTIONAL:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    aput-object v1, v0, v6

    sget-object v1, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_REPEAT:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_REPEAT_MIN:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_REPEAT_MINMAX:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_COMPLEMENT:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_CHAR:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_CHAR_RANGE:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_ANYCHAR:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_EMPTY:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_STRING:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_ANYSTRING:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_AUTOMATON:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_INTERVAL:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    aput-object v2, v0, v1

    sput-object v0, Lorg/apache/lucene/util/automaton/RegExp$Kind;->ENUM$VALUES:[Lorg/apache/lucene/util/automaton/RegExp$Kind;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 319
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/lucene/util/automaton/RegExp$Kind;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lorg/apache/lucene/util/automaton/RegExp$Kind;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/util/automaton/RegExp$Kind;

    return-object v0
.end method

.method public static values()[Lorg/apache/lucene/util/automaton/RegExp$Kind;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lorg/apache/lucene/util/automaton/RegExp$Kind;->ENUM$VALUES:[Lorg/apache/lucene/util/automaton/RegExp$Kind;

    array-length v1, v0

    new-array v2, v1, [Lorg/apache/lucene/util/automaton/RegExp$Kind;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
