.class public Lorg/apache/lucene/util/BytesRefHash$DirectBytesStartArray;
.super Lorg/apache/lucene/util/BytesRefHash$BytesStartArray;
.source "BytesRefHash.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/BytesRefHash;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DirectBytesStartArray"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private bytesStart:[I

.field private final bytesUsed:Lorg/apache/lucene/util/Counter;

.field protected final initSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 583
    const-class v0, Lorg/apache/lucene/util/BytesRefHash;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/BytesRefHash$DirectBytesStartArray;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "initSize"    # I

    .prologue
    .line 598
    invoke-static {}, Lorg/apache/lucene/util/Counter;->newCounter()Lorg/apache/lucene/util/Counter;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/util/BytesRefHash$DirectBytesStartArray;-><init>(ILorg/apache/lucene/util/Counter;)V

    .line 599
    return-void
.end method

.method public constructor <init>(ILorg/apache/lucene/util/Counter;)V
    .locals 0
    .param p1, "initSize"    # I
    .param p2, "counter"    # Lorg/apache/lucene/util/Counter;

    .prologue
    .line 592
    invoke-direct {p0}, Lorg/apache/lucene/util/BytesRefHash$BytesStartArray;-><init>()V

    .line 593
    iput-object p2, p0, Lorg/apache/lucene/util/BytesRefHash$DirectBytesStartArray;->bytesUsed:Lorg/apache/lucene/util/Counter;

    .line 594
    iput p1, p0, Lorg/apache/lucene/util/BytesRefHash$DirectBytesStartArray;->initSize:I

    .line 595
    return-void
.end method


# virtual methods
.method public bytesUsed()Lorg/apache/lucene/util/Counter;
    .locals 1

    .prologue
    .line 620
    iget-object v0, p0, Lorg/apache/lucene/util/BytesRefHash$DirectBytesStartArray;->bytesUsed:Lorg/apache/lucene/util/Counter;

    return-object v0
.end method

.method public clear()[I
    .locals 1

    .prologue
    .line 603
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/util/BytesRefHash$DirectBytesStartArray;->bytesStart:[I

    return-object v0
.end method

.method public grow()[I
    .locals 2

    .prologue
    .line 608
    sget-boolean v0, Lorg/apache/lucene/util/BytesRefHash$DirectBytesStartArray;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/util/BytesRefHash$DirectBytesStartArray;->bytesStart:[I

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 609
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/util/BytesRefHash$DirectBytesStartArray;->bytesStart:[I

    iget-object v1, p0, Lorg/apache/lucene/util/BytesRefHash$DirectBytesStartArray;->bytesStart:[I

    array-length v1, v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Lorg/apache/lucene/util/ArrayUtil;->grow([II)[I

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/util/BytesRefHash$DirectBytesStartArray;->bytesStart:[I

    return-object v0
.end method

.method public init()[I
    .locals 2

    .prologue
    .line 614
    iget v0, p0, Lorg/apache/lucene/util/BytesRefHash$DirectBytesStartArray;->initSize:I

    .line 615
    const/4 v1, 0x4

    .line 614
    invoke-static {v0, v1}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v0

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/apache/lucene/util/BytesRefHash$DirectBytesStartArray;->bytesStart:[I

    return-object v0
.end method
