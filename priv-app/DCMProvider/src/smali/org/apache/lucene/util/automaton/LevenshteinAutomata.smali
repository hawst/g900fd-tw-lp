.class public Lorg/apache/lucene/util/automaton/LevenshteinAutomata;
.super Ljava/lang/Object;
.source "LevenshteinAutomata.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/automaton/LevenshteinAutomata$ParametricDescription;
    }
.end annotation


# static fields
.field public static final MAXIMUM_SUPPORTED_DISTANCE:I = 0x2


# instance fields
.field final alphaMax:I

.field final alphabet:[I

.field descriptions:[Lorg/apache/lucene/util/automaton/LevenshteinAutomata$ParametricDescription;

.field numRanges:I

.field final rangeLower:[I

.field final rangeUpper:[I

.field final word:[I


# direct methods
.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "input"    # Ljava/lang/String;
    .param p2, "withTranspositions"    # Z

    .prologue
    .line 54
    invoke-static {p1}, Lorg/apache/lucene/util/automaton/LevenshteinAutomata;->codePoints(Ljava/lang/String;)[I

    move-result-object v0

    const v1, 0x10ffff

    invoke-direct {p0, v0, v1, p2}, Lorg/apache/lucene/util/automaton/LevenshteinAutomata;-><init>([IIZ)V

    .line 55
    return-void
.end method

.method public constructor <init>([IIZ)V
    .locals 10
    .param p1, "word"    # [I
    .param p2, "alphaMax"    # I
    .param p3, "withTranspositions"    # Z

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    const/4 v6, 0x0

    iput v6, p0, Lorg/apache/lucene/util/automaton/LevenshteinAutomata;->numRanges:I

    .line 62
    iput-object p1, p0, Lorg/apache/lucene/util/automaton/LevenshteinAutomata;->word:[I

    .line 63
    iput p2, p0, Lorg/apache/lucene/util/automaton/LevenshteinAutomata;->alphaMax:I

    .line 66
    new-instance v4, Ljava/util/TreeSet;

    invoke-direct {v4}, Ljava/util/TreeSet;-><init>()V

    .line 67
    .local v4, "set":Ljava/util/SortedSet;, "Ljava/util/SortedSet<Ljava/lang/Integer;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v6, p1

    if-lt v1, v6, :cond_1

    .line 74
    invoke-interface {v4}, Ljava/util/SortedSet;->size()I

    move-result v6

    new-array v6, v6, [I

    iput-object v6, p0, Lorg/apache/lucene/util/automaton/LevenshteinAutomata;->alphabet:[I

    .line 75
    invoke-interface {v4}, Ljava/util/SortedSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 76
    .local v2, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    const/4 v1, 0x0

    :goto_1
    iget-object v6, p0, Lorg/apache/lucene/util/automaton/LevenshteinAutomata;->alphabet:[I

    array-length v6, v6

    if-lt v1, v6, :cond_3

    .line 79
    iget-object v6, p0, Lorg/apache/lucene/util/automaton/LevenshteinAutomata;->alphabet:[I

    array-length v6, v6

    add-int/lit8 v6, v6, 0x2

    new-array v6, v6, [I

    iput-object v6, p0, Lorg/apache/lucene/util/automaton/LevenshteinAutomata;->rangeLower:[I

    .line 80
    iget-object v6, p0, Lorg/apache/lucene/util/automaton/LevenshteinAutomata;->alphabet:[I

    array-length v6, v6

    add-int/lit8 v6, v6, 0x2

    new-array v6, v6, [I

    iput-object v6, p0, Lorg/apache/lucene/util/automaton/LevenshteinAutomata;->rangeUpper:[I

    .line 83
    const/4 v3, 0x0

    .line 84
    .local v3, "lower":I
    const/4 v1, 0x0

    :goto_2
    iget-object v6, p0, Lorg/apache/lucene/util/automaton/LevenshteinAutomata;->alphabet:[I

    array-length v6, v6

    if-lt v1, v6, :cond_4

    .line 94
    if-gt v3, p2, :cond_0

    .line 95
    iget-object v6, p0, Lorg/apache/lucene/util/automaton/LevenshteinAutomata;->rangeLower:[I

    iget v7, p0, Lorg/apache/lucene/util/automaton/LevenshteinAutomata;->numRanges:I

    aput v3, v6, v7

    .line 96
    iget-object v6, p0, Lorg/apache/lucene/util/automaton/LevenshteinAutomata;->rangeUpper:[I

    iget v7, p0, Lorg/apache/lucene/util/automaton/LevenshteinAutomata;->numRanges:I

    aput p2, v6, v7

    .line 97
    iget v6, p0, Lorg/apache/lucene/util/automaton/LevenshteinAutomata;->numRanges:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lorg/apache/lucene/util/automaton/LevenshteinAutomata;->numRanges:I

    .line 100
    :cond_0
    const/4 v6, 0x3

    new-array v7, v6, [Lorg/apache/lucene/util/automaton/LevenshteinAutomata$ParametricDescription;

    const/4 v8, 0x1

    .line 102
    if-eqz p3, :cond_6

    new-instance v6, Lorg/apache/lucene/util/automaton/Lev1TParametricDescription;

    array-length v9, p1

    invoke-direct {v6, v9}, Lorg/apache/lucene/util/automaton/Lev1TParametricDescription;-><init>(I)V

    :goto_3
    aput-object v6, v7, v8

    const/4 v8, 0x2

    .line 103
    if-eqz p3, :cond_7

    new-instance v6, Lorg/apache/lucene/util/automaton/Lev2TParametricDescription;

    array-length v9, p1

    invoke-direct {v6, v9}, Lorg/apache/lucene/util/automaton/Lev2TParametricDescription;-><init>(I)V

    :goto_4
    aput-object v6, v7, v8

    .line 100
    iput-object v7, p0, Lorg/apache/lucene/util/automaton/LevenshteinAutomata;->descriptions:[Lorg/apache/lucene/util/automaton/LevenshteinAutomata$ParametricDescription;

    .line 105
    return-void

    .line 68
    .end local v2    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    .end local v3    # "lower":I
    :cond_1
    aget v5, p1, v1

    .line 69
    .local v5, "v":I
    if-le v5, p2, :cond_2

    .line 70
    new-instance v6, Ljava/lang/IllegalArgumentException;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "alphaMax exceeded by symbol "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " in word"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 72
    :cond_2
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/SortedSet;->add(Ljava/lang/Object;)Z

    .line 67
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 77
    .end local v5    # "v":I
    .restart local v2    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    :cond_3
    iget-object v7, p0, Lorg/apache/lucene/util/automaton/LevenshteinAutomata;->alphabet:[I

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    aput v6, v7, v1

    .line 76
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_1

    .line 85
    .restart local v3    # "lower":I
    :cond_4
    iget-object v6, p0, Lorg/apache/lucene/util/automaton/LevenshteinAutomata;->alphabet:[I

    aget v0, v6, v1

    .line 86
    .local v0, "higher":I
    if-le v0, v3, :cond_5

    .line 87
    iget-object v6, p0, Lorg/apache/lucene/util/automaton/LevenshteinAutomata;->rangeLower:[I

    iget v7, p0, Lorg/apache/lucene/util/automaton/LevenshteinAutomata;->numRanges:I

    aput v3, v6, v7

    .line 88
    iget-object v6, p0, Lorg/apache/lucene/util/automaton/LevenshteinAutomata;->rangeUpper:[I

    iget v7, p0, Lorg/apache/lucene/util/automaton/LevenshteinAutomata;->numRanges:I

    add-int/lit8 v8, v0, -0x1

    aput v8, v6, v7

    .line 89
    iget v6, p0, Lorg/apache/lucene/util/automaton/LevenshteinAutomata;->numRanges:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lorg/apache/lucene/util/automaton/LevenshteinAutomata;->numRanges:I

    .line 91
    :cond_5
    add-int/lit8 v3, v0, 0x1

    .line 84
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_2

    .line 102
    .end local v0    # "higher":I
    :cond_6
    new-instance v6, Lorg/apache/lucene/util/automaton/Lev1ParametricDescription;

    array-length v9, p1

    invoke-direct {v6, v9}, Lorg/apache/lucene/util/automaton/Lev1ParametricDescription;-><init>(I)V

    goto :goto_3

    .line 103
    :cond_7
    new-instance v6, Lorg/apache/lucene/util/automaton/Lev2ParametricDescription;

    array-length v9, p1

    invoke-direct {v6, v9}, Lorg/apache/lucene/util/automaton/Lev2ParametricDescription;-><init>(I)V

    goto :goto_4
.end method

.method private static codePoints(Ljava/lang/String;)[I
    .locals 8
    .param p0, "input"    # Ljava/lang/String;

    .prologue
    .line 108
    const/4 v6, 0x0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v7

    invoke-static {p0, v6, v7}, Ljava/lang/Character;->codePointCount(Ljava/lang/CharSequence;II)I

    move-result v4

    .line 109
    .local v4, "length":I
    new-array v5, v4, [I

    .line 110
    .local v5, "word":[I
    const/4 v1, 0x0

    .local v1, "i":I
    const/4 v2, 0x0

    .local v2, "j":I
    const/4 v0, 0x0

    .local v0, "cp":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v6

    if-lt v1, v6, :cond_0

    .line 113
    return-object v5

    .line 111
    :cond_0
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "j":I
    .local v3, "j":I
    invoke-virtual {p0, v1}, Ljava/lang/String;->codePointAt(I)I

    move-result v0

    aput v0, v5, v2

    .line 110
    invoke-static {v0}, Ljava/lang/Character;->charCount(I)I

    move-result v6

    add-int/2addr v1, v6

    move v2, v3

    .end local v3    # "j":I
    .restart local v2    # "j":I
    goto :goto_0
.end method


# virtual methods
.method getVector(III)I
    .locals 3
    .param p1, "x"    # I
    .param p2, "pos"    # I
    .param p3, "end"    # I

    .prologue
    .line 187
    const/4 v1, 0x0

    .line 188
    .local v1, "vector":I
    move v0, p2

    .local v0, "i":I
    :goto_0
    if-lt v0, p3, :cond_0

    .line 193
    return v1

    .line 189
    :cond_0
    shl-int/lit8 v1, v1, 0x1

    .line 190
    iget-object v2, p0, Lorg/apache/lucene/util/automaton/LevenshteinAutomata;->word:[I

    aget v2, v2, v0

    if-ne v2, p1, :cond_1

    .line 191
    or-int/lit8 v1, v1, 0x1

    .line 188
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public toAutomaton(I)Lorg/apache/lucene/util/automaton/Automaton;
    .locals 19
    .param p1, "n"    # I

    .prologue
    .line 128
    if-nez p1, :cond_0

    .line 129
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/util/automaton/LevenshteinAutomata;->word:[I

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/LevenshteinAutomata;->word:[I

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v16, v0

    invoke-static/range {v14 .. v16}, Lorg/apache/lucene/util/automaton/BasicAutomata;->makeString([III)Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v1

    .line 179
    :goto_0
    return-object v1

    .line 132
    :cond_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/util/automaton/LevenshteinAutomata;->descriptions:[Lorg/apache/lucene/util/automaton/LevenshteinAutomata$ParametricDescription;

    array-length v14, v14

    move/from16 v0, p1

    if-lt v0, v14, :cond_1

    .line 133
    const/4 v1, 0x0

    goto :goto_0

    .line 135
    :cond_1
    mul-int/lit8 v14, p1, 0x2

    add-int/lit8 v10, v14, 0x1

    .line 136
    .local v10, "range":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/util/automaton/LevenshteinAutomata;->descriptions:[Lorg/apache/lucene/util/automaton/LevenshteinAutomata$ParametricDescription;

    aget-object v4, v14, p1

    .line 138
    .local v4, "description":Lorg/apache/lucene/util/automaton/LevenshteinAutomata$ParametricDescription;
    invoke-virtual {v4}, Lorg/apache/lucene/util/automaton/LevenshteinAutomata$ParametricDescription;->size()I

    move-result v14

    new-array v11, v14, [Lorg/apache/lucene/util/automaton/State;

    .line 140
    .local v11, "states":[Lorg/apache/lucene/util/automaton/State;
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    array-length v14, v11

    if-lt v7, v14, :cond_2

    .line 146
    const/4 v8, 0x0

    .local v8, "k":I
    :goto_2
    array-length v14, v11

    if-lt v8, v14, :cond_3

    .line 169
    new-instance v1, Lorg/apache/lucene/util/automaton/Automaton;

    const/4 v14, 0x0

    aget-object v14, v11, v14

    invoke-direct {v1, v14}, Lorg/apache/lucene/util/automaton/Automaton;-><init>(Lorg/apache/lucene/util/automaton/State;)V

    .line 170
    .local v1, "a":Lorg/apache/lucene/util/automaton/Automaton;
    const/4 v14, 0x1

    invoke-virtual {v1, v14}, Lorg/apache/lucene/util/automaton/Automaton;->setDeterministic(Z)V

    .line 176
    invoke-virtual {v1}, Lorg/apache/lucene/util/automaton/Automaton;->reduce()V

    goto :goto_0

    .line 141
    .end local v1    # "a":Lorg/apache/lucene/util/automaton/Automaton;
    .end local v8    # "k":I
    :cond_2
    new-instance v14, Lorg/apache/lucene/util/automaton/State;

    invoke-direct {v14}, Lorg/apache/lucene/util/automaton/State;-><init>()V

    aput-object v14, v11, v7

    .line 142
    aget-object v14, v11, v7

    iput v7, v14, Lorg/apache/lucene/util/automaton/State;->number:I

    .line 143
    aget-object v14, v11, v7

    invoke-virtual {v4, v7}, Lorg/apache/lucene/util/automaton/LevenshteinAutomata$ParametricDescription;->isAccept(I)Z

    move-result v15

    invoke-virtual {v14, v15}, Lorg/apache/lucene/util/automaton/State;->setAccept(Z)V

    .line 140
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 147
    .restart local v8    # "k":I
    :cond_3
    invoke-virtual {v4, v8}, Lorg/apache/lucene/util/automaton/LevenshteinAutomata$ParametricDescription;->getPosition(I)I

    move-result v13

    .line 148
    .local v13, "xpos":I
    if-gez v13, :cond_5

    .line 146
    :cond_4
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 150
    :cond_5
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/util/automaton/LevenshteinAutomata;->word:[I

    array-length v14, v14

    sub-int/2addr v14, v13

    invoke-static {v14, v10}, Ljava/lang/Math;->min(II)I

    move-result v14

    add-int v6, v13, v14

    .line 152
    .local v6, "end":I
    const/4 v12, 0x0

    .local v12, "x":I
    :goto_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/util/automaton/LevenshteinAutomata;->alphabet:[I

    array-length v14, v14

    if-lt v12, v14, :cond_6

    .line 163
    const/4 v14, 0x0

    invoke-virtual {v4, v8, v13, v14}, Lorg/apache/lucene/util/automaton/LevenshteinAutomata$ParametricDescription;->transition(III)I

    move-result v5

    .line 164
    .local v5, "dest":I
    if-ltz v5, :cond_4

    .line 165
    const/4 v9, 0x0

    .local v9, "r":I
    :goto_4
    move-object/from16 v0, p0

    iget v14, v0, Lorg/apache/lucene/util/automaton/LevenshteinAutomata;->numRanges:I

    if-ge v9, v14, :cond_4

    .line 166
    aget-object v14, v11, v8

    new-instance v15, Lorg/apache/lucene/util/automaton/Transition;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/LevenshteinAutomata;->rangeLower:[I

    move-object/from16 v16, v0

    aget v16, v16, v9

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/LevenshteinAutomata;->rangeUpper:[I

    move-object/from16 v17, v0

    aget v17, v17, v9

    aget-object v18, v11, v5

    invoke-direct/range {v15 .. v18}, Lorg/apache/lucene/util/automaton/Transition;-><init>(IILorg/apache/lucene/util/automaton/State;)V

    invoke-virtual {v14, v15}, Lorg/apache/lucene/util/automaton/State;->addTransition(Lorg/apache/lucene/util/automaton/Transition;)V

    .line 165
    add-int/lit8 v9, v9, 0x1

    goto :goto_4

    .line 153
    .end local v5    # "dest":I
    .end local v9    # "r":I
    :cond_6
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/util/automaton/LevenshteinAutomata;->alphabet:[I

    aget v2, v14, v12

    .line 155
    .local v2, "ch":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v13, v6}, Lorg/apache/lucene/util/automaton/LevenshteinAutomata;->getVector(III)I

    move-result v3

    .line 156
    .local v3, "cvec":I
    invoke-virtual {v4, v8, v13, v3}, Lorg/apache/lucene/util/automaton/LevenshteinAutomata$ParametricDescription;->transition(III)I

    move-result v5

    .line 157
    .restart local v5    # "dest":I
    if-ltz v5, :cond_7

    .line 158
    aget-object v14, v11, v8

    new-instance v15, Lorg/apache/lucene/util/automaton/Transition;

    aget-object v16, v11, v5

    move-object/from16 v0, v16

    invoke-direct {v15, v2, v0}, Lorg/apache/lucene/util/automaton/Transition;-><init>(ILorg/apache/lucene/util/automaton/State;)V

    invoke-virtual {v14, v15}, Lorg/apache/lucene/util/automaton/State;->addTransition(Lorg/apache/lucene/util/automaton/Transition;)V

    .line 152
    :cond_7
    add-int/lit8 v12, v12, 0x1

    goto :goto_3
.end method
