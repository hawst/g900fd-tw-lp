.class public final Lorg/apache/lucene/util/NamedSPILoader;
.super Ljava/lang/Object;
.source "NamedSPILoader.java"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/NamedSPILoader$NamedSPI;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<S::",
        "Lorg/apache/lucene/util/NamedSPILoader$NamedSPI;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable",
        "<TS;>;"
    }
.end annotation


# instance fields
.field private final clazz:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TS;>;"
        }
    .end annotation
.end field

.field private volatile services:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "TS;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<TS;>;)V"
        }
    .end annotation

    .prologue
    .line 37
    .local p0, "this":Lorg/apache/lucene/util/NamedSPILoader;, "Lorg/apache/lucene/util/NamedSPILoader<TS;>;"
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TS;>;"
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getContextClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/util/NamedSPILoader;-><init>(Ljava/lang/Class;Ljava/lang/ClassLoader;)V

    .line 38
    return-void
.end method

.method public constructor <init>(Ljava/lang/Class;Ljava/lang/ClassLoader;)V
    .locals 2
    .param p2, "classloader"    # Ljava/lang/ClassLoader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<TS;>;",
            "Ljava/lang/ClassLoader;",
            ")V"
        }
    .end annotation

    .prologue
    .line 40
    .local p0, "this":Lorg/apache/lucene/util/NamedSPILoader;, "Lorg/apache/lucene/util/NamedSPILoader<TS;>;"
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TS;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/util/NamedSPILoader;->services:Ljava/util/Map;

    .line 41
    iput-object p1, p0, Lorg/apache/lucene/util/NamedSPILoader;->clazz:Ljava/lang/Class;

    .line 43
    invoke-virtual {p1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    .line 44
    .local v0, "clazzClassloader":Ljava/lang/ClassLoader;
    if-eqz v0, :cond_0

    invoke-static {v0, p2}, Lorg/apache/lucene/util/SPIClassIterator;->isParentClassLoader(Ljava/lang/ClassLoader;Ljava/lang/ClassLoader;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 45
    invoke-virtual {p0, v0}, Lorg/apache/lucene/util/NamedSPILoader;->reload(Ljava/lang/ClassLoader;)V

    .line 47
    :cond_0
    invoke-virtual {p0, p2}, Lorg/apache/lucene/util/NamedSPILoader;->reload(Ljava/lang/ClassLoader;)V

    .line 48
    return-void
.end method

.method public static checkServiceName(Ljava/lang/String;)V
    .locals 6
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 88
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v4, 0x80

    if-lt v3, v4, :cond_0

    .line 89
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Illegal service name: \'"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\' is too long (must be < 128 chars)."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 91
    :cond_0
    const/4 v1, 0x0

    .local v1, "i":I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    .local v2, "len":I
    :goto_0
    if-lt v1, v2, :cond_1

    .line 97
    return-void

    .line 92
    :cond_1
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 93
    .local v0, "c":C
    invoke-static {v0}, Lorg/apache/lucene/util/NamedSPILoader;->isLetterOrDigit(C)Z

    move-result v3

    if-nez v3, :cond_2

    .line 94
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Illegal service name: \'"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\' must be simple ascii alphanumeric."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 91
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private static isLetterOrDigit(C)Z
    .locals 1
    .param p0, "c"    # C

    .prologue
    .line 103
    const/16 v0, 0x61

    if-gt v0, p0, :cond_0

    const/16 v0, 0x7a

    if-le p0, v0, :cond_3

    :cond_0
    const/16 v0, 0x41

    if-gt v0, p0, :cond_1

    const/16 v0, 0x5a

    if-le p0, v0, :cond_3

    :cond_1
    const/16 v0, 0x30

    if-gt v0, p0, :cond_2

    const/16 v0, 0x39

    if-le p0, v0, :cond_3

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public availableServices()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 115
    .local p0, "this":Lorg/apache/lucene/util/NamedSPILoader;, "Lorg/apache/lucene/util/NamedSPILoader<TS;>;"
    iget-object v0, p0, Lorg/apache/lucene/util/NamedSPILoader;->services:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TS;>;"
        }
    .end annotation

    .prologue
    .line 120
    .local p0, "this":Lorg/apache/lucene/util/NamedSPILoader;, "Lorg/apache/lucene/util/NamedSPILoader<TS;>;"
    iget-object v0, p0, Lorg/apache/lucene/util/NamedSPILoader;->services:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public lookup(Ljava/lang/String;)Lorg/apache/lucene/util/NamedSPILoader$NamedSPI;
    .locals 4
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TS;"
        }
    .end annotation

    .prologue
    .line 107
    .local p0, "this":Lorg/apache/lucene/util/NamedSPILoader;, "Lorg/apache/lucene/util/NamedSPILoader<TS;>;"
    iget-object v1, p0, Lorg/apache/lucene/util/NamedSPILoader;->services:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/util/NamedSPILoader$NamedSPI;

    .line 108
    .local v0, "service":Lorg/apache/lucene/util/NamedSPILoader$NamedSPI;, "TS;"
    if-eqz v0, :cond_0

    return-object v0

    .line 109
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "A SPI class of type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lorg/apache/lucene/util/NamedSPILoader;->clazz:Ljava/lang/Class;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " with name \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\' does not exist. "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 110
    const-string v3, "You need to add the corresponding JAR file supporting this SPI to your classpath."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 111
    const-string v3, "The current classpath supports the following names: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lorg/apache/lucene/util/NamedSPILoader;->availableServices()Ljava/util/Set;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 109
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public declared-synchronized reload(Ljava/lang/ClassLoader;)V
    .locals 9
    .param p1, "classloader"    # Ljava/lang/ClassLoader;

    .prologue
    .line 62
    .local p0, "this":Lorg/apache/lucene/util/NamedSPILoader;, "Lorg/apache/lucene/util/NamedSPILoader<TS;>;"
    monitor-enter p0

    :try_start_0
    new-instance v5, Ljava/util/LinkedHashMap;

    iget-object v6, p0, Lorg/apache/lucene/util/NamedSPILoader;->services:Ljava/util/Map;

    invoke-direct {v5, v6}, Ljava/util/LinkedHashMap;-><init>(Ljava/util/Map;)V

    .line 63
    .local v5, "services":Ljava/util/LinkedHashMap;, "Ljava/util/LinkedHashMap<Ljava/lang/String;TS;>;"
    iget-object v6, p0, Lorg/apache/lucene/util/NamedSPILoader;->clazz:Ljava/lang/Class;

    invoke-static {v6, p1}, Lorg/apache/lucene/util/SPIClassIterator;->get(Ljava/lang/Class;Ljava/lang/ClassLoader;)Lorg/apache/lucene/util/SPIClassIterator;

    move-result-object v2

    .line 64
    .local v2, "loader":Lorg/apache/lucene/util/SPIClassIterator;, "Lorg/apache/lucene/util/SPIClassIterator<TS;>;"
    :cond_0
    :goto_0
    invoke-virtual {v2}, Lorg/apache/lucene/util/SPIClassIterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_1

    .line 80
    invoke-static {v5}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v6

    iput-object v6, p0, Lorg/apache/lucene/util/NamedSPILoader;->services:Ljava/util/Map;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 81
    monitor-exit p0

    return-void

    .line 65
    :cond_1
    :try_start_1
    invoke-virtual {v2}, Lorg/apache/lucene/util/SPIClassIterator;->next()Ljava/lang/Class;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 67
    .local v0, "c":Ljava/lang/Class;, "Ljava/lang/Class<+TS;>;"
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/util/NamedSPILoader$NamedSPI;

    .line 68
    .local v4, "service":Lorg/apache/lucene/util/NamedSPILoader$NamedSPI;, "TS;"
    invoke-interface {v4}, Lorg/apache/lucene/util/NamedSPILoader$NamedSPI;->getName()Ljava/lang/String;

    move-result-object v3

    .line 72
    .local v3, "name":Ljava/lang/String;
    invoke-virtual {v5, v3}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 73
    invoke-static {v3}, Lorg/apache/lucene/util/NamedSPILoader;->checkServiceName(Ljava/lang/String;)V

    .line 74
    invoke-virtual {v5, v3, v4}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 76
    .end local v3    # "name":Ljava/lang/String;
    .end local v4    # "service":Lorg/apache/lucene/util/NamedSPILoader$NamedSPI;, "TS;"
    :catch_0
    move-exception v1

    .line 77
    .local v1, "e":Ljava/lang/Exception;
    :try_start_3
    new-instance v6, Ljava/util/ServiceConfigurationError;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Cannot instantiate SPI class: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7, v1}, Ljava/util/ServiceConfigurationError;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 62
    .end local v0    # "c":Ljava/lang/Class;, "Ljava/lang/Class<+TS;>;"
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "loader":Lorg/apache/lucene/util/SPIClassIterator;, "Lorg/apache/lucene/util/SPIClassIterator<TS;>;"
    .end local v5    # "services":Ljava/util/LinkedHashMap;, "Ljava/util/LinkedHashMap<Ljava/lang/String;TS;>;"
    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6
.end method
