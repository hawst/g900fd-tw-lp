.class final Lorg/apache/lucene/util/packed/BulkOperationPacked5;
.super Lorg/apache/lucene/util/packed/BulkOperationPacked;
.source "BulkOperationPacked5.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lorg/apache/lucene/util/packed/BulkOperationPacked;-><init>(I)V

    .line 29
    return-void
.end method


# virtual methods
.method public decode([BI[III)V
    .locals 10
    .param p1, "blocks"    # [B
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [I
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 108
    const/4 v6, 0x0

    .local v6, "i":I
    move v7, p4

    .end local p4    # "valuesOffset":I
    .local v7, "valuesOffset":I
    move v0, p2

    .end local p2    # "blocksOffset":I
    .local v0, "blocksOffset":I
    :goto_0
    if-lt v6, p5, :cond_0

    .line 123
    return-void

    .line 109
    :cond_0
    add-int/lit8 p2, v0, 0x1

    .end local v0    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v8, p1, v0

    and-int/lit16 v1, v8, 0xff

    .line 110
    .local v1, "byte0":I
    add-int/lit8 p4, v7, 0x1

    .end local v7    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    ushr-int/lit8 v8, v1, 0x3

    aput v8, p3, v7

    .line 111
    add-int/lit8 v0, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v0    # "blocksOffset":I
    aget-byte v8, p1, p2

    and-int/lit16 v2, v8, 0xff

    .line 112
    .local v2, "byte1":I
    add-int/lit8 v7, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v7    # "valuesOffset":I
    and-int/lit8 v8, v1, 0x7

    shl-int/lit8 v8, v8, 0x2

    ushr-int/lit8 v9, v2, 0x6

    or-int/2addr v8, v9

    aput v8, p3, p4

    .line 113
    add-int/lit8 p4, v7, 0x1

    .end local v7    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    ushr-int/lit8 v8, v2, 0x1

    and-int/lit8 v8, v8, 0x1f

    aput v8, p3, v7

    .line 114
    add-int/lit8 p2, v0, 0x1

    .end local v0    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v8, p1, v0

    and-int/lit16 v3, v8, 0xff

    .line 115
    .local v3, "byte2":I
    add-int/lit8 v7, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v7    # "valuesOffset":I
    and-int/lit8 v8, v2, 0x1

    shl-int/lit8 v8, v8, 0x4

    ushr-int/lit8 v9, v3, 0x4

    or-int/2addr v8, v9

    aput v8, p3, p4

    .line 116
    add-int/lit8 v0, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v0    # "blocksOffset":I
    aget-byte v8, p1, p2

    and-int/lit16 v4, v8, 0xff

    .line 117
    .local v4, "byte3":I
    add-int/lit8 p4, v7, 0x1

    .end local v7    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    and-int/lit8 v8, v3, 0xf

    shl-int/lit8 v8, v8, 0x1

    ushr-int/lit8 v9, v4, 0x7

    or-int/2addr v8, v9

    aput v8, p3, v7

    .line 118
    add-int/lit8 v7, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v7    # "valuesOffset":I
    ushr-int/lit8 v8, v4, 0x2

    and-int/lit8 v8, v8, 0x1f

    aput v8, p3, p4

    .line 119
    add-int/lit8 p2, v0, 0x1

    .end local v0    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v8, p1, v0

    and-int/lit16 v5, v8, 0xff

    .line 120
    .local v5, "byte4":I
    add-int/lit8 p4, v7, 0x1

    .end local v7    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    and-int/lit8 v8, v4, 0x3

    shl-int/lit8 v8, v8, 0x3

    ushr-int/lit8 v9, v5, 0x5

    or-int/2addr v8, v9

    aput v8, p3, v7

    .line 121
    add-int/lit8 v7, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v7    # "valuesOffset":I
    and-int/lit8 v8, v5, 0x1f

    aput v8, p3, p4

    .line 108
    add-int/lit8 v6, v6, 0x1

    move v0, p2

    .end local p2    # "blocksOffset":I
    .restart local v0    # "blocksOffset":I
    goto :goto_0
.end method

.method public decode([BI[JII)V
    .locals 20
    .param p1, "blocks"    # [B
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [J
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 202
    const/4 v3, 0x0

    .local v3, "i":I
    move/from16 v14, p4

    .end local p4    # "valuesOffset":I
    .local v14, "valuesOffset":I
    move/from16 v2, p2

    .end local p2    # "blocksOffset":I
    .local v2, "blocksOffset":I
    :goto_0
    move/from16 v0, p5

    if-lt v3, v0, :cond_0

    .line 217
    return-void

    .line 203
    :cond_0
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v15, p1, v2

    and-int/lit16 v15, v15, 0xff

    int-to-long v4, v15

    .line 204
    .local v4, "byte0":J
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/4 v15, 0x3

    ushr-long v16, v4, v15

    aput-wide v16, p3, v14

    .line 205
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v15, p1, p2

    and-int/lit16 v15, v15, 0xff

    int-to-long v6, v15

    .line 206
    .local v6, "byte1":J
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const-wide/16 v16, 0x7

    and-long v16, v16, v4

    const/4 v15, 0x2

    shl-long v16, v16, v15

    const/4 v15, 0x6

    ushr-long v18, v6, v15

    or-long v16, v16, v18

    aput-wide v16, p3, p4

    .line 207
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/4 v15, 0x1

    ushr-long v16, v6, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, v14

    .line 208
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v15, p1, v2

    and-int/lit16 v15, v15, 0xff

    int-to-long v8, v15

    .line 209
    .local v8, "byte2":J
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const-wide/16 v16, 0x1

    and-long v16, v16, v6

    const/4 v15, 0x4

    shl-long v16, v16, v15

    const/4 v15, 0x4

    ushr-long v18, v8, v15

    or-long v16, v16, v18

    aput-wide v16, p3, p4

    .line 210
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v15, p1, p2

    and-int/lit16 v15, v15, 0xff

    int-to-long v10, v15

    .line 211
    .local v10, "byte3":J
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v16, 0xf

    and-long v16, v16, v8

    const/4 v15, 0x1

    shl-long v16, v16, v15

    const/4 v15, 0x7

    ushr-long v18, v10, v15

    or-long v16, v16, v18

    aput-wide v16, p3, v14

    .line 212
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/4 v15, 0x2

    ushr-long v16, v10, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, p4

    .line 213
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v15, p1, v2

    and-int/lit16 v15, v15, 0xff

    int-to-long v12, v15

    .line 214
    .local v12, "byte4":J
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v16, 0x3

    and-long v16, v16, v10

    const/4 v15, 0x3

    shl-long v16, v16, v15

    const/4 v15, 0x5

    ushr-long v18, v12, v15

    or-long v16, v16, v18

    aput-wide v16, p3, v14

    .line 215
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const-wide/16 v16, 0x1f

    and-long v16, v16, v12

    aput-wide v16, p3, p4

    .line 202
    add-int/lit8 v3, v3, 0x1

    move/from16 v2, p2

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    goto/16 :goto_0
.end method

.method public decode([JI[III)V
    .locals 20
    .param p1, "blocks"    # [J
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [I
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 33
    const/4 v13, 0x0

    .local v13, "i":I
    move/from16 v14, p4

    .end local p4    # "valuesOffset":I
    .local v14, "valuesOffset":I
    move/from16 v12, p2

    .end local p2    # "blocksOffset":I
    .local v12, "blocksOffset":I
    :goto_0
    move/from16 v0, p5

    if-lt v13, v0, :cond_0

    .line 104
    return-void

    .line 34
    :cond_0
    add-int/lit8 p2, v12, 0x1

    .end local v12    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v2, p1, v12

    .line 35
    .local v2, "block0":J
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0x3b

    ushr-long v16, v2, v15

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, v14

    .line 36
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0x36

    ushr-long v16, v2, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, p4

    .line 37
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0x31

    ushr-long v16, v2, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, v14

    .line 38
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0x2c

    ushr-long v16, v2, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, p4

    .line 39
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0x27

    ushr-long v16, v2, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, v14

    .line 40
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0x22

    ushr-long v16, v2, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, p4

    .line 41
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0x1d

    ushr-long v16, v2, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, v14

    .line 42
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0x18

    ushr-long v16, v2, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, p4

    .line 43
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0x13

    ushr-long v16, v2, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, v14

    .line 44
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0xe

    ushr-long v16, v2, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, p4

    .line 45
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0x9

    ushr-long v16, v2, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, v14

    .line 46
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/4 v15, 0x4

    ushr-long v16, v2, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, p4

    .line 47
    add-int/lit8 v12, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v12    # "blocksOffset":I
    aget-wide v4, p1, p2

    .line 48
    .local v4, "block1":J
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v16, 0xf

    and-long v16, v16, v2

    const/4 v15, 0x1

    shl-long v16, v16, v15

    const/16 v15, 0x3f

    ushr-long v18, v4, v15

    or-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, v14

    .line 49
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0x3a

    ushr-long v16, v4, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, p4

    .line 50
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0x35

    ushr-long v16, v4, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, v14

    .line 51
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0x30

    ushr-long v16, v4, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, p4

    .line 52
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0x2b

    ushr-long v16, v4, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, v14

    .line 53
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0x26

    ushr-long v16, v4, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, p4

    .line 54
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0x21

    ushr-long v16, v4, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, v14

    .line 55
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0x1c

    ushr-long v16, v4, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, p4

    .line 56
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0x17

    ushr-long v16, v4, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, v14

    .line 57
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0x12

    ushr-long v16, v4, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, p4

    .line 58
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0xd

    ushr-long v16, v4, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, v14

    .line 59
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0x8

    ushr-long v16, v4, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, p4

    .line 60
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/4 v15, 0x3

    ushr-long v16, v4, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, v14

    .line 61
    add-int/lit8 p2, v12, 0x1

    .end local v12    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v6, p1, v12

    .line 62
    .local v6, "block2":J
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const-wide/16 v16, 0x7

    and-long v16, v16, v4

    const/4 v15, 0x2

    shl-long v16, v16, v15

    const/16 v15, 0x3e

    ushr-long v18, v6, v15

    or-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, p4

    .line 63
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0x39

    ushr-long v16, v6, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, v14

    .line 64
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0x34

    ushr-long v16, v6, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, p4

    .line 65
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0x2f

    ushr-long v16, v6, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, v14

    .line 66
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0x2a

    ushr-long v16, v6, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, p4

    .line 67
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0x25

    ushr-long v16, v6, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, v14

    .line 68
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0x20

    ushr-long v16, v6, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, p4

    .line 69
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0x1b

    ushr-long v16, v6, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, v14

    .line 70
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0x16

    ushr-long v16, v6, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, p4

    .line 71
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0x11

    ushr-long v16, v6, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, v14

    .line 72
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0xc

    ushr-long v16, v6, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, p4

    .line 73
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/4 v15, 0x7

    ushr-long v16, v6, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, v14

    .line 74
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/4 v15, 0x2

    ushr-long v16, v6, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, p4

    .line 75
    add-int/lit8 v12, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v12    # "blocksOffset":I
    aget-wide v8, p1, p2

    .line 76
    .local v8, "block3":J
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v16, 0x3

    and-long v16, v16, v6

    const/4 v15, 0x3

    shl-long v16, v16, v15

    const/16 v15, 0x3d

    ushr-long v18, v8, v15

    or-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, v14

    .line 77
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0x38

    ushr-long v16, v8, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, p4

    .line 78
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0x33

    ushr-long v16, v8, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, v14

    .line 79
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0x2e

    ushr-long v16, v8, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, p4

    .line 80
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0x29

    ushr-long v16, v8, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, v14

    .line 81
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0x24

    ushr-long v16, v8, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, p4

    .line 82
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0x1f

    ushr-long v16, v8, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, v14

    .line 83
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0x1a

    ushr-long v16, v8, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, p4

    .line 84
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0x15

    ushr-long v16, v8, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, v14

    .line 85
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0x10

    ushr-long v16, v8, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, p4

    .line 86
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0xb

    ushr-long v16, v8, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, v14

    .line 87
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/4 v15, 0x6

    ushr-long v16, v8, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, p4

    .line 88
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/4 v15, 0x1

    ushr-long v16, v8, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, v14

    .line 89
    add-int/lit8 p2, v12, 0x1

    .end local v12    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v10, p1, v12

    .line 90
    .local v10, "block4":J
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const-wide/16 v16, 0x1

    and-long v16, v16, v8

    const/4 v15, 0x4

    shl-long v16, v16, v15

    const/16 v15, 0x3c

    ushr-long v18, v10, v15

    or-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, p4

    .line 91
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0x37

    ushr-long v16, v10, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, v14

    .line 92
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0x32

    ushr-long v16, v10, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, p4

    .line 93
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0x2d

    ushr-long v16, v10, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, v14

    .line 94
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0x28

    ushr-long v16, v10, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, p4

    .line 95
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0x23

    ushr-long v16, v10, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, v14

    .line 96
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0x1e

    ushr-long v16, v10, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, p4

    .line 97
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0x19

    ushr-long v16, v10, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, v14

    .line 98
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0x14

    ushr-long v16, v10, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, p4

    .line 99
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0xf

    ushr-long v16, v10, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, v14

    .line 100
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0xa

    ushr-long v16, v10, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, p4

    .line 101
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/4 v15, 0x5

    ushr-long v16, v10, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, v14

    .line 102
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const-wide/16 v16, 0x1f

    and-long v16, v16, v10

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, p4

    .line 33
    add-int/lit8 v13, v13, 0x1

    move/from16 v12, p2

    .end local p2    # "blocksOffset":I
    .restart local v12    # "blocksOffset":I
    goto/16 :goto_0
.end method

.method public decode([JI[JII)V
    .locals 20
    .param p1, "blocks"    # [J
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [J
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 127
    const/4 v13, 0x0

    .local v13, "i":I
    move/from16 v14, p4

    .end local p4    # "valuesOffset":I
    .local v14, "valuesOffset":I
    move/from16 v12, p2

    .end local p2    # "blocksOffset":I
    .local v12, "blocksOffset":I
    :goto_0
    move/from16 v0, p5

    if-lt v13, v0, :cond_0

    .line 198
    return-void

    .line 128
    :cond_0
    add-int/lit8 p2, v12, 0x1

    .end local v12    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v2, p1, v12

    .line 129
    .local v2, "block0":J
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0x3b

    ushr-long v16, v2, v15

    aput-wide v16, p3, v14

    .line 130
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0x36

    ushr-long v16, v2, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, p4

    .line 131
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0x31

    ushr-long v16, v2, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, v14

    .line 132
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0x2c

    ushr-long v16, v2, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, p4

    .line 133
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0x27

    ushr-long v16, v2, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, v14

    .line 134
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0x22

    ushr-long v16, v2, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, p4

    .line 135
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0x1d

    ushr-long v16, v2, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, v14

    .line 136
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0x18

    ushr-long v16, v2, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, p4

    .line 137
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0x13

    ushr-long v16, v2, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, v14

    .line 138
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0xe

    ushr-long v16, v2, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, p4

    .line 139
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0x9

    ushr-long v16, v2, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, v14

    .line 140
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/4 v15, 0x4

    ushr-long v16, v2, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, p4

    .line 141
    add-int/lit8 v12, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v12    # "blocksOffset":I
    aget-wide v4, p1, p2

    .line 142
    .local v4, "block1":J
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v16, 0xf

    and-long v16, v16, v2

    const/4 v15, 0x1

    shl-long v16, v16, v15

    const/16 v15, 0x3f

    ushr-long v18, v4, v15

    or-long v16, v16, v18

    aput-wide v16, p3, v14

    .line 143
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0x3a

    ushr-long v16, v4, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, p4

    .line 144
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0x35

    ushr-long v16, v4, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, v14

    .line 145
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0x30

    ushr-long v16, v4, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, p4

    .line 146
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0x2b

    ushr-long v16, v4, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, v14

    .line 147
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0x26

    ushr-long v16, v4, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, p4

    .line 148
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0x21

    ushr-long v16, v4, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, v14

    .line 149
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0x1c

    ushr-long v16, v4, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, p4

    .line 150
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0x17

    ushr-long v16, v4, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, v14

    .line 151
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0x12

    ushr-long v16, v4, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, p4

    .line 152
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0xd

    ushr-long v16, v4, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, v14

    .line 153
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0x8

    ushr-long v16, v4, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, p4

    .line 154
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/4 v15, 0x3

    ushr-long v16, v4, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, v14

    .line 155
    add-int/lit8 p2, v12, 0x1

    .end local v12    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v6, p1, v12

    .line 156
    .local v6, "block2":J
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const-wide/16 v16, 0x7

    and-long v16, v16, v4

    const/4 v15, 0x2

    shl-long v16, v16, v15

    const/16 v15, 0x3e

    ushr-long v18, v6, v15

    or-long v16, v16, v18

    aput-wide v16, p3, p4

    .line 157
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0x39

    ushr-long v16, v6, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, v14

    .line 158
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0x34

    ushr-long v16, v6, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, p4

    .line 159
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0x2f

    ushr-long v16, v6, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, v14

    .line 160
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0x2a

    ushr-long v16, v6, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, p4

    .line 161
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0x25

    ushr-long v16, v6, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, v14

    .line 162
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0x20

    ushr-long v16, v6, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, p4

    .line 163
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0x1b

    ushr-long v16, v6, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, v14

    .line 164
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0x16

    ushr-long v16, v6, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, p4

    .line 165
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0x11

    ushr-long v16, v6, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, v14

    .line 166
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0xc

    ushr-long v16, v6, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, p4

    .line 167
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/4 v15, 0x7

    ushr-long v16, v6, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, v14

    .line 168
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/4 v15, 0x2

    ushr-long v16, v6, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, p4

    .line 169
    add-int/lit8 v12, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v12    # "blocksOffset":I
    aget-wide v8, p1, p2

    .line 170
    .local v8, "block3":J
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v16, 0x3

    and-long v16, v16, v6

    const/4 v15, 0x3

    shl-long v16, v16, v15

    const/16 v15, 0x3d

    ushr-long v18, v8, v15

    or-long v16, v16, v18

    aput-wide v16, p3, v14

    .line 171
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0x38

    ushr-long v16, v8, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, p4

    .line 172
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0x33

    ushr-long v16, v8, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, v14

    .line 173
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0x2e

    ushr-long v16, v8, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, p4

    .line 174
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0x29

    ushr-long v16, v8, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, v14

    .line 175
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0x24

    ushr-long v16, v8, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, p4

    .line 176
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0x1f

    ushr-long v16, v8, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, v14

    .line 177
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0x1a

    ushr-long v16, v8, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, p4

    .line 178
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0x15

    ushr-long v16, v8, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, v14

    .line 179
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0x10

    ushr-long v16, v8, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, p4

    .line 180
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0xb

    ushr-long v16, v8, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, v14

    .line 181
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/4 v15, 0x6

    ushr-long v16, v8, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, p4

    .line 182
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/4 v15, 0x1

    ushr-long v16, v8, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, v14

    .line 183
    add-int/lit8 p2, v12, 0x1

    .end local v12    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v10, p1, v12

    .line 184
    .local v10, "block4":J
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const-wide/16 v16, 0x1

    and-long v16, v16, v8

    const/4 v15, 0x4

    shl-long v16, v16, v15

    const/16 v15, 0x3c

    ushr-long v18, v10, v15

    or-long v16, v16, v18

    aput-wide v16, p3, p4

    .line 185
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0x37

    ushr-long v16, v10, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, v14

    .line 186
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0x32

    ushr-long v16, v10, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, p4

    .line 187
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0x2d

    ushr-long v16, v10, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, v14

    .line 188
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0x28

    ushr-long v16, v10, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, p4

    .line 189
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0x23

    ushr-long v16, v10, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, v14

    .line 190
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0x1e

    ushr-long v16, v10, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, p4

    .line 191
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0x19

    ushr-long v16, v10, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, v14

    .line 192
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0x14

    ushr-long v16, v10, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, p4

    .line 193
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0xf

    ushr-long v16, v10, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, v14

    .line 194
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0xa

    ushr-long v16, v10, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, p4

    .line 195
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/4 v15, 0x5

    ushr-long v16, v10, v15

    const-wide/16 v18, 0x1f

    and-long v16, v16, v18

    aput-wide v16, p3, v14

    .line 196
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const-wide/16 v16, 0x1f

    and-long v16, v16, v10

    aput-wide v16, p3, p4

    .line 127
    add-int/lit8 v13, v13, 0x1

    move/from16 v12, p2

    .end local p2    # "blocksOffset":I
    .restart local v12    # "blocksOffset":I
    goto/16 :goto_0
.end method
