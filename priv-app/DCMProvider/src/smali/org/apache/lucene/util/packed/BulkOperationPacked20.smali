.class final Lorg/apache/lucene/util/packed/BulkOperationPacked20;
.super Lorg/apache/lucene/util/packed/BulkOperationPacked;
.source "BulkOperationPacked20.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    const/16 v0, 0x14

    invoke-direct {p0, v0}, Lorg/apache/lucene/util/packed/BulkOperationPacked;-><init>(I)V

    .line 29
    return-void
.end method


# virtual methods
.method public decode([BI[III)V
    .locals 10
    .param p1, "blocks"    # [B
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [I
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 60
    const/4 v6, 0x0

    .local v6, "i":I
    move v7, p4

    .end local p4    # "valuesOffset":I
    .local v7, "valuesOffset":I
    move v0, p2

    .end local p2    # "blocksOffset":I
    .local v0, "blocksOffset":I
    :goto_0
    if-lt v6, p5, :cond_0

    .line 69
    return-void

    .line 61
    :cond_0
    add-int/lit8 p2, v0, 0x1

    .end local v0    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v8, p1, v0

    and-int/lit16 v1, v8, 0xff

    .line 62
    .local v1, "byte0":I
    add-int/lit8 v0, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v0    # "blocksOffset":I
    aget-byte v8, p1, p2

    and-int/lit16 v2, v8, 0xff

    .line 63
    .local v2, "byte1":I
    add-int/lit8 p2, v0, 0x1

    .end local v0    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v8, p1, v0

    and-int/lit16 v3, v8, 0xff

    .line 64
    .local v3, "byte2":I
    add-int/lit8 p4, v7, 0x1

    .end local v7    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    shl-int/lit8 v8, v1, 0xc

    shl-int/lit8 v9, v2, 0x4

    or-int/2addr v8, v9

    ushr-int/lit8 v9, v3, 0x4

    or-int/2addr v8, v9

    aput v8, p3, v7

    .line 65
    add-int/lit8 v0, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v0    # "blocksOffset":I
    aget-byte v8, p1, p2

    and-int/lit16 v4, v8, 0xff

    .line 66
    .local v4, "byte3":I
    add-int/lit8 p2, v0, 0x1

    .end local v0    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v8, p1, v0

    and-int/lit16 v5, v8, 0xff

    .line 67
    .local v5, "byte4":I
    add-int/lit8 v7, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v7    # "valuesOffset":I
    and-int/lit8 v8, v3, 0xf

    shl-int/lit8 v8, v8, 0x10

    shl-int/lit8 v9, v4, 0x8

    or-int/2addr v8, v9

    or-int/2addr v8, v5

    aput v8, p3, p4

    .line 60
    add-int/lit8 v6, v6, 0x1

    move v0, p2

    .end local p2    # "blocksOffset":I
    .restart local v0    # "blocksOffset":I
    goto :goto_0
.end method

.method public decode([BI[JII)V
    .locals 20
    .param p1, "blocks"    # [B
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [J
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 100
    const/4 v3, 0x0

    .local v3, "i":I
    move/from16 v14, p4

    .end local p4    # "valuesOffset":I
    .local v14, "valuesOffset":I
    move/from16 v2, p2

    .end local p2    # "blocksOffset":I
    .local v2, "blocksOffset":I
    :goto_0
    move/from16 v0, p5

    if-lt v3, v0, :cond_0

    .line 109
    return-void

    .line 101
    :cond_0
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v15, p1, v2

    and-int/lit16 v15, v15, 0xff

    int-to-long v4, v15

    .line 102
    .local v4, "byte0":J
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v15, p1, p2

    and-int/lit16 v15, v15, 0xff

    int-to-long v6, v15

    .line 103
    .local v6, "byte1":J
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v15, p1, v2

    and-int/lit16 v15, v15, 0xff

    int-to-long v8, v15

    .line 104
    .local v8, "byte2":J
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0xc

    shl-long v16, v4, v15

    const/4 v15, 0x4

    shl-long v18, v6, v15

    or-long v16, v16, v18

    const/4 v15, 0x4

    ushr-long v18, v8, v15

    or-long v16, v16, v18

    aput-wide v16, p3, v14

    .line 105
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v15, p1, p2

    and-int/lit16 v15, v15, 0xff

    int-to-long v10, v15

    .line 106
    .local v10, "byte3":J
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v15, p1, v2

    and-int/lit16 v15, v15, 0xff

    int-to-long v12, v15

    .line 107
    .local v12, "byte4":J
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const-wide/16 v16, 0xf

    and-long v16, v16, v8

    const/16 v15, 0x10

    shl-long v16, v16, v15

    const/16 v15, 0x8

    shl-long v18, v10, v15

    or-long v16, v16, v18

    or-long v16, v16, v12

    aput-wide v16, p3, p4

    .line 100
    add-int/lit8 v3, v3, 0x1

    move/from16 v2, p2

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    goto :goto_0
.end method

.method public decode([JI[III)V
    .locals 20
    .param p1, "blocks"    # [J
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [I
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 33
    const/4 v13, 0x0

    .local v13, "i":I
    move/from16 v14, p4

    .end local p4    # "valuesOffset":I
    .local v14, "valuesOffset":I
    move/from16 v12, p2

    .end local p2    # "blocksOffset":I
    .local v12, "blocksOffset":I
    :goto_0
    move/from16 v0, p5

    if-lt v13, v0, :cond_0

    .line 56
    return-void

    .line 34
    :cond_0
    add-int/lit8 p2, v12, 0x1

    .end local v12    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v2, p1, v12

    .line 35
    .local v2, "block0":J
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0x2c

    ushr-long v16, v2, v15

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, v14

    .line 36
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0x18

    ushr-long v16, v2, v15

    const-wide/32 v18, 0xfffff

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, p4

    .line 37
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/4 v15, 0x4

    ushr-long v16, v2, v15

    const-wide/32 v18, 0xfffff

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, v14

    .line 38
    add-int/lit8 v12, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v12    # "blocksOffset":I
    aget-wide v4, p1, p2

    .line 39
    .local v4, "block1":J
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const-wide/16 v16, 0xf

    and-long v16, v16, v2

    const/16 v15, 0x10

    shl-long v16, v16, v15

    const/16 v15, 0x30

    ushr-long v18, v4, v15

    or-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, p4

    .line 40
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0x1c

    ushr-long v16, v4, v15

    const-wide/32 v18, 0xfffff

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, v14

    .line 41
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0x8

    ushr-long v16, v4, v15

    const-wide/32 v18, 0xfffff

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, p4

    .line 42
    add-int/lit8 p2, v12, 0x1

    .end local v12    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v6, p1, v12

    .line 43
    .local v6, "block2":J
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v16, 0xff

    and-long v16, v16, v4

    const/16 v15, 0xc

    shl-long v16, v16, v15

    const/16 v15, 0x34

    ushr-long v18, v6, v15

    or-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, v14

    .line 44
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0x20

    ushr-long v16, v6, v15

    const-wide/32 v18, 0xfffff

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, p4

    .line 45
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0xc

    ushr-long v16, v6, v15

    const-wide/32 v18, 0xfffff

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, v14

    .line 46
    add-int/lit8 v12, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v12    # "blocksOffset":I
    aget-wide v8, p1, p2

    .line 47
    .local v8, "block3":J
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const-wide/16 v16, 0xfff

    and-long v16, v16, v6

    const/16 v15, 0x8

    shl-long v16, v16, v15

    const/16 v15, 0x38

    ushr-long v18, v8, v15

    or-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, p4

    .line 48
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0x24

    ushr-long v16, v8, v15

    const-wide/32 v18, 0xfffff

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, v14

    .line 49
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0x10

    ushr-long v16, v8, v15

    const-wide/32 v18, 0xfffff

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, p4

    .line 50
    add-int/lit8 p2, v12, 0x1

    .end local v12    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v10, p1, v12

    .line 51
    .local v10, "block4":J
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/32 v16, 0xffff

    and-long v16, v16, v8

    const/4 v15, 0x4

    shl-long v16, v16, v15

    const/16 v15, 0x3c

    ushr-long v18, v10, v15

    or-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, v14

    .line 52
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0x28

    ushr-long v16, v10, v15

    const-wide/32 v18, 0xfffff

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, p4

    .line 53
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0x14

    ushr-long v16, v10, v15

    const-wide/32 v18, 0xfffff

    and-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, v14

    .line 54
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const-wide/32 v16, 0xfffff

    and-long v16, v16, v10

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, p3, p4

    .line 33
    add-int/lit8 v13, v13, 0x1

    move/from16 v12, p2

    .end local p2    # "blocksOffset":I
    .restart local v12    # "blocksOffset":I
    goto/16 :goto_0
.end method

.method public decode([JI[JII)V
    .locals 20
    .param p1, "blocks"    # [J
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [J
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 73
    const/4 v13, 0x0

    .local v13, "i":I
    move/from16 v14, p4

    .end local p4    # "valuesOffset":I
    .local v14, "valuesOffset":I
    move/from16 v12, p2

    .end local p2    # "blocksOffset":I
    .local v12, "blocksOffset":I
    :goto_0
    move/from16 v0, p5

    if-lt v13, v0, :cond_0

    .line 96
    return-void

    .line 74
    :cond_0
    add-int/lit8 p2, v12, 0x1

    .end local v12    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v2, p1, v12

    .line 75
    .local v2, "block0":J
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0x2c

    ushr-long v16, v2, v15

    aput-wide v16, p3, v14

    .line 76
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0x18

    ushr-long v16, v2, v15

    const-wide/32 v18, 0xfffff

    and-long v16, v16, v18

    aput-wide v16, p3, p4

    .line 77
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/4 v15, 0x4

    ushr-long v16, v2, v15

    const-wide/32 v18, 0xfffff

    and-long v16, v16, v18

    aput-wide v16, p3, v14

    .line 78
    add-int/lit8 v12, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v12    # "blocksOffset":I
    aget-wide v4, p1, p2

    .line 79
    .local v4, "block1":J
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const-wide/16 v16, 0xf

    and-long v16, v16, v2

    const/16 v15, 0x10

    shl-long v16, v16, v15

    const/16 v15, 0x30

    ushr-long v18, v4, v15

    or-long v16, v16, v18

    aput-wide v16, p3, p4

    .line 80
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0x1c

    ushr-long v16, v4, v15

    const-wide/32 v18, 0xfffff

    and-long v16, v16, v18

    aput-wide v16, p3, v14

    .line 81
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0x8

    ushr-long v16, v4, v15

    const-wide/32 v18, 0xfffff

    and-long v16, v16, v18

    aput-wide v16, p3, p4

    .line 82
    add-int/lit8 p2, v12, 0x1

    .end local v12    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v6, p1, v12

    .line 83
    .local v6, "block2":J
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v16, 0xff

    and-long v16, v16, v4

    const/16 v15, 0xc

    shl-long v16, v16, v15

    const/16 v15, 0x34

    ushr-long v18, v6, v15

    or-long v16, v16, v18

    aput-wide v16, p3, v14

    .line 84
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0x20

    ushr-long v16, v6, v15

    const-wide/32 v18, 0xfffff

    and-long v16, v16, v18

    aput-wide v16, p3, p4

    .line 85
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0xc

    ushr-long v16, v6, v15

    const-wide/32 v18, 0xfffff

    and-long v16, v16, v18

    aput-wide v16, p3, v14

    .line 86
    add-int/lit8 v12, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v12    # "blocksOffset":I
    aget-wide v8, p1, p2

    .line 87
    .local v8, "block3":J
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const-wide/16 v16, 0xfff

    and-long v16, v16, v6

    const/16 v15, 0x8

    shl-long v16, v16, v15

    const/16 v15, 0x38

    ushr-long v18, v8, v15

    or-long v16, v16, v18

    aput-wide v16, p3, p4

    .line 88
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0x24

    ushr-long v16, v8, v15

    const-wide/32 v18, 0xfffff

    and-long v16, v16, v18

    aput-wide v16, p3, v14

    .line 89
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0x10

    ushr-long v16, v8, v15

    const-wide/32 v18, 0xfffff

    and-long v16, v16, v18

    aput-wide v16, p3, p4

    .line 90
    add-int/lit8 p2, v12, 0x1

    .end local v12    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v10, p1, v12

    .line 91
    .local v10, "block4":J
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/32 v16, 0xffff

    and-long v16, v16, v8

    const/4 v15, 0x4

    shl-long v16, v16, v15

    const/16 v15, 0x3c

    ushr-long v18, v10, v15

    or-long v16, v16, v18

    aput-wide v16, p3, v14

    .line 92
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const/16 v15, 0x28

    ushr-long v16, v10, v15

    const-wide/32 v18, 0xfffff

    and-long v16, v16, v18

    aput-wide v16, p3, p4

    .line 93
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v15, 0x14

    ushr-long v16, v10, v15

    const-wide/32 v18, 0xfffff

    and-long v16, v16, v18

    aput-wide v16, p3, v14

    .line 94
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    const-wide/32 v16, 0xfffff

    and-long v16, v16, v10

    aput-wide v16, p3, p4

    .line 73
    add-int/lit8 v13, v13, 0x1

    move/from16 v12, p2

    .end local p2    # "blocksOffset":I
    .restart local v12    # "blocksOffset":I
    goto/16 :goto_0
.end method
