.class Lorg/apache/lucene/util/BytesRefHash$1;
.super Lorg/apache/lucene/util/SorterTemplate;
.source "BytesRefHash.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/util/BytesRefHash;->sort(Ljava/util/Comparator;)[I
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final pivot:Lorg/apache/lucene/util/BytesRef;

.field private final scratch1:Lorg/apache/lucene/util/BytesRef;

.field private final scratch2:Lorg/apache/lucene/util/BytesRef;

.field final synthetic this$0:Lorg/apache/lucene/util/BytesRefHash;

.field private final synthetic val$comp:Ljava/util/Comparator;

.field private final synthetic val$compact:[I


# direct methods
.method constructor <init>(Lorg/apache/lucene/util/BytesRefHash;[ILjava/util/Comparator;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/util/BytesRefHash$1;->this$0:Lorg/apache/lucene/util/BytesRefHash;

    iput-object p2, p0, Lorg/apache/lucene/util/BytesRefHash$1;->val$compact:[I

    iput-object p3, p0, Lorg/apache/lucene/util/BytesRefHash$1;->val$comp:Ljava/util/Comparator;

    .line 166
    invoke-direct {p0}, Lorg/apache/lucene/util/SorterTemplate;-><init>()V

    .line 198
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/util/BytesRefHash$1;->pivot:Lorg/apache/lucene/util/BytesRef;

    .line 199
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/util/BytesRefHash$1;->scratch1:Lorg/apache/lucene/util/BytesRef;

    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/util/BytesRefHash$1;->scratch2:Lorg/apache/lucene/util/BytesRef;

    return-void
.end method


# virtual methods
.method protected compare(II)I
    .locals 5
    .param p1, "i"    # I
    .param p2, "j"    # I

    .prologue
    .line 176
    iget-object v2, p0, Lorg/apache/lucene/util/BytesRefHash$1;->val$compact:[I

    aget v0, v2, p1

    .local v0, "id1":I
    iget-object v2, p0, Lorg/apache/lucene/util/BytesRefHash$1;->val$compact:[I

    aget v1, v2, p2

    .line 177
    .local v1, "id2":I
    sget-boolean v2, Lorg/apache/lucene/util/BytesRefHash;->$assertionsDisabled:Z

    if-nez v2, :cond_1

    iget-object v2, p0, Lorg/apache/lucene/util/BytesRefHash$1;->this$0:Lorg/apache/lucene/util/BytesRefHash;

    iget-object v2, v2, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    array-length v2, v2

    if-le v2, v0, :cond_0

    iget-object v2, p0, Lorg/apache/lucene/util/BytesRefHash$1;->this$0:Lorg/apache/lucene/util/BytesRefHash;

    iget-object v2, v2, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    array-length v2, v2

    if-gt v2, v1, :cond_1

    :cond_0
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 178
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/util/BytesRefHash$1;->this$0:Lorg/apache/lucene/util/BytesRefHash;

    iget-object v2, v2, Lorg/apache/lucene/util/BytesRefHash;->pool:Lorg/apache/lucene/util/ByteBlockPool;

    iget-object v3, p0, Lorg/apache/lucene/util/BytesRefHash$1;->scratch1:Lorg/apache/lucene/util/BytesRef;

    iget-object v4, p0, Lorg/apache/lucene/util/BytesRefHash$1;->this$0:Lorg/apache/lucene/util/BytesRefHash;

    iget-object v4, v4, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    aget v4, v4, v0

    invoke-virtual {v2, v3, v4}, Lorg/apache/lucene/util/ByteBlockPool;->setBytesRef(Lorg/apache/lucene/util/BytesRef;I)V

    .line 179
    iget-object v2, p0, Lorg/apache/lucene/util/BytesRefHash$1;->this$0:Lorg/apache/lucene/util/BytesRefHash;

    iget-object v2, v2, Lorg/apache/lucene/util/BytesRefHash;->pool:Lorg/apache/lucene/util/ByteBlockPool;

    iget-object v3, p0, Lorg/apache/lucene/util/BytesRefHash$1;->scratch2:Lorg/apache/lucene/util/BytesRef;

    iget-object v4, p0, Lorg/apache/lucene/util/BytesRefHash$1;->this$0:Lorg/apache/lucene/util/BytesRefHash;

    iget-object v4, v4, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    aget v4, v4, v1

    invoke-virtual {v2, v3, v4}, Lorg/apache/lucene/util/ByteBlockPool;->setBytesRef(Lorg/apache/lucene/util/BytesRef;I)V

    .line 180
    iget-object v2, p0, Lorg/apache/lucene/util/BytesRefHash$1;->val$comp:Ljava/util/Comparator;

    iget-object v3, p0, Lorg/apache/lucene/util/BytesRefHash$1;->scratch1:Lorg/apache/lucene/util/BytesRef;

    iget-object v4, p0, Lorg/apache/lucene/util/BytesRefHash$1;->scratch2:Lorg/apache/lucene/util/BytesRef;

    invoke-interface {v2, v3, v4}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v2

    return v2
.end method

.method protected comparePivot(I)I
    .locals 4
    .param p1, "j"    # I

    .prologue
    .line 192
    iget-object v1, p0, Lorg/apache/lucene/util/BytesRefHash$1;->val$compact:[I

    aget v0, v1, p1

    .line 193
    .local v0, "id":I
    sget-boolean v1, Lorg/apache/lucene/util/BytesRefHash;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/util/BytesRefHash$1;->this$0:Lorg/apache/lucene/util/BytesRefHash;

    iget-object v1, v1, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    array-length v1, v1

    if-gt v1, v0, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 194
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/util/BytesRefHash$1;->this$0:Lorg/apache/lucene/util/BytesRefHash;

    iget-object v1, v1, Lorg/apache/lucene/util/BytesRefHash;->pool:Lorg/apache/lucene/util/ByteBlockPool;

    iget-object v2, p0, Lorg/apache/lucene/util/BytesRefHash$1;->scratch2:Lorg/apache/lucene/util/BytesRef;

    iget-object v3, p0, Lorg/apache/lucene/util/BytesRefHash$1;->this$0:Lorg/apache/lucene/util/BytesRefHash;

    iget-object v3, v3, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    aget v3, v3, v0

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/util/ByteBlockPool;->setBytesRef(Lorg/apache/lucene/util/BytesRef;I)V

    .line 195
    iget-object v1, p0, Lorg/apache/lucene/util/BytesRefHash$1;->val$comp:Ljava/util/Comparator;

    iget-object v2, p0, Lorg/apache/lucene/util/BytesRefHash$1;->pivot:Lorg/apache/lucene/util/BytesRef;

    iget-object v3, p0, Lorg/apache/lucene/util/BytesRefHash$1;->scratch2:Lorg/apache/lucene/util/BytesRef;

    invoke-interface {v1, v2, v3}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v1

    return v1
.end method

.method protected setPivot(I)V
    .locals 4
    .param p1, "i"    # I

    .prologue
    .line 185
    iget-object v1, p0, Lorg/apache/lucene/util/BytesRefHash$1;->val$compact:[I

    aget v0, v1, p1

    .line 186
    .local v0, "id":I
    sget-boolean v1, Lorg/apache/lucene/util/BytesRefHash;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/util/BytesRefHash$1;->this$0:Lorg/apache/lucene/util/BytesRefHash;

    iget-object v1, v1, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    array-length v1, v1

    if-gt v1, v0, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 187
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/util/BytesRefHash$1;->this$0:Lorg/apache/lucene/util/BytesRefHash;

    iget-object v1, v1, Lorg/apache/lucene/util/BytesRefHash;->pool:Lorg/apache/lucene/util/ByteBlockPool;

    iget-object v2, p0, Lorg/apache/lucene/util/BytesRefHash$1;->pivot:Lorg/apache/lucene/util/BytesRef;

    iget-object v3, p0, Lorg/apache/lucene/util/BytesRefHash$1;->this$0:Lorg/apache/lucene/util/BytesRefHash;

    iget-object v3, v3, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    aget v3, v3, v0

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/util/ByteBlockPool;->setBytesRef(Lorg/apache/lucene/util/BytesRef;I)V

    .line 188
    return-void
.end method

.method protected swap(II)V
    .locals 3
    .param p1, "i"    # I
    .param p2, "j"    # I

    .prologue
    .line 169
    iget-object v1, p0, Lorg/apache/lucene/util/BytesRefHash$1;->val$compact:[I

    aget v0, v1, p1

    .line 170
    .local v0, "o":I
    iget-object v1, p0, Lorg/apache/lucene/util/BytesRefHash$1;->val$compact:[I

    iget-object v2, p0, Lorg/apache/lucene/util/BytesRefHash$1;->val$compact:[I

    aget v2, v2, p2

    aput v2, v1, p1

    .line 171
    iget-object v1, p0, Lorg/apache/lucene/util/BytesRefHash$1;->val$compact:[I

    aput v0, v1, p2

    .line 172
    return-void
.end method
