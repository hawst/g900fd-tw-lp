.class final Lorg/apache/lucene/util/packed/BulkOperationPacked23;
.super Lorg/apache/lucene/util/packed/BulkOperationPacked;
.source "BulkOperationPacked23.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    const/16 v0, 0x17

    invoke-direct {p0, v0}, Lorg/apache/lucene/util/packed/BulkOperationPacked;-><init>(I)V

    .line 29
    return-void
.end method


# virtual methods
.method public decode([BI[III)V
    .locals 30
    .param p1, "blocks"    # [B
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [I
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 126
    const/16 v26, 0x0

    .local v26, "i":I
    move/from16 v27, p4

    .end local p4    # "valuesOffset":I
    .local v27, "valuesOffset":I
    move/from16 v2, p2

    .end local p2    # "blocksOffset":I
    .local v2, "blocksOffset":I
    :goto_0
    move/from16 v0, v26

    move/from16 v1, p5

    if-lt v0, v1, :cond_0

    .line 159
    return-void

    .line 127
    :cond_0
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v28, p1, v2

    move/from16 v0, v28

    and-int/lit16 v3, v0, 0xff

    .line 128
    .local v3, "byte0":I
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v28, p1, p2

    move/from16 v0, v28

    and-int/lit16 v4, v0, 0xff

    .line 129
    .local v4, "byte1":I
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v28, p1, v2

    move/from16 v0, v28

    and-int/lit16 v15, v0, 0xff

    .line 130
    .local v15, "byte2":I
    add-int/lit8 p4, v27, 0x1

    .end local v27    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    shl-int/lit8 v28, v3, 0xf

    shl-int/lit8 v29, v4, 0x7

    or-int v28, v28, v29

    ushr-int/lit8 v29, v15, 0x1

    or-int v28, v28, v29

    aput v28, p3, v27

    .line 131
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v28, p1, p2

    move/from16 v0, v28

    and-int/lit16 v0, v0, 0xff

    move/from16 v19, v0

    .line 132
    .local v19, "byte3":I
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v28, p1, v2

    move/from16 v0, v28

    and-int/lit16 v0, v0, 0xff

    move/from16 v20, v0

    .line 133
    .local v20, "byte4":I
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v28, p1, p2

    move/from16 v0, v28

    and-int/lit16 v0, v0, 0xff

    move/from16 v21, v0

    .line 134
    .local v21, "byte5":I
    add-int/lit8 v27, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v27    # "valuesOffset":I
    and-int/lit8 v28, v15, 0x1

    shl-int/lit8 v28, v28, 0x16

    shl-int/lit8 v29, v19, 0xe

    or-int v28, v28, v29

    shl-int/lit8 v29, v20, 0x6

    or-int v28, v28, v29

    ushr-int/lit8 v29, v21, 0x2

    or-int v28, v28, v29

    aput v28, p3, p4

    .line 135
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v28, p1, v2

    move/from16 v0, v28

    and-int/lit16 v0, v0, 0xff

    move/from16 v22, v0

    .line 136
    .local v22, "byte6":I
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v28, p1, p2

    move/from16 v0, v28

    and-int/lit16 v0, v0, 0xff

    move/from16 v23, v0

    .line 137
    .local v23, "byte7":I
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v28, p1, v2

    move/from16 v0, v28

    and-int/lit16 v0, v0, 0xff

    move/from16 v24, v0

    .line 138
    .local v24, "byte8":I
    add-int/lit8 p4, v27, 0x1

    .end local v27    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    and-int/lit8 v28, v21, 0x3

    shl-int/lit8 v28, v28, 0x15

    shl-int/lit8 v29, v22, 0xd

    or-int v28, v28, v29

    shl-int/lit8 v29, v23, 0x5

    or-int v28, v28, v29

    ushr-int/lit8 v29, v24, 0x3

    or-int v28, v28, v29

    aput v28, p3, v27

    .line 139
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v28, p1, p2

    move/from16 v0, v28

    and-int/lit16 v0, v0, 0xff

    move/from16 v25, v0

    .line 140
    .local v25, "byte9":I
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v28, p1, v2

    move/from16 v0, v28

    and-int/lit16 v5, v0, 0xff

    .line 141
    .local v5, "byte10":I
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v28, p1, p2

    move/from16 v0, v28

    and-int/lit16 v6, v0, 0xff

    .line 142
    .local v6, "byte11":I
    add-int/lit8 v27, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v27    # "valuesOffset":I
    and-int/lit8 v28, v24, 0x7

    shl-int/lit8 v28, v28, 0x14

    shl-int/lit8 v29, v25, 0xc

    or-int v28, v28, v29

    shl-int/lit8 v29, v5, 0x4

    or-int v28, v28, v29

    ushr-int/lit8 v29, v6, 0x4

    or-int v28, v28, v29

    aput v28, p3, p4

    .line 143
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v28, p1, v2

    move/from16 v0, v28

    and-int/lit16 v7, v0, 0xff

    .line 144
    .local v7, "byte12":I
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v28, p1, p2

    move/from16 v0, v28

    and-int/lit16 v8, v0, 0xff

    .line 145
    .local v8, "byte13":I
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v28, p1, v2

    move/from16 v0, v28

    and-int/lit16 v9, v0, 0xff

    .line 146
    .local v9, "byte14":I
    add-int/lit8 p4, v27, 0x1

    .end local v27    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    and-int/lit8 v28, v6, 0xf

    shl-int/lit8 v28, v28, 0x13

    shl-int/lit8 v29, v7, 0xb

    or-int v28, v28, v29

    shl-int/lit8 v29, v8, 0x3

    or-int v28, v28, v29

    ushr-int/lit8 v29, v9, 0x5

    or-int v28, v28, v29

    aput v28, p3, v27

    .line 147
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v28, p1, p2

    move/from16 v0, v28

    and-int/lit16 v10, v0, 0xff

    .line 148
    .local v10, "byte15":I
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v28, p1, v2

    move/from16 v0, v28

    and-int/lit16 v11, v0, 0xff

    .line 149
    .local v11, "byte16":I
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v28, p1, p2

    move/from16 v0, v28

    and-int/lit16 v12, v0, 0xff

    .line 150
    .local v12, "byte17":I
    add-int/lit8 v27, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v27    # "valuesOffset":I
    and-int/lit8 v28, v9, 0x1f

    shl-int/lit8 v28, v28, 0x12

    shl-int/lit8 v29, v10, 0xa

    or-int v28, v28, v29

    shl-int/lit8 v29, v11, 0x2

    or-int v28, v28, v29

    ushr-int/lit8 v29, v12, 0x6

    or-int v28, v28, v29

    aput v28, p3, p4

    .line 151
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v28, p1, v2

    move/from16 v0, v28

    and-int/lit16 v13, v0, 0xff

    .line 152
    .local v13, "byte18":I
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v28, p1, p2

    move/from16 v0, v28

    and-int/lit16 v14, v0, 0xff

    .line 153
    .local v14, "byte19":I
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v28, p1, v2

    move/from16 v0, v28

    and-int/lit16 v0, v0, 0xff

    move/from16 v16, v0

    .line 154
    .local v16, "byte20":I
    add-int/lit8 p4, v27, 0x1

    .end local v27    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    and-int/lit8 v28, v12, 0x3f

    shl-int/lit8 v28, v28, 0x11

    shl-int/lit8 v29, v13, 0x9

    or-int v28, v28, v29

    shl-int/lit8 v29, v14, 0x1

    or-int v28, v28, v29

    ushr-int/lit8 v29, v16, 0x7

    or-int v28, v28, v29

    aput v28, p3, v27

    .line 155
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v28, p1, p2

    move/from16 v0, v28

    and-int/lit16 v0, v0, 0xff

    move/from16 v17, v0

    .line 156
    .local v17, "byte21":I
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v28, p1, v2

    move/from16 v0, v28

    and-int/lit16 v0, v0, 0xff

    move/from16 v18, v0

    .line 157
    .local v18, "byte22":I
    add-int/lit8 v27, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v27    # "valuesOffset":I
    and-int/lit8 v28, v16, 0x7f

    shl-int/lit8 v28, v28, 0x10

    shl-int/lit8 v29, v17, 0x8

    or-int v28, v28, v29

    or-int v28, v28, v18

    aput v28, p3, p4

    .line 126
    add-int/lit8 v26, v26, 0x1

    move/from16 v2, p2

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    goto/16 :goto_0
.end method

.method public decode([BI[JII)V
    .locals 56
    .param p1, "blocks"    # [B
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [J
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 256
    const/4 v3, 0x0

    .local v3, "i":I
    move/from16 v50, p4

    .end local p4    # "valuesOffset":I
    .local v50, "valuesOffset":I
    move/from16 v2, p2

    .end local p2    # "blocksOffset":I
    .local v2, "blocksOffset":I
    :goto_0
    move/from16 v0, p5

    if-lt v3, v0, :cond_0

    .line 289
    return-void

    .line 257
    :cond_0
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v51, p1, v2

    move/from16 v0, v51

    and-int/lit16 v0, v0, 0xff

    move/from16 v51, v0

    move/from16 v0, v51

    int-to-long v4, v0

    .line 258
    .local v4, "byte0":J
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v51, p1, p2

    move/from16 v0, v51

    and-int/lit16 v0, v0, 0xff

    move/from16 v51, v0

    move/from16 v0, v51

    int-to-long v6, v0

    .line 259
    .local v6, "byte1":J
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v51, p1, v2

    move/from16 v0, v51

    and-int/lit16 v0, v0, 0xff

    move/from16 v51, v0

    move/from16 v0, v51

    int-to-long v0, v0

    move-wide/from16 v28, v0

    .line 260
    .local v28, "byte2":J
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v51, 0xf

    shl-long v52, v4, v51

    const/16 v51, 0x7

    shl-long v54, v6, v51

    or-long v52, v52, v54

    const/16 v51, 0x1

    ushr-long v54, v28, v51

    or-long v52, v52, v54

    aput-wide v52, p3, v50

    .line 261
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v51, p1, p2

    move/from16 v0, v51

    and-int/lit16 v0, v0, 0xff

    move/from16 v51, v0

    move/from16 v0, v51

    int-to-long v0, v0

    move-wide/from16 v36, v0

    .line 262
    .local v36, "byte3":J
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v51, p1, v2

    move/from16 v0, v51

    and-int/lit16 v0, v0, 0xff

    move/from16 v51, v0

    move/from16 v0, v51

    int-to-long v0, v0

    move-wide/from16 v38, v0

    .line 263
    .local v38, "byte4":J
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v51, p1, p2

    move/from16 v0, v51

    and-int/lit16 v0, v0, 0xff

    move/from16 v51, v0

    move/from16 v0, v51

    int-to-long v0, v0

    move-wide/from16 v40, v0

    .line 264
    .local v40, "byte5":J
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const-wide/16 v52, 0x1

    and-long v52, v52, v28

    const/16 v51, 0x16

    shl-long v52, v52, v51

    const/16 v51, 0xe

    shl-long v54, v36, v51

    or-long v52, v52, v54

    const/16 v51, 0x6

    shl-long v54, v38, v51

    or-long v52, v52, v54

    const/16 v51, 0x2

    ushr-long v54, v40, v51

    or-long v52, v52, v54

    aput-wide v52, p3, p4

    .line 265
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v51, p1, v2

    move/from16 v0, v51

    and-int/lit16 v0, v0, 0xff

    move/from16 v51, v0

    move/from16 v0, v51

    int-to-long v0, v0

    move-wide/from16 v42, v0

    .line 266
    .local v42, "byte6":J
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v51, p1, p2

    move/from16 v0, v51

    and-int/lit16 v0, v0, 0xff

    move/from16 v51, v0

    move/from16 v0, v51

    int-to-long v0, v0

    move-wide/from16 v44, v0

    .line 267
    .local v44, "byte7":J
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v51, p1, v2

    move/from16 v0, v51

    and-int/lit16 v0, v0, 0xff

    move/from16 v51, v0

    move/from16 v0, v51

    int-to-long v0, v0

    move-wide/from16 v46, v0

    .line 268
    .local v46, "byte8":J
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v52, 0x3

    and-long v52, v52, v40

    const/16 v51, 0x15

    shl-long v52, v52, v51

    const/16 v51, 0xd

    shl-long v54, v42, v51

    or-long v52, v52, v54

    const/16 v51, 0x5

    shl-long v54, v44, v51

    or-long v52, v52, v54

    const/16 v51, 0x3

    ushr-long v54, v46, v51

    or-long v52, v52, v54

    aput-wide v52, p3, v50

    .line 269
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v51, p1, p2

    move/from16 v0, v51

    and-int/lit16 v0, v0, 0xff

    move/from16 v51, v0

    move/from16 v0, v51

    int-to-long v0, v0

    move-wide/from16 v48, v0

    .line 270
    .local v48, "byte9":J
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v51, p1, v2

    move/from16 v0, v51

    and-int/lit16 v0, v0, 0xff

    move/from16 v51, v0

    move/from16 v0, v51

    int-to-long v8, v0

    .line 271
    .local v8, "byte10":J
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v51, p1, p2

    move/from16 v0, v51

    and-int/lit16 v0, v0, 0xff

    move/from16 v51, v0

    move/from16 v0, v51

    int-to-long v10, v0

    .line 272
    .local v10, "byte11":J
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const-wide/16 v52, 0x7

    and-long v52, v52, v46

    const/16 v51, 0x14

    shl-long v52, v52, v51

    const/16 v51, 0xc

    shl-long v54, v48, v51

    or-long v52, v52, v54

    const/16 v51, 0x4

    shl-long v54, v8, v51

    or-long v52, v52, v54

    const/16 v51, 0x4

    ushr-long v54, v10, v51

    or-long v52, v52, v54

    aput-wide v52, p3, p4

    .line 273
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v51, p1, v2

    move/from16 v0, v51

    and-int/lit16 v0, v0, 0xff

    move/from16 v51, v0

    move/from16 v0, v51

    int-to-long v12, v0

    .line 274
    .local v12, "byte12":J
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v51, p1, p2

    move/from16 v0, v51

    and-int/lit16 v0, v0, 0xff

    move/from16 v51, v0

    move/from16 v0, v51

    int-to-long v14, v0

    .line 275
    .local v14, "byte13":J
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v51, p1, v2

    move/from16 v0, v51

    and-int/lit16 v0, v0, 0xff

    move/from16 v51, v0

    move/from16 v0, v51

    int-to-long v0, v0

    move-wide/from16 v16, v0

    .line 276
    .local v16, "byte14":J
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v52, 0xf

    and-long v52, v52, v10

    const/16 v51, 0x13

    shl-long v52, v52, v51

    const/16 v51, 0xb

    shl-long v54, v12, v51

    or-long v52, v52, v54

    const/16 v51, 0x3

    shl-long v54, v14, v51

    or-long v52, v52, v54

    const/16 v51, 0x5

    ushr-long v54, v16, v51

    or-long v52, v52, v54

    aput-wide v52, p3, v50

    .line 277
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v51, p1, p2

    move/from16 v0, v51

    and-int/lit16 v0, v0, 0xff

    move/from16 v51, v0

    move/from16 v0, v51

    int-to-long v0, v0

    move-wide/from16 v18, v0

    .line 278
    .local v18, "byte15":J
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v51, p1, v2

    move/from16 v0, v51

    and-int/lit16 v0, v0, 0xff

    move/from16 v51, v0

    move/from16 v0, v51

    int-to-long v0, v0

    move-wide/from16 v20, v0

    .line 279
    .local v20, "byte16":J
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v51, p1, p2

    move/from16 v0, v51

    and-int/lit16 v0, v0, 0xff

    move/from16 v51, v0

    move/from16 v0, v51

    int-to-long v0, v0

    move-wide/from16 v22, v0

    .line 280
    .local v22, "byte17":J
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const-wide/16 v52, 0x1f

    and-long v52, v52, v16

    const/16 v51, 0x12

    shl-long v52, v52, v51

    const/16 v51, 0xa

    shl-long v54, v18, v51

    or-long v52, v52, v54

    const/16 v51, 0x2

    shl-long v54, v20, v51

    or-long v52, v52, v54

    const/16 v51, 0x6

    ushr-long v54, v22, v51

    or-long v52, v52, v54

    aput-wide v52, p3, p4

    .line 281
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v51, p1, v2

    move/from16 v0, v51

    and-int/lit16 v0, v0, 0xff

    move/from16 v51, v0

    move/from16 v0, v51

    int-to-long v0, v0

    move-wide/from16 v24, v0

    .line 282
    .local v24, "byte18":J
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v51, p1, p2

    move/from16 v0, v51

    and-int/lit16 v0, v0, 0xff

    move/from16 v51, v0

    move/from16 v0, v51

    int-to-long v0, v0

    move-wide/from16 v26, v0

    .line 283
    .local v26, "byte19":J
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v51, p1, v2

    move/from16 v0, v51

    and-int/lit16 v0, v0, 0xff

    move/from16 v51, v0

    move/from16 v0, v51

    int-to-long v0, v0

    move-wide/from16 v30, v0

    .line 284
    .local v30, "byte20":J
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v52, 0x3f

    and-long v52, v52, v22

    const/16 v51, 0x11

    shl-long v52, v52, v51

    const/16 v51, 0x9

    shl-long v54, v24, v51

    or-long v52, v52, v54

    const/16 v51, 0x1

    shl-long v54, v26, v51

    or-long v52, v52, v54

    const/16 v51, 0x7

    ushr-long v54, v30, v51

    or-long v52, v52, v54

    aput-wide v52, p3, v50

    .line 285
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v51, p1, p2

    move/from16 v0, v51

    and-int/lit16 v0, v0, 0xff

    move/from16 v51, v0

    move/from16 v0, v51

    int-to-long v0, v0

    move-wide/from16 v32, v0

    .line 286
    .local v32, "byte21":J
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v51, p1, v2

    move/from16 v0, v51

    and-int/lit16 v0, v0, 0xff

    move/from16 v51, v0

    move/from16 v0, v51

    int-to-long v0, v0

    move-wide/from16 v34, v0

    .line 287
    .local v34, "byte22":J
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const-wide/16 v52, 0x7f

    and-long v52, v52, v30

    const/16 v51, 0x10

    shl-long v52, v52, v51

    const/16 v51, 0x8

    shl-long v54, v32, v51

    or-long v52, v52, v54

    or-long v52, v52, v34

    aput-wide v52, p3, p4

    .line 256
    add-int/lit8 v3, v3, 0x1

    move/from16 v2, p2

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    goto/16 :goto_0
.end method

.method public decode([JI[III)V
    .locals 56
    .param p1, "blocks"    # [J
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [I
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 33
    const/16 v49, 0x0

    .local v49, "i":I
    move/from16 v50, p4

    .end local p4    # "valuesOffset":I
    .local v50, "valuesOffset":I
    move/from16 v48, p2

    .end local p2    # "blocksOffset":I
    .local v48, "blocksOffset":I
    :goto_0
    move/from16 v0, v49

    move/from16 v1, p5

    if-lt v0, v1, :cond_0

    .line 122
    return-void

    .line 34
    :cond_0
    add-int/lit8 p2, v48, 0x1

    .end local v48    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v2, p1, v48

    .line 35
    .local v2, "block0":J
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v51, 0x29

    ushr-long v52, v2, v51

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, v50

    .line 36
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const/16 v51, 0x12

    ushr-long v52, v2, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, p4

    .line 37
    add-int/lit8 v48, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v48    # "blocksOffset":I
    aget-wide v4, p1, p2

    .line 38
    .local v4, "block1":J
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/32 v52, 0x3ffff

    and-long v52, v52, v2

    const/16 v51, 0x5

    shl-long v52, v52, v51

    const/16 v51, 0x3b

    ushr-long v54, v4, v51

    or-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, v50

    .line 39
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const/16 v51, 0x24

    ushr-long v52, v4, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, p4

    .line 40
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v51, 0xd

    ushr-long v52, v4, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, v50

    .line 41
    add-int/lit8 p2, v48, 0x1

    .end local v48    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v26, p1, v48

    .line 42
    .local v26, "block2":J
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const-wide/16 v52, 0x1fff

    and-long v52, v52, v4

    const/16 v51, 0xa

    shl-long v52, v52, v51

    const/16 v51, 0x36

    ushr-long v54, v26, v51

    or-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, p4

    .line 43
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v51, 0x1f

    ushr-long v52, v26, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, v50

    .line 44
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const/16 v51, 0x8

    ushr-long v52, v26, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, p4

    .line 45
    add-int/lit8 v48, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v48    # "blocksOffset":I
    aget-wide v34, p1, p2

    .line 46
    .local v34, "block3":J
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v52, 0xff

    and-long v52, v52, v26

    const/16 v51, 0xf

    shl-long v52, v52, v51

    const/16 v51, 0x31

    ushr-long v54, v34, v51

    or-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, v50

    .line 47
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const/16 v51, 0x1a

    ushr-long v52, v34, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, p4

    .line 48
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v51, 0x3

    ushr-long v52, v34, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, v50

    .line 49
    add-int/lit8 p2, v48, 0x1

    .end local v48    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v36, p1, v48

    .line 50
    .local v36, "block4":J
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const-wide/16 v52, 0x7

    and-long v52, v52, v34

    const/16 v51, 0x14

    shl-long v52, v52, v51

    const/16 v51, 0x2c

    ushr-long v54, v36, v51

    or-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, p4

    .line 51
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v51, 0x15

    ushr-long v52, v36, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, v50

    .line 52
    add-int/lit8 v48, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v48    # "blocksOffset":I
    aget-wide v38, p1, p2

    .line 53
    .local v38, "block5":J
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const-wide/32 v52, 0x1fffff

    and-long v52, v52, v36

    const/16 v51, 0x2

    shl-long v52, v52, v51

    const/16 v51, 0x3e

    ushr-long v54, v38, v51

    or-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, p4

    .line 54
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v51, 0x27

    ushr-long v52, v38, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, v50

    .line 55
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const/16 v51, 0x10

    ushr-long v52, v38, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, p4

    .line 56
    add-int/lit8 p2, v48, 0x1

    .end local v48    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v40, p1, v48

    .line 57
    .local v40, "block6":J
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/32 v52, 0xffff

    and-long v52, v52, v38

    const/16 v51, 0x7

    shl-long v52, v52, v51

    const/16 v51, 0x39

    ushr-long v54, v40, v51

    or-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, v50

    .line 58
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const/16 v51, 0x22

    ushr-long v52, v40, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, p4

    .line 59
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v51, 0xb

    ushr-long v52, v40, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, v50

    .line 60
    add-int/lit8 v48, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v48    # "blocksOffset":I
    aget-wide v42, p1, p2

    .line 61
    .local v42, "block7":J
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const-wide/16 v52, 0x7ff

    and-long v52, v52, v40

    const/16 v51, 0xc

    shl-long v52, v52, v51

    const/16 v51, 0x34

    ushr-long v54, v42, v51

    or-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, p4

    .line 62
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v51, 0x1d

    ushr-long v52, v42, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, v50

    .line 63
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const/16 v51, 0x6

    ushr-long v52, v42, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, p4

    .line 64
    add-int/lit8 p2, v48, 0x1

    .end local v48    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v44, p1, v48

    .line 65
    .local v44, "block8":J
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v52, 0x3f

    and-long v52, v52, v42

    const/16 v51, 0x11

    shl-long v52, v52, v51

    const/16 v51, 0x2f

    ushr-long v54, v44, v51

    or-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, v50

    .line 66
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const/16 v51, 0x18

    ushr-long v52, v44, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, p4

    .line 67
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v51, 0x1

    ushr-long v52, v44, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, v50

    .line 68
    add-int/lit8 v48, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v48    # "blocksOffset":I
    aget-wide v46, p1, p2

    .line 69
    .local v46, "block9":J
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const-wide/16 v52, 0x1

    and-long v52, v52, v44

    const/16 v51, 0x16

    shl-long v52, v52, v51

    const/16 v51, 0x2a

    ushr-long v54, v46, v51

    or-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, p4

    .line 70
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v51, 0x13

    ushr-long v52, v46, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, v50

    .line 71
    add-int/lit8 p2, v48, 0x1

    .end local v48    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v6, p1, v48

    .line 72
    .local v6, "block10":J
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const-wide/32 v52, 0x7ffff

    and-long v52, v52, v46

    const/16 v51, 0x4

    shl-long v52, v52, v51

    const/16 v51, 0x3c

    ushr-long v54, v6, v51

    or-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, p4

    .line 73
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v51, 0x25

    ushr-long v52, v6, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, v50

    .line 74
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const/16 v51, 0xe

    ushr-long v52, v6, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, p4

    .line 75
    add-int/lit8 v48, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v48    # "blocksOffset":I
    aget-wide v8, p1, p2

    .line 76
    .local v8, "block11":J
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v52, 0x3fff

    and-long v52, v52, v6

    const/16 v51, 0x9

    shl-long v52, v52, v51

    const/16 v51, 0x37

    ushr-long v54, v8, v51

    or-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, v50

    .line 77
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const/16 v51, 0x20

    ushr-long v52, v8, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, p4

    .line 78
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v51, 0x9

    ushr-long v52, v8, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, v50

    .line 79
    add-int/lit8 p2, v48, 0x1

    .end local v48    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v10, p1, v48

    .line 80
    .local v10, "block12":J
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const-wide/16 v52, 0x1ff

    and-long v52, v52, v8

    const/16 v51, 0xe

    shl-long v52, v52, v51

    const/16 v51, 0x32

    ushr-long v54, v10, v51

    or-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, p4

    .line 81
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v51, 0x1b

    ushr-long v52, v10, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, v50

    .line 82
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const/16 v51, 0x4

    ushr-long v52, v10, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, p4

    .line 83
    add-int/lit8 v48, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v48    # "blocksOffset":I
    aget-wide v12, p1, p2

    .line 84
    .local v12, "block13":J
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v52, 0xf

    and-long v52, v52, v10

    const/16 v51, 0x13

    shl-long v52, v52, v51

    const/16 v51, 0x2d

    ushr-long v54, v12, v51

    or-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, v50

    .line 85
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const/16 v51, 0x16

    ushr-long v52, v12, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, p4

    .line 86
    add-int/lit8 p2, v48, 0x1

    .end local v48    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v14, p1, v48

    .line 87
    .local v14, "block14":J
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/32 v52, 0x3fffff

    and-long v52, v52, v12

    const/16 v51, 0x1

    shl-long v52, v52, v51

    const/16 v51, 0x3f

    ushr-long v54, v14, v51

    or-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, v50

    .line 88
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const/16 v51, 0x28

    ushr-long v52, v14, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, p4

    .line 89
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v51, 0x11

    ushr-long v52, v14, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, v50

    .line 90
    add-int/lit8 v48, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v48    # "blocksOffset":I
    aget-wide v16, p1, p2

    .line 91
    .local v16, "block15":J
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const-wide/32 v52, 0x1ffff

    and-long v52, v52, v14

    const/16 v51, 0x6

    shl-long v52, v52, v51

    const/16 v51, 0x3a

    ushr-long v54, v16, v51

    or-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, p4

    .line 92
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v51, 0x23

    ushr-long v52, v16, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, v50

    .line 93
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const/16 v51, 0xc

    ushr-long v52, v16, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, p4

    .line 94
    add-int/lit8 p2, v48, 0x1

    .end local v48    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v18, p1, v48

    .line 95
    .local v18, "block16":J
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v52, 0xfff

    and-long v52, v52, v16

    const/16 v51, 0xb

    shl-long v52, v52, v51

    const/16 v51, 0x35

    ushr-long v54, v18, v51

    or-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, v50

    .line 96
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const/16 v51, 0x1e

    ushr-long v52, v18, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, p4

    .line 97
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v51, 0x7

    ushr-long v52, v18, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, v50

    .line 98
    add-int/lit8 v48, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v48    # "blocksOffset":I
    aget-wide v20, p1, p2

    .line 99
    .local v20, "block17":J
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const-wide/16 v52, 0x7f

    and-long v52, v52, v18

    const/16 v51, 0x10

    shl-long v52, v52, v51

    const/16 v51, 0x30

    ushr-long v54, v20, v51

    or-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, p4

    .line 100
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v51, 0x19

    ushr-long v52, v20, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, v50

    .line 101
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const/16 v51, 0x2

    ushr-long v52, v20, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, p4

    .line 102
    add-int/lit8 p2, v48, 0x1

    .end local v48    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v22, p1, v48

    .line 103
    .local v22, "block18":J
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v52, 0x3

    and-long v52, v52, v20

    const/16 v51, 0x15

    shl-long v52, v52, v51

    const/16 v51, 0x2b

    ushr-long v54, v22, v51

    or-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, v50

    .line 104
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const/16 v51, 0x14

    ushr-long v52, v22, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, p4

    .line 105
    add-int/lit8 v48, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v48    # "blocksOffset":I
    aget-wide v24, p1, p2

    .line 106
    .local v24, "block19":J
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/32 v52, 0xfffff

    and-long v52, v52, v22

    const/16 v51, 0x3

    shl-long v52, v52, v51

    const/16 v51, 0x3d

    ushr-long v54, v24, v51

    or-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, v50

    .line 107
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const/16 v51, 0x26

    ushr-long v52, v24, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, p4

    .line 108
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v51, 0xf

    ushr-long v52, v24, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, v50

    .line 109
    add-int/lit8 p2, v48, 0x1

    .end local v48    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v28, p1, v48

    .line 110
    .local v28, "block20":J
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const-wide/16 v52, 0x7fff

    and-long v52, v52, v24

    const/16 v51, 0x8

    shl-long v52, v52, v51

    const/16 v51, 0x38

    ushr-long v54, v28, v51

    or-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, p4

    .line 111
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v51, 0x21

    ushr-long v52, v28, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, v50

    .line 112
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const/16 v51, 0xa

    ushr-long v52, v28, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, p4

    .line 113
    add-int/lit8 v48, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v48    # "blocksOffset":I
    aget-wide v30, p1, p2

    .line 114
    .local v30, "block21":J
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v52, 0x3ff

    and-long v52, v52, v28

    const/16 v51, 0xd

    shl-long v52, v52, v51

    const/16 v51, 0x33

    ushr-long v54, v30, v51

    or-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, v50

    .line 115
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const/16 v51, 0x1c

    ushr-long v52, v30, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, p4

    .line 116
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v51, 0x5

    ushr-long v52, v30, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, v50

    .line 117
    add-int/lit8 p2, v48, 0x1

    .end local v48    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v32, p1, v48

    .line 118
    .local v32, "block22":J
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const-wide/16 v52, 0x1f

    and-long v52, v52, v30

    const/16 v51, 0x12

    shl-long v52, v52, v51

    const/16 v51, 0x2e

    ushr-long v54, v32, v51

    or-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, p4

    .line 119
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v51, 0x17

    ushr-long v52, v32, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, v50

    .line 120
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const-wide/32 v52, 0x7fffff

    and-long v52, v52, v32

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v51, v0

    aput v51, p3, p4

    .line 33
    add-int/lit8 v49, v49, 0x1

    move/from16 v48, p2

    .end local p2    # "blocksOffset":I
    .restart local v48    # "blocksOffset":I
    goto/16 :goto_0
.end method

.method public decode([JI[JII)V
    .locals 56
    .param p1, "blocks"    # [J
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [J
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 163
    const/16 v49, 0x0

    .local v49, "i":I
    move/from16 v50, p4

    .end local p4    # "valuesOffset":I
    .local v50, "valuesOffset":I
    move/from16 v48, p2

    .end local p2    # "blocksOffset":I
    .local v48, "blocksOffset":I
    :goto_0
    move/from16 v0, v49

    move/from16 v1, p5

    if-lt v0, v1, :cond_0

    .line 252
    return-void

    .line 164
    :cond_0
    add-int/lit8 p2, v48, 0x1

    .end local v48    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v2, p1, v48

    .line 165
    .local v2, "block0":J
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v51, 0x29

    ushr-long v52, v2, v51

    aput-wide v52, p3, v50

    .line 166
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const/16 v51, 0x12

    ushr-long v52, v2, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    aput-wide v52, p3, p4

    .line 167
    add-int/lit8 v48, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v48    # "blocksOffset":I
    aget-wide v4, p1, p2

    .line 168
    .local v4, "block1":J
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/32 v52, 0x3ffff

    and-long v52, v52, v2

    const/16 v51, 0x5

    shl-long v52, v52, v51

    const/16 v51, 0x3b

    ushr-long v54, v4, v51

    or-long v52, v52, v54

    aput-wide v52, p3, v50

    .line 169
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const/16 v51, 0x24

    ushr-long v52, v4, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    aput-wide v52, p3, p4

    .line 170
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v51, 0xd

    ushr-long v52, v4, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    aput-wide v52, p3, v50

    .line 171
    add-int/lit8 p2, v48, 0x1

    .end local v48    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v26, p1, v48

    .line 172
    .local v26, "block2":J
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const-wide/16 v52, 0x1fff

    and-long v52, v52, v4

    const/16 v51, 0xa

    shl-long v52, v52, v51

    const/16 v51, 0x36

    ushr-long v54, v26, v51

    or-long v52, v52, v54

    aput-wide v52, p3, p4

    .line 173
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v51, 0x1f

    ushr-long v52, v26, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    aput-wide v52, p3, v50

    .line 174
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const/16 v51, 0x8

    ushr-long v52, v26, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    aput-wide v52, p3, p4

    .line 175
    add-int/lit8 v48, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v48    # "blocksOffset":I
    aget-wide v34, p1, p2

    .line 176
    .local v34, "block3":J
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v52, 0xff

    and-long v52, v52, v26

    const/16 v51, 0xf

    shl-long v52, v52, v51

    const/16 v51, 0x31

    ushr-long v54, v34, v51

    or-long v52, v52, v54

    aput-wide v52, p3, v50

    .line 177
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const/16 v51, 0x1a

    ushr-long v52, v34, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    aput-wide v52, p3, p4

    .line 178
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v51, 0x3

    ushr-long v52, v34, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    aput-wide v52, p3, v50

    .line 179
    add-int/lit8 p2, v48, 0x1

    .end local v48    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v36, p1, v48

    .line 180
    .local v36, "block4":J
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const-wide/16 v52, 0x7

    and-long v52, v52, v34

    const/16 v51, 0x14

    shl-long v52, v52, v51

    const/16 v51, 0x2c

    ushr-long v54, v36, v51

    or-long v52, v52, v54

    aput-wide v52, p3, p4

    .line 181
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v51, 0x15

    ushr-long v52, v36, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    aput-wide v52, p3, v50

    .line 182
    add-int/lit8 v48, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v48    # "blocksOffset":I
    aget-wide v38, p1, p2

    .line 183
    .local v38, "block5":J
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const-wide/32 v52, 0x1fffff

    and-long v52, v52, v36

    const/16 v51, 0x2

    shl-long v52, v52, v51

    const/16 v51, 0x3e

    ushr-long v54, v38, v51

    or-long v52, v52, v54

    aput-wide v52, p3, p4

    .line 184
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v51, 0x27

    ushr-long v52, v38, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    aput-wide v52, p3, v50

    .line 185
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const/16 v51, 0x10

    ushr-long v52, v38, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    aput-wide v52, p3, p4

    .line 186
    add-int/lit8 p2, v48, 0x1

    .end local v48    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v40, p1, v48

    .line 187
    .local v40, "block6":J
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/32 v52, 0xffff

    and-long v52, v52, v38

    const/16 v51, 0x7

    shl-long v52, v52, v51

    const/16 v51, 0x39

    ushr-long v54, v40, v51

    or-long v52, v52, v54

    aput-wide v52, p3, v50

    .line 188
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const/16 v51, 0x22

    ushr-long v52, v40, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    aput-wide v52, p3, p4

    .line 189
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v51, 0xb

    ushr-long v52, v40, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    aput-wide v52, p3, v50

    .line 190
    add-int/lit8 v48, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v48    # "blocksOffset":I
    aget-wide v42, p1, p2

    .line 191
    .local v42, "block7":J
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const-wide/16 v52, 0x7ff

    and-long v52, v52, v40

    const/16 v51, 0xc

    shl-long v52, v52, v51

    const/16 v51, 0x34

    ushr-long v54, v42, v51

    or-long v52, v52, v54

    aput-wide v52, p3, p4

    .line 192
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v51, 0x1d

    ushr-long v52, v42, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    aput-wide v52, p3, v50

    .line 193
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const/16 v51, 0x6

    ushr-long v52, v42, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    aput-wide v52, p3, p4

    .line 194
    add-int/lit8 p2, v48, 0x1

    .end local v48    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v44, p1, v48

    .line 195
    .local v44, "block8":J
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v52, 0x3f

    and-long v52, v52, v42

    const/16 v51, 0x11

    shl-long v52, v52, v51

    const/16 v51, 0x2f

    ushr-long v54, v44, v51

    or-long v52, v52, v54

    aput-wide v52, p3, v50

    .line 196
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const/16 v51, 0x18

    ushr-long v52, v44, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    aput-wide v52, p3, p4

    .line 197
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v51, 0x1

    ushr-long v52, v44, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    aput-wide v52, p3, v50

    .line 198
    add-int/lit8 v48, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v48    # "blocksOffset":I
    aget-wide v46, p1, p2

    .line 199
    .local v46, "block9":J
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const-wide/16 v52, 0x1

    and-long v52, v52, v44

    const/16 v51, 0x16

    shl-long v52, v52, v51

    const/16 v51, 0x2a

    ushr-long v54, v46, v51

    or-long v52, v52, v54

    aput-wide v52, p3, p4

    .line 200
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v51, 0x13

    ushr-long v52, v46, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    aput-wide v52, p3, v50

    .line 201
    add-int/lit8 p2, v48, 0x1

    .end local v48    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v6, p1, v48

    .line 202
    .local v6, "block10":J
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const-wide/32 v52, 0x7ffff

    and-long v52, v52, v46

    const/16 v51, 0x4

    shl-long v52, v52, v51

    const/16 v51, 0x3c

    ushr-long v54, v6, v51

    or-long v52, v52, v54

    aput-wide v52, p3, p4

    .line 203
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v51, 0x25

    ushr-long v52, v6, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    aput-wide v52, p3, v50

    .line 204
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const/16 v51, 0xe

    ushr-long v52, v6, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    aput-wide v52, p3, p4

    .line 205
    add-int/lit8 v48, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v48    # "blocksOffset":I
    aget-wide v8, p1, p2

    .line 206
    .local v8, "block11":J
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v52, 0x3fff

    and-long v52, v52, v6

    const/16 v51, 0x9

    shl-long v52, v52, v51

    const/16 v51, 0x37

    ushr-long v54, v8, v51

    or-long v52, v52, v54

    aput-wide v52, p3, v50

    .line 207
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const/16 v51, 0x20

    ushr-long v52, v8, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    aput-wide v52, p3, p4

    .line 208
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v51, 0x9

    ushr-long v52, v8, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    aput-wide v52, p3, v50

    .line 209
    add-int/lit8 p2, v48, 0x1

    .end local v48    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v10, p1, v48

    .line 210
    .local v10, "block12":J
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const-wide/16 v52, 0x1ff

    and-long v52, v52, v8

    const/16 v51, 0xe

    shl-long v52, v52, v51

    const/16 v51, 0x32

    ushr-long v54, v10, v51

    or-long v52, v52, v54

    aput-wide v52, p3, p4

    .line 211
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v51, 0x1b

    ushr-long v52, v10, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    aput-wide v52, p3, v50

    .line 212
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const/16 v51, 0x4

    ushr-long v52, v10, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    aput-wide v52, p3, p4

    .line 213
    add-int/lit8 v48, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v48    # "blocksOffset":I
    aget-wide v12, p1, p2

    .line 214
    .local v12, "block13":J
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v52, 0xf

    and-long v52, v52, v10

    const/16 v51, 0x13

    shl-long v52, v52, v51

    const/16 v51, 0x2d

    ushr-long v54, v12, v51

    or-long v52, v52, v54

    aput-wide v52, p3, v50

    .line 215
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const/16 v51, 0x16

    ushr-long v52, v12, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    aput-wide v52, p3, p4

    .line 216
    add-int/lit8 p2, v48, 0x1

    .end local v48    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v14, p1, v48

    .line 217
    .local v14, "block14":J
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/32 v52, 0x3fffff

    and-long v52, v52, v12

    const/16 v51, 0x1

    shl-long v52, v52, v51

    const/16 v51, 0x3f

    ushr-long v54, v14, v51

    or-long v52, v52, v54

    aput-wide v52, p3, v50

    .line 218
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const/16 v51, 0x28

    ushr-long v52, v14, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    aput-wide v52, p3, p4

    .line 219
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v51, 0x11

    ushr-long v52, v14, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    aput-wide v52, p3, v50

    .line 220
    add-int/lit8 v48, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v48    # "blocksOffset":I
    aget-wide v16, p1, p2

    .line 221
    .local v16, "block15":J
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const-wide/32 v52, 0x1ffff

    and-long v52, v52, v14

    const/16 v51, 0x6

    shl-long v52, v52, v51

    const/16 v51, 0x3a

    ushr-long v54, v16, v51

    or-long v52, v52, v54

    aput-wide v52, p3, p4

    .line 222
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v51, 0x23

    ushr-long v52, v16, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    aput-wide v52, p3, v50

    .line 223
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const/16 v51, 0xc

    ushr-long v52, v16, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    aput-wide v52, p3, p4

    .line 224
    add-int/lit8 p2, v48, 0x1

    .end local v48    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v18, p1, v48

    .line 225
    .local v18, "block16":J
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v52, 0xfff

    and-long v52, v52, v16

    const/16 v51, 0xb

    shl-long v52, v52, v51

    const/16 v51, 0x35

    ushr-long v54, v18, v51

    or-long v52, v52, v54

    aput-wide v52, p3, v50

    .line 226
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const/16 v51, 0x1e

    ushr-long v52, v18, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    aput-wide v52, p3, p4

    .line 227
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v51, 0x7

    ushr-long v52, v18, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    aput-wide v52, p3, v50

    .line 228
    add-int/lit8 v48, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v48    # "blocksOffset":I
    aget-wide v20, p1, p2

    .line 229
    .local v20, "block17":J
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const-wide/16 v52, 0x7f

    and-long v52, v52, v18

    const/16 v51, 0x10

    shl-long v52, v52, v51

    const/16 v51, 0x30

    ushr-long v54, v20, v51

    or-long v52, v52, v54

    aput-wide v52, p3, p4

    .line 230
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v51, 0x19

    ushr-long v52, v20, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    aput-wide v52, p3, v50

    .line 231
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const/16 v51, 0x2

    ushr-long v52, v20, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    aput-wide v52, p3, p4

    .line 232
    add-int/lit8 p2, v48, 0x1

    .end local v48    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v22, p1, v48

    .line 233
    .local v22, "block18":J
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v52, 0x3

    and-long v52, v52, v20

    const/16 v51, 0x15

    shl-long v52, v52, v51

    const/16 v51, 0x2b

    ushr-long v54, v22, v51

    or-long v52, v52, v54

    aput-wide v52, p3, v50

    .line 234
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const/16 v51, 0x14

    ushr-long v52, v22, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    aput-wide v52, p3, p4

    .line 235
    add-int/lit8 v48, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v48    # "blocksOffset":I
    aget-wide v24, p1, p2

    .line 236
    .local v24, "block19":J
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/32 v52, 0xfffff

    and-long v52, v52, v22

    const/16 v51, 0x3

    shl-long v52, v52, v51

    const/16 v51, 0x3d

    ushr-long v54, v24, v51

    or-long v52, v52, v54

    aput-wide v52, p3, v50

    .line 237
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const/16 v51, 0x26

    ushr-long v52, v24, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    aput-wide v52, p3, p4

    .line 238
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v51, 0xf

    ushr-long v52, v24, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    aput-wide v52, p3, v50

    .line 239
    add-int/lit8 p2, v48, 0x1

    .end local v48    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v28, p1, v48

    .line 240
    .local v28, "block20":J
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const-wide/16 v52, 0x7fff

    and-long v52, v52, v24

    const/16 v51, 0x8

    shl-long v52, v52, v51

    const/16 v51, 0x38

    ushr-long v54, v28, v51

    or-long v52, v52, v54

    aput-wide v52, p3, p4

    .line 241
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v51, 0x21

    ushr-long v52, v28, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    aput-wide v52, p3, v50

    .line 242
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const/16 v51, 0xa

    ushr-long v52, v28, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    aput-wide v52, p3, p4

    .line 243
    add-int/lit8 v48, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v48    # "blocksOffset":I
    aget-wide v30, p1, p2

    .line 244
    .local v30, "block21":J
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v52, 0x3ff

    and-long v52, v52, v28

    const/16 v51, 0xd

    shl-long v52, v52, v51

    const/16 v51, 0x33

    ushr-long v54, v30, v51

    or-long v52, v52, v54

    aput-wide v52, p3, v50

    .line 245
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const/16 v51, 0x1c

    ushr-long v52, v30, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    aput-wide v52, p3, p4

    .line 246
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v51, 0x5

    ushr-long v52, v30, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    aput-wide v52, p3, v50

    .line 247
    add-int/lit8 p2, v48, 0x1

    .end local v48    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v32, p1, v48

    .line 248
    .local v32, "block22":J
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const-wide/16 v52, 0x1f

    and-long v52, v52, v30

    const/16 v51, 0x12

    shl-long v52, v52, v51

    const/16 v51, 0x2e

    ushr-long v54, v32, v51

    or-long v52, v52, v54

    aput-wide v52, p3, p4

    .line 249
    add-int/lit8 p4, v50, 0x1

    .end local v50    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v51, 0x17

    ushr-long v52, v32, v51

    const-wide/32 v54, 0x7fffff

    and-long v52, v52, v54

    aput-wide v52, p3, v50

    .line 250
    add-int/lit8 v50, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v50    # "valuesOffset":I
    const-wide/32 v52, 0x7fffff

    and-long v52, v52, v32

    aput-wide v52, p3, p4

    .line 163
    add-int/lit8 v49, v49, 0x1

    move/from16 v48, p2

    .end local p2    # "blocksOffset":I
    .restart local v48    # "blocksOffset":I
    goto/16 :goto_0
.end method
