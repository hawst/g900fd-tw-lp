.class final Lorg/apache/lucene/util/RamUsageEstimator$ClassCache;
.super Ljava/lang/Object;
.source "RamUsageEstimator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/RamUsageEstimator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ClassCache"
.end annotation


# instance fields
.field public final alignedShallowInstanceSize:J

.field public final referenceFields:[Ljava/lang/reflect/Field;


# direct methods
.method public constructor <init>(J[Ljava/lang/reflect/Field;)V
    .locals 1
    .param p1, "alignedShallowInstanceSize"    # J
    .param p3, "referenceFields"    # [Ljava/lang/reflect/Field;

    .prologue
    .line 253
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 254
    iput-wide p1, p0, Lorg/apache/lucene/util/RamUsageEstimator$ClassCache;->alignedShallowInstanceSize:J

    .line 255
    iput-object p3, p0, Lorg/apache/lucene/util/RamUsageEstimator$ClassCache;->referenceFields:[Ljava/lang/reflect/Field;

    .line 256
    return-void
.end method
