.class public interface abstract Lorg/apache/lucene/util/BytesRefIterator;
.super Ljava/lang/Object;
.source "BytesRefIterator.java"


# static fields
.field public static final EMPTY:Lorg/apache/lucene/util/BytesRefIterator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    new-instance v0, Lorg/apache/lucene/util/BytesRefIterator$1;

    invoke-direct {v0}, Lorg/apache/lucene/util/BytesRefIterator$1;-><init>()V

    sput-object v0, Lorg/apache/lucene/util/BytesRefIterator;->EMPTY:Lorg/apache/lucene/util/BytesRefIterator;

    .line 61
    return-void
.end method


# virtual methods
.method public abstract getComparator()Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation
.end method

.method public abstract next()Lorg/apache/lucene/util/BytesRef;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
