.class final Lorg/apache/lucene/util/packed/PackedReaderIterator;
.super Lorg/apache/lucene/util/packed/PackedInts$ReaderIteratorImpl;
.source "PackedReaderIterator.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final bulkOperation:Lorg/apache/lucene/util/packed/BulkOperation;

.field final format:Lorg/apache/lucene/util/packed/PackedInts$Format;

.field final iterations:I

.field final nextBlocks:[B

.field final nextValues:Lorg/apache/lucene/util/LongsRef;

.field final packedIntsVersion:I

.field position:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lorg/apache/lucene/util/packed/PackedReaderIterator;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/packed/PackedReaderIterator;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/util/packed/PackedInts$Format;IIILorg/apache/lucene/store/DataInput;I)V
    .locals 4
    .param p1, "format"    # Lorg/apache/lucene/util/packed/PackedInts$Format;
    .param p2, "packedIntsVersion"    # I
    .param p3, "valueCount"    # I
    .param p4, "bitsPerValue"    # I
    .param p5, "in"    # Lorg/apache/lucene/store/DataInput;
    .param p6, "mem"    # I

    .prologue
    const/4 v3, 0x0

    .line 38
    invoke-direct {p0, p3, p4, p5}, Lorg/apache/lucene/util/packed/PackedInts$ReaderIteratorImpl;-><init>(IILorg/apache/lucene/store/DataInput;)V

    .line 39
    iput-object p1, p0, Lorg/apache/lucene/util/packed/PackedReaderIterator;->format:Lorg/apache/lucene/util/packed/PackedInts$Format;

    .line 40
    iput p2, p0, Lorg/apache/lucene/util/packed/PackedReaderIterator;->packedIntsVersion:I

    .line 41
    invoke-static {p1, p4}, Lorg/apache/lucene/util/packed/BulkOperation;->of(Lorg/apache/lucene/util/packed/PackedInts$Format;I)Lorg/apache/lucene/util/packed/BulkOperation;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/util/packed/PackedReaderIterator;->bulkOperation:Lorg/apache/lucene/util/packed/BulkOperation;

    .line 42
    invoke-direct {p0, p6}, Lorg/apache/lucene/util/packed/PackedReaderIterator;->iterations(I)I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/util/packed/PackedReaderIterator;->iterations:I

    .line 43
    sget-boolean v0, Lorg/apache/lucene/util/packed/PackedReaderIterator;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-eqz p3, :cond_0

    iget v0, p0, Lorg/apache/lucene/util/packed/PackedReaderIterator;->iterations:I

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 44
    :cond_0
    iget v0, p0, Lorg/apache/lucene/util/packed/PackedReaderIterator;->iterations:I

    iget-object v1, p0, Lorg/apache/lucene/util/packed/PackedReaderIterator;->bulkOperation:Lorg/apache/lucene/util/packed/BulkOperation;

    invoke-virtual {v1}, Lorg/apache/lucene/util/packed/BulkOperation;->byteBlockCount()I

    move-result v1

    mul-int/2addr v0, v1

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/lucene/util/packed/PackedReaderIterator;->nextBlocks:[B

    .line 45
    new-instance v0, Lorg/apache/lucene/util/LongsRef;

    iget v1, p0, Lorg/apache/lucene/util/packed/PackedReaderIterator;->iterations:I

    iget-object v2, p0, Lorg/apache/lucene/util/packed/PackedReaderIterator;->bulkOperation:Lorg/apache/lucene/util/packed/BulkOperation;

    invoke-virtual {v2}, Lorg/apache/lucene/util/packed/BulkOperation;->byteValueCount()I

    move-result v2

    mul-int/2addr v1, v2

    new-array v1, v1, [J

    invoke-direct {v0, v1, v3, v3}, Lorg/apache/lucene/util/LongsRef;-><init>([JII)V

    iput-object v0, p0, Lorg/apache/lucene/util/packed/PackedReaderIterator;->nextValues:Lorg/apache/lucene/util/LongsRef;

    .line 46
    iget-object v0, p0, Lorg/apache/lucene/util/packed/PackedReaderIterator;->nextValues:Lorg/apache/lucene/util/LongsRef;

    iget-object v1, p0, Lorg/apache/lucene/util/packed/PackedReaderIterator;->nextValues:Lorg/apache/lucene/util/LongsRef;

    iget-object v1, v1, Lorg/apache/lucene/util/LongsRef;->longs:[J

    array-length v1, v1

    iput v1, v0, Lorg/apache/lucene/util/LongsRef;->offset:I

    .line 47
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/util/packed/PackedReaderIterator;->position:I

    .line 48
    return-void
.end method

.method private iterations(I)I
    .locals 3
    .param p1, "mem"    # I

    .prologue
    .line 51
    iget-object v1, p0, Lorg/apache/lucene/util/packed/PackedReaderIterator;->bulkOperation:Lorg/apache/lucene/util/packed/BulkOperation;

    iget v2, p0, Lorg/apache/lucene/util/packed/PackedReaderIterator;->valueCount:I

    invoke-virtual {v1, v2, p1}, Lorg/apache/lucene/util/packed/BulkOperation;->computeIterations(II)I

    move-result v0

    .line 52
    .local v0, "iterations":I
    iget v1, p0, Lorg/apache/lucene/util/packed/PackedReaderIterator;->packedIntsVersion:I

    const/4 v2, 0x1

    if-ge v1, v2, :cond_0

    .line 54
    add-int/lit8 v1, v0, 0x7

    and-int/lit8 v0, v1, -0x8

    .line 56
    :cond_0
    return v0
.end method


# virtual methods
.method public next(I)Lorg/apache/lucene/util/LongsRef;
    .locals 10
    .param p1, "count"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 61
    sget-boolean v0, Lorg/apache/lucene/util/packed/PackedReaderIterator;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/util/packed/PackedReaderIterator;->nextValues:Lorg/apache/lucene/util/LongsRef;

    iget v0, v0, Lorg/apache/lucene/util/LongsRef;->length:I

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 62
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/util/packed/PackedReaderIterator;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-gtz p1, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 63
    :cond_1
    sget-boolean v0, Lorg/apache/lucene/util/packed/PackedReaderIterator;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lorg/apache/lucene/util/packed/PackedReaderIterator;->nextValues:Lorg/apache/lucene/util/LongsRef;

    iget v0, v0, Lorg/apache/lucene/util/LongsRef;->offset:I

    iget-object v1, p0, Lorg/apache/lucene/util/packed/PackedReaderIterator;->nextValues:Lorg/apache/lucene/util/LongsRef;

    iget v1, v1, Lorg/apache/lucene/util/LongsRef;->length:I

    add-int/2addr v0, v1

    iget-object v1, p0, Lorg/apache/lucene/util/packed/PackedReaderIterator;->nextValues:Lorg/apache/lucene/util/LongsRef;

    iget-object v1, v1, Lorg/apache/lucene/util/LongsRef;->longs:[J

    array-length v1, v1

    if-le v0, v1, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 65
    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/util/packed/PackedReaderIterator;->nextValues:Lorg/apache/lucene/util/LongsRef;

    iget v1, v0, Lorg/apache/lucene/util/LongsRef;->offset:I

    iget-object v3, p0, Lorg/apache/lucene/util/packed/PackedReaderIterator;->nextValues:Lorg/apache/lucene/util/LongsRef;

    iget v3, v3, Lorg/apache/lucene/util/LongsRef;->length:I

    add-int/2addr v1, v3

    iput v1, v0, Lorg/apache/lucene/util/LongsRef;->offset:I

    .line 67
    iget v0, p0, Lorg/apache/lucene/util/packed/PackedReaderIterator;->valueCount:I

    iget v1, p0, Lorg/apache/lucene/util/packed/PackedReaderIterator;->position:I

    sub-int/2addr v0, v1

    add-int/lit8 v7, v0, -0x1

    .line 68
    .local v7, "remaining":I
    if-gtz v7, :cond_3

    .line 69
    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 71
    :cond_3
    invoke-static {v7, p1}, Ljava/lang/Math;->min(II)I

    move-result p1

    .line 73
    iget-object v0, p0, Lorg/apache/lucene/util/packed/PackedReaderIterator;->nextValues:Lorg/apache/lucene/util/LongsRef;

    iget v0, v0, Lorg/apache/lucene/util/LongsRef;->offset:I

    iget-object v1, p0, Lorg/apache/lucene/util/packed/PackedReaderIterator;->nextValues:Lorg/apache/lucene/util/LongsRef;

    iget-object v1, v1, Lorg/apache/lucene/util/LongsRef;->longs:[J

    array-length v1, v1

    if-ne v0, v1, :cond_5

    .line 74
    iget-object v0, p0, Lorg/apache/lucene/util/packed/PackedReaderIterator;->format:Lorg/apache/lucene/util/packed/PackedInts$Format;

    iget v1, p0, Lorg/apache/lucene/util/packed/PackedReaderIterator;->packedIntsVersion:I

    iget v3, p0, Lorg/apache/lucene/util/packed/PackedReaderIterator;->bitsPerValue:I

    invoke-virtual {v0, v1, v7, v3}, Lorg/apache/lucene/util/packed/PackedInts$Format;->byteCount(III)J

    move-result-wide v8

    .line 75
    .local v8, "remainingBlocks":J
    iget-object v0, p0, Lorg/apache/lucene/util/packed/PackedReaderIterator;->nextBlocks:[B

    array-length v0, v0

    int-to-long v0, v0

    invoke-static {v8, v9, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    long-to-int v6, v0

    .line 76
    .local v6, "blocksToRead":I
    iget-object v0, p0, Lorg/apache/lucene/util/packed/PackedReaderIterator;->in:Lorg/apache/lucene/store/DataInput;

    iget-object v1, p0, Lorg/apache/lucene/util/packed/PackedReaderIterator;->nextBlocks:[B

    invoke-virtual {v0, v1, v2, v6}, Lorg/apache/lucene/store/DataInput;->readBytes([BII)V

    .line 77
    iget-object v0, p0, Lorg/apache/lucene/util/packed/PackedReaderIterator;->nextBlocks:[B

    array-length v0, v0

    if-ge v6, v0, :cond_4

    .line 78
    iget-object v0, p0, Lorg/apache/lucene/util/packed/PackedReaderIterator;->nextBlocks:[B

    iget-object v1, p0, Lorg/apache/lucene/util/packed/PackedReaderIterator;->nextBlocks:[B

    array-length v1, v1

    invoke-static {v0, v6, v1, v2}, Ljava/util/Arrays;->fill([BIIB)V

    .line 81
    :cond_4
    iget-object v0, p0, Lorg/apache/lucene/util/packed/PackedReaderIterator;->bulkOperation:Lorg/apache/lucene/util/packed/BulkOperation;

    iget-object v1, p0, Lorg/apache/lucene/util/packed/PackedReaderIterator;->nextBlocks:[B

    iget-object v3, p0, Lorg/apache/lucene/util/packed/PackedReaderIterator;->nextValues:Lorg/apache/lucene/util/LongsRef;

    iget-object v3, v3, Lorg/apache/lucene/util/LongsRef;->longs:[J

    iget v5, p0, Lorg/apache/lucene/util/packed/PackedReaderIterator;->iterations:I

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Lorg/apache/lucene/util/packed/BulkOperation;->decode([BI[JII)V

    .line 82
    iget-object v0, p0, Lorg/apache/lucene/util/packed/PackedReaderIterator;->nextValues:Lorg/apache/lucene/util/LongsRef;

    iput v2, v0, Lorg/apache/lucene/util/LongsRef;->offset:I

    .line 85
    .end local v6    # "blocksToRead":I
    .end local v8    # "remainingBlocks":J
    :cond_5
    iget-object v0, p0, Lorg/apache/lucene/util/packed/PackedReaderIterator;->nextValues:Lorg/apache/lucene/util/LongsRef;

    iget-object v1, p0, Lorg/apache/lucene/util/packed/PackedReaderIterator;->nextValues:Lorg/apache/lucene/util/LongsRef;

    iget-object v1, v1, Lorg/apache/lucene/util/LongsRef;->longs:[J

    array-length v1, v1

    iget-object v2, p0, Lorg/apache/lucene/util/packed/PackedReaderIterator;->nextValues:Lorg/apache/lucene/util/LongsRef;

    iget v2, v2, Lorg/apache/lucene/util/LongsRef;->offset:I

    sub-int/2addr v1, v2

    invoke-static {v1, p1}, Ljava/lang/Math;->min(II)I

    move-result v1

    iput v1, v0, Lorg/apache/lucene/util/LongsRef;->length:I

    .line 86
    iget v0, p0, Lorg/apache/lucene/util/packed/PackedReaderIterator;->position:I

    iget-object v1, p0, Lorg/apache/lucene/util/packed/PackedReaderIterator;->nextValues:Lorg/apache/lucene/util/LongsRef;

    iget v1, v1, Lorg/apache/lucene/util/LongsRef;->length:I

    add-int/2addr v0, v1

    iput v0, p0, Lorg/apache/lucene/util/packed/PackedReaderIterator;->position:I

    .line 87
    iget-object v0, p0, Lorg/apache/lucene/util/packed/PackedReaderIterator;->nextValues:Lorg/apache/lucene/util/LongsRef;

    return-object v0
.end method

.method public ord()I
    .locals 1

    .prologue
    .line 92
    iget v0, p0, Lorg/apache/lucene/util/packed/PackedReaderIterator;->position:I

    return v0
.end method
