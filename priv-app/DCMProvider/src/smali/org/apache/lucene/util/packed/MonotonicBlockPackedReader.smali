.class public final Lorg/apache/lucene/util/packed/MonotonicBlockPackedReader;
.super Ljava/lang/Object;
.source "MonotonicBlockPackedReader.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final averages:[F

.field private final blockMask:I

.field private final blockShift:I

.field private final minValues:[J

.field private final subReaders:[Lorg/apache/lucene/util/packed/PackedInts$Reader;

.field private final valueCount:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lorg/apache/lucene/util/packed/MonotonicBlockPackedReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/packed/MonotonicBlockPackedReader;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/store/IndexInput;IIJZ)V
    .locals 14
    .param p1, "in"    # Lorg/apache/lucene/store/IndexInput;
    .param p2, "packedIntsVersion"    # I
    .param p3, "blockSize"    # I
    .param p4, "valueCount"    # J
    .param p6, "direct"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    invoke-static/range {p3 .. p3}, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->checkBlockSize(I)V

    .line 43
    move-wide/from16 v0, p4

    iput-wide v0, p0, Lorg/apache/lucene/util/packed/MonotonicBlockPackedReader;->valueCount:J

    .line 44
    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->numberOfTrailingZeros(I)I

    move-result v8

    iput v8, p0, Lorg/apache/lucene/util/packed/MonotonicBlockPackedReader;->blockShift:I

    .line 45
    add-int/lit8 v8, p3, -0x1

    iput v8, p0, Lorg/apache/lucene/util/packed/MonotonicBlockPackedReader;->blockMask:I

    .line 46
    move/from16 v0, p3

    int-to-long v8, v0

    div-long v8, p4, v8

    long-to-int v9, v8

    move/from16 v0, p3

    int-to-long v10, v0

    rem-long v10, p4, v10

    const-wide/16 v12, 0x0

    cmp-long v8, v10, v12

    if-nez v8, :cond_0

    const/4 v8, 0x0

    :goto_0
    add-int v4, v9, v8

    .line 47
    .local v4, "numBlocks":I
    int-to-long v8, v4

    move/from16 v0, p3

    int-to-long v10, v0

    mul-long/2addr v8, v10

    cmp-long v8, v8, p4

    if-gez v8, :cond_1

    .line 48
    new-instance v8, Ljava/lang/IllegalArgumentException;

    const-string v9, "valueCount is too large for this block size"

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 46
    .end local v4    # "numBlocks":I
    :cond_0
    const/4 v8, 0x1

    goto :goto_0

    .line 50
    .restart local v4    # "numBlocks":I
    :cond_1
    new-array v8, v4, [J

    iput-object v8, p0, Lorg/apache/lucene/util/packed/MonotonicBlockPackedReader;->minValues:[J

    .line 51
    new-array v8, v4, [F

    iput-object v8, p0, Lorg/apache/lucene/util/packed/MonotonicBlockPackedReader;->averages:[F

    .line 52
    new-array v8, v4, [Lorg/apache/lucene/util/packed/PackedInts$Reader;

    iput-object v8, p0, Lorg/apache/lucene/util/packed/MonotonicBlockPackedReader;->subReaders:[Lorg/apache/lucene/util/packed/PackedInts$Reader;

    .line 53
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-lt v3, v4, :cond_2

    .line 73
    return-void

    .line 54
    :cond_2
    iget-object v8, p0, Lorg/apache/lucene/util/packed/MonotonicBlockPackedReader;->minValues:[J

    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readVLong()J

    move-result-wide v10

    aput-wide v10, v8, v3

    .line 55
    iget-object v8, p0, Lorg/apache/lucene/util/packed/MonotonicBlockPackedReader;->averages:[F

    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v9

    aput v9, v8, v3

    .line 56
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v2

    .line 57
    .local v2, "bitsPerValue":I
    const/16 v8, 0x40

    if-le v2, v8, :cond_3

    .line 58
    new-instance v8, Ljava/io/IOException;

    const-string v9, "Corrupted"

    invoke-direct {v8, v9}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 60
    :cond_3
    if-nez v2, :cond_4

    .line 61
    iget-object v8, p0, Lorg/apache/lucene/util/packed/MonotonicBlockPackedReader;->subReaders:[Lorg/apache/lucene/util/packed/PackedInts$Reader;

    new-instance v9, Lorg/apache/lucene/util/packed/PackedInts$NullReader;

    move/from16 v0, p3

    invoke-direct {v9, v0}, Lorg/apache/lucene/util/packed/PackedInts$NullReader;-><init>(I)V

    aput-object v9, v8, v3

    .line 53
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 63
    :cond_4
    move/from16 v0, p3

    int-to-long v8, v0

    int-to-long v10, v3

    move/from16 v0, p3

    int-to-long v12, v0

    mul-long/2addr v10, v12

    sub-long v10, p4, v10

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v8

    long-to-int v5, v8

    .line 64
    .local v5, "size":I
    if-eqz p6, :cond_5

    .line 65
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v6

    .line 66
    .local v6, "pointer":J
    iget-object v8, p0, Lorg/apache/lucene/util/packed/MonotonicBlockPackedReader;->subReaders:[Lorg/apache/lucene/util/packed/PackedInts$Reader;

    sget-object v9, Lorg/apache/lucene/util/packed/PackedInts$Format;->PACKED:Lorg/apache/lucene/util/packed/PackedInts$Format;

    move/from16 v0, p2

    invoke-static {p1, v9, v0, v5, v2}, Lorg/apache/lucene/util/packed/PackedInts;->getDirectReaderNoHeader(Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/util/packed/PackedInts$Format;III)Lorg/apache/lucene/util/packed/PackedInts$Reader;

    move-result-object v9

    aput-object v9, v8, v3

    .line 67
    sget-object v8, Lorg/apache/lucene/util/packed/PackedInts$Format;->PACKED:Lorg/apache/lucene/util/packed/PackedInts$Format;

    move/from16 v0, p2

    invoke-virtual {v8, v0, v5, v2}, Lorg/apache/lucene/util/packed/PackedInts$Format;->byteCount(III)J

    move-result-wide v8

    add-long/2addr v8, v6

    invoke-virtual {p1, v8, v9}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    goto :goto_2

    .line 69
    .end local v6    # "pointer":J
    :cond_5
    iget-object v8, p0, Lorg/apache/lucene/util/packed/MonotonicBlockPackedReader;->subReaders:[Lorg/apache/lucene/util/packed/PackedInts$Reader;

    sget-object v9, Lorg/apache/lucene/util/packed/PackedInts$Format;->PACKED:Lorg/apache/lucene/util/packed/PackedInts$Format;

    move/from16 v0, p2

    invoke-static {p1, v9, v0, v5, v2}, Lorg/apache/lucene/util/packed/PackedInts;->getReaderNoHeader(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/packed/PackedInts$Format;III)Lorg/apache/lucene/util/packed/PackedInts$Reader;

    move-result-object v9

    aput-object v9, v8, v3

    goto :goto_2
.end method


# virtual methods
.method public get(J)J
    .locals 7
    .param p1, "index"    # J

    .prologue
    .line 77
    sget-boolean v2, Lorg/apache/lucene/util/packed/MonotonicBlockPackedReader;->$assertionsDisabled:Z

    if-nez v2, :cond_1

    const-wide/16 v2, 0x0

    cmp-long v2, p1, v2

    if-ltz v2, :cond_0

    iget-wide v2, p0, Lorg/apache/lucene/util/packed/MonotonicBlockPackedReader;->valueCount:J

    cmp-long v2, p1, v2

    if-ltz v2, :cond_1

    :cond_0
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 78
    :cond_1
    iget v2, p0, Lorg/apache/lucene/util/packed/MonotonicBlockPackedReader;->blockShift:I

    ushr-long v2, p1, v2

    long-to-int v0, v2

    .line 79
    .local v0, "block":I
    iget v2, p0, Lorg/apache/lucene/util/packed/MonotonicBlockPackedReader;->blockMask:I

    int-to-long v2, v2

    and-long/2addr v2, p1

    long-to-int v1, v2

    .line 80
    .local v1, "idx":I
    iget-object v2, p0, Lorg/apache/lucene/util/packed/MonotonicBlockPackedReader;->minValues:[J

    aget-wide v2, v2, v0

    int-to-float v4, v1

    iget-object v5, p0, Lorg/apache/lucene/util/packed/MonotonicBlockPackedReader;->averages:[F

    aget v5, v5, v0

    mul-float/2addr v4, v5

    float-to-long v4, v4

    add-long/2addr v2, v4

    iget-object v4, p0, Lorg/apache/lucene/util/packed/MonotonicBlockPackedReader;->subReaders:[Lorg/apache/lucene/util/packed/PackedInts$Reader;

    aget-object v4, v4, v0

    invoke-interface {v4, v1}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->zigZagDecode(J)J

    move-result-wide v4

    add-long/2addr v2, v4

    return-wide v2
.end method
