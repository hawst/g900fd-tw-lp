.class public final Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;
.super Ljava/lang/Object;
.source "Builder.java"

# interfaces
.implements Lorg/apache/lucene/util/fst/Builder$Node;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/fst/Builder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "UnCompiledNode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lorg/apache/lucene/util/fst/Builder$Node;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field public arcs:[Lorg/apache/lucene/util/fst/Builder$Arc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lorg/apache/lucene/util/fst/Builder$Arc",
            "<TT;>;"
        }
    .end annotation
.end field

.field public final depth:I

.field public inputCount:J

.field public isFinal:Z

.field public numArcs:I

.field public output:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field final owner:Lorg/apache/lucene/util/fst/Builder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/fst/Builder",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 531
    const-class v0, Lorg/apache/lucene/util/fst/Builder;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/util/fst/Builder;I)V
    .locals 3
    .param p2, "depth"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/Builder",
            "<TT;>;I)V"
        }
    .end annotation

    .prologue
    .line 553
    .local p0, "this":Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;, "Lorg/apache/lucene/util/fst/Builder<TT;>.UnCompiledNode<TT;>;"
    .local p1, "owner":Lorg/apache/lucene/util/fst/Builder;, "Lorg/apache/lucene/util/fst/Builder<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 554
    iput-object p1, p0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->owner:Lorg/apache/lucene/util/fst/Builder;

    .line 555
    const/4 v0, 0x1

    new-array v0, v0, [Lorg/apache/lucene/util/fst/Builder$Arc;

    iput-object v0, p0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->arcs:[Lorg/apache/lucene/util/fst/Builder$Arc;

    .line 556
    iget-object v0, p0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->arcs:[Lorg/apache/lucene/util/fst/Builder$Arc;

    const/4 v1, 0x0

    new-instance v2, Lorg/apache/lucene/util/fst/Builder$Arc;

    invoke-direct {v2}, Lorg/apache/lucene/util/fst/Builder$Arc;-><init>()V

    aput-object v2, v0, v1

    .line 557
    # getter for: Lorg/apache/lucene/util/fst/Builder;->NO_OUTPUT:Ljava/lang/Object;
    invoke-static {p1}, Lorg/apache/lucene/util/fst/Builder;->access$0(Lorg/apache/lucene/util/fst/Builder;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->output:Ljava/lang/Object;

    .line 558
    iput p2, p0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->depth:I

    .line 559
    return-void
.end method


# virtual methods
.method public addArc(ILorg/apache/lucene/util/fst/Builder$Node;)V
    .locals 7
    .param p1, "label"    # I

    .prologue
    .local p0, "this":Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;, "Lorg/apache/lucene/util/fst/Builder<TT;>.UnCompiledNode<TT;>;"
    .local p2, "target":Lorg/apache/lucene/util/fst/Builder$Node;, "Lorg/apache/lucene/util/fst/Builder$Node;"
    const/4 v6, 0x0

    .line 583
    sget-boolean v3, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    if-gez p1, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 584
    :cond_0
    sget-boolean v3, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->$assertionsDisabled:Z

    if-nez v3, :cond_1

    iget v3, p0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    if-eqz v3, :cond_1

    iget-object v3, p0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->arcs:[Lorg/apache/lucene/util/fst/Builder$Arc;

    iget v4, p0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    add-int/lit8 v4, v4, -0x1

    aget-object v3, v3, v4

    iget v3, v3, Lorg/apache/lucene/util/fst/Builder$Arc;->label:I

    if-gt p1, v3, :cond_1

    new-instance v3, Ljava/lang/AssertionError;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "arc[-1].label="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->arcs:[Lorg/apache/lucene/util/fst/Builder$Arc;

    iget v6, p0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    add-int/lit8 v6, v6, -0x1

    aget-object v5, v5, v6

    iget v5, v5, Lorg/apache/lucene/util/fst/Builder$Arc;->label:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " new label="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " numArcs="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 585
    :cond_1
    iget v3, p0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    iget-object v4, p0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->arcs:[Lorg/apache/lucene/util/fst/Builder$Arc;

    array-length v4, v4

    if-ne v3, v4, :cond_2

    .line 587
    iget v3, p0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    add-int/lit8 v3, v3, 0x1

    sget v4, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_REF:I

    invoke-static {v3, v4}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v3

    new-array v2, v3, [Lorg/apache/lucene/util/fst/Builder$Arc;

    .line 588
    .local v2, "newArcs":[Lorg/apache/lucene/util/fst/Builder$Arc;
    iget-object v3, p0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->arcs:[Lorg/apache/lucene/util/fst/Builder$Arc;

    iget-object v4, p0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->arcs:[Lorg/apache/lucene/util/fst/Builder$Arc;

    array-length v4, v4

    invoke-static {v3, v6, v2, v6, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 589
    iget v1, p0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    .local v1, "arcIdx":I
    :goto_0
    array-length v3, v2

    if-lt v1, v3, :cond_3

    .line 592
    iput-object v2, p0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->arcs:[Lorg/apache/lucene/util/fst/Builder$Arc;

    .line 594
    .end local v1    # "arcIdx":I
    .end local v2    # "newArcs":[Lorg/apache/lucene/util/fst/Builder$Arc;
    :cond_2
    iget-object v3, p0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->arcs:[Lorg/apache/lucene/util/fst/Builder$Arc;

    iget v4, p0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    aget-object v0, v3, v4

    .line 595
    .local v0, "arc":Lorg/apache/lucene/util/fst/Builder$Arc;, "Lorg/apache/lucene/util/fst/Builder$Arc<TT;>;"
    iput p1, v0, Lorg/apache/lucene/util/fst/Builder$Arc;->label:I

    .line 596
    iput-object p2, v0, Lorg/apache/lucene/util/fst/Builder$Arc;->target:Lorg/apache/lucene/util/fst/Builder$Node;

    .line 597
    iget-object v3, p0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->owner:Lorg/apache/lucene/util/fst/Builder;

    # getter for: Lorg/apache/lucene/util/fst/Builder;->NO_OUTPUT:Ljava/lang/Object;
    invoke-static {v3}, Lorg/apache/lucene/util/fst/Builder;->access$0(Lorg/apache/lucene/util/fst/Builder;)Ljava/lang/Object;

    move-result-object v3

    iput-object v3, v0, Lorg/apache/lucene/util/fst/Builder$Arc;->nextFinalOutput:Ljava/lang/Object;

    iput-object v3, v0, Lorg/apache/lucene/util/fst/Builder$Arc;->output:Ljava/lang/Object;

    .line 598
    iput-boolean v6, v0, Lorg/apache/lucene/util/fst/Builder$Arc;->isFinal:Z

    .line 599
    return-void

    .line 590
    .end local v0    # "arc":Lorg/apache/lucene/util/fst/Builder$Arc;, "Lorg/apache/lucene/util/fst/Builder$Arc<TT;>;"
    .restart local v1    # "arcIdx":I
    .restart local v2    # "newArcs":[Lorg/apache/lucene/util/fst/Builder$Arc;
    :cond_3
    new-instance v3, Lorg/apache/lucene/util/fst/Builder$Arc;

    invoke-direct {v3}, Lorg/apache/lucene/util/fst/Builder$Arc;-><init>()V

    aput-object v3, v2, v1

    .line 589
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public clear()V
    .locals 2

    .prologue
    .local p0, "this":Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;, "Lorg/apache/lucene/util/fst/Builder<TT;>.UnCompiledNode<TT;>;"
    const/4 v0, 0x0

    .line 567
    iput v0, p0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    .line 568
    iput-boolean v0, p0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->isFinal:Z

    .line 569
    iget-object v0, p0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->owner:Lorg/apache/lucene/util/fst/Builder;

    # getter for: Lorg/apache/lucene/util/fst/Builder;->NO_OUTPUT:Ljava/lang/Object;
    invoke-static {v0}, Lorg/apache/lucene/util/fst/Builder;->access$0(Lorg/apache/lucene/util/fst/Builder;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->output:Ljava/lang/Object;

    .line 570
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->inputCount:J

    .line 574
    return-void
.end method

.method public deleteLast(ILorg/apache/lucene/util/fst/Builder$Node;)V
    .locals 2
    .param p1, "label"    # I

    .prologue
    .line 612
    .local p0, "this":Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;, "Lorg/apache/lucene/util/fst/Builder<TT;>.UnCompiledNode<TT;>;"
    .local p2, "target":Lorg/apache/lucene/util/fst/Builder$Node;, "Lorg/apache/lucene/util/fst/Builder$Node;"
    sget-boolean v0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 613
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->arcs:[Lorg/apache/lucene/util/fst/Builder$Arc;

    iget v1, p0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    iget v0, v0, Lorg/apache/lucene/util/fst/Builder$Arc;->label:I

    if-eq p1, v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 614
    :cond_1
    sget-boolean v0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->arcs:[Lorg/apache/lucene/util/fst/Builder$Arc;

    iget v1, p0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    iget-object v0, v0, Lorg/apache/lucene/util/fst/Builder$Arc;->target:Lorg/apache/lucene/util/fst/Builder$Node;

    if-eq p2, v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 615
    :cond_2
    iget v0, p0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    .line 616
    return-void
.end method

.method public getLastOutput(I)Ljava/lang/Object;
    .locals 2
    .param p1, "labelToMatch"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .prologue
    .line 577
    .local p0, "this":Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;, "Lorg/apache/lucene/util/fst/Builder<TT;>.UnCompiledNode<TT;>;"
    sget-boolean v0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 578
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->arcs:[Lorg/apache/lucene/util/fst/Builder$Arc;

    iget v1, p0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    iget v0, v0, Lorg/apache/lucene/util/fst/Builder$Arc;->label:I

    if-eq v0, p1, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 579
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->arcs:[Lorg/apache/lucene/util/fst/Builder$Arc;

    iget v1, p0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    iget-object v0, v0, Lorg/apache/lucene/util/fst/Builder$Arc;->output:Ljava/lang/Object;

    return-object v0
.end method

.method public isCompiled()Z
    .locals 1

    .prologue
    .line 563
    .local p0, "this":Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;, "Lorg/apache/lucene/util/fst/Builder<TT;>.UnCompiledNode<TT;>;"
    const/4 v0, 0x0

    return v0
.end method

.method public prependOutput(Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 628
    .local p0, "this":Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;, "Lorg/apache/lucene/util/fst/Builder<TT;>.UnCompiledNode<TT;>;"
    .local p1, "outputPrefix":Ljava/lang/Object;, "TT;"
    sget-boolean v1, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->owner:Lorg/apache/lucene/util/fst/Builder;

    # invokes: Lorg/apache/lucene/util/fst/Builder;->validOutput(Ljava/lang/Object;)Z
    invoke-static {v1, p1}, Lorg/apache/lucene/util/fst/Builder;->access$1(Lorg/apache/lucene/util/fst/Builder;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 630
    :cond_0
    const/4 v0, 0x0

    .local v0, "arcIdx":I
    :goto_0
    iget v1, p0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    if-lt v0, v1, :cond_1

    .line 635
    iget-boolean v1, p0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->isFinal:Z

    if-eqz v1, :cond_3

    .line 636
    iget-object v1, p0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->owner:Lorg/apache/lucene/util/fst/Builder;

    # getter for: Lorg/apache/lucene/util/fst/Builder;->fst:Lorg/apache/lucene/util/fst/FST;
    invoke-static {v1}, Lorg/apache/lucene/util/fst/Builder;->access$2(Lorg/apache/lucene/util/fst/Builder;)Lorg/apache/lucene/util/fst/FST;

    move-result-object v1

    iget-object v1, v1, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    iget-object v2, p0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->output:Ljava/lang/Object;

    invoke-virtual {v1, p1, v2}, Lorg/apache/lucene/util/fst/Outputs;->add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->output:Ljava/lang/Object;

    .line 637
    sget-boolean v1, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->$assertionsDisabled:Z

    if-nez v1, :cond_3

    iget-object v1, p0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->owner:Lorg/apache/lucene/util/fst/Builder;

    iget-object v2, p0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->output:Ljava/lang/Object;

    # invokes: Lorg/apache/lucene/util/fst/Builder;->validOutput(Ljava/lang/Object;)Z
    invoke-static {v1, v2}, Lorg/apache/lucene/util/fst/Builder;->access$1(Lorg/apache/lucene/util/fst/Builder;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 631
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->arcs:[Lorg/apache/lucene/util/fst/Builder$Arc;

    aget-object v1, v1, v0

    iget-object v2, p0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->owner:Lorg/apache/lucene/util/fst/Builder;

    # getter for: Lorg/apache/lucene/util/fst/Builder;->fst:Lorg/apache/lucene/util/fst/FST;
    invoke-static {v2}, Lorg/apache/lucene/util/fst/Builder;->access$2(Lorg/apache/lucene/util/fst/Builder;)Lorg/apache/lucene/util/fst/FST;

    move-result-object v2

    iget-object v2, v2, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    iget-object v3, p0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->arcs:[Lorg/apache/lucene/util/fst/Builder$Arc;

    aget-object v3, v3, v0

    iget-object v3, v3, Lorg/apache/lucene/util/fst/Builder$Arc;->output:Ljava/lang/Object;

    invoke-virtual {v2, p1, v3}, Lorg/apache/lucene/util/fst/Outputs;->add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    iput-object v2, v1, Lorg/apache/lucene/util/fst/Builder$Arc;->output:Ljava/lang/Object;

    .line 632
    sget-boolean v1, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->$assertionsDisabled:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->owner:Lorg/apache/lucene/util/fst/Builder;

    iget-object v2, p0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->arcs:[Lorg/apache/lucene/util/fst/Builder$Arc;

    aget-object v2, v2, v0

    iget-object v2, v2, Lorg/apache/lucene/util/fst/Builder$Arc;->output:Ljava/lang/Object;

    # invokes: Lorg/apache/lucene/util/fst/Builder;->validOutput(Ljava/lang/Object;)Z
    invoke-static {v1, v2}, Lorg/apache/lucene/util/fst/Builder;->access$1(Lorg/apache/lucene/util/fst/Builder;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 630
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 639
    :cond_3
    return-void
.end method

.method public replaceLast(ILorg/apache/lucene/util/fst/Builder$Node;Ljava/lang/Object;Z)V
    .locals 4
    .param p1, "labelToMatch"    # I
    .param p4, "isFinal"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lorg/apache/lucene/util/fst/Builder$Node;",
            "TT;Z)V"
        }
    .end annotation

    .prologue
    .line 602
    .local p0, "this":Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;, "Lorg/apache/lucene/util/fst/Builder<TT;>.UnCompiledNode<TT;>;"
    .local p2, "target":Lorg/apache/lucene/util/fst/Builder$Node;, "Lorg/apache/lucene/util/fst/Builder$Node;"
    .local p3, "nextFinalOutput":Ljava/lang/Object;, "TT;"
    sget-boolean v1, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget v1, p0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    if-gtz v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 603
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->arcs:[Lorg/apache/lucene/util/fst/Builder$Arc;

    iget v2, p0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    add-int/lit8 v2, v2, -0x1

    aget-object v0, v1, v2

    .line 604
    .local v0, "arc":Lorg/apache/lucene/util/fst/Builder$Arc;, "Lorg/apache/lucene/util/fst/Builder$Arc<TT;>;"
    sget-boolean v1, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    iget v1, v0, Lorg/apache/lucene/util/fst/Builder$Arc;->label:I

    if-eq v1, p1, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "arc.label="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, v0, Lorg/apache/lucene/util/fst/Builder$Arc;->label:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " vs "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 605
    :cond_1
    iput-object p2, v0, Lorg/apache/lucene/util/fst/Builder$Arc;->target:Lorg/apache/lucene/util/fst/Builder$Node;

    .line 607
    iput-object p3, v0, Lorg/apache/lucene/util/fst/Builder$Arc;->nextFinalOutput:Ljava/lang/Object;

    .line 608
    iput-boolean p4, v0, Lorg/apache/lucene/util/fst/Builder$Arc;->isFinal:Z

    .line 609
    return-void
.end method

.method public setLastOutput(ILjava/lang/Object;)V
    .locals 3
    .param p1, "labelToMatch"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITT;)V"
        }
    .end annotation

    .prologue
    .line 619
    .local p0, "this":Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;, "Lorg/apache/lucene/util/fst/Builder<TT;>.UnCompiledNode<TT;>;"
    .local p2, "newOutput":Ljava/lang/Object;, "TT;"
    sget-boolean v1, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->owner:Lorg/apache/lucene/util/fst/Builder;

    # invokes: Lorg/apache/lucene/util/fst/Builder;->validOutput(Ljava/lang/Object;)Z
    invoke-static {v1, p2}, Lorg/apache/lucene/util/fst/Builder;->access$1(Lorg/apache/lucene/util/fst/Builder;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 620
    :cond_0
    sget-boolean v1, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    iget v1, p0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    if-gtz v1, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 621
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->arcs:[Lorg/apache/lucene/util/fst/Builder$Arc;

    iget v2, p0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    add-int/lit8 v2, v2, -0x1

    aget-object v0, v1, v2

    .line 622
    .local v0, "arc":Lorg/apache/lucene/util/fst/Builder$Arc;, "Lorg/apache/lucene/util/fst/Builder$Arc<TT;>;"
    sget-boolean v1, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->$assertionsDisabled:Z

    if-nez v1, :cond_2

    iget v1, v0, Lorg/apache/lucene/util/fst/Builder$Arc;->label:I

    if-eq v1, p1, :cond_2

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 623
    :cond_2
    iput-object p2, v0, Lorg/apache/lucene/util/fst/Builder$Arc;->output:Ljava/lang/Object;

    .line 624
    return-void
.end method
