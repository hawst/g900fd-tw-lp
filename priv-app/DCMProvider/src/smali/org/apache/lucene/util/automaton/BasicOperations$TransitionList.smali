.class final Lorg/apache/lucene/util/automaton/BasicOperations$TransitionList;
.super Ljava/lang/Object;
.source "BasicOperations.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/automaton/BasicOperations;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "TransitionList"
.end annotation


# instance fields
.field count:I

.field transitions:[Lorg/apache/lucene/util/automaton/Transition;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 456
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 457
    const/4 v0, 0x2

    new-array v0, v0, [Lorg/apache/lucene/util/automaton/Transition;

    iput-object v0, p0, Lorg/apache/lucene/util/automaton/BasicOperations$TransitionList;->transitions:[Lorg/apache/lucene/util/automaton/Transition;

    .line 456
    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/util/automaton/BasicOperations$TransitionList;)V
    .locals 0

    .prologue
    .line 456
    invoke-direct {p0}, Lorg/apache/lucene/util/automaton/BasicOperations$TransitionList;-><init>()V

    return-void
.end method


# virtual methods
.method public add(Lorg/apache/lucene/util/automaton/Transition;)V
    .locals 4
    .param p1, "t"    # Lorg/apache/lucene/util/automaton/Transition;

    .prologue
    const/4 v3, 0x0

    .line 461
    iget-object v1, p0, Lorg/apache/lucene/util/automaton/BasicOperations$TransitionList;->transitions:[Lorg/apache/lucene/util/automaton/Transition;

    array-length v1, v1

    iget v2, p0, Lorg/apache/lucene/util/automaton/BasicOperations$TransitionList;->count:I

    if-ne v1, v2, :cond_0

    .line 462
    iget v1, p0, Lorg/apache/lucene/util/automaton/BasicOperations$TransitionList;->count:I

    add-int/lit8 v1, v1, 0x1

    sget v2, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_REF:I

    invoke-static {v1, v2}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v1

    new-array v0, v1, [Lorg/apache/lucene/util/automaton/Transition;

    .line 463
    .local v0, "newArray":[Lorg/apache/lucene/util/automaton/Transition;
    iget-object v1, p0, Lorg/apache/lucene/util/automaton/BasicOperations$TransitionList;->transitions:[Lorg/apache/lucene/util/automaton/Transition;

    iget v2, p0, Lorg/apache/lucene/util/automaton/BasicOperations$TransitionList;->count:I

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 464
    iput-object v0, p0, Lorg/apache/lucene/util/automaton/BasicOperations$TransitionList;->transitions:[Lorg/apache/lucene/util/automaton/Transition;

    .line 466
    .end local v0    # "newArray":[Lorg/apache/lucene/util/automaton/Transition;
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/util/automaton/BasicOperations$TransitionList;->transitions:[Lorg/apache/lucene/util/automaton/Transition;

    iget v2, p0, Lorg/apache/lucene/util/automaton/BasicOperations$TransitionList;->count:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lorg/apache/lucene/util/automaton/BasicOperations$TransitionList;->count:I

    aput-object p1, v1, v2

    .line 467
    return-void
.end method
