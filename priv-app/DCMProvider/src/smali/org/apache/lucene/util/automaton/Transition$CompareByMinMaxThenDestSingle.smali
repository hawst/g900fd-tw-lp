.class final Lorg/apache/lucene/util/automaton/Transition$CompareByMinMaxThenDestSingle;
.super Ljava/lang/Object;
.source "Transition.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/automaton/Transition;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "CompareByMinMaxThenDestSingle"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lorg/apache/lucene/util/automaton/Transition;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 198
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/util/automaton/Transition$CompareByMinMaxThenDestSingle;)V
    .locals 0

    .prologue
    .line 198
    invoke-direct {p0}, Lorg/apache/lucene/util/automaton/Transition$CompareByMinMaxThenDestSingle;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/util/automaton/Transition;

    check-cast p2, Lorg/apache/lucene/util/automaton/Transition;

    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/util/automaton/Transition$CompareByMinMaxThenDestSingle;->compare(Lorg/apache/lucene/util/automaton/Transition;Lorg/apache/lucene/util/automaton/Transition;)I

    move-result v0

    return v0
.end method

.method public compare(Lorg/apache/lucene/util/automaton/Transition;Lorg/apache/lucene/util/automaton/Transition;)I
    .locals 4
    .param p1, "t1"    # Lorg/apache/lucene/util/automaton/Transition;
    .param p2, "t2"    # Lorg/apache/lucene/util/automaton/Transition;

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 201
    iget v2, p1, Lorg/apache/lucene/util/automaton/Transition;->min:I

    iget v3, p2, Lorg/apache/lucene/util/automaton/Transition;->min:I

    if-ge v2, v3, :cond_1

    .line 209
    :cond_0
    :goto_0
    return v0

    .line 202
    :cond_1
    iget v2, p1, Lorg/apache/lucene/util/automaton/Transition;->min:I

    iget v3, p2, Lorg/apache/lucene/util/automaton/Transition;->min:I

    if-le v2, v3, :cond_2

    move v0, v1

    goto :goto_0

    .line 203
    :cond_2
    iget v2, p1, Lorg/apache/lucene/util/automaton/Transition;->max:I

    iget v3, p2, Lorg/apache/lucene/util/automaton/Transition;->max:I

    if-gt v2, v3, :cond_0

    .line 204
    iget v2, p1, Lorg/apache/lucene/util/automaton/Transition;->max:I

    iget v3, p2, Lorg/apache/lucene/util/automaton/Transition;->max:I

    if-ge v2, v3, :cond_3

    move v0, v1

    goto :goto_0

    .line 205
    :cond_3
    iget-object v2, p1, Lorg/apache/lucene/util/automaton/Transition;->to:Lorg/apache/lucene/util/automaton/State;

    iget-object v3, p2, Lorg/apache/lucene/util/automaton/Transition;->to:Lorg/apache/lucene/util/automaton/State;

    if-eq v2, v3, :cond_4

    .line 206
    iget-object v2, p1, Lorg/apache/lucene/util/automaton/Transition;->to:Lorg/apache/lucene/util/automaton/State;

    iget v2, v2, Lorg/apache/lucene/util/automaton/State;->number:I

    iget-object v3, p2, Lorg/apache/lucene/util/automaton/Transition;->to:Lorg/apache/lucene/util/automaton/State;

    iget v3, v3, Lorg/apache/lucene/util/automaton/State;->number:I

    if-lt v2, v3, :cond_0

    .line 207
    iget-object v0, p1, Lorg/apache/lucene/util/automaton/Transition;->to:Lorg/apache/lucene/util/automaton/State;

    iget v0, v0, Lorg/apache/lucene/util/automaton/State;->number:I

    iget-object v2, p2, Lorg/apache/lucene/util/automaton/Transition;->to:Lorg/apache/lucene/util/automaton/State;

    iget v2, v2, Lorg/apache/lucene/util/automaton/State;->number:I

    if-le v0, v2, :cond_4

    move v0, v1

    goto :goto_0

    .line 209
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method
