.class public final enum Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;
.super Ljava/lang/Enum;
.source "FST.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/fst/FST;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "INPUT_TYPE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum BYTE1:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

.field public static final enum BYTE2:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

.field public static final enum BYTE4:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

.field private static final synthetic ENUM$VALUES:[Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 75
    new-instance v0, Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    const-string v1, "BYTE1"

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;->BYTE1:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    new-instance v0, Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    const-string v1, "BYTE2"

    invoke-direct {v0, v1, v3}, Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;->BYTE2:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    new-instance v0, Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    const-string v1, "BYTE4"

    invoke-direct {v0, v1, v4}, Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;->BYTE4:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    const/4 v0, 0x3

    new-array v0, v0, [Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    sget-object v1, Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;->BYTE1:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    aput-object v1, v0, v2

    sget-object v1, Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;->BYTE2:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    aput-object v1, v0, v3

    sget-object v1, Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;->BYTE4:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    aput-object v1, v0, v4

    sput-object v0, Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;->ENUM$VALUES:[Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 75
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    return-object v0
.end method

.method public static values()[Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;->ENUM$VALUES:[Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    array-length v1, v0

    new-array v2, v1, [Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
