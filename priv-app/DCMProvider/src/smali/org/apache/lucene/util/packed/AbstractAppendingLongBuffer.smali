.class abstract Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;
.super Ljava/lang/Object;
.source "AbstractAppendingLongBuffer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer$Iterator;
    }
.end annotation


# static fields
.field static final BLOCK_BITS:I = 0xa

.field static final BLOCK_MASK:I = 0x3ff

.field static final MAX_PENDING_COUNT:I = 0x400


# instance fields
.field deltas:[Lorg/apache/lucene/util/packed/PackedInts$Reader;

.field private deltasBytes:J

.field minValues:[J

.field pending:[J

.field pendingOff:I

.field valuesOff:I


# direct methods
.method constructor <init>(I)V
    .locals 3
    .param p1, "initialBlockCount"    # I

    .prologue
    const/16 v2, 0x10

    const/4 v1, 0x0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-array v0, v2, [J

    iput-object v0, p0, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;->minValues:[J

    .line 41
    new-array v0, v2, [Lorg/apache/lucene/util/packed/PackedInts$Reader;

    iput-object v0, p0, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;->deltas:[Lorg/apache/lucene/util/packed/PackedInts$Reader;

    .line 42
    const/16 v0, 0x400

    new-array v0, v0, [J

    iput-object v0, p0, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;->pending:[J

    .line 43
    iput v1, p0, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;->valuesOff:I

    .line 44
    iput v1, p0, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;->pendingOff:I

    .line 45
    return-void
.end method


# virtual methods
.method public final add(J)V
    .locals 7
    .param p1, "l"    # J

    .prologue
    .line 54
    iget v1, p0, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;->pendingOff:I

    const/16 v2, 0x400

    if-ne v1, v2, :cond_2

    .line 56
    iget-object v1, p0, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;->deltas:[Lorg/apache/lucene/util/packed/PackedInts$Reader;

    array-length v1, v1

    iget v2, p0, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;->valuesOff:I

    if-ne v1, v2, :cond_0

    .line 57
    iget v1, p0, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;->valuesOff:I

    add-int/lit8 v1, v1, 0x1

    const/16 v2, 0x8

    invoke-static {v1, v2}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v0

    .line 58
    .local v0, "newLength":I
    invoke-virtual {p0, v0}, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;->grow(I)V

    .line 60
    .end local v0    # "newLength":I
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;->packPendingValues()V

    .line 61
    iget-object v1, p0, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;->deltas:[Lorg/apache/lucene/util/packed/PackedInts$Reader;

    iget v2, p0, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;->valuesOff:I

    aget-object v1, v1, v2

    if-eqz v1, :cond_1

    .line 62
    iget-wide v2, p0, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;->deltasBytes:J

    iget-object v1, p0, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;->deltas:[Lorg/apache/lucene/util/packed/PackedInts$Reader;

    iget v4, p0, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;->valuesOff:I

    aget-object v1, v1, v4

    invoke-interface {v1}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->ramBytesUsed()J

    move-result-wide v4

    add-long/2addr v2, v4

    iput-wide v2, p0, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;->deltasBytes:J

    .line 64
    :cond_1
    iget v1, p0, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;->valuesOff:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;->valuesOff:I

    .line 66
    const/4 v1, 0x0

    iput v1, p0, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;->pendingOff:I

    .line 68
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;->pending:[J

    iget v2, p0, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;->pendingOff:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;->pendingOff:I

    aput-wide p1, v1, v2

    .line 69
    return-void
.end method

.method baseRamBytesUsed()J
    .locals 2

    .prologue
    .line 131
    sget v0, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_HEADER:I

    .line 132
    sget v1, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_REF:I

    mul-int/lit8 v1, v1, 0x3

    .line 131
    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x8

    int-to-long v0, v0

    return-wide v0
.end method

.method abstract get(II)J
.end method

.method public final get(J)J
    .locals 5
    .param p1, "index"    # J

    .prologue
    .line 80
    const-wide/16 v2, 0x0

    cmp-long v2, p1, v2

    if-ltz v2, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;->size()J

    move-result-wide v2

    cmp-long v2, p1, v2

    if-ltz v2, :cond_1

    .line 81
    :cond_0
    new-instance v2, Ljava/lang/IndexOutOfBoundsException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 83
    :cond_1
    const/16 v2, 0xa

    shr-long v2, p1, v2

    long-to-int v0, v2

    .line 84
    .local v0, "block":I
    const-wide/16 v2, 0x3ff

    and-long/2addr v2, p1

    long-to-int v1, v2

    .line 85
    .local v1, "element":I
    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;->get(II)J

    move-result-wide v2

    return-wide v2
.end method

.method grow(I)V
    .locals 1
    .param p1, "newBlockCount"    # I

    .prologue
    .line 72
    iget-object v0, p0, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;->minValues:[J

    invoke-static {v0, p1}, Ljava/util/Arrays;->copyOf([JI)[J

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;->minValues:[J

    .line 73
    iget-object v0, p0, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;->deltas:[Lorg/apache/lucene/util/packed/PackedInts$Reader;

    invoke-static {v0, p1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/lucene/util/packed/PackedInts$Reader;

    iput-object v0, p0, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;->deltas:[Lorg/apache/lucene/util/packed/PackedInts$Reader;

    .line 74
    return-void
.end method

.method abstract iterator()Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer$Iterator;
.end method

.method abstract packPendingValues()V
.end method

.method public ramBytesUsed()J
    .locals 10

    .prologue
    .line 141
    invoke-virtual {p0}, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;->baseRamBytesUsed()J

    move-result-wide v2

    invoke-static {v2, v3}, Lorg/apache/lucene/util/RamUsageEstimator;->alignObjectSize(J)J

    move-result-wide v2

    .line 142
    const-wide/16 v4, 0x8

    .line 141
    add-long/2addr v2, v4

    .line 143
    iget-object v4, p0, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;->pending:[J

    invoke-static {v4}, Lorg/apache/lucene/util/RamUsageEstimator;->sizeOf([J)J

    move-result-wide v4

    .line 141
    add-long/2addr v2, v4

    .line 144
    iget-object v4, p0, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;->minValues:[J

    invoke-static {v4}, Lorg/apache/lucene/util/RamUsageEstimator;->sizeOf([J)J

    move-result-wide v4

    .line 141
    add-long/2addr v2, v4

    .line 145
    sget v4, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_ARRAY_HEADER:I

    int-to-long v4, v4

    sget v6, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_REF:I

    int-to-long v6, v6

    iget-object v8, p0, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;->deltas:[Lorg/apache/lucene/util/packed/PackedInts$Reader;

    array-length v8, v8

    int-to-long v8, v8

    mul-long/2addr v6, v8

    add-long/2addr v4, v6

    invoke-static {v4, v5}, Lorg/apache/lucene/util/RamUsageEstimator;->alignObjectSize(J)J

    move-result-wide v4

    .line 141
    add-long v0, v2, v4

    .line 147
    .local v0, "bytesUsed":J
    iget-wide v2, p0, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;->deltasBytes:J

    add-long/2addr v2, v0

    return-wide v2
.end method

.method public final size()J
    .locals 4

    .prologue
    .line 49
    iget v0, p0, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;->valuesOff:I

    int-to-long v0, v0

    const-wide/16 v2, 0x400

    mul-long/2addr v0, v2

    iget v2, p0, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;->pendingOff:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    return-wide v0
.end method
