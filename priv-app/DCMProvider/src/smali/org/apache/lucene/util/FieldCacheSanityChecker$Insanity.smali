.class public final Lorg/apache/lucene/util/FieldCacheSanityChecker$Insanity;
.super Ljava/lang/Object;
.source "FieldCacheSanityChecker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/FieldCacheSanityChecker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Insanity"
.end annotation


# instance fields
.field private final entries:[Lorg/apache/lucene/search/FieldCache$CacheEntry;

.field private final msg:Ljava/lang/String;

.field private final type:Lorg/apache/lucene/util/FieldCacheSanityChecker$InsanityType;


# direct methods
.method public varargs constructor <init>(Lorg/apache/lucene/util/FieldCacheSanityChecker$InsanityType;Ljava/lang/String;[Lorg/apache/lucene/search/FieldCache$CacheEntry;)V
    .locals 2
    .param p1, "type"    # Lorg/apache/lucene/util/FieldCacheSanityChecker$InsanityType;
    .param p2, "msg"    # Ljava/lang/String;
    .param p3, "entries"    # [Lorg/apache/lucene/search/FieldCache$CacheEntry;

    .prologue
    .line 341
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 342
    if-nez p1, :cond_0

    .line 343
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 344
    const-string v1, "Insanity requires non-null InsanityType"

    .line 343
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 346
    :cond_0
    if-eqz p3, :cond_1

    array-length v0, p3

    if-nez v0, :cond_2

    .line 347
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 348
    const-string v1, "Insanity requires non-null/non-empty CacheEntry[]"

    .line 347
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 350
    :cond_2
    iput-object p1, p0, Lorg/apache/lucene/util/FieldCacheSanityChecker$Insanity;->type:Lorg/apache/lucene/util/FieldCacheSanityChecker$InsanityType;

    .line 351
    iput-object p2, p0, Lorg/apache/lucene/util/FieldCacheSanityChecker$Insanity;->msg:Ljava/lang/String;

    .line 352
    iput-object p3, p0, Lorg/apache/lucene/util/FieldCacheSanityChecker$Insanity;->entries:[Lorg/apache/lucene/search/FieldCache$CacheEntry;

    .line 354
    return-void
.end method


# virtual methods
.method public getCacheEntries()[Lorg/apache/lucene/search/FieldCache$CacheEntry;
    .locals 1

    .prologue
    .line 366
    iget-object v0, p0, Lorg/apache/lucene/util/FieldCacheSanityChecker$Insanity;->entries:[Lorg/apache/lucene/search/FieldCache$CacheEntry;

    return-object v0
.end method

.method public getMsg()Ljava/lang/String;
    .locals 1

    .prologue
    .line 362
    iget-object v0, p0, Lorg/apache/lucene/util/FieldCacheSanityChecker$Insanity;->msg:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Lorg/apache/lucene/util/FieldCacheSanityChecker$InsanityType;
    .locals 1

    .prologue
    .line 358
    iget-object v0, p0, Lorg/apache/lucene/util/FieldCacheSanityChecker$Insanity;->type:Lorg/apache/lucene/util/FieldCacheSanityChecker$InsanityType;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 7

    .prologue
    const/16 v6, 0xa

    .line 374
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 375
    .local v0, "buf":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lorg/apache/lucene/util/FieldCacheSanityChecker$Insanity;->getType()Lorg/apache/lucene/util/FieldCacheSanityChecker$InsanityType;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 377
    invoke-virtual {p0}, Lorg/apache/lucene/util/FieldCacheSanityChecker$Insanity;->getMsg()Ljava/lang/String;

    move-result-object v3

    .line 378
    .local v3, "m":Ljava/lang/String;
    if-eqz v3, :cond_0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 380
    :cond_0
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 382
    invoke-virtual {p0}, Lorg/apache/lucene/util/FieldCacheSanityChecker$Insanity;->getCacheEntries()[Lorg/apache/lucene/search/FieldCache$CacheEntry;

    move-result-object v1

    .line 383
    .local v1, "ce":[Lorg/apache/lucene/search/FieldCache$CacheEntry;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v4, v1

    if-lt v2, v4, :cond_1

    .line 387
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4

    .line 384
    :cond_1
    const/16 v4, 0x9

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v1, v2

    invoke-virtual {v5}, Lorg/apache/lucene/search/FieldCache$CacheEntry;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 383
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method
