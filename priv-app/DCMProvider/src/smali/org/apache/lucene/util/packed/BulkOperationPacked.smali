.class Lorg/apache/lucene/util/packed/BulkOperationPacked;
.super Lorg/apache/lucene/util/packed/BulkOperation;
.source "BulkOperationPacked.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final bitsPerValue:I

.field private final byteBlockCount:I

.field private final byteValueCount:I

.field private final intMask:I

.field private final longBlockCount:I

.field private final longValueCount:I

.field private final mask:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lorg/apache/lucene/util/packed/BulkOperationPacked;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(I)V
    .locals 8
    .param p1, "bitsPerValue"    # I

    .prologue
    const-wide/16 v6, 0x1

    const/16 v4, 0x40

    .line 34
    invoke-direct {p0}, Lorg/apache/lucene/util/packed/BulkOperation;-><init>()V

    .line 35
    iput p1, p0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->bitsPerValue:I

    .line 36
    sget-boolean v3, Lorg/apache/lucene/util/packed/BulkOperationPacked;->$assertionsDisabled:Z

    if-nez v3, :cond_1

    if-lez p1, :cond_0

    if-le p1, v4, :cond_1

    :cond_0
    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 37
    :cond_1
    move v0, p1

    .line 38
    .local v0, "blocks":I
    :goto_0
    and-int/lit8 v3, v0, 0x1

    if-eqz v3, :cond_3

    .line 41
    iput v0, p0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->longBlockCount:I

    .line 42
    iget v3, p0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->longBlockCount:I

    mul-int/lit8 v3, v3, 0x40

    div-int/2addr v3, p1

    iput v3, p0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->longValueCount:I

    .line 43
    iget v3, p0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->longBlockCount:I

    mul-int/lit8 v1, v3, 0x8

    .line 44
    .local v1, "byteBlockCount":I
    iget v2, p0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->longValueCount:I

    .line 45
    .local v2, "byteValueCount":I
    :goto_1
    and-int/lit8 v3, v1, 0x1

    if-nez v3, :cond_2

    and-int/lit8 v3, v2, 0x1

    if-eqz v3, :cond_4

    .line 49
    :cond_2
    iput v1, p0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->byteBlockCount:I

    .line 50
    iput v2, p0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->byteValueCount:I

    .line 51
    if-ne p1, v4, :cond_5

    .line 52
    const-wide/16 v4, -0x1

    iput-wide v4, p0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->mask:J

    .line 56
    :goto_2
    iget-wide v4, p0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->mask:J

    long-to-int v3, v4

    iput v3, p0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->intMask:I

    .line 57
    sget-boolean v3, Lorg/apache/lucene/util/packed/BulkOperationPacked;->$assertionsDisabled:Z

    if-nez v3, :cond_6

    iget v3, p0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->longValueCount:I

    mul-int/2addr v3, p1

    iget v4, p0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->longBlockCount:I

    mul-int/lit8 v4, v4, 0x40

    if-eq v3, v4, :cond_6

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 39
    .end local v1    # "byteBlockCount":I
    .end local v2    # "byteValueCount":I
    :cond_3
    ushr-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 46
    .restart local v1    # "byteBlockCount":I
    .restart local v2    # "byteValueCount":I
    :cond_4
    ushr-int/lit8 v1, v1, 0x1

    .line 47
    ushr-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 54
    :cond_5
    shl-long v4, v6, p1

    sub-long/2addr v4, v6

    iput-wide v4, p0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->mask:J

    goto :goto_2

    .line 58
    :cond_6
    return-void
.end method


# virtual methods
.method public byteBlockCount()I
    .locals 1

    .prologue
    .line 72
    iget v0, p0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->byteBlockCount:I

    return v0
.end method

.method public byteValueCount()I
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->byteValueCount:I

    return v0
.end method

.method public decode([BI[III)V
    .locals 9
    .param p1, "blocks"    # [B
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [I
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 147
    const/4 v5, 0x0

    .line 148
    .local v5, "nextValue":I
    iget v1, p0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->bitsPerValue:I

    .line 149
    .local v1, "bitsLeft":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    iget v7, p0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->byteBlockCount:I

    mul-int/2addr v7, p5

    if-lt v4, v7, :cond_0

    .line 168
    sget-boolean v7, Lorg/apache/lucene/util/packed/BulkOperationPacked;->$assertionsDisabled:Z

    if-nez v7, :cond_3

    iget v7, p0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->bitsPerValue:I

    if-eq v1, v7, :cond_3

    new-instance v7, Ljava/lang/AssertionError;

    invoke-direct {v7}, Ljava/lang/AssertionError;-><init>()V

    throw v7

    .line 150
    :cond_0
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .local v2, "blocksOffset":I
    aget-byte v7, p1, p2

    and-int/lit16 v3, v7, 0xff

    .line 151
    .local v3, "bytes":I
    const/16 v7, 0x8

    if-le v1, v7, :cond_1

    .line 153
    add-int/lit8 v1, v1, -0x8

    .line 154
    shl-int v7, v3, v1

    or-int/2addr v5, v7

    .line 149
    :goto_1
    add-int/lit8 v4, v4, 0x1

    move p2, v2

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    goto :goto_0

    .line 157
    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    :cond_1
    rsub-int/lit8 v0, v1, 0x8

    .line 158
    .local v0, "bits":I
    add-int/lit8 v6, p4, 0x1

    .end local p4    # "valuesOffset":I
    .local v6, "valuesOffset":I
    ushr-int v7, v3, v0

    or-int/2addr v7, v5

    aput v7, p3, p4

    move p4, v6

    .line 159
    .end local v6    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    :goto_2
    iget v7, p0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->bitsPerValue:I

    if-ge v0, v7, :cond_2

    .line 164
    iget v7, p0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->bitsPerValue:I

    sub-int v1, v7, v0

    .line 165
    const/4 v7, 0x1

    shl-int/2addr v7, v0

    add-int/lit8 v7, v7, -0x1

    and-int/2addr v7, v3

    shl-int v5, v7, v1

    goto :goto_1

    .line 160
    :cond_2
    iget v7, p0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->bitsPerValue:I

    sub-int/2addr v0, v7

    .line 161
    add-int/lit8 v6, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v6    # "valuesOffset":I
    ushr-int v7, v3, v0

    iget v8, p0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->intMask:I

    and-int/2addr v7, v8

    aput v7, p3, p4

    move p4, v6

    .end local v6    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    goto :goto_2

    .line 169
    .end local v0    # "bits":I
    .end local v2    # "blocksOffset":I
    .end local v3    # "bytes":I
    .restart local p2    # "blocksOffset":I
    :cond_3
    return-void
.end method

.method public decode([BI[JII)V
    .locals 14
    .param p1, "blocks"    # [B
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [J
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 100
    const-wide/16 v6, 0x0

    .line 101
    .local v6, "nextValue":J
    iget v1, p0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->bitsPerValue:I

    .line 102
    .local v1, "bitsLeft":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget v9, p0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->byteBlockCount:I

    mul-int v9, v9, p5

    if-lt v3, v9, :cond_0

    .line 121
    sget-boolean v9, Lorg/apache/lucene/util/packed/BulkOperationPacked;->$assertionsDisabled:Z

    if-nez v9, :cond_3

    iget v9, p0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->bitsPerValue:I

    if-eq v1, v9, :cond_3

    new-instance v9, Ljava/lang/AssertionError;

    invoke-direct {v9}, Ljava/lang/AssertionError;-><init>()V

    throw v9

    .line 103
    :cond_0
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .local v2, "blocksOffset":I
    aget-byte v9, p1, p2

    int-to-long v10, v9

    const-wide/16 v12, 0xff

    and-long v4, v10, v12

    .line 104
    .local v4, "bytes":J
    const/16 v9, 0x8

    if-le v1, v9, :cond_1

    .line 106
    add-int/lit8 v1, v1, -0x8

    .line 107
    shl-long v10, v4, v1

    or-long/2addr v6, v10

    .line 102
    :goto_1
    add-int/lit8 v3, v3, 0x1

    move/from16 p2, v2

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    goto :goto_0

    .line 110
    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    :cond_1
    rsub-int/lit8 v0, v1, 0x8

    .line 111
    .local v0, "bits":I
    add-int/lit8 v8, p4, 0x1

    .end local p4    # "valuesOffset":I
    .local v8, "valuesOffset":I
    ushr-long v10, v4, v0

    or-long/2addr v10, v6

    aput-wide v10, p3, p4

    move/from16 p4, v8

    .line 112
    .end local v8    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    :goto_2
    iget v9, p0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->bitsPerValue:I

    if-ge v0, v9, :cond_2

    .line 117
    iget v9, p0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->bitsPerValue:I

    sub-int v1, v9, v0

    .line 118
    const-wide/16 v10, 0x1

    shl-long/2addr v10, v0

    const-wide/16 v12, 0x1

    sub-long/2addr v10, v12

    and-long/2addr v10, v4

    shl-long v6, v10, v1

    goto :goto_1

    .line 113
    :cond_2
    iget v9, p0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->bitsPerValue:I

    sub-int/2addr v0, v9

    .line 114
    add-int/lit8 v8, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v8    # "valuesOffset":I
    ushr-long v10, v4, v0

    iget-wide v12, p0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->mask:J

    and-long/2addr v10, v12

    aput-wide v10, p3, p4

    move/from16 p4, v8

    .end local v8    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    goto :goto_2

    .line 122
    .end local v0    # "bits":I
    .end local v2    # "blocksOffset":I
    .end local v4    # "bytes":J
    .restart local p2    # "blocksOffset":I
    :cond_3
    return-void
.end method

.method public decode([JI[III)V
    .locals 10
    .param p1, "blocks"    # [J
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [I
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 127
    iget v4, p0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->bitsPerValue:I

    const/16 v5, 0x20

    if-le v4, v5, :cond_0

    .line 128
    new-instance v4, Ljava/lang/UnsupportedOperationException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Cannot decode "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->bitsPerValue:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "-bits values into an int[]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 130
    :cond_0
    const/16 v0, 0x40

    .line 131
    .local v0, "bitsLeft":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget v4, p0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->longValueCount:I

    mul-int/2addr v4, p5

    if-lt v2, v4, :cond_1

    .line 142
    return-void

    .line 132
    :cond_1
    iget v4, p0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->bitsPerValue:I

    sub-int/2addr v0, v4

    .line 133
    if-gez v0, :cond_2

    .line 134
    add-int/lit8 v3, p4, 0x1

    .line 135
    .end local p4    # "valuesOffset":I
    .local v3, "valuesOffset":I
    add-int/lit8 v1, p2, 0x1

    .end local p2    # "blocksOffset":I
    .local v1, "blocksOffset":I
    aget-wide v4, p1, p2

    const-wide/16 v6, 0x1

    iget v8, p0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->bitsPerValue:I

    add-int/2addr v8, v0

    shl-long/2addr v6, v8

    const-wide/16 v8, 0x1

    sub-long/2addr v6, v8

    and-long/2addr v4, v6

    neg-int v6, v0

    shl-long/2addr v4, v6

    .line 136
    aget-wide v6, p1, v1

    add-int/lit8 v8, v0, 0x40

    ushr-long/2addr v6, v8

    .line 135
    or-long/2addr v4, v6

    long-to-int v4, v4

    .line 134
    aput v4, p3, p4

    .line 137
    add-int/lit8 v0, v0, 0x40

    move p4, v3

    .end local v3    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    move p2, v1

    .line 131
    .end local v1    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 139
    :cond_2
    add-int/lit8 v3, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v3    # "valuesOffset":I
    aget-wide v4, p1, p2

    ushr-long/2addr v4, v0

    iget-wide v6, p0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->mask:J

    and-long/2addr v4, v6

    long-to-int v4, v4

    aput v4, p3, p4

    move p4, v3

    .end local v3    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    goto :goto_1
.end method

.method public decode([JI[JII)V
    .locals 10
    .param p1, "blocks"    # [J
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [J
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 83
    const/16 v0, 0x40

    .line 84
    .local v0, "bitsLeft":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget v4, p0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->longValueCount:I

    mul-int/2addr v4, p5

    if-lt v2, v4, :cond_0

    .line 95
    return-void

    .line 85
    :cond_0
    iget v4, p0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->bitsPerValue:I

    sub-int/2addr v0, v4

    .line 86
    if-gez v0, :cond_1

    .line 87
    add-int/lit8 v3, p4, 0x1

    .line 88
    .end local p4    # "valuesOffset":I
    .local v3, "valuesOffset":I
    add-int/lit8 v1, p2, 0x1

    .end local p2    # "blocksOffset":I
    .local v1, "blocksOffset":I
    aget-wide v4, p1, p2

    const-wide/16 v6, 0x1

    iget v8, p0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->bitsPerValue:I

    add-int/2addr v8, v0

    shl-long/2addr v6, v8

    const-wide/16 v8, 0x1

    sub-long/2addr v6, v8

    and-long/2addr v4, v6

    neg-int v6, v0

    shl-long/2addr v4, v6

    .line 89
    aget-wide v6, p1, v1

    add-int/lit8 v8, v0, 0x40

    ushr-long/2addr v6, v8

    .line 88
    or-long/2addr v4, v6

    .line 87
    aput-wide v4, p3, p4

    .line 90
    add-int/lit8 v0, v0, 0x40

    move p4, v3

    .end local v3    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    move p2, v1

    .line 84
    .end local v1    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 92
    :cond_1
    add-int/lit8 v3, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v3    # "valuesOffset":I
    aget-wide v4, p1, p2

    ushr-long/2addr v4, v0

    iget-wide v6, p0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->mask:J

    and-long/2addr v4, v6

    aput-wide v4, p3, p4

    move p4, v3

    .end local v3    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    goto :goto_1
.end method

.method public encode([II[BII)V
    .locals 12
    .param p1, "values"    # [I
    .param p2, "valuesOffset"    # I
    .param p3, "blocks"    # [B
    .param p4, "blocksOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 248
    const/4 v4, 0x0

    .line 249
    .local v4, "nextBlock":I
    const/16 v1, 0x8

    .line 250
    .local v1, "bitsLeft":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget v7, p0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->byteValueCount:I

    mul-int v7, v7, p5

    if-lt v3, v7, :cond_0

    .line 270
    sget-boolean v7, Lorg/apache/lucene/util/packed/BulkOperationPacked;->$assertionsDisabled:Z

    if-nez v7, :cond_4

    const/16 v7, 0x8

    if-eq v1, v7, :cond_4

    new-instance v7, Ljava/lang/AssertionError;

    invoke-direct {v7}, Ljava/lang/AssertionError;-><init>()V

    throw v7

    .line 251
    :cond_0
    add-int/lit8 v6, p2, 0x1

    .end local p2    # "valuesOffset":I
    .local v6, "valuesOffset":I
    aget v5, p1, p2

    .line 252
    .local v5, "v":I
    sget-boolean v7, Lorg/apache/lucene/util/packed/BulkOperationPacked;->$assertionsDisabled:Z

    if-nez v7, :cond_1

    int-to-long v8, v5

    const-wide v10, 0xffffffffL

    and-long/2addr v8, v10

    invoke-static {v8, v9}, Lorg/apache/lucene/util/packed/PackedInts;->bitsRequired(J)I

    move-result v7

    iget v8, p0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->bitsPerValue:I

    if-le v7, v8, :cond_1

    new-instance v7, Ljava/lang/AssertionError;

    invoke-direct {v7}, Ljava/lang/AssertionError;-><init>()V

    throw v7

    .line 253
    :cond_1
    iget v7, p0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->bitsPerValue:I

    if-ge v7, v1, :cond_2

    .line 255
    iget v7, p0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->bitsPerValue:I

    sub-int v7, v1, v7

    shl-int v7, v5, v7

    or-int/2addr v4, v7

    .line 256
    iget v7, p0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->bitsPerValue:I

    sub-int/2addr v1, v7

    .line 250
    :goto_1
    add-int/lit8 v3, v3, 0x1

    move p2, v6

    .end local v6    # "valuesOffset":I
    .restart local p2    # "valuesOffset":I
    goto :goto_0

    .line 259
    .end local p2    # "valuesOffset":I
    .restart local v6    # "valuesOffset":I
    :cond_2
    iget v7, p0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->bitsPerValue:I

    sub-int v0, v7, v1

    .line 260
    .local v0, "bits":I
    add-int/lit8 v2, p4, 0x1

    .end local p4    # "blocksOffset":I
    .local v2, "blocksOffset":I
    ushr-int v7, v5, v0

    or-int/2addr v7, v4

    int-to-byte v7, v7

    aput-byte v7, p3, p4

    .line 261
    :goto_2
    const/16 v7, 0x8

    if-ge v0, v7, :cond_3

    .line 266
    rsub-int/lit8 v1, v0, 0x8

    .line 267
    const/4 v7, 0x1

    shl-int/2addr v7, v0

    add-int/lit8 v7, v7, -0x1

    and-int/2addr v7, v5

    shl-int v4, v7, v1

    move/from16 p4, v2

    .end local v2    # "blocksOffset":I
    .restart local p4    # "blocksOffset":I
    goto :goto_1

    .line 262
    .end local p4    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    :cond_3
    add-int/lit8 v0, v0, -0x8

    .line 263
    add-int/lit8 p4, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p4    # "blocksOffset":I
    ushr-int v7, v5, v0

    int-to-byte v7, v7

    aput-byte v7, p3, v2

    move/from16 v2, p4

    .end local p4    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    goto :goto_2

    .line 271
    .end local v0    # "bits":I
    .end local v2    # "blocksOffset":I
    .end local v5    # "v":I
    .end local v6    # "valuesOffset":I
    .restart local p2    # "valuesOffset":I
    .restart local p4    # "blocksOffset":I
    :cond_4
    return-void
.end method

.method public encode([II[JII)V
    .locals 12
    .param p1, "values"    # [I
    .param p2, "valuesOffset"    # I
    .param p3, "blocks"    # [J
    .param p4, "blocksOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 197
    const-wide/16 v4, 0x0

    .line 198
    .local v4, "nextBlock":J
    const/16 v0, 0x40

    .line 199
    .local v0, "bitsLeft":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget v6, p0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->longValueCount:I

    mul-int v6, v6, p5

    if-lt v2, v6, :cond_0

    .line 215
    return-void

    .line 200
    :cond_0
    iget v6, p0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->bitsPerValue:I

    sub-int/2addr v0, v6

    .line 201
    if-lez v0, :cond_1

    .line 202
    add-int/lit8 v3, p2, 0x1

    .end local p2    # "valuesOffset":I
    .local v3, "valuesOffset":I
    aget v6, p1, p2

    int-to-long v6, v6

    const-wide v8, 0xffffffffL

    and-long/2addr v6, v8

    shl-long/2addr v6, v0

    or-long/2addr v4, v6

    move p2, v3

    .line 199
    .end local v3    # "valuesOffset":I
    .restart local p2    # "valuesOffset":I
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 203
    :cond_1
    if-nez v0, :cond_2

    .line 204
    add-int/lit8 v3, p2, 0x1

    .end local p2    # "valuesOffset":I
    .restart local v3    # "valuesOffset":I
    aget v6, p1, p2

    int-to-long v6, v6

    const-wide v8, 0xffffffffL

    and-long/2addr v6, v8

    or-long/2addr v4, v6

    .line 205
    add-int/lit8 v1, p4, 0x1

    .end local p4    # "blocksOffset":I
    .local v1, "blocksOffset":I
    aput-wide v4, p3, p4

    .line 206
    const-wide/16 v4, 0x0

    .line 207
    const/16 v0, 0x40

    move/from16 p4, v1

    .end local v1    # "blocksOffset":I
    .restart local p4    # "blocksOffset":I
    move p2, v3

    .line 208
    .end local v3    # "valuesOffset":I
    .restart local p2    # "valuesOffset":I
    goto :goto_1

    .line 209
    :cond_2
    aget v6, p1, p2

    int-to-long v6, v6

    const-wide v8, 0xffffffffL

    and-long/2addr v6, v8

    neg-int v8, v0

    ushr-long/2addr v6, v8

    or-long/2addr v4, v6

    .line 210
    add-int/lit8 v1, p4, 0x1

    .end local p4    # "blocksOffset":I
    .restart local v1    # "blocksOffset":I
    aput-wide v4, p3, p4

    .line 211
    add-int/lit8 v3, p2, 0x1

    .end local p2    # "valuesOffset":I
    .restart local v3    # "valuesOffset":I
    aget v6, p1, p2

    int-to-long v6, v6

    const-wide/16 v8, 0x1

    neg-int v10, v0

    shl-long/2addr v8, v10

    const-wide/16 v10, 0x1

    sub-long/2addr v8, v10

    and-long/2addr v6, v8

    add-int/lit8 v8, v0, 0x40

    shl-long v4, v6, v8

    .line 212
    add-int/lit8 v0, v0, 0x40

    move/from16 p4, v1

    .end local v1    # "blocksOffset":I
    .restart local p4    # "blocksOffset":I
    move p2, v3

    .end local v3    # "valuesOffset":I
    .restart local p2    # "valuesOffset":I
    goto :goto_1
.end method

.method public encode([JI[BII)V
    .locals 12
    .param p1, "values"    # [J
    .param p2, "valuesOffset"    # I
    .param p3, "blocks"    # [B
    .param p4, "blocksOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 220
    const/4 v4, 0x0

    .line 221
    .local v4, "nextBlock":I
    const/16 v1, 0x8

    .line 222
    .local v1, "bitsLeft":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget v8, p0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->byteValueCount:I

    mul-int v8, v8, p5

    if-lt v3, v8, :cond_0

    .line 242
    sget-boolean v8, Lorg/apache/lucene/util/packed/BulkOperationPacked;->$assertionsDisabled:Z

    if-nez v8, :cond_4

    const/16 v8, 0x8

    if-eq v1, v8, :cond_4

    new-instance v8, Ljava/lang/AssertionError;

    invoke-direct {v8}, Ljava/lang/AssertionError;-><init>()V

    throw v8

    .line 223
    :cond_0
    add-int/lit8 v5, p2, 0x1

    .end local p2    # "valuesOffset":I
    .local v5, "valuesOffset":I
    aget-wide v6, p1, p2

    .line 224
    .local v6, "v":J
    sget-boolean v8, Lorg/apache/lucene/util/packed/BulkOperationPacked;->$assertionsDisabled:Z

    if-nez v8, :cond_1

    iget v8, p0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->bitsPerValue:I

    const/16 v9, 0x40

    if-eq v8, v9, :cond_1

    invoke-static {v6, v7}, Lorg/apache/lucene/util/packed/PackedInts;->bitsRequired(J)I

    move-result v8

    iget v9, p0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->bitsPerValue:I

    if-le v8, v9, :cond_1

    new-instance v8, Ljava/lang/AssertionError;

    invoke-direct {v8}, Ljava/lang/AssertionError;-><init>()V

    throw v8

    .line 225
    :cond_1
    iget v8, p0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->bitsPerValue:I

    if-ge v8, v1, :cond_2

    .line 227
    int-to-long v8, v4

    iget v10, p0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->bitsPerValue:I

    sub-int v10, v1, v10

    shl-long v10, v6, v10

    or-long/2addr v8, v10

    long-to-int v4, v8

    .line 228
    iget v8, p0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->bitsPerValue:I

    sub-int/2addr v1, v8

    .line 222
    :goto_1
    add-int/lit8 v3, v3, 0x1

    move p2, v5

    .end local v5    # "valuesOffset":I
    .restart local p2    # "valuesOffset":I
    goto :goto_0

    .line 231
    .end local p2    # "valuesOffset":I
    .restart local v5    # "valuesOffset":I
    :cond_2
    iget v8, p0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->bitsPerValue:I

    sub-int v0, v8, v1

    .line 232
    .local v0, "bits":I
    add-int/lit8 v2, p4, 0x1

    .end local p4    # "blocksOffset":I
    .local v2, "blocksOffset":I
    int-to-long v8, v4

    ushr-long v10, v6, v0

    or-long/2addr v8, v10

    long-to-int v8, v8

    int-to-byte v8, v8

    aput-byte v8, p3, p4

    .line 233
    :goto_2
    const/16 v8, 0x8

    if-ge v0, v8, :cond_3

    .line 238
    rsub-int/lit8 v1, v0, 0x8

    .line 239
    const-wide/16 v8, 0x1

    shl-long/2addr v8, v0

    const-wide/16 v10, 0x1

    sub-long/2addr v8, v10

    and-long/2addr v8, v6

    shl-long/2addr v8, v1

    long-to-int v4, v8

    move/from16 p4, v2

    .end local v2    # "blocksOffset":I
    .restart local p4    # "blocksOffset":I
    goto :goto_1

    .line 234
    .end local p4    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    :cond_3
    add-int/lit8 v0, v0, -0x8

    .line 235
    add-int/lit8 p4, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p4    # "blocksOffset":I
    ushr-long v8, v6, v0

    long-to-int v8, v8

    int-to-byte v8, v8

    aput-byte v8, p3, v2

    move/from16 v2, p4

    .end local p4    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    goto :goto_2

    .line 243
    .end local v0    # "bits":I
    .end local v2    # "blocksOffset":I
    .end local v5    # "valuesOffset":I
    .end local v6    # "v":J
    .restart local p2    # "valuesOffset":I
    .restart local p4    # "blocksOffset":I
    :cond_4
    return-void
.end method

.method public encode([JI[JII)V
    .locals 12
    .param p1, "values"    # [J
    .param p2, "valuesOffset"    # I
    .param p3, "blocks"    # [J
    .param p4, "blocksOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 174
    const-wide/16 v4, 0x0

    .line 175
    .local v4, "nextBlock":J
    const/16 v0, 0x40

    .line 176
    .local v0, "bitsLeft":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget v6, p0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->longValueCount:I

    mul-int v6, v6, p5

    if-lt v2, v6, :cond_0

    .line 192
    return-void

    .line 177
    :cond_0
    iget v6, p0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->bitsPerValue:I

    sub-int/2addr v0, v6

    .line 178
    if-lez v0, :cond_1

    .line 179
    add-int/lit8 v3, p2, 0x1

    .end local p2    # "valuesOffset":I
    .local v3, "valuesOffset":I
    aget-wide v6, p1, p2

    shl-long/2addr v6, v0

    or-long/2addr v4, v6

    move p2, v3

    .line 176
    .end local v3    # "valuesOffset":I
    .restart local p2    # "valuesOffset":I
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 180
    :cond_1
    if-nez v0, :cond_2

    .line 181
    add-int/lit8 v3, p2, 0x1

    .end local p2    # "valuesOffset":I
    .restart local v3    # "valuesOffset":I
    aget-wide v6, p1, p2

    or-long/2addr v4, v6

    .line 182
    add-int/lit8 v1, p4, 0x1

    .end local p4    # "blocksOffset":I
    .local v1, "blocksOffset":I
    aput-wide v4, p3, p4

    .line 183
    const-wide/16 v4, 0x0

    .line 184
    const/16 v0, 0x40

    move/from16 p4, v1

    .end local v1    # "blocksOffset":I
    .restart local p4    # "blocksOffset":I
    move p2, v3

    .line 185
    .end local v3    # "valuesOffset":I
    .restart local p2    # "valuesOffset":I
    goto :goto_1

    .line 186
    :cond_2
    aget-wide v6, p1, p2

    neg-int v8, v0

    ushr-long/2addr v6, v8

    or-long/2addr v4, v6

    .line 187
    add-int/lit8 v1, p4, 0x1

    .end local p4    # "blocksOffset":I
    .restart local v1    # "blocksOffset":I
    aput-wide v4, p3, p4

    .line 188
    add-int/lit8 v3, p2, 0x1

    .end local p2    # "valuesOffset":I
    .restart local v3    # "valuesOffset":I
    aget-wide v6, p1, p2

    const-wide/16 v8, 0x1

    neg-int v10, v0

    shl-long/2addr v8, v10

    const-wide/16 v10, 0x1

    sub-long/2addr v8, v10

    and-long/2addr v6, v8

    add-int/lit8 v8, v0, 0x40

    shl-long v4, v6, v8

    .line 189
    add-int/lit8 v0, v0, 0x40

    move/from16 p4, v1

    .end local v1    # "blocksOffset":I
    .restart local p4    # "blocksOffset":I
    move p2, v3

    .end local v3    # "valuesOffset":I
    .restart local p2    # "valuesOffset":I
    goto :goto_1
.end method

.method public longBlockCount()I
    .locals 1

    .prologue
    .line 62
    iget v0, p0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->longBlockCount:I

    return v0
.end method

.method public longValueCount()I
    .locals 1

    .prologue
    .line 67
    iget v0, p0, Lorg/apache/lucene/util/packed/BulkOperationPacked;->longValueCount:I

    return v0
.end method
