.class public final Lorg/apache/lucene/util/automaton/SpecialOperations;
.super Ljava/lang/Object;
.source "SpecialOperations.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static findIndex(I[I)I
    .locals 5
    .param p0, "c"    # I
    .param p1, "points"    # [I

    .prologue
    .line 55
    const/4 v0, 0x0

    .line 56
    .local v0, "a":I
    array-length v1, p1

    .line 57
    .local v1, "b":I
    :goto_0
    sub-int v3, v1, v0

    const/4 v4, 0x1

    if-gt v3, v4, :cond_1

    move v2, v0

    .line 63
    :cond_0
    return v2

    .line 58
    :cond_1
    add-int v3, v0, v1

    ushr-int/lit8 v2, v3, 0x1

    .line 59
    .local v2, "d":I
    aget v3, p1, v2

    if-le v3, p0, :cond_2

    move v1, v2

    goto :goto_0

    .line 60
    :cond_2
    aget v3, p1, v2

    if-ge v3, p0, :cond_0

    move v0, v2

    goto :goto_0
.end method

.method public static getCommonPrefix(Lorg/apache/lucene/util/automaton/Automaton;)Ljava/lang/String;
    .locals 7
    .param p0, "a"    # Lorg/apache/lucene/util/automaton/Automaton;

    .prologue
    .line 96
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->isSingleton()Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lorg/apache/lucene/util/automaton/Automaton;->singleton:Ljava/lang/String;

    .line 113
    :goto_0
    return-object v5

    .line 97
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 98
    .local v0, "b":Ljava/lang/StringBuilder;
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 99
    .local v4, "visited":Ljava/util/HashSet;, "Ljava/util/HashSet<Lorg/apache/lucene/util/automaton/State;>;"
    iget-object v2, p0, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    .line 102
    .local v2, "s":Lorg/apache/lucene/util/automaton/State;
    :cond_1
    const/4 v1, 0x1

    .line 103
    .local v1, "done":Z
    invoke-virtual {v4, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 104
    iget-boolean v5, v2, Lorg/apache/lucene/util/automaton/State;->accept:Z

    if-nez v5, :cond_2

    invoke-virtual {v2}, Lorg/apache/lucene/util/automaton/State;->numTransitions()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_2

    .line 105
    invoke-virtual {v2}, Lorg/apache/lucene/util/automaton/State;->getTransitions()Ljava/lang/Iterable;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/util/automaton/Transition;

    .line 106
    .local v3, "t":Lorg/apache/lucene/util/automaton/Transition;
    iget v5, v3, Lorg/apache/lucene/util/automaton/Transition;->min:I

    iget v6, v3, Lorg/apache/lucene/util/automaton/Transition;->max:I

    if-ne v5, v6, :cond_2

    iget-object v5, v3, Lorg/apache/lucene/util/automaton/Transition;->to:Lorg/apache/lucene/util/automaton/State;

    invoke-virtual {v4, v5}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 107
    iget v5, v3, Lorg/apache/lucene/util/automaton/Transition;->min:I

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->appendCodePoint(I)Ljava/lang/StringBuilder;

    .line 108
    iget-object v2, v3, Lorg/apache/lucene/util/automaton/Transition;->to:Lorg/apache/lucene/util/automaton/State;

    .line 109
    const/4 v1, 0x0

    .line 112
    .end local v3    # "t":Lorg/apache/lucene/util/automaton/Transition;
    :cond_2
    if-eqz v1, :cond_1

    .line 113
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_0
.end method

.method public static getCommonPrefixBytesRef(Lorg/apache/lucene/util/automaton/Automaton;)Lorg/apache/lucene/util/BytesRef;
    .locals 8
    .param p0, "a"    # Lorg/apache/lucene/util/automaton/Automaton;

    .prologue
    .line 120
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->isSingleton()Z

    move-result v5

    if-eqz v5, :cond_0

    new-instance v1, Lorg/apache/lucene/util/BytesRef;

    iget-object v5, p0, Lorg/apache/lucene/util/automaton/Automaton;->singleton:Ljava/lang/String;

    invoke-direct {v1, v5}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    .line 138
    :goto_0
    return-object v1

    .line 121
    :cond_0
    new-instance v1, Lorg/apache/lucene/util/BytesRef;

    const/16 v5, 0xa

    invoke-direct {v1, v5}, Lorg/apache/lucene/util/BytesRef;-><init>(I)V

    .line 122
    .local v1, "ref":Lorg/apache/lucene/util/BytesRef;
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 123
    .local v4, "visited":Ljava/util/HashSet;, "Ljava/util/HashSet<Lorg/apache/lucene/util/automaton/State;>;"
    iget-object v2, p0, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    .line 126
    .local v2, "s":Lorg/apache/lucene/util/automaton/State;
    :cond_1
    const/4 v0, 0x1

    .line 127
    .local v0, "done":Z
    invoke-virtual {v4, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 128
    iget-boolean v5, v2, Lorg/apache/lucene/util/automaton/State;->accept:Z

    if-nez v5, :cond_2

    invoke-virtual {v2}, Lorg/apache/lucene/util/automaton/State;->numTransitions()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_2

    .line 129
    invoke-virtual {v2}, Lorg/apache/lucene/util/automaton/State;->getTransitions()Ljava/lang/Iterable;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/util/automaton/Transition;

    .line 130
    .local v3, "t":Lorg/apache/lucene/util/automaton/Transition;
    iget v5, v3, Lorg/apache/lucene/util/automaton/Transition;->min:I

    iget v6, v3, Lorg/apache/lucene/util/automaton/Transition;->max:I

    if-ne v5, v6, :cond_2

    iget-object v5, v3, Lorg/apache/lucene/util/automaton/Transition;->to:Lorg/apache/lucene/util/automaton/State;

    invoke-virtual {v4, v5}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 131
    iget v5, v1, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v5, v5, 0x1

    iput v5, v1, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-virtual {v1, v5}, Lorg/apache/lucene/util/BytesRef;->grow(I)V

    .line 132
    iget-object v5, v1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v6, v1, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v6, v6, -0x1

    iget v7, v3, Lorg/apache/lucene/util/automaton/Transition;->min:I

    int-to-byte v7, v7

    aput-byte v7, v5, v6

    .line 133
    iget-object v2, v3, Lorg/apache/lucene/util/automaton/Transition;->to:Lorg/apache/lucene/util/automaton/State;

    .line 134
    const/4 v0, 0x0

    .line 137
    .end local v3    # "t":Lorg/apache/lucene/util/automaton/Transition;
    :cond_2
    if-eqz v0, :cond_1

    goto :goto_0
.end method

.method public static getCommonSuffix(Lorg/apache/lucene/util/automaton/Automaton;)Ljava/lang/String;
    .locals 3
    .param p0, "a"    # Lorg/apache/lucene/util/automaton/Automaton;

    .prologue
    .line 148
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->isSingleton()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 149
    iget-object v1, p0, Lorg/apache/lucene/util/automaton/Automaton;->singleton:Ljava/lang/String;

    .line 155
    :goto_0
    return-object v1

    .line 152
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->clone()Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v0

    .line 153
    .local v0, "r":Lorg/apache/lucene/util/automaton/Automaton;
    invoke-static {v0}, Lorg/apache/lucene/util/automaton/SpecialOperations;->reverse(Lorg/apache/lucene/util/automaton/Automaton;)Ljava/util/Set;

    .line 154
    invoke-virtual {v0}, Lorg/apache/lucene/util/automaton/Automaton;->determinize()V

    .line 155
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Lorg/apache/lucene/util/automaton/SpecialOperations;->getCommonPrefix(Lorg/apache/lucene/util/automaton/Automaton;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->reverse()Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static getCommonSuffixBytesRef(Lorg/apache/lucene/util/automaton/Automaton;)Lorg/apache/lucene/util/BytesRef;
    .locals 3
    .param p0, "a"    # Lorg/apache/lucene/util/automaton/Automaton;

    .prologue
    .line 159
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->isSingleton()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 160
    new-instance v1, Lorg/apache/lucene/util/BytesRef;

    iget-object v2, p0, Lorg/apache/lucene/util/automaton/Automaton;->singleton:Ljava/lang/String;

    invoke-direct {v1, v2}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    .line 168
    :goto_0
    return-object v1

    .line 163
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->clone()Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v0

    .line 164
    .local v0, "r":Lorg/apache/lucene/util/automaton/Automaton;
    invoke-static {v0}, Lorg/apache/lucene/util/automaton/SpecialOperations;->reverse(Lorg/apache/lucene/util/automaton/Automaton;)Ljava/util/Set;

    .line 165
    invoke-virtual {v0}, Lorg/apache/lucene/util/automaton/Automaton;->determinize()V

    .line 166
    invoke-static {v0}, Lorg/apache/lucene/util/automaton/SpecialOperations;->getCommonPrefixBytesRef(Lorg/apache/lucene/util/automaton/Automaton;)Lorg/apache/lucene/util/BytesRef;

    move-result-object v1

    .line 167
    .local v1, "ref":Lorg/apache/lucene/util/BytesRef;
    invoke-static {v1}, Lorg/apache/lucene/util/automaton/SpecialOperations;->reverseBytes(Lorg/apache/lucene/util/BytesRef;)V

    goto :goto_0
.end method

.method public static getFiniteStrings(Lorg/apache/lucene/util/automaton/Automaton;I)Ljava/util/Set;
    .locals 5
    .param p0, "a"    # Lorg/apache/lucene/util/automaton/Automaton;
    .param p1, "limit"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/automaton/Automaton;",
            "I)",
            "Ljava/util/Set",
            "<",
            "Lorg/apache/lucene/util/IntsRef;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 226
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 227
    .local v0, "strings":Ljava/util/HashSet;, "Ljava/util/HashSet<Lorg/apache/lucene/util/IntsRef;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->isSingleton()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 228
    if-lez p1, :cond_1

    .line 229
    iget-object v1, p0, Lorg/apache/lucene/util/automaton/Automaton;->singleton:Ljava/lang/String;

    new-instance v2, Lorg/apache/lucene/util/IntsRef;

    invoke-direct {v2}, Lorg/apache/lucene/util/IntsRef;-><init>()V

    invoke-static {v1, v2}, Lorg/apache/lucene/util/fst/Util;->toUTF32(Ljava/lang/CharSequence;Lorg/apache/lucene/util/IntsRef;)Lorg/apache/lucene/util/IntsRef;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 236
    .end local v0    # "strings":Ljava/util/HashSet;, "Ljava/util/HashSet<Lorg/apache/lucene/util/IntsRef;>;"
    :cond_0
    :goto_0
    return-object v0

    .restart local v0    # "strings":Ljava/util/HashSet;, "Ljava/util/HashSet<Lorg/apache/lucene/util/IntsRef;>;"
    :cond_1
    move-object v0, v1

    .line 231
    goto :goto_0

    .line 233
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    new-instance v4, Lorg/apache/lucene/util/IntsRef;

    invoke-direct {v4}, Lorg/apache/lucene/util/IntsRef;-><init>()V

    invoke-static {v2, v3, v0, v4, p1}, Lorg/apache/lucene/util/automaton/SpecialOperations;->getFiniteStrings(Lorg/apache/lucene/util/automaton/State;Ljava/util/HashSet;Ljava/util/HashSet;Lorg/apache/lucene/util/IntsRef;I)Z

    move-result v2

    if-nez v2, :cond_0

    move-object v0, v1

    .line 234
    goto :goto_0
.end method

.method private static getFiniteStrings(Lorg/apache/lucene/util/automaton/State;Ljava/util/HashSet;Ljava/util/HashSet;Lorg/apache/lucene/util/IntsRef;I)Z
    .locals 6
    .param p0, "s"    # Lorg/apache/lucene/util/automaton/State;
    .param p3, "path"    # Lorg/apache/lucene/util/IntsRef;
    .param p4, "limit"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/automaton/State;",
            "Ljava/util/HashSet",
            "<",
            "Lorg/apache/lucene/util/automaton/State;",
            ">;",
            "Ljava/util/HashSet",
            "<",
            "Lorg/apache/lucene/util/IntsRef;",
            ">;",
            "Lorg/apache/lucene/util/IntsRef;",
            "I)Z"
        }
    .end annotation

    .prologue
    .local p1, "pathstates":Ljava/util/HashSet;, "Ljava/util/HashSet<Lorg/apache/lucene/util/automaton/State;>;"
    .local p2, "strings":Ljava/util/HashSet;, "Ljava/util/HashSet<Lorg/apache/lucene/util/IntsRef;>;"
    const/4 v2, 0x0

    .line 246
    invoke-virtual {p1, p0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 247
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/State;->getTransitions()Ljava/lang/Iterable;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_2

    .line 267
    invoke-virtual {p1, p0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 268
    const/4 v2, 0x1

    :cond_1
    return v2

    .line 247
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/util/automaton/Transition;

    .line 248
    .local v1, "t":Lorg/apache/lucene/util/automaton/Transition;
    iget-object v4, v1, Lorg/apache/lucene/util/automaton/Transition;->to:Lorg/apache/lucene/util/automaton/State;

    invoke-virtual {p1, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 251
    iget v0, v1, Lorg/apache/lucene/util/automaton/Transition;->min:I

    .local v0, "n":I
    :goto_0
    iget v4, v1, Lorg/apache/lucene/util/automaton/Transition;->max:I

    if-gt v0, v4, :cond_0

    .line 252
    iget v4, p3, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {p3, v4}, Lorg/apache/lucene/util/IntsRef;->grow(I)V

    .line 253
    iget-object v4, p3, Lorg/apache/lucene/util/IntsRef;->ints:[I

    iget v5, p3, Lorg/apache/lucene/util/IntsRef;->length:I

    aput v0, v4, v5

    .line 254
    iget v4, p3, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p3, Lorg/apache/lucene/util/IntsRef;->length:I

    .line 255
    iget-object v4, v1, Lorg/apache/lucene/util/automaton/Transition;->to:Lorg/apache/lucene/util/automaton/State;

    iget-boolean v4, v4, Lorg/apache/lucene/util/automaton/State;->accept:Z

    if-eqz v4, :cond_3

    .line 256
    invoke-static {p3}, Lorg/apache/lucene/util/IntsRef;->deepCopyOf(Lorg/apache/lucene/util/IntsRef;)Lorg/apache/lucene/util/IntsRef;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 257
    if-ltz p4, :cond_3

    invoke-virtual {p2}, Ljava/util/HashSet;->size()I

    move-result v4

    if-gt v4, p4, :cond_1

    .line 261
    :cond_3
    iget-object v4, v1, Lorg/apache/lucene/util/automaton/Transition;->to:Lorg/apache/lucene/util/automaton/State;

    invoke-static {v4, p1, p2, p3, p4}, Lorg/apache/lucene/util/automaton/SpecialOperations;->getFiniteStrings(Lorg/apache/lucene/util/automaton/State;Ljava/util/HashSet;Ljava/util/HashSet;Lorg/apache/lucene/util/IntsRef;I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 264
    iget v4, p3, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p3, Lorg/apache/lucene/util/IntsRef;->length:I

    .line 251
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static isFinite(Lorg/apache/lucene/util/automaton/Automaton;)Z
    .locals 4
    .param p0, "a"    # Lorg/apache/lucene/util/automaton/Automaton;

    .prologue
    .line 70
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->isSingleton()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 71
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    new-instance v1, Ljava/util/BitSet;

    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->getNumberOfStates()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/BitSet;-><init>(I)V

    new-instance v2, Ljava/util/BitSet;

    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->getNumberOfStates()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/BitSet;-><init>(I)V

    invoke-static {v0, v1, v2}, Lorg/apache/lucene/util/automaton/SpecialOperations;->isFinite(Lorg/apache/lucene/util/automaton/State;Ljava/util/BitSet;Ljava/util/BitSet;)Z

    move-result v0

    goto :goto_0
.end method

.method private static isFinite(Lorg/apache/lucene/util/automaton/State;Ljava/util/BitSet;Ljava/util/BitSet;)Z
    .locals 3
    .param p0, "s"    # Lorg/apache/lucene/util/automaton/State;
    .param p1, "path"    # Ljava/util/BitSet;
    .param p2, "visited"    # Ljava/util/BitSet;

    .prologue
    .line 81
    iget v1, p0, Lorg/apache/lucene/util/automaton/State;->number:I

    invoke-virtual {p1, v1}, Ljava/util/BitSet;->set(I)V

    .line 82
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/State;->getTransitions()Ljava/lang/Iterable;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 84
    iget v1, p0, Lorg/apache/lucene/util/automaton/State;->number:I

    invoke-virtual {p1, v1}, Ljava/util/BitSet;->clear(I)V

    .line 85
    iget v1, p0, Lorg/apache/lucene/util/automaton/State;->number:I

    invoke-virtual {p2, v1}, Ljava/util/BitSet;->set(I)V

    .line 86
    const/4 v1, 0x1

    :goto_0
    return v1

    .line 82
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/util/automaton/Transition;

    .line 83
    .local v0, "t":Lorg/apache/lucene/util/automaton/Transition;
    iget-object v2, v0, Lorg/apache/lucene/util/automaton/Transition;->to:Lorg/apache/lucene/util/automaton/State;

    iget v2, v2, Lorg/apache/lucene/util/automaton/State;->number:I

    invoke-virtual {p1, v2}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, v0, Lorg/apache/lucene/util/automaton/Transition;->to:Lorg/apache/lucene/util/automaton/State;

    iget v2, v2, Lorg/apache/lucene/util/automaton/State;->number:I

    invoke-virtual {p2, v2}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, v0, Lorg/apache/lucene/util/automaton/Transition;->to:Lorg/apache/lucene/util/automaton/State;

    invoke-static {v2, p1, p2}, Lorg/apache/lucene/util/automaton/SpecialOperations;->isFinite(Lorg/apache/lucene/util/automaton/State;Ljava/util/BitSet;Ljava/util/BitSet;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static reverse(Lorg/apache/lucene/util/automaton/Automaton;)Ljava/util/Set;
    .locals 15
    .param p0, "a"    # Lorg/apache/lucene/util/automaton/Automaton;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/automaton/Automaton;",
            ")",
            "Ljava/util/Set",
            "<",
            "Lorg/apache/lucene/util/automaton/State;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 186
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->expandSingleton()V

    .line 188
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 189
    .local v1, "m":Ljava/util/HashMap;, "Ljava/util/HashMap<Lorg/apache/lucene/util/automaton/State;Ljava/util/HashSet<Lorg/apache/lucene/util/automaton/Transition;>;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->getNumberedStates()[Lorg/apache/lucene/util/automaton/State;

    move-result-object v4

    .line 190
    .local v4, "states":[Lorg/apache/lucene/util/automaton/State;
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 191
    .local v0, "accept":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/util/automaton/State;>;"
    array-length v8, v4

    move v7, v9

    :goto_0
    if-lt v7, v8, :cond_0

    .line 194
    array-length v8, v4

    move v7, v9

    :goto_1
    if-lt v7, v8, :cond_2

    .line 198
    array-length v10, v4

    move v8, v9

    :goto_2
    if-lt v8, v10, :cond_3

    .line 201
    array-length v10, v4

    move v8, v9

    :goto_3
    if-lt v8, v10, :cond_5

    .line 206
    iget-object v7, p0, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    const/4 v8, 0x1

    iput-boolean v8, v7, Lorg/apache/lucene/util/automaton/State;->accept:Z

    .line 207
    new-instance v7, Lorg/apache/lucene/util/automaton/State;

    invoke-direct {v7}, Lorg/apache/lucene/util/automaton/State;-><init>()V

    iput-object v7, p0, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    .line 208
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_4
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_6

    .line 210
    iput-boolean v9, p0, Lorg/apache/lucene/util/automaton/Automaton;->deterministic:Z

    .line 211
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->clearNumberedStates()V

    .line 212
    return-object v0

    .line 191
    :cond_0
    aget-object v3, v4, v7

    .line 192
    .local v3, "s":Lorg/apache/lucene/util/automaton/State;
    invoke-virtual {v3}, Lorg/apache/lucene/util/automaton/State;->isAccept()Z

    move-result v10

    if-eqz v10, :cond_1

    .line 193
    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 191
    :cond_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 194
    .end local v3    # "s":Lorg/apache/lucene/util/automaton/State;
    :cond_2
    aget-object v2, v4, v7

    .line 195
    .local v2, "r":Lorg/apache/lucene/util/automaton/State;
    new-instance v10, Ljava/util/HashSet;

    invoke-direct {v10}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {v1, v2, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 196
    iput-boolean v9, v2, Lorg/apache/lucene/util/automaton/State;->accept:Z

    .line 194
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 198
    .end local v2    # "r":Lorg/apache/lucene/util/automaton/State;
    :cond_3
    aget-object v2, v4, v8

    .line 199
    .restart local v2    # "r":Lorg/apache/lucene/util/automaton/State;
    invoke-virtual {v2}, Lorg/apache/lucene/util/automaton/State;->getTransitions()Ljava/lang/Iterable;

    move-result-object v7

    invoke-interface {v7}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_5
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_4

    .line 198
    add-int/lit8 v7, v8, 0x1

    move v8, v7

    goto :goto_2

    .line 199
    :cond_4
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/util/automaton/Transition;

    .line 200
    .local v5, "t":Lorg/apache/lucene/util/automaton/Transition;
    iget-object v7, v5, Lorg/apache/lucene/util/automaton/Transition;->to:Lorg/apache/lucene/util/automaton/State;

    invoke-virtual {v1, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/HashSet;

    new-instance v12, Lorg/apache/lucene/util/automaton/Transition;

    iget v13, v5, Lorg/apache/lucene/util/automaton/Transition;->min:I

    iget v14, v5, Lorg/apache/lucene/util/automaton/Transition;->max:I

    invoke-direct {v12, v13, v14, v2}, Lorg/apache/lucene/util/automaton/Transition;-><init>(IILorg/apache/lucene/util/automaton/State;)V

    invoke-virtual {v7, v12}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 201
    .end local v2    # "r":Lorg/apache/lucene/util/automaton/State;
    .end local v5    # "t":Lorg/apache/lucene/util/automaton/Transition;
    :cond_5
    aget-object v2, v4, v8

    .line 202
    .restart local v2    # "r":Lorg/apache/lucene/util/automaton/State;
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Set;

    .line 203
    .local v6, "tr":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/util/automaton/Transition;>;"
    invoke-interface {v6}, Ljava/util/Set;->size()I

    move-result v7

    new-array v7, v7, [Lorg/apache/lucene/util/automaton/Transition;

    invoke-interface {v6, v7}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Lorg/apache/lucene/util/automaton/Transition;

    invoke-virtual {v2, v7}, Lorg/apache/lucene/util/automaton/State;->setTransitions([Lorg/apache/lucene/util/automaton/Transition;)V

    .line 201
    add-int/lit8 v7, v8, 0x1

    move v8, v7

    goto/16 :goto_3

    .line 208
    .end local v2    # "r":Lorg/apache/lucene/util/automaton/State;
    .end local v6    # "tr":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/util/automaton/Transition;>;"
    :cond_6
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/util/automaton/State;

    .line 209
    .restart local v2    # "r":Lorg/apache/lucene/util/automaton/State;
    iget-object v8, p0, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    invoke-virtual {v8, v2}, Lorg/apache/lucene/util/automaton/State;->addEpsilon(Lorg/apache/lucene/util/automaton/State;)V

    goto :goto_4
.end method

.method private static reverseBytes(Lorg/apache/lucene/util/BytesRef;)V
    .locals 7
    .param p0, "ref"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 172
    iget v3, p0, Lorg/apache/lucene/util/BytesRef;->length:I

    const/4 v4, 0x1

    if-gt v3, v4, :cond_1

    .line 179
    :cond_0
    return-void

    .line 173
    :cond_1
    iget v3, p0, Lorg/apache/lucene/util/BytesRef;->length:I

    shr-int/lit8 v2, v3, 0x1

    .line 174
    .local v2, "num":I
    iget v1, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    .local v1, "i":I
    :goto_0
    iget v3, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    add-int/2addr v3, v2

    if-ge v1, v3, :cond_0

    .line 175
    iget-object v3, p0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    aget-byte v0, v3, v1

    .line 176
    .local v0, "b":B
    iget-object v3, p0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget-object v4, p0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v5, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    mul-int/lit8 v5, v5, 0x2

    iget v6, p0, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/2addr v5, v6

    sub-int/2addr v5, v1

    add-int/lit8 v5, v5, -0x1

    aget-byte v4, v4, v5

    aput-byte v4, v3, v1

    .line 177
    iget-object v3, p0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v4, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    mul-int/lit8 v4, v4, 0x2

    iget v5, p0, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/2addr v4, v5

    sub-int/2addr v4, v1

    add-int/lit8 v4, v4, -0x1

    aput-byte v0, v3, v4

    .line 174
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
