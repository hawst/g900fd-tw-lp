.class public final Lorg/apache/lucene/util/LongsRef;
.super Ljava/lang/Object;
.source "LongsRef.java"

# interfaces
.implements Ljava/lang/Cloneable;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lorg/apache/lucene/util/LongsRef;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final EMPTY_LONGS:[J


# instance fields
.field public length:I

.field public longs:[J

.field public offset:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 25
    const-class v0, Lorg/apache/lucene/util/LongsRef;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/LongsRef;->$assertionsDisabled:Z

    .line 27
    new-array v0, v1, [J

    sput-object v0, Lorg/apache/lucene/util/LongsRef;->EMPTY_LONGS:[J

    return-void

    :cond_0
    move v0, v1

    .line 25
    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    sget-object v0, Lorg/apache/lucene/util/LongsRef;->EMPTY_LONGS:[J

    iput-object v0, p0, Lorg/apache/lucene/util/LongsRef;->longs:[J

    .line 39
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "capacity"    # I

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    new-array v0, p1, [J

    iput-object v0, p0, Lorg/apache/lucene/util/LongsRef;->longs:[J

    .line 47
    return-void
.end method

.method public constructor <init>([JII)V
    .locals 1
    .param p1, "longs"    # [J
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, Lorg/apache/lucene/util/LongsRef;->longs:[J

    .line 53
    iput p2, p0, Lorg/apache/lucene/util/LongsRef;->offset:I

    .line 54
    iput p3, p0, Lorg/apache/lucene/util/LongsRef;->length:I

    .line 55
    sget-boolean v0, Lorg/apache/lucene/util/LongsRef;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/util/LongsRef;->isValid()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 56
    :cond_0
    return-void
.end method

.method public static deepCopyOf(Lorg/apache/lucene/util/LongsRef;)Lorg/apache/lucene/util/LongsRef;
    .locals 1
    .param p0, "other"    # Lorg/apache/lucene/util/LongsRef;

    .prologue
    .line 171
    new-instance v0, Lorg/apache/lucene/util/LongsRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/LongsRef;-><init>()V

    .line 172
    .local v0, "clone":Lorg/apache/lucene/util/LongsRef;
    invoke-virtual {v0, p0}, Lorg/apache/lucene/util/LongsRef;->copyLongs(Lorg/apache/lucene/util/LongsRef;)V

    .line 173
    return-object v0
.end method


# virtual methods
.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/util/LongsRef;->clone()Lorg/apache/lucene/util/LongsRef;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lorg/apache/lucene/util/LongsRef;
    .locals 4

    .prologue
    .line 60
    new-instance v0, Lorg/apache/lucene/util/LongsRef;

    iget-object v1, p0, Lorg/apache/lucene/util/LongsRef;->longs:[J

    iget v2, p0, Lorg/apache/lucene/util/LongsRef;->offset:I

    iget v3, p0, Lorg/apache/lucene/util/LongsRef;->length:I

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/util/LongsRef;-><init>([JII)V

    return-object v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/util/LongsRef;

    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/LongsRef;->compareTo(Lorg/apache/lucene/util/LongsRef;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lorg/apache/lucene/util/LongsRef;)I
    .locals 14
    .param p1, "other"    # Lorg/apache/lucene/util/LongsRef;

    .prologue
    .line 104
    if-ne p0, p1, :cond_0

    const/4 v12, 0x0

    .line 124
    :goto_0
    return v12

    .line 106
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/util/LongsRef;->longs:[J

    .line 107
    .local v2, "aInts":[J
    iget v3, p0, Lorg/apache/lucene/util/LongsRef;->offset:I

    .line 108
    .local v3, "aUpto":I
    iget-object v7, p1, Lorg/apache/lucene/util/LongsRef;->longs:[J

    .line 109
    .local v7, "bInts":[J
    iget v10, p1, Lorg/apache/lucene/util/LongsRef;->offset:I

    .line 111
    .local v10, "bUpto":I
    iget v12, p0, Lorg/apache/lucene/util/LongsRef;->length:I

    iget v13, p1, Lorg/apache/lucene/util/LongsRef;->length:I

    invoke-static {v12, v13}, Ljava/lang/Math;->min(II)I

    move-result v12

    add-int/2addr v12, v3

    int-to-long v4, v12

    .local v4, "aStop":J
    move v11, v10

    .end local v10    # "bUpto":I
    .local v11, "bUpto":I
    move v6, v3

    .line 113
    .end local v3    # "aUpto":I
    .local v6, "aUpto":I
    :goto_1
    int-to-long v12, v6

    cmp-long v12, v12, v4

    if-ltz v12, :cond_1

    .line 124
    iget v12, p0, Lorg/apache/lucene/util/LongsRef;->length:I

    iget v13, p1, Lorg/apache/lucene/util/LongsRef;->length:I

    sub-int/2addr v12, v13

    goto :goto_0

    .line 114
    :cond_1
    add-int/lit8 v3, v6, 0x1

    .end local v6    # "aUpto":I
    .restart local v3    # "aUpto":I
    aget-wide v0, v2, v6

    .line 115
    .local v0, "aInt":J
    add-int/lit8 v10, v11, 0x1

    .end local v11    # "bUpto":I
    .restart local v10    # "bUpto":I
    aget-wide v8, v7, v11

    .line 116
    .local v8, "bInt":J
    cmp-long v12, v0, v8

    if-lez v12, :cond_2

    .line 117
    const/4 v12, 0x1

    goto :goto_0

    .line 118
    :cond_2
    cmp-long v12, v0, v8

    if-gez v12, :cond_3

    .line 119
    const/4 v12, -0x1

    goto :goto_0

    :cond_3
    move v11, v10

    .end local v10    # "bUpto":I
    .restart local v11    # "bUpto":I
    move v6, v3

    .end local v3    # "aUpto":I
    .restart local v6    # "aUpto":I
    goto :goto_1
.end method

.method public copyLongs(Lorg/apache/lucene/util/LongsRef;)V
    .locals 5
    .param p1, "other"    # Lorg/apache/lucene/util/LongsRef;

    .prologue
    .line 128
    iget-object v0, p0, Lorg/apache/lucene/util/LongsRef;->longs:[J

    array-length v0, v0

    iget v1, p0, Lorg/apache/lucene/util/LongsRef;->offset:I

    sub-int/2addr v0, v1

    iget v1, p1, Lorg/apache/lucene/util/LongsRef;->length:I

    if-ge v0, v1, :cond_0

    .line 129
    iget v0, p1, Lorg/apache/lucene/util/LongsRef;->length:I

    new-array v0, v0, [J

    iput-object v0, p0, Lorg/apache/lucene/util/LongsRef;->longs:[J

    .line 130
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/util/LongsRef;->offset:I

    .line 132
    :cond_0
    iget-object v0, p1, Lorg/apache/lucene/util/LongsRef;->longs:[J

    iget v1, p1, Lorg/apache/lucene/util/LongsRef;->offset:I

    iget-object v2, p0, Lorg/apache/lucene/util/LongsRef;->longs:[J

    iget v3, p0, Lorg/apache/lucene/util/LongsRef;->offset:I

    iget v4, p1, Lorg/apache/lucene/util/LongsRef;->length:I

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 133
    iget v0, p1, Lorg/apache/lucene/util/LongsRef;->length:I

    iput v0, p0, Lorg/apache/lucene/util/LongsRef;->length:I

    .line 134
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x0

    .line 76
    if-nez p1, :cond_1

    .line 82
    .end local p1    # "other":Ljava/lang/Object;
    :cond_0
    :goto_0
    return v0

    .line 79
    .restart local p1    # "other":Ljava/lang/Object;
    :cond_1
    instance-of v1, p1, Lorg/apache/lucene/util/LongsRef;

    if-eqz v1, :cond_0

    .line 80
    check-cast p1, Lorg/apache/lucene/util/LongsRef;

    .end local p1    # "other":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/LongsRef;->longsEquals(Lorg/apache/lucene/util/LongsRef;)Z

    move-result v0

    goto :goto_0
.end method

.method public grow(I)V
    .locals 1
    .param p1, "newLength"    # I

    .prologue
    .line 142
    sget-boolean v0, Lorg/apache/lucene/util/LongsRef;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/util/LongsRef;->offset:I

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 143
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/util/LongsRef;->longs:[J

    array-length v0, v0

    if-ge v0, p1, :cond_1

    .line 144
    iget-object v0, p0, Lorg/apache/lucene/util/LongsRef;->longs:[J

    invoke-static {v0, p1}, Lorg/apache/lucene/util/ArrayUtil;->grow([JI)[J

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/util/LongsRef;->longs:[J

    .line 146
    :cond_1
    return-void
.end method

.method public hashCode()I
    .locals 11

    .prologue
    .line 65
    const/16 v3, 0x1f

    .line 66
    .local v3, "prime":I
    const/4 v4, 0x0

    .line 67
    .local v4, "result":I
    iget v5, p0, Lorg/apache/lucene/util/LongsRef;->offset:I

    iget v6, p0, Lorg/apache/lucene/util/LongsRef;->length:I

    add-int/2addr v5, v6

    int-to-long v0, v5

    .line 68
    .local v0, "end":J
    iget v2, p0, Lorg/apache/lucene/util/LongsRef;->offset:I

    .local v2, "i":I
    :goto_0
    int-to-long v6, v2

    cmp-long v5, v6, v0

    if-ltz v5, :cond_0

    .line 71
    return v4

    .line 69
    :cond_0
    mul-int/lit8 v5, v4, 0x1f

    iget-object v6, p0, Lorg/apache/lucene/util/LongsRef;->longs:[J

    aget-wide v6, v6, v2

    iget-object v8, p0, Lorg/apache/lucene/util/LongsRef;->longs:[J

    aget-wide v8, v8, v2

    const/16 v10, 0x20

    ushr-long/2addr v8, v10

    xor-long/2addr v6, v8

    long-to-int v6, v6

    add-int v4, v5, v6

    .line 68
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public isValid()Z
    .locals 3

    .prologue
    .line 181
    iget-object v0, p0, Lorg/apache/lucene/util/LongsRef;->longs:[J

    if-nez v0, :cond_0

    .line 182
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "longs is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 184
    :cond_0
    iget v0, p0, Lorg/apache/lucene/util/LongsRef;->length:I

    if-gez v0, :cond_1

    .line 185
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "length is negative: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/lucene/util/LongsRef;->length:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 187
    :cond_1
    iget v0, p0, Lorg/apache/lucene/util/LongsRef;->length:I

    iget-object v1, p0, Lorg/apache/lucene/util/LongsRef;->longs:[J

    array-length v1, v1

    if-le v0, v1, :cond_2

    .line 188
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "length is out of bounds: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/lucene/util/LongsRef;->length:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",longs.length="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/util/LongsRef;->longs:[J

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 190
    :cond_2
    iget v0, p0, Lorg/apache/lucene/util/LongsRef;->offset:I

    if-gez v0, :cond_3

    .line 191
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "offset is negative: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/lucene/util/LongsRef;->offset:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 193
    :cond_3
    iget v0, p0, Lorg/apache/lucene/util/LongsRef;->offset:I

    iget-object v1, p0, Lorg/apache/lucene/util/LongsRef;->longs:[J

    array-length v1, v1

    if-le v0, v1, :cond_4

    .line 194
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "offset out of bounds: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/lucene/util/LongsRef;->offset:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",longs.length="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/util/LongsRef;->longs:[J

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 196
    :cond_4
    iget v0, p0, Lorg/apache/lucene/util/LongsRef;->offset:I

    iget v1, p0, Lorg/apache/lucene/util/LongsRef;->length:I

    add-int/2addr v0, v1

    if-gez v0, :cond_5

    .line 197
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "offset+length is negative: offset="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/lucene/util/LongsRef;->offset:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",length="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/util/LongsRef;->length:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 199
    :cond_5
    iget v0, p0, Lorg/apache/lucene/util/LongsRef;->offset:I

    iget v1, p0, Lorg/apache/lucene/util/LongsRef;->length:I

    add-int/2addr v0, v1

    iget-object v1, p0, Lorg/apache/lucene/util/LongsRef;->longs:[J

    array-length v1, v1

    if-le v0, v1, :cond_6

    .line 200
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "offset+length out of bounds: offset="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/lucene/util/LongsRef;->offset:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",length="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/util/LongsRef;->length:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",longs.length="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/util/LongsRef;->longs:[J

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 202
    :cond_6
    const/4 v0, 0x1

    return v0
.end method

.method public longsEquals(Lorg/apache/lucene/util/LongsRef;)Z
    .locals 10
    .param p1, "other"    # Lorg/apache/lucene/util/LongsRef;

    .prologue
    const/4 v5, 0x0

    .line 86
    iget v6, p0, Lorg/apache/lucene/util/LongsRef;->length:I

    iget v7, p1, Lorg/apache/lucene/util/LongsRef;->length:I

    if-ne v6, v7, :cond_0

    .line 87
    iget v3, p1, Lorg/apache/lucene/util/LongsRef;->offset:I

    .line 88
    .local v3, "otherUpto":I
    iget-object v2, p1, Lorg/apache/lucene/util/LongsRef;->longs:[J

    .line 89
    .local v2, "otherInts":[J
    iget v6, p0, Lorg/apache/lucene/util/LongsRef;->offset:I

    iget v7, p0, Lorg/apache/lucene/util/LongsRef;->length:I

    add-int/2addr v6, v7

    int-to-long v0, v6

    .line 90
    .local v0, "end":J
    iget v4, p0, Lorg/apache/lucene/util/LongsRef;->offset:I

    .local v4, "upto":I
    :goto_0
    int-to-long v6, v4

    cmp-long v6, v6, v0

    if-ltz v6, :cond_1

    .line 95
    const/4 v5, 0x1

    .line 97
    .end local v0    # "end":J
    .end local v2    # "otherInts":[J
    .end local v3    # "otherUpto":I
    .end local v4    # "upto":I
    :cond_0
    return v5

    .line 91
    .restart local v0    # "end":J
    .restart local v2    # "otherInts":[J
    .restart local v3    # "otherUpto":I
    .restart local v4    # "upto":I
    :cond_1
    iget-object v6, p0, Lorg/apache/lucene/util/LongsRef;->longs:[J

    aget-wide v6, v6, v4

    aget-wide v8, v2, v3

    cmp-long v6, v6, v8

    if-nez v6, :cond_0

    .line 90
    add-int/lit8 v4, v4, 0x1

    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 150
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 151
    .local v3, "sb":Ljava/lang/StringBuilder;
    const/16 v4, 0x5b

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 152
    iget v4, p0, Lorg/apache/lucene/util/LongsRef;->offset:I

    iget v5, p0, Lorg/apache/lucene/util/LongsRef;->length:I

    add-int/2addr v4, v5

    int-to-long v0, v4

    .line 153
    .local v0, "end":J
    iget v2, p0, Lorg/apache/lucene/util/LongsRef;->offset:I

    .local v2, "i":I
    :goto_0
    int-to-long v4, v2

    cmp-long v4, v4, v0

    if-ltz v4, :cond_0

    .line 159
    const/16 v4, 0x5d

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 160
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4

    .line 154
    :cond_0
    iget v4, p0, Lorg/apache/lucene/util/LongsRef;->offset:I

    if-le v2, v4, :cond_1

    .line 155
    const/16 v4, 0x20

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 157
    :cond_1
    iget-object v4, p0, Lorg/apache/lucene/util/LongsRef;->longs:[J

    aget-wide v4, v4, v2

    invoke-static {v4, v5}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method
