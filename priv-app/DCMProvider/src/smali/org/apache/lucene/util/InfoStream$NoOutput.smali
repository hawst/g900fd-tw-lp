.class final Lorg/apache/lucene/util/InfoStream$NoOutput;
.super Lorg/apache/lucene/util/InfoStream;
.source "InfoStream.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/InfoStream;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "NoOutput"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lorg/apache/lucene/util/InfoStream;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/InfoStream$NoOutput;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lorg/apache/lucene/util/InfoStream;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/util/InfoStream$NoOutput;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lorg/apache/lucene/util/InfoStream$NoOutput;-><init>()V

    return-void
.end method


# virtual methods
.method public close()V
    .locals 0

    .prologue
    .line 49
    return-void
.end method

.method public isEnabled(Ljava/lang/String;)Z
    .locals 1
    .param p1, "component"    # Ljava/lang/String;

    .prologue
    .line 45
    const/4 v0, 0x0

    return v0
.end method

.method public message(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "component"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 40
    sget-boolean v0, Lorg/apache/lucene/util/InfoStream$NoOutput;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "message() should not be called when isEnabled returns false"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 41
    :cond_0
    return-void
.end method
