.class Lorg/apache/lucene/util/fst/FST$NodeAndInCount;
.super Ljava/lang/Object;
.source "FST.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/fst/FST;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "NodeAndInCount"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lorg/apache/lucene/util/fst/FST$NodeAndInCount;",
        ">;"
    }
.end annotation


# instance fields
.field final count:I

.field final node:I


# direct methods
.method public constructor <init>(II)V
    .locals 0
    .param p1, "node"    # I
    .param p2, "count"    # I

    .prologue
    .line 1784
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1785
    iput p1, p0, Lorg/apache/lucene/util/fst/FST$NodeAndInCount;->node:I

    .line 1786
    iput p2, p0, Lorg/apache/lucene/util/fst/FST$NodeAndInCount;->count:I

    .line 1787
    return-void
.end method


# virtual methods
.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/util/fst/FST$NodeAndInCount;

    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/fst/FST$NodeAndInCount;->compareTo(Lorg/apache/lucene/util/fst/FST$NodeAndInCount;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lorg/apache/lucene/util/fst/FST$NodeAndInCount;)I
    .locals 2

    .prologue
    .line 1791
    .local p1, "other":Lorg/apache/lucene/util/fst/FST$NodeAndInCount;, "Lorg/apache/lucene/util/fst/FST$NodeAndInCount;"
    iget v0, p0, Lorg/apache/lucene/util/fst/FST$NodeAndInCount;->count:I

    iget v1, p1, Lorg/apache/lucene/util/fst/FST$NodeAndInCount;->count:I

    if-le v0, v1, :cond_0

    .line 1792
    const/4 v0, 0x1

    .line 1797
    :goto_0
    return v0

    .line 1793
    :cond_0
    iget v0, p0, Lorg/apache/lucene/util/fst/FST$NodeAndInCount;->count:I

    iget v1, p1, Lorg/apache/lucene/util/fst/FST$NodeAndInCount;->count:I

    if-ge v0, v1, :cond_1

    .line 1794
    const/4 v0, -0x1

    goto :goto_0

    .line 1797
    :cond_1
    iget v0, p1, Lorg/apache/lucene/util/fst/FST$NodeAndInCount;->node:I

    iget v1, p0, Lorg/apache/lucene/util/fst/FST$NodeAndInCount;->node:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method
