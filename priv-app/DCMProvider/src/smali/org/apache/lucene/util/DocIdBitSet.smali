.class public Lorg/apache/lucene/util/DocIdBitSet;
.super Lorg/apache/lucene/search/DocIdSet;
.source "DocIdBitSet.java"

# interfaces
.implements Lorg/apache/lucene/util/Bits;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/DocIdBitSet$DocIdBitSetIterator;
    }
.end annotation


# instance fields
.field private final bitSet:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(Ljava/util/BitSet;)V
    .locals 0
    .param p1, "bitSet"    # Ljava/util/BitSet;

    .prologue
    .line 30
    invoke-direct {p0}, Lorg/apache/lucene/search/DocIdSet;-><init>()V

    .line 31
    iput-object p1, p0, Lorg/apache/lucene/util/DocIdBitSet;->bitSet:Ljava/util/BitSet;

    .line 32
    return-void
.end method


# virtual methods
.method public bits()Lorg/apache/lucene/util/Bits;
    .locals 0

    .prologue
    .line 41
    return-object p0
.end method

.method public get(I)Z
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 59
    iget-object v0, p0, Lorg/apache/lucene/util/DocIdBitSet;->bitSet:Ljava/util/BitSet;

    invoke-virtual {v0, p1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    return v0
.end method

.method public getBitSet()Ljava/util/BitSet;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lorg/apache/lucene/util/DocIdBitSet;->bitSet:Ljava/util/BitSet;

    return-object v0
.end method

.method public isCacheable()Z
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x1

    return v0
.end method

.method public iterator()Lorg/apache/lucene/search/DocIdSetIterator;
    .locals 2

    .prologue
    .line 36
    new-instance v0, Lorg/apache/lucene/util/DocIdBitSet$DocIdBitSetIterator;

    iget-object v1, p0, Lorg/apache/lucene/util/DocIdBitSet;->bitSet:Ljava/util/BitSet;

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/DocIdBitSet$DocIdBitSetIterator;-><init>(Ljava/util/BitSet;)V

    return-object v0
.end method

.method public length()I
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lorg/apache/lucene/util/DocIdBitSet;->bitSet:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->size()I

    move-result v0

    return v0
.end method
