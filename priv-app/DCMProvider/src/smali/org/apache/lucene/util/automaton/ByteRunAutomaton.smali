.class public Lorg/apache/lucene/util/automaton/ByteRunAutomaton;
.super Lorg/apache/lucene/util/automaton/RunAutomaton;
.source "ByteRunAutomaton.java"


# direct methods
.method public constructor <init>(Lorg/apache/lucene/util/automaton/Automaton;)V
    .locals 1
    .param p1, "a"    # Lorg/apache/lucene/util/automaton/Automaton;

    .prologue
    .line 26
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/util/automaton/ByteRunAutomaton;-><init>(Lorg/apache/lucene/util/automaton/Automaton;Z)V

    .line 27
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/automaton/Automaton;Z)V
    .locals 2
    .param p1, "a"    # Lorg/apache/lucene/util/automaton/Automaton;
    .param p2, "utf8"    # Z

    .prologue
    .line 31
    if-eqz p2, :cond_0

    .end local p1    # "a":Lorg/apache/lucene/util/automaton/Automaton;
    :goto_0
    const/16 v0, 0x100

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Lorg/apache/lucene/util/automaton/RunAutomaton;-><init>(Lorg/apache/lucene/util/automaton/Automaton;IZ)V

    .line 32
    return-void

    .line 31
    .restart local p1    # "a":Lorg/apache/lucene/util/automaton/Automaton;
    :cond_0
    new-instance v0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;

    invoke-direct {v0}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;-><init>()V

    invoke-virtual {v0, p1}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->convert(Lorg/apache/lucene/util/automaton/Automaton;)Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object p1

    goto :goto_0
.end method


# virtual methods
.method public run([BII)Z
    .locals 4
    .param p1, "s"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 38
    iget v2, p0, Lorg/apache/lucene/util/automaton/ByteRunAutomaton;->initial:I

    .line 39
    .local v2, "p":I
    add-int v1, p2, p3

    .line 40
    .local v1, "l":I
    move v0, p2

    .local v0, "i":I
    :goto_0
    if-lt v0, v1, :cond_0

    .line 44
    iget-object v3, p0, Lorg/apache/lucene/util/automaton/ByteRunAutomaton;->accept:[Z

    aget-boolean v3, v3, v2

    :goto_1
    return v3

    .line 41
    :cond_0
    aget-byte v3, p1, v0

    and-int/lit16 v3, v3, 0xff

    invoke-virtual {p0, v2, v3}, Lorg/apache/lucene/util/automaton/ByteRunAutomaton;->step(II)I

    move-result v2

    .line 42
    const/4 v3, -0x1

    if-ne v2, v3, :cond_1

    const/4 v3, 0x0

    goto :goto_1

    .line 40
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
