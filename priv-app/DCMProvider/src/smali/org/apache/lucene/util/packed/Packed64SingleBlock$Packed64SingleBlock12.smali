.class Lorg/apache/lucene/util/packed/Packed64SingleBlock$Packed64SingleBlock12;
.super Lorg/apache/lucene/util/packed/Packed64SingleBlock;
.source "Packed64SingleBlock.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/packed/Packed64SingleBlock;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Packed64SingleBlock12"
.end annotation


# direct methods
.method constructor <init>(I)V
    .locals 1
    .param p1, "valueCount"    # I

    .prologue
    .line 496
    const/16 v0, 0xc

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/util/packed/Packed64SingleBlock;-><init>(II)V

    .line 497
    return-void
.end method


# virtual methods
.method public get(I)J
    .locals 8
    .param p1, "index"    # I

    .prologue
    .line 501
    div-int/lit8 v1, p1, 0x5

    .line 502
    .local v1, "o":I
    rem-int/lit8 v0, p1, 0x5

    .line 503
    .local v0, "b":I
    mul-int/lit8 v2, v0, 0xc

    .line 504
    .local v2, "shift":I
    iget-object v3, p0, Lorg/apache/lucene/util/packed/Packed64SingleBlock$Packed64SingleBlock12;->blocks:[J

    aget-wide v4, v3, v1

    ushr-long/2addr v4, v2

    const-wide/16 v6, 0xfff

    and-long/2addr v4, v6

    return-wide v4
.end method

.method public set(IJ)V
    .locals 10
    .param p1, "index"    # I
    .param p2, "value"    # J

    .prologue
    .line 509
    div-int/lit8 v1, p1, 0x5

    .line 510
    .local v1, "o":I
    rem-int/lit8 v0, p1, 0x5

    .line 511
    .local v0, "b":I
    mul-int/lit8 v2, v0, 0xc

    .line 512
    .local v2, "shift":I
    iget-object v3, p0, Lorg/apache/lucene/util/packed/Packed64SingleBlock$Packed64SingleBlock12;->blocks:[J

    iget-object v4, p0, Lorg/apache/lucene/util/packed/Packed64SingleBlock$Packed64SingleBlock12;->blocks:[J

    aget-wide v4, v4, v1

    const-wide/16 v6, 0xfff

    shl-long/2addr v6, v2

    const-wide/16 v8, -0x1

    xor-long/2addr v6, v8

    and-long/2addr v4, v6

    shl-long v6, p2, v2

    or-long/2addr v4, v6

    aput-wide v4, v3, v1

    .line 513
    return-void
.end method
