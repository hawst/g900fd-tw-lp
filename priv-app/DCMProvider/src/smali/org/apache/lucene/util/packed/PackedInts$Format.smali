.class public enum Lorg/apache/lucene/util/packed/PackedInts$Format;
.super Ljava/lang/Enum;
.source "PackedInts.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/packed/PackedInts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4009
    name = "Format"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/lucene/util/packed/PackedInts$Format;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final synthetic ENUM$VALUES:[Lorg/apache/lucene/util/packed/PackedInts$Format;

.field public static final enum PACKED:Lorg/apache/lucene/util/packed/PackedInts$Format;

.field public static final enum PACKED_SINGLE_BLOCK:Lorg/apache/lucene/util/packed/PackedInts$Format;


# instance fields
.field public id:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 84
    const-class v0, Lorg/apache/lucene/util/packed/PackedInts;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/packed/PackedInts$Format;->$assertionsDisabled:Z

    .line 85
    new-instance v0, Lorg/apache/lucene/util/packed/PackedInts$Format$1;

    const-string v3, "PACKED"

    .line 88
    invoke-direct {v0, v3, v2, v2}, Lorg/apache/lucene/util/packed/PackedInts$Format$1;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/lucene/util/packed/PackedInts$Format;->PACKED:Lorg/apache/lucene/util/packed/PackedInts$Format;

    .line 101
    new-instance v0, Lorg/apache/lucene/util/packed/PackedInts$Format$2;

    const-string v3, "PACKED_SINGLE_BLOCK"

    .line 108
    invoke-direct {v0, v3, v1, v1}, Lorg/apache/lucene/util/packed/PackedInts$Format$2;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/lucene/util/packed/PackedInts$Format;->PACKED_SINGLE_BLOCK:Lorg/apache/lucene/util/packed/PackedInts$Format;

    const/4 v0, 0x2

    new-array v0, v0, [Lorg/apache/lucene/util/packed/PackedInts$Format;

    sget-object v3, Lorg/apache/lucene/util/packed/PackedInts$Format;->PACKED:Lorg/apache/lucene/util/packed/PackedInts$Format;

    aput-object v3, v0, v2

    sget-object v2, Lorg/apache/lucene/util/packed/PackedInts$Format;->PACKED_SINGLE_BLOCK:Lorg/apache/lucene/util/packed/PackedInts$Format;

    aput-object v2, v0, v1

    sput-object v0, Lorg/apache/lucene/util/packed/PackedInts$Format;->ENUM$VALUES:[Lorg/apache/lucene/util/packed/PackedInts$Format;

    return-void

    :cond_0
    move v0, v2

    .line 84
    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "id"    # I

    .prologue
    .line 143
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 144
    iput p3, p0, Lorg/apache/lucene/util/packed/PackedInts$Format;->id:I

    .line 145
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IILorg/apache/lucene/util/packed/PackedInts$Format;)V
    .locals 0

    .prologue
    .line 143
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/util/packed/PackedInts$Format;-><init>(Ljava/lang/String;II)V

    return-void
.end method

.method public static byId(I)Lorg/apache/lucene/util/packed/PackedInts$Format;
    .locals 5
    .param p0, "id"    # I

    .prologue
    .line 135
    invoke-static {}, Lorg/apache/lucene/util/packed/PackedInts$Format;->values()[Lorg/apache/lucene/util/packed/PackedInts$Format;

    move-result-object v2

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v3, :cond_0

    .line 140
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown format id: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 135
    :cond_0
    aget-object v0, v2, v1

    .line 136
    .local v0, "format":Lorg/apache/lucene/util/packed/PackedInts$Format;
    invoke-virtual {v0}, Lorg/apache/lucene/util/packed/PackedInts$Format;->getId()I

    move-result v4

    if-ne v4, p0, :cond_1

    .line 137
    return-object v0

    .line 135
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/lucene/util/packed/PackedInts$Format;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lorg/apache/lucene/util/packed/PackedInts$Format;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/util/packed/PackedInts$Format;

    return-object v0
.end method

.method public static values()[Lorg/apache/lucene/util/packed/PackedInts$Format;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lorg/apache/lucene/util/packed/PackedInts$Format;->ENUM$VALUES:[Lorg/apache/lucene/util/packed/PackedInts$Format;

    array-length v1, v0

    new-array v2, v1, [Lorg/apache/lucene/util/packed/PackedInts$Format;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public byteCount(III)J
    .locals 4
    .param p1, "packedIntsVersion"    # I
    .param p2, "valueCount"    # I
    .param p3, "bitsPerValue"    # I

    .prologue
    .line 161
    sget-boolean v0, Lorg/apache/lucene/util/packed/PackedInts$Format;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-ltz p3, :cond_0

    const/16 v0, 0x40

    if-le p3, v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, p3}, Ljava/lang/AssertionError;-><init>(I)V

    throw v0

    .line 163
    :cond_1
    const-wide/16 v0, 0x8

    invoke-virtual {p0, p1, p2, p3}, Lorg/apache/lucene/util/packed/PackedInts$Format;->longCount(III)I

    move-result v2

    int-to-long v2, v2

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 153
    iget v0, p0, Lorg/apache/lucene/util/packed/PackedInts$Format;->id:I

    return v0
.end method

.method public isSupported(I)Z
    .locals 2
    .param p1, "bitsPerValue"    # I

    .prologue
    const/4 v0, 0x1

    .line 186
    if-lt p1, v0, :cond_0

    const/16 v1, 0x40

    if-gt p1, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public longCount(III)I
    .locals 8
    .param p1, "packedIntsVersion"    # I
    .param p2, "valueCount"    # I
    .param p3, "bitsPerValue"    # I

    .prologue
    const-wide/16 v6, 0x8

    .line 171
    sget-boolean v2, Lorg/apache/lucene/util/packed/PackedInts$Format;->$assertionsDisabled:Z

    if-nez v2, :cond_1

    if-ltz p3, :cond_0

    const/16 v2, 0x40

    if-le p3, v2, :cond_1

    :cond_0
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, p3}, Ljava/lang/AssertionError;-><init>(I)V

    throw v2

    .line 172
    :cond_1
    invoke-virtual {p0, p1, p2, p3}, Lorg/apache/lucene/util/packed/PackedInts$Format;->byteCount(III)J

    move-result-wide v0

    .line 173
    .local v0, "byteCount":J
    sget-boolean v2, Lorg/apache/lucene/util/packed/PackedInts$Format;->$assertionsDisabled:Z

    if-nez v2, :cond_2

    const-wide v2, 0x3fffffff8L

    cmp-long v2, v0, v2

    if-ltz v2, :cond_2

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 174
    :cond_2
    rem-long v2, v0, v6

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    .line 175
    div-long v2, v0, v6

    long-to-int v2, v2

    .line 177
    :goto_0
    return v2

    :cond_3
    div-long v2, v0, v6

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    long-to-int v2, v2

    goto :goto_0
.end method

.method public overheadPerValue(I)F
    .locals 1
    .param p1, "bitsPerValue"    # I

    .prologue
    .line 193
    sget-boolean v0, Lorg/apache/lucene/util/packed/PackedInts$Format;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/packed/PackedInts$Format;->isSupported(I)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 194
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final overheadRatio(I)F
    .locals 2
    .param p1, "bitsPerValue"    # I

    .prologue
    .line 201
    sget-boolean v0, Lorg/apache/lucene/util/packed/PackedInts$Format;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/packed/PackedInts$Format;->isSupported(I)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 202
    :cond_0
    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/packed/PackedInts$Format;->overheadPerValue(I)F

    move-result v0

    int-to-float v1, p1

    div-float/2addr v0, v1

    return v0
.end method
