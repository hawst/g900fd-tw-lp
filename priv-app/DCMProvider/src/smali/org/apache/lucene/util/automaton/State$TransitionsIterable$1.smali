.class Lorg/apache/lucene/util/automaton/State$TransitionsIterable$1;
.super Ljava/lang/Object;
.source "State.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/util/automaton/State$TransitionsIterable;->iterator()Ljava/util/Iterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Lorg/apache/lucene/util/automaton/Transition;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lorg/apache/lucene/util/automaton/State$TransitionsIterable;

.field upto:I


# direct methods
.method constructor <init>(Lorg/apache/lucene/util/automaton/State$TransitionsIterable;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/util/automaton/State$TransitionsIterable$1;->this$1:Lorg/apache/lucene/util/automaton/State$TransitionsIterable;

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 2

    .prologue
    .line 77
    iget v0, p0, Lorg/apache/lucene/util/automaton/State$TransitionsIterable$1;->upto:I

    iget-object v1, p0, Lorg/apache/lucene/util/automaton/State$TransitionsIterable$1;->this$1:Lorg/apache/lucene/util/automaton/State$TransitionsIterable;

    # getter for: Lorg/apache/lucene/util/automaton/State$TransitionsIterable;->this$0:Lorg/apache/lucene/util/automaton/State;
    invoke-static {v1}, Lorg/apache/lucene/util/automaton/State$TransitionsIterable;->access$1(Lorg/apache/lucene/util/automaton/State$TransitionsIterable;)Lorg/apache/lucene/util/automaton/State;

    move-result-object v1

    iget v1, v1, Lorg/apache/lucene/util/automaton/State;->numTransitions:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/State$TransitionsIterable$1;->next()Lorg/apache/lucene/util/automaton/Transition;

    move-result-object v0

    return-object v0
.end method

.method public next()Lorg/apache/lucene/util/automaton/Transition;
    .locals 3

    .prologue
    .line 81
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/State$TransitionsIterable$1;->this$1:Lorg/apache/lucene/util/automaton/State$TransitionsIterable;

    # getter for: Lorg/apache/lucene/util/automaton/State$TransitionsIterable;->this$0:Lorg/apache/lucene/util/automaton/State;
    invoke-static {v0}, Lorg/apache/lucene/util/automaton/State$TransitionsIterable;->access$1(Lorg/apache/lucene/util/automaton/State$TransitionsIterable;)Lorg/apache/lucene/util/automaton/State;

    move-result-object v0

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/State;->transitionsArray:[Lorg/apache/lucene/util/automaton/Transition;

    iget v1, p0, Lorg/apache/lucene/util/automaton/State$TransitionsIterable$1;->upto:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/util/automaton/State$TransitionsIterable$1;->upto:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 85
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
