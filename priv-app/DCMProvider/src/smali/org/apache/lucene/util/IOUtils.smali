.class public final Lorg/apache/lucene/util/IOUtils;
.super Ljava/lang/Object;
.source "IOUtils.java"


# static fields
.field public static final CHARSET_UTF_8:Ljava/nio/charset/Charset;

.field private static final SUPPRESS_METHOD:Ljava/lang/reflect/Method;

.field public static final UTF_8:Ljava/lang/String; = "UTF-8"


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    .line 51
    const-string v2, "UTF-8"

    invoke-static {v2}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v2

    sput-object v2, Lorg/apache/lucene/util/IOUtils;->CHARSET_UTF_8:Ljava/nio/charset/Charset;

    .line 230
    :try_start_0
    const-class v2, Ljava/lang/Throwable;

    const-string v3, "addSuppressed"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    const-class v6, Ljava/lang/Throwable;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 234
    .local v1, "m":Ljava/lang/reflect/Method;
    :goto_0
    sput-object v1, Lorg/apache/lucene/util/IOUtils;->SUPPRESS_METHOD:Ljava/lang/reflect/Method;

    .line 235
    return-void

    .line 231
    .end local v1    # "m":Ljava/lang/reflect/Method;
    :catch_0
    move-exception v0

    .line 232
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    .restart local v1    # "m":Ljava/lang/reflect/Method;
    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static final addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V
    .locals 3
    .param p0, "exception"    # Ljava/lang/Throwable;
    .param p1, "suppressed"    # Ljava/lang/Throwable;

    .prologue
    .line 242
    sget-object v0, Lorg/apache/lucene/util/IOUtils;->SUPPRESS_METHOD:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_0

    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    .line 244
    :try_start_0
    sget-object v0, Lorg/apache/lucene/util/IOUtils;->SUPPRESS_METHOD:Ljava/lang/reflect/Method;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 249
    :cond_0
    :goto_0
    return-void

    .line 245
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static close(Ljava/lang/Iterable;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Ljava/io/Closeable;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 169
    .local p0, "objects":Ljava/lang/Iterable;, "Ljava/lang/Iterable<+Ljava/io/Closeable;>;"
    const/4 v2, 0x0

    .line 171
    .local v2, "th":Ljava/lang/Throwable;
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 184
    if-eqz v2, :cond_5

    .line 185
    instance-of v3, v2, Ljava/io/IOException;

    if-eqz v3, :cond_2

    check-cast v2, Ljava/io/IOException;

    .end local v2    # "th":Ljava/lang/Throwable;
    throw v2

    .line 171
    .restart local v2    # "th":Ljava/lang/Throwable;
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/Closeable;

    .line 173
    .local v0, "object":Ljava/io/Closeable;
    if-eqz v0, :cond_0

    .line 174
    :try_start_0
    invoke-interface {v0}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 176
    :catch_0
    move-exception v1

    .line 177
    .local v1, "t":Ljava/lang/Throwable;
    invoke-static {v2, v1}, Lorg/apache/lucene/util/IOUtils;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    .line 178
    if-nez v2, :cond_0

    .line 179
    move-object v2, v1

    goto :goto_0

    .line 186
    .end local v0    # "object":Ljava/io/Closeable;
    .end local v1    # "t":Ljava/lang/Throwable;
    :cond_2
    instance-of v3, v2, Ljava/lang/RuntimeException;

    if-eqz v3, :cond_3

    check-cast v2, Ljava/lang/RuntimeException;

    .end local v2    # "th":Ljava/lang/Throwable;
    throw v2

    .line 187
    .restart local v2    # "th":Ljava/lang/Throwable;
    :cond_3
    instance-of v3, v2, Ljava/lang/Error;

    if-eqz v3, :cond_4

    check-cast v2, Ljava/lang/Error;

    .end local v2    # "th":Ljava/lang/Throwable;
    throw v2

    .line 188
    .restart local v2    # "th":Ljava/lang/Throwable;
    :cond_4
    new-instance v3, Ljava/lang/RuntimeException;

    invoke-direct {v3, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v3

    .line 190
    :cond_5
    return-void
.end method

.method public static varargs close([Ljava/io/Closeable;)V
    .locals 5
    .param p0, "objects"    # [Ljava/io/Closeable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 141
    const/4 v2, 0x0

    .line 143
    .local v2, "th":Ljava/lang/Throwable;
    array-length v4, p0

    const/4 v3, 0x0

    :goto_0
    if-lt v3, v4, :cond_0

    .line 156
    if-eqz v2, :cond_5

    .line 157
    instance-of v3, v2, Ljava/io/IOException;

    if-eqz v3, :cond_2

    check-cast v2, Ljava/io/IOException;

    .end local v2    # "th":Ljava/lang/Throwable;
    throw v2

    .line 143
    .restart local v2    # "th":Ljava/lang/Throwable;
    :cond_0
    aget-object v0, p0, v3

    .line 145
    .local v0, "object":Ljava/io/Closeable;
    if-eqz v0, :cond_1

    .line 146
    :try_start_0
    invoke-interface {v0}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 143
    :cond_1
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 148
    :catch_0
    move-exception v1

    .line 149
    .local v1, "t":Ljava/lang/Throwable;
    invoke-static {v2, v1}, Lorg/apache/lucene/util/IOUtils;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    .line 150
    if-nez v2, :cond_1

    .line 151
    move-object v2, v1

    goto :goto_1

    .line 158
    .end local v0    # "object":Ljava/io/Closeable;
    .end local v1    # "t":Ljava/lang/Throwable;
    :cond_2
    instance-of v3, v2, Ljava/lang/RuntimeException;

    if-eqz v3, :cond_3

    check-cast v2, Ljava/lang/RuntimeException;

    .end local v2    # "th":Ljava/lang/Throwable;
    throw v2

    .line 159
    .restart local v2    # "th":Ljava/lang/Throwable;
    :cond_3
    instance-of v3, v2, Ljava/lang/Error;

    if-eqz v3, :cond_4

    check-cast v2, Ljava/lang/Error;

    .end local v2    # "th":Ljava/lang/Throwable;
    throw v2

    .line 160
    .restart local v2    # "th":Ljava/lang/Throwable;
    :cond_4
    new-instance v3, Ljava/lang/RuntimeException;

    invoke-direct {v3, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v3

    .line 162
    :cond_5
    return-void
.end method

.method public static closeWhileHandlingException(Ljava/lang/Exception;Ljava/lang/Iterable;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Exception;",
            ">(TE;",
            "Ljava/lang/Iterable",
            "<+",
            "Ljava/io/Closeable;",
            ">;)V^TE;^",
            "Ljava/io/IOException;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 105
    .local p0, "priorException":Ljava/lang/Exception;, "TE;"
    .local p1, "objects":Ljava/lang/Iterable;, "Ljava/lang/Iterable<+Ljava/io/Closeable;>;"
    const/4 v2, 0x0

    .line 107
    .local v2, "th":Ljava/lang/Throwable;
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 120
    if-eqz p0, :cond_3

    .line 121
    throw p0

    .line 107
    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/Closeable;

    .line 109
    .local v0, "object":Ljava/io/Closeable;
    if-eqz v0, :cond_0

    .line 110
    :try_start_0
    invoke-interface {v0}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 112
    :catch_0
    move-exception v1

    .line 113
    .local v1, "t":Ljava/lang/Throwable;
    if-nez p0, :cond_2

    move-object v3, v2

    :goto_1
    invoke-static {v3, v1}, Lorg/apache/lucene/util/IOUtils;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    .line 114
    if-nez v2, :cond_0

    .line 115
    move-object v2, v1

    goto :goto_0

    :cond_2
    move-object v3, p0

    .line 113
    goto :goto_1

    .line 122
    .end local v0    # "object":Ljava/io/Closeable;
    .end local v1    # "t":Ljava/lang/Throwable;
    :cond_3
    if-eqz v2, :cond_7

    .line 123
    instance-of v3, v2, Ljava/io/IOException;

    if-eqz v3, :cond_4

    check-cast v2, Ljava/io/IOException;

    .end local v2    # "th":Ljava/lang/Throwable;
    throw v2

    .line 124
    .restart local v2    # "th":Ljava/lang/Throwable;
    :cond_4
    instance-of v3, v2, Ljava/lang/RuntimeException;

    if-eqz v3, :cond_5

    check-cast v2, Ljava/lang/RuntimeException;

    .end local v2    # "th":Ljava/lang/Throwable;
    throw v2

    .line 125
    .restart local v2    # "th":Ljava/lang/Throwable;
    :cond_5
    instance-of v3, v2, Ljava/lang/Error;

    if-eqz v3, :cond_6

    check-cast v2, Ljava/lang/Error;

    .end local v2    # "th":Ljava/lang/Throwable;
    throw v2

    .line 126
    .restart local v2    # "th":Ljava/lang/Throwable;
    :cond_6
    new-instance v3, Ljava/lang/RuntimeException;

    invoke-direct {v3, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v3

    .line 128
    :cond_7
    return-void
.end method

.method public static varargs closeWhileHandlingException(Ljava/lang/Exception;[Ljava/io/Closeable;)V
    .locals 6
    .param p1, "objects"    # [Ljava/io/Closeable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Exception;",
            ">(TE;[",
            "Ljava/io/Closeable;",
            ")V^TE;^",
            "Ljava/io/IOException;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 76
    .local p0, "priorException":Ljava/lang/Exception;, "TE;"
    const/4 v2, 0x0

    .line 78
    .local v2, "th":Ljava/lang/Throwable;
    array-length v5, p1

    const/4 v3, 0x0

    move v4, v3

    :goto_0
    if-lt v4, v5, :cond_0

    .line 91
    if-eqz p0, :cond_3

    .line 92
    throw p0

    .line 78
    :cond_0
    aget-object v0, p1, v4

    .line 80
    .local v0, "object":Ljava/io/Closeable;
    if-eqz v0, :cond_1

    .line 81
    :try_start_0
    invoke-interface {v0}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 78
    :cond_1
    :goto_1
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_0

    .line 83
    :catch_0
    move-exception v1

    .line 84
    .local v1, "t":Ljava/lang/Throwable;
    if-nez p0, :cond_2

    move-object v3, v2

    :goto_2
    invoke-static {v3, v1}, Lorg/apache/lucene/util/IOUtils;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    .line 85
    if-nez v2, :cond_1

    .line 86
    move-object v2, v1

    goto :goto_1

    :cond_2
    move-object v3, p0

    .line 84
    goto :goto_2

    .line 93
    .end local v0    # "object":Ljava/io/Closeable;
    .end local v1    # "t":Ljava/lang/Throwable;
    :cond_3
    if-eqz v2, :cond_7

    .line 94
    instance-of v3, v2, Ljava/io/IOException;

    if-eqz v3, :cond_4

    check-cast v2, Ljava/io/IOException;

    .end local v2    # "th":Ljava/lang/Throwable;
    throw v2

    .line 95
    .restart local v2    # "th":Ljava/lang/Throwable;
    :cond_4
    instance-of v3, v2, Ljava/lang/RuntimeException;

    if-eqz v3, :cond_5

    check-cast v2, Ljava/lang/RuntimeException;

    .end local v2    # "th":Ljava/lang/Throwable;
    throw v2

    .line 96
    .restart local v2    # "th":Ljava/lang/Throwable;
    :cond_5
    instance-of v3, v2, Ljava/lang/Error;

    if-eqz v3, :cond_6

    check-cast v2, Ljava/lang/Error;

    .end local v2    # "th":Ljava/lang/Throwable;
    throw v2

    .line 97
    .restart local v2    # "th":Ljava/lang/Throwable;
    :cond_6
    new-instance v3, Ljava/lang/RuntimeException;

    invoke-direct {v3, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v3

    .line 99
    :cond_7
    return-void
.end method

.method public static closeWhileHandlingException(Ljava/lang/Iterable;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Ljava/io/Closeable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 215
    .local p0, "objects":Ljava/lang/Iterable;, "Ljava/lang/Iterable<+Ljava/io/Closeable;>;"
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 223
    return-void

    .line 215
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/Closeable;

    .line 217
    .local v0, "object":Ljava/io/Closeable;
    if-eqz v0, :cond_0

    .line 218
    :try_start_0
    invoke-interface {v0}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 220
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public static varargs closeWhileHandlingException([Ljava/io/Closeable;)V
    .locals 4
    .param p0, "objects"    # [Ljava/io/Closeable;

    .prologue
    .line 200
    array-length v2, p0

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v2, :cond_0

    .line 208
    return-void

    .line 200
    :cond_0
    aget-object v0, p0, v1

    .line 202
    .local v0, "object":Ljava/io/Closeable;
    if-eqz v0, :cond_1

    .line 203
    :try_start_0
    invoke-interface {v0}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 200
    :cond_1
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 205
    :catch_0
    move-exception v3

    goto :goto_1
.end method

.method public static copy(Ljava/io/File;Ljava/io/File;)V
    .locals 10
    .param p0, "source"    # Ljava/io/File;
    .param p1, "target"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 349
    const/4 v1, 0x0

    .line 350
    .local v1, "fis":Ljava/io/FileInputStream;
    const/4 v3, 0x0

    .line 352
    .local v3, "fos":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 353
    .end local v1    # "fis":Ljava/io/FileInputStream;
    .local v2, "fis":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 355
    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .local v4, "fos":Ljava/io/FileOutputStream;
    const/16 v6, 0x2000

    :try_start_2
    new-array v0, v6, [B

    .line 357
    .local v0, "buffer":[B
    :goto_0
    invoke-virtual {v2, v0}, Ljava/io/FileInputStream;->read([B)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v5

    .local v5, "len":I
    if-gtz v5, :cond_0

    .line 360
    new-array v6, v7, [Ljava/io/Closeable;

    .line 361
    aput-object v2, v6, v8

    aput-object v4, v6, v9

    invoke-static {v6}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 363
    return-void

    .line 358
    :cond_0
    const/4 v6, 0x0

    :try_start_3
    invoke-virtual {v4, v0, v6, v5}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 360
    .end local v0    # "buffer":[B
    .end local v5    # "len":I
    :catchall_0
    move-exception v6

    move-object v3, v4

    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    move-object v1, v2

    .end local v2    # "fis":Ljava/io/FileInputStream;
    .restart local v1    # "fis":Ljava/io/FileInputStream;
    :goto_1
    new-array v7, v7, [Ljava/io/Closeable;

    .line 361
    aput-object v1, v7, v8

    aput-object v3, v7, v9

    invoke-static {v7}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 362
    throw v6

    .line 360
    :catchall_1
    move-exception v6

    goto :goto_1

    .end local v1    # "fis":Ljava/io/FileInputStream;
    .restart local v2    # "fis":Ljava/io/FileInputStream;
    :catchall_2
    move-exception v6

    move-object v1, v2

    .end local v2    # "fis":Ljava/io/FileInputStream;
    .restart local v1    # "fis":Ljava/io/FileInputStream;
    goto :goto_1
.end method

.method public static varargs deleteFilesIgnoringExceptions(Lorg/apache/lucene/store/Directory;[Ljava/lang/String;)V
    .locals 4
    .param p0, "dir"    # Lorg/apache/lucene/store/Directory;
    .param p1, "files"    # [Ljava/lang/String;

    .prologue
    .line 335
    array-length v2, p1

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v2, :cond_0

    .line 342
    return-void

    .line 335
    :cond_0
    aget-object v0, p1, v1

    .line 337
    .local v0, "name":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0, v0}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 335
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 338
    :catch_0
    move-exception v3

    goto :goto_1
.end method

.method public static getDecodingReader(Ljava/io/File;Ljava/nio/charset/Charset;)Ljava/io/Reader;
    .locals 7
    .param p0, "file"    # Ljava/io/File;
    .param p1, "charSet"    # Ljava/nio/charset/Charset;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 284
    const/4 v1, 0x0

    .line 285
    .local v1, "stream":Ljava/io/FileInputStream;
    const/4 v3, 0x0

    .line 287
    .local v3, "success":Z
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 288
    .end local v1    # "stream":Ljava/io/FileInputStream;
    .local v2, "stream":Ljava/io/FileInputStream;
    :try_start_1
    invoke-static {v2, p1}, Lorg/apache/lucene/util/IOUtils;->getDecodingReader(Ljava/io/InputStream;Ljava/nio/charset/Charset;)Ljava/io/Reader;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 289
    .local v0, "reader":Ljava/io/Reader;
    const/4 v3, 0x1

    .line 293
    if-nez v3, :cond_0

    new-array v4, v5, [Ljava/io/Closeable;

    .line 294
    aput-object v2, v4, v6

    invoke-static {v4}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 290
    :cond_0
    return-object v0

    .line 292
    .end local v0    # "reader":Ljava/io/Reader;
    .end local v2    # "stream":Ljava/io/FileInputStream;
    .restart local v1    # "stream":Ljava/io/FileInputStream;
    :catchall_0
    move-exception v4

    .line 293
    :goto_0
    if-nez v3, :cond_1

    new-array v5, v5, [Ljava/io/Closeable;

    .line 294
    aput-object v1, v5, v6

    invoke-static {v5}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 296
    :cond_1
    throw v4

    .line 292
    .end local v1    # "stream":Ljava/io/FileInputStream;
    .restart local v2    # "stream":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v4

    move-object v1, v2

    .end local v2    # "stream":Ljava/io/FileInputStream;
    .restart local v1    # "stream":Ljava/io/FileInputStream;
    goto :goto_0
.end method

.method public static getDecodingReader(Ljava/io/InputStream;Ljava/nio/charset/Charset;)Ljava/io/Reader;
    .locals 3
    .param p0, "stream"    # Ljava/io/InputStream;
    .param p1, "charSet"    # Ljava/nio/charset/Charset;

    .prologue
    .line 265
    invoke-virtual {p1}, Ljava/nio/charset/Charset;->newDecoder()Ljava/nio/charset/CharsetDecoder;

    move-result-object v1

    .line 266
    sget-object v2, Ljava/nio/charset/CodingErrorAction;->REPORT:Ljava/nio/charset/CodingErrorAction;

    invoke-virtual {v1, v2}, Ljava/nio/charset/CharsetDecoder;->onMalformedInput(Ljava/nio/charset/CodingErrorAction;)Ljava/nio/charset/CharsetDecoder;

    move-result-object v1

    .line 267
    sget-object v2, Ljava/nio/charset/CodingErrorAction;->REPORT:Ljava/nio/charset/CodingErrorAction;

    invoke-virtual {v1, v2}, Ljava/nio/charset/CharsetDecoder;->onUnmappableCharacter(Ljava/nio/charset/CodingErrorAction;)Ljava/nio/charset/CharsetDecoder;

    move-result-object v0

    .line 268
    .local v0, "charSetDecoder":Ljava/nio/charset/CharsetDecoder;
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v2, Ljava/io/InputStreamReader;

    invoke-direct {v2, p0, v0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/nio/charset/CharsetDecoder;)V

    invoke-direct {v1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    return-object v1
.end method

.method public static getDecodingReader(Ljava/lang/Class;Ljava/lang/String;Ljava/nio/charset/Charset;)Ljava/io/Reader;
    .locals 6
    .param p1, "resource"    # Ljava/lang/String;
    .param p2, "charSet"    # Ljava/nio/charset/Charset;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            "Ljava/nio/charset/Charset;",
            ")",
            "Ljava/io/Reader;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 314
    const/4 v1, 0x0

    .line 315
    .local v1, "stream":Ljava/io/InputStream;
    const/4 v2, 0x0

    .line 318
    .local v2, "success":Z
    :try_start_0
    invoke-virtual {p0, p1}, Ljava/lang/Class;->getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    .line 319
    invoke-static {v1, p2}, Lorg/apache/lucene/util/IOUtils;->getDecodingReader(Ljava/io/InputStream;Ljava/nio/charset/Charset;)Ljava/io/Reader;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 320
    .local v0, "reader":Ljava/io/Reader;
    const/4 v2, 0x1

    .line 323
    if-nez v2, :cond_0

    new-array v3, v4, [Ljava/io/Closeable;

    .line 324
    aput-object v1, v3, v5

    invoke-static {v3}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 321
    :cond_0
    return-object v0

    .line 322
    .end local v0    # "reader":Ljava/io/Reader;
    :catchall_0
    move-exception v3

    .line 323
    if-nez v2, :cond_1

    new-array v4, v4, [Ljava/io/Closeable;

    .line 324
    aput-object v1, v4, v5

    invoke-static {v4}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 326
    :cond_1
    throw v3
.end method
