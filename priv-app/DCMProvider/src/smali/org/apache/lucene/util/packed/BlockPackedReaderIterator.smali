.class public final Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;
.super Ljava/lang/Object;
.source "BlockPackedReaderIterator.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final blockSize:I

.field blocks:[B

.field in:Lorg/apache/lucene/store/DataInput;

.field off:I

.field ord:J

.field final packedIntsVersion:I

.field valueCount:J

.field final values:[J

.field final valuesRef:Lorg/apache/lucene/util/LongsRef;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/store/DataInput;IIJ)V
    .locals 4
    .param p1, "in"    # Lorg/apache/lucene/store/DataInput;
    .param p2, "packedIntsVersion"    # I
    .param p3, "blockSize"    # I
    .param p4, "valueCount"    # J

    .prologue
    const/4 v2, 0x0

    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    invoke-static {p3}, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->checkBlockSize(I)V

    .line 91
    iput p2, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->packedIntsVersion:I

    .line 92
    iput p3, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->blockSize:I

    .line 93
    new-array v0, p3, [J

    iput-object v0, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->values:[J

    .line 94
    new-instance v0, Lorg/apache/lucene/util/LongsRef;

    iget-object v1, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->values:[J

    invoke-direct {v0, v1, v2, v2}, Lorg/apache/lucene/util/LongsRef;-><init>([JII)V

    iput-object v0, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->valuesRef:Lorg/apache/lucene/util/LongsRef;

    .line 95
    invoke-virtual {p0, p1, p4, p5}, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->reset(Lorg/apache/lucene/store/DataInput;J)V

    .line 96
    return-void
.end method

.method static readVLong(Lorg/apache/lucene/store/DataInput;)J
    .locals 8
    .param p0, "in"    # Lorg/apache/lucene/store/DataInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v6, 0x7f

    .line 45
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v0

    .line 46
    .local v0, "b":B
    if-ltz v0, :cond_1

    int-to-long v2, v0

    .line 71
    :cond_0
    :goto_0
    return-wide v2

    .line 47
    :cond_1
    int-to-long v4, v0

    and-long v2, v4, v6

    .line 48
    .local v2, "i":J
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v0

    .line 49
    int-to-long v4, v0

    and-long/2addr v4, v6

    const/4 v1, 0x7

    shl-long/2addr v4, v1

    or-long/2addr v2, v4

    .line 50
    if-gez v0, :cond_0

    .line 51
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v0

    .line 52
    int-to-long v4, v0

    and-long/2addr v4, v6

    const/16 v1, 0xe

    shl-long/2addr v4, v1

    or-long/2addr v2, v4

    .line 53
    if-gez v0, :cond_0

    .line 54
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v0

    .line 55
    int-to-long v4, v0

    and-long/2addr v4, v6

    const/16 v1, 0x15

    shl-long/2addr v4, v1

    or-long/2addr v2, v4

    .line 56
    if-gez v0, :cond_0

    .line 57
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v0

    .line 58
    int-to-long v4, v0

    and-long/2addr v4, v6

    const/16 v1, 0x1c

    shl-long/2addr v4, v1

    or-long/2addr v2, v4

    .line 59
    if-gez v0, :cond_0

    .line 60
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v0

    .line 61
    int-to-long v4, v0

    and-long/2addr v4, v6

    const/16 v1, 0x23

    shl-long/2addr v4, v1

    or-long/2addr v2, v4

    .line 62
    if-gez v0, :cond_0

    .line 63
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v0

    .line 64
    int-to-long v4, v0

    and-long/2addr v4, v6

    const/16 v1, 0x2a

    shl-long/2addr v4, v1

    or-long/2addr v2, v4

    .line 65
    if-gez v0, :cond_0

    .line 66
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v0

    .line 67
    int-to-long v4, v0

    and-long/2addr v4, v6

    const/16 v1, 0x31

    shl-long/2addr v4, v1

    or-long/2addr v2, v4

    .line 68
    if-gez v0, :cond_0

    .line 69
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v0

    .line 70
    int-to-long v4, v0

    const-wide/16 v6, 0xff

    and-long/2addr v4, v6

    const/16 v1, 0x38

    shl-long/2addr v4, v1

    or-long/2addr v2, v4

    .line 71
    goto :goto_0
.end method

.method private refill()V
    .locals 20
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 202
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->in:Lorg/apache/lucene/store/DataInput;

    invoke-virtual {v3}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v3

    and-int/lit16 v13, v3, 0xff

    .line 203
    .local v13, "token":I
    and-int/lit8 v3, v13, 0x1

    if-eqz v3, :cond_0

    const/4 v12, 0x1

    .line 204
    .local v12, "minEquals0":Z
    :goto_0
    ushr-int/lit8 v8, v13, 0x1

    .line 205
    .local v8, "bitsPerValue":I
    const/16 v3, 0x40

    if-le v8, v3, :cond_1

    .line 206
    new-instance v3, Ljava/io/IOException;

    const-string v4, "Corrupted"

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 203
    .end local v8    # "bitsPerValue":I
    .end local v12    # "minEquals0":Z
    :cond_0
    const/4 v12, 0x0

    goto :goto_0

    .line 208
    .restart local v8    # "bitsPerValue":I
    .restart local v12    # "minEquals0":Z
    :cond_1
    if-eqz v12, :cond_2

    const-wide/16 v14, 0x0

    .line 209
    .local v14, "minValue":J
    :goto_1
    sget-boolean v3, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->$assertionsDisabled:Z

    if-nez v3, :cond_3

    if-nez v12, :cond_3

    const-wide/16 v4, 0x0

    cmp-long v3, v14, v4

    if-nez v3, :cond_3

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 208
    .end local v14    # "minValue":J
    :cond_2
    const-wide/16 v4, 0x1

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->in:Lorg/apache/lucene/store/DataInput;

    invoke-static {v3}, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->readVLong(Lorg/apache/lucene/store/DataInput;)J

    move-result-wide v18

    add-long v4, v4, v18

    invoke-static {v4, v5}, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->zigZagDecode(J)J

    move-result-wide v14

    goto :goto_1

    .line 211
    .restart local v14    # "minValue":J
    :cond_3
    if-nez v8, :cond_5

    .line 212
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->values:[J

    invoke-static {v3, v14, v15}, Ljava/util/Arrays;->fill([JJ)V

    .line 233
    :cond_4
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->off:I

    .line 234
    return-void

    .line 214
    :cond_5
    sget-object v3, Lorg/apache/lucene/util/packed/PackedInts$Format;->PACKED:Lorg/apache/lucene/util/packed/PackedInts$Format;

    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->packedIntsVersion:I

    invoke-static {v3, v4, v8}, Lorg/apache/lucene/util/packed/PackedInts;->getDecoder(Lorg/apache/lucene/util/packed/PackedInts$Format;II)Lorg/apache/lucene/util/packed/PackedInts$Decoder;

    move-result-object v2

    .line 215
    .local v2, "decoder":Lorg/apache/lucene/util/packed/PackedInts$Decoder;
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->blockSize:I

    invoke-interface {v2}, Lorg/apache/lucene/util/packed/PackedInts$Decoder;->byteValueCount()I

    move-result v4

    div-int v7, v3, v4

    .line 216
    .local v7, "iterations":I
    invoke-interface {v2}, Lorg/apache/lucene/util/packed/PackedInts$Decoder;->byteBlockCount()I

    move-result v3

    mul-int v10, v7, v3

    .line 217
    .local v10, "blocksSize":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->blocks:[B

    if-eqz v3, :cond_6

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->blocks:[B

    array-length v3, v3

    if-ge v3, v10, :cond_7

    .line 218
    :cond_6
    new-array v3, v10, [B

    move-object/from16 v0, p0

    iput-object v3, v0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->blocks:[B

    .line 221
    :cond_7
    move-object/from16 v0, p0

    iget-wide v4, v0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->valueCount:J

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->ord:J

    move-wide/from16 v18, v0

    sub-long v4, v4, v18

    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->blockSize:I

    int-to-long v0, v3

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    long-to-int v0, v4

    move/from16 v16, v0

    .line 222
    .local v16, "valueCount":I
    sget-object v3, Lorg/apache/lucene/util/packed/PackedInts$Format;->PACKED:Lorg/apache/lucene/util/packed/PackedInts$Format;

    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->packedIntsVersion:I

    move/from16 v0, v16

    invoke-virtual {v3, v4, v0, v8}, Lorg/apache/lucene/util/packed/PackedInts$Format;->byteCount(III)J

    move-result-wide v4

    long-to-int v9, v4

    .line 223
    .local v9, "blocksCount":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->in:Lorg/apache/lucene/store/DataInput;

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->blocks:[B

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5, v9}, Lorg/apache/lucene/store/DataInput;->readBytes([BII)V

    .line 225
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->blocks:[B

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->values:[J

    const/4 v6, 0x0

    invoke-interface/range {v2 .. v7}, Lorg/apache/lucene/util/packed/PackedInts$Decoder;->decode([BI[JII)V

    .line 227
    const-wide/16 v4, 0x0

    cmp-long v3, v14, v4

    if-eqz v3, :cond_4

    .line 228
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_2
    move/from16 v0, v16

    if-ge v11, v0, :cond_4

    .line 229
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->values:[J

    aget-wide v4, v3, v11

    add-long/2addr v4, v14

    aput-wide v4, v3, v11

    .line 228
    add-int/lit8 v11, v11, 0x1

    goto :goto_2
.end method

.method private skipBytes(J)V
    .locals 9
    .param p1, "count"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 152
    iget-object v4, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->in:Lorg/apache/lucene/store/DataInput;

    instance-of v4, v4, Lorg/apache/lucene/store/IndexInput;

    if-eqz v4, :cond_1

    .line 153
    iget-object v0, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->in:Lorg/apache/lucene/store/DataInput;

    check-cast v0, Lorg/apache/lucene/store/IndexInput;

    .line 154
    .local v0, "iin":Lorg/apache/lucene/store/IndexInput;
    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v4

    add-long/2addr v4, p1

    invoke-virtual {v0, v4, v5}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 166
    .end local v0    # "iin":Lorg/apache/lucene/store/IndexInput;
    :cond_0
    return-void

    .line 156
    :cond_1
    iget-object v4, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->blocks:[B

    if-nez v4, :cond_2

    .line 157
    iget v4, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->blockSize:I

    new-array v4, v4, [B

    iput-object v4, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->blocks:[B

    .line 159
    :cond_2
    const-wide/16 v2, 0x0

    .line 160
    .local v2, "skipped":J
    :goto_0
    cmp-long v4, v2, p1

    if-gez v4, :cond_0

    .line 161
    iget-object v4, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->blocks:[B

    array-length v4, v4

    int-to-long v4, v4

    sub-long v6, p1, v2

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    long-to-int v1, v4

    .line 162
    .local v1, "toSkip":I
    iget-object v4, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->in:Lorg/apache/lucene/store/DataInput;

    iget-object v5, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->blocks:[B

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6, v1}, Lorg/apache/lucene/store/DataInput;->readBytes([BII)V

    .line 163
    int-to-long v4, v1

    add-long/2addr v2, v4

    goto :goto_0
.end method

.method static zigZagDecode(J)J
    .locals 4
    .param p0, "n"    # J

    .prologue
    .line 40
    const/4 v0, 0x1

    ushr-long v0, p0, v0

    const-wide/16 v2, 0x1

    and-long/2addr v2, p0

    neg-long v2, v2

    xor-long/2addr v0, v2

    return-wide v0
.end method


# virtual methods
.method public next()J
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 170
    iget-wide v2, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->ord:J

    iget-wide v4, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->valueCount:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 171
    new-instance v2, Ljava/io/EOFException;

    invoke-direct {v2}, Ljava/io/EOFException;-><init>()V

    throw v2

    .line 173
    :cond_0
    iget v2, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->off:I

    iget v3, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->blockSize:I

    if-ne v2, v3, :cond_1

    .line 174
    invoke-direct {p0}, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->refill()V

    .line 176
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->values:[J

    iget v3, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->off:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->off:I

    aget-wide v0, v2, v3

    .line 177
    .local v0, "value":J
    iget-wide v2, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->ord:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->ord:J

    .line 178
    return-wide v0
.end method

.method public next(I)Lorg/apache/lucene/util/LongsRef;
    .locals 6
    .param p1, "count"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 183
    sget-boolean v0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-gtz p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 184
    :cond_0
    iget-wide v0, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->ord:J

    iget-wide v2, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->valueCount:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 185
    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 187
    :cond_1
    iget v0, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->off:I

    iget v1, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->blockSize:I

    if-ne v0, v1, :cond_2

    .line 188
    invoke-direct {p0}, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->refill()V

    .line 191
    :cond_2
    iget v0, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->blockSize:I

    iget v1, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->off:I

    sub-int/2addr v0, v1

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result p1

    .line 192
    int-to-long v0, p1

    iget-wide v2, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->valueCount:J

    iget-wide v4, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->ord:J

    sub-long/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    long-to-int p1, v0

    .line 194
    iget-object v0, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->valuesRef:Lorg/apache/lucene/util/LongsRef;

    iget v1, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->off:I

    iput v1, v0, Lorg/apache/lucene/util/LongsRef;->offset:I

    .line 195
    iget-object v0, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->valuesRef:Lorg/apache/lucene/util/LongsRef;

    iput p1, v0, Lorg/apache/lucene/util/LongsRef;->length:I

    .line 196
    iget v0, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->off:I

    add-int/2addr v0, p1

    iput v0, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->off:I

    .line 197
    iget-wide v0, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->ord:J

    int-to-long v2, p1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->ord:J

    .line 198
    iget-object v0, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->valuesRef:Lorg/apache/lucene/util/LongsRef;

    return-object v0
.end method

.method public ord()J
    .locals 2

    .prologue
    .line 238
    iget-wide v0, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->ord:J

    return-wide v0
.end method

.method public reset(Lorg/apache/lucene/store/DataInput;J)V
    .locals 4
    .param p1, "in"    # Lorg/apache/lucene/store/DataInput;
    .param p2, "valueCount"    # J

    .prologue
    const-wide/16 v2, 0x0

    .line 101
    iput-object p1, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->in:Lorg/apache/lucene/store/DataInput;

    .line 102
    sget-boolean v0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    cmp-long v0, p2, v2

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 103
    :cond_0
    iput-wide p2, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->valueCount:J

    .line 104
    iget v0, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->blockSize:I

    iput v0, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->off:I

    .line 105
    iput-wide v2, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->ord:J

    .line 106
    return-void
.end method

.method public skip(J)V
    .locals 13
    .param p1, "count"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v10, 0x0

    .line 110
    sget-boolean v5, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->$assertionsDisabled:Z

    if-nez v5, :cond_0

    cmp-long v5, p1, v10

    if-gez v5, :cond_0

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 111
    :cond_0
    iget-wide v6, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->ord:J

    add-long/2addr v6, p1

    iget-wide v8, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->valueCount:J

    cmp-long v5, v6, v8

    if-gtz v5, :cond_1

    iget-wide v6, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->ord:J

    add-long/2addr v6, p1

    cmp-long v5, v6, v10

    if-gez v5, :cond_2

    .line 112
    :cond_1
    new-instance v5, Ljava/io/EOFException;

    invoke-direct {v5}, Ljava/io/EOFException;-><init>()V

    throw v5

    .line 116
    :cond_2
    iget v5, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->blockSize:I

    iget v6, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->off:I

    sub-int/2addr v5, v6

    int-to-long v6, v5

    invoke-static {p1, p2, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v6

    long-to-int v1, v6

    .line 117
    .local v1, "skipBuffer":I
    iget v5, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->off:I

    add-int/2addr v5, v1

    iput v5, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->off:I

    .line 118
    iget-wide v6, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->ord:J

    int-to-long v8, v1

    add-long/2addr v6, v8

    iput-wide v6, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->ord:J

    .line 119
    int-to-long v6, v1

    sub-long/2addr p1, v6

    .line 120
    cmp-long v5, p1, v10

    if-nez v5, :cond_4

    .line 149
    :cond_3
    :goto_0
    return-void

    .line 125
    :cond_4
    sget-boolean v5, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->$assertionsDisabled:Z

    if-nez v5, :cond_8

    iget v5, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->off:I

    iget v6, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->blockSize:I

    if-eq v5, v6, :cond_8

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 127
    :cond_5
    iget-object v5, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->in:Lorg/apache/lucene/store/DataInput;

    invoke-virtual {v5}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v5

    and-int/lit16 v4, v5, 0xff

    .line 128
    .local v4, "token":I
    ushr-int/lit8 v0, v4, 0x1

    .line 129
    .local v0, "bitsPerValue":I
    const/16 v5, 0x40

    if-le v0, v5, :cond_6

    .line 130
    new-instance v5, Ljava/io/IOException;

    const-string v6, "Corrupted"

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 132
    :cond_6
    and-int/lit8 v5, v4, 0x1

    if-nez v5, :cond_7

    .line 133
    iget-object v5, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->in:Lorg/apache/lucene/store/DataInput;

    invoke-static {v5}, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->readVLong(Lorg/apache/lucene/store/DataInput;)J

    .line 135
    :cond_7
    sget-object v5, Lorg/apache/lucene/util/packed/PackedInts$Format;->PACKED:Lorg/apache/lucene/util/packed/PackedInts$Format;

    iget v6, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->packedIntsVersion:I

    iget v7, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->blockSize:I

    invoke-virtual {v5, v6, v7, v0}, Lorg/apache/lucene/util/packed/PackedInts$Format;->byteCount(III)J

    move-result-wide v2

    .line 136
    .local v2, "blockBytes":J
    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->skipBytes(J)V

    .line 137
    iget-wide v6, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->ord:J

    iget v5, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->blockSize:I

    int-to-long v8, v5

    add-long/2addr v6, v8

    iput-wide v6, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->ord:J

    .line 138
    iget v5, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->blockSize:I

    int-to-long v6, v5

    sub-long/2addr p1, v6

    .line 126
    .end local v0    # "bitsPerValue":I
    .end local v2    # "blockBytes":J
    .end local v4    # "token":I
    :cond_8
    iget v5, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->blockSize:I

    int-to-long v6, v5

    cmp-long v5, p1, v6

    if-gez v5, :cond_5

    .line 140
    cmp-long v5, p1, v10

    if-eqz v5, :cond_3

    .line 145
    sget-boolean v5, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->$assertionsDisabled:Z

    if-nez v5, :cond_9

    iget v5, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->blockSize:I

    int-to-long v6, v5

    cmp-long v5, p1, v6

    if-ltz v5, :cond_9

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 146
    :cond_9
    invoke-direct {p0}, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->refill()V

    .line 147
    iget-wide v6, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->ord:J

    add-long/2addr v6, p1

    iput-wide v6, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->ord:J

    .line 148
    iget v5, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->off:I

    int-to-long v6, v5

    add-long/2addr v6, p1

    long-to-int v5, v6

    iput v5, p0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->off:I

    goto :goto_0
.end method
