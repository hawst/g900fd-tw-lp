.class final Lorg/apache/lucene/util/packed/BulkOperationPacked22;
.super Lorg/apache/lucene/util/packed/BulkOperationPacked;
.source "BulkOperationPacked22.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    const/16 v0, 0x16

    invoke-direct {p0, v0}, Lorg/apache/lucene/util/packed/BulkOperationPacked;-><init>(I)V

    .line 29
    return-void
.end method


# virtual methods
.method public decode([BI[III)V
    .locals 17
    .param p1, "blocks"    # [B
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [I
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 82
    const/4 v13, 0x0

    .local v13, "i":I
    move/from16 v14, p4

    .end local p4    # "valuesOffset":I
    .local v14, "valuesOffset":I
    move/from16 v1, p2

    .end local p2    # "blocksOffset":I
    .local v1, "blocksOffset":I
    :goto_0
    move/from16 v0, p5

    if-lt v13, v0, :cond_0

    .line 99
    return-void

    .line 83
    :cond_0
    add-int/lit8 p2, v1, 0x1

    .end local v1    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v15, p1, v1

    and-int/lit16 v2, v15, 0xff

    .line 84
    .local v2, "byte0":I
    add-int/lit8 v1, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v1    # "blocksOffset":I
    aget-byte v15, p1, p2

    and-int/lit16 v3, v15, 0xff

    .line 85
    .local v3, "byte1":I
    add-int/lit8 p2, v1, 0x1

    .end local v1    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v15, p1, v1

    and-int/lit16 v5, v15, 0xff

    .line 86
    .local v5, "byte2":I
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    shl-int/lit8 v15, v2, 0xe

    shl-int/lit8 v16, v3, 0x6

    or-int v15, v15, v16

    ushr-int/lit8 v16, v5, 0x2

    or-int v15, v15, v16

    aput v15, p3, v14

    .line 87
    add-int/lit8 v1, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v1    # "blocksOffset":I
    aget-byte v15, p1, p2

    and-int/lit16 v6, v15, 0xff

    .line 88
    .local v6, "byte3":I
    add-int/lit8 p2, v1, 0x1

    .end local v1    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v15, p1, v1

    and-int/lit16 v7, v15, 0xff

    .line 89
    .local v7, "byte4":I
    add-int/lit8 v1, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v1    # "blocksOffset":I
    aget-byte v15, p1, p2

    and-int/lit16 v8, v15, 0xff

    .line 90
    .local v8, "byte5":I
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    and-int/lit8 v15, v5, 0x3

    shl-int/lit8 v15, v15, 0x14

    shl-int/lit8 v16, v6, 0xc

    or-int v15, v15, v16

    shl-int/lit8 v16, v7, 0x4

    or-int v15, v15, v16

    ushr-int/lit8 v16, v8, 0x4

    or-int v15, v15, v16

    aput v15, p3, p4

    .line 91
    add-int/lit8 p2, v1, 0x1

    .end local v1    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v15, p1, v1

    and-int/lit16 v9, v15, 0xff

    .line 92
    .local v9, "byte6":I
    add-int/lit8 v1, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v1    # "blocksOffset":I
    aget-byte v15, p1, p2

    and-int/lit16 v10, v15, 0xff

    .line 93
    .local v10, "byte7":I
    add-int/lit8 p2, v1, 0x1

    .end local v1    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v15, p1, v1

    and-int/lit16 v11, v15, 0xff

    .line 94
    .local v11, "byte8":I
    add-int/lit8 p4, v14, 0x1

    .end local v14    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    and-int/lit8 v15, v8, 0xf

    shl-int/lit8 v15, v15, 0x12

    shl-int/lit8 v16, v9, 0xa

    or-int v15, v15, v16

    shl-int/lit8 v16, v10, 0x2

    or-int v15, v15, v16

    ushr-int/lit8 v16, v11, 0x6

    or-int v15, v15, v16

    aput v15, p3, v14

    .line 95
    add-int/lit8 v1, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v1    # "blocksOffset":I
    aget-byte v15, p1, p2

    and-int/lit16 v12, v15, 0xff

    .line 96
    .local v12, "byte9":I
    add-int/lit8 p2, v1, 0x1

    .end local v1    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v15, p1, v1

    and-int/lit16 v4, v15, 0xff

    .line 97
    .local v4, "byte10":I
    add-int/lit8 v14, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v14    # "valuesOffset":I
    and-int/lit8 v15, v11, 0x3f

    shl-int/lit8 v15, v15, 0x10

    shl-int/lit8 v16, v12, 0x8

    or-int v15, v15, v16

    or-int/2addr v15, v4

    aput v15, p3, p4

    .line 82
    add-int/lit8 v13, v13, 0x1

    move/from16 v1, p2

    .end local p2    # "blocksOffset":I
    .restart local v1    # "blocksOffset":I
    goto/16 :goto_0
.end method

.method public decode([BI[JII)V
    .locals 32
    .param p1, "blocks"    # [B
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [J
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 152
    const/4 v3, 0x0

    .local v3, "i":I
    move/from16 v26, p4

    .end local p4    # "valuesOffset":I
    .local v26, "valuesOffset":I
    move/from16 v2, p2

    .end local p2    # "blocksOffset":I
    .local v2, "blocksOffset":I
    :goto_0
    move/from16 v0, p5

    if-lt v3, v0, :cond_0

    .line 169
    return-void

    .line 153
    :cond_0
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v27, p1, v2

    move/from16 v0, v27

    and-int/lit16 v0, v0, 0xff

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-long v4, v0

    .line 154
    .local v4, "byte0":J
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v27, p1, p2

    move/from16 v0, v27

    and-int/lit16 v0, v0, 0xff

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-long v6, v0

    .line 155
    .local v6, "byte1":J
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v27, p1, v2

    move/from16 v0, v27

    and-int/lit16 v0, v0, 0xff

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-long v10, v0

    .line 156
    .local v10, "byte2":J
    add-int/lit8 p4, v26, 0x1

    .end local v26    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v27, 0xe

    shl-long v28, v4, v27

    const/16 v27, 0x6

    shl-long v30, v6, v27

    or-long v28, v28, v30

    const/16 v27, 0x2

    ushr-long v30, v10, v27

    or-long v28, v28, v30

    aput-wide v28, p3, v26

    .line 157
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v27, p1, p2

    move/from16 v0, v27

    and-int/lit16 v0, v0, 0xff

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-long v12, v0

    .line 158
    .local v12, "byte3":J
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v27, p1, v2

    move/from16 v0, v27

    and-int/lit16 v0, v0, 0xff

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-long v14, v0

    .line 159
    .local v14, "byte4":J
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v27, p1, p2

    move/from16 v0, v27

    and-int/lit16 v0, v0, 0xff

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-long v0, v0

    move-wide/from16 v16, v0

    .line 160
    .local v16, "byte5":J
    add-int/lit8 v26, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v26    # "valuesOffset":I
    const-wide/16 v28, 0x3

    and-long v28, v28, v10

    const/16 v27, 0x14

    shl-long v28, v28, v27

    const/16 v27, 0xc

    shl-long v30, v12, v27

    or-long v28, v28, v30

    const/16 v27, 0x4

    shl-long v30, v14, v27

    or-long v28, v28, v30

    const/16 v27, 0x4

    ushr-long v30, v16, v27

    or-long v28, v28, v30

    aput-wide v28, p3, p4

    .line 161
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v27, p1, v2

    move/from16 v0, v27

    and-int/lit16 v0, v0, 0xff

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-long v0, v0

    move-wide/from16 v18, v0

    .line 162
    .local v18, "byte6":J
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v27, p1, p2

    move/from16 v0, v27

    and-int/lit16 v0, v0, 0xff

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-long v0, v0

    move-wide/from16 v20, v0

    .line 163
    .local v20, "byte7":J
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v27, p1, v2

    move/from16 v0, v27

    and-int/lit16 v0, v0, 0xff

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-long v0, v0

    move-wide/from16 v22, v0

    .line 164
    .local v22, "byte8":J
    add-int/lit8 p4, v26, 0x1

    .end local v26    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v28, 0xf

    and-long v28, v28, v16

    const/16 v27, 0x12

    shl-long v28, v28, v27

    const/16 v27, 0xa

    shl-long v30, v18, v27

    or-long v28, v28, v30

    const/16 v27, 0x2

    shl-long v30, v20, v27

    or-long v28, v28, v30

    const/16 v27, 0x6

    ushr-long v30, v22, v27

    or-long v28, v28, v30

    aput-wide v28, p3, v26

    .line 165
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v27, p1, p2

    move/from16 v0, v27

    and-int/lit16 v0, v0, 0xff

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-long v0, v0

    move-wide/from16 v24, v0

    .line 166
    .local v24, "byte9":J
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v27, p1, v2

    move/from16 v0, v27

    and-int/lit16 v0, v0, 0xff

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-long v8, v0

    .line 167
    .local v8, "byte10":J
    add-int/lit8 v26, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v26    # "valuesOffset":I
    const-wide/16 v28, 0x3f

    and-long v28, v28, v22

    const/16 v27, 0x10

    shl-long v28, v28, v27

    const/16 v27, 0x8

    shl-long v30, v24, v27

    or-long v28, v28, v30

    or-long v28, v28, v8

    aput-wide v28, p3, p4

    .line 152
    add-int/lit8 v3, v3, 0x1

    move/from16 v2, p2

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    goto/16 :goto_0
.end method

.method public decode([JI[III)V
    .locals 32
    .param p1, "blocks"    # [J
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [I
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 33
    const/16 v25, 0x0

    .local v25, "i":I
    move/from16 v26, p4

    .end local p4    # "valuesOffset":I
    .local v26, "valuesOffset":I
    move/from16 v24, p2

    .end local p2    # "blocksOffset":I
    .local v24, "blocksOffset":I
    :goto_0
    move/from16 v0, v25

    move/from16 v1, p5

    if-lt v0, v1, :cond_0

    .line 78
    return-void

    .line 34
    :cond_0
    add-int/lit8 p2, v24, 0x1

    .end local v24    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v2, p1, v24

    .line 35
    .local v2, "block0":J
    add-int/lit8 p4, v26, 0x1

    .end local v26    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v27, 0x2a

    ushr-long v28, v2, v27

    move-wide/from16 v0, v28

    long-to-int v0, v0

    move/from16 v27, v0

    aput v27, p3, v26

    .line 36
    add-int/lit8 v26, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v26    # "valuesOffset":I
    const/16 v27, 0x14

    ushr-long v28, v2, v27

    const-wide/32 v30, 0x3fffff

    and-long v28, v28, v30

    move-wide/from16 v0, v28

    long-to-int v0, v0

    move/from16 v27, v0

    aput v27, p3, p4

    .line 37
    add-int/lit8 v24, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v24    # "blocksOffset":I
    aget-wide v4, p1, p2

    .line 38
    .local v4, "block1":J
    add-int/lit8 p4, v26, 0x1

    .end local v26    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/32 v28, 0xfffff

    and-long v28, v28, v2

    const/16 v27, 0x2

    shl-long v28, v28, v27

    const/16 v27, 0x3e

    ushr-long v30, v4, v27

    or-long v28, v28, v30

    move-wide/from16 v0, v28

    long-to-int v0, v0

    move/from16 v27, v0

    aput v27, p3, v26

    .line 39
    add-int/lit8 v26, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v26    # "valuesOffset":I
    const/16 v27, 0x28

    ushr-long v28, v4, v27

    const-wide/32 v30, 0x3fffff

    and-long v28, v28, v30

    move-wide/from16 v0, v28

    long-to-int v0, v0

    move/from16 v27, v0

    aput v27, p3, p4

    .line 40
    add-int/lit8 p4, v26, 0x1

    .end local v26    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v27, 0x12

    ushr-long v28, v4, v27

    const-wide/32 v30, 0x3fffff

    and-long v28, v28, v30

    move-wide/from16 v0, v28

    long-to-int v0, v0

    move/from16 v27, v0

    aput v27, p3, v26

    .line 41
    add-int/lit8 p2, v24, 0x1

    .end local v24    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v8, p1, v24

    .line 42
    .local v8, "block2":J
    add-int/lit8 v26, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v26    # "valuesOffset":I
    const-wide/32 v28, 0x3ffff

    and-long v28, v28, v4

    const/16 v27, 0x4

    shl-long v28, v28, v27

    const/16 v27, 0x3c

    ushr-long v30, v8, v27

    or-long v28, v28, v30

    move-wide/from16 v0, v28

    long-to-int v0, v0

    move/from16 v27, v0

    aput v27, p3, p4

    .line 43
    add-int/lit8 p4, v26, 0x1

    .end local v26    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v27, 0x26

    ushr-long v28, v8, v27

    const-wide/32 v30, 0x3fffff

    and-long v28, v28, v30

    move-wide/from16 v0, v28

    long-to-int v0, v0

    move/from16 v27, v0

    aput v27, p3, v26

    .line 44
    add-int/lit8 v26, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v26    # "valuesOffset":I
    const/16 v27, 0x10

    ushr-long v28, v8, v27

    const-wide/32 v30, 0x3fffff

    and-long v28, v28, v30

    move-wide/from16 v0, v28

    long-to-int v0, v0

    move/from16 v27, v0

    aput v27, p3, p4

    .line 45
    add-int/lit8 v24, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v24    # "blocksOffset":I
    aget-wide v10, p1, p2

    .line 46
    .local v10, "block3":J
    add-int/lit8 p4, v26, 0x1

    .end local v26    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/32 v28, 0xffff

    and-long v28, v28, v8

    const/16 v27, 0x6

    shl-long v28, v28, v27

    const/16 v27, 0x3a

    ushr-long v30, v10, v27

    or-long v28, v28, v30

    move-wide/from16 v0, v28

    long-to-int v0, v0

    move/from16 v27, v0

    aput v27, p3, v26

    .line 47
    add-int/lit8 v26, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v26    # "valuesOffset":I
    const/16 v27, 0x24

    ushr-long v28, v10, v27

    const-wide/32 v30, 0x3fffff

    and-long v28, v28, v30

    move-wide/from16 v0, v28

    long-to-int v0, v0

    move/from16 v27, v0

    aput v27, p3, p4

    .line 48
    add-int/lit8 p4, v26, 0x1

    .end local v26    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v27, 0xe

    ushr-long v28, v10, v27

    const-wide/32 v30, 0x3fffff

    and-long v28, v28, v30

    move-wide/from16 v0, v28

    long-to-int v0, v0

    move/from16 v27, v0

    aput v27, p3, v26

    .line 49
    add-int/lit8 p2, v24, 0x1

    .end local v24    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v12, p1, v24

    .line 50
    .local v12, "block4":J
    add-int/lit8 v26, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v26    # "valuesOffset":I
    const-wide/16 v28, 0x3fff

    and-long v28, v28, v10

    const/16 v27, 0x8

    shl-long v28, v28, v27

    const/16 v27, 0x38

    ushr-long v30, v12, v27

    or-long v28, v28, v30

    move-wide/from16 v0, v28

    long-to-int v0, v0

    move/from16 v27, v0

    aput v27, p3, p4

    .line 51
    add-int/lit8 p4, v26, 0x1

    .end local v26    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v27, 0x22

    ushr-long v28, v12, v27

    const-wide/32 v30, 0x3fffff

    and-long v28, v28, v30

    move-wide/from16 v0, v28

    long-to-int v0, v0

    move/from16 v27, v0

    aput v27, p3, v26

    .line 52
    add-int/lit8 v26, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v26    # "valuesOffset":I
    const/16 v27, 0xc

    ushr-long v28, v12, v27

    const-wide/32 v30, 0x3fffff

    and-long v28, v28, v30

    move-wide/from16 v0, v28

    long-to-int v0, v0

    move/from16 v27, v0

    aput v27, p3, p4

    .line 53
    add-int/lit8 v24, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v24    # "blocksOffset":I
    aget-wide v14, p1, p2

    .line 54
    .local v14, "block5":J
    add-int/lit8 p4, v26, 0x1

    .end local v26    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v28, 0xfff

    and-long v28, v28, v12

    const/16 v27, 0xa

    shl-long v28, v28, v27

    const/16 v27, 0x36

    ushr-long v30, v14, v27

    or-long v28, v28, v30

    move-wide/from16 v0, v28

    long-to-int v0, v0

    move/from16 v27, v0

    aput v27, p3, v26

    .line 55
    add-int/lit8 v26, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v26    # "valuesOffset":I
    const/16 v27, 0x20

    ushr-long v28, v14, v27

    const-wide/32 v30, 0x3fffff

    and-long v28, v28, v30

    move-wide/from16 v0, v28

    long-to-int v0, v0

    move/from16 v27, v0

    aput v27, p3, p4

    .line 56
    add-int/lit8 p4, v26, 0x1

    .end local v26    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v27, 0xa

    ushr-long v28, v14, v27

    const-wide/32 v30, 0x3fffff

    and-long v28, v28, v30

    move-wide/from16 v0, v28

    long-to-int v0, v0

    move/from16 v27, v0

    aput v27, p3, v26

    .line 57
    add-int/lit8 p2, v24, 0x1

    .end local v24    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v16, p1, v24

    .line 58
    .local v16, "block6":J
    add-int/lit8 v26, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v26    # "valuesOffset":I
    const-wide/16 v28, 0x3ff

    and-long v28, v28, v14

    const/16 v27, 0xc

    shl-long v28, v28, v27

    const/16 v27, 0x34

    ushr-long v30, v16, v27

    or-long v28, v28, v30

    move-wide/from16 v0, v28

    long-to-int v0, v0

    move/from16 v27, v0

    aput v27, p3, p4

    .line 59
    add-int/lit8 p4, v26, 0x1

    .end local v26    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v27, 0x1e

    ushr-long v28, v16, v27

    const-wide/32 v30, 0x3fffff

    and-long v28, v28, v30

    move-wide/from16 v0, v28

    long-to-int v0, v0

    move/from16 v27, v0

    aput v27, p3, v26

    .line 60
    add-int/lit8 v26, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v26    # "valuesOffset":I
    const/16 v27, 0x8

    ushr-long v28, v16, v27

    const-wide/32 v30, 0x3fffff

    and-long v28, v28, v30

    move-wide/from16 v0, v28

    long-to-int v0, v0

    move/from16 v27, v0

    aput v27, p3, p4

    .line 61
    add-int/lit8 v24, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v24    # "blocksOffset":I
    aget-wide v18, p1, p2

    .line 62
    .local v18, "block7":J
    add-int/lit8 p4, v26, 0x1

    .end local v26    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v28, 0xff

    and-long v28, v28, v16

    const/16 v27, 0xe

    shl-long v28, v28, v27

    const/16 v27, 0x32

    ushr-long v30, v18, v27

    or-long v28, v28, v30

    move-wide/from16 v0, v28

    long-to-int v0, v0

    move/from16 v27, v0

    aput v27, p3, v26

    .line 63
    add-int/lit8 v26, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v26    # "valuesOffset":I
    const/16 v27, 0x1c

    ushr-long v28, v18, v27

    const-wide/32 v30, 0x3fffff

    and-long v28, v28, v30

    move-wide/from16 v0, v28

    long-to-int v0, v0

    move/from16 v27, v0

    aput v27, p3, p4

    .line 64
    add-int/lit8 p4, v26, 0x1

    .end local v26    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v27, 0x6

    ushr-long v28, v18, v27

    const-wide/32 v30, 0x3fffff

    and-long v28, v28, v30

    move-wide/from16 v0, v28

    long-to-int v0, v0

    move/from16 v27, v0

    aput v27, p3, v26

    .line 65
    add-int/lit8 p2, v24, 0x1

    .end local v24    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v20, p1, v24

    .line 66
    .local v20, "block8":J
    add-int/lit8 v26, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v26    # "valuesOffset":I
    const-wide/16 v28, 0x3f

    and-long v28, v28, v18

    const/16 v27, 0x10

    shl-long v28, v28, v27

    const/16 v27, 0x30

    ushr-long v30, v20, v27

    or-long v28, v28, v30

    move-wide/from16 v0, v28

    long-to-int v0, v0

    move/from16 v27, v0

    aput v27, p3, p4

    .line 67
    add-int/lit8 p4, v26, 0x1

    .end local v26    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v27, 0x1a

    ushr-long v28, v20, v27

    const-wide/32 v30, 0x3fffff

    and-long v28, v28, v30

    move-wide/from16 v0, v28

    long-to-int v0, v0

    move/from16 v27, v0

    aput v27, p3, v26

    .line 68
    add-int/lit8 v26, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v26    # "valuesOffset":I
    const/16 v27, 0x4

    ushr-long v28, v20, v27

    const-wide/32 v30, 0x3fffff

    and-long v28, v28, v30

    move-wide/from16 v0, v28

    long-to-int v0, v0

    move/from16 v27, v0

    aput v27, p3, p4

    .line 69
    add-int/lit8 v24, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v24    # "blocksOffset":I
    aget-wide v22, p1, p2

    .line 70
    .local v22, "block9":J
    add-int/lit8 p4, v26, 0x1

    .end local v26    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v28, 0xf

    and-long v28, v28, v20

    const/16 v27, 0x12

    shl-long v28, v28, v27

    const/16 v27, 0x2e

    ushr-long v30, v22, v27

    or-long v28, v28, v30

    move-wide/from16 v0, v28

    long-to-int v0, v0

    move/from16 v27, v0

    aput v27, p3, v26

    .line 71
    add-int/lit8 v26, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v26    # "valuesOffset":I
    const/16 v27, 0x18

    ushr-long v28, v22, v27

    const-wide/32 v30, 0x3fffff

    and-long v28, v28, v30

    move-wide/from16 v0, v28

    long-to-int v0, v0

    move/from16 v27, v0

    aput v27, p3, p4

    .line 72
    add-int/lit8 p4, v26, 0x1

    .end local v26    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v27, 0x2

    ushr-long v28, v22, v27

    const-wide/32 v30, 0x3fffff

    and-long v28, v28, v30

    move-wide/from16 v0, v28

    long-to-int v0, v0

    move/from16 v27, v0

    aput v27, p3, v26

    .line 73
    add-int/lit8 p2, v24, 0x1

    .end local v24    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v6, p1, v24

    .line 74
    .local v6, "block10":J
    add-int/lit8 v26, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v26    # "valuesOffset":I
    const-wide/16 v28, 0x3

    and-long v28, v28, v22

    const/16 v27, 0x14

    shl-long v28, v28, v27

    const/16 v27, 0x2c

    ushr-long v30, v6, v27

    or-long v28, v28, v30

    move-wide/from16 v0, v28

    long-to-int v0, v0

    move/from16 v27, v0

    aput v27, p3, p4

    .line 75
    add-int/lit8 p4, v26, 0x1

    .end local v26    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v27, 0x16

    ushr-long v28, v6, v27

    const-wide/32 v30, 0x3fffff

    and-long v28, v28, v30

    move-wide/from16 v0, v28

    long-to-int v0, v0

    move/from16 v27, v0

    aput v27, p3, v26

    .line 76
    add-int/lit8 v26, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v26    # "valuesOffset":I
    const-wide/32 v28, 0x3fffff

    and-long v28, v28, v6

    move-wide/from16 v0, v28

    long-to-int v0, v0

    move/from16 v27, v0

    aput v27, p3, p4

    .line 33
    add-int/lit8 v25, v25, 0x1

    move/from16 v24, p2

    .end local p2    # "blocksOffset":I
    .restart local v24    # "blocksOffset":I
    goto/16 :goto_0
.end method

.method public decode([JI[JII)V
    .locals 32
    .param p1, "blocks"    # [J
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [J
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 103
    const/16 v25, 0x0

    .local v25, "i":I
    move/from16 v26, p4

    .end local p4    # "valuesOffset":I
    .local v26, "valuesOffset":I
    move/from16 v24, p2

    .end local p2    # "blocksOffset":I
    .local v24, "blocksOffset":I
    :goto_0
    move/from16 v0, v25

    move/from16 v1, p5

    if-lt v0, v1, :cond_0

    .line 148
    return-void

    .line 104
    :cond_0
    add-int/lit8 p2, v24, 0x1

    .end local v24    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v2, p1, v24

    .line 105
    .local v2, "block0":J
    add-int/lit8 p4, v26, 0x1

    .end local v26    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v27, 0x2a

    ushr-long v28, v2, v27

    aput-wide v28, p3, v26

    .line 106
    add-int/lit8 v26, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v26    # "valuesOffset":I
    const/16 v27, 0x14

    ushr-long v28, v2, v27

    const-wide/32 v30, 0x3fffff

    and-long v28, v28, v30

    aput-wide v28, p3, p4

    .line 107
    add-int/lit8 v24, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v24    # "blocksOffset":I
    aget-wide v4, p1, p2

    .line 108
    .local v4, "block1":J
    add-int/lit8 p4, v26, 0x1

    .end local v26    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/32 v28, 0xfffff

    and-long v28, v28, v2

    const/16 v27, 0x2

    shl-long v28, v28, v27

    const/16 v27, 0x3e

    ushr-long v30, v4, v27

    or-long v28, v28, v30

    aput-wide v28, p3, v26

    .line 109
    add-int/lit8 v26, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v26    # "valuesOffset":I
    const/16 v27, 0x28

    ushr-long v28, v4, v27

    const-wide/32 v30, 0x3fffff

    and-long v28, v28, v30

    aput-wide v28, p3, p4

    .line 110
    add-int/lit8 p4, v26, 0x1

    .end local v26    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v27, 0x12

    ushr-long v28, v4, v27

    const-wide/32 v30, 0x3fffff

    and-long v28, v28, v30

    aput-wide v28, p3, v26

    .line 111
    add-int/lit8 p2, v24, 0x1

    .end local v24    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v8, p1, v24

    .line 112
    .local v8, "block2":J
    add-int/lit8 v26, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v26    # "valuesOffset":I
    const-wide/32 v28, 0x3ffff

    and-long v28, v28, v4

    const/16 v27, 0x4

    shl-long v28, v28, v27

    const/16 v27, 0x3c

    ushr-long v30, v8, v27

    or-long v28, v28, v30

    aput-wide v28, p3, p4

    .line 113
    add-int/lit8 p4, v26, 0x1

    .end local v26    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v27, 0x26

    ushr-long v28, v8, v27

    const-wide/32 v30, 0x3fffff

    and-long v28, v28, v30

    aput-wide v28, p3, v26

    .line 114
    add-int/lit8 v26, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v26    # "valuesOffset":I
    const/16 v27, 0x10

    ushr-long v28, v8, v27

    const-wide/32 v30, 0x3fffff

    and-long v28, v28, v30

    aput-wide v28, p3, p4

    .line 115
    add-int/lit8 v24, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v24    # "blocksOffset":I
    aget-wide v10, p1, p2

    .line 116
    .local v10, "block3":J
    add-int/lit8 p4, v26, 0x1

    .end local v26    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/32 v28, 0xffff

    and-long v28, v28, v8

    const/16 v27, 0x6

    shl-long v28, v28, v27

    const/16 v27, 0x3a

    ushr-long v30, v10, v27

    or-long v28, v28, v30

    aput-wide v28, p3, v26

    .line 117
    add-int/lit8 v26, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v26    # "valuesOffset":I
    const/16 v27, 0x24

    ushr-long v28, v10, v27

    const-wide/32 v30, 0x3fffff

    and-long v28, v28, v30

    aput-wide v28, p3, p4

    .line 118
    add-int/lit8 p4, v26, 0x1

    .end local v26    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v27, 0xe

    ushr-long v28, v10, v27

    const-wide/32 v30, 0x3fffff

    and-long v28, v28, v30

    aput-wide v28, p3, v26

    .line 119
    add-int/lit8 p2, v24, 0x1

    .end local v24    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v12, p1, v24

    .line 120
    .local v12, "block4":J
    add-int/lit8 v26, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v26    # "valuesOffset":I
    const-wide/16 v28, 0x3fff

    and-long v28, v28, v10

    const/16 v27, 0x8

    shl-long v28, v28, v27

    const/16 v27, 0x38

    ushr-long v30, v12, v27

    or-long v28, v28, v30

    aput-wide v28, p3, p4

    .line 121
    add-int/lit8 p4, v26, 0x1

    .end local v26    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v27, 0x22

    ushr-long v28, v12, v27

    const-wide/32 v30, 0x3fffff

    and-long v28, v28, v30

    aput-wide v28, p3, v26

    .line 122
    add-int/lit8 v26, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v26    # "valuesOffset":I
    const/16 v27, 0xc

    ushr-long v28, v12, v27

    const-wide/32 v30, 0x3fffff

    and-long v28, v28, v30

    aput-wide v28, p3, p4

    .line 123
    add-int/lit8 v24, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v24    # "blocksOffset":I
    aget-wide v14, p1, p2

    .line 124
    .local v14, "block5":J
    add-int/lit8 p4, v26, 0x1

    .end local v26    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v28, 0xfff

    and-long v28, v28, v12

    const/16 v27, 0xa

    shl-long v28, v28, v27

    const/16 v27, 0x36

    ushr-long v30, v14, v27

    or-long v28, v28, v30

    aput-wide v28, p3, v26

    .line 125
    add-int/lit8 v26, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v26    # "valuesOffset":I
    const/16 v27, 0x20

    ushr-long v28, v14, v27

    const-wide/32 v30, 0x3fffff

    and-long v28, v28, v30

    aput-wide v28, p3, p4

    .line 126
    add-int/lit8 p4, v26, 0x1

    .end local v26    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v27, 0xa

    ushr-long v28, v14, v27

    const-wide/32 v30, 0x3fffff

    and-long v28, v28, v30

    aput-wide v28, p3, v26

    .line 127
    add-int/lit8 p2, v24, 0x1

    .end local v24    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v16, p1, v24

    .line 128
    .local v16, "block6":J
    add-int/lit8 v26, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v26    # "valuesOffset":I
    const-wide/16 v28, 0x3ff

    and-long v28, v28, v14

    const/16 v27, 0xc

    shl-long v28, v28, v27

    const/16 v27, 0x34

    ushr-long v30, v16, v27

    or-long v28, v28, v30

    aput-wide v28, p3, p4

    .line 129
    add-int/lit8 p4, v26, 0x1

    .end local v26    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v27, 0x1e

    ushr-long v28, v16, v27

    const-wide/32 v30, 0x3fffff

    and-long v28, v28, v30

    aput-wide v28, p3, v26

    .line 130
    add-int/lit8 v26, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v26    # "valuesOffset":I
    const/16 v27, 0x8

    ushr-long v28, v16, v27

    const-wide/32 v30, 0x3fffff

    and-long v28, v28, v30

    aput-wide v28, p3, p4

    .line 131
    add-int/lit8 v24, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v24    # "blocksOffset":I
    aget-wide v18, p1, p2

    .line 132
    .local v18, "block7":J
    add-int/lit8 p4, v26, 0x1

    .end local v26    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v28, 0xff

    and-long v28, v28, v16

    const/16 v27, 0xe

    shl-long v28, v28, v27

    const/16 v27, 0x32

    ushr-long v30, v18, v27

    or-long v28, v28, v30

    aput-wide v28, p3, v26

    .line 133
    add-int/lit8 v26, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v26    # "valuesOffset":I
    const/16 v27, 0x1c

    ushr-long v28, v18, v27

    const-wide/32 v30, 0x3fffff

    and-long v28, v28, v30

    aput-wide v28, p3, p4

    .line 134
    add-int/lit8 p4, v26, 0x1

    .end local v26    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v27, 0x6

    ushr-long v28, v18, v27

    const-wide/32 v30, 0x3fffff

    and-long v28, v28, v30

    aput-wide v28, p3, v26

    .line 135
    add-int/lit8 p2, v24, 0x1

    .end local v24    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v20, p1, v24

    .line 136
    .local v20, "block8":J
    add-int/lit8 v26, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v26    # "valuesOffset":I
    const-wide/16 v28, 0x3f

    and-long v28, v28, v18

    const/16 v27, 0x10

    shl-long v28, v28, v27

    const/16 v27, 0x30

    ushr-long v30, v20, v27

    or-long v28, v28, v30

    aput-wide v28, p3, p4

    .line 137
    add-int/lit8 p4, v26, 0x1

    .end local v26    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v27, 0x1a

    ushr-long v28, v20, v27

    const-wide/32 v30, 0x3fffff

    and-long v28, v28, v30

    aput-wide v28, p3, v26

    .line 138
    add-int/lit8 v26, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v26    # "valuesOffset":I
    const/16 v27, 0x4

    ushr-long v28, v20, v27

    const-wide/32 v30, 0x3fffff

    and-long v28, v28, v30

    aput-wide v28, p3, p4

    .line 139
    add-int/lit8 v24, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v24    # "blocksOffset":I
    aget-wide v22, p1, p2

    .line 140
    .local v22, "block9":J
    add-int/lit8 p4, v26, 0x1

    .end local v26    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v28, 0xf

    and-long v28, v28, v20

    const/16 v27, 0x12

    shl-long v28, v28, v27

    const/16 v27, 0x2e

    ushr-long v30, v22, v27

    or-long v28, v28, v30

    aput-wide v28, p3, v26

    .line 141
    add-int/lit8 v26, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v26    # "valuesOffset":I
    const/16 v27, 0x18

    ushr-long v28, v22, v27

    const-wide/32 v30, 0x3fffff

    and-long v28, v28, v30

    aput-wide v28, p3, p4

    .line 142
    add-int/lit8 p4, v26, 0x1

    .end local v26    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v27, 0x2

    ushr-long v28, v22, v27

    const-wide/32 v30, 0x3fffff

    and-long v28, v28, v30

    aput-wide v28, p3, v26

    .line 143
    add-int/lit8 p2, v24, 0x1

    .end local v24    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v6, p1, v24

    .line 144
    .local v6, "block10":J
    add-int/lit8 v26, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v26    # "valuesOffset":I
    const-wide/16 v28, 0x3

    and-long v28, v28, v22

    const/16 v27, 0x14

    shl-long v28, v28, v27

    const/16 v27, 0x2c

    ushr-long v30, v6, v27

    or-long v28, v28, v30

    aput-wide v28, p3, p4

    .line 145
    add-int/lit8 p4, v26, 0x1

    .end local v26    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v27, 0x16

    ushr-long v28, v6, v27

    const-wide/32 v30, 0x3fffff

    and-long v28, v28, v30

    aput-wide v28, p3, v26

    .line 146
    add-int/lit8 v26, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v26    # "valuesOffset":I
    const-wide/32 v28, 0x3fffff

    and-long v28, v28, v6

    aput-wide v28, p3, p4

    .line 103
    add-int/lit8 v25, v25, 0x1

    move/from16 v24, p2

    .end local p2    # "blocksOffset":I
    .restart local v24    # "blocksOffset":I
    goto/16 :goto_0
.end method
