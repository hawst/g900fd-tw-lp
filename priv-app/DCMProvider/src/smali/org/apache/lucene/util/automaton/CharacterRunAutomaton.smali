.class public Lorg/apache/lucene/util/automaton/CharacterRunAutomaton;
.super Lorg/apache/lucene/util/automaton/RunAutomaton;
.source "CharacterRunAutomaton.java"


# direct methods
.method public constructor <init>(Lorg/apache/lucene/util/automaton/Automaton;)V
    .locals 2
    .param p1, "a"    # Lorg/apache/lucene/util/automaton/Automaton;

    .prologue
    .line 26
    const v0, 0x10ffff

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lorg/apache/lucene/util/automaton/RunAutomaton;-><init>(Lorg/apache/lucene/util/automaton/Automaton;IZ)V

    .line 27
    return-void
.end method


# virtual methods
.method public run(Ljava/lang/String;)Z
    .locals 5
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 33
    iget v3, p0, Lorg/apache/lucene/util/automaton/CharacterRunAutomaton;->initial:I

    .line 34
    .local v3, "p":I
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    .line 35
    .local v2, "l":I
    const/4 v1, 0x0

    .local v1, "i":I
    const/4 v0, 0x0

    .local v0, "cp":I
    :goto_0
    if-lt v1, v2, :cond_0

    .line 39
    iget-object v4, p0, Lorg/apache/lucene/util/automaton/CharacterRunAutomaton;->accept:[Z

    aget-boolean v4, v4, v3

    :goto_1
    return v4

    .line 36
    :cond_0
    invoke-virtual {p1, v1}, Ljava/lang/String;->codePointAt(I)I

    move-result v0

    invoke-virtual {p0, v3, v0}, Lorg/apache/lucene/util/automaton/CharacterRunAutomaton;->step(II)I

    move-result v3

    .line 37
    const/4 v4, -0x1

    if-ne v3, v4, :cond_1

    const/4 v4, 0x0

    goto :goto_1

    .line 35
    :cond_1
    invoke-static {v0}, Ljava/lang/Character;->charCount(I)I

    move-result v4

    add-int/2addr v1, v4

    goto :goto_0
.end method

.method public run([CII)Z
    .locals 5
    .param p1, "s"    # [C
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 46
    iget v3, p0, Lorg/apache/lucene/util/automaton/CharacterRunAutomaton;->initial:I

    .line 47
    .local v3, "p":I
    add-int v2, p2, p3

    .line 48
    .local v2, "l":I
    move v1, p2

    .local v1, "i":I
    const/4 v0, 0x0

    .local v0, "cp":I
    :goto_0
    if-lt v1, v2, :cond_0

    .line 52
    iget-object v4, p0, Lorg/apache/lucene/util/automaton/CharacterRunAutomaton;->accept:[Z

    aget-boolean v4, v4, v3

    :goto_1
    return v4

    .line 49
    :cond_0
    invoke-static {p1, v1, v2}, Ljava/lang/Character;->codePointAt([CII)I

    move-result v0

    invoke-virtual {p0, v3, v0}, Lorg/apache/lucene/util/automaton/CharacterRunAutomaton;->step(II)I

    move-result v3

    .line 50
    const/4 v4, -0x1

    if-ne v3, v4, :cond_1

    const/4 v4, 0x0

    goto :goto_1

    .line 48
    :cond_1
    invoke-static {v0}, Ljava/lang/Character;->charCount(I)I

    move-result v4

    add-int/2addr v1, v4

    goto :goto_0
.end method
