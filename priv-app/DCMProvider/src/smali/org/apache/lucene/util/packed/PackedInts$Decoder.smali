.class public interface abstract Lorg/apache/lucene/util/packed/PackedInts$Decoder;
.super Ljava/lang/Object;
.source "PackedInts.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/packed/PackedInts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Decoder"
.end annotation


# virtual methods
.method public abstract byteBlockCount()I
.end method

.method public abstract byteValueCount()I
.end method

.method public abstract decode([BI[III)V
.end method

.method public abstract decode([BI[JII)V
.end method

.method public abstract decode([JI[III)V
.end method

.method public abstract decode([JI[JII)V
.end method

.method public abstract longBlockCount()I
.end method

.method public abstract longValueCount()I
.end method
