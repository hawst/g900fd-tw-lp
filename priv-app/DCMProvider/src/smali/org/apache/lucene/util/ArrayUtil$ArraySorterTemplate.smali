.class abstract Lorg/apache/lucene/util/ArrayUtil$ArraySorterTemplate;
.super Lorg/apache/lucene/util/SorterTemplate;
.source "ArrayUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/ArrayUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "ArraySorterTemplate"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lorg/apache/lucene/util/SorterTemplate;"
    }
.end annotation


# instance fields
.field protected final a:[Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[TT;"
        }
    .end annotation
.end field

.field private pivot:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>([Ljava/lang/Object;)V
    .locals 0
    .param p1, "a"    # [Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TT;)V"
        }
    .end annotation

    .prologue
    .line 617
    .local p0, "this":Lorg/apache/lucene/util/ArrayUtil$ArraySorterTemplate;, "Lorg/apache/lucene/util/ArrayUtil$ArraySorterTemplate<TT;>;"
    invoke-direct {p0}, Lorg/apache/lucene/util/SorterTemplate;-><init>()V

    .line 618
    iput-object p1, p0, Lorg/apache/lucene/util/ArrayUtil$ArraySorterTemplate;->a:[Ljava/lang/Object;

    .line 619
    return-void
.end method


# virtual methods
.method protected compare(II)I
    .locals 2
    .param p1, "i"    # I
    .param p2, "j"    # I

    .prologue
    .line 632
    .local p0, "this":Lorg/apache/lucene/util/ArrayUtil$ArraySorterTemplate;, "Lorg/apache/lucene/util/ArrayUtil$ArraySorterTemplate<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/util/ArrayUtil$ArraySorterTemplate;->a:[Ljava/lang/Object;

    aget-object v0, v0, p1

    iget-object v1, p0, Lorg/apache/lucene/util/ArrayUtil$ArraySorterTemplate;->a:[Ljava/lang/Object;

    aget-object v1, v1, p2

    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/util/ArrayUtil$ArraySorterTemplate;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method protected abstract compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TT;)I"
        }
    .end annotation
.end method

.method protected comparePivot(I)I
    .locals 2
    .param p1, "j"    # I

    .prologue
    .line 642
    .local p0, "this":Lorg/apache/lucene/util/ArrayUtil$ArraySorterTemplate;, "Lorg/apache/lucene/util/ArrayUtil$ArraySorterTemplate<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/util/ArrayUtil$ArraySorterTemplate;->pivot:Ljava/lang/Object;

    iget-object v1, p0, Lorg/apache/lucene/util/ArrayUtil$ArraySorterTemplate;->a:[Ljava/lang/Object;

    aget-object v1, v1, p1

    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/util/ArrayUtil$ArraySorterTemplate;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method protected setPivot(I)V
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 637
    .local p0, "this":Lorg/apache/lucene/util/ArrayUtil$ArraySorterTemplate;, "Lorg/apache/lucene/util/ArrayUtil$ArraySorterTemplate<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/util/ArrayUtil$ArraySorterTemplate;->a:[Ljava/lang/Object;

    aget-object v0, v0, p1

    iput-object v0, p0, Lorg/apache/lucene/util/ArrayUtil$ArraySorterTemplate;->pivot:Ljava/lang/Object;

    .line 638
    return-void
.end method

.method protected swap(II)V
    .locals 3
    .param p1, "i"    # I
    .param p2, "j"    # I

    .prologue
    .line 625
    .local p0, "this":Lorg/apache/lucene/util/ArrayUtil$ArraySorterTemplate;, "Lorg/apache/lucene/util/ArrayUtil$ArraySorterTemplate<TT;>;"
    iget-object v1, p0, Lorg/apache/lucene/util/ArrayUtil$ArraySorterTemplate;->a:[Ljava/lang/Object;

    aget-object v0, v1, p1

    .line 626
    .local v0, "o":Ljava/lang/Object;, "TT;"
    iget-object v1, p0, Lorg/apache/lucene/util/ArrayUtil$ArraySorterTemplate;->a:[Ljava/lang/Object;

    iget-object v2, p0, Lorg/apache/lucene/util/ArrayUtil$ArraySorterTemplate;->a:[Ljava/lang/Object;

    aget-object v2, v2, p2

    aput-object v2, v1, p1

    .line 627
    iget-object v1, p0, Lorg/apache/lucene/util/ArrayUtil$ArraySorterTemplate;->a:[Ljava/lang/Object;

    aput-object v0, v1, p2

    .line 628
    return-void
.end method
