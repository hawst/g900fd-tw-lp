.class public final Lorg/apache/lucene/util/automaton/UTF32ToUTF8;
.super Ljava/lang/Object;
.source "UTF32ToUTF8.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Byte;,
        Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field static MASKS:[I

.field private static final endCodes:[I

.field private static final startCodes:[I


# instance fields
.field private final endUTF8:Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;

.field private final startUTF8:Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;

.field private final tmpUTF8a:Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;

.field private final tmpUTF8b:Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;

.field private utf8StateCount:I

.field private utf8States:[Lorg/apache/lucene/util/automaton/State;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/16 v6, 0x20

    const/4 v5, 0x4

    const/4 v3, 0x1

    .line 33
    const-class v2, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;

    invoke-virtual {v2}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v2

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    sput-boolean v2, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->$assertionsDisabled:Z

    .line 36
    new-array v2, v5, [I

    const/16 v4, 0x80

    aput v4, v2, v3

    const/4 v3, 0x2

    const/16 v4, 0x800

    aput v4, v2, v3

    const/4 v3, 0x3

    const/high16 v4, 0x10000

    aput v4, v2, v3

    sput-object v2, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->startCodes:[I

    .line 37
    new-array v2, v5, [I

    fill-array-data v2, :array_0

    sput-object v2, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->endCodes:[I

    .line 39
    new-array v2, v6, [I

    sput-object v2, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->MASKS:[I

    .line 41
    const/4 v1, 0x2

    .line 42
    .local v1, "v":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-lt v0, v6, :cond_1

    .line 46
    return-void

    .line 33
    .end local v0    # "i":I
    .end local v1    # "v":I
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 43
    .restart local v0    # "i":I
    .restart local v1    # "v":I
    :cond_1
    sget-object v2, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->MASKS:[I

    add-int/lit8 v3, v1, -0x1

    aput v3, v2, v0

    .line 44
    mul-int/lit8 v1, v1, 0x2

    .line 42
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 37
    :array_0
    .array-data 4
        0x7f
        0x7ff
        0xffff
        0x10ffff
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 125
    new-instance v0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;

    invoke-direct {v0}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->startUTF8:Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;

    .line 126
    new-instance v0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;

    invoke-direct {v0}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->endUTF8:Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;

    .line 128
    new-instance v0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;

    invoke-direct {v0}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->tmpUTF8a:Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;

    .line 129
    new-instance v0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;

    invoke-direct {v0}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->tmpUTF8b:Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;

    .line 33
    return-void
.end method

.method private all(Lorg/apache/lucene/util/automaton/State;Lorg/apache/lucene/util/automaton/State;III)V
    .locals 5
    .param p1, "start"    # Lorg/apache/lucene/util/automaton/State;
    .param p2, "end"    # Lorg/apache/lucene/util/automaton/State;
    .param p3, "startCode"    # I
    .param p4, "endCode"    # I
    .param p5, "left"    # I

    .prologue
    const/16 v4, 0xbf

    const/16 v3, 0x80

    .line 235
    if-nez p5, :cond_0

    .line 236
    new-instance v2, Lorg/apache/lucene/util/automaton/Transition;

    invoke-direct {v2, p3, p4, p2}, Lorg/apache/lucene/util/automaton/Transition;-><init>(IILorg/apache/lucene/util/automaton/State;)V

    invoke-virtual {p1, v2}, Lorg/apache/lucene/util/automaton/State;->addTransition(Lorg/apache/lucene/util/automaton/Transition;)V

    .line 248
    :goto_0
    return-void

    .line 238
    :cond_0
    invoke-direct {p0}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->newUTF8State()Lorg/apache/lucene/util/automaton/State;

    move-result-object v0

    .line 239
    .local v0, "lastN":Lorg/apache/lucene/util/automaton/State;
    new-instance v2, Lorg/apache/lucene/util/automaton/Transition;

    invoke-direct {v2, p3, p4, v0}, Lorg/apache/lucene/util/automaton/Transition;-><init>(IILorg/apache/lucene/util/automaton/State;)V

    invoke-virtual {p1, v2}, Lorg/apache/lucene/util/automaton/State;->addTransition(Lorg/apache/lucene/util/automaton/Transition;)V

    .line 240
    :goto_1
    const/4 v2, 0x1

    if-gt p5, v2, :cond_1

    .line 246
    new-instance v2, Lorg/apache/lucene/util/automaton/Transition;

    invoke-direct {v2, v3, v4, p2}, Lorg/apache/lucene/util/automaton/Transition;-><init>(IILorg/apache/lucene/util/automaton/State;)V

    invoke-virtual {v0, v2}, Lorg/apache/lucene/util/automaton/State;->addTransition(Lorg/apache/lucene/util/automaton/Transition;)V

    goto :goto_0

    .line 241
    :cond_1
    invoke-direct {p0}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->newUTF8State()Lorg/apache/lucene/util/automaton/State;

    move-result-object v1

    .line 242
    .local v1, "n":Lorg/apache/lucene/util/automaton/State;
    new-instance v2, Lorg/apache/lucene/util/automaton/Transition;

    invoke-direct {v2, v3, v4, v1}, Lorg/apache/lucene/util/automaton/Transition;-><init>(IILorg/apache/lucene/util/automaton/State;)V

    invoke-virtual {v0, v2}, Lorg/apache/lucene/util/automaton/State;->addTransition(Lorg/apache/lucene/util/automaton/Transition;)V

    .line 243
    add-int/lit8 p5, p5, -0x1

    .line 244
    move-object v0, v1

    goto :goto_1
.end method

.method private build(Lorg/apache/lucene/util/automaton/State;Lorg/apache/lucene/util/automaton/State;Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;I)V
    .locals 10
    .param p1, "start"    # Lorg/apache/lucene/util/automaton/State;
    .param p2, "end"    # Lorg/apache/lucene/util/automaton/State;
    .param p3, "startUTF8"    # Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;
    .param p4, "endUTF8"    # Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;
    .param p5, "upto"    # I

    .prologue
    .line 143
    invoke-virtual {p3, p5}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->byteAt(I)I

    move-result v0

    invoke-virtual {p4, p5}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->byteAt(I)I

    move-result v2

    if-ne v0, v2, :cond_3

    .line 145
    # getter for: Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->len:I
    invoke-static {p3}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->access$1(Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p5, v0, :cond_0

    # getter for: Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->len:I
    invoke-static {p4}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->access$1(Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p5, v0, :cond_0

    .line 147
    new-instance v0, Lorg/apache/lucene/util/automaton/Transition;

    invoke-virtual {p3, p5}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->byteAt(I)I

    move-result v2

    invoke-virtual {p4, p5}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->byteAt(I)I

    move-result v3

    invoke-direct {v0, v2, v3, p2}, Lorg/apache/lucene/util/automaton/Transition;-><init>(IILorg/apache/lucene/util/automaton/State;)V

    invoke-virtual {p1, v0}, Lorg/apache/lucene/util/automaton/State;->addTransition(Lorg/apache/lucene/util/automaton/Transition;)V

    .line 194
    :goto_0
    return-void

    .line 150
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    # getter for: Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->len:I
    invoke-static {p3}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->access$1(Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;)I

    move-result v0

    add-int/lit8 v2, p5, 0x1

    if-gt v0, v2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 151
    :cond_1
    sget-boolean v0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    # getter for: Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->len:I
    invoke-static {p4}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->access$1(Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;)I

    move-result v0

    add-int/lit8 v2, p5, 0x1

    if-gt v0, v2, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 152
    :cond_2
    invoke-direct {p0}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->newUTF8State()Lorg/apache/lucene/util/automaton/State;

    move-result-object v1

    .line 155
    .local v1, "n":Lorg/apache/lucene/util/automaton/State;
    new-instance v0, Lorg/apache/lucene/util/automaton/Transition;

    invoke-virtual {p3, p5}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->byteAt(I)I

    move-result v2

    invoke-direct {v0, v2, v1}, Lorg/apache/lucene/util/automaton/Transition;-><init>(ILorg/apache/lucene/util/automaton/State;)V

    invoke-virtual {p1, v0}, Lorg/apache/lucene/util/automaton/State;->addTransition(Lorg/apache/lucene/util/automaton/Transition;)V

    .line 158
    add-int/lit8 v5, p5, 0x1

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->build(Lorg/apache/lucene/util/automaton/State;Lorg/apache/lucene/util/automaton/State;Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;I)V

    goto :goto_0

    .line 160
    .end local v1    # "n":Lorg/apache/lucene/util/automaton/State;
    :cond_3
    # getter for: Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->len:I
    invoke-static {p3}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->access$1(Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;)I

    move-result v0

    # getter for: Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->len:I
    invoke-static {p4}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->access$1(Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;)I

    move-result v2

    if-ne v0, v2, :cond_6

    .line 161
    # getter for: Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->len:I
    invoke-static {p3}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->access$1(Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p5, v0, :cond_4

    .line 162
    new-instance v0, Lorg/apache/lucene/util/automaton/Transition;

    invoke-virtual {p3, p5}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->byteAt(I)I

    move-result v2

    invoke-virtual {p4, p5}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->byteAt(I)I

    move-result v3

    invoke-direct {v0, v2, v3, p2}, Lorg/apache/lucene/util/automaton/Transition;-><init>(IILorg/apache/lucene/util/automaton/State;)V

    invoke-virtual {p1, v0}, Lorg/apache/lucene/util/automaton/State;->addTransition(Lorg/apache/lucene/util/automaton/Transition;)V

    goto :goto_0

    .line 164
    :cond_4
    const/4 v7, 0x0

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move v6, p5

    invoke-direct/range {v2 .. v7}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->start(Lorg/apache/lucene/util/automaton/State;Lorg/apache/lucene/util/automaton/State;Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;IZ)V

    .line 165
    invoke-virtual {p4, p5}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->byteAt(I)I

    move-result v0

    invoke-virtual {p3, p5}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->byteAt(I)I

    move-result v2

    sub-int/2addr v0, v2

    const/4 v2, 0x1

    if-le v0, v2, :cond_5

    .line 167
    invoke-virtual {p3, p5}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->byteAt(I)I

    move-result v0

    add-int/lit8 v5, v0, 0x1

    invoke-virtual {p4, p5}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->byteAt(I)I

    move-result v0

    add-int/lit8 v6, v0, -0x1

    # getter for: Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->len:I
    invoke-static {p3}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->access$1(Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;)I

    move-result v0

    sub-int/2addr v0, p5

    add-int/lit8 v7, v0, -0x1

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v2 .. v7}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->all(Lorg/apache/lucene/util/automaton/State;Lorg/apache/lucene/util/automaton/State;III)V

    .line 169
    :cond_5
    const/4 v7, 0x0

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p4

    move v6, p5

    invoke-direct/range {v2 .. v7}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->end(Lorg/apache/lucene/util/automaton/State;Lorg/apache/lucene/util/automaton/State;Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;IZ)V

    goto/16 :goto_0

    .line 174
    :cond_6
    const/4 v7, 0x1

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move v6, p5

    invoke-direct/range {v2 .. v7}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->start(Lorg/apache/lucene/util/automaton/State;Lorg/apache/lucene/util/automaton/State;Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;IZ)V

    .line 177
    # getter for: Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->len:I
    invoke-static {p3}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->access$1(Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    sub-int v8, v0, p5

    .line 178
    .local v8, "byteCount":I
    # getter for: Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->len:I
    invoke-static {p4}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->access$1(Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;)I

    move-result v0

    sub-int v9, v0, p5

    .line 179
    .local v9, "limit":I
    :goto_1
    if-lt v8, v9, :cond_7

    .line 192
    const/4 v7, 0x1

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p4

    move v6, p5

    invoke-direct/range {v2 .. v7}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->end(Lorg/apache/lucene/util/automaton/State;Lorg/apache/lucene/util/automaton/State;Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;IZ)V

    goto/16 :goto_0

    .line 182
    :cond_7
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->tmpUTF8a:Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;

    sget-object v2, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->startCodes:[I

    add-int/lit8 v3, v8, -0x1

    aget v2, v2, v3

    # invokes: Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->set(I)V
    invoke-static {v0, v2}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->access$0(Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;I)V

    .line 183
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->tmpUTF8b:Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;

    sget-object v2, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->endCodes:[I

    add-int/lit8 v3, v8, -0x1

    aget v2, v2, v3

    # invokes: Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->set(I)V
    invoke-static {v0, v2}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->access$0(Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;I)V

    .line 185
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->tmpUTF8a:Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->byteAt(I)I

    move-result v5

    .line 186
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->tmpUTF8b:Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->byteAt(I)I

    move-result v6

    .line 187
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->tmpUTF8a:Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;

    # getter for: Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->len:I
    invoke-static {v0}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->access$1(Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;)I

    move-result v0

    add-int/lit8 v7, v0, -0x1

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    .line 184
    invoke-direct/range {v2 .. v7}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->all(Lorg/apache/lucene/util/automaton/State;Lorg/apache/lucene/util/automaton/State;III)V

    .line 188
    add-int/lit8 v8, v8, 0x1

    goto :goto_1
.end method

.method private end(Lorg/apache/lucene/util/automaton/State;Lorg/apache/lucene/util/automaton/State;Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;IZ)V
    .locals 10
    .param p1, "start"    # Lorg/apache/lucene/util/automaton/State;
    .param p2, "end"    # Lorg/apache/lucene/util/automaton/State;
    .param p3, "utf8"    # Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;
    .param p4, "upto"    # I
    .param p5, "doAll"    # Z

    .prologue
    .line 212
    # getter for: Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->len:I
    invoke-static {p3}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->access$1(Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p4, v0, :cond_0

    .line 214
    new-instance v0, Lorg/apache/lucene/util/automaton/Transition;

    invoke-virtual {p3, p4}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->byteAt(I)I

    move-result v1

    sget-object v2, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->MASKS:[I

    invoke-virtual {p3, p4}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->numBits(I)I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    aget v2, v2, v4

    xor-int/lit8 v2, v2, -0x1

    and-int/2addr v1, v2

    invoke-virtual {p3, p4}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->byteAt(I)I

    move-result v2

    invoke-direct {v0, v1, v2, p2}, Lorg/apache/lucene/util/automaton/Transition;-><init>(IILorg/apache/lucene/util/automaton/State;)V

    invoke-virtual {p1, v0}, Lorg/apache/lucene/util/automaton/State;->addTransition(Lorg/apache/lucene/util/automaton/Transition;)V

    .line 232
    :goto_0
    return-void

    .line 217
    :cond_0
    invoke-virtual {p3, p4}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->numBits(I)I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_2

    .line 221
    const/16 v3, 0xc2

    .line 225
    .local v3, "startCode":I
    :goto_1
    if-eqz p5, :cond_1

    invoke-virtual {p3, p4}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->byteAt(I)I

    move-result v0

    if-eq v0, v3, :cond_1

    .line 226
    invoke-virtual {p3, p4}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->byteAt(I)I

    move-result v0

    add-int/lit8 v4, v0, -0x1

    # getter for: Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->len:I
    invoke-static {p3}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->access$1(Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;)I

    move-result v0

    sub-int/2addr v0, p4

    add-int/lit8 v5, v0, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->all(Lorg/apache/lucene/util/automaton/State;Lorg/apache/lucene/util/automaton/State;III)V

    .line 228
    :cond_1
    invoke-direct {p0}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->newUTF8State()Lorg/apache/lucene/util/automaton/State;

    move-result-object v5

    .line 229
    .local v5, "n":Lorg/apache/lucene/util/automaton/State;
    new-instance v0, Lorg/apache/lucene/util/automaton/Transition;

    invoke-virtual {p3, p4}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->byteAt(I)I

    move-result v1

    invoke-direct {v0, v1, v5}, Lorg/apache/lucene/util/automaton/Transition;-><init>(ILorg/apache/lucene/util/automaton/State;)V

    invoke-virtual {p1, v0}, Lorg/apache/lucene/util/automaton/State;->addTransition(Lorg/apache/lucene/util/automaton/Transition;)V

    .line 230
    add-int/lit8 v8, p4, 0x1

    const/4 v9, 0x1

    move-object v4, p0

    move-object v6, p2

    move-object v7, p3

    invoke-direct/range {v4 .. v9}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->end(Lorg/apache/lucene/util/automaton/State;Lorg/apache/lucene/util/automaton/State;Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;IZ)V

    goto :goto_0

    .line 223
    .end local v3    # "startCode":I
    .end local v5    # "n":Lorg/apache/lucene/util/automaton/State;
    :cond_2
    invoke-virtual {p3, p4}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->byteAt(I)I

    move-result v0

    sget-object v1, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->MASKS:[I

    invoke-virtual {p3, p4}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->numBits(I)I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    aget v1, v1, v2

    xor-int/lit8 v1, v1, -0x1

    and-int v3, v0, v1

    .restart local v3    # "startCode":I
    goto :goto_1
.end method

.method private newUTF8State()Lorg/apache/lucene/util/automaton/State;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 305
    new-instance v1, Lorg/apache/lucene/util/automaton/State;

    invoke-direct {v1}, Lorg/apache/lucene/util/automaton/State;-><init>()V

    .line 306
    .local v1, "s":Lorg/apache/lucene/util/automaton/State;
    iget v2, p0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->utf8StateCount:I

    iget-object v3, p0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->utf8States:[Lorg/apache/lucene/util/automaton/State;

    array-length v3, v3

    if-ne v2, v3, :cond_0

    .line 307
    iget v2, p0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->utf8StateCount:I

    add-int/lit8 v2, v2, 0x1

    sget v3, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_REF:I

    invoke-static {v2, v3}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v2

    new-array v0, v2, [Lorg/apache/lucene/util/automaton/State;

    .line 308
    .local v0, "newArray":[Lorg/apache/lucene/util/automaton/State;
    iget-object v2, p0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->utf8States:[Lorg/apache/lucene/util/automaton/State;

    iget v3, p0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->utf8StateCount:I

    invoke-static {v2, v4, v0, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 309
    iput-object v0, p0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->utf8States:[Lorg/apache/lucene/util/automaton/State;

    .line 311
    .end local v0    # "newArray":[Lorg/apache/lucene/util/automaton/State;
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->utf8States:[Lorg/apache/lucene/util/automaton/State;

    iget v3, p0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->utf8StateCount:I

    aput-object v1, v2, v3

    .line 312
    iget v2, p0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->utf8StateCount:I

    iput v2, v1, Lorg/apache/lucene/util/automaton/State;->number:I

    .line 313
    iget v2, p0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->utf8StateCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->utf8StateCount:I

    .line 314
    return-object v1
.end method

.method private start(Lorg/apache/lucene/util/automaton/State;Lorg/apache/lucene/util/automaton/State;Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;IZ)V
    .locals 8
    .param p1, "start"    # Lorg/apache/lucene/util/automaton/State;
    .param p2, "end"    # Lorg/apache/lucene/util/automaton/State;
    .param p3, "utf8"    # Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;
    .param p4, "upto"    # I
    .param p5, "doAll"    # Z

    .prologue
    .line 197
    # getter for: Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->len:I
    invoke-static {p3}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->access$1(Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p4, v0, :cond_1

    .line 199
    new-instance v0, Lorg/apache/lucene/util/automaton/Transition;

    invoke-virtual {p3, p4}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->byteAt(I)I

    move-result v2

    invoke-virtual {p3, p4}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->byteAt(I)I

    move-result v3

    sget-object v4, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->MASKS:[I

    invoke-virtual {p3, p4}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->numBits(I)I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    aget v4, v4, v5

    or-int/2addr v3, v4

    invoke-direct {v0, v2, v3, p2}, Lorg/apache/lucene/util/automaton/Transition;-><init>(IILorg/apache/lucene/util/automaton/State;)V

    invoke-virtual {p1, v0}, Lorg/apache/lucene/util/automaton/State;->addTransition(Lorg/apache/lucene/util/automaton/Transition;)V

    .line 209
    :cond_0
    :goto_0
    return-void

    .line 201
    :cond_1
    invoke-direct {p0}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->newUTF8State()Lorg/apache/lucene/util/automaton/State;

    move-result-object v1

    .line 202
    .local v1, "n":Lorg/apache/lucene/util/automaton/State;
    new-instance v0, Lorg/apache/lucene/util/automaton/Transition;

    invoke-virtual {p3, p4}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->byteAt(I)I

    move-result v2

    invoke-direct {v0, v2, v1}, Lorg/apache/lucene/util/automaton/Transition;-><init>(ILorg/apache/lucene/util/automaton/State;)V

    invoke-virtual {p1, v0}, Lorg/apache/lucene/util/automaton/State;->addTransition(Lorg/apache/lucene/util/automaton/Transition;)V

    .line 203
    add-int/lit8 v4, p4, 0x1

    const/4 v5, 0x1

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->start(Lorg/apache/lucene/util/automaton/State;Lorg/apache/lucene/util/automaton/State;Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;IZ)V

    .line 204
    invoke-virtual {p3, p4}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->byteAt(I)I

    move-result v0

    sget-object v2, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->MASKS:[I

    invoke-virtual {p3, p4}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->numBits(I)I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    aget v2, v2, v3

    or-int v6, v0, v2

    .line 205
    .local v6, "endCode":I
    if-eqz p5, :cond_0

    invoke-virtual {p3, p4}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->byteAt(I)I

    move-result v0

    if-eq v0, v6, :cond_0

    .line 206
    invoke-virtual {p3, p4}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->byteAt(I)I

    move-result v0

    add-int/lit8 v5, v0, 0x1

    # getter for: Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->len:I
    invoke-static {p3}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->access$1(Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;)I

    move-result v0

    sub-int/2addr v0, p4

    add-int/lit8 v7, v0, -0x1

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v2 .. v7}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->all(Lorg/apache/lucene/util/automaton/State;Lorg/apache/lucene/util/automaton/State;III)V

    goto :goto_0
.end method


# virtual methods
.method public convert(Lorg/apache/lucene/util/automaton/Automaton;)Lorg/apache/lucene/util/automaton/Automaton;
    .locals 11
    .param p1, "utf32"    # Lorg/apache/lucene/util/automaton/Automaton;

    .prologue
    const/4 v10, 0x0

    .line 259
    invoke-virtual {p1}, Lorg/apache/lucene/util/automaton/Automaton;->isSingleton()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 260
    invoke-virtual {p1}, Lorg/apache/lucene/util/automaton/Automaton;->cloneExpanded()Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object p1

    .line 263
    :cond_0
    invoke-virtual {p1}, Lorg/apache/lucene/util/automaton/Automaton;->getNumberedStates()[Lorg/apache/lucene/util/automaton/State;

    move-result-object v9

    array-length v9, v9

    new-array v3, v9, [Lorg/apache/lucene/util/automaton/State;

    .line 264
    .local v3, "map":[Lorg/apache/lucene/util/automaton/State;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 265
    .local v4, "pending":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/util/automaton/State;>;"
    invoke-virtual {p1}, Lorg/apache/lucene/util/automaton/Automaton;->getInitialState()Lorg/apache/lucene/util/automaton/State;

    move-result-object v6

    .line 266
    .local v6, "utf32State":Lorg/apache/lucene/util/automaton/State;
    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 267
    new-instance v7, Lorg/apache/lucene/util/automaton/Automaton;

    invoke-direct {v7}, Lorg/apache/lucene/util/automaton/Automaton;-><init>()V

    .line 268
    .local v7, "utf8":Lorg/apache/lucene/util/automaton/Automaton;
    invoke-virtual {v7, v10}, Lorg/apache/lucene/util/automaton/Automaton;->setDeterministic(Z)V

    .line 270
    invoke-virtual {v7}, Lorg/apache/lucene/util/automaton/Automaton;->getInitialState()Lorg/apache/lucene/util/automaton/State;

    move-result-object v8

    .line 272
    .local v8, "utf8State":Lorg/apache/lucene/util/automaton/State;
    const/4 v9, 0x5

    new-array v9, v9, [Lorg/apache/lucene/util/automaton/State;

    iput-object v9, p0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->utf8States:[Lorg/apache/lucene/util/automaton/State;

    .line 273
    iput v10, p0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->utf8StateCount:I

    .line 274
    iget v9, p0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->utf8StateCount:I

    iput v9, v8, Lorg/apache/lucene/util/automaton/State;->number:I

    .line 275
    iget-object v9, p0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->utf8States:[Lorg/apache/lucene/util/automaton/State;

    iget v10, p0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->utf8StateCount:I

    aput-object v8, v9, v10

    .line 276
    iget v9, p0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->utf8StateCount:I

    add-int/lit8 v9, v9, 0x1

    iput v9, p0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->utf8StateCount:I

    .line 278
    invoke-virtual {v6}, Lorg/apache/lucene/util/automaton/State;->isAccept()Z

    move-result v9

    invoke-virtual {v8, v9}, Lorg/apache/lucene/util/automaton/State;->setAccept(Z)V

    .line 280
    iget v9, v6, Lorg/apache/lucene/util/automaton/State;->number:I

    aput-object v8, v3, v9

    .line 282
    :cond_1
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v9

    if-nez v9, :cond_2

    .line 299
    iget-object v9, p0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->utf8States:[Lorg/apache/lucene/util/automaton/State;

    iget v10, p0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->utf8StateCount:I

    invoke-virtual {v7, v9, v10}, Lorg/apache/lucene/util/automaton/Automaton;->setNumberedStates([Lorg/apache/lucene/util/automaton/State;I)V

    .line 301
    return-object v7

    .line 283
    :cond_2
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-interface {v4, v9}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "utf32State":Lorg/apache/lucene/util/automaton/State;
    check-cast v6, Lorg/apache/lucene/util/automaton/State;

    .line 284
    .restart local v6    # "utf32State":Lorg/apache/lucene/util/automaton/State;
    iget v9, v6, Lorg/apache/lucene/util/automaton/State;->number:I

    aget-object v8, v3, v9

    .line 285
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget v9, v6, Lorg/apache/lucene/util/automaton/State;->numTransitions:I

    if-ge v2, v9, :cond_1

    .line 286
    iget-object v9, v6, Lorg/apache/lucene/util/automaton/State;->transitionsArray:[Lorg/apache/lucene/util/automaton/Transition;

    aget-object v5, v9, v2

    .line 287
    .local v5, "t":Lorg/apache/lucene/util/automaton/Transition;
    iget-object v0, v5, Lorg/apache/lucene/util/automaton/Transition;->to:Lorg/apache/lucene/util/automaton/State;

    .line 288
    .local v0, "destUTF32":Lorg/apache/lucene/util/automaton/State;
    iget v9, v0, Lorg/apache/lucene/util/automaton/State;->number:I

    aget-object v1, v3, v9

    .line 289
    .local v1, "destUTF8":Lorg/apache/lucene/util/automaton/State;
    if-nez v1, :cond_3

    .line 290
    invoke-direct {p0}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->newUTF8State()Lorg/apache/lucene/util/automaton/State;

    move-result-object v1

    .line 291
    iget-boolean v9, v0, Lorg/apache/lucene/util/automaton/State;->accept:Z

    iput-boolean v9, v1, Lorg/apache/lucene/util/automaton/State;->accept:Z

    .line 292
    iget v9, v0, Lorg/apache/lucene/util/automaton/State;->number:I

    aput-object v1, v3, v9

    .line 293
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 295
    :cond_3
    iget v9, v5, Lorg/apache/lucene/util/automaton/Transition;->min:I

    iget v10, v5, Lorg/apache/lucene/util/automaton/Transition;->max:I

    invoke-virtual {p0, v8, v1, v9, v10}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->convertOneEdge(Lorg/apache/lucene/util/automaton/State;Lorg/apache/lucene/util/automaton/State;II)V

    .line 285
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method convertOneEdge(Lorg/apache/lucene/util/automaton/State;Lorg/apache/lucene/util/automaton/State;II)V
    .locals 6
    .param p1, "start"    # Lorg/apache/lucene/util/automaton/State;
    .param p2, "end"    # Lorg/apache/lucene/util/automaton/State;
    .param p3, "startCodePoint"    # I
    .param p4, "endCodePoint"    # I

    .prologue
    .line 133
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->startUTF8:Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;

    # invokes: Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->set(I)V
    invoke-static {v0, p3}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->access$0(Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;I)V

    .line 134
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->endUTF8:Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;

    # invokes: Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->set(I)V
    invoke-static {v0, p4}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->access$0(Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;I)V

    .line 137
    iget-object v3, p0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->startUTF8:Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;

    iget-object v4, p0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->endUTF8:Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->build(Lorg/apache/lucene/util/automaton/State;Lorg/apache/lucene/util/automaton/State;Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;I)V

    .line 138
    return-void
.end method
