.class public final Lorg/apache/lucene/util/IntsRef;
.super Ljava/lang/Object;
.source "IntsRef.java"

# interfaces
.implements Ljava/lang/Cloneable;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lorg/apache/lucene/util/IntsRef;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final EMPTY_INTS:[I


# instance fields
.field public ints:[I

.field public length:I

.field public offset:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 25
    const-class v0, Lorg/apache/lucene/util/IntsRef;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/IntsRef;->$assertionsDisabled:Z

    .line 27
    new-array v0, v1, [I

    sput-object v0, Lorg/apache/lucene/util/IntsRef;->EMPTY_INTS:[I

    return-void

    :cond_0
    move v0, v1

    .line 25
    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    sget-object v0, Lorg/apache/lucene/util/IntsRef;->EMPTY_INTS:[I

    iput-object v0, p0, Lorg/apache/lucene/util/IntsRef;->ints:[I

    .line 39
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "capacity"    # I

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    new-array v0, p1, [I

    iput-object v0, p0, Lorg/apache/lucene/util/IntsRef;->ints:[I

    .line 47
    return-void
.end method

.method public constructor <init>([III)V
    .locals 1
    .param p1, "ints"    # [I
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p1, p0, Lorg/apache/lucene/util/IntsRef;->ints:[I

    .line 54
    iput p2, p0, Lorg/apache/lucene/util/IntsRef;->offset:I

    .line 55
    iput p3, p0, Lorg/apache/lucene/util/IntsRef;->length:I

    .line 56
    sget-boolean v0, Lorg/apache/lucene/util/IntsRef;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/util/IntsRef;->isValid()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 57
    :cond_0
    return-void
.end method

.method public static deepCopyOf(Lorg/apache/lucene/util/IntsRef;)Lorg/apache/lucene/util/IntsRef;
    .locals 1
    .param p0, "other"    # Lorg/apache/lucene/util/IntsRef;

    .prologue
    .line 172
    new-instance v0, Lorg/apache/lucene/util/IntsRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/IntsRef;-><init>()V

    .line 173
    .local v0, "clone":Lorg/apache/lucene/util/IntsRef;
    invoke-virtual {v0, p0}, Lorg/apache/lucene/util/IntsRef;->copyInts(Lorg/apache/lucene/util/IntsRef;)V

    .line 174
    return-object v0
.end method


# virtual methods
.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/util/IntsRef;->clone()Lorg/apache/lucene/util/IntsRef;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lorg/apache/lucene/util/IntsRef;
    .locals 4

    .prologue
    .line 61
    new-instance v0, Lorg/apache/lucene/util/IntsRef;

    iget-object v1, p0, Lorg/apache/lucene/util/IntsRef;->ints:[I

    iget v2, p0, Lorg/apache/lucene/util/IntsRef;->offset:I

    iget v3, p0, Lorg/apache/lucene/util/IntsRef;->length:I

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/util/IntsRef;-><init>([III)V

    return-object v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/util/IntsRef;

    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/IntsRef;->compareTo(Lorg/apache/lucene/util/IntsRef;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lorg/apache/lucene/util/IntsRef;)I
    .locals 11
    .param p1, "other"    # Lorg/apache/lucene/util/IntsRef;

    .prologue
    .line 105
    if-ne p0, p1, :cond_0

    const/4 v9, 0x0

    .line 125
    :goto_0
    return v9

    .line 107
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/util/IntsRef;->ints:[I

    .line 108
    .local v1, "aInts":[I
    iget v3, p0, Lorg/apache/lucene/util/IntsRef;->offset:I

    .line 109
    .local v3, "aUpto":I
    iget-object v6, p1, Lorg/apache/lucene/util/IntsRef;->ints:[I

    .line 110
    .local v6, "bInts":[I
    iget v7, p1, Lorg/apache/lucene/util/IntsRef;->offset:I

    .line 112
    .local v7, "bUpto":I
    iget v9, p0, Lorg/apache/lucene/util/IntsRef;->length:I

    iget v10, p1, Lorg/apache/lucene/util/IntsRef;->length:I

    invoke-static {v9, v10}, Ljava/lang/Math;->min(II)I

    move-result v9

    add-int v2, v3, v9

    .local v2, "aStop":I
    move v8, v7

    .end local v7    # "bUpto":I
    .local v8, "bUpto":I
    move v4, v3

    .line 114
    .end local v3    # "aUpto":I
    .local v4, "aUpto":I
    :goto_1
    if-lt v4, v2, :cond_1

    .line 125
    iget v9, p0, Lorg/apache/lucene/util/IntsRef;->length:I

    iget v10, p1, Lorg/apache/lucene/util/IntsRef;->length:I

    sub-int/2addr v9, v10

    goto :goto_0

    .line 115
    :cond_1
    add-int/lit8 v3, v4, 0x1

    .end local v4    # "aUpto":I
    .restart local v3    # "aUpto":I
    aget v0, v1, v4

    .line 116
    .local v0, "aInt":I
    add-int/lit8 v7, v8, 0x1

    .end local v8    # "bUpto":I
    .restart local v7    # "bUpto":I
    aget v5, v6, v8

    .line 117
    .local v5, "bInt":I
    if-le v0, v5, :cond_2

    .line 118
    const/4 v9, 0x1

    goto :goto_0

    .line 119
    :cond_2
    if-ge v0, v5, :cond_3

    .line 120
    const/4 v9, -0x1

    goto :goto_0

    :cond_3
    move v8, v7

    .end local v7    # "bUpto":I
    .restart local v8    # "bUpto":I
    move v4, v3

    .end local v3    # "aUpto":I
    .restart local v4    # "aUpto":I
    goto :goto_1
.end method

.method public copyInts(Lorg/apache/lucene/util/IntsRef;)V
    .locals 5
    .param p1, "other"    # Lorg/apache/lucene/util/IntsRef;

    .prologue
    .line 129
    iget-object v0, p0, Lorg/apache/lucene/util/IntsRef;->ints:[I

    array-length v0, v0

    iget v1, p0, Lorg/apache/lucene/util/IntsRef;->offset:I

    sub-int/2addr v0, v1

    iget v1, p1, Lorg/apache/lucene/util/IntsRef;->length:I

    if-ge v0, v1, :cond_0

    .line 130
    iget v0, p1, Lorg/apache/lucene/util/IntsRef;->length:I

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/apache/lucene/util/IntsRef;->ints:[I

    .line 131
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/util/IntsRef;->offset:I

    .line 133
    :cond_0
    iget-object v0, p1, Lorg/apache/lucene/util/IntsRef;->ints:[I

    iget v1, p1, Lorg/apache/lucene/util/IntsRef;->offset:I

    iget-object v2, p0, Lorg/apache/lucene/util/IntsRef;->ints:[I

    iget v3, p0, Lorg/apache/lucene/util/IntsRef;->offset:I

    iget v4, p1, Lorg/apache/lucene/util/IntsRef;->length:I

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 134
    iget v0, p1, Lorg/apache/lucene/util/IntsRef;->length:I

    iput v0, p0, Lorg/apache/lucene/util/IntsRef;->length:I

    .line 135
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x0

    .line 77
    if-nez p1, :cond_1

    .line 83
    .end local p1    # "other":Ljava/lang/Object;
    :cond_0
    :goto_0
    return v0

    .line 80
    .restart local p1    # "other":Ljava/lang/Object;
    :cond_1
    instance-of v1, p1, Lorg/apache/lucene/util/IntsRef;

    if-eqz v1, :cond_0

    .line 81
    check-cast p1, Lorg/apache/lucene/util/IntsRef;

    .end local p1    # "other":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/IntsRef;->intsEquals(Lorg/apache/lucene/util/IntsRef;)Z

    move-result v0

    goto :goto_0
.end method

.method public grow(I)V
    .locals 1
    .param p1, "newLength"    # I

    .prologue
    .line 143
    sget-boolean v0, Lorg/apache/lucene/util/IntsRef;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/util/IntsRef;->offset:I

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 144
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/util/IntsRef;->ints:[I

    array-length v0, v0

    if-ge v0, p1, :cond_1

    .line 145
    iget-object v0, p0, Lorg/apache/lucene/util/IntsRef;->ints:[I

    invoke-static {v0, p1}, Lorg/apache/lucene/util/ArrayUtil;->grow([II)[I

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/util/IntsRef;->ints:[I

    .line 147
    :cond_1
    return-void
.end method

.method public hashCode()I
    .locals 6

    .prologue
    .line 66
    const/16 v2, 0x1f

    .line 67
    .local v2, "prime":I
    const/4 v3, 0x0

    .line 68
    .local v3, "result":I
    iget v4, p0, Lorg/apache/lucene/util/IntsRef;->offset:I

    iget v5, p0, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int v0, v4, v5

    .line 69
    .local v0, "end":I
    iget v1, p0, Lorg/apache/lucene/util/IntsRef;->offset:I

    .local v1, "i":I
    :goto_0
    if-lt v1, v0, :cond_0

    .line 72
    return v3

    .line 70
    :cond_0
    mul-int/lit8 v4, v3, 0x1f

    iget-object v5, p0, Lorg/apache/lucene/util/IntsRef;->ints:[I

    aget v5, v5, v1

    add-int v3, v4, v5

    .line 69
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public intsEquals(Lorg/apache/lucene/util/IntsRef;)Z
    .locals 7
    .param p1, "other"    # Lorg/apache/lucene/util/IntsRef;

    .prologue
    const/4 v4, 0x0

    .line 87
    iget v5, p0, Lorg/apache/lucene/util/IntsRef;->length:I

    iget v6, p1, Lorg/apache/lucene/util/IntsRef;->length:I

    if-ne v5, v6, :cond_0

    .line 88
    iget v2, p1, Lorg/apache/lucene/util/IntsRef;->offset:I

    .line 89
    .local v2, "otherUpto":I
    iget-object v1, p1, Lorg/apache/lucene/util/IntsRef;->ints:[I

    .line 90
    .local v1, "otherInts":[I
    iget v5, p0, Lorg/apache/lucene/util/IntsRef;->offset:I

    iget v6, p0, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int v0, v5, v6

    .line 91
    .local v0, "end":I
    iget v3, p0, Lorg/apache/lucene/util/IntsRef;->offset:I

    .local v3, "upto":I
    :goto_0
    if-lt v3, v0, :cond_1

    .line 96
    const/4 v4, 0x1

    .line 98
    .end local v0    # "end":I
    .end local v1    # "otherInts":[I
    .end local v2    # "otherUpto":I
    .end local v3    # "upto":I
    :cond_0
    return v4

    .line 92
    .restart local v0    # "end":I
    .restart local v1    # "otherInts":[I
    .restart local v2    # "otherUpto":I
    .restart local v3    # "upto":I
    :cond_1
    iget-object v5, p0, Lorg/apache/lucene/util/IntsRef;->ints:[I

    aget v5, v5, v3

    aget v6, v1, v2

    if-ne v5, v6, :cond_0

    .line 91
    add-int/lit8 v3, v3, 0x1

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public isValid()Z
    .locals 3

    .prologue
    .line 182
    iget-object v0, p0, Lorg/apache/lucene/util/IntsRef;->ints:[I

    if-nez v0, :cond_0

    .line 183
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "ints is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 185
    :cond_0
    iget v0, p0, Lorg/apache/lucene/util/IntsRef;->length:I

    if-gez v0, :cond_1

    .line 186
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "length is negative: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/lucene/util/IntsRef;->length:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 188
    :cond_1
    iget v0, p0, Lorg/apache/lucene/util/IntsRef;->length:I

    iget-object v1, p0, Lorg/apache/lucene/util/IntsRef;->ints:[I

    array-length v1, v1

    if-le v0, v1, :cond_2

    .line 189
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "length is out of bounds: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/lucene/util/IntsRef;->length:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",ints.length="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/util/IntsRef;->ints:[I

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 191
    :cond_2
    iget v0, p0, Lorg/apache/lucene/util/IntsRef;->offset:I

    if-gez v0, :cond_3

    .line 192
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "offset is negative: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/lucene/util/IntsRef;->offset:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 194
    :cond_3
    iget v0, p0, Lorg/apache/lucene/util/IntsRef;->offset:I

    iget-object v1, p0, Lorg/apache/lucene/util/IntsRef;->ints:[I

    array-length v1, v1

    if-le v0, v1, :cond_4

    .line 195
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "offset out of bounds: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/lucene/util/IntsRef;->offset:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",ints.length="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/util/IntsRef;->ints:[I

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 197
    :cond_4
    iget v0, p0, Lorg/apache/lucene/util/IntsRef;->offset:I

    iget v1, p0, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/2addr v0, v1

    if-gez v0, :cond_5

    .line 198
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "offset+length is negative: offset="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/lucene/util/IntsRef;->offset:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",length="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/util/IntsRef;->length:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 200
    :cond_5
    iget v0, p0, Lorg/apache/lucene/util/IntsRef;->offset:I

    iget v1, p0, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/2addr v0, v1

    iget-object v1, p0, Lorg/apache/lucene/util/IntsRef;->ints:[I

    array-length v1, v1

    if-le v0, v1, :cond_6

    .line 201
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "offset+length out of bounds: offset="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/lucene/util/IntsRef;->offset:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",length="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/util/IntsRef;->length:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",ints.length="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/util/IntsRef;->ints:[I

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 203
    :cond_6
    const/4 v0, 0x1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 151
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 152
    .local v2, "sb":Ljava/lang/StringBuilder;
    const/16 v3, 0x5b

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 153
    iget v3, p0, Lorg/apache/lucene/util/IntsRef;->offset:I

    iget v4, p0, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int v0, v3, v4

    .line 154
    .local v0, "end":I
    iget v1, p0, Lorg/apache/lucene/util/IntsRef;->offset:I

    .local v1, "i":I
    :goto_0
    if-lt v1, v0, :cond_0

    .line 160
    const/16 v3, 0x5d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 161
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 155
    :cond_0
    iget v3, p0, Lorg/apache/lucene/util/IntsRef;->offset:I

    if-le v1, v3, :cond_1

    .line 156
    const/16 v3, 0x20

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 158
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/util/IntsRef;->ints:[I

    aget v3, v3, v1

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 154
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
