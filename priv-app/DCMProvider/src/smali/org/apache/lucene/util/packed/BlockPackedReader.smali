.class public final Lorg/apache/lucene/util/packed/BlockPackedReader;
.super Ljava/lang/Object;
.source "BlockPackedReader.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final blockMask:I

.field private final blockShift:I

.field private final minValues:[J

.field private final subReaders:[Lorg/apache/lucene/util/packed/PackedInts$Reader;

.field private final valueCount:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lorg/apache/lucene/util/packed/BlockPackedReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/packed/BlockPackedReader;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/store/IndexInput;IIJZ)V
    .locals 18
    .param p1, "in"    # Lorg/apache/lucene/store/IndexInput;
    .param p2, "packedIntsVersion"    # I
    .param p3, "blockSize"    # I
    .param p4, "valueCount"    # J
    .param p6, "direct"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 42
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 43
    invoke-static/range {p3 .. p3}, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->checkBlockSize(I)V

    .line 44
    move-wide/from16 v0, p4

    move-object/from16 v2, p0

    iput-wide v0, v2, Lorg/apache/lucene/util/packed/BlockPackedReader;->valueCount:J

    .line 45
    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->numberOfTrailingZeros(I)I

    move-result v12

    move-object/from16 v0, p0

    iput v12, v0, Lorg/apache/lucene/util/packed/BlockPackedReader;->blockShift:I

    .line 46
    add-int/lit8 v12, p3, -0x1

    move-object/from16 v0, p0

    iput v12, v0, Lorg/apache/lucene/util/packed/BlockPackedReader;->blockMask:I

    .line 47
    move/from16 v0, p3

    int-to-long v12, v0

    div-long v12, p4, v12

    long-to-int v13, v12

    move/from16 v0, p3

    int-to-long v14, v0

    rem-long v14, p4, v14

    const-wide/16 v16, 0x0

    cmp-long v12, v14, v16

    if-nez v12, :cond_0

    const/4 v12, 0x0

    :goto_0
    add-int v7, v13, v12

    .line 48
    .local v7, "numBlocks":I
    int-to-long v12, v7

    move/from16 v0, p3

    int-to-long v14, v0

    mul-long/2addr v12, v14

    cmp-long v12, v12, p4

    if-gez v12, :cond_1

    .line 49
    new-instance v12, Ljava/lang/IllegalArgumentException;

    const-string v13, "valueCount is too large for this block size"

    invoke-direct {v12, v13}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v12

    .line 47
    .end local v7    # "numBlocks":I
    :cond_0
    const/4 v12, 0x1

    goto :goto_0

    .line 51
    .restart local v7    # "numBlocks":I
    :cond_1
    const/4 v6, 0x0

    .line 52
    .local v6, "minValues":[J
    new-array v12, v7, [Lorg/apache/lucene/util/packed/PackedInts$Reader;

    move-object/from16 v0, p0

    iput-object v12, v0, Lorg/apache/lucene/util/packed/BlockPackedReader;->subReaders:[Lorg/apache/lucene/util/packed/PackedInts$Reader;

    .line 53
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    if-lt v5, v7, :cond_2

    .line 78
    move-object/from16 v0, p0

    iput-object v6, v0, Lorg/apache/lucene/util/packed/BlockPackedReader;->minValues:[J

    .line 79
    return-void

    .line 54
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/store/IndexInput;->readByte()B

    move-result v12

    and-int/lit16 v11, v12, 0xff

    .line 55
    .local v11, "token":I
    ushr-int/lit8 v4, v11, 0x1

    .line 56
    .local v4, "bitsPerValue":I
    const/16 v12, 0x40

    if-le v4, v12, :cond_3

    .line 57
    new-instance v12, Ljava/io/IOException;

    const-string v13, "Corrupted"

    invoke-direct {v12, v13}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v12

    .line 59
    :cond_3
    and-int/lit8 v12, v11, 0x1

    if-nez v12, :cond_5

    .line 60
    if-nez v6, :cond_4

    .line 61
    new-array v6, v7, [J

    .line 63
    :cond_4
    const-wide/16 v12, 0x1

    invoke-static/range {p1 .. p1}, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->readVLong(Lorg/apache/lucene/store/DataInput;)J

    move-result-wide v14

    add-long/2addr v12, v14

    invoke-static {v12, v13}, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->zigZagDecode(J)J

    move-result-wide v12

    aput-wide v12, v6, v5

    .line 65
    :cond_5
    if-nez v4, :cond_6

    .line 66
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/util/packed/BlockPackedReader;->subReaders:[Lorg/apache/lucene/util/packed/PackedInts$Reader;

    new-instance v13, Lorg/apache/lucene/util/packed/PackedInts$NullReader;

    move/from16 v0, p3

    invoke-direct {v13, v0}, Lorg/apache/lucene/util/packed/PackedInts$NullReader;-><init>(I)V

    aput-object v13, v12, v5

    .line 53
    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 68
    :cond_6
    move/from16 v0, p3

    int-to-long v12, v0

    int-to-long v14, v5

    move/from16 v0, p3

    int-to-long v0, v0

    move-wide/from16 v16, v0

    mul-long v14, v14, v16

    sub-long v14, p4, v14

    invoke-static {v12, v13, v14, v15}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v12

    long-to-int v10, v12

    .line 69
    .local v10, "size":I
    if-eqz p6, :cond_7

    .line 70
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v8

    .line 71
    .local v8, "pointer":J
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/util/packed/BlockPackedReader;->subReaders:[Lorg/apache/lucene/util/packed/PackedInts$Reader;

    sget-object v13, Lorg/apache/lucene/util/packed/PackedInts$Format;->PACKED:Lorg/apache/lucene/util/packed/PackedInts$Format;

    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-static {v0, v13, v1, v10, v4}, Lorg/apache/lucene/util/packed/PackedInts;->getDirectReaderNoHeader(Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/util/packed/PackedInts$Format;III)Lorg/apache/lucene/util/packed/PackedInts$Reader;

    move-result-object v13

    aput-object v13, v12, v5

    .line 72
    sget-object v12, Lorg/apache/lucene/util/packed/PackedInts$Format;->PACKED:Lorg/apache/lucene/util/packed/PackedInts$Format;

    move/from16 v0, p2

    invoke-virtual {v12, v0, v10, v4}, Lorg/apache/lucene/util/packed/PackedInts$Format;->byteCount(III)J

    move-result-wide v12

    add-long/2addr v12, v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v13}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    goto :goto_2

    .line 74
    .end local v8    # "pointer":J
    :cond_7
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/util/packed/BlockPackedReader;->subReaders:[Lorg/apache/lucene/util/packed/PackedInts$Reader;

    sget-object v13, Lorg/apache/lucene/util/packed/PackedInts$Format;->PACKED:Lorg/apache/lucene/util/packed/PackedInts$Format;

    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-static {v0, v13, v1, v10, v4}, Lorg/apache/lucene/util/packed/PackedInts;->getReaderNoHeader(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/packed/PackedInts$Format;III)Lorg/apache/lucene/util/packed/PackedInts$Reader;

    move-result-object v13

    aput-object v13, v12, v5

    goto :goto_2
.end method


# virtual methods
.method public get(J)J
    .locals 7
    .param p1, "index"    # J

    .prologue
    const-wide/16 v2, 0x0

    .line 83
    sget-boolean v4, Lorg/apache/lucene/util/packed/BlockPackedReader;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    cmp-long v4, p1, v2

    if-ltz v4, :cond_0

    iget-wide v4, p0, Lorg/apache/lucene/util/packed/BlockPackedReader;->valueCount:J

    cmp-long v4, p1, v4

    if-ltz v4, :cond_1

    :cond_0
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 84
    :cond_1
    iget v4, p0, Lorg/apache/lucene/util/packed/BlockPackedReader;->blockShift:I

    ushr-long v4, p1, v4

    long-to-int v0, v4

    .line 85
    .local v0, "block":I
    iget v4, p0, Lorg/apache/lucene/util/packed/BlockPackedReader;->blockMask:I

    int-to-long v4, v4

    and-long/2addr v4, p1

    long-to-int v1, v4

    .line 86
    .local v1, "idx":I
    iget-object v4, p0, Lorg/apache/lucene/util/packed/BlockPackedReader;->minValues:[J

    if-nez v4, :cond_2

    :goto_0
    iget-object v4, p0, Lorg/apache/lucene/util/packed/BlockPackedReader;->subReaders:[Lorg/apache/lucene/util/packed/PackedInts$Reader;

    aget-object v4, v4, v0

    invoke-interface {v4, v1}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I)J

    move-result-wide v4

    add-long/2addr v2, v4

    return-wide v2

    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/util/packed/BlockPackedReader;->minValues:[J

    aget-wide v2, v2, v0

    goto :goto_0
.end method
