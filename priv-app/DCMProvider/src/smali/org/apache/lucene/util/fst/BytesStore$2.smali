.class Lorg/apache/lucene/util/fst/BytesStore$2;
.super Lorg/apache/lucene/util/fst/FST$BytesReader;
.source "BytesStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/util/fst/BytesStore;->getReverseReader(Z)Lorg/apache/lucene/util/fst/FST$BytesReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/util/fst/FST$BytesReader;"
    }
.end annotation


# instance fields
.field private current:[B

.field private nextBuffer:I

.field private nextRead:I

.field final synthetic this$0:Lorg/apache/lucene/util/fst/BytesStore;


# direct methods
.method constructor <init>(Lorg/apache/lucene/util/fst/BytesStore;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1
    iput-object p1, p0, Lorg/apache/lucene/util/fst/BytesStore$2;->this$0:Lorg/apache/lucene/util/fst/BytesStore;

    .line 418
    invoke-direct {p0}, Lorg/apache/lucene/util/fst/FST$BytesReader;-><init>()V

    .line 419
    # getter for: Lorg/apache/lucene/util/fst/BytesStore;->blocks:Ljava/util/List;
    invoke-static {p1}, Lorg/apache/lucene/util/fst/BytesStore;->access$1(Lorg/apache/lucene/util/fst/BytesStore;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lorg/apache/lucene/util/fst/BytesStore$2;->current:[B

    .line 420
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/util/fst/BytesStore$2;->nextBuffer:I

    .line 421
    iput v1, p0, Lorg/apache/lucene/util/fst/BytesStore$2;->nextRead:I

    return-void

    .line 419
    :cond_0
    # getter for: Lorg/apache/lucene/util/fst/BytesStore;->blocks:Ljava/util/List;
    invoke-static {p1}, Lorg/apache/lucene/util/fst/BytesStore;->access$1(Lorg/apache/lucene/util/fst/BytesStore;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    goto :goto_0
.end method


# virtual methods
.method public getPosition()J
    .locals 4

    .prologue
    .line 446
    iget v0, p0, Lorg/apache/lucene/util/fst/BytesStore$2;->nextBuffer:I

    int-to-long v0, v0

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iget-object v2, p0, Lorg/apache/lucene/util/fst/BytesStore$2;->this$0:Lorg/apache/lucene/util/fst/BytesStore;

    # getter for: Lorg/apache/lucene/util/fst/BytesStore;->blockSize:I
    invoke-static {v2}, Lorg/apache/lucene/util/fst/BytesStore;->access$0(Lorg/apache/lucene/util/fst/BytesStore;)I

    move-result v2

    int-to-long v2, v2

    mul-long/2addr v0, v2

    iget v2, p0, Lorg/apache/lucene/util/fst/BytesStore$2;->nextRead:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public readByte()B
    .locals 3

    .prologue
    .line 425
    iget v0, p0, Lorg/apache/lucene/util/fst/BytesStore$2;->nextRead:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 426
    iget-object v0, p0, Lorg/apache/lucene/util/fst/BytesStore$2;->this$0:Lorg/apache/lucene/util/fst/BytesStore;

    # getter for: Lorg/apache/lucene/util/fst/BytesStore;->blocks:Ljava/util/List;
    invoke-static {v0}, Lorg/apache/lucene/util/fst/BytesStore;->access$1(Lorg/apache/lucene/util/fst/BytesStore;)Ljava/util/List;

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/util/fst/BytesStore$2;->nextBuffer:I

    add-int/lit8 v2, v1, -0x1

    iput v2, p0, Lorg/apache/lucene/util/fst/BytesStore$2;->nextBuffer:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iput-object v0, p0, Lorg/apache/lucene/util/fst/BytesStore$2;->current:[B

    .line 427
    iget-object v0, p0, Lorg/apache/lucene/util/fst/BytesStore$2;->this$0:Lorg/apache/lucene/util/fst/BytesStore;

    # getter for: Lorg/apache/lucene/util/fst/BytesStore;->blockSize:I
    invoke-static {v0}, Lorg/apache/lucene/util/fst/BytesStore;->access$0(Lorg/apache/lucene/util/fst/BytesStore;)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/util/fst/BytesStore$2;->nextRead:I

    .line 429
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/util/fst/BytesStore$2;->current:[B

    iget v1, p0, Lorg/apache/lucene/util/fst/BytesStore$2;->nextRead:I

    add-int/lit8 v2, v1, -0x1

    iput v2, p0, Lorg/apache/lucene/util/fst/BytesStore$2;->nextRead:I

    aget-byte v0, v0, v1

    return v0
.end method

.method public readBytes([BII)V
    .locals 3
    .param p1, "b"    # [B
    .param p2, "offset"    # I
    .param p3, "len"    # I

    .prologue
    .line 439
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, p3, :cond_0

    .line 442
    return-void

    .line 440
    :cond_0
    add-int v1, p2, v0

    invoke-virtual {p0}, Lorg/apache/lucene/util/fst/BytesStore$2;->readByte()B

    move-result v2

    aput-byte v2, p1, v1

    .line 439
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public reversed()Z
    .locals 1

    .prologue
    .line 464
    const/4 v0, 0x1

    return v0
.end method

.method public setPosition(J)V
    .locals 7
    .param p1, "pos"    # J

    .prologue
    .line 455
    iget-object v1, p0, Lorg/apache/lucene/util/fst/BytesStore$2;->this$0:Lorg/apache/lucene/util/fst/BytesStore;

    # getter for: Lorg/apache/lucene/util/fst/BytesStore;->blockBits:I
    invoke-static {v1}, Lorg/apache/lucene/util/fst/BytesStore;->access$2(Lorg/apache/lucene/util/fst/BytesStore;)I

    move-result v1

    shr-long v2, p1, v1

    long-to-int v0, v2

    .line 456
    .local v0, "bufferIndex":I
    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lorg/apache/lucene/util/fst/BytesStore$2;->nextBuffer:I

    .line 457
    iget-object v1, p0, Lorg/apache/lucene/util/fst/BytesStore$2;->this$0:Lorg/apache/lucene/util/fst/BytesStore;

    # getter for: Lorg/apache/lucene/util/fst/BytesStore;->blocks:Ljava/util/List;
    invoke-static {v1}, Lorg/apache/lucene/util/fst/BytesStore;->access$1(Lorg/apache/lucene/util/fst/BytesStore;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    iput-object v1, p0, Lorg/apache/lucene/util/fst/BytesStore$2;->current:[B

    .line 458
    iget-object v1, p0, Lorg/apache/lucene/util/fst/BytesStore$2;->this$0:Lorg/apache/lucene/util/fst/BytesStore;

    # getter for: Lorg/apache/lucene/util/fst/BytesStore;->blockMask:I
    invoke-static {v1}, Lorg/apache/lucene/util/fst/BytesStore;->access$3(Lorg/apache/lucene/util/fst/BytesStore;)I

    move-result v1

    int-to-long v2, v1

    and-long/2addr v2, p1

    long-to-int v1, v2

    iput v1, p0, Lorg/apache/lucene/util/fst/BytesStore$2;->nextRead:I

    .line 459
    sget-boolean v1, Lorg/apache/lucene/util/fst/BytesStore;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/util/fst/BytesStore$2;->getPosition()J

    move-result-wide v2

    cmp-long v1, v2, p1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "pos="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " getPos()="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lorg/apache/lucene/util/fst/BytesStore$2;->getPosition()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 460
    :cond_0
    return-void
.end method

.method public skipBytes(I)V
    .locals 4
    .param p1, "count"    # I

    .prologue
    .line 434
    invoke-virtual {p0}, Lorg/apache/lucene/util/fst/BytesStore$2;->getPosition()J

    move-result-wide v0

    int-to-long v2, p1

    sub-long/2addr v0, v2

    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/util/fst/BytesStore$2;->setPosition(J)V

    .line 435
    return-void
.end method
