.class public final Lorg/apache/lucene/util/packed/BlockPackedWriter;
.super Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;
.source "BlockPackedWriter.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    const-class v0, Lorg/apache/lucene/util/packed/BlockPackedWriter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/packed/BlockPackedWriter;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/store/DataOutput;I)V
    .locals 0
    .param p1, "out"    # Lorg/apache/lucene/store/DataOutput;
    .param p2, "blockSize"    # I

    .prologue
    .line 65
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;-><init>(Lorg/apache/lucene/store/DataOutput;I)V

    .line 66
    return-void
.end method


# virtual methods
.method public bridge synthetic add(J)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-super {p0, p1, p2}, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->add(J)V

    return-void
.end method

.method public bridge synthetic finish()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-super {p0}, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->finish()V

    return-void
.end method

.method protected flush()V
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 69
    sget-boolean v9, Lorg/apache/lucene/util/packed/BlockPackedWriter;->$assertionsDisabled:Z

    if-nez v9, :cond_0

    iget v9, p0, Lorg/apache/lucene/util/packed/BlockPackedWriter;->off:I

    if-gtz v9, :cond_0

    new-instance v9, Ljava/lang/AssertionError;

    invoke-direct {v9}, Ljava/lang/AssertionError;-><init>()V

    throw v9

    .line 70
    :cond_0
    const-wide v6, 0x7fffffffffffffffL

    .local v6, "min":J
    const-wide/high16 v4, -0x8000000000000000L

    .line 71
    .local v4, "max":J
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v9, p0, Lorg/apache/lucene/util/packed/BlockPackedWriter;->off:I

    if-lt v1, v9, :cond_5

    .line 76
    sub-long v2, v4, v6

    .line 77
    .local v2, "delta":J
    const-wide/16 v10, 0x0

    cmp-long v9, v2, v10

    if-gez v9, :cond_6

    const/16 v0, 0x40

    .line 78
    .local v0, "bitsRequired":I
    :goto_1
    const/16 v9, 0x40

    if-ne v0, v9, :cond_8

    .line 80
    const-wide/16 v6, 0x0

    .line 86
    :cond_1
    :goto_2
    shl-int/lit8 v10, v0, 0x1

    const-wide/16 v12, 0x0

    cmp-long v9, v6, v12

    if-nez v9, :cond_9

    const/4 v9, 0x1

    :goto_3
    or-int v8, v10, v9

    .line 87
    .local v8, "token":I
    iget-object v9, p0, Lorg/apache/lucene/util/packed/BlockPackedWriter;->out:Lorg/apache/lucene/store/DataOutput;

    int-to-byte v10, v8

    invoke-virtual {v9, v10}, Lorg/apache/lucene/store/DataOutput;->writeByte(B)V

    .line 89
    const-wide/16 v10, 0x0

    cmp-long v9, v6, v10

    if-eqz v9, :cond_2

    .line 90
    iget-object v9, p0, Lorg/apache/lucene/util/packed/BlockPackedWriter;->out:Lorg/apache/lucene/store/DataOutput;

    invoke-static {v6, v7}, Lorg/apache/lucene/util/packed/BlockPackedWriter;->zigZagEncode(J)J

    move-result-wide v10

    const-wide/16 v12, 0x1

    sub-long/2addr v10, v12

    invoke-static {v9, v10, v11}, Lorg/apache/lucene/util/packed/BlockPackedWriter;->writeVLong(Lorg/apache/lucene/store/DataOutput;J)V

    .line 93
    :cond_2
    if-lez v0, :cond_4

    .line 94
    const-wide/16 v10, 0x0

    cmp-long v9, v6, v10

    if-eqz v9, :cond_3

    .line 95
    const/4 v1, 0x0

    :goto_4
    iget v9, p0, Lorg/apache/lucene/util/packed/BlockPackedWriter;->off:I

    if-lt v1, v9, :cond_a

    .line 99
    :cond_3
    invoke-virtual {p0, v0}, Lorg/apache/lucene/util/packed/BlockPackedWriter;->writeValues(I)V

    .line 102
    :cond_4
    const/4 v9, 0x0

    iput v9, p0, Lorg/apache/lucene/util/packed/BlockPackedWriter;->off:I

    .line 103
    return-void

    .line 72
    .end local v0    # "bitsRequired":I
    .end local v2    # "delta":J
    .end local v8    # "token":I
    :cond_5
    iget-object v9, p0, Lorg/apache/lucene/util/packed/BlockPackedWriter;->values:[J

    aget-wide v10, v9, v1

    invoke-static {v10, v11, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v6

    .line 73
    iget-object v9, p0, Lorg/apache/lucene/util/packed/BlockPackedWriter;->values:[J

    aget-wide v10, v9, v1

    invoke-static {v10, v11, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    .line 71
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 77
    .restart local v2    # "delta":J
    :cond_6
    const-wide/16 v10, 0x0

    cmp-long v9, v2, v10

    if-nez v9, :cond_7

    const/4 v0, 0x0

    goto :goto_1

    :cond_7
    invoke-static {v2, v3}, Lorg/apache/lucene/util/packed/PackedInts;->bitsRequired(J)I

    move-result v0

    goto :goto_1

    .line 81
    .restart local v0    # "bitsRequired":I
    :cond_8
    const-wide/16 v10, 0x0

    cmp-long v9, v6, v10

    if-lez v9, :cond_1

    .line 83
    const-wide/16 v10, 0x0

    invoke-static {v0}, Lorg/apache/lucene/util/packed/PackedInts;->maxValue(I)J

    move-result-wide v12

    sub-long v12, v4, v12

    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v6

    goto :goto_2

    .line 86
    :cond_9
    const/4 v9, 0x0

    goto :goto_3

    .line 96
    .restart local v8    # "token":I
    :cond_a
    iget-object v9, p0, Lorg/apache/lucene/util/packed/BlockPackedWriter;->values:[J

    aget-wide v10, v9, v1

    sub-long/2addr v10, v6

    aput-wide v10, v9, v1

    .line 95
    add-int/lit8 v1, v1, 0x1

    goto :goto_4
.end method

.method public bridge synthetic ord()J
    .locals 2

    .prologue
    .line 1
    invoke-super {p0}, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->ord()J

    move-result-wide v0

    return-wide v0
.end method

.method public bridge synthetic reset(Lorg/apache/lucene/store/DataOutput;)V
    .locals 0

    .prologue
    .line 1
    invoke-super {p0, p1}, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->reset(Lorg/apache/lucene/store/DataOutput;)V

    return-void
.end method
