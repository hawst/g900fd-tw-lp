.class public final enum Lorg/apache/lucene/util/Version;
.super Ljava/lang/Enum;
.source "Version.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/lucene/util/Version;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lorg/apache/lucene/util/Version;

.field public static final enum LUCENE_30:Lorg/apache/lucene/util/Version;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum LUCENE_31:Lorg/apache/lucene/util/Version;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum LUCENE_32:Lorg/apache/lucene/util/Version;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum LUCENE_33:Lorg/apache/lucene/util/Version;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum LUCENE_34:Lorg/apache/lucene/util/Version;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum LUCENE_35:Lorg/apache/lucene/util/Version;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum LUCENE_36:Lorg/apache/lucene/util/Version;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum LUCENE_40:Lorg/apache/lucene/util/Version;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum LUCENE_41:Lorg/apache/lucene/util/Version;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum LUCENE_42:Lorg/apache/lucene/util/Version;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum LUCENE_43:Lorg/apache/lucene/util/Version;

.field public static final enum LUCENE_CURRENT:Lorg/apache/lucene/util/Version;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 36
    new-instance v0, Lorg/apache/lucene/util/Version;

    const-string v1, "LUCENE_30"

    invoke-direct {v0, v1, v3}, Lorg/apache/lucene/util/Version;-><init>(Ljava/lang/String;I)V

    .line 41
    sput-object v0, Lorg/apache/lucene/util/Version;->LUCENE_30:Lorg/apache/lucene/util/Version;

    .line 43
    new-instance v0, Lorg/apache/lucene/util/Version;

    const-string v1, "LUCENE_31"

    invoke-direct {v0, v1, v4}, Lorg/apache/lucene/util/Version;-><init>(Ljava/lang/String;I)V

    .line 48
    sput-object v0, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    .line 50
    new-instance v0, Lorg/apache/lucene/util/Version;

    const-string v1, "LUCENE_32"

    invoke-direct {v0, v1, v5}, Lorg/apache/lucene/util/Version;-><init>(Ljava/lang/String;I)V

    .line 55
    sput-object v0, Lorg/apache/lucene/util/Version;->LUCENE_32:Lorg/apache/lucene/util/Version;

    .line 57
    new-instance v0, Lorg/apache/lucene/util/Version;

    const-string v1, "LUCENE_33"

    invoke-direct {v0, v1, v6}, Lorg/apache/lucene/util/Version;-><init>(Ljava/lang/String;I)V

    .line 62
    sput-object v0, Lorg/apache/lucene/util/Version;->LUCENE_33:Lorg/apache/lucene/util/Version;

    .line 64
    new-instance v0, Lorg/apache/lucene/util/Version;

    const-string v1, "LUCENE_34"

    invoke-direct {v0, v1, v7}, Lorg/apache/lucene/util/Version;-><init>(Ljava/lang/String;I)V

    .line 69
    sput-object v0, Lorg/apache/lucene/util/Version;->LUCENE_34:Lorg/apache/lucene/util/Version;

    .line 71
    new-instance v0, Lorg/apache/lucene/util/Version;

    const-string v1, "LUCENE_35"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/util/Version;-><init>(Ljava/lang/String;I)V

    .line 76
    sput-object v0, Lorg/apache/lucene/util/Version;->LUCENE_35:Lorg/apache/lucene/util/Version;

    .line 78
    new-instance v0, Lorg/apache/lucene/util/Version;

    const-string v1, "LUCENE_36"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/util/Version;-><init>(Ljava/lang/String;I)V

    .line 83
    sput-object v0, Lorg/apache/lucene/util/Version;->LUCENE_36:Lorg/apache/lucene/util/Version;

    .line 85
    new-instance v0, Lorg/apache/lucene/util/Version;

    const-string v1, "LUCENE_40"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/util/Version;-><init>(Ljava/lang/String;I)V

    .line 90
    sput-object v0, Lorg/apache/lucene/util/Version;->LUCENE_40:Lorg/apache/lucene/util/Version;

    .line 92
    new-instance v0, Lorg/apache/lucene/util/Version;

    const-string v1, "LUCENE_41"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/util/Version;-><init>(Ljava/lang/String;I)V

    .line 96
    sput-object v0, Lorg/apache/lucene/util/Version;->LUCENE_41:Lorg/apache/lucene/util/Version;

    .line 98
    new-instance v0, Lorg/apache/lucene/util/Version;

    const-string v1, "LUCENE_42"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/util/Version;-><init>(Ljava/lang/String;I)V

    .line 102
    sput-object v0, Lorg/apache/lucene/util/Version;->LUCENE_42:Lorg/apache/lucene/util/Version;

    .line 104
    new-instance v0, Lorg/apache/lucene/util/Version;

    const-string v1, "LUCENE_43"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/util/Version;-><init>(Ljava/lang/String;I)V

    .line 109
    sput-object v0, Lorg/apache/lucene/util/Version;->LUCENE_43:Lorg/apache/lucene/util/Version;

    .line 111
    new-instance v0, Lorg/apache/lucene/util/Version;

    const-string v1, "LUCENE_CURRENT"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/util/Version;-><init>(Ljava/lang/String;I)V

    .line 130
    sput-object v0, Lorg/apache/lucene/util/Version;->LUCENE_CURRENT:Lorg/apache/lucene/util/Version;

    .line 35
    const/16 v0, 0xc

    new-array v0, v0, [Lorg/apache/lucene/util/Version;

    sget-object v1, Lorg/apache/lucene/util/Version;->LUCENE_30:Lorg/apache/lucene/util/Version;

    aput-object v1, v0, v3

    sget-object v1, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    aput-object v1, v0, v4

    sget-object v1, Lorg/apache/lucene/util/Version;->LUCENE_32:Lorg/apache/lucene/util/Version;

    aput-object v1, v0, v5

    sget-object v1, Lorg/apache/lucene/util/Version;->LUCENE_33:Lorg/apache/lucene/util/Version;

    aput-object v1, v0, v6

    sget-object v1, Lorg/apache/lucene/util/Version;->LUCENE_34:Lorg/apache/lucene/util/Version;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lorg/apache/lucene/util/Version;->LUCENE_35:Lorg/apache/lucene/util/Version;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lorg/apache/lucene/util/Version;->LUCENE_36:Lorg/apache/lucene/util/Version;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lorg/apache/lucene/util/Version;->LUCENE_40:Lorg/apache/lucene/util/Version;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lorg/apache/lucene/util/Version;->LUCENE_41:Lorg/apache/lucene/util/Version;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lorg/apache/lucene/util/Version;->LUCENE_42:Lorg/apache/lucene/util/Version;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lorg/apache/lucene/util/Version;->LUCENE_43:Lorg/apache/lucene/util/Version;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lorg/apache/lucene/util/Version;->LUCENE_CURRENT:Lorg/apache/lucene/util/Version;

    aput-object v2, v0, v1

    sput-object v0, Lorg/apache/lucene/util/Version;->ENUM$VALUES:[Lorg/apache/lucene/util/Version;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static parseLeniently(Ljava/lang/String;)Lorg/apache/lucene/util/Version;
    .locals 3
    .param p0, "version"    # Ljava/lang/String;

    .prologue
    .line 137
    sget-object v1, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-virtual {p0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 138
    .local v0, "parsedMatchVersion":Ljava/lang/String;
    const-string v1, "^(\\d)\\.(\\d)$"

    const-string v2, "LUCENE_$1$2"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/apache/lucene/util/Version;->valueOf(Ljava/lang/String;)Lorg/apache/lucene/util/Version;

    move-result-object v1

    return-object v1
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/lucene/util/Version;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lorg/apache/lucene/util/Version;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/util/Version;

    return-object v0
.end method

.method public static values()[Lorg/apache/lucene/util/Version;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lorg/apache/lucene/util/Version;->ENUM$VALUES:[Lorg/apache/lucene/util/Version;

    array-length v1, v0

    new-array v2, v1, [Lorg/apache/lucene/util/Version;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public onOrAfter(Lorg/apache/lucene/util/Version;)Z
    .locals 1
    .param p1, "other"    # Lorg/apache/lucene/util/Version;

    .prologue
    .line 133
    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/Version;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
