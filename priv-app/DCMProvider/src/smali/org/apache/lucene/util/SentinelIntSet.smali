.class public Lorg/apache/lucene/util/SentinelIntSet;
.super Ljava/lang/Object;
.source "SentinelIntSet.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field public count:I

.field public final emptyVal:I

.field public keys:[I

.field public rehashCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lorg/apache/lucene/util/SentinelIntSet;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/SentinelIntSet;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(II)V
    .locals 3
    .param p1, "size"    # I
    .param p2, "emptyVal"    # I

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput p2, p0, Lorg/apache/lucene/util/SentinelIntSet;->emptyVal:I

    .line 56
    invoke-static {p1}, Lorg/apache/lucene/util/BitUtil;->nextHighestPowerOfTwo(I)I

    move-result v1

    const/4 v2, 0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 57
    .local v0, "tsize":I
    shr-int/lit8 v1, v0, 0x2

    sub-int v1, v0, v1

    iput v1, p0, Lorg/apache/lucene/util/SentinelIntSet;->rehashCount:I

    .line 58
    iget v1, p0, Lorg/apache/lucene/util/SentinelIntSet;->rehashCount:I

    if-lt p1, v1, :cond_0

    .line 59
    shl-int/lit8 v0, v0, 0x1

    .line 60
    shr-int/lit8 v1, v0, 0x2

    sub-int v1, v0, v1

    iput v1, p0, Lorg/apache/lucene/util/SentinelIntSet;->rehashCount:I

    .line 62
    :cond_0
    new-array v1, v0, [I

    iput-object v1, p0, Lorg/apache/lucene/util/SentinelIntSet;->keys:[I

    .line 63
    if-eqz p2, :cond_1

    .line 64
    invoke-virtual {p0}, Lorg/apache/lucene/util/SentinelIntSet;->clear()V

    .line 65
    :cond_1
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 2

    .prologue
    .line 68
    iget-object v0, p0, Lorg/apache/lucene/util/SentinelIntSet;->keys:[I

    iget v1, p0, Lorg/apache/lucene/util/SentinelIntSet;->emptyVal:I

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 69
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/util/SentinelIntSet;->count:I

    .line 70
    return-void
.end method

.method public exists(I)Z
    .locals 1
    .param p1, "key"    # I

    .prologue
    .line 114
    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/SentinelIntSet;->find(I)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public find(I)I
    .locals 5
    .param p1, "key"    # I

    .prologue
    .line 98
    sget-boolean v3, Lorg/apache/lucene/util/SentinelIntSet;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    iget v3, p0, Lorg/apache/lucene/util/SentinelIntSet;->emptyVal:I

    if-ne p1, v3, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 99
    :cond_0
    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/SentinelIntSet;->hash(I)I

    move-result v0

    .line 100
    .local v0, "h":I
    iget-object v3, p0, Lorg/apache/lucene/util/SentinelIntSet;->keys:[I

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    and-int v2, v0, v3

    .line 101
    .local v2, "s":I
    iget-object v3, p0, Lorg/apache/lucene/util/SentinelIntSet;->keys:[I

    aget v3, v3, v2

    if-ne v3, p1, :cond_1

    move v3, v2

    .line 108
    :goto_0
    return v3

    .line 102
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/util/SentinelIntSet;->keys:[I

    aget v3, v3, v2

    iget v4, p0, Lorg/apache/lucene/util/SentinelIntSet;->emptyVal:I

    if-ne v3, v4, :cond_2

    neg-int v3, v2

    add-int/lit8 v3, v3, -0x1

    goto :goto_0

    .line 104
    :cond_2
    shr-int/lit8 v3, v0, 0x7

    or-int/lit8 v1, v3, 0x1

    .line 106
    .local v1, "increment":I
    :cond_3
    add-int v3, v2, v1

    iget-object v4, p0, Lorg/apache/lucene/util/SentinelIntSet;->keys:[I

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    and-int v2, v3, v4

    .line 107
    iget-object v3, p0, Lorg/apache/lucene/util/SentinelIntSet;->keys:[I

    aget v3, v3, v2

    if-ne v3, p1, :cond_4

    move v3, v2

    goto :goto_0

    .line 108
    :cond_4
    iget-object v3, p0, Lorg/apache/lucene/util/SentinelIntSet;->keys:[I

    aget v3, v3, v2

    iget v4, p0, Lorg/apache/lucene/util/SentinelIntSet;->emptyVal:I

    if-ne v3, v4, :cond_3

    neg-int v3, v2

    add-int/lit8 v3, v3, -0x1

    goto :goto_0
.end method

.method public getSlot(I)I
    .locals 6
    .param p1, "key"    # I

    .prologue
    .line 84
    sget-boolean v4, Lorg/apache/lucene/util/SentinelIntSet;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    iget v4, p0, Lorg/apache/lucene/util/SentinelIntSet;->emptyVal:I

    if-ne p1, v4, :cond_0

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 85
    :cond_0
    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/SentinelIntSet;->hash(I)I

    move-result v0

    .line 86
    .local v0, "h":I
    iget-object v4, p0, Lorg/apache/lucene/util/SentinelIntSet;->keys:[I

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    and-int v2, v0, v4

    .line 87
    .local v2, "s":I
    iget-object v4, p0, Lorg/apache/lucene/util/SentinelIntSet;->keys:[I

    aget v4, v4, v2

    if-eq v4, p1, :cond_1

    iget-object v4, p0, Lorg/apache/lucene/util/SentinelIntSet;->keys:[I

    aget v4, v4, v2

    iget v5, p0, Lorg/apache/lucene/util/SentinelIntSet;->emptyVal:I

    if-ne v4, v5, :cond_2

    :cond_1
    move v3, v2

    .line 93
    .end local v2    # "s":I
    .local v3, "s":I
    :goto_0
    return v3

    .line 89
    .end local v3    # "s":I
    .restart local v2    # "s":I
    :cond_2
    shr-int/lit8 v4, v0, 0x7

    or-int/lit8 v1, v4, 0x1

    .line 91
    .local v1, "increment":I
    :cond_3
    add-int v4, v2, v1

    iget-object v5, p0, Lorg/apache/lucene/util/SentinelIntSet;->keys:[I

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    and-int v2, v4, v5

    .line 92
    iget-object v4, p0, Lorg/apache/lucene/util/SentinelIntSet;->keys:[I

    aget v4, v4, v2

    if-eq v4, p1, :cond_4

    iget-object v4, p0, Lorg/apache/lucene/util/SentinelIntSet;->keys:[I

    aget v4, v4, v2

    iget v5, p0, Lorg/apache/lucene/util/SentinelIntSet;->emptyVal:I

    if-ne v4, v5, :cond_3

    :cond_4
    move v3, v2

    .line 93
    .end local v2    # "s":I
    .restart local v3    # "s":I
    goto :goto_0
.end method

.method public hash(I)I
    .locals 0
    .param p1, "key"    # I

    .prologue
    .line 76
    return p1
.end method

.method public put(I)I
    .locals 3
    .param p1, "key"    # I

    .prologue
    .line 120
    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/SentinelIntSet;->find(I)I

    move-result v0

    .line 121
    .local v0, "s":I
    if-gez v0, :cond_0

    .line 122
    iget v1, p0, Lorg/apache/lucene/util/SentinelIntSet;->count:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/util/SentinelIntSet;->count:I

    .line 123
    iget v1, p0, Lorg/apache/lucene/util/SentinelIntSet;->count:I

    iget v2, p0, Lorg/apache/lucene/util/SentinelIntSet;->rehashCount:I

    if-lt v1, v2, :cond_1

    .line 124
    invoke-virtual {p0}, Lorg/apache/lucene/util/SentinelIntSet;->rehash()V

    .line 125
    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/SentinelIntSet;->getSlot(I)I

    move-result v0

    .line 129
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/util/SentinelIntSet;->keys:[I

    aput p1, v1, v0

    .line 131
    :cond_0
    return v0

    .line 127
    :cond_1
    neg-int v1, v0

    add-int/lit8 v0, v1, -0x1

    goto :goto_0
.end method

.method public rehash()V
    .locals 7

    .prologue
    .line 136
    iget-object v4, p0, Lorg/apache/lucene/util/SentinelIntSet;->keys:[I

    array-length v4, v4

    shl-int/lit8 v1, v4, 0x1

    .line 137
    .local v1, "newSize":I
    iget-object v3, p0, Lorg/apache/lucene/util/SentinelIntSet;->keys:[I

    .line 138
    .local v3, "oldKeys":[I
    new-array v4, v1, [I

    iput-object v4, p0, Lorg/apache/lucene/util/SentinelIntSet;->keys:[I

    .line 139
    iget v4, p0, Lorg/apache/lucene/util/SentinelIntSet;->emptyVal:I

    if-eqz v4, :cond_0

    iget-object v4, p0, Lorg/apache/lucene/util/SentinelIntSet;->keys:[I

    iget v5, p0, Lorg/apache/lucene/util/SentinelIntSet;->emptyVal:I

    invoke-static {v4, v5}, Ljava/util/Arrays;->fill([II)V

    .line 141
    :cond_0
    array-length v5, v3

    const/4 v4, 0x0

    :goto_0
    if-lt v4, v5, :cond_1

    .line 146
    shr-int/lit8 v4, v1, 0x2

    sub-int v4, v1, v4

    iput v4, p0, Lorg/apache/lucene/util/SentinelIntSet;->rehashCount:I

    .line 147
    return-void

    .line 141
    :cond_1
    aget v0, v3, v4

    .line 142
    .local v0, "key":I
    iget v6, p0, Lorg/apache/lucene/util/SentinelIntSet;->emptyVal:I

    if-ne v0, v6, :cond_2

    .line 141
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 143
    :cond_2
    invoke-virtual {p0, v0}, Lorg/apache/lucene/util/SentinelIntSet;->getSlot(I)I

    move-result v2

    .line 144
    .local v2, "newSlot":I
    iget-object v6, p0, Lorg/apache/lucene/util/SentinelIntSet;->keys:[I

    aput v0, v6, v2

    goto :goto_1
.end method

.method public size()I
    .locals 1

    .prologue
    .line 80
    iget v0, p0, Lorg/apache/lucene/util/SentinelIntSet;->count:I

    return v0
.end method
