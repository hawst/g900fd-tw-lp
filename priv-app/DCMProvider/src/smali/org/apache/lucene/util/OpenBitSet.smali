.class public Lorg/apache/lucene/util/OpenBitSet;
.super Lorg/apache/lucene/search/DocIdSet;
.source "OpenBitSet.java"

# interfaces
.implements Ljava/lang/Cloneable;
.implements Lorg/apache/lucene/util/Bits;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field protected bits:[J

.field private numBits:J

.field protected wlen:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 77
    const-class v0, Lorg/apache/lucene/util/OpenBitSet;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/OpenBitSet;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 93
    const-wide/16 v0, 0x40

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/util/OpenBitSet;-><init>(J)V

    .line 94
    return-void
.end method

.method public constructor <init>(J)V
    .locals 1
    .param p1, "numBits"    # J

    .prologue
    .line 86
    invoke-direct {p0}, Lorg/apache/lucene/search/DocIdSet;-><init>()V

    .line 87
    iput-wide p1, p0, Lorg/apache/lucene/util/OpenBitSet;->numBits:J

    .line 88
    invoke-static {p1, p2}, Lorg/apache/lucene/util/OpenBitSet;->bits2words(J)I

    move-result v0

    new-array v0, v0, [J

    iput-object v0, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    .line 89
    iget-object v0, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    array-length v0, v0

    iput v0, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    .line 90
    return-void
.end method

.method public constructor <init>([JI)V
    .locals 2
    .param p1, "bits"    # [J
    .param p2, "numWords"    # I

    .prologue
    .line 109
    invoke-direct {p0}, Lorg/apache/lucene/search/DocIdSet;-><init>()V

    .line 110
    iput-object p1, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    .line 111
    iput p2, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    .line 112
    iget v0, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    mul-int/lit8 v0, v0, 0x40

    int-to-long v0, v0

    iput-wide v0, p0, Lorg/apache/lucene/util/OpenBitSet;->numBits:J

    .line 113
    return-void
.end method

.method public static andNotCount(Lorg/apache/lucene/util/OpenBitSet;Lorg/apache/lucene/util/OpenBitSet;)J
    .locals 7
    .param p0, "a"    # Lorg/apache/lucene/util/OpenBitSet;
    .param p1, "b"    # Lorg/apache/lucene/util/OpenBitSet;

    .prologue
    .line 602
    iget-object v2, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    iget-object v3, p1, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    const/4 v4, 0x0

    iget v5, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v6, p1, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    invoke-static {v2, v3, v4, v5}, Lorg/apache/lucene/util/BitUtil;->pop_andnot([J[JII)J

    move-result-wide v0

    .line 603
    .local v0, "tot":J
    iget v2, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v3, p1, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    if-le v2, v3, :cond_0

    .line 604
    iget-object v2, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    iget v3, p1, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v4, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v5, p1, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    sub-int/2addr v4, v5

    invoke-static {v2, v3, v4}, Lorg/apache/lucene/util/BitUtil;->pop_array([JII)J

    move-result-wide v2

    add-long/2addr v0, v2

    .line 606
    :cond_0
    return-wide v0
.end method

.method public static bits2words(J)I
    .locals 6
    .param p0, "numBits"    # J

    .prologue
    const-wide/16 v4, 0x1

    .line 865
    sub-long v0, p0, v4

    const/4 v2, 0x6

    ushr-long/2addr v0, v2

    add-long/2addr v0, v4

    long-to-int v0, v0

    return v0
.end method

.method public static intersectionCount(Lorg/apache/lucene/util/OpenBitSet;Lorg/apache/lucene/util/OpenBitSet;)J
    .locals 5
    .param p0, "a"    # Lorg/apache/lucene/util/OpenBitSet;
    .param p1, "b"    # Lorg/apache/lucene/util/OpenBitSet;

    .prologue
    .line 581
    iget-object v0, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    iget-object v1, p1, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    const/4 v2, 0x0

    iget v3, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v4, p1, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-static {v0, v1, v2, v3}, Lorg/apache/lucene/util/BitUtil;->pop_intersect([J[JII)J

    move-result-wide v0

    return-wide v0
.end method

.method public static unionCount(Lorg/apache/lucene/util/OpenBitSet;Lorg/apache/lucene/util/OpenBitSet;)J
    .locals 7
    .param p0, "a"    # Lorg/apache/lucene/util/OpenBitSet;
    .param p1, "b"    # Lorg/apache/lucene/util/OpenBitSet;

    .prologue
    .line 588
    iget-object v2, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    iget-object v3, p1, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    const/4 v4, 0x0

    iget v5, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v6, p1, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    invoke-static {v2, v3, v4, v5}, Lorg/apache/lucene/util/BitUtil;->pop_union([J[JII)J

    move-result-wide v0

    .line 589
    .local v0, "tot":J
    iget v2, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v3, p1, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    if-ge v2, v3, :cond_1

    .line 590
    iget-object v2, p1, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    iget v3, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v4, p1, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v5, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    sub-int/2addr v4, v5

    invoke-static {v2, v3, v4}, Lorg/apache/lucene/util/BitUtil;->pop_array([JII)J

    move-result-wide v2

    add-long/2addr v0, v2

    .line 594
    :cond_0
    :goto_0
    return-wide v0

    .line 591
    :cond_1
    iget v2, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v3, p1, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    if-le v2, v3, :cond_0

    .line 592
    iget-object v2, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    iget v3, p1, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v4, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v5, p1, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    sub-int/2addr v4, v5

    invoke-static {v2, v3, v4}, Lorg/apache/lucene/util/BitUtil;->pop_array([JII)J

    move-result-wide v2

    add-long/2addr v0, v2

    goto :goto_0
.end method

.method public static xorCount(Lorg/apache/lucene/util/OpenBitSet;Lorg/apache/lucene/util/OpenBitSet;)J
    .locals 7
    .param p0, "a"    # Lorg/apache/lucene/util/OpenBitSet;
    .param p1, "b"    # Lorg/apache/lucene/util/OpenBitSet;

    .prologue
    .line 613
    iget-object v2, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    iget-object v3, p1, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    const/4 v4, 0x0

    iget v5, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v6, p1, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    invoke-static {v2, v3, v4, v5}, Lorg/apache/lucene/util/BitUtil;->pop_xor([J[JII)J

    move-result-wide v0

    .line 614
    .local v0, "tot":J
    iget v2, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v3, p1, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    if-ge v2, v3, :cond_1

    .line 615
    iget-object v2, p1, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    iget v3, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v4, p1, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v5, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    sub-int/2addr v4, v5

    invoke-static {v2, v3, v4}, Lorg/apache/lucene/util/BitUtil;->pop_array([JII)J

    move-result-wide v2

    add-long/2addr v0, v2

    .line 619
    :cond_0
    :goto_0
    return-wide v0

    .line 616
    :cond_1
    iget v2, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v3, p1, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    if-le v2, v3, :cond_0

    .line 617
    iget-object v2, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    iget v3, p1, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v4, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v5, p1, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    sub-int/2addr v4, v5

    invoke-static {v2, v3, v4}, Lorg/apache/lucene/util/BitUtil;->pop_array([JII)J

    move-result-wide v2

    add-long/2addr v0, v2

    goto :goto_0
.end method


# virtual methods
.method public and(Lorg/apache/lucene/util/OpenBitSet;)V
    .locals 0
    .param p1, "other"    # Lorg/apache/lucene/util/OpenBitSet;

    .prologue
    .line 812
    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/OpenBitSet;->intersect(Lorg/apache/lucene/util/OpenBitSet;)V

    .line 813
    return-void
.end method

.method public andNot(Lorg/apache/lucene/util/OpenBitSet;)V
    .locals 0
    .param p1, "other"    # Lorg/apache/lucene/util/OpenBitSet;

    .prologue
    .line 822
    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/OpenBitSet;->remove(Lorg/apache/lucene/util/OpenBitSet;)V

    .line 823
    return-void
.end method

.method public bits()Lorg/apache/lucene/util/Bits;
    .locals 0

    .prologue
    .line 122
    return-object p0
.end method

.method public capacity()J
    .locals 2

    .prologue
    .line 132
    iget-object v0, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    array-length v0, v0

    shl-int/lit8 v0, v0, 0x6

    int-to-long v0, v0

    return-wide v0
.end method

.method public cardinality()J
    .locals 3

    .prologue
    .line 574
    iget-object v0, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    const/4 v1, 0x0

    iget v2, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    invoke-static {v0, v1, v2}, Lorg/apache/lucene/util/BitUtil;->pop_array([JII)J

    move-result-wide v0

    return-wide v0
.end method

.method public clear(II)V
    .locals 12
    .param p1, "startIndex"    # I
    .param p2, "endIndex"    # I

    .prologue
    const-wide/16 v8, -0x1

    .line 362
    if-gt p2, p1, :cond_1

    .line 390
    :cond_0
    :goto_0
    return-void

    .line 364
    :cond_1
    shr-int/lit8 v4, p1, 0x6

    .line 365
    .local v4, "startWord":I
    iget v5, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    if-ge v4, v5, :cond_0

    .line 369
    add-int/lit8 v5, p2, -0x1

    shr-int/lit8 v0, v5, 0x6

    .line 371
    .local v0, "endWord":I
    shl-long v6, v8, p1

    .line 372
    .local v6, "startmask":J
    neg-int v5, p2

    ushr-long v2, v8, v5

    .line 375
    .local v2, "endmask":J
    xor-long/2addr v6, v8

    .line 376
    xor-long/2addr v2, v8

    .line 378
    if-ne v4, v0, :cond_2

    .line 379
    iget-object v5, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v8, v5, v4

    or-long v10, v6, v2

    and-long/2addr v8, v10

    aput-wide v8, v5, v4

    goto :goto_0

    .line 383
    :cond_2
    iget-object v5, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v8, v5, v4

    and-long/2addr v8, v6

    aput-wide v8, v5, v4

    .line 385
    iget v5, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    invoke-static {v5, v0}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 386
    .local v1, "middle":I
    iget-object v5, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    add-int/lit8 v8, v4, 0x1

    const-wide/16 v10, 0x0

    invoke-static {v5, v8, v1, v10, v11}, Ljava/util/Arrays;->fill([JIIJ)V

    .line 387
    iget v5, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    if-ge v0, v5, :cond_0

    .line 388
    iget-object v5, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v8, v5, v0

    and-long/2addr v8, v2

    aput-wide v8, v5, v0

    goto :goto_0
.end method

.method public clear(J)V
    .locals 11
    .param p1, "index"    # J

    .prologue
    .line 349
    const/4 v4, 0x6

    shr-long v4, p1, v4

    long-to-int v1, v4

    .line 350
    .local v1, "wordNum":I
    iget v4, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    if-lt v1, v4, :cond_0

    .line 354
    :goto_0
    return-void

    .line 351
    :cond_0
    long-to-int v4, p1

    and-int/lit8 v0, v4, 0x3f

    .line 352
    .local v0, "bit":I
    const-wide/16 v4, 0x1

    shl-long v2, v4, v0

    .line 353
    .local v2, "bitmask":J
    iget-object v4, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v6, v4, v1

    const-wide/16 v8, -0x1

    xor-long/2addr v8, v2

    and-long/2addr v6, v8

    aput-wide v6, v4, v1

    goto :goto_0
.end method

.method public clear(JJ)V
    .locals 15
    .param p1, "startIndex"    # J
    .param p3, "endIndex"    # J

    .prologue
    .line 399
    cmp-long v7, p3, p1

    if-gtz v7, :cond_1

    .line 427
    :cond_0
    :goto_0
    return-void

    .line 401
    :cond_1
    const/4 v7, 0x6

    shr-long v10, p1, v7

    long-to-int v6, v10

    .line 402
    .local v6, "startWord":I
    iget v7, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    if-ge v6, v7, :cond_0

    .line 406
    const-wide/16 v10, 0x1

    sub-long v10, p3, v10

    const/4 v7, 0x6

    shr-long/2addr v10, v7

    long-to-int v2, v10

    .line 408
    .local v2, "endWord":I
    const-wide/16 v10, -0x1

    move-wide/from16 v0, p1

    long-to-int v7, v0

    shl-long v8, v10, v7

    .line 409
    .local v8, "startmask":J
    const-wide/16 v10, -0x1

    move-wide/from16 v0, p3

    neg-long v12, v0

    long-to-int v7, v12

    ushr-long v4, v10, v7

    .line 412
    .local v4, "endmask":J
    const-wide/16 v10, -0x1

    xor-long/2addr v8, v10

    .line 413
    const-wide/16 v10, -0x1

    xor-long/2addr v4, v10

    .line 415
    if-ne v6, v2, :cond_2

    .line 416
    iget-object v7, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v10, v7, v6

    or-long v12, v8, v4

    and-long/2addr v10, v12

    aput-wide v10, v7, v6

    goto :goto_0

    .line 420
    :cond_2
    iget-object v7, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v10, v7, v6

    and-long/2addr v10, v8

    aput-wide v10, v7, v6

    .line 422
    iget v7, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    invoke-static {v7, v2}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 423
    .local v3, "middle":I
    iget-object v7, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    add-int/lit8 v10, v6, 0x1

    const-wide/16 v12, 0x0

    invoke-static {v7, v10, v3, v12, v13}, Ljava/util/Arrays;->fill([JIIJ)V

    .line 424
    iget v7, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    if-ge v2, v7, :cond_0

    .line 425
    iget-object v7, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v10, v7, v2

    and-long/2addr v10, v4

    aput-wide v10, v7, v2

    goto :goto_0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/util/OpenBitSet;->clone()Lorg/apache/lucene/util/OpenBitSet;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lorg/apache/lucene/util/OpenBitSet;
    .locals 3

    .prologue
    .line 735
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/util/OpenBitSet;

    .line 736
    .local v1, "obs":Lorg/apache/lucene/util/OpenBitSet;
    iget-object v2, v1, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    invoke-virtual {v2}, [J->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [J

    iput-object v2, v1, Lorg/apache/lucene/util/OpenBitSet;->bits:[J
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 737
    return-object v1

    .line 738
    .end local v1    # "obs":Lorg/apache/lucene/util/OpenBitSet;
    :catch_0
    move-exception v0

    .line 739
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method public ensureCapacity(J)V
    .locals 1
    .param p1, "numBits"    # J

    .prologue
    .line 851
    invoke-static {p1, p2}, Lorg/apache/lucene/util/OpenBitSet;->bits2words(J)I

    move-result v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/util/OpenBitSet;->ensureCapacityWords(I)V

    .line 852
    return-void
.end method

.method public ensureCapacityWords(I)V
    .locals 1
    .param p1, "numWords"    # I

    .prologue
    .line 842
    iget-object v0, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    array-length v0, v0

    if-ge v0, p1, :cond_0

    .line 843
    iget-object v0, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    invoke-static {v0, p1}, Lorg/apache/lucene/util/ArrayUtil;->grow([JI)[J

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    .line 845
    :cond_0
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 10
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 872
    if-ne p0, p1, :cond_1

    .line 892
    :cond_0
    :goto_0
    return v3

    .line 873
    :cond_1
    instance-of v5, p1, Lorg/apache/lucene/util/OpenBitSet;

    if-nez v5, :cond_2

    move v3, v4

    goto :goto_0

    :cond_2
    move-object v1, p1

    .line 875
    check-cast v1, Lorg/apache/lucene/util/OpenBitSet;

    .line 877
    .local v1, "b":Lorg/apache/lucene/util/OpenBitSet;
    iget v5, v1, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v6, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    if-le v5, v6, :cond_3

    .line 878
    move-object v0, v1

    .local v0, "a":Lorg/apache/lucene/util/OpenBitSet;
    move-object v1, p0

    .line 884
    :goto_1
    iget v5, v0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    add-int/lit8 v2, v5, -0x1

    .local v2, "i":I
    :goto_2
    iget v5, v1, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    if-ge v2, v5, :cond_4

    .line 888
    iget v5, v1, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    add-int/lit8 v2, v5, -0x1

    :goto_3
    if-ltz v2, :cond_0

    .line 889
    iget-object v5, v0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v6, v5, v2

    iget-object v5, v1, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v8, v5, v2

    cmp-long v5, v6, v8

    if-eqz v5, :cond_6

    move v3, v4

    goto :goto_0

    .line 880
    .end local v0    # "a":Lorg/apache/lucene/util/OpenBitSet;
    .end local v2    # "i":I
    :cond_3
    move-object v0, p0

    .restart local v0    # "a":Lorg/apache/lucene/util/OpenBitSet;
    goto :goto_1

    .line 885
    .restart local v2    # "i":I
    :cond_4
    iget-object v5, v0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v6, v5, v2

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-eqz v5, :cond_5

    move v3, v4

    goto :goto_0

    .line 884
    :cond_5
    add-int/lit8 v2, v2, -0x1

    goto :goto_2

    .line 888
    :cond_6
    add-int/lit8 v2, v2, -0x1

    goto :goto_3
.end method

.method protected expandingWordNum(J)I
    .locals 7
    .param p1, "index"    # J

    .prologue
    const-wide/16 v4, 0x1

    .line 308
    const/4 v1, 0x6

    shr-long v2, p1, v1

    long-to-int v0, v2

    .line 309
    .local v0, "wordNum":I
    iget v1, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    if-lt v0, v1, :cond_0

    .line 310
    add-long v2, p1, v4

    invoke-virtual {p0, v2, v3}, Lorg/apache/lucene/util/OpenBitSet;->ensureCapacity(J)V

    .line 311
    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    .line 313
    :cond_0
    sget-boolean v1, Lorg/apache/lucene/util/OpenBitSet;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    iget-wide v2, p0, Lorg/apache/lucene/util/OpenBitSet;->numBits:J

    add-long/2addr v4, p1

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    iput-wide v2, p0, Lorg/apache/lucene/util/OpenBitSet;->numBits:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-gez v1, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 314
    :cond_1
    return v0
.end method

.method public fastClear(I)V
    .locals 10
    .param p1, "index"    # I

    .prologue
    .line 322
    sget-boolean v4, Lorg/apache/lucene/util/OpenBitSet;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    if-ltz p1, :cond_0

    int-to-long v4, p1

    iget-wide v6, p0, Lorg/apache/lucene/util/OpenBitSet;->numBits:J

    cmp-long v4, v4, v6

    if-ltz v4, :cond_1

    :cond_0
    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 323
    :cond_1
    shr-int/lit8 v1, p1, 0x6

    .line 324
    .local v1, "wordNum":I
    and-int/lit8 v0, p1, 0x3f

    .line 325
    .local v0, "bit":I
    const-wide/16 v4, 0x1

    shl-long v2, v4, v0

    .line 326
    .local v2, "bitmask":J
    iget-object v4, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v6, v4, v1

    const-wide/16 v8, -0x1

    xor-long/2addr v8, v2

    and-long/2addr v6, v8

    aput-wide v6, v4, v1

    .line 334
    return-void
.end method

.method public fastClear(J)V
    .locals 11
    .param p1, "index"    # J

    .prologue
    .line 340
    sget-boolean v4, Lorg/apache/lucene/util/OpenBitSet;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    const-wide/16 v4, 0x0

    cmp-long v4, p1, v4

    if-ltz v4, :cond_0

    iget-wide v4, p0, Lorg/apache/lucene/util/OpenBitSet;->numBits:J

    cmp-long v4, p1, v4

    if-ltz v4, :cond_1

    :cond_0
    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 341
    :cond_1
    const/4 v4, 0x6

    shr-long v4, p1, v4

    long-to-int v1, v4

    .line 342
    .local v1, "wordNum":I
    long-to-int v4, p1

    and-int/lit8 v0, v4, 0x3f

    .line 343
    .local v0, "bit":I
    const-wide/16 v4, 0x1

    shl-long v2, v4, v0

    .line 344
    .local v2, "bitmask":J
    iget-object v4, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v6, v4, v1

    const-wide/16 v8, -0x1

    xor-long/2addr v8, v2

    and-long/2addr v6, v8

    aput-wide v6, v4, v1

    .line 345
    return-void
.end method

.method public fastFlip(I)V
    .locals 8
    .param p1, "index"    # I

    .prologue
    .line 461
    sget-boolean v4, Lorg/apache/lucene/util/OpenBitSet;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    if-ltz p1, :cond_0

    int-to-long v4, p1

    iget-wide v6, p0, Lorg/apache/lucene/util/OpenBitSet;->numBits:J

    cmp-long v4, v4, v6

    if-ltz v4, :cond_1

    :cond_0
    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 462
    :cond_1
    shr-int/lit8 v1, p1, 0x6

    .line 463
    .local v1, "wordNum":I
    and-int/lit8 v0, p1, 0x3f

    .line 464
    .local v0, "bit":I
    const-wide/16 v4, 0x1

    shl-long v2, v4, v0

    .line 465
    .local v2, "bitmask":J
    iget-object v4, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v6, v4, v1

    xor-long/2addr v6, v2

    aput-wide v6, v4, v1

    .line 466
    return-void
.end method

.method public fastFlip(J)V
    .locals 9
    .param p1, "index"    # J

    .prologue
    .line 472
    sget-boolean v4, Lorg/apache/lucene/util/OpenBitSet;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    const-wide/16 v4, 0x0

    cmp-long v4, p1, v4

    if-ltz v4, :cond_0

    iget-wide v4, p0, Lorg/apache/lucene/util/OpenBitSet;->numBits:J

    cmp-long v4, p1, v4

    if-ltz v4, :cond_1

    :cond_0
    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 473
    :cond_1
    const/4 v4, 0x6

    shr-long v4, p1, v4

    long-to-int v1, v4

    .line 474
    .local v1, "wordNum":I
    long-to-int v4, p1

    and-int/lit8 v0, v4, 0x3f

    .line 475
    .local v0, "bit":I
    const-wide/16 v4, 0x1

    shl-long v2, v4, v0

    .line 476
    .local v2, "bitmask":J
    iget-object v4, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v6, v4, v1

    xor-long/2addr v6, v2

    aput-wide v6, v4, v1

    .line 477
    return-void
.end method

.method public fastGet(I)Z
    .locals 8
    .param p1, "index"    # I

    .prologue
    .line 182
    sget-boolean v4, Lorg/apache/lucene/util/OpenBitSet;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    if-ltz p1, :cond_0

    int-to-long v4, p1

    iget-wide v6, p0, Lorg/apache/lucene/util/OpenBitSet;->numBits:J

    cmp-long v4, v4, v6

    if-ltz v4, :cond_1

    :cond_0
    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 183
    :cond_1
    shr-int/lit8 v1, p1, 0x6

    .line 186
    .local v1, "i":I
    and-int/lit8 v0, p1, 0x3f

    .line 187
    .local v0, "bit":I
    const-wide/16 v4, 0x1

    shl-long v2, v4, v0

    .line 188
    .local v2, "bitmask":J
    iget-object v4, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v4, v4, v1

    and-long/2addr v4, v2

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_2

    const/4 v4, 0x1

    :goto_0
    return v4

    :cond_2
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public fastGet(J)Z
    .locals 9
    .param p1, "index"    # J

    .prologue
    const-wide/16 v6, 0x0

    .line 207
    sget-boolean v4, Lorg/apache/lucene/util/OpenBitSet;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    cmp-long v4, p1, v6

    if-ltz v4, :cond_0

    iget-wide v4, p0, Lorg/apache/lucene/util/OpenBitSet;->numBits:J

    cmp-long v4, p1, v4

    if-ltz v4, :cond_1

    :cond_0
    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 208
    :cond_1
    const/4 v4, 0x6

    shr-long v4, p1, v4

    long-to-int v1, v4

    .line 209
    .local v1, "i":I
    long-to-int v4, p1

    and-int/lit8 v0, v4, 0x3f

    .line 210
    .local v0, "bit":I
    const-wide/16 v4, 0x1

    shl-long v2, v4, v0

    .line 211
    .local v2, "bitmask":J
    iget-object v4, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v4, v4, v1

    and-long/2addr v4, v2

    cmp-long v4, v4, v6

    if-eqz v4, :cond_2

    const/4 v4, 0x1

    :goto_0
    return v4

    :cond_2
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public fastSet(I)V
    .locals 8
    .param p1, "index"    # I

    .prologue
    .line 260
    sget-boolean v4, Lorg/apache/lucene/util/OpenBitSet;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    if-ltz p1, :cond_0

    int-to-long v4, p1

    iget-wide v6, p0, Lorg/apache/lucene/util/OpenBitSet;->numBits:J

    cmp-long v4, v4, v6

    if-ltz v4, :cond_1

    :cond_0
    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 261
    :cond_1
    shr-int/lit8 v1, p1, 0x6

    .line 262
    .local v1, "wordNum":I
    and-int/lit8 v0, p1, 0x3f

    .line 263
    .local v0, "bit":I
    const-wide/16 v4, 0x1

    shl-long v2, v4, v0

    .line 264
    .local v2, "bitmask":J
    iget-object v4, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v6, v4, v1

    or-long/2addr v6, v2

    aput-wide v6, v4, v1

    .line 265
    return-void
.end method

.method public fastSet(J)V
    .locals 9
    .param p1, "index"    # J

    .prologue
    .line 271
    sget-boolean v4, Lorg/apache/lucene/util/OpenBitSet;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    const-wide/16 v4, 0x0

    cmp-long v4, p1, v4

    if-ltz v4, :cond_0

    iget-wide v4, p0, Lorg/apache/lucene/util/OpenBitSet;->numBits:J

    cmp-long v4, p1, v4

    if-ltz v4, :cond_1

    :cond_0
    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 272
    :cond_1
    const/4 v4, 0x6

    shr-long v4, p1, v4

    long-to-int v1, v4

    .line 273
    .local v1, "wordNum":I
    long-to-int v4, p1

    and-int/lit8 v0, v4, 0x3f

    .line 274
    .local v0, "bit":I
    const-wide/16 v4, 0x1

    shl-long v2, v4, v0

    .line 275
    .local v2, "bitmask":J
    iget-object v4, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v6, v4, v1

    or-long/2addr v6, v2

    aput-wide v6, v4, v1

    .line 276
    return-void
.end method

.method public flip(J)V
    .locals 9
    .param p1, "index"    # J

    .prologue
    .line 481
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/util/OpenBitSet;->expandingWordNum(J)I

    move-result v1

    .line 482
    .local v1, "wordNum":I
    long-to-int v4, p1

    and-int/lit8 v0, v4, 0x3f

    .line 483
    .local v0, "bit":I
    const-wide/16 v4, 0x1

    shl-long v2, v4, v0

    .line 484
    .local v2, "bitmask":J
    iget-object v4, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v6, v4, v1

    xor-long/2addr v6, v2

    aput-wide v6, v4, v1

    .line 485
    return-void
.end method

.method public flip(JJ)V
    .locals 15
    .param p1, "startIndex"    # J
    .param p3, "endIndex"    # J

    .prologue
    .line 517
    cmp-long v7, p3, p1

    if-gtz v7, :cond_0

    .line 546
    :goto_0
    return-void

    .line 518
    :cond_0
    const/4 v7, 0x6

    shr-long v10, p1, v7

    long-to-int v6, v10

    .line 522
    .local v6, "startWord":I
    const-wide/16 v10, 0x1

    sub-long v10, p3, v10

    invoke-virtual {p0, v10, v11}, Lorg/apache/lucene/util/OpenBitSet;->expandingWordNum(J)I

    move-result v2

    .line 531
    .local v2, "endWord":I
    const-wide/16 v10, -0x1

    move-wide/from16 v0, p1

    long-to-int v7, v0

    shl-long v8, v10, v7

    .line 532
    .local v8, "startmask":J
    const-wide/16 v10, -0x1

    move-wide/from16 v0, p3

    neg-long v12, v0

    long-to-int v7, v12

    ushr-long v4, v10, v7

    .line 534
    .local v4, "endmask":J
    if-ne v6, v2, :cond_1

    .line 535
    iget-object v7, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v10, v7, v6

    and-long v12, v8, v4

    xor-long/2addr v10, v12

    aput-wide v10, v7, v6

    goto :goto_0

    .line 539
    :cond_1
    iget-object v7, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v10, v7, v6

    xor-long/2addr v10, v8

    aput-wide v10, v7, v6

    .line 541
    add-int/lit8 v3, v6, 0x1

    .local v3, "i":I
    :goto_1
    if-lt v3, v2, :cond_2

    .line 545
    iget-object v7, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v10, v7, v2

    xor-long/2addr v10, v4

    aput-wide v10, v7, v2

    goto :goto_0

    .line 542
    :cond_2
    iget-object v7, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    iget-object v10, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v10, v10, v3

    const-wide/16 v12, -0x1

    xor-long/2addr v10, v12

    aput-wide v10, v7, v3

    .line 541
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method public flipAndGet(I)Z
    .locals 8
    .param p1, "index"    # I

    .prologue
    .line 491
    sget-boolean v4, Lorg/apache/lucene/util/OpenBitSet;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    if-ltz p1, :cond_0

    int-to-long v4, p1

    iget-wide v6, p0, Lorg/apache/lucene/util/OpenBitSet;->numBits:J

    cmp-long v4, v4, v6

    if-ltz v4, :cond_1

    :cond_0
    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 492
    :cond_1
    shr-int/lit8 v1, p1, 0x6

    .line 493
    .local v1, "wordNum":I
    and-int/lit8 v0, p1, 0x3f

    .line 494
    .local v0, "bit":I
    const-wide/16 v4, 0x1

    shl-long v2, v4, v0

    .line 495
    .local v2, "bitmask":J
    iget-object v4, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v6, v4, v1

    xor-long/2addr v6, v2

    aput-wide v6, v4, v1

    .line 496
    iget-object v4, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v4, v4, v1

    and-long/2addr v4, v2

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_2

    const/4 v4, 0x1

    :goto_0
    return v4

    :cond_2
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public flipAndGet(J)Z
    .locals 11
    .param p1, "index"    # J

    .prologue
    const-wide/16 v8, 0x0

    .line 503
    sget-boolean v4, Lorg/apache/lucene/util/OpenBitSet;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    cmp-long v4, p1, v8

    if-ltz v4, :cond_0

    iget-wide v4, p0, Lorg/apache/lucene/util/OpenBitSet;->numBits:J

    cmp-long v4, p1, v4

    if-ltz v4, :cond_1

    :cond_0
    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 504
    :cond_1
    const/4 v4, 0x6

    shr-long v4, p1, v4

    long-to-int v1, v4

    .line 505
    .local v1, "wordNum":I
    long-to-int v4, p1

    and-int/lit8 v0, v4, 0x3f

    .line 506
    .local v0, "bit":I
    const-wide/16 v4, 0x1

    shl-long v2, v4, v0

    .line 507
    .local v2, "bitmask":J
    iget-object v4, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v6, v4, v1

    xor-long/2addr v6, v2

    aput-wide v6, v4, v1

    .line 508
    iget-object v4, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v4, v4, v1

    and-long/2addr v4, v2

    cmp-long v4, v4, v8

    if-eqz v4, :cond_2

    const/4 v4, 0x1

    :goto_0
    return v4

    :cond_2
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public get(I)Z
    .locals 10
    .param p1, "index"    # I

    .prologue
    const/4 v4, 0x0

    .line 167
    shr-int/lit8 v1, p1, 0x6

    .line 170
    .local v1, "i":I
    iget-object v5, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    array-length v5, v5

    if-lt v1, v5, :cond_1

    .line 174
    :cond_0
    :goto_0
    return v4

    .line 172
    :cond_1
    and-int/lit8 v0, p1, 0x3f

    .line 173
    .local v0, "bit":I
    const-wide/16 v6, 0x1

    shl-long v2, v6, v0

    .line 174
    .local v2, "bitmask":J
    iget-object v5, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v6, v5, v1

    and-long/2addr v6, v2

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-eqz v5, :cond_0

    const/4 v4, 0x1

    goto :goto_0
.end method

.method public get(J)Z
    .locals 11
    .param p1, "index"    # J

    .prologue
    const/4 v4, 0x0

    .line 196
    const/4 v5, 0x6

    shr-long v6, p1, v5

    long-to-int v1, v6

    .line 197
    .local v1, "i":I
    iget-object v5, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    array-length v5, v5

    if-lt v1, v5, :cond_1

    .line 200
    :cond_0
    :goto_0
    return v4

    .line 198
    :cond_1
    long-to-int v5, p1

    and-int/lit8 v0, v5, 0x3f

    .line 199
    .local v0, "bit":I
    const-wide/16 v6, 0x1

    shl-long v2, v6, v0

    .line 200
    .local v2, "bitmask":J
    iget-object v5, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v6, v5, v1

    and-long/2addr v6, v2

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-eqz v5, :cond_0

    const/4 v4, 0x1

    goto :goto_0
.end method

.method public getAndSet(I)Z
    .locals 10
    .param p1, "index"    # I

    .prologue
    .line 435
    sget-boolean v5, Lorg/apache/lucene/util/OpenBitSet;->$assertionsDisabled:Z

    if-nez v5, :cond_1

    if-ltz p1, :cond_0

    int-to-long v6, p1

    iget-wide v8, p0, Lorg/apache/lucene/util/OpenBitSet;->numBits:J

    cmp-long v5, v6, v8

    if-ltz v5, :cond_1

    :cond_0
    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 436
    :cond_1
    shr-int/lit8 v4, p1, 0x6

    .line 437
    .local v4, "wordNum":I
    and-int/lit8 v0, p1, 0x3f

    .line 438
    .local v0, "bit":I
    const-wide/16 v6, 0x1

    shl-long v2, v6, v0

    .line 439
    .local v2, "bitmask":J
    iget-object v5, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v6, v5, v4

    and-long/2addr v6, v2

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-eqz v5, :cond_2

    const/4 v1, 0x1

    .line 440
    .local v1, "val":Z
    :goto_0
    iget-object v5, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v6, v5, v4

    or-long/2addr v6, v2

    aput-wide v6, v5, v4

    .line 441
    return v1

    .line 439
    .end local v1    # "val":Z
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getAndSet(J)Z
    .locals 11
    .param p1, "index"    # J

    .prologue
    const-wide/16 v8, 0x0

    .line 448
    sget-boolean v5, Lorg/apache/lucene/util/OpenBitSet;->$assertionsDisabled:Z

    if-nez v5, :cond_1

    cmp-long v5, p1, v8

    if-ltz v5, :cond_0

    iget-wide v6, p0, Lorg/apache/lucene/util/OpenBitSet;->numBits:J

    cmp-long v5, p1, v6

    if-ltz v5, :cond_1

    :cond_0
    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 449
    :cond_1
    const/4 v5, 0x6

    shr-long v6, p1, v5

    long-to-int v4, v6

    .line 450
    .local v4, "wordNum":I
    long-to-int v5, p1

    and-int/lit8 v0, v5, 0x3f

    .line 451
    .local v0, "bit":I
    const-wide/16 v6, 0x1

    shl-long v2, v6, v0

    .line 452
    .local v2, "bitmask":J
    iget-object v5, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v6, v5, v4

    and-long/2addr v6, v2

    cmp-long v5, v6, v8

    if-eqz v5, :cond_2

    const/4 v1, 0x1

    .line 453
    .local v1, "val":Z
    :goto_0
    iget-object v5, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v6, v5, v4

    or-long/2addr v6, v2

    aput-wide v6, v5, v4

    .line 454
    return v1

    .line 452
    .end local v1    # "val":Z
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getBit(I)I
    .locals 6
    .param p1, "index"    # I

    .prologue
    .line 231
    sget-boolean v2, Lorg/apache/lucene/util/OpenBitSet;->$assertionsDisabled:Z

    if-nez v2, :cond_1

    if-ltz p1, :cond_0

    int-to-long v2, p1

    iget-wide v4, p0, Lorg/apache/lucene/util/OpenBitSet;->numBits:J

    cmp-long v2, v2, v4

    if-ltz v2, :cond_1

    :cond_0
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 232
    :cond_1
    shr-int/lit8 v1, p1, 0x6

    .line 233
    .local v1, "i":I
    and-int/lit8 v0, p1, 0x3f

    .line 234
    .local v0, "bit":I
    iget-object v2, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v2, v2, v1

    ushr-long/2addr v2, v0

    long-to-int v2, v2

    and-int/lit8 v2, v2, 0x1

    return v2
.end method

.method public getBits()[J
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    return-object v0
.end method

.method public getNumWords()I
    .locals 1

    .prologue
    .line 157
    iget v0, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    return v0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    .line 900
    const-wide/16 v0, 0x0

    .line 901
    .local v0, "h":J
    iget-object v3, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    array-length v2, v3

    .local v2, "i":I
    :goto_0
    add-int/lit8 v2, v2, -0x1

    if-gez v2, :cond_0

    .line 907
    const/16 v3, 0x20

    shr-long v4, v0, v3

    xor-long/2addr v4, v0

    long-to-int v3, v4

    const v4, -0x6789edcc

    add-int/2addr v3, v4

    return v3

    .line 902
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v4, v3, v2

    xor-long/2addr v0, v4

    .line 903
    const/4 v3, 0x1

    shl-long v4, v0, v3

    const/16 v3, 0x3f

    ushr-long v6, v0, v3

    or-long v0, v4, v6

    goto :goto_0
.end method

.method public intersect(Lorg/apache/lucene/util/OpenBitSet;)V
    .locals 8
    .param p1, "other"    # Lorg/apache/lucene/util/OpenBitSet;

    .prologue
    .line 745
    iget v4, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v5, p1, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 746
    .local v0, "newLen":I
    iget-object v3, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    .line 747
    .local v3, "thisArr":[J
    iget-object v1, p1, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    .line 749
    .local v1, "otherArr":[J
    move v2, v0

    .line 750
    .local v2, "pos":I
    :goto_0
    add-int/lit8 v2, v2, -0x1

    if-gez v2, :cond_1

    .line 753
    iget v4, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    if-le v4, v0, :cond_0

    .line 755
    iget-object v4, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    iget v5, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    const-wide/16 v6, 0x0

    invoke-static {v4, v0, v5, v6, v7}, Ljava/util/Arrays;->fill([JIIJ)V

    .line 757
    :cond_0
    iput v0, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    .line 758
    return-void

    .line 751
    :cond_1
    aget-wide v4, v3, v2

    aget-wide v6, v1, v2

    and-long/2addr v4, v6

    aput-wide v4, v3, v2

    goto :goto_0
.end method

.method public intersects(Lorg/apache/lucene/util/OpenBitSet;)Z
    .locals 8
    .param p1, "other"    # Lorg/apache/lucene/util/OpenBitSet;

    .prologue
    .line 827
    iget v3, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v4, p1, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 828
    .local v1, "pos":I
    iget-object v2, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    .line 829
    .local v2, "thisArr":[J
    iget-object v0, p1, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    .line 830
    .local v0, "otherArr":[J
    :cond_0
    add-int/lit8 v1, v1, -0x1

    if-gez v1, :cond_1

    .line 833
    const/4 v3, 0x0

    :goto_0
    return v3

    .line 831
    :cond_1
    aget-wide v4, v2, v1

    aget-wide v6, v0, v1

    and-long/2addr v4, v6

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    goto :goto_0
.end method

.method public isCacheable()Z
    .locals 1

    .prologue
    .line 128
    const/4 v0, 0x1

    return v0
.end method

.method public isEmpty()Z
    .locals 4

    .prologue
    .line 148
    invoke-virtual {p0}, Lorg/apache/lucene/util/OpenBitSet;->cardinality()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public iterator()Lorg/apache/lucene/search/DocIdSetIterator;
    .locals 3

    .prologue
    .line 117
    new-instance v0, Lorg/apache/lucene/util/OpenBitSetIterator;

    iget-object v1, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    iget v2, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/util/OpenBitSetIterator;-><init>([JI)V

    return-object v0
.end method

.method public length()I
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    array-length v0, v0

    shl-int/lit8 v0, v0, 0x6

    return v0
.end method

.method public nextSetBit(I)I
    .locals 10
    .param p1, "index"    # I

    .prologue
    const-wide/16 v8, 0x0

    const/4 v4, -0x1

    .line 627
    shr-int/lit8 v0, p1, 0x6

    .line 628
    .local v0, "i":I
    iget v5, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    if-lt v0, v5, :cond_0

    .line 641
    :goto_0
    return v4

    .line 629
    :cond_0
    and-int/lit8 v1, p1, 0x3f

    .line 630
    .local v1, "subIndex":I
    iget-object v5, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v6, v5, v0

    shr-long v2, v6, v1

    .line 632
    .local v2, "word":J
    cmp-long v5, v2, v8

    if-eqz v5, :cond_2

    .line 633
    shl-int/lit8 v4, v0, 0x6

    add-int/2addr v4, v1

    invoke-static {v2, v3}, Ljava/lang/Long;->numberOfTrailingZeros(J)I

    move-result v5

    add-int/2addr v4, v5

    goto :goto_0

    .line 637
    :cond_1
    iget-object v5, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v2, v5, v0

    .line 638
    cmp-long v5, v2, v8

    if-eqz v5, :cond_2

    shl-int/lit8 v4, v0, 0x6

    invoke-static {v2, v3}, Ljava/lang/Long;->numberOfTrailingZeros(J)I

    move-result v5

    add-int/2addr v4, v5

    goto :goto_0

    .line 636
    :cond_2
    add-int/lit8 v0, v0, 0x1

    iget v5, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    if-lt v0, v5, :cond_1

    goto :goto_0
.end method

.method public nextSetBit(J)J
    .locals 13
    .param p1, "index"    # J

    .prologue
    const-wide/16 v10, 0x0

    const-wide/16 v4, -0x1

    const/4 v8, 0x6

    .line 648
    ushr-long v6, p1, v8

    long-to-int v0, v6

    .line 649
    .local v0, "i":I
    iget v6, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    if-lt v0, v6, :cond_0

    .line 662
    :goto_0
    return-wide v4

    .line 650
    :cond_0
    long-to-int v6, p1

    and-int/lit8 v1, v6, 0x3f

    .line 651
    .local v1, "subIndex":I
    iget-object v6, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v6, v6, v0

    ushr-long v2, v6, v1

    .line 653
    .local v2, "word":J
    cmp-long v6, v2, v10

    if-eqz v6, :cond_2

    .line 654
    int-to-long v4, v0

    shl-long/2addr v4, v8

    invoke-static {v2, v3}, Ljava/lang/Long;->numberOfTrailingZeros(J)I

    move-result v6

    add-int/2addr v6, v1

    int-to-long v6, v6

    add-long/2addr v4, v6

    goto :goto_0

    .line 658
    :cond_1
    iget-object v6, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v2, v6, v0

    .line 659
    cmp-long v6, v2, v10

    if-eqz v6, :cond_2

    int-to-long v4, v0

    shl-long/2addr v4, v8

    invoke-static {v2, v3}, Ljava/lang/Long;->numberOfTrailingZeros(J)I

    move-result v6

    int-to-long v6, v6

    add-long/2addr v4, v6

    goto :goto_0

    .line 657
    :cond_2
    add-int/lit8 v0, v0, 0x1

    iget v6, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    if-lt v0, v6, :cond_1

    goto :goto_0
.end method

.method public or(Lorg/apache/lucene/util/OpenBitSet;)V
    .locals 0
    .param p1, "other"    # Lorg/apache/lucene/util/OpenBitSet;

    .prologue
    .line 817
    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/OpenBitSet;->union(Lorg/apache/lucene/util/OpenBitSet;)V

    .line 818
    return-void
.end method

.method public prevSetBit(I)I
    .locals 10
    .param p1, "index"    # I

    .prologue
    const-wide/16 v8, 0x0

    const/4 v4, -0x1

    .line 671
    shr-int/lit8 v0, p1, 0x6

    .line 674
    .local v0, "i":I
    iget v5, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    if-lt v0, v5, :cond_2

    .line 675
    iget v5, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    add-int/lit8 v0, v5, -0x1

    .line 676
    if-gez v0, :cond_1

    .line 696
    :cond_0
    :goto_0
    return v4

    .line 677
    :cond_1
    const/16 v1, 0x3f

    .line 678
    .local v1, "subIndex":I
    iget-object v5, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v2, v5, v0

    .line 685
    .local v2, "word":J
    :goto_1
    cmp-long v5, v2, v8

    if-eqz v5, :cond_4

    .line 686
    shl-int/lit8 v4, v0, 0x6

    add-int/2addr v4, v1

    invoke-static {v2, v3}, Ljava/lang/Long;->numberOfLeadingZeros(J)I

    move-result v5

    sub-int/2addr v4, v5

    goto :goto_0

    .line 680
    .end local v1    # "subIndex":I
    .end local v2    # "word":J
    :cond_2
    if-ltz v0, :cond_0

    .line 681
    and-int/lit8 v1, p1, 0x3f

    .line 682
    .restart local v1    # "subIndex":I
    iget-object v5, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v6, v5, v0

    rsub-int/lit8 v5, v1, 0x3f

    shl-long v2, v6, v5

    .restart local v2    # "word":J
    goto :goto_1

    .line 690
    :cond_3
    iget-object v5, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v2, v5, v0

    .line 691
    cmp-long v5, v2, v8

    if-eqz v5, :cond_4

    .line 692
    shl-int/lit8 v4, v0, 0x6

    add-int/lit8 v4, v4, 0x3f

    invoke-static {v2, v3}, Ljava/lang/Long;->numberOfLeadingZeros(J)I

    move-result v5

    sub-int/2addr v4, v5

    goto :goto_0

    .line 689
    :cond_4
    add-int/lit8 v0, v0, -0x1

    if-gez v0, :cond_3

    goto :goto_0
.end method

.method public prevSetBit(J)J
    .locals 13
    .param p1, "index"    # J

    .prologue
    const-wide/16 v10, 0x0

    const-wide/16 v4, -0x1

    const/4 v9, 0x6

    .line 704
    shr-long v6, p1, v9

    long-to-int v0, v6

    .line 707
    .local v0, "i":I
    iget v6, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    if-lt v0, v6, :cond_2

    .line 708
    iget v6, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    add-int/lit8 v0, v6, -0x1

    .line 709
    if-gez v0, :cond_1

    .line 729
    :cond_0
    :goto_0
    return-wide v4

    .line 710
    :cond_1
    const/16 v1, 0x3f

    .line 711
    .local v1, "subIndex":I
    iget-object v6, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v2, v6, v0

    .line 718
    .local v2, "word":J
    :goto_1
    cmp-long v6, v2, v10

    if-eqz v6, :cond_4

    .line 719
    int-to-long v4, v0

    shl-long/2addr v4, v9

    int-to-long v6, v1

    add-long/2addr v4, v6

    invoke-static {v2, v3}, Ljava/lang/Long;->numberOfLeadingZeros(J)I

    move-result v6

    int-to-long v6, v6

    sub-long/2addr v4, v6

    goto :goto_0

    .line 713
    .end local v1    # "subIndex":I
    .end local v2    # "word":J
    :cond_2
    if-ltz v0, :cond_0

    .line 714
    long-to-int v6, p1

    and-int/lit8 v1, v6, 0x3f

    .line 715
    .restart local v1    # "subIndex":I
    iget-object v6, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v6, v6, v0

    rsub-int/lit8 v8, v1, 0x3f

    shl-long v2, v6, v8

    .restart local v2    # "word":J
    goto :goto_1

    .line 723
    :cond_3
    iget-object v6, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v2, v6, v0

    .line 724
    cmp-long v6, v2, v10

    if-eqz v6, :cond_4

    .line 725
    int-to-long v4, v0

    shl-long/2addr v4, v9

    const-wide/16 v6, 0x3f

    add-long/2addr v4, v6

    invoke-static {v2, v3}, Ljava/lang/Long;->numberOfLeadingZeros(J)I

    move-result v6

    int-to-long v6, v6

    sub-long/2addr v4, v6

    goto :goto_0

    .line 722
    :cond_4
    add-int/lit8 v0, v0, -0x1

    if-gez v0, :cond_3

    goto :goto_0
.end method

.method public remove(Lorg/apache/lucene/util/OpenBitSet;)V
    .locals 10
    .param p1, "other"    # Lorg/apache/lucene/util/OpenBitSet;

    .prologue
    .line 781
    iget v3, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v4, p1, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 782
    .local v0, "idx":I
    iget-object v2, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    .line 783
    .local v2, "thisArr":[J
    iget-object v1, p1, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    .line 784
    .local v1, "otherArr":[J
    :goto_0
    add-int/lit8 v0, v0, -0x1

    if-gez v0, :cond_0

    .line 787
    return-void

    .line 785
    :cond_0
    aget-wide v4, v2, v0

    aget-wide v6, v1, v0

    const-wide/16 v8, -0x1

    xor-long/2addr v6, v8

    and-long/2addr v4, v6

    aput-wide v4, v2, v0

    goto :goto_0
.end method

.method public set(J)V
    .locals 9
    .param p1, "index"    # J

    .prologue
    .line 249
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/util/OpenBitSet;->expandingWordNum(J)I

    move-result v1

    .line 250
    .local v1, "wordNum":I
    long-to-int v4, p1

    and-int/lit8 v0, v4, 0x3f

    .line 251
    .local v0, "bit":I
    const-wide/16 v4, 0x1

    shl-long v2, v4, v0

    .line 252
    .local v2, "bitmask":J
    iget-object v4, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v6, v4, v1

    or-long/2addr v6, v2

    aput-wide v6, v4, v1

    .line 253
    return-void
.end method

.method public set(JJ)V
    .locals 15
    .param p1, "startIndex"    # J
    .param p3, "endIndex"    # J

    .prologue
    .line 284
    cmp-long v8, p3, p1

    if-gtz v8, :cond_0

    .line 303
    :goto_0
    return-void

    .line 286
    :cond_0
    const/4 v8, 0x6

    shr-long v8, p1, v8

    long-to-int v3, v8

    .line 290
    .local v3, "startWord":I
    const-wide/16 v8, 0x1

    sub-long v8, p3, v8

    invoke-virtual {p0, v8, v9}, Lorg/apache/lucene/util/OpenBitSet;->expandingWordNum(J)I

    move-result v2

    .line 292
    .local v2, "endWord":I
    const-wide/16 v8, -0x1

    move-wide/from16 v0, p1

    long-to-int v10, v0

    shl-long v6, v8, v10

    .line 293
    .local v6, "startmask":J
    const-wide/16 v8, -0x1

    move-wide/from16 v0, p3

    neg-long v10, v0

    long-to-int v10, v10

    ushr-long v4, v8, v10

    .line 295
    .local v4, "endmask":J
    if-ne v3, v2, :cond_1

    .line 296
    iget-object v8, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v10, v8, v3

    and-long v12, v6, v4

    or-long/2addr v10, v12

    aput-wide v10, v8, v3

    goto :goto_0

    .line 300
    :cond_1
    iget-object v8, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v10, v8, v3

    or-long/2addr v10, v6

    aput-wide v10, v8, v3

    .line 301
    iget-object v8, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    add-int/lit8 v9, v3, 0x1

    const-wide/16 v10, -0x1

    invoke-static {v8, v9, v2, v10, v11}, Ljava/util/Arrays;->fill([JIIJ)V

    .line 302
    iget-object v8, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v10, v8, v2

    or-long/2addr v10, v4

    aput-wide v10, v8, v2

    goto :goto_0
.end method

.method public setBits([J)V
    .locals 0
    .param p1, "bits"    # [J

    .prologue
    .line 154
    iput-object p1, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    return-void
.end method

.method public setNumWords(I)V
    .locals 0
    .param p1, "nWords"    # I

    .prologue
    .line 160
    iput p1, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    return-void
.end method

.method public size()J
    .locals 2

    .prologue
    .line 139
    invoke-virtual {p0}, Lorg/apache/lucene/util/OpenBitSet;->capacity()J

    move-result-wide v0

    return-wide v0
.end method

.method public trimTrailingZeros()V
    .locals 6

    .prologue
    .line 858
    iget v1, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    add-int/lit8 v0, v1, -0x1

    .line 859
    .local v0, "idx":I
    :goto_0
    if-ltz v0, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v2, v1, v0

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 860
    :cond_0
    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    .line 861
    return-void

    .line 859
    :cond_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method public union(Lorg/apache/lucene/util/OpenBitSet;)V
    .locals 8
    .param p1, "other"    # Lorg/apache/lucene/util/OpenBitSet;

    .prologue
    .line 762
    iget v4, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v5, p1, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 763
    .local v0, "newLen":I
    invoke-virtual {p0, v0}, Lorg/apache/lucene/util/OpenBitSet;->ensureCapacityWords(I)V

    .line 764
    sget-boolean v4, Lorg/apache/lucene/util/OpenBitSet;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    iget-wide v4, p1, Lorg/apache/lucene/util/OpenBitSet;->numBits:J

    iget-wide v6, p0, Lorg/apache/lucene/util/OpenBitSet;->numBits:J

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    iput-wide v4, p0, Lorg/apache/lucene/util/OpenBitSet;->numBits:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-gez v4, :cond_0

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 766
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    .line 767
    .local v3, "thisArr":[J
    iget-object v1, p1, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    .line 768
    .local v1, "otherArr":[J
    iget v4, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v5, p1, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 769
    .local v2, "pos":I
    :goto_0
    add-int/lit8 v2, v2, -0x1

    if-gez v2, :cond_2

    .line 772
    iget v4, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    if-ge v4, v0, :cond_1

    .line 773
    iget v4, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v5, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v6, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    sub-int v6, v0, v6

    invoke-static {v1, v4, v3, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 775
    :cond_1
    iput v0, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    .line 776
    return-void

    .line 770
    :cond_2
    aget-wide v4, v3, v2

    aget-wide v6, v1, v2

    or-long/2addr v4, v6

    aput-wide v4, v3, v2

    goto :goto_0
.end method

.method public xor(Lorg/apache/lucene/util/OpenBitSet;)V
    .locals 8
    .param p1, "other"    # Lorg/apache/lucene/util/OpenBitSet;

    .prologue
    .line 791
    iget v4, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v5, p1, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 792
    .local v0, "newLen":I
    invoke-virtual {p0, v0}, Lorg/apache/lucene/util/OpenBitSet;->ensureCapacityWords(I)V

    .line 793
    sget-boolean v4, Lorg/apache/lucene/util/OpenBitSet;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    iget-wide v4, p1, Lorg/apache/lucene/util/OpenBitSet;->numBits:J

    iget-wide v6, p0, Lorg/apache/lucene/util/OpenBitSet;->numBits:J

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    iput-wide v4, p0, Lorg/apache/lucene/util/OpenBitSet;->numBits:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-gez v4, :cond_0

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 795
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    .line 796
    .local v3, "thisArr":[J
    iget-object v1, p1, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    .line 797
    .local v1, "otherArr":[J
    iget v4, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v5, p1, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 798
    .local v2, "pos":I
    :goto_0
    add-int/lit8 v2, v2, -0x1

    if-gez v2, :cond_2

    .line 801
    iget v4, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    if-ge v4, v0, :cond_1

    .line 802
    iget v4, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v5, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v6, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    sub-int v6, v0, v6

    invoke-static {v1, v4, v3, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 804
    :cond_1
    iput v0, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    .line 805
    return-void

    .line 799
    :cond_2
    aget-wide v4, v3, v2

    aget-wide v6, v1, v2

    xor-long/2addr v4, v6

    aput-wide v4, v3, v2

    goto :goto_0
.end method
