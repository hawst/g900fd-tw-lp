.class final Lorg/apache/lucene/util/packed/BulkOperationPacked15;
.super Lorg/apache/lucene/util/packed/BulkOperationPacked;
.source "BulkOperationPacked15.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    const/16 v0, 0xf

    invoke-direct {p0, v0}, Lorg/apache/lucene/util/packed/BulkOperationPacked;-><init>(I)V

    .line 29
    return-void
.end method


# virtual methods
.method public decode([BI[III)V
    .locals 22
    .param p1, "blocks"    # [B
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [I
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 118
    const/16 v18, 0x0

    .local v18, "i":I
    move/from16 v19, p4

    .end local p4    # "valuesOffset":I
    .local v19, "valuesOffset":I
    move/from16 v2, p2

    .end local p2    # "blocksOffset":I
    .local v2, "blocksOffset":I
    :goto_0
    move/from16 v0, v18

    move/from16 v1, p5

    if-lt v0, v1, :cond_0

    .line 143
    return-void

    .line 119
    :cond_0
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v20, p1, v2

    move/from16 v0, v20

    and-int/lit16 v3, v0, 0xff

    .line 120
    .local v3, "byte0":I
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v20, p1, p2

    move/from16 v0, v20

    and-int/lit16 v4, v0, 0xff

    .line 121
    .local v4, "byte1":I
    add-int/lit8 p4, v19, 0x1

    .end local v19    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    shl-int/lit8 v20, v3, 0x7

    ushr-int/lit8 v21, v4, 0x1

    or-int v20, v20, v21

    aput v20, p3, v19

    .line 122
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v20, p1, v2

    move/from16 v0, v20

    and-int/lit16 v10, v0, 0xff

    .line 123
    .local v10, "byte2":I
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v20, p1, p2

    move/from16 v0, v20

    and-int/lit16 v11, v0, 0xff

    .line 124
    .local v11, "byte3":I
    add-int/lit8 v19, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v19    # "valuesOffset":I
    and-int/lit8 v20, v4, 0x1

    shl-int/lit8 v20, v20, 0xe

    shl-int/lit8 v21, v10, 0x6

    or-int v20, v20, v21

    ushr-int/lit8 v21, v11, 0x2

    or-int v20, v20, v21

    aput v20, p3, p4

    .line 125
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v20, p1, v2

    move/from16 v0, v20

    and-int/lit16 v12, v0, 0xff

    .line 126
    .local v12, "byte4":I
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v20, p1, p2

    move/from16 v0, v20

    and-int/lit16 v13, v0, 0xff

    .line 127
    .local v13, "byte5":I
    add-int/lit8 p4, v19, 0x1

    .end local v19    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    and-int/lit8 v20, v11, 0x3

    shl-int/lit8 v20, v20, 0xd

    shl-int/lit8 v21, v12, 0x5

    or-int v20, v20, v21

    ushr-int/lit8 v21, v13, 0x3

    or-int v20, v20, v21

    aput v20, p3, v19

    .line 128
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v20, p1, v2

    move/from16 v0, v20

    and-int/lit16 v14, v0, 0xff

    .line 129
    .local v14, "byte6":I
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v20, p1, p2

    move/from16 v0, v20

    and-int/lit16 v15, v0, 0xff

    .line 130
    .local v15, "byte7":I
    add-int/lit8 v19, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v19    # "valuesOffset":I
    and-int/lit8 v20, v13, 0x7

    shl-int/lit8 v20, v20, 0xc

    shl-int/lit8 v21, v14, 0x4

    or-int v20, v20, v21

    ushr-int/lit8 v21, v15, 0x4

    or-int v20, v20, v21

    aput v20, p3, p4

    .line 131
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v20, p1, v2

    move/from16 v0, v20

    and-int/lit16 v0, v0, 0xff

    move/from16 v16, v0

    .line 132
    .local v16, "byte8":I
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v20, p1, p2

    move/from16 v0, v20

    and-int/lit16 v0, v0, 0xff

    move/from16 v17, v0

    .line 133
    .local v17, "byte9":I
    add-int/lit8 p4, v19, 0x1

    .end local v19    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    and-int/lit8 v20, v15, 0xf

    shl-int/lit8 v20, v20, 0xb

    shl-int/lit8 v21, v16, 0x3

    or-int v20, v20, v21

    ushr-int/lit8 v21, v17, 0x5

    or-int v20, v20, v21

    aput v20, p3, v19

    .line 134
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v20, p1, v2

    move/from16 v0, v20

    and-int/lit16 v5, v0, 0xff

    .line 135
    .local v5, "byte10":I
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v20, p1, p2

    move/from16 v0, v20

    and-int/lit16 v6, v0, 0xff

    .line 136
    .local v6, "byte11":I
    add-int/lit8 v19, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v19    # "valuesOffset":I
    and-int/lit8 v20, v17, 0x1f

    shl-int/lit8 v20, v20, 0xa

    shl-int/lit8 v21, v5, 0x2

    or-int v20, v20, v21

    ushr-int/lit8 v21, v6, 0x6

    or-int v20, v20, v21

    aput v20, p3, p4

    .line 137
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v20, p1, v2

    move/from16 v0, v20

    and-int/lit16 v7, v0, 0xff

    .line 138
    .local v7, "byte12":I
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v20, p1, p2

    move/from16 v0, v20

    and-int/lit16 v8, v0, 0xff

    .line 139
    .local v8, "byte13":I
    add-int/lit8 p4, v19, 0x1

    .end local v19    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    and-int/lit8 v20, v6, 0x3f

    shl-int/lit8 v20, v20, 0x9

    shl-int/lit8 v21, v7, 0x1

    or-int v20, v20, v21

    ushr-int/lit8 v21, v8, 0x7

    or-int v20, v20, v21

    aput v20, p3, v19

    .line 140
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v20, p1, v2

    move/from16 v0, v20

    and-int/lit16 v9, v0, 0xff

    .line 141
    .local v9, "byte14":I
    add-int/lit8 v19, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v19    # "valuesOffset":I
    and-int/lit8 v20, v8, 0x7f

    shl-int/lit8 v20, v20, 0x8

    or-int v20, v20, v9

    aput v20, p3, p4

    .line 118
    add-int/lit8 v18, v18, 0x1

    move/from16 v2, p2

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    goto/16 :goto_0
.end method

.method public decode([BI[JII)V
    .locals 40
    .param p1, "blocks"    # [B
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [J
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 232
    const/4 v3, 0x0

    .local v3, "i":I
    move/from16 v34, p4

    .end local p4    # "valuesOffset":I
    .local v34, "valuesOffset":I
    move/from16 v2, p2

    .end local p2    # "blocksOffset":I
    .local v2, "blocksOffset":I
    :goto_0
    move/from16 v0, p5

    if-lt v3, v0, :cond_0

    .line 257
    return-void

    .line 233
    :cond_0
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v35, p1, v2

    move/from16 v0, v35

    and-int/lit16 v0, v0, 0xff

    move/from16 v35, v0

    move/from16 v0, v35

    int-to-long v4, v0

    .line 234
    .local v4, "byte0":J
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v35, p1, p2

    move/from16 v0, v35

    and-int/lit16 v0, v0, 0xff

    move/from16 v35, v0

    move/from16 v0, v35

    int-to-long v6, v0

    .line 235
    .local v6, "byte1":J
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v35, 0x7

    shl-long v36, v4, v35

    const/16 v35, 0x1

    ushr-long v38, v6, v35

    or-long v36, v36, v38

    aput-wide v36, p3, v34

    .line 236
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v35, p1, v2

    move/from16 v0, v35

    and-int/lit16 v0, v0, 0xff

    move/from16 v35, v0

    move/from16 v0, v35

    int-to-long v0, v0

    move-wide/from16 v18, v0

    .line 237
    .local v18, "byte2":J
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v35, p1, p2

    move/from16 v0, v35

    and-int/lit16 v0, v0, 0xff

    move/from16 v35, v0

    move/from16 v0, v35

    int-to-long v0, v0

    move-wide/from16 v20, v0

    .line 238
    .local v20, "byte3":J
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const-wide/16 v36, 0x1

    and-long v36, v36, v6

    const/16 v35, 0xe

    shl-long v36, v36, v35

    const/16 v35, 0x6

    shl-long v38, v18, v35

    or-long v36, v36, v38

    const/16 v35, 0x2

    ushr-long v38, v20, v35

    or-long v36, v36, v38

    aput-wide v36, p3, p4

    .line 239
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v35, p1, v2

    move/from16 v0, v35

    and-int/lit16 v0, v0, 0xff

    move/from16 v35, v0

    move/from16 v0, v35

    int-to-long v0, v0

    move-wide/from16 v22, v0

    .line 240
    .local v22, "byte4":J
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v35, p1, p2

    move/from16 v0, v35

    and-int/lit16 v0, v0, 0xff

    move/from16 v35, v0

    move/from16 v0, v35

    int-to-long v0, v0

    move-wide/from16 v24, v0

    .line 241
    .local v24, "byte5":J
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v36, 0x3

    and-long v36, v36, v20

    const/16 v35, 0xd

    shl-long v36, v36, v35

    const/16 v35, 0x5

    shl-long v38, v22, v35

    or-long v36, v36, v38

    const/16 v35, 0x3

    ushr-long v38, v24, v35

    or-long v36, v36, v38

    aput-wide v36, p3, v34

    .line 242
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v35, p1, v2

    move/from16 v0, v35

    and-int/lit16 v0, v0, 0xff

    move/from16 v35, v0

    move/from16 v0, v35

    int-to-long v0, v0

    move-wide/from16 v26, v0

    .line 243
    .local v26, "byte6":J
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v35, p1, p2

    move/from16 v0, v35

    and-int/lit16 v0, v0, 0xff

    move/from16 v35, v0

    move/from16 v0, v35

    int-to-long v0, v0

    move-wide/from16 v28, v0

    .line 244
    .local v28, "byte7":J
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const-wide/16 v36, 0x7

    and-long v36, v36, v24

    const/16 v35, 0xc

    shl-long v36, v36, v35

    const/16 v35, 0x4

    shl-long v38, v26, v35

    or-long v36, v36, v38

    const/16 v35, 0x4

    ushr-long v38, v28, v35

    or-long v36, v36, v38

    aput-wide v36, p3, p4

    .line 245
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v35, p1, v2

    move/from16 v0, v35

    and-int/lit16 v0, v0, 0xff

    move/from16 v35, v0

    move/from16 v0, v35

    int-to-long v0, v0

    move-wide/from16 v30, v0

    .line 246
    .local v30, "byte8":J
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v35, p1, p2

    move/from16 v0, v35

    and-int/lit16 v0, v0, 0xff

    move/from16 v35, v0

    move/from16 v0, v35

    int-to-long v0, v0

    move-wide/from16 v32, v0

    .line 247
    .local v32, "byte9":J
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v36, 0xf

    and-long v36, v36, v28

    const/16 v35, 0xb

    shl-long v36, v36, v35

    const/16 v35, 0x3

    shl-long v38, v30, v35

    or-long v36, v36, v38

    const/16 v35, 0x5

    ushr-long v38, v32, v35

    or-long v36, v36, v38

    aput-wide v36, p3, v34

    .line 248
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v35, p1, v2

    move/from16 v0, v35

    and-int/lit16 v0, v0, 0xff

    move/from16 v35, v0

    move/from16 v0, v35

    int-to-long v8, v0

    .line 249
    .local v8, "byte10":J
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v35, p1, p2

    move/from16 v0, v35

    and-int/lit16 v0, v0, 0xff

    move/from16 v35, v0

    move/from16 v0, v35

    int-to-long v10, v0

    .line 250
    .local v10, "byte11":J
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const-wide/16 v36, 0x1f

    and-long v36, v36, v32

    const/16 v35, 0xa

    shl-long v36, v36, v35

    const/16 v35, 0x2

    shl-long v38, v8, v35

    or-long v36, v36, v38

    const/16 v35, 0x6

    ushr-long v38, v10, v35

    or-long v36, v36, v38

    aput-wide v36, p3, p4

    .line 251
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v35, p1, v2

    move/from16 v0, v35

    and-int/lit16 v0, v0, 0xff

    move/from16 v35, v0

    move/from16 v0, v35

    int-to-long v12, v0

    .line 252
    .local v12, "byte12":J
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v35, p1, p2

    move/from16 v0, v35

    and-int/lit16 v0, v0, 0xff

    move/from16 v35, v0

    move/from16 v0, v35

    int-to-long v14, v0

    .line 253
    .local v14, "byte13":J
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v36, 0x3f

    and-long v36, v36, v10

    const/16 v35, 0x9

    shl-long v36, v36, v35

    const/16 v35, 0x1

    shl-long v38, v12, v35

    or-long v36, v36, v38

    const/16 v35, 0x7

    ushr-long v38, v14, v35

    or-long v36, v36, v38

    aput-wide v36, p3, v34

    .line 254
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v35, p1, v2

    move/from16 v0, v35

    and-int/lit16 v0, v0, 0xff

    move/from16 v35, v0

    move/from16 v0, v35

    int-to-long v0, v0

    move-wide/from16 v16, v0

    .line 255
    .local v16, "byte14":J
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const-wide/16 v36, 0x7f

    and-long v36, v36, v14

    const/16 v35, 0x8

    shl-long v36, v36, v35

    or-long v36, v36, v16

    aput-wide v36, p3, p4

    .line 232
    add-int/lit8 v3, v3, 0x1

    move/from16 v2, p2

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    goto/16 :goto_0
.end method

.method public decode([JI[III)V
    .locals 40
    .param p1, "blocks"    # [J
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [I
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 33
    const/16 v33, 0x0

    .local v33, "i":I
    move/from16 v34, p4

    .end local p4    # "valuesOffset":I
    .local v34, "valuesOffset":I
    move/from16 v32, p2

    .end local p2    # "blocksOffset":I
    .local v32, "blocksOffset":I
    :goto_0
    move/from16 v0, v33

    move/from16 v1, p5

    if-lt v0, v1, :cond_0

    .line 114
    return-void

    .line 34
    :cond_0
    add-int/lit8 p2, v32, 0x1

    .end local v32    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v2, p1, v32

    .line 35
    .local v2, "block0":J
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v35, 0x31

    ushr-long v36, v2, v35

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, v34

    .line 36
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const/16 v35, 0x22

    ushr-long v36, v2, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, p4

    .line 37
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v35, 0x13

    ushr-long v36, v2, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, v34

    .line 38
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const/16 v35, 0x4

    ushr-long v36, v2, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, p4

    .line 39
    add-int/lit8 v32, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v32    # "blocksOffset":I
    aget-wide v4, p1, p2

    .line 40
    .local v4, "block1":J
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v36, 0xf

    and-long v36, v36, v2

    const/16 v35, 0xb

    shl-long v36, v36, v35

    const/16 v35, 0x35

    ushr-long v38, v4, v35

    or-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, v34

    .line 41
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const/16 v35, 0x26

    ushr-long v36, v4, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, p4

    .line 42
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v35, 0x17

    ushr-long v36, v4, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, v34

    .line 43
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const/16 v35, 0x8

    ushr-long v36, v4, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, p4

    .line 44
    add-int/lit8 p2, v32, 0x1

    .end local v32    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v16, p1, v32

    .line 45
    .local v16, "block2":J
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v36, 0xff

    and-long v36, v36, v4

    const/16 v35, 0x7

    shl-long v36, v36, v35

    const/16 v35, 0x39

    ushr-long v38, v16, v35

    or-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, v34

    .line 46
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const/16 v35, 0x2a

    ushr-long v36, v16, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, p4

    .line 47
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v35, 0x1b

    ushr-long v36, v16, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, v34

    .line 48
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const/16 v35, 0xc

    ushr-long v36, v16, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, p4

    .line 49
    add-int/lit8 v32, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v32    # "blocksOffset":I
    aget-wide v18, p1, p2

    .line 50
    .local v18, "block3":J
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v36, 0xfff

    and-long v36, v36, v16

    const/16 v35, 0x3

    shl-long v36, v36, v35

    const/16 v35, 0x3d

    ushr-long v38, v18, v35

    or-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, v34

    .line 51
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const/16 v35, 0x2e

    ushr-long v36, v18, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, p4

    .line 52
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v35, 0x1f

    ushr-long v36, v18, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, v34

    .line 53
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const/16 v35, 0x10

    ushr-long v36, v18, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, p4

    .line 54
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v35, 0x1

    ushr-long v36, v18, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, v34

    .line 55
    add-int/lit8 p2, v32, 0x1

    .end local v32    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v20, p1, v32

    .line 56
    .local v20, "block4":J
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const-wide/16 v36, 0x1

    and-long v36, v36, v18

    const/16 v35, 0xe

    shl-long v36, v36, v35

    const/16 v35, 0x32

    ushr-long v38, v20, v35

    or-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, p4

    .line 57
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v35, 0x23

    ushr-long v36, v20, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, v34

    .line 58
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const/16 v35, 0x14

    ushr-long v36, v20, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, p4

    .line 59
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v35, 0x5

    ushr-long v36, v20, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, v34

    .line 60
    add-int/lit8 v32, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v32    # "blocksOffset":I
    aget-wide v22, p1, p2

    .line 61
    .local v22, "block5":J
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const-wide/16 v36, 0x1f

    and-long v36, v36, v20

    const/16 v35, 0xa

    shl-long v36, v36, v35

    const/16 v35, 0x36

    ushr-long v38, v22, v35

    or-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, p4

    .line 62
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v35, 0x27

    ushr-long v36, v22, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, v34

    .line 63
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const/16 v35, 0x18

    ushr-long v36, v22, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, p4

    .line 64
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v35, 0x9

    ushr-long v36, v22, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, v34

    .line 65
    add-int/lit8 p2, v32, 0x1

    .end local v32    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v24, p1, v32

    .line 66
    .local v24, "block6":J
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const-wide/16 v36, 0x1ff

    and-long v36, v36, v22

    const/16 v35, 0x6

    shl-long v36, v36, v35

    const/16 v35, 0x3a

    ushr-long v38, v24, v35

    or-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, p4

    .line 67
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v35, 0x2b

    ushr-long v36, v24, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, v34

    .line 68
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const/16 v35, 0x1c

    ushr-long v36, v24, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, p4

    .line 69
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v35, 0xd

    ushr-long v36, v24, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, v34

    .line 70
    add-int/lit8 v32, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v32    # "blocksOffset":I
    aget-wide v26, p1, p2

    .line 71
    .local v26, "block7":J
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const-wide/16 v36, 0x1fff

    and-long v36, v36, v24

    const/16 v35, 0x2

    shl-long v36, v36, v35

    const/16 v35, 0x3e

    ushr-long v38, v26, v35

    or-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, p4

    .line 72
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v35, 0x2f

    ushr-long v36, v26, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, v34

    .line 73
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const/16 v35, 0x20

    ushr-long v36, v26, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, p4

    .line 74
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v35, 0x11

    ushr-long v36, v26, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, v34

    .line 75
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const/16 v35, 0x2

    ushr-long v36, v26, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, p4

    .line 76
    add-int/lit8 p2, v32, 0x1

    .end local v32    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v28, p1, v32

    .line 77
    .local v28, "block8":J
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v36, 0x3

    and-long v36, v36, v26

    const/16 v35, 0xd

    shl-long v36, v36, v35

    const/16 v35, 0x33

    ushr-long v38, v28, v35

    or-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, v34

    .line 78
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const/16 v35, 0x24

    ushr-long v36, v28, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, p4

    .line 79
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v35, 0x15

    ushr-long v36, v28, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, v34

    .line 80
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const/16 v35, 0x6

    ushr-long v36, v28, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, p4

    .line 81
    add-int/lit8 v32, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v32    # "blocksOffset":I
    aget-wide v30, p1, p2

    .line 82
    .local v30, "block9":J
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v36, 0x3f

    and-long v36, v36, v28

    const/16 v35, 0x9

    shl-long v36, v36, v35

    const/16 v35, 0x37

    ushr-long v38, v30, v35

    or-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, v34

    .line 83
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const/16 v35, 0x28

    ushr-long v36, v30, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, p4

    .line 84
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v35, 0x19

    ushr-long v36, v30, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, v34

    .line 85
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const/16 v35, 0xa

    ushr-long v36, v30, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, p4

    .line 86
    add-int/lit8 p2, v32, 0x1

    .end local v32    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v6, p1, v32

    .line 87
    .local v6, "block10":J
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v36, 0x3ff

    and-long v36, v36, v30

    const/16 v35, 0x5

    shl-long v36, v36, v35

    const/16 v35, 0x3b

    ushr-long v38, v6, v35

    or-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, v34

    .line 88
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const/16 v35, 0x2c

    ushr-long v36, v6, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, p4

    .line 89
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v35, 0x1d

    ushr-long v36, v6, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, v34

    .line 90
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const/16 v35, 0xe

    ushr-long v36, v6, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, p4

    .line 91
    add-int/lit8 v32, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v32    # "blocksOffset":I
    aget-wide v8, p1, p2

    .line 92
    .local v8, "block11":J
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v36, 0x3fff

    and-long v36, v36, v6

    const/16 v35, 0x1

    shl-long v36, v36, v35

    const/16 v35, 0x3f

    ushr-long v38, v8, v35

    or-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, v34

    .line 93
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const/16 v35, 0x30

    ushr-long v36, v8, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, p4

    .line 94
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v35, 0x21

    ushr-long v36, v8, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, v34

    .line 95
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const/16 v35, 0x12

    ushr-long v36, v8, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, p4

    .line 96
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v35, 0x3

    ushr-long v36, v8, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, v34

    .line 97
    add-int/lit8 p2, v32, 0x1

    .end local v32    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v10, p1, v32

    .line 98
    .local v10, "block12":J
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const-wide/16 v36, 0x7

    and-long v36, v36, v8

    const/16 v35, 0xc

    shl-long v36, v36, v35

    const/16 v35, 0x34

    ushr-long v38, v10, v35

    or-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, p4

    .line 99
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v35, 0x25

    ushr-long v36, v10, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, v34

    .line 100
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const/16 v35, 0x16

    ushr-long v36, v10, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, p4

    .line 101
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v35, 0x7

    ushr-long v36, v10, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, v34

    .line 102
    add-int/lit8 v32, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v32    # "blocksOffset":I
    aget-wide v12, p1, p2

    .line 103
    .local v12, "block13":J
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const-wide/16 v36, 0x7f

    and-long v36, v36, v10

    const/16 v35, 0x8

    shl-long v36, v36, v35

    const/16 v35, 0x38

    ushr-long v38, v12, v35

    or-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, p4

    .line 104
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v35, 0x29

    ushr-long v36, v12, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, v34

    .line 105
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const/16 v35, 0x1a

    ushr-long v36, v12, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, p4

    .line 106
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v35, 0xb

    ushr-long v36, v12, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, v34

    .line 107
    add-int/lit8 p2, v32, 0x1

    .end local v32    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v14, p1, v32

    .line 108
    .local v14, "block14":J
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const-wide/16 v36, 0x7ff

    and-long v36, v36, v12

    const/16 v35, 0x4

    shl-long v36, v36, v35

    const/16 v35, 0x3c

    ushr-long v38, v14, v35

    or-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, p4

    .line 109
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v35, 0x2d

    ushr-long v36, v14, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, v34

    .line 110
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const/16 v35, 0x1e

    ushr-long v36, v14, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, p4

    .line 111
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v35, 0xf

    ushr-long v36, v14, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, v34

    .line 112
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const-wide/16 v36, 0x7fff

    and-long v36, v36, v14

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    aput v35, p3, p4

    .line 33
    add-int/lit8 v33, v33, 0x1

    move/from16 v32, p2

    .end local p2    # "blocksOffset":I
    .restart local v32    # "blocksOffset":I
    goto/16 :goto_0
.end method

.method public decode([JI[JII)V
    .locals 40
    .param p1, "blocks"    # [J
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [J
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 147
    const/16 v33, 0x0

    .local v33, "i":I
    move/from16 v34, p4

    .end local p4    # "valuesOffset":I
    .local v34, "valuesOffset":I
    move/from16 v32, p2

    .end local p2    # "blocksOffset":I
    .local v32, "blocksOffset":I
    :goto_0
    move/from16 v0, v33

    move/from16 v1, p5

    if-lt v0, v1, :cond_0

    .line 228
    return-void

    .line 148
    :cond_0
    add-int/lit8 p2, v32, 0x1

    .end local v32    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v2, p1, v32

    .line 149
    .local v2, "block0":J
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v35, 0x31

    ushr-long v36, v2, v35

    aput-wide v36, p3, v34

    .line 150
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const/16 v35, 0x22

    ushr-long v36, v2, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    aput-wide v36, p3, p4

    .line 151
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v35, 0x13

    ushr-long v36, v2, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    aput-wide v36, p3, v34

    .line 152
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const/16 v35, 0x4

    ushr-long v36, v2, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    aput-wide v36, p3, p4

    .line 153
    add-int/lit8 v32, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v32    # "blocksOffset":I
    aget-wide v4, p1, p2

    .line 154
    .local v4, "block1":J
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v36, 0xf

    and-long v36, v36, v2

    const/16 v35, 0xb

    shl-long v36, v36, v35

    const/16 v35, 0x35

    ushr-long v38, v4, v35

    or-long v36, v36, v38

    aput-wide v36, p3, v34

    .line 155
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const/16 v35, 0x26

    ushr-long v36, v4, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    aput-wide v36, p3, p4

    .line 156
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v35, 0x17

    ushr-long v36, v4, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    aput-wide v36, p3, v34

    .line 157
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const/16 v35, 0x8

    ushr-long v36, v4, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    aput-wide v36, p3, p4

    .line 158
    add-int/lit8 p2, v32, 0x1

    .end local v32    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v16, p1, v32

    .line 159
    .local v16, "block2":J
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v36, 0xff

    and-long v36, v36, v4

    const/16 v35, 0x7

    shl-long v36, v36, v35

    const/16 v35, 0x39

    ushr-long v38, v16, v35

    or-long v36, v36, v38

    aput-wide v36, p3, v34

    .line 160
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const/16 v35, 0x2a

    ushr-long v36, v16, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    aput-wide v36, p3, p4

    .line 161
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v35, 0x1b

    ushr-long v36, v16, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    aput-wide v36, p3, v34

    .line 162
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const/16 v35, 0xc

    ushr-long v36, v16, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    aput-wide v36, p3, p4

    .line 163
    add-int/lit8 v32, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v32    # "blocksOffset":I
    aget-wide v18, p1, p2

    .line 164
    .local v18, "block3":J
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v36, 0xfff

    and-long v36, v36, v16

    const/16 v35, 0x3

    shl-long v36, v36, v35

    const/16 v35, 0x3d

    ushr-long v38, v18, v35

    or-long v36, v36, v38

    aput-wide v36, p3, v34

    .line 165
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const/16 v35, 0x2e

    ushr-long v36, v18, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    aput-wide v36, p3, p4

    .line 166
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v35, 0x1f

    ushr-long v36, v18, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    aput-wide v36, p3, v34

    .line 167
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const/16 v35, 0x10

    ushr-long v36, v18, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    aput-wide v36, p3, p4

    .line 168
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v35, 0x1

    ushr-long v36, v18, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    aput-wide v36, p3, v34

    .line 169
    add-int/lit8 p2, v32, 0x1

    .end local v32    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v20, p1, v32

    .line 170
    .local v20, "block4":J
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const-wide/16 v36, 0x1

    and-long v36, v36, v18

    const/16 v35, 0xe

    shl-long v36, v36, v35

    const/16 v35, 0x32

    ushr-long v38, v20, v35

    or-long v36, v36, v38

    aput-wide v36, p3, p4

    .line 171
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v35, 0x23

    ushr-long v36, v20, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    aput-wide v36, p3, v34

    .line 172
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const/16 v35, 0x14

    ushr-long v36, v20, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    aput-wide v36, p3, p4

    .line 173
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v35, 0x5

    ushr-long v36, v20, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    aput-wide v36, p3, v34

    .line 174
    add-int/lit8 v32, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v32    # "blocksOffset":I
    aget-wide v22, p1, p2

    .line 175
    .local v22, "block5":J
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const-wide/16 v36, 0x1f

    and-long v36, v36, v20

    const/16 v35, 0xa

    shl-long v36, v36, v35

    const/16 v35, 0x36

    ushr-long v38, v22, v35

    or-long v36, v36, v38

    aput-wide v36, p3, p4

    .line 176
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v35, 0x27

    ushr-long v36, v22, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    aput-wide v36, p3, v34

    .line 177
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const/16 v35, 0x18

    ushr-long v36, v22, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    aput-wide v36, p3, p4

    .line 178
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v35, 0x9

    ushr-long v36, v22, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    aput-wide v36, p3, v34

    .line 179
    add-int/lit8 p2, v32, 0x1

    .end local v32    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v24, p1, v32

    .line 180
    .local v24, "block6":J
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const-wide/16 v36, 0x1ff

    and-long v36, v36, v22

    const/16 v35, 0x6

    shl-long v36, v36, v35

    const/16 v35, 0x3a

    ushr-long v38, v24, v35

    or-long v36, v36, v38

    aput-wide v36, p3, p4

    .line 181
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v35, 0x2b

    ushr-long v36, v24, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    aput-wide v36, p3, v34

    .line 182
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const/16 v35, 0x1c

    ushr-long v36, v24, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    aput-wide v36, p3, p4

    .line 183
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v35, 0xd

    ushr-long v36, v24, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    aput-wide v36, p3, v34

    .line 184
    add-int/lit8 v32, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v32    # "blocksOffset":I
    aget-wide v26, p1, p2

    .line 185
    .local v26, "block7":J
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const-wide/16 v36, 0x1fff

    and-long v36, v36, v24

    const/16 v35, 0x2

    shl-long v36, v36, v35

    const/16 v35, 0x3e

    ushr-long v38, v26, v35

    or-long v36, v36, v38

    aput-wide v36, p3, p4

    .line 186
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v35, 0x2f

    ushr-long v36, v26, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    aput-wide v36, p3, v34

    .line 187
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const/16 v35, 0x20

    ushr-long v36, v26, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    aput-wide v36, p3, p4

    .line 188
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v35, 0x11

    ushr-long v36, v26, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    aput-wide v36, p3, v34

    .line 189
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const/16 v35, 0x2

    ushr-long v36, v26, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    aput-wide v36, p3, p4

    .line 190
    add-int/lit8 p2, v32, 0x1

    .end local v32    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v28, p1, v32

    .line 191
    .local v28, "block8":J
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v36, 0x3

    and-long v36, v36, v26

    const/16 v35, 0xd

    shl-long v36, v36, v35

    const/16 v35, 0x33

    ushr-long v38, v28, v35

    or-long v36, v36, v38

    aput-wide v36, p3, v34

    .line 192
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const/16 v35, 0x24

    ushr-long v36, v28, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    aput-wide v36, p3, p4

    .line 193
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v35, 0x15

    ushr-long v36, v28, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    aput-wide v36, p3, v34

    .line 194
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const/16 v35, 0x6

    ushr-long v36, v28, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    aput-wide v36, p3, p4

    .line 195
    add-int/lit8 v32, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v32    # "blocksOffset":I
    aget-wide v30, p1, p2

    .line 196
    .local v30, "block9":J
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v36, 0x3f

    and-long v36, v36, v28

    const/16 v35, 0x9

    shl-long v36, v36, v35

    const/16 v35, 0x37

    ushr-long v38, v30, v35

    or-long v36, v36, v38

    aput-wide v36, p3, v34

    .line 197
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const/16 v35, 0x28

    ushr-long v36, v30, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    aput-wide v36, p3, p4

    .line 198
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v35, 0x19

    ushr-long v36, v30, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    aput-wide v36, p3, v34

    .line 199
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const/16 v35, 0xa

    ushr-long v36, v30, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    aput-wide v36, p3, p4

    .line 200
    add-int/lit8 p2, v32, 0x1

    .end local v32    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v6, p1, v32

    .line 201
    .local v6, "block10":J
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v36, 0x3ff

    and-long v36, v36, v30

    const/16 v35, 0x5

    shl-long v36, v36, v35

    const/16 v35, 0x3b

    ushr-long v38, v6, v35

    or-long v36, v36, v38

    aput-wide v36, p3, v34

    .line 202
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const/16 v35, 0x2c

    ushr-long v36, v6, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    aput-wide v36, p3, p4

    .line 203
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v35, 0x1d

    ushr-long v36, v6, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    aput-wide v36, p3, v34

    .line 204
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const/16 v35, 0xe

    ushr-long v36, v6, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    aput-wide v36, p3, p4

    .line 205
    add-int/lit8 v32, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v32    # "blocksOffset":I
    aget-wide v8, p1, p2

    .line 206
    .local v8, "block11":J
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v36, 0x3fff

    and-long v36, v36, v6

    const/16 v35, 0x1

    shl-long v36, v36, v35

    const/16 v35, 0x3f

    ushr-long v38, v8, v35

    or-long v36, v36, v38

    aput-wide v36, p3, v34

    .line 207
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const/16 v35, 0x30

    ushr-long v36, v8, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    aput-wide v36, p3, p4

    .line 208
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v35, 0x21

    ushr-long v36, v8, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    aput-wide v36, p3, v34

    .line 209
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const/16 v35, 0x12

    ushr-long v36, v8, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    aput-wide v36, p3, p4

    .line 210
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v35, 0x3

    ushr-long v36, v8, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    aput-wide v36, p3, v34

    .line 211
    add-int/lit8 p2, v32, 0x1

    .end local v32    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v10, p1, v32

    .line 212
    .local v10, "block12":J
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const-wide/16 v36, 0x7

    and-long v36, v36, v8

    const/16 v35, 0xc

    shl-long v36, v36, v35

    const/16 v35, 0x34

    ushr-long v38, v10, v35

    or-long v36, v36, v38

    aput-wide v36, p3, p4

    .line 213
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v35, 0x25

    ushr-long v36, v10, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    aput-wide v36, p3, v34

    .line 214
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const/16 v35, 0x16

    ushr-long v36, v10, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    aput-wide v36, p3, p4

    .line 215
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v35, 0x7

    ushr-long v36, v10, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    aput-wide v36, p3, v34

    .line 216
    add-int/lit8 v32, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v32    # "blocksOffset":I
    aget-wide v12, p1, p2

    .line 217
    .local v12, "block13":J
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const-wide/16 v36, 0x7f

    and-long v36, v36, v10

    const/16 v35, 0x8

    shl-long v36, v36, v35

    const/16 v35, 0x38

    ushr-long v38, v12, v35

    or-long v36, v36, v38

    aput-wide v36, p3, p4

    .line 218
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v35, 0x29

    ushr-long v36, v12, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    aput-wide v36, p3, v34

    .line 219
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const/16 v35, 0x1a

    ushr-long v36, v12, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    aput-wide v36, p3, p4

    .line 220
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v35, 0xb

    ushr-long v36, v12, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    aput-wide v36, p3, v34

    .line 221
    add-int/lit8 p2, v32, 0x1

    .end local v32    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v14, p1, v32

    .line 222
    .local v14, "block14":J
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const-wide/16 v36, 0x7ff

    and-long v36, v36, v12

    const/16 v35, 0x4

    shl-long v36, v36, v35

    const/16 v35, 0x3c

    ushr-long v38, v14, v35

    or-long v36, v36, v38

    aput-wide v36, p3, p4

    .line 223
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v35, 0x2d

    ushr-long v36, v14, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    aput-wide v36, p3, v34

    .line 224
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const/16 v35, 0x1e

    ushr-long v36, v14, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    aput-wide v36, p3, p4

    .line 225
    add-int/lit8 p4, v34, 0x1

    .end local v34    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v35, 0xf

    ushr-long v36, v14, v35

    const-wide/16 v38, 0x7fff

    and-long v36, v36, v38

    aput-wide v36, p3, v34

    .line 226
    add-int/lit8 v34, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v34    # "valuesOffset":I
    const-wide/16 v36, 0x7fff

    and-long v36, v36, v14

    aput-wide v36, p3, p4

    .line 147
    add-int/lit8 v33, v33, 0x1

    move/from16 v32, p2

    .end local p2    # "blocksOffset":I
    .restart local v32    # "blocksOffset":I
    goto/16 :goto_0
.end method
