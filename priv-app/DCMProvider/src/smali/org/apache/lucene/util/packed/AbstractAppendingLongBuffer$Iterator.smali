.class abstract Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer$Iterator;
.super Ljava/lang/Object;
.source "AbstractAppendingLongBuffer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x400
    name = "Iterator"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field currentValues:[J

.field pOff:I

.field final synthetic this$0:Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;

.field vOff:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 92
    const-class v0, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer$Iterator;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;)V
    .locals 1

    .prologue
    .line 97
    iput-object p1, p0, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer$Iterator;->this$0:Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer$Iterator;->pOff:I

    iput v0, p0, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer$Iterator;->vOff:I

    .line 99
    iget v0, p1, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;->valuesOff:I

    if-nez v0, :cond_0

    .line 100
    iget-object v0, p1, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;->pending:[J

    iput-object v0, p0, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer$Iterator;->currentValues:[J

    .line 105
    :goto_0
    return-void

    .line 102
    :cond_0
    const/16 v0, 0x400

    new-array v0, v0, [J

    iput-object v0, p0, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer$Iterator;->currentValues:[J

    .line 103
    invoke-virtual {p0}, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer$Iterator;->fillValues()V

    goto :goto_0
.end method


# virtual methods
.method abstract fillValues()V
.end method

.method public final hasNext()Z
    .locals 2

    .prologue
    .line 111
    iget v0, p0, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer$Iterator;->vOff:I

    iget-object v1, p0, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer$Iterator;->this$0:Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;

    iget v1, v1, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;->valuesOff:I

    if-lt v0, v1, :cond_1

    iget v0, p0, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer$Iterator;->vOff:I

    iget-object v1, p0, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer$Iterator;->this$0:Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;

    iget v1, v1, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;->valuesOff:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer$Iterator;->pOff:I

    iget-object v1, p0, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer$Iterator;->this$0:Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;

    iget v1, v1, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;->pendingOff:I

    if-lt v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final next()J
    .locals 5

    .prologue
    .line 116
    sget-boolean v2, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer$Iterator;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer$Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 117
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer$Iterator;->currentValues:[J

    iget v3, p0, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer$Iterator;->pOff:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer$Iterator;->pOff:I

    aget-wide v0, v2, v3

    .line 118
    .local v0, "result":J
    iget v2, p0, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer$Iterator;->pOff:I

    const/16 v3, 0x400

    if-ne v2, v3, :cond_1

    .line 119
    iget v2, p0, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer$Iterator;->vOff:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer$Iterator;->vOff:I

    .line 120
    const/4 v2, 0x0

    iput v2, p0, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer$Iterator;->pOff:I

    .line 121
    iget v2, p0, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer$Iterator;->vOff:I

    iget-object v3, p0, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer$Iterator;->this$0:Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;

    iget v3, v3, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;->valuesOff:I

    if-gt v2, v3, :cond_1

    .line 122
    invoke-virtual {p0}, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer$Iterator;->fillValues()V

    .line 125
    :cond_1
    return-wide v0
.end method
