.class public Lorg/apache/lucene/util/mutable/MutableValueDate;
.super Lorg/apache/lucene/util/mutable/MutableValueLong;
.source "MutableValueDate.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lorg/apache/lucene/util/mutable/MutableValueLong;-><init>()V

    return-void
.end method


# virtual methods
.method public duplicate()Lorg/apache/lucene/util/mutable/MutableValue;
    .locals 4

    .prologue
    .line 33
    new-instance v0, Lorg/apache/lucene/util/mutable/MutableValueDate;

    invoke-direct {v0}, Lorg/apache/lucene/util/mutable/MutableValueDate;-><init>()V

    .line 34
    .local v0, "v":Lorg/apache/lucene/util/mutable/MutableValueDate;
    iget-wide v2, p0, Lorg/apache/lucene/util/mutable/MutableValueDate;->value:J

    iput-wide v2, v0, Lorg/apache/lucene/util/mutable/MutableValueDate;->value:J

    .line 35
    iget-boolean v1, p0, Lorg/apache/lucene/util/mutable/MutableValueDate;->exists:Z

    iput-boolean v1, v0, Lorg/apache/lucene/util/mutable/MutableValueDate;->exists:Z

    .line 36
    return-object v0
.end method

.method public toObject()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 28
    iget-boolean v0, p0, Lorg/apache/lucene/util/mutable/MutableValueDate;->exists:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/Date;

    iget-wide v2, p0, Lorg/apache/lucene/util/mutable/MutableValueDate;->value:J

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
