.class public final Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;
.super Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;
.source "MonotonicAppendingLongBuffer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer$Iterator;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private averages:[F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/16 v0, 0x10

    .line 44
    invoke-direct {p0, v0}, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;-><init>(I)V

    .line 45
    new-array v0, v0, [F

    iput-object v0, p0, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;->averages:[F

    .line 46
    return-void
.end method

.method static synthetic access$1(Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;)[F
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;->averages:[F

    return-object v0
.end method

.method static zigZagDecode(J)J
    .locals 4
    .param p0, "n"    # J

    .prologue
    .line 33
    const/4 v0, 0x1

    ushr-long v0, p0, v0

    const-wide/16 v2, 0x1

    and-long/2addr v2, p0

    neg-long v2, v2

    xor-long/2addr v0, v2

    return-wide v0
.end method

.method static zigZagEncode(J)J
    .locals 4
    .param p0, "n"    # J

    .prologue
    .line 37
    const/16 v0, 0x3f

    shr-long v0, p0, v0

    const/4 v2, 0x1

    shl-long v2, p0, v2

    xor-long/2addr v0, v2

    return-wide v0
.end method


# virtual methods
.method baseRamBytesUsed()J
    .locals 4

    .prologue
    .line 129
    invoke-super {p0}, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;->baseRamBytesUsed()J

    move-result-wide v0

    .line 130
    sget v2, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_REF:I

    int-to-long v2, v2

    .line 129
    add-long/2addr v0, v2

    return-wide v0
.end method

.method get(II)J
    .locals 8
    .param p1, "block"    # I
    .param p2, "element"    # I

    .prologue
    .line 49
    iget v2, p0, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;->valuesOff:I

    if-ne p1, v2, :cond_1

    .line 50
    iget-object v2, p0, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;->pending:[J

    aget-wide v0, v2, p2

    .line 56
    :cond_0
    :goto_0
    return-wide v0

    .line 52
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;->minValues:[J

    aget-wide v2, v2, p1

    iget-object v4, p0, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;->averages:[F

    aget v4, v4, p1

    int-to-long v6, p2

    long-to-float v5, v6

    mul-float/2addr v4, v5

    float-to-long v4, v4

    add-long v0, v2, v4

    .line 53
    .local v0, "base":J
    iget-object v2, p0, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;->deltas:[Lorg/apache/lucene/util/packed/PackedInts$Reader;

    aget-object v2, v2, p1

    if-eqz v2, :cond_0

    .line 56
    iget-object v2, p0, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;->deltas:[Lorg/apache/lucene/util/packed/PackedInts$Reader;

    aget-object v2, v2, p1

    invoke-interface {v2, p2}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;->zigZagDecode(J)J

    move-result-wide v2

    add-long/2addr v0, v2

    goto :goto_0
.end method

.method grow(I)V
    .locals 1
    .param p1, "newBlockCount"    # I

    .prologue
    .line 63
    invoke-super {p0, p1}, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;->grow(I)V

    .line 64
    iget-object v0, p0, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;->averages:[F

    invoke-static {v0, p1}, Ljava/util/Arrays;->copyOf([FI)[F

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;->averages:[F

    .line 65
    return-void
.end method

.method public bridge synthetic iterator()Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer$Iterator;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;->iterator()Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer$Iterator;

    move-result-object v0

    return-object v0
.end method

.method public iterator()Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer$Iterator;
    .locals 1

    .prologue
    .line 98
    new-instance v0, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer$Iterator;

    invoke-direct {v0, p0}, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer$Iterator;-><init>(Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;)V

    return-object v0
.end method

.method packPendingValues()V
    .locals 12

    .prologue
    .line 69
    sget-boolean v5, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;->$assertionsDisabled:Z

    if-nez v5, :cond_0

    iget v5, p0, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;->pendingOff:I

    const/16 v6, 0x400

    if-eq v5, v6, :cond_0

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 71
    :cond_0
    iget-object v5, p0, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;->minValues:[J

    iget v6, p0, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;->valuesOff:I

    iget-object v7, p0, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;->pending:[J

    const/4 v8, 0x0

    aget-wide v8, v7, v8

    aput-wide v8, v5, v6

    .line 72
    iget-object v5, p0, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;->averages:[F

    iget v6, p0, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;->valuesOff:I

    iget-object v7, p0, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;->pending:[J

    const/16 v8, 0x3ff

    aget-wide v8, v7, v8

    iget-object v7, p0, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;->pending:[J

    const/4 v10, 0x0

    aget-wide v10, v7, v10

    sub-long/2addr v8, v10

    long-to-float v7, v8

    const v8, 0x447fc000    # 1023.0f

    div-float/2addr v7, v8

    aput v7, v5, v6

    .line 74
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/16 v5, 0x400

    if-lt v1, v5, :cond_2

    .line 77
    const-wide/16 v2, 0x0

    .line 78
    .local v2, "maxDelta":J
    const/4 v1, 0x0

    :goto_1
    const/16 v5, 0x400

    if-lt v1, v5, :cond_3

    .line 86
    :goto_2
    const-wide/16 v6, 0x0

    cmp-long v5, v2, v6

    if-eqz v5, :cond_1

    .line 87
    const-wide/16 v6, 0x0

    cmp-long v5, v2, v6

    if-gez v5, :cond_5

    const/16 v0, 0x40

    .line 88
    .local v0, "bitsRequired":I
    :goto_3
    iget v5, p0, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;->pendingOff:I

    const/4 v6, 0x0

    invoke-static {v5, v0, v6}, Lorg/apache/lucene/util/packed/PackedInts;->getMutable(IIF)Lorg/apache/lucene/util/packed/PackedInts$Mutable;

    move-result-object v4

    .line 89
    .local v4, "mutable":Lorg/apache/lucene/util/packed/PackedInts$Mutable;
    const/4 v1, 0x0

    :goto_4
    iget v5, p0, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;->pendingOff:I

    if-lt v1, v5, :cond_6

    .line 92
    iget-object v5, p0, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;->deltas:[Lorg/apache/lucene/util/packed/PackedInts$Reader;

    iget v6, p0, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;->valuesOff:I

    aput-object v4, v5, v6

    .line 94
    .end local v0    # "bitsRequired":I
    .end local v4    # "mutable":Lorg/apache/lucene/util/packed/PackedInts$Mutable;
    :cond_1
    return-void

    .line 75
    .end local v2    # "maxDelta":J
    :cond_2
    iget-object v5, p0, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;->pending:[J

    iget-object v6, p0, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;->pending:[J

    aget-wide v6, v6, v1

    iget-object v8, p0, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;->minValues:[J

    iget v9, p0, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;->valuesOff:I

    aget-wide v8, v8, v9

    sub-long/2addr v6, v8

    iget-object v8, p0, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;->averages:[F

    iget v9, p0, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;->valuesOff:I

    aget v8, v8, v9

    int-to-long v10, v1

    long-to-float v9, v10

    mul-float/2addr v8, v9

    float-to-long v8, v8

    sub-long/2addr v6, v8

    invoke-static {v6, v7}, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;->zigZagEncode(J)J

    move-result-wide v6

    aput-wide v6, v5, v1

    .line 74
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 79
    .restart local v2    # "maxDelta":J
    :cond_3
    iget-object v5, p0, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;->pending:[J

    aget-wide v6, v5, v1

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-gez v5, :cond_4

    .line 80
    const-wide/16 v2, -0x1

    .line 81
    goto :goto_2

    .line 83
    :cond_4
    iget-object v5, p0, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;->pending:[J

    aget-wide v6, v5, v1

    invoke-static {v2, v3, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 78
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 87
    :cond_5
    invoke-static {v2, v3}, Lorg/apache/lucene/util/packed/PackedInts;->bitsRequired(J)I

    move-result v0

    goto :goto_3

    .line 90
    .restart local v0    # "bitsRequired":I
    .restart local v4    # "mutable":Lorg/apache/lucene/util/packed/PackedInts$Mutable;
    :cond_6
    iget-object v5, p0, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;->pending:[J

    iget v6, p0, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;->pendingOff:I

    sub-int/2addr v6, v1

    invoke-interface {v4, v1, v5, v1, v6}, Lorg/apache/lucene/util/packed/PackedInts$Mutable;->set(I[JII)I

    move-result v5

    add-int/2addr v1, v5

    goto :goto_4
.end method

.method public ramBytesUsed()J
    .locals 4

    .prologue
    .line 135
    invoke-super {p0}, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;->ramBytesUsed()J

    move-result-wide v0

    .line 136
    iget-object v2, p0, Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;->averages:[F

    invoke-static {v2}, Lorg/apache/lucene/util/RamUsageEstimator;->sizeOf([F)J

    move-result-wide v2

    .line 135
    add-long/2addr v0, v2

    return-wide v0
.end method
