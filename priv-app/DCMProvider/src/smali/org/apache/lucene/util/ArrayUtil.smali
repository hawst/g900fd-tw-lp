.class public final Lorg/apache/lucene/util/ArrayUtil;
.super Ljava/lang/Object;
.source "ArrayUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/ArrayUtil$ArrayMergeSorterTemplate;,
        Lorg/apache/lucene/util/ArrayUtil$ArraySorterTemplate;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field static final MERGE_EXTRA_MEMORY_THRESHOLD:I = 0x5dc

.field static final MERGE_OVERHEAD_RATIO:F = 0.01f


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lorg/apache/lucene/util/ArrayUtil;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/ArrayUtil;->$assertionsDisabled:Z

    .line 34
    return-void

    .line 29
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static binarySort([Ljava/lang/Comparable;)V
    .locals 2
    .param p0, "a"    # [Ljava/lang/Comparable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/lang/Comparable",
            "<-TT;>;>([TT;)V"
        }
    .end annotation

    .prologue
    .line 946
    const/4 v0, 0x0

    array-length v1, p0

    invoke-static {p0, v0, v1}, Lorg/apache/lucene/util/ArrayUtil;->binarySort([Ljava/lang/Comparable;II)V

    .line 947
    return-void
.end method

.method public static binarySort([Ljava/lang/Comparable;II)V
    .locals 2
    .param p0, "a"    # [Ljava/lang/Comparable;
    .param p1, "fromIndex"    # I
    .param p2, "toIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/lang/Comparable",
            "<-TT;>;>([TT;II)V"
        }
    .end annotation

    .prologue
    .line 937
    sub-int v0, p2, p1

    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    .line 939
    :goto_0
    return-void

    .line 938
    :cond_0
    invoke-static {p0}, Lorg/apache/lucene/util/ArrayUtil;->getSorter([Ljava/lang/Comparable;)Lorg/apache/lucene/util/SorterTemplate;

    move-result-object v0

    add-int/lit8 v1, p2, -0x1

    invoke-virtual {v0, p1, v1}, Lorg/apache/lucene/util/SorterTemplate;->binarySort(II)V

    goto :goto_0
.end method

.method public static binarySort([Ljava/lang/Object;IILjava/util/Comparator;)V
    .locals 2
    .param p0, "a"    # [Ljava/lang/Object;
    .param p1, "fromIndex"    # I
    .param p2, "toIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;II",
            "Ljava/util/Comparator",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 918
    .local p3, "comp":Ljava/util/Comparator;, "Ljava/util/Comparator<-TT;>;"
    sub-int v0, p2, p1

    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    .line 920
    :goto_0
    return-void

    .line 919
    :cond_0
    invoke-static {p0, p3}, Lorg/apache/lucene/util/ArrayUtil;->getSorter([Ljava/lang/Object;Ljava/util/Comparator;)Lorg/apache/lucene/util/SorterTemplate;

    move-result-object v0

    add-int/lit8 v1, p2, -0x1

    invoke-virtual {v0, p1, v1}, Lorg/apache/lucene/util/SorterTemplate;->binarySort(II)V

    goto :goto_0
.end method

.method public static binarySort([Ljava/lang/Object;Ljava/util/Comparator;)V
    .locals 2
    .param p0, "a"    # [Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;",
            "Ljava/util/Comparator",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 927
    .local p1, "comp":Ljava/util/Comparator;, "Ljava/util/Comparator<-TT;>;"
    const/4 v0, 0x0

    array-length v1, p0

    invoke-static {p0, v0, v1, p1}, Lorg/apache/lucene/util/ArrayUtil;->binarySort([Ljava/lang/Object;IILjava/util/Comparator;)V

    .line 928
    return-void
.end method

.method public static equals([BI[BII)Z
    .locals 4
    .param p0, "left"    # [B
    .param p1, "offsetLeft"    # I
    .param p2, "right"    # [B
    .param p3, "offsetRight"    # I
    .param p4, "length"    # I

    .prologue
    const/4 v1, 0x0

    .line 532
    add-int v2, p1, p4

    array-length v3, p0

    if-gt v2, v3, :cond_0

    add-int v2, p3, p4

    array-length v3, p2

    if-gt v2, v3, :cond_0

    .line 533
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, p4, :cond_1

    .line 539
    const/4 v1, 0x1

    .line 541
    .end local v0    # "i":I
    :cond_0
    return v1

    .line 534
    .restart local v0    # "i":I
    :cond_1
    add-int v2, p1, v0

    aget-byte v2, p0, v2

    add-int v3, p3, v0

    aget-byte v3, p2, v3

    if-ne v2, v3, :cond_0

    .line 533
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static equals([CI[CII)Z
    .locals 4
    .param p0, "left"    # [C
    .param p1, "offsetLeft"    # I
    .param p2, "right"    # [C
    .param p3, "offsetRight"    # I
    .param p4, "length"    # I

    .prologue
    const/4 v1, 0x0

    .line 506
    add-int v2, p1, p4

    array-length v3, p0

    if-gt v2, v3, :cond_0

    add-int v2, p3, p4

    array-length v3, p2

    if-gt v2, v3, :cond_0

    .line 507
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, p4, :cond_1

    .line 513
    const/4 v1, 0x1

    .line 515
    .end local v0    # "i":I
    :cond_0
    return v1

    .line 508
    .restart local v0    # "i":I
    :cond_1
    add-int v2, p1, v0

    aget-char v2, p0, v2

    add-int v3, p3, v0

    aget-char v3, p2, v3

    if-ne v2, v3, :cond_0

    .line 507
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static equals([II[III)Z
    .locals 4
    .param p0, "left"    # [I
    .param p1, "offsetLeft"    # I
    .param p2, "right"    # [I
    .param p3, "offsetRight"    # I
    .param p4, "length"    # I

    .prologue
    const/4 v1, 0x0

    .line 587
    add-int v2, p1, p4

    array-length v3, p0

    if-gt v2, v3, :cond_0

    add-int v2, p3, p4

    array-length v3, p2

    if-gt v2, v3, :cond_0

    .line 588
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, p4, :cond_1

    .line 594
    const/4 v1, 0x1

    .line 596
    .end local v0    # "i":I
    :cond_0
    return v1

    .line 589
    .restart local v0    # "i":I
    :cond_1
    add-int v2, p1, v0

    aget v2, p0, v2

    add-int v3, p3, v0

    aget v3, p2, v3

    if-ne v2, v3, :cond_0

    .line 588
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private static getMergeSorter([Ljava/lang/Comparable;)Lorg/apache/lucene/util/SorterTemplate;
    .locals 2
    .param p0, "a"    # [Ljava/lang/Comparable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/lang/Comparable",
            "<-TT;>;>([TT;)",
            "Lorg/apache/lucene/util/SorterTemplate;"
        }
    .end annotation

    .prologue
    .line 734
    array-length v0, p0

    const/16 v1, 0x5dc

    if-ge v0, v1, :cond_0

    .line 735
    invoke-static {p0}, Lorg/apache/lucene/util/ArrayUtil;->getSorter([Ljava/lang/Comparable;)Lorg/apache/lucene/util/SorterTemplate;

    move-result-object v0

    .line 737
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/apache/lucene/util/ArrayUtil$4;

    const v1, 0x3c23d70a    # 0.01f

    invoke-direct {v0, p0, v1}, Lorg/apache/lucene/util/ArrayUtil$4;-><init>([Ljava/lang/Comparable;F)V

    goto :goto_0
.end method

.method private static getMergeSorter([Ljava/lang/Object;Ljava/util/Comparator;)Lorg/apache/lucene/util/SorterTemplate;
    .locals 2
    .param p0, "a"    # [Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;",
            "Ljava/util/Comparator",
            "<-TT;>;)",
            "Lorg/apache/lucene/util/SorterTemplate;"
        }
    .end annotation

    .prologue
    .line 718
    .local p1, "comp":Ljava/util/Comparator;, "Ljava/util/Comparator<-TT;>;"
    array-length v0, p0

    const/16 v1, 0x5dc

    if-ge v0, v1, :cond_0

    .line 719
    invoke-static {p0, p1}, Lorg/apache/lucene/util/ArrayUtil;->getSorter([Ljava/lang/Object;Ljava/util/Comparator;)Lorg/apache/lucene/util/SorterTemplate;

    move-result-object v0

    .line 721
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/apache/lucene/util/ArrayUtil$3;

    const v1, 0x3c23d70a    # 0.01f

    invoke-direct {v0, p0, v1, p1}, Lorg/apache/lucene/util/ArrayUtil$3;-><init>([Ljava/lang/Object;FLjava/util/Comparator;)V

    goto :goto_0
.end method

.method public static getShrinkSize(III)I
    .locals 2
    .param p0, "currentSize"    # I
    .param p1, "targetSize"    # I
    .param p2, "bytesPerElement"    # I

    .prologue
    .line 227
    invoke-static {p1, p2}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v0

    .line 231
    .local v0, "newSize":I
    div-int/lit8 v1, p0, 0x2

    if-ge v0, v1, :cond_0

    .line 234
    .end local v0    # "newSize":I
    :goto_0
    return v0

    .restart local v0    # "newSize":I
    :cond_0
    move v0, p0

    goto :goto_0
.end method

.method private static getSorter([Ljava/lang/Comparable;)Lorg/apache/lucene/util/SorterTemplate;
    .locals 1
    .param p0, "a"    # [Ljava/lang/Comparable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/lang/Comparable",
            "<-TT;>;>([TT;)",
            "Lorg/apache/lucene/util/SorterTemplate;"
        }
    .end annotation

    .prologue
    .line 706
    new-instance v0, Lorg/apache/lucene/util/ArrayUtil$2;

    invoke-direct {v0, p0}, Lorg/apache/lucene/util/ArrayUtil$2;-><init>([Ljava/lang/Comparable;)V

    return-object v0
.end method

.method private static getSorter([Ljava/lang/Object;Ljava/util/Comparator;)Lorg/apache/lucene/util/SorterTemplate;
    .locals 1
    .param p0, "a"    # [Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;",
            "Ljava/util/Comparator",
            "<-TT;>;)",
            "Lorg/apache/lucene/util/SorterTemplate;"
        }
    .end annotation

    .prologue
    .line 694
    .local p1, "comp":Ljava/util/Comparator;, "Ljava/util/Comparator<-TT;>;"
    new-instance v0, Lorg/apache/lucene/util/ArrayUtil$1;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/util/ArrayUtil$1;-><init>([Ljava/lang/Object;Ljava/util/Comparator;)V

    return-object v0
.end method

.method public static grow([B)[B
    .locals 1
    .param p0, "array"    # [B

    .prologue
    .line 351
    array-length v0, p0

    add-int/lit8 v0, v0, 0x1

    invoke-static {p0, v0}, Lorg/apache/lucene/util/ArrayUtil;->grow([BI)[B

    move-result-object v0

    return-object v0
.end method

.method public static grow([BI)[B
    .locals 4
    .param p0, "array"    # [B
    .param p1, "minSize"    # I

    .prologue
    const/4 v2, 0x0

    .line 341
    sget-boolean v1, Lorg/apache/lucene/util/ArrayUtil;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-gez p1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "size must be positive (got "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "): likely integer overflow?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 342
    :cond_0
    array-length v1, p0

    if-ge v1, p1, :cond_1

    .line 343
    const/4 v1, 0x1

    invoke-static {p1, v1}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v1

    new-array v0, v1, [B

    .line 344
    .local v0, "newArray":[B
    array-length v1, p0

    invoke-static {p0, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 347
    .end local v0    # "newArray":[B
    :goto_0
    return-object v0

    :cond_1
    move-object v0, p0

    goto :goto_0
.end method

.method public static grow([C)[C
    .locals 1
    .param p0, "array"    # [C

    .prologue
    .line 401
    array-length v0, p0

    add-int/lit8 v0, v0, 0x1

    invoke-static {p0, v0}, Lorg/apache/lucene/util/ArrayUtil;->grow([CI)[C

    move-result-object v0

    return-object v0
.end method

.method public static grow([CI)[C
    .locals 4
    .param p0, "array"    # [C
    .param p1, "minSize"    # I

    .prologue
    const/4 v2, 0x0

    .line 391
    sget-boolean v1, Lorg/apache/lucene/util/ArrayUtil;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-gez p1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "size must be positive (got "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "): likely integer overflow?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 392
    :cond_0
    array-length v1, p0

    if-ge v1, p1, :cond_1

    .line 393
    const/4 v1, 0x2

    invoke-static {p1, v1}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v1

    new-array v0, v1, [C

    .line 394
    .local v0, "newArray":[C
    array-length v1, p0

    invoke-static {p0, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 397
    .end local v0    # "newArray":[C
    :goto_0
    return-object v0

    :cond_1
    move-object v0, p0

    goto :goto_0
.end method

.method public static grow([D)[D
    .locals 1
    .param p0, "array"    # [D

    .prologue
    .line 276
    array-length v0, p0

    add-int/lit8 v0, v0, 0x1

    invoke-static {p0, v0}, Lorg/apache/lucene/util/ArrayUtil;->grow([DI)[D

    move-result-object v0

    return-object v0
.end method

.method public static grow([DI)[D
    .locals 4
    .param p0, "array"    # [D
    .param p1, "minSize"    # I

    .prologue
    const/4 v2, 0x0

    .line 266
    sget-boolean v1, Lorg/apache/lucene/util/ArrayUtil;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-gez p1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "size must be positive (got "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "): likely integer overflow?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 267
    :cond_0
    array-length v1, p0

    if-ge v1, p1, :cond_1

    .line 268
    const/16 v1, 0x8

    invoke-static {p1, v1}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v1

    new-array v0, v1, [D

    .line 269
    .local v0, "newArray":[D
    array-length v1, p0

    invoke-static {p0, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 272
    .end local v0    # "newArray":[D
    :goto_0
    return-object v0

    :cond_1
    move-object v0, p0

    goto :goto_0
.end method

.method public static grow([F)[F
    .locals 1
    .param p0, "array"    # [F

    .prologue
    .line 262
    array-length v0, p0

    add-int/lit8 v0, v0, 0x1

    invoke-static {p0, v0}, Lorg/apache/lucene/util/ArrayUtil;->grow([FI)[F

    move-result-object v0

    return-object v0
.end method

.method public static grow([FI)[F
    .locals 4
    .param p0, "array"    # [F
    .param p1, "minSize"    # I

    .prologue
    const/4 v2, 0x0

    .line 252
    sget-boolean v1, Lorg/apache/lucene/util/ArrayUtil;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-gez p1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "size must be positive (got "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "): likely integer overflow?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 253
    :cond_0
    array-length v1, p0

    if-ge v1, p1, :cond_1

    .line 254
    const/4 v1, 0x4

    invoke-static {p1, v1}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v1

    new-array v0, v1, [F

    .line 255
    .local v0, "newArray":[F
    array-length v1, p0

    invoke-static {p0, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 258
    .end local v0    # "newArray":[F
    :goto_0
    return-object v0

    :cond_1
    move-object v0, p0

    goto :goto_0
.end method

.method public static grow([I)[I
    .locals 1
    .param p0, "array"    # [I

    .prologue
    .line 301
    array-length v0, p0

    add-int/lit8 v0, v0, 0x1

    invoke-static {p0, v0}, Lorg/apache/lucene/util/ArrayUtil;->grow([II)[I

    move-result-object v0

    return-object v0
.end method

.method public static grow([II)[I
    .locals 4
    .param p0, "array"    # [I
    .param p1, "minSize"    # I

    .prologue
    const/4 v2, 0x0

    .line 291
    sget-boolean v1, Lorg/apache/lucene/util/ArrayUtil;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-gez p1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "size must be positive (got "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "): likely integer overflow?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 292
    :cond_0
    array-length v1, p0

    if-ge v1, p1, :cond_1

    .line 293
    const/4 v1, 0x4

    invoke-static {p1, v1}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v1

    new-array v0, v1, [I

    .line 294
    .local v0, "newArray":[I
    array-length v1, p0

    invoke-static {p0, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 297
    .end local v0    # "newArray":[I
    :goto_0
    return-object v0

    :cond_1
    move-object v0, p0

    goto :goto_0
.end method

.method public static grow([J)[J
    .locals 1
    .param p0, "array"    # [J

    .prologue
    .line 326
    array-length v0, p0

    add-int/lit8 v0, v0, 0x1

    invoke-static {p0, v0}, Lorg/apache/lucene/util/ArrayUtil;->grow([JI)[J

    move-result-object v0

    return-object v0
.end method

.method public static grow([JI)[J
    .locals 4
    .param p0, "array"    # [J
    .param p1, "minSize"    # I

    .prologue
    const/4 v2, 0x0

    .line 316
    sget-boolean v1, Lorg/apache/lucene/util/ArrayUtil;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-gez p1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "size must be positive (got "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "): likely integer overflow?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 317
    :cond_0
    array-length v1, p0

    if-ge v1, p1, :cond_1

    .line 318
    const/16 v1, 0x8

    invoke-static {p1, v1}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v1

    new-array v0, v1, [J

    .line 319
    .local v0, "newArray":[J
    array-length v1, p0

    invoke-static {p0, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 322
    .end local v0    # "newArray":[J
    :goto_0
    return-object v0

    :cond_1
    move-object v0, p0

    goto :goto_0
.end method

.method public static grow([S)[S
    .locals 1
    .param p0, "array"    # [S

    .prologue
    .line 248
    array-length v0, p0

    add-int/lit8 v0, v0, 0x1

    invoke-static {p0, v0}, Lorg/apache/lucene/util/ArrayUtil;->grow([SI)[S

    move-result-object v0

    return-object v0
.end method

.method public static grow([SI)[S
    .locals 4
    .param p0, "array"    # [S
    .param p1, "minSize"    # I

    .prologue
    const/4 v2, 0x0

    .line 238
    sget-boolean v1, Lorg/apache/lucene/util/ArrayUtil;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-gez p1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "size must be positive (got "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "): likely integer overflow?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 239
    :cond_0
    array-length v1, p0

    if-ge v1, p1, :cond_1

    .line 240
    const/4 v1, 0x2

    invoke-static {p1, v1}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v1

    new-array v0, v1, [S

    .line 241
    .local v0, "newArray":[S
    array-length v1, p0

    invoke-static {p0, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 244
    .end local v0    # "newArray":[S
    :goto_0
    return-object v0

    :cond_1
    move-object v0, p0

    goto :goto_0
.end method

.method public static grow([Z)[Z
    .locals 1
    .param p0, "array"    # [Z

    .prologue
    .line 376
    array-length v0, p0

    add-int/lit8 v0, v0, 0x1

    invoke-static {p0, v0}, Lorg/apache/lucene/util/ArrayUtil;->grow([ZI)[Z

    move-result-object v0

    return-object v0
.end method

.method public static grow([ZI)[Z
    .locals 4
    .param p0, "array"    # [Z
    .param p1, "minSize"    # I

    .prologue
    const/4 v2, 0x0

    .line 366
    sget-boolean v1, Lorg/apache/lucene/util/ArrayUtil;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-gez p1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "size must be positive (got "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "): likely integer overflow?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 367
    :cond_0
    array-length v1, p0

    if-ge v1, p1, :cond_1

    .line 368
    const/4 v1, 0x1

    invoke-static {p1, v1}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v1

    new-array v0, v1, [Z

    .line 369
    .local v0, "newArray":[Z
    array-length v1, p0

    invoke-static {p0, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 372
    .end local v0    # "newArray":[Z
    :goto_0
    return-object v0

    :cond_1
    move-object v0, p0

    goto :goto_0
.end method

.method public static grow([[F)[[F
    .locals 1
    .param p0, "array"    # [[F

    .prologue
    .line 454
    array-length v0, p0

    add-int/lit8 v0, v0, 0x1

    invoke-static {p0, v0}, Lorg/apache/lucene/util/ArrayUtil;->grow([[FI)[[F

    move-result-object v0

    return-object v0
.end method

.method public static grow([[FI)[[F
    .locals 4
    .param p0, "array"    # [[F
    .param p1, "minSize"    # I

    .prologue
    const/4 v2, 0x0

    .line 443
    sget-boolean v1, Lorg/apache/lucene/util/ArrayUtil;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-gez p1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "size must be positive (got "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "): likely integer overflow?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 444
    :cond_0
    array-length v1, p0

    if-ge v1, p1, :cond_1

    .line 445
    sget v1, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_REF:I

    invoke-static {p1, v1}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v1

    new-array v0, v1, [[F

    .line 446
    .local v0, "newArray":[[F
    array-length v1, p0

    invoke-static {p0, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 449
    .end local v0    # "newArray":[[F
    :goto_0
    return-object v0

    :cond_1
    move-object v0, p0

    goto :goto_0
.end method

.method public static grow([[I)[[I
    .locals 1
    .param p0, "array"    # [[I

    .prologue
    .line 427
    array-length v0, p0

    add-int/lit8 v0, v0, 0x1

    invoke-static {p0, v0}, Lorg/apache/lucene/util/ArrayUtil;->grow([[II)[[I

    move-result-object v0

    return-object v0
.end method

.method public static grow([[II)[[I
    .locals 4
    .param p0, "array"    # [[I
    .param p1, "minSize"    # I

    .prologue
    const/4 v2, 0x0

    .line 416
    sget-boolean v1, Lorg/apache/lucene/util/ArrayUtil;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-gez p1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "size must be positive (got "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "): likely integer overflow?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 417
    :cond_0
    array-length v1, p0

    if-ge v1, p1, :cond_1

    .line 418
    sget v1, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_REF:I

    invoke-static {p1, v1}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v1

    new-array v0, v1, [[I

    .line 419
    .local v0, "newArray":[[I
    array-length v1, p0

    invoke-static {p0, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 422
    .end local v0    # "newArray":[[I
    :goto_0
    return-object v0

    :cond_1
    move-object v0, p0

    goto :goto_0
.end method

.method public static hashCode([BII)I
    .locals 4
    .param p0, "array"    # [B
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 485
    const/4 v0, 0x0

    .line 486
    .local v0, "code":I
    add-int/lit8 v1, p2, -0x1

    .local v1, "i":I
    :goto_0
    if-ge v1, p1, :cond_0

    .line 488
    return v0

    .line 487
    :cond_0
    mul-int/lit8 v2, v0, 0x1f

    aget-byte v3, p0, v1

    add-int v0, v2, v3

    .line 486
    add-int/lit8 v1, v1, -0x1

    goto :goto_0
.end method

.method public static hashCode([CII)I
    .locals 4
    .param p0, "array"    # [C
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 474
    const/4 v0, 0x0

    .line 475
    .local v0, "code":I
    add-int/lit8 v1, p2, -0x1

    .local v1, "i":I
    :goto_0
    if-ge v1, p1, :cond_0

    .line 477
    return v0

    .line 476
    :cond_0
    mul-int/lit8 v2, v0, 0x1f

    aget-char v3, p0, v1

    add-int v0, v2, v3

    .line 475
    add-int/lit8 v1, v1, -0x1

    goto :goto_0
.end method

.method public static insertionSort([Ljava/lang/Comparable;)V
    .locals 2
    .param p0, "a"    # [Ljava/lang/Comparable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/lang/Comparable",
            "<-TT;>;>([TT;)V"
        }
    .end annotation

    .prologue
    .line 906
    const/4 v0, 0x0

    array-length v1, p0

    invoke-static {p0, v0, v1}, Lorg/apache/lucene/util/ArrayUtil;->insertionSort([Ljava/lang/Comparable;II)V

    .line 907
    return-void
.end method

.method public static insertionSort([Ljava/lang/Comparable;II)V
    .locals 2
    .param p0, "a"    # [Ljava/lang/Comparable;
    .param p1, "fromIndex"    # I
    .param p2, "toIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/lang/Comparable",
            "<-TT;>;>([TT;II)V"
        }
    .end annotation

    .prologue
    .line 897
    sub-int v0, p2, p1

    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    .line 899
    :goto_0
    return-void

    .line 898
    :cond_0
    invoke-static {p0}, Lorg/apache/lucene/util/ArrayUtil;->getSorter([Ljava/lang/Comparable;)Lorg/apache/lucene/util/SorterTemplate;

    move-result-object v0

    add-int/lit8 v1, p2, -0x1

    invoke-virtual {v0, p1, v1}, Lorg/apache/lucene/util/SorterTemplate;->insertionSort(II)V

    goto :goto_0
.end method

.method public static insertionSort([Ljava/lang/Object;IILjava/util/Comparator;)V
    .locals 2
    .param p0, "a"    # [Ljava/lang/Object;
    .param p1, "fromIndex"    # I
    .param p2, "toIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;II",
            "Ljava/util/Comparator",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 878
    .local p3, "comp":Ljava/util/Comparator;, "Ljava/util/Comparator<-TT;>;"
    sub-int v0, p2, p1

    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    .line 880
    :goto_0
    return-void

    .line 879
    :cond_0
    invoke-static {p0, p3}, Lorg/apache/lucene/util/ArrayUtil;->getSorter([Ljava/lang/Object;Ljava/util/Comparator;)Lorg/apache/lucene/util/SorterTemplate;

    move-result-object v0

    add-int/lit8 v1, p2, -0x1

    invoke-virtual {v0, p1, v1}, Lorg/apache/lucene/util/SorterTemplate;->insertionSort(II)V

    goto :goto_0
.end method

.method public static insertionSort([Ljava/lang/Object;Ljava/util/Comparator;)V
    .locals 2
    .param p0, "a"    # [Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;",
            "Ljava/util/Comparator",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 887
    .local p1, "comp":Ljava/util/Comparator;, "Ljava/util/Comparator<-TT;>;"
    const/4 v0, 0x0

    array-length v1, p0

    invoke-static {p0, v0, v1, p1}, Lorg/apache/lucene/util/ArrayUtil;->insertionSort([Ljava/lang/Object;IILjava/util/Comparator;)V

    .line 888
    return-void
.end method

.method public static mergeSort([Ljava/lang/Comparable;)V
    .locals 2
    .param p0, "a"    # [Ljava/lang/Comparable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/lang/Comparable",
            "<-TT;>;>([TT;)V"
        }
    .end annotation

    .prologue
    .line 826
    const/4 v0, 0x0

    array-length v1, p0

    invoke-static {p0, v0, v1}, Lorg/apache/lucene/util/ArrayUtil;->mergeSort([Ljava/lang/Comparable;II)V

    .line 827
    return-void
.end method

.method public static mergeSort([Ljava/lang/Comparable;II)V
    .locals 2
    .param p0, "a"    # [Ljava/lang/Comparable;
    .param p1, "fromIndex"    # I
    .param p2, "toIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/lang/Comparable",
            "<-TT;>;>([TT;II)V"
        }
    .end annotation

    .prologue
    .line 817
    sub-int v0, p2, p1

    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    .line 819
    :goto_0
    return-void

    .line 818
    :cond_0
    invoke-static {p0}, Lorg/apache/lucene/util/ArrayUtil;->getMergeSorter([Ljava/lang/Comparable;)Lorg/apache/lucene/util/SorterTemplate;

    move-result-object v0

    add-int/lit8 v1, p2, -0x1

    invoke-virtual {v0, p1, v1}, Lorg/apache/lucene/util/SorterTemplate;->mergeSort(II)V

    goto :goto_0
.end method

.method public static mergeSort([Ljava/lang/Object;IILjava/util/Comparator;)V
    .locals 2
    .param p0, "a"    # [Ljava/lang/Object;
    .param p1, "fromIndex"    # I
    .param p2, "toIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;II",
            "Ljava/util/Comparator",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 797
    .local p3, "comp":Ljava/util/Comparator;, "Ljava/util/Comparator<-TT;>;"
    sub-int v0, p2, p1

    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    .line 800
    :goto_0
    return-void

    .line 799
    :cond_0
    invoke-static {p0, p3}, Lorg/apache/lucene/util/ArrayUtil;->getMergeSorter([Ljava/lang/Object;Ljava/util/Comparator;)Lorg/apache/lucene/util/SorterTemplate;

    move-result-object v0

    add-int/lit8 v1, p2, -0x1

    invoke-virtual {v0, p1, v1}, Lorg/apache/lucene/util/SorterTemplate;->mergeSort(II)V

    goto :goto_0
.end method

.method public static mergeSort([Ljava/lang/Object;Ljava/util/Comparator;)V
    .locals 2
    .param p0, "a"    # [Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;",
            "Ljava/util/Comparator",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 807
    .local p1, "comp":Ljava/util/Comparator;, "Ljava/util/Comparator<-TT;>;"
    const/4 v0, 0x0

    array-length v1, p0

    invoke-static {p0, v0, v1, p1}, Lorg/apache/lucene/util/ArrayUtil;->mergeSort([Ljava/lang/Object;IILjava/util/Comparator;)V

    .line 808
    return-void
.end method

.method public static oversize(II)I
    .locals 5
    .param p0, "minTargetSize"    # I
    .param p1, "bytesPerElement"    # I

    .prologue
    const v4, 0x7ffffffe

    const v3, 0x7ffffffc

    .line 159
    if-gez p0, :cond_0

    .line 161
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "invalid array size "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 164
    :cond_0
    if-nez p0, :cond_1

    .line 166
    const/4 v1, 0x0

    .line 221
    :goto_0
    return v1

    .line 172
    :cond_1
    shr-int/lit8 v0, p0, 0x3

    .line 174
    .local v0, "extra":I
    const/4 v2, 0x3

    if-ge v0, v2, :cond_2

    .line 178
    const/4 v0, 0x3

    .line 181
    :cond_2
    add-int v1, p0, v0

    .line 184
    .local v1, "newSize":I
    add-int/lit8 v2, v1, 0x7

    if-gez v2, :cond_3

    .line 186
    const v1, 0x7fffffff

    goto :goto_0

    .line 189
    :cond_3
    sget-boolean v2, Lorg/apache/lucene/util/Constants;->JRE_IS_64BIT:Z

    if-eqz v2, :cond_4

    .line 191
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 200
    :pswitch_1
    add-int/lit8 v2, v1, 0x7

    const v3, 0x7ffffff8

    and-int v1, v2, v3

    goto :goto_0

    .line 194
    :pswitch_2
    add-int/lit8 v2, v1, 0x1

    and-int v1, v2, v4

    goto :goto_0

    .line 197
    :pswitch_3
    add-int/lit8 v2, v1, 0x3

    and-int v1, v2, v3

    goto :goto_0

    .line 209
    :cond_4
    packed-switch p1, :pswitch_data_1

    goto :goto_0

    .line 215
    :pswitch_4
    add-int/lit8 v2, v1, 0x3

    and-int v1, v2, v3

    goto :goto_0

    .line 212
    :pswitch_5
    add-int/lit8 v2, v1, 0x1

    and-int v1, v2, v4

    goto :goto_0

    .line 191
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch

    .line 209
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private static parse([CIIIZ)I
    .locals 7
    .param p0, "chars"    # [C
    .param p1, "offset"    # I
    .param p2, "len"    # I
    .param p3, "radix"    # I
    .param p4, "negative"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NumberFormatException;
        }
    .end annotation

    .prologue
    .line 105
    const/high16 v5, -0x80000000

    div-int v2, v5, p3

    .line 106
    .local v2, "max":I
    const/4 v4, 0x0

    .line 107
    .local v4, "result":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, p2, :cond_0

    .line 124
    if-nez p4, :cond_4

    .line 125
    neg-int v4, v4

    .line 126
    if-gez v4, :cond_4

    .line 127
    new-instance v5, Ljava/lang/NumberFormatException;

    const-string v6, "Unable to parse"

    invoke-direct {v5, v6}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 108
    :cond_0
    add-int v5, v1, p1

    aget-char v5, p0, v5

    invoke-static {v5, p3}, Ljava/lang/Character;->digit(CI)I

    move-result v0

    .line 109
    .local v0, "digit":I
    const/4 v5, -0x1

    if-ne v0, v5, :cond_1

    .line 110
    new-instance v5, Ljava/lang/NumberFormatException;

    const-string v6, "Unable to parse"

    invoke-direct {v5, v6}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 112
    :cond_1
    if-le v2, v4, :cond_2

    .line 113
    new-instance v5, Ljava/lang/NumberFormatException;

    const-string v6, "Unable to parse"

    invoke-direct {v5, v6}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 115
    :cond_2
    mul-int v5, v4, p3

    sub-int v3, v5, v0

    .line 116
    .local v3, "next":I
    if-le v3, v4, :cond_3

    .line 117
    new-instance v5, Ljava/lang/NumberFormatException;

    const-string v6, "Unable to parse"

    invoke-direct {v5, v6}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 119
    :cond_3
    move v4, v3

    .line 107
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 130
    .end local v0    # "digit":I
    .end local v3    # "next":I
    :cond_4
    return v4
.end method

.method public static parseInt([C)I
    .locals 3
    .param p0, "chars"    # [C
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NumberFormatException;
        }
    .end annotation

    .prologue
    .line 55
    const/4 v0, 0x0

    array-length v1, p0

    const/16 v2, 0xa

    invoke-static {p0, v0, v1, v2}, Lorg/apache/lucene/util/ArrayUtil;->parseInt([CIII)I

    move-result v0

    return v0
.end method

.method public static parseInt([CII)I
    .locals 1
    .param p0, "chars"    # [C
    .param p1, "offset"    # I
    .param p2, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NumberFormatException;
        }
    .end annotation

    .prologue
    .line 67
    const/16 v0, 0xa

    invoke-static {p0, p1, p2, v0}, Lorg/apache/lucene/util/ArrayUtil;->parseInt([CIII)I

    move-result v0

    return v0
.end method

.method public static parseInt([CIII)I
    .locals 4
    .param p0, "chars"    # [C
    .param p1, "offset"    # I
    .param p2, "len"    # I
    .param p3, "radix"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NumberFormatException;
        }
    .end annotation

    .prologue
    .line 83
    if-eqz p0, :cond_0

    const/4 v2, 0x2

    if-lt p3, v2, :cond_0

    .line 84
    const/16 v2, 0x24

    if-le p3, v2, :cond_1

    .line 85
    :cond_0
    new-instance v2, Ljava/lang/NumberFormatException;

    invoke-direct {v2}, Ljava/lang/NumberFormatException;-><init>()V

    throw v2

    .line 87
    :cond_1
    const/4 v0, 0x0

    .line 88
    .local v0, "i":I
    if-nez p2, :cond_2

    .line 89
    new-instance v2, Ljava/lang/NumberFormatException;

    const-string v3, "chars length is 0"

    invoke-direct {v2, v3}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 91
    :cond_2
    add-int v2, p1, v0

    aget-char v2, p0, v2

    const/16 v3, 0x2d

    if-ne v2, v3, :cond_3

    const/4 v1, 0x1

    .line 92
    .local v1, "negative":Z
    :goto_0
    if-eqz v1, :cond_4

    add-int/lit8 v0, v0, 0x1

    if-ne v0, p2, :cond_4

    .line 93
    new-instance v2, Ljava/lang/NumberFormatException;

    const-string v3, "can\'t convert to an int"

    invoke-direct {v2, v3}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 91
    .end local v1    # "negative":Z
    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    .line 95
    .restart local v1    # "negative":Z
    :cond_4
    if-eqz v1, :cond_5

    .line 96
    add-int/lit8 p1, p1, 0x1

    .line 97
    add-int/lit8 p2, p2, -0x1

    .line 99
    :cond_5
    invoke-static {p0, p1, p2, p3, v1}, Lorg/apache/lucene/util/ArrayUtil;->parse([CIIIZ)I

    move-result v2

    return v2
.end method

.method public static quickSort([Ljava/lang/Comparable;)V
    .locals 2
    .param p0, "a"    # [Ljava/lang/Comparable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/lang/Comparable",
            "<-TT;>;>([TT;)V"
        }
    .end annotation

    .prologue
    .line 785
    const/4 v0, 0x0

    array-length v1, p0

    invoke-static {p0, v0, v1}, Lorg/apache/lucene/util/ArrayUtil;->quickSort([Ljava/lang/Comparable;II)V

    .line 786
    return-void
.end method

.method public static quickSort([Ljava/lang/Comparable;II)V
    .locals 2
    .param p0, "a"    # [Ljava/lang/Comparable;
    .param p1, "fromIndex"    # I
    .param p2, "toIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/lang/Comparable",
            "<-TT;>;>([TT;II)V"
        }
    .end annotation

    .prologue
    .line 776
    sub-int v0, p2, p1

    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    .line 778
    :goto_0
    return-void

    .line 777
    :cond_0
    invoke-static {p0}, Lorg/apache/lucene/util/ArrayUtil;->getSorter([Ljava/lang/Comparable;)Lorg/apache/lucene/util/SorterTemplate;

    move-result-object v0

    add-int/lit8 v1, p2, -0x1

    invoke-virtual {v0, p1, v1}, Lorg/apache/lucene/util/SorterTemplate;->quickSort(II)V

    goto :goto_0
.end method

.method public static quickSort([Ljava/lang/Object;IILjava/util/Comparator;)V
    .locals 2
    .param p0, "a"    # [Ljava/lang/Object;
    .param p1, "fromIndex"    # I
    .param p2, "toIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;II",
            "Ljava/util/Comparator",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 757
    .local p3, "comp":Ljava/util/Comparator;, "Ljava/util/Comparator<-TT;>;"
    sub-int v0, p2, p1

    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    .line 759
    :goto_0
    return-void

    .line 758
    :cond_0
    invoke-static {p0, p3}, Lorg/apache/lucene/util/ArrayUtil;->getSorter([Ljava/lang/Object;Ljava/util/Comparator;)Lorg/apache/lucene/util/SorterTemplate;

    move-result-object v0

    add-int/lit8 v1, p2, -0x1

    invoke-virtual {v0, p1, v1}, Lorg/apache/lucene/util/SorterTemplate;->quickSort(II)V

    goto :goto_0
.end method

.method public static quickSort([Ljava/lang/Object;Ljava/util/Comparator;)V
    .locals 2
    .param p0, "a"    # [Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;",
            "Ljava/util/Comparator",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 766
    .local p1, "comp":Ljava/util/Comparator;, "Ljava/util/Comparator<-TT;>;"
    const/4 v0, 0x0

    array-length v1, p0

    invoke-static {p0, v0, v1, p1}, Lorg/apache/lucene/util/ArrayUtil;->quickSort([Ljava/lang/Object;IILjava/util/Comparator;)V

    .line 767
    return-void
.end method

.method public static shrink([BI)[B
    .locals 5
    .param p0, "array"    # [B
    .param p1, "targetSize"    # I

    .prologue
    const/4 v4, 0x0

    .line 355
    sget-boolean v2, Lorg/apache/lucene/util/ArrayUtil;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    if-gez p1, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "size must be positive (got "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "): likely integer overflow?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 356
    :cond_0
    array-length v2, p0

    const/4 v3, 0x1

    invoke-static {v2, p1, v3}, Lorg/apache/lucene/util/ArrayUtil;->getShrinkSize(III)I

    move-result v1

    .line 357
    .local v1, "newSize":I
    array-length v2, p0

    if-eq v1, v2, :cond_1

    .line 358
    new-array v0, v1, [B

    .line 359
    .local v0, "newArray":[B
    invoke-static {p0, v4, v0, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 362
    .end local v0    # "newArray":[B
    :goto_0
    return-object v0

    :cond_1
    move-object v0, p0

    goto :goto_0
.end method

.method public static shrink([CI)[C
    .locals 5
    .param p0, "array"    # [C
    .param p1, "targetSize"    # I

    .prologue
    const/4 v4, 0x0

    .line 405
    sget-boolean v2, Lorg/apache/lucene/util/ArrayUtil;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    if-gez p1, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "size must be positive (got "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "): likely integer overflow?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 406
    :cond_0
    array-length v2, p0

    const/4 v3, 0x2

    invoke-static {v2, p1, v3}, Lorg/apache/lucene/util/ArrayUtil;->getShrinkSize(III)I

    move-result v1

    .line 407
    .local v1, "newSize":I
    array-length v2, p0

    if-eq v1, v2, :cond_1

    .line 408
    new-array v0, v1, [C

    .line 409
    .local v0, "newArray":[C
    invoke-static {p0, v4, v0, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 412
    .end local v0    # "newArray":[C
    :goto_0
    return-object v0

    :cond_1
    move-object v0, p0

    goto :goto_0
.end method

.method public static shrink([II)[I
    .locals 5
    .param p0, "array"    # [I
    .param p1, "targetSize"    # I

    .prologue
    const/4 v4, 0x0

    .line 305
    sget-boolean v2, Lorg/apache/lucene/util/ArrayUtil;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    if-gez p1, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "size must be positive (got "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "): likely integer overflow?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 306
    :cond_0
    array-length v2, p0

    const/4 v3, 0x4

    invoke-static {v2, p1, v3}, Lorg/apache/lucene/util/ArrayUtil;->getShrinkSize(III)I

    move-result v1

    .line 307
    .local v1, "newSize":I
    array-length v2, p0

    if-eq v1, v2, :cond_1

    .line 308
    new-array v0, v1, [I

    .line 309
    .local v0, "newArray":[I
    invoke-static {p0, v4, v0, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 312
    .end local v0    # "newArray":[I
    :goto_0
    return-object v0

    :cond_1
    move-object v0, p0

    goto :goto_0
.end method

.method public static shrink([JI)[J
    .locals 5
    .param p0, "array"    # [J
    .param p1, "targetSize"    # I

    .prologue
    const/4 v4, 0x0

    .line 330
    sget-boolean v2, Lorg/apache/lucene/util/ArrayUtil;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    if-gez p1, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "size must be positive (got "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "): likely integer overflow?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 331
    :cond_0
    array-length v2, p0

    const/16 v3, 0x8

    invoke-static {v2, p1, v3}, Lorg/apache/lucene/util/ArrayUtil;->getShrinkSize(III)I

    move-result v1

    .line 332
    .local v1, "newSize":I
    array-length v2, p0

    if-eq v1, v2, :cond_1

    .line 333
    new-array v0, v1, [J

    .line 334
    .local v0, "newArray":[J
    invoke-static {p0, v4, v0, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 337
    .end local v0    # "newArray":[J
    :goto_0
    return-object v0

    :cond_1
    move-object v0, p0

    goto :goto_0
.end method

.method public static shrink([SI)[S
    .locals 5
    .param p0, "array"    # [S
    .param p1, "targetSize"    # I

    .prologue
    const/4 v4, 0x0

    .line 280
    sget-boolean v2, Lorg/apache/lucene/util/ArrayUtil;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    if-gez p1, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "size must be positive (got "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "): likely integer overflow?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 281
    :cond_0
    array-length v2, p0

    const/4 v3, 0x2

    invoke-static {v2, p1, v3}, Lorg/apache/lucene/util/ArrayUtil;->getShrinkSize(III)I

    move-result v1

    .line 282
    .local v1, "newSize":I
    array-length v2, p0

    if-eq v1, v2, :cond_1

    .line 283
    new-array v0, v1, [S

    .line 284
    .local v0, "newArray":[S
    invoke-static {p0, v4, v0, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 287
    .end local v0    # "newArray":[S
    :goto_0
    return-object v0

    :cond_1
    move-object v0, p0

    goto :goto_0
.end method

.method public static shrink([ZI)[Z
    .locals 5
    .param p0, "array"    # [Z
    .param p1, "targetSize"    # I

    .prologue
    const/4 v4, 0x0

    .line 380
    sget-boolean v2, Lorg/apache/lucene/util/ArrayUtil;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    if-gez p1, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "size must be positive (got "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "): likely integer overflow?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 381
    :cond_0
    array-length v2, p0

    const/4 v3, 0x1

    invoke-static {v2, p1, v3}, Lorg/apache/lucene/util/ArrayUtil;->getShrinkSize(III)I

    move-result v1

    .line 382
    .local v1, "newSize":I
    array-length v2, p0

    if-eq v1, v2, :cond_1

    .line 383
    new-array v0, v1, [Z

    .line 384
    .local v0, "newArray":[Z
    invoke-static {p0, v4, v0, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 387
    .end local v0    # "newArray":[Z
    :goto_0
    return-object v0

    :cond_1
    move-object v0, p0

    goto :goto_0
.end method

.method public static shrink([[FI)[[F
    .locals 5
    .param p0, "array"    # [[F
    .param p1, "targetSize"    # I

    .prologue
    const/4 v4, 0x0

    .line 458
    sget-boolean v2, Lorg/apache/lucene/util/ArrayUtil;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    if-gez p1, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "size must be positive (got "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "): likely integer overflow?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 459
    :cond_0
    array-length v2, p0

    sget v3, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_REF:I

    invoke-static {v2, p1, v3}, Lorg/apache/lucene/util/ArrayUtil;->getShrinkSize(III)I

    move-result v1

    .line 460
    .local v1, "newSize":I
    array-length v2, p0

    if-eq v1, v2, :cond_1

    .line 461
    new-array v0, v1, [[F

    .line 462
    .local v0, "newArray":[[F
    invoke-static {p0, v4, v0, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 465
    .end local v0    # "newArray":[[F
    :goto_0
    return-object v0

    :cond_1
    move-object v0, p0

    goto :goto_0
.end method

.method public static shrink([[II)[[I
    .locals 5
    .param p0, "array"    # [[I
    .param p1, "targetSize"    # I

    .prologue
    const/4 v4, 0x0

    .line 431
    sget-boolean v2, Lorg/apache/lucene/util/ArrayUtil;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    if-gez p1, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "size must be positive (got "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "): likely integer overflow?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 432
    :cond_0
    array-length v2, p0

    sget v3, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_REF:I

    invoke-static {v2, p1, v3}, Lorg/apache/lucene/util/ArrayUtil;->getShrinkSize(III)I

    move-result v1

    .line 433
    .local v1, "newSize":I
    array-length v2, p0

    if-eq v1, v2, :cond_1

    .line 434
    new-array v0, v1, [[I

    .line 435
    .local v0, "newArray":[[I
    invoke-static {p0, v4, v0, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 438
    .end local v0    # "newArray":[[I
    :goto_0
    return-object v0

    :cond_1
    move-object v0, p0

    goto :goto_0
.end method

.method public static timSort([Ljava/lang/Comparable;)V
    .locals 2
    .param p0, "a"    # [Ljava/lang/Comparable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/lang/Comparable",
            "<-TT;>;>([TT;)V"
        }
    .end annotation

    .prologue
    .line 866
    const/4 v0, 0x0

    array-length v1, p0

    invoke-static {p0, v0, v1}, Lorg/apache/lucene/util/ArrayUtil;->timSort([Ljava/lang/Comparable;II)V

    .line 867
    return-void
.end method

.method public static timSort([Ljava/lang/Comparable;II)V
    .locals 2
    .param p0, "a"    # [Ljava/lang/Comparable;
    .param p1, "fromIndex"    # I
    .param p2, "toIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/lang/Comparable",
            "<-TT;>;>([TT;II)V"
        }
    .end annotation

    .prologue
    .line 857
    sub-int v0, p2, p1

    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    .line 859
    :goto_0
    return-void

    .line 858
    :cond_0
    invoke-static {p0}, Lorg/apache/lucene/util/ArrayUtil;->getMergeSorter([Ljava/lang/Comparable;)Lorg/apache/lucene/util/SorterTemplate;

    move-result-object v0

    add-int/lit8 v1, p2, -0x1

    invoke-virtual {v0, p1, v1}, Lorg/apache/lucene/util/SorterTemplate;->timSort(II)V

    goto :goto_0
.end method

.method public static timSort([Ljava/lang/Object;IILjava/util/Comparator;)V
    .locals 2
    .param p0, "a"    # [Ljava/lang/Object;
    .param p1, "fromIndex"    # I
    .param p2, "toIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;II",
            "Ljava/util/Comparator",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 838
    .local p3, "comp":Ljava/util/Comparator;, "Ljava/util/Comparator<-TT;>;"
    sub-int v0, p2, p1

    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    .line 840
    :goto_0
    return-void

    .line 839
    :cond_0
    invoke-static {p0, p3}, Lorg/apache/lucene/util/ArrayUtil;->getMergeSorter([Ljava/lang/Object;Ljava/util/Comparator;)Lorg/apache/lucene/util/SorterTemplate;

    move-result-object v0

    add-int/lit8 v1, p2, -0x1

    invoke-virtual {v0, p1, v1}, Lorg/apache/lucene/util/SorterTemplate;->timSort(II)V

    goto :goto_0
.end method

.method public static timSort([Ljava/lang/Object;Ljava/util/Comparator;)V
    .locals 2
    .param p0, "a"    # [Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;",
            "Ljava/util/Comparator",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 847
    .local p1, "comp":Ljava/util/Comparator;, "Ljava/util/Comparator<-TT;>;"
    const/4 v0, 0x0

    array-length v1, p0

    invoke-static {p0, v0, v1, p1}, Lorg/apache/lucene/util/ArrayUtil;->timSort([Ljava/lang/Object;IILjava/util/Comparator;)V

    .line 848
    return-void
.end method

.method public static toIntArray(Ljava/util/Collection;)[I
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Integer;",
            ">;)[I"
        }
    .end annotation

    .prologue
    .line 601
    .local p0, "ints":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Integer;>;"
    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v4

    new-array v0, v4, [I

    .line 602
    .local v0, "result":[I
    const/4 v1, 0x0

    .line 603
    .local v1, "upto":I
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_0

    .line 608
    sget-boolean v4, Lorg/apache/lucene/util/ArrayUtil;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    array-length v4, v0

    if-eq v1, v4, :cond_1

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 603
    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 604
    .local v3, "v":I
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "upto":I
    .local v2, "upto":I
    aput v3, v0, v1

    move v1, v2

    .end local v2    # "upto":I
    .restart local v1    # "upto":I
    goto :goto_0

    .line 610
    .end local v3    # "v":I
    :cond_1
    return-object v0
.end method
