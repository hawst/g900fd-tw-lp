.class Lorg/apache/lucene/util/WeakIdentityMap$1;
.super Ljava/lang/Object;
.source "WeakIdentityMap.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/util/WeakIdentityMap;->keyIterator()Ljava/util/Iterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TK;>;"
    }
.end annotation


# instance fields
.field private next:Ljava/lang/Object;

.field private nextIsSet:Z

.field final synthetic this$0:Lorg/apache/lucene/util/WeakIdentityMap;

.field private final synthetic val$iterator:Ljava/util/Iterator;


# direct methods
.method constructor <init>(Lorg/apache/lucene/util/WeakIdentityMap;Ljava/util/Iterator;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/util/WeakIdentityMap$1;->this$0:Lorg/apache/lucene/util/WeakIdentityMap;

    iput-object p2, p0, Lorg/apache/lucene/util/WeakIdentityMap$1;->val$iterator:Ljava/util/Iterator;

    .line 168
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 170
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/util/WeakIdentityMap$1;->next:Ljava/lang/Object;

    .line 172
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/util/WeakIdentityMap$1;->nextIsSet:Z

    return-void
.end method

.method private setNext()Z
    .locals 2

    .prologue
    .line 200
    sget-boolean v0, Lorg/apache/lucene/util/WeakIdentityMap;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lorg/apache/lucene/util/WeakIdentityMap$1;->nextIsSet:Z

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 202
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/util/WeakIdentityMap$1;->val$iterator:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/util/WeakIdentityMap$IdentityWeakReference;

    invoke-virtual {v0}, Lorg/apache/lucene/util/WeakIdentityMap$IdentityWeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/util/WeakIdentityMap$1;->next:Ljava/lang/Object;

    .line 203
    iget-object v0, p0, Lorg/apache/lucene/util/WeakIdentityMap$1;->next:Ljava/lang/Object;

    if-nez v0, :cond_2

    .line 205
    iget-object v0, p0, Lorg/apache/lucene/util/WeakIdentityMap$1;->val$iterator:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 201
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/util/WeakIdentityMap$1;->val$iterator:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 214
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 208
    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/util/WeakIdentityMap$1;->next:Ljava/lang/Object;

    sget-object v1, Lorg/apache/lucene/util/WeakIdentityMap;->NULL:Ljava/lang/Object;

    if-ne v0, v1, :cond_3

    .line 209
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/util/WeakIdentityMap$1;->next:Ljava/lang/Object;

    .line 211
    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/util/WeakIdentityMap$1;->nextIsSet:Z

    goto :goto_0
.end method


# virtual methods
.method public hasNext()Z
    .locals 1

    .prologue
    .line 176
    iget-boolean v0, p0, Lorg/apache/lucene/util/WeakIdentityMap$1;->nextIsSet:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/util/WeakIdentityMap$1;->setNext()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public next()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TK;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 181
    invoke-virtual {p0}, Lorg/apache/lucene/util/WeakIdentityMap$1;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 182
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 184
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/util/WeakIdentityMap;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lorg/apache/lucene/util/WeakIdentityMap$1;->nextIsSet:Z

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 186
    :cond_1
    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/util/WeakIdentityMap$1;->next:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 189
    iput-boolean v1, p0, Lorg/apache/lucene/util/WeakIdentityMap$1;->nextIsSet:Z

    .line 190
    iput-object v2, p0, Lorg/apache/lucene/util/WeakIdentityMap$1;->next:Ljava/lang/Object;

    .line 186
    return-object v0

    .line 187
    :catchall_0
    move-exception v0

    .line 189
    iput-boolean v1, p0, Lorg/apache/lucene/util/WeakIdentityMap$1;->nextIsSet:Z

    .line 190
    iput-object v2, p0, Lorg/apache/lucene/util/WeakIdentityMap$1;->next:Ljava/lang/Object;

    .line 191
    throw v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 196
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
