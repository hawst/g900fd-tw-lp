.class public interface abstract Lorg/apache/lucene/util/packed/PackedInts$Encoder;
.super Ljava/lang/Object;
.source "PackedInts.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/packed/PackedInts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Encoder"
.end annotation


# virtual methods
.method public abstract byteBlockCount()I
.end method

.method public abstract byteValueCount()I
.end method

.method public abstract encode([II[BII)V
.end method

.method public abstract encode([II[JII)V
.end method

.method public abstract encode([JI[BII)V
.end method

.method public abstract encode([JI[JII)V
.end method

.method public abstract longBlockCount()I
.end method

.method public abstract longValueCount()I
.end method
