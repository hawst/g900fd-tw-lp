.class Lorg/apache/lucene/util/fst/FST$NodeQueue;
.super Lorg/apache/lucene/util/PriorityQueue;
.source "FST.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/fst/FST;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "NodeQueue"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/util/PriorityQueue",
        "<",
        "Lorg/apache/lucene/util/fst/FST$NodeAndInCount;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1802
    const-class v0, Lorg/apache/lucene/util/fst/FST;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/fst/FST$NodeQueue;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "topN"    # I

    .prologue
    .line 1804
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/util/PriorityQueue;-><init>(IZ)V

    .line 1805
    return-void
.end method


# virtual methods
.method public bridge synthetic lessThan(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/util/fst/FST$NodeAndInCount;

    check-cast p2, Lorg/apache/lucene/util/fst/FST$NodeAndInCount;

    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/util/fst/FST$NodeQueue;->lessThan(Lorg/apache/lucene/util/fst/FST$NodeAndInCount;Lorg/apache/lucene/util/fst/FST$NodeAndInCount;)Z

    move-result v0

    return v0
.end method

.method public lessThan(Lorg/apache/lucene/util/fst/FST$NodeAndInCount;Lorg/apache/lucene/util/fst/FST$NodeAndInCount;)Z
    .locals 2

    .prologue
    .line 1809
    .local p1, "a":Lorg/apache/lucene/util/fst/FST$NodeAndInCount;, "Lorg/apache/lucene/util/fst/FST$NodeAndInCount;"
    .local p2, "b":Lorg/apache/lucene/util/fst/FST$NodeAndInCount;, "Lorg/apache/lucene/util/fst/FST$NodeAndInCount;"
    invoke-virtual {p1, p2}, Lorg/apache/lucene/util/fst/FST$NodeAndInCount;->compareTo(Lorg/apache/lucene/util/fst/FST$NodeAndInCount;)I

    move-result v0

    .line 1810
    .local v0, "cmp":I
    sget-boolean v1, Lorg/apache/lucene/util/fst/FST$NodeQueue;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 1811
    :cond_0
    if-gez v0, :cond_1

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
