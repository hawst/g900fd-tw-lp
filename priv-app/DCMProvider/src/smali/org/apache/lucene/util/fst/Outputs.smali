.class public abstract Lorg/apache/lucene/util/fst/Outputs;
.super Ljava/lang/Object;
.source "Outputs.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    .local p0, "this":Lorg/apache/lucene/util/fst/Outputs;, "Lorg/apache/lucene/util/fst/Outputs<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TT;)TT;"
        }
    .end annotation
.end method

.method public abstract common(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TT;)TT;"
        }
    .end annotation
.end method

.method public abstract getNoOutput()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public merge(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TT;)TT;"
        }
    .end annotation

    .prologue
    .line 83
    .local p0, "this":Lorg/apache/lucene/util/fst/Outputs;, "Lorg/apache/lucene/util/fst/Outputs<TT;>;"
    .local p1, "first":Ljava/lang/Object;, "TT;"
    .local p2, "second":Ljava/lang/Object;, "TT;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public abstract outputToString(Ljava/lang/Object;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Ljava/lang/String;"
        }
    .end annotation
.end method

.method public abstract read(Lorg/apache/lucene/store/DataInput;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/store/DataInput;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public readFinalOutput(Lorg/apache/lucene/store/DataInput;)Ljava/lang/Object;
    .locals 1
    .param p1, "in"    # Lorg/apache/lucene/store/DataInput;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/store/DataInput;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 70
    .local p0, "this":Lorg/apache/lucene/util/fst/Outputs;, "Lorg/apache/lucene/util/fst/Outputs<TT;>;"
    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/fst/Outputs;->read(Lorg/apache/lucene/store/DataInput;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public abstract subtract(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TT;)TT;"
        }
    .end annotation
.end method

.method public abstract write(Ljava/lang/Object;Lorg/apache/lucene/store/DataOutput;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lorg/apache/lucene/store/DataOutput;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public writeFinalOutput(Ljava/lang/Object;Lorg/apache/lucene/store/DataOutput;)V
    .locals 0
    .param p2, "out"    # Lorg/apache/lucene/store/DataOutput;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lorg/apache/lucene/store/DataOutput;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 59
    .local p0, "this":Lorg/apache/lucene/util/fst/Outputs;, "Lorg/apache/lucene/util/fst/Outputs<TT;>;"
    .local p1, "output":Ljava/lang/Object;, "TT;"
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/util/fst/Outputs;->write(Ljava/lang/Object;Lorg/apache/lucene/store/DataOutput;)V

    .line 60
    return-void
.end method
