.class Lorg/apache/lucene/util/fst/BytesStore;
.super Lorg/apache/lucene/store/DataOutput;
.source "BytesStore.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final blockBits:I

.field private final blockMask:I

.field private final blockSize:I

.field private final blocks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<[B>;"
        }
    .end annotation
.end field

.field private current:[B

.field private nextWrite:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lorg/apache/lucene/util/fst/BytesStore;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/fst/BytesStore;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "blockBits"    # I

    .prologue
    .line 41
    invoke-direct {p0}, Lorg/apache/lucene/store/DataOutput;-><init>()V

    .line 32
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/util/fst/BytesStore;->blocks:Ljava/util/List;

    .line 42
    iput p1, p0, Lorg/apache/lucene/util/fst/BytesStore;->blockBits:I

    .line 43
    const/4 v0, 0x1

    shl-int/2addr v0, p1

    iput v0, p0, Lorg/apache/lucene/util/fst/BytesStore;->blockSize:I

    .line 44
    iget v0, p0, Lorg/apache/lucene/util/fst/BytesStore;->blockSize:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/util/fst/BytesStore;->blockMask:I

    .line 45
    iget v0, p0, Lorg/apache/lucene/util/fst/BytesStore;->blockSize:I

    iput v0, p0, Lorg/apache/lucene/util/fst/BytesStore;->nextWrite:I

    .line 46
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/store/DataInput;JI)V
    .locals 8
    .param p1, "in"    # Lorg/apache/lucene/store/DataInput;
    .param p2, "numBytes"    # J
    .param p4, "maxBlockSize"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 49
    invoke-direct {p0}, Lorg/apache/lucene/store/DataOutput;-><init>()V

    .line 32
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lorg/apache/lucene/util/fst/BytesStore;->blocks:Ljava/util/List;

    .line 50
    const/4 v2, 0x2

    .line 51
    .local v2, "blockSize":I
    const/4 v1, 0x1

    .line 52
    .local v1, "blockBits":I
    :goto_0
    int-to-long v6, v2

    cmp-long v6, v6, p2

    if-gez v6, :cond_0

    if-lt v2, p4, :cond_1

    .line 56
    :cond_0
    iput v1, p0, Lorg/apache/lucene/util/fst/BytesStore;->blockBits:I

    .line 57
    iput v2, p0, Lorg/apache/lucene/util/fst/BytesStore;->blockSize:I

    .line 58
    add-int/lit8 v6, v2, -0x1

    iput v6, p0, Lorg/apache/lucene/util/fst/BytesStore;->blockMask:I

    .line 59
    move-wide v4, p2

    .line 60
    .local v4, "left":J
    :goto_1
    const-wide/16 v6, 0x0

    cmp-long v6, v4, v6

    if-gtz v6, :cond_2

    .line 69
    iget-object v6, p0, Lorg/apache/lucene/util/fst/BytesStore;->blocks:Ljava/util/List;

    iget-object v7, p0, Lorg/apache/lucene/util/fst/BytesStore;->blocks:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [B

    array-length v6, v6

    iput v6, p0, Lorg/apache/lucene/util/fst/BytesStore;->nextWrite:I

    .line 70
    return-void

    .line 53
    .end local v4    # "left":J
    :cond_1
    mul-int/lit8 v2, v2, 0x2

    .line 54
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 61
    .restart local v4    # "left":J
    :cond_2
    int-to-long v6, v2

    invoke-static {v6, v7, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v6

    long-to-int v3, v6

    .line 62
    .local v3, "chunk":I
    new-array v0, v3, [B

    .line 63
    .local v0, "block":[B
    const/4 v6, 0x0

    array-length v7, v0

    invoke-virtual {p1, v0, v6, v7}, Lorg/apache/lucene/store/DataInput;->readBytes([BII)V

    .line 64
    iget-object v6, p0, Lorg/apache/lucene/util/fst/BytesStore;->blocks:Ljava/util/List;

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 65
    int-to-long v6, v3

    sub-long/2addr v4, v6

    goto :goto_1
.end method

.method static synthetic access$0(Lorg/apache/lucene/util/fst/BytesStore;)I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lorg/apache/lucene/util/fst/BytesStore;->blockSize:I

    return v0
.end method

.method static synthetic access$1(Lorg/apache/lucene/util/fst/BytesStore;)Ljava/util/List;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lorg/apache/lucene/util/fst/BytesStore;->blocks:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$2(Lorg/apache/lucene/util/fst/BytesStore;)I
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lorg/apache/lucene/util/fst/BytesStore;->blockBits:I

    return v0
.end method

.method static synthetic access$3(Lorg/apache/lucene/util/fst/BytesStore;)I
    .locals 1

    .prologue
    .line 36
    iget v0, p0, Lorg/apache/lucene/util/fst/BytesStore;->blockMask:I

    return v0
.end method


# virtual methods
.method public copyBytes(JJI)V
    .locals 13
    .param p1, "src"    # J
    .param p3, "dest"    # J
    .param p5, "len"    # I

    .prologue
    .line 179
    sget-boolean v3, Lorg/apache/lucene/util/fst/BytesStore;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    cmp-long v3, p1, p3

    if-ltz v3, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 207
    :cond_0
    move/from16 v0, p5

    int-to-long v4, v0

    add-long v10, p1, v4

    .line 209
    .local v10, "end":J
    iget v3, p0, Lorg/apache/lucene/util/fst/BytesStore;->blockBits:I

    shr-long v4, v10, v3

    long-to-int v2, v4

    .line 210
    .local v2, "blockIndex":I
    iget v3, p0, Lorg/apache/lucene/util/fst/BytesStore;->blockMask:I

    int-to-long v4, v3

    and-long/2addr v4, v10

    long-to-int v8, v4

    .line 211
    .local v8, "downTo":I
    if-nez v8, :cond_1

    .line 212
    add-int/lit8 v2, v2, -0x1

    .line 213
    iget v8, p0, Lorg/apache/lucene/util/fst/BytesStore;->blockSize:I

    .line 215
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/util/fst/BytesStore;->blocks:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [B

    .line 217
    .local v6, "block":[B
    :goto_0
    if-gtz p5, :cond_2

    .line 232
    .end local v8    # "downTo":I
    :goto_1
    return-void

    .line 219
    .restart local v8    # "downTo":I
    :cond_2
    move/from16 v0, p5

    if-gt v0, v8, :cond_3

    .line 221
    sub-int v7, v8, p5

    move-object v3, p0

    move-wide/from16 v4, p3

    move/from16 v8, p5

    invoke-virtual/range {v3 .. v8}, Lorg/apache/lucene/util/fst/BytesStore;->writeBytes(J[BII)V

    goto :goto_1

    .line 225
    :cond_3
    sub-int p5, p5, v8

    .line 226
    move/from16 v0, p5

    int-to-long v4, v0

    add-long v4, v4, p3

    const/4 v7, 0x0

    move-object v3, p0

    invoke-virtual/range {v3 .. v8}, Lorg/apache/lucene/util/fst/BytesStore;->writeBytes(J[BII)V

    .line 227
    add-int/lit8 v2, v2, -0x1

    .line 228
    iget-object v3, p0, Lorg/apache/lucene/util/fst/BytesStore;->blocks:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "block":[B
    check-cast v6, [B

    .line 229
    .restart local v6    # "block":[B
    iget v8, p0, Lorg/apache/lucene/util/fst/BytesStore;->blockSize:I

    goto :goto_0
.end method

.method public finish()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 331
    iget-object v1, p0, Lorg/apache/lucene/util/fst/BytesStore;->current:[B

    if-eqz v1, :cond_0

    .line 332
    iget v1, p0, Lorg/apache/lucene/util/fst/BytesStore;->nextWrite:I

    new-array v0, v1, [B

    .line 333
    .local v0, "lastBuffer":[B
    iget-object v1, p0, Lorg/apache/lucene/util/fst/BytesStore;->current:[B

    iget v2, p0, Lorg/apache/lucene/util/fst/BytesStore;->nextWrite:I

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 334
    iget-object v1, p0, Lorg/apache/lucene/util/fst/BytesStore;->blocks:Ljava/util/List;

    iget-object v2, p0, Lorg/apache/lucene/util/fst/BytesStore;->blocks:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v1, v2, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 335
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/lucene/util/fst/BytesStore;->current:[B

    .line 337
    .end local v0    # "lastBuffer":[B
    :cond_0
    return-void
.end method

.method getBlockBits()I
    .locals 1

    .prologue
    .line 112
    iget v0, p0, Lorg/apache/lucene/util/fst/BytesStore;->blockBits:I

    return v0
.end method

.method public getForwardReader()Lorg/apache/lucene/util/fst/FST$BytesReader;
    .locals 3

    .prologue
    .line 347
    iget-object v0, p0, Lorg/apache/lucene/util/fst/BytesStore;->blocks:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 348
    new-instance v1, Lorg/apache/lucene/util/fst/ForwardBytesReader;

    iget-object v0, p0, Lorg/apache/lucene/util/fst/BytesStore;->blocks:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    invoke-direct {v1, v0}, Lorg/apache/lucene/util/fst/ForwardBytesReader;-><init>([B)V

    move-object v0, v1

    .line 350
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/apache/lucene/util/fst/BytesStore$1;

    invoke-direct {v0, p0}, Lorg/apache/lucene/util/fst/BytesStore$1;-><init>(Lorg/apache/lucene/util/fst/BytesStore;)V

    goto :goto_0
.end method

.method public getPosition()J
    .locals 4

    .prologue
    .line 307
    iget-object v0, p0, Lorg/apache/lucene/util/fst/BytesStore;->blocks:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    int-to-long v0, v0

    const-wide/16 v2, 0x1

    sub-long/2addr v0, v2

    iget v2, p0, Lorg/apache/lucene/util/fst/BytesStore;->blockSize:I

    int-to-long v2, v2

    mul-long/2addr v0, v2

    iget v2, p0, Lorg/apache/lucene/util/fst/BytesStore;->nextWrite:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public getReverseReader()Lorg/apache/lucene/util/fst/FST$BytesReader;
    .locals 1

    .prologue
    .line 411
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lorg/apache/lucene/util/fst/BytesStore;->getReverseReader(Z)Lorg/apache/lucene/util/fst/FST$BytesReader;

    move-result-object v0

    return-object v0
.end method

.method getReverseReader(Z)Lorg/apache/lucene/util/fst/FST$BytesReader;
    .locals 3
    .param p1, "allowSingle"    # Z

    .prologue
    .line 415
    if-eqz p1, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/util/fst/BytesStore;->blocks:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 416
    new-instance v1, Lorg/apache/lucene/util/fst/ReverseBytesReader;

    iget-object v0, p0, Lorg/apache/lucene/util/fst/BytesStore;->blocks:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    invoke-direct {v1, v0}, Lorg/apache/lucene/util/fst/ReverseBytesReader;-><init>([B)V

    move-object v0, v1

    .line 418
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/apache/lucene/util/fst/BytesStore$2;

    invoke-direct {v0, p0}, Lorg/apache/lucene/util/fst/BytesStore$2;-><init>(Lorg/apache/lucene/util/fst/BytesStore;)V

    goto :goto_0
.end method

.method public reverse(JJ)V
    .locals 15
    .param p1, "srcPos"    # J
    .param p3, "destPos"    # J

    .prologue
    .line 254
    sget-boolean v9, Lorg/apache/lucene/util/fst/BytesStore;->$assertionsDisabled:Z

    if-nez v9, :cond_0

    cmp-long v9, p1, p3

    if-ltz v9, :cond_0

    new-instance v9, Ljava/lang/AssertionError;

    invoke-direct {v9}, Ljava/lang/AssertionError;-><init>()V

    throw v9

    .line 255
    :cond_0
    sget-boolean v9, Lorg/apache/lucene/util/fst/BytesStore;->$assertionsDisabled:Z

    if-nez v9, :cond_1

    invoke-virtual {p0}, Lorg/apache/lucene/util/fst/BytesStore;->getPosition()J

    move-result-wide v10

    cmp-long v9, p3, v10

    if-ltz v9, :cond_1

    new-instance v9, Ljava/lang/AssertionError;

    invoke-direct {v9}, Ljava/lang/AssertionError;-><init>()V

    throw v9

    .line 258
    :cond_1
    iget v9, p0, Lorg/apache/lucene/util/fst/BytesStore;->blockBits:I

    shr-long v10, p1, v9

    long-to-int v8, v10

    .line 259
    .local v8, "srcBlockIndex":I
    iget v9, p0, Lorg/apache/lucene/util/fst/BytesStore;->blockMask:I

    int-to-long v10, v9

    and-long v10, v10, p1

    long-to-int v6, v10

    .line 260
    .local v6, "src":I
    iget-object v9, p0, Lorg/apache/lucene/util/fst/BytesStore;->blocks:Ljava/util/List;

    invoke-interface {v9, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [B

    .line 262
    .local v7, "srcBlock":[B
    iget v9, p0, Lorg/apache/lucene/util/fst/BytesStore;->blockBits:I

    shr-long v10, p3, v9

    long-to-int v3, v10

    .line 263
    .local v3, "destBlockIndex":I
    iget v9, p0, Lorg/apache/lucene/util/fst/BytesStore;->blockMask:I

    int-to-long v10, v9

    and-long v10, v10, p3

    long-to-int v1, v10

    .line 264
    .local v1, "dest":I
    iget-object v9, p0, Lorg/apache/lucene/util/fst/BytesStore;->blocks:Ljava/util/List;

    invoke-interface {v9, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [B

    .line 267
    .local v2, "destBlock":[B
    sub-long v10, p3, p1

    const-wide/16 v12, 0x1

    add-long/2addr v10, v12

    long-to-int v9, v10

    div-int/lit8 v5, v9, 0x2

    .line 268
    .local v5, "limit":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-lt v4, v5, :cond_2

    .line 289
    return-void

    .line 270
    :cond_2
    aget-byte v0, v7, v6

    .line 271
    .local v0, "b":B
    aget-byte v9, v2, v1

    aput-byte v9, v7, v6

    .line 272
    aput-byte v0, v2, v1

    .line 273
    add-int/lit8 v6, v6, 0x1

    .line 274
    iget v9, p0, Lorg/apache/lucene/util/fst/BytesStore;->blockSize:I

    if-ne v6, v9, :cond_3

    .line 275
    add-int/lit8 v8, v8, 0x1

    .line 276
    iget-object v9, p0, Lorg/apache/lucene/util/fst/BytesStore;->blocks:Ljava/util/List;

    invoke-interface {v9, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    .end local v7    # "srcBlock":[B
    check-cast v7, [B

    .line 278
    .restart local v7    # "srcBlock":[B
    const/4 v6, 0x0

    .line 281
    :cond_3
    add-int/lit8 v1, v1, -0x1

    .line 282
    const/4 v9, -0x1

    if-ne v1, v9, :cond_4

    .line 283
    add-int/lit8 v3, v3, -0x1

    .line 284
    iget-object v9, p0, Lorg/apache/lucene/util/fst/BytesStore;->blocks:Ljava/util/List;

    invoke-interface {v9, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "destBlock":[B
    check-cast v2, [B

    .line 286
    .restart local v2    # "destBlock":[B
    iget v9, p0, Lorg/apache/lucene/util/fst/BytesStore;->blockSize:I

    add-int/lit8 v1, v9, -0x1

    .line 268
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method public skipBytes(I)V
    .locals 3
    .param p1, "len"    # I

    .prologue
    .line 292
    :goto_0
    if-gtz p1, :cond_0

    .line 304
    :goto_1
    return-void

    .line 293
    :cond_0
    iget v1, p0, Lorg/apache/lucene/util/fst/BytesStore;->blockSize:I

    iget v2, p0, Lorg/apache/lucene/util/fst/BytesStore;->nextWrite:I

    sub-int v0, v1, v2

    .line 294
    .local v0, "chunk":I
    if-gt p1, v0, :cond_1

    .line 295
    iget v1, p0, Lorg/apache/lucene/util/fst/BytesStore;->nextWrite:I

    add-int/2addr v1, p1

    iput v1, p0, Lorg/apache/lucene/util/fst/BytesStore;->nextWrite:I

    goto :goto_1

    .line 298
    :cond_1
    sub-int/2addr p1, v0

    .line 299
    iget v1, p0, Lorg/apache/lucene/util/fst/BytesStore;->blockSize:I

    new-array v1, v1, [B

    iput-object v1, p0, Lorg/apache/lucene/util/fst/BytesStore;->current:[B

    .line 300
    iget-object v1, p0, Lorg/apache/lucene/util/fst/BytesStore;->blocks:Ljava/util/List;

    iget-object v2, p0, Lorg/apache/lucene/util/fst/BytesStore;->current:[B

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 301
    const/4 v1, 0x0

    iput v1, p0, Lorg/apache/lucene/util/fst/BytesStore;->nextWrite:I

    goto :goto_0
.end method

.method public truncate(J)V
    .locals 7
    .param p1, "newLen"    # J

    .prologue
    const-wide/16 v4, 0x0

    .line 313
    sget-boolean v1, Lorg/apache/lucene/util/fst/BytesStore;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/util/fst/BytesStore;->getPosition()J

    move-result-wide v2

    cmp-long v1, p1, v2

    if-lez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 314
    :cond_0
    sget-boolean v1, Lorg/apache/lucene/util/fst/BytesStore;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    cmp-long v1, p1, v4

    if-gez v1, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 315
    :cond_1
    iget v1, p0, Lorg/apache/lucene/util/fst/BytesStore;->blockBits:I

    shr-long v2, p1, v1

    long-to-int v0, v2

    .line 316
    .local v0, "blockIndex":I
    iget v1, p0, Lorg/apache/lucene/util/fst/BytesStore;->blockMask:I

    int-to-long v2, v1

    and-long/2addr v2, p1

    long-to-int v1, v2

    iput v1, p0, Lorg/apache/lucene/util/fst/BytesStore;->nextWrite:I

    .line 317
    iget v1, p0, Lorg/apache/lucene/util/fst/BytesStore;->nextWrite:I

    if-nez v1, :cond_2

    .line 318
    add-int/lit8 v0, v0, -0x1

    .line 319
    iget v1, p0, Lorg/apache/lucene/util/fst/BytesStore;->blockSize:I

    iput v1, p0, Lorg/apache/lucene/util/fst/BytesStore;->nextWrite:I

    .line 321
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/util/fst/BytesStore;->blocks:Ljava/util/List;

    add-int/lit8 v2, v0, 0x1

    iget-object v3, p0, Lorg/apache/lucene/util/fst/BytesStore;->blocks:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-interface {v1, v2, v3}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 322
    cmp-long v1, p1, v4

    if-nez v1, :cond_3

    .line 323
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/lucene/util/fst/BytesStore;->current:[B

    .line 327
    :goto_0
    sget-boolean v1, Lorg/apache/lucene/util/fst/BytesStore;->$assertionsDisabled:Z

    if-nez v1, :cond_4

    invoke-virtual {p0}, Lorg/apache/lucene/util/fst/BytesStore;->getPosition()J

    move-result-wide v2

    cmp-long v1, p1, v2

    if-eqz v1, :cond_4

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 325
    :cond_3
    iget-object v1, p0, Lorg/apache/lucene/util/fst/BytesStore;->blocks:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    iput-object v1, p0, Lorg/apache/lucene/util/fst/BytesStore;->current:[B

    goto :goto_0

    .line 328
    :cond_4
    return-void
.end method

.method public writeByte(B)V
    .locals 3
    .param p1, "b"    # B

    .prologue
    .line 82
    iget v0, p0, Lorg/apache/lucene/util/fst/BytesStore;->nextWrite:I

    iget v1, p0, Lorg/apache/lucene/util/fst/BytesStore;->blockSize:I

    if-ne v0, v1, :cond_0

    .line 83
    iget v0, p0, Lorg/apache/lucene/util/fst/BytesStore;->blockSize:I

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/lucene/util/fst/BytesStore;->current:[B

    .line 84
    iget-object v0, p0, Lorg/apache/lucene/util/fst/BytesStore;->blocks:Ljava/util/List;

    iget-object v1, p0, Lorg/apache/lucene/util/fst/BytesStore;->current:[B

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 85
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/util/fst/BytesStore;->nextWrite:I

    .line 87
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/util/fst/BytesStore;->current:[B

    iget v1, p0, Lorg/apache/lucene/util/fst/BytesStore;->nextWrite:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/util/fst/BytesStore;->nextWrite:I

    aput-byte p1, v0, v1

    .line 88
    return-void
.end method

.method public writeByte(IB)V
    .locals 3
    .param p1, "dest"    # I
    .param p2, "b"    # B

    .prologue
    .line 75
    iget v2, p0, Lorg/apache/lucene/util/fst/BytesStore;->blockBits:I

    shr-int v1, p1, v2

    .line 76
    .local v1, "blockIndex":I
    iget-object v2, p0, Lorg/apache/lucene/util/fst/BytesStore;->blocks:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 77
    .local v0, "block":[B
    iget v2, p0, Lorg/apache/lucene/util/fst/BytesStore;->blockMask:I

    and-int/2addr v2, p1

    aput-byte p2, v0, v2

    .line 78
    return-void
.end method

.method writeBytes(J[BII)V
    .locals 15
    .param p1, "dest"    # J
    .param p3, "b"    # [B
    .param p4, "offset"    # I
    .param p5, "len"    # I

    .prologue
    .line 120
    sget-boolean v7, Lorg/apache/lucene/util/fst/BytesStore;->$assertionsDisabled:Z

    if-nez v7, :cond_0

    move/from16 v0, p5

    int-to-long v10, v0

    add-long v10, v10, p1

    invoke-virtual {p0}, Lorg/apache/lucene/util/fst/BytesStore;->getPosition()J

    move-result-wide v12

    cmp-long v7, v10, v12

    if-lez v7, :cond_0

    new-instance v7, Ljava/lang/AssertionError;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "dest="

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p1

    invoke-virtual {v10, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " pos="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {p0}, Lorg/apache/lucene/util/fst/BytesStore;->getPosition()J

    move-result-wide v12

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " len="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, p5

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v7, v10}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v7

    .line 148
    :cond_0
    move/from16 v0, p5

    int-to-long v10, v0

    add-long v8, p1, v10

    .line 149
    .local v8, "end":J
    iget v7, p0, Lorg/apache/lucene/util/fst/BytesStore;->blockBits:I

    shr-long v10, v8, v7

    long-to-int v5, v10

    .line 150
    .local v5, "blockIndex":I
    iget v7, p0, Lorg/apache/lucene/util/fst/BytesStore;->blockMask:I

    int-to-long v10, v7

    and-long/2addr v10, v8

    long-to-int v6, v10

    .line 151
    .local v6, "downTo":I
    if-nez v6, :cond_1

    .line 152
    add-int/lit8 v5, v5, -0x1

    .line 153
    iget v6, p0, Lorg/apache/lucene/util/fst/BytesStore;->blockSize:I

    .line 155
    :cond_1
    iget-object v7, p0, Lorg/apache/lucene/util/fst/BytesStore;->blocks:Ljava/util/List;

    invoke-interface {v7, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [B

    .line 157
    .local v4, "block":[B
    :goto_0
    if-gtz p5, :cond_2

    .line 172
    :goto_1
    return-void

    .line 159
    :cond_2
    move/from16 v0, p5

    if-gt v0, v6, :cond_3

    .line 161
    sub-int v7, v6, p5

    move-object/from16 v0, p3

    move/from16 v1, p4

    move/from16 v2, p5

    invoke-static {v0, v1, v4, v7, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_1

    .line 164
    :cond_3
    sub-int p5, p5, v6

    .line 166
    add-int v7, p4, p5

    const/4 v10, 0x0

    move-object/from16 v0, p3

    invoke-static {v0, v7, v4, v10, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 167
    add-int/lit8 v5, v5, -0x1

    .line 168
    iget-object v7, p0, Lorg/apache/lucene/util/fst/BytesStore;->blocks:Ljava/util/List;

    invoke-interface {v7, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "block":[B
    check-cast v4, [B

    .line 169
    .restart local v4    # "block":[B
    iget v6, p0, Lorg/apache/lucene/util/fst/BytesStore;->blockSize:I

    goto :goto_0
.end method

.method public writeBytes([BII)V
    .locals 3
    .param p1, "b"    # [B
    .param p2, "offset"    # I
    .param p3, "len"    # I

    .prologue
    .line 92
    :goto_0
    if-gtz p3, :cond_0

    .line 109
    :goto_1
    return-void

    .line 93
    :cond_0
    iget v1, p0, Lorg/apache/lucene/util/fst/BytesStore;->blockSize:I

    iget v2, p0, Lorg/apache/lucene/util/fst/BytesStore;->nextWrite:I

    sub-int v0, v1, v2

    .line 94
    .local v0, "chunk":I
    if-gt p3, v0, :cond_1

    .line 95
    iget-object v1, p0, Lorg/apache/lucene/util/fst/BytesStore;->current:[B

    iget v2, p0, Lorg/apache/lucene/util/fst/BytesStore;->nextWrite:I

    invoke-static {p1, p2, v1, v2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 96
    iget v1, p0, Lorg/apache/lucene/util/fst/BytesStore;->nextWrite:I

    add-int/2addr v1, p3

    iput v1, p0, Lorg/apache/lucene/util/fst/BytesStore;->nextWrite:I

    goto :goto_1

    .line 99
    :cond_1
    if-lez v0, :cond_2

    .line 100
    iget-object v1, p0, Lorg/apache/lucene/util/fst/BytesStore;->current:[B

    iget v2, p0, Lorg/apache/lucene/util/fst/BytesStore;->nextWrite:I

    invoke-static {p1, p2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 101
    add-int/2addr p2, v0

    .line 102
    sub-int/2addr p3, v0

    .line 104
    :cond_2
    iget v1, p0, Lorg/apache/lucene/util/fst/BytesStore;->blockSize:I

    new-array v1, v1, [B

    iput-object v1, p0, Lorg/apache/lucene/util/fst/BytesStore;->current:[B

    .line 105
    iget-object v1, p0, Lorg/apache/lucene/util/fst/BytesStore;->blocks:Ljava/util/List;

    iget-object v2, p0, Lorg/apache/lucene/util/fst/BytesStore;->current:[B

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 106
    const/4 v1, 0x0

    iput v1, p0, Lorg/apache/lucene/util/fst/BytesStore;->nextWrite:I

    goto :goto_0
.end method

.method public writeInt(JI)V
    .locals 9
    .param p1, "pos"    # J
    .param p3, "value"    # I

    .prologue
    .line 237
    iget v6, p0, Lorg/apache/lucene/util/fst/BytesStore;->blockBits:I

    shr-long v6, p1, v6

    long-to-int v1, v6

    .line 238
    .local v1, "blockIndex":I
    iget v6, p0, Lorg/apache/lucene/util/fst/BytesStore;->blockMask:I

    int-to-long v6, v6

    and-long/2addr v6, p1

    long-to-int v4, v6

    .line 239
    .local v4, "upto":I
    iget-object v6, p0, Lorg/apache/lucene/util/fst/BytesStore;->blocks:Ljava/util/List;

    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 240
    .local v0, "block":[B
    const/16 v3, 0x18

    .line 241
    .local v3, "shift":I
    const/4 v2, 0x0

    .local v2, "i":I
    move v5, v4

    .end local v4    # "upto":I
    .local v5, "upto":I
    :goto_0
    const/4 v6, 0x4

    if-lt v2, v6, :cond_0

    .line 250
    return-void

    .line 242
    :cond_0
    add-int/lit8 v4, v5, 0x1

    .end local v5    # "upto":I
    .restart local v4    # "upto":I
    shr-int v6, p3, v3

    int-to-byte v6, v6

    aput-byte v6, v0, v5

    .line 243
    add-int/lit8 v3, v3, -0x8

    .line 244
    iget v6, p0, Lorg/apache/lucene/util/fst/BytesStore;->blockSize:I

    if-ne v4, v6, :cond_1

    .line 245
    const/4 v4, 0x0

    .line 246
    add-int/lit8 v1, v1, 0x1

    .line 247
    iget-object v6, p0, Lorg/apache/lucene/util/fst/BytesStore;->blocks:Ljava/util/List;

    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "block":[B
    check-cast v0, [B

    .line 241
    .restart local v0    # "block":[B
    :cond_1
    add-int/lit8 v2, v2, 0x1

    move v5, v4

    .end local v4    # "upto":I
    .restart local v5    # "upto":I
    goto :goto_0
.end method

.method public writeTo(Lorg/apache/lucene/store/DataOutput;)V
    .locals 4
    .param p1, "out"    # Lorg/apache/lucene/store/DataOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 341
    iget-object v1, p0, Lorg/apache/lucene/util/fst/BytesStore;->blocks:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 344
    return-void

    .line 341
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 342
    .local v0, "block":[B
    const/4 v2, 0x0

    array-length v3, v0

    invoke-virtual {p1, v0, v2, v3}, Lorg/apache/lucene/store/DataOutput;->writeBytes([BII)V

    goto :goto_0
.end method
