.class abstract Lorg/apache/lucene/util/packed/PackedInts$MutableImpl;
.super Lorg/apache/lucene/util/packed/PackedInts$ReaderImpl;
.source "PackedInts.java"

# interfaces
.implements Lorg/apache/lucene/util/packed/PackedInts$Mutable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/packed/PackedInts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x408
    name = "MutableImpl"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 640
    const-class v0, Lorg/apache/lucene/util/packed/PackedInts;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/packed/PackedInts$MutableImpl;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected constructor <init>(II)V
    .locals 0
    .param p1, "valueCount"    # I
    .param p2, "bitsPerValue"    # I

    .prologue
    .line 643
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/util/packed/PackedInts$ReaderImpl;-><init>(II)V

    .line 644
    return-void
.end method


# virtual methods
.method public fill(IIJ)V
    .locals 5
    .param p1, "fromIndex"    # I
    .param p2, "toIndex"    # I
    .param p3, "val"    # J

    .prologue
    .line 661
    sget-boolean v1, Lorg/apache/lucene/util/packed/PackedInts$MutableImpl;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget v1, p0, Lorg/apache/lucene/util/packed/PackedInts$MutableImpl;->bitsPerValue:I

    invoke-static {v1}, Lorg/apache/lucene/util/packed/PackedInts;->maxValue(I)J

    move-result-wide v2

    cmp-long v1, p3, v2

    if-lez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 662
    :cond_0
    sget-boolean v1, Lorg/apache/lucene/util/packed/PackedInts$MutableImpl;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    if-le p1, p2, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 663
    :cond_1
    move v0, p1

    .local v0, "i":I
    :goto_0
    if-lt v0, p2, :cond_2

    .line 666
    return-void

    .line 664
    :cond_2
    invoke-virtual {p0, v0, p3, p4}, Lorg/apache/lucene/util/packed/PackedInts$MutableImpl;->set(IJ)V

    .line 663
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected getFormat()Lorg/apache/lucene/util/packed/PackedInts$Format;
    .locals 1

    .prologue
    .line 669
    sget-object v0, Lorg/apache/lucene/util/packed/PackedInts$Format;->PACKED:Lorg/apache/lucene/util/packed/PackedInts$Format;

    return-object v0
.end method

.method public save(Lorg/apache/lucene/store/DataOutput;)V
    .locals 6
    .param p1, "out"    # Lorg/apache/lucene/store/DataOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 674
    invoke-virtual {p0}, Lorg/apache/lucene/util/packed/PackedInts$MutableImpl;->getFormat()Lorg/apache/lucene/util/packed/PackedInts$Format;

    move-result-object v2

    .line 675
    iget v3, p0, Lorg/apache/lucene/util/packed/PackedInts$MutableImpl;->valueCount:I

    iget v4, p0, Lorg/apache/lucene/util/packed/PackedInts$MutableImpl;->bitsPerValue:I

    const/16 v5, 0x400

    .line 674
    invoke-static {p1, v2, v3, v4, v5}, Lorg/apache/lucene/util/packed/PackedInts;->getWriterNoHeader(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/packed/PackedInts$Format;III)Lorg/apache/lucene/util/packed/PackedInts$Writer;

    move-result-object v1

    .line 676
    .local v1, "writer":Lorg/apache/lucene/util/packed/PackedInts$Writer;
    invoke-virtual {v1}, Lorg/apache/lucene/util/packed/PackedInts$Writer;->writeHeader()V

    .line 677
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lorg/apache/lucene/util/packed/PackedInts$MutableImpl;->valueCount:I

    if-lt v0, v2, :cond_0

    .line 680
    invoke-virtual {v1}, Lorg/apache/lucene/util/packed/PackedInts$Writer;->finish()V

    .line 681
    return-void

    .line 678
    :cond_0
    invoke-virtual {p0, v0}, Lorg/apache/lucene/util/packed/PackedInts$MutableImpl;->get(I)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/util/packed/PackedInts$Writer;->add(J)V

    .line 677
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public set(I[JII)I
    .locals 6
    .param p1, "index"    # I
    .param p2, "arr"    # [J
    .param p3, "off"    # I
    .param p4, "len"    # I

    .prologue
    .line 648
    sget-boolean v3, Lorg/apache/lucene/util/packed/PackedInts$MutableImpl;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    if-gtz p4, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "len must be > 0 (got "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 649
    :cond_0
    sget-boolean v3, Lorg/apache/lucene/util/packed/PackedInts$MutableImpl;->$assertionsDisabled:Z

    if-nez v3, :cond_2

    if-ltz p1, :cond_1

    iget v3, p0, Lorg/apache/lucene/util/packed/PackedInts$MutableImpl;->valueCount:I

    if-lt p1, v3, :cond_2

    :cond_1
    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 650
    :cond_2
    iget v3, p0, Lorg/apache/lucene/util/packed/PackedInts$MutableImpl;->valueCount:I

    sub-int/2addr v3, p1

    invoke-static {p4, v3}, Ljava/lang/Math;->min(II)I

    move-result p4

    .line 651
    sget-boolean v3, Lorg/apache/lucene/util/packed/PackedInts$MutableImpl;->$assertionsDisabled:Z

    if-nez v3, :cond_3

    add-int v3, p3, p4

    array-length v4, p2

    if-le v3, v4, :cond_3

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 653
    :cond_3
    move v1, p1

    .local v1, "i":I
    move v2, p3

    .local v2, "o":I
    add-int v0, p1, p4

    .local v0, "end":I
    :goto_0
    if-lt v1, v0, :cond_4

    .line 656
    return p4

    .line 654
    :cond_4
    aget-wide v4, p2, v2

    invoke-virtual {p0, v1, v4, v5}, Lorg/apache/lucene/util/packed/PackedInts$MutableImpl;->set(IJ)V

    .line 653
    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method
