.class final Lorg/apache/lucene/util/automaton/MinimizationOperations$StateListNode;
.super Ljava/lang/Object;
.source "MinimizationOperations.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/automaton/MinimizationOperations;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "StateListNode"
.end annotation


# instance fields
.field next:Lorg/apache/lucene/util/automaton/MinimizationOperations$StateListNode;

.field prev:Lorg/apache/lucene/util/automaton/MinimizationOperations$StateListNode;

.field final q:Lorg/apache/lucene/util/automaton/State;

.field final sl:Lorg/apache/lucene/util/automaton/MinimizationOperations$StateList;


# direct methods
.method constructor <init>(Lorg/apache/lucene/util/automaton/State;Lorg/apache/lucene/util/automaton/MinimizationOperations$StateList;)V
    .locals 2
    .param p1, "q"    # Lorg/apache/lucene/util/automaton/State;
    .param p2, "sl"    # Lorg/apache/lucene/util/automaton/MinimizationOperations$StateList;

    .prologue
    .line 241
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 242
    iput-object p1, p0, Lorg/apache/lucene/util/automaton/MinimizationOperations$StateListNode;->q:Lorg/apache/lucene/util/automaton/State;

    .line 243
    iput-object p2, p0, Lorg/apache/lucene/util/automaton/MinimizationOperations$StateListNode;->sl:Lorg/apache/lucene/util/automaton/MinimizationOperations$StateList;

    .line 244
    iget v0, p2, Lorg/apache/lucene/util/automaton/MinimizationOperations$StateList;->size:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p2, Lorg/apache/lucene/util/automaton/MinimizationOperations$StateList;->size:I

    if-nez v0, :cond_0

    iput-object p0, p2, Lorg/apache/lucene/util/automaton/MinimizationOperations$StateList;->last:Lorg/apache/lucene/util/automaton/MinimizationOperations$StateListNode;

    iput-object p0, p2, Lorg/apache/lucene/util/automaton/MinimizationOperations$StateList;->first:Lorg/apache/lucene/util/automaton/MinimizationOperations$StateListNode;

    .line 250
    :goto_0
    return-void

    .line 246
    :cond_0
    iget-object v0, p2, Lorg/apache/lucene/util/automaton/MinimizationOperations$StateList;->last:Lorg/apache/lucene/util/automaton/MinimizationOperations$StateListNode;

    iput-object p0, v0, Lorg/apache/lucene/util/automaton/MinimizationOperations$StateListNode;->next:Lorg/apache/lucene/util/automaton/MinimizationOperations$StateListNode;

    .line 247
    iget-object v0, p2, Lorg/apache/lucene/util/automaton/MinimizationOperations$StateList;->last:Lorg/apache/lucene/util/automaton/MinimizationOperations$StateListNode;

    iput-object v0, p0, Lorg/apache/lucene/util/automaton/MinimizationOperations$StateListNode;->prev:Lorg/apache/lucene/util/automaton/MinimizationOperations$StateListNode;

    .line 248
    iput-object p0, p2, Lorg/apache/lucene/util/automaton/MinimizationOperations$StateList;->last:Lorg/apache/lucene/util/automaton/MinimizationOperations$StateListNode;

    goto :goto_0
.end method


# virtual methods
.method remove()V
    .locals 2

    .prologue
    .line 253
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/MinimizationOperations$StateListNode;->sl:Lorg/apache/lucene/util/automaton/MinimizationOperations$StateList;

    iget v1, v0, Lorg/apache/lucene/util/automaton/MinimizationOperations$StateList;->size:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Lorg/apache/lucene/util/automaton/MinimizationOperations$StateList;->size:I

    .line 254
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/MinimizationOperations$StateListNode;->sl:Lorg/apache/lucene/util/automaton/MinimizationOperations$StateList;

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/MinimizationOperations$StateList;->first:Lorg/apache/lucene/util/automaton/MinimizationOperations$StateListNode;

    if-ne v0, p0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/util/automaton/MinimizationOperations$StateListNode;->sl:Lorg/apache/lucene/util/automaton/MinimizationOperations$StateList;

    iget-object v1, p0, Lorg/apache/lucene/util/automaton/MinimizationOperations$StateListNode;->next:Lorg/apache/lucene/util/automaton/MinimizationOperations$StateListNode;

    iput-object v1, v0, Lorg/apache/lucene/util/automaton/MinimizationOperations$StateList;->first:Lorg/apache/lucene/util/automaton/MinimizationOperations$StateListNode;

    .line 256
    :goto_0
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/MinimizationOperations$StateListNode;->sl:Lorg/apache/lucene/util/automaton/MinimizationOperations$StateList;

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/MinimizationOperations$StateList;->last:Lorg/apache/lucene/util/automaton/MinimizationOperations$StateListNode;

    if-ne v0, p0, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/util/automaton/MinimizationOperations$StateListNode;->sl:Lorg/apache/lucene/util/automaton/MinimizationOperations$StateList;

    iget-object v1, p0, Lorg/apache/lucene/util/automaton/MinimizationOperations$StateListNode;->prev:Lorg/apache/lucene/util/automaton/MinimizationOperations$StateListNode;

    iput-object v1, v0, Lorg/apache/lucene/util/automaton/MinimizationOperations$StateList;->last:Lorg/apache/lucene/util/automaton/MinimizationOperations$StateListNode;

    .line 258
    :goto_1
    return-void

    .line 255
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/MinimizationOperations$StateListNode;->prev:Lorg/apache/lucene/util/automaton/MinimizationOperations$StateListNode;

    iget-object v1, p0, Lorg/apache/lucene/util/automaton/MinimizationOperations$StateListNode;->next:Lorg/apache/lucene/util/automaton/MinimizationOperations$StateListNode;

    iput-object v1, v0, Lorg/apache/lucene/util/automaton/MinimizationOperations$StateListNode;->next:Lorg/apache/lucene/util/automaton/MinimizationOperations$StateListNode;

    goto :goto_0

    .line 257
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/MinimizationOperations$StateListNode;->next:Lorg/apache/lucene/util/automaton/MinimizationOperations$StateListNode;

    iget-object v1, p0, Lorg/apache/lucene/util/automaton/MinimizationOperations$StateListNode;->prev:Lorg/apache/lucene/util/automaton/MinimizationOperations$StateListNode;

    iput-object v1, v0, Lorg/apache/lucene/util/automaton/MinimizationOperations$StateListNode;->prev:Lorg/apache/lucene/util/automaton/MinimizationOperations$StateListNode;

    goto :goto_1
.end method
