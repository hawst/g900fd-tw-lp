.class enum Lorg/apache/lucene/util/packed/PackedInts$Format$2;
.super Lorg/apache/lucene/util/packed/PackedInts$Format;
.source "PackedInts.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/packed/PackedInts$Format;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4000
    name = null
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;II)V
    .locals 1
    .param p3, "$anonymous0"    # I

    .prologue
    .line 108
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lorg/apache/lucene/util/packed/PackedInts$Format;-><init>(Ljava/lang/String;IILorg/apache/lucene/util/packed/PackedInts$Format;)V

    .line 1
    return-void
.end method


# virtual methods
.method public isSupported(I)Z
    .locals 1
    .param p1, "bitsPerValue"    # I

    .prologue
    .line 118
    invoke-static {p1}, Lorg/apache/lucene/util/packed/Packed64SingleBlock;->isSupported(I)Z

    move-result v0

    return v0
.end method

.method public longCount(III)I
    .locals 6
    .param p1, "packedIntsVersion"    # I
    .param p2, "valueCount"    # I
    .param p3, "bitsPerValue"    # I

    .prologue
    .line 112
    const/16 v1, 0x40

    div-int v0, v1, p3

    .line 113
    .local v0, "valuesPerBlock":I
    int-to-double v2, p2

    int-to-double v4, v0

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v1, v2

    return v1
.end method

.method public overheadPerValue(I)F
    .locals 4
    .param p1, "bitsPerValue"    # I

    .prologue
    const/16 v3, 0x40

    .line 123
    sget-boolean v2, Lorg/apache/lucene/util/packed/PackedInts$Format;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/packed/PackedInts$Format$2;->isSupported(I)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 124
    :cond_0
    div-int v1, v3, p1

    .line 125
    .local v1, "valuesPerBlock":I
    rem-int v0, v3, p1

    .line 126
    .local v0, "overhead":I
    int-to-float v2, v0

    int-to-float v3, v1

    div-float/2addr v2, v3

    return v2
.end method
