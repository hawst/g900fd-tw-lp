.class public Lorg/apache/lucene/util/packed/PackedInts$FormatAndBits;
.super Ljava/lang/Object;
.source "PackedInts.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/packed/PackedInts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FormatAndBits"
.end annotation


# instance fields
.field public final bitsPerValue:I

.field public final format:Lorg/apache/lucene/util/packed/PackedInts$Format;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/util/packed/PackedInts$Format;I)V
    .locals 0
    .param p1, "format"    # Lorg/apache/lucene/util/packed/PackedInts$Format;
    .param p2, "bitsPerValue"    # I

    .prologue
    .line 212
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 213
    iput-object p1, p0, Lorg/apache/lucene/util/packed/PackedInts$FormatAndBits;->format:Lorg/apache/lucene/util/packed/PackedInts$Format;

    .line 214
    iput p2, p0, Lorg/apache/lucene/util/packed/PackedInts$FormatAndBits;->bitsPerValue:I

    .line 215
    return-void
.end method
