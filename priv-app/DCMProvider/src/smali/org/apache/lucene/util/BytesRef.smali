.class public final Lorg/apache/lucene/util/BytesRef;
.super Ljava/lang/Object;
.source "BytesRef.java"

# interfaces
.implements Ljava/lang/Cloneable;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/BytesRef$UTF8SortedAsUTF16Comparator;,
        Lorg/apache/lucene/util/BytesRef$UTF8SortedAsUnicodeComparator;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lorg/apache/lucene/util/BytesRef;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final EMPTY_BYTES:[B

.field private static final utf8SortedAsUTF16SortOrder:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final utf8SortedAsUnicodeSortOrder:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public bytes:[B

.field public length:I

.field public offset:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 33
    const-class v0, Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/BytesRef;->$assertionsDisabled:Z

    .line 35
    new-array v0, v1, [B

    sput-object v0, Lorg/apache/lucene/util/BytesRef;->EMPTY_BYTES:[B

    .line 231
    new-instance v0, Lorg/apache/lucene/util/BytesRef$UTF8SortedAsUnicodeComparator;

    invoke-direct {v0, v2}, Lorg/apache/lucene/util/BytesRef$UTF8SortedAsUnicodeComparator;-><init>(Lorg/apache/lucene/util/BytesRef$UTF8SortedAsUnicodeComparator;)V

    sput-object v0, Lorg/apache/lucene/util/BytesRef;->utf8SortedAsUnicodeSortOrder:Ljava/util/Comparator;

    .line 266
    new-instance v0, Lorg/apache/lucene/util/BytesRef$UTF8SortedAsUTF16Comparator;

    invoke-direct {v0, v2}, Lorg/apache/lucene/util/BytesRef$UTF8SortedAsUTF16Comparator;-><init>(Lorg/apache/lucene/util/BytesRef$UTF8SortedAsUTF16Comparator;)V

    sput-object v0, Lorg/apache/lucene/util/BytesRef;->utf8SortedAsUTF16SortOrder:Ljava/util/Comparator;

    return-void

    :cond_0
    move v0, v1

    .line 33
    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lorg/apache/lucene/util/BytesRef;->EMPTY_BYTES:[B

    invoke-direct {p0, v0}, Lorg/apache/lucene/util/BytesRef;-><init>([B)V

    .line 49
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "capacity"    # I

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    new-array v0, p1, [B

    iput-object v0, p0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 73
    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 83
    invoke-direct {p0}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    .line 84
    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/BytesRef;->copyChars(Ljava/lang/CharSequence;)V

    .line 85
    return-void
.end method

.method public constructor <init>([B)V
    .locals 2
    .param p1, "bytes"    # [B

    .prologue
    .line 64
    const/4 v0, 0x0

    array-length v1, p1

    invoke-direct {p0, p1, v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>([BII)V

    .line 65
    return-void
.end method

.method public constructor <init>([BII)V
    .locals 1
    .param p1, "bytes"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object p1, p0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 56
    iput p2, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 57
    iput p3, p0, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 58
    sget-boolean v0, Lorg/apache/lucene/util/BytesRef;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/util/BytesRef;->isValid()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 59
    :cond_0
    return-void
.end method

.method public static deepCopyOf(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/BytesRef;
    .locals 1
    .param p0, "other"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 336
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    .line 337
    .local v0, "copy":Lorg/apache/lucene/util/BytesRef;
    invoke-virtual {v0, p0}, Lorg/apache/lucene/util/BytesRef;->copyBytes(Lorg/apache/lucene/util/BytesRef;)V

    .line 338
    return-object v0
.end method

.method public static getUTF8SortedAsUTF16Comparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 271
    sget-object v0, Lorg/apache/lucene/util/BytesRef;->utf8SortedAsUTF16SortOrder:Ljava/util/Comparator;

    return-object v0
.end method

.method public static getUTF8SortedAsUnicodeComparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation

    .prologue
    .line 234
    sget-object v0, Lorg/apache/lucene/util/BytesRef;->utf8SortedAsUnicodeSortOrder:Ljava/util/Comparator;

    return-object v0
.end method


# virtual methods
.method public append(Lorg/apache/lucene/util/BytesRef;)V
    .locals 7
    .param p1, "other"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    const/4 v5, 0x0

    .line 204
    iget v2, p0, Lorg/apache/lucene/util/BytesRef;->length:I

    iget v3, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int v1, v2, v3

    .line 205
    .local v1, "newLen":I
    iget-object v2, p0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    array-length v2, v2

    iget v3, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    sub-int/2addr v2, v3

    if-ge v2, v1, :cond_0

    .line 206
    new-array v0, v1, [B

    .line 207
    .local v0, "newBytes":[B
    iget-object v2, p0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v3, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget v4, p0, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-static {v2, v3, v0, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 208
    iput v5, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 209
    iput-object v0, p0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 211
    .end local v0    # "newBytes":[B
    :cond_0
    iget-object v2, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v3, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget-object v4, p0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v5, p0, Lorg/apache/lucene/util/BytesRef;->length:I

    iget v6, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    add-int/2addr v5, v6

    iget v6, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-static {v2, v3, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 212
    iput v1, p0, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 213
    return-void
.end method

.method public bytesEquals(Lorg/apache/lucene/util/BytesRef;)Z
    .locals 7
    .param p1, "other"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    const/4 v4, 0x0

    .line 106
    sget-boolean v5, Lorg/apache/lucene/util/BytesRef;->$assertionsDisabled:Z

    if-nez v5, :cond_0

    if-nez p1, :cond_0

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 107
    :cond_0
    iget v5, p0, Lorg/apache/lucene/util/BytesRef;->length:I

    iget v6, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    if-ne v5, v6, :cond_1

    .line 108
    iget v2, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 109
    .local v2, "otherUpto":I
    iget-object v1, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 110
    .local v1, "otherBytes":[B
    iget v5, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget v6, p0, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int v0, v5, v6

    .line 111
    .local v0, "end":I
    iget v3, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    .local v3, "upto":I
    :goto_0
    if-lt v3, v0, :cond_2

    .line 116
    const/4 v4, 0x1

    .line 118
    .end local v0    # "end":I
    .end local v1    # "otherBytes":[B
    .end local v2    # "otherUpto":I
    .end local v3    # "upto":I
    :cond_1
    return v4

    .line 112
    .restart local v0    # "end":I
    .restart local v1    # "otherBytes":[B
    .restart local v2    # "otherUpto":I
    .restart local v3    # "upto":I
    :cond_2
    iget-object v5, p0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    aget-byte v5, v5, v3

    aget-byte v6, v1, v2

    if-ne v5, v6, :cond_1

    .line 111
    add-int/lit8 v3, v3, 0x1

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/util/BytesRef;->clone()Lorg/apache/lucene/util/BytesRef;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lorg/apache/lucene/util/BytesRef;
    .locals 4

    .prologue
    .line 124
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    iget-object v1, p0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v2, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget v3, p0, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/util/BytesRef;-><init>([BII)V

    return-object v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/BytesRef;->compareTo(Lorg/apache/lucene/util/BytesRef;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lorg/apache/lucene/util/BytesRef;)I
    .locals 1
    .param p1, "other"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 228
    sget-object v0, Lorg/apache/lucene/util/BytesRef;->utf8SortedAsUnicodeSortOrder:Ljava/util/Comparator;

    invoke-interface {v0, p0, p1}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public copyBytes(Lorg/apache/lucene/util/BytesRef;)V
    .locals 5
    .param p1, "other"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 189
    iget-object v0, p0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    array-length v0, v0

    iget v1, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    sub-int/2addr v0, v1

    iget v1, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    if-ge v0, v1, :cond_0

    .line 190
    iget v0, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 191
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 193
    :cond_0
    iget-object v0, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v1, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget-object v2, p0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v3, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget v4, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 194
    iget v0, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    iput v0, p0, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 195
    return-void
.end method

.method public copyChars(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 94
    sget-boolean v0, Lorg/apache/lucene/util/BytesRef;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 95
    :cond_0
    const/4 v0, 0x0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    invoke-static {p1, v0, v1, p0}, Lorg/apache/lucene/util/UnicodeUtil;->UTF16toUTF8(Ljava/lang/CharSequence;IILorg/apache/lucene/util/BytesRef;)V

    .line 96
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x0

    .line 149
    if-nez p1, :cond_1

    .line 155
    .end local p1    # "other":Ljava/lang/Object;
    :cond_0
    :goto_0
    return v0

    .line 152
    .restart local p1    # "other":Ljava/lang/Object;
    :cond_1
    instance-of v1, p1, Lorg/apache/lucene/util/BytesRef;

    if-eqz v1, :cond_0

    .line 153
    check-cast p1, Lorg/apache/lucene/util/BytesRef;

    .end local p1    # "other":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/BytesRef;->bytesEquals(Lorg/apache/lucene/util/BytesRef;)Z

    move-result v0

    goto :goto_0
.end method

.method public grow(I)V
    .locals 1
    .param p1, "newLength"    # I

    .prologue
    .line 221
    sget-boolean v0, Lorg/apache/lucene/util/BytesRef;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 222
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    invoke-static {v0, p1}, Lorg/apache/lucene/util/ArrayUtil;->grow([BI)[B

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 223
    return-void
.end method

.method public hashCode()I
    .locals 5

    .prologue
    .line 139
    const/4 v1, 0x0

    .line 140
    .local v1, "hash":I
    iget v3, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget v4, p0, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int v0, v3, v4

    .line 141
    .local v0, "end":I
    iget v2, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    .local v2, "i":I
    :goto_0
    if-lt v2, v0, :cond_0

    .line 144
    return v1

    .line 142
    :cond_0
    mul-int/lit8 v3, v1, 0x1f

    iget-object v4, p0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    aget-byte v4, v4, v2

    add-int v1, v3, v4

    .line 141
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public isValid()Z
    .locals 3

    .prologue
    .line 346
    iget-object v0, p0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    if-nez v0, :cond_0

    .line 347
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "bytes is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 349
    :cond_0
    iget v0, p0, Lorg/apache/lucene/util/BytesRef;->length:I

    if-gez v0, :cond_1

    .line 350
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "length is negative: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 352
    :cond_1
    iget v0, p0, Lorg/apache/lucene/util/BytesRef;->length:I

    iget-object v1, p0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    array-length v1, v1

    if-le v0, v1, :cond_2

    .line 353
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "length is out of bounds: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",bytes.length="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 355
    :cond_2
    iget v0, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    if-gez v0, :cond_3

    .line 356
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "offset is negative: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 358
    :cond_3
    iget v0, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget-object v1, p0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    array-length v1, v1

    if-le v0, v1, :cond_4

    .line 359
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "offset out of bounds: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",bytes.length="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 361
    :cond_4
    iget v0, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget v1, p0, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/2addr v0, v1

    if-gez v0, :cond_5

    .line 362
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "offset+length is negative: offset="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",length="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 364
    :cond_5
    iget v0, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget v1, p0, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/2addr v0, v1

    iget-object v1, p0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    array-length v1, v1

    if-le v0, v1, :cond_6

    .line 365
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "offset+length out of bounds: offset="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",length="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",bytes.length="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 367
    :cond_6
    const/4 v0, 0x1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 169
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 170
    .local v2, "sb":Ljava/lang/StringBuilder;
    const/16 v3, 0x5b

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 171
    iget v3, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget v4, p0, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int v0, v3, v4

    .line 172
    .local v0, "end":I
    iget v1, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    .local v1, "i":I
    :goto_0
    if-lt v1, v0, :cond_0

    .line 178
    const/16 v3, 0x5d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 179
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 173
    :cond_0
    iget v3, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    if-le v1, v3, :cond_1

    .line 174
    const/16 v3, 0x20

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 176
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    aget-byte v3, v3, v1

    and-int/lit16 v3, v3, 0xff

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 172
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public utf8ToString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 161
    new-instance v0, Lorg/apache/lucene/util/CharsRef;

    iget v1, p0, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/CharsRef;-><init>(I)V

    .line 162
    .local v0, "ref":Lorg/apache/lucene/util/CharsRef;
    iget-object v1, p0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v2, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget v3, p0, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-static {v1, v2, v3, v0}, Lorg/apache/lucene/util/UnicodeUtil;->UTF8toUTF16([BIILorg/apache/lucene/util/CharsRef;)V

    .line 163
    invoke-virtual {v0}, Lorg/apache/lucene/util/CharsRef;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
