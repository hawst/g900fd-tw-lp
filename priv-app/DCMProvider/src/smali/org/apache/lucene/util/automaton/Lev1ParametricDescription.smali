.class Lorg/apache/lucene/util/automaton/Lev1ParametricDescription;
.super Lorg/apache/lucene/util/automaton/LevenshteinAutomata$ParametricDescription;
.source "Lev1ParametricDescription.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final offsetIncrs0:[J

.field private static final offsetIncrs1:[J

.field private static final offsetIncrs2:[J

.field private static final offsetIncrs3:[J

.field private static final toStates0:[J

.field private static final toStates1:[J

.field private static final toStates2:[J

.field private static final toStates3:[J


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 27
    const-class v0, Lorg/apache/lucene/util/automaton/Lev1ParametricDescription;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/automaton/Lev1ParametricDescription;->$assertionsDisabled:Z

    .line 75
    new-array v0, v1, [J

    .line 76
    const-wide/16 v4, 0x2

    aput-wide v4, v0, v2

    .line 75
    sput-object v0, Lorg/apache/lucene/util/automaton/Lev1ParametricDescription;->toStates0:[J

    .line 78
    new-array v0, v1, [J

    sput-object v0, Lorg/apache/lucene/util/automaton/Lev1ParametricDescription;->offsetIncrs0:[J

    .line 83
    new-array v0, v1, [J

    .line 84
    const-wide/16 v4, 0xa43

    aput-wide v4, v0, v2

    .line 83
    sput-object v0, Lorg/apache/lucene/util/automaton/Lev1ParametricDescription;->toStates1:[J

    .line 86
    new-array v0, v1, [J

    .line 87
    const-wide/16 v4, 0x38

    aput-wide v4, v0, v2

    .line 86
    sput-object v0, Lorg/apache/lucene/util/automaton/Lev1ParametricDescription;->offsetIncrs1:[J

    .line 91
    new-array v0, v1, [J

    .line 92
    const-wide v4, 0x69a292450428003L

    aput-wide v4, v0, v2

    .line 91
    sput-object v0, Lorg/apache/lucene/util/automaton/Lev1ParametricDescription;->toStates2:[J

    .line 94
    new-array v0, v1, [J

    .line 95
    const-wide v4, 0x5555588000L

    aput-wide v4, v0, v2

    .line 94
    sput-object v0, Lorg/apache/lucene/util/automaton/Lev1ParametricDescription;->offsetIncrs2:[J

    .line 99
    new-array v0, v3, [J

    fill-array-data v0, :array_0

    sput-object v0, Lorg/apache/lucene/util/automaton/Lev1ParametricDescription;->toStates3:[J

    .line 102
    new-array v0, v3, [J

    fill-array-data v0, :array_1

    sput-object v0, Lorg/apache/lucene/util/automaton/Lev1ParametricDescription;->offsetIncrs3:[J

    .line 104
    return-void

    :cond_0
    move v0, v2

    .line 27
    goto :goto_0

    .line 99
    nop

    :array_0
    .array-data 8
        0x1690a82152018003L    # 5.440181321888772E-200
        0xb1a2d346448a49L
    .end array-data

    .line 102
    :array_1
    .array-data 8
        0x555555b8220f0000L    # 1.1946149431197133E103
        0x5555
    .end array-data
.end method

.method public constructor <init>(I)V
    .locals 4
    .param p1, "w"    # I

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x1

    .line 115
    const/4 v0, 0x5

    new-array v0, v0, [I

    aput v2, v0, v2

    const/4 v1, 0x3

    aput v3, v0, v1

    const/4 v1, 0x4

    aput v3, v0, v1

    invoke-direct {p0, p1, v2, v0}, Lorg/apache/lucene/util/automaton/LevenshteinAutomata$ParametricDescription;-><init>(II[I)V

    .line 116
    return-void
.end method


# virtual methods
.method transition(III)I
    .locals 9
    .param p1, "absState"    # I
    .param p2, "position"    # I
    .param p3, "vector"    # I

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x1

    const/4 v6, 0x3

    const/4 v3, -0x1

    const/4 v5, 0x2

    .line 32
    sget-boolean v4, Lorg/apache/lucene/util/automaton/Lev1ParametricDescription;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    if-ne p1, v3, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 35
    :cond_0
    iget v4, p0, Lorg/apache/lucene/util/automaton/Lev1ParametricDescription;->w:I

    add-int/lit8 v4, v4, 0x1

    div-int v2, p1, v4

    .line 36
    .local v2, "state":I
    iget v4, p0, Lorg/apache/lucene/util/automaton/Lev1ParametricDescription;->w:I

    add-int/lit8 v4, v4, 0x1

    rem-int v1, p1, v4

    .line 37
    .local v1, "offset":I
    sget-boolean v4, Lorg/apache/lucene/util/automaton/Lev1ParametricDescription;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    if-gez v1, :cond_1

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 39
    :cond_1
    iget v4, p0, Lorg/apache/lucene/util/automaton/Lev1ParametricDescription;->w:I

    if-ne p2, v4, :cond_3

    .line 40
    if-ge v2, v5, :cond_2

    .line 41
    mul-int/lit8 v4, p3, 0x2

    add-int v0, v4, v2

    .line 42
    .local v0, "loc":I
    sget-object v4, Lorg/apache/lucene/util/automaton/Lev1ParametricDescription;->offsetIncrs0:[J

    invoke-virtual {p0, v4, v0, v7}, Lorg/apache/lucene/util/automaton/Lev1ParametricDescription;->unpack([JII)I

    move-result v4

    add-int/2addr v1, v4

    .line 43
    sget-object v4, Lorg/apache/lucene/util/automaton/Lev1ParametricDescription;->toStates0:[J

    invoke-virtual {p0, v4, v0, v5}, Lorg/apache/lucene/util/automaton/Lev1ParametricDescription;->unpack([JII)I

    move-result v4

    add-int/lit8 v2, v4, -0x1

    .line 65
    .end local v0    # "loc":I
    :cond_2
    :goto_0
    if-ne v2, v3, :cond_6

    .line 70
    :goto_1
    return v3

    .line 45
    :cond_3
    iget v4, p0, Lorg/apache/lucene/util/automaton/Lev1ParametricDescription;->w:I

    add-int/lit8 v4, v4, -0x1

    if-ne p2, v4, :cond_4

    .line 46
    if-ge v2, v6, :cond_2

    .line 47
    mul-int/lit8 v4, p3, 0x3

    add-int v0, v4, v2

    .line 48
    .restart local v0    # "loc":I
    sget-object v4, Lorg/apache/lucene/util/automaton/Lev1ParametricDescription;->offsetIncrs1:[J

    invoke-virtual {p0, v4, v0, v7}, Lorg/apache/lucene/util/automaton/Lev1ParametricDescription;->unpack([JII)I

    move-result v4

    add-int/2addr v1, v4

    .line 49
    sget-object v4, Lorg/apache/lucene/util/automaton/Lev1ParametricDescription;->toStates1:[J

    invoke-virtual {p0, v4, v0, v5}, Lorg/apache/lucene/util/automaton/Lev1ParametricDescription;->unpack([JII)I

    move-result v4

    add-int/lit8 v2, v4, -0x1

    .line 51
    goto :goto_0

    .end local v0    # "loc":I
    :cond_4
    iget v4, p0, Lorg/apache/lucene/util/automaton/Lev1ParametricDescription;->w:I

    add-int/lit8 v4, v4, -0x2

    if-ne p2, v4, :cond_5

    .line 52
    if-ge v2, v8, :cond_2

    .line 53
    mul-int/lit8 v4, p3, 0x5

    add-int v0, v4, v2

    .line 54
    .restart local v0    # "loc":I
    sget-object v4, Lorg/apache/lucene/util/automaton/Lev1ParametricDescription;->offsetIncrs2:[J

    invoke-virtual {p0, v4, v0, v5}, Lorg/apache/lucene/util/automaton/Lev1ParametricDescription;->unpack([JII)I

    move-result v4

    add-int/2addr v1, v4

    .line 55
    sget-object v4, Lorg/apache/lucene/util/automaton/Lev1ParametricDescription;->toStates2:[J

    invoke-virtual {p0, v4, v0, v6}, Lorg/apache/lucene/util/automaton/Lev1ParametricDescription;->unpack([JII)I

    move-result v4

    add-int/lit8 v2, v4, -0x1

    .line 57
    goto :goto_0

    .line 58
    .end local v0    # "loc":I
    :cond_5
    if-ge v2, v8, :cond_2

    .line 59
    mul-int/lit8 v4, p3, 0x5

    add-int v0, v4, v2

    .line 60
    .restart local v0    # "loc":I
    sget-object v4, Lorg/apache/lucene/util/automaton/Lev1ParametricDescription;->offsetIncrs3:[J

    invoke-virtual {p0, v4, v0, v5}, Lorg/apache/lucene/util/automaton/Lev1ParametricDescription;->unpack([JII)I

    move-result v4

    add-int/2addr v1, v4

    .line 61
    sget-object v4, Lorg/apache/lucene/util/automaton/Lev1ParametricDescription;->toStates3:[J

    invoke-virtual {p0, v4, v0, v6}, Lorg/apache/lucene/util/automaton/Lev1ParametricDescription;->unpack([JII)I

    move-result v4

    add-int/lit8 v2, v4, -0x1

    goto :goto_0

    .line 70
    .end local v0    # "loc":I
    :cond_6
    iget v3, p0, Lorg/apache/lucene/util/automaton/Lev1ParametricDescription;->w:I

    add-int/lit8 v3, v3, 0x1

    mul-int/2addr v3, v2

    add-int/2addr v3, v1

    goto :goto_1
.end method
