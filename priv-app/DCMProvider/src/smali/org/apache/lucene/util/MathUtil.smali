.class public final Lorg/apache/lucene/util/MathUtil;
.super Ljava/lang/Object;
.source "MathUtil.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    return-void
.end method

.method public static log(JI)I
    .locals 4
    .param p0, "x"    # J
    .param p2, "base"    # I

    .prologue
    .line 35
    const/4 v1, 0x1

    if-gt p2, v1, :cond_0

    .line 36
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "base must be > 1"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 38
    :cond_0
    const/4 v0, 0x0

    .line 39
    .local v0, "ret":I
    :goto_0
    int-to-long v2, p2

    cmp-long v1, p0, v2

    if-gez v1, :cond_1

    .line 43
    return v0

    .line 40
    :cond_1
    int-to-long v2, p2

    div-long/2addr p0, v2

    .line 41
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
