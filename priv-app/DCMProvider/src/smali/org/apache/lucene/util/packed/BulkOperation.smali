.class abstract Lorg/apache/lucene/util/packed/BulkOperation;
.super Ljava/lang/Object;
.source "BulkOperation.java"

# interfaces
.implements Lorg/apache/lucene/util/packed/PackedInts$Decoder;
.implements Lorg/apache/lucene/util/packed/PackedInts$Encoder;


# static fields
.field private static synthetic $SWITCH_TABLE$org$apache$lucene$util$packed$PackedInts$Format:[I

.field static final synthetic $assertionsDisabled:Z

.field private static final packedBulkOps:[Lorg/apache/lucene/util/packed/BulkOperation;

.field private static final packedSingleBlockBulkOps:[Lorg/apache/lucene/util/packed/BulkOperation;


# direct methods
.method static synthetic $SWITCH_TABLE$org$apache$lucene$util$packed$PackedInts$Format()[I
    .locals 3

    .prologue
    .line 26
    sget-object v0, Lorg/apache/lucene/util/packed/BulkOperation;->$SWITCH_TABLE$org$apache$lucene$util$packed$PackedInts$Format:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lorg/apache/lucene/util/packed/PackedInts$Format;->values()[Lorg/apache/lucene/util/packed/PackedInts$Format;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lorg/apache/lucene/util/packed/PackedInts$Format;->PACKED:Lorg/apache/lucene/util/packed/PackedInts$Format;

    invoke-virtual {v1}, Lorg/apache/lucene/util/packed/PackedInts$Format;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_1

    :goto_1
    :try_start_1
    sget-object v1, Lorg/apache/lucene/util/packed/PackedInts$Format;->PACKED_SINGLE_BLOCK:Lorg/apache/lucene/util/packed/PackedInts$Format;

    invoke-virtual {v1}, Lorg/apache/lucene/util/packed/PackedInts$Format;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_0

    :goto_2
    sput-object v0, Lorg/apache/lucene/util/packed/BulkOperation;->$SWITCH_TABLE$org$apache$lucene$util$packed$PackedInts$Format:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_2

    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v2, 0x0

    const/16 v6, 0x20

    const/4 v1, 0x1

    .line 26
    const-class v0, Lorg/apache/lucene/util/packed/BulkOperation;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/packed/BulkOperation;->$assertionsDisabled:Z

    .line 27
    const/16 v0, 0x40

    new-array v0, v0, [Lorg/apache/lucene/util/packed/BulkOperation;

    .line 28
    new-instance v3, Lorg/apache/lucene/util/packed/BulkOperationPacked1;

    invoke-direct {v3}, Lorg/apache/lucene/util/packed/BulkOperationPacked1;-><init>()V

    aput-object v3, v0, v2

    .line 29
    new-instance v3, Lorg/apache/lucene/util/packed/BulkOperationPacked2;

    invoke-direct {v3}, Lorg/apache/lucene/util/packed/BulkOperationPacked2;-><init>()V

    aput-object v3, v0, v1

    .line 30
    new-instance v3, Lorg/apache/lucene/util/packed/BulkOperationPacked3;

    invoke-direct {v3}, Lorg/apache/lucene/util/packed/BulkOperationPacked3;-><init>()V

    aput-object v3, v0, v7

    .line 31
    new-instance v3, Lorg/apache/lucene/util/packed/BulkOperationPacked4;

    invoke-direct {v3}, Lorg/apache/lucene/util/packed/BulkOperationPacked4;-><init>()V

    aput-object v3, v0, v8

    const/4 v3, 0x4

    .line 32
    new-instance v4, Lorg/apache/lucene/util/packed/BulkOperationPacked5;

    invoke-direct {v4}, Lorg/apache/lucene/util/packed/BulkOperationPacked5;-><init>()V

    aput-object v4, v0, v3

    const/4 v3, 0x5

    .line 33
    new-instance v4, Lorg/apache/lucene/util/packed/BulkOperationPacked6;

    invoke-direct {v4}, Lorg/apache/lucene/util/packed/BulkOperationPacked6;-><init>()V

    aput-object v4, v0, v3

    const/4 v3, 0x6

    .line 34
    new-instance v4, Lorg/apache/lucene/util/packed/BulkOperationPacked7;

    invoke-direct {v4}, Lorg/apache/lucene/util/packed/BulkOperationPacked7;-><init>()V

    aput-object v4, v0, v3

    const/4 v3, 0x7

    .line 35
    new-instance v4, Lorg/apache/lucene/util/packed/BulkOperationPacked8;

    invoke-direct {v4}, Lorg/apache/lucene/util/packed/BulkOperationPacked8;-><init>()V

    aput-object v4, v0, v3

    const/16 v3, 0x8

    .line 36
    new-instance v4, Lorg/apache/lucene/util/packed/BulkOperationPacked9;

    invoke-direct {v4}, Lorg/apache/lucene/util/packed/BulkOperationPacked9;-><init>()V

    aput-object v4, v0, v3

    const/16 v3, 0x9

    .line 37
    new-instance v4, Lorg/apache/lucene/util/packed/BulkOperationPacked10;

    invoke-direct {v4}, Lorg/apache/lucene/util/packed/BulkOperationPacked10;-><init>()V

    aput-object v4, v0, v3

    const/16 v3, 0xa

    .line 38
    new-instance v4, Lorg/apache/lucene/util/packed/BulkOperationPacked11;

    invoke-direct {v4}, Lorg/apache/lucene/util/packed/BulkOperationPacked11;-><init>()V

    aput-object v4, v0, v3

    const/16 v3, 0xb

    .line 39
    new-instance v4, Lorg/apache/lucene/util/packed/BulkOperationPacked12;

    invoke-direct {v4}, Lorg/apache/lucene/util/packed/BulkOperationPacked12;-><init>()V

    aput-object v4, v0, v3

    const/16 v3, 0xc

    .line 40
    new-instance v4, Lorg/apache/lucene/util/packed/BulkOperationPacked13;

    invoke-direct {v4}, Lorg/apache/lucene/util/packed/BulkOperationPacked13;-><init>()V

    aput-object v4, v0, v3

    const/16 v3, 0xd

    .line 41
    new-instance v4, Lorg/apache/lucene/util/packed/BulkOperationPacked14;

    invoke-direct {v4}, Lorg/apache/lucene/util/packed/BulkOperationPacked14;-><init>()V

    aput-object v4, v0, v3

    const/16 v3, 0xe

    .line 42
    new-instance v4, Lorg/apache/lucene/util/packed/BulkOperationPacked15;

    invoke-direct {v4}, Lorg/apache/lucene/util/packed/BulkOperationPacked15;-><init>()V

    aput-object v4, v0, v3

    const/16 v3, 0xf

    .line 43
    new-instance v4, Lorg/apache/lucene/util/packed/BulkOperationPacked16;

    invoke-direct {v4}, Lorg/apache/lucene/util/packed/BulkOperationPacked16;-><init>()V

    aput-object v4, v0, v3

    const/16 v3, 0x10

    .line 44
    new-instance v4, Lorg/apache/lucene/util/packed/BulkOperationPacked17;

    invoke-direct {v4}, Lorg/apache/lucene/util/packed/BulkOperationPacked17;-><init>()V

    aput-object v4, v0, v3

    const/16 v3, 0x11

    .line 45
    new-instance v4, Lorg/apache/lucene/util/packed/BulkOperationPacked18;

    invoke-direct {v4}, Lorg/apache/lucene/util/packed/BulkOperationPacked18;-><init>()V

    aput-object v4, v0, v3

    const/16 v3, 0x12

    .line 46
    new-instance v4, Lorg/apache/lucene/util/packed/BulkOperationPacked19;

    invoke-direct {v4}, Lorg/apache/lucene/util/packed/BulkOperationPacked19;-><init>()V

    aput-object v4, v0, v3

    const/16 v3, 0x13

    .line 47
    new-instance v4, Lorg/apache/lucene/util/packed/BulkOperationPacked20;

    invoke-direct {v4}, Lorg/apache/lucene/util/packed/BulkOperationPacked20;-><init>()V

    aput-object v4, v0, v3

    const/16 v3, 0x14

    .line 48
    new-instance v4, Lorg/apache/lucene/util/packed/BulkOperationPacked21;

    invoke-direct {v4}, Lorg/apache/lucene/util/packed/BulkOperationPacked21;-><init>()V

    aput-object v4, v0, v3

    const/16 v3, 0x15

    .line 49
    new-instance v4, Lorg/apache/lucene/util/packed/BulkOperationPacked22;

    invoke-direct {v4}, Lorg/apache/lucene/util/packed/BulkOperationPacked22;-><init>()V

    aput-object v4, v0, v3

    const/16 v3, 0x16

    .line 50
    new-instance v4, Lorg/apache/lucene/util/packed/BulkOperationPacked23;

    invoke-direct {v4}, Lorg/apache/lucene/util/packed/BulkOperationPacked23;-><init>()V

    aput-object v4, v0, v3

    const/16 v3, 0x17

    .line 51
    new-instance v4, Lorg/apache/lucene/util/packed/BulkOperationPacked24;

    invoke-direct {v4}, Lorg/apache/lucene/util/packed/BulkOperationPacked24;-><init>()V

    aput-object v4, v0, v3

    const/16 v3, 0x18

    .line 52
    new-instance v4, Lorg/apache/lucene/util/packed/BulkOperationPacked;

    const/16 v5, 0x19

    invoke-direct {v4, v5}, Lorg/apache/lucene/util/packed/BulkOperationPacked;-><init>(I)V

    aput-object v4, v0, v3

    const/16 v3, 0x19

    .line 53
    new-instance v4, Lorg/apache/lucene/util/packed/BulkOperationPacked;

    const/16 v5, 0x1a

    invoke-direct {v4, v5}, Lorg/apache/lucene/util/packed/BulkOperationPacked;-><init>(I)V

    aput-object v4, v0, v3

    const/16 v3, 0x1a

    .line 54
    new-instance v4, Lorg/apache/lucene/util/packed/BulkOperationPacked;

    const/16 v5, 0x1b

    invoke-direct {v4, v5}, Lorg/apache/lucene/util/packed/BulkOperationPacked;-><init>(I)V

    aput-object v4, v0, v3

    const/16 v3, 0x1b

    .line 55
    new-instance v4, Lorg/apache/lucene/util/packed/BulkOperationPacked;

    const/16 v5, 0x1c

    invoke-direct {v4, v5}, Lorg/apache/lucene/util/packed/BulkOperationPacked;-><init>(I)V

    aput-object v4, v0, v3

    const/16 v3, 0x1c

    .line 56
    new-instance v4, Lorg/apache/lucene/util/packed/BulkOperationPacked;

    const/16 v5, 0x1d

    invoke-direct {v4, v5}, Lorg/apache/lucene/util/packed/BulkOperationPacked;-><init>(I)V

    aput-object v4, v0, v3

    const/16 v3, 0x1d

    .line 57
    new-instance v4, Lorg/apache/lucene/util/packed/BulkOperationPacked;

    const/16 v5, 0x1e

    invoke-direct {v4, v5}, Lorg/apache/lucene/util/packed/BulkOperationPacked;-><init>(I)V

    aput-object v4, v0, v3

    const/16 v3, 0x1e

    .line 58
    new-instance v4, Lorg/apache/lucene/util/packed/BulkOperationPacked;

    const/16 v5, 0x1f

    invoke-direct {v4, v5}, Lorg/apache/lucene/util/packed/BulkOperationPacked;-><init>(I)V

    aput-object v4, v0, v3

    const/16 v3, 0x1f

    .line 59
    new-instance v4, Lorg/apache/lucene/util/packed/BulkOperationPacked;

    invoke-direct {v4, v6}, Lorg/apache/lucene/util/packed/BulkOperationPacked;-><init>(I)V

    aput-object v4, v0, v3

    .line 60
    new-instance v3, Lorg/apache/lucene/util/packed/BulkOperationPacked;

    const/16 v4, 0x21

    invoke-direct {v3, v4}, Lorg/apache/lucene/util/packed/BulkOperationPacked;-><init>(I)V

    aput-object v3, v0, v6

    const/16 v3, 0x21

    .line 61
    new-instance v4, Lorg/apache/lucene/util/packed/BulkOperationPacked;

    const/16 v5, 0x22

    invoke-direct {v4, v5}, Lorg/apache/lucene/util/packed/BulkOperationPacked;-><init>(I)V

    aput-object v4, v0, v3

    const/16 v3, 0x22

    .line 62
    new-instance v4, Lorg/apache/lucene/util/packed/BulkOperationPacked;

    const/16 v5, 0x23

    invoke-direct {v4, v5}, Lorg/apache/lucene/util/packed/BulkOperationPacked;-><init>(I)V

    aput-object v4, v0, v3

    const/16 v3, 0x23

    .line 63
    new-instance v4, Lorg/apache/lucene/util/packed/BulkOperationPacked;

    const/16 v5, 0x24

    invoke-direct {v4, v5}, Lorg/apache/lucene/util/packed/BulkOperationPacked;-><init>(I)V

    aput-object v4, v0, v3

    const/16 v3, 0x24

    .line 64
    new-instance v4, Lorg/apache/lucene/util/packed/BulkOperationPacked;

    const/16 v5, 0x25

    invoke-direct {v4, v5}, Lorg/apache/lucene/util/packed/BulkOperationPacked;-><init>(I)V

    aput-object v4, v0, v3

    const/16 v3, 0x25

    .line 65
    new-instance v4, Lorg/apache/lucene/util/packed/BulkOperationPacked;

    const/16 v5, 0x26

    invoke-direct {v4, v5}, Lorg/apache/lucene/util/packed/BulkOperationPacked;-><init>(I)V

    aput-object v4, v0, v3

    const/16 v3, 0x26

    .line 66
    new-instance v4, Lorg/apache/lucene/util/packed/BulkOperationPacked;

    const/16 v5, 0x27

    invoke-direct {v4, v5}, Lorg/apache/lucene/util/packed/BulkOperationPacked;-><init>(I)V

    aput-object v4, v0, v3

    const/16 v3, 0x27

    .line 67
    new-instance v4, Lorg/apache/lucene/util/packed/BulkOperationPacked;

    const/16 v5, 0x28

    invoke-direct {v4, v5}, Lorg/apache/lucene/util/packed/BulkOperationPacked;-><init>(I)V

    aput-object v4, v0, v3

    const/16 v3, 0x28

    .line 68
    new-instance v4, Lorg/apache/lucene/util/packed/BulkOperationPacked;

    const/16 v5, 0x29

    invoke-direct {v4, v5}, Lorg/apache/lucene/util/packed/BulkOperationPacked;-><init>(I)V

    aput-object v4, v0, v3

    const/16 v3, 0x29

    .line 69
    new-instance v4, Lorg/apache/lucene/util/packed/BulkOperationPacked;

    const/16 v5, 0x2a

    invoke-direct {v4, v5}, Lorg/apache/lucene/util/packed/BulkOperationPacked;-><init>(I)V

    aput-object v4, v0, v3

    const/16 v3, 0x2a

    .line 70
    new-instance v4, Lorg/apache/lucene/util/packed/BulkOperationPacked;

    const/16 v5, 0x2b

    invoke-direct {v4, v5}, Lorg/apache/lucene/util/packed/BulkOperationPacked;-><init>(I)V

    aput-object v4, v0, v3

    const/16 v3, 0x2b

    .line 71
    new-instance v4, Lorg/apache/lucene/util/packed/BulkOperationPacked;

    const/16 v5, 0x2c

    invoke-direct {v4, v5}, Lorg/apache/lucene/util/packed/BulkOperationPacked;-><init>(I)V

    aput-object v4, v0, v3

    const/16 v3, 0x2c

    .line 72
    new-instance v4, Lorg/apache/lucene/util/packed/BulkOperationPacked;

    const/16 v5, 0x2d

    invoke-direct {v4, v5}, Lorg/apache/lucene/util/packed/BulkOperationPacked;-><init>(I)V

    aput-object v4, v0, v3

    const/16 v3, 0x2d

    .line 73
    new-instance v4, Lorg/apache/lucene/util/packed/BulkOperationPacked;

    const/16 v5, 0x2e

    invoke-direct {v4, v5}, Lorg/apache/lucene/util/packed/BulkOperationPacked;-><init>(I)V

    aput-object v4, v0, v3

    const/16 v3, 0x2e

    .line 74
    new-instance v4, Lorg/apache/lucene/util/packed/BulkOperationPacked;

    const/16 v5, 0x2f

    invoke-direct {v4, v5}, Lorg/apache/lucene/util/packed/BulkOperationPacked;-><init>(I)V

    aput-object v4, v0, v3

    const/16 v3, 0x2f

    .line 75
    new-instance v4, Lorg/apache/lucene/util/packed/BulkOperationPacked;

    const/16 v5, 0x30

    invoke-direct {v4, v5}, Lorg/apache/lucene/util/packed/BulkOperationPacked;-><init>(I)V

    aput-object v4, v0, v3

    const/16 v3, 0x30

    .line 76
    new-instance v4, Lorg/apache/lucene/util/packed/BulkOperationPacked;

    const/16 v5, 0x31

    invoke-direct {v4, v5}, Lorg/apache/lucene/util/packed/BulkOperationPacked;-><init>(I)V

    aput-object v4, v0, v3

    const/16 v3, 0x31

    .line 77
    new-instance v4, Lorg/apache/lucene/util/packed/BulkOperationPacked;

    const/16 v5, 0x32

    invoke-direct {v4, v5}, Lorg/apache/lucene/util/packed/BulkOperationPacked;-><init>(I)V

    aput-object v4, v0, v3

    const/16 v3, 0x32

    .line 78
    new-instance v4, Lorg/apache/lucene/util/packed/BulkOperationPacked;

    const/16 v5, 0x33

    invoke-direct {v4, v5}, Lorg/apache/lucene/util/packed/BulkOperationPacked;-><init>(I)V

    aput-object v4, v0, v3

    const/16 v3, 0x33

    .line 79
    new-instance v4, Lorg/apache/lucene/util/packed/BulkOperationPacked;

    const/16 v5, 0x34

    invoke-direct {v4, v5}, Lorg/apache/lucene/util/packed/BulkOperationPacked;-><init>(I)V

    aput-object v4, v0, v3

    const/16 v3, 0x34

    .line 80
    new-instance v4, Lorg/apache/lucene/util/packed/BulkOperationPacked;

    const/16 v5, 0x35

    invoke-direct {v4, v5}, Lorg/apache/lucene/util/packed/BulkOperationPacked;-><init>(I)V

    aput-object v4, v0, v3

    const/16 v3, 0x35

    .line 81
    new-instance v4, Lorg/apache/lucene/util/packed/BulkOperationPacked;

    const/16 v5, 0x36

    invoke-direct {v4, v5}, Lorg/apache/lucene/util/packed/BulkOperationPacked;-><init>(I)V

    aput-object v4, v0, v3

    const/16 v3, 0x36

    .line 82
    new-instance v4, Lorg/apache/lucene/util/packed/BulkOperationPacked;

    const/16 v5, 0x37

    invoke-direct {v4, v5}, Lorg/apache/lucene/util/packed/BulkOperationPacked;-><init>(I)V

    aput-object v4, v0, v3

    const/16 v3, 0x37

    .line 83
    new-instance v4, Lorg/apache/lucene/util/packed/BulkOperationPacked;

    const/16 v5, 0x38

    invoke-direct {v4, v5}, Lorg/apache/lucene/util/packed/BulkOperationPacked;-><init>(I)V

    aput-object v4, v0, v3

    const/16 v3, 0x38

    .line 84
    new-instance v4, Lorg/apache/lucene/util/packed/BulkOperationPacked;

    const/16 v5, 0x39

    invoke-direct {v4, v5}, Lorg/apache/lucene/util/packed/BulkOperationPacked;-><init>(I)V

    aput-object v4, v0, v3

    const/16 v3, 0x39

    .line 85
    new-instance v4, Lorg/apache/lucene/util/packed/BulkOperationPacked;

    const/16 v5, 0x3a

    invoke-direct {v4, v5}, Lorg/apache/lucene/util/packed/BulkOperationPacked;-><init>(I)V

    aput-object v4, v0, v3

    const/16 v3, 0x3a

    .line 86
    new-instance v4, Lorg/apache/lucene/util/packed/BulkOperationPacked;

    const/16 v5, 0x3b

    invoke-direct {v4, v5}, Lorg/apache/lucene/util/packed/BulkOperationPacked;-><init>(I)V

    aput-object v4, v0, v3

    const/16 v3, 0x3b

    .line 87
    new-instance v4, Lorg/apache/lucene/util/packed/BulkOperationPacked;

    const/16 v5, 0x3c

    invoke-direct {v4, v5}, Lorg/apache/lucene/util/packed/BulkOperationPacked;-><init>(I)V

    aput-object v4, v0, v3

    const/16 v3, 0x3c

    .line 88
    new-instance v4, Lorg/apache/lucene/util/packed/BulkOperationPacked;

    const/16 v5, 0x3d

    invoke-direct {v4, v5}, Lorg/apache/lucene/util/packed/BulkOperationPacked;-><init>(I)V

    aput-object v4, v0, v3

    const/16 v3, 0x3d

    .line 89
    new-instance v4, Lorg/apache/lucene/util/packed/BulkOperationPacked;

    const/16 v5, 0x3e

    invoke-direct {v4, v5}, Lorg/apache/lucene/util/packed/BulkOperationPacked;-><init>(I)V

    aput-object v4, v0, v3

    const/16 v3, 0x3e

    .line 90
    new-instance v4, Lorg/apache/lucene/util/packed/BulkOperationPacked;

    const/16 v5, 0x3f

    invoke-direct {v4, v5}, Lorg/apache/lucene/util/packed/BulkOperationPacked;-><init>(I)V

    aput-object v4, v0, v3

    const/16 v3, 0x3f

    .line 91
    new-instance v4, Lorg/apache/lucene/util/packed/BulkOperationPacked;

    const/16 v5, 0x40

    invoke-direct {v4, v5}, Lorg/apache/lucene/util/packed/BulkOperationPacked;-><init>(I)V

    aput-object v4, v0, v3

    .line 27
    sput-object v0, Lorg/apache/lucene/util/packed/BulkOperation;->packedBulkOps:[Lorg/apache/lucene/util/packed/BulkOperation;

    .line 95
    new-array v0, v6, [Lorg/apache/lucene/util/packed/BulkOperation;

    .line 96
    new-instance v3, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;

    invoke-direct {v3, v1}, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;-><init>(I)V

    aput-object v3, v0, v2

    .line 97
    new-instance v2, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;

    invoke-direct {v2, v7}, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;-><init>(I)V

    aput-object v2, v0, v1

    .line 98
    new-instance v1, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;

    invoke-direct {v1, v8}, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;-><init>(I)V

    aput-object v1, v0, v7

    .line 99
    new-instance v1, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;

    const/4 v2, 0x4

    invoke-direct {v1, v2}, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;-><init>(I)V

    aput-object v1, v0, v8

    const/4 v1, 0x4

    .line 100
    new-instance v2, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;

    const/4 v3, 0x5

    invoke-direct {v2, v3}, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;-><init>(I)V

    aput-object v2, v0, v1

    const/4 v1, 0x5

    .line 101
    new-instance v2, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;

    const/4 v3, 0x6

    invoke-direct {v2, v3}, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;-><init>(I)V

    aput-object v2, v0, v1

    const/4 v1, 0x6

    .line 102
    new-instance v2, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;

    const/4 v3, 0x7

    invoke-direct {v2, v3}, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;-><init>(I)V

    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 103
    new-instance v2, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;

    const/16 v3, 0x8

    invoke-direct {v2, v3}, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;-><init>(I)V

    aput-object v2, v0, v1

    const/16 v1, 0x8

    .line 104
    new-instance v2, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;

    const/16 v3, 0x9

    invoke-direct {v2, v3}, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;-><init>(I)V

    aput-object v2, v0, v1

    const/16 v1, 0x9

    .line 105
    new-instance v2, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;

    const/16 v3, 0xa

    invoke-direct {v2, v3}, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;-><init>(I)V

    aput-object v2, v0, v1

    const/16 v1, 0xb

    .line 107
    new-instance v2, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;

    const/16 v3, 0xc

    invoke-direct {v2, v3}, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;-><init>(I)V

    aput-object v2, v0, v1

    const/16 v1, 0xf

    .line 111
    new-instance v2, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;

    const/16 v3, 0x10

    invoke-direct {v2, v3}, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;-><init>(I)V

    aput-object v2, v0, v1

    const/16 v1, 0x14

    .line 116
    new-instance v2, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;

    const/16 v3, 0x15

    invoke-direct {v2, v3}, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;-><init>(I)V

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    .line 127
    new-instance v2, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;

    invoke-direct {v2, v6}, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;-><init>(I)V

    aput-object v2, v0, v1

    .line 95
    sput-object v0, Lorg/apache/lucene/util/packed/BulkOperation;->packedSingleBlockBulkOps:[Lorg/apache/lucene/util/packed/BulkOperation;

    .line 128
    return-void

    :cond_0
    move v0, v2

    .line 26
    goto/16 :goto_0
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static of(Lorg/apache/lucene/util/packed/PackedInts$Format;I)Lorg/apache/lucene/util/packed/BulkOperation;
    .locals 2
    .param p0, "format"    # Lorg/apache/lucene/util/packed/PackedInts$Format;
    .param p1, "bitsPerValue"    # I

    .prologue
    .line 132
    invoke-static {}, Lorg/apache/lucene/util/packed/BulkOperation;->$SWITCH_TABLE$org$apache$lucene$util$packed$PackedInts$Format()[I

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/lucene/util/packed/PackedInts$Format;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 140
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 134
    :pswitch_0
    sget-boolean v0, Lorg/apache/lucene/util/packed/BulkOperation;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    sget-object v0, Lorg/apache/lucene/util/packed/BulkOperation;->packedBulkOps:[Lorg/apache/lucene/util/packed/BulkOperation;

    add-int/lit8 v1, p1, -0x1

    aget-object v0, v0, v1

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 135
    :cond_0
    sget-object v0, Lorg/apache/lucene/util/packed/BulkOperation;->packedBulkOps:[Lorg/apache/lucene/util/packed/BulkOperation;

    add-int/lit8 v1, p1, -0x1

    aget-object v0, v0, v1

    .line 138
    :goto_0
    return-object v0

    .line 137
    :pswitch_1
    sget-boolean v0, Lorg/apache/lucene/util/packed/BulkOperation;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    sget-object v0, Lorg/apache/lucene/util/packed/BulkOperation;->packedSingleBlockBulkOps:[Lorg/apache/lucene/util/packed/BulkOperation;

    add-int/lit8 v1, p1, -0x1

    aget-object v0, v0, v1

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 138
    :cond_1
    sget-object v0, Lorg/apache/lucene/util/packed/BulkOperation;->packedSingleBlockBulkOps:[Lorg/apache/lucene/util/packed/BulkOperation;

    add-int/lit8 v1, p1, -0x1

    aget-object v0, v0, v1

    goto :goto_0

    .line 132
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final computeIterations(II)I
    .locals 6
    .param p1, "valueCount"    # I
    .param p2, "ramBudget"    # I

    .prologue
    .line 170
    invoke-virtual {p0}, Lorg/apache/lucene/util/packed/BulkOperation;->byteBlockCount()I

    move-result v1

    invoke-virtual {p0}, Lorg/apache/lucene/util/packed/BulkOperation;->byteValueCount()I

    move-result v2

    mul-int/lit8 v2, v2, 0x8

    add-int/2addr v1, v2

    div-int v0, p2, v1

    .line 171
    .local v0, "iterations":I
    if-nez v0, :cond_1

    .line 173
    const/4 v0, 0x1

    .line 178
    .end local v0    # "iterations":I
    :cond_0
    :goto_0
    return v0

    .line 174
    .restart local v0    # "iterations":I
    :cond_1
    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p0}, Lorg/apache/lucene/util/packed/BulkOperation;->byteValueCount()I

    move-result v2

    mul-int/2addr v1, v2

    if-lt v1, p1, :cond_0

    .line 176
    int-to-double v2, p1

    invoke-virtual {p0}, Lorg/apache/lucene/util/packed/BulkOperation;->byteValueCount()I

    move-result v1

    int-to-double v4, v1

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v0, v2

    goto :goto_0
.end method

.method protected writeLong(J[BI)I
    .locals 5
    .param p1, "block"    # J
    .param p3, "blocks"    # [B
    .param p4, "blocksOffset"    # I

    .prologue
    .line 145
    const/4 v1, 0x1

    .local v1, "j":I
    move v0, p4

    .end local p4    # "blocksOffset":I
    .local v0, "blocksOffset":I
    :goto_0
    const/16 v2, 0x8

    if-le v1, v2, :cond_0

    .line 148
    return v0

    .line 146
    :cond_0
    add-int/lit8 p4, v0, 0x1

    .end local v0    # "blocksOffset":I
    .restart local p4    # "blocksOffset":I
    shl-int/lit8 v2, v1, 0x3

    rsub-int/lit8 v2, v2, 0x40

    ushr-long v2, p1, v2

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, p3, v0

    .line 145
    add-int/lit8 v1, v1, 0x1

    move v0, p4

    .end local p4    # "blocksOffset":I
    .restart local v0    # "blocksOffset":I
    goto :goto_0
.end method
