.class public abstract Lorg/apache/lucene/util/RollingBuffer;
.super Ljava/lang/Object;
.source "RollingBuffer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/RollingBuffer$Resettable;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lorg/apache/lucene/util/RollingBuffer$Resettable;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private buffer:[Lorg/apache/lucene/util/RollingBuffer$Resettable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[TT;"
        }
    .end annotation
.end field

.field private count:I

.field private nextPos:I

.field private nextWrite:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lorg/apache/lucene/util/RollingBuffer;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/RollingBuffer;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 48
    .local p0, "this":Lorg/apache/lucene/util/RollingBuffer;, "Lorg/apache/lucene/util/RollingBuffer<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    const/16 v1, 0x8

    new-array v1, v1, [Lorg/apache/lucene/util/RollingBuffer$Resettable;

    iput-object v1, p0, Lorg/apache/lucene/util/RollingBuffer;->buffer:[Lorg/apache/lucene/util/RollingBuffer$Resettable;

    .line 49
    const/4 v0, 0x0

    .local v0, "idx":I
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/util/RollingBuffer;->buffer:[Lorg/apache/lucene/util/RollingBuffer$Resettable;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 52
    return-void

    .line 50
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/util/RollingBuffer;->buffer:[Lorg/apache/lucene/util/RollingBuffer$Resettable;

    invoke-virtual {p0}, Lorg/apache/lucene/util/RollingBuffer;->newInstance()Lorg/apache/lucene/util/RollingBuffer$Resettable;

    move-result-object v2

    aput-object v2, v1, v0

    .line 49
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private getIndex(I)I
    .locals 3
    .param p1, "pos"    # I

    .prologue
    .line 76
    .local p0, "this":Lorg/apache/lucene/util/RollingBuffer;, "Lorg/apache/lucene/util/RollingBuffer<TT;>;"
    iget v1, p0, Lorg/apache/lucene/util/RollingBuffer;->nextWrite:I

    iget v2, p0, Lorg/apache/lucene/util/RollingBuffer;->nextPos:I

    sub-int/2addr v2, p1

    sub-int v0, v1, v2

    .line 77
    .local v0, "index":I
    if-gez v0, :cond_0

    .line 78
    iget-object v1, p0, Lorg/apache/lucene/util/RollingBuffer;->buffer:[Lorg/apache/lucene/util/RollingBuffer$Resettable;

    array-length v1, v1

    add-int/2addr v0, v1

    .line 80
    :cond_0
    return v0
.end method

.method private inBounds(I)Z
    .locals 2
    .param p1, "pos"    # I

    .prologue
    .line 72
    .local p0, "this":Lorg/apache/lucene/util/RollingBuffer;, "Lorg/apache/lucene/util/RollingBuffer<TT;>;"
    iget v0, p0, Lorg/apache/lucene/util/RollingBuffer;->nextPos:I

    if-ge p1, v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/util/RollingBuffer;->nextPos:I

    iget v1, p0, Lorg/apache/lucene/util/RollingBuffer;->count:I

    sub-int/2addr v0, v1

    if-lt p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public freeBefore(I)V
    .locals 6
    .param p1, "pos"    # I

    .prologue
    .line 122
    .local p0, "this":Lorg/apache/lucene/util/RollingBuffer;, "Lorg/apache/lucene/util/RollingBuffer<TT;>;"
    iget v3, p0, Lorg/apache/lucene/util/RollingBuffer;->count:I

    iget v4, p0, Lorg/apache/lucene/util/RollingBuffer;->nextPos:I

    sub-int/2addr v4, p1

    sub-int v2, v3, v4

    .line 123
    .local v2, "toFree":I
    sget-boolean v3, Lorg/apache/lucene/util/RollingBuffer;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    if-gez v2, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 124
    :cond_0
    sget-boolean v3, Lorg/apache/lucene/util/RollingBuffer;->$assertionsDisabled:Z

    if-nez v3, :cond_1

    iget v3, p0, Lorg/apache/lucene/util/RollingBuffer;->count:I

    if-le v2, v3, :cond_1

    new-instance v3, Ljava/lang/AssertionError;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "toFree="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " count="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lorg/apache/lucene/util/RollingBuffer;->count:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 125
    :cond_1
    iget v3, p0, Lorg/apache/lucene/util/RollingBuffer;->nextWrite:I

    iget v4, p0, Lorg/apache/lucene/util/RollingBuffer;->count:I

    sub-int v1, v3, v4

    .line 126
    .local v1, "index":I
    if-gez v1, :cond_2

    .line 127
    iget-object v3, p0, Lorg/apache/lucene/util/RollingBuffer;->buffer:[Lorg/apache/lucene/util/RollingBuffer$Resettable;

    array-length v3, v3

    add-int/2addr v1, v3

    .line 129
    :cond_2
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v2, :cond_3

    .line 137
    iget v3, p0, Lorg/apache/lucene/util/RollingBuffer;->count:I

    sub-int/2addr v3, v2

    iput v3, p0, Lorg/apache/lucene/util/RollingBuffer;->count:I

    .line 138
    return-void

    .line 130
    :cond_3
    iget-object v3, p0, Lorg/apache/lucene/util/RollingBuffer;->buffer:[Lorg/apache/lucene/util/RollingBuffer$Resettable;

    array-length v3, v3

    if-ne v1, v3, :cond_4

    .line 131
    const/4 v1, 0x0

    .line 134
    :cond_4
    iget-object v3, p0, Lorg/apache/lucene/util/RollingBuffer;->buffer:[Lorg/apache/lucene/util/RollingBuffer$Resettable;

    aget-object v3, v3, v1

    invoke-interface {v3}, Lorg/apache/lucene/util/RollingBuffer$Resettable;->reset()V

    .line 135
    add-int/lit8 v1, v1, 0x1

    .line 129
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public get(I)Lorg/apache/lucene/util/RollingBuffer$Resettable;
    .locals 8
    .param p1, "pos"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/util/RollingBuffer;, "Lorg/apache/lucene/util/RollingBuffer<TT;>;"
    const/4 v7, 0x0

    .line 88
    :goto_0
    iget v3, p0, Lorg/apache/lucene/util/RollingBuffer;->nextPos:I

    if-ge p1, v3, :cond_0

    .line 108
    sget-boolean v3, Lorg/apache/lucene/util/RollingBuffer;->$assertionsDisabled:Z

    if-nez v3, :cond_4

    invoke-direct {p0, p1}, Lorg/apache/lucene/util/RollingBuffer;->inBounds(I)Z

    move-result v3

    if-nez v3, :cond_4

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 89
    :cond_0
    iget v3, p0, Lorg/apache/lucene/util/RollingBuffer;->count:I

    iget-object v4, p0, Lorg/apache/lucene/util/RollingBuffer;->buffer:[Lorg/apache/lucene/util/RollingBuffer$Resettable;

    array-length v4, v4

    if-ne v3, v4, :cond_1

    .line 90
    iget v3, p0, Lorg/apache/lucene/util/RollingBuffer;->count:I

    add-int/lit8 v3, v3, 0x1

    sget v4, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_REF:I

    invoke-static {v3, v4}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v3

    new-array v2, v3, [Lorg/apache/lucene/util/RollingBuffer$Resettable;

    .line 92
    .local v2, "newBuffer":[Lorg/apache/lucene/util/RollingBuffer$Resettable;
    iget-object v3, p0, Lorg/apache/lucene/util/RollingBuffer;->buffer:[Lorg/apache/lucene/util/RollingBuffer$Resettable;

    iget v4, p0, Lorg/apache/lucene/util/RollingBuffer;->nextWrite:I

    iget-object v5, p0, Lorg/apache/lucene/util/RollingBuffer;->buffer:[Lorg/apache/lucene/util/RollingBuffer$Resettable;

    array-length v5, v5

    iget v6, p0, Lorg/apache/lucene/util/RollingBuffer;->nextWrite:I

    sub-int/2addr v5, v6

    invoke-static {v3, v4, v2, v7, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 93
    iget-object v3, p0, Lorg/apache/lucene/util/RollingBuffer;->buffer:[Lorg/apache/lucene/util/RollingBuffer$Resettable;

    iget-object v4, p0, Lorg/apache/lucene/util/RollingBuffer;->buffer:[Lorg/apache/lucene/util/RollingBuffer$Resettable;

    array-length v4, v4

    iget v5, p0, Lorg/apache/lucene/util/RollingBuffer;->nextWrite:I

    sub-int/2addr v4, v5

    iget v5, p0, Lorg/apache/lucene/util/RollingBuffer;->nextWrite:I

    invoke-static {v3, v7, v2, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 94
    iget-object v3, p0, Lorg/apache/lucene/util/RollingBuffer;->buffer:[Lorg/apache/lucene/util/RollingBuffer$Resettable;

    array-length v0, v3

    .local v0, "i":I
    :goto_1
    array-length v3, v2

    if-lt v0, v3, :cond_3

    .line 97
    iget-object v3, p0, Lorg/apache/lucene/util/RollingBuffer;->buffer:[Lorg/apache/lucene/util/RollingBuffer$Resettable;

    array-length v3, v3

    iput v3, p0, Lorg/apache/lucene/util/RollingBuffer;->nextWrite:I

    .line 98
    iput-object v2, p0, Lorg/apache/lucene/util/RollingBuffer;->buffer:[Lorg/apache/lucene/util/RollingBuffer$Resettable;

    .line 100
    .end local v0    # "i":I
    .end local v2    # "newBuffer":[Lorg/apache/lucene/util/RollingBuffer$Resettable;
    :cond_1
    iget v3, p0, Lorg/apache/lucene/util/RollingBuffer;->nextWrite:I

    iget-object v4, p0, Lorg/apache/lucene/util/RollingBuffer;->buffer:[Lorg/apache/lucene/util/RollingBuffer$Resettable;

    array-length v4, v4

    if-ne v3, v4, :cond_2

    .line 101
    iput v7, p0, Lorg/apache/lucene/util/RollingBuffer;->nextWrite:I

    .line 104
    :cond_2
    iget v3, p0, Lorg/apache/lucene/util/RollingBuffer;->nextWrite:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/lucene/util/RollingBuffer;->nextWrite:I

    .line 105
    iget v3, p0, Lorg/apache/lucene/util/RollingBuffer;->nextPos:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/lucene/util/RollingBuffer;->nextPos:I

    .line 106
    iget v3, p0, Lorg/apache/lucene/util/RollingBuffer;->count:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/lucene/util/RollingBuffer;->count:I

    goto :goto_0

    .line 95
    .restart local v0    # "i":I
    .restart local v2    # "newBuffer":[Lorg/apache/lucene/util/RollingBuffer$Resettable;
    :cond_3
    invoke-virtual {p0}, Lorg/apache/lucene/util/RollingBuffer;->newInstance()Lorg/apache/lucene/util/RollingBuffer$Resettable;

    move-result-object v3

    aput-object v3, v2, v0

    .line 94
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 109
    .end local v0    # "i":I
    .end local v2    # "newBuffer":[Lorg/apache/lucene/util/RollingBuffer$Resettable;
    :cond_4
    invoke-direct {p0, p1}, Lorg/apache/lucene/util/RollingBuffer;->getIndex(I)I

    move-result v1

    .line 112
    .local v1, "index":I
    iget-object v3, p0, Lorg/apache/lucene/util/RollingBuffer;->buffer:[Lorg/apache/lucene/util/RollingBuffer$Resettable;

    aget-object v3, v3, v1

    return-object v3
.end method

.method public getMaxPos()I
    .locals 1

    .prologue
    .line 118
    .local p0, "this":Lorg/apache/lucene/util/RollingBuffer;, "Lorg/apache/lucene/util/RollingBuffer<TT;>;"
    iget v0, p0, Lorg/apache/lucene/util/RollingBuffer;->nextPos:I

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method protected abstract newInstance()Lorg/apache/lucene/util/RollingBuffer$Resettable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public reset()V
    .locals 4

    .prologue
    .local p0, "this":Lorg/apache/lucene/util/RollingBuffer;, "Lorg/apache/lucene/util/RollingBuffer<TT;>;"
    const/4 v3, 0x0

    .line 57
    iget v0, p0, Lorg/apache/lucene/util/RollingBuffer;->nextWrite:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/util/RollingBuffer;->nextWrite:I

    .line 58
    :goto_0
    iget v0, p0, Lorg/apache/lucene/util/RollingBuffer;->count:I

    if-gtz v0, :cond_0

    .line 65
    iput v3, p0, Lorg/apache/lucene/util/RollingBuffer;->nextWrite:I

    .line 66
    iput v3, p0, Lorg/apache/lucene/util/RollingBuffer;->nextPos:I

    .line 67
    iput v3, p0, Lorg/apache/lucene/util/RollingBuffer;->count:I

    .line 68
    return-void

    .line 59
    :cond_0
    iget v0, p0, Lorg/apache/lucene/util/RollingBuffer;->nextWrite:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 60
    iget-object v0, p0, Lorg/apache/lucene/util/RollingBuffer;->buffer:[Lorg/apache/lucene/util/RollingBuffer$Resettable;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/util/RollingBuffer;->nextWrite:I

    .line 62
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/util/RollingBuffer;->buffer:[Lorg/apache/lucene/util/RollingBuffer$Resettable;

    iget v1, p0, Lorg/apache/lucene/util/RollingBuffer;->nextWrite:I

    add-int/lit8 v2, v1, -0x1

    iput v2, p0, Lorg/apache/lucene/util/RollingBuffer;->nextWrite:I

    aget-object v0, v0, v1

    invoke-interface {v0}, Lorg/apache/lucene/util/RollingBuffer$Resettable;->reset()V

    .line 63
    iget v0, p0, Lorg/apache/lucene/util/RollingBuffer;->count:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/util/RollingBuffer;->count:I

    goto :goto_0
.end method
