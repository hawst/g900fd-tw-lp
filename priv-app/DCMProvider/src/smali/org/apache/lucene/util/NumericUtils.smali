.class public final Lorg/apache/lucene/util/NumericUtils;
.super Ljava/lang/Object;
.source "NumericUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/NumericUtils$IntRangeBuilder;,
        Lorg/apache/lucene/util/NumericUtils$LongRangeBuilder;
    }
.end annotation


# static fields
.field public static final BUF_SIZE_INT:I = 0x6

.field public static final BUF_SIZE_LONG:I = 0xb

.field public static final PRECISION_STEP_DEFAULT:I = 0x4

.field public static final SHIFT_START_INT:B = 0x60t

.field public static final SHIFT_START_LONG:B = 0x20t


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static addRange(Ljava/lang/Object;IJJI)V
    .locals 8
    .param p0, "builder"    # Ljava/lang/Object;
    .param p1, "valSize"    # I
    .param p2, "minBound"    # J
    .param p4, "maxBound"    # J
    .param p6, "shift"    # I

    .prologue
    const-wide/16 v2, 0x1

    .line 388
    shl-long v0, v2, p6

    sub-long/2addr v0, v2

    or-long/2addr p4, v0

    .line 390
    sparse-switch p1, :sswitch_data_0

    .line 399
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "valSize must be 32 or 64."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_0
    move-object v1, p0

    .line 392
    check-cast v1, Lorg/apache/lucene/util/NumericUtils$LongRangeBuilder;

    move-wide v2, p2

    move-wide v4, p4

    move v6, p6

    invoke-virtual/range {v1 .. v6}, Lorg/apache/lucene/util/NumericUtils$LongRangeBuilder;->addRange(JJI)V

    .line 401
    .end local p0    # "builder":Ljava/lang/Object;
    :goto_0
    return-void

    .line 395
    .restart local p0    # "builder":Ljava/lang/Object;
    :sswitch_1
    check-cast p0, Lorg/apache/lucene/util/NumericUtils$IntRangeBuilder;

    .end local p0    # "builder":Ljava/lang/Object;
    long-to-int v0, p2

    long-to-int v1, p4

    invoke-virtual {p0, v0, v1, p6}, Lorg/apache/lucene/util/NumericUtils$IntRangeBuilder;->addRange(III)V

    goto :goto_0

    .line 390
    nop

    :sswitch_data_0
    .sparse-switch
        0x20 -> :sswitch_1
        0x40 -> :sswitch_0
    .end sparse-switch
.end method

.method public static doubleToSortableLong(D)J
    .locals 4
    .param p0, "val"    # D

    .prologue
    .line 272
    invoke-static {p0, p1}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v0

    .line 273
    .local v0, "f":J
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    const-wide v2, 0x7fffffffffffffffL

    xor-long/2addr v0, v2

    .line 274
    :cond_0
    return-wide v0
.end method

.method public static filterPrefixCodedInts(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;
    .locals 2
    .param p0, "termsEnum"    # Lorg/apache/lucene/index/TermsEnum;

    .prologue
    .line 489
    new-instance v0, Lorg/apache/lucene/util/NumericUtils$2;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/apache/lucene/util/NumericUtils$2;-><init>(Lorg/apache/lucene/index/TermsEnum;Z)V

    return-object v0
.end method

.method public static filterPrefixCodedLongs(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;
    .locals 2
    .param p0, "termsEnum"    # Lorg/apache/lucene/index/TermsEnum;

    .prologue
    .line 471
    new-instance v0, Lorg/apache/lucene/util/NumericUtils$1;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/apache/lucene/util/NumericUtils$1;-><init>(Lorg/apache/lucene/index/TermsEnum;Z)V

    return-object v0
.end method

.method public static floatToSortableInt(F)I
    .locals 2
    .param p0, "val"    # F

    .prologue
    .line 296
    invoke-static {p0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    .line 297
    .local v0, "f":I
    if-gez v0, :cond_0

    const v1, 0x7fffffff

    xor-int/2addr v0, v1

    .line 298
    :cond_0
    return v0
.end method

.method public static getPrefixCodedIntShift(Lorg/apache/lucene/util/BytesRef;)I
    .locals 3
    .param p0, "val"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 208
    iget-object v1, p0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v2, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    aget-byte v1, v1, v2

    add-int/lit8 v0, v1, -0x60

    .line 209
    .local v0, "shift":I
    const/16 v1, 0x1f

    if-gt v0, v1, :cond_0

    if-gez v0, :cond_1

    .line 210
    :cond_0
    new-instance v1, Ljava/lang/NumberFormatException;

    const-string v2, "Invalid shift value in prefixCoded bytes (is encoded value really an INT?)"

    invoke-direct {v1, v2}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 211
    :cond_1
    return v0
.end method

.method public static getPrefixCodedLongShift(Lorg/apache/lucene/util/BytesRef;)I
    .locals 4
    .param p0, "val"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 196
    iget-object v1, p0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v2, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    aget-byte v1, v1, v2

    add-int/lit8 v0, v1, -0x20

    .line 197
    .local v0, "shift":I
    const/16 v1, 0x3f

    if-gt v0, v1, :cond_0

    if-gez v0, :cond_1

    .line 198
    :cond_0
    new-instance v1, Ljava/lang/NumberFormatException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid shift value ("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") in prefixCoded bytes (is encoded value really an INT?)"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 199
    :cond_1
    return v0
.end method

.method public static intToPrefixCoded(IILorg/apache/lucene/util/BytesRef;)I
    .locals 1
    .param p0, "val"    # I
    .param p1, "shift"    # I
    .param p2, "bytes"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 128
    invoke-static {p0, p1, p2}, Lorg/apache/lucene/util/NumericUtils;->intToPrefixCodedBytes(IILorg/apache/lucene/util/BytesRef;)V

    .line 129
    invoke-virtual {p2}, Lorg/apache/lucene/util/BytesRef;->hashCode()I

    move-result v0

    return v0
.end method

.method public static intToPrefixCodedBytes(IILorg/apache/lucene/util/BytesRef;)V
    .locals 6
    .param p0, "val"    # I
    .param p1, "shift"    # I
    .param p2, "bytes"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    const/4 v5, 0x0

    .line 170
    and-int/lit8 v3, p1, -0x20

    if-eqz v3, :cond_0

    .line 171
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Illegal shift value, must be 0..31"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 172
    :cond_0
    rsub-int/lit8 v3, p1, 0x1f

    mul-int/lit8 v3, v3, 0x25

    shr-int/lit8 v3, v3, 0x8

    add-int/lit8 v0, v3, 0x1

    .line 173
    .local v0, "nChars":I
    iput v5, p2, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 174
    add-int/lit8 v3, v0, 0x1

    iput v3, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 175
    iget-object v3, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    array-length v3, v3

    iget v4, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    if-ge v3, v4, :cond_1

    .line 176
    const/16 v3, 0xb

    new-array v3, v3, [B

    iput-object v3, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 178
    :cond_1
    iget-object v3, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    add-int/lit8 v4, p1, 0x60

    int-to-byte v4, v4

    aput-byte v4, v3, v5

    .line 179
    const/high16 v3, -0x80000000

    xor-int v2, p0, v3

    .line 180
    .local v2, "sortableBits":I
    ushr-int/2addr v2, p1

    move v1, v0

    .line 181
    .end local v0    # "nChars":I
    .local v1, "nChars":I
    :goto_0
    if-gtz v1, :cond_2

    .line 187
    return-void

    .line 184
    :cond_2
    iget-object v3, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    add-int/lit8 v0, v1, -0x1

    .end local v1    # "nChars":I
    .restart local v0    # "nChars":I
    and-int/lit8 v4, v2, 0x7f

    int-to-byte v4, v4

    aput-byte v4, v3, v1

    .line 185
    ushr-int/lit8 v2, v2, 0x7

    move v1, v0

    .end local v0    # "nChars":I
    .restart local v1    # "nChars":I
    goto :goto_0
.end method

.method public static longToPrefixCoded(JILorg/apache/lucene/util/BytesRef;)I
    .locals 2
    .param p0, "val"    # J
    .param p2, "shift"    # I
    .param p3, "bytes"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 114
    invoke-static {p0, p1, p2, p3}, Lorg/apache/lucene/util/NumericUtils;->longToPrefixCodedBytes(JILorg/apache/lucene/util/BytesRef;)V

    .line 115
    invoke-virtual {p3}, Lorg/apache/lucene/util/BytesRef;->hashCode()I

    move-result v0

    return v0
.end method

.method public static longToPrefixCodedBytes(JILorg/apache/lucene/util/BytesRef;)V
    .locals 8
    .param p0, "val"    # J
    .param p2, "shift"    # I
    .param p3, "bytes"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    const/4 v6, 0x0

    .line 141
    and-int/lit8 v4, p2, -0x40

    if-eqz v4, :cond_0

    .line 142
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "Illegal shift value, must be 0..63"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 143
    :cond_0
    rsub-int/lit8 v4, p2, 0x3f

    mul-int/lit8 v4, v4, 0x25

    shr-int/lit8 v4, v4, 0x8

    add-int/lit8 v0, v4, 0x1

    .line 144
    .local v0, "nChars":I
    iput v6, p3, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 145
    add-int/lit8 v4, v0, 0x1

    iput v4, p3, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 146
    iget-object v4, p3, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    array-length v4, v4

    iget v5, p3, Lorg/apache/lucene/util/BytesRef;->length:I

    if-ge v4, v5, :cond_1

    .line 147
    const/16 v4, 0xb

    new-array v4, v4, [B

    iput-object v4, p3, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 149
    :cond_1
    iget-object v4, p3, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    add-int/lit8 v5, p2, 0x20

    int-to-byte v5, v5

    aput-byte v5, v4, v6

    .line 150
    const-wide/high16 v4, -0x8000000000000000L

    xor-long v2, p0, v4

    .line 151
    .local v2, "sortableBits":J
    ushr-long/2addr v2, p2

    move v1, v0

    .line 152
    .end local v0    # "nChars":I
    .local v1, "nChars":I
    :goto_0
    if-gtz v1, :cond_2

    .line 158
    return-void

    .line 155
    :cond_2
    iget-object v4, p3, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    add-int/lit8 v0, v1, -0x1

    .end local v1    # "nChars":I
    .restart local v0    # "nChars":I
    const-wide/16 v6, 0x7f

    and-long/2addr v6, v2

    long-to-int v5, v6

    int-to-byte v5, v5

    aput-byte v5, v4, v1

    .line 156
    const/4 v4, 0x7

    ushr-long/2addr v2, v4

    move v1, v0

    .end local v0    # "nChars":I
    .restart local v1    # "nChars":I
    goto :goto_0
.end method

.method public static prefixCodedToInt(Lorg/apache/lucene/util/BytesRef;)I
    .locals 7
    .param p0, "val"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 247
    const/4 v3, 0x0

    .line 248
    .local v3, "sortableBits":I
    iget v4, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    add-int/lit8 v1, v4, 0x1

    .local v1, "i":I
    iget v4, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget v5, p0, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int v2, v4, v5

    .local v2, "limit":I
    :goto_0
    if-lt v1, v2, :cond_0

    .line 259
    invoke-static {p0}, Lorg/apache/lucene/util/NumericUtils;->getPrefixCodedIntShift(Lorg/apache/lucene/util/BytesRef;)I

    move-result v4

    shl-int v4, v3, v4

    const/high16 v5, -0x80000000

    xor-int/2addr v4, v5

    return v4

    .line 249
    :cond_0
    shl-int/lit8 v3, v3, 0x7

    .line 250
    iget-object v4, p0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    aget-byte v0, v4, v1

    .line 251
    .local v0, "b":B
    if-gez v0, :cond_1

    .line 252
    new-instance v4, Ljava/lang/NumberFormatException;

    .line 253
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Invalid prefixCoded numerical value representation (byte "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 254
    and-int/lit16 v6, v0, 0xff

    invoke-static {v6}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " at position "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    sub-int v6, v1, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " is invalid)"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 253
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 252
    invoke-direct {v4, v5}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 257
    :cond_1
    or-int/2addr v3, v0

    .line 248
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static prefixCodedToLong(Lorg/apache/lucene/util/BytesRef;)J
    .locals 10
    .param p0, "val"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 223
    const-wide/16 v4, 0x0

    .line 224
    .local v4, "sortableBits":J
    iget v3, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    add-int/lit8 v1, v3, 0x1

    .local v1, "i":I
    iget v3, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget v6, p0, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int v2, v3, v6

    .local v2, "limit":I
    :goto_0
    if-lt v1, v2, :cond_0

    .line 235
    invoke-static {p0}, Lorg/apache/lucene/util/NumericUtils;->getPrefixCodedLongShift(Lorg/apache/lucene/util/BytesRef;)I

    move-result v3

    shl-long v6, v4, v3

    const-wide/high16 v8, -0x8000000000000000L

    xor-long/2addr v6, v8

    return-wide v6

    .line 225
    :cond_0
    const/4 v3, 0x7

    shl-long/2addr v4, v3

    .line 226
    iget-object v3, p0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    aget-byte v0, v3, v1

    .line 227
    .local v0, "b":B
    if-gez v0, :cond_1

    .line 228
    new-instance v3, Ljava/lang/NumberFormatException;

    .line 229
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Invalid prefixCoded numerical value representation (byte "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 230
    and-int/lit16 v7, v0, 0xff

    invoke-static {v7}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " at position "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    sub-int v7, v1, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " is invalid)"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 229
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 228
    invoke-direct {v3, v6}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 233
    :cond_1
    int-to-long v6, v0

    or-long/2addr v4, v6

    .line 224
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static sortableIntToFloat(I)F
    .locals 1
    .param p0, "val"    # I

    .prologue
    .line 306
    if-gez p0, :cond_0

    const v0, 0x7fffffff

    xor-int/2addr p0, v0

    .line 307
    :cond_0
    invoke-static {p0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    return v0
.end method

.method public static sortableLongToDouble(J)D
    .locals 2
    .param p0, "val"    # J

    .prologue
    .line 282
    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-gez v0, :cond_0

    const-wide v0, 0x7fffffffffffffffL

    xor-long/2addr p0, v0

    .line 283
    :cond_0
    invoke-static {p0, p1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    return-wide v0
.end method

.method public static splitIntRange(Lorg/apache/lucene/util/NumericUtils$IntRangeBuilder;III)V
    .locals 8
    .param p0, "builder"    # Lorg/apache/lucene/util/NumericUtils$IntRangeBuilder;
    .param p1, "precisionStep"    # I
    .param p2, "minBound"    # I
    .param p3, "maxBound"    # I

    .prologue
    .line 335
    const/16 v2, 0x20

    int-to-long v4, p2

    int-to-long v6, p3

    move-object v1, p0

    move v3, p1

    invoke-static/range {v1 .. v7}, Lorg/apache/lucene/util/NumericUtils;->splitRange(Ljava/lang/Object;IIJJ)V

    .line 336
    return-void
.end method

.method public static splitLongRange(Lorg/apache/lucene/util/NumericUtils$LongRangeBuilder;IJJ)V
    .locals 8
    .param p0, "builder"    # Lorg/apache/lucene/util/NumericUtils$LongRangeBuilder;
    .param p1, "precisionStep"    # I
    .param p2, "minBound"    # J
    .param p4, "maxBound"    # J

    .prologue
    .line 321
    const/16 v2, 0x40

    move-object v1, p0

    move v3, p1

    move-wide v4, p2

    move-wide v6, p4

    invoke-static/range {v1 .. v7}, Lorg/apache/lucene/util/NumericUtils;->splitRange(Ljava/lang/Object;IIJJ)V

    .line 322
    return-void
.end method

.method private static splitRange(Ljava/lang/Object;IIJJ)V
    .locals 21
    .param p0, "builder"    # Ljava/lang/Object;
    .param p1, "valSize"    # I
    .param p2, "precisionStep"    # I
    .param p3, "minBound"    # J
    .param p5, "maxBound"    # J

    .prologue
    .line 343
    const/4 v2, 0x1

    move/from16 v0, p2

    if-ge v0, v2, :cond_0

    .line 344
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "precisionStep must be >=1"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 345
    :cond_0
    cmp-long v2, p3, p5

    if-lez v2, :cond_1

    .line 376
    :goto_0
    return-void

    .line 346
    :cond_1
    const/4 v8, 0x0

    .line 348
    .local v8, "shift":I
    :goto_1
    const-wide/16 v2, 0x1

    add-int v4, v8, p2

    shl-long v10, v2, v4

    .line 349
    .local v10, "diff":J
    const-wide/16 v2, 0x1

    shl-long v2, v2, p2

    const-wide/16 v4, 0x1

    sub-long/2addr v2, v4

    shl-long v14, v2, v8

    .line 351
    .local v14, "mask":J
    and-long v2, p3, v14

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    const/4 v9, 0x1

    .line 352
    .local v9, "hasLower":Z
    :goto_2
    and-long v2, p5, v14

    cmp-long v2, v2, v14

    if-eqz v2, :cond_4

    const/4 v12, 0x1

    .line 354
    .local v12, "hasUpper":Z
    :goto_3
    if-eqz v9, :cond_5

    add-long v2, p3, v10

    :goto_4
    const-wide/16 v4, -0x1

    xor-long/2addr v4, v14

    and-long v18, v2, v4

    .line 355
    .local v18, "nextMinBound":J
    if-eqz v12, :cond_6

    sub-long v2, p5, v10

    :goto_5
    const-wide/16 v4, -0x1

    xor-long/2addr v4, v14

    and-long v16, v2, v4

    .line 357
    .local v16, "nextMaxBound":J
    cmp-long v2, v18, p3

    if-gez v2, :cond_7

    const/4 v13, 0x1

    .line 358
    .local v13, "lowerWrapped":Z
    :goto_6
    cmp-long v2, v16, p5

    if-lez v2, :cond_8

    const/16 v20, 0x1

    .line 360
    .local v20, "upperWrapped":Z
    :goto_7
    add-int v2, v8, p2

    move/from16 v0, p1

    if-ge v2, v0, :cond_2

    cmp-long v2, v18, v16

    if-gtz v2, :cond_2

    if-nez v13, :cond_2

    if-eqz v20, :cond_9

    :cond_2
    move-object/from16 v2, p0

    move/from16 v3, p1

    move-wide/from16 v4, p3

    move-wide/from16 v6, p5

    .line 362
    invoke-static/range {v2 .. v8}, Lorg/apache/lucene/util/NumericUtils;->addRange(Ljava/lang/Object;IJJI)V

    goto :goto_0

    .line 351
    .end local v9    # "hasLower":Z
    .end local v12    # "hasUpper":Z
    .end local v13    # "lowerWrapped":Z
    .end local v16    # "nextMaxBound":J
    .end local v18    # "nextMinBound":J
    .end local v20    # "upperWrapped":Z
    :cond_3
    const/4 v9, 0x0

    goto :goto_2

    .line 352
    .restart local v9    # "hasLower":Z
    :cond_4
    const/4 v12, 0x0

    goto :goto_3

    .restart local v12    # "hasUpper":Z
    :cond_5
    move-wide/from16 v2, p3

    .line 354
    goto :goto_4

    .restart local v18    # "nextMinBound":J
    :cond_6
    move-wide/from16 v2, p5

    .line 355
    goto :goto_5

    .line 357
    .restart local v16    # "nextMaxBound":J
    :cond_7
    const/4 v13, 0x0

    goto :goto_6

    .line 358
    .restart local v13    # "lowerWrapped":Z
    :cond_8
    const/16 v20, 0x0

    goto :goto_7

    .line 367
    .restart local v20    # "upperWrapped":Z
    :cond_9
    if-eqz v9, :cond_a

    .line 368
    or-long v6, p3, v14

    move-object/from16 v2, p0

    move/from16 v3, p1

    move-wide/from16 v4, p3

    invoke-static/range {v2 .. v8}, Lorg/apache/lucene/util/NumericUtils;->addRange(Ljava/lang/Object;IJJI)V

    .line 369
    :cond_a
    if-eqz v12, :cond_b

    .line 370
    const-wide/16 v2, -0x1

    xor-long/2addr v2, v14

    and-long v4, p5, v2

    move-object/from16 v2, p0

    move/from16 v3, p1

    move-wide/from16 v6, p5

    invoke-static/range {v2 .. v8}, Lorg/apache/lucene/util/NumericUtils;->addRange(Ljava/lang/Object;IJJI)V

    .line 373
    :cond_b
    move-wide/from16 p3, v18

    .line 374
    move-wide/from16 p5, v16

    .line 346
    add-int v8, v8, p2

    goto/16 :goto_1
.end method
