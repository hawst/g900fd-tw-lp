.class Lorg/apache/lucene/util/fst/FST$ArcAndState;
.super Ljava/lang/Object;
.source "FST.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/fst/FST;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ArcAndState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field final arc:Lorg/apache/lucene/util/fst/FST$Arc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;"
        }
    .end annotation
.end field

.field final chain:Lorg/apache/lucene/util/IntsRef;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/IntsRef;)V
    .locals 0
    .param p2, "chain"    # Lorg/apache/lucene/util/IntsRef;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;",
            "Lorg/apache/lucene/util/IntsRef;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1291
    .local p0, "this":Lorg/apache/lucene/util/fst/FST$ArcAndState;, "Lorg/apache/lucene/util/fst/FST<TT;>.ArcAndState<TT;>;"
    .local p1, "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1292
    iput-object p1, p0, Lorg/apache/lucene/util/fst/FST$ArcAndState;->arc:Lorg/apache/lucene/util/fst/FST$Arc;

    .line 1293
    iput-object p2, p0, Lorg/apache/lucene/util/fst/FST$ArcAndState;->chain:Lorg/apache/lucene/util/IntsRef;

    .line 1294
    return-void
.end method
