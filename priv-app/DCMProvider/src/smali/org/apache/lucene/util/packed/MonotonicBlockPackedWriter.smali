.class public final Lorg/apache/lucene/util/packed/MonotonicBlockPackedWriter;
.super Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;
.source "MonotonicBlockPackedWriter.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    const-class v0, Lorg/apache/lucene/util/packed/MonotonicBlockPackedWriter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/packed/MonotonicBlockPackedWriter;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/store/DataOutput;I)V
    .locals 0
    .param p1, "out"    # Lorg/apache/lucene/store/DataOutput;
    .param p2, "blockSize"    # I

    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;-><init>(Lorg/apache/lucene/store/DataOutput;I)V

    .line 62
    return-void
.end method


# virtual methods
.method public add(J)V
    .locals 3
    .param p1, "l"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 66
    sget-boolean v0, Lorg/apache/lucene/util/packed/MonotonicBlockPackedWriter;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 67
    :cond_0
    invoke-super {p0, p1, p2}, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->add(J)V

    .line 68
    return-void
.end method

.method public bridge synthetic finish()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-super {p0}, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->finish()V

    return-void
.end method

.method protected flush()V
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v12, 0x0

    .line 71
    sget-boolean v3, Lorg/apache/lucene/util/packed/MonotonicBlockPackedWriter;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    iget v3, p0, Lorg/apache/lucene/util/packed/MonotonicBlockPackedWriter;->off:I

    if-gtz v3, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 74
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/util/packed/MonotonicBlockPackedWriter;->values:[J

    aget-wide v6, v3, v12

    .line 75
    .local v6, "min":J
    iget v3, p0, Lorg/apache/lucene/util/packed/MonotonicBlockPackedWriter;->off:I

    const/4 v8, 0x1

    if-ne v3, v8, :cond_1

    const/4 v0, 0x0

    .line 77
    .local v0, "avg":F
    :goto_0
    const-wide/16 v4, 0x0

    .line 78
    .local v4, "maxZigZagDelta":J
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    iget v3, p0, Lorg/apache/lucene/util/packed/MonotonicBlockPackedWriter;->off:I

    if-lt v2, v3, :cond_2

    .line 83
    iget-object v3, p0, Lorg/apache/lucene/util/packed/MonotonicBlockPackedWriter;->out:Lorg/apache/lucene/store/DataOutput;

    invoke-virtual {v3, v6, v7}, Lorg/apache/lucene/store/DataOutput;->writeVLong(J)V

    .line 84
    iget-object v3, p0, Lorg/apache/lucene/util/packed/MonotonicBlockPackedWriter;->out:Lorg/apache/lucene/store/DataOutput;

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v8

    invoke-virtual {v3, v8}, Lorg/apache/lucene/store/DataOutput;->writeInt(I)V

    .line 85
    const-wide/16 v8, 0x0

    cmp-long v3, v4, v8

    if-nez v3, :cond_3

    .line 86
    iget-object v3, p0, Lorg/apache/lucene/util/packed/MonotonicBlockPackedWriter;->out:Lorg/apache/lucene/store/DataOutput;

    invoke-virtual {v3, v12}, Lorg/apache/lucene/store/DataOutput;->writeVInt(I)V

    .line 93
    :goto_2
    iput v12, p0, Lorg/apache/lucene/util/packed/MonotonicBlockPackedWriter;->off:I

    .line 94
    return-void

    .line 75
    .end local v0    # "avg":F
    .end local v2    # "i":I
    .end local v4    # "maxZigZagDelta":J
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/util/packed/MonotonicBlockPackedWriter;->values:[J

    iget v8, p0, Lorg/apache/lucene/util/packed/MonotonicBlockPackedWriter;->off:I

    add-int/lit8 v8, v8, -0x1

    aget-wide v8, v3, v8

    sub-long/2addr v8, v6

    long-to-float v3, v8

    iget v8, p0, Lorg/apache/lucene/util/packed/MonotonicBlockPackedWriter;->off:I

    add-int/lit8 v8, v8, -0x1

    int-to-float v8, v8

    div-float v0, v3, v8

    goto :goto_0

    .line 79
    .restart local v0    # "avg":F
    .restart local v2    # "i":I
    .restart local v4    # "maxZigZagDelta":J
    :cond_2
    iget-object v3, p0, Lorg/apache/lucene/util/packed/MonotonicBlockPackedWriter;->values:[J

    iget-object v8, p0, Lorg/apache/lucene/util/packed/MonotonicBlockPackedWriter;->values:[J

    aget-wide v8, v8, v2

    sub-long/2addr v8, v6

    int-to-float v10, v2

    mul-float/2addr v10, v0

    float-to-long v10, v10

    sub-long/2addr v8, v10

    invoke-static {v8, v9}, Lorg/apache/lucene/util/packed/MonotonicBlockPackedWriter;->zigZagEncode(J)J

    move-result-wide v8

    aput-wide v8, v3, v2

    .line 80
    iget-object v3, p0, Lorg/apache/lucene/util/packed/MonotonicBlockPackedWriter;->values:[J

    aget-wide v8, v3, v2

    invoke-static {v4, v5, v8, v9}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    .line 78
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 88
    :cond_3
    invoke-static {v4, v5}, Lorg/apache/lucene/util/packed/PackedInts;->bitsRequired(J)I

    move-result v1

    .line 89
    .local v1, "bitsRequired":I
    iget-object v3, p0, Lorg/apache/lucene/util/packed/MonotonicBlockPackedWriter;->out:Lorg/apache/lucene/store/DataOutput;

    invoke-virtual {v3, v1}, Lorg/apache/lucene/store/DataOutput;->writeVInt(I)V

    .line 90
    invoke-virtual {p0, v1}, Lorg/apache/lucene/util/packed/MonotonicBlockPackedWriter;->writeValues(I)V

    goto :goto_2
.end method

.method public bridge synthetic ord()J
    .locals 2

    .prologue
    .line 1
    invoke-super {p0}, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->ord()J

    move-result-wide v0

    return-wide v0
.end method

.method public bridge synthetic reset(Lorg/apache/lucene/store/DataOutput;)V
    .locals 0

    .prologue
    .line 1
    invoke-super {p0, p1}, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->reset(Lorg/apache/lucene/store/DataOutput;)V

    return-void
.end method
