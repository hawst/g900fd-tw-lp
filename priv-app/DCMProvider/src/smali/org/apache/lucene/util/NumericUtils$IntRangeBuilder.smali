.class public abstract Lorg/apache/lucene/util/NumericUtils$IntRangeBuilder;
.super Ljava/lang/Object;
.source "NumericUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/NumericUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "IntRangeBuilder"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 438
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public addRange(III)V
    .locals 3
    .param p1, "min"    # I
    .param p2, "max"    # I
    .param p3, "shift"    # I

    .prologue
    const/4 v2, 0x6

    .line 453
    new-instance v1, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v1, v2}, Lorg/apache/lucene/util/BytesRef;-><init>(I)V

    .local v1, "minBytes":Lorg/apache/lucene/util/BytesRef;
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0, v2}, Lorg/apache/lucene/util/BytesRef;-><init>(I)V

    .line 454
    .local v0, "maxBytes":Lorg/apache/lucene/util/BytesRef;
    invoke-static {p1, p3, v1}, Lorg/apache/lucene/util/NumericUtils;->intToPrefixCodedBytes(IILorg/apache/lucene/util/BytesRef;)V

    .line 455
    invoke-static {p2, p3, v0}, Lorg/apache/lucene/util/NumericUtils;->intToPrefixCodedBytes(IILorg/apache/lucene/util/BytesRef;)V

    .line 456
    invoke-virtual {p0, v1, v0}, Lorg/apache/lucene/util/NumericUtils$IntRangeBuilder;->addRange(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)V

    .line 457
    return-void
.end method

.method public addRange(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)V
    .locals 1
    .param p1, "minPrefixCoded"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "maxPrefixCoded"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 445
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
