.class public final Lorg/apache/lucene/util/fst/Util;
.super Ljava/lang/Object;
.source "Util.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/fst/Util$FSTPath;,
        Lorg/apache/lucene/util/fst/Util$MinResult;,
        Lorg/apache/lucene/util/fst/Util$TieBreakByInputComparator;,
        Lorg/apache/lucene/util/fst/Util$TopNSearcher;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lorg/apache/lucene/util/fst/Util;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/fst/Util;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    return-void
.end method

.method private static emitDotState(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0, "out"    # Ljava/io/Writer;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "shape"    # Ljava/lang/String;
    .param p3, "color"    # Ljava/lang/String;
    .param p4, "label"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 758
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "  "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 759
    const-string v1, " ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 760
    if-eqz p2, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "shape="

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 761
    if-eqz p3, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "color="

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 762
    if-eqz p4, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "label=\""

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\""

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 763
    const-string v1, "]\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 758
    invoke-virtual {p0, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 764
    return-void

    .line 760
    :cond_0
    const-string v0, ""

    goto :goto_0

    .line 761
    :cond_1
    const-string v0, ""

    goto :goto_1

    .line 762
    :cond_2
    const-string v0, "label=\"\""

    goto :goto_2
.end method

.method public static get(Lorg/apache/lucene/util/fst/FST;Lorg/apache/lucene/util/BytesRef;)Ljava/lang/Object;
    .locals 7
    .param p1, "input"    # Lorg/apache/lucene/util/BytesRef;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/lucene/util/fst/FST",
            "<TT;>;",
            "Lorg/apache/lucene/util/BytesRef;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p0, "fst":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    const/4 v4, 0x0

    .line 65
    sget-boolean v5, Lorg/apache/lucene/util/fst/Util;->$assertionsDisabled:Z

    if-nez v5, :cond_0

    iget-object v5, p0, Lorg/apache/lucene/util/fst/FST;->inputType:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    sget-object v6, Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;->BYTE1:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    if-eq v5, v6, :cond_0

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 67
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/util/fst/FST;->getBytesReader()Lorg/apache/lucene/util/fst/FST$BytesReader;

    move-result-object v1

    .line 70
    .local v1, "fstReader":Lorg/apache/lucene/util/fst/FST$BytesReader;, "Lorg/apache/lucene/util/fst/FST$BytesReader;"
    new-instance v5, Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-direct {v5}, Lorg/apache/lucene/util/fst/FST$Arc;-><init>()V

    invoke-virtual {p0, v5}, Lorg/apache/lucene/util/fst/FST;->getFirstArc(Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v0

    .line 73
    .local v0, "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    iget-object v5, p0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    invoke-virtual {v5}, Lorg/apache/lucene/util/fst/Outputs;->getNoOutput()Ljava/lang/Object;

    move-result-object v3

    .line 74
    .local v3, "output":Ljava/lang/Object;, "TT;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget v5, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    if-lt v2, v5, :cond_2

    .line 81
    invoke-virtual {v0}, Lorg/apache/lucene/util/fst/FST$Arc;->isFinal()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 82
    iget-object v4, p0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    iget-object v5, v0, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    invoke-virtual {v4, v3, v5}, Lorg/apache/lucene/util/fst/Outputs;->add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .line 84
    :cond_1
    return-object v4

    .line 75
    :cond_2
    iget-object v5, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v6, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    add-int/2addr v6, v2

    aget-byte v5, v5, v6

    and-int/lit16 v5, v5, 0xff

    invoke-virtual {p0, v5, v0, v0, v1}, Lorg/apache/lucene/util/fst/FST;->findTargetArc(ILorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 78
    iget-object v5, p0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    iget-object v6, v0, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    invoke-virtual {v5, v3, v6}, Lorg/apache/lucene/util/fst/Outputs;->add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 74
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static get(Lorg/apache/lucene/util/fst/FST;Lorg/apache/lucene/util/IntsRef;)Ljava/lang/Object;
    .locals 7
    .param p1, "input"    # Lorg/apache/lucene/util/IntsRef;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/lucene/util/fst/FST",
            "<TT;>;",
            "Lorg/apache/lucene/util/IntsRef;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p0, "fst":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    const/4 v4, 0x0

    .line 40
    new-instance v5, Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-direct {v5}, Lorg/apache/lucene/util/fst/FST$Arc;-><init>()V

    invoke-virtual {p0, v5}, Lorg/apache/lucene/util/fst/FST;->getFirstArc(Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v0

    .line 42
    .local v0, "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/util/fst/FST;->getBytesReader()Lorg/apache/lucene/util/fst/FST$BytesReader;

    move-result-object v1

    .line 45
    .local v1, "fstReader":Lorg/apache/lucene/util/fst/FST$BytesReader;, "Lorg/apache/lucene/util/fst/FST$BytesReader;"
    iget-object v5, p0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    invoke-virtual {v5}, Lorg/apache/lucene/util/fst/Outputs;->getNoOutput()Ljava/lang/Object;

    move-result-object v3

    .line 46
    .local v3, "output":Ljava/lang/Object;, "TT;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget v5, p1, Lorg/apache/lucene/util/IntsRef;->length:I

    if-lt v2, v5, :cond_1

    .line 53
    invoke-virtual {v0}, Lorg/apache/lucene/util/fst/FST$Arc;->isFinal()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 54
    iget-object v4, p0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    iget-object v5, v0, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    invoke-virtual {v4, v3, v5}, Lorg/apache/lucene/util/fst/Outputs;->add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .line 56
    :cond_0
    return-object v4

    .line 47
    :cond_1
    iget-object v5, p1, Lorg/apache/lucene/util/IntsRef;->ints:[I

    iget v6, p1, Lorg/apache/lucene/util/IntsRef;->offset:I

    add-int/2addr v6, v2

    aget v5, v5, v6

    invoke-virtual {p0, v5, v0, v0, v1}, Lorg/apache/lucene/util/fst/FST;->findTargetArc(ILorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 50
    iget-object v5, p0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    iget-object v6, v0, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    invoke-virtual {v5, v3, v6}, Lorg/apache/lucene/util/fst/Outputs;->add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 46
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static getByOutput(Lorg/apache/lucene/util/fst/FST;J)Lorg/apache/lucene/util/IntsRef;
    .locals 9
    .param p1, "targetOutput"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/FST",
            "<",
            "Ljava/lang/Long;",
            ">;J)",
            "Lorg/apache/lucene/util/IntsRef;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 104
    .local p0, "fst":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<Ljava/lang/Long;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/util/fst/FST;->getBytesReader()Lorg/apache/lucene/util/fst/FST$BytesReader;

    move-result-object v4

    .line 107
    .local v4, "in":Lorg/apache/lucene/util/fst/FST$BytesReader;, "Lorg/apache/lucene/util/fst/FST$BytesReader;"
    new-instance v0, Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-direct {v0}, Lorg/apache/lucene/util/fst/FST$Arc;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/lucene/util/fst/FST;->getFirstArc(Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v5

    .line 109
    .local v5, "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<Ljava/lang/Long;>;"
    new-instance v6, Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-direct {v6}, Lorg/apache/lucene/util/fst/FST$Arc;-><init>()V

    .line 111
    .local v6, "scratchArc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<Ljava/lang/Long;>;"
    new-instance v7, Lorg/apache/lucene/util/IntsRef;

    invoke-direct {v7}, Lorg/apache/lucene/util/IntsRef;-><init>()V

    .local v7, "result":Lorg/apache/lucene/util/IntsRef;
    move-object v1, p0

    move-wide v2, p1

    .line 113
    invoke-static/range {v1 .. v7}, Lorg/apache/lucene/util/fst/Util;->getByOutput(Lorg/apache/lucene/util/fst/FST;JLorg/apache/lucene/util/fst/FST$BytesReader;Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/IntsRef;)Lorg/apache/lucene/util/IntsRef;

    move-result-object v0

    return-object v0
.end method

.method public static getByOutput(Lorg/apache/lucene/util/fst/FST;JLorg/apache/lucene/util/fst/FST$BytesReader;Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/IntsRef;)Lorg/apache/lucene/util/IntsRef;
    .locals 25
    .param p1, "targetOutput"    # J
    .param p6, "result"    # Lorg/apache/lucene/util/IntsRef;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/FST",
            "<",
            "Ljava/lang/Long;",
            ">;J",
            "Lorg/apache/lucene/util/fst/FST$BytesReader;",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lorg/apache/lucene/util/IntsRef;",
            ")",
            "Lorg/apache/lucene/util/IntsRef;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 121
    .local p0, "fst":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<Ljava/lang/Long;>;"
    .local p3, "in":Lorg/apache/lucene/util/fst/FST$BytesReader;, "Lorg/apache/lucene/util/fst/FST$BytesReader;"
    .local p4, "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<Ljava/lang/Long;>;"
    .local p5, "scratchArc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<Ljava/lang/Long;>;"
    move-object/from16 v0, p4

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    move-object/from16 v22, v0

    check-cast v22, Ljava/lang/Long;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Long;->longValue()J

    move-result-wide v18

    .line 122
    .local v18, "output":J
    const/16 v20, 0x0

    .line 128
    .local v20, "upto":I
    :goto_0
    invoke-virtual/range {p4 .. p4}, Lorg/apache/lucene/util/fst/FST$Arc;->isFinal()Z

    move-result v22

    if-eqz v22, :cond_1

    .line 129
    move-object/from16 v0, p4

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    move-object/from16 v22, v0

    check-cast v22, Ljava/lang/Long;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Long;->longValue()J

    move-result-wide v22

    add-long v10, v18, v22

    .line 131
    .local v10, "finalOutput":J
    cmp-long v22, v10, p1

    if-nez v22, :cond_0

    .line 132
    move/from16 v0, v20

    move-object/from16 v1, p6

    iput v0, v1, Lorg/apache/lucene/util/IntsRef;->length:I

    .line 238
    .end local v10    # "finalOutput":J
    .end local p6    # "result":Lorg/apache/lucene/util/IntsRef;
    :goto_1
    return-object p6

    .line 135
    .restart local v10    # "finalOutput":J
    .restart local p6    # "result":Lorg/apache/lucene/util/IntsRef;
    :cond_0
    cmp-long v22, v10, p1

    if-lez v22, :cond_1

    .line 137
    const/16 p6, 0x0

    goto :goto_1

    .line 141
    .end local v10    # "finalOutput":J
    :cond_1
    invoke-static/range {p4 .. p4}, Lorg/apache/lucene/util/fst/FST;->targetHasArcs(Lorg/apache/lucene/util/fst/FST$Arc;)Z

    move-result v22

    if-eqz v22, :cond_e

    .line 143
    move-object/from16 v0, p6

    iget-object v0, v0, Lorg/apache/lucene/util/IntsRef;->ints:[I

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    array-length v0, v0

    move/from16 v22, v0

    move/from16 v0, v22

    move/from16 v1, v20

    if-ne v0, v1, :cond_2

    .line 144
    add-int/lit8 v22, v20, 0x1

    move-object/from16 v0, p6

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/IntsRef;->grow(I)V

    .line 147
    :cond_2
    move-object/from16 v0, p4

    iget-wide v0, v0, Lorg/apache/lucene/util/fst/FST$Arc;->target:J

    move-wide/from16 v22, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v22

    move-object/from16 v3, p4

    move-object/from16 v4, p3

    invoke-virtual {v0, v1, v2, v3, v4}, Lorg/apache/lucene/util/fst/FST;->readFirstRealTargetArc(JLorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 149
    move-object/from16 v0, p4

    iget v0, v0, Lorg/apache/lucene/util/fst/FST$Arc;->bytesPerArc:I

    move/from16 v22, v0

    if-eqz v22, :cond_9

    .line 151
    const/4 v13, 0x0

    .line 152
    .local v13, "low":I
    move-object/from16 v0, p4

    iget v0, v0, Lorg/apache/lucene/util/fst/FST$Arc;->numArcs:I

    move/from16 v22, v0

    add-int/lit8 v12, v22, -0x1

    .line 153
    .local v12, "high":I
    const/4 v14, 0x0

    .line 155
    .local v14, "mid":I
    const/4 v8, 0x0

    .line 156
    .local v8, "exact":Z
    :goto_2
    if-le v13, v12, :cond_3

    .line 180
    :goto_3
    const/16 v22, -0x1

    move/from16 v0, v22

    if-ne v12, v0, :cond_7

    .line 181
    const/16 p6, 0x0

    goto :goto_1

    .line 157
    :cond_3
    add-int v22, v13, v12

    ushr-int/lit8 v14, v22, 0x1

    .line 158
    move-object/from16 v0, p4

    iget-wide v0, v0, Lorg/apache/lucene/util/fst/FST$Arc;->posArcsStart:J

    move-wide/from16 v22, v0

    move-object/from16 v0, p3

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/fst/FST$BytesReader;->setPosition(J)V

    .line 159
    move-object/from16 v0, p4

    iget v0, v0, Lorg/apache/lucene/util/fst/FST$Arc;->bytesPerArc:I

    move/from16 v22, v0

    mul-int v22, v22, v14

    move-object/from16 v0, p3

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/fst/FST$BytesReader;->skipBytes(I)V

    .line 160
    invoke-virtual/range {p3 .. p3}, Lorg/apache/lucene/util/fst/FST$BytesReader;->readByte()B

    move-result v9

    .line 161
    .local v9, "flags":B
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/fst/FST;->readLabel(Lorg/apache/lucene/store/DataInput;)I

    .line 163
    and-int/lit8 v22, v9, 0x10

    if-eqz v22, :cond_4

    .line 164
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/fst/Outputs;->read(Lorg/apache/lucene/store/DataInput;)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Ljava/lang/Long;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 165
    .local v6, "arcOutput":J
    add-long v16, v18, v6

    .line 170
    .end local v6    # "arcOutput":J
    .local v16, "minArcOutput":J
    :goto_4
    cmp-long v22, v16, p1

    if-nez v22, :cond_5

    .line 171
    const/4 v8, 0x1

    .line 172
    goto :goto_3

    .line 167
    .end local v16    # "minArcOutput":J
    :cond_4
    move-wide/from16 v16, v18

    .restart local v16    # "minArcOutput":J
    goto :goto_4

    .line 173
    :cond_5
    cmp-long v22, v16, p1

    if-gez v22, :cond_6

    .line 174
    add-int/lit8 v13, v14, 0x1

    .line 175
    goto :goto_2

    .line 176
    :cond_6
    add-int/lit8 v12, v14, -0x1

    goto :goto_2

    .line 182
    .end local v9    # "flags":B
    .end local v16    # "minArcOutput":J
    :cond_7
    if-eqz v8, :cond_8

    .line 183
    add-int/lit8 v22, v14, -0x1

    move/from16 v0, v22

    move-object/from16 v1, p4

    iput v0, v1, Lorg/apache/lucene/util/fst/FST$Arc;->arcIdx:I

    .line 188
    :goto_5
    move-object/from16 v0, p0

    move-object/from16 v1, p4

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/fst/FST;->readNextRealArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 189
    move-object/from16 v0, p6

    iget-object v0, v0, Lorg/apache/lucene/util/IntsRef;->ints:[I

    move-object/from16 v22, v0

    add-int/lit8 v21, v20, 0x1

    .end local v20    # "upto":I
    .local v21, "upto":I
    move-object/from16 v0, p4

    iget v0, v0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    move/from16 v23, v0

    aput v23, v22, v20

    .line 190
    move-object/from16 v0, p4

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    move-object/from16 v22, v0

    check-cast v22, Ljava/lang/Long;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Long;->longValue()J

    move-result-wide v22

    add-long v18, v18, v22

    move/from16 v20, v21

    .line 192
    .end local v21    # "upto":I
    .restart local v20    # "upto":I
    goto/16 :goto_0

    .line 185
    :cond_8
    add-int/lit8 v22, v13, -0x2

    move/from16 v0, v22

    move-object/from16 v1, p4

    iput v0, v1, Lorg/apache/lucene/util/fst/FST$Arc;->arcIdx:I

    goto :goto_5

    .line 194
    .end local v8    # "exact":Z
    .end local v12    # "high":I
    .end local v13    # "low":I
    .end local v14    # "mid":I
    :cond_9
    const/4 v15, 0x0

    .line 201
    .local v15, "prevArc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<Ljava/lang/Long;>;"
    :goto_6
    move-object/from16 v0, p4

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    move-object/from16 v22, v0

    check-cast v22, Ljava/lang/Long;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Long;->longValue()J

    move-result-wide v22

    add-long v16, v18, v22

    .line 203
    .restart local v16    # "minArcOutput":J
    cmp-long v22, v16, p1

    if-nez v22, :cond_a

    .line 206
    move-wide/from16 v18, v16

    .line 207
    move-object/from16 v0, p6

    iget-object v0, v0, Lorg/apache/lucene/util/IntsRef;->ints:[I

    move-object/from16 v22, v0

    add-int/lit8 v21, v20, 0x1

    .end local v20    # "upto":I
    .restart local v21    # "upto":I
    move-object/from16 v0, p4

    iget v0, v0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    move/from16 v23, v0

    aput v23, v22, v20

    move/from16 v20, v21

    .line 208
    .end local v21    # "upto":I
    .restart local v20    # "upto":I
    goto/16 :goto_0

    .line 209
    :cond_a
    cmp-long v22, v16, p1

    if-lez v22, :cond_c

    .line 210
    if-nez v15, :cond_b

    .line 212
    const/16 p6, 0x0

    goto/16 :goto_1

    .line 215
    :cond_b
    move-object/from16 v0, p4

    invoke-virtual {v0, v15}, Lorg/apache/lucene/util/fst/FST$Arc;->copyFrom(Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 216
    move-object/from16 v0, p6

    iget-object v0, v0, Lorg/apache/lucene/util/IntsRef;->ints:[I

    move-object/from16 v22, v0

    add-int/lit8 v21, v20, 0x1

    .end local v20    # "upto":I
    .restart local v21    # "upto":I
    move-object/from16 v0, p4

    iget v0, v0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    move/from16 v23, v0

    aput v23, v22, v20

    .line 217
    move-object/from16 v0, p4

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    move-object/from16 v22, v0

    check-cast v22, Ljava/lang/Long;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Long;->longValue()J

    move-result-wide v22

    add-long v18, v18, v22

    move/from16 v20, v21

    .line 219
    .end local v21    # "upto":I
    .restart local v20    # "upto":I
    goto/16 :goto_0

    .line 221
    :cond_c
    invoke-virtual/range {p4 .. p4}, Lorg/apache/lucene/util/fst/FST$Arc;->isLast()Z

    move-result v22

    if-eqz v22, :cond_d

    .line 223
    move-wide/from16 v18, v16

    .line 225
    move-object/from16 v0, p6

    iget-object v0, v0, Lorg/apache/lucene/util/IntsRef;->ints:[I

    move-object/from16 v22, v0

    add-int/lit8 v21, v20, 0x1

    .end local v20    # "upto":I
    .restart local v21    # "upto":I
    move-object/from16 v0, p4

    iget v0, v0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    move/from16 v23, v0

    aput v23, v22, v20

    move/from16 v20, v21

    .line 226
    .end local v21    # "upto":I
    .restart local v20    # "upto":I
    goto/16 :goto_0

    .line 229
    :cond_d
    move-object/from16 v15, p5

    .line 230
    move-object/from16 v0, p4

    invoke-virtual {v15, v0}, Lorg/apache/lucene/util/fst/FST$Arc;->copyFrom(Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 232
    move-object/from16 v0, p0

    move-object/from16 v1, p4

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/fst/FST;->readNextRealArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    goto/16 :goto_6

    .line 238
    .end local v15    # "prevArc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<Ljava/lang/Long;>;"
    .end local v16    # "minArcOutput":J
    :cond_e
    const/16 p6, 0x0

    goto/16 :goto_1
.end method

.method private static printableLabel(I)Ljava/lang/String;
    .locals 2
    .param p0, "label"    # I

    .prologue
    .line 770
    const/16 v0, 0x20

    if-lt p0, v0, :cond_0

    const/16 v0, 0x7d

    if-gt p0, v0, :cond_0

    .line 771
    int-to-char v0, p0

    invoke-static {v0}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v0

    .line 773
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "0x"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static readCeilArc(ILorg/apache/lucene/util/fst/FST;Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;
    .locals 11
    .param p0, "label"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(I",
            "Lorg/apache/lucene/util/fst/FST",
            "<TT;>;",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;",
            "Lorg/apache/lucene/util/fst/FST$BytesReader;",
            ")",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p1, "fst":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    .local p2, "follow":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    .local p3, "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    .local p4, "in":Lorg/apache/lucene/util/fst/FST$BytesReader;, "Lorg/apache/lucene/util/fst/FST$BytesReader;"
    const/4 v10, -0x1

    const/4 v5, 0x0

    .line 874
    if-ne p0, v10, :cond_3

    .line 875
    invoke-virtual {p2}, Lorg/apache/lucene/util/fst/FST$Arc;->isFinal()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 876
    iget-wide v6, p2, Lorg/apache/lucene/util/fst/FST$Arc;->target:J

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-gtz v5, :cond_1

    .line 877
    const/4 v5, 0x2

    iput-byte v5, p3, Lorg/apache/lucene/util/fst/FST$Arc;->flags:B

    .line 884
    :goto_0
    iget-object v5, p2, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    iput-object v5, p3, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    .line 885
    iput v10, p3, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    .line 943
    .end local p3    # "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    :cond_0
    :goto_1
    return-object p3

    .line 879
    .restart local p3    # "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    :cond_1
    const/4 v5, 0x0

    iput-byte v5, p3, Lorg/apache/lucene/util/fst/FST$Arc;->flags:B

    .line 881
    iget-wide v6, p2, Lorg/apache/lucene/util/fst/FST$Arc;->target:J

    iput-wide v6, p3, Lorg/apache/lucene/util/fst/FST$Arc;->nextArc:J

    .line 882
    iget-wide v6, p2, Lorg/apache/lucene/util/fst/FST$Arc;->target:J

    iput-wide v6, p3, Lorg/apache/lucene/util/fst/FST$Arc;->node:J

    goto :goto_0

    :cond_2
    move-object p3, v5

    .line 888
    goto :goto_1

    .line 892
    :cond_3
    invoke-static {p2}, Lorg/apache/lucene/util/fst/FST;->targetHasArcs(Lorg/apache/lucene/util/fst/FST$Arc;)Z

    move-result v6

    if-nez v6, :cond_4

    move-object p3, v5

    .line 893
    goto :goto_1

    .line 895
    :cond_4
    invoke-virtual {p1, p2, p3, p4}, Lorg/apache/lucene/util/fst/FST;->readFirstTargetArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 896
    iget v6, p3, Lorg/apache/lucene/util/fst/FST$Arc;->bytesPerArc:I

    if-eqz v6, :cond_a

    iget v6, p3, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    if-eq v6, v10, :cond_a

    .line 900
    iget v2, p3, Lorg/apache/lucene/util/fst/FST$Arc;->arcIdx:I

    .line 901
    .local v2, "low":I
    iget v6, p3, Lorg/apache/lucene/util/fst/FST$Arc;->numArcs:I

    add-int/lit8 v1, v6, -0x1

    .line 902
    .local v1, "high":I
    const/4 v3, 0x0

    .line 905
    .local v3, "mid":I
    :goto_2
    if-le v2, v1, :cond_5

    .line 922
    iget v6, p3, Lorg/apache/lucene/util/fst/FST$Arc;->numArcs:I

    if-ne v2, v6, :cond_8

    move-object p3, v5

    .line 924
    goto :goto_1

    .line 906
    :cond_5
    add-int v6, v2, v1

    ushr-int/lit8 v3, v6, 0x1

    .line 907
    iget-wide v6, p3, Lorg/apache/lucene/util/fst/FST$Arc;->posArcsStart:J

    invoke-virtual {p4, v6, v7}, Lorg/apache/lucene/util/fst/FST$BytesReader;->setPosition(J)V

    .line 908
    iget v6, p3, Lorg/apache/lucene/util/fst/FST$Arc;->bytesPerArc:I

    mul-int/2addr v6, v3

    add-int/lit8 v6, v6, 0x1

    invoke-virtual {p4, v6}, Lorg/apache/lucene/util/fst/FST$BytesReader;->skipBytes(I)V

    .line 909
    invoke-virtual {p1, p4}, Lorg/apache/lucene/util/fst/FST;->readLabel(Lorg/apache/lucene/store/DataInput;)I

    move-result v4

    .line 910
    .local v4, "midLabel":I
    sub-int v0, v4, p0

    .line 913
    .local v0, "cmp":I
    if-gez v0, :cond_6

    .line 914
    add-int/lit8 v2, v3, 0x1

    .line 915
    goto :goto_2

    :cond_6
    if-lez v0, :cond_7

    .line 916
    add-int/lit8 v1, v3, -0x1

    .line 917
    goto :goto_2

    .line 918
    :cond_7
    add-int/lit8 v5, v3, -0x1

    iput v5, p3, Lorg/apache/lucene/util/fst/FST$Arc;->arcIdx:I

    .line 919
    invoke-virtual {p1, p3, p4}, Lorg/apache/lucene/util/fst/FST;->readNextRealArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object p3

    goto :goto_1

    .line 927
    .end local v0    # "cmp":I
    .end local v4    # "midLabel":I
    :cond_8
    if-le v2, v1, :cond_9

    .end local v1    # "high":I
    :goto_3
    iput v1, p3, Lorg/apache/lucene/util/fst/FST$Arc;->arcIdx:I

    .line 928
    invoke-virtual {p1, p3, p4}, Lorg/apache/lucene/util/fst/FST;->readNextRealArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object p3

    goto :goto_1

    .restart local v1    # "high":I
    :cond_9
    move v1, v2

    .line 927
    goto :goto_3

    .line 932
    .end local v1    # "high":I
    .end local v2    # "low":I
    .end local v3    # "mid":I
    :cond_a
    iget-wide v6, p2, Lorg/apache/lucene/util/fst/FST$Arc;->target:J

    invoke-virtual {p1, v6, v7, p3, p4}, Lorg/apache/lucene/util/fst/FST;->readFirstRealTargetArc(JLorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 939
    :goto_4
    iget v6, p3, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    if-ge v6, p0, :cond_0

    .line 942
    invoke-virtual {p3}, Lorg/apache/lucene/util/fst/FST$Arc;->isLast()Z

    move-result v6

    if-eqz v6, :cond_b

    move-object p3, v5

    .line 943
    goto :goto_1

    .line 945
    :cond_b
    invoke-virtual {p1, p3, p4}, Lorg/apache/lucene/util/fst/FST;->readNextRealArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    goto :goto_4
.end method

.method public static shortestPaths(Lorg/apache/lucene/util/fst/FST;Lorg/apache/lucene/util/fst/FST$Arc;Ljava/lang/Object;Ljava/util/Comparator;IZ)[Lorg/apache/lucene/util/fst/Util$MinResult;
    .locals 2
    .param p4, "topN"    # I
    .param p5, "allowEmptyString"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/lucene/util/fst/FST",
            "<TT;>;",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;TT;",
            "Ljava/util/Comparator",
            "<TT;>;IZ)[",
            "Lorg/apache/lucene/util/fst/Util$MinResult",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 530
    .local p0, "fst":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    .local p1, "fromNode":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    .local p2, "startOutput":Ljava/lang/Object;, "TT;"
    .local p3, "comparator":Ljava/util/Comparator;, "Ljava/util/Comparator<TT;>;"
    new-instance v0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;

    invoke-direct {v0, p0, p4, p4, p3}, Lorg/apache/lucene/util/fst/Util$TopNSearcher;-><init>(Lorg/apache/lucene/util/fst/FST;IILjava/util/Comparator;)V

    .line 534
    .local v0, "searcher":Lorg/apache/lucene/util/fst/Util$TopNSearcher;, "Lorg/apache/lucene/util/fst/Util$TopNSearcher<TT;>;"
    new-instance v1, Lorg/apache/lucene/util/IntsRef;

    invoke-direct {v1}, Lorg/apache/lucene/util/IntsRef;-><init>()V

    invoke-virtual {v0, p1, p2, p5, v1}, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->addStartPaths(Lorg/apache/lucene/util/fst/FST$Arc;Ljava/lang/Object;ZLorg/apache/lucene/util/IntsRef;)V

    .line 535
    invoke-virtual {v0}, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->search()[Lorg/apache/lucene/util/fst/Util$MinResult;

    move-result-object v1

    return-object v1
.end method

.method public static toBytesRef(Lorg/apache/lucene/util/IntsRef;Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/BytesRef;
    .locals 5
    .param p0, "input"    # Lorg/apache/lucene/util/IntsRef;
    .param p1, "scratch"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 840
    iget v2, p0, Lorg/apache/lucene/util/IntsRef;->length:I

    invoke-virtual {p1, v2}, Lorg/apache/lucene/util/BytesRef;->grow(I)V

    .line 841
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lorg/apache/lucene/util/IntsRef;->length:I

    if-lt v0, v2, :cond_0

    .line 847
    iget v2, p0, Lorg/apache/lucene/util/IntsRef;->length:I

    iput v2, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 848
    return-object p1

    .line 842
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/util/IntsRef;->ints:[I

    iget v3, p0, Lorg/apache/lucene/util/IntsRef;->offset:I

    add-int/2addr v3, v0

    aget v1, v2, v3

    .line 844
    .local v1, "value":I
    sget-boolean v2, Lorg/apache/lucene/util/fst/Util;->$assertionsDisabled:Z

    if-nez v2, :cond_2

    const/16 v2, -0x80

    if-lt v1, v2, :cond_1

    const/16 v2, 0xff

    if-le v1, v2, :cond_2

    :cond_1
    new-instance v2, Ljava/lang/AssertionError;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "value "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " doesn\'t fit into byte"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 845
    :cond_2
    iget-object v2, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    int-to-byte v3, v1

    aput-byte v3, v2, v0

    .line 841
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static toDot(Lorg/apache/lucene/util/fst/FST;Ljava/io/Writer;ZZ)V
    .locals 32
    .param p1, "out"    # Ljava/io/Writer;
    .param p2, "sameRank"    # Z
    .param p3, "labelStates"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/lucene/util/fst/FST",
            "<TT;>;",
            "Ljava/io/Writer;",
            "ZZ)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 572
    .local p0, "fst":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    const-string v9, "blue"

    .line 576
    .local v9, "expandedNodeColor":Ljava/lang/String;
    new-instance v27, Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-direct/range {v27 .. v27}, Lorg/apache/lucene/util/fst/FST$Arc;-><init>()V

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/fst/FST;->getFirstArc(Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v22

    .line 579
    .local v22, "startArc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    new-instance v26, Ljava/util/ArrayList;

    invoke-direct/range {v26 .. v26}, Ljava/util/ArrayList;-><init>()V

    .line 582
    .local v26, "thisLevelQueue":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/util/fst/FST$Arc<TT;>;>;"
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 583
    .local v15, "nextLevelQueue":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/util/fst/FST$Arc<TT;>;>;"
    move-object/from16 v0, v22

    invoke-interface {v15, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 587
    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    .line 590
    .local v20, "sameLevelStates":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    new-instance v21, Ljava/util/BitSet;

    invoke-direct/range {v21 .. v21}, Ljava/util/BitSet;-><init>()V

    .line 591
    .local v21, "seen":Ljava/util/BitSet;
    move-object/from16 v0, v22

    iget-wide v0, v0, Lorg/apache/lucene/util/fst/FST$Arc;->target:J

    move-wide/from16 v28, v0

    move-wide/from16 v0, v28

    long-to-int v0, v0

    move/from16 v27, v0

    move-object/from16 v0, v21

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 594
    const-string v25, "circle"

    .line 595
    .local v25, "stateShape":Ljava/lang/String;
    const-string v12, "doublecircle"

    .line 598
    .local v12, "finalStateShape":Ljava/lang/String;
    const-string v27, "digraph FST {\n"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 599
    const-string v27, "  rankdir = LR; splines=true; concentrate=true; ordering=out; ranksep=2.5; \n"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 601
    if-nez p3, :cond_0

    .line 602
    const-string v27, "  node [shape=circle, width=.2, height=.2, style=filled]\n"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 605
    :cond_0
    const-string v27, "initial"

    const-string v28, "point"

    const-string/jumbo v29, "white"

    const-string v30, ""

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    move-object/from16 v3, v29

    move-object/from16 v4, v30

    invoke-static {v0, v1, v2, v3, v4}, Lorg/apache/lucene/util/fst/Util;->emitDotState(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 607
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lorg/apache/lucene/util/fst/Outputs;->getNoOutput()Ljava/lang/Object;

    move-result-object v6

    .line 608
    .local v6, "NO_OUTPUT":Ljava/lang/Object;, "TT;"
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/util/fst/FST;->getBytesReader()Lorg/apache/lucene/util/fst/FST$BytesReader;

    move-result-object v19

    .line 614
    .local v19, "r":Lorg/apache/lucene/util/fst/FST$BytesReader;, "Lorg/apache/lucene/util/fst/FST$BytesReader;"
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/fst/FST;->isExpandedTarget(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Z

    move-result v27

    if-eqz v27, :cond_1

    .line 615
    const-string v24, "blue"

    .line 622
    .local v24, "stateColor":Ljava/lang/String;
    :goto_0
    invoke-virtual/range {v22 .. v22}, Lorg/apache/lucene/util/fst/FST$Arc;->isFinal()Z

    move-result v27

    if-eqz v27, :cond_3

    .line 623
    const/4 v13, 0x1

    .line 624
    .local v13, "isFinal":Z
    move-object/from16 v0, v22

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    if-ne v0, v6, :cond_2

    const/4 v11, 0x0

    .line 630
    :goto_1
    move-object/from16 v0, v22

    iget-wide v0, v0, Lorg/apache/lucene/util/fst/FST$Arc;->target:J

    move-wide/from16 v28, v0

    invoke-static/range {v28 .. v29}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v29

    if-eqz v13, :cond_4

    const-string v27, "doublecircle"

    move-object/from16 v28, v27

    :goto_2
    if-nez v11, :cond_5

    const-string v27, ""

    :goto_3
    move-object/from16 v0, p1

    move-object/from16 v1, v29

    move-object/from16 v2, v28

    move-object/from16 v3, v24

    move-object/from16 v4, v27

    invoke-static {v0, v1, v2, v3, v4}, Lorg/apache/lucene/util/fst/Util;->emitDotState(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 633
    new-instance v27, Ljava/lang/StringBuilder;

    const-string v28, "  initial -> "

    invoke-direct/range {v27 .. v28}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v22

    iget-wide v0, v0, Lorg/apache/lucene/util/fst/FST$Arc;->target:J

    move-wide/from16 v28, v0

    invoke-virtual/range {v27 .. v29}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, "\n"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 635
    const/4 v14, 0x0

    .line 637
    .local v14, "level":I
    :goto_4
    invoke-interface {v15}, Ljava/util/List;->isEmpty()Z

    move-result v27

    if-eqz v27, :cond_6

    .line 746
    const-string v27, "  -1 [style=filled, color=black, shape=doublecircle, label=\"\"]\n\n"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 747
    const-string v27, "  {rank=sink; -1 }\n"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 749
    const-string/jumbo v27, "}\n"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 750
    invoke-virtual/range {p1 .. p1}, Ljava/io/Writer;->flush()V

    .line 751
    return-void

    .line 617
    .end local v13    # "isFinal":Z
    .end local v14    # "level":I
    .end local v24    # "stateColor":Ljava/lang/String;
    :cond_1
    const/16 v24, 0x0

    .restart local v24    # "stateColor":Ljava/lang/String;
    goto :goto_0

    .line 624
    .restart local v13    # "isFinal":Z
    :cond_2
    move-object/from16 v0, v22

    iget-object v11, v0, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    goto :goto_1

    .line 626
    .end local v13    # "isFinal":Z
    :cond_3
    const/4 v13, 0x0

    .line 627
    .restart local v13    # "isFinal":Z
    const/4 v11, 0x0

    .local v11, "finalOutput":Ljava/lang/Object;, "TT;"
    goto :goto_1

    .line 630
    .end local v11    # "finalOutput":Ljava/lang/Object;, "TT;"
    :cond_4
    const-string v27, "circle"

    move-object/from16 v28, v27

    goto :goto_2

    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-virtual {v0, v11}, Lorg/apache/lucene/util/fst/Outputs;->outputToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v27

    goto :goto_3

    .line 640
    .restart local v14    # "level":I
    :cond_6
    move-object/from16 v0, v26

    invoke-interface {v0, v15}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 641
    invoke-interface {v15}, Ljava/util/List;->clear()V

    .line 643
    add-int/lit8 v14, v14, 0x1

    .line 644
    new-instance v27, Ljava/lang/StringBuilder;

    const-string v28, "\n  // Transitions and states at level: "

    invoke-direct/range {v27 .. v28}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v27

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, "\n"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 645
    :cond_7
    invoke-interface/range {v26 .. v26}, Ljava/util/List;->isEmpty()Z

    move-result v27

    if-eqz v27, :cond_9

    .line 735
    if-eqz p2, :cond_8

    invoke-interface/range {v20 .. v20}, Ljava/util/List;->size()I

    move-result v27

    const/16 v28, 0x1

    move/from16 v0, v27

    move/from16 v1, v28

    if-le v0, v1, :cond_8

    .line 736
    const-string v27, "  {rank=same; "

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 737
    invoke-interface/range {v20 .. v20}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    :goto_5
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v27

    if-nez v27, :cond_12

    .line 740
    const-string v27, " }\n"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 742
    :cond_8
    invoke-interface/range {v20 .. v20}, Ljava/util/List;->clear()V

    goto/16 :goto_4

    .line 646
    :cond_9
    invoke-interface/range {v26 .. v26}, Ljava/util/List;->size()I

    move-result v27

    add-int/lit8 v27, v27, -0x1

    invoke-interface/range {v26 .. v27}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/lucene/util/fst/FST$Arc;

    .line 648
    .local v7, "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    invoke-static {v7}, Lorg/apache/lucene/util/fst/FST;->targetHasArcs(Lorg/apache/lucene/util/fst/FST$Arc;)Z

    move-result v27

    if-eqz v27, :cond_7

    .line 652
    iget-wide v0, v7, Lorg/apache/lucene/util/fst/FST$Arc;->target:J

    move-wide/from16 v16, v0

    .line 654
    .local v16, "node":J
    iget-wide v0, v7, Lorg/apache/lucene/util/fst/FST$Arc;->target:J

    move-wide/from16 v28, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v28

    move-object/from16 v3, v19

    invoke-virtual {v0, v1, v2, v7, v3}, Lorg/apache/lucene/util/fst/FST;->readFirstRealTargetArc(JLorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 662
    :goto_6
    iget-wide v0, v7, Lorg/apache/lucene/util/fst/FST$Arc;->target:J

    move-wide/from16 v28, v0

    const-wide/16 v30, 0x0

    cmp-long v27, v28, v30

    if-ltz v27, :cond_a

    iget-wide v0, v7, Lorg/apache/lucene/util/fst/FST$Arc;->target:J

    move-wide/from16 v28, v0

    move-wide/from16 v0, v28

    long-to-int v0, v0

    move/from16 v27, v0

    move-object/from16 v0, v21

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v27

    if-nez v27, :cond_a

    .line 676
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v7, v1}, Lorg/apache/lucene/util/fst/FST;->isExpandedTarget(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Z

    move-result v27

    if-eqz v27, :cond_c

    .line 677
    const-string v24, "blue"

    .line 683
    :goto_7
    iget-object v0, v7, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    move-object/from16 v27, v0

    if-eqz v27, :cond_d

    iget-object v0, v7, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    if-eq v0, v6, :cond_d

    .line 684
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    move-object/from16 v27, v0

    iget-object v0, v7, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    move-object/from16 v28, v0

    invoke-virtual/range {v27 .. v28}, Lorg/apache/lucene/util/fst/Outputs;->outputToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 689
    .local v10, "finalOutput":Ljava/lang/String;
    :goto_8
    iget-wide v0, v7, Lorg/apache/lucene/util/fst/FST$Arc;->target:J

    move-wide/from16 v28, v0

    invoke-static/range {v28 .. v29}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v27

    const-string v28, "circle"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    move-object/from16 v3, v24

    invoke-static {v0, v1, v2, v3, v10}, Lorg/apache/lucene/util/fst/Util;->emitDotState(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 692
    iget-wide v0, v7, Lorg/apache/lucene/util/fst/FST$Arc;->target:J

    move-wide/from16 v28, v0

    move-wide/from16 v0, v28

    long-to-int v0, v0

    move/from16 v27, v0

    move-object/from16 v0, v21

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 693
    new-instance v27, Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-direct/range {v27 .. v27}, Lorg/apache/lucene/util/fst/FST$Arc;-><init>()V

    move-object/from16 v0, v27

    invoke-virtual {v0, v7}, Lorg/apache/lucene/util/fst/FST$Arc;->copyFrom(Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-interface {v15, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 694
    iget-wide v0, v7, Lorg/apache/lucene/util/fst/FST$Arc;->target:J

    move-wide/from16 v28, v0

    move-wide/from16 v0, v28

    long-to-int v0, v0

    move/from16 v27, v0

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    move-object/from16 v0, v20

    move-object/from16 v1, v27

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 698
    .end local v10    # "finalOutput":Ljava/lang/String;
    :cond_a
    iget-object v0, v7, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    if-eq v0, v6, :cond_e

    .line 699
    new-instance v27, Ljava/lang/StringBuilder;

    const-string v28, "/"

    invoke-direct/range {v27 .. v28}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    move-object/from16 v28, v0

    iget-object v0, v7, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    move-object/from16 v29, v0

    invoke-virtual/range {v28 .. v29}, Lorg/apache/lucene/util/fst/Outputs;->outputToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    .line 704
    .local v18, "outs":Ljava/lang/String;
    :goto_9
    invoke-static {v7}, Lorg/apache/lucene/util/fst/FST;->targetHasArcs(Lorg/apache/lucene/util/fst/FST$Arc;)Z

    move-result v27

    if-nez v27, :cond_b

    invoke-virtual {v7}, Lorg/apache/lucene/util/fst/FST$Arc;->isFinal()Z

    move-result v27

    if-eqz v27, :cond_b

    iget-object v0, v7, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    if-eq v0, v6, :cond_b

    .line 711
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-static/range {v18 .. v18}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v28

    invoke-direct/range {v27 .. v28}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v28, "/["

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    move-object/from16 v28, v0

    iget-object v0, v7, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    move-object/from16 v29, v0

    invoke-virtual/range {v28 .. v29}, Lorg/apache/lucene/util/fst/Outputs;->outputToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, "]"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    .line 715
    :cond_b
    const/16 v27, 0x4

    move/from16 v0, v27

    invoke-virtual {v7, v0}, Lorg/apache/lucene/util/fst/FST$Arc;->flag(I)Z

    move-result v27

    if-eqz v27, :cond_f

    .line 716
    const-string v8, "red"

    .line 721
    .local v8, "arcColor":Ljava/lang/String;
    :goto_a
    sget-boolean v27, Lorg/apache/lucene/util/fst/Util;->$assertionsDisabled:Z

    if-nez v27, :cond_10

    iget v0, v7, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    move/from16 v27, v0

    const/16 v28, -0x1

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_10

    new-instance v27, Ljava/lang/AssertionError;

    invoke-direct/range {v27 .. v27}, Ljava/lang/AssertionError;-><init>()V

    throw v27

    .line 679
    .end local v8    # "arcColor":Ljava/lang/String;
    .end local v18    # "outs":Ljava/lang/String;
    :cond_c
    const/16 v24, 0x0

    goto/16 :goto_7

    .line 686
    :cond_d
    const-string v10, ""

    .restart local v10    # "finalOutput":Ljava/lang/String;
    goto/16 :goto_8

    .line 701
    .end local v10    # "finalOutput":Ljava/lang/String;
    :cond_e
    const-string v18, ""

    .restart local v18    # "outs":Ljava/lang/String;
    goto :goto_9

    .line 718
    :cond_f
    const-string v8, "black"

    .restart local v8    # "arcColor":Ljava/lang/String;
    goto :goto_a

    .line 722
    :cond_10
    new-instance v27, Ljava/lang/StringBuilder;

    const-string v28, "  "

    invoke-direct/range {v27 .. v28}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v27

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, " -> "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    iget-wide v0, v7, Lorg/apache/lucene/util/fst/FST$Arc;->target:J

    move-wide/from16 v28, v0

    invoke-virtual/range {v27 .. v29}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, " [label=\""

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    iget v0, v7, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    move/from16 v28, v0

    invoke-static/range {v28 .. v28}, Lorg/apache/lucene/util/fst/Util;->printableLabel(I)Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, "\""

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual {v7}, Lorg/apache/lucene/util/fst/FST$Arc;->isFinal()Z

    move-result v27

    if-eqz v27, :cond_11

    const-string v27, " style=\"bold\""

    :goto_b
    move-object/from16 v0, v28

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, " color=\""

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, "\"]\n"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 725
    invoke-virtual {v7}, Lorg/apache/lucene/util/fst/FST$Arc;->isLast()Z

    move-result v27

    if-nez v27, :cond_7

    .line 729
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v7, v1}, Lorg/apache/lucene/util/fst/FST;->readNextRealArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    goto/16 :goto_6

    .line 722
    :cond_11
    const-string v27, ""

    goto :goto_b

    .line 737
    .end local v7    # "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    .end local v8    # "arcColor":Ljava/lang/String;
    .end local v16    # "node":J
    .end local v18    # "outs":Ljava/lang/String;
    :cond_12
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/lang/Integer;

    invoke-virtual/range {v27 .. v27}, Ljava/lang/Integer;->intValue()I

    move-result v23

    .line 738
    .local v23, "state":I
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-static/range {v23 .. v23}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v27

    move-object/from16 v1, v29

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v29, "; "

    move-object/from16 v0, v27

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    goto/16 :goto_5
.end method

.method public static toIntsRef(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/IntsRef;)Lorg/apache/lucene/util/IntsRef;
    .locals 4
    .param p0, "input"    # Lorg/apache/lucene/util/BytesRef;
    .param p1, "scratch"    # Lorg/apache/lucene/util/IntsRef;

    .prologue
    .line 829
    iget v1, p0, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-virtual {p1, v1}, Lorg/apache/lucene/util/IntsRef;->grow(I)V

    .line 830
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lorg/apache/lucene/util/BytesRef;->length:I

    if-lt v0, v1, :cond_0

    .line 833
    iget v1, p0, Lorg/apache/lucene/util/BytesRef;->length:I

    iput v1, p1, Lorg/apache/lucene/util/IntsRef;->length:I

    .line 834
    return-object p1

    .line 831
    :cond_0
    iget-object v1, p1, Lorg/apache/lucene/util/IntsRef;->ints:[I

    iget-object v2, p0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v3, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    add-int/2addr v3, v0

    aget-byte v2, v2, v3

    and-int/lit16 v2, v2, 0xff

    aput v2, v1, v0

    .line 830
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static toUTF16(Ljava/lang/CharSequence;Lorg/apache/lucene/util/IntsRef;)Lorg/apache/lucene/util/IntsRef;
    .locals 4
    .param p0, "s"    # Ljava/lang/CharSequence;
    .param p1, "scratch"    # Lorg/apache/lucene/util/IntsRef;

    .prologue
    .line 780
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    .line 781
    .local v0, "charLimit":I
    const/4 v2, 0x0

    iput v2, p1, Lorg/apache/lucene/util/IntsRef;->offset:I

    .line 782
    iput v0, p1, Lorg/apache/lucene/util/IntsRef;->length:I

    .line 783
    invoke-virtual {p1, v0}, Lorg/apache/lucene/util/IntsRef;->grow(I)V

    .line 784
    const/4 v1, 0x0

    .local v1, "idx":I
    :goto_0
    if-lt v1, v0, :cond_0

    .line 787
    return-object p1

    .line 785
    :cond_0
    iget-object v2, p1, Lorg/apache/lucene/util/IntsRef;->ints:[I

    invoke-interface {p0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    aput v3, v2, v1

    .line 784
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static toUTF32(Ljava/lang/CharSequence;Lorg/apache/lucene/util/IntsRef;)Lorg/apache/lucene/util/IntsRef;
    .locals 5
    .param p0, "s"    # Ljava/lang/CharSequence;
    .param p1, "scratch"    # Lorg/apache/lucene/util/IntsRef;

    .prologue
    .line 794
    const/4 v0, 0x0

    .line 795
    .local v0, "charIdx":I
    const/4 v2, 0x0

    .line 796
    .local v2, "intIdx":I
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    .line 797
    .local v1, "charLimit":I
    :goto_0
    if-lt v0, v1, :cond_0

    .line 804
    iput v2, p1, Lorg/apache/lucene/util/IntsRef;->length:I

    .line 805
    return-object p1

    .line 798
    :cond_0
    add-int/lit8 v4, v2, 0x1

    invoke-virtual {p1, v4}, Lorg/apache/lucene/util/IntsRef;->grow(I)V

    .line 799
    invoke-static {p0, v0}, Ljava/lang/Character;->codePointAt(Ljava/lang/CharSequence;I)I

    move-result v3

    .line 800
    .local v3, "utf32":I
    iget-object v4, p1, Lorg/apache/lucene/util/IntsRef;->ints:[I

    aput v3, v4, v2

    .line 801
    invoke-static {v3}, Ljava/lang/Character;->charCount(I)I

    move-result v4

    add-int/2addr v0, v4

    .line 802
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static toUTF32([CIILorg/apache/lucene/util/IntsRef;)Lorg/apache/lucene/util/IntsRef;
    .locals 5
    .param p0, "s"    # [C
    .param p1, "offset"    # I
    .param p2, "length"    # I
    .param p3, "scratch"    # Lorg/apache/lucene/util/IntsRef;

    .prologue
    .line 812
    move v0, p1

    .line 813
    .local v0, "charIdx":I
    const/4 v2, 0x0

    .line 814
    .local v2, "intIdx":I
    add-int v1, p1, p2

    .line 815
    .local v1, "charLimit":I
    :goto_0
    if-lt v0, v1, :cond_0

    .line 822
    iput v2, p3, Lorg/apache/lucene/util/IntsRef;->length:I

    .line 823
    return-object p3

    .line 816
    :cond_0
    add-int/lit8 v4, v2, 0x1

    invoke-virtual {p3, v4}, Lorg/apache/lucene/util/IntsRef;->grow(I)V

    .line 817
    invoke-static {p0, v0}, Ljava/lang/Character;->codePointAt([CI)I

    move-result v3

    .line 818
    .local v3, "utf32":I
    iget-object v4, p3, Lorg/apache/lucene/util/IntsRef;->ints:[I

    aput v3, v4, v2

    .line 819
    invoke-static {v3}, Ljava/lang/Character;->charCount(I)I

    move-result v4

    add-int/2addr v0, v4

    .line 820
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method
