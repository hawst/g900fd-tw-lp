.class public final Lorg/apache/lucene/util/fst/FST;
.super Ljava/lang/Object;
.source "FST.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/fst/FST$Arc;,
        Lorg/apache/lucene/util/fst/FST$ArcAndState;,
        Lorg/apache/lucene/util/fst/FST$BytesReader;,
        Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;,
        Lorg/apache/lucene/util/fst/FST$NodeAndInCount;,
        Lorg/apache/lucene/util/fst/FST$NodeQueue;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final ARCS_AS_FIXED_ARRAY:B = 0x20t

.field static final BIT_ARC_HAS_FINAL_OUTPUT:I = 0x20

.field static final BIT_ARC_HAS_OUTPUT:I = 0x10

.field static final BIT_FINAL_ARC:I = 0x1

.field static final BIT_LAST_ARC:I = 0x2

.field static final BIT_STOP_NODE:I = 0x8

.field private static final BIT_TARGET_DELTA:I = 0x40

.field static final BIT_TARGET_NEXT:I = 0x4

.field public static final DEFAULT_MAX_BLOCK_BITS:I

.field public static final END_LABEL:I = -0x1

.field private static final FILE_FORMAT_NAME:Ljava/lang/String; = "FST"

.field private static final FINAL_END_NODE:J = -0x1L

.field static final FIXED_ARRAY_NUM_ARCS_DEEP:I = 0xa

.field static final FIXED_ARRAY_NUM_ARCS_SHALLOW:I = 0x5

.field static final FIXED_ARRAY_SHALLOW_DISTANCE:I = 0x3

.field private static final NON_FINAL_END_NODE:J = 0x0L

.field private static final VERSION_CURRENT:I = 0x4

.field private static final VERSION_INT_NUM_BYTES_PER_ARC:I = 0x1

.field private static final VERSION_PACKED:I = 0x3

.field private static final VERSION_SHORT_BYTE2_LABELS:I = 0x2

.field private static final VERSION_START:I = 0x0

.field private static final VERSION_VINT_TARGET:I = 0x4


# instance fields
.field private final NO_OUTPUT:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final allowArrayArcs:Z

.field public arcCount:J

.field public arcWithOutputCount:J

.field final bytes:Lorg/apache/lucene/util/fst/BytesStore;

.field private bytesPerArc:[I

.field private cachedRootArcs:[Lorg/apache/lucene/util/fst/FST$Arc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;"
        }
    .end annotation
.end field

.field emptyOutput:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private inCounts:Lorg/apache/lucene/util/packed/GrowableWriter;

.field public final inputType:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

.field private lastFrozenNode:J

.field private nodeAddress:Lorg/apache/lucene/util/packed/GrowableWriter;

.field public nodeCount:J

.field private nodeRefToAddress:Lorg/apache/lucene/util/packed/PackedInts$Reader;

.field public final outputs:Lorg/apache/lucene/util/fst/Outputs;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/fst/Outputs",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final packed:Z

.field private startNode:J

.field private final version:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 72
    const-class v0, Lorg/apache/lucene/util/fst/FST;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/fst/FST;->$assertionsDisabled:Z

    .line 295
    sget-boolean v0, Lorg/apache/lucene/util/Constants;->JRE_IS_64BIT:Z

    if-eqz v0, :cond_1

    const/16 v0, 0x1e

    :goto_1
    sput v0, Lorg/apache/lucene/util/fst/FST;->DEFAULT_MAX_BLOCK_BITS:I

    return-void

    .line 72
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 295
    :cond_1
    const/16 v0, 0x1c

    goto :goto_1
.end method

.method public constructor <init>(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/fst/Outputs;)V
    .locals 1
    .param p1, "in"    # Lorg/apache/lucene/store/DataInput;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/store/DataInput;",
            "Lorg/apache/lucene/util/fst/Outputs",
            "<TT;>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 299
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    .local p2, "outputs":Lorg/apache/lucene/util/fst/Outputs;, "Lorg/apache/lucene/util/fst/Outputs<TT;>;"
    sget v0, Lorg/apache/lucene/util/fst/FST;->DEFAULT_MAX_BLOCK_BITS:I

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/util/fst/FST;-><init>(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/fst/Outputs;I)V

    .line 300
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/fst/Outputs;I)V
    .locals 11
    .param p1, "in"    # Lorg/apache/lucene/store/DataInput;
    .param p3, "maxBlockBits"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/store/DataInput;",
            "Lorg/apache/lucene/util/fst/Outputs",
            "<TT;>;I)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    .local p2, "outputs":Lorg/apache/lucene/util/fst/Outputs;, "Lorg/apache/lucene/util/fst/Outputs<TT;>;"
    const/4 v10, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 304
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114
    new-array v5, v7, [I

    iput-object v5, p0, Lorg/apache/lucene/util/fst/FST;->bytesPerArc:[I

    .line 149
    const-wide/16 v8, -0x1

    iput-wide v8, p0, Lorg/apache/lucene/util/fst/FST;->startNode:J

    .line 305
    iput-object p2, p0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    .line 307
    if-lt p3, v6, :cond_0

    const/16 v5, 0x1e

    if-le p3, v5, :cond_1

    .line 308
    :cond_0
    new-instance v5, Ljava/lang/IllegalArgumentException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "maxBlockBits should be 1 .. 30; got "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 313
    :cond_1
    const-string v5, "FST"

    const/4 v8, 0x3

    const/4 v9, 0x4

    invoke-static {p1, v5, v8, v9}, Lorg/apache/lucene/codecs/CodecUtil;->checkHeader(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I

    move-result v5

    iput v5, p0, Lorg/apache/lucene/util/fst/FST;->version:I

    .line 314
    invoke-virtual {p1}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v5

    if-ne v5, v6, :cond_3

    move v5, v6

    :goto_0
    iput-boolean v5, p0, Lorg/apache/lucene/util/fst/FST;->packed:Z

    .line 315
    invoke-virtual {p1}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v5

    if-ne v5, v6, :cond_5

    .line 318
    new-instance v0, Lorg/apache/lucene/util/fst/BytesStore;

    const/16 v5, 0xa

    invoke-direct {v0, v5}, Lorg/apache/lucene/util/fst/BytesStore;-><init>(I)V

    .line 319
    .local v0, "emptyBytes":Lorg/apache/lucene/util/fst/BytesStore;
    invoke-virtual {p1}, Lorg/apache/lucene/store/DataInput;->readVInt()I

    move-result v2

    .line 320
    .local v2, "numBytes":I
    int-to-long v8, v2

    invoke-virtual {v0, p1, v8, v9}, Lorg/apache/lucene/util/fst/BytesStore;->copyBytes(Lorg/apache/lucene/store/DataInput;J)V

    .line 324
    iget-boolean v5, p0, Lorg/apache/lucene/util/fst/FST;->packed:Z

    if-eqz v5, :cond_4

    .line 325
    invoke-virtual {v0}, Lorg/apache/lucene/util/fst/BytesStore;->getForwardReader()Lorg/apache/lucene/util/fst/FST$BytesReader;

    move-result-object v1

    .line 335
    .local v1, "reader":Lorg/apache/lucene/util/fst/FST$BytesReader;, "Lorg/apache/lucene/util/fst/FST$BytesReader;"
    :cond_2
    :goto_1
    invoke-virtual {p2, v1}, Lorg/apache/lucene/util/fst/Outputs;->readFinalOutput(Lorg/apache/lucene/store/DataInput;)Ljava/lang/Object;

    move-result-object v5

    iput-object v5, p0, Lorg/apache/lucene/util/fst/FST;->emptyOutput:Ljava/lang/Object;

    .line 339
    .end local v0    # "emptyBytes":Lorg/apache/lucene/util/fst/BytesStore;
    .end local v1    # "reader":Lorg/apache/lucene/util/fst/FST$BytesReader;, "Lorg/apache/lucene/util/fst/FST$BytesReader;"
    .end local v2    # "numBytes":I
    :goto_2
    invoke-virtual {p1}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v4

    .line 340
    .local v4, "t":B
    packed-switch v4, :pswitch_data_0

    .line 351
    new-instance v5, Ljava/lang/IllegalStateException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "invalid input type "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v5

    .end local v4    # "t":B
    :cond_3
    move v5, v7

    .line 314
    goto :goto_0

    .line 327
    .restart local v0    # "emptyBytes":Lorg/apache/lucene/util/fst/BytesStore;
    .restart local v2    # "numBytes":I
    :cond_4
    invoke-virtual {v0}, Lorg/apache/lucene/util/fst/BytesStore;->getReverseReader()Lorg/apache/lucene/util/fst/FST$BytesReader;

    move-result-object v1

    .line 331
    .restart local v1    # "reader":Lorg/apache/lucene/util/fst/FST$BytesReader;, "Lorg/apache/lucene/util/fst/FST$BytesReader;"
    if-lez v2, :cond_2

    .line 332
    add-int/lit8 v5, v2, -0x1

    int-to-long v8, v5

    invoke-virtual {v1, v8, v9}, Lorg/apache/lucene/util/fst/FST$BytesReader;->setPosition(J)V

    goto :goto_1

    .line 337
    .end local v0    # "emptyBytes":Lorg/apache/lucene/util/fst/BytesStore;
    .end local v1    # "reader":Lorg/apache/lucene/util/fst/FST$BytesReader;, "Lorg/apache/lucene/util/fst/FST$BytesReader;"
    .end local v2    # "numBytes":I
    :cond_5
    iput-object v10, p0, Lorg/apache/lucene/util/fst/FST;->emptyOutput:Ljava/lang/Object;

    goto :goto_2

    .line 342
    .restart local v4    # "t":B
    :pswitch_0
    sget-object v5, Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;->BYTE1:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    iput-object v5, p0, Lorg/apache/lucene/util/fst/FST;->inputType:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    .line 353
    :goto_3
    iget-boolean v5, p0, Lorg/apache/lucene/util/fst/FST;->packed:Z

    if-eqz v5, :cond_6

    .line 354
    invoke-static {p1}, Lorg/apache/lucene/util/packed/PackedInts;->getReader(Lorg/apache/lucene/store/DataInput;)Lorg/apache/lucene/util/packed/PackedInts$Reader;

    move-result-object v5

    iput-object v5, p0, Lorg/apache/lucene/util/fst/FST;->nodeRefToAddress:Lorg/apache/lucene/util/packed/PackedInts$Reader;

    .line 358
    :goto_4
    invoke-virtual {p1}, Lorg/apache/lucene/store/DataInput;->readVLong()J

    move-result-wide v8

    iput-wide v8, p0, Lorg/apache/lucene/util/fst/FST;->startNode:J

    .line 359
    invoke-virtual {p1}, Lorg/apache/lucene/store/DataInput;->readVLong()J

    move-result-wide v8

    iput-wide v8, p0, Lorg/apache/lucene/util/fst/FST;->nodeCount:J

    .line 360
    invoke-virtual {p1}, Lorg/apache/lucene/store/DataInput;->readVLong()J

    move-result-wide v8

    iput-wide v8, p0, Lorg/apache/lucene/util/fst/FST;->arcCount:J

    .line 361
    invoke-virtual {p1}, Lorg/apache/lucene/store/DataInput;->readVLong()J

    move-result-wide v8

    iput-wide v8, p0, Lorg/apache/lucene/util/fst/FST;->arcWithOutputCount:J

    .line 363
    invoke-virtual {p1}, Lorg/apache/lucene/store/DataInput;->readVLong()J

    move-result-wide v2

    .line 364
    .local v2, "numBytes":J
    new-instance v5, Lorg/apache/lucene/util/fst/BytesStore;

    shl-int/2addr v6, p3

    invoke-direct {v5, p1, v2, v3, v6}, Lorg/apache/lucene/util/fst/BytesStore;-><init>(Lorg/apache/lucene/store/DataInput;JI)V

    iput-object v5, p0, Lorg/apache/lucene/util/fst/FST;->bytes:Lorg/apache/lucene/util/fst/BytesStore;

    .line 366
    invoke-virtual {p2}, Lorg/apache/lucene/util/fst/Outputs;->getNoOutput()Ljava/lang/Object;

    move-result-object v5

    iput-object v5, p0, Lorg/apache/lucene/util/fst/FST;->NO_OUTPUT:Ljava/lang/Object;

    .line 368
    invoke-direct {p0}, Lorg/apache/lucene/util/fst/FST;->cacheRootArcs()V

    .line 373
    iput-boolean v7, p0, Lorg/apache/lucene/util/fst/FST;->allowArrayArcs:Z

    .line 383
    return-void

    .line 345
    .end local v2    # "numBytes":J
    :pswitch_1
    sget-object v5, Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;->BYTE2:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    iput-object v5, p0, Lorg/apache/lucene/util/fst/FST;->inputType:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    goto :goto_3

    .line 348
    :pswitch_2
    sget-object v5, Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;->BYTE4:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    iput-object v5, p0, Lorg/apache/lucene/util/fst/FST;->inputType:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    goto :goto_3

    .line 356
    :cond_6
    iput-object v10, p0, Lorg/apache/lucene/util/fst/FST;->nodeRefToAddress:Lorg/apache/lucene/util/packed/PackedInts$Reader;

    goto :goto_4

    .line 340
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private constructor <init>(Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;Lorg/apache/lucene/util/fst/Outputs;I)V
    .locals 3
    .param p3, "bytesPageBits"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;",
            "Lorg/apache/lucene/util/fst/Outputs",
            "<TT;>;I)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    .local p1, "inputType":Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;, "Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;"
    .local p2, "outputs":Lorg/apache/lucene/util/fst/Outputs;, "Lorg/apache/lucene/util/fst/Outputs<TT;>;"
    const/4 v2, 0x0

    .line 1406
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114
    new-array v0, v2, [I

    iput-object v0, p0, Lorg/apache/lucene/util/fst/FST;->bytesPerArc:[I

    .line 149
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lorg/apache/lucene/util/fst/FST;->startNode:J

    .line 1407
    const/4 v0, 0x4

    iput v0, p0, Lorg/apache/lucene/util/fst/FST;->version:I

    .line 1408
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/util/fst/FST;->packed:Z

    .line 1409
    iput-object p1, p0, Lorg/apache/lucene/util/fst/FST;->inputType:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    .line 1410
    new-instance v0, Lorg/apache/lucene/util/fst/BytesStore;

    invoke-direct {v0, p3}, Lorg/apache/lucene/util/fst/BytesStore;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/util/fst/FST;->bytes:Lorg/apache/lucene/util/fst/BytesStore;

    .line 1411
    iput-object p2, p0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    .line 1412
    invoke-virtual {p2}, Lorg/apache/lucene/util/fst/Outputs;->getNoOutput()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/util/fst/FST;->NO_OUTPUT:Ljava/lang/Object;

    .line 1417
    iput-boolean v2, p0, Lorg/apache/lucene/util/fst/FST;->allowArrayArcs:Z

    .line 1418
    return-void
.end method

.method constructor <init>(Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;Lorg/apache/lucene/util/fst/Outputs;ZFZI)V
    .locals 5
    .param p3, "willPackFST"    # Z
    .param p4, "acceptableOverheadRatio"    # F
    .param p5, "allowArrayArcs"    # Z
    .param p6, "bytesPageBits"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;",
            "Lorg/apache/lucene/util/fst/Outputs",
            "<TT;>;ZFZI)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    .local p1, "inputType":Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;, "Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;"
    .local p2, "outputs":Lorg/apache/lucene/util/fst/Outputs;, "Lorg/apache/lucene/util/fst/Outputs<TT;>;"
    const/16 v4, 0x8

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 272
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114
    new-array v0, v3, [I

    iput-object v0, p0, Lorg/apache/lucene/util/fst/FST;->bytesPerArc:[I

    .line 149
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lorg/apache/lucene/util/fst/FST;->startNode:J

    .line 273
    iput-object p1, p0, Lorg/apache/lucene/util/fst/FST;->inputType:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    .line 274
    iput-object p2, p0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    .line 275
    iput-boolean p5, p0, Lorg/apache/lucene/util/fst/FST;->allowArrayArcs:Z

    .line 276
    const/4 v0, 0x4

    iput v0, p0, Lorg/apache/lucene/util/fst/FST;->version:I

    .line 277
    new-instance v0, Lorg/apache/lucene/util/fst/BytesStore;

    invoke-direct {v0, p6}, Lorg/apache/lucene/util/fst/BytesStore;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/util/fst/FST;->bytes:Lorg/apache/lucene/util/fst/BytesStore;

    .line 280
    iget-object v0, p0, Lorg/apache/lucene/util/fst/FST;->bytes:Lorg/apache/lucene/util/fst/BytesStore;

    invoke-virtual {v0, v3}, Lorg/apache/lucene/util/fst/BytesStore;->writeByte(B)V

    .line 281
    invoke-virtual {p2}, Lorg/apache/lucene/util/fst/Outputs;->getNoOutput()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/util/fst/FST;->NO_OUTPUT:Ljava/lang/Object;

    .line 282
    if-eqz p3, :cond_0

    .line 283
    new-instance v0, Lorg/apache/lucene/util/packed/GrowableWriter;

    const/16 v1, 0xf

    invoke-direct {v0, v1, v4, p4}, Lorg/apache/lucene/util/packed/GrowableWriter;-><init>(IIF)V

    iput-object v0, p0, Lorg/apache/lucene/util/fst/FST;->nodeAddress:Lorg/apache/lucene/util/packed/GrowableWriter;

    .line 284
    new-instance v0, Lorg/apache/lucene/util/packed/GrowableWriter;

    const/4 v1, 0x1

    invoke-direct {v0, v1, v4, p4}, Lorg/apache/lucene/util/packed/GrowableWriter;-><init>(IIF)V

    iput-object v0, p0, Lorg/apache/lucene/util/fst/FST;->inCounts:Lorg/apache/lucene/util/packed/GrowableWriter;

    .line 290
    :goto_0
    iput-object v2, p0, Lorg/apache/lucene/util/fst/FST;->emptyOutput:Ljava/lang/Object;

    .line 291
    iput-boolean v3, p0, Lorg/apache/lucene/util/fst/FST;->packed:Z

    .line 292
    iput-object v2, p0, Lorg/apache/lucene/util/fst/FST;->nodeRefToAddress:Lorg/apache/lucene/util/packed/PackedInts$Reader;

    .line 293
    return-void

    .line 286
    :cond_0
    iput-object v2, p0, Lorg/apache/lucene/util/fst/FST;->nodeAddress:Lorg/apache/lucene/util/packed/GrowableWriter;

    .line 287
    iput-object v2, p0, Lorg/apache/lucene/util/fst/FST;->inCounts:Lorg/apache/lucene/util/packed/GrowableWriter;

    goto :goto_0
.end method

.method static synthetic access$0(II)Z
    .locals 1

    .prologue
    .line 257
    invoke-static {p0, p1}, Lorg/apache/lucene/util/fst/FST;->flag(II)Z

    move-result v0

    return v0
.end method

.method private cacheRootArcs()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 427
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    const/16 v2, 0x80

    new-array v2, v2, [Lorg/apache/lucene/util/fst/FST$Arc;

    iput-object v2, p0, Lorg/apache/lucene/util/fst/FST;->cachedRootArcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    .line 428
    new-instance v0, Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-direct {v0}, Lorg/apache/lucene/util/fst/FST$Arc;-><init>()V

    .line 429
    .local v0, "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    invoke-virtual {p0, v0}, Lorg/apache/lucene/util/fst/FST;->getFirstArc(Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 430
    invoke-virtual {p0}, Lorg/apache/lucene/util/fst/FST;->getBytesReader()Lorg/apache/lucene/util/fst/FST$BytesReader;

    move-result-object v1

    .line 431
    .local v1, "in":Lorg/apache/lucene/util/fst/FST$BytesReader;, "Lorg/apache/lucene/util/fst/FST$BytesReader;"
    invoke-static {v0}, Lorg/apache/lucene/util/fst/FST;->targetHasArcs(Lorg/apache/lucene/util/fst/FST$Arc;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 432
    iget-wide v2, v0, Lorg/apache/lucene/util/fst/FST$Arc;->target:J

    invoke-virtual {p0, v2, v3, v0, v1}, Lorg/apache/lucene/util/fst/FST;->readFirstRealTargetArc(JLorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 434
    :goto_0
    sget-boolean v2, Lorg/apache/lucene/util/fst/FST;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    iget v2, v0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 435
    :cond_0
    iget v2, v0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    iget-object v3, p0, Lorg/apache/lucene/util/fst/FST;->cachedRootArcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    array-length v3, v3

    if-ge v2, v3, :cond_1

    .line 436
    iget-object v2, p0, Lorg/apache/lucene/util/fst/FST;->cachedRootArcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    iget v3, v0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    new-instance v4, Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-direct {v4}, Lorg/apache/lucene/util/fst/FST$Arc;-><init>()V

    invoke-virtual {v4, v0}, Lorg/apache/lucene/util/fst/FST$Arc;->copyFrom(Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v4

    aput-object v4, v2, v3

    .line 440
    invoke-virtual {v0}, Lorg/apache/lucene/util/fst/FST$Arc;->isLast()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 446
    :cond_1
    return-void

    .line 443
    :cond_2
    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/util/fst/FST;->readNextRealArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    goto :goto_0
.end method

.method private static flag(II)Z
    .locals 1
    .param p0, "flags"    # I
    .param p1, "bit"    # I

    .prologue
    .line 258
    and-int v0, p0, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getNodeAddress(J)J
    .locals 3
    .param p1, "node"    # J

    .prologue
    .line 415
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/util/fst/FST;->nodeAddress:Lorg/apache/lucene/util/packed/GrowableWriter;

    if-eqz v0, :cond_0

    .line 417
    iget-object v0, p0, Lorg/apache/lucene/util/fst/FST;->nodeAddress:Lorg/apache/lucene/util/packed/GrowableWriter;

    long-to-int v1, p1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/packed/GrowableWriter;->get(I)J

    move-result-wide p1

    .line 420
    .end local p1    # "node":J
    :cond_0
    return-wide p1
.end method

.method public static read(Ljava/io/File;Lorg/apache/lucene/util/fst/Outputs;)Lorg/apache/lucene/util/fst/FST;
    .locals 6
    .param p0, "file"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/io/File;",
            "Lorg/apache/lucene/util/fst/Outputs",
            "<TT;>;)",
            "Lorg/apache/lucene/util/fst/FST",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p1, "outputs":Lorg/apache/lucene/util/fst/Outputs;, "Lorg/apache/lucene/util/fst/Outputs<TT;>;"
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 548
    new-instance v1, Ljava/io/BufferedInputStream;

    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v3}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 549
    .local v1, "is":Ljava/io/InputStream;
    const/4 v2, 0x0

    .line 551
    .local v2, "success":Z
    :try_start_0
    new-instance v0, Lorg/apache/lucene/util/fst/FST;

    new-instance v3, Lorg/apache/lucene/store/InputStreamDataInput;

    invoke-direct {v3, v1}, Lorg/apache/lucene/store/InputStreamDataInput;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v0, v3, p1}, Lorg/apache/lucene/util/fst/FST;-><init>(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/fst/Outputs;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 552
    .local v0, "fst":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    const/4 v2, 0x1

    .line 555
    if-eqz v2, :cond_0

    new-array v3, v4, [Ljava/io/Closeable;

    .line 556
    aput-object v1, v3, v5

    invoke-static {v3}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 553
    :goto_0
    return-object v0

    .line 557
    :cond_0
    new-array v3, v4, [Ljava/io/Closeable;

    .line 558
    aput-object v1, v3, v5

    invoke-static {v3}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto :goto_0

    .line 554
    .end local v0    # "fst":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    :catchall_0
    move-exception v3

    .line 555
    if-eqz v2, :cond_1

    new-array v4, v4, [Ljava/io/Closeable;

    .line 556
    aput-object v1, v4, v5

    invoke-static {v4}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 560
    :goto_1
    throw v3

    .line 557
    :cond_1
    new-array v4, v4, [Ljava/io/Closeable;

    .line 558
    aput-object v1, v4, v5

    invoke-static {v4}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto :goto_1
.end method

.method private readUnpackedNodeTarget(Lorg/apache/lucene/util/fst/FST$BytesReader;)J
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 874
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    .local p1, "in":Lorg/apache/lucene/util/fst/FST$BytesReader;, "Lorg/apache/lucene/util/fst/FST$BytesReader;"
    iget v2, p0, Lorg/apache/lucene/util/fst/FST;->version:I

    const/4 v3, 0x4

    if-ge v2, v3, :cond_0

    .line 875
    invoke-virtual {p1}, Lorg/apache/lucene/util/fst/FST$BytesReader;->readInt()I

    move-result v2

    int-to-long v0, v2

    .line 879
    .local v0, "target":J
    :goto_0
    return-wide v0

    .line 877
    .end local v0    # "target":J
    :cond_0
    invoke-virtual {p1}, Lorg/apache/lucene/util/fst/FST$BytesReader;->readVLong()J

    move-result-wide v0

    .restart local v0    # "target":J
    goto :goto_0
.end method

.method private seekToNextNode(Lorg/apache/lucene/util/fst/FST$BytesReader;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1200
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    .local p1, "in":Lorg/apache/lucene/util/fst/FST$BytesReader;, "Lorg/apache/lucene/util/fst/FST$BytesReader;"
    :cond_0
    invoke-virtual {p1}, Lorg/apache/lucene/util/fst/FST$BytesReader;->readByte()B

    move-result v0

    .line 1201
    .local v0, "flags":I
    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/fst/FST;->readLabel(Lorg/apache/lucene/store/DataInput;)I

    .line 1203
    const/16 v1, 0x10

    invoke-static {v0, v1}, Lorg/apache/lucene/util/fst/FST;->flag(II)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1204
    iget-object v1, p0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/util/fst/Outputs;->read(Lorg/apache/lucene/store/DataInput;)Ljava/lang/Object;

    .line 1207
    :cond_1
    const/16 v1, 0x20

    invoke-static {v0, v1}, Lorg/apache/lucene/util/fst/FST;->flag(II)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1208
    iget-object v1, p0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/util/fst/Outputs;->readFinalOutput(Lorg/apache/lucene/store/DataInput;)Ljava/lang/Object;

    .line 1211
    :cond_2
    const/16 v1, 0x8

    invoke-static {v0, v1}, Lorg/apache/lucene/util/fst/FST;->flag(II)Z

    move-result v1

    if-nez v1, :cond_3

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lorg/apache/lucene/util/fst/FST;->flag(II)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1212
    iget-boolean v1, p0, Lorg/apache/lucene/util/fst/FST;->packed:Z

    if-eqz v1, :cond_4

    .line 1213
    invoke-virtual {p1}, Lorg/apache/lucene/util/fst/FST$BytesReader;->readVLong()J

    .line 1219
    :cond_3
    :goto_0
    const/4 v1, 0x2

    invoke-static {v0, v1}, Lorg/apache/lucene/util/fst/FST;->flag(II)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1220
    return-void

    .line 1215
    :cond_4
    invoke-direct {p0, p1}, Lorg/apache/lucene/util/fst/FST;->readUnpackedNodeTarget(Lorg/apache/lucene/util/fst/FST$BytesReader;)J

    goto :goto_0
.end method

.method private shouldExpand(Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/Builder$UnCompiledNode",
            "<TT;>;)Z"
        }
    .end annotation

    .prologue
    .line 1254
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    .local p1, "node":Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;, "Lorg/apache/lucene/util/fst/Builder$UnCompiledNode<TT;>;"
    iget-boolean v0, p0, Lorg/apache/lucene/util/fst/FST;->allowArrayArcs:Z

    if-eqz v0, :cond_2

    .line 1255
    iget v0, p1, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->depth:I

    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    iget v0, p1, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    const/4 v1, 0x5

    if-ge v0, v1, :cond_1

    .line 1256
    :cond_0
    iget v0, p1, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    const/16 v1, 0xa

    .line 1254
    if-lt v0, v1, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static targetHasArcs(Lorg/apache/lucene/util/fst/FST$Arc;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;)Z"
        }
    .end annotation

    .prologue
    .line 593
    .local p0, "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    iget-wide v0, p0, Lorg/apache/lucene/util/fst/FST$Arc;->target:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private writeLabel(Lorg/apache/lucene/store/DataOutput;I)V
    .locals 3
    .param p1, "out"    # Lorg/apache/lucene/store/DataOutput;
    .param p2, "v"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 564
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    sget-boolean v0, Lorg/apache/lucene/util/fst/FST;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-gez p2, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "v="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 565
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/util/fst/FST;->inputType:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    sget-object v1, Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;->BYTE1:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    if-ne v0, v1, :cond_2

    .line 566
    sget-boolean v0, Lorg/apache/lucene/util/fst/FST;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    const/16 v0, 0xff

    if-le p2, v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "v="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 567
    :cond_1
    int-to-byte v0, p2

    invoke-virtual {p1, v0}, Lorg/apache/lucene/store/DataOutput;->writeByte(B)V

    .line 574
    :goto_0
    return-void

    .line 568
    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/util/fst/FST;->inputType:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    sget-object v1, Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;->BYTE2:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    if-ne v0, v1, :cond_4

    .line 569
    sget-boolean v0, Lorg/apache/lucene/util/fst/FST;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    const v0, 0xffff

    if-le p2, v0, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "v="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 570
    :cond_3
    int-to-short v0, p2

    invoke-virtual {p1, v0}, Lorg/apache/lucene/store/DataOutput;->writeShort(S)V

    goto :goto_0

    .line 572
    :cond_4
    invoke-virtual {p1, p2}, Lorg/apache/lucene/store/DataOutput;->writeVInt(I)V

    goto :goto_0
.end method


# virtual methods
.method addNode(Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;)J
    .locals 38
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/Builder$UnCompiledNode",
            "<TT;>;)J"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 601
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    .local p1, "nodeIn":Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;, "Lorg/apache/lucene/util/fst/Builder$UnCompiledNode<TT;>;"
    move-object/from16 v0, p1

    iget v5, v0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    if-nez v5, :cond_1

    .line 602
    move-object/from16 v0, p1

    iget-boolean v5, v0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->isFinal:Z

    if-eqz v5, :cond_0

    .line 603
    const-wide/16 v28, -0x1

    .line 786
    :goto_0
    return-wide v28

    .line 605
    :cond_0
    const-wide/16 v28, 0x0

    goto :goto_0

    .line 609
    :cond_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/util/fst/FST;->bytes:Lorg/apache/lucene/util/fst/BytesStore;

    invoke-virtual {v5}, Lorg/apache/lucene/util/fst/BytesStore;->getPosition()J

    move-result-wide v12

    .line 612
    .local v12, "startAddress":J
    invoke-direct/range {p0 .. p1}, Lorg/apache/lucene/util/fst/FST;->shouldExpand(Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;)Z

    move-result v20

    .line 613
    .local v20, "doFixedArray":Z
    if-eqz v20, :cond_2

    .line 615
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/util/fst/FST;->bytesPerArc:[I

    array-length v5, v5

    move-object/from16 v0, p1

    iget v10, v0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    if-ge v5, v10, :cond_2

    .line 616
    move-object/from16 v0, p1

    iget v5, v0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    const/4 v10, 0x1

    invoke-static {v5, v10}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v5

    new-array v5, v5, [I

    move-object/from16 v0, p0

    iput-object v5, v0, Lorg/apache/lucene/util/fst/FST;->bytesPerArc:[I

    .line 620
    :cond_2
    move-object/from16 v0, p0

    iget-wide v10, v0, Lorg/apache/lucene/util/fst/FST;->arcCount:J

    move-object/from16 v0, p1

    iget v5, v0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    int-to-long v0, v5

    move-wide/from16 v34, v0

    add-long v10, v10, v34

    move-object/from16 v0, p0

    iput-wide v10, v0, Lorg/apache/lucene/util/fst/FST;->arcCount:J

    .line 622
    move-object/from16 v0, p1

    iget v5, v0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    add-int/lit8 v24, v5, -0x1

    .line 624
    .local v24, "lastArc":I
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/util/fst/FST;->bytes:Lorg/apache/lucene/util/fst/BytesStore;

    invoke-virtual {v5}, Lorg/apache/lucene/util/fst/BytesStore;->getPosition()J

    move-result-wide v26

    .line 625
    .local v26, "lastArcStart":J
    const/16 v25, 0x0

    .line 626
    .local v25, "maxBytesPerArc":I
    const/16 v18, 0x0

    .local v18, "arcIdx":I
    :goto_1
    move-object/from16 v0, p1

    iget v5, v0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    move/from16 v0, v18

    if-lt v0, v5, :cond_3

    .line 717
    if-eqz v20, :cond_14

    .line 718
    const/16 v4, 0xb

    .line 719
    .local v4, "MAX_HEADER_SIZE":I
    sget-boolean v5, Lorg/apache/lucene/util/fst/FST;->$assertionsDisabled:Z

    if-nez v5, :cond_11

    if-gtz v25, :cond_11

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 627
    .end local v4    # "MAX_HEADER_SIZE":I
    :cond_3
    move-object/from16 v0, p1

    iget-object v5, v0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->arcs:[Lorg/apache/lucene/util/fst/Builder$Arc;

    aget-object v17, v5, v18

    .line 628
    .local v17, "arc":Lorg/apache/lucene/util/fst/Builder$Arc;, "Lorg/apache/lucene/util/fst/Builder$Arc<TT;>;"
    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/apache/lucene/util/fst/Builder$Arc;->target:Lorg/apache/lucene/util/fst/Builder$Node;

    move-object/from16 v30, v0

    check-cast v30, Lorg/apache/lucene/util/fst/Builder$CompiledNode;

    .line 629
    .local v30, "target":Lorg/apache/lucene/util/fst/Builder$CompiledNode;, "Lorg/apache/lucene/util/fst/Builder$CompiledNode;"
    const/16 v21, 0x0

    .line 632
    .local v21, "flags":I
    move/from16 v0, v18

    move/from16 v1, v24

    if-ne v0, v1, :cond_4

    .line 633
    add-int/lit8 v21, v21, 0x2

    .line 636
    :cond_4
    move-object/from16 v0, p0

    iget-wide v10, v0, Lorg/apache/lucene/util/fst/FST;->lastFrozenNode:J

    move-object/from16 v0, v30

    iget-wide v0, v0, Lorg/apache/lucene/util/fst/Builder$CompiledNode;->node:J

    move-wide/from16 v34, v0

    cmp-long v5, v10, v34

    if-nez v5, :cond_5

    if-nez v20, :cond_5

    .line 640
    add-int/lit8 v21, v21, 0x4

    .line 643
    :cond_5
    move-object/from16 v0, v17

    iget-boolean v5, v0, Lorg/apache/lucene/util/fst/Builder$Arc;->isFinal:Z

    if-eqz v5, :cond_b

    .line 644
    add-int/lit8 v21, v21, 0x1

    .line 645
    move-object/from16 v0, v17

    iget-object v5, v0, Lorg/apache/lucene/util/fst/Builder$Arc;->nextFinalOutput:Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/lucene/util/fst/FST;->NO_OUTPUT:Ljava/lang/Object;

    if-eq v5, v10, :cond_6

    .line 646
    add-int/lit8 v21, v21, 0x20

    .line 652
    :cond_6
    move-object/from16 v0, v30

    iget-wide v10, v0, Lorg/apache/lucene/util/fst/Builder$CompiledNode;->node:J

    const-wide/16 v34, 0x0

    cmp-long v5, v10, v34

    if-lez v5, :cond_c

    const/16 v31, 0x1

    .line 654
    .local v31, "targetHasArcs":Z
    :goto_2
    if-nez v31, :cond_d

    .line 655
    add-int/lit8 v21, v21, 0x8

    .line 660
    :cond_7
    :goto_3
    move-object/from16 v0, v17

    iget-object v5, v0, Lorg/apache/lucene/util/fst/Builder$Arc;->output:Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/lucene/util/fst/FST;->NO_OUTPUT:Ljava/lang/Object;

    if-eq v5, v10, :cond_8

    .line 661
    add-int/lit8 v21, v21, 0x10

    .line 664
    :cond_8
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/util/fst/FST;->bytes:Lorg/apache/lucene/util/fst/BytesStore;

    move/from16 v0, v21

    int-to-byte v10, v0

    invoke-virtual {v5, v10}, Lorg/apache/lucene/util/fst/BytesStore;->writeByte(B)V

    .line 665
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/util/fst/FST;->bytes:Lorg/apache/lucene/util/fst/BytesStore;

    move-object/from16 v0, v17

    iget v10, v0, Lorg/apache/lucene/util/fst/Builder$Arc;->label:I

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v10}, Lorg/apache/lucene/util/fst/FST;->writeLabel(Lorg/apache/lucene/store/DataOutput;I)V

    .line 669
    move-object/from16 v0, v17

    iget-object v5, v0, Lorg/apache/lucene/util/fst/Builder$Arc;->output:Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/lucene/util/fst/FST;->NO_OUTPUT:Ljava/lang/Object;

    if-eq v5, v10, :cond_9

    .line 670
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    move-object/from16 v0, v17

    iget-object v10, v0, Lorg/apache/lucene/util/fst/Builder$Arc;->output:Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/util/fst/FST;->bytes:Lorg/apache/lucene/util/fst/BytesStore;

    invoke-virtual {v5, v10, v11}, Lorg/apache/lucene/util/fst/Outputs;->write(Ljava/lang/Object;Lorg/apache/lucene/store/DataOutput;)V

    .line 672
    move-object/from16 v0, p0

    iget-wide v10, v0, Lorg/apache/lucene/util/fst/FST;->arcWithOutputCount:J

    const-wide/16 v34, 0x1

    add-long v10, v10, v34

    move-object/from16 v0, p0

    iput-wide v10, v0, Lorg/apache/lucene/util/fst/FST;->arcWithOutputCount:J

    .line 675
    :cond_9
    move-object/from16 v0, v17

    iget-object v5, v0, Lorg/apache/lucene/util/fst/Builder$Arc;->nextFinalOutput:Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/lucene/util/fst/FST;->NO_OUTPUT:Ljava/lang/Object;

    if-eq v5, v10, :cond_a

    .line 677
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    move-object/from16 v0, v17

    iget-object v10, v0, Lorg/apache/lucene/util/fst/Builder$Arc;->nextFinalOutput:Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/util/fst/FST;->bytes:Lorg/apache/lucene/util/fst/BytesStore;

    invoke-virtual {v5, v10, v11}, Lorg/apache/lucene/util/fst/Outputs;->writeFinalOutput(Ljava/lang/Object;Lorg/apache/lucene/store/DataOutput;)V

    .line 680
    :cond_a
    if-eqz v31, :cond_f

    and-int/lit8 v5, v21, 0x4

    if-nez v5, :cond_f

    .line 681
    sget-boolean v5, Lorg/apache/lucene/util/fst/FST;->$assertionsDisabled:Z

    if-nez v5, :cond_e

    move-object/from16 v0, v30

    iget-wide v10, v0, Lorg/apache/lucene/util/fst/Builder$CompiledNode;->node:J

    const-wide/16 v34, 0x0

    cmp-long v5, v10, v34

    if-gtz v5, :cond_e

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 649
    .end local v31    # "targetHasArcs":Z
    :cond_b
    sget-boolean v5, Lorg/apache/lucene/util/fst/FST;->$assertionsDisabled:Z

    if-nez v5, :cond_6

    move-object/from16 v0, v17

    iget-object v5, v0, Lorg/apache/lucene/util/fst/Builder$Arc;->nextFinalOutput:Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/lucene/util/fst/FST;->NO_OUTPUT:Ljava/lang/Object;

    if-eq v5, v10, :cond_6

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 652
    :cond_c
    const/16 v31, 0x0

    goto/16 :goto_2

    .line 656
    .restart local v31    # "targetHasArcs":Z
    :cond_d
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/util/fst/FST;->inCounts:Lorg/apache/lucene/util/packed/GrowableWriter;

    if-eqz v5, :cond_7

    .line 657
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/util/fst/FST;->inCounts:Lorg/apache/lucene/util/packed/GrowableWriter;

    move-object/from16 v0, v30

    iget-wide v10, v0, Lorg/apache/lucene/util/fst/Builder$CompiledNode;->node:J

    long-to-int v10, v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/util/fst/FST;->inCounts:Lorg/apache/lucene/util/packed/GrowableWriter;

    move-object/from16 v0, v30

    iget-wide v0, v0, Lorg/apache/lucene/util/fst/Builder$CompiledNode;->node:J

    move-wide/from16 v34, v0

    move-wide/from16 v0, v34

    long-to-int v15, v0

    invoke-virtual {v11, v15}, Lorg/apache/lucene/util/packed/GrowableWriter;->get(I)J

    move-result-wide v34

    const-wide/16 v36, 0x1

    add-long v34, v34, v36

    move-wide/from16 v0, v34

    invoke-virtual {v5, v10, v0, v1}, Lorg/apache/lucene/util/packed/GrowableWriter;->set(IJ)V

    goto/16 :goto_3

    .line 683
    :cond_e
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/util/fst/FST;->bytes:Lorg/apache/lucene/util/fst/BytesStore;

    move-object/from16 v0, v30

    iget-wide v10, v0, Lorg/apache/lucene/util/fst/Builder$CompiledNode;->node:J

    invoke-virtual {v5, v10, v11}, Lorg/apache/lucene/util/fst/BytesStore;->writeVLong(J)V

    .line 689
    :cond_f
    if-eqz v20, :cond_10

    .line 690
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/util/fst/FST;->bytesPerArc:[I

    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/lucene/util/fst/FST;->bytes:Lorg/apache/lucene/util/fst/BytesStore;

    invoke-virtual {v10}, Lorg/apache/lucene/util/fst/BytesStore;->getPosition()J

    move-result-wide v10

    sub-long v10, v10, v26

    long-to-int v10, v10

    aput v10, v5, v18

    .line 691
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/util/fst/FST;->bytes:Lorg/apache/lucene/util/fst/BytesStore;

    invoke-virtual {v5}, Lorg/apache/lucene/util/fst/BytesStore;->getPosition()J

    move-result-wide v26

    .line 692
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/util/fst/FST;->bytesPerArc:[I

    aget v5, v5, v18

    move/from16 v0, v25

    invoke-static {v0, v5}, Ljava/lang/Math;->max(II)I

    move-result v25

    .line 626
    :cond_10
    add-int/lit8 v18, v18, 0x1

    goto/16 :goto_1

    .line 726
    .end local v17    # "arc":Lorg/apache/lucene/util/fst/Builder$Arc;, "Lorg/apache/lucene/util/fst/Builder$Arc<TT;>;"
    .end local v21    # "flags":I
    .end local v30    # "target":Lorg/apache/lucene/util/fst/Builder$CompiledNode;, "Lorg/apache/lucene/util/fst/Builder$CompiledNode;"
    .end local v31    # "targetHasArcs":Z
    .restart local v4    # "MAX_HEADER_SIZE":I
    :cond_11
    const/16 v5, 0xb

    new-array v14, v5, [B

    .line 727
    .local v14, "header":[B
    new-instance v19, Lorg/apache/lucene/store/ByteArrayDataOutput;

    move-object/from16 v0, v19

    invoke-direct {v0, v14}, Lorg/apache/lucene/store/ByteArrayDataOutput;-><init>([B)V

    .line 729
    .local v19, "bad":Lorg/apache/lucene/store/ByteArrayDataOutput;
    const/16 v5, 0x20

    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Lorg/apache/lucene/store/ByteArrayDataOutput;->writeByte(B)V

    .line 730
    move-object/from16 v0, p1

    iget v5, v0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Lorg/apache/lucene/store/ByteArrayDataOutput;->writeVInt(I)V

    .line 731
    move-object/from16 v0, v19

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lorg/apache/lucene/store/ByteArrayDataOutput;->writeVInt(I)V

    .line 732
    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/store/ByteArrayDataOutput;->getPosition()I

    move-result v16

    .line 734
    .local v16, "headerLen":I
    move/from16 v0, v16

    int-to-long v10, v0

    add-long v22, v12, v10

    .line 737
    .local v22, "fixedArrayStart":J
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/util/fst/FST;->bytes:Lorg/apache/lucene/util/fst/BytesStore;

    invoke-virtual {v5}, Lorg/apache/lucene/util/fst/BytesStore;->getPosition()J

    move-result-wide v6

    .line 738
    .local v6, "srcPos":J
    move-object/from16 v0, p1

    iget v5, v0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    mul-int v5, v5, v25

    int-to-long v10, v5

    add-long v8, v22, v10

    .line 739
    .local v8, "destPos":J
    sget-boolean v5, Lorg/apache/lucene/util/fst/FST;->$assertionsDisabled:Z

    if-nez v5, :cond_12

    cmp-long v5, v8, v6

    if-gez v5, :cond_12

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 740
    :cond_12
    cmp-long v5, v8, v6

    if-lez v5, :cond_13

    .line 741
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/util/fst/FST;->bytes:Lorg/apache/lucene/util/fst/BytesStore;

    sub-long v10, v8, v6

    long-to-int v10, v10

    invoke-virtual {v5, v10}, Lorg/apache/lucene/util/fst/BytesStore;->skipBytes(I)V

    .line 742
    move-object/from16 v0, p1

    iget v5, v0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    add-int/lit8 v18, v5, -0x1

    :goto_4
    if-gez v18, :cond_15

    .line 755
    :cond_13
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/util/fst/FST;->bytes:Lorg/apache/lucene/util/fst/BytesStore;

    const/4 v15, 0x0

    invoke-virtual/range {v11 .. v16}, Lorg/apache/lucene/util/fst/BytesStore;->writeBytes(J[BII)V

    .line 758
    .end local v4    # "MAX_HEADER_SIZE":I
    .end local v6    # "srcPos":J
    .end local v8    # "destPos":J
    .end local v14    # "header":[B
    .end local v16    # "headerLen":I
    .end local v19    # "bad":Lorg/apache/lucene/store/ByteArrayDataOutput;
    .end local v22    # "fixedArrayStart":J
    :cond_14
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/util/fst/FST;->bytes:Lorg/apache/lucene/util/fst/BytesStore;

    invoke-virtual {v5}, Lorg/apache/lucene/util/fst/BytesStore;->getPosition()J

    move-result-wide v10

    const-wide/16 v34, 0x1

    sub-long v32, v10, v34

    .line 760
    .local v32, "thisNodeAddress":J
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/util/fst/FST;->bytes:Lorg/apache/lucene/util/fst/BytesStore;

    move-wide/from16 v0, v32

    invoke-virtual {v5, v12, v13, v0, v1}, Lorg/apache/lucene/util/fst/BytesStore;->reverse(JJ)V

    .line 764
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/util/fst/FST;->nodeAddress:Lorg/apache/lucene/util/packed/GrowableWriter;

    if-eqz v5, :cond_18

    move-object/from16 v0, p0

    iget-wide v10, v0, Lorg/apache/lucene/util/fst/FST;->nodeCount:J

    const-wide/32 v34, 0x7fffffff

    cmp-long v5, v10, v34

    if-nez v5, :cond_18

    .line 765
    new-instance v5, Ljava/lang/IllegalStateException;

    const-string v10, "cannot create a packed FST with more than 2.1 billion nodes"

    invoke-direct {v5, v10}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 743
    .end local v32    # "thisNodeAddress":J
    .restart local v4    # "MAX_HEADER_SIZE":I
    .restart local v6    # "srcPos":J
    .restart local v8    # "destPos":J
    .restart local v14    # "header":[B
    .restart local v16    # "headerLen":I
    .restart local v19    # "bad":Lorg/apache/lucene/store/ByteArrayDataOutput;
    .restart local v22    # "fixedArrayStart":J
    :cond_15
    move/from16 v0, v25

    int-to-long v10, v0

    sub-long/2addr v8, v10

    .line 744
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/util/fst/FST;->bytesPerArc:[I

    aget v5, v5, v18

    int-to-long v10, v5

    sub-long/2addr v6, v10

    .line 746
    cmp-long v5, v6, v8

    if-eqz v5, :cond_17

    .line 748
    sget-boolean v5, Lorg/apache/lucene/util/fst/FST;->$assertionsDisabled:Z

    if-nez v5, :cond_16

    cmp-long v5, v8, v6

    if-gtz v5, :cond_16

    new-instance v5, Ljava/lang/AssertionError;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "destPos="

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " srcPos="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " arcIdx="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v18

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " maxBytesPerArc="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v25

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " bytesPerArc[arcIdx]="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/util/fst/FST;->bytesPerArc:[I

    aget v11, v11, v18

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " nodeIn.numArcs="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, p1

    iget v11, v0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v5, v10}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v5

    .line 749
    :cond_16
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/util/fst/FST;->bytes:Lorg/apache/lucene/util/fst/BytesStore;

    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/lucene/util/fst/FST;->bytesPerArc:[I

    aget v10, v10, v18

    invoke-virtual/range {v5 .. v10}, Lorg/apache/lucene/util/fst/BytesStore;->copyBytes(JJI)V

    .line 742
    :cond_17
    add-int/lit8 v18, v18, -0x1

    goto/16 :goto_4

    .line 768
    .end local v4    # "MAX_HEADER_SIZE":I
    .end local v6    # "srcPos":J
    .end local v8    # "destPos":J
    .end local v14    # "header":[B
    .end local v16    # "headerLen":I
    .end local v19    # "bad":Lorg/apache/lucene/store/ByteArrayDataOutput;
    .end local v22    # "fixedArrayStart":J
    .restart local v32    # "thisNodeAddress":J
    :cond_18
    move-object/from16 v0, p0

    iget-wide v10, v0, Lorg/apache/lucene/util/fst/FST;->nodeCount:J

    const-wide/16 v34, 0x1

    add-long v10, v10, v34

    move-object/from16 v0, p0

    iput-wide v10, v0, Lorg/apache/lucene/util/fst/FST;->nodeCount:J

    .line 770
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/util/fst/FST;->nodeAddress:Lorg/apache/lucene/util/packed/GrowableWriter;

    if-eqz v5, :cond_1a

    .line 773
    move-object/from16 v0, p0

    iget-wide v10, v0, Lorg/apache/lucene/util/fst/FST;->nodeCount:J

    long-to-int v5, v10

    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/lucene/util/fst/FST;->nodeAddress:Lorg/apache/lucene/util/packed/GrowableWriter;

    invoke-virtual {v10}, Lorg/apache/lucene/util/packed/GrowableWriter;->size()I

    move-result v10

    if-ne v5, v10, :cond_19

    .line 774
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/util/fst/FST;->nodeAddress:Lorg/apache/lucene/util/packed/GrowableWriter;

    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/lucene/util/fst/FST;->nodeAddress:Lorg/apache/lucene/util/packed/GrowableWriter;

    invoke-virtual {v10}, Lorg/apache/lucene/util/packed/GrowableWriter;->size()I

    move-result v10

    add-int/lit8 v10, v10, 0x1

    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/util/fst/FST;->nodeAddress:Lorg/apache/lucene/util/packed/GrowableWriter;

    invoke-virtual {v11}, Lorg/apache/lucene/util/packed/GrowableWriter;->getBitsPerValue()I

    move-result v11

    invoke-static {v10, v11}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v10

    invoke-virtual {v5, v10}, Lorg/apache/lucene/util/packed/GrowableWriter;->resize(I)Lorg/apache/lucene/util/packed/GrowableWriter;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lorg/apache/lucene/util/fst/FST;->nodeAddress:Lorg/apache/lucene/util/packed/GrowableWriter;

    .line 775
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/util/fst/FST;->inCounts:Lorg/apache/lucene/util/packed/GrowableWriter;

    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/lucene/util/fst/FST;->inCounts:Lorg/apache/lucene/util/packed/GrowableWriter;

    invoke-virtual {v10}, Lorg/apache/lucene/util/packed/GrowableWriter;->size()I

    move-result v10

    add-int/lit8 v10, v10, 0x1

    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/util/fst/FST;->inCounts:Lorg/apache/lucene/util/packed/GrowableWriter;

    invoke-virtual {v11}, Lorg/apache/lucene/util/packed/GrowableWriter;->getBitsPerValue()I

    move-result v11

    invoke-static {v10, v11}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v10

    invoke-virtual {v5, v10}, Lorg/apache/lucene/util/packed/GrowableWriter;->resize(I)Lorg/apache/lucene/util/packed/GrowableWriter;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lorg/apache/lucene/util/fst/FST;->inCounts:Lorg/apache/lucene/util/packed/GrowableWriter;

    .line 777
    :cond_19
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/util/fst/FST;->nodeAddress:Lorg/apache/lucene/util/packed/GrowableWriter;

    move-object/from16 v0, p0

    iget-wide v10, v0, Lorg/apache/lucene/util/fst/FST;->nodeCount:J

    long-to-int v10, v10

    move-wide/from16 v0, v32

    invoke-virtual {v5, v10, v0, v1}, Lorg/apache/lucene/util/packed/GrowableWriter;->set(IJ)V

    .line 779
    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/util/fst/FST;->nodeCount:J

    move-wide/from16 v28, v0

    .line 783
    .local v28, "node":J
    :goto_5
    move-wide/from16 v0, v28

    move-object/from16 v2, p0

    iput-wide v0, v2, Lorg/apache/lucene/util/fst/FST;->lastFrozenNode:J

    goto/16 :goto_0

    .line 781
    .end local v28    # "node":J
    :cond_1a
    move-wide/from16 v28, v32

    .restart local v28    # "node":J
    goto :goto_5
.end method

.method public findTargetArc(ILorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;
    .locals 11
    .param p1, "labelToMatch"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;",
            "Lorg/apache/lucene/util/fst/FST$BytesReader;",
            ")",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    .local p2, "follow":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    .local p3, "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    .local p4, "in":Lorg/apache/lucene/util/fst/FST$BytesReader;, "Lorg/apache/lucene/util/fst/FST$BytesReader;"
    const/4 v10, -0x1

    const/4 v5, 0x0

    .line 1102
    sget-boolean v6, Lorg/apache/lucene/util/fst/FST;->$assertionsDisabled:Z

    if-nez v6, :cond_0

    iget-object v6, p0, Lorg/apache/lucene/util/fst/FST;->cachedRootArcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    if-nez v6, :cond_0

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 1104
    :cond_0
    if-ne p1, v10, :cond_4

    .line 1105
    invoke-virtual {p2}, Lorg/apache/lucene/util/fst/FST$Arc;->isFinal()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1106
    iget-wide v6, p2, Lorg/apache/lucene/util/fst/FST$Arc;->target:J

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-gtz v6, :cond_2

    .line 1107
    const/4 v6, 0x2

    iput-byte v6, p3, Lorg/apache/lucene/util/fst/FST$Arc;->flags:B

    .line 1114
    :goto_0
    iget-object v6, p2, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    iput-object v6, p3, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    .line 1115
    iput v10, p3, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    .line 1189
    .end local p3    # "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    :cond_1
    :goto_1
    return-object p3

    .line 1109
    .restart local p3    # "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    :cond_2
    const/4 v6, 0x0

    iput-byte v6, p3, Lorg/apache/lucene/util/fst/FST$Arc;->flags:B

    .line 1111
    iget-wide v6, p2, Lorg/apache/lucene/util/fst/FST$Arc;->target:J

    iput-wide v6, p3, Lorg/apache/lucene/util/fst/FST$Arc;->nextArc:J

    .line 1112
    iget-wide v6, p2, Lorg/apache/lucene/util/fst/FST$Arc;->target:J

    iput-wide v6, p3, Lorg/apache/lucene/util/fst/FST$Arc;->node:J

    goto :goto_0

    :cond_3
    move-object p3, v5

    .line 1118
    goto :goto_1

    .line 1123
    :cond_4
    iget-wide v6, p2, Lorg/apache/lucene/util/fst/FST$Arc;->target:J

    iget-wide v8, p0, Lorg/apache/lucene/util/fst/FST;->startNode:J

    cmp-long v6, v6, v8

    if-nez v6, :cond_6

    iget-object v6, p0, Lorg/apache/lucene/util/fst/FST;->cachedRootArcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    array-length v6, v6

    if-ge p1, v6, :cond_6

    .line 1124
    iget-object v6, p0, Lorg/apache/lucene/util/fst/FST;->cachedRootArcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    aget-object v5, v6, p1

    .line 1125
    .local v5, "result":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    if-nez v5, :cond_5

    move-object p3, v5

    .line 1126
    goto :goto_1

    .line 1128
    :cond_5
    invoke-virtual {p3, v5}, Lorg/apache/lucene/util/fst/FST$Arc;->copyFrom(Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    goto :goto_1

    .line 1133
    .end local v5    # "result":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    :cond_6
    invoke-static {p2}, Lorg/apache/lucene/util/fst/FST;->targetHasArcs(Lorg/apache/lucene/util/fst/FST$Arc;)Z

    move-result v6

    if-nez v6, :cond_7

    move-object p3, v5

    .line 1134
    goto :goto_1

    .line 1137
    :cond_7
    iget-wide v6, p2, Lorg/apache/lucene/util/fst/FST$Arc;->target:J

    invoke-direct {p0, v6, v7}, Lorg/apache/lucene/util/fst/FST;->getNodeAddress(J)J

    move-result-wide v6

    invoke-virtual {p4, v6, v7}, Lorg/apache/lucene/util/fst/FST$BytesReader;->setPosition(J)V

    .line 1139
    iget-wide v6, p2, Lorg/apache/lucene/util/fst/FST$Arc;->target:J

    iput-wide v6, p3, Lorg/apache/lucene/util/fst/FST$Arc;->node:J

    .line 1143
    invoke-virtual {p4}, Lorg/apache/lucene/util/fst/FST$BytesReader;->readByte()B

    move-result v6

    const/16 v7, 0x20

    if-ne v6, v7, :cond_d

    .line 1145
    invoke-virtual {p4}, Lorg/apache/lucene/util/fst/FST$BytesReader;->readVInt()I

    move-result v6

    iput v6, p3, Lorg/apache/lucene/util/fst/FST$Arc;->numArcs:I

    .line 1146
    iget-boolean v6, p0, Lorg/apache/lucene/util/fst/FST;->packed:Z

    if-nez v6, :cond_8

    iget v6, p0, Lorg/apache/lucene/util/fst/FST;->version:I

    const/4 v7, 0x4

    if-lt v6, v7, :cond_9

    .line 1147
    :cond_8
    invoke-virtual {p4}, Lorg/apache/lucene/util/fst/FST$BytesReader;->readVInt()I

    move-result v6

    iput v6, p3, Lorg/apache/lucene/util/fst/FST$Arc;->bytesPerArc:I

    .line 1151
    :goto_2
    invoke-virtual {p4}, Lorg/apache/lucene/util/fst/FST$BytesReader;->getPosition()J

    move-result-wide v6

    iput-wide v6, p3, Lorg/apache/lucene/util/fst/FST$Arc;->posArcsStart:J

    .line 1152
    const/4 v2, 0x0

    .line 1153
    .local v2, "low":I
    iget v6, p3, Lorg/apache/lucene/util/fst/FST$Arc;->numArcs:I

    add-int/lit8 v1, v6, -0x1

    .line 1154
    .local v1, "high":I
    :goto_3
    if-le v2, v1, :cond_a

    move-object p3, v5

    .line 1172
    goto :goto_1

    .line 1149
    .end local v1    # "high":I
    .end local v2    # "low":I
    :cond_9
    invoke-virtual {p4}, Lorg/apache/lucene/util/fst/FST$BytesReader;->readInt()I

    move-result v6

    iput v6, p3, Lorg/apache/lucene/util/fst/FST$Arc;->bytesPerArc:I

    goto :goto_2

    .line 1156
    .restart local v1    # "high":I
    .restart local v2    # "low":I
    :cond_a
    add-int v6, v2, v1

    ushr-int/lit8 v3, v6, 0x1

    .line 1157
    .local v3, "mid":I
    iget-wide v6, p3, Lorg/apache/lucene/util/fst/FST$Arc;->posArcsStart:J

    invoke-virtual {p4, v6, v7}, Lorg/apache/lucene/util/fst/FST$BytesReader;->setPosition(J)V

    .line 1158
    iget v6, p3, Lorg/apache/lucene/util/fst/FST$Arc;->bytesPerArc:I

    mul-int/2addr v6, v3

    add-int/lit8 v6, v6, 0x1

    invoke-virtual {p4, v6}, Lorg/apache/lucene/util/fst/FST$BytesReader;->skipBytes(I)V

    .line 1159
    invoke-virtual {p0, p4}, Lorg/apache/lucene/util/fst/FST;->readLabel(Lorg/apache/lucene/store/DataInput;)I

    move-result v4

    .line 1160
    .local v4, "midLabel":I
    sub-int v0, v4, p1

    .line 1161
    .local v0, "cmp":I
    if-gez v0, :cond_b

    .line 1162
    add-int/lit8 v2, v3, 0x1

    .line 1163
    goto :goto_3

    :cond_b
    if-lez v0, :cond_c

    .line 1164
    add-int/lit8 v1, v3, -0x1

    .line 1165
    goto :goto_3

    .line 1166
    :cond_c
    add-int/lit8 v6, v3, -0x1

    iput v6, p3, Lorg/apache/lucene/util/fst/FST$Arc;->arcIdx:I

    .line 1168
    invoke-virtual {p0, p3, p4}, Lorg/apache/lucene/util/fst/FST;->readNextRealArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object p3

    goto/16 :goto_1

    .line 1176
    .end local v0    # "cmp":I
    .end local v1    # "high":I
    .end local v2    # "low":I
    .end local v3    # "mid":I
    .end local v4    # "midLabel":I
    :cond_d
    iget-wide v6, p2, Lorg/apache/lucene/util/fst/FST$Arc;->target:J

    invoke-virtual {p0, v6, v7, p3, p4}, Lorg/apache/lucene/util/fst/FST;->readFirstRealTargetArc(JLorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 1183
    :goto_4
    iget v6, p3, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    if-eq v6, p1, :cond_1

    .line 1186
    iget v6, p3, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    if-le v6, p1, :cond_e

    move-object p3, v5

    .line 1187
    goto/16 :goto_1

    .line 1188
    :cond_e
    invoke-virtual {p3}, Lorg/apache/lucene/util/fst/FST$Arc;->isLast()Z

    move-result v6

    if-eqz v6, :cond_f

    move-object p3, v5

    .line 1189
    goto/16 :goto_1

    .line 1191
    :cond_f
    invoke-virtual {p0, p3, p4}, Lorg/apache/lucene/util/fst/FST;->readNextRealArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    goto :goto_4
.end method

.method finish(J)V
    .locals 5
    .param p1, "startNode"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    const-wide/16 v2, -0x1

    .line 402
    iget-wide v0, p0, Lorg/apache/lucene/util/fst/FST;->startNode:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 403
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "already finished"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 405
    :cond_0
    cmp-long v0, p1, v2

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/util/fst/FST;->emptyOutput:Ljava/lang/Object;

    if-eqz v0, :cond_1

    .line 406
    const-wide/16 p1, 0x0

    .line 408
    :cond_1
    iput-wide p1, p0, Lorg/apache/lucene/util/fst/FST;->startNode:J

    .line 409
    iget-object v0, p0, Lorg/apache/lucene/util/fst/FST;->bytes:Lorg/apache/lucene/util/fst/BytesStore;

    invoke-virtual {v0}, Lorg/apache/lucene/util/fst/BytesStore;->finish()V

    .line 411
    invoke-direct {p0}, Lorg/apache/lucene/util/fst/FST;->cacheRootArcs()V

    .line 412
    return-void
.end method

.method public getArcCount()J
    .locals 2

    .prologue
    .line 1231
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    iget-wide v0, p0, Lorg/apache/lucene/util/fst/FST;->arcCount:J

    return-wide v0
.end method

.method public getArcWithOutputCount()J
    .locals 2

    .prologue
    .line 1235
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    iget-wide v0, p0, Lorg/apache/lucene/util/fst/FST;->arcWithOutputCount:J

    return-wide v0
.end method

.method public getBytesReader()Lorg/apache/lucene/util/fst/FST$BytesReader;
    .locals 2

    .prologue
    .line 1263
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    iget-boolean v1, p0, Lorg/apache/lucene/util/fst/FST;->packed:Z

    if-eqz v1, :cond_0

    .line 1264
    iget-object v1, p0, Lorg/apache/lucene/util/fst/FST;->bytes:Lorg/apache/lucene/util/fst/BytesStore;

    invoke-virtual {v1}, Lorg/apache/lucene/util/fst/BytesStore;->getForwardReader()Lorg/apache/lucene/util/fst/FST$BytesReader;

    move-result-object v0

    .line 1268
    .local v0, "in":Lorg/apache/lucene/util/fst/FST$BytesReader;, "Lorg/apache/lucene/util/fst/FST$BytesReader;"
    :goto_0
    return-object v0

    .line 1266
    .end local v0    # "in":Lorg/apache/lucene/util/fst/FST$BytesReader;, "Lorg/apache/lucene/util/fst/FST$BytesReader;"
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/util/fst/FST;->bytes:Lorg/apache/lucene/util/fst/BytesStore;

    invoke-virtual {v1}, Lorg/apache/lucene/util/fst/BytesStore;->getReverseReader()Lorg/apache/lucene/util/fst/FST$BytesReader;

    move-result-object v0

    .restart local v0    # "in":Lorg/apache/lucene/util/fst/FST$BytesReader;, "Lorg/apache/lucene/util/fst/FST$BytesReader;"
    goto :goto_0
.end method

.method public getEmptyOutput()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 449
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/util/fst/FST;->emptyOutput:Ljava/lang/Object;

    return-object v0
.end method

.method public getFirstArc(Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;)",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 793
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    .local p1, "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/util/fst/FST;->emptyOutput:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 794
    const/4 v0, 0x3

    iput-byte v0, p1, Lorg/apache/lucene/util/fst/FST$Arc;->flags:B

    .line 795
    iget-object v0, p0, Lorg/apache/lucene/util/fst/FST;->emptyOutput:Ljava/lang/Object;

    iput-object v0, p1, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    .line 800
    :goto_0
    iget-object v0, p0, Lorg/apache/lucene/util/fst/FST;->NO_OUTPUT:Ljava/lang/Object;

    iput-object v0, p1, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    .line 804
    iget-wide v0, p0, Lorg/apache/lucene/util/fst/FST;->startNode:J

    iput-wide v0, p1, Lorg/apache/lucene/util/fst/FST$Arc;->target:J

    .line 805
    return-object p1

    .line 797
    :cond_0
    const/4 v0, 0x2

    iput-byte v0, p1, Lorg/apache/lucene/util/fst/FST$Arc;->flags:B

    .line 798
    iget-object v0, p0, Lorg/apache/lucene/util/fst/FST;->NO_OUTPUT:Ljava/lang/Object;

    iput-object v0, p1, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    goto :goto_0
.end method

.method public getInputType()Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;
    .locals 1

    .prologue
    .line 386
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/util/fst/FST;->inputType:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    return-object v0
.end method

.method public getNodeCount()J
    .locals 4

    .prologue
    .line 1227
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    const-wide/16 v0, 0x1

    iget-wide v2, p0, Lorg/apache/lucene/util/fst/FST;->nodeCount:J

    add-long/2addr v0, v2

    return-wide v0
.end method

.method isExpandedTarget(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;",
            "Lorg/apache/lucene/util/fst/FST$BytesReader;",
            ")Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    .local p1, "follow":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    .local p2, "in":Lorg/apache/lucene/util/fst/FST$BytesReader;, "Lorg/apache/lucene/util/fst/FST$BytesReader;"
    const/4 v0, 0x0

    .line 948
    invoke-static {p1}, Lorg/apache/lucene/util/fst/FST;->targetHasArcs(Lorg/apache/lucene/util/fst/FST$Arc;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 952
    :cond_0
    :goto_0
    return v0

    .line 951
    :cond_1
    iget-wide v2, p1, Lorg/apache/lucene/util/fst/FST$Arc;->target:J

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/util/fst/FST;->getNodeAddress(J)J

    move-result-wide v2

    invoke-virtual {p2, v2, v3}, Lorg/apache/lucene/util/fst/FST$BytesReader;->setPosition(J)V

    .line 952
    invoke-virtual {p2}, Lorg/apache/lucene/util/fst/FST$BytesReader;->readByte()B

    move-result v1

    const/16 v2, 0x20

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method pack(IIF)Lorg/apache/lucene/util/fst/FST;
    .locals 54
    .param p1, "minInCountDeref"    # I
    .param p2, "maxDerefNodes"    # I
    .param p3, "acceptableOverheadRatio"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIF)",
            "Lorg/apache/lucene/util/fst/FST",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1448
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->nodeAddress:Lorg/apache/lucene/util/packed/GrowableWriter;

    move-object/from16 v49, v0

    if-nez v49, :cond_0

    .line 1449
    new-instance v49, Ljava/lang/IllegalArgumentException;

    const-string v50, "this FST was not built with willPackFST=true"

    invoke-direct/range {v49 .. v50}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v49

    .line 1452
    :cond_0
    new-instance v12, Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-direct {v12}, Lorg/apache/lucene/util/fst/FST$Arc;-><init>()V

    .line 1454
    .local v12, "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/util/fst/FST;->getBytesReader()Lorg/apache/lucene/util/fst/FST$BytesReader;

    move-result-object v42

    .line 1456
    .local v42, "r":Lorg/apache/lucene/util/fst/FST$BytesReader;, "Lorg/apache/lucene/util/fst/FST$BytesReader;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->inCounts:Lorg/apache/lucene/util/packed/GrowableWriter;

    move-object/from16 v49, v0

    invoke-virtual/range {v49 .. v49}, Lorg/apache/lucene/util/packed/GrowableWriter;->size()I

    move-result v49

    move/from16 v0, p2

    move/from16 v1, v49

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v45

    .line 1459
    .local v45, "topN":I
    new-instance v41, Lorg/apache/lucene/util/fst/FST$NodeQueue;

    move-object/from16 v0, v41

    move/from16 v1, v45

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/fst/FST$NodeQueue;-><init>(I)V

    .line 1462
    .local v41, "q":Lorg/apache/lucene/util/fst/FST$NodeQueue;, "Lorg/apache/lucene/util/fst/FST$NodeQueue;"
    const/16 v16, 0x0

    .line 1463
    .local v16, "bottom":Lorg/apache/lucene/util/fst/FST$NodeAndInCount;, "Lorg/apache/lucene/util/fst/FST$NodeAndInCount;"
    const/16 v37, 0x0

    .local v37, "node":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->inCounts:Lorg/apache/lucene/util/packed/GrowableWriter;

    move-object/from16 v49, v0

    invoke-virtual/range {v49 .. v49}, Lorg/apache/lucene/util/packed/GrowableWriter;->size()I

    move-result v49

    move/from16 v0, v37

    move/from16 v1, v49

    if-lt v0, v1, :cond_2

    .line 1477
    const/16 v49, 0x0

    move-object/from16 v0, v49

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/util/fst/FST;->inCounts:Lorg/apache/lucene/util/packed/GrowableWriter;

    .line 1479
    new-instance v46, Ljava/util/HashMap;

    invoke-direct/range {v46 .. v46}, Ljava/util/HashMap;-><init>()V

    .line 1480
    .local v46, "topNodeMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    invoke-virtual/range {v41 .. v41}, Lorg/apache/lucene/util/fst/FST$NodeQueue;->size()I

    move-result v49

    add-int/lit8 v24, v49, -0x1

    .local v24, "downTo":I
    :goto_1
    if-gez v24, :cond_5

    .line 1487
    new-instance v35, Lorg/apache/lucene/util/packed/GrowableWriter;

    .line 1488
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->bytes:Lorg/apache/lucene/util/fst/BytesStore;

    move-object/from16 v49, v0

    invoke-virtual/range {v49 .. v49}, Lorg/apache/lucene/util/fst/BytesStore;->getPosition()J

    move-result-wide v50

    invoke-static/range {v50 .. v51}, Lorg/apache/lucene/util/packed/PackedInts;->bitsRequired(J)I

    move-result v49

    const-wide/16 v50, 0x1

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/util/fst/FST;->nodeCount:J

    move-wide/from16 v52, v0

    add-long v50, v50, v52

    move-wide/from16 v0, v50

    long-to-int v0, v0

    move/from16 v50, v0

    .line 1487
    move-object/from16 v0, v35

    move/from16 v1, v49

    move/from16 v2, v50

    move/from16 v3, p3

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/util/packed/GrowableWriter;-><init>(IIF)V

    .line 1491
    .local v35, "newNodeAddress":Lorg/apache/lucene/util/packed/GrowableWriter;
    const/16 v37, 0x1

    :goto_2
    move/from16 v0, v37

    int-to-long v0, v0

    move-wide/from16 v50, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/util/fst/FST;->nodeCount:J

    move-wide/from16 v52, v0

    cmp-long v49, v50, v52

    if-lez v49, :cond_6

    .line 1506
    :cond_1
    const/16 v18, 0x0

    .line 1509
    .local v18, "changed":Z
    const/16 v34, 0x0

    .line 1511
    .local v34, "negDelta":Z
    new-instance v27, Lorg/apache/lucene/util/fst/FST;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->inputType:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    move-object/from16 v49, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    move-object/from16 v50, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->bytes:Lorg/apache/lucene/util/fst/BytesStore;

    move-object/from16 v51, v0

    invoke-virtual/range {v51 .. v51}, Lorg/apache/lucene/util/fst/BytesStore;->getBlockBits()I

    move-result v51

    move-object/from16 v0, v27

    move-object/from16 v1, v49

    move-object/from16 v2, v50

    move/from16 v3, v51

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/util/fst/FST;-><init>(Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;Lorg/apache/lucene/util/fst/Outputs;I)V

    .line 1513
    .local v27, "fst":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    move-object/from16 v0, v27

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->bytes:Lorg/apache/lucene/util/fst/BytesStore;

    move-object/from16 v48, v0

    .line 1516
    .local v48, "writer":Lorg/apache/lucene/util/fst/BytesStore;
    const/16 v49, 0x0

    invoke-virtual/range {v48 .. v49}, Lorg/apache/lucene/util/fst/BytesStore;->writeByte(B)V

    .line 1518
    const-wide/16 v50, 0x0

    move-wide/from16 v0, v50

    move-object/from16 v2, v27

    iput-wide v0, v2, Lorg/apache/lucene/util/fst/FST;->arcWithOutputCount:J

    .line 1519
    const-wide/16 v50, 0x0

    move-wide/from16 v0, v50

    move-object/from16 v2, v27

    iput-wide v0, v2, Lorg/apache/lucene/util/fst/FST;->nodeCount:J

    .line 1520
    const-wide/16 v50, 0x0

    move-wide/from16 v0, v50

    move-object/from16 v2, v27

    iput-wide v0, v2, Lorg/apache/lucene/util/fst/FST;->arcCount:J

    .line 1522
    const/16 v36, 0x0

    .local v36, "nextCount":I
    move/from16 v44, v36

    .local v44, "topCount":I
    move/from16 v22, v36

    .local v22, "deltaCount":I
    move/from16 v4, v36

    .line 1524
    .local v4, "absCount":I
    const/16 v19, 0x0

    .line 1526
    .local v19, "changedCount":I
    const-wide/16 v10, 0x0

    .line 1533
    .local v10, "addressError":J
    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/util/fst/FST;->nodeCount:J

    move-wide/from16 v50, v0

    move-wide/from16 v0, v50

    long-to-int v0, v0

    move/from16 v37, v0

    :goto_3
    const/16 v49, 0x1

    move/from16 v0, v37

    move/from16 v1, v49

    if-ge v0, v1, :cond_7

    .line 1735
    if-nez v18, :cond_1

    .line 1740
    sget-boolean v49, Lorg/apache/lucene/util/fst/FST;->$assertionsDisabled:Z

    if-nez v49, :cond_22

    if-eqz v34, :cond_22

    new-instance v49, Ljava/lang/AssertionError;

    invoke-direct/range {v49 .. v49}, Ljava/lang/AssertionError;-><init>()V

    throw v49

    .line 1464
    .end local v4    # "absCount":I
    .end local v10    # "addressError":J
    .end local v18    # "changed":Z
    .end local v19    # "changedCount":I
    .end local v22    # "deltaCount":I
    .end local v24    # "downTo":I
    .end local v27    # "fst":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    .end local v34    # "negDelta":Z
    .end local v35    # "newNodeAddress":Lorg/apache/lucene/util/packed/GrowableWriter;
    .end local v36    # "nextCount":I
    .end local v44    # "topCount":I
    .end local v46    # "topNodeMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    .end local v48    # "writer":Lorg/apache/lucene/util/fst/BytesStore;
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->inCounts:Lorg/apache/lucene/util/packed/GrowableWriter;

    move-object/from16 v49, v0

    move-object/from16 v0, v49

    move/from16 v1, v37

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/packed/GrowableWriter;->get(I)J

    move-result-wide v50

    move/from16 v0, p1

    int-to-long v0, v0

    move-wide/from16 v52, v0

    cmp-long v49, v50, v52

    if-ltz v49, :cond_3

    .line 1465
    if-nez v16, :cond_4

    .line 1466
    new-instance v49, Lorg/apache/lucene/util/fst/FST$NodeAndInCount;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->inCounts:Lorg/apache/lucene/util/packed/GrowableWriter;

    move-object/from16 v50, v0

    move-object/from16 v0, v50

    move/from16 v1, v37

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/packed/GrowableWriter;->get(I)J

    move-result-wide v50

    move-wide/from16 v0, v50

    long-to-int v0, v0

    move/from16 v50, v0

    move-object/from16 v0, v49

    move/from16 v1, v37

    move/from16 v2, v50

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/util/fst/FST$NodeAndInCount;-><init>(II)V

    move-object/from16 v0, v41

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/fst/FST$NodeQueue;->add(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1467
    invoke-virtual/range {v41 .. v41}, Lorg/apache/lucene/util/fst/FST$NodeQueue;->size()I

    move-result v49

    move/from16 v0, v49

    move/from16 v1, v45

    if-ne v0, v1, :cond_3

    .line 1468
    invoke-virtual/range {v41 .. v41}, Lorg/apache/lucene/util/fst/FST$NodeQueue;->top()Ljava/lang/Object;

    move-result-object v16

    .end local v16    # "bottom":Lorg/apache/lucene/util/fst/FST$NodeAndInCount;, "Lorg/apache/lucene/util/fst/FST$NodeAndInCount;"
    check-cast v16, Lorg/apache/lucene/util/fst/FST$NodeAndInCount;

    .line 1463
    .restart local v16    # "bottom":Lorg/apache/lucene/util/fst/FST$NodeAndInCount;, "Lorg/apache/lucene/util/fst/FST$NodeAndInCount;"
    :cond_3
    :goto_4
    add-int/lit8 v37, v37, 0x1

    goto/16 :goto_0

    .line 1470
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->inCounts:Lorg/apache/lucene/util/packed/GrowableWriter;

    move-object/from16 v49, v0

    move-object/from16 v0, v49

    move/from16 v1, v37

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/packed/GrowableWriter;->get(I)J

    move-result-wide v50

    move-object/from16 v0, v16

    iget v0, v0, Lorg/apache/lucene/util/fst/FST$NodeAndInCount;->count:I

    move/from16 v49, v0

    move/from16 v0, v49

    int-to-long v0, v0

    move-wide/from16 v52, v0

    cmp-long v49, v50, v52

    if-lez v49, :cond_3

    .line 1471
    new-instance v49, Lorg/apache/lucene/util/fst/FST$NodeAndInCount;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->inCounts:Lorg/apache/lucene/util/packed/GrowableWriter;

    move-object/from16 v50, v0

    move-object/from16 v0, v50

    move/from16 v1, v37

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/packed/GrowableWriter;->get(I)J

    move-result-wide v50

    move-wide/from16 v0, v50

    long-to-int v0, v0

    move/from16 v50, v0

    move-object/from16 v0, v49

    move/from16 v1, v37

    move/from16 v2, v50

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/util/fst/FST$NodeAndInCount;-><init>(II)V

    move-object/from16 v0, v41

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/fst/FST$NodeQueue;->insertWithOverflow(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    .line 1481
    .restart local v24    # "downTo":I
    .restart local v46    # "topNodeMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :cond_5
    invoke-virtual/range {v41 .. v41}, Lorg/apache/lucene/util/fst/FST$NodeQueue;->pop()Ljava/lang/Object;

    move-result-object v33

    check-cast v33, Lorg/apache/lucene/util/fst/FST$NodeAndInCount;

    .line 1482
    .local v33, "n":Lorg/apache/lucene/util/fst/FST$NodeAndInCount;, "Lorg/apache/lucene/util/fst/FST$NodeAndInCount;"
    move-object/from16 v0, v33

    iget v0, v0, Lorg/apache/lucene/util/fst/FST$NodeAndInCount;->node:I

    move/from16 v49, v0

    invoke-static/range {v49 .. v49}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v49

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v50

    move-object/from16 v0, v46

    move-object/from16 v1, v49

    move-object/from16 v2, v50

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1480
    add-int/lit8 v24, v24, -0x1

    goto/16 :goto_1

    .line 1492
    .end local v33    # "n":Lorg/apache/lucene/util/fst/FST$NodeAndInCount;, "Lorg/apache/lucene/util/fst/FST$NodeAndInCount;"
    .restart local v35    # "newNodeAddress":Lorg/apache/lucene/util/packed/GrowableWriter;
    :cond_6
    const-wide/16 v50, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->bytes:Lorg/apache/lucene/util/fst/BytesStore;

    move-object/from16 v49, v0

    invoke-virtual/range {v49 .. v49}, Lorg/apache/lucene/util/fst/BytesStore;->getPosition()J

    move-result-wide v52

    add-long v50, v50, v52

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->nodeAddress:Lorg/apache/lucene/util/packed/GrowableWriter;

    move-object/from16 v49, v0

    move-object/from16 v0, v49

    move/from16 v1, v37

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/packed/GrowableWriter;->get(I)J

    move-result-wide v52

    sub-long v50, v50, v52

    move-object/from16 v0, v35

    move/from16 v1, v37

    move-wide/from16 v2, v50

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/lucene/util/packed/GrowableWriter;->set(IJ)V

    .line 1491
    add-int/lit8 v37, v37, 0x1

    goto/16 :goto_2

    .line 1534
    .restart local v4    # "absCount":I
    .restart local v10    # "addressError":J
    .restart local v18    # "changed":Z
    .restart local v19    # "changedCount":I
    .restart local v22    # "deltaCount":I
    .restart local v27    # "fst":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    .restart local v34    # "negDelta":Z
    .restart local v36    # "nextCount":I
    .restart local v44    # "topCount":I
    .restart local v48    # "writer":Lorg/apache/lucene/util/fst/BytesStore;
    :cond_7
    move-object/from16 v0, v27

    iget-wide v0, v0, Lorg/apache/lucene/util/fst/FST;->nodeCount:J

    move-wide/from16 v50, v0

    const-wide/16 v52, 0x1

    add-long v50, v50, v52

    move-wide/from16 v0, v50

    move-object/from16 v2, v27

    iput-wide v0, v2, Lorg/apache/lucene/util/fst/FST;->nodeCount:J

    .line 1535
    invoke-virtual/range {v48 .. v48}, Lorg/apache/lucene/util/fst/BytesStore;->getPosition()J

    move-result-wide v8

    .line 1538
    .local v8, "address":J
    move-object/from16 v0, v35

    move/from16 v1, v37

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/packed/GrowableWriter;->get(I)J

    move-result-wide v50

    cmp-long v49, v8, v50

    if-eqz v49, :cond_8

    .line 1539
    move-object/from16 v0, v35

    move/from16 v1, v37

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/packed/GrowableWriter;->get(I)J

    move-result-wide v50

    sub-long v10, v8, v50

    .line 1541
    const/16 v18, 0x1

    .line 1542
    move-object/from16 v0, v35

    move/from16 v1, v37

    invoke-virtual {v0, v1, v8, v9}, Lorg/apache/lucene/util/packed/GrowableWriter;->set(IJ)V

    .line 1543
    add-int/lit8 v19, v19, 0x1

    .line 1546
    :cond_8
    const/16 v38, 0x0

    .line 1547
    .local v38, "nodeArcCount":I
    const/16 v17, 0x0

    .line 1549
    .local v17, "bytesPerArc":I
    const/16 v43, 0x0

    .line 1552
    .local v43, "retry":Z
    const/4 v5, 0x0

    .line 1560
    .local v5, "anyNegDelta":Z
    :goto_5
    move/from16 v0, v37

    int-to-long v0, v0

    move-wide/from16 v50, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v50

    move-object/from16 v3, v42

    invoke-virtual {v0, v1, v2, v12, v3}, Lorg/apache/lucene/util/fst/FST;->readFirstRealTargetArc(JLorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 1562
    iget v0, v12, Lorg/apache/lucene/util/fst/FST$Arc;->bytesPerArc:I

    move/from16 v49, v0

    if-eqz v49, :cond_12

    const/16 v47, 0x1

    .line 1563
    .local v47, "useArcArray":Z
    :goto_6
    if-eqz v47, :cond_a

    .line 1565
    if-nez v17, :cond_9

    .line 1566
    iget v0, v12, Lorg/apache/lucene/util/fst/FST$Arc;->bytesPerArc:I

    move/from16 v17, v0

    .line 1568
    :cond_9
    const/16 v49, 0x20

    invoke-virtual/range {v48 .. v49}, Lorg/apache/lucene/util/fst/BytesStore;->writeByte(B)V

    .line 1569
    iget v0, v12, Lorg/apache/lucene/util/fst/FST$Arc;->numArcs:I

    move/from16 v49, v0

    invoke-virtual/range {v48 .. v49}, Lorg/apache/lucene/util/fst/BytesStore;->writeVInt(I)V

    .line 1570
    move-object/from16 v0, v48

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/fst/BytesStore;->writeVInt(I)V

    .line 1574
    :cond_a
    const/16 v32, 0x0

    .line 1579
    .local v32, "maxBytesPerArc":I
    :goto_7
    invoke-virtual/range {v48 .. v48}, Lorg/apache/lucene/util/fst/BytesStore;->getPosition()J

    move-result-wide v14

    .line 1580
    .local v14, "arcStartPos":J
    add-int/lit8 v38, v38, 0x1

    .line 1582
    const/16 v26, 0x0

    .line 1584
    .local v26, "flags":B
    invoke-virtual {v12}, Lorg/apache/lucene/util/fst/FST$Arc;->isLast()Z

    move-result v49

    if-eqz v49, :cond_b

    .line 1585
    const/16 v49, 0x2

    move/from16 v0, v49

    int-to-byte v0, v0

    move/from16 v26, v0

    .line 1592
    :cond_b
    if-nez v47, :cond_c

    const/16 v49, 0x1

    move/from16 v0, v37

    move/from16 v1, v49

    if-eq v0, v1, :cond_c

    iget-wide v0, v12, Lorg/apache/lucene/util/fst/FST$Arc;->target:J

    move-wide/from16 v50, v0

    add-int/lit8 v49, v37, -0x1

    move/from16 v0, v49

    int-to-long v0, v0

    move-wide/from16 v52, v0

    cmp-long v49, v50, v52

    if-nez v49, :cond_c

    .line 1593
    add-int/lit8 v49, v26, 0x4

    move/from16 v0, v49

    int-to-byte v0, v0

    move/from16 v26, v0

    .line 1594
    if-nez v43, :cond_c

    .line 1595
    add-int/lit8 v36, v36, 0x1

    .line 1598
    :cond_c
    invoke-virtual {v12}, Lorg/apache/lucene/util/fst/FST$Arc;->isFinal()Z

    move-result v49

    if-eqz v49, :cond_13

    .line 1599
    add-int/lit8 v49, v26, 0x1

    move/from16 v0, v49

    int-to-byte v0, v0

    move/from16 v26, v0

    .line 1600
    iget-object v0, v12, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    move-object/from16 v49, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->NO_OUTPUT:Ljava/lang/Object;

    move-object/from16 v50, v0

    move-object/from16 v0, v49

    move-object/from16 v1, v50

    if-eq v0, v1, :cond_d

    .line 1601
    add-int/lit8 v49, v26, 0x20

    move/from16 v0, v49

    int-to-byte v0, v0

    move/from16 v26, v0

    .line 1606
    :cond_d
    invoke-static {v12}, Lorg/apache/lucene/util/fst/FST;->targetHasArcs(Lorg/apache/lucene/util/fst/FST$Arc;)Z

    move-result v49

    if-nez v49, :cond_e

    .line 1607
    add-int/lit8 v49, v26, 0x8

    move/from16 v0, v49

    int-to-byte v0, v0

    move/from16 v26, v0

    .line 1610
    :cond_e
    iget-object v0, v12, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    move-object/from16 v49, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->NO_OUTPUT:Ljava/lang/Object;

    move-object/from16 v50, v0

    move-object/from16 v0, v49

    move-object/from16 v1, v50

    if-eq v0, v1, :cond_f

    .line 1611
    add-int/lit8 v49, v26, 0x10

    move/from16 v0, v49

    int-to-byte v0, v0

    move/from16 v26, v0

    .line 1615
    :cond_f
    invoke-static {v12}, Lorg/apache/lucene/util/fst/FST;->targetHasArcs(Lorg/apache/lucene/util/fst/FST$Arc;)Z

    move-result v49

    if-eqz v49, :cond_14

    and-int/lit8 v49, v26, 0x4

    if-nez v49, :cond_14

    const/16 v23, 0x1

    .line 1616
    .local v23, "doWriteTarget":Z
    :goto_8
    if-eqz v23, :cond_16

    .line 1618
    iget-wide v0, v12, Lorg/apache/lucene/util/fst/FST$Arc;->target:J

    move-wide/from16 v50, v0

    invoke-static/range {v50 .. v51}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v49

    move-object/from16 v0, v46

    move-object/from16 v1, v49

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Ljava/lang/Integer;

    .line 1619
    .local v40, "ptr":Ljava/lang/Integer;
    if-eqz v40, :cond_15

    .line 1620
    invoke-virtual/range {v40 .. v40}, Ljava/lang/Integer;->intValue()I

    move-result v49

    move/from16 v0, v49

    int-to-long v6, v0

    .line 1625
    .local v6, "absPtr":J
    :goto_9
    iget-wide v0, v12, Lorg/apache/lucene/util/fst/FST$Arc;->target:J

    move-wide/from16 v50, v0

    move-wide/from16 v0, v50

    long-to-int v0, v0

    move/from16 v49, v0

    move-object/from16 v0, v35

    move/from16 v1, v49

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/packed/GrowableWriter;->get(I)J

    move-result-wide v50

    add-long v50, v50, v10

    invoke-virtual/range {v48 .. v48}, Lorg/apache/lucene/util/fst/BytesStore;->getPosition()J

    move-result-wide v52

    sub-long v50, v50, v52

    const-wide/16 v52, 0x2

    sub-long v20, v50, v52

    .line 1626
    .local v20, "delta":J
    const-wide/16 v50, 0x0

    cmp-long v49, v20, v50

    if-gez v49, :cond_10

    .line 1628
    const/4 v5, 0x1

    .line 1629
    const-wide/16 v20, 0x0

    .line 1632
    :cond_10
    cmp-long v49, v20, v6

    if-gez v49, :cond_11

    .line 1633
    or-int/lit8 v49, v26, 0x40

    move/from16 v0, v49

    int-to-byte v0, v0

    move/from16 v26, v0

    .line 1639
    .end local v20    # "delta":J
    .end local v40    # "ptr":Ljava/lang/Integer;
    :cond_11
    :goto_a
    sget-boolean v49, Lorg/apache/lucene/util/fst/FST;->$assertionsDisabled:Z

    if-nez v49, :cond_17

    const/16 v49, 0x20

    move/from16 v0, v26

    move/from16 v1, v49

    if-ne v0, v1, :cond_17

    new-instance v49, Ljava/lang/AssertionError;

    invoke-direct/range {v49 .. v49}, Ljava/lang/AssertionError;-><init>()V

    throw v49

    .line 1562
    .end local v6    # "absPtr":J
    .end local v14    # "arcStartPos":J
    .end local v23    # "doWriteTarget":Z
    .end local v26    # "flags":B
    .end local v32    # "maxBytesPerArc":I
    .end local v47    # "useArcArray":Z
    :cond_12
    const/16 v47, 0x0

    goto/16 :goto_6

    .line 1604
    .restart local v14    # "arcStartPos":J
    .restart local v26    # "flags":B
    .restart local v32    # "maxBytesPerArc":I
    .restart local v47    # "useArcArray":Z
    :cond_13
    sget-boolean v49, Lorg/apache/lucene/util/fst/FST;->$assertionsDisabled:Z

    if-nez v49, :cond_d

    iget-object v0, v12, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    move-object/from16 v49, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->NO_OUTPUT:Ljava/lang/Object;

    move-object/from16 v50, v0

    move-object/from16 v0, v49

    move-object/from16 v1, v50

    if-eq v0, v1, :cond_d

    new-instance v49, Ljava/lang/AssertionError;

    invoke-direct/range {v49 .. v49}, Ljava/lang/AssertionError;-><init>()V

    throw v49

    .line 1615
    :cond_14
    const/16 v23, 0x0

    goto :goto_8

    .line 1622
    .restart local v23    # "doWriteTarget":Z
    .restart local v40    # "ptr":Ljava/lang/Integer;
    :cond_15
    invoke-interface/range {v46 .. v46}, Ljava/util/Map;->size()I

    move-result v49

    move/from16 v0, v49

    int-to-long v0, v0

    move-wide/from16 v50, v0

    iget-wide v0, v12, Lorg/apache/lucene/util/fst/FST$Arc;->target:J

    move-wide/from16 v52, v0

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v49, v0

    move-object/from16 v0, v35

    move/from16 v1, v49

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/packed/GrowableWriter;->get(I)J

    move-result-wide v52

    add-long v50, v50, v52

    add-long v6, v50, v10

    .restart local v6    # "absPtr":J
    goto/16 :goto_9

    .line 1636
    .end local v6    # "absPtr":J
    .end local v40    # "ptr":Ljava/lang/Integer;
    :cond_16
    const-wide/16 v6, 0x0

    .restart local v6    # "absPtr":J
    goto :goto_a

    .line 1640
    :cond_17
    move-object/from16 v0, v48

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/fst/BytesStore;->writeByte(B)V

    .line 1642
    iget v0, v12, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    move/from16 v49, v0

    move-object/from16 v0, v27

    move-object/from16 v1, v48

    move/from16 v2, v49

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/util/fst/FST;->writeLabel(Lorg/apache/lucene/store/DataOutput;I)V

    .line 1644
    iget-object v0, v12, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    move-object/from16 v49, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->NO_OUTPUT:Ljava/lang/Object;

    move-object/from16 v50, v0

    move-object/from16 v0, v49

    move-object/from16 v1, v50

    if-eq v0, v1, :cond_18

    .line 1645
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    move-object/from16 v49, v0

    iget-object v0, v12, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    move-object/from16 v50, v0

    move-object/from16 v0, v49

    move-object/from16 v1, v50

    move-object/from16 v2, v48

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/fst/Outputs;->write(Ljava/lang/Object;Lorg/apache/lucene/store/DataOutput;)V

    .line 1646
    if-nez v43, :cond_18

    .line 1647
    move-object/from16 v0, v27

    iget-wide v0, v0, Lorg/apache/lucene/util/fst/FST;->arcWithOutputCount:J

    move-wide/from16 v50, v0

    const-wide/16 v52, 0x1

    add-long v50, v50, v52

    move-wide/from16 v0, v50

    move-object/from16 v2, v27

    iput-wide v0, v2, Lorg/apache/lucene/util/fst/FST;->arcWithOutputCount:J

    .line 1650
    :cond_18
    iget-object v0, v12, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    move-object/from16 v49, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->NO_OUTPUT:Ljava/lang/Object;

    move-object/from16 v50, v0

    move-object/from16 v0, v49

    move-object/from16 v1, v50

    if-eq v0, v1, :cond_19

    .line 1651
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    move-object/from16 v49, v0

    iget-object v0, v12, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    move-object/from16 v50, v0

    move-object/from16 v0, v49

    move-object/from16 v1, v50

    move-object/from16 v2, v48

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/fst/Outputs;->writeFinalOutput(Ljava/lang/Object;Lorg/apache/lucene/store/DataOutput;)V

    .line 1654
    :cond_19
    if-eqz v23, :cond_1b

    .line 1656
    iget-wide v0, v12, Lorg/apache/lucene/util/fst/FST$Arc;->target:J

    move-wide/from16 v50, v0

    move-wide/from16 v0, v50

    long-to-int v0, v0

    move/from16 v49, v0

    move-object/from16 v0, v35

    move/from16 v1, v49

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/packed/GrowableWriter;->get(I)J

    move-result-wide v50

    add-long v50, v50, v10

    invoke-virtual/range {v48 .. v48}, Lorg/apache/lucene/util/fst/BytesStore;->getPosition()J

    move-result-wide v52

    sub-long v20, v50, v52

    .line 1657
    .restart local v20    # "delta":J
    const-wide/16 v50, 0x0

    cmp-long v49, v20, v50

    if-gez v49, :cond_1a

    .line 1658
    const/4 v5, 0x1

    .line 1660
    const-wide/16 v20, 0x0

    .line 1663
    :cond_1a
    const/16 v49, 0x40

    move/from16 v0, v26

    move/from16 v1, v49

    invoke-static {v0, v1}, Lorg/apache/lucene/util/fst/FST;->flag(II)Z

    move-result v49

    if-eqz v49, :cond_1e

    .line 1665
    move-object/from16 v0, v48

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/fst/BytesStore;->writeVLong(J)V

    .line 1666
    if-nez v43, :cond_1b

    .line 1667
    add-int/lit8 v22, v22, 0x1

    .line 1688
    .end local v20    # "delta":J
    :cond_1b
    :goto_b
    if-eqz v47, :cond_1c

    .line 1689
    invoke-virtual/range {v48 .. v48}, Lorg/apache/lucene/util/fst/BytesStore;->getPosition()J

    move-result-wide v50

    sub-long v50, v50, v14

    move-wide/from16 v0, v50

    long-to-int v13, v0

    .line 1691
    .local v13, "arcBytes":I
    move/from16 v0, v32

    invoke-static {v0, v13}, Ljava/lang/Math;->max(II)I

    move-result v32

    .line 1699
    move/from16 v0, v17

    int-to-long v0, v0

    move-wide/from16 v50, v0

    add-long v50, v50, v14

    invoke-virtual/range {v48 .. v48}, Lorg/apache/lucene/util/fst/BytesStore;->getPosition()J

    move-result-wide v52

    sub-long v50, v50, v52

    move-wide/from16 v0, v50

    long-to-int v0, v0

    move/from16 v49, v0

    invoke-virtual/range {v48 .. v49}, Lorg/apache/lucene/util/fst/BytesStore;->skipBytes(I)V

    .line 1702
    .end local v13    # "arcBytes":I
    :cond_1c
    invoke-virtual {v12}, Lorg/apache/lucene/util/fst/FST$Arc;->isLast()Z

    move-result v49

    if-eqz v49, :cond_20

    .line 1709
    if-eqz v47, :cond_1d

    .line 1710
    move/from16 v0, v32

    move/from16 v1, v17

    if-eq v0, v1, :cond_1d

    if-eqz v43, :cond_21

    move/from16 v0, v32

    move/from16 v1, v17

    if-gt v0, v1, :cond_21

    .line 1730
    :cond_1d
    or-int v34, v34, v5

    .line 1732
    move-object/from16 v0, v27

    iget-wide v0, v0, Lorg/apache/lucene/util/fst/FST;->arcCount:J

    move-wide/from16 v50, v0

    move/from16 v0, v38

    int-to-long v0, v0

    move-wide/from16 v52, v0

    add-long v50, v50, v52

    move-wide/from16 v0, v50

    move-object/from16 v2, v27

    iput-wide v0, v2, Lorg/apache/lucene/util/fst/FST;->arcCount:J

    .line 1533
    add-int/lit8 v37, v37, -0x1

    goto/16 :goto_3

    .line 1677
    .restart local v20    # "delta":J
    :cond_1e
    move-object/from16 v0, v48

    invoke-virtual {v0, v6, v7}, Lorg/apache/lucene/util/fst/BytesStore;->writeVLong(J)V

    .line 1678
    if-nez v43, :cond_1b

    .line 1679
    invoke-interface/range {v46 .. v46}, Ljava/util/Map;->size()I

    move-result v49

    move/from16 v0, v49

    int-to-long v0, v0

    move-wide/from16 v50, v0

    cmp-long v49, v6, v50

    if-ltz v49, :cond_1f

    .line 1680
    add-int/lit8 v4, v4, 0x1

    .line 1681
    goto :goto_b

    .line 1682
    :cond_1f
    add-int/lit8 v44, v44, 0x1

    goto :goto_b

    .line 1706
    .end local v20    # "delta":J
    :cond_20
    move-object/from16 v0, p0

    move-object/from16 v1, v42

    invoke-virtual {v0, v12, v1}, Lorg/apache/lucene/util/fst/FST;->readNextRealArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    goto/16 :goto_7

    .line 1723
    :cond_21
    move/from16 v17, v32

    .line 1724
    move-object/from16 v0, v48

    invoke-virtual {v0, v8, v9}, Lorg/apache/lucene/util/fst/BytesStore;->truncate(J)V

    .line 1725
    const/16 v38, 0x0

    .line 1726
    const/16 v43, 0x1

    .line 1727
    const/4 v5, 0x0

    .line 1557
    goto/16 :goto_5

    .line 1748
    .end local v5    # "anyNegDelta":Z
    .end local v6    # "absPtr":J
    .end local v8    # "address":J
    .end local v14    # "arcStartPos":J
    .end local v17    # "bytesPerArc":I
    .end local v23    # "doWriteTarget":Z
    .end local v26    # "flags":B
    .end local v32    # "maxBytesPerArc":I
    .end local v38    # "nodeArcCount":I
    .end local v43    # "retry":Z
    .end local v47    # "useArcArray":Z
    :cond_22
    const-wide/16 v30, 0x0

    .line 1749
    .local v30, "maxAddress":J
    invoke-interface/range {v46 .. v46}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v49

    invoke-interface/range {v49 .. v49}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v50

    :goto_c
    invoke-interface/range {v50 .. v50}, Ljava/util/Iterator;->hasNext()Z

    move-result v49

    if-nez v49, :cond_24

    .line 1753
    invoke-interface/range {v46 .. v46}, Ljava/util/Map;->size()I

    move-result v49

    .line 1754
    invoke-static/range {v30 .. v31}, Lorg/apache/lucene/util/packed/PackedInts;->bitsRequired(J)I

    move-result v50

    .line 1753
    move/from16 v0, v49

    move/from16 v1, v50

    move/from16 v2, p3

    invoke-static {v0, v1, v2}, Lorg/apache/lucene/util/packed/PackedInts;->getMutable(IIF)Lorg/apache/lucene/util/packed/PackedInts$Mutable;

    move-result-object v39

    .line 1755
    .local v39, "nodeRefToAddressIn":Lorg/apache/lucene/util/packed/PackedInts$Mutable;
    invoke-interface/range {v46 .. v46}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v49

    invoke-interface/range {v49 .. v49}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v50

    :goto_d
    invoke-interface/range {v50 .. v50}, Ljava/util/Iterator;->hasNext()Z

    move-result v49

    if-nez v49, :cond_25

    .line 1758
    move-object/from16 v0, v39

    move-object/from16 v1, v27

    iput-object v0, v1, Lorg/apache/lucene/util/fst/FST;->nodeRefToAddress:Lorg/apache/lucene/util/packed/PackedInts$Reader;

    .line 1760
    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/util/fst/FST;->startNode:J

    move-wide/from16 v50, v0

    move-wide/from16 v0, v50

    long-to-int v0, v0

    move/from16 v49, v0

    move-object/from16 v0, v35

    move/from16 v1, v49

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/packed/GrowableWriter;->get(I)J

    move-result-wide v50

    move-wide/from16 v0, v50

    move-object/from16 v2, v27

    iput-wide v0, v2, Lorg/apache/lucene/util/fst/FST;->startNode:J

    .line 1763
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->emptyOutput:Ljava/lang/Object;

    move-object/from16 v49, v0

    if-eqz v49, :cond_23

    .line 1764
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->emptyOutput:Ljava/lang/Object;

    move-object/from16 v49, v0

    move-object/from16 v0, v27

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/fst/FST;->setEmptyOutput(Ljava/lang/Object;)V

    .line 1767
    :cond_23
    sget-boolean v49, Lorg/apache/lucene/util/fst/FST;->$assertionsDisabled:Z

    if-nez v49, :cond_26

    move-object/from16 v0, v27

    iget-wide v0, v0, Lorg/apache/lucene/util/fst/FST;->nodeCount:J

    move-wide/from16 v50, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/util/fst/FST;->nodeCount:J

    move-wide/from16 v52, v0

    cmp-long v49, v50, v52

    if-eqz v49, :cond_26

    new-instance v49, Ljava/lang/AssertionError;

    new-instance v50, Ljava/lang/StringBuilder;

    const-string v51, "fst.nodeCount="

    invoke-direct/range {v50 .. v51}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v27

    iget-wide v0, v0, Lorg/apache/lucene/util/fst/FST;->nodeCount:J

    move-wide/from16 v52, v0

    move-object/from16 v0, v50

    move-wide/from16 v1, v52

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v50

    const-string v51, " nodeCount="

    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v50

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/util/fst/FST;->nodeCount:J

    move-wide/from16 v52, v0

    move-object/from16 v0, v50

    move-wide/from16 v1, v52

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v50

    invoke-virtual/range {v50 .. v50}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v50

    invoke-direct/range {v49 .. v50}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v49

    .line 1749
    .end local v39    # "nodeRefToAddressIn":Lorg/apache/lucene/util/packed/PackedInts$Mutable;
    :cond_24
    invoke-interface/range {v50 .. v50}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v49

    check-cast v49, Ljava/lang/Integer;

    invoke-virtual/range {v49 .. v49}, Ljava/lang/Integer;->intValue()I

    move-result v49

    move/from16 v0, v49

    int-to-long v0, v0

    move-wide/from16 v28, v0

    .line 1750
    .local v28, "key":J
    move-wide/from16 v0, v28

    long-to-int v0, v0

    move/from16 v49, v0

    move-object/from16 v0, v35

    move/from16 v1, v49

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/packed/GrowableWriter;->get(I)J

    move-result-wide v52

    move-wide/from16 v0, v30

    move-wide/from16 v2, v52

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v30

    goto/16 :goto_c

    .line 1755
    .end local v28    # "key":J
    .restart local v39    # "nodeRefToAddressIn":Lorg/apache/lucene/util/packed/PackedInts$Mutable;
    :cond_25
    invoke-interface/range {v50 .. v50}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Ljava/util/Map$Entry;

    .line 1756
    .local v25, "ent":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    invoke-interface/range {v25 .. v25}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v49

    check-cast v49, Ljava/lang/Integer;

    invoke-virtual/range {v49 .. v49}, Ljava/lang/Integer;->intValue()I

    move-result v51

    invoke-interface/range {v25 .. v25}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v49

    check-cast v49, Ljava/lang/Integer;

    invoke-virtual/range {v49 .. v49}, Ljava/lang/Integer;->intValue()I

    move-result v49

    move-object/from16 v0, v35

    move/from16 v1, v49

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/packed/GrowableWriter;->get(I)J

    move-result-wide v52

    move-object/from16 v0, v39

    move/from16 v1, v51

    move-wide/from16 v2, v52

    invoke-interface {v0, v1, v2, v3}, Lorg/apache/lucene/util/packed/PackedInts$Mutable;->set(IJ)V

    goto/16 :goto_d

    .line 1768
    .end local v25    # "ent":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :cond_26
    sget-boolean v49, Lorg/apache/lucene/util/fst/FST;->$assertionsDisabled:Z

    if-nez v49, :cond_27

    move-object/from16 v0, v27

    iget-wide v0, v0, Lorg/apache/lucene/util/fst/FST;->arcCount:J

    move-wide/from16 v50, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/util/fst/FST;->arcCount:J

    move-wide/from16 v52, v0

    cmp-long v49, v50, v52

    if-eqz v49, :cond_27

    new-instance v49, Ljava/lang/AssertionError;

    invoke-direct/range {v49 .. v49}, Ljava/lang/AssertionError;-><init>()V

    throw v49

    .line 1769
    :cond_27
    sget-boolean v49, Lorg/apache/lucene/util/fst/FST;->$assertionsDisabled:Z

    if-nez v49, :cond_28

    move-object/from16 v0, v27

    iget-wide v0, v0, Lorg/apache/lucene/util/fst/FST;->arcWithOutputCount:J

    move-wide/from16 v50, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/util/fst/FST;->arcWithOutputCount:J

    move-wide/from16 v52, v0

    cmp-long v49, v50, v52

    if-eqz v49, :cond_28

    new-instance v49, Ljava/lang/AssertionError;

    new-instance v50, Ljava/lang/StringBuilder;

    const-string v51, "fst.arcWithOutputCount="

    invoke-direct/range {v50 .. v51}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v27

    iget-wide v0, v0, Lorg/apache/lucene/util/fst/FST;->arcWithOutputCount:J

    move-wide/from16 v52, v0

    move-object/from16 v0, v50

    move-wide/from16 v1, v52

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v50

    const-string v51, " arcWithOutputCount="

    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v50

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/util/fst/FST;->arcWithOutputCount:J

    move-wide/from16 v52, v0

    move-object/from16 v0, v50

    move-wide/from16 v1, v52

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v50

    invoke-virtual/range {v50 .. v50}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v50

    invoke-direct/range {v49 .. v50}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v49

    .line 1771
    :cond_28
    move-object/from16 v0, v27

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->bytes:Lorg/apache/lucene/util/fst/BytesStore;

    move-object/from16 v49, v0

    invoke-virtual/range {v49 .. v49}, Lorg/apache/lucene/util/fst/BytesStore;->finish()V

    .line 1772
    invoke-direct/range {v27 .. v27}, Lorg/apache/lucene/util/fst/FST;->cacheRootArcs()V

    .line 1777
    return-object v27
.end method

.method public readFirstRealTargetArc(JLorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;
    .locals 5
    .param p1, "node"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;",
            "Lorg/apache/lucene/util/fst/FST$BytesReader;",
            ")",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 913
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    .local p3, "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    .local p4, "in":Lorg/apache/lucene/util/fst/FST$BytesReader;, "Lorg/apache/lucene/util/fst/FST$BytesReader;"
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/util/fst/FST;->getNodeAddress(J)J

    move-result-wide v0

    .line 914
    .local v0, "address":J
    invoke-virtual {p4, v0, v1}, Lorg/apache/lucene/util/fst/FST$BytesReader;->setPosition(J)V

    .line 918
    iput-wide p1, p3, Lorg/apache/lucene/util/fst/FST$Arc;->node:J

    .line 920
    invoke-virtual {p4}, Lorg/apache/lucene/util/fst/FST$BytesReader;->readByte()B

    move-result v2

    const/16 v3, 0x20

    if-ne v2, v3, :cond_2

    .line 923
    invoke-virtual {p4}, Lorg/apache/lucene/util/fst/FST$BytesReader;->readVInt()I

    move-result v2

    iput v2, p3, Lorg/apache/lucene/util/fst/FST$Arc;->numArcs:I

    .line 924
    iget-boolean v2, p0, Lorg/apache/lucene/util/fst/FST;->packed:Z

    if-nez v2, :cond_0

    iget v2, p0, Lorg/apache/lucene/util/fst/FST;->version:I

    const/4 v3, 0x4

    if-lt v2, v3, :cond_1

    .line 925
    :cond_0
    invoke-virtual {p4}, Lorg/apache/lucene/util/fst/FST$BytesReader;->readVInt()I

    move-result v2

    iput v2, p3, Lorg/apache/lucene/util/fst/FST$Arc;->bytesPerArc:I

    .line 929
    :goto_0
    const/4 v2, -0x1

    iput v2, p3, Lorg/apache/lucene/util/fst/FST$Arc;->arcIdx:I

    .line 930
    invoke-virtual {p4}, Lorg/apache/lucene/util/fst/FST$BytesReader;->getPosition()J

    move-result-wide v2

    iput-wide v2, p3, Lorg/apache/lucene/util/fst/FST$Arc;->posArcsStart:J

    iput-wide v2, p3, Lorg/apache/lucene/util/fst/FST$Arc;->nextArc:J

    .line 938
    :goto_1
    invoke-virtual {p0, p3, p4}, Lorg/apache/lucene/util/fst/FST;->readNextRealArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v2

    return-object v2

    .line 927
    :cond_1
    invoke-virtual {p4}, Lorg/apache/lucene/util/fst/FST$BytesReader;->readInt()I

    move-result v2

    iput v2, p3, Lorg/apache/lucene/util/fst/FST$Arc;->bytesPerArc:I

    goto :goto_0

    .line 934
    :cond_2
    iput-wide v0, p3, Lorg/apache/lucene/util/fst/FST$Arc;->nextArc:J

    .line 935
    const/4 v2, 0x0

    iput v2, p3, Lorg/apache/lucene/util/fst/FST$Arc;->bytesPerArc:I

    goto :goto_1
.end method

.method public readFirstTargetArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;",
            "Lorg/apache/lucene/util/fst/FST$BytesReader;",
            ")",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 892
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    .local p1, "follow":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    .local p2, "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    .local p3, "in":Lorg/apache/lucene/util/fst/FST$BytesReader;, "Lorg/apache/lucene/util/fst/FST$BytesReader;"
    invoke-virtual {p1}, Lorg/apache/lucene/util/fst/FST$Arc;->isFinal()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 894
    const/4 v0, -0x1

    iput v0, p2, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    .line 895
    iget-object v0, p1, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    iput-object v0, p2, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    .line 896
    const/4 v0, 0x1

    iput-byte v0, p2, Lorg/apache/lucene/util/fst/FST$Arc;->flags:B

    .line 897
    iget-wide v0, p1, Lorg/apache/lucene/util/fst/FST$Arc;->target:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    .line 898
    iget-byte v0, p2, Lorg/apache/lucene/util/fst/FST$Arc;->flags:B

    or-int/lit8 v0, v0, 0x2

    int-to-byte v0, v0

    iput-byte v0, p2, Lorg/apache/lucene/util/fst/FST$Arc;->flags:B

    .line 904
    :goto_0
    const-wide/16 v0, -0x1

    iput-wide v0, p2, Lorg/apache/lucene/util/fst/FST$Arc;->target:J

    .line 908
    .end local p2    # "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    :goto_1
    return-object p2

    .line 900
    .restart local p2    # "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    :cond_0
    iget-wide v0, p1, Lorg/apache/lucene/util/fst/FST$Arc;->target:J

    iput-wide v0, p2, Lorg/apache/lucene/util/fst/FST$Arc;->node:J

    .line 902
    iget-wide v0, p1, Lorg/apache/lucene/util/fst/FST$Arc;->target:J

    iput-wide v0, p2, Lorg/apache/lucene/util/fst/FST$Arc;->nextArc:J

    goto :goto_0

    .line 908
    :cond_1
    iget-wide v0, p1, Lorg/apache/lucene/util/fst/FST$Arc;->target:J

    invoke-virtual {p0, v0, v1, p2, p3}, Lorg/apache/lucene/util/fst/FST;->readFirstRealTargetArc(JLorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object p2

    goto :goto_1
.end method

.method readLabel(Lorg/apache/lucene/store/DataInput;)I
    .locals 3
    .param p1, "in"    # Lorg/apache/lucene/store/DataInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 578
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    iget-object v1, p0, Lorg/apache/lucene/util/fst/FST;->inputType:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    sget-object v2, Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;->BYTE1:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    if-ne v1, v2, :cond_0

    .line 580
    invoke-virtual {p1}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v1

    and-int/lit16 v0, v1, 0xff

    .line 587
    .local v0, "v":I
    :goto_0
    return v0

    .line 581
    .end local v0    # "v":I
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/util/fst/FST;->inputType:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    sget-object v2, Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;->BYTE2:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    if-ne v1, v2, :cond_1

    .line 583
    invoke-virtual {p1}, Lorg/apache/lucene/store/DataInput;->readShort()S

    move-result v1

    const v2, 0xffff

    and-int v0, v1, v2

    .line 584
    .restart local v0    # "v":I
    goto :goto_0

    .line 585
    .end local v0    # "v":I
    :cond_1
    invoke-virtual {p1}, Lorg/apache/lucene/store/DataInput;->readVInt()I

    move-result v0

    .restart local v0    # "v":I
    goto :goto_0
.end method

.method public readLastTargetArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;",
            "Lorg/apache/lucene/util/fst/FST$BytesReader;",
            ")",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    .local p1, "follow":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    .local p2, "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    .local p3, "in":Lorg/apache/lucene/util/fst/FST$BytesReader;, "Lorg/apache/lucene/util/fst/FST$BytesReader;"
    const/16 v6, 0x20

    const/4 v5, 0x4

    const/4 v4, -0x1

    .line 816
    invoke-static {p1}, Lorg/apache/lucene/util/fst/FST;->targetHasArcs(Lorg/apache/lucene/util/fst/FST$Arc;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 818
    sget-boolean v1, Lorg/apache/lucene/util/fst/FST;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    invoke-virtual {p1}, Lorg/apache/lucene/util/fst/FST$Arc;->isFinal()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 819
    :cond_0
    iput v4, p2, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    .line 820
    const-wide/16 v2, -0x1

    iput-wide v2, p2, Lorg/apache/lucene/util/fst/FST$Arc;->target:J

    .line 821
    iget-object v1, p1, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    iput-object v1, p2, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    .line 822
    const/4 v1, 0x2

    iput-byte v1, p2, Lorg/apache/lucene/util/fst/FST$Arc;->flags:B

    .line 868
    :cond_1
    return-object p2

    .line 825
    :cond_2
    iget-wide v2, p1, Lorg/apache/lucene/util/fst/FST$Arc;->target:J

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/util/fst/FST;->getNodeAddress(J)J

    move-result-wide v2

    invoke-virtual {p3, v2, v3}, Lorg/apache/lucene/util/fst/FST$BytesReader;->setPosition(J)V

    .line 826
    iget-wide v2, p1, Lorg/apache/lucene/util/fst/FST$Arc;->target:J

    iput-wide v2, p2, Lorg/apache/lucene/util/fst/FST$Arc;->node:J

    .line 827
    invoke-virtual {p3}, Lorg/apache/lucene/util/fst/FST$BytesReader;->readByte()B

    move-result v0

    .line 828
    .local v0, "b":B
    if-ne v0, v6, :cond_5

    .line 830
    invoke-virtual {p3}, Lorg/apache/lucene/util/fst/FST$BytesReader;->readVInt()I

    move-result v1

    iput v1, p2, Lorg/apache/lucene/util/fst/FST$Arc;->numArcs:I

    .line 831
    iget-boolean v1, p0, Lorg/apache/lucene/util/fst/FST;->packed:Z

    if-nez v1, :cond_3

    iget v1, p0, Lorg/apache/lucene/util/fst/FST;->version:I

    if-lt v1, v5, :cond_4

    .line 832
    :cond_3
    invoke-virtual {p3}, Lorg/apache/lucene/util/fst/FST$BytesReader;->readVInt()I

    move-result v1

    iput v1, p2, Lorg/apache/lucene/util/fst/FST$Arc;->bytesPerArc:I

    .line 837
    :goto_0
    invoke-virtual {p3}, Lorg/apache/lucene/util/fst/FST$BytesReader;->getPosition()J

    move-result-wide v2

    iput-wide v2, p2, Lorg/apache/lucene/util/fst/FST$Arc;->posArcsStart:J

    .line 838
    iget v1, p2, Lorg/apache/lucene/util/fst/FST$Arc;->numArcs:I

    add-int/lit8 v1, v1, -0x2

    iput v1, p2, Lorg/apache/lucene/util/fst/FST$Arc;->arcIdx:I

    .line 866
    :goto_1
    invoke-virtual {p0, p2, p3}, Lorg/apache/lucene/util/fst/FST;->readNextRealArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 867
    sget-boolean v1, Lorg/apache/lucene/util/fst/FST;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    invoke-virtual {p2}, Lorg/apache/lucene/util/fst/FST$Arc;->isLast()Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 834
    :cond_4
    invoke-virtual {p3}, Lorg/apache/lucene/util/fst/FST$BytesReader;->readInt()I

    move-result v1

    iput v1, p2, Lorg/apache/lucene/util/fst/FST$Arc;->bytesPerArc:I

    goto :goto_0

    .line 840
    :cond_5
    iput-byte v0, p2, Lorg/apache/lucene/util/fst/FST$Arc;->flags:B

    .line 842
    const/4 v1, 0x0

    iput v1, p2, Lorg/apache/lucene/util/fst/FST$Arc;->bytesPerArc:I

    .line 844
    :goto_2
    invoke-virtual {p2}, Lorg/apache/lucene/util/fst/FST$Arc;->isLast()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 863
    invoke-virtual {p3, v4}, Lorg/apache/lucene/util/fst/FST$BytesReader;->skipBytes(I)V

    .line 864
    invoke-virtual {p3}, Lorg/apache/lucene/util/fst/FST$BytesReader;->getPosition()J

    move-result-wide v2

    iput-wide v2, p2, Lorg/apache/lucene/util/fst/FST$Arc;->nextArc:J

    goto :goto_1

    .line 846
    :cond_6
    invoke-virtual {p0, p3}, Lorg/apache/lucene/util/fst/FST;->readLabel(Lorg/apache/lucene/store/DataInput;)I

    .line 847
    const/16 v1, 0x10

    invoke-virtual {p2, v1}, Lorg/apache/lucene/util/fst/FST$Arc;->flag(I)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 848
    iget-object v1, p0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    invoke-virtual {v1, p3}, Lorg/apache/lucene/util/fst/Outputs;->read(Lorg/apache/lucene/store/DataInput;)Ljava/lang/Object;

    .line 850
    :cond_7
    invoke-virtual {p2, v6}, Lorg/apache/lucene/util/fst/FST$Arc;->flag(I)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 851
    iget-object v1, p0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    invoke-virtual {v1, p3}, Lorg/apache/lucene/util/fst/Outputs;->readFinalOutput(Lorg/apache/lucene/store/DataInput;)Ljava/lang/Object;

    .line 853
    :cond_8
    const/16 v1, 0x8

    invoke-virtual {p2, v1}, Lorg/apache/lucene/util/fst/FST$Arc;->flag(I)Z

    move-result v1

    if-nez v1, :cond_9

    .line 854
    invoke-virtual {p2, v5}, Lorg/apache/lucene/util/fst/FST$Arc;->flag(I)Z

    move-result v1

    if-nez v1, :cond_9

    .line 855
    iget-boolean v1, p0, Lorg/apache/lucene/util/fst/FST;->packed:Z

    if-eqz v1, :cond_a

    .line 856
    invoke-virtual {p3}, Lorg/apache/lucene/util/fst/FST$BytesReader;->readVLong()J

    .line 860
    :cond_9
    :goto_3
    invoke-virtual {p3}, Lorg/apache/lucene/util/fst/FST$BytesReader;->readByte()B

    move-result v1

    iput-byte v1, p2, Lorg/apache/lucene/util/fst/FST$Arc;->flags:B

    goto :goto_2

    .line 858
    :cond_a
    invoke-direct {p0, p3}, Lorg/apache/lucene/util/fst/FST;->readUnpackedNodeTarget(Lorg/apache/lucene/util/fst/FST$BytesReader;)J

    goto :goto_3
.end method

.method public readNextArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;",
            "Lorg/apache/lucene/util/fst/FST$BytesReader;",
            ")",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 958
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    .local p1, "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    .local p2, "in":Lorg/apache/lucene/util/fst/FST$BytesReader;, "Lorg/apache/lucene/util/fst/FST$BytesReader;"
    iget v0, p1, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 960
    iget-wide v0, p1, Lorg/apache/lucene/util/fst/FST$Arc;->nextArc:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    .line 961
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "cannot readNextArc when arc.isLast()=true"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 963
    :cond_0
    iget-wide v0, p1, Lorg/apache/lucene/util/fst/FST$Arc;->nextArc:J

    invoke-virtual {p0, v0, v1, p1, p2}, Lorg/apache/lucene/util/fst/FST;->readFirstRealTargetArc(JLorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v0

    .line 965
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/util/fst/FST;->readNextRealArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v0

    goto :goto_0
.end method

.method public readNextArcLabel(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)I
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;",
            "Lorg/apache/lucene/util/fst/FST$BytesReader;",
            ")I"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 972
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    .local p1, "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    .local p2, "in":Lorg/apache/lucene/util/fst/FST$BytesReader;, "Lorg/apache/lucene/util/fst/FST$BytesReader;"
    sget-boolean v1, Lorg/apache/lucene/util/fst/FST;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    invoke-virtual {p1}, Lorg/apache/lucene/util/fst/FST$Arc;->isLast()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 974
    :cond_0
    iget v1, p1, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    const/4 v4, -0x1

    if-ne v1, v4, :cond_4

    .line 978
    iget-wide v4, p1, Lorg/apache/lucene/util/fst/FST$Arc;->nextArc:J

    invoke-direct {p0, v4, v5}, Lorg/apache/lucene/util/fst/FST;->getNodeAddress(J)J

    move-result-wide v2

    .line 979
    .local v2, "pos":J
    invoke-virtual {p2, v2, v3}, Lorg/apache/lucene/util/fst/FST$BytesReader;->setPosition(J)V

    .line 981
    invoke-virtual {p2}, Lorg/apache/lucene/util/fst/FST$BytesReader;->readByte()B

    move-result v0

    .line 982
    .local v0, "b":B
    const/16 v1, 0x20

    if-ne v0, v1, :cond_3

    .line 984
    invoke-virtual {p2}, Lorg/apache/lucene/util/fst/FST$BytesReader;->readVInt()I

    .line 987
    iget-boolean v1, p0, Lorg/apache/lucene/util/fst/FST;->packed:Z

    if-nez v1, :cond_1

    iget v1, p0, Lorg/apache/lucene/util/fst/FST;->version:I

    const/4 v4, 0x4

    if-lt v1, v4, :cond_2

    .line 988
    :cond_1
    invoke-virtual {p2}, Lorg/apache/lucene/util/fst/FST$BytesReader;->readVInt()I

    .line 1008
    .end local v0    # "b":B
    .end local v2    # "pos":J
    :goto_0
    invoke-virtual {p2}, Lorg/apache/lucene/util/fst/FST$BytesReader;->readByte()B

    .line 1009
    invoke-virtual {p0, p2}, Lorg/apache/lucene/util/fst/FST;->readLabel(Lorg/apache/lucene/store/DataInput;)I

    move-result v1

    return v1

    .line 990
    .restart local v0    # "b":B
    .restart local v2    # "pos":J
    :cond_2
    invoke-virtual {p2}, Lorg/apache/lucene/util/fst/FST$BytesReader;->readInt()I

    goto :goto_0

    .line 993
    :cond_3
    invoke-virtual {p2, v2, v3}, Lorg/apache/lucene/util/fst/FST$BytesReader;->setPosition(J)V

    goto :goto_0

    .line 996
    .end local v0    # "b":B
    .end local v2    # "pos":J
    :cond_4
    iget v1, p1, Lorg/apache/lucene/util/fst/FST$Arc;->bytesPerArc:I

    if-eqz v1, :cond_5

    .line 999
    iget-wide v4, p1, Lorg/apache/lucene/util/fst/FST$Arc;->posArcsStart:J

    invoke-virtual {p2, v4, v5}, Lorg/apache/lucene/util/fst/FST$BytesReader;->setPosition(J)V

    .line 1000
    iget v1, p1, Lorg/apache/lucene/util/fst/FST$Arc;->arcIdx:I

    add-int/lit8 v1, v1, 0x1

    iget v4, p1, Lorg/apache/lucene/util/fst/FST$Arc;->bytesPerArc:I

    mul-int/2addr v1, v4

    invoke-virtual {p2, v1}, Lorg/apache/lucene/util/fst/FST$BytesReader;->skipBytes(I)V

    goto :goto_0

    .line 1004
    :cond_5
    iget-wide v4, p1, Lorg/apache/lucene/util/fst/FST$Arc;->nextArc:J

    invoke-virtual {p2, v4, v5}, Lorg/apache/lucene/util/fst/FST$BytesReader;->setPosition(J)V

    goto :goto_0
.end method

.method public readNextRealArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;",
            "Lorg/apache/lucene/util/fst/FST$BytesReader;",
            ")",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    .local p1, "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    .local p2, "in":Lorg/apache/lucene/util/fst/FST$BytesReader;, "Lorg/apache/lucene/util/fst/FST$BytesReader;"
    const-wide/16 v8, 0x0

    .line 1020
    iget v4, p1, Lorg/apache/lucene/util/fst/FST$Arc;->bytesPerArc:I

    if-eqz v4, :cond_2

    .line 1022
    iget v4, p1, Lorg/apache/lucene/util/fst/FST$Arc;->arcIdx:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p1, Lorg/apache/lucene/util/fst/FST$Arc;->arcIdx:I

    .line 1023
    sget-boolean v4, Lorg/apache/lucene/util/fst/FST;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    iget v4, p1, Lorg/apache/lucene/util/fst/FST$Arc;->arcIdx:I

    iget v5, p1, Lorg/apache/lucene/util/fst/FST$Arc;->numArcs:I

    if-lt v4, v5, :cond_0

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 1024
    :cond_0
    iget-wide v4, p1, Lorg/apache/lucene/util/fst/FST$Arc;->posArcsStart:J

    invoke-virtual {p2, v4, v5}, Lorg/apache/lucene/util/fst/FST$BytesReader;->setPosition(J)V

    .line 1025
    iget v4, p1, Lorg/apache/lucene/util/fst/FST$Arc;->arcIdx:I

    iget v5, p1, Lorg/apache/lucene/util/fst/FST$Arc;->bytesPerArc:I

    mul-int/2addr v4, v5

    invoke-virtual {p2, v4}, Lorg/apache/lucene/util/fst/FST$BytesReader;->skipBytes(I)V

    .line 1030
    :goto_0
    invoke-virtual {p2}, Lorg/apache/lucene/util/fst/FST$BytesReader;->readByte()B

    move-result v4

    iput-byte v4, p1, Lorg/apache/lucene/util/fst/FST$Arc;->flags:B

    .line 1031
    invoke-virtual {p0, p2}, Lorg/apache/lucene/util/fst/FST;->readLabel(Lorg/apache/lucene/store/DataInput;)I

    move-result v4

    iput v4, p1, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    .line 1033
    const/16 v4, 0x10

    invoke-virtual {p1, v4}, Lorg/apache/lucene/util/fst/FST$Arc;->flag(I)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1034
    iget-object v4, p0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    invoke-virtual {v4, p2}, Lorg/apache/lucene/util/fst/Outputs;->read(Lorg/apache/lucene/store/DataInput;)Ljava/lang/Object;

    move-result-object v4

    iput-object v4, p1, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    .line 1039
    :goto_1
    const/16 v4, 0x20

    invoke-virtual {p1, v4}, Lorg/apache/lucene/util/fst/FST$Arc;->flag(I)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1040
    iget-object v4, p0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    invoke-virtual {v4, p2}, Lorg/apache/lucene/util/fst/Outputs;->readFinalOutput(Lorg/apache/lucene/store/DataInput;)Ljava/lang/Object;

    move-result-object v4

    iput-object v4, p1, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    .line 1045
    :goto_2
    const/16 v4, 0x8

    invoke-virtual {p1, v4}, Lorg/apache/lucene/util/fst/FST$Arc;->flag(I)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1046
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, Lorg/apache/lucene/util/fst/FST$Arc;->flag(I)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1047
    const-wide/16 v4, -0x1

    iput-wide v4, p1, Lorg/apache/lucene/util/fst/FST$Arc;->target:J

    .line 1051
    :goto_3
    invoke-virtual {p2}, Lorg/apache/lucene/util/fst/FST$BytesReader;->getPosition()J

    move-result-wide v4

    iput-wide v4, p1, Lorg/apache/lucene/util/fst/FST$Arc;->nextArc:J

    .line 1093
    :cond_1
    :goto_4
    return-object p1

    .line 1028
    :cond_2
    iget-wide v4, p1, Lorg/apache/lucene/util/fst/FST$Arc;->nextArc:J

    invoke-virtual {p2, v4, v5}, Lorg/apache/lucene/util/fst/FST$BytesReader;->setPosition(J)V

    goto :goto_0

    .line 1036
    :cond_3
    iget-object v4, p0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    invoke-virtual {v4}, Lorg/apache/lucene/util/fst/Outputs;->getNoOutput()Ljava/lang/Object;

    move-result-object v4

    iput-object v4, p1, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    goto :goto_1

    .line 1042
    :cond_4
    iget-object v4, p0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    invoke-virtual {v4}, Lorg/apache/lucene/util/fst/Outputs;->getNoOutput()Ljava/lang/Object;

    move-result-object v4

    iput-object v4, p1, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    goto :goto_2

    .line 1049
    :cond_5
    iput-wide v8, p1, Lorg/apache/lucene/util/fst/FST$Arc;->target:J

    goto :goto_3

    .line 1052
    :cond_6
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, Lorg/apache/lucene/util/fst/FST$Arc;->flag(I)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 1053
    invoke-virtual {p2}, Lorg/apache/lucene/util/fst/FST$BytesReader;->getPosition()J

    move-result-wide v4

    iput-wide v4, p1, Lorg/apache/lucene/util/fst/FST$Arc;->nextArc:J

    .line 1056
    iget-object v4, p0, Lorg/apache/lucene/util/fst/FST;->nodeAddress:Lorg/apache/lucene/util/packed/GrowableWriter;

    if-nez v4, :cond_9

    .line 1057
    const/4 v4, 0x2

    invoke-virtual {p1, v4}, Lorg/apache/lucene/util/fst/FST$Arc;->flag(I)Z

    move-result v4

    if-nez v4, :cond_7

    .line 1058
    iget v4, p1, Lorg/apache/lucene/util/fst/FST$Arc;->bytesPerArc:I

    if-nez v4, :cond_8

    .line 1060
    invoke-direct {p0, p2}, Lorg/apache/lucene/util/fst/FST;->seekToNextNode(Lorg/apache/lucene/util/fst/FST$BytesReader;)V

    .line 1066
    :cond_7
    :goto_5
    invoke-virtual {p2}, Lorg/apache/lucene/util/fst/FST$BytesReader;->getPosition()J

    move-result-wide v4

    iput-wide v4, p1, Lorg/apache/lucene/util/fst/FST$Arc;->target:J

    goto :goto_4

    .line 1062
    :cond_8
    iget-wide v4, p1, Lorg/apache/lucene/util/fst/FST$Arc;->posArcsStart:J

    invoke-virtual {p2, v4, v5}, Lorg/apache/lucene/util/fst/FST$BytesReader;->setPosition(J)V

    .line 1063
    iget v4, p1, Lorg/apache/lucene/util/fst/FST$Arc;->bytesPerArc:I

    iget v5, p1, Lorg/apache/lucene/util/fst/FST$Arc;->numArcs:I

    mul-int/2addr v4, v5

    invoke-virtual {p2, v4}, Lorg/apache/lucene/util/fst/FST$BytesReader;->skipBytes(I)V

    goto :goto_5

    .line 1068
    :cond_9
    iget-wide v4, p1, Lorg/apache/lucene/util/fst/FST$Arc;->node:J

    const-wide/16 v6, 0x1

    sub-long/2addr v4, v6

    iput-wide v4, p1, Lorg/apache/lucene/util/fst/FST$Arc;->target:J

    .line 1069
    sget-boolean v4, Lorg/apache/lucene/util/fst/FST;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    iget-wide v4, p1, Lorg/apache/lucene/util/fst/FST$Arc;->target:J

    cmp-long v4, v4, v8

    if-gtz v4, :cond_1

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 1072
    :cond_a
    iget-boolean v4, p0, Lorg/apache/lucene/util/fst/FST;->packed:Z

    if-eqz v4, :cond_d

    .line 1073
    invoke-virtual {p2}, Lorg/apache/lucene/util/fst/FST$BytesReader;->getPosition()J

    move-result-wide v2

    .line 1074
    .local v2, "pos":J
    invoke-virtual {p2}, Lorg/apache/lucene/util/fst/FST$BytesReader;->readVLong()J

    move-result-wide v0

    .line 1075
    .local v0, "code":J
    const/16 v4, 0x40

    invoke-virtual {p1, v4}, Lorg/apache/lucene/util/fst/FST$Arc;->flag(I)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 1077
    add-long v4, v2, v0

    iput-wide v4, p1, Lorg/apache/lucene/util/fst/FST$Arc;->target:J

    .line 1091
    .end local v0    # "code":J
    .end local v2    # "pos":J
    :goto_6
    invoke-virtual {p2}, Lorg/apache/lucene/util/fst/FST$BytesReader;->getPosition()J

    move-result-wide v4

    iput-wide v4, p1, Lorg/apache/lucene/util/fst/FST$Arc;->nextArc:J

    goto/16 :goto_4

    .line 1079
    .restart local v0    # "code":J
    .restart local v2    # "pos":J
    :cond_b
    iget-object v4, p0, Lorg/apache/lucene/util/fst/FST;->nodeRefToAddress:Lorg/apache/lucene/util/packed/PackedInts$Reader;

    invoke-interface {v4}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->size()I

    move-result v4

    int-to-long v4, v4

    cmp-long v4, v0, v4

    if-gez v4, :cond_c

    .line 1081
    iget-object v4, p0, Lorg/apache/lucene/util/fst/FST;->nodeRefToAddress:Lorg/apache/lucene/util/packed/PackedInts$Reader;

    long-to-int v5, v0

    invoke-interface {v4, v5}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I)J

    move-result-wide v4

    iput-wide v4, p1, Lorg/apache/lucene/util/fst/FST$Arc;->target:J

    goto :goto_6

    .line 1085
    :cond_c
    iput-wide v0, p1, Lorg/apache/lucene/util/fst/FST$Arc;->target:J

    goto :goto_6

    .line 1089
    .end local v0    # "code":J
    .end local v2    # "pos":J
    :cond_d
    invoke-direct {p0, p2}, Lorg/apache/lucene/util/fst/FST;->readUnpackedNodeTarget(Lorg/apache/lucene/util/fst/FST$BytesReader;)J

    move-result-wide v4

    iput-wide v4, p1, Lorg/apache/lucene/util/fst/FST$Arc;->target:J

    goto :goto_6
.end method

.method public save(Ljava/io/File;)V
    .locals 5
    .param p1, "file"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 530
    const/4 v1, 0x0

    .line 531
    .local v1, "success":Z
    new-instance v0, Ljava/io/BufferedOutputStream;

    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v0, v2}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 533
    .local v0, "os":Ljava/io/OutputStream;
    :try_start_0
    new-instance v2, Lorg/apache/lucene/store/OutputStreamDataOutput;

    invoke-direct {v2, v0}, Lorg/apache/lucene/store/OutputStreamDataOutput;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual {p0, v2}, Lorg/apache/lucene/util/fst/FST;->save(Lorg/apache/lucene/store/DataOutput;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 534
    const/4 v1, 0x1

    .line 536
    if-eqz v1, :cond_1

    new-array v2, v3, [Ljava/io/Closeable;

    .line 537
    aput-object v0, v2, v4

    invoke-static {v2}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 542
    :goto_0
    return-void

    .line 535
    :catchall_0
    move-exception v2

    .line 536
    if-eqz v1, :cond_0

    new-array v3, v3, [Ljava/io/Closeable;

    .line 537
    aput-object v0, v3, v4

    invoke-static {v3}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 541
    :goto_1
    throw v2

    .line 538
    :cond_0
    new-array v3, v3, [Ljava/io/Closeable;

    .line 539
    aput-object v0, v3, v4

    invoke-static {v3}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto :goto_1

    .line 538
    :cond_1
    new-array v2, v3, [Ljava/io/Closeable;

    .line 539
    aput-object v0, v2, v4

    invoke-static {v2}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto :goto_0
.end method

.method public save(Lorg/apache/lucene/store/DataOutput;)V
    .locals 14
    .param p1, "out"    # Lorg/apache/lucene/store/DataOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    const/4 v13, 0x1

    const/4 v12, 0x0

    .line 461
    iget-wide v8, p0, Lorg/apache/lucene/util/fst/FST;->startNode:J

    const-wide/16 v10, -0x1

    cmp-long v8, v8, v10

    if-nez v8, :cond_0

    .line 462
    new-instance v8, Ljava/lang/IllegalStateException;

    const-string v9, "call finish first"

    invoke-direct {v8, v9}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 464
    :cond_0
    iget-object v8, p0, Lorg/apache/lucene/util/fst/FST;->nodeAddress:Lorg/apache/lucene/util/packed/GrowableWriter;

    if-eqz v8, :cond_1

    .line 465
    new-instance v8, Ljava/lang/IllegalStateException;

    const-string v9, "cannot save an FST pre-packed FST; it must first be packed"

    invoke-direct {v8, v9}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 467
    :cond_1
    iget-boolean v8, p0, Lorg/apache/lucene/util/fst/FST;->packed:Z

    if-eqz v8, :cond_2

    iget-object v8, p0, Lorg/apache/lucene/util/fst/FST;->nodeRefToAddress:Lorg/apache/lucene/util/packed/PackedInts$Reader;

    instance-of v8, v8, Lorg/apache/lucene/util/packed/PackedInts$Mutable;

    if-nez v8, :cond_2

    .line 468
    new-instance v8, Ljava/lang/IllegalStateException;

    const-string v9, "cannot save a FST which has been loaded from disk "

    invoke-direct {v8, v9}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 470
    :cond_2
    const-string v8, "FST"

    const/4 v9, 0x4

    invoke-static {p1, v8, v9}, Lorg/apache/lucene/codecs/CodecUtil;->writeHeader(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;I)V

    .line 471
    iget-boolean v8, p0, Lorg/apache/lucene/util/fst/FST;->packed:Z

    if-eqz v8, :cond_5

    .line 472
    invoke-virtual {p1, v13}, Lorg/apache/lucene/store/DataOutput;->writeByte(B)V

    .line 478
    :goto_0
    iget-object v8, p0, Lorg/apache/lucene/util/fst/FST;->emptyOutput:Ljava/lang/Object;

    if-eqz v8, :cond_7

    .line 480
    invoke-virtual {p1, v13}, Lorg/apache/lucene/store/DataOutput;->writeByte(B)V

    .line 483
    new-instance v4, Lorg/apache/lucene/store/RAMOutputStream;

    invoke-direct {v4}, Lorg/apache/lucene/store/RAMOutputStream;-><init>()V

    .line 484
    .local v4, "ros":Lorg/apache/lucene/store/RAMOutputStream;
    iget-object v8, p0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    iget-object v9, p0, Lorg/apache/lucene/util/fst/FST;->emptyOutput:Ljava/lang/Object;

    invoke-virtual {v8, v9, v4}, Lorg/apache/lucene/util/fst/Outputs;->writeFinalOutput(Ljava/lang/Object;Lorg/apache/lucene/store/DataOutput;)V

    .line 486
    invoke-virtual {v4}, Lorg/apache/lucene/store/RAMOutputStream;->getFilePointer()J

    move-result-wide v8

    long-to-int v8, v8

    new-array v1, v8, [B

    .line 487
    .local v1, "emptyOutputBytes":[B
    invoke-virtual {v4, v1, v12}, Lorg/apache/lucene/store/RAMOutputStream;->writeTo([BI)V

    .line 489
    iget-boolean v8, p0, Lorg/apache/lucene/util/fst/FST;->packed:Z

    if-nez v8, :cond_3

    .line 491
    array-length v8, v1

    div-int/lit8 v5, v8, 0x2

    .line 492
    .local v5, "stopAt":I
    const/4 v7, 0x0

    .line 493
    .local v7, "upto":I
    :goto_1
    if-lt v7, v5, :cond_6

    .line 500
    .end local v5    # "stopAt":I
    .end local v7    # "upto":I
    :cond_3
    array-length v8, v1

    invoke-virtual {p1, v8}, Lorg/apache/lucene/store/DataOutput;->writeVInt(I)V

    .line 501
    array-length v8, v1

    invoke-virtual {p1, v1, v12, v8}, Lorg/apache/lucene/store/DataOutput;->writeBytes([BII)V

    .line 506
    .end local v1    # "emptyOutputBytes":[B
    .end local v4    # "ros":Lorg/apache/lucene/store/RAMOutputStream;
    :goto_2
    iget-object v8, p0, Lorg/apache/lucene/util/fst/FST;->inputType:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    sget-object v9, Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;->BYTE1:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    if-ne v8, v9, :cond_8

    .line 507
    const/4 v6, 0x0

    .line 513
    .local v6, "t":B
    :goto_3
    invoke-virtual {p1, v6}, Lorg/apache/lucene/store/DataOutput;->writeByte(B)V

    .line 514
    iget-boolean v8, p0, Lorg/apache/lucene/util/fst/FST;->packed:Z

    if-eqz v8, :cond_4

    .line 515
    iget-object v8, p0, Lorg/apache/lucene/util/fst/FST;->nodeRefToAddress:Lorg/apache/lucene/util/packed/PackedInts$Reader;

    check-cast v8, Lorg/apache/lucene/util/packed/PackedInts$Mutable;

    invoke-interface {v8, p1}, Lorg/apache/lucene/util/packed/PackedInts$Mutable;->save(Lorg/apache/lucene/store/DataOutput;)V

    .line 517
    :cond_4
    iget-wide v8, p0, Lorg/apache/lucene/util/fst/FST;->startNode:J

    invoke-virtual {p1, v8, v9}, Lorg/apache/lucene/store/DataOutput;->writeVLong(J)V

    .line 518
    iget-wide v8, p0, Lorg/apache/lucene/util/fst/FST;->nodeCount:J

    invoke-virtual {p1, v8, v9}, Lorg/apache/lucene/store/DataOutput;->writeVLong(J)V

    .line 519
    iget-wide v8, p0, Lorg/apache/lucene/util/fst/FST;->arcCount:J

    invoke-virtual {p1, v8, v9}, Lorg/apache/lucene/store/DataOutput;->writeVLong(J)V

    .line 520
    iget-wide v8, p0, Lorg/apache/lucene/util/fst/FST;->arcWithOutputCount:J

    invoke-virtual {p1, v8, v9}, Lorg/apache/lucene/store/DataOutput;->writeVLong(J)V

    .line 521
    iget-object v8, p0, Lorg/apache/lucene/util/fst/FST;->bytes:Lorg/apache/lucene/util/fst/BytesStore;

    invoke-virtual {v8}, Lorg/apache/lucene/util/fst/BytesStore;->getPosition()J

    move-result-wide v2

    .line 522
    .local v2, "numBytes":J
    invoke-virtual {p1, v2, v3}, Lorg/apache/lucene/store/DataOutput;->writeVLong(J)V

    .line 523
    iget-object v8, p0, Lorg/apache/lucene/util/fst/FST;->bytes:Lorg/apache/lucene/util/fst/BytesStore;

    invoke-virtual {v8, p1}, Lorg/apache/lucene/util/fst/BytesStore;->writeTo(Lorg/apache/lucene/store/DataOutput;)V

    .line 524
    return-void

    .line 474
    .end local v2    # "numBytes":J
    .end local v6    # "t":B
    :cond_5
    invoke-virtual {p1, v12}, Lorg/apache/lucene/store/DataOutput;->writeByte(B)V

    goto :goto_0

    .line 494
    .restart local v1    # "emptyOutputBytes":[B
    .restart local v4    # "ros":Lorg/apache/lucene/store/RAMOutputStream;
    .restart local v5    # "stopAt":I
    .restart local v7    # "upto":I
    :cond_6
    aget-byte v0, v1, v7

    .line 495
    .local v0, "b":B
    array-length v8, v1

    sub-int/2addr v8, v7

    add-int/lit8 v8, v8, -0x1

    aget-byte v8, v1, v8

    aput-byte v8, v1, v7

    .line 496
    array-length v8, v1

    sub-int/2addr v8, v7

    add-int/lit8 v8, v8, -0x1

    aput-byte v0, v1, v8

    .line 497
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 503
    .end local v0    # "b":B
    .end local v1    # "emptyOutputBytes":[B
    .end local v4    # "ros":Lorg/apache/lucene/store/RAMOutputStream;
    .end local v5    # "stopAt":I
    .end local v7    # "upto":I
    :cond_7
    invoke-virtual {p1, v12}, Lorg/apache/lucene/store/DataOutput;->writeByte(B)V

    goto :goto_2

    .line 508
    :cond_8
    iget-object v8, p0, Lorg/apache/lucene/util/fst/FST;->inputType:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    sget-object v9, Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;->BYTE2:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    if-ne v8, v9, :cond_9

    .line 509
    const/4 v6, 0x1

    .line 510
    .restart local v6    # "t":B
    goto :goto_3

    .line 511
    .end local v6    # "t":B
    :cond_9
    const/4 v6, 0x2

    .restart local v6    # "t":B
    goto :goto_3
.end method

.method setEmptyOutput(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 453
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    .local p1, "v":Ljava/lang/Object;, "TT;"
    iget-object v0, p0, Lorg/apache/lucene/util/fst/FST;->emptyOutput:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 454
    iget-object v0, p0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    iget-object v1, p0, Lorg/apache/lucene/util/fst/FST;->emptyOutput:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lorg/apache/lucene/util/fst/Outputs;->merge(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/util/fst/FST;->emptyOutput:Ljava/lang/Object;

    .line 458
    :goto_0
    return-void

    .line 456
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/util/fst/FST;->emptyOutput:Ljava/lang/Object;

    goto :goto_0
.end method

.method public sizeInBytes()J
    .locals 4

    .prologue
    .line 391
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    iget-object v2, p0, Lorg/apache/lucene/util/fst/FST;->bytes:Lorg/apache/lucene/util/fst/BytesStore;

    invoke-virtual {v2}, Lorg/apache/lucene/util/fst/BytesStore;->getPosition()J

    move-result-wide v0

    .line 392
    .local v0, "size":J
    iget-boolean v2, p0, Lorg/apache/lucene/util/fst/FST;->packed:Z

    if-eqz v2, :cond_1

    .line 393
    iget-object v2, p0, Lorg/apache/lucene/util/fst/FST;->nodeRefToAddress:Lorg/apache/lucene/util/packed/PackedInts$Reader;

    invoke-interface {v2}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->ramBytesUsed()J

    move-result-wide v2

    add-long/2addr v0, v2

    .line 398
    :cond_0
    :goto_0
    return-wide v0

    .line 394
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/util/fst/FST;->nodeAddress:Lorg/apache/lucene/util/packed/GrowableWriter;

    if-eqz v2, :cond_0

    .line 395
    iget-object v2, p0, Lorg/apache/lucene/util/fst/FST;->nodeAddress:Lorg/apache/lucene/util/packed/GrowableWriter;

    invoke-virtual {v2}, Lorg/apache/lucene/util/packed/GrowableWriter;->ramBytesUsed()J

    move-result-wide v2

    add-long/2addr v0, v2

    .line 396
    iget-object v2, p0, Lorg/apache/lucene/util/fst/FST;->inCounts:Lorg/apache/lucene/util/packed/GrowableWriter;

    invoke-virtual {v2}, Lorg/apache/lucene/util/packed/GrowableWriter;->ramBytesUsed()J

    move-result-wide v2

    add-long/2addr v0, v2

    goto :goto_0
.end method
