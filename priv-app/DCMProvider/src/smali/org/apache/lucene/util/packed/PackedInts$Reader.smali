.class public interface abstract Lorg/apache/lucene/util/packed/PackedInts$Reader;
.super Ljava/lang/Object;
.source "PackedInts.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/packed/PackedInts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Reader"
.end annotation


# virtual methods
.method public abstract get(I[JII)I
.end method

.method public abstract get(I)J
.end method

.method public abstract getArray()Ljava/lang/Object;
.end method

.method public abstract getBitsPerValue()I
.end method

.method public abstract hasArray()Z
.end method

.method public abstract ramBytesUsed()J
.end method

.method public abstract size()I
.end method
