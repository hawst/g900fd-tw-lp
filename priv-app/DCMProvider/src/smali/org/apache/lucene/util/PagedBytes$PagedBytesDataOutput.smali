.class public final Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;
.super Lorg/apache/lucene/store/DataOutput;
.source "PagedBytes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/PagedBytes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "PagedBytesDataOutput"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/util/PagedBytes;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 323
    const-class v0, Lorg/apache/lucene/util/PagedBytes;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/util/PagedBytes;)V
    .locals 0

    .prologue
    .line 323
    iput-object p1, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    invoke-direct {p0}, Lorg/apache/lucene/store/DataOutput;-><init>()V

    return-void
.end method


# virtual methods
.method public getPosition()J
    .locals 2

    .prologue
    .line 375
    iget-object v0, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    invoke-virtual {v0}, Lorg/apache/lucene/util/PagedBytes;->getPointer()J

    move-result-wide v0

    return-wide v0
.end method

.method public writeByte(B)V
    .locals 4
    .param p1, "b"    # B

    .prologue
    .line 326
    iget-object v0, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->upto:I
    invoke-static {v0}, Lorg/apache/lucene/util/PagedBytes;->access$5(Lorg/apache/lucene/util/PagedBytes;)I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->blockSize:I
    invoke-static {v1}, Lorg/apache/lucene/util/PagedBytes;->access$4(Lorg/apache/lucene/util/PagedBytes;)I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 327
    iget-object v0, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->currentBlock:[B
    invoke-static {v0}, Lorg/apache/lucene/util/PagedBytes;->access$6(Lorg/apache/lucene/util/PagedBytes;)[B

    move-result-object v0

    if-eqz v0, :cond_0

    .line 328
    iget-object v0, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->blocks:Ljava/util/List;
    invoke-static {v0}, Lorg/apache/lucene/util/PagedBytes;->access$0(Lorg/apache/lucene/util/PagedBytes;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->currentBlock:[B
    invoke-static {v1}, Lorg/apache/lucene/util/PagedBytes;->access$6(Lorg/apache/lucene/util/PagedBytes;)[B

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 329
    iget-object v0, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->blockEnd:Ljava/util/List;
    invoke-static {v0}, Lorg/apache/lucene/util/PagedBytes;->access$1(Lorg/apache/lucene/util/PagedBytes;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->upto:I
    invoke-static {v1}, Lorg/apache/lucene/util/PagedBytes;->access$5(Lorg/apache/lucene/util/PagedBytes;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 331
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    iget-object v1, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->blockSize:I
    invoke-static {v1}, Lorg/apache/lucene/util/PagedBytes;->access$4(Lorg/apache/lucene/util/PagedBytes;)I

    move-result v1

    new-array v1, v1, [B

    invoke-static {v0, v1}, Lorg/apache/lucene/util/PagedBytes;->access$7(Lorg/apache/lucene/util/PagedBytes;[B)V

    .line 332
    iget-object v0, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lorg/apache/lucene/util/PagedBytes;->access$8(Lorg/apache/lucene/util/PagedBytes;I)V

    .line 334
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->currentBlock:[B
    invoke-static {v0}, Lorg/apache/lucene/util/PagedBytes;->access$6(Lorg/apache/lucene/util/PagedBytes;)[B

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->upto:I
    invoke-static {v1}, Lorg/apache/lucene/util/PagedBytes;->access$5(Lorg/apache/lucene/util/PagedBytes;)I

    move-result v2

    add-int/lit8 v3, v2, 0x1

    invoke-static {v1, v3}, Lorg/apache/lucene/util/PagedBytes;->access$8(Lorg/apache/lucene/util/PagedBytes;I)V

    aput-byte p1, v0, v2

    .line 335
    return-void
.end method

.method public writeBytes([BII)V
    .locals 6
    .param p1, "b"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    const/4 v5, 0x0

    .line 339
    sget-boolean v3, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    array-length v3, p1

    add-int v4, p2, p3

    if-ge v3, v4, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 340
    :cond_0
    if-nez p3, :cond_1

    .line 371
    :goto_0
    return-void

    .line 344
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->upto:I
    invoke-static {v3}, Lorg/apache/lucene/util/PagedBytes;->access$5(Lorg/apache/lucene/util/PagedBytes;)I

    move-result v3

    iget-object v4, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->blockSize:I
    invoke-static {v4}, Lorg/apache/lucene/util/PagedBytes;->access$4(Lorg/apache/lucene/util/PagedBytes;)I

    move-result v4

    if-ne v3, v4, :cond_3

    .line 345
    iget-object v3, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->currentBlock:[B
    invoke-static {v3}, Lorg/apache/lucene/util/PagedBytes;->access$6(Lorg/apache/lucene/util/PagedBytes;)[B

    move-result-object v3

    if-eqz v3, :cond_2

    .line 346
    iget-object v3, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->blocks:Ljava/util/List;
    invoke-static {v3}, Lorg/apache/lucene/util/PagedBytes;->access$0(Lorg/apache/lucene/util/PagedBytes;)Ljava/util/List;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->currentBlock:[B
    invoke-static {v4}, Lorg/apache/lucene/util/PagedBytes;->access$6(Lorg/apache/lucene/util/PagedBytes;)[B

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 347
    iget-object v3, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->blockEnd:Ljava/util/List;
    invoke-static {v3}, Lorg/apache/lucene/util/PagedBytes;->access$1(Lorg/apache/lucene/util/PagedBytes;)Ljava/util/List;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->upto:I
    invoke-static {v4}, Lorg/apache/lucene/util/PagedBytes;->access$5(Lorg/apache/lucene/util/PagedBytes;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 349
    :cond_2
    iget-object v3, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    iget-object v4, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->blockSize:I
    invoke-static {v4}, Lorg/apache/lucene/util/PagedBytes;->access$4(Lorg/apache/lucene/util/PagedBytes;)I

    move-result v4

    new-array v4, v4, [B

    invoke-static {v3, v4}, Lorg/apache/lucene/util/PagedBytes;->access$7(Lorg/apache/lucene/util/PagedBytes;[B)V

    .line 350
    iget-object v3, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    invoke-static {v3, v5}, Lorg/apache/lucene/util/PagedBytes;->access$8(Lorg/apache/lucene/util/PagedBytes;I)V

    .line 353
    :cond_3
    add-int v2, p2, p3

    .line 355
    .local v2, "offsetEnd":I
    :goto_1
    sub-int v1, v2, p2

    .line 356
    .local v1, "left":I
    iget-object v3, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->blockSize:I
    invoke-static {v3}, Lorg/apache/lucene/util/PagedBytes;->access$4(Lorg/apache/lucene/util/PagedBytes;)I

    move-result v3

    iget-object v4, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->upto:I
    invoke-static {v4}, Lorg/apache/lucene/util/PagedBytes;->access$5(Lorg/apache/lucene/util/PagedBytes;)I

    move-result v4

    sub-int v0, v3, v4

    .line 357
    .local v0, "blockLeft":I
    if-ge v0, v1, :cond_4

    .line 358
    iget-object v3, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->currentBlock:[B
    invoke-static {v3}, Lorg/apache/lucene/util/PagedBytes;->access$6(Lorg/apache/lucene/util/PagedBytes;)[B

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->upto:I
    invoke-static {v4}, Lorg/apache/lucene/util/PagedBytes;->access$5(Lorg/apache/lucene/util/PagedBytes;)I

    move-result v4

    invoke-static {p1, p2, v3, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 359
    iget-object v3, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->blocks:Ljava/util/List;
    invoke-static {v3}, Lorg/apache/lucene/util/PagedBytes;->access$0(Lorg/apache/lucene/util/PagedBytes;)Ljava/util/List;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->currentBlock:[B
    invoke-static {v4}, Lorg/apache/lucene/util/PagedBytes;->access$6(Lorg/apache/lucene/util/PagedBytes;)[B

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 360
    iget-object v3, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->blockEnd:Ljava/util/List;
    invoke-static {v3}, Lorg/apache/lucene/util/PagedBytes;->access$1(Lorg/apache/lucene/util/PagedBytes;)Ljava/util/List;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->blockSize:I
    invoke-static {v4}, Lorg/apache/lucene/util/PagedBytes;->access$4(Lorg/apache/lucene/util/PagedBytes;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 361
    iget-object v3, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    iget-object v4, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->blockSize:I
    invoke-static {v4}, Lorg/apache/lucene/util/PagedBytes;->access$4(Lorg/apache/lucene/util/PagedBytes;)I

    move-result v4

    new-array v4, v4, [B

    invoke-static {v3, v4}, Lorg/apache/lucene/util/PagedBytes;->access$7(Lorg/apache/lucene/util/PagedBytes;[B)V

    .line 362
    iget-object v3, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    invoke-static {v3, v5}, Lorg/apache/lucene/util/PagedBytes;->access$8(Lorg/apache/lucene/util/PagedBytes;I)V

    .line 363
    add-int/2addr p2, v0

    .line 364
    goto :goto_1

    .line 366
    :cond_4
    iget-object v3, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->currentBlock:[B
    invoke-static {v3}, Lorg/apache/lucene/util/PagedBytes;->access$6(Lorg/apache/lucene/util/PagedBytes;)[B

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->upto:I
    invoke-static {v4}, Lorg/apache/lucene/util/PagedBytes;->access$5(Lorg/apache/lucene/util/PagedBytes;)I

    move-result v4

    invoke-static {p1, p2, v3, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 367
    iget-object v3, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->upto:I
    invoke-static {v3}, Lorg/apache/lucene/util/PagedBytes;->access$5(Lorg/apache/lucene/util/PagedBytes;)I

    move-result v4

    add-int/2addr v4, v1

    invoke-static {v3, v4}, Lorg/apache/lucene/util/PagedBytes;->access$8(Lorg/apache/lucene/util/PagedBytes;I)V

    goto/16 :goto_0
.end method
