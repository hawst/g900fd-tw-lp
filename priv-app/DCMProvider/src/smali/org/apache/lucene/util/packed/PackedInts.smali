.class public Lorg/apache/lucene/util/packed/PackedInts;
.super Ljava/lang/Object;
.source "PackedInts.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/packed/PackedInts$Decoder;,
        Lorg/apache/lucene/util/packed/PackedInts$Encoder;,
        Lorg/apache/lucene/util/packed/PackedInts$Format;,
        Lorg/apache/lucene/util/packed/PackedInts$FormatAndBits;,
        Lorg/apache/lucene/util/packed/PackedInts$Header;,
        Lorg/apache/lucene/util/packed/PackedInts$Mutable;,
        Lorg/apache/lucene/util/packed/PackedInts$MutableImpl;,
        Lorg/apache/lucene/util/packed/PackedInts$NullReader;,
        Lorg/apache/lucene/util/packed/PackedInts$Reader;,
        Lorg/apache/lucene/util/packed/PackedInts$ReaderImpl;,
        Lorg/apache/lucene/util/packed/PackedInts$ReaderIterator;,
        Lorg/apache/lucene/util/packed/PackedInts$ReaderIteratorImpl;,
        Lorg/apache/lucene/util/packed/PackedInts$Writer;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$org$apache$lucene$util$packed$PackedInts$Format:[I = null

.field static final synthetic $assertionsDisabled:Z

.field public static final CODEC_NAME:Ljava/lang/String; = "PackedInts"

.field public static final COMPACT:F = 0.0f

.field public static final DEFAULT:F = 0.2f

.field public static final DEFAULT_BUFFER_SIZE:I = 0x400

.field public static final FAST:F = 0.5f

.field public static final FASTEST:F = 7.0f

.field public static final VERSION_BYTE_ALIGNED:I = 0x1

.field public static final VERSION_CURRENT:I = 0x1

.field public static final VERSION_START:I


# direct methods
.method static synthetic $SWITCH_TABLE$org$apache$lucene$util$packed$PackedInts$Format()[I
    .locals 3

    .prologue
    .line 36
    sget-object v0, Lorg/apache/lucene/util/packed/PackedInts;->$SWITCH_TABLE$org$apache$lucene$util$packed$PackedInts$Format:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lorg/apache/lucene/util/packed/PackedInts$Format;->values()[Lorg/apache/lucene/util/packed/PackedInts$Format;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lorg/apache/lucene/util/packed/PackedInts$Format;->PACKED:Lorg/apache/lucene/util/packed/PackedInts$Format;

    invoke-virtual {v1}, Lorg/apache/lucene/util/packed/PackedInts$Format;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_1

    :goto_1
    :try_start_1
    sget-object v1, Lorg/apache/lucene/util/packed/PackedInts$Format;->PACKED_SINGLE_BLOCK:Lorg/apache/lucene/util/packed/PackedInts$Format;

    invoke-virtual {v1}, Lorg/apache/lucene/util/packed/PackedInts$Format;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_0

    :goto_2
    sput-object v0, Lorg/apache/lucene/util/packed/PackedInts;->$SWITCH_TABLE$org$apache$lucene$util$packed$PackedInts$Format:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_2

    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lorg/apache/lucene/util/packed/PackedInts;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/packed/PackedInts;->$assertionsDisabled:Z

    .line 66
    return-void

    .line 36
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static bitsRequired(J)I
    .locals 4
    .param p0, "maxValue"    # J

    .prologue
    .line 1171
    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-gez v0, :cond_0

    .line 1172
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "maxValue must be non-negative (got: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1174
    :cond_0
    const/4 v0, 0x1

    invoke-static {p0, p1}, Ljava/lang/Long;->numberOfLeadingZeros(J)I

    move-result v1

    rsub-int/lit8 v1, v1, 0x40

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public static checkVersion(I)V
    .locals 3
    .param p0, "version"    # I

    .prologue
    .line 72
    if-gez p0, :cond_0

    .line 73
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Version is too old, should be at least 0 (got "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 74
    :cond_0
    const/4 v0, 0x1

    if-le p0, v0, :cond_1

    .line 75
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Version is too new, should be at most 1 (got "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 77
    :cond_1
    return-void
.end method

.method public static copy(Lorg/apache/lucene/util/packed/PackedInts$Reader;ILorg/apache/lucene/util/packed/PackedInts$Mutable;III)V
    .locals 10
    .param p0, "src"    # Lorg/apache/lucene/util/packed/PackedInts$Reader;
    .param p1, "srcPos"    # I
    .param p2, "dest"    # Lorg/apache/lucene/util/packed/PackedInts$Mutable;
    .param p3, "destPos"    # I
    .param p4, "len"    # I
    .param p5, "mem"    # I

    .prologue
    .line 1194
    sget-boolean v8, Lorg/apache/lucene/util/packed/PackedInts;->$assertionsDisabled:Z

    if-nez v8, :cond_0

    add-int v8, p1, p4

    invoke-interface {p0}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->size()I

    move-result v9

    if-le v8, v9, :cond_0

    new-instance v8, Ljava/lang/AssertionError;

    invoke-direct {v8}, Ljava/lang/AssertionError;-><init>()V

    throw v8

    .line 1195
    :cond_0
    sget-boolean v8, Lorg/apache/lucene/util/packed/PackedInts;->$assertionsDisabled:Z

    if-nez v8, :cond_1

    add-int v8, p3, p4

    invoke-interface {p2}, Lorg/apache/lucene/util/packed/PackedInts$Mutable;->size()I

    move-result v9

    if-le v8, v9, :cond_1

    new-instance v8, Ljava/lang/AssertionError;

    invoke-direct {v8}, Ljava/lang/AssertionError;-><init>()V

    throw v8

    .line 1196
    :cond_1
    ushr-int/lit8 v1, p5, 0x3

    .line 1197
    .local v1, "capacity":I
    if-nez v1, :cond_4

    .line 1198
    const/4 v3, 0x0

    .local v3, "i":I
    move v2, p3

    .end local p3    # "destPos":I
    .local v2, "destPos":I
    move v6, p1

    .end local p1    # "srcPos":I
    .local v6, "srcPos":I
    :goto_0
    if-lt v3, p4, :cond_3

    move p3, v2

    .end local v2    # "destPos":I
    .restart local p3    # "destPos":I
    move p1, v6

    .line 1226
    .end local v3    # "i":I
    .end local v6    # "srcPos":I
    .restart local p1    # "srcPos":I
    :cond_2
    return-void

    .line 1199
    .end local p1    # "srcPos":I
    .end local p3    # "destPos":I
    .restart local v2    # "destPos":I
    .restart local v3    # "i":I
    .restart local v6    # "srcPos":I
    :cond_3
    add-int/lit8 p3, v2, 0x1

    .end local v2    # "destPos":I
    .restart local p3    # "destPos":I
    add-int/lit8 p1, v6, 0x1

    .end local v6    # "srcPos":I
    .restart local p1    # "srcPos":I
    invoke-interface {p0, v6}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I)J

    move-result-wide v8

    invoke-interface {p2, v2, v8, v9}, Lorg/apache/lucene/util/packed/PackedInts$Mutable;->set(IJ)V

    .line 1198
    add-int/lit8 v3, v3, 0x1

    move v2, p3

    .end local p3    # "destPos":I
    .restart local v2    # "destPos":I
    move v6, p1

    .end local p1    # "srcPos":I
    .restart local v6    # "srcPos":I
    goto :goto_0

    .line 1203
    .end local v2    # "destPos":I
    .end local v3    # "i":I
    .end local v6    # "srcPos":I
    .restart local p1    # "srcPos":I
    .restart local p3    # "destPos":I
    :cond_4
    invoke-static {v1, p4}, Ljava/lang/Math;->min(II)I

    move-result v8

    new-array v0, v8, [J

    .line 1204
    .local v0, "buf":[J
    const/4 v5, 0x0

    .line 1205
    .local v5, "remaining":I
    :goto_1
    if-gtz p4, :cond_5

    .line 1219
    :goto_2
    if-lez v5, :cond_2

    .line 1220
    const/4 v8, 0x0

    invoke-interface {p2, p3, v0, v8, v5}, Lorg/apache/lucene/util/packed/PackedInts$Mutable;->set(I[JII)I

    move-result v7

    .line 1221
    .local v7, "written":I
    add-int/2addr p3, v7

    .line 1222
    sub-int/2addr v5, v7

    .line 1223
    const/4 v8, 0x0

    invoke-static {v0, v7, v0, v8, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_2

    .line 1206
    .end local v7    # "written":I
    :cond_5
    array-length v8, v0

    sub-int/2addr v8, v5

    invoke-static {p4, v8}, Ljava/lang/Math;->min(II)I

    move-result v8

    invoke-interface {p0, p1, v0, v5, v8}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I[JII)I

    move-result v4

    .line 1207
    .local v4, "read":I
    sget-boolean v8, Lorg/apache/lucene/util/packed/PackedInts;->$assertionsDisabled:Z

    if-nez v8, :cond_6

    if-gtz v4, :cond_6

    new-instance v8, Ljava/lang/AssertionError;

    invoke-direct {v8}, Ljava/lang/AssertionError;-><init>()V

    throw v8

    .line 1208
    :cond_6
    add-int/2addr p1, v4

    .line 1209
    sub-int/2addr p4, v4

    .line 1210
    add-int/2addr v5, v4

    .line 1211
    const/4 v8, 0x0

    invoke-interface {p2, p3, v0, v8, v5}, Lorg/apache/lucene/util/packed/PackedInts$Mutable;->set(I[JII)I

    move-result v7

    .line 1212
    .restart local v7    # "written":I
    sget-boolean v8, Lorg/apache/lucene/util/packed/PackedInts;->$assertionsDisabled:Z

    if-nez v8, :cond_7

    if-gtz v7, :cond_7

    new-instance v8, Ljava/lang/AssertionError;

    invoke-direct {v8}, Ljava/lang/AssertionError;-><init>()V

    throw v8

    .line 1213
    :cond_7
    add-int/2addr p3, v7

    .line 1214
    if-ge v7, v5, :cond_8

    .line 1215
    const/4 v8, 0x0

    sub-int v9, v5, v7

    invoke-static {v0, v7, v0, v8, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1217
    :cond_8
    sub-int/2addr v5, v7

    goto :goto_1
.end method

.method public static fastestFormatAndBits(IIF)Lorg/apache/lucene/util/packed/PackedInts$FormatAndBits;
    .locals 13
    .param p0, "valueCount"    # I
    .param p1, "bitsPerValue"    # I
    .param p2, "acceptableOverheadRatio"    # F

    .prologue
    const/16 v12, 0x30

    const/16 v11, 0x20

    const/16 v10, 0x18

    const/16 v9, 0x10

    const/16 v8, 0x8

    .line 232
    const/4 v7, -0x1

    if-ne p0, v7, :cond_0

    .line 233
    const p0, 0x7fffffff

    .line 236
    :cond_0
    const/4 v7, 0x0

    invoke-static {v7, p2}, Ljava/lang/Math;->max(FF)F

    move-result p2

    .line 237
    const/high16 v7, 0x40e00000    # 7.0f

    invoke-static {v7, p2}, Ljava/lang/Math;->min(FF)F

    move-result p2

    .line 238
    int-to-float v7, p1

    mul-float v1, p2, v7

    .line 240
    .local v1, "acceptableOverheadPerValue":F
    float-to-int v7, v1

    add-int v5, p1, v7

    .line 242
    .local v5, "maxBitsPerValue":I
    const/4 v2, -0x1

    .line 243
    .local v2, "actualBitsPerValue":I
    sget-object v4, Lorg/apache/lucene/util/packed/PackedInts$Format;->PACKED:Lorg/apache/lucene/util/packed/PackedInts$Format;

    .line 245
    .local v4, "format":Lorg/apache/lucene/util/packed/PackedInts$Format;
    if-gt p1, v8, :cond_2

    if-lt v5, v8, :cond_2

    .line 246
    const/16 v2, 0x8

    .line 274
    :cond_1
    :goto_0
    new-instance v7, Lorg/apache/lucene/util/packed/PackedInts$FormatAndBits;

    invoke-direct {v7, v4, v2}, Lorg/apache/lucene/util/packed/PackedInts$FormatAndBits;-><init>(Lorg/apache/lucene/util/packed/PackedInts$Format;I)V

    return-object v7

    .line 247
    :cond_2
    if-gt p1, v9, :cond_3

    if-lt v5, v9, :cond_3

    .line 248
    const/16 v2, 0x10

    .line 249
    goto :goto_0

    :cond_3
    if-gt p1, v11, :cond_4

    if-lt v5, v11, :cond_4

    .line 250
    const/16 v2, 0x20

    .line 251
    goto :goto_0

    :cond_4
    const/16 v7, 0x40

    if-gt p1, v7, :cond_5

    const/16 v7, 0x40

    if-lt v5, v7, :cond_5

    .line 252
    const/16 v2, 0x40

    .line 253
    goto :goto_0

    :cond_5
    const v7, 0x2aaaaaaa

    if-gt p0, v7, :cond_6

    if-gt p1, v10, :cond_6

    if-lt v5, v10, :cond_6

    .line 254
    const/16 v2, 0x18

    .line 255
    goto :goto_0

    :cond_6
    const v7, 0x2aaaaaaa

    if-gt p0, v7, :cond_7

    if-gt p1, v12, :cond_7

    if-lt v5, v12, :cond_7

    .line 256
    const/16 v2, 0x30

    .line 257
    goto :goto_0

    .line 258
    :cond_7
    move v3, p1

    .local v3, "bpv":I
    :goto_1
    if-le v3, v5, :cond_8

    .line 269
    :goto_2
    if-gez v2, :cond_1

    .line 270
    move v2, p1

    goto :goto_0

    .line 259
    :cond_8
    sget-object v7, Lorg/apache/lucene/util/packed/PackedInts$Format;->PACKED_SINGLE_BLOCK:Lorg/apache/lucene/util/packed/PackedInts$Format;

    invoke-virtual {v7, v3}, Lorg/apache/lucene/util/packed/PackedInts$Format;->isSupported(I)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 260
    sget-object v7, Lorg/apache/lucene/util/packed/PackedInts$Format;->PACKED_SINGLE_BLOCK:Lorg/apache/lucene/util/packed/PackedInts$Format;

    invoke-virtual {v7, v3}, Lorg/apache/lucene/util/packed/PackedInts$Format;->overheadPerValue(I)F

    move-result v6

    .line 261
    .local v6, "overhead":F
    int-to-float v7, p1

    add-float/2addr v7, v1

    int-to-float v8, v3

    sub-float v0, v7, v8

    .line 262
    .local v0, "acceptableOverhead":F
    cmpg-float v7, v6, v0

    if-gtz v7, :cond_9

    .line 263
    move v2, v3

    .line 264
    sget-object v4, Lorg/apache/lucene/util/packed/PackedInts$Format;->PACKED_SINGLE_BLOCK:Lorg/apache/lucene/util/packed/PackedInts$Format;

    .line 265
    goto :goto_2

    .line 258
    .end local v0    # "acceptableOverhead":F
    .end local v6    # "overhead":F
    :cond_9
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method public static getDecoder(Lorg/apache/lucene/util/packed/PackedInts$Format;II)Lorg/apache/lucene/util/packed/PackedInts$Decoder;
    .locals 1
    .param p0, "format"    # Lorg/apache/lucene/util/packed/PackedInts$Format;
    .param p1, "version"    # I
    .param p2, "bitsPerValue"    # I

    .prologue
    .line 785
    invoke-static {p1}, Lorg/apache/lucene/util/packed/PackedInts;->checkVersion(I)V

    .line 786
    invoke-static {p0, p2}, Lorg/apache/lucene/util/packed/BulkOperation;->of(Lorg/apache/lucene/util/packed/PackedInts$Format;I)Lorg/apache/lucene/util/packed/BulkOperation;

    move-result-object v0

    return-object v0
.end method

.method public static getDirectReader(Lorg/apache/lucene/store/IndexInput;)Lorg/apache/lucene/util/packed/PackedInts$Reader;
    .locals 7
    .param p0, "in"    # Lorg/apache/lucene/store/IndexInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1010
    const-string v4, "PackedInts"

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-static {p0, v4, v5, v6}, Lorg/apache/lucene/codecs/CodecUtil;->checkHeader(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I

    move-result v3

    .line 1011
    .local v3, "version":I
    invoke-virtual {p0}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v0

    .line 1012
    .local v0, "bitsPerValue":I
    sget-boolean v4, Lorg/apache/lucene/util/packed/PackedInts;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    if-lez v0, :cond_0

    const/16 v4, 0x40

    if-le v0, v4, :cond_1

    :cond_0
    new-instance v4, Ljava/lang/AssertionError;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "bitsPerValue="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    .line 1013
    :cond_1
    invoke-virtual {p0}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v2

    .line 1014
    .local v2, "valueCount":I
    invoke-virtual {p0}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v4

    invoke-static {v4}, Lorg/apache/lucene/util/packed/PackedInts$Format;->byId(I)Lorg/apache/lucene/util/packed/PackedInts$Format;

    move-result-object v1

    .line 1015
    .local v1, "format":Lorg/apache/lucene/util/packed/PackedInts$Format;
    invoke-static {p0, v1, v3, v2, v0}, Lorg/apache/lucene/util/packed/PackedInts;->getDirectReaderNoHeader(Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/util/packed/PackedInts$Format;III)Lorg/apache/lucene/util/packed/PackedInts$Reader;

    move-result-object v4

    return-object v4
.end method

.method public static getDirectReaderNoHeader(Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/util/packed/PackedInts$Format;III)Lorg/apache/lucene/util/packed/PackedInts$Reader;
    .locals 10
    .param p0, "in"    # Lorg/apache/lucene/store/IndexInput;
    .param p1, "format"    # Lorg/apache/lucene/util/packed/PackedInts$Format;
    .param p2, "version"    # I
    .param p3, "valueCount"    # I
    .param p4, "bitsPerValue"    # I

    .prologue
    .line 943
    invoke-static {p2}, Lorg/apache/lucene/util/packed/PackedInts;->checkVersion(I)V

    .line 944
    invoke-static {}, Lorg/apache/lucene/util/packed/PackedInts;->$SWITCH_TABLE$org$apache$lucene$util$packed$PackedInts$Format()[I

    move-result-object v0

    invoke-virtual {p1}, Lorg/apache/lucene/util/packed/PackedInts$Format;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 975
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknwown format: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 946
    :pswitch_0
    invoke-virtual {p1, p2, p3, p4}, Lorg/apache/lucene/util/packed/PackedInts$Format;->byteCount(III)J

    move-result-wide v8

    .line 947
    .local v8, "byteCount":J
    const/4 v0, 0x1

    invoke-virtual {p1, v0, p3, p4}, Lorg/apache/lucene/util/packed/PackedInts$Format;->byteCount(III)J

    move-result-wide v0

    cmp-long v0, v8, v0

    if-eqz v0, :cond_1

    .line 948
    sget-boolean v0, Lorg/apache/lucene/util/packed/PackedInts;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-eqz p2, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 949
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v0

    add-long v6, v0, v8

    .line 955
    .local v6, "endPointer":J
    new-instance v1, Lorg/apache/lucene/util/packed/PackedInts$1;

    move v2, p4

    move v3, p3

    move-object v4, p0

    move-object v5, p0

    invoke-direct/range {v1 .. v7}, Lorg/apache/lucene/util/packed/PackedInts$1;-><init>(IILorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/store/IndexInput;J)V

    .line 973
    .end local v6    # "endPointer":J
    .end local v8    # "byteCount":J
    :goto_0
    return-object v1

    .line 970
    .restart local v8    # "byteCount":J
    :cond_1
    new-instance v1, Lorg/apache/lucene/util/packed/DirectPackedReader;

    invoke-direct {v1, p4, p3, p0}, Lorg/apache/lucene/util/packed/DirectPackedReader;-><init>(IILorg/apache/lucene/store/IndexInput;)V

    goto :goto_0

    .line 973
    .end local v8    # "byteCount":J
    :pswitch_1
    new-instance v1, Lorg/apache/lucene/util/packed/DirectPacked64SingleBlockReader;

    invoke-direct {v1, p4, p3, p0}, Lorg/apache/lucene/util/packed/DirectPacked64SingleBlockReader;-><init>(IILorg/apache/lucene/store/IndexInput;)V

    goto :goto_0

    .line 944
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static getDirectReaderNoHeader(Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/util/packed/PackedInts$Header;)Lorg/apache/lucene/util/packed/PackedInts$Reader;
    .locals 4
    .param p0, "in"    # Lorg/apache/lucene/store/IndexInput;
    .param p1, "header"    # Lorg/apache/lucene/util/packed/PackedInts$Header;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 993
    # getter for: Lorg/apache/lucene/util/packed/PackedInts$Header;->format:Lorg/apache/lucene/util/packed/PackedInts$Format;
    invoke-static {p1}, Lorg/apache/lucene/util/packed/PackedInts$Header;->access$0(Lorg/apache/lucene/util/packed/PackedInts$Header;)Lorg/apache/lucene/util/packed/PackedInts$Format;

    move-result-object v0

    # getter for: Lorg/apache/lucene/util/packed/PackedInts$Header;->version:I
    invoke-static {p1}, Lorg/apache/lucene/util/packed/PackedInts$Header;->access$1(Lorg/apache/lucene/util/packed/PackedInts$Header;)I

    move-result v1

    # getter for: Lorg/apache/lucene/util/packed/PackedInts$Header;->valueCount:I
    invoke-static {p1}, Lorg/apache/lucene/util/packed/PackedInts$Header;->access$2(Lorg/apache/lucene/util/packed/PackedInts$Header;)I

    move-result v2

    # getter for: Lorg/apache/lucene/util/packed/PackedInts$Header;->bitsPerValue:I
    invoke-static {p1}, Lorg/apache/lucene/util/packed/PackedInts$Header;->access$3(Lorg/apache/lucene/util/packed/PackedInts$Header;)I

    move-result v3

    invoke-static {p0, v0, v1, v2, v3}, Lorg/apache/lucene/util/packed/PackedInts;->getDirectReaderNoHeader(Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/util/packed/PackedInts$Format;III)Lorg/apache/lucene/util/packed/PackedInts$Reader;

    move-result-object v0

    return-object v0
.end method

.method public static getEncoder(Lorg/apache/lucene/util/packed/PackedInts$Format;II)Lorg/apache/lucene/util/packed/PackedInts$Encoder;
    .locals 1
    .param p0, "format"    # Lorg/apache/lucene/util/packed/PackedInts$Format;
    .param p1, "version"    # I
    .param p2, "bitsPerValue"    # I

    .prologue
    .line 798
    invoke-static {p1}, Lorg/apache/lucene/util/packed/PackedInts;->checkVersion(I)V

    .line 799
    invoke-static {p0, p2}, Lorg/apache/lucene/util/packed/BulkOperation;->of(Lorg/apache/lucene/util/packed/PackedInts$Format;I)Lorg/apache/lucene/util/packed/BulkOperation;

    move-result-object v0

    return-object v0
.end method

.method public static getMutable(IIF)Lorg/apache/lucene/util/packed/PackedInts$Mutable;
    .locals 4
    .param p0, "valueCount"    # I
    .param p1, "bitsPerValue"    # I
    .param p2, "acceptableOverheadRatio"    # F

    .prologue
    const v3, 0x2aaaaaaa

    .line 1039
    sget-boolean v1, Lorg/apache/lucene/util/packed/PackedInts;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-gez p0, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 1041
    :cond_0
    invoke-static {p0, p1, p2}, Lorg/apache/lucene/util/packed/PackedInts;->fastestFormatAndBits(IIF)Lorg/apache/lucene/util/packed/PackedInts$FormatAndBits;

    move-result-object v0

    .line 1042
    .local v0, "formatAndBits":Lorg/apache/lucene/util/packed/PackedInts$FormatAndBits;
    invoke-static {}, Lorg/apache/lucene/util/packed/PackedInts;->$SWITCH_TABLE$org$apache$lucene$util$packed$PackedInts$Format()[I

    move-result-object v1

    iget-object v2, v0, Lorg/apache/lucene/util/packed/PackedInts$FormatAndBits;->format:Lorg/apache/lucene/util/packed/PackedInts$Format;

    invoke-virtual {v2}, Lorg/apache/lucene/util/packed/PackedInts$Format;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1068
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 1044
    :pswitch_0
    iget v1, v0, Lorg/apache/lucene/util/packed/PackedInts$FormatAndBits;->bitsPerValue:I

    invoke-static {p0, v1}, Lorg/apache/lucene/util/packed/Packed64SingleBlock;->create(II)Lorg/apache/lucene/util/packed/Packed64SingleBlock;

    move-result-object v1

    .line 1066
    :goto_0
    return-object v1

    .line 1046
    :pswitch_1
    iget v1, v0, Lorg/apache/lucene/util/packed/PackedInts$FormatAndBits;->bitsPerValue:I

    sparse-switch v1, :sswitch_data_0

    .line 1066
    :cond_1
    new-instance v1, Lorg/apache/lucene/util/packed/Packed64;

    iget v2, v0, Lorg/apache/lucene/util/packed/PackedInts$FormatAndBits;->bitsPerValue:I

    invoke-direct {v1, p0, v2}, Lorg/apache/lucene/util/packed/Packed64;-><init>(II)V

    goto :goto_0

    .line 1048
    :sswitch_0
    new-instance v1, Lorg/apache/lucene/util/packed/Direct8;

    invoke-direct {v1, p0}, Lorg/apache/lucene/util/packed/Direct8;-><init>(I)V

    goto :goto_0

    .line 1050
    :sswitch_1
    new-instance v1, Lorg/apache/lucene/util/packed/Direct16;

    invoke-direct {v1, p0}, Lorg/apache/lucene/util/packed/Direct16;-><init>(I)V

    goto :goto_0

    .line 1052
    :sswitch_2
    new-instance v1, Lorg/apache/lucene/util/packed/Direct32;

    invoke-direct {v1, p0}, Lorg/apache/lucene/util/packed/Direct32;-><init>(I)V

    goto :goto_0

    .line 1054
    :sswitch_3
    new-instance v1, Lorg/apache/lucene/util/packed/Direct64;

    invoke-direct {v1, p0}, Lorg/apache/lucene/util/packed/Direct64;-><init>(I)V

    goto :goto_0

    .line 1056
    :sswitch_4
    if-gt p0, v3, :cond_1

    .line 1057
    new-instance v1, Lorg/apache/lucene/util/packed/Packed8ThreeBlocks;

    invoke-direct {v1, p0}, Lorg/apache/lucene/util/packed/Packed8ThreeBlocks;-><init>(I)V

    goto :goto_0

    .line 1061
    :sswitch_5
    if-gt p0, v3, :cond_1

    .line 1062
    new-instance v1, Lorg/apache/lucene/util/packed/Packed16ThreeBlocks;

    invoke-direct {v1, p0}, Lorg/apache/lucene/util/packed/Packed16ThreeBlocks;-><init>(I)V

    goto :goto_0

    .line 1042
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch

    .line 1046
    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_0
        0x10 -> :sswitch_1
        0x18 -> :sswitch_4
        0x20 -> :sswitch_2
        0x30 -> :sswitch_5
        0x40 -> :sswitch_3
    .end sparse-switch
.end method

.method public static getReader(Lorg/apache/lucene/store/DataInput;)Lorg/apache/lucene/util/packed/PackedInts$Reader;
    .locals 7
    .param p0, "in"    # Lorg/apache/lucene/store/DataInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 876
    const-string v4, "PackedInts"

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-static {p0, v4, v5, v6}, Lorg/apache/lucene/codecs/CodecUtil;->checkHeader(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I

    move-result v3

    .line 877
    .local v3, "version":I
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readVInt()I

    move-result v0

    .line 878
    .local v0, "bitsPerValue":I
    sget-boolean v4, Lorg/apache/lucene/util/packed/PackedInts;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    if-lez v0, :cond_0

    const/16 v4, 0x40

    if-le v0, v4, :cond_1

    :cond_0
    new-instance v4, Ljava/lang/AssertionError;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "bitsPerValue="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    .line 879
    :cond_1
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readVInt()I

    move-result v2

    .line 880
    .local v2, "valueCount":I
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readVInt()I

    move-result v4

    invoke-static {v4}, Lorg/apache/lucene/util/packed/PackedInts$Format;->byId(I)Lorg/apache/lucene/util/packed/PackedInts$Format;

    move-result-object v1

    .line 882
    .local v1, "format":Lorg/apache/lucene/util/packed/PackedInts$Format;
    invoke-static {p0, v1, v3, v2, v0}, Lorg/apache/lucene/util/packed/PackedInts;->getReaderNoHeader(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/packed/PackedInts$Format;III)Lorg/apache/lucene/util/packed/PackedInts$Reader;

    move-result-object v4

    return-object v4
.end method

.method public static getReaderIterator(Lorg/apache/lucene/store/DataInput;I)Lorg/apache/lucene/util/packed/PackedInts$ReaderIterator;
    .locals 7
    .param p0, "in"    # Lorg/apache/lucene/store/DataInput;
    .param p1, "mem"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 916
    const-string v0, "PackedInts"

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-static {p0, v0, v5, v6}, Lorg/apache/lucene/codecs/CodecUtil;->checkHeader(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I

    move-result v2

    .line 917
    .local v2, "version":I
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readVInt()I

    move-result v4

    .line 918
    .local v4, "bitsPerValue":I
    sget-boolean v0, Lorg/apache/lucene/util/packed/PackedInts;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-lez v4, :cond_0

    const/16 v0, 0x40

    if-le v4, v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "bitsPerValue="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 919
    :cond_1
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readVInt()I

    move-result v3

    .line 920
    .local v3, "valueCount":I
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readVInt()I

    move-result v0

    invoke-static {v0}, Lorg/apache/lucene/util/packed/PackedInts$Format;->byId(I)Lorg/apache/lucene/util/packed/PackedInts$Format;

    move-result-object v1

    .local v1, "format":Lorg/apache/lucene/util/packed/PackedInts$Format;
    move-object v0, p0

    move v5, p1

    .line 921
    invoke-static/range {v0 .. v5}, Lorg/apache/lucene/util/packed/PackedInts;->getReaderIteratorNoHeader(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/packed/PackedInts$Format;IIII)Lorg/apache/lucene/util/packed/PackedInts$ReaderIterator;

    move-result-object v0

    return-object v0
.end method

.method public static getReaderIteratorNoHeader(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/packed/PackedInts$Format;IIII)Lorg/apache/lucene/util/packed/PackedInts$ReaderIterator;
    .locals 7
    .param p0, "in"    # Lorg/apache/lucene/store/DataInput;
    .param p1, "format"    # Lorg/apache/lucene/util/packed/PackedInts$Format;
    .param p2, "version"    # I
    .param p3, "valueCount"    # I
    .param p4, "bitsPerValue"    # I
    .param p5, "mem"    # I

    .prologue
    .line 903
    invoke-static {p2}, Lorg/apache/lucene/util/packed/PackedInts;->checkVersion(I)V

    .line 904
    new-instance v0, Lorg/apache/lucene/util/packed/PackedReaderIterator;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p0

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/util/packed/PackedReaderIterator;-><init>(Lorg/apache/lucene/util/packed/PackedInts$Format;IIILorg/apache/lucene/store/DataInput;I)V

    return-object v0
.end method

.method public static getReaderNoHeader(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/packed/PackedInts$Format;III)Lorg/apache/lucene/util/packed/PackedInts$Reader;
    .locals 3
    .param p0, "in"    # Lorg/apache/lucene/store/DataInput;
    .param p1, "format"    # Lorg/apache/lucene/util/packed/PackedInts$Format;
    .param p2, "version"    # I
    .param p3, "valueCount"    # I
    .param p4, "bitsPerValue"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const v2, 0x2aaaaaaa

    .line 820
    invoke-static {p2}, Lorg/apache/lucene/util/packed/PackedInts;->checkVersion(I)V

    .line 821
    invoke-static {}, Lorg/apache/lucene/util/packed/PackedInts;->$SWITCH_TABLE$org$apache$lucene$util$packed$PackedInts$Format()[I

    move-result-object v0

    invoke-virtual {p1}, Lorg/apache/lucene/util/packed/PackedInts$Format;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 847
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown Writer format: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 823
    :pswitch_0
    invoke-static {p0, p3, p4}, Lorg/apache/lucene/util/packed/Packed64SingleBlock;->create(Lorg/apache/lucene/store/DataInput;II)Lorg/apache/lucene/util/packed/Packed64SingleBlock;

    move-result-object v0

    .line 845
    :goto_0
    return-object v0

    .line 825
    :pswitch_1
    sparse-switch p4, :sswitch_data_0

    .line 845
    :cond_0
    new-instance v0, Lorg/apache/lucene/util/packed/Packed64;

    invoke-direct {v0, p2, p0, p3, p4}, Lorg/apache/lucene/util/packed/Packed64;-><init>(ILorg/apache/lucene/store/DataInput;II)V

    goto :goto_0

    .line 827
    :sswitch_0
    new-instance v0, Lorg/apache/lucene/util/packed/Direct8;

    invoke-direct {v0, p2, p0, p3}, Lorg/apache/lucene/util/packed/Direct8;-><init>(ILorg/apache/lucene/store/DataInput;I)V

    goto :goto_0

    .line 829
    :sswitch_1
    new-instance v0, Lorg/apache/lucene/util/packed/Direct16;

    invoke-direct {v0, p2, p0, p3}, Lorg/apache/lucene/util/packed/Direct16;-><init>(ILorg/apache/lucene/store/DataInput;I)V

    goto :goto_0

    .line 831
    :sswitch_2
    new-instance v0, Lorg/apache/lucene/util/packed/Direct32;

    invoke-direct {v0, p2, p0, p3}, Lorg/apache/lucene/util/packed/Direct32;-><init>(ILorg/apache/lucene/store/DataInput;I)V

    goto :goto_0

    .line 833
    :sswitch_3
    new-instance v0, Lorg/apache/lucene/util/packed/Direct64;

    invoke-direct {v0, p2, p0, p3}, Lorg/apache/lucene/util/packed/Direct64;-><init>(ILorg/apache/lucene/store/DataInput;I)V

    goto :goto_0

    .line 835
    :sswitch_4
    if-gt p3, v2, :cond_0

    .line 836
    new-instance v0, Lorg/apache/lucene/util/packed/Packed8ThreeBlocks;

    invoke-direct {v0, p2, p0, p3}, Lorg/apache/lucene/util/packed/Packed8ThreeBlocks;-><init>(ILorg/apache/lucene/store/DataInput;I)V

    goto :goto_0

    .line 840
    :sswitch_5
    if-gt p3, v2, :cond_0

    .line 841
    new-instance v0, Lorg/apache/lucene/util/packed/Packed16ThreeBlocks;

    invoke-direct {v0, p2, p0, p3}, Lorg/apache/lucene/util/packed/Packed16ThreeBlocks;-><init>(ILorg/apache/lucene/store/DataInput;I)V

    goto :goto_0

    .line 821
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch

    .line 825
    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_0
        0x10 -> :sswitch_1
        0x18 -> :sswitch_4
        0x20 -> :sswitch_2
        0x30 -> :sswitch_5
        0x40 -> :sswitch_3
    .end sparse-switch
.end method

.method public static getReaderNoHeader(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/packed/PackedInts$Header;)Lorg/apache/lucene/util/packed/PackedInts$Reader;
    .locals 4
    .param p0, "in"    # Lorg/apache/lucene/store/DataInput;
    .param p1, "header"    # Lorg/apache/lucene/util/packed/PackedInts$Header;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 864
    # getter for: Lorg/apache/lucene/util/packed/PackedInts$Header;->format:Lorg/apache/lucene/util/packed/PackedInts$Format;
    invoke-static {p1}, Lorg/apache/lucene/util/packed/PackedInts$Header;->access$0(Lorg/apache/lucene/util/packed/PackedInts$Header;)Lorg/apache/lucene/util/packed/PackedInts$Format;

    move-result-object v0

    # getter for: Lorg/apache/lucene/util/packed/PackedInts$Header;->version:I
    invoke-static {p1}, Lorg/apache/lucene/util/packed/PackedInts$Header;->access$1(Lorg/apache/lucene/util/packed/PackedInts$Header;)I

    move-result v1

    # getter for: Lorg/apache/lucene/util/packed/PackedInts$Header;->valueCount:I
    invoke-static {p1}, Lorg/apache/lucene/util/packed/PackedInts$Header;->access$2(Lorg/apache/lucene/util/packed/PackedInts$Header;)I

    move-result v2

    # getter for: Lorg/apache/lucene/util/packed/PackedInts$Header;->bitsPerValue:I
    invoke-static {p1}, Lorg/apache/lucene/util/packed/PackedInts$Header;->access$3(Lorg/apache/lucene/util/packed/PackedInts$Header;)I

    move-result v3

    invoke-static {p0, v0, v1, v2, v3}, Lorg/apache/lucene/util/packed/PackedInts;->getReaderNoHeader(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/packed/PackedInts$Format;III)Lorg/apache/lucene/util/packed/PackedInts$Reader;

    move-result-object v0

    return-object v0
.end method

.method public static getWriter(Lorg/apache/lucene/store/DataOutput;IIF)Lorg/apache/lucene/util/packed/PackedInts$Writer;
    .locals 5
    .param p0, "out"    # Lorg/apache/lucene/store/DataOutput;
    .param p1, "valueCount"    # I
    .param p2, "bitsPerValue"    # I
    .param p3, "acceptableOverheadRatio"    # F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1156
    sget-boolean v2, Lorg/apache/lucene/util/packed/PackedInts;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    if-gez p1, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 1158
    :cond_0
    invoke-static {p1, p2, p3}, Lorg/apache/lucene/util/packed/PackedInts;->fastestFormatAndBits(IIF)Lorg/apache/lucene/util/packed/PackedInts$FormatAndBits;

    move-result-object v0

    .line 1159
    .local v0, "formatAndBits":Lorg/apache/lucene/util/packed/PackedInts$FormatAndBits;
    iget-object v2, v0, Lorg/apache/lucene/util/packed/PackedInts$FormatAndBits;->format:Lorg/apache/lucene/util/packed/PackedInts$Format;

    iget v3, v0, Lorg/apache/lucene/util/packed/PackedInts$FormatAndBits;->bitsPerValue:I

    const/16 v4, 0x400

    invoke-static {p0, v2, p1, v3, v4}, Lorg/apache/lucene/util/packed/PackedInts;->getWriterNoHeader(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/packed/PackedInts$Format;III)Lorg/apache/lucene/util/packed/PackedInts$Writer;

    move-result-object v1

    .line 1160
    .local v1, "writer":Lorg/apache/lucene/util/packed/PackedInts$Writer;
    invoke-virtual {v1}, Lorg/apache/lucene/util/packed/PackedInts$Writer;->writeHeader()V

    .line 1161
    return-object v1
.end method

.method public static getWriterNoHeader(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/packed/PackedInts$Format;III)Lorg/apache/lucene/util/packed/PackedInts$Writer;
    .locals 6
    .param p0, "out"    # Lorg/apache/lucene/store/DataOutput;
    .param p1, "format"    # Lorg/apache/lucene/util/packed/PackedInts$Format;
    .param p2, "valueCount"    # I
    .param p3, "bitsPerValue"    # I
    .param p4, "mem"    # I

    .prologue
    .line 1117
    new-instance v0, Lorg/apache/lucene/util/packed/PackedWriter;

    move-object v1, p1

    move-object v2, p0

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/util/packed/PackedWriter;-><init>(Lorg/apache/lucene/util/packed/PackedInts$Format;Lorg/apache/lucene/store/DataOutput;III)V

    return-object v0
.end method

.method public static maxValue(I)J
    .locals 4
    .param p0, "bitsPerValue"    # I

    .prologue
    const-wide/16 v2, -0x1

    .line 1185
    const/16 v0, 0x40

    if-ne p0, v0, :cond_0

    const-wide v0, 0x7fffffffffffffffL

    :goto_0
    return-wide v0

    :cond_0
    shl-long v0, v2, p0

    xor-long/2addr v0, v2

    goto :goto_0
.end method

.method public static readHeader(Lorg/apache/lucene/store/DataInput;)Lorg/apache/lucene/util/packed/PackedInts$Header;
    .locals 7
    .param p0, "in"    # Lorg/apache/lucene/store/DataInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1240
    const-string v4, "PackedInts"

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-static {p0, v4, v5, v6}, Lorg/apache/lucene/codecs/CodecUtil;->checkHeader(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I

    move-result v3

    .line 1241
    .local v3, "version":I
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readVInt()I

    move-result v0

    .line 1242
    .local v0, "bitsPerValue":I
    sget-boolean v4, Lorg/apache/lucene/util/packed/PackedInts;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    if-lez v0, :cond_0

    const/16 v4, 0x40

    if-le v0, v4, :cond_1

    :cond_0
    new-instance v4, Ljava/lang/AssertionError;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "bitsPerValue="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    .line 1243
    :cond_1
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readVInt()I

    move-result v2

    .line 1244
    .local v2, "valueCount":I
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readVInt()I

    move-result v4

    invoke-static {v4}, Lorg/apache/lucene/util/packed/PackedInts$Format;->byId(I)Lorg/apache/lucene/util/packed/PackedInts$Format;

    move-result-object v1

    .line 1245
    .local v1, "format":Lorg/apache/lucene/util/packed/PackedInts$Format;
    new-instance v4, Lorg/apache/lucene/util/packed/PackedInts$Header;

    invoke-direct {v4, v1, v2, v0, v3}, Lorg/apache/lucene/util/packed/PackedInts$Header;-><init>(Lorg/apache/lucene/util/packed/PackedInts$Format;III)V

    return-object v4
.end method
