.class public final Lorg/apache/lucene/util/packed/PackedDataOutput;
.super Ljava/lang/Object;
.source "PackedDataOutput.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field current:J

.field final out:Lorg/apache/lucene/store/DataOutput;

.field remainingBits:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lorg/apache/lucene/util/packed/PackedDataOutput;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/packed/PackedDataOutput;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/store/DataOutput;)V
    .locals 2
    .param p1, "out"    # Lorg/apache/lucene/store/DataOutput;

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lorg/apache/lucene/util/packed/PackedDataOutput;->out:Lorg/apache/lucene/store/DataOutput;

    .line 41
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/apache/lucene/util/packed/PackedDataOutput;->current:J

    .line 42
    const/16 v0, 0x8

    iput v0, p0, Lorg/apache/lucene/util/packed/PackedDataOutput;->remainingBits:I

    .line 43
    return-void
.end method


# virtual methods
.method public flush()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v4, 0x8

    .line 67
    iget v0, p0, Lorg/apache/lucene/util/packed/PackedDataOutput;->remainingBits:I

    if-ge v0, v4, :cond_0

    .line 68
    iget-object v0, p0, Lorg/apache/lucene/util/packed/PackedDataOutput;->out:Lorg/apache/lucene/store/DataOutput;

    iget-wide v2, p0, Lorg/apache/lucene/util/packed/PackedDataOutput;->current:J

    long-to-int v1, v2

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/store/DataOutput;->writeByte(B)V

    .line 70
    :cond_0
    iput v4, p0, Lorg/apache/lucene/util/packed/PackedDataOutput;->remainingBits:I

    .line 71
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/apache/lucene/util/packed/PackedDataOutput;->current:J

    .line 72
    return-void
.end method

.method public writeLong(JI)V
    .locals 15
    .param p1, "value"    # J
    .param p3, "bitsPerValue"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v12, 0x1

    const-wide/16 v10, 0x0

    .line 49
    sget-boolean v3, Lorg/apache/lucene/util/packed/PackedDataOutput;->$assertionsDisabled:Z

    if-nez v3, :cond_3

    const/16 v3, 0x40

    move/from16 v0, p3

    if-eq v0, v3, :cond_3

    cmp-long v3, p1, v10

    if-ltz v3, :cond_0

    invoke-static/range {p3 .. p3}, Lorg/apache/lucene/util/packed/PackedInts;->maxValue(I)J

    move-result-wide v4

    cmp-long v3, p1, v4

    if-lez v3, :cond_3

    :cond_0
    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 51
    :cond_1
    iget v3, p0, Lorg/apache/lucene/util/packed/PackedDataOutput;->remainingBits:I

    if-nez v3, :cond_2

    .line 52
    iget-object v3, p0, Lorg/apache/lucene/util/packed/PackedDataOutput;->out:Lorg/apache/lucene/store/DataOutput;

    iget-wide v4, p0, Lorg/apache/lucene/util/packed/PackedDataOutput;->current:J

    long-to-int v4, v4

    int-to-byte v4, v4

    invoke-virtual {v3, v4}, Lorg/apache/lucene/store/DataOutput;->writeByte(B)V

    .line 53
    iput-wide v10, p0, Lorg/apache/lucene/util/packed/PackedDataOutput;->current:J

    .line 54
    const/16 v3, 0x8

    iput v3, p0, Lorg/apache/lucene/util/packed/PackedDataOutput;->remainingBits:I

    .line 56
    :cond_2
    iget v3, p0, Lorg/apache/lucene/util/packed/PackedDataOutput;->remainingBits:I

    move/from16 v0, p3

    invoke-static {v3, v0}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 57
    .local v2, "bits":I
    iget-wide v4, p0, Lorg/apache/lucene/util/packed/PackedDataOutput;->current:J

    sub-int v3, p3, v2

    ushr-long v6, p1, v3

    shl-long v8, v12, v2

    sub-long/2addr v8, v12

    and-long/2addr v6, v8

    iget v3, p0, Lorg/apache/lucene/util/packed/PackedDataOutput;->remainingBits:I

    sub-int/2addr v3, v2

    shl-long/2addr v6, v3

    or-long/2addr v4, v6

    iput-wide v4, p0, Lorg/apache/lucene/util/packed/PackedDataOutput;->current:J

    .line 58
    sub-int p3, p3, v2

    .line 59
    iget v3, p0, Lorg/apache/lucene/util/packed/PackedDataOutput;->remainingBits:I

    sub-int/2addr v3, v2

    iput v3, p0, Lorg/apache/lucene/util/packed/PackedDataOutput;->remainingBits:I

    .line 50
    .end local v2    # "bits":I
    :cond_3
    if-gtz p3, :cond_1

    .line 61
    return-void
.end method
