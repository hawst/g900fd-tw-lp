.class Lorg/apache/lucene/util/CharsRef$UTF16SortedAsUTF8Comparator;
.super Ljava/lang/Object;
.source "CharsRef.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/CharsRef;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "UTF16SortedAsUTF8Comparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lorg/apache/lucene/util/CharsRef;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 235
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/util/CharsRef$UTF16SortedAsUTF8Comparator;)V
    .locals 0

    .prologue
    .line 235
    invoke-direct {p0}, Lorg/apache/lucene/util/CharsRef$UTF16SortedAsUTF8Comparator;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/util/CharsRef;

    check-cast p2, Lorg/apache/lucene/util/CharsRef;

    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/util/CharsRef$UTF16SortedAsUTF8Comparator;->compare(Lorg/apache/lucene/util/CharsRef;Lorg/apache/lucene/util/CharsRef;)I

    move-result v0

    return v0
.end method

.method public compare(Lorg/apache/lucene/util/CharsRef;Lorg/apache/lucene/util/CharsRef;)I
    .locals 13
    .param p1, "a"    # Lorg/apache/lucene/util/CharsRef;
    .param p2, "b"    # Lorg/apache/lucene/util/CharsRef;

    .prologue
    const v12, 0xe000

    const v11, 0xd800

    .line 239
    if-ne p1, p2, :cond_0

    .line 240
    const/4 v9, 0x0

    .line 276
    :goto_0
    return v9

    .line 242
    :cond_0
    iget-object v1, p1, Lorg/apache/lucene/util/CharsRef;->chars:[C

    .line 243
    .local v1, "aChars":[C
    iget v3, p1, Lorg/apache/lucene/util/CharsRef;->offset:I

    .line 244
    .local v3, "aUpto":I
    iget-object v6, p2, Lorg/apache/lucene/util/CharsRef;->chars:[C

    .line 245
    .local v6, "bChars":[C
    iget v7, p2, Lorg/apache/lucene/util/CharsRef;->offset:I

    .line 247
    .local v7, "bUpto":I
    iget v9, p1, Lorg/apache/lucene/util/CharsRef;->length:I

    iget v10, p2, Lorg/apache/lucene/util/CharsRef;->length:I

    invoke-static {v9, v10}, Ljava/lang/Math;->min(II)I

    move-result v9

    add-int v2, v3, v9

    .local v2, "aStop":I
    move v8, v7

    .end local v7    # "bUpto":I
    .local v8, "bUpto":I
    move v4, v3

    .line 249
    .end local v3    # "aUpto":I
    .local v4, "aUpto":I
    :goto_1
    if-lt v4, v2, :cond_1

    .line 276
    iget v9, p1, Lorg/apache/lucene/util/CharsRef;->length:I

    iget v10, p2, Lorg/apache/lucene/util/CharsRef;->length:I

    sub-int/2addr v9, v10

    goto :goto_0

    .line 250
    :cond_1
    add-int/lit8 v3, v4, 0x1

    .end local v4    # "aUpto":I
    .restart local v3    # "aUpto":I
    aget-char v0, v1, v4

    .line 251
    .local v0, "aChar":C
    add-int/lit8 v7, v8, 0x1

    .end local v8    # "bUpto":I
    .restart local v7    # "bUpto":I
    aget-char v5, v6, v8

    .line 252
    .local v5, "bChar":C
    if-eq v0, v5, :cond_5

    .line 256
    if-lt v0, v11, :cond_2

    if-lt v5, v11, :cond_2

    .line 257
    if-lt v0, v12, :cond_3

    .line 258
    add-int/lit16 v9, v0, -0x800

    int-to-char v0, v9

    .line 263
    :goto_2
    if-lt v5, v12, :cond_4

    .line 264
    add-int/lit16 v9, v5, -0x800

    int-to-char v5, v9

    .line 271
    :cond_2
    :goto_3
    sub-int v9, v0, v5

    goto :goto_0

    .line 260
    :cond_3
    add-int/lit16 v9, v0, 0x2000

    int-to-char v0, v9

    goto :goto_2

    .line 266
    :cond_4
    add-int/lit16 v9, v5, 0x2000

    int-to-char v5, v9

    goto :goto_3

    :cond_5
    move v8, v7

    .end local v7    # "bUpto":I
    .restart local v8    # "bUpto":I
    move v4, v3

    .end local v3    # "aUpto":I
    .restart local v4    # "aUpto":I
    goto :goto_1
.end method
