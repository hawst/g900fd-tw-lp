.class public abstract Lorg/apache/lucene/util/FilterIterator;
.super Ljava/lang/Object;
.source "FilterIterator.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TT;>;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final iterator:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<TT;>;"
        }
    .end annotation
.end field

.field private next:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private nextIsSet:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lorg/apache/lucene/util/FilterIterator;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/FilterIterator;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/util/Iterator;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 36
    .local p0, "this":Lorg/apache/lucene/util/FilterIterator;, "Lorg/apache/lucene/util/FilterIterator<TT;>;"
    .local p1, "baseIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/util/FilterIterator;->next:Ljava/lang/Object;

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/util/FilterIterator;->nextIsSet:Z

    .line 37
    iput-object p1, p0, Lorg/apache/lucene/util/FilterIterator;->iterator:Ljava/util/Iterator;

    .line 38
    return-void
.end method

.method private setNext()Z
    .locals 3

    .prologue
    .local p0, "this":Lorg/apache/lucene/util/FilterIterator;, "Lorg/apache/lucene/util/FilterIterator<TT;>;"
    const/4 v1, 0x1

    .line 65
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/util/FilterIterator;->iterator:Ljava/util/Iterator;

    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 73
    const/4 v1, 0x0

    :goto_0
    return v1

    .line 66
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/util/FilterIterator;->iterator:Ljava/util/Iterator;

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 67
    .local v0, "object":Ljava/lang/Object;, "TT;"
    invoke-virtual {p0, v0}, Lorg/apache/lucene/util/FilterIterator;->predicateFunction(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 68
    iput-object v0, p0, Lorg/apache/lucene/util/FilterIterator;->next:Ljava/lang/Object;

    .line 69
    iput-boolean v1, p0, Lorg/apache/lucene/util/FilterIterator;->nextIsSet:Z

    goto :goto_0
.end method


# virtual methods
.method public final hasNext()Z
    .locals 1

    .prologue
    .line 42
    .local p0, "this":Lorg/apache/lucene/util/FilterIterator;, "Lorg/apache/lucene/util/FilterIterator<TT;>;"
    iget-boolean v0, p0, Lorg/apache/lucene/util/FilterIterator;->nextIsSet:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/util/FilterIterator;->setNext()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final next()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/util/FilterIterator;, "Lorg/apache/lucene/util/FilterIterator<TT;>;"
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 47
    invoke-virtual {p0}, Lorg/apache/lucene/util/FilterIterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 48
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 50
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/util/FilterIterator;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lorg/apache/lucene/util/FilterIterator;->nextIsSet:Z

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 52
    :cond_1
    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/util/FilterIterator;->next:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 54
    iput-boolean v1, p0, Lorg/apache/lucene/util/FilterIterator;->nextIsSet:Z

    .line 55
    iput-object v2, p0, Lorg/apache/lucene/util/FilterIterator;->next:Ljava/lang/Object;

    .line 52
    return-object v0

    .line 53
    :catchall_0
    move-exception v0

    .line 54
    iput-boolean v1, p0, Lorg/apache/lucene/util/FilterIterator;->nextIsSet:Z

    .line 55
    iput-object v2, p0, Lorg/apache/lucene/util/FilterIterator;->next:Ljava/lang/Object;

    .line 56
    throw v0
.end method

.method protected abstract predicateFunction(Ljava/lang/Object;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation
.end method

.method public final remove()V
    .locals 1

    .prologue
    .line 61
    .local p0, "this":Lorg/apache/lucene/util/FilterIterator;, "Lorg/apache/lucene/util/FilterIterator<TT;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
