.class public final Lorg/apache/lucene/util/fst/PositiveIntOutputs;
.super Lorg/apache/lucene/util/fst/Outputs;
.source "PositiveIntOutputs.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/util/fst/Outputs",
        "<",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final NO_OUTPUT:Ljava/lang/Long;

.field private static final singletonNoShare:Lorg/apache/lucene/util/fst/PositiveIntOutputs;

.field private static final singletonShare:Lorg/apache/lucene/util/fst/PositiveIntOutputs;


# instance fields
.field private final doShare:Z


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 32
    const-class v0, Lorg/apache/lucene/util/fst/PositiveIntOutputs;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/fst/PositiveIntOutputs;->$assertionsDisabled:Z

    .line 34
    new-instance v0, Ljava/lang/Long;

    const-wide/16 v4, 0x0

    invoke-direct {v0, v4, v5}, Ljava/lang/Long;-><init>(J)V

    sput-object v0, Lorg/apache/lucene/util/fst/PositiveIntOutputs;->NO_OUTPUT:Ljava/lang/Long;

    .line 38
    new-instance v0, Lorg/apache/lucene/util/fst/PositiveIntOutputs;

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/fst/PositiveIntOutputs;-><init>(Z)V

    sput-object v0, Lorg/apache/lucene/util/fst/PositiveIntOutputs;->singletonShare:Lorg/apache/lucene/util/fst/PositiveIntOutputs;

    .line 39
    new-instance v0, Lorg/apache/lucene/util/fst/PositiveIntOutputs;

    invoke-direct {v0, v2}, Lorg/apache/lucene/util/fst/PositiveIntOutputs;-><init>(Z)V

    sput-object v0, Lorg/apache/lucene/util/fst/PositiveIntOutputs;->singletonNoShare:Lorg/apache/lucene/util/fst/PositiveIntOutputs;

    return-void

    :cond_0
    move v0, v2

    .line 32
    goto :goto_0
.end method

.method private constructor <init>(Z)V
    .locals 0
    .param p1, "doShare"    # Z

    .prologue
    .line 41
    invoke-direct {p0}, Lorg/apache/lucene/util/fst/Outputs;-><init>()V

    .line 42
    iput-boolean p1, p0, Lorg/apache/lucene/util/fst/PositiveIntOutputs;->doShare:Z

    .line 43
    return-void
.end method

.method public static getSingleton()Lorg/apache/lucene/util/fst/PositiveIntOutputs;
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x1

    invoke-static {v0}, Lorg/apache/lucene/util/fst/PositiveIntOutputs;->getSingleton(Z)Lorg/apache/lucene/util/fst/PositiveIntOutputs;

    move-result-object v0

    return-object v0
.end method

.method public static getSingleton(Z)Lorg/apache/lucene/util/fst/PositiveIntOutputs;
    .locals 1
    .param p0, "doShare"    # Z

    .prologue
    .line 55
    if-eqz p0, :cond_0

    sget-object v0, Lorg/apache/lucene/util/fst/PositiveIntOutputs;->singletonShare:Lorg/apache/lucene/util/fst/PositiveIntOutputs;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lorg/apache/lucene/util/fst/PositiveIntOutputs;->singletonNoShare:Lorg/apache/lucene/util/fst/PositiveIntOutputs;

    goto :goto_0
.end method

.method private valid(Ljava/lang/Long;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Long;

    .prologue
    .line 120
    sget-boolean v0, Lorg/apache/lucene/util/fst/PositiveIntOutputs;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 121
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/util/fst/PositiveIntOutputs;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    sget-object v0, Lorg/apache/lucene/util/fst/PositiveIntOutputs;->NO_OUTPUT:Ljava/lang/Long;

    if-eq p1, v0, :cond_1

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "o="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 122
    :cond_1
    const/4 v0, 0x1

    return v0
.end method


# virtual methods
.method public add(Ljava/lang/Long;Ljava/lang/Long;)Ljava/lang/Long;
    .locals 4
    .param p1, "prefix"    # Ljava/lang/Long;
    .param p2, "output"    # Ljava/lang/Long;

    .prologue
    .line 92
    sget-boolean v0, Lorg/apache/lucene/util/fst/PositiveIntOutputs;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lorg/apache/lucene/util/fst/PositiveIntOutputs;->valid(Ljava/lang/Long;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 93
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/util/fst/PositiveIntOutputs;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    invoke-direct {p0, p2}, Lorg/apache/lucene/util/fst/PositiveIntOutputs;->valid(Ljava/lang/Long;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 94
    :cond_1
    sget-object v0, Lorg/apache/lucene/util/fst/PositiveIntOutputs;->NO_OUTPUT:Ljava/lang/Long;

    if-ne p1, v0, :cond_2

    .line 99
    .end local p2    # "output":Ljava/lang/Long;
    :goto_0
    return-object p2

    .line 96
    .restart local p2    # "output":Ljava/lang/Long;
    :cond_2
    sget-object v0, Lorg/apache/lucene/util/fst/PositiveIntOutputs;->NO_OUTPUT:Ljava/lang/Long;

    if-ne p2, v0, :cond_3

    move-object p2, p1

    .line 97
    goto :goto_0

    .line 99
    :cond_3
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    add-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    goto :goto_0
.end method

.method public bridge synthetic add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, Ljava/lang/Long;

    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/util/fst/PositiveIntOutputs;->add(Ljava/lang/Long;Ljava/lang/Long;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public common(Ljava/lang/Long;Ljava/lang/Long;)Ljava/lang/Long;
    .locals 4
    .param p1, "output1"    # Ljava/lang/Long;
    .param p2, "output2"    # Ljava/lang/Long;

    .prologue
    const-wide/16 v2, 0x0

    .line 60
    sget-boolean v0, Lorg/apache/lucene/util/fst/PositiveIntOutputs;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lorg/apache/lucene/util/fst/PositiveIntOutputs;->valid(Ljava/lang/Long;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 61
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/util/fst/PositiveIntOutputs;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    invoke-direct {p0, p2}, Lorg/apache/lucene/util/fst/PositiveIntOutputs;->valid(Ljava/lang/Long;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 62
    :cond_1
    sget-object v0, Lorg/apache/lucene/util/fst/PositiveIntOutputs;->NO_OUTPUT:Ljava/lang/Long;

    if-eq p1, v0, :cond_2

    sget-object v0, Lorg/apache/lucene/util/fst/PositiveIntOutputs;->NO_OUTPUT:Ljava/lang/Long;

    if-ne p2, v0, :cond_4

    .line 63
    :cond_2
    sget-object p1, Lorg/apache/lucene/util/fst/PositiveIntOutputs;->NO_OUTPUT:Ljava/lang/Long;

    .line 71
    .end local p1    # "output1":Ljava/lang/Long;
    :cond_3
    :goto_0
    return-object p1

    .line 64
    .restart local p1    # "output1":Ljava/lang/Long;
    :cond_4
    iget-boolean v0, p0, Lorg/apache/lucene/util/fst/PositiveIntOutputs;->doShare:Z

    if-eqz v0, :cond_7

    .line 65
    sget-boolean v0, Lorg/apache/lucene/util/fst/PositiveIntOutputs;->$assertionsDisabled:Z

    if-nez v0, :cond_5

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_5

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 66
    :cond_5
    sget-boolean v0, Lorg/apache/lucene/util/fst/PositiveIntOutputs;->$assertionsDisabled:Z

    if-nez v0, :cond_6

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_6

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 67
    :cond_6
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    goto :goto_0

    .line 68
    :cond_7
    invoke-virtual {p1, p2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 71
    sget-object p1, Lorg/apache/lucene/util/fst/PositiveIntOutputs;->NO_OUTPUT:Ljava/lang/Long;

    goto :goto_0
.end method

.method public bridge synthetic common(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, Ljava/lang/Long;

    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/util/fst/PositiveIntOutputs;->common(Ljava/lang/Long;Ljava/lang/Long;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public getNoOutput()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 127
    sget-object v0, Lorg/apache/lucene/util/fst/PositiveIntOutputs;->NO_OUTPUT:Ljava/lang/Long;

    return-object v0
.end method

.method public bridge synthetic getNoOutput()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/util/fst/PositiveIntOutputs;->getNoOutput()Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public outputToString(Ljava/lang/Long;)Ljava/lang/String;
    .locals 1
    .param p1, "output"    # Ljava/lang/Long;

    .prologue
    .line 132
    invoke-virtual {p1}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic outputToString(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/fst/PositiveIntOutputs;->outputToString(Ljava/lang/Long;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public read(Lorg/apache/lucene/store/DataInput;)Ljava/lang/Long;
    .locals 4
    .param p1, "in"    # Lorg/apache/lucene/store/DataInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 111
    invoke-virtual {p1}, Lorg/apache/lucene/store/DataInput;->readVLong()J

    move-result-wide v0

    .line 112
    .local v0, "v":J
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 113
    sget-object v2, Lorg/apache/lucene/util/fst/PositiveIntOutputs;->NO_OUTPUT:Ljava/lang/Long;

    .line 115
    :goto_0
    return-object v2

    :cond_0
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    goto :goto_0
.end method

.method public bridge synthetic read(Lorg/apache/lucene/store/DataInput;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/fst/PositiveIntOutputs;->read(Lorg/apache/lucene/store/DataInput;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public subtract(Ljava/lang/Long;Ljava/lang/Long;)Ljava/lang/Long;
    .locals 4
    .param p1, "output"    # Ljava/lang/Long;
    .param p2, "inc"    # Ljava/lang/Long;

    .prologue
    .line 77
    sget-boolean v0, Lorg/apache/lucene/util/fst/PositiveIntOutputs;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lorg/apache/lucene/util/fst/PositiveIntOutputs;->valid(Ljava/lang/Long;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 78
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/util/fst/PositiveIntOutputs;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    invoke-direct {p0, p2}, Lorg/apache/lucene/util/fst/PositiveIntOutputs;->valid(Ljava/lang/Long;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 79
    :cond_1
    sget-boolean v0, Lorg/apache/lucene/util/fst/PositiveIntOutputs;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 81
    :cond_2
    sget-object v0, Lorg/apache/lucene/util/fst/PositiveIntOutputs;->NO_OUTPUT:Ljava/lang/Long;

    if-ne p2, v0, :cond_3

    .line 86
    .end local p1    # "output":Ljava/lang/Long;
    :goto_0
    return-object p1

    .line 83
    .restart local p1    # "output":Ljava/lang/Long;
    :cond_3
    invoke-virtual {p1, p2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 84
    sget-object p1, Lorg/apache/lucene/util/fst/PositiveIntOutputs;->NO_OUTPUT:Ljava/lang/Long;

    goto :goto_0

    .line 86
    :cond_4
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sub-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    goto :goto_0
.end method

.method public bridge synthetic subtract(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, Ljava/lang/Long;

    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/util/fst/PositiveIntOutputs;->subtract(Ljava/lang/Long;Ljava/lang/Long;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 137
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PositiveIntOutputs(doShare="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lorg/apache/lucene/util/fst/PositiveIntOutputs;->doShare:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public write(Ljava/lang/Long;Lorg/apache/lucene/store/DataOutput;)V
    .locals 2
    .param p1, "output"    # Ljava/lang/Long;
    .param p2, "out"    # Lorg/apache/lucene/store/DataOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 105
    sget-boolean v0, Lorg/apache/lucene/util/fst/PositiveIntOutputs;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lorg/apache/lucene/util/fst/PositiveIntOutputs;->valid(Ljava/lang/Long;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 106
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p2, v0, v1}, Lorg/apache/lucene/store/DataOutput;->writeVLong(J)V

    .line 107
    return-void
.end method

.method public bridge synthetic write(Ljava/lang/Object;Lorg/apache/lucene/store/DataOutput;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/util/fst/PositiveIntOutputs;->write(Ljava/lang/Long;Lorg/apache/lucene/store/DataOutput;)V

    return-void
.end method
