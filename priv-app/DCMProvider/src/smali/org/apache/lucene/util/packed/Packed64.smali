.class Lorg/apache/lucene/util/packed/Packed64;
.super Lorg/apache/lucene/util/packed/PackedInts$MutableImpl;
.source "Packed64.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field static final BLOCK_BITS:I = 0x6

.field static final BLOCK_SIZE:I = 0x40

.field static final MOD_MASK:I = 0x3f


# instance fields
.field private final blocks:[J

.field private final bpvMinusBlockSize:I

.field private final maskRight:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const-class v0, Lorg/apache/lucene/util/packed/Packed64;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/packed/Packed64;->$assertionsDisabled:Z

    .line 48
    return-void

    .line 45
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(II)V
    .locals 5
    .param p1, "valueCount"    # I
    .param p2, "bitsPerValue"    # I

    .prologue
    .line 70
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/util/packed/PackedInts$MutableImpl;-><init>(II)V

    .line 71
    sget-object v0, Lorg/apache/lucene/util/packed/PackedInts$Format;->PACKED:Lorg/apache/lucene/util/packed/PackedInts$Format;

    .line 72
    .local v0, "format":Lorg/apache/lucene/util/packed/PackedInts$Format;
    const/4 v2, 0x1

    invoke-virtual {v0, v2, p1, p2}, Lorg/apache/lucene/util/packed/PackedInts$Format;->longCount(III)I

    move-result v1

    .line 73
    .local v1, "longCount":I
    new-array v2, v1, [J

    iput-object v2, p0, Lorg/apache/lucene/util/packed/Packed64;->blocks:[J

    .line 74
    const-wide/16 v2, -0x1

    rsub-int/lit8 v4, p2, 0x40

    shl-long/2addr v2, v4

    rsub-int/lit8 v4, p2, 0x40

    ushr-long/2addr v2, v4

    iput-wide v2, p0, Lorg/apache/lucene/util/packed/Packed64;->maskRight:J

    .line 75
    add-int/lit8 v2, p2, -0x40

    iput v2, p0, Lorg/apache/lucene/util/packed/Packed64;->bpvMinusBlockSize:I

    .line 76
    return-void
.end method

.method public constructor <init>(ILorg/apache/lucene/store/DataInput;II)V
    .locals 14
    .param p1, "packedIntsVersion"    # I
    .param p2, "in"    # Lorg/apache/lucene/store/DataInput;
    .param p3, "valueCount"    # I
    .param p4, "bitsPerValue"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 88
    move/from16 v0, p3

    move/from16 v1, p4

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/util/packed/PackedInts$MutableImpl;-><init>(II)V

    .line 89
    sget-object v4, Lorg/apache/lucene/util/packed/PackedInts$Format;->PACKED:Lorg/apache/lucene/util/packed/PackedInts$Format;

    .line 90
    .local v4, "format":Lorg/apache/lucene/util/packed/PackedInts$Format;
    move/from16 v0, p3

    move/from16 v1, p4

    invoke-virtual {v4, p1, v0, v1}, Lorg/apache/lucene/util/packed/PackedInts$Format;->byteCount(III)J

    move-result-wide v2

    .line 91
    .local v2, "byteCount":J
    const/4 v10, 0x1

    move/from16 v0, p3

    move/from16 v1, p4

    invoke-virtual {v4, v10, v0, v1}, Lorg/apache/lucene/util/packed/PackedInts$Format;->longCount(III)I

    move-result v8

    .line 92
    .local v8, "longCount":I
    new-array v10, v8, [J

    iput-object v10, p0, Lorg/apache/lucene/util/packed/Packed64;->blocks:[J

    .line 94
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    int-to-long v10, v5

    const-wide/16 v12, 0x8

    div-long v12, v2, v12

    cmp-long v10, v10, v12

    if-ltz v10, :cond_1

    .line 97
    const-wide/16 v10, 0x8

    rem-long v10, v2, v10

    long-to-int v9, v10

    .line 98
    .local v9, "remaining":I
    if-eqz v9, :cond_0

    .line 100
    const-wide/16 v6, 0x0

    .line 101
    .local v6, "lastLong":J
    const/4 v5, 0x0

    :goto_1
    if-lt v5, v9, :cond_2

    .line 104
    iget-object v10, p0, Lorg/apache/lucene/util/packed/Packed64;->blocks:[J

    iget-object v11, p0, Lorg/apache/lucene/util/packed/Packed64;->blocks:[J

    array-length v11, v11

    add-int/lit8 v11, v11, -0x1

    aput-wide v6, v10, v11

    .line 106
    .end local v6    # "lastLong":J
    :cond_0
    const-wide/16 v10, -0x1

    rsub-int/lit8 v12, p4, 0x40

    shl-long/2addr v10, v12

    rsub-int/lit8 v12, p4, 0x40

    ushr-long/2addr v10, v12

    iput-wide v10, p0, Lorg/apache/lucene/util/packed/Packed64;->maskRight:J

    .line 107
    add-int/lit8 v10, p4, -0x40

    iput v10, p0, Lorg/apache/lucene/util/packed/Packed64;->bpvMinusBlockSize:I

    .line 108
    return-void

    .line 95
    .end local v9    # "remaining":I
    :cond_1
    iget-object v10, p0, Lorg/apache/lucene/util/packed/Packed64;->blocks:[J

    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/store/DataInput;->readLong()J

    move-result-wide v12

    aput-wide v12, v10, v5

    .line 94
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 102
    .restart local v6    # "lastLong":J
    .restart local v9    # "remaining":I
    :cond_2
    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v10

    int-to-long v10, v10

    const-wide/16 v12, 0xff

    and-long/2addr v10, v12

    mul-int/lit8 v12, v5, 0x8

    rsub-int/lit8 v12, v12, 0x38

    shl-long/2addr v10, v12

    or-long/2addr v6, v10

    .line 101
    add-int/lit8 v5, v5, 0x1

    goto :goto_1
.end method

.method private static gcd(II)I
    .locals 1
    .param p0, "a"    # I
    .param p1, "b"    # I

    .prologue
    .line 306
    if-ge p0, p1, :cond_1

    .line 307
    invoke-static {p1, p0}, Lorg/apache/lucene/util/packed/Packed64;->gcd(II)I

    move-result p0

    .line 311
    .end local p0    # "a":I
    :cond_0
    :goto_0
    return p0

    .line 308
    .restart local p0    # "a":I
    :cond_1
    if-eqz p1, :cond_0

    .line 311
    rem-int v0, p0, p1

    invoke-static {p1, v0}, Lorg/apache/lucene/util/packed/Packed64;->gcd(II)I

    move-result p0

    goto :goto_0
.end method


# virtual methods
.method public clear()V
    .locals 4

    .prologue
    .line 317
    iget-object v0, p0, Lorg/apache/lucene/util/packed/Packed64;->blocks:[J

    const-wide/16 v2, 0x0

    invoke-static {v0, v2, v3}, Ljava/util/Arrays;->fill([JJ)V

    .line 318
    return-void
.end method

.method public fill(IIJ)V
    .locals 23
    .param p1, "fromIndex"    # I
    .param p2, "toIndex"    # I
    .param p3, "val"    # J

    .prologue
    .line 257
    sget-boolean v17, Lorg/apache/lucene/util/packed/Packed64;->$assertionsDisabled:Z

    if-nez v17, :cond_0

    invoke-static/range {p3 .. p4}, Lorg/apache/lucene/util/packed/PackedInts;->bitsRequired(J)I

    move-result v17

    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/util/packed/Packed64;->getBitsPerValue()I

    move-result v18

    move/from16 v0, v17

    move/from16 v1, v18

    if-le v0, v1, :cond_0

    new-instance v17, Ljava/lang/AssertionError;

    invoke-direct/range {v17 .. v17}, Ljava/lang/AssertionError;-><init>()V

    throw v17

    .line 258
    :cond_0
    sget-boolean v17, Lorg/apache/lucene/util/packed/Packed64;->$assertionsDisabled:Z

    if-nez v17, :cond_1

    move/from16 v0, p1

    move/from16 v1, p2

    if-le v0, v1, :cond_1

    new-instance v17, Ljava/lang/AssertionError;

    invoke-direct/range {v17 .. v17}, Ljava/lang/AssertionError;-><init>()V

    throw v17

    .line 261
    :cond_1
    const/16 v17, 0x40

    const/16 v18, 0x40

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/util/packed/Packed64;->bitsPerValue:I

    move/from16 v19, v0

    invoke-static/range {v18 .. v19}, Lorg/apache/lucene/util/packed/Packed64;->gcd(II)I

    move-result v18

    div-int v12, v17, v18

    .line 262
    .local v12, "nAlignedValues":I
    sub-int v14, p2, p1

    .line 263
    .local v14, "span":I
    mul-int/lit8 v17, v12, 0x3

    move/from16 v0, v17

    if-gt v14, v0, :cond_3

    .line 266
    invoke-super/range {p0 .. p4}, Lorg/apache/lucene/util/packed/PackedInts$MutableImpl;->fill(IIJ)V

    .line 303
    :cond_2
    return-void

    .line 271
    :cond_3
    rem-int v9, p1, v12

    .line 272
    .local v9, "fromIndexModNAlignedValues":I
    if-eqz v9, :cond_4

    .line 273
    move v10, v9

    .local v10, "i":I
    move/from16 v8, p1

    .end local p1    # "fromIndex":I
    .local v8, "fromIndex":I
    :goto_0
    if-lt v10, v12, :cond_5

    move/from16 p1, v8

    .line 277
    .end local v8    # "fromIndex":I
    .end local v10    # "i":I
    .restart local p1    # "fromIndex":I
    :cond_4
    sget-boolean v17, Lorg/apache/lucene/util/packed/Packed64;->$assertionsDisabled:Z

    if-nez v17, :cond_6

    rem-int v17, p1, v12

    if-eqz v17, :cond_6

    new-instance v17, Ljava/lang/AssertionError;

    invoke-direct/range {v17 .. v17}, Ljava/lang/AssertionError;-><init>()V

    throw v17

    .line 274
    .end local p1    # "fromIndex":I
    .restart local v8    # "fromIndex":I
    .restart local v10    # "i":I
    :cond_5
    add-int/lit8 p1, v8, 0x1

    .end local v8    # "fromIndex":I
    .restart local p1    # "fromIndex":I
    move-object/from16 v0, p0

    move-wide/from16 v1, p3

    invoke-virtual {v0, v8, v1, v2}, Lorg/apache/lucene/util/packed/Packed64;->set(IJ)V

    .line 273
    add-int/lit8 v10, v10, 0x1

    move/from16 v8, p1

    .end local p1    # "fromIndex":I
    .restart local v8    # "fromIndex":I
    goto :goto_0

    .line 282
    .end local v8    # "fromIndex":I
    .end local v10    # "i":I
    .restart local p1    # "fromIndex":I
    :cond_6
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/util/packed/Packed64;->bitsPerValue:I

    move/from16 v17, v0

    mul-int v17, v17, v12

    shr-int/lit8 v11, v17, 0x6

    .line 285
    .local v11, "nAlignedBlocks":I
    new-instance v16, Lorg/apache/lucene/util/packed/Packed64;

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/util/packed/Packed64;->bitsPerValue:I

    move/from16 v17, v0

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-direct {v0, v12, v1}, Lorg/apache/lucene/util/packed/Packed64;-><init>(II)V

    .line 286
    .local v16, "values":Lorg/apache/lucene/util/packed/Packed64;
    const/4 v10, 0x0

    .restart local v10    # "i":I
    :goto_1
    if-lt v10, v12, :cond_7

    .line 289
    move-object/from16 v0, v16

    iget-object v13, v0, Lorg/apache/lucene/util/packed/Packed64;->blocks:[J

    .line 290
    .local v13, "nAlignedValuesBlocks":[J
    sget-boolean v17, Lorg/apache/lucene/util/packed/Packed64;->$assertionsDisabled:Z

    if-nez v17, :cond_8

    array-length v0, v13

    move/from16 v17, v0

    move/from16 v0, v17

    if-le v11, v0, :cond_8

    new-instance v17, Ljava/lang/AssertionError;

    invoke-direct/range {v17 .. v17}, Ljava/lang/AssertionError;-><init>()V

    throw v17

    .line 287
    .end local v13    # "nAlignedValuesBlocks":[J
    :cond_7
    move-object/from16 v0, v16

    move-wide/from16 v1, p3

    invoke-virtual {v0, v10, v1, v2}, Lorg/apache/lucene/util/packed/Packed64;->set(IJ)V

    .line 286
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 292
    .restart local v13    # "nAlignedValuesBlocks":[J
    :cond_8
    move/from16 v0, p1

    int-to-long v0, v0

    move-wide/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/util/packed/Packed64;->bitsPerValue:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-long v0, v0

    move-wide/from16 v20, v0

    mul-long v18, v18, v20

    const/16 v17, 0x6

    ushr-long v18, v18, v17

    move-wide/from16 v0, v18

    long-to-int v15, v0

    .line 293
    .local v15, "startBlock":I
    move/from16 v0, p2

    int-to-long v0, v0

    move-wide/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/util/packed/Packed64;->bitsPerValue:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-long v0, v0

    move-wide/from16 v20, v0

    mul-long v18, v18, v20

    const/16 v17, 0x6

    ushr-long v18, v18, v17

    move-wide/from16 v0, v18

    long-to-int v5, v0

    .line 294
    .local v5, "endBlock":I
    move v4, v15

    .local v4, "block":I
    :goto_2
    if-lt v4, v5, :cond_9

    .line 300
    int-to-long v0, v5

    move-wide/from16 v18, v0

    const/16 v17, 0x6

    shl-long v18, v18, v17

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/util/packed/Packed64;->bitsPerValue:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-long v0, v0

    move-wide/from16 v20, v0

    div-long v18, v18, v20

    move-wide/from16 v0, v18

    long-to-int v10, v0

    :goto_3
    move/from16 v0, p2

    if-ge v10, v0, :cond_2

    .line 301
    move-object/from16 v0, p0

    move-wide/from16 v1, p3

    invoke-virtual {v0, v10, v1, v2}, Lorg/apache/lucene/util/packed/Packed64;->set(IJ)V

    .line 300
    add-int/lit8 v10, v10, 0x1

    goto :goto_3

    .line 295
    :cond_9
    rem-int v17, v4, v11

    aget-wide v6, v13, v17

    .line 296
    .local v6, "blockValue":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/packed/Packed64;->blocks:[J

    move-object/from16 v17, v0

    aput-wide v6, v17, v4

    .line 294
    add-int/lit8 v4, v4, 0x1

    goto :goto_2
.end method

.method public get(I[JII)I
    .locals 18
    .param p1, "index"    # I
    .param p2, "arr"    # [J
    .param p3, "off"    # I
    .param p4, "len"    # I

    .prologue
    .line 134
    sget-boolean v3, Lorg/apache/lucene/util/packed/Packed64;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    if-gtz p4, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "len must be > 0 (got "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p4

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 135
    :cond_0
    sget-boolean v3, Lorg/apache/lucene/util/packed/Packed64;->$assertionsDisabled:Z

    if-nez v3, :cond_2

    if-ltz p1, :cond_1

    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/util/packed/Packed64;->valueCount:I

    move/from16 v0, p1

    if-lt v0, v3, :cond_2

    :cond_1
    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 136
    :cond_2
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/util/packed/Packed64;->valueCount:I

    sub-int v3, v3, p1

    move/from16 v0, p4

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result p4

    .line 137
    sget-boolean v3, Lorg/apache/lucene/util/packed/Packed64;->$assertionsDisabled:Z

    if-nez v3, :cond_3

    add-int v3, p3, p4

    move-object/from16 v0, p2

    array-length v5, v0

    if-le v3, v5, :cond_3

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 139
    :cond_3
    move/from16 v13, p1

    .line 140
    .local v13, "originalIndex":I
    sget-object v3, Lorg/apache/lucene/util/packed/PackedInts$Format;->PACKED:Lorg/apache/lucene/util/packed/PackedInts$Format;

    move-object/from16 v0, p0

    iget v5, v0, Lorg/apache/lucene/util/packed/Packed64;->bitsPerValue:I

    invoke-static {v3, v5}, Lorg/apache/lucene/util/packed/BulkOperation;->of(Lorg/apache/lucene/util/packed/PackedInts$Format;I)Lorg/apache/lucene/util/packed/BulkOperation;

    move-result-object v2

    .line 143
    .local v2, "decoder":Lorg/apache/lucene/util/packed/PackedInts$Decoder;
    invoke-interface {v2}, Lorg/apache/lucene/util/packed/PackedInts$Decoder;->longValueCount()I

    move-result v3

    rem-int v12, p1, v3

    .line 144
    .local v12, "offsetInBlocks":I
    if-eqz v12, :cond_6

    .line 145
    move v9, v12

    .local v9, "i":I
    :goto_0
    invoke-interface {v2}, Lorg/apache/lucene/util/packed/PackedInts$Decoder;->longValueCount()I

    move-result v3

    if-ge v9, v3, :cond_4

    if-gtz p4, :cond_5

    .line 149
    :cond_4
    if-nez p4, :cond_6

    .line 150
    sub-int v3, p1, v13

    .line 171
    .end local v9    # "i":I
    :goto_1
    return v3

    .line 146
    .restart local v9    # "i":I
    :cond_5
    add-int/lit8 v11, p3, 0x1

    .end local p3    # "off":I
    .local v11, "off":I
    add-int/lit8 v10, p1, 0x1

    .end local p1    # "index":I
    .local v10, "index":I
    invoke-virtual/range {p0 .. p1}, Lorg/apache/lucene/util/packed/Packed64;->get(I)J

    move-result-wide v14

    aput-wide v14, p2, p3

    .line 147
    add-int/lit8 p4, p4, -0x1

    .line 145
    add-int/lit8 v9, v9, 0x1

    move/from16 p3, v11

    .end local v11    # "off":I
    .restart local p3    # "off":I
    move/from16 p1, v10

    .end local v10    # "index":I
    .restart local p1    # "index":I
    goto :goto_0

    .line 155
    .end local v9    # "i":I
    :cond_6
    sget-boolean v3, Lorg/apache/lucene/util/packed/Packed64;->$assertionsDisabled:Z

    if-nez v3, :cond_7

    invoke-interface {v2}, Lorg/apache/lucene/util/packed/PackedInts$Decoder;->longValueCount()I

    move-result v3

    rem-int v3, p1, v3

    if-eqz v3, :cond_7

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 156
    :cond_7
    move/from16 v0, p1

    int-to-long v14, v0

    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/util/packed/Packed64;->bitsPerValue:I

    int-to-long v0, v3

    move-wide/from16 v16, v0

    mul-long v14, v14, v16

    const/4 v3, 0x6

    ushr-long/2addr v14, v3

    long-to-int v4, v14

    .line 157
    .local v4, "blockIndex":I
    sget-boolean v3, Lorg/apache/lucene/util/packed/Packed64;->$assertionsDisabled:Z

    if-nez v3, :cond_8

    move/from16 v0, p1

    int-to-long v14, v0

    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/util/packed/Packed64;->bitsPerValue:I

    int-to-long v0, v3

    move-wide/from16 v16, v0

    mul-long v14, v14, v16

    const-wide/16 v16, 0x3f

    and-long v14, v14, v16

    const-wide/16 v16, 0x0

    cmp-long v3, v14, v16

    if-eqz v3, :cond_8

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 158
    :cond_8
    invoke-interface {v2}, Lorg/apache/lucene/util/packed/PackedInts$Decoder;->longValueCount()I

    move-result v3

    div-int v7, p4, v3

    .line 159
    .local v7, "iterations":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/util/packed/Packed64;->blocks:[J

    move-object/from16 v5, p2

    move/from16 v6, p3

    invoke-interface/range {v2 .. v7}, Lorg/apache/lucene/util/packed/PackedInts$Decoder;->decode([JI[JII)V

    .line 160
    invoke-interface {v2}, Lorg/apache/lucene/util/packed/PackedInts$Decoder;->longValueCount()I

    move-result v3

    mul-int v8, v7, v3

    .line 161
    .local v8, "gotValues":I
    add-int p1, p1, v8

    .line 162
    sub-int p4, p4, v8

    .line 163
    sget-boolean v3, Lorg/apache/lucene/util/packed/Packed64;->$assertionsDisabled:Z

    if-nez v3, :cond_9

    if-gez p4, :cond_9

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 165
    :cond_9
    move/from16 v0, p1

    if-le v0, v13, :cond_a

    .line 167
    sub-int v3, p1, v13

    goto/16 :goto_1

    .line 170
    :cond_a
    sget-boolean v3, Lorg/apache/lucene/util/packed/Packed64;->$assertionsDisabled:Z

    if-nez v3, :cond_b

    move/from16 v0, p1

    if-eq v0, v13, :cond_b

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 171
    :cond_b
    invoke-super/range {p0 .. p4}, Lorg/apache/lucene/util/packed/PackedInts$MutableImpl;->get(I[JII)I

    move-result v3

    goto/16 :goto_1
.end method

.method public get(I)J
    .locals 12
    .param p1, "index"    # I

    .prologue
    .line 117
    int-to-long v6, p1

    iget v1, p0, Lorg/apache/lucene/util/packed/Packed64;->bitsPerValue:I

    int-to-long v8, v1

    mul-long v4, v6, v8

    .line 119
    .local v4, "majorBitPos":J
    const/4 v1, 0x6

    ushr-long v6, v4, v1

    long-to-int v0, v6

    .line 121
    .local v0, "elementPos":I
    const-wide/16 v6, 0x3f

    and-long/2addr v6, v4

    iget v1, p0, Lorg/apache/lucene/util/packed/Packed64;->bpvMinusBlockSize:I

    int-to-long v8, v1

    add-long v2, v6, v8

    .line 123
    .local v2, "endBits":J
    const-wide/16 v6, 0x0

    cmp-long v1, v2, v6

    if-gtz v1, :cond_0

    .line 124
    iget-object v1, p0, Lorg/apache/lucene/util/packed/Packed64;->blocks:[J

    aget-wide v6, v1, v0

    neg-long v8, v2

    long-to-int v1, v8

    ushr-long/2addr v6, v1

    iget-wide v8, p0, Lorg/apache/lucene/util/packed/Packed64;->maskRight:J

    and-long/2addr v6, v8

    .line 127
    :goto_0
    return-wide v6

    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/util/packed/Packed64;->blocks:[J

    aget-wide v6, v1, v0

    long-to-int v1, v2

    shl-long/2addr v6, v1

    .line 128
    iget-object v1, p0, Lorg/apache/lucene/util/packed/Packed64;->blocks:[J

    add-int/lit8 v8, v0, 0x1

    aget-wide v8, v1, v8

    const-wide/16 v10, 0x40

    sub-long/2addr v10, v2

    long-to-int v1, v10

    ushr-long/2addr v8, v1

    .line 127
    or-long/2addr v6, v8

    .line 129
    iget-wide v8, p0, Lorg/apache/lucene/util/packed/Packed64;->maskRight:J

    .line 127
    and-long/2addr v6, v8

    goto :goto_0
.end method

.method public ramBytesUsed()J
    .locals 4

    .prologue
    .line 248
    sget v0, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_HEADER:I

    add-int/lit8 v0, v0, 0xc

    add-int/lit8 v0, v0, 0x8

    .line 251
    sget v1, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_REF:I

    .line 248
    add-int/2addr v0, v1

    int-to-long v0, v0

    .line 247
    invoke-static {v0, v1}, Lorg/apache/lucene/util/RamUsageEstimator;->alignObjectSize(J)J

    move-result-wide v0

    .line 252
    iget-object v2, p0, Lorg/apache/lucene/util/packed/Packed64;->blocks:[J

    invoke-static {v2}, Lorg/apache/lucene/util/RamUsageEstimator;->sizeOf([J)J

    move-result-wide v2

    .line 247
    add-long/2addr v0, v2

    return-wide v0
.end method

.method public set(I[JII)I
    .locals 16
    .param p1, "index"    # I
    .param p2, "arr"    # [J
    .param p3, "off"    # I
    .param p4, "len"    # I

    .prologue
    .line 198
    sget-boolean v3, Lorg/apache/lucene/util/packed/Packed64;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    if-gtz p4, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "len must be > 0 (got "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 199
    :cond_0
    sget-boolean v3, Lorg/apache/lucene/util/packed/Packed64;->$assertionsDisabled:Z

    if-nez v3, :cond_2

    if-ltz p1, :cond_1

    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/util/packed/Packed64;->valueCount:I

    move/from16 v0, p1

    if-lt v0, v3, :cond_2

    :cond_1
    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 200
    :cond_2
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/util/packed/Packed64;->valueCount:I

    sub-int v3, v3, p1

    move/from16 v0, p4

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result p4

    .line 201
    sget-boolean v3, Lorg/apache/lucene/util/packed/Packed64;->$assertionsDisabled:Z

    if-nez v3, :cond_3

    add-int v3, p3, p4

    move-object/from16 v0, p2

    array-length v4, v0

    if-le v3, v4, :cond_3

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 203
    :cond_3
    move/from16 v12, p1

    .line 204
    .local v12, "originalIndex":I
    sget-object v3, Lorg/apache/lucene/util/packed/PackedInts$Format;->PACKED:Lorg/apache/lucene/util/packed/PackedInts$Format;

    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/lucene/util/packed/Packed64;->bitsPerValue:I

    invoke-static {v3, v4}, Lorg/apache/lucene/util/packed/BulkOperation;->of(Lorg/apache/lucene/util/packed/PackedInts$Format;I)Lorg/apache/lucene/util/packed/BulkOperation;

    move-result-object v2

    .line 207
    .local v2, "encoder":Lorg/apache/lucene/util/packed/PackedInts$Encoder;
    invoke-interface {v2}, Lorg/apache/lucene/util/packed/PackedInts$Encoder;->longValueCount()I

    move-result v3

    rem-int v11, p1, v3

    .line 208
    .local v11, "offsetInBlocks":I
    if-eqz v11, :cond_6

    .line 209
    move v8, v11

    .local v8, "i":I
    :goto_0
    invoke-interface {v2}, Lorg/apache/lucene/util/packed/PackedInts$Encoder;->longValueCount()I

    move-result v3

    if-ge v8, v3, :cond_4

    if-gtz p4, :cond_5

    .line 213
    :cond_4
    if-nez p4, :cond_6

    .line 214
    sub-int v3, p1, v12

    .line 235
    .end local v8    # "i":I
    :goto_1
    return v3

    .line 210
    .restart local v8    # "i":I
    :cond_5
    add-int/lit8 v9, p1, 0x1

    .end local p1    # "index":I
    .local v9, "index":I
    add-int/lit8 v10, p3, 0x1

    .end local p3    # "off":I
    .local v10, "off":I
    aget-wide v4, p2, p3

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v4, v5}, Lorg/apache/lucene/util/packed/Packed64;->set(IJ)V

    .line 211
    add-int/lit8 p4, p4, -0x1

    .line 209
    add-int/lit8 v8, v8, 0x1

    move/from16 p3, v10

    .end local v10    # "off":I
    .restart local p3    # "off":I
    move/from16 p1, v9

    .end local v9    # "index":I
    .restart local p1    # "index":I
    goto :goto_0

    .line 219
    .end local v8    # "i":I
    :cond_6
    sget-boolean v3, Lorg/apache/lucene/util/packed/Packed64;->$assertionsDisabled:Z

    if-nez v3, :cond_7

    invoke-interface {v2}, Lorg/apache/lucene/util/packed/PackedInts$Encoder;->longValueCount()I

    move-result v3

    rem-int v3, p1, v3

    if-eqz v3, :cond_7

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 220
    :cond_7
    move/from16 v0, p1

    int-to-long v4, v0

    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/util/packed/Packed64;->bitsPerValue:I

    int-to-long v14, v3

    mul-long/2addr v4, v14

    const/4 v3, 0x6

    ushr-long/2addr v4, v3

    long-to-int v6, v4

    .line 221
    .local v6, "blockIndex":I
    sget-boolean v3, Lorg/apache/lucene/util/packed/Packed64;->$assertionsDisabled:Z

    if-nez v3, :cond_8

    move/from16 v0, p1

    int-to-long v4, v0

    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/util/packed/Packed64;->bitsPerValue:I

    int-to-long v14, v3

    mul-long/2addr v4, v14

    const-wide/16 v14, 0x3f

    and-long/2addr v4, v14

    const-wide/16 v14, 0x0

    cmp-long v3, v4, v14

    if-eqz v3, :cond_8

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 222
    :cond_8
    invoke-interface {v2}, Lorg/apache/lucene/util/packed/PackedInts$Encoder;->longValueCount()I

    move-result v3

    div-int v7, p4, v3

    .line 223
    .local v7, "iterations":I
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/util/packed/Packed64;->blocks:[J

    move-object/from16 v3, p2

    move/from16 v4, p3

    invoke-interface/range {v2 .. v7}, Lorg/apache/lucene/util/packed/PackedInts$Encoder;->encode([JI[JII)V

    .line 224
    invoke-interface {v2}, Lorg/apache/lucene/util/packed/PackedInts$Encoder;->longValueCount()I

    move-result v3

    mul-int v13, v7, v3

    .line 225
    .local v13, "setValues":I
    add-int p1, p1, v13

    .line 226
    sub-int p4, p4, v13

    .line 227
    sget-boolean v3, Lorg/apache/lucene/util/packed/Packed64;->$assertionsDisabled:Z

    if-nez v3, :cond_9

    if-gez p4, :cond_9

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 229
    :cond_9
    move/from16 v0, p1

    if-le v0, v12, :cond_a

    .line 231
    sub-int v3, p1, v12

    goto :goto_1

    .line 234
    :cond_a
    sget-boolean v3, Lorg/apache/lucene/util/packed/Packed64;->$assertionsDisabled:Z

    if-nez v3, :cond_b

    move/from16 v0, p1

    if-eq v0, v12, :cond_b

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 235
    :cond_b
    invoke-super/range {p0 .. p4}, Lorg/apache/lucene/util/packed/PackedInts$MutableImpl;->set(I[JII)I

    move-result v3

    goto/16 :goto_1
.end method

.method public set(IJ)V
    .locals 12
    .param p1, "index"    # I
    .param p2, "value"    # J

    .prologue
    .line 178
    int-to-long v6, p1

    iget v1, p0, Lorg/apache/lucene/util/packed/Packed64;->bitsPerValue:I

    int-to-long v8, v1

    mul-long v4, v6, v8

    .line 180
    .local v4, "majorBitPos":J
    const/4 v1, 0x6

    ushr-long v6, v4, v1

    long-to-int v0, v6

    .line 182
    .local v0, "elementPos":I
    const-wide/16 v6, 0x3f

    and-long/2addr v6, v4

    iget v1, p0, Lorg/apache/lucene/util/packed/Packed64;->bpvMinusBlockSize:I

    int-to-long v8, v1

    add-long v2, v6, v8

    .line 184
    .local v2, "endBits":J
    const-wide/16 v6, 0x0

    cmp-long v1, v2, v6

    if-gtz v1, :cond_0

    .line 185
    iget-object v1, p0, Lorg/apache/lucene/util/packed/Packed64;->blocks:[J

    iget-object v6, p0, Lorg/apache/lucene/util/packed/Packed64;->blocks:[J

    aget-wide v6, v6, v0

    iget-wide v8, p0, Lorg/apache/lucene/util/packed/Packed64;->maskRight:J

    neg-long v10, v2

    long-to-int v10, v10

    shl-long/2addr v8, v10

    const-wide/16 v10, -0x1

    xor-long/2addr v8, v10

    and-long/2addr v6, v8

    .line 186
    neg-long v8, v2

    long-to-int v8, v8

    shl-long v8, p2, v8

    or-long/2addr v6, v8

    .line 185
    aput-wide v6, v1, v0

    .line 194
    :goto_0
    return-void

    .line 190
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/util/packed/Packed64;->blocks:[J

    iget-object v6, p0, Lorg/apache/lucene/util/packed/Packed64;->blocks:[J

    aget-wide v6, v6, v0

    iget-wide v8, p0, Lorg/apache/lucene/util/packed/Packed64;->maskRight:J

    long-to-int v10, v2

    ushr-long/2addr v8, v10

    const-wide/16 v10, -0x1

    xor-long/2addr v8, v10

    and-long/2addr v6, v8

    .line 191
    long-to-int v8, v2

    ushr-long v8, p2, v8

    or-long/2addr v6, v8

    .line 190
    aput-wide v6, v1, v0

    .line 192
    iget-object v1, p0, Lorg/apache/lucene/util/packed/Packed64;->blocks:[J

    add-int/lit8 v6, v0, 0x1

    iget-object v7, p0, Lorg/apache/lucene/util/packed/Packed64;->blocks:[J

    add-int/lit8 v8, v0, 0x1

    aget-wide v8, v7, v8

    const-wide/16 v10, -0x1

    long-to-int v7, v2

    ushr-long/2addr v10, v7

    and-long/2addr v8, v10

    .line 193
    const-wide/16 v10, 0x40

    sub-long/2addr v10, v2

    long-to-int v7, v10

    shl-long v10, p2, v7

    or-long/2addr v8, v10

    .line 192
    aput-wide v8, v1, v6

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 241
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Packed64(bitsPerValue="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lorg/apache/lucene/util/packed/Packed64;->bitsPerValue:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", size="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 242
    invoke-virtual {p0}, Lorg/apache/lucene/util/packed/Packed64;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", elements.length="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/util/packed/Packed64;->blocks:[J

    array-length v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 241
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
