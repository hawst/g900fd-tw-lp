.class abstract Lorg/apache/lucene/util/packed/PackedInts$ReaderIteratorImpl;
.super Ljava/lang/Object;
.source "PackedInts.java"

# interfaces
.implements Lorg/apache/lucene/util/packed/PackedInts$ReaderIterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/packed/PackedInts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x408
    name = "ReaderIteratorImpl"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field protected final bitsPerValue:I

.field protected final in:Lorg/apache/lucene/store/DataInput;

.field protected final valueCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 517
    const-class v0, Lorg/apache/lucene/util/packed/PackedInts;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/packed/PackedInts$ReaderIteratorImpl;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected constructor <init>(IILorg/apache/lucene/store/DataInput;)V
    .locals 0
    .param p1, "valueCount"    # I
    .param p2, "bitsPerValue"    # I
    .param p3, "in"    # Lorg/apache/lucene/store/DataInput;

    .prologue
    .line 523
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 524
    iput-object p3, p0, Lorg/apache/lucene/util/packed/PackedInts$ReaderIteratorImpl;->in:Lorg/apache/lucene/store/DataInput;

    .line 525
    iput p2, p0, Lorg/apache/lucene/util/packed/PackedInts$ReaderIteratorImpl;->bitsPerValue:I

    .line 526
    iput p1, p0, Lorg/apache/lucene/util/packed/PackedInts$ReaderIteratorImpl;->valueCount:I

    .line 527
    return-void
.end method


# virtual methods
.method public getBitsPerValue()I
    .locals 1

    .prologue
    .line 541
    iget v0, p0, Lorg/apache/lucene/util/packed/PackedInts$ReaderIteratorImpl;->bitsPerValue:I

    return v0
.end method

.method public next()J
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 531
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lorg/apache/lucene/util/packed/PackedInts$ReaderIteratorImpl;->next(I)Lorg/apache/lucene/util/LongsRef;

    move-result-object v0

    .line 532
    .local v0, "nextValues":Lorg/apache/lucene/util/LongsRef;
    sget-boolean v1, Lorg/apache/lucene/util/packed/PackedInts$ReaderIteratorImpl;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget v1, v0, Lorg/apache/lucene/util/LongsRef;->length:I

    if-gtz v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 533
    :cond_0
    iget-object v1, v0, Lorg/apache/lucene/util/LongsRef;->longs:[J

    iget v4, v0, Lorg/apache/lucene/util/LongsRef;->offset:I

    aget-wide v2, v1, v4

    .line 534
    .local v2, "result":J
    iget v1, v0, Lorg/apache/lucene/util/LongsRef;->offset:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lorg/apache/lucene/util/LongsRef;->offset:I

    .line 535
    iget v1, v0, Lorg/apache/lucene/util/LongsRef;->length:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Lorg/apache/lucene/util/LongsRef;->length:I

    .line 536
    return-wide v2
.end method

.method public size()I
    .locals 1

    .prologue
    .line 546
    iget v0, p0, Lorg/apache/lucene/util/packed/PackedInts$ReaderIteratorImpl;->valueCount:I

    return v0
.end method
