.class Lorg/apache/lucene/util/automaton/Lev2TParametricDescription;
.super Lorg/apache/lucene/util/automaton/LevenshteinAutomata$ParametricDescription;
.source "Lev2TParametricDescription.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final offsetIncrs0:[J

.field private static final offsetIncrs1:[J

.field private static final offsetIncrs2:[J

.field private static final offsetIncrs3:[J

.field private static final offsetIncrs4:[J

.field private static final offsetIncrs5:[J

.field private static final toStates0:[J

.field private static final toStates1:[J

.field private static final toStates2:[J

.field private static final toStates3:[J

.field private static final toStates4:[J

.field private static final toStates5:[J


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x7

    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 28
    const-class v0, Lorg/apache/lucene/util/automaton/Lev2TParametricDescription;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/automaton/Lev2TParametricDescription;->$assertionsDisabled:Z

    .line 88
    new-array v0, v1, [J

    .line 89
    const-wide/16 v4, 0x23

    aput-wide v4, v0, v2

    .line 88
    sput-object v0, Lorg/apache/lucene/util/automaton/Lev2TParametricDescription;->toStates0:[J

    .line 91
    new-array v0, v1, [J

    sput-object v0, Lorg/apache/lucene/util/automaton/Lev2TParametricDescription;->offsetIncrs0:[J

    .line 96
    new-array v0, v1, [J

    .line 97
    const-wide/32 v4, 0x13688b44

    aput-wide v4, v0, v2

    .line 96
    sput-object v0, Lorg/apache/lucene/util/automaton/Lev2TParametricDescription;->toStates1:[J

    .line 99
    new-array v0, v1, [J

    .line 100
    const-wide/16 v4, 0x3e0

    aput-wide v4, v0, v2

    .line 99
    sput-object v0, Lorg/apache/lucene/util/automaton/Lev2TParametricDescription;->offsetIncrs1:[J

    .line 104
    new-array v0, v7, [J

    fill-array-data v0, :array_0

    sput-object v0, Lorg/apache/lucene/util/automaton/Lev2TParametricDescription;->toStates2:[J

    .line 107
    new-array v0, v6, [J

    fill-array-data v0, :array_1

    sput-object v0, Lorg/apache/lucene/util/automaton/Lev2TParametricDescription;->offsetIncrs2:[J

    .line 112
    const/16 v0, 0x12

    new-array v0, v0, [J

    fill-array-data v0, :array_2

    sput-object v0, Lorg/apache/lucene/util/automaton/Lev2TParametricDescription;->toStates3:[J

    .line 119
    new-array v0, v8, [J

    fill-array-data v0, :array_3

    sput-object v0, Lorg/apache/lucene/util/automaton/Lev2TParametricDescription;->offsetIncrs3:[J

    .line 125
    const/16 v0, 0x44

    new-array v0, v0, [J

    .line 126
    const-wide v4, 0x3801450002c5004L

    aput-wide v4, v0, v2

    const-wide v4, -0x3afffeb4fffff1c8L    # -2.4186148732672623E24

    aput-wide v4, v0, v1

    const-wide v4, 0x51451401402L

    aput-wide v4, v0, v6

    .line 127
    const-wide v4, 0x518000b14010000L

    aput-wide v4, v0, v7

    const/4 v3, 0x5

    const-wide v4, 0x9f1c20828e20230L

    aput-wide v4, v0, v3

    const/4 v3, 0x6

    const-wide v4, 0x219f0df0830a70c2L    # 9.71462370588668E-147

    aput-wide v4, v0, v3

    const-wide v4, -0x7dffff7df7df7e00L    # -4.77890227506568E-299

    aput-wide v4, v0, v8

    const/16 v3, 0x8

    .line 128
    const-wide v4, 0x805050160800800L

    aput-wide v4, v0, v3

    const/16 v3, 0x9

    const-wide v4, 0x3082098602602643L    # 4.984713878299693E-75

    aput-wide v4, v0, v3

    const/16 v3, 0xa

    const-wide v4, 0x4564014250508064L    # 1.9347569624920805E26

    aput-wide v4, v0, v3

    const/16 v3, 0xb

    const-wide v4, 0x850051420000831L

    aput-wide v4, v0, v3

    const/16 v3, 0xc

    .line 129
    const-wide v4, 0x4140582085002082L    # 2142273.0390663752

    aput-wide v4, v0, v3

    const/16 v3, 0xd

    const-wide v4, 0x456180980990c201L    # 1.6927205150095414E26

    aput-wide v4, v0, v3

    const/16 v3, 0xe

    const-wide v4, 0x8316d0c50a01051L

    aput-wide v4, v0, v3

    const/16 v3, 0xf

    const-wide v4, 0x21451420050df0e0L

    aput-wide v4, v0, v3

    const/16 v3, 0x10

    .line 130
    const-wide v4, 0xd14214014508214L

    aput-wide v4, v0, v3

    const/16 v3, 0x11

    const-wide v4, 0x3c21c01850821c60L    # 4.811247702758788E-19

    aput-wide v4, v0, v3

    const/16 v3, 0x12

    const-wide v4, 0x1cb1403cb142087L

    aput-wide v4, v0, v3

    const/16 v3, 0x13

    const-wide v4, -0x7ff7debae7ae7dd4L

    aput-wide v4, v0, v3

    const/16 v3, 0x14

    .line 131
    const-wide v4, 0x20020820800020L

    aput-wide v4, v0, v3

    const/16 v3, 0x15

    const-wide v4, -0x2ff9e7df78e7fcbbL    # -3.1979236788452093E77

    aput-wide v4, v0, v3

    const/16 v3, 0x16

    const-wide v4, -0x34f57e34db6894f7L    # -3.1735834721388346E53

    aput-wide v4, v0, v3

    const/16 v3, 0x17

    const-wide v4, 0x8b1a60e624709d1L

    aput-wide v4, v0, v3

    const/16 v3, 0x18

    .line 132
    const-wide v4, 0x249082082249089L

    aput-wide v4, v0, v3

    const/16 v3, 0x19

    const-wide v4, -0x3cebde39ff2d3fdcL    # -1.416658458816521E15

    aput-wide v4, v0, v3

    const/16 v3, 0x1a

    const-wide v4, 0x3c31451515454423L    # 9.362006044746005E-19

    aput-wide v4, v0, v3

    const/16 v3, 0x1b

    const-wide v4, 0x31853c22c21cb140L    # 3.8459297657838966E-70

    aput-wide v4, v0, v3

    const/16 v3, 0x1c

    .line 133
    const-wide v4, 0x4514500b2c208214L    # 6.139127951197831E24

    aput-wide v4, v0, v3

    const/16 v3, 0x1d

    const-wide v4, 0x8718034508b0051L

    aput-wide v4, v0, v3

    const/16 v3, 0x1e

    const-wide v4, -0x4d34baaeaef70f3bL    # -5.1790292680743316E-64

    aput-wide v4, v0, v3

    const/16 v3, 0x1f

    const-wide v4, -0x17db8ea2e34f57f0L    # -4.663459218451745E193

    aput-wide v4, v0, v3

    const/16 v3, 0x20

    .line 134
    const-wide v4, 0x1422cb14908b0e60L

    aput-wide v4, v0, v3

    const/16 v3, 0x21

    const-wide v4, 0x30812c22c02cb145L    # 4.745721160451521E-75

    aput-wide v4, v0, v3

    const/16 v3, 0x22

    const-wide v4, -0x7bdfddfdf34ebdf4L    # -8.277231372551397E-289

    aput-wide v4, v0, v3

    const/16 v3, 0x23

    const-wide v4, 0x5c20ce0820ce0850L    # 6.107193885659166E135

    aput-wide v4, v0, v3

    const/16 v3, 0x24

    .line 135
    const-wide v4, 0x208208208b0d70c2L    # 4.3035811165895E-152

    aput-wide v4, v0, v3

    const/16 v3, 0x25

    const-wide v4, 0x4208508214214208L    # 1.3053739652157242E10

    aput-wide v4, v0, v3

    const/16 v3, 0x26

    const-wide v4, 0x920834050830c20L

    aput-wide v4, v0, v3

    const/16 v3, 0x27

    const-wide v4, -0x39ecb239ec9aca6eL    # -3.8235122310787354E29

    aput-wide v4, v0, v3

    const/16 v3, 0x28

    .line 136
    const-wide v4, -0x2cf6cbe3923b24b3L    # -1.0268111196903335E92

    aput-wide v4, v0, v3

    const/16 v3, 0x29

    const-wide v4, 0x6424d90854d34d34L

    aput-wide v4, v0, v3

    const/16 v3, 0x2a

    const-wide v4, -0x6df8d3ddfcf7eb3eL

    aput-wide v4, v0, v3

    const/16 v3, 0x2b

    const-wide v4, 0x4220724b24a30930L    # 3.5318501969517944E10

    aput-wide v4, v0, v3

    const/16 v3, 0x2c

    .line 137
    const-wide v4, 0x2470d72025c920e2L    # 3.707085760962119E-133

    aput-wide v4, v0, v3

    const/16 v3, 0x2d

    const-wide v4, -0x6d36d28f68a36f7eL

    aput-wide v4, v0, v3

    const/16 v3, 0x2e

    const-wide v4, -0x34f77f3dfb6db1f8L    # -2.933646692959005E53

    aput-wide v4, v0, v3

    const/16 v3, 0x2f

    const-wide v4, 0x45739728c24c2481L    # 3.7893471651135515E26

    aput-wide v4, v0, v3

    const/16 v3, 0x30

    .line 138
    const-wide v4, -0x3925b24a259e8b26L    # -2.1339979960054928E33

    aput-wide v4, v0, v3

    const/16 v3, 0x31

    const-wide v4, 0x4b5d35d75d30971dL    # 1.1191170307625987E55

    aput-wide v4, v0, v3

    const/16 v3, 0x32

    const-wide v4, 0x1030815c93825ce2L

    aput-wide v4, v0, v3

    const/16 v3, 0x33

    const-wide v4, 0x51442051020cb145L    # 3.0545791217289346E83

    aput-wide v4, v0, v3

    const/16 v3, 0x34

    .line 139
    const-wide v4, -0x3ac7def1d3ddf1d4L    # -2.91703191992227E25

    aput-wide v4, v0, v3

    const/16 v3, 0x35

    const-wide v4, 0x851421452cb0d70L

    aput-wide v4, v0, v3

    const/16 v3, 0x36

    const-wide v4, 0x204b085085145142L

    aput-wide v4, v0, v3

    const/16 v3, 0x37

    const-wide v4, -0x6dea9f7cbfaebbf4L

    aput-wide v4, v0, v3

    const/16 v3, 0x38

    .line 140
    const-wide v4, 0x4d660e4da60e6595L    # 7.258607685125584E64

    aput-wide v4, v0, v3

    const/16 v3, 0x39

    const-wide v4, -0x6b26eb1be39239a8L    # -3.0516754599910276E-208

    aput-wide v4, v0, v3

    const/16 v3, 0x3a

    const-wide v4, -0x7d9bd9a6ebab2c9bL    # -3.851278790958013E-297

    aput-wide v4, v0, v3

    const/16 v3, 0x3b

    const-wide v4, 0x2892072c51030813L    # 2.928260231305763E-113

    aput-wide v4, v0, v3

    const/16 v3, 0x3c

    .line 141
    const-wide v4, -0x1d3ddf8d34d35cf5L    # -5.344506208445048E167

    aput-wide v4, v0, v3

    const/16 v3, 0x3d

    const-wide v4, 0x452c70d720538910L    # 1.71913981873115E25

    aput-wide v4, v0, v3

    const/16 v3, 0x3e

    const-wide v4, 0x8b2cb2d708e3891L

    aput-wide v4, v0, v3

    const/16 v3, 0x3f

    const-wide v4, -0x7e34ebbf3dfb4db2L    # -5.054390220532729E-300

    aput-wide v4, v0, v3

    const/16 v3, 0x40

    .line 142
    const-wide v4, -0x25bb1c71d73d35dcL    # -7.070066711511585E126

    aput-wide v4, v0, v3

    const/16 v3, 0x41

    const-wide v4, 0x1dc6da6585d660e4L    # 3.1004000119177366E-165

    aput-wide v4, v0, v3

    const/16 v3, 0x42

    const-wide v4, -0x1d34a2cc71a26eb2L    # -8.068044075996416E167

    aput-wide v4, v0, v3

    const/16 v3, 0x43

    const-wide/32 v4, 0x38938238

    aput-wide v4, v0, v3

    .line 125
    sput-object v0, Lorg/apache/lucene/util/automaton/Lev2TParametricDescription;->toStates4:[J

    .line 144
    const/16 v0, 0x22

    new-array v0, v0, [J

    fill-array-data v0, :array_4

    sput-object v0, Lorg/apache/lucene/util/automaton/Lev2TParametricDescription;->offsetIncrs4:[J

    .line 157
    const/16 v0, 0x87

    new-array v0, v0, [J

    .line 158
    const-wide v4, 0x3801450002c5004L

    aput-wide v4, v0, v2

    const-wide v2, -0x3afffeb4fffff1c8L    # -2.4186148732672623E24

    aput-wide v2, v0, v1

    const-wide v2, 0x51451401402L

    aput-wide v2, v0, v6

    .line 159
    const-wide v2, 0x514000b14010000L

    aput-wide v2, v0, v7

    const/4 v1, 0x5

    const-wide v2, 0x550000038e00e0L    # 4.6726551500092494E-307

    aput-wide v2, v0, v1

    const/4 v1, 0x6

    const-wide v2, 0x264518500600b180L

    aput-wide v2, v0, v1

    const-wide v2, -0x7df7df7df7df7df8L    # -7.205387683174058E-299

    aput-wide v2, v0, v8

    const/16 v1, 0x8

    .line 160
    const-wide v2, 0x2c50040820820L

    aput-wide v2, v0, v1

    const/16 v1, 0x9

    const-wide v2, 0x70820a38808c0146L    # 8.96233901099368E233

    aput-wide v2, v0, v1

    const/16 v1, 0xa

    const-wide v2, -0x3c83df3d63cf7d84L    # -1.26677113025079232E17

    aput-wide v2, v0, v1

    const/16 v1, 0xb

    const-wide v2, 0x20820820800867L

    aput-wide v2, v0, v1

    const/16 v1, 0xc

    .line 161
    const-wide v2, -0x4ebfefdffdffdf80L

    aput-wide v2, v0, v1

    const/16 v1, 0xd

    const-wide v2, -0x7d71dfdcffae8000L

    aput-wide v2, v0, v1

    const/16 v1, 0xe

    const-wide v2, 0x830a70c209f1c20L

    aput-wide v2, v0, v1

    const/16 v1, 0xf

    const-wide v2, 0x51451450853df0dfL    # 3.199234733929497E83

    aput-wide v2, v0, v1

    const/16 v1, 0x10

    .line 162
    const-wide v2, 0x1614214214508214L    # 2.568176417001488E-202

    aput-wide v2, v0, v1

    const/16 v1, 0x11

    const-wide v2, 0x6026026430805050L    # 1.4754851020504926E155

    aput-wide v2, v0, v1

    const/16 v1, 0x12

    const-wide v2, 0x2505080643082098L

    aput-wide v2, v0, v1

    const/16 v1, 0x13

    const-wide v2, 0x4200008314564014L    # 8.591008394781288E9

    aput-wide v2, v0, v1

    const/16 v1, 0x14

    .line 163
    const-wide v2, 0x850020820850051L

    aput-wide v2, v0, v1

    const/16 v1, 0x15

    const-wide v2, -0x7f66f3dfebebfa7eL    # -8.917157747921556E-306

    aput-wide v2, v0, v1

    const/16 v1, 0x16

    const-wide v2, -0x7dfe6dfdf7d9e7f7L    # -5.247283953990403E-299

    aput-wide v2, v0, v1

    const/16 v1, 0x17

    const-wide v2, 0x892051990060941L

    aput-wide v2, v0, v1

    const/16 v1, 0x18

    .line 164
    const-wide v2, 0x22492492c22cb242L

    aput-wide v2, v0, v1

    const/16 v1, 0x19

    const-wide v2, 0x430805050162492cL    # 8.451148121561975E14

    aput-wide v2, v0, v1

    const/16 v1, 0x1a

    const-wide v2, -0x7fbebaea79fd9fdaL

    aput-wide v2, v0, v1

    const/16 v1, 0x1b

    const-wide v2, 0x37c38020c5b43142L    # 4.477095247068853E-40

    aput-wide v2, v0, v1

    const/16 v1, 0x1c

    .line 165
    const-wide v2, 0x4208508514508014L    # 1.3053764234062538E10

    aput-wide v2, v0, v1

    const/16 v1, 0x1d

    const-wide v2, 0x141405850850051L

    aput-wide v2, v0, v1

    const/16 v1, 0x1e

    const-wide v2, 0x51456180980990c2L    # 3.244995937624179E83

    aput-wide v2, v0, v1

    const/16 v1, 0x1f

    const-wide v2, -0x1ff7ce92f3af5ff0L    # -4.054700479630406E154

    aput-wide v2, v0, v1

    const/16 v1, 0x20

    .line 166
    const-wide v2, 0x2c52cb2c508b21f0L

    aput-wide v2, v0, v1

    const/16 v1, 0x21

    const-wide v2, 0x600d2c92c22cb249L    # 4.889511522795528E154

    aput-wide v2, v0, v1

    const/16 v1, 0x22

    const-wide v2, -0x78c3de3fe7af7de4L    # -8.125349541330615E-274

    aput-wide v2, v0, v1

    const/16 v1, 0x23

    const-wide v2, 0x2c01cb1403cb1420L    # 1.0412793858675453E-96

    aput-wide v2, v0, v1

    const/16 v1, 0x24

    .line 167
    const-wide v2, 0x2080082145185182L

    aput-wide v2, v0, v1

    const/16 v1, 0x25

    const-wide v2, 0x4500200208208000L    # 2.4367457900478415E24

    aput-wide v2, v0, v1

    const/16 v1, 0x26

    const-wide v2, 0x870061420871803L

    aput-wide v2, v0, v1

    const/16 v1, 0x27

    const-wide v2, 0x740500f5050821cfL    # 7.519050683134454E250

    aput-wide v2, v0, v1

    const/16 v1, 0x28

    .line 168
    const-wide v2, -0x6cb269b9e79f7000L    # -1.072838012873165E-215

    aput-wide v2, v0, v1

    const/16 v1, 0x29

    const-wide v2, 0x4c24d34d30824d30L    # 6.536156646892763E58

    aput-wide v2, v0, v1

    const/16 v1, 0x2a

    const-wide v2, 0x1860821c600d642L

    aput-wide v2, v0, v1

    const/16 v1, 0x2b

    const-wide v2, -0x3d5f8d36da253d8cL    # -9.042593508705227E12

    aput-wide v2, v0, v1

    const/16 v1, 0x2c

    .line 169
    const-wide v2, 0x2c69839891c27472L    # 9.555880759505044E-95

    aput-wide v2, v0, v1

    const/16 v1, 0x2d

    const-wide v2, -0x6dbdf7df76dbddbeL    # -9.976793404238572E-221

    aput-wide v2, v0, v1

    const/16 v1, 0x2e

    const-wide v2, -0x7df78e7fcb4ff700L

    aput-wide v2, v0, v1

    const/16 v1, 0x2f

    const-wide v2, 0x1cb24976b09d0061L

    aput-wide v2, v0, v1

    const/16 v1, 0x30

    .line 170
    const-wide v2, 0x60e624709d1cb0a8L    # 6.0801082724766E158

    aput-wide v2, v0, v1

    const/16 v1, 0x31

    const-wide v2, -0x2cebaa28ea8b31c2L    # -1.656950580573932E92

    aput-wide v2, v0, v1

    const/16 v1, 0x32

    const-wide v2, 0x1c600d3825c25d74L    # 5.191966017023265E-172

    aput-wide v2, v0, v1

    const/16 v1, 0x33

    const-wide v2, 0x51515454423c3142L    # 5.260204228232752E83

    aput-wide v2, v0, v1

    const/16 v1, 0x34

    .line 171
    const-wide v2, -0x3dd3de34ebfc3cecL    # -6.041302067388101E10

    aput-wide v2, v0, v1

    const/16 v1, 0x35

    const-wide v2, 0xb2c20821431853L

    aput-wide v2, v0, v1

    const/16 v1, 0x36

    const-wide v2, 0x34508b005145145L

    aput-wide v2, v0, v1

    const/16 v1, 0x37

    const-wide v2, 0x5515108f0c508718L    # 7.371798814756586E101

    aput-wide v2, v0, v1

    const/16 v1, 0x38

    .line 172
    const-wide v2, 0x8740500f2051454L

    aput-wide v2, v0, v1

    const/16 v1, 0x39

    const-wide v2, -0x1dacb26df9e70f70L    # -4.446339852837395E165

    aput-wide v2, v0, v1

    const/16 v1, 0x3a

    const-wide v2, 0x493826596592c238L    # 5.3855857177483995E44

    aput-wide v2, v0, v1

    const/16 v1, 0x3b

    const-wide v2, 0x4423c31421c600d6L    # 1.8227251743585645E20

    aput-wide v2, v0, v1

    const/16 v1, 0x3c

    .line 173
    const-wide v2, 0x72c2a042cb2d1545L    # 6.358966517152554E244

    aput-wide v2, v0, v1

    const/16 v1, 0x3d

    const-wide v2, 0x422c3983a091c574L    # 6.061200596088565E10

    aput-wide v2, v0, v1

    const/16 v1, 0x3e

    const-wide v2, 0xb2c514508b2c52L

    aput-wide v2, v0, v1

    const/16 v1, 0x3f

    const-wide v2, -0xf3af78e7fcb4f75L    # -1.67188914745477E235

    aput-wide v2, v0, v1

    const/16 v1, 0x40

    .line 174
    const-wide v2, -0x57ef4d34baaeaef8L

    aput-wide v2, v0, v1

    const/16 v1, 0x41

    const-wide v2, 0x2260e824715d1cb0L    # 4.332650321038598E-143

    aput-wide v2, v0, v1

    const/16 v1, 0x42

    const-wide v2, -0x19a6d3ac71d28b32L    # -1.0696333071526975E185

    aput-wide v2, v0, v1

    const/16 v1, 0x43

    const-wide v2, 0x420c308138938238L    # 1.5134107410438583E10

    aput-wide v2, v0, v1

    const/16 v1, 0x44

    .line 175
    const-wide v2, 0x850842022020cb1L

    aput-wide v2, v0, v1

    const/16 v1, 0x45

    const-wide v2, 0x70c25c20ce0820ceL    # 1.459406795874704E235

    aput-wide v2, v0, v1

    const/16 v1, 0x46

    const-wide v2, 0x4208208208208b0dL    # 1.2953075972067896E10

    aput-wide v2, v0, v1

    const/16 v1, 0x47

    const-wide v2, 0xc20420850821421L

    aput-wide v2, v0, v1

    const/16 v1, 0x48

    .line 176
    const-wide v2, 0x21080880832c5083L    # 1.468398600860699E-149

    aput-wide v2, v0, v1

    const/16 v1, 0x49

    const-wide v2, -0x5af7c77df7c73decL    # -2.729852543379502E-130

    aput-wide v2, v0, v1

    const/16 v1, 0x4a

    const-wide v2, -0x55555555563c6bd0L    # -3.72066207348995E-103

    aput-wide v2, v0, v1

    const/16 v1, 0x4b

    const-wide v2, 0x1aaa7eaa9fa9faaaL

    aput-wide v2, v0, v1

    const/16 v1, 0x4c

    .line 177
    const-wide v2, -0x7db7df2febdf3cf8L

    aput-wide v2, v0, v1

    const/16 v1, 0x4d

    const-wide v2, 0x7184d37184d94d64L    # 6.780651428555087E238

    aput-wide v2, v0, v1

    const/16 v1, 0x4e

    const-wide v2, 0x34c24d071b7136d3L    # 1.4927359663692907E-54

    aput-wide v2, v0, v1

    const/16 v1, 0x4f

    const-wide v2, -0x66f6c9bdeacb2cb3L    # -4.526881063545833E-188

    aput-wide v2, v0, v1

    const/16 v1, 0x50

    .line 178
    const-wide v2, 0x834050830c20530L    # 3.78948769041266E-269

    aput-wide v2, v0, v1

    const/16 v1, 0x51

    const-wide v2, 0x34dc613653592092L    # 4.6296603213134565E-54

    aput-wide v2, v0, v1

    const/16 v1, 0x52

    const-wide v2, -0x5b863923b24b239fL    # -5.674281945336279E-133

    aput-wide v2, v0, v1

    const/16 v1, 0x53

    const-wide v2, -0x6df5606db6db6db6L    # -9.20652024826964E-222

    aput-wide v2, v0, v1

    const/16 v1, 0x54

    .line 179
    const-wide v2, 0x72c220308192a82aL    # 6.18817008095364E244

    aput-wide v2, v0, v1

    const/16 v1, 0x55

    const-wide v2, 0x724b24a30930920L

    aput-wide v2, v0, v1

    const/16 v1, 0x56

    const-wide v2, 0xd72025c920e2422L

    aput-wide v2, v0, v1

    const/16 v1, 0x57

    const-wide v2, -0x6d28f68a36f7ddb9L    # -6.526016895833043E-218

    aput-wide v2, v0, v1

    const/16 v1, 0x58

    .line 180
    const-wide v2, -0x77f3dfb6db1f76d4L    # -6.654932625647617E-270

    aput-wide v2, v0, v1

    const/16 v1, 0x59

    const-wide v2, 0x2c928c24c2481cb0L    # 5.557317892915225E-94

    aput-wide v2, v0, v1

    const/16 v1, 0x5a

    const-wide v2, -0x7f5adb7776f778b7L

    aput-wide v2, v0, v1

    const/16 v1, 0x5b

    const-wide v2, 0x6a861b2aaac74394L    # 1.3861798571955992E205

    aput-wide v2, v0, v1

    const/16 v1, 0x5c

    .line 181
    const-wide v2, 0x81b2ca6ab27b278L

    aput-wide v2, v0, v1

    const/16 v1, 0x5d

    const-wide v2, -0x5cf6cf6df8d3ddfdL    # -6.610213800892379E-140

    aput-wide v2, v0, v1

    const/16 v1, 0x5e

    const-wide v2, -0x28967a2c96ea31a4L    # -1.2275974537533616E113

    aput-wide v2, v0, v1

    const/16 v1, 0x5f

    const-wide v2, 0x5d74c25c771b6936L

    aput-wide v2, v0, v1

    const/16 v1, 0x60

    .line 182
    const-wide v2, 0x724e0973892d74d7L    # 4.005732213184863E242

    aput-wide v2, v0, v1

    const/16 v1, 0x61

    const-wide v2, 0x4c2481cb0880c205L    # 6.436227611129538E58

    aput-wide v2, v0, v1

    const/16 v1, 0x62

    const-wide v2, 0x6174da45739728c2L

    aput-wide v2, v0, v1

    const/16 v1, 0x63

    const-wide v2, 0x4aa175c6da4db5daL    # 3.266293032868204E51

    aput-wide v2, v0, v1

    const/16 v1, 0x64

    .line 183
    const-wide v2, 0x6a869b2786486186L    # 1.4175297035400495E205

    aput-wide v2, v0, v1

    const/16 v1, 0x65

    const-wide v2, 0xcb14510308186caL

    aput-wide v2, v0, v1

    const/16 v1, 0x66

    const-wide v2, 0x220e2c5144205102L

    aput-wide v2, v0, v1

    const/16 v1, 0x67

    const-wide v2, -0x34f28f3ac7def1d4L    # -3.5248007237852146E53

    aput-wide v2, v0, v1

    const/16 v1, 0x68

    .line 184
    const-wide v2, 0x1451420851421452L    # 8.202230767742134E-211

    aput-wide v2, v0, v1

    const/16 v1, 0x69

    const-wide v2, 0x51440c204b085085L    # 3.042609195765637E83

    aput-wide v2, v0, v1

    const/16 v1, 0x6a

    const-wide v2, -0x34ebaef7ebbf7cd4L    # -4.864842329164108E53

    aput-wide v2, v0, v1

    const/16 v1, 0x6b

    const-wide v2, -0x6bce9df7b774f778L    # -2.065409908076389E-211

    aput-wide v2, v0, v1

    const/16 v1, 0x6c

    .line 185
    const-wide v2, -0x55582056081863dL    # -7.694181396690184E282

    aput-wide v2, v0, v1

    const/16 v1, 0x6d

    const-wide v2, 0x30819ea7ea7df7dL

    aput-wide v2, v0, v1

    const/16 v1, 0x6e

    const-wide v2, 0x6564855820d01451L    # 2.6609904488407788E180

    aput-wide v2, v0, v1

    const/16 v1, 0x6f

    const-wide v2, -0x69eca67c6c967c67L

    aput-wide v2, v0, v1

    const/16 v1, 0x70

    .line 186
    const-wide v2, -0x269ac9bac6f8e48fL    # -4.381973463143005E122

    aput-wide v2, v0, v1

    const/16 v1, 0x71

    const-wide v2, 0x4e0990996451534L

    aput-wide v2, v0, v1

    const/16 v1, 0x72

    const-wide v2, 0x21560834051440c2L    # 4.307615465211896E-148

    aput-wide v2, v0, v1

    const/16 v1, 0x73

    const-wide v2, -0x299f1b259f19a6a7L    # -1.2398805580078503E108

    aput-wide v2, v0, v1

    const/16 v1, 0x74

    .line 187
    const-wide v2, -0x6df8168639239a7cL    # -8.26893100390551E-222

    aput-wide v2, v0, v1

    const/16 v1, 0x75

    const-wide v2, -0x57d57df8206db7e0L    # -3.363779563062417E-115

    aput-wide v2, v0, v1

    const/16 v1, 0x76

    const-wide v2, -0x76df8d3aefcf7e5aL    # -1.02022356820814E-264

    aput-wide v2, v0, v1

    const/16 v1, 0x77

    const-wide v2, 0x2c22072cb2ca30b2L    # 4.220069103942148E-96

    aput-wide v2, v0, v1

    const/16 v1, 0x78

    .line 188
    const-wide v2, 0x52c70d720538910eL    # 5.869851843854816E90

    aput-wide v2, v0, v1

    const/16 v1, 0x79

    const-wide v2, -0x74d34d28f71c76ecL    # -7.645295861170126E-255

    aput-wide v2, v0, v1

    const/16 v1, 0x7a

    const-wide v2, 0x1cb1440c204b24e0L

    aput-wide v2, v0, v1

    const/16 v1, 0x7b

    const-wide v2, -0x78b4d34d73d35db8L

    aput-wide v2, v0, v1

    const/16 v1, 0x7c

    .line 189
    const-wide v2, 0x4394816224488b08L    # 3.6939251518241229E17

    aput-wide v2, v0, v1

    const/16 v1, 0x7d

    const-wide v2, -0x6187955964e08189L    # -6.784027717604961E-162

    aput-wide v2, v0, v1

    const/16 v1, 0x7e

    const-wide v2, 0x51030819eca6a9e7L    # 1.80528251107711E82

    aput-wide v2, v0, v1

    const/16 v1, 0x7f

    const-wide v2, -0x71c75cf4d76df8d4L

    aput-wide v2, v0, v1

    const/16 v1, 0x80

    .line 190
    const-wide v2, 0x6996175983936913L    # 4.2274319587091186E200

    aput-wide v2, v0, v1

    const/16 v1, 0x81

    const-wide v2, 0x74ce39764538771bL    # 4.4318486122211425E254

    aput-wide v2, v0, v1

    const/16 v1, 0x82

    const-wide v2, -0x3dfb1db1f71c74d3L    # -1.1212013852442957E10

    aput-wide v2, v0, v1

    const/16 v1, 0x83

    const-wide v2, 0x28c2ca2481cb1440L    # 2.441572432384625E-112

    aput-wide v2, v0, v1

    const/16 v1, 0x84

    .line 191
    const-wide v2, -0x7a299f1b25bb1c72L

    aput-wide v2, v0, v1

    const/16 v1, 0x85

    const-wide v2, 0x698607e975c6da65L    # 2.1079460557551947E200

    aput-wide v2, v0, v1

    const/16 v1, 0x86

    const-wide v2, -0x59359559661879b6L    # -7.992244493531821E-122

    aput-wide v2, v0, v1

    .line 157
    sput-object v0, Lorg/apache/lucene/util/automaton/Lev2TParametricDescription;->toStates5:[J

    .line 193
    const/16 v0, 0x44

    new-array v0, v0, [J

    fill-array-data v0, :array_5

    sput-object v0, Lorg/apache/lucene/util/automaton/Lev2TParametricDescription;->offsetIncrs5:[J

    .line 211
    return-void

    :cond_0
    move v0, v2

    .line 28
    goto/16 :goto_0

    .line 104
    :array_0
    .array-data 8
        0x60dbb0b05200b504L    # 3.8017510900830766E158
        0x5233217627062227L    # 9.514146170950325E87
        0x2355543214323235L
        0x4354
    .end array-data

    .line 107
    :array_1
    .array-data 8
        0x555080a800002000L    # 9.240382444787878E102
        0x5555555555L
    .end array-data

    .line 112
    :array_2
    .array-data 8
        -0x18fe3fd6bffa6bfcL    # -1.54468903890854E188
        -0x5fefe9dfff5b0000L    # -2.999451122756337E-154
        -0x4fd373bf5ebe9d78L    # -1.2327207049335856E-76
        0xa821032310858c0L
        0x314423980d28b201L    # 2.2796584120651146E-71
        0x5281e528847788e0L    # 2.8478955724761355E89
        -0x5dc67f2cf73dd7f2L    # -8.169530542091038E-144
        0x1e3294b1a962278cL    # 3.226618404160583E-163
        -0x73becf61dd771ad8L    # -1.200458858065023E-249
        0x11444409021aca21L
        0x11a4624886b1086bL    # 1.101389466530272E-223
        0x2a6258941d6240c4L    # 1.599827436065187E-104
        0x5024a50b489074adL    # 1.195246670450266E78
        0x14821aca520c411aL    # 6.883719358085554E-210
        0x5888b5890b594a44L    # 3.115484778065889E118
        -0x6be29adf3bee5b9bL    # -8.731663428011222E-212
        -0x74a76f8a52959d2cL    # -5.235136181475283E-254
        0x1a5055a4
    .end array-data

    .line 119
    :array_3
    .array-data 8
        0x30c30200002000L
        0x2a0030f3c3fc333cL    # 2.206130005715405E-106
        0x233a00328282a820L
        0x5555555532b283a8L    # 1.1945304135645074E103
        0x5555555555555555L    # 1.1945305291614955E103
        0x5555555555555555L    # 1.1945305291614955E103
        0x5555555555555555L    # 1.1945305291614955E103
    .end array-data

    .line 144
    :array_4
    .array-data 8
        0x3002000000080000L    # 1.9431379250973265E-77
        0x20c060
        -0x7eb6fffffc000000L
        0x4024924110824824L    # 10.285652652648487
        -0x249fcf3c9fffdf7eL
        0x6c36c06c301b0d80L    # 1.9148284044732248E213
        -0x4fe79e4ffff24f25L    # -5.26408841067124E-77
        0x1b7036209188e06dL
        0x800920006d86db7L
        0x4920c2402402490L
        0x49000208249009L
        0x4908128128124804L    # 6.710373195011016E43
        0x34800104124a44a2L    # 8.158653620723385E-56
        -0x3cf6cf6ff2dbfdf4L    # -8.862837089567375E14
        0x40009a0924c24d24L    # 2.07521275251351
        0x4984a069201061aL
        0x494d049271269262L    # 1.2942397830594507E45
        0x2492492492492492L
        -0x6db6db6db6db6db7L
        0x4924924924924924L    # 2.2937909347060067E44
        0x2492492492492492L
        -0x6db6db6db6db6db7L
        0x4924924924924924L    # 2.2937909347060067E44
        0x2492492492492492L
        -0x6db6db6db6db6db7L
        0x4924924924924924L    # 2.2937909347060067E44
        0x2492492492492492L
        -0x6db6db6db6db6db7L
        0x4924924924924924L    # 2.2937909347060067E44
        0x2492492492492492L
        -0x6db6db6db6db6db7L
        0x4924924924924924L    # 2.2937909347060067E44
        0x2492492492492492L
        0x249249249249L
    .end array-data

    .line 193
    :array_5
    .array-data 8
        0x3002000000080000L    # 1.9431379250973265E-77
        0x20c060
        0x100000004000000L
        -0x24924924af9fcfe8L    # -2.6364126282829077E132
        -0x5b7ffffdffffd24aL
        0x1249208841241240L    # 1.390257329937359E-220
        0x4000010000104120L    # 2.000488281723065
        0x2492c42092092052L
        0xc30d800096592d9L
        -0x4fe4f3f93c9fc928L    # -5.8395565656063244E-77
        0x186c00036c36db0dL    # 4.909662099410938E-191
        -0x5279fc9e4fe493faL    # -2.1612980433416358E-89
        0x360001b75b6dd6ddL    # 1.3690289193951604E-48
        -0x3bedcee3f249fcf4L    # -8.389502795234116E19
        0xdb0db6e36e06L
        0x9188e06db01861bL
        0x6dd6db71b72b62L
        0x4024024900800920L    # 10.004463210706092
        0x20824900904920c2L    # 4.36406367290163E-152
        0x1201248040049000L
        0x5524ad4aa4906120L
        0x4092402002480015L    # 1168.031258702283
        -0x6daddaedb7bf6bf7L
        0x4920100124000820L    # 1.7910305394306885E44
        0x29128924204a04a0L    # 7.70745098659839E-111
        -0x6ff7cf2dfffaaab7L
        0x934930c24c24034L
        0x418690002682493L
        -0x65b679ed9edfe5b8L    # -4.8054777275429E-182
        0xc348001355249d4L
        0x24c40930940d2402L
        0x1a40009a0924e24dL    # 3.012842352639923E-182
        0x6204984a06920106L    # 1.4824734026962623E164
        -0x6db6b2ab6d8ed96eL    # -1.3999395058141706E-220
        0x4924924924924924L    # 2.2937909347060067E44
        0x2492492492492492L
        -0x6db6db6db6db6db7L
        0x4924924924924924L    # 2.2937909347060067E44
        0x2492492492492492L
        -0x6db6db6db6db6db7L
        0x4924924924924924L    # 2.2937909347060067E44
        0x2492492492492492L
        -0x6db6db6db6db6db7L
        0x4924924924924924L    # 2.2937909347060067E44
        0x2492492492492492L
        -0x6db6db6db6db6db7L
        0x4924924924924924L    # 2.2937909347060067E44
        0x2492492492492492L
        -0x6db6db6db6db6db7L
        0x4924924924924924L    # 2.2937909347060067E44
        0x2492492492492492L
        -0x6db6db6db6db6db7L
        0x4924924924924924L    # 2.2937909347060067E44
        0x2492492492492492L
        -0x6db6db6db6db6db7L
        0x4924924924924924L    # 2.2937909347060067E44
        0x2492492492492492L
        -0x6db6db6db6db6db7L
        0x4924924924924924L    # 2.2937909347060067E44
        0x2492492492492492L
        -0x6db6db6db6db6db7L
        0x4924924924924924L    # 2.2937909347060067E44
        0x2492492492492492L
        -0x6db6db6db6db6db7L
        0x4924924924924924L    # 2.2937909347060067E44
        0x2492492492492492L
        -0x6db6db6db6db6db7L
        0x24924924
    .end array-data
.end method

.method public constructor <init>(I)V
    .locals 6
    .param p1, "w"    # I

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, -0x1

    const/4 v2, -0x2

    .line 262
    const/16 v0, 0x2d

    new-array v0, v0, [I

    aput v5, v0, v4

    aput v4, v0, v5

    const/4 v1, 0x4

    aput v4, v0, v1

    const/4 v1, 0x6

    aput v3, v0, v1

    const/16 v1, 0x9

    aput v3, v0, v1

    const/16 v1, 0xb

    aput v3, v0, v1

    const/16 v1, 0xc

    aput v3, v0, v1

    const/16 v1, 0xd

    aput v3, v0, v1

    const/16 v1, 0xe

    aput v3, v0, v1

    const/16 v1, 0xf

    aput v3, v0, v1

    const/16 v1, 0x10

    aput v2, v0, v1

    const/16 v1, 0x11

    aput v3, v0, v1

    const/16 v1, 0x12

    aput v3, v0, v1

    const/16 v1, 0x13

    aput v3, v0, v1

    const/16 v1, 0x14

    aput v2, v0, v1

    const/16 v1, 0x15

    aput v3, v0, v1

    const/16 v1, 0x16

    aput v3, v0, v1

    const/16 v1, 0x17

    aput v2, v0, v1

    const/16 v1, 0x18

    aput v3, v0, v1

    const/16 v1, 0x19

    aput v3, v0, v1

    const/16 v1, 0x1a

    aput v2, v0, v1

    const/16 v1, 0x1b

    aput v3, v0, v1

    const/16 v1, 0x1c

    aput v2, v0, v1

    const/16 v1, 0x1d

    aput v2, v0, v1

    const/16 v1, 0x1e

    aput v2, v0, v1

    const/16 v1, 0x1f

    aput v2, v0, v1

    const/16 v1, 0x20

    aput v2, v0, v1

    const/16 v1, 0x21

    aput v2, v0, v1

    const/16 v1, 0x22

    aput v2, v0, v1

    const/16 v1, 0x23

    aput v2, v0, v1

    const/16 v1, 0x24

    aput v2, v0, v1

    const/16 v1, 0x25

    aput v2, v0, v1

    const/16 v1, 0x26

    aput v2, v0, v1

    const/16 v1, 0x27

    aput v2, v0, v1

    const/16 v1, 0x28

    aput v2, v0, v1

    const/16 v1, 0x29

    aput v2, v0, v1

    const/16 v1, 0x2a

    aput v2, v0, v1

    const/16 v1, 0x2b

    aput v2, v0, v1

    const/16 v1, 0x2c

    aput v2, v0, v1

    invoke-direct {p0, p1, v5, v0}, Lorg/apache/lucene/util/automaton/LevenshteinAutomata$ParametricDescription;-><init>(II[I)V

    .line 263
    return-void
.end method


# virtual methods
.method transition(III)I
    .locals 9
    .param p1, "absState"    # I
    .param p2, "position"    # I
    .param p3, "vector"    # I

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x1

    const/4 v6, 0x2

    const/4 v3, -0x1

    const/4 v5, 0x3

    .line 33
    sget-boolean v4, Lorg/apache/lucene/util/automaton/Lev2TParametricDescription;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    if-ne p1, v3, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 36
    :cond_0
    iget v4, p0, Lorg/apache/lucene/util/automaton/Lev2TParametricDescription;->w:I

    add-int/lit8 v4, v4, 0x1

    div-int v2, p1, v4

    .line 37
    .local v2, "state":I
    iget v4, p0, Lorg/apache/lucene/util/automaton/Lev2TParametricDescription;->w:I

    add-int/lit8 v4, v4, 0x1

    rem-int v1, p1, v4

    .line 38
    .local v1, "offset":I
    sget-boolean v4, Lorg/apache/lucene/util/automaton/Lev2TParametricDescription;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    if-gez v1, :cond_1

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 40
    :cond_1
    iget v4, p0, Lorg/apache/lucene/util/automaton/Lev2TParametricDescription;->w:I

    if-ne p2, v4, :cond_3

    .line 41
    if-ge v2, v5, :cond_2

    .line 42
    mul-int/lit8 v4, p3, 0x3

    add-int v0, v4, v2

    .line 43
    .local v0, "loc":I
    sget-object v4, Lorg/apache/lucene/util/automaton/Lev2TParametricDescription;->offsetIncrs0:[J

    invoke-virtual {p0, v4, v0, v7}, Lorg/apache/lucene/util/automaton/Lev2TParametricDescription;->unpack([JII)I

    move-result v4

    add-int/2addr v1, v4

    .line 44
    sget-object v4, Lorg/apache/lucene/util/automaton/Lev2TParametricDescription;->toStates0:[J

    invoke-virtual {p0, v4, v0, v6}, Lorg/apache/lucene/util/automaton/Lev2TParametricDescription;->unpack([JII)I

    move-result v4

    add-int/lit8 v2, v4, -0x1

    .line 78
    .end local v0    # "loc":I
    :cond_2
    :goto_0
    if-ne v2, v3, :cond_8

    .line 83
    :goto_1
    return v3

    .line 46
    :cond_3
    iget v4, p0, Lorg/apache/lucene/util/automaton/Lev2TParametricDescription;->w:I

    add-int/lit8 v4, v4, -0x1

    if-ne p2, v4, :cond_4

    .line 47
    if-ge v2, v8, :cond_2

    .line 48
    mul-int/lit8 v4, p3, 0x5

    add-int v0, v4, v2

    .line 49
    .restart local v0    # "loc":I
    sget-object v4, Lorg/apache/lucene/util/automaton/Lev2TParametricDescription;->offsetIncrs1:[J

    invoke-virtual {p0, v4, v0, v7}, Lorg/apache/lucene/util/automaton/Lev2TParametricDescription;->unpack([JII)I

    move-result v4

    add-int/2addr v1, v4

    .line 50
    sget-object v4, Lorg/apache/lucene/util/automaton/Lev2TParametricDescription;->toStates1:[J

    invoke-virtual {p0, v4, v0, v5}, Lorg/apache/lucene/util/automaton/Lev2TParametricDescription;->unpack([JII)I

    move-result v4

    add-int/lit8 v2, v4, -0x1

    .line 52
    goto :goto_0

    .end local v0    # "loc":I
    :cond_4
    iget v4, p0, Lorg/apache/lucene/util/automaton/Lev2TParametricDescription;->w:I

    add-int/lit8 v4, v4, -0x2

    if-ne p2, v4, :cond_5

    .line 53
    const/16 v4, 0xd

    if-ge v2, v4, :cond_2

    .line 54
    mul-int/lit8 v4, p3, 0xd

    add-int v0, v4, v2

    .line 55
    .restart local v0    # "loc":I
    sget-object v4, Lorg/apache/lucene/util/automaton/Lev2TParametricDescription;->offsetIncrs2:[J

    invoke-virtual {p0, v4, v0, v6}, Lorg/apache/lucene/util/automaton/Lev2TParametricDescription;->unpack([JII)I

    move-result v4

    add-int/2addr v1, v4

    .line 56
    sget-object v4, Lorg/apache/lucene/util/automaton/Lev2TParametricDescription;->toStates2:[J

    const/4 v5, 0x4

    invoke-virtual {p0, v4, v0, v5}, Lorg/apache/lucene/util/automaton/Lev2TParametricDescription;->unpack([JII)I

    move-result v4

    add-int/lit8 v2, v4, -0x1

    .line 58
    goto :goto_0

    .end local v0    # "loc":I
    :cond_5
    iget v4, p0, Lorg/apache/lucene/util/automaton/Lev2TParametricDescription;->w:I

    add-int/lit8 v4, v4, -0x3

    if-ne p2, v4, :cond_6

    .line 59
    const/16 v4, 0x1c

    if-ge v2, v4, :cond_2

    .line 60
    mul-int/lit8 v4, p3, 0x1c

    add-int v0, v4, v2

    .line 61
    .restart local v0    # "loc":I
    sget-object v4, Lorg/apache/lucene/util/automaton/Lev2TParametricDescription;->offsetIncrs3:[J

    invoke-virtual {p0, v4, v0, v6}, Lorg/apache/lucene/util/automaton/Lev2TParametricDescription;->unpack([JII)I

    move-result v4

    add-int/2addr v1, v4

    .line 62
    sget-object v4, Lorg/apache/lucene/util/automaton/Lev2TParametricDescription;->toStates3:[J

    invoke-virtual {p0, v4, v0, v8}, Lorg/apache/lucene/util/automaton/Lev2TParametricDescription;->unpack([JII)I

    move-result v4

    add-int/lit8 v2, v4, -0x1

    .line 64
    goto :goto_0

    .end local v0    # "loc":I
    :cond_6
    iget v4, p0, Lorg/apache/lucene/util/automaton/Lev2TParametricDescription;->w:I

    add-int/lit8 v4, v4, -0x4

    if-ne p2, v4, :cond_7

    .line 65
    const/16 v4, 0x2d

    if-ge v2, v4, :cond_2

    .line 66
    mul-int/lit8 v4, p3, 0x2d

    add-int v0, v4, v2

    .line 67
    .restart local v0    # "loc":I
    sget-object v4, Lorg/apache/lucene/util/automaton/Lev2TParametricDescription;->offsetIncrs4:[J

    invoke-virtual {p0, v4, v0, v5}, Lorg/apache/lucene/util/automaton/Lev2TParametricDescription;->unpack([JII)I

    move-result v4

    add-int/2addr v1, v4

    .line 68
    sget-object v4, Lorg/apache/lucene/util/automaton/Lev2TParametricDescription;->toStates4:[J

    const/4 v5, 0x6

    invoke-virtual {p0, v4, v0, v5}, Lorg/apache/lucene/util/automaton/Lev2TParametricDescription;->unpack([JII)I

    move-result v4

    add-int/lit8 v2, v4, -0x1

    .line 70
    goto :goto_0

    .line 71
    .end local v0    # "loc":I
    :cond_7
    const/16 v4, 0x2d

    if-ge v2, v4, :cond_2

    .line 72
    mul-int/lit8 v4, p3, 0x2d

    add-int v0, v4, v2

    .line 73
    .restart local v0    # "loc":I
    sget-object v4, Lorg/apache/lucene/util/automaton/Lev2TParametricDescription;->offsetIncrs5:[J

    invoke-virtual {p0, v4, v0, v5}, Lorg/apache/lucene/util/automaton/Lev2TParametricDescription;->unpack([JII)I

    move-result v4

    add-int/2addr v1, v4

    .line 74
    sget-object v4, Lorg/apache/lucene/util/automaton/Lev2TParametricDescription;->toStates5:[J

    const/4 v5, 0x6

    invoke-virtual {p0, v4, v0, v5}, Lorg/apache/lucene/util/automaton/Lev2TParametricDescription;->unpack([JII)I

    move-result v4

    add-int/lit8 v2, v4, -0x1

    goto/16 :goto_0

    .line 83
    .end local v0    # "loc":I
    :cond_8
    iget v3, p0, Lorg/apache/lucene/util/automaton/Lev2TParametricDescription;->w:I

    add-int/lit8 v3, v3, 0x1

    mul-int/2addr v3, v2

    add-int/2addr v3, v1

    goto/16 :goto_1
.end method
