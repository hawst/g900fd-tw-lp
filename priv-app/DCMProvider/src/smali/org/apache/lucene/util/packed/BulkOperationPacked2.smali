.class final Lorg/apache/lucene/util/packed/BulkOperationPacked2;
.super Lorg/apache/lucene/util/packed/BulkOperationPacked;
.source "BulkOperationPacked2.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lorg/apache/lucene/util/packed/BulkOperationPacked;-><init>(I)V

    .line 29
    return-void
.end method


# virtual methods
.method public decode([BI[III)V
    .locals 5
    .param p1, "blocks"    # [B
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [I
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 43
    const/4 v2, 0x0

    .local v2, "j":I
    move v3, p4

    .end local p4    # "valuesOffset":I
    .local v3, "valuesOffset":I
    move v1, p2

    .end local p2    # "blocksOffset":I
    .local v1, "blocksOffset":I
    :goto_0
    if-lt v2, p5, :cond_0

    .line 50
    return-void

    .line 44
    :cond_0
    add-int/lit8 p2, v1, 0x1

    .end local v1    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v0, p1, v1

    .line 45
    .local v0, "block":B
    add-int/lit8 p4, v3, 0x1

    .end local v3    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    ushr-int/lit8 v4, v0, 0x6

    and-int/lit8 v4, v4, 0x3

    aput v4, p3, v3

    .line 46
    add-int/lit8 v3, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v3    # "valuesOffset":I
    ushr-int/lit8 v4, v0, 0x4

    and-int/lit8 v4, v4, 0x3

    aput v4, p3, p4

    .line 47
    add-int/lit8 p4, v3, 0x1

    .end local v3    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    ushr-int/lit8 v4, v0, 0x2

    and-int/lit8 v4, v4, 0x3

    aput v4, p3, v3

    .line 48
    add-int/lit8 v3, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v3    # "valuesOffset":I
    and-int/lit8 v4, v0, 0x3

    aput v4, p3, p4

    .line 43
    add-int/lit8 v2, v2, 0x1

    move v1, p2

    .end local p2    # "blocksOffset":I
    .restart local v1    # "blocksOffset":I
    goto :goto_0
.end method

.method public decode([BI[JII)V
    .locals 6
    .param p1, "blocks"    # [B
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [J
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 64
    const/4 v2, 0x0

    .local v2, "j":I
    move v3, p4

    .end local p4    # "valuesOffset":I
    .local v3, "valuesOffset":I
    move v1, p2

    .end local p2    # "blocksOffset":I
    .local v1, "blocksOffset":I
    :goto_0
    if-lt v2, p5, :cond_0

    .line 71
    return-void

    .line 65
    :cond_0
    add-int/lit8 p2, v1, 0x1

    .end local v1    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v0, p1, v1

    .line 66
    .local v0, "block":B
    add-int/lit8 p4, v3, 0x1

    .end local v3    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    ushr-int/lit8 v4, v0, 0x6

    and-int/lit8 v4, v4, 0x3

    int-to-long v4, v4

    aput-wide v4, p3, v3

    .line 67
    add-int/lit8 v3, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v3    # "valuesOffset":I
    ushr-int/lit8 v4, v0, 0x4

    and-int/lit8 v4, v4, 0x3

    int-to-long v4, v4

    aput-wide v4, p3, p4

    .line 68
    add-int/lit8 p4, v3, 0x1

    .end local v3    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    ushr-int/lit8 v4, v0, 0x2

    and-int/lit8 v4, v4, 0x3

    int-to-long v4, v4

    aput-wide v4, p3, v3

    .line 69
    add-int/lit8 v3, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v3    # "valuesOffset":I
    and-int/lit8 v4, v0, 0x3

    int-to-long v4, v4

    aput-wide v4, p3, p4

    .line 64
    add-int/lit8 v2, v2, 0x1

    move v1, p2

    .end local p2    # "blocksOffset":I
    .restart local v1    # "blocksOffset":I
    goto :goto_0
.end method

.method public decode([JI[III)V
    .locals 10
    .param p1, "blocks"    # [J
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [I
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 33
    const/4 v3, 0x0

    .local v3, "i":I
    move v2, p2

    .end local p2    # "blocksOffset":I
    .local v2, "blocksOffset":I
    :goto_0
    if-lt v3, p5, :cond_0

    .line 39
    return-void

    .line 34
    :cond_0
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v0, p1, v2

    .line 35
    .local v0, "block":J
    const/16 v4, 0x3e

    .local v4, "shift":I
    move v5, p4

    .end local p4    # "valuesOffset":I
    .local v5, "valuesOffset":I
    :goto_1
    if-gez v4, :cond_1

    .line 33
    add-int/lit8 v3, v3, 0x1

    move p4, v5

    .end local v5    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    move v2, p2

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    goto :goto_0

    .line 36
    .end local v2    # "blocksOffset":I
    .end local p4    # "valuesOffset":I
    .restart local v5    # "valuesOffset":I
    .restart local p2    # "blocksOffset":I
    :cond_1
    add-int/lit8 p4, v5, 0x1

    .end local v5    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    ushr-long v6, v0, v4

    const-wide/16 v8, 0x3

    and-long/2addr v6, v8

    long-to-int v6, v6

    aput v6, p3, v5

    .line 35
    add-int/lit8 v4, v4, -0x2

    move v5, p4

    .end local p4    # "valuesOffset":I
    .restart local v5    # "valuesOffset":I
    goto :goto_1
.end method

.method public decode([JI[JII)V
    .locals 10
    .param p1, "blocks"    # [J
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [J
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 54
    const/4 v3, 0x0

    .local v3, "i":I
    move v2, p2

    .end local p2    # "blocksOffset":I
    .local v2, "blocksOffset":I
    :goto_0
    if-lt v3, p5, :cond_0

    .line 60
    return-void

    .line 55
    :cond_0
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v0, p1, v2

    .line 56
    .local v0, "block":J
    const/16 v4, 0x3e

    .local v4, "shift":I
    move v5, p4

    .end local p4    # "valuesOffset":I
    .local v5, "valuesOffset":I
    :goto_1
    if-gez v4, :cond_1

    .line 54
    add-int/lit8 v3, v3, 0x1

    move p4, v5

    .end local v5    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    move v2, p2

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    goto :goto_0

    .line 57
    .end local v2    # "blocksOffset":I
    .end local p4    # "valuesOffset":I
    .restart local v5    # "valuesOffset":I
    .restart local p2    # "blocksOffset":I
    :cond_1
    add-int/lit8 p4, v5, 0x1

    .end local v5    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    ushr-long v6, v0, v4

    const-wide/16 v8, 0x3

    and-long/2addr v6, v8

    aput-wide v6, p3, v5

    .line 56
    add-int/lit8 v4, v4, -0x2

    move v5, p4

    .end local p4    # "valuesOffset":I
    .restart local v5    # "valuesOffset":I
    goto :goto_1
.end method
