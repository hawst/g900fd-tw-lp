.class public Lorg/apache/lucene/util/OpenBitSetIterator;
.super Lorg/apache/lucene/search/DocIdSetIterator;
.source "OpenBitSetIterator.java"


# static fields
.field protected static final bitlist:[I


# instance fields
.field final arr:[J

.field private curDocId:I

.field private i:I

.field private indexArray:I

.field private word:J

.field private wordShift:I

.field final words:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x5

    const/4 v4, 0x4

    const/4 v3, 0x3

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 33
    const/16 v0, 0x100

    new-array v0, v0, [I

    .line 34
    aput v1, v0, v1

    aput v2, v0, v2

    const/16 v1, 0x21

    aput v1, v0, v3

    aput v3, v0, v4

    const/16 v1, 0x31

    aput v1, v0, v5

    const/4 v1, 0x6

    const/16 v2, 0x32

    aput v2, v0, v1

    const/4 v1, 0x7

    const/16 v2, 0x321

    aput v2, v0, v1

    const/16 v1, 0x8

    aput v4, v0, v1

    const/16 v1, 0x9

    const/16 v2, 0x41

    aput v2, v0, v1

    const/16 v1, 0xa

    const/16 v2, 0x42

    aput v2, v0, v1

    const/16 v1, 0xb

    const/16 v2, 0x421

    aput v2, v0, v1

    const/16 v1, 0xc

    const/16 v2, 0x43

    aput v2, v0, v1

    const/16 v1, 0xd

    .line 35
    const/16 v2, 0x431

    aput v2, v0, v1

    const/16 v1, 0xe

    const/16 v2, 0x432

    aput v2, v0, v1

    const/16 v1, 0xf

    const/16 v2, 0x4321

    aput v2, v0, v1

    const/16 v1, 0x10

    aput v5, v0, v1

    const/16 v1, 0x11

    const/16 v2, 0x51

    aput v2, v0, v1

    const/16 v1, 0x12

    const/16 v2, 0x52

    aput v2, v0, v1

    const/16 v1, 0x13

    const/16 v2, 0x521

    aput v2, v0, v1

    const/16 v1, 0x14

    const/16 v2, 0x53

    aput v2, v0, v1

    const/16 v1, 0x15

    const/16 v2, 0x531

    aput v2, v0, v1

    const/16 v1, 0x16

    const/16 v2, 0x532

    aput v2, v0, v1

    const/16 v1, 0x17

    const/16 v2, 0x5321

    aput v2, v0, v1

    const/16 v1, 0x18

    .line 36
    const/16 v2, 0x54

    aput v2, v0, v1

    const/16 v1, 0x19

    const/16 v2, 0x541

    aput v2, v0, v1

    const/16 v1, 0x1a

    const/16 v2, 0x542

    aput v2, v0, v1

    const/16 v1, 0x1b

    const/16 v2, 0x5421

    aput v2, v0, v1

    const/16 v1, 0x1c

    const/16 v2, 0x543

    aput v2, v0, v1

    const/16 v1, 0x1d

    const/16 v2, 0x5431

    aput v2, v0, v1

    const/16 v1, 0x1e

    const/16 v2, 0x5432

    aput v2, v0, v1

    const/16 v1, 0x1f

    const v2, 0x54321

    aput v2, v0, v1

    const/16 v1, 0x20

    const/4 v2, 0x6

    aput v2, v0, v1

    const/16 v1, 0x21

    const/16 v2, 0x61

    aput v2, v0, v1

    const/16 v1, 0x22

    const/16 v2, 0x62

    aput v2, v0, v1

    const/16 v1, 0x23

    .line 37
    const/16 v2, 0x621

    aput v2, v0, v1

    const/16 v1, 0x24

    const/16 v2, 0x63

    aput v2, v0, v1

    const/16 v1, 0x25

    const/16 v2, 0x631

    aput v2, v0, v1

    const/16 v1, 0x26

    const/16 v2, 0x632

    aput v2, v0, v1

    const/16 v1, 0x27

    const/16 v2, 0x6321

    aput v2, v0, v1

    const/16 v1, 0x28

    const/16 v2, 0x64

    aput v2, v0, v1

    const/16 v1, 0x29

    const/16 v2, 0x641

    aput v2, v0, v1

    const/16 v1, 0x2a

    const/16 v2, 0x642

    aput v2, v0, v1

    const/16 v1, 0x2b

    const/16 v2, 0x6421

    aput v2, v0, v1

    const/16 v1, 0x2c

    const/16 v2, 0x643

    aput v2, v0, v1

    const/16 v1, 0x2d

    .line 38
    const/16 v2, 0x6431

    aput v2, v0, v1

    const/16 v1, 0x2e

    const/16 v2, 0x6432

    aput v2, v0, v1

    const/16 v1, 0x2f

    const v2, 0x64321

    aput v2, v0, v1

    const/16 v1, 0x30

    const/16 v2, 0x65

    aput v2, v0, v1

    const/16 v1, 0x31

    const/16 v2, 0x651

    aput v2, v0, v1

    const/16 v1, 0x32

    const/16 v2, 0x652

    aput v2, v0, v1

    const/16 v1, 0x33

    const/16 v2, 0x6521

    aput v2, v0, v1

    const/16 v1, 0x34

    const/16 v2, 0x653

    aput v2, v0, v1

    const/16 v1, 0x35

    const/16 v2, 0x6531

    aput v2, v0, v1

    const/16 v1, 0x36

    const/16 v2, 0x6532

    aput v2, v0, v1

    const/16 v1, 0x37

    .line 39
    const v2, 0x65321

    aput v2, v0, v1

    const/16 v1, 0x38

    const/16 v2, 0x654

    aput v2, v0, v1

    const/16 v1, 0x39

    const/16 v2, 0x6541

    aput v2, v0, v1

    const/16 v1, 0x3a

    const/16 v2, 0x6542

    aput v2, v0, v1

    const/16 v1, 0x3b

    const v2, 0x65421

    aput v2, v0, v1

    const/16 v1, 0x3c

    const/16 v2, 0x6543

    aput v2, v0, v1

    const/16 v1, 0x3d

    const v2, 0x65431

    aput v2, v0, v1

    const/16 v1, 0x3e

    const v2, 0x65432

    aput v2, v0, v1

    const/16 v1, 0x3f

    const v2, 0x654321

    aput v2, v0, v1

    const/16 v1, 0x40

    .line 40
    const/4 v2, 0x7

    aput v2, v0, v1

    const/16 v1, 0x41

    const/16 v2, 0x71

    aput v2, v0, v1

    const/16 v1, 0x42

    const/16 v2, 0x72

    aput v2, v0, v1

    const/16 v1, 0x43

    const/16 v2, 0x721

    aput v2, v0, v1

    const/16 v1, 0x44

    const/16 v2, 0x73

    aput v2, v0, v1

    const/16 v1, 0x45

    const/16 v2, 0x731

    aput v2, v0, v1

    const/16 v1, 0x46

    const/16 v2, 0x732

    aput v2, v0, v1

    const/16 v1, 0x47

    const/16 v2, 0x7321

    aput v2, v0, v1

    const/16 v1, 0x48

    const/16 v2, 0x74

    aput v2, v0, v1

    const/16 v1, 0x49

    const/16 v2, 0x741

    aput v2, v0, v1

    const/16 v1, 0x4a

    const/16 v2, 0x742

    aput v2, v0, v1

    const/16 v1, 0x4b

    .line 41
    const/16 v2, 0x7421

    aput v2, v0, v1

    const/16 v1, 0x4c

    const/16 v2, 0x743

    aput v2, v0, v1

    const/16 v1, 0x4d

    const/16 v2, 0x7431

    aput v2, v0, v1

    const/16 v1, 0x4e

    const/16 v2, 0x7432

    aput v2, v0, v1

    const/16 v1, 0x4f

    const v2, 0x74321

    aput v2, v0, v1

    const/16 v1, 0x50

    const/16 v2, 0x75

    aput v2, v0, v1

    const/16 v1, 0x51

    const/16 v2, 0x751

    aput v2, v0, v1

    const/16 v1, 0x52

    const/16 v2, 0x752

    aput v2, v0, v1

    const/16 v1, 0x53

    const/16 v2, 0x7521

    aput v2, v0, v1

    const/16 v1, 0x54

    const/16 v2, 0x753

    aput v2, v0, v1

    const/16 v1, 0x55

    .line 42
    const/16 v2, 0x7531

    aput v2, v0, v1

    const/16 v1, 0x56

    const/16 v2, 0x7532

    aput v2, v0, v1

    const/16 v1, 0x57

    const v2, 0x75321

    aput v2, v0, v1

    const/16 v1, 0x58

    const/16 v2, 0x754

    aput v2, v0, v1

    const/16 v1, 0x59

    const/16 v2, 0x7541

    aput v2, v0, v1

    const/16 v1, 0x5a

    const/16 v2, 0x7542

    aput v2, v0, v1

    const/16 v1, 0x5b

    const v2, 0x75421

    aput v2, v0, v1

    const/16 v1, 0x5c

    const/16 v2, 0x7543

    aput v2, v0, v1

    const/16 v1, 0x5d

    const v2, 0x75431

    aput v2, v0, v1

    const/16 v1, 0x5e

    .line 43
    const v2, 0x75432

    aput v2, v0, v1

    const/16 v1, 0x5f

    const v2, 0x754321

    aput v2, v0, v1

    const/16 v1, 0x60

    const/16 v2, 0x76

    aput v2, v0, v1

    const/16 v1, 0x61

    const/16 v2, 0x761

    aput v2, v0, v1

    const/16 v1, 0x62

    const/16 v2, 0x762

    aput v2, v0, v1

    const/16 v1, 0x63

    const/16 v2, 0x7621

    aput v2, v0, v1

    const/16 v1, 0x64

    const/16 v2, 0x763

    aput v2, v0, v1

    const/16 v1, 0x65

    const/16 v2, 0x7631

    aput v2, v0, v1

    const/16 v1, 0x66

    const/16 v2, 0x7632

    aput v2, v0, v1

    const/16 v1, 0x67

    .line 44
    const v2, 0x76321

    aput v2, v0, v1

    const/16 v1, 0x68

    const/16 v2, 0x764

    aput v2, v0, v1

    const/16 v1, 0x69

    const/16 v2, 0x7641

    aput v2, v0, v1

    const/16 v1, 0x6a

    const/16 v2, 0x7642

    aput v2, v0, v1

    const/16 v1, 0x6b

    const v2, 0x76421

    aput v2, v0, v1

    const/16 v1, 0x6c

    const/16 v2, 0x7643

    aput v2, v0, v1

    const/16 v1, 0x6d

    const v2, 0x76431

    aput v2, v0, v1

    const/16 v1, 0x6e

    const v2, 0x76432

    aput v2, v0, v1

    const/16 v1, 0x6f

    const v2, 0x764321

    aput v2, v0, v1

    const/16 v1, 0x70

    .line 45
    const/16 v2, 0x765

    aput v2, v0, v1

    const/16 v1, 0x71

    const/16 v2, 0x7651

    aput v2, v0, v1

    const/16 v1, 0x72

    const/16 v2, 0x7652

    aput v2, v0, v1

    const/16 v1, 0x73

    const v2, 0x76521

    aput v2, v0, v1

    const/16 v1, 0x74

    const/16 v2, 0x7653

    aput v2, v0, v1

    const/16 v1, 0x75

    const v2, 0x76531

    aput v2, v0, v1

    const/16 v1, 0x76

    const v2, 0x76532

    aput v2, v0, v1

    const/16 v1, 0x77

    const v2, 0x765321

    aput v2, v0, v1

    const/16 v1, 0x78

    const/16 v2, 0x7654

    aput v2, v0, v1

    const/16 v1, 0x79

    .line 46
    const v2, 0x76541

    aput v2, v0, v1

    const/16 v1, 0x7a

    const v2, 0x76542

    aput v2, v0, v1

    const/16 v1, 0x7b

    const v2, 0x765421

    aput v2, v0, v1

    const/16 v1, 0x7c

    const v2, 0x76543

    aput v2, v0, v1

    const/16 v1, 0x7d

    const v2, 0x765431

    aput v2, v0, v1

    const/16 v1, 0x7e

    const v2, 0x765432

    aput v2, v0, v1

    const/16 v1, 0x7f

    const v2, 0x7654321

    aput v2, v0, v1

    const/16 v1, 0x80

    const/16 v2, 0x8

    aput v2, v0, v1

    const/16 v1, 0x81

    .line 47
    const/16 v2, 0x81

    aput v2, v0, v1

    const/16 v1, 0x82

    const/16 v2, 0x82

    aput v2, v0, v1

    const/16 v1, 0x83

    const/16 v2, 0x821

    aput v2, v0, v1

    const/16 v1, 0x84

    const/16 v2, 0x83

    aput v2, v0, v1

    const/16 v1, 0x85

    const/16 v2, 0x831

    aput v2, v0, v1

    const/16 v1, 0x86

    const/16 v2, 0x832

    aput v2, v0, v1

    const/16 v1, 0x87

    const v2, 0x8321

    aput v2, v0, v1

    const/16 v1, 0x88

    const/16 v2, 0x84

    aput v2, v0, v1

    const/16 v1, 0x89

    const/16 v2, 0x841

    aput v2, v0, v1

    const/16 v1, 0x8a

    const/16 v2, 0x842

    aput v2, v0, v1

    const/16 v1, 0x8b

    const v2, 0x8421

    aput v2, v0, v1

    const/16 v1, 0x8c

    .line 48
    const/16 v2, 0x843

    aput v2, v0, v1

    const/16 v1, 0x8d

    const v2, 0x8431

    aput v2, v0, v1

    const/16 v1, 0x8e

    const v2, 0x8432

    aput v2, v0, v1

    const/16 v1, 0x8f

    const v2, 0x84321

    aput v2, v0, v1

    const/16 v1, 0x90

    const/16 v2, 0x85

    aput v2, v0, v1

    const/16 v1, 0x91

    const/16 v2, 0x851

    aput v2, v0, v1

    const/16 v1, 0x92

    const/16 v2, 0x852

    aput v2, v0, v1

    const/16 v1, 0x93

    const v2, 0x8521

    aput v2, v0, v1

    const/16 v1, 0x94

    const/16 v2, 0x853

    aput v2, v0, v1

    const/16 v1, 0x95

    const v2, 0x8531

    aput v2, v0, v1

    const/16 v1, 0x96

    .line 49
    const v2, 0x8532

    aput v2, v0, v1

    const/16 v1, 0x97

    const v2, 0x85321

    aput v2, v0, v1

    const/16 v1, 0x98

    const/16 v2, 0x854

    aput v2, v0, v1

    const/16 v1, 0x99

    const v2, 0x8541

    aput v2, v0, v1

    const/16 v1, 0x9a

    const v2, 0x8542

    aput v2, v0, v1

    const/16 v1, 0x9b

    const v2, 0x85421

    aput v2, v0, v1

    const/16 v1, 0x9c

    const v2, 0x8543

    aput v2, v0, v1

    const/16 v1, 0x9d

    const v2, 0x85431

    aput v2, v0, v1

    const/16 v1, 0x9e

    const v2, 0x85432

    aput v2, v0, v1

    const/16 v1, 0x9f

    .line 50
    const v2, 0x854321

    aput v2, v0, v1

    const/16 v1, 0xa0

    const/16 v2, 0x86

    aput v2, v0, v1

    const/16 v1, 0xa1

    const/16 v2, 0x861

    aput v2, v0, v1

    const/16 v1, 0xa2

    const/16 v2, 0x862

    aput v2, v0, v1

    const/16 v1, 0xa3

    const v2, 0x8621

    aput v2, v0, v1

    const/16 v1, 0xa4

    const/16 v2, 0x863

    aput v2, v0, v1

    const/16 v1, 0xa5

    const v2, 0x8631

    aput v2, v0, v1

    const/16 v1, 0xa6

    const v2, 0x8632

    aput v2, v0, v1

    const/16 v1, 0xa7

    const v2, 0x86321

    aput v2, v0, v1

    const/16 v1, 0xa8

    const/16 v2, 0x864

    aput v2, v0, v1

    const/16 v1, 0xa9

    .line 51
    const v2, 0x8641

    aput v2, v0, v1

    const/16 v1, 0xaa

    const v2, 0x8642

    aput v2, v0, v1

    const/16 v1, 0xab

    const v2, 0x86421

    aput v2, v0, v1

    const/16 v1, 0xac

    const v2, 0x8643

    aput v2, v0, v1

    const/16 v1, 0xad

    const v2, 0x86431

    aput v2, v0, v1

    const/16 v1, 0xae

    const v2, 0x86432

    aput v2, v0, v1

    const/16 v1, 0xaf

    const v2, 0x864321

    aput v2, v0, v1

    const/16 v1, 0xb0

    const/16 v2, 0x865

    aput v2, v0, v1

    const/16 v1, 0xb1

    const v2, 0x8651

    aput v2, v0, v1

    const/16 v1, 0xb2

    .line 52
    const v2, 0x8652

    aput v2, v0, v1

    const/16 v1, 0xb3

    const v2, 0x86521

    aput v2, v0, v1

    const/16 v1, 0xb4

    const v2, 0x8653

    aput v2, v0, v1

    const/16 v1, 0xb5

    const v2, 0x86531

    aput v2, v0, v1

    const/16 v1, 0xb6

    const v2, 0x86532

    aput v2, v0, v1

    const/16 v1, 0xb7

    const v2, 0x865321

    aput v2, v0, v1

    const/16 v1, 0xb8

    const v2, 0x8654

    aput v2, v0, v1

    const/16 v1, 0xb9

    const v2, 0x86541

    aput v2, v0, v1

    const/16 v1, 0xba

    .line 53
    const v2, 0x86542

    aput v2, v0, v1

    const/16 v1, 0xbb

    const v2, 0x865421

    aput v2, v0, v1

    const/16 v1, 0xbc

    const v2, 0x86543

    aput v2, v0, v1

    const/16 v1, 0xbd

    const v2, 0x865431

    aput v2, v0, v1

    const/16 v1, 0xbe

    const v2, 0x865432

    aput v2, v0, v1

    const/16 v1, 0xbf

    const v2, 0x8654321

    aput v2, v0, v1

    const/16 v1, 0xc0

    const/16 v2, 0x87

    aput v2, v0, v1

    const/16 v1, 0xc1

    const/16 v2, 0x871

    aput v2, v0, v1

    const/16 v1, 0xc2

    .line 54
    const/16 v2, 0x872

    aput v2, v0, v1

    const/16 v1, 0xc3

    const v2, 0x8721

    aput v2, v0, v1

    const/16 v1, 0xc4

    const/16 v2, 0x873

    aput v2, v0, v1

    const/16 v1, 0xc5

    const v2, 0x8731

    aput v2, v0, v1

    const/16 v1, 0xc6

    const v2, 0x8732

    aput v2, v0, v1

    const/16 v1, 0xc7

    const v2, 0x87321

    aput v2, v0, v1

    const/16 v1, 0xc8

    const/16 v2, 0x874

    aput v2, v0, v1

    const/16 v1, 0xc9

    const v2, 0x8741

    aput v2, v0, v1

    const/16 v1, 0xca

    const v2, 0x8742

    aput v2, v0, v1

    const/16 v1, 0xcb

    .line 55
    const v2, 0x87421

    aput v2, v0, v1

    const/16 v1, 0xcc

    const v2, 0x8743

    aput v2, v0, v1

    const/16 v1, 0xcd

    const v2, 0x87431

    aput v2, v0, v1

    const/16 v1, 0xce

    const v2, 0x87432

    aput v2, v0, v1

    const/16 v1, 0xcf

    const v2, 0x874321

    aput v2, v0, v1

    const/16 v1, 0xd0

    const/16 v2, 0x875

    aput v2, v0, v1

    const/16 v1, 0xd1

    const v2, 0x8751

    aput v2, v0, v1

    const/16 v1, 0xd2

    const v2, 0x8752

    aput v2, v0, v1

    const/16 v1, 0xd3

    const v2, 0x87521

    aput v2, v0, v1

    const/16 v1, 0xd4

    .line 56
    const v2, 0x8753

    aput v2, v0, v1

    const/16 v1, 0xd5

    const v2, 0x87531

    aput v2, v0, v1

    const/16 v1, 0xd6

    const v2, 0x87532

    aput v2, v0, v1

    const/16 v1, 0xd7

    const v2, 0x875321

    aput v2, v0, v1

    const/16 v1, 0xd8

    const v2, 0x8754

    aput v2, v0, v1

    const/16 v1, 0xd9

    const v2, 0x87541

    aput v2, v0, v1

    const/16 v1, 0xda

    const v2, 0x87542

    aput v2, v0, v1

    const/16 v1, 0xdb

    const v2, 0x875421

    aput v2, v0, v1

    const/16 v1, 0xdc

    .line 57
    const v2, 0x87543

    aput v2, v0, v1

    const/16 v1, 0xdd

    const v2, 0x875431

    aput v2, v0, v1

    const/16 v1, 0xde

    const v2, 0x875432

    aput v2, v0, v1

    const/16 v1, 0xdf

    const v2, 0x8754321

    aput v2, v0, v1

    const/16 v1, 0xe0

    const/16 v2, 0x876

    aput v2, v0, v1

    const/16 v1, 0xe1

    const v2, 0x8761

    aput v2, v0, v1

    const/16 v1, 0xe2

    const v2, 0x8762

    aput v2, v0, v1

    const/16 v1, 0xe3

    const v2, 0x87621

    aput v2, v0, v1

    const/16 v1, 0xe4

    .line 58
    const v2, 0x8763

    aput v2, v0, v1

    const/16 v1, 0xe5

    const v2, 0x87631

    aput v2, v0, v1

    const/16 v1, 0xe6

    const v2, 0x87632

    aput v2, v0, v1

    const/16 v1, 0xe7

    const v2, 0x876321

    aput v2, v0, v1

    const/16 v1, 0xe8

    const v2, 0x8764

    aput v2, v0, v1

    const/16 v1, 0xe9

    const v2, 0x87641

    aput v2, v0, v1

    const/16 v1, 0xea

    const v2, 0x87642

    aput v2, v0, v1

    const/16 v1, 0xeb

    const v2, 0x876421

    aput v2, v0, v1

    const/16 v1, 0xec

    .line 59
    const v2, 0x87643

    aput v2, v0, v1

    const/16 v1, 0xed

    const v2, 0x876431

    aput v2, v0, v1

    const/16 v1, 0xee

    const v2, 0x876432

    aput v2, v0, v1

    const/16 v1, 0xef

    const v2, 0x8764321

    aput v2, v0, v1

    const/16 v1, 0xf0

    const v2, 0x8765

    aput v2, v0, v1

    const/16 v1, 0xf1

    const v2, 0x87651

    aput v2, v0, v1

    const/16 v1, 0xf2

    const v2, 0x87652

    aput v2, v0, v1

    const/16 v1, 0xf3

    const v2, 0x876521

    aput v2, v0, v1

    const/16 v1, 0xf4

    .line 60
    const v2, 0x87653

    aput v2, v0, v1

    const/16 v1, 0xf5

    const v2, 0x876531

    aput v2, v0, v1

    const/16 v1, 0xf6

    const v2, 0x876532

    aput v2, v0, v1

    const/16 v1, 0xf7

    const v2, 0x8765321

    aput v2, v0, v1

    const/16 v1, 0xf8

    const v2, 0x87654

    aput v2, v0, v1

    const/16 v1, 0xf9

    const v2, 0x876541

    aput v2, v0, v1

    const/16 v1, 0xfa

    const v2, 0x876542

    aput v2, v0, v1

    const/16 v1, 0xfb

    .line 61
    const v2, 0x8765421

    aput v2, v0, v1

    const/16 v1, 0xfc

    const v2, 0x876543

    aput v2, v0, v1

    const/16 v1, 0xfd

    const v2, 0x8765431

    aput v2, v0, v1

    const/16 v1, 0xfe

    const v2, 0x8765432

    aput v2, v0, v1

    const/16 v1, 0xff

    const v2, -0x789abcdf

    aput v2, v0, v1

    .line 33
    sput-object v0, Lorg/apache/lucene/util/OpenBitSetIterator;->bitlist:[I

    .line 62
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/OpenBitSet;)V
    .locals 2
    .param p1, "obs"    # Lorg/apache/lucene/util/OpenBitSet;

    .prologue
    .line 91
    invoke-virtual {p1}, Lorg/apache/lucene/util/OpenBitSet;->getBits()[J

    move-result-object v0

    invoke-virtual {p1}, Lorg/apache/lucene/util/OpenBitSet;->getNumWords()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/util/OpenBitSetIterator;-><init>([JI)V

    .line 92
    return-void
.end method

.method public constructor <init>([JI)V
    .locals 1
    .param p1, "bits"    # [J
    .param p2, "numWords"    # I

    .prologue
    const/4 v0, -0x1

    .line 94
    invoke-direct {p0}, Lorg/apache/lucene/search/DocIdSetIterator;-><init>()V

    .line 84
    iput v0, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->i:I

    .line 88
    iput v0, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->curDocId:I

    .line 95
    iput-object p1, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->arr:[J

    .line 96
    iput p2, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->words:I

    .line 97
    return-void
.end method

.method private shift()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 101
    iget-wide v0, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->word:J

    long-to-int v0, v0

    if-nez v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->wordShift:I

    add-int/lit8 v0, v0, 0x20

    iput v0, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->wordShift:I

    iget-wide v0, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->word:J

    const/16 v2, 0x20

    ushr-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->word:J

    .line 102
    :cond_0
    iget-wide v0, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->word:J

    const-wide/32 v2, 0xffff

    and-long/2addr v0, v2

    cmp-long v0, v0, v4

    if-nez v0, :cond_1

    iget v0, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->wordShift:I

    add-int/lit8 v0, v0, 0x10

    iput v0, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->wordShift:I

    iget-wide v0, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->word:J

    const/16 v2, 0x10

    ushr-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->word:J

    .line 103
    :cond_1
    iget-wide v0, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->word:J

    const-wide/16 v2, 0xff

    and-long/2addr v0, v2

    cmp-long v0, v0, v4

    if-nez v0, :cond_2

    iget v0, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->wordShift:I

    add-int/lit8 v0, v0, 0x8

    iput v0, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->wordShift:I

    iget-wide v0, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->word:J

    const/16 v2, 0x8

    ushr-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->word:J

    .line 104
    :cond_2
    sget-object v0, Lorg/apache/lucene/util/OpenBitSetIterator;->bitlist:[I

    iget-wide v2, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->word:J

    long-to-int v1, v2

    and-int/lit16 v1, v1, 0xff

    aget v0, v0, v1

    iput v0, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->indexArray:I

    .line 105
    return-void
.end method


# virtual methods
.method public advance(I)I
    .locals 8
    .param p1, "target"    # I

    .prologue
    const v1, 0x7fffffff

    const-wide/16 v6, 0x0

    .line 159
    const/4 v2, 0x0

    iput v2, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->indexArray:I

    .line 160
    shr-int/lit8 v2, p1, 0x6

    iput v2, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->i:I

    .line 161
    iget v2, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->i:I

    iget v3, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->words:I

    if-lt v2, v3, :cond_0

    .line 162
    iput-wide v6, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->word:J

    .line 163
    iput v1, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->curDocId:I

    .line 185
    :goto_0
    return v1

    .line 165
    :cond_0
    and-int/lit8 v2, p1, 0x3f

    iput v2, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->wordShift:I

    .line 166
    iget-object v2, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->arr:[J

    iget v3, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->i:I

    aget-wide v2, v2, v3

    iget v4, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->wordShift:I

    ushr-long/2addr v2, v4

    iput-wide v2, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->word:J

    .line 167
    iget-wide v2, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->word:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_3

    .line 168
    iget v1, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->wordShift:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->wordShift:I

    .line 179
    :goto_1
    invoke-direct {p0}, Lorg/apache/lucene/util/OpenBitSetIterator;->shift()V

    .line 181
    iget v1, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->indexArray:I

    and-int/lit8 v1, v1, 0xf

    iget v2, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->wordShift:I

    add-int v0, v1, v2

    .line 182
    .local v0, "bitIndex":I
    iget v1, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->indexArray:I

    ushr-int/lit8 v1, v1, 0x4

    iput v1, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->indexArray:I

    .line 185
    iget v1, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->i:I

    shl-int/lit8 v1, v1, 0x6

    add-int/2addr v1, v0

    iput v1, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->curDocId:I

    goto :goto_0

    .line 171
    .end local v0    # "bitIndex":I
    :cond_1
    iget v2, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->i:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->i:I

    iget v3, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->words:I

    if-lt v2, v3, :cond_2

    .line 172
    iput v1, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->curDocId:I

    goto :goto_0

    .line 174
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->arr:[J

    iget v3, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->i:I

    aget-wide v2, v2, v3

    iput-wide v2, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->word:J

    .line 170
    :cond_3
    iget-wide v2, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->word:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_1

    .line 176
    const/4 v1, -0x1

    iput v1, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->wordShift:I

    goto :goto_1
.end method

.method public cost()J
    .locals 2

    .prologue
    .line 195
    iget v0, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->words:I

    div-int/lit8 v0, v0, 0x40

    int-to-long v0, v0

    return-wide v0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 190
    iget v0, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->curDocId:I

    return v0
.end method

.method public nextDoc()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 131
    iget v1, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->indexArray:I

    if-nez v1, :cond_1

    .line 132
    iget-wide v2, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->word:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 133
    iget-wide v2, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->word:J

    const/16 v1, 0x8

    ushr-long/2addr v2, v1

    iput-wide v2, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->word:J

    .line 134
    iget v1, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->wordShift:I

    add-int/lit8 v1, v1, 0x8

    iput v1, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->wordShift:I

    .line 137
    :cond_0
    :goto_0
    iget-wide v2, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->word:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 147
    invoke-direct {p0}, Lorg/apache/lucene/util/OpenBitSetIterator;->shift()V

    .line 150
    :cond_1
    iget v1, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->indexArray:I

    and-int/lit8 v1, v1, 0xf

    iget v2, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->wordShift:I

    add-int v0, v1, v2

    .line 151
    .local v0, "bitIndex":I
    iget v1, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->indexArray:I

    ushr-int/lit8 v1, v1, 0x4

    iput v1, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->indexArray:I

    .line 154
    iget v1, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->i:I

    shl-int/lit8 v1, v1, 0x6

    add-int/2addr v1, v0

    iput v1, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->curDocId:I

    .end local v0    # "bitIndex":I
    :goto_1
    return v1

    .line 138
    :cond_2
    iget v1, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->i:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->i:I

    iget v2, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->words:I

    if-lt v1, v2, :cond_3

    .line 139
    const v1, 0x7fffffff

    iput v1, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->curDocId:I

    goto :goto_1

    .line 141
    :cond_3
    iget-object v1, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->arr:[J

    iget v2, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->i:I

    aget-wide v2, v1, v2

    iput-wide v2, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->word:J

    .line 142
    const/4 v1, -0x1

    iput v1, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->wordShift:I

    goto :goto_0
.end method
