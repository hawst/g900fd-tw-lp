.class final Lorg/apache/lucene/util/packed/PackedWriter;
.super Lorg/apache/lucene/util/packed/PackedInts$Writer;
.source "PackedWriter.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final encoder:Lorg/apache/lucene/util/packed/BulkOperation;

.field finished:Z

.field final format:Lorg/apache/lucene/util/packed/PackedInts$Format;

.field final iterations:I

.field final nextBlocks:[B

.field final nextValues:[J

.field off:I

.field written:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lorg/apache/lucene/util/packed/PackedWriter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/packed/PackedWriter;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/util/packed/PackedInts$Format;Lorg/apache/lucene/store/DataOutput;III)V
    .locals 3
    .param p1, "format"    # Lorg/apache/lucene/util/packed/PackedInts$Format;
    .param p2, "out"    # Lorg/apache/lucene/store/DataOutput;
    .param p3, "valueCount"    # I
    .param p4, "bitsPerValue"    # I
    .param p5, "mem"    # I

    .prologue
    const/4 v2, 0x0

    .line 41
    invoke-direct {p0, p2, p3, p4}, Lorg/apache/lucene/util/packed/PackedInts$Writer;-><init>(Lorg/apache/lucene/store/DataOutput;II)V

    .line 42
    iput-object p1, p0, Lorg/apache/lucene/util/packed/PackedWriter;->format:Lorg/apache/lucene/util/packed/PackedInts$Format;

    .line 43
    invoke-static {p1, p4}, Lorg/apache/lucene/util/packed/BulkOperation;->of(Lorg/apache/lucene/util/packed/PackedInts$Format;I)Lorg/apache/lucene/util/packed/BulkOperation;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/util/packed/PackedWriter;->encoder:Lorg/apache/lucene/util/packed/BulkOperation;

    .line 44
    iget-object v0, p0, Lorg/apache/lucene/util/packed/PackedWriter;->encoder:Lorg/apache/lucene/util/packed/BulkOperation;

    invoke-virtual {v0, p3, p5}, Lorg/apache/lucene/util/packed/BulkOperation;->computeIterations(II)I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/util/packed/PackedWriter;->iterations:I

    .line 45
    iget v0, p0, Lorg/apache/lucene/util/packed/PackedWriter;->iterations:I

    iget-object v1, p0, Lorg/apache/lucene/util/packed/PackedWriter;->encoder:Lorg/apache/lucene/util/packed/BulkOperation;

    invoke-virtual {v1}, Lorg/apache/lucene/util/packed/BulkOperation;->byteBlockCount()I

    move-result v1

    mul-int/2addr v0, v1

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/lucene/util/packed/PackedWriter;->nextBlocks:[B

    .line 46
    iget v0, p0, Lorg/apache/lucene/util/packed/PackedWriter;->iterations:I

    iget-object v1, p0, Lorg/apache/lucene/util/packed/PackedWriter;->encoder:Lorg/apache/lucene/util/packed/BulkOperation;

    invoke-virtual {v1}, Lorg/apache/lucene/util/packed/BulkOperation;->byteValueCount()I

    move-result v1

    mul-int/2addr v0, v1

    new-array v0, v0, [J

    iput-object v0, p0, Lorg/apache/lucene/util/packed/PackedWriter;->nextValues:[J

    .line 47
    iput v2, p0, Lorg/apache/lucene/util/packed/PackedWriter;->off:I

    .line 48
    iput v2, p0, Lorg/apache/lucene/util/packed/PackedWriter;->written:I

    .line 49
    iput-boolean v2, p0, Lorg/apache/lucene/util/packed/PackedWriter;->finished:Z

    .line 50
    return-void
.end method

.method private flush()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 84
    iget-object v0, p0, Lorg/apache/lucene/util/packed/PackedWriter;->encoder:Lorg/apache/lucene/util/packed/BulkOperation;

    iget-object v1, p0, Lorg/apache/lucene/util/packed/PackedWriter;->nextValues:[J

    iget-object v3, p0, Lorg/apache/lucene/util/packed/PackedWriter;->nextBlocks:[B

    iget v5, p0, Lorg/apache/lucene/util/packed/PackedWriter;->iterations:I

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Lorg/apache/lucene/util/packed/BulkOperation;->encode([JI[BII)V

    .line 85
    iget-object v0, p0, Lorg/apache/lucene/util/packed/PackedWriter;->format:Lorg/apache/lucene/util/packed/PackedInts$Format;

    const/4 v1, 0x1

    iget v3, p0, Lorg/apache/lucene/util/packed/PackedWriter;->off:I

    iget v4, p0, Lorg/apache/lucene/util/packed/PackedWriter;->bitsPerValue:I

    invoke-virtual {v0, v1, v3, v4}, Lorg/apache/lucene/util/packed/PackedInts$Format;->byteCount(III)J

    move-result-wide v0

    long-to-int v6, v0

    .line 86
    .local v6, "blockCount":I
    iget-object v0, p0, Lorg/apache/lucene/util/packed/PackedWriter;->out:Lorg/apache/lucene/store/DataOutput;

    iget-object v1, p0, Lorg/apache/lucene/util/packed/PackedWriter;->nextBlocks:[B

    invoke-virtual {v0, v1, v6}, Lorg/apache/lucene/store/DataOutput;->writeBytes([BI)V

    .line 87
    iget-object v0, p0, Lorg/apache/lucene/util/packed/PackedWriter;->nextValues:[J

    const-wide/16 v4, 0x0

    invoke-static {v0, v4, v5}, Ljava/util/Arrays;->fill([JJ)V

    .line 88
    iput v2, p0, Lorg/apache/lucene/util/packed/PackedWriter;->off:I

    .line 89
    return-void
.end method


# virtual methods
.method public add(J)V
    .locals 3
    .param p1, "v"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 59
    sget-boolean v0, Lorg/apache/lucene/util/packed/PackedWriter;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget v0, p0, Lorg/apache/lucene/util/packed/PackedWriter;->bitsPerValue:I

    const/16 v1, 0x40

    if-eq v0, v1, :cond_1

    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/util/packed/PackedWriter;->bitsPerValue:I

    invoke-static {v0}, Lorg/apache/lucene/util/packed/PackedInts;->maxValue(I)J

    move-result-wide v0

    cmp-long v0, p1, v0

    if-lez v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    iget v1, p0, Lorg/apache/lucene/util/packed/PackedWriter;->bitsPerValue:I

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(I)V

    throw v0

    .line 60
    :cond_1
    sget-boolean v0, Lorg/apache/lucene/util/packed/PackedWriter;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lorg/apache/lucene/util/packed/PackedWriter;->finished:Z

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 61
    :cond_2
    iget v0, p0, Lorg/apache/lucene/util/packed/PackedWriter;->valueCount:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_3

    iget v0, p0, Lorg/apache/lucene/util/packed/PackedWriter;->written:I

    iget v1, p0, Lorg/apache/lucene/util/packed/PackedWriter;->valueCount:I

    if-lt v0, v1, :cond_3

    .line 62
    new-instance v0, Ljava/io/EOFException;

    const-string v1, "Writing past end of stream"

    invoke-direct {v0, v1}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 64
    :cond_3
    iget-object v0, p0, Lorg/apache/lucene/util/packed/PackedWriter;->nextValues:[J

    iget v1, p0, Lorg/apache/lucene/util/packed/PackedWriter;->off:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/util/packed/PackedWriter;->off:I

    aput-wide p1, v0, v1

    .line 65
    iget v0, p0, Lorg/apache/lucene/util/packed/PackedWriter;->off:I

    iget-object v1, p0, Lorg/apache/lucene/util/packed/PackedWriter;->nextValues:[J

    array-length v1, v1

    if-ne v0, v1, :cond_4

    .line 66
    invoke-direct {p0}, Lorg/apache/lucene/util/packed/PackedWriter;->flush()V

    .line 68
    :cond_4
    iget v0, p0, Lorg/apache/lucene/util/packed/PackedWriter;->written:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/util/packed/PackedWriter;->written:I

    .line 69
    return-void
.end method

.method public finish()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 73
    sget-boolean v0, Lorg/apache/lucene/util/packed/PackedWriter;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/apache/lucene/util/packed/PackedWriter;->finished:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 74
    :cond_0
    iget v0, p0, Lorg/apache/lucene/util/packed/PackedWriter;->valueCount:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 75
    :goto_0
    iget v0, p0, Lorg/apache/lucene/util/packed/PackedWriter;->written:I

    iget v1, p0, Lorg/apache/lucene/util/packed/PackedWriter;->valueCount:I

    if-lt v0, v1, :cond_2

    .line 79
    :cond_1
    invoke-direct {p0}, Lorg/apache/lucene/util/packed/PackedWriter;->flush()V

    .line 80
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/util/packed/PackedWriter;->finished:Z

    .line 81
    return-void

    .line 76
    :cond_2
    const-wide/16 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/util/packed/PackedWriter;->add(J)V

    goto :goto_0
.end method

.method protected getFormat()Lorg/apache/lucene/util/packed/PackedInts$Format;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lorg/apache/lucene/util/packed/PackedWriter;->format:Lorg/apache/lucene/util/packed/PackedInts$Format;

    return-object v0
.end method

.method public ord()I
    .locals 1

    .prologue
    .line 93
    iget v0, p0, Lorg/apache/lucene/util/packed/PackedWriter;->written:I

    add-int/lit8 v0, v0, -0x1

    return v0
.end method
