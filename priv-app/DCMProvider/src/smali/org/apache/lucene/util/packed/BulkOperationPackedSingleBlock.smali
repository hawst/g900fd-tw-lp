.class final Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;
.super Lorg/apache/lucene/util/packed/BulkOperation;
.source "BulkOperationPackedSingleBlock.java"


# static fields
.field private static final BLOCK_COUNT:I = 0x1


# instance fields
.field private final bitsPerValue:I

.field private final mask:J

.field private final valueCount:I


# direct methods
.method public constructor <init>(I)V
    .locals 4
    .param p1, "bitsPerValue"    # I

    .prologue
    const-wide/16 v2, 0x1

    .line 31
    invoke-direct {p0}, Lorg/apache/lucene/util/packed/BulkOperation;-><init>()V

    .line 32
    iput p1, p0, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;->bitsPerValue:I

    .line 33
    const/16 v0, 0x40

    div-int/2addr v0, p1

    iput v0, p0, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;->valueCount:I

    .line 34
    shl-long v0, v2, p1

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;->mask:J

    .line 35
    return-void
.end method

.method private decode(J[II)I
    .locals 5
    .param p1, "block"    # J
    .param p3, "values"    # [I
    .param p4, "valuesOffset"    # I

    .prologue
    .line 78
    add-int/lit8 v1, p4, 0x1

    .end local p4    # "valuesOffset":I
    .local v1, "valuesOffset":I
    iget-wide v2, p0, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;->mask:J

    and-long/2addr v2, p1

    long-to-int v2, v2

    aput v2, p3, p4

    .line 79
    const/4 v0, 0x1

    .local v0, "j":I
    move p4, v1

    .end local v1    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    :goto_0
    iget v2, p0, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;->valueCount:I

    if-lt v0, v2, :cond_0

    .line 83
    return p4

    .line 80
    :cond_0
    iget v2, p0, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;->bitsPerValue:I

    ushr-long/2addr p1, v2

    .line 81
    add-int/lit8 v1, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v1    # "valuesOffset":I
    iget-wide v2, p0, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;->mask:J

    and-long/2addr v2, p1

    long-to-int v2, v2

    aput v2, p3, p4

    .line 79
    add-int/lit8 v0, v0, 0x1

    move p4, v1

    .end local v1    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    goto :goto_0
.end method

.method private decode(J[JI)I
    .locals 5
    .param p1, "block"    # J
    .param p3, "values"    # [J
    .param p4, "valuesOffset"    # I

    .prologue
    .line 69
    add-int/lit8 v1, p4, 0x1

    .end local p4    # "valuesOffset":I
    .local v1, "valuesOffset":I
    iget-wide v2, p0, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;->mask:J

    and-long/2addr v2, p1

    aput-wide v2, p3, p4

    .line 70
    const/4 v0, 0x1

    .local v0, "j":I
    move p4, v1

    .end local v1    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    :goto_0
    iget v2, p0, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;->valueCount:I

    if-lt v0, v2, :cond_0

    .line 74
    return p4

    .line 71
    :cond_0
    iget v2, p0, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;->bitsPerValue:I

    ushr-long/2addr p1, v2

    .line 72
    add-int/lit8 v1, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v1    # "valuesOffset":I
    iget-wide v2, p0, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;->mask:J

    and-long/2addr v2, p1

    aput-wide v2, p3, p4

    .line 70
    add-int/lit8 v0, v0, 0x1

    move p4, v1

    .end local v1    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    goto :goto_0
.end method

.method private encode([II)J
    .locals 10
    .param p1, "values"    # [I
    .param p2, "valuesOffset"    # I

    .prologue
    const-wide v8, 0xffffffffL

    .line 95
    add-int/lit8 v3, p2, 0x1

    .end local p2    # "valuesOffset":I
    .local v3, "valuesOffset":I
    aget v4, p1, p2

    int-to-long v4, v4

    and-long v0, v4, v8

    .line 96
    .local v0, "block":J
    const/4 v2, 0x1

    .local v2, "j":I
    move p2, v3

    .end local v3    # "valuesOffset":I
    .restart local p2    # "valuesOffset":I
    :goto_0
    iget v4, p0, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;->valueCount:I

    if-lt v2, v4, :cond_0

    .line 99
    return-wide v0

    .line 97
    :cond_0
    add-int/lit8 v3, p2, 0x1

    .end local p2    # "valuesOffset":I
    .restart local v3    # "valuesOffset":I
    aget v4, p1, p2

    int-to-long v4, v4

    and-long/2addr v4, v8

    iget v6, p0, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;->bitsPerValue:I

    mul-int/2addr v6, v2

    shl-long/2addr v4, v6

    or-long/2addr v0, v4

    .line 96
    add-int/lit8 v2, v2, 0x1

    move p2, v3

    .end local v3    # "valuesOffset":I
    .restart local p2    # "valuesOffset":I
    goto :goto_0
.end method

.method private encode([JI)J
    .locals 7
    .param p1, "values"    # [J
    .param p2, "valuesOffset"    # I

    .prologue
    .line 87
    add-int/lit8 v3, p2, 0x1

    .end local p2    # "valuesOffset":I
    .local v3, "valuesOffset":I
    aget-wide v0, p1, p2

    .line 88
    .local v0, "block":J
    const/4 v2, 0x1

    .local v2, "j":I
    move p2, v3

    .end local v3    # "valuesOffset":I
    .restart local p2    # "valuesOffset":I
    :goto_0
    iget v4, p0, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;->valueCount:I

    if-lt v2, v4, :cond_0

    .line 91
    return-wide v0

    .line 89
    :cond_0
    add-int/lit8 v3, p2, 0x1

    .end local p2    # "valuesOffset":I
    .restart local v3    # "valuesOffset":I
    aget-wide v4, p1, p2

    iget v6, p0, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;->bitsPerValue:I

    mul-int/2addr v6, v2

    shl-long/2addr v4, v6

    or-long/2addr v0, v4

    .line 88
    add-int/lit8 v2, v2, 0x1

    move p2, v3

    .end local v3    # "valuesOffset":I
    .restart local p2    # "valuesOffset":I
    goto :goto_0
.end method

.method private static readLong([BI)J
    .locals 8
    .param p0, "blocks"    # [B
    .param p1, "blocksOffset"    # I

    .prologue
    const-wide/16 v6, 0xff

    .line 58
    add-int/lit8 v0, p1, 0x1

    .end local p1    # "blocksOffset":I
    .local v0, "blocksOffset":I
    aget-byte v1, p0, p1

    int-to-long v2, v1

    and-long/2addr v2, v6

    const/16 v1, 0x38

    shl-long/2addr v2, v1

    .line 59
    add-int/lit8 p1, v0, 0x1

    .end local v0    # "blocksOffset":I
    .restart local p1    # "blocksOffset":I
    aget-byte v1, p0, v0

    int-to-long v4, v1

    and-long/2addr v4, v6

    const/16 v1, 0x30

    shl-long/2addr v4, v1

    .line 58
    or-long/2addr v2, v4

    .line 60
    add-int/lit8 v0, p1, 0x1

    .end local p1    # "blocksOffset":I
    .restart local v0    # "blocksOffset":I
    aget-byte v1, p0, p1

    int-to-long v4, v1

    and-long/2addr v4, v6

    const/16 v1, 0x28

    shl-long/2addr v4, v1

    .line 58
    or-long/2addr v2, v4

    .line 61
    add-int/lit8 p1, v0, 0x1

    .end local v0    # "blocksOffset":I
    .restart local p1    # "blocksOffset":I
    aget-byte v1, p0, v0

    int-to-long v4, v1

    and-long/2addr v4, v6

    const/16 v1, 0x20

    shl-long/2addr v4, v1

    .line 58
    or-long/2addr v2, v4

    .line 62
    add-int/lit8 v0, p1, 0x1

    .end local p1    # "blocksOffset":I
    .restart local v0    # "blocksOffset":I
    aget-byte v1, p0, p1

    int-to-long v4, v1

    and-long/2addr v4, v6

    const/16 v1, 0x18

    shl-long/2addr v4, v1

    .line 58
    or-long/2addr v2, v4

    .line 63
    add-int/lit8 p1, v0, 0x1

    .end local v0    # "blocksOffset":I
    .restart local p1    # "blocksOffset":I
    aget-byte v1, p0, v0

    int-to-long v4, v1

    and-long/2addr v4, v6

    const/16 v1, 0x10

    shl-long/2addr v4, v1

    .line 58
    or-long/2addr v2, v4

    .line 64
    add-int/lit8 v0, p1, 0x1

    .end local p1    # "blocksOffset":I
    .restart local v0    # "blocksOffset":I
    aget-byte v1, p0, p1

    int-to-long v4, v1

    and-long/2addr v4, v6

    const/16 v1, 0x8

    shl-long/2addr v4, v1

    .line 58
    or-long/2addr v2, v4

    .line 65
    add-int/lit8 p1, v0, 0x1

    .end local v0    # "blocksOffset":I
    .restart local p1    # "blocksOffset":I
    aget-byte v1, p0, v0

    int-to-long v4, v1

    and-long/2addr v4, v6

    .line 58
    or-long/2addr v2, v4

    return-wide v2
.end method


# virtual methods
.method public final byteBlockCount()I
    .locals 1

    .prologue
    .line 44
    const/16 v0, 0x8

    return v0
.end method

.method public final byteValueCount()I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;->valueCount:I

    return v0
.end method

.method public decode([BI[III)V
    .locals 6
    .param p1, "blocks"    # [B
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [I
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 136
    iget v3, p0, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;->bitsPerValue:I

    const/16 v4, 0x20

    if-le v3, v4, :cond_0

    .line 137
    new-instance v3, Ljava/lang/UnsupportedOperationException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Cannot decode "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, p0, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;->bitsPerValue:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "-bits values into an int[]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 139
    :cond_0
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-lt v2, p5, :cond_1

    .line 144
    return-void

    .line 140
    :cond_1
    invoke-static {p1, p2}, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;->readLong([BI)J

    move-result-wide v0

    .line 141
    .local v0, "block":J
    add-int/lit8 p2, p2, 0x8

    .line 142
    invoke-direct {p0, v0, v1, p3, p4}, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;->decode(J[II)I

    move-result p4

    .line 139
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public decode([BI[JII)V
    .locals 3
    .param p1, "blocks"    # [B
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [J
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 114
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-lt v2, p5, :cond_0

    .line 119
    return-void

    .line 115
    :cond_0
    invoke-static {p1, p2}, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;->readLong([BI)J

    move-result-wide v0

    .line 116
    .local v0, "block":J
    add-int/lit8 p2, p2, 0x8

    .line 117
    invoke-direct {p0, v0, v1, p3, p4}, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;->decode(J[JI)I

    move-result p4

    .line 114
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public decode([JI[III)V
    .locals 7
    .param p1, "blocks"    # [J
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [I
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 124
    iget v4, p0, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;->bitsPerValue:I

    const/16 v5, 0x20

    if-le v4, v5, :cond_0

    .line 125
    new-instance v4, Ljava/lang/UnsupportedOperationException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Cannot decode "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;->bitsPerValue:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "-bits values into an int[]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 127
    :cond_0
    const/4 v3, 0x0

    .local v3, "i":I
    move v2, p2

    .end local p2    # "blocksOffset":I
    .local v2, "blocksOffset":I
    :goto_0
    if-lt v3, p5, :cond_1

    .line 131
    return-void

    .line 128
    :cond_1
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v0, p1, v2

    .line 129
    .local v0, "block":J
    invoke-direct {p0, v0, v1, p3, p4}, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;->decode(J[II)I

    move-result p4

    .line 127
    add-int/lit8 v3, v3, 0x1

    move v2, p2

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    goto :goto_0
.end method

.method public decode([JI[JII)V
    .locals 4
    .param p1, "blocks"    # [J
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [J
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 105
    const/4 v3, 0x0

    .local v3, "i":I
    move v2, p2

    .end local p2    # "blocksOffset":I
    .local v2, "blocksOffset":I
    :goto_0
    if-lt v3, p5, :cond_0

    .line 109
    return-void

    .line 106
    :cond_0
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v0, p1, v2

    .line 107
    .local v0, "block":J
    invoke-direct {p0, v0, v1, p3, p4}, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;->decode(J[JI)I

    move-result p4

    .line 105
    add-int/lit8 v3, v3, 0x1

    move v2, p2

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    goto :goto_0
.end method

.method public encode([II[BII)V
    .locals 4
    .param p1, "values"    # [I
    .param p2, "valuesOffset"    # I
    .param p3, "blocks"    # [B
    .param p4, "blocksOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 176
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-lt v2, p5, :cond_0

    .line 181
    return-void

    .line 177
    :cond_0
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;->encode([II)J

    move-result-wide v0

    .line 178
    .local v0, "block":J
    iget v3, p0, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;->valueCount:I

    add-int/2addr p2, v3

    .line 179
    invoke-virtual {p0, v0, v1, p3, p4}, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;->writeLong(J[BI)I

    move-result p4

    .line 176
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public encode([II[JII)V
    .locals 4
    .param p1, "values"    # [I
    .param p2, "valuesOffset"    # I
    .param p3, "blocks"    # [J
    .param p4, "blocksOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 158
    const/4 v1, 0x0

    .local v1, "i":I
    move v0, p4

    .end local p4    # "blocksOffset":I
    .local v0, "blocksOffset":I
    :goto_0
    if-lt v1, p5, :cond_0

    .line 162
    return-void

    .line 159
    :cond_0
    add-int/lit8 p4, v0, 0x1

    .end local v0    # "blocksOffset":I
    .restart local p4    # "blocksOffset":I
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;->encode([II)J

    move-result-wide v2

    aput-wide v2, p3, v0

    .line 160
    iget v2, p0, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;->valueCount:I

    add-int/2addr p2, v2

    .line 158
    add-int/lit8 v1, v1, 0x1

    move v0, p4

    .end local p4    # "blocksOffset":I
    .restart local v0    # "blocksOffset":I
    goto :goto_0
.end method

.method public encode([JI[BII)V
    .locals 4
    .param p1, "values"    # [J
    .param p2, "valuesOffset"    # I
    .param p3, "blocks"    # [B
    .param p4, "blocksOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 166
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-lt v2, p5, :cond_0

    .line 171
    return-void

    .line 167
    :cond_0
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;->encode([JI)J

    move-result-wide v0

    .line 168
    .local v0, "block":J
    iget v3, p0, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;->valueCount:I

    add-int/2addr p2, v3

    .line 169
    invoke-virtual {p0, v0, v1, p3, p4}, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;->writeLong(J[BI)I

    move-result p4

    .line 166
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public encode([JI[JII)V
    .locals 4
    .param p1, "values"    # [J
    .param p2, "valuesOffset"    # I
    .param p3, "blocks"    # [J
    .param p4, "blocksOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 149
    const/4 v1, 0x0

    .local v1, "i":I
    move v0, p4

    .end local p4    # "blocksOffset":I
    .local v0, "blocksOffset":I
    :goto_0
    if-lt v1, p5, :cond_0

    .line 153
    return-void

    .line 150
    :cond_0
    add-int/lit8 p4, v0, 0x1

    .end local v0    # "blocksOffset":I
    .restart local p4    # "blocksOffset":I
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;->encode([JI)J

    move-result-wide v2

    aput-wide v2, p3, v0

    .line 151
    iget v2, p0, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;->valueCount:I

    add-int/2addr p2, v2

    .line 149
    add-int/lit8 v1, v1, 0x1

    move v0, p4

    .end local p4    # "blocksOffset":I
    .restart local v0    # "blocksOffset":I
    goto :goto_0
.end method

.method public final longBlockCount()I
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x1

    return v0
.end method

.method public longValueCount()I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lorg/apache/lucene/util/packed/BulkOperationPackedSingleBlock;->valueCount:I

    return v0
.end method
