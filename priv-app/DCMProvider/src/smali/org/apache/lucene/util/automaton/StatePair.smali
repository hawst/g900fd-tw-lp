.class public Lorg/apache/lucene/util/automaton/StatePair;
.super Ljava/lang/Object;
.source "StatePair.java"


# instance fields
.field s:Lorg/apache/lucene/util/automaton/State;

.field s1:Lorg/apache/lucene/util/automaton/State;

.field s2:Lorg/apache/lucene/util/automaton/State;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/util/automaton/State;Lorg/apache/lucene/util/automaton/State;)V
    .locals 0
    .param p1, "s1"    # Lorg/apache/lucene/util/automaton/State;
    .param p2, "s2"    # Lorg/apache/lucene/util/automaton/State;

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object p1, p0, Lorg/apache/lucene/util/automaton/StatePair;->s1:Lorg/apache/lucene/util/automaton/State;

    .line 56
    iput-object p2, p0, Lorg/apache/lucene/util/automaton/StatePair;->s2:Lorg/apache/lucene/util/automaton/State;

    .line 57
    return-void
.end method

.method constructor <init>(Lorg/apache/lucene/util/automaton/State;Lorg/apache/lucene/util/automaton/State;Lorg/apache/lucene/util/automaton/State;)V
    .locals 0
    .param p1, "s"    # Lorg/apache/lucene/util/automaton/State;
    .param p2, "s1"    # Lorg/apache/lucene/util/automaton/State;
    .param p3, "s2"    # Lorg/apache/lucene/util/automaton/State;

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lorg/apache/lucene/util/automaton/StatePair;->s:Lorg/apache/lucene/util/automaton/State;

    .line 44
    iput-object p2, p0, Lorg/apache/lucene/util/automaton/StatePair;->s1:Lorg/apache/lucene/util/automaton/State;

    .line 45
    iput-object p3, p0, Lorg/apache/lucene/util/automaton/StatePair;->s2:Lorg/apache/lucene/util/automaton/State;

    .line 46
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 86
    instance-of v2, p1, Lorg/apache/lucene/util/automaton/StatePair;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 87
    check-cast v0, Lorg/apache/lucene/util/automaton/StatePair;

    .line 88
    .local v0, "p":Lorg/apache/lucene/util/automaton/StatePair;
    iget-object v2, v0, Lorg/apache/lucene/util/automaton/StatePair;->s1:Lorg/apache/lucene/util/automaton/State;

    iget-object v3, p0, Lorg/apache/lucene/util/automaton/StatePair;->s1:Lorg/apache/lucene/util/automaton/State;

    if-ne v2, v3, :cond_0

    iget-object v2, v0, Lorg/apache/lucene/util/automaton/StatePair;->s2:Lorg/apache/lucene/util/automaton/State;

    iget-object v3, p0, Lorg/apache/lucene/util/automaton/StatePair;->s2:Lorg/apache/lucene/util/automaton/State;

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    .line 89
    .end local v0    # "p":Lorg/apache/lucene/util/automaton/StatePair;
    :cond_0
    return v1
.end method

.method public getFirstState()Lorg/apache/lucene/util/automaton/State;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/StatePair;->s1:Lorg/apache/lucene/util/automaton/State;

    return-object v0
.end method

.method public getSecondState()Lorg/apache/lucene/util/automaton/State;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/StatePair;->s2:Lorg/apache/lucene/util/automaton/State;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 99
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/StatePair;->s1:Lorg/apache/lucene/util/automaton/State;

    invoke-virtual {v0}, Lorg/apache/lucene/util/automaton/State;->hashCode()I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/util/automaton/StatePair;->s2:Lorg/apache/lucene/util/automaton/State;

    invoke-virtual {v1}, Lorg/apache/lucene/util/automaton/State;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
