.class public abstract Lorg/apache/lucene/util/PriorityQueue;
.super Ljava/lang/Object;
.source "PriorityQueue.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final heap:[Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[TT;"
        }
    .end annotation
.end field

.field private final maxSize:I

.field private size:I


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "maxSize"    # I

    .prologue
    .line 37
    .local p0, "this":Lorg/apache/lucene/util/PriorityQueue;, "Lorg/apache/lucene/util/PriorityQueue<TT;>;"
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/util/PriorityQueue;-><init>(IZ)V

    .line 38
    return-void
.end method

.method public constructor <init>(IZ)V
    .locals 5
    .param p1, "maxSize"    # I
    .param p2, "prepopulate"    # Z

    .prologue
    .line 41
    .local p0, "this":Lorg/apache/lucene/util/PriorityQueue;, "Lorg/apache/lucene/util/PriorityQueue<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    const/4 v3, 0x0

    iput v3, p0, Lorg/apache/lucene/util/PriorityQueue;->size:I

    .line 44
    if-nez p1, :cond_1

    .line 46
    const/4 v0, 0x2

    .line 64
    .local v0, "heapSize":I
    :goto_0
    new-array v3, v0, [Ljava/lang/Object;

    iput-object v3, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    .line 65
    iput p1, p0, Lorg/apache/lucene/util/PriorityQueue;->maxSize:I

    .line 67
    if-eqz p2, :cond_0

    .line 69
    invoke-virtual {p0}, Lorg/apache/lucene/util/PriorityQueue;->getSentinelObject()Ljava/lang/Object;

    move-result-object v2

    .line 70
    .local v2, "sentinel":Ljava/lang/Object;, "TT;"
    if-eqz v2, :cond_0

    .line 71
    iget-object v3, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    const/4 v4, 0x1

    aput-object v2, v3, v4

    .line 72
    const/4 v1, 0x2

    .local v1, "i":I
    :goto_1
    iget-object v3, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    array-length v3, v3

    if-lt v1, v3, :cond_3

    .line 75
    iput p1, p0, Lorg/apache/lucene/util/PriorityQueue;->size:I

    .line 78
    .end local v1    # "i":I
    .end local v2    # "sentinel":Ljava/lang/Object;, "TT;"
    :cond_0
    return-void

    .line 48
    .end local v0    # "heapSize":I
    :cond_1
    const v3, 0x7fffffff

    if-ne p1, v3, :cond_2

    .line 57
    const v0, 0x7fffffff

    .line 58
    .restart local v0    # "heapSize":I
    goto :goto_0

    .line 61
    .end local v0    # "heapSize":I
    :cond_2
    add-int/lit8 v0, p1, 0x1

    .restart local v0    # "heapSize":I
    goto :goto_0

    .line 73
    .restart local v1    # "i":I
    .restart local v2    # "sentinel":Ljava/lang/Object;, "TT;"
    :cond_3
    iget-object v3, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    invoke-virtual {p0}, Lorg/apache/lucene/util/PriorityQueue;->getSentinelObject()Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v3, v1

    .line 72
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private final downHeap()V
    .locals 6

    .prologue
    .line 240
    .local p0, "this":Lorg/apache/lucene/util/PriorityQueue;, "Lorg/apache/lucene/util/PriorityQueue<TT;>;"
    const/4 v0, 0x1

    .line 241
    .local v0, "i":I
    iget-object v4, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    aget-object v3, v4, v0

    .line 242
    .local v3, "node":Ljava/lang/Object;, "TT;"
    shl-int/lit8 v1, v0, 0x1

    .line 243
    .local v1, "j":I
    add-int/lit8 v2, v1, 0x1

    .line 244
    .local v2, "k":I
    iget v4, p0, Lorg/apache/lucene/util/PriorityQueue;->size:I

    if-gt v2, v4, :cond_0

    iget-object v4, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    aget-object v4, v4, v2

    iget-object v5, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    aget-object v5, v5, v1

    invoke-virtual {p0, v4, v5}, Lorg/apache/lucene/util/PriorityQueue;->lessThan(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 245
    move v1, v2

    .line 247
    :cond_0
    :goto_0
    iget v4, p0, Lorg/apache/lucene/util/PriorityQueue;->size:I

    if-gt v1, v4, :cond_1

    iget-object v4, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    aget-object v4, v4, v1

    invoke-virtual {p0, v4, v3}, Lorg/apache/lucene/util/PriorityQueue;->lessThan(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 256
    :cond_1
    iget-object v4, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    aput-object v3, v4, v0

    .line 257
    return-void

    .line 248
    :cond_2
    iget-object v4, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    iget-object v5, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    aget-object v5, v5, v1

    aput-object v5, v4, v0

    .line 249
    move v0, v1

    .line 250
    shl-int/lit8 v1, v0, 0x1

    .line 251
    add-int/lit8 v2, v1, 0x1

    .line 252
    iget v4, p0, Lorg/apache/lucene/util/PriorityQueue;->size:I

    if-gt v2, v4, :cond_0

    iget-object v4, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    aget-object v4, v4, v2

    iget-object v5, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    aget-object v5, v5, v1

    invoke-virtual {p0, v4, v5}, Lorg/apache/lucene/util/PriorityQueue;->lessThan(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 253
    move v1, v2

    goto :goto_0
.end method

.method private final upHeap()V
    .locals 5

    .prologue
    .line 228
    .local p0, "this":Lorg/apache/lucene/util/PriorityQueue;, "Lorg/apache/lucene/util/PriorityQueue<TT;>;"
    iget v0, p0, Lorg/apache/lucene/util/PriorityQueue;->size:I

    .line 229
    .local v0, "i":I
    iget-object v3, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    aget-object v2, v3, v0

    .line 230
    .local v2, "node":Ljava/lang/Object;, "TT;"
    ushr-int/lit8 v1, v0, 0x1

    .line 231
    .local v1, "j":I
    :goto_0
    if-lez v1, :cond_0

    iget-object v3, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    aget-object v3, v3, v1

    invoke-virtual {p0, v2, v3}, Lorg/apache/lucene/util/PriorityQueue;->lessThan(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 236
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    aput-object v2, v3, v0

    .line 237
    return-void

    .line 232
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    iget-object v4, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    aget-object v4, v4, v1

    aput-object v4, v3, v0

    .line 233
    move v0, v1

    .line 234
    ushr-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final add(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)TT;"
        }
    .end annotation

    .prologue
    .line 138
    .local p0, "this":Lorg/apache/lucene/util/PriorityQueue;, "Lorg/apache/lucene/util/PriorityQueue<TT;>;"
    .local p1, "element":Ljava/lang/Object;, "TT;"
    iget v0, p0, Lorg/apache/lucene/util/PriorityQueue;->size:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/util/PriorityQueue;->size:I

    .line 139
    iget-object v0, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    iget v1, p0, Lorg/apache/lucene/util/PriorityQueue;->size:I

    aput-object p1, v0, v1

    .line 140
    invoke-direct {p0}, Lorg/apache/lucene/util/PriorityQueue;->upHeap()V

    .line 141
    iget-object v0, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public final clear()V
    .locals 3

    .prologue
    .line 221
    .local p0, "this":Lorg/apache/lucene/util/PriorityQueue;, "Lorg/apache/lucene/util/PriorityQueue<TT;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lorg/apache/lucene/util/PriorityQueue;->size:I

    if-le v0, v1, :cond_0

    .line 224
    const/4 v1, 0x0

    iput v1, p0, Lorg/apache/lucene/util/PriorityQueue;->size:I

    .line 225
    return-void

    .line 222
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v2, v1, v0

    .line 221
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected final getHeapArray()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 263
    .local p0, "this":Lorg/apache/lucene/util/PriorityQueue;, "Lorg/apache/lucene/util/PriorityQueue<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    return-object v0
.end method

.method protected getSentinelObject()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 127
    .local p0, "this":Lorg/apache/lucene/util/PriorityQueue;, "Lorg/apache/lucene/util/PriorityQueue<TT;>;"
    const/4 v0, 0x0

    return-object v0
.end method

.method public insertWithOverflow(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)TT;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/util/PriorityQueue;, "Lorg/apache/lucene/util/PriorityQueue<TT;>;"
    .local p1, "element":Ljava/lang/Object;, "TT;"
    const/4 v3, 0x1

    .line 155
    iget v1, p0, Lorg/apache/lucene/util/PriorityQueue;->size:I

    iget v2, p0, Lorg/apache/lucene/util/PriorityQueue;->maxSize:I

    if-ge v1, v2, :cond_0

    .line 156
    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/PriorityQueue;->add(Ljava/lang/Object;)Ljava/lang/Object;

    .line 157
    const/4 v0, 0x0

    .line 164
    :goto_0
    return-object v0

    .line 158
    :cond_0
    iget v1, p0, Lorg/apache/lucene/util/PriorityQueue;->size:I

    if-lez v1, :cond_1

    iget-object v1, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    aget-object v1, v1, v3

    invoke-virtual {p0, p1, v1}, Lorg/apache/lucene/util/PriorityQueue;->lessThan(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 159
    iget-object v1, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    aget-object v0, v1, v3

    .line 160
    .local v0, "ret":Ljava/lang/Object;, "TT;"
    iget-object v1, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    aput-object p1, v1, v3

    .line 161
    invoke-virtual {p0}, Lorg/apache/lucene/util/PriorityQueue;->updateTop()Ljava/lang/Object;

    goto :goto_0

    .end local v0    # "ret":Ljava/lang/Object;, "TT;"
    :cond_1
    move-object v0, p1

    .line 164
    goto :goto_0
.end method

.method protected abstract lessThan(Ljava/lang/Object;Ljava/lang/Object;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TT;)Z"
        }
    .end annotation
.end method

.method public final pop()Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/util/PriorityQueue;, "Lorg/apache/lucene/util/PriorityQueue<TT;>;"
    const/4 v1, 0x0

    const/4 v5, 0x1

    .line 179
    iget v2, p0, Lorg/apache/lucene/util/PriorityQueue;->size:I

    if-lez v2, :cond_0

    .line 180
    iget-object v2, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    aget-object v0, v2, v5

    .line 181
    .local v0, "result":Ljava/lang/Object;, "TT;"
    iget-object v2, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    iget-object v3, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    iget v4, p0, Lorg/apache/lucene/util/PriorityQueue;->size:I

    aget-object v3, v3, v4

    aput-object v3, v2, v5

    .line 182
    iget-object v2, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    iget v3, p0, Lorg/apache/lucene/util/PriorityQueue;->size:I

    aput-object v1, v2, v3

    .line 183
    iget v1, p0, Lorg/apache/lucene/util/PriorityQueue;->size:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/lucene/util/PriorityQueue;->size:I

    .line 184
    invoke-direct {p0}, Lorg/apache/lucene/util/PriorityQueue;->downHeap()V

    .line 187
    .end local v0    # "result":Ljava/lang/Object;, "TT;"
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 216
    .local p0, "this":Lorg/apache/lucene/util/PriorityQueue;, "Lorg/apache/lucene/util/PriorityQueue<TT;>;"
    iget v0, p0, Lorg/apache/lucene/util/PriorityQueue;->size:I

    return v0
.end method

.method public final top()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 173
    .local p0, "this":Lorg/apache/lucene/util/PriorityQueue;, "Lorg/apache/lucene/util/PriorityQueue<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public final updateTop()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 210
    .local p0, "this":Lorg/apache/lucene/util/PriorityQueue;, "Lorg/apache/lucene/util/PriorityQueue<TT;>;"
    invoke-direct {p0}, Lorg/apache/lucene/util/PriorityQueue;->downHeap()V

    .line 211
    iget-object v0, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    return-object v0
.end method
