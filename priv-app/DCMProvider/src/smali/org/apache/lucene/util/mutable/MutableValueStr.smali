.class public Lorg/apache/lucene/util/mutable/MutableValueStr;
.super Lorg/apache/lucene/util/mutable/MutableValue;
.source "MutableValueStr.java"


# instance fields
.field public value:Lorg/apache/lucene/util/BytesRef;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lorg/apache/lucene/util/mutable/MutableValue;-><init>()V

    .line 26
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/util/mutable/MutableValueStr;->value:Lorg/apache/lucene/util/BytesRef;

    .line 25
    return-void
.end method


# virtual methods
.method public compareSameType(Ljava/lang/Object;)I
    .locals 4
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 56
    move-object v0, p1

    check-cast v0, Lorg/apache/lucene/util/mutable/MutableValueStr;

    .line 57
    .local v0, "b":Lorg/apache/lucene/util/mutable/MutableValueStr;
    iget-object v2, p0, Lorg/apache/lucene/util/mutable/MutableValueStr;->value:Lorg/apache/lucene/util/BytesRef;

    iget-object v3, v0, Lorg/apache/lucene/util/mutable/MutableValueStr;->value:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/util/BytesRef;->compareTo(Lorg/apache/lucene/util/BytesRef;)I

    move-result v1

    .line 58
    .local v1, "c":I
    if-eqz v1, :cond_0

    .line 60
    .end local v1    # "c":I
    :goto_0
    return v1

    .line 59
    .restart local v1    # "c":I
    :cond_0
    iget-boolean v2, p0, Lorg/apache/lucene/util/mutable/MutableValueStr;->exists:Z

    iget-boolean v3, v0, Lorg/apache/lucene/util/mutable/MutableValueStr;->exists:Z

    if-ne v2, v3, :cond_1

    const/4 v1, 0x0

    goto :goto_0

    .line 60
    :cond_1
    iget-boolean v2, p0, Lorg/apache/lucene/util/mutable/MutableValueStr;->exists:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :goto_1
    move v1, v2

    goto :goto_0

    :cond_2
    const/4 v2, -0x1

    goto :goto_1
.end method

.method public copy(Lorg/apache/lucene/util/mutable/MutableValue;)V
    .locals 3
    .param p1, "source"    # Lorg/apache/lucene/util/mutable/MutableValue;

    .prologue
    .line 35
    move-object v0, p1

    check-cast v0, Lorg/apache/lucene/util/mutable/MutableValueStr;

    .line 36
    .local v0, "s":Lorg/apache/lucene/util/mutable/MutableValueStr;
    iget-boolean v1, v0, Lorg/apache/lucene/util/mutable/MutableValueStr;->exists:Z

    iput-boolean v1, p0, Lorg/apache/lucene/util/mutable/MutableValueStr;->exists:Z

    .line 37
    iget-object v1, p0, Lorg/apache/lucene/util/mutable/MutableValueStr;->value:Lorg/apache/lucene/util/BytesRef;

    iget-object v2, v0, Lorg/apache/lucene/util/mutable/MutableValueStr;->value:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/BytesRef;->copyBytes(Lorg/apache/lucene/util/BytesRef;)V

    .line 38
    return-void
.end method

.method public duplicate()Lorg/apache/lucene/util/mutable/MutableValue;
    .locals 3

    .prologue
    .line 42
    new-instance v0, Lorg/apache/lucene/util/mutable/MutableValueStr;

    invoke-direct {v0}, Lorg/apache/lucene/util/mutable/MutableValueStr;-><init>()V

    .line 43
    .local v0, "v":Lorg/apache/lucene/util/mutable/MutableValueStr;
    iget-object v1, v0, Lorg/apache/lucene/util/mutable/MutableValueStr;->value:Lorg/apache/lucene/util/BytesRef;

    iget-object v2, p0, Lorg/apache/lucene/util/mutable/MutableValueStr;->value:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/BytesRef;->copyBytes(Lorg/apache/lucene/util/BytesRef;)V

    .line 44
    iget-boolean v1, p0, Lorg/apache/lucene/util/mutable/MutableValueStr;->exists:Z

    iput-boolean v1, v0, Lorg/apache/lucene/util/mutable/MutableValueStr;->exists:Z

    .line 45
    return-object v0
.end method

.method public equalsSameType(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 50
    move-object v0, p1

    check-cast v0, Lorg/apache/lucene/util/mutable/MutableValueStr;

    .line 51
    .local v0, "b":Lorg/apache/lucene/util/mutable/MutableValueStr;
    iget-object v1, p0, Lorg/apache/lucene/util/mutable/MutableValueStr;->value:Lorg/apache/lucene/util/BytesRef;

    iget-object v2, v0, Lorg/apache/lucene/util/mutable/MutableValueStr;->value:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/BytesRef;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lorg/apache/lucene/util/mutable/MutableValueStr;->exists:Z

    iget-boolean v2, v0, Lorg/apache/lucene/util/mutable/MutableValueStr;->exists:Z

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lorg/apache/lucene/util/mutable/MutableValueStr;->value:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v0}, Lorg/apache/lucene/util/BytesRef;->hashCode()I

    move-result v0

    return v0
.end method

.method public toObject()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 30
    iget-boolean v0, p0, Lorg/apache/lucene/util/mutable/MutableValueStr;->exists:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/util/mutable/MutableValueStr;->value:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v0}, Lorg/apache/lucene/util/BytesRef;->utf8ToString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
