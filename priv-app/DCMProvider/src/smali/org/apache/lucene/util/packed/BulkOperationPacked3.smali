.class final Lorg/apache/lucene/util/packed/BulkOperationPacked3;
.super Lorg/apache/lucene/util/packed/BulkOperationPacked;
.source "BulkOperationPacked3.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lorg/apache/lucene/util/packed/BulkOperationPacked;-><init>(I)V

    .line 29
    return-void
.end method


# virtual methods
.method public decode([BI[III)V
    .locals 8
    .param p1, "blocks"    # [B
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [I
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 106
    const/4 v4, 0x0

    .local v4, "i":I
    move v5, p4

    .end local p4    # "valuesOffset":I
    .local v5, "valuesOffset":I
    move v0, p2

    .end local p2    # "blocksOffset":I
    .local v0, "blocksOffset":I
    :goto_0
    if-lt v4, p5, :cond_0

    .line 119
    return-void

    .line 107
    :cond_0
    add-int/lit8 p2, v0, 0x1

    .end local v0    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v6, p1, v0

    and-int/lit16 v1, v6, 0xff

    .line 108
    .local v1, "byte0":I
    add-int/lit8 p4, v5, 0x1

    .end local v5    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    ushr-int/lit8 v6, v1, 0x5

    aput v6, p3, v5

    .line 109
    add-int/lit8 v5, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v5    # "valuesOffset":I
    ushr-int/lit8 v6, v1, 0x2

    and-int/lit8 v6, v6, 0x7

    aput v6, p3, p4

    .line 110
    add-int/lit8 v0, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v0    # "blocksOffset":I
    aget-byte v6, p1, p2

    and-int/lit16 v2, v6, 0xff

    .line 111
    .local v2, "byte1":I
    add-int/lit8 p4, v5, 0x1

    .end local v5    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    and-int/lit8 v6, v1, 0x3

    shl-int/lit8 v6, v6, 0x1

    ushr-int/lit8 v7, v2, 0x7

    or-int/2addr v6, v7

    aput v6, p3, v5

    .line 112
    add-int/lit8 v5, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v5    # "valuesOffset":I
    ushr-int/lit8 v6, v2, 0x4

    and-int/lit8 v6, v6, 0x7

    aput v6, p3, p4

    .line 113
    add-int/lit8 p4, v5, 0x1

    .end local v5    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    ushr-int/lit8 v6, v2, 0x1

    and-int/lit8 v6, v6, 0x7

    aput v6, p3, v5

    .line 114
    add-int/lit8 p2, v0, 0x1

    .end local v0    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v6, p1, v0

    and-int/lit16 v3, v6, 0xff

    .line 115
    .local v3, "byte2":I
    add-int/lit8 v5, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v5    # "valuesOffset":I
    and-int/lit8 v6, v2, 0x1

    shl-int/lit8 v6, v6, 0x2

    ushr-int/lit8 v7, v3, 0x6

    or-int/2addr v6, v7

    aput v6, p3, p4

    .line 116
    add-int/lit8 p4, v5, 0x1

    .end local v5    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    ushr-int/lit8 v6, v3, 0x3

    and-int/lit8 v6, v6, 0x7

    aput v6, p3, v5

    .line 117
    add-int/lit8 v5, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v5    # "valuesOffset":I
    and-int/lit8 v6, v3, 0x7

    aput v6, p3, p4

    .line 106
    add-int/lit8 v4, v4, 0x1

    move v0, p2

    .end local p2    # "blocksOffset":I
    .restart local v0    # "blocksOffset":I
    goto :goto_0
.end method

.method public decode([BI[JII)V
    .locals 16
    .param p1, "blocks"    # [B
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [J
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 196
    const/4 v3, 0x0

    .local v3, "i":I
    move/from16 v10, p4

    .end local p4    # "valuesOffset":I
    .local v10, "valuesOffset":I
    move/from16 v2, p2

    .end local p2    # "blocksOffset":I
    .local v2, "blocksOffset":I
    :goto_0
    move/from16 v0, p5

    if-lt v3, v0, :cond_0

    .line 209
    return-void

    .line 197
    :cond_0
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v11, p1, v2

    and-int/lit16 v11, v11, 0xff

    int-to-long v4, v11

    .line 198
    .local v4, "byte0":J
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/4 v11, 0x5

    ushr-long v12, v4, v11

    aput-wide v12, p3, v10

    .line 199
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/4 v11, 0x2

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, p4

    .line 200
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v11, p1, p2

    and-int/lit16 v11, v11, 0xff

    int-to-long v6, v11

    .line 201
    .local v6, "byte1":J
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v12, 0x3

    and-long/2addr v12, v4

    const/4 v11, 0x1

    shl-long/2addr v12, v11

    const/4 v11, 0x7

    ushr-long v14, v6, v11

    or-long/2addr v12, v14

    aput-wide v12, p3, v10

    .line 202
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/4 v11, 0x4

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, p4

    .line 203
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/4 v11, 0x1

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, v10

    .line 204
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v11, p1, v2

    and-int/lit16 v11, v11, 0xff

    int-to-long v8, v11

    .line 205
    .local v8, "byte2":J
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const-wide/16 v12, 0x1

    and-long/2addr v12, v6

    const/4 v11, 0x2

    shl-long/2addr v12, v11

    const/4 v11, 0x6

    ushr-long v14, v8, v11

    or-long/2addr v12, v14

    aput-wide v12, p3, p4

    .line 206
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/4 v11, 0x3

    ushr-long v12, v8, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, v10

    .line 207
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const-wide/16 v12, 0x7

    and-long/2addr v12, v8

    aput-wide v12, p3, p4

    .line 196
    add-int/lit8 v3, v3, 0x1

    move/from16 v2, p2

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    goto :goto_0
.end method

.method public decode([JI[III)V
    .locals 16
    .param p1, "blocks"    # [J
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [I
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 33
    const/4 v9, 0x0

    .local v9, "i":I
    move/from16 v10, p4

    .end local p4    # "valuesOffset":I
    .local v10, "valuesOffset":I
    move/from16 v8, p2

    .end local p2    # "blocksOffset":I
    .local v8, "blocksOffset":I
    :goto_0
    move/from16 v0, p5

    if-lt v9, v0, :cond_0

    .line 102
    return-void

    .line 34
    :cond_0
    add-int/lit8 p2, v8, 0x1

    .end local v8    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v2, p1, v8

    .line 35
    .local v2, "block0":J
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x3d

    ushr-long v12, v2, v11

    long-to-int v11, v12

    aput v11, p3, v10

    .line 36
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x3a

    ushr-long v12, v2, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, p4

    .line 37
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x37

    ushr-long v12, v2, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, v10

    .line 38
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x34

    ushr-long v12, v2, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, p4

    .line 39
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x31

    ushr-long v12, v2, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, v10

    .line 40
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x2e

    ushr-long v12, v2, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, p4

    .line 41
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x2b

    ushr-long v12, v2, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, v10

    .line 42
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x28

    ushr-long v12, v2, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, p4

    .line 43
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x25

    ushr-long v12, v2, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, v10

    .line 44
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x22

    ushr-long v12, v2, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, p4

    .line 45
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x1f

    ushr-long v12, v2, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, v10

    .line 46
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x1c

    ushr-long v12, v2, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, p4

    .line 47
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x19

    ushr-long v12, v2, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, v10

    .line 48
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x16

    ushr-long v12, v2, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, p4

    .line 49
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x13

    ushr-long v12, v2, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, v10

    .line 50
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x10

    ushr-long v12, v2, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, p4

    .line 51
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0xd

    ushr-long v12, v2, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, v10

    .line 52
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0xa

    ushr-long v12, v2, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, p4

    .line 53
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/4 v11, 0x7

    ushr-long v12, v2, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, v10

    .line 54
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/4 v11, 0x4

    ushr-long v12, v2, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, p4

    .line 55
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/4 v11, 0x1

    ushr-long v12, v2, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, v10

    .line 56
    add-int/lit8 v8, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v8    # "blocksOffset":I
    aget-wide v4, p1, p2

    .line 57
    .local v4, "block1":J
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const-wide/16 v12, 0x1

    and-long/2addr v12, v2

    const/4 v11, 0x2

    shl-long/2addr v12, v11

    const/16 v11, 0x3e

    ushr-long v14, v4, v11

    or-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, p4

    .line 58
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x3b

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, v10

    .line 59
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x38

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, p4

    .line 60
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x35

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, v10

    .line 61
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x32

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, p4

    .line 62
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x2f

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, v10

    .line 63
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x2c

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, p4

    .line 64
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x29

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, v10

    .line 65
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x26

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, p4

    .line 66
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x23

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, v10

    .line 67
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x20

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, p4

    .line 68
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x1d

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, v10

    .line 69
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x1a

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, p4

    .line 70
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x17

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, v10

    .line 71
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x14

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, p4

    .line 72
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x11

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, v10

    .line 73
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0xe

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, p4

    .line 74
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0xb

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, v10

    .line 75
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x8

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, p4

    .line 76
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/4 v11, 0x5

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, v10

    .line 77
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/4 v11, 0x2

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, p4

    .line 78
    add-int/lit8 p2, v8, 0x1

    .end local v8    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v6, p1, v8

    .line 79
    .local v6, "block2":J
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v12, 0x3

    and-long/2addr v12, v4

    const/4 v11, 0x1

    shl-long/2addr v12, v11

    const/16 v11, 0x3f

    ushr-long v14, v6, v11

    or-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, v10

    .line 80
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x3c

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, p4

    .line 81
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x39

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, v10

    .line 82
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x36

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, p4

    .line 83
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x33

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, v10

    .line 84
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x30

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, p4

    .line 85
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x2d

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, v10

    .line 86
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x2a

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, p4

    .line 87
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x27

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, v10

    .line 88
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x24

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, p4

    .line 89
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x21

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, v10

    .line 90
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x1e

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, p4

    .line 91
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x1b

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, v10

    .line 92
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x18

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, p4

    .line 93
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x15

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, v10

    .line 94
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x12

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, p4

    .line 95
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0xf

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, v10

    .line 96
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0xc

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, p4

    .line 97
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x9

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, v10

    .line 98
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/4 v11, 0x6

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, p4

    .line 99
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/4 v11, 0x3

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    long-to-int v11, v12

    aput v11, p3, v10

    .line 100
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const-wide/16 v12, 0x7

    and-long/2addr v12, v6

    long-to-int v11, v12

    aput v11, p3, p4

    .line 33
    add-int/lit8 v9, v9, 0x1

    move/from16 v8, p2

    .end local p2    # "blocksOffset":I
    .restart local v8    # "blocksOffset":I
    goto/16 :goto_0
.end method

.method public decode([JI[JII)V
    .locals 16
    .param p1, "blocks"    # [J
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [J
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 123
    const/4 v9, 0x0

    .local v9, "i":I
    move/from16 v10, p4

    .end local p4    # "valuesOffset":I
    .local v10, "valuesOffset":I
    move/from16 v8, p2

    .end local p2    # "blocksOffset":I
    .local v8, "blocksOffset":I
    :goto_0
    move/from16 v0, p5

    if-lt v9, v0, :cond_0

    .line 192
    return-void

    .line 124
    :cond_0
    add-int/lit8 p2, v8, 0x1

    .end local v8    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v2, p1, v8

    .line 125
    .local v2, "block0":J
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x3d

    ushr-long v12, v2, v11

    aput-wide v12, p3, v10

    .line 126
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x3a

    ushr-long v12, v2, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, p4

    .line 127
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x37

    ushr-long v12, v2, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, v10

    .line 128
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x34

    ushr-long v12, v2, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, p4

    .line 129
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x31

    ushr-long v12, v2, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, v10

    .line 130
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x2e

    ushr-long v12, v2, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, p4

    .line 131
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x2b

    ushr-long v12, v2, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, v10

    .line 132
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x28

    ushr-long v12, v2, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, p4

    .line 133
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x25

    ushr-long v12, v2, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, v10

    .line 134
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x22

    ushr-long v12, v2, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, p4

    .line 135
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x1f

    ushr-long v12, v2, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, v10

    .line 136
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x1c

    ushr-long v12, v2, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, p4

    .line 137
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x19

    ushr-long v12, v2, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, v10

    .line 138
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x16

    ushr-long v12, v2, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, p4

    .line 139
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x13

    ushr-long v12, v2, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, v10

    .line 140
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x10

    ushr-long v12, v2, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, p4

    .line 141
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0xd

    ushr-long v12, v2, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, v10

    .line 142
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0xa

    ushr-long v12, v2, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, p4

    .line 143
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/4 v11, 0x7

    ushr-long v12, v2, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, v10

    .line 144
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/4 v11, 0x4

    ushr-long v12, v2, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, p4

    .line 145
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/4 v11, 0x1

    ushr-long v12, v2, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, v10

    .line 146
    add-int/lit8 v8, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v8    # "blocksOffset":I
    aget-wide v4, p1, p2

    .line 147
    .local v4, "block1":J
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const-wide/16 v12, 0x1

    and-long/2addr v12, v2

    const/4 v11, 0x2

    shl-long/2addr v12, v11

    const/16 v11, 0x3e

    ushr-long v14, v4, v11

    or-long/2addr v12, v14

    aput-wide v12, p3, p4

    .line 148
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x3b

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, v10

    .line 149
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x38

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, p4

    .line 150
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x35

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, v10

    .line 151
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x32

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, p4

    .line 152
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x2f

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, v10

    .line 153
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x2c

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, p4

    .line 154
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x29

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, v10

    .line 155
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x26

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, p4

    .line 156
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x23

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, v10

    .line 157
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x20

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, p4

    .line 158
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x1d

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, v10

    .line 159
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x1a

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, p4

    .line 160
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x17

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, v10

    .line 161
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x14

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, p4

    .line 162
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x11

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, v10

    .line 163
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0xe

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, p4

    .line 164
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0xb

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, v10

    .line 165
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x8

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, p4

    .line 166
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/4 v11, 0x5

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, v10

    .line 167
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/4 v11, 0x2

    ushr-long v12, v4, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, p4

    .line 168
    add-int/lit8 p2, v8, 0x1

    .end local v8    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v6, p1, v8

    .line 169
    .local v6, "block2":J
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v12, 0x3

    and-long/2addr v12, v4

    const/4 v11, 0x1

    shl-long/2addr v12, v11

    const/16 v11, 0x3f

    ushr-long v14, v6, v11

    or-long/2addr v12, v14

    aput-wide v12, p3, v10

    .line 170
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x3c

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, p4

    .line 171
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x39

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, v10

    .line 172
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x36

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, p4

    .line 173
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x33

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, v10

    .line 174
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x30

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, p4

    .line 175
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x2d

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, v10

    .line 176
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x2a

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, p4

    .line 177
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x27

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, v10

    .line 178
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x24

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, p4

    .line 179
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x21

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, v10

    .line 180
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x1e

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, p4

    .line 181
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x1b

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, v10

    .line 182
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x18

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, p4

    .line 183
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x15

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, v10

    .line 184
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0x12

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, p4

    .line 185
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0xf

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, v10

    .line 186
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/16 v11, 0xc

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, p4

    .line 187
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v11, 0x9

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, v10

    .line 188
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const/4 v11, 0x6

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, p4

    .line 189
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/4 v11, 0x3

    ushr-long v12, v6, v11

    const-wide/16 v14, 0x7

    and-long/2addr v12, v14

    aput-wide v12, p3, v10

    .line 190
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    const-wide/16 v12, 0x7

    and-long/2addr v12, v6

    aput-wide v12, p3, p4

    .line 123
    add-int/lit8 v9, v9, 0x1

    move/from16 v8, p2

    .end local p2    # "blocksOffset":I
    .restart local v8    # "blocksOffset":I
    goto/16 :goto_0
.end method
