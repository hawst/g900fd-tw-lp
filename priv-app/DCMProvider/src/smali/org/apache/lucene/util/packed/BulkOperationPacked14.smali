.class final Lorg/apache/lucene/util/packed/BulkOperationPacked14;
.super Lorg/apache/lucene/util/packed/BulkOperationPacked;
.source "BulkOperationPacked14.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    const/16 v0, 0xe

    invoke-direct {p0, v0}, Lorg/apache/lucene/util/packed/BulkOperationPacked;-><init>(I)V

    .line 29
    return-void
.end method


# virtual methods
.method public decode([BI[III)V
    .locals 13
    .param p1, "blocks"    # [B
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [I
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 78
    const/4 v9, 0x0

    .local v9, "i":I
    move/from16 v10, p4

    .end local p4    # "valuesOffset":I
    .local v10, "valuesOffset":I
    move v1, p2

    .end local p2    # "blocksOffset":I
    .local v1, "blocksOffset":I
    :goto_0
    move/from16 v0, p5

    if-lt v9, v0, :cond_0

    .line 91
    return-void

    .line 79
    :cond_0
    add-int/lit8 p2, v1, 0x1

    .end local v1    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v11, p1, v1

    and-int/lit16 v2, v11, 0xff

    .line 80
    .local v2, "byte0":I
    add-int/lit8 v1, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v1    # "blocksOffset":I
    aget-byte v11, p1, p2

    and-int/lit16 v3, v11, 0xff

    .line 81
    .local v3, "byte1":I
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    shl-int/lit8 v11, v2, 0x6

    ushr-int/lit8 v12, v3, 0x2

    or-int/2addr v11, v12

    aput v11, p3, v10

    .line 82
    add-int/lit8 p2, v1, 0x1

    .end local v1    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v11, p1, v1

    and-int/lit16 v4, v11, 0xff

    .line 83
    .local v4, "byte2":I
    add-int/lit8 v1, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v1    # "blocksOffset":I
    aget-byte v11, p1, p2

    and-int/lit16 v5, v11, 0xff

    .line 84
    .local v5, "byte3":I
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    and-int/lit8 v11, v3, 0x3

    shl-int/lit8 v11, v11, 0xc

    shl-int/lit8 v12, v4, 0x4

    or-int/2addr v11, v12

    ushr-int/lit8 v12, v5, 0x4

    or-int/2addr v11, v12

    aput v11, p3, p4

    .line 85
    add-int/lit8 p2, v1, 0x1

    .end local v1    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v11, p1, v1

    and-int/lit16 v6, v11, 0xff

    .line 86
    .local v6, "byte4":I
    add-int/lit8 v1, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v1    # "blocksOffset":I
    aget-byte v11, p1, p2

    and-int/lit16 v7, v11, 0xff

    .line 87
    .local v7, "byte5":I
    add-int/lit8 p4, v10, 0x1

    .end local v10    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    and-int/lit8 v11, v5, 0xf

    shl-int/lit8 v11, v11, 0xa

    shl-int/lit8 v12, v6, 0x2

    or-int/2addr v11, v12

    ushr-int/lit8 v12, v7, 0x6

    or-int/2addr v11, v12

    aput v11, p3, v10

    .line 88
    add-int/lit8 p2, v1, 0x1

    .end local v1    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v11, p1, v1

    and-int/lit16 v8, v11, 0xff

    .line 89
    .local v8, "byte6":I
    add-int/lit8 v10, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v10    # "valuesOffset":I
    and-int/lit8 v11, v7, 0x3f

    shl-int/lit8 v11, v11, 0x8

    or-int/2addr v11, v8

    aput v11, p3, p4

    .line 78
    add-int/lit8 v9, v9, 0x1

    move v1, p2

    .end local p2    # "blocksOffset":I
    .restart local v1    # "blocksOffset":I
    goto :goto_0
.end method

.method public decode([BI[JII)V
    .locals 24
    .param p1, "blocks"    # [B
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [J
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 140
    const/4 v3, 0x0

    .local v3, "i":I
    move/from16 v18, p4

    .end local p4    # "valuesOffset":I
    .local v18, "valuesOffset":I
    move/from16 v2, p2

    .end local p2    # "blocksOffset":I
    .local v2, "blocksOffset":I
    :goto_0
    move/from16 v0, p5

    if-lt v3, v0, :cond_0

    .line 153
    return-void

    .line 141
    :cond_0
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v19, p1, v2

    move/from16 v0, v19

    and-int/lit16 v0, v0, 0xff

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-long v4, v0

    .line 142
    .local v4, "byte0":J
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v19, p1, p2

    move/from16 v0, v19

    and-int/lit16 v0, v0, 0xff

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-long v6, v0

    .line 143
    .local v6, "byte1":J
    add-int/lit8 p4, v18, 0x1

    .end local v18    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v19, 0x6

    shl-long v20, v4, v19

    const/16 v19, 0x2

    ushr-long v22, v6, v19

    or-long v20, v20, v22

    aput-wide v20, p3, v18

    .line 144
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v19, p1, v2

    move/from16 v0, v19

    and-int/lit16 v0, v0, 0xff

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-long v8, v0

    .line 145
    .local v8, "byte2":J
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v19, p1, p2

    move/from16 v0, v19

    and-int/lit16 v0, v0, 0xff

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-long v10, v0

    .line 146
    .local v10, "byte3":J
    add-int/lit8 v18, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v18    # "valuesOffset":I
    const-wide/16 v20, 0x3

    and-long v20, v20, v6

    const/16 v19, 0xc

    shl-long v20, v20, v19

    const/16 v19, 0x4

    shl-long v22, v8, v19

    or-long v20, v20, v22

    const/16 v19, 0x4

    ushr-long v22, v10, v19

    or-long v20, v20, v22

    aput-wide v20, p3, p4

    .line 147
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v19, p1, v2

    move/from16 v0, v19

    and-int/lit16 v0, v0, 0xff

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-long v12, v0

    .line 148
    .local v12, "byte4":J
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v19, p1, p2

    move/from16 v0, v19

    and-int/lit16 v0, v0, 0xff

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-long v14, v0

    .line 149
    .local v14, "byte5":J
    add-int/lit8 p4, v18, 0x1

    .end local v18    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v20, 0xf

    and-long v20, v20, v10

    const/16 v19, 0xa

    shl-long v20, v20, v19

    const/16 v19, 0x2

    shl-long v22, v12, v19

    or-long v20, v20, v22

    const/16 v19, 0x6

    ushr-long v22, v14, v19

    or-long v20, v20, v22

    aput-wide v20, p3, v18

    .line 150
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v19, p1, v2

    move/from16 v0, v19

    and-int/lit16 v0, v0, 0xff

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-long v0, v0

    move-wide/from16 v16, v0

    .line 151
    .local v16, "byte6":J
    add-int/lit8 v18, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v18    # "valuesOffset":I
    const-wide/16 v20, 0x3f

    and-long v20, v20, v14

    const/16 v19, 0x8

    shl-long v20, v20, v19

    or-long v20, v20, v16

    aput-wide v20, p3, p4

    .line 140
    add-int/lit8 v3, v3, 0x1

    move/from16 v2, p2

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    goto/16 :goto_0
.end method

.method public decode([JI[III)V
    .locals 24
    .param p1, "blocks"    # [J
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [I
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 33
    const/16 v17, 0x0

    .local v17, "i":I
    move/from16 v18, p4

    .end local p4    # "valuesOffset":I
    .local v18, "valuesOffset":I
    move/from16 v16, p2

    .end local p2    # "blocksOffset":I
    .local v16, "blocksOffset":I
    :goto_0
    move/from16 v0, v17

    move/from16 v1, p5

    if-lt v0, v1, :cond_0

    .line 74
    return-void

    .line 34
    :cond_0
    add-int/lit8 p2, v16, 0x1

    .end local v16    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v2, p1, v16

    .line 35
    .local v2, "block0":J
    add-int/lit8 p4, v18, 0x1

    .end local v18    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v19, 0x32

    ushr-long v20, v2, v19

    move-wide/from16 v0, v20

    long-to-int v0, v0

    move/from16 v19, v0

    aput v19, p3, v18

    .line 36
    add-int/lit8 v18, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v18    # "valuesOffset":I
    const/16 v19, 0x24

    ushr-long v20, v2, v19

    const-wide/16 v22, 0x3fff

    and-long v20, v20, v22

    move-wide/from16 v0, v20

    long-to-int v0, v0

    move/from16 v19, v0

    aput v19, p3, p4

    .line 37
    add-int/lit8 p4, v18, 0x1

    .end local v18    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v19, 0x16

    ushr-long v20, v2, v19

    const-wide/16 v22, 0x3fff

    and-long v20, v20, v22

    move-wide/from16 v0, v20

    long-to-int v0, v0

    move/from16 v19, v0

    aput v19, p3, v18

    .line 38
    add-int/lit8 v18, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v18    # "valuesOffset":I
    const/16 v19, 0x8

    ushr-long v20, v2, v19

    const-wide/16 v22, 0x3fff

    and-long v20, v20, v22

    move-wide/from16 v0, v20

    long-to-int v0, v0

    move/from16 v19, v0

    aput v19, p3, p4

    .line 39
    add-int/lit8 v16, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v16    # "blocksOffset":I
    aget-wide v4, p1, p2

    .line 40
    .local v4, "block1":J
    add-int/lit8 p4, v18, 0x1

    .end local v18    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v20, 0xff

    and-long v20, v20, v2

    const/16 v19, 0x6

    shl-long v20, v20, v19

    const/16 v19, 0x3a

    ushr-long v22, v4, v19

    or-long v20, v20, v22

    move-wide/from16 v0, v20

    long-to-int v0, v0

    move/from16 v19, v0

    aput v19, p3, v18

    .line 41
    add-int/lit8 v18, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v18    # "valuesOffset":I
    const/16 v19, 0x2c

    ushr-long v20, v4, v19

    const-wide/16 v22, 0x3fff

    and-long v20, v20, v22

    move-wide/from16 v0, v20

    long-to-int v0, v0

    move/from16 v19, v0

    aput v19, p3, p4

    .line 42
    add-int/lit8 p4, v18, 0x1

    .end local v18    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v19, 0x1e

    ushr-long v20, v4, v19

    const-wide/16 v22, 0x3fff

    and-long v20, v20, v22

    move-wide/from16 v0, v20

    long-to-int v0, v0

    move/from16 v19, v0

    aput v19, p3, v18

    .line 43
    add-int/lit8 v18, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v18    # "valuesOffset":I
    const/16 v19, 0x10

    ushr-long v20, v4, v19

    const-wide/16 v22, 0x3fff

    and-long v20, v20, v22

    move-wide/from16 v0, v20

    long-to-int v0, v0

    move/from16 v19, v0

    aput v19, p3, p4

    .line 44
    add-int/lit8 p4, v18, 0x1

    .end local v18    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v19, 0x2

    ushr-long v20, v4, v19

    const-wide/16 v22, 0x3fff

    and-long v20, v20, v22

    move-wide/from16 v0, v20

    long-to-int v0, v0

    move/from16 v19, v0

    aput v19, p3, v18

    .line 45
    add-int/lit8 p2, v16, 0x1

    .end local v16    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v6, p1, v16

    .line 46
    .local v6, "block2":J
    add-int/lit8 v18, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v18    # "valuesOffset":I
    const-wide/16 v20, 0x3

    and-long v20, v20, v4

    const/16 v19, 0xc

    shl-long v20, v20, v19

    const/16 v19, 0x34

    ushr-long v22, v6, v19

    or-long v20, v20, v22

    move-wide/from16 v0, v20

    long-to-int v0, v0

    move/from16 v19, v0

    aput v19, p3, p4

    .line 47
    add-int/lit8 p4, v18, 0x1

    .end local v18    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v19, 0x26

    ushr-long v20, v6, v19

    const-wide/16 v22, 0x3fff

    and-long v20, v20, v22

    move-wide/from16 v0, v20

    long-to-int v0, v0

    move/from16 v19, v0

    aput v19, p3, v18

    .line 48
    add-int/lit8 v18, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v18    # "valuesOffset":I
    const/16 v19, 0x18

    ushr-long v20, v6, v19

    const-wide/16 v22, 0x3fff

    and-long v20, v20, v22

    move-wide/from16 v0, v20

    long-to-int v0, v0

    move/from16 v19, v0

    aput v19, p3, p4

    .line 49
    add-int/lit8 p4, v18, 0x1

    .end local v18    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v19, 0xa

    ushr-long v20, v6, v19

    const-wide/16 v22, 0x3fff

    and-long v20, v20, v22

    move-wide/from16 v0, v20

    long-to-int v0, v0

    move/from16 v19, v0

    aput v19, p3, v18

    .line 50
    add-int/lit8 v16, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v16    # "blocksOffset":I
    aget-wide v8, p1, p2

    .line 51
    .local v8, "block3":J
    add-int/lit8 v18, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v18    # "valuesOffset":I
    const-wide/16 v20, 0x3ff

    and-long v20, v20, v6

    const/16 v19, 0x4

    shl-long v20, v20, v19

    const/16 v19, 0x3c

    ushr-long v22, v8, v19

    or-long v20, v20, v22

    move-wide/from16 v0, v20

    long-to-int v0, v0

    move/from16 v19, v0

    aput v19, p3, p4

    .line 52
    add-int/lit8 p4, v18, 0x1

    .end local v18    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v19, 0x2e

    ushr-long v20, v8, v19

    const-wide/16 v22, 0x3fff

    and-long v20, v20, v22

    move-wide/from16 v0, v20

    long-to-int v0, v0

    move/from16 v19, v0

    aput v19, p3, v18

    .line 53
    add-int/lit8 v18, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v18    # "valuesOffset":I
    const/16 v19, 0x20

    ushr-long v20, v8, v19

    const-wide/16 v22, 0x3fff

    and-long v20, v20, v22

    move-wide/from16 v0, v20

    long-to-int v0, v0

    move/from16 v19, v0

    aput v19, p3, p4

    .line 54
    add-int/lit8 p4, v18, 0x1

    .end local v18    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v19, 0x12

    ushr-long v20, v8, v19

    const-wide/16 v22, 0x3fff

    and-long v20, v20, v22

    move-wide/from16 v0, v20

    long-to-int v0, v0

    move/from16 v19, v0

    aput v19, p3, v18

    .line 55
    add-int/lit8 v18, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v18    # "valuesOffset":I
    const/16 v19, 0x4

    ushr-long v20, v8, v19

    const-wide/16 v22, 0x3fff

    and-long v20, v20, v22

    move-wide/from16 v0, v20

    long-to-int v0, v0

    move/from16 v19, v0

    aput v19, p3, p4

    .line 56
    add-int/lit8 p2, v16, 0x1

    .end local v16    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v10, p1, v16

    .line 57
    .local v10, "block4":J
    add-int/lit8 p4, v18, 0x1

    .end local v18    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v20, 0xf

    and-long v20, v20, v8

    const/16 v19, 0xa

    shl-long v20, v20, v19

    const/16 v19, 0x36

    ushr-long v22, v10, v19

    or-long v20, v20, v22

    move-wide/from16 v0, v20

    long-to-int v0, v0

    move/from16 v19, v0

    aput v19, p3, v18

    .line 58
    add-int/lit8 v18, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v18    # "valuesOffset":I
    const/16 v19, 0x28

    ushr-long v20, v10, v19

    const-wide/16 v22, 0x3fff

    and-long v20, v20, v22

    move-wide/from16 v0, v20

    long-to-int v0, v0

    move/from16 v19, v0

    aput v19, p3, p4

    .line 59
    add-int/lit8 p4, v18, 0x1

    .end local v18    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v19, 0x1a

    ushr-long v20, v10, v19

    const-wide/16 v22, 0x3fff

    and-long v20, v20, v22

    move-wide/from16 v0, v20

    long-to-int v0, v0

    move/from16 v19, v0

    aput v19, p3, v18

    .line 60
    add-int/lit8 v18, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v18    # "valuesOffset":I
    const/16 v19, 0xc

    ushr-long v20, v10, v19

    const-wide/16 v22, 0x3fff

    and-long v20, v20, v22

    move-wide/from16 v0, v20

    long-to-int v0, v0

    move/from16 v19, v0

    aput v19, p3, p4

    .line 61
    add-int/lit8 v16, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v16    # "blocksOffset":I
    aget-wide v12, p1, p2

    .line 62
    .local v12, "block5":J
    add-int/lit8 p4, v18, 0x1

    .end local v18    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v20, 0xfff

    and-long v20, v20, v10

    const/16 v19, 0x2

    shl-long v20, v20, v19

    const/16 v19, 0x3e

    ushr-long v22, v12, v19

    or-long v20, v20, v22

    move-wide/from16 v0, v20

    long-to-int v0, v0

    move/from16 v19, v0

    aput v19, p3, v18

    .line 63
    add-int/lit8 v18, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v18    # "valuesOffset":I
    const/16 v19, 0x30

    ushr-long v20, v12, v19

    const-wide/16 v22, 0x3fff

    and-long v20, v20, v22

    move-wide/from16 v0, v20

    long-to-int v0, v0

    move/from16 v19, v0

    aput v19, p3, p4

    .line 64
    add-int/lit8 p4, v18, 0x1

    .end local v18    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v19, 0x22

    ushr-long v20, v12, v19

    const-wide/16 v22, 0x3fff

    and-long v20, v20, v22

    move-wide/from16 v0, v20

    long-to-int v0, v0

    move/from16 v19, v0

    aput v19, p3, v18

    .line 65
    add-int/lit8 v18, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v18    # "valuesOffset":I
    const/16 v19, 0x14

    ushr-long v20, v12, v19

    const-wide/16 v22, 0x3fff

    and-long v20, v20, v22

    move-wide/from16 v0, v20

    long-to-int v0, v0

    move/from16 v19, v0

    aput v19, p3, p4

    .line 66
    add-int/lit8 p4, v18, 0x1

    .end local v18    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v19, 0x6

    ushr-long v20, v12, v19

    const-wide/16 v22, 0x3fff

    and-long v20, v20, v22

    move-wide/from16 v0, v20

    long-to-int v0, v0

    move/from16 v19, v0

    aput v19, p3, v18

    .line 67
    add-int/lit8 p2, v16, 0x1

    .end local v16    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v14, p1, v16

    .line 68
    .local v14, "block6":J
    add-int/lit8 v18, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v18    # "valuesOffset":I
    const-wide/16 v20, 0x3f

    and-long v20, v20, v12

    const/16 v19, 0x8

    shl-long v20, v20, v19

    const/16 v19, 0x38

    ushr-long v22, v14, v19

    or-long v20, v20, v22

    move-wide/from16 v0, v20

    long-to-int v0, v0

    move/from16 v19, v0

    aput v19, p3, p4

    .line 69
    add-int/lit8 p4, v18, 0x1

    .end local v18    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v19, 0x2a

    ushr-long v20, v14, v19

    const-wide/16 v22, 0x3fff

    and-long v20, v20, v22

    move-wide/from16 v0, v20

    long-to-int v0, v0

    move/from16 v19, v0

    aput v19, p3, v18

    .line 70
    add-int/lit8 v18, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v18    # "valuesOffset":I
    const/16 v19, 0x1c

    ushr-long v20, v14, v19

    const-wide/16 v22, 0x3fff

    and-long v20, v20, v22

    move-wide/from16 v0, v20

    long-to-int v0, v0

    move/from16 v19, v0

    aput v19, p3, p4

    .line 71
    add-int/lit8 p4, v18, 0x1

    .end local v18    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v19, 0xe

    ushr-long v20, v14, v19

    const-wide/16 v22, 0x3fff

    and-long v20, v20, v22

    move-wide/from16 v0, v20

    long-to-int v0, v0

    move/from16 v19, v0

    aput v19, p3, v18

    .line 72
    add-int/lit8 v18, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v18    # "valuesOffset":I
    const-wide/16 v20, 0x3fff

    and-long v20, v20, v14

    move-wide/from16 v0, v20

    long-to-int v0, v0

    move/from16 v19, v0

    aput v19, p3, p4

    .line 33
    add-int/lit8 v17, v17, 0x1

    move/from16 v16, p2

    .end local p2    # "blocksOffset":I
    .restart local v16    # "blocksOffset":I
    goto/16 :goto_0
.end method

.method public decode([JI[JII)V
    .locals 24
    .param p1, "blocks"    # [J
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [J
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 95
    const/16 v17, 0x0

    .local v17, "i":I
    move/from16 v18, p4

    .end local p4    # "valuesOffset":I
    .local v18, "valuesOffset":I
    move/from16 v16, p2

    .end local p2    # "blocksOffset":I
    .local v16, "blocksOffset":I
    :goto_0
    move/from16 v0, v17

    move/from16 v1, p5

    if-lt v0, v1, :cond_0

    .line 136
    return-void

    .line 96
    :cond_0
    add-int/lit8 p2, v16, 0x1

    .end local v16    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v2, p1, v16

    .line 97
    .local v2, "block0":J
    add-int/lit8 p4, v18, 0x1

    .end local v18    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v19, 0x32

    ushr-long v20, v2, v19

    aput-wide v20, p3, v18

    .line 98
    add-int/lit8 v18, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v18    # "valuesOffset":I
    const/16 v19, 0x24

    ushr-long v20, v2, v19

    const-wide/16 v22, 0x3fff

    and-long v20, v20, v22

    aput-wide v20, p3, p4

    .line 99
    add-int/lit8 p4, v18, 0x1

    .end local v18    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v19, 0x16

    ushr-long v20, v2, v19

    const-wide/16 v22, 0x3fff

    and-long v20, v20, v22

    aput-wide v20, p3, v18

    .line 100
    add-int/lit8 v18, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v18    # "valuesOffset":I
    const/16 v19, 0x8

    ushr-long v20, v2, v19

    const-wide/16 v22, 0x3fff

    and-long v20, v20, v22

    aput-wide v20, p3, p4

    .line 101
    add-int/lit8 v16, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v16    # "blocksOffset":I
    aget-wide v4, p1, p2

    .line 102
    .local v4, "block1":J
    add-int/lit8 p4, v18, 0x1

    .end local v18    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v20, 0xff

    and-long v20, v20, v2

    const/16 v19, 0x6

    shl-long v20, v20, v19

    const/16 v19, 0x3a

    ushr-long v22, v4, v19

    or-long v20, v20, v22

    aput-wide v20, p3, v18

    .line 103
    add-int/lit8 v18, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v18    # "valuesOffset":I
    const/16 v19, 0x2c

    ushr-long v20, v4, v19

    const-wide/16 v22, 0x3fff

    and-long v20, v20, v22

    aput-wide v20, p3, p4

    .line 104
    add-int/lit8 p4, v18, 0x1

    .end local v18    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v19, 0x1e

    ushr-long v20, v4, v19

    const-wide/16 v22, 0x3fff

    and-long v20, v20, v22

    aput-wide v20, p3, v18

    .line 105
    add-int/lit8 v18, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v18    # "valuesOffset":I
    const/16 v19, 0x10

    ushr-long v20, v4, v19

    const-wide/16 v22, 0x3fff

    and-long v20, v20, v22

    aput-wide v20, p3, p4

    .line 106
    add-int/lit8 p4, v18, 0x1

    .end local v18    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v19, 0x2

    ushr-long v20, v4, v19

    const-wide/16 v22, 0x3fff

    and-long v20, v20, v22

    aput-wide v20, p3, v18

    .line 107
    add-int/lit8 p2, v16, 0x1

    .end local v16    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v6, p1, v16

    .line 108
    .local v6, "block2":J
    add-int/lit8 v18, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v18    # "valuesOffset":I
    const-wide/16 v20, 0x3

    and-long v20, v20, v4

    const/16 v19, 0xc

    shl-long v20, v20, v19

    const/16 v19, 0x34

    ushr-long v22, v6, v19

    or-long v20, v20, v22

    aput-wide v20, p3, p4

    .line 109
    add-int/lit8 p4, v18, 0x1

    .end local v18    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v19, 0x26

    ushr-long v20, v6, v19

    const-wide/16 v22, 0x3fff

    and-long v20, v20, v22

    aput-wide v20, p3, v18

    .line 110
    add-int/lit8 v18, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v18    # "valuesOffset":I
    const/16 v19, 0x18

    ushr-long v20, v6, v19

    const-wide/16 v22, 0x3fff

    and-long v20, v20, v22

    aput-wide v20, p3, p4

    .line 111
    add-int/lit8 p4, v18, 0x1

    .end local v18    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v19, 0xa

    ushr-long v20, v6, v19

    const-wide/16 v22, 0x3fff

    and-long v20, v20, v22

    aput-wide v20, p3, v18

    .line 112
    add-int/lit8 v16, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v16    # "blocksOffset":I
    aget-wide v8, p1, p2

    .line 113
    .local v8, "block3":J
    add-int/lit8 v18, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v18    # "valuesOffset":I
    const-wide/16 v20, 0x3ff

    and-long v20, v20, v6

    const/16 v19, 0x4

    shl-long v20, v20, v19

    const/16 v19, 0x3c

    ushr-long v22, v8, v19

    or-long v20, v20, v22

    aput-wide v20, p3, p4

    .line 114
    add-int/lit8 p4, v18, 0x1

    .end local v18    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v19, 0x2e

    ushr-long v20, v8, v19

    const-wide/16 v22, 0x3fff

    and-long v20, v20, v22

    aput-wide v20, p3, v18

    .line 115
    add-int/lit8 v18, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v18    # "valuesOffset":I
    const/16 v19, 0x20

    ushr-long v20, v8, v19

    const-wide/16 v22, 0x3fff

    and-long v20, v20, v22

    aput-wide v20, p3, p4

    .line 116
    add-int/lit8 p4, v18, 0x1

    .end local v18    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v19, 0x12

    ushr-long v20, v8, v19

    const-wide/16 v22, 0x3fff

    and-long v20, v20, v22

    aput-wide v20, p3, v18

    .line 117
    add-int/lit8 v18, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v18    # "valuesOffset":I
    const/16 v19, 0x4

    ushr-long v20, v8, v19

    const-wide/16 v22, 0x3fff

    and-long v20, v20, v22

    aput-wide v20, p3, p4

    .line 118
    add-int/lit8 p2, v16, 0x1

    .end local v16    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v10, p1, v16

    .line 119
    .local v10, "block4":J
    add-int/lit8 p4, v18, 0x1

    .end local v18    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v20, 0xf

    and-long v20, v20, v8

    const/16 v19, 0xa

    shl-long v20, v20, v19

    const/16 v19, 0x36

    ushr-long v22, v10, v19

    or-long v20, v20, v22

    aput-wide v20, p3, v18

    .line 120
    add-int/lit8 v18, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v18    # "valuesOffset":I
    const/16 v19, 0x28

    ushr-long v20, v10, v19

    const-wide/16 v22, 0x3fff

    and-long v20, v20, v22

    aput-wide v20, p3, p4

    .line 121
    add-int/lit8 p4, v18, 0x1

    .end local v18    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v19, 0x1a

    ushr-long v20, v10, v19

    const-wide/16 v22, 0x3fff

    and-long v20, v20, v22

    aput-wide v20, p3, v18

    .line 122
    add-int/lit8 v18, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v18    # "valuesOffset":I
    const/16 v19, 0xc

    ushr-long v20, v10, v19

    const-wide/16 v22, 0x3fff

    and-long v20, v20, v22

    aput-wide v20, p3, p4

    .line 123
    add-int/lit8 v16, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v16    # "blocksOffset":I
    aget-wide v12, p1, p2

    .line 124
    .local v12, "block5":J
    add-int/lit8 p4, v18, 0x1

    .end local v18    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v20, 0xfff

    and-long v20, v20, v10

    const/16 v19, 0x2

    shl-long v20, v20, v19

    const/16 v19, 0x3e

    ushr-long v22, v12, v19

    or-long v20, v20, v22

    aput-wide v20, p3, v18

    .line 125
    add-int/lit8 v18, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v18    # "valuesOffset":I
    const/16 v19, 0x30

    ushr-long v20, v12, v19

    const-wide/16 v22, 0x3fff

    and-long v20, v20, v22

    aput-wide v20, p3, p4

    .line 126
    add-int/lit8 p4, v18, 0x1

    .end local v18    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v19, 0x22

    ushr-long v20, v12, v19

    const-wide/16 v22, 0x3fff

    and-long v20, v20, v22

    aput-wide v20, p3, v18

    .line 127
    add-int/lit8 v18, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v18    # "valuesOffset":I
    const/16 v19, 0x14

    ushr-long v20, v12, v19

    const-wide/16 v22, 0x3fff

    and-long v20, v20, v22

    aput-wide v20, p3, p4

    .line 128
    add-int/lit8 p4, v18, 0x1

    .end local v18    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v19, 0x6

    ushr-long v20, v12, v19

    const-wide/16 v22, 0x3fff

    and-long v20, v20, v22

    aput-wide v20, p3, v18

    .line 129
    add-int/lit8 p2, v16, 0x1

    .end local v16    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v14, p1, v16

    .line 130
    .local v14, "block6":J
    add-int/lit8 v18, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v18    # "valuesOffset":I
    const-wide/16 v20, 0x3f

    and-long v20, v20, v12

    const/16 v19, 0x8

    shl-long v20, v20, v19

    const/16 v19, 0x38

    ushr-long v22, v14, v19

    or-long v20, v20, v22

    aput-wide v20, p3, p4

    .line 131
    add-int/lit8 p4, v18, 0x1

    .end local v18    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v19, 0x2a

    ushr-long v20, v14, v19

    const-wide/16 v22, 0x3fff

    and-long v20, v20, v22

    aput-wide v20, p3, v18

    .line 132
    add-int/lit8 v18, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v18    # "valuesOffset":I
    const/16 v19, 0x1c

    ushr-long v20, v14, v19

    const-wide/16 v22, 0x3fff

    and-long v20, v20, v22

    aput-wide v20, p3, p4

    .line 133
    add-int/lit8 p4, v18, 0x1

    .end local v18    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v19, 0xe

    ushr-long v20, v14, v19

    const-wide/16 v22, 0x3fff

    and-long v20, v20, v22

    aput-wide v20, p3, v18

    .line 134
    add-int/lit8 v18, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v18    # "valuesOffset":I
    const-wide/16 v20, 0x3fff

    and-long v20, v20, v14

    aput-wide v20, p3, p4

    .line 95
    add-int/lit8 v17, v17, 0x1

    move/from16 v16, p2

    .end local p2    # "blocksOffset":I
    .restart local v16    # "blocksOffset":I
    goto/16 :goto_0
.end method
