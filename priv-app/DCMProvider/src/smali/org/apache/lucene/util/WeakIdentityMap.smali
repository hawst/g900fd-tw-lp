.class public final Lorg/apache/lucene/util/WeakIdentityMap;
.super Ljava/lang/Object;
.source "WeakIdentityMap.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/WeakIdentityMap$IdentityWeakReference;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field static final NULL:Ljava/lang/Object;


# instance fields
.field private final backingStore:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lorg/apache/lucene/util/WeakIdentityMap$IdentityWeakReference;",
            "TV;>;"
        }
    .end annotation
.end field

.field private final queue:Ljava/lang/ref/ReferenceQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/ReferenceQueue",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final reapOnRead:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 64
    const-class v0, Lorg/apache/lucene/util/WeakIdentityMap;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/WeakIdentityMap;->$assertionsDisabled:Z

    .line 243
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lorg/apache/lucene/util/WeakIdentityMap;->NULL:Ljava/lang/Object;

    return-void

    .line 64
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Ljava/util/Map;Z)V
    .locals 1
    .param p2, "reapOnRead"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lorg/apache/lucene/util/WeakIdentityMap$IdentityWeakReference;",
            "TV;>;Z)V"
        }
    .end annotation

    .prologue
    .line 103
    .local p0, "this":Lorg/apache/lucene/util/WeakIdentityMap;, "Lorg/apache/lucene/util/WeakIdentityMap<TK;TV;>;"
    .local p1, "backingStore":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/util/WeakIdentityMap$IdentityWeakReference;TV;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    new-instance v0, Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v0}, Ljava/lang/ref/ReferenceQueue;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/util/WeakIdentityMap;->queue:Ljava/lang/ref/ReferenceQueue;

    .line 104
    iput-object p1, p0, Lorg/apache/lucene/util/WeakIdentityMap;->backingStore:Ljava/util/Map;

    .line 105
    iput-boolean p2, p0, Lorg/apache/lucene/util/WeakIdentityMap;->reapOnRead:Z

    .line 106
    return-void
.end method

.method public static newConcurrentHashMap()Lorg/apache/lucene/util/WeakIdentityMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">()",
            "Lorg/apache/lucene/util/WeakIdentityMap",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 91
    const/4 v0, 0x1

    invoke-static {v0}, Lorg/apache/lucene/util/WeakIdentityMap;->newConcurrentHashMap(Z)Lorg/apache/lucene/util/WeakIdentityMap;

    move-result-object v0

    return-object v0
.end method

.method public static newConcurrentHashMap(Z)Lorg/apache/lucene/util/WeakIdentityMap;
    .locals 2
    .param p0, "reapOnRead"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(Z)",
            "Lorg/apache/lucene/util/WeakIdentityMap",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 99
    new-instance v0, Lorg/apache/lucene/util/WeakIdentityMap;

    new-instance v1, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    invoke-direct {v0, v1, p0}, Lorg/apache/lucene/util/WeakIdentityMap;-><init>(Ljava/util/Map;Z)V

    return-object v0
.end method

.method public static newHashMap()Lorg/apache/lucene/util/WeakIdentityMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">()",
            "Lorg/apache/lucene/util/WeakIdentityMap",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 75
    const/4 v0, 0x1

    invoke-static {v0}, Lorg/apache/lucene/util/WeakIdentityMap;->newHashMap(Z)Lorg/apache/lucene/util/WeakIdentityMap;

    move-result-object v0

    return-object v0
.end method

.method public static newHashMap(Z)Lorg/apache/lucene/util/WeakIdentityMap;
    .locals 2
    .param p0, "reapOnRead"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(Z)",
            "Lorg/apache/lucene/util/WeakIdentityMap",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 83
    new-instance v0, Lorg/apache/lucene/util/WeakIdentityMap;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-direct {v0, v1, p0}, Lorg/apache/lucene/util/WeakIdentityMap;-><init>(Ljava/util/Map;Z)V

    return-object v0
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 110
    .local p0, "this":Lorg/apache/lucene/util/WeakIdentityMap;, "Lorg/apache/lucene/util/WeakIdentityMap<TK;TV;>;"
    iget-object v0, p0, Lorg/apache/lucene/util/WeakIdentityMap;->backingStore:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 111
    invoke-virtual {p0}, Lorg/apache/lucene/util/WeakIdentityMap;->reap()V

    .line 112
    return-void
.end method

.method public containsKey(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "key"    # Ljava/lang/Object;

    .prologue
    .line 116
    .local p0, "this":Lorg/apache/lucene/util/WeakIdentityMap;, "Lorg/apache/lucene/util/WeakIdentityMap<TK;TV;>;"
    iget-boolean v0, p0, Lorg/apache/lucene/util/WeakIdentityMap;->reapOnRead:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/util/WeakIdentityMap;->reap()V

    .line 117
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/util/WeakIdentityMap;->backingStore:Ljava/util/Map;

    new-instance v1, Lorg/apache/lucene/util/WeakIdentityMap$IdentityWeakReference;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Lorg/apache/lucene/util/WeakIdentityMap$IdentityWeakReference;-><init>(Ljava/lang/Object;Ljava/lang/ref/ReferenceQueue;)V

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1, "key"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 122
    .local p0, "this":Lorg/apache/lucene/util/WeakIdentityMap;, "Lorg/apache/lucene/util/WeakIdentityMap<TK;TV;>;"
    iget-boolean v0, p0, Lorg/apache/lucene/util/WeakIdentityMap;->reapOnRead:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/util/WeakIdentityMap;->reap()V

    .line 123
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/util/WeakIdentityMap;->backingStore:Ljava/util/Map;

    new-instance v1, Lorg/apache/lucene/util/WeakIdentityMap$IdentityWeakReference;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Lorg/apache/lucene/util/WeakIdentityMap$IdentityWeakReference;-><init>(Ljava/lang/Object;Ljava/lang/ref/ReferenceQueue;)V

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 136
    .local p0, "this":Lorg/apache/lucene/util/WeakIdentityMap;, "Lorg/apache/lucene/util/WeakIdentityMap<TK;TV;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/util/WeakIdentityMap;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public keyIterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 164
    .local p0, "this":Lorg/apache/lucene/util/WeakIdentityMap;, "Lorg/apache/lucene/util/WeakIdentityMap<TK;TV;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/util/WeakIdentityMap;->reap()V

    .line 165
    iget-object v1, p0, Lorg/apache/lucene/util/WeakIdentityMap;->backingStore:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 168
    .local v0, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/util/WeakIdentityMap$IdentityWeakReference;>;"
    new-instance v1, Lorg/apache/lucene/util/WeakIdentityMap$1;

    invoke-direct {v1, p0, v0}, Lorg/apache/lucene/util/WeakIdentityMap$1;-><init>(Lorg/apache/lucene/util/WeakIdentityMap;Ljava/util/Iterator;)V

    return-object v1
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    .prologue
    .line 130
    .local p0, "this":Lorg/apache/lucene/util/WeakIdentityMap;, "Lorg/apache/lucene/util/WeakIdentityMap<TK;TV;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    .local p2, "value":Ljava/lang/Object;, "TV;"
    invoke-virtual {p0}, Lorg/apache/lucene/util/WeakIdentityMap;->reap()V

    .line 131
    iget-object v0, p0, Lorg/apache/lucene/util/WeakIdentityMap;->backingStore:Ljava/util/Map;

    new-instance v1, Lorg/apache/lucene/util/WeakIdentityMap$IdentityWeakReference;

    iget-object v2, p0, Lorg/apache/lucene/util/WeakIdentityMap;->queue:Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v1, p1, v2}, Lorg/apache/lucene/util/WeakIdentityMap$IdentityWeakReference;-><init>(Ljava/lang/Object;Ljava/lang/ref/ReferenceQueue;)V

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public reap()V
    .locals 2

    .prologue
    .line 237
    .local p0, "this":Lorg/apache/lucene/util/WeakIdentityMap;, "Lorg/apache/lucene/util/WeakIdentityMap<TK;TV;>;"
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/util/WeakIdentityMap;->queue:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v1}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    .local v0, "zombie":Ljava/lang/ref/Reference;, "Ljava/lang/ref/Reference<*>;"
    if-nez v0, :cond_0

    .line 240
    return-void

    .line 238
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/util/WeakIdentityMap;->backingStore:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1, "key"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 145
    .local p0, "this":Lorg/apache/lucene/util/WeakIdentityMap;, "Lorg/apache/lucene/util/WeakIdentityMap<TK;TV;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/util/WeakIdentityMap;->reap()V

    .line 146
    iget-object v0, p0, Lorg/apache/lucene/util/WeakIdentityMap;->backingStore:Ljava/util/Map;

    new-instance v1, Lorg/apache/lucene/util/WeakIdentityMap$IdentityWeakReference;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Lorg/apache/lucene/util/WeakIdentityMap$IdentityWeakReference;-><init>(Ljava/lang/Object;Ljava/lang/ref/ReferenceQueue;)V

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 154
    .local p0, "this":Lorg/apache/lucene/util/WeakIdentityMap;, "Lorg/apache/lucene/util/WeakIdentityMap<TK;TV;>;"
    iget-object v0, p0, Lorg/apache/lucene/util/WeakIdentityMap;->backingStore:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 155
    const/4 v0, 0x0

    .line 157
    :goto_0
    return v0

    .line 156
    :cond_0
    iget-boolean v0, p0, Lorg/apache/lucene/util/WeakIdentityMap;->reapOnRead:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lorg/apache/lucene/util/WeakIdentityMap;->reap()V

    .line 157
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/util/WeakIdentityMap;->backingStore:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    goto :goto_0
.end method

.method public valueIterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 224
    .local p0, "this":Lorg/apache/lucene/util/WeakIdentityMap;, "Lorg/apache/lucene/util/WeakIdentityMap<TK;TV;>;"
    iget-boolean v0, p0, Lorg/apache/lucene/util/WeakIdentityMap;->reapOnRead:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/util/WeakIdentityMap;->reap()V

    .line 225
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/util/WeakIdentityMap;->backingStore:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method
