.class public Lorg/apache/lucene/util/automaton/State;
.super Ljava/lang/Object;
.source "State.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/automaton/State$TransitionsIterable;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lorg/apache/lucene/util/automaton/State;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field static next_id:I


# instance fields
.field accept:Z

.field id:I

.field public numTransitions:I

.field number:I

.field public transitionsArray:[Lorg/apache/lucene/util/automaton/Transition;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-class v0, Lorg/apache/lucene/util/automaton/State;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/automaton/State;->$assertionsDisabled:Z

    .line 52
    return-void

    .line 43
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/State;->resetTransitions()V

    .line 59
    sget v0, Lorg/apache/lucene/util/automaton/State;->next_id:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lorg/apache/lucene/util/automaton/State;->next_id:I

    iput v0, p0, Lorg/apache/lucene/util/automaton/State;->id:I

    .line 60
    return-void
.end method


# virtual methods
.method addEpsilon(Lorg/apache/lucene/util/automaton/State;)V
    .locals 3
    .param p1, "to"    # Lorg/apache/lucene/util/automaton/State;

    .prologue
    .line 177
    iget-boolean v1, p1, Lorg/apache/lucene/util/automaton/State;->accept:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/apache/lucene/util/automaton/State;->accept:Z

    .line 178
    :cond_0
    invoke-virtual {p1}, Lorg/apache/lucene/util/automaton/State;->getTransitions()Ljava/lang/Iterable;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 180
    return-void

    .line 178
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/util/automaton/Transition;

    .line 179
    .local v0, "t":Lorg/apache/lucene/util/automaton/Transition;
    invoke-virtual {p0, v0}, Lorg/apache/lucene/util/automaton/State;->addTransition(Lorg/apache/lucene/util/automaton/Transition;)V

    goto :goto_0
.end method

.method public addTransition(Lorg/apache/lucene/util/automaton/Transition;)V
    .locals 4
    .param p1, "t"    # Lorg/apache/lucene/util/automaton/Transition;

    .prologue
    const/4 v3, 0x0

    .line 116
    iget v1, p0, Lorg/apache/lucene/util/automaton/State;->numTransitions:I

    iget-object v2, p0, Lorg/apache/lucene/util/automaton/State;->transitionsArray:[Lorg/apache/lucene/util/automaton/Transition;

    array-length v2, v2

    if-ne v1, v2, :cond_0

    .line 117
    iget v1, p0, Lorg/apache/lucene/util/automaton/State;->numTransitions:I

    add-int/lit8 v1, v1, 0x1

    sget v2, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_REF:I

    invoke-static {v1, v2}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v1

    new-array v0, v1, [Lorg/apache/lucene/util/automaton/Transition;

    .line 118
    .local v0, "newArray":[Lorg/apache/lucene/util/automaton/Transition;
    iget-object v1, p0, Lorg/apache/lucene/util/automaton/State;->transitionsArray:[Lorg/apache/lucene/util/automaton/Transition;

    iget v2, p0, Lorg/apache/lucene/util/automaton/State;->numTransitions:I

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 119
    iput-object v0, p0, Lorg/apache/lucene/util/automaton/State;->transitionsArray:[Lorg/apache/lucene/util/automaton/Transition;

    .line 121
    .end local v0    # "newArray":[Lorg/apache/lucene/util/automaton/Transition;
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/util/automaton/State;->transitionsArray:[Lorg/apache/lucene/util/automaton/Transition;

    iget v2, p0, Lorg/apache/lucene/util/automaton/State;->numTransitions:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lorg/apache/lucene/util/automaton/State;->numTransitions:I

    aput-object p1, v1, v2

    .line 122
    return-void
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/util/automaton/State;

    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/automaton/State;->compareTo(Lorg/apache/lucene/util/automaton/State;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lorg/apache/lucene/util/automaton/State;)I
    .locals 2
    .param p1, "s"    # Lorg/apache/lucene/util/automaton/State;

    .prologue
    .line 278
    iget v0, p1, Lorg/apache/lucene/util/automaton/State;->id:I

    iget v1, p0, Lorg/apache/lucene/util/automaton/State;->id:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public getNumber()I
    .locals 1

    .prologue
    .line 253
    iget v0, p0, Lorg/apache/lucene/util/automaton/State;->number:I

    return v0
.end method

.method public getTransitions()Ljava/lang/Iterable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "Lorg/apache/lucene/util/automaton/Transition;",
            ">;"
        }
    .end annotation

    .prologue
    .line 98
    new-instance v0, Lorg/apache/lucene/util/automaton/State$TransitionsIterable;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/apache/lucene/util/automaton/State$TransitionsIterable;-><init>(Lorg/apache/lucene/util/automaton/State;Lorg/apache/lucene/util/automaton/State$TransitionsIterable;)V

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 283
    iget v0, p0, Lorg/apache/lucene/util/automaton/State;->id:I

    return v0
.end method

.method public isAccept()Z
    .locals 1

    .prologue
    .line 139
    iget-boolean v0, p0, Lorg/apache/lucene/util/automaton/State;->accept:Z

    return v0
.end method

.method public numTransitions()I
    .locals 1

    .prologue
    .line 102
    iget v0, p0, Lorg/apache/lucene/util/automaton/State;->numTransitions:I

    return v0
.end method

.method public reduce()V
    .locals 9

    .prologue
    .line 196
    iget v7, p0, Lorg/apache/lucene/util/automaton/State;->numTransitions:I

    const/4 v8, 0x1

    if-gt v7, v8, :cond_0

    .line 229
    :goto_0
    return-void

    .line 199
    :cond_0
    sget-object v7, Lorg/apache/lucene/util/automaton/Transition;->CompareByDestThenMinMax:Ljava/util/Comparator;

    invoke-virtual {p0, v7}, Lorg/apache/lucene/util/automaton/State;->sortTransitions(Ljava/util/Comparator;)V

    .line 200
    const/4 v3, 0x0

    .line 201
    .local v3, "p":Lorg/apache/lucene/util/automaton/State;
    const/4 v2, -0x1

    .local v2, "min":I
    const/4 v1, -0x1

    .line 202
    .local v1, "max":I
    const/4 v5, 0x0

    .line 203
    .local v5, "upto":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget v7, p0, Lorg/apache/lucene/util/automaton/State;->numTransitions:I

    if-lt v0, v7, :cond_2

    .line 225
    if-eqz v3, :cond_1

    .line 226
    iget-object v7, p0, Lorg/apache/lucene/util/automaton/State;->transitionsArray:[Lorg/apache/lucene/util/automaton/Transition;

    add-int/lit8 v6, v5, 0x1

    .end local v5    # "upto":I
    .local v6, "upto":I
    new-instance v8, Lorg/apache/lucene/util/automaton/Transition;

    invoke-direct {v8, v2, v1, v3}, Lorg/apache/lucene/util/automaton/Transition;-><init>(IILorg/apache/lucene/util/automaton/State;)V

    aput-object v8, v7, v5

    move v5, v6

    .line 228
    .end local v6    # "upto":I
    .restart local v5    # "upto":I
    :cond_1
    iput v5, p0, Lorg/apache/lucene/util/automaton/State;->numTransitions:I

    goto :goto_0

    .line 204
    :cond_2
    iget-object v7, p0, Lorg/apache/lucene/util/automaton/State;->transitionsArray:[Lorg/apache/lucene/util/automaton/Transition;

    aget-object v4, v7, v0

    .line 205
    .local v4, "t":Lorg/apache/lucene/util/automaton/Transition;
    iget-object v7, v4, Lorg/apache/lucene/util/automaton/Transition;->to:Lorg/apache/lucene/util/automaton/State;

    if-ne v3, v7, :cond_6

    .line 206
    iget v7, v4, Lorg/apache/lucene/util/automaton/Transition;->min:I

    add-int/lit8 v8, v1, 0x1

    if-gt v7, v8, :cond_4

    .line 207
    iget v7, v4, Lorg/apache/lucene/util/automaton/Transition;->max:I

    if-le v7, v1, :cond_3

    iget v1, v4, Lorg/apache/lucene/util/automaton/Transition;->max:I

    .line 203
    :cond_3
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 209
    :cond_4
    if-eqz v3, :cond_5

    .line 210
    iget-object v7, p0, Lorg/apache/lucene/util/automaton/State;->transitionsArray:[Lorg/apache/lucene/util/automaton/Transition;

    add-int/lit8 v6, v5, 0x1

    .end local v5    # "upto":I
    .restart local v6    # "upto":I
    new-instance v8, Lorg/apache/lucene/util/automaton/Transition;

    invoke-direct {v8, v2, v1, v3}, Lorg/apache/lucene/util/automaton/Transition;-><init>(IILorg/apache/lucene/util/automaton/State;)V

    aput-object v8, v7, v5

    move v5, v6

    .line 212
    .end local v6    # "upto":I
    .restart local v5    # "upto":I
    :cond_5
    iget v2, v4, Lorg/apache/lucene/util/automaton/Transition;->min:I

    .line 213
    iget v1, v4, Lorg/apache/lucene/util/automaton/Transition;->max:I

    .line 215
    goto :goto_2

    .line 216
    :cond_6
    if-eqz v3, :cond_7

    .line 217
    iget-object v7, p0, Lorg/apache/lucene/util/automaton/State;->transitionsArray:[Lorg/apache/lucene/util/automaton/Transition;

    add-int/lit8 v6, v5, 0x1

    .end local v5    # "upto":I
    .restart local v6    # "upto":I
    new-instance v8, Lorg/apache/lucene/util/automaton/Transition;

    invoke-direct {v8, v2, v1, v3}, Lorg/apache/lucene/util/automaton/Transition;-><init>(IILorg/apache/lucene/util/automaton/State;)V

    aput-object v8, v7, v5

    move v5, v6

    .line 219
    .end local v6    # "upto":I
    .restart local v5    # "upto":I
    :cond_7
    iget-object v3, v4, Lorg/apache/lucene/util/automaton/Transition;->to:Lorg/apache/lucene/util/automaton/State;

    .line 220
    iget v2, v4, Lorg/apache/lucene/util/automaton/Transition;->min:I

    .line 221
    iget v1, v4, Lorg/apache/lucene/util/automaton/Transition;->max:I

    goto :goto_2
.end method

.method final resetTransitions()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 66
    new-array v0, v1, [Lorg/apache/lucene/util/automaton/Transition;

    iput-object v0, p0, Lorg/apache/lucene/util/automaton/State;->transitionsArray:[Lorg/apache/lucene/util/automaton/Transition;

    .line 67
    iput v1, p0, Lorg/apache/lucene/util/automaton/State;->numTransitions:I

    .line 68
    return-void
.end method

.method public setAccept(Z)V
    .locals 0
    .param p1, "accept"    # Z

    .prologue
    .line 130
    iput-boolean p1, p0, Lorg/apache/lucene/util/automaton/State;->accept:Z

    .line 131
    return-void
.end method

.method public setTransitions([Lorg/apache/lucene/util/automaton/Transition;)V
    .locals 1
    .param p1, "transitions"    # [Lorg/apache/lucene/util/automaton/Transition;

    .prologue
    .line 106
    array-length v0, p1

    iput v0, p0, Lorg/apache/lucene/util/automaton/State;->numTransitions:I

    .line 107
    iput-object p1, p0, Lorg/apache/lucene/util/automaton/State;->transitionsArray:[Lorg/apache/lucene/util/automaton/Transition;

    .line 108
    return-void
.end method

.method public sortTransitions(Ljava/util/Comparator;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/automaton/Transition;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 242
    .local p1, "comparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Lorg/apache/lucene/util/automaton/Transition;>;"
    iget v0, p0, Lorg/apache/lucene/util/automaton/State;->numTransitions:I

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/util/automaton/State;->transitionsArray:[Lorg/apache/lucene/util/automaton/Transition;

    const/4 v1, 0x0

    iget v2, p0, Lorg/apache/lucene/util/automaton/State;->numTransitions:I

    invoke-static {v0, v1, v2, p1}, Lorg/apache/lucene/util/ArrayUtil;->mergeSort([Ljava/lang/Object;IILjava/util/Comparator;)V

    .line 243
    :cond_0
    return-void
.end method

.method public step(I)Lorg/apache/lucene/util/automaton/State;
    .locals 3
    .param p1, "c"    # I

    .prologue
    .line 150
    sget-boolean v2, Lorg/apache/lucene/util/automaton/State;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    if-gez p1, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 151
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lorg/apache/lucene/util/automaton/State;->numTransitions:I

    if-lt v0, v2, :cond_1

    .line 155
    const/4 v2, 0x0

    :goto_1
    return-object v2

    .line 152
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/util/automaton/State;->transitionsArray:[Lorg/apache/lucene/util/automaton/Transition;

    aget-object v1, v2, v0

    .line 153
    .local v1, "t":Lorg/apache/lucene/util/automaton/Transition;
    iget v2, v1, Lorg/apache/lucene/util/automaton/Transition;->min:I

    if-gt v2, p1, :cond_2

    iget v2, v1, Lorg/apache/lucene/util/automaton/Transition;->max:I

    if-gt p1, v2, :cond_2

    iget-object v2, v1, Lorg/apache/lucene/util/automaton/Transition;->to:Lorg/apache/lucene/util/automaton/State;

    goto :goto_1

    .line 151
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public step(ILjava/util/Collection;)V
    .locals 3
    .param p1, "c"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/util/automaton/State;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 166
    .local p2, "dest":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/util/automaton/State;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lorg/apache/lucene/util/automaton/State;->numTransitions:I

    if-lt v0, v2, :cond_0

    .line 170
    return-void

    .line 167
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/util/automaton/State;->transitionsArray:[Lorg/apache/lucene/util/automaton/Transition;

    aget-object v1, v2, v0

    .line 168
    .local v1, "t":Lorg/apache/lucene/util/automaton/Transition;
    iget v2, v1, Lorg/apache/lucene/util/automaton/Transition;->min:I

    if-gt v2, p1, :cond_1

    iget v2, v1, Lorg/apache/lucene/util/automaton/Transition;->max:I

    if-gt p1, v2, :cond_1

    iget-object v2, v1, Lorg/apache/lucene/util/automaton/Transition;->to:Lorg/apache/lucene/util/automaton/State;

    invoke-interface {p2, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 166
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 262
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 263
    .local v0, "b":Ljava/lang/StringBuilder;
    const-string v2, "state "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lorg/apache/lucene/util/automaton/State;->number:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 264
    iget-boolean v2, p0, Lorg/apache/lucene/util/automaton/State;->accept:Z

    if-eqz v2, :cond_0

    const-string v2, " [accept]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 266
    :goto_0
    const-string v2, ":\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 267
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/State;->getTransitions()Ljava/lang/Iterable;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 269
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 265
    :cond_0
    const-string v2, " [reject]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 267
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/util/automaton/Transition;

    .line 268
    .local v1, "t":Lorg/apache/lucene/util/automaton/Transition;
    const-string v3, "  "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lorg/apache/lucene/util/automaton/Transition;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method public trimTransitionsArray()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 184
    iget v1, p0, Lorg/apache/lucene/util/automaton/State;->numTransitions:I

    iget-object v2, p0, Lorg/apache/lucene/util/automaton/State;->transitionsArray:[Lorg/apache/lucene/util/automaton/Transition;

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 185
    iget v1, p0, Lorg/apache/lucene/util/automaton/State;->numTransitions:I

    new-array v0, v1, [Lorg/apache/lucene/util/automaton/Transition;

    .line 186
    .local v0, "newArray":[Lorg/apache/lucene/util/automaton/Transition;
    iget-object v1, p0, Lorg/apache/lucene/util/automaton/State;->transitionsArray:[Lorg/apache/lucene/util/automaton/Transition;

    iget v2, p0, Lorg/apache/lucene/util/automaton/State;->numTransitions:I

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 187
    iput-object v0, p0, Lorg/apache/lucene/util/automaton/State;->transitionsArray:[Lorg/apache/lucene/util/automaton/Transition;

    .line 189
    .end local v0    # "newArray":[Lorg/apache/lucene/util/automaton/Transition;
    :cond_0
    return-void
.end method
