.class final Lorg/apache/lucene/util/packed/BulkOperationPacked16;
.super Lorg/apache/lucene/util/packed/BulkOperationPacked;
.source "BulkOperationPacked16.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lorg/apache/lucene/util/packed/BulkOperationPacked;-><init>(I)V

    .line 29
    return-void
.end method


# virtual methods
.method public decode([BI[III)V
    .locals 5
    .param p1, "blocks"    # [B
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [I
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 43
    const/4 v1, 0x0

    .local v1, "j":I
    move v2, p4

    .end local p4    # "valuesOffset":I
    .local v2, "valuesOffset":I
    move v0, p2

    .end local p2    # "blocksOffset":I
    .local v0, "blocksOffset":I
    :goto_0
    if-lt v1, p5, :cond_0

    .line 46
    return-void

    .line 44
    :cond_0
    add-int/lit8 p4, v2, 0x1

    .end local v2    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    add-int/lit8 p2, v0, 0x1

    .end local v0    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v3, p1, v0

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x8

    add-int/lit8 v0, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v0    # "blocksOffset":I
    aget-byte v4, p1, p2

    and-int/lit16 v4, v4, 0xff

    or-int/2addr v3, v4

    aput v3, p3, v2

    .line 43
    add-int/lit8 v1, v1, 0x1

    move v2, p4

    .end local p4    # "valuesOffset":I
    .restart local v2    # "valuesOffset":I
    goto :goto_0
.end method

.method public decode([BI[JII)V
    .locals 10
    .param p1, "blocks"    # [B
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [J
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    const-wide/16 v8, 0xff

    .line 60
    const/4 v1, 0x0

    .local v1, "j":I
    move v2, p4

    .end local p4    # "valuesOffset":I
    .local v2, "valuesOffset":I
    move v0, p2

    .end local p2    # "blocksOffset":I
    .local v0, "blocksOffset":I
    :goto_0
    if-lt v1, p5, :cond_0

    .line 63
    return-void

    .line 61
    :cond_0
    add-int/lit8 p4, v2, 0x1

    .end local v2    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    add-int/lit8 p2, v0, 0x1

    .end local v0    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v3, p1, v0

    int-to-long v4, v3

    and-long/2addr v4, v8

    const/16 v3, 0x8

    shl-long/2addr v4, v3

    add-int/lit8 v0, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v0    # "blocksOffset":I
    aget-byte v3, p1, p2

    int-to-long v6, v3

    and-long/2addr v6, v8

    or-long/2addr v4, v6

    aput-wide v4, p3, v2

    .line 60
    add-int/lit8 v1, v1, 0x1

    move v2, p4

    .end local p4    # "valuesOffset":I
    .restart local v2    # "valuesOffset":I
    goto :goto_0
.end method

.method public decode([JI[III)V
    .locals 10
    .param p1, "blocks"    # [J
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [I
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 33
    const/4 v3, 0x0

    .local v3, "i":I
    move v2, p2

    .end local p2    # "blocksOffset":I
    .local v2, "blocksOffset":I
    :goto_0
    if-lt v3, p5, :cond_0

    .line 39
    return-void

    .line 34
    :cond_0
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v0, p1, v2

    .line 35
    .local v0, "block":J
    const/16 v4, 0x30

    .local v4, "shift":I
    move v5, p4

    .end local p4    # "valuesOffset":I
    .local v5, "valuesOffset":I
    :goto_1
    if-gez v4, :cond_1

    .line 33
    add-int/lit8 v3, v3, 0x1

    move p4, v5

    .end local v5    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    move v2, p2

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    goto :goto_0

    .line 36
    .end local v2    # "blocksOffset":I
    .end local p4    # "valuesOffset":I
    .restart local v5    # "valuesOffset":I
    .restart local p2    # "blocksOffset":I
    :cond_1
    add-int/lit8 p4, v5, 0x1

    .end local v5    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    ushr-long v6, v0, v4

    const-wide/32 v8, 0xffff

    and-long/2addr v6, v8

    long-to-int v6, v6

    aput v6, p3, v5

    .line 35
    add-int/lit8 v4, v4, -0x10

    move v5, p4

    .end local p4    # "valuesOffset":I
    .restart local v5    # "valuesOffset":I
    goto :goto_1
.end method

.method public decode([JI[JII)V
    .locals 10
    .param p1, "blocks"    # [J
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [J
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 50
    const/4 v3, 0x0

    .local v3, "i":I
    move v2, p2

    .end local p2    # "blocksOffset":I
    .local v2, "blocksOffset":I
    :goto_0
    if-lt v3, p5, :cond_0

    .line 56
    return-void

    .line 51
    :cond_0
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v0, p1, v2

    .line 52
    .local v0, "block":J
    const/16 v4, 0x30

    .local v4, "shift":I
    move v5, p4

    .end local p4    # "valuesOffset":I
    .local v5, "valuesOffset":I
    :goto_1
    if-gez v4, :cond_1

    .line 50
    add-int/lit8 v3, v3, 0x1

    move p4, v5

    .end local v5    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    move v2, p2

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    goto :goto_0

    .line 53
    .end local v2    # "blocksOffset":I
    .end local p4    # "valuesOffset":I
    .restart local v5    # "valuesOffset":I
    .restart local p2    # "blocksOffset":I
    :cond_1
    add-int/lit8 p4, v5, 0x1

    .end local v5    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    ushr-long v6, v0, v4

    const-wide/32 v8, 0xffff

    and-long/2addr v6, v8

    aput-wide v6, p3, v5

    .line 52
    add-int/lit8 v4, v4, -0x10

    move v5, p4

    .end local p4    # "valuesOffset":I
    .restart local v5    # "valuesOffset":I
    goto :goto_1
.end method
