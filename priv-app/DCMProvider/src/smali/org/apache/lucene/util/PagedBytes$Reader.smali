.class public final Lorg/apache/lucene/util/PagedBytes$Reader;
.super Ljava/lang/Object;
.source "PagedBytes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/PagedBytes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Reader"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final blockBits:I

.field private final blockEnds:[I

.field private final blockMask:I

.field private final blockSize:I

.field private final blocks:[[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    const-class v0, Lorg/apache/lucene/util/PagedBytes;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/PagedBytes$Reader;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lorg/apache/lucene/util/PagedBytes;)V
    .locals 3
    .param p1, "pagedBytes"    # Lorg/apache/lucene/util/PagedBytes;

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    # getter for: Lorg/apache/lucene/util/PagedBytes;->blocks:Ljava/util/List;
    invoke-static {p1}, Lorg/apache/lucene/util/PagedBytes;->access$0(Lorg/apache/lucene/util/PagedBytes;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [[B

    iput-object v1, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blocks:[[B

    .line 63
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blocks:[[B

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 66
    iget-object v1, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blocks:[[B

    array-length v1, v1

    new-array v1, v1, [I

    iput-object v1, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blockEnds:[I

    .line 67
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blockEnds:[I

    array-length v1, v1

    if-lt v0, v1, :cond_1

    .line 70
    # getter for: Lorg/apache/lucene/util/PagedBytes;->blockBits:I
    invoke-static {p1}, Lorg/apache/lucene/util/PagedBytes;->access$2(Lorg/apache/lucene/util/PagedBytes;)I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blockBits:I

    .line 71
    # getter for: Lorg/apache/lucene/util/PagedBytes;->blockMask:I
    invoke-static {p1}, Lorg/apache/lucene/util/PagedBytes;->access$3(Lorg/apache/lucene/util/PagedBytes;)I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blockMask:I

    .line 72
    # getter for: Lorg/apache/lucene/util/PagedBytes;->blockSize:I
    invoke-static {p1}, Lorg/apache/lucene/util/PagedBytes;->access$4(Lorg/apache/lucene/util/PagedBytes;)I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blockSize:I

    .line 73
    return-void

    .line 64
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blocks:[[B

    # getter for: Lorg/apache/lucene/util/PagedBytes;->blocks:Ljava/util/List;
    invoke-static {p1}, Lorg/apache/lucene/util/PagedBytes;->access$0(Lorg/apache/lucene/util/PagedBytes;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    aput-object v1, v2, v0

    .line 63
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 68
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blockEnds:[I

    # getter for: Lorg/apache/lucene/util/PagedBytes;->blockEnd:Ljava/util/List;
    invoke-static {p1}, Lorg/apache/lucene/util/PagedBytes;->access$1(Lorg/apache/lucene/util/PagedBytes;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    aput v1, v2, v0

    .line 67
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/util/PagedBytes;Lorg/apache/lucene/util/PagedBytes$Reader;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lorg/apache/lucene/util/PagedBytes$Reader;-><init>(Lorg/apache/lucene/util/PagedBytes;)V

    return-void
.end method


# virtual methods
.method public fill(Lorg/apache/lucene/util/BytesRef;J)V
    .locals 6
    .param p1, "b"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "start"    # J

    .prologue
    .line 114
    iget v3, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blockBits:I

    shr-long v4, p2, v3

    long-to-int v1, v4

    .line 115
    .local v1, "index":I
    iget v3, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blockMask:I

    int-to-long v4, v3

    and-long/2addr v4, p2

    long-to-int v2, v4

    .line 116
    .local v2, "offset":I
    iget-object v3, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blocks:[[B

    aget-object v0, v3, v1

    iput-object v0, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 118
    .local v0, "block":[B
    aget-byte v3, v0, v2

    and-int/lit16 v3, v3, 0x80

    if-nez v3, :cond_1

    .line 119
    aget-byte v3, v0, v2

    iput v3, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 120
    add-int/lit8 v3, v2, 0x1

    iput v3, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 126
    :cond_0
    return-void

    .line 122
    :cond_1
    aget-byte v3, v0, v2

    and-int/lit8 v3, v3, 0x7f

    shl-int/lit8 v3, v3, 0x8

    add-int/lit8 v4, v2, 0x1

    aget-byte v4, v0, v4

    and-int/lit16 v4, v4, 0xff

    or-int/2addr v3, v4

    iput v3, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 123
    add-int/lit8 v3, v2, 0x2

    iput v3, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 124
    sget-boolean v3, Lorg/apache/lucene/util/PagedBytes$Reader;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    iget v3, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    if-gtz v3, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3
.end method

.method public fillSlice(Lorg/apache/lucene/util/BytesRef;JI)V
    .locals 8
    .param p1, "b"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "start"    # J
    .param p4, "length"    # I

    .prologue
    const/4 v6, 0x0

    .line 85
    sget-boolean v2, Lorg/apache/lucene/util/PagedBytes$Reader;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    if-gez p4, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "length="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 86
    :cond_0
    sget-boolean v2, Lorg/apache/lucene/util/PagedBytes$Reader;->$assertionsDisabled:Z

    if-nez v2, :cond_1

    iget v2, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blockSize:I

    add-int/lit8 v2, v2, 0x1

    if-le p4, v2, :cond_1

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 87
    :cond_1
    iget v2, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blockBits:I

    shr-long v2, p2, v2

    long-to-int v0, v2

    .line 88
    .local v0, "index":I
    iget v2, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blockMask:I

    int-to-long v2, v2

    and-long/2addr v2, p2

    long-to-int v1, v2

    .line 89
    .local v1, "offset":I
    iput p4, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 90
    iget v2, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blockSize:I

    sub-int/2addr v2, v1

    if-lt v2, p4, :cond_2

    .line 92
    iget-object v2, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blocks:[[B

    aget-object v2, v2, v0

    iput-object v2, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 93
    iput v1, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 101
    :goto_0
    return-void

    .line 96
    :cond_2
    new-array v2, p4, [B

    iput-object v2, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 97
    iput v6, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 98
    iget-object v2, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blocks:[[B

    aget-object v2, v2, v0

    iget-object v3, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v4, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blockSize:I

    sub-int/2addr v4, v1

    invoke-static {v2, v1, v3, v6, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 99
    iget-object v2, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blocks:[[B

    add-int/lit8 v3, v0, 0x1

    aget-object v2, v2, v3

    iget-object v3, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v4, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blockSize:I

    sub-int/2addr v4, v1

    iget v5, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blockSize:I

    sub-int/2addr v5, v1

    sub-int v5, p4, v5

    invoke-static {v2, v6, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method
