.class Lorg/apache/lucene/util/fst/Util$TieBreakByInputComparator;
.super Ljava/lang/Object;
.source "Util.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/fst/Util;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TieBreakByInputComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lorg/apache/lucene/util/fst/Util$FSTPath",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field private final comparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Comparator;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Comparator",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 264
    .local p0, "this":Lorg/apache/lucene/util/fst/Util$TieBreakByInputComparator;, "Lorg/apache/lucene/util/fst/Util$TieBreakByInputComparator<TT;>;"
    .local p1, "comparator":Ljava/util/Comparator;, "Ljava/util/Comparator<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 265
    iput-object p1, p0, Lorg/apache/lucene/util/fst/Util$TieBreakByInputComparator;->comparator:Ljava/util/Comparator;

    .line 266
    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/util/fst/Util$FSTPath;

    check-cast p2, Lorg/apache/lucene/util/fst/Util$FSTPath;

    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/util/fst/Util$TieBreakByInputComparator;->compare(Lorg/apache/lucene/util/fst/Util$FSTPath;Lorg/apache/lucene/util/fst/Util$FSTPath;)I

    move-result v0

    return v0
.end method

.method public compare(Lorg/apache/lucene/util/fst/Util$FSTPath;Lorg/apache/lucene/util/fst/Util$FSTPath;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/Util$FSTPath",
            "<TT;>;",
            "Lorg/apache/lucene/util/fst/Util$FSTPath",
            "<TT;>;)I"
        }
    .end annotation

    .prologue
    .line 270
    .local p0, "this":Lorg/apache/lucene/util/fst/Util$TieBreakByInputComparator;, "Lorg/apache/lucene/util/fst/Util$TieBreakByInputComparator<TT;>;"
    .local p1, "a":Lorg/apache/lucene/util/fst/Util$FSTPath;, "Lorg/apache/lucene/util/fst/Util$FSTPath<TT;>;"
    .local p2, "b":Lorg/apache/lucene/util/fst/Util$FSTPath;, "Lorg/apache/lucene/util/fst/Util$FSTPath<TT;>;"
    iget-object v1, p0, Lorg/apache/lucene/util/fst/Util$TieBreakByInputComparator;->comparator:Ljava/util/Comparator;

    iget-object v2, p1, Lorg/apache/lucene/util/fst/Util$FSTPath;->cost:Ljava/lang/Object;

    iget-object v3, p2, Lorg/apache/lucene/util/fst/Util$FSTPath;->cost:Ljava/lang/Object;

    invoke-interface {v1, v2, v3}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    .line 271
    .local v0, "cmp":I
    if-nez v0, :cond_0

    .line 272
    iget-object v1, p1, Lorg/apache/lucene/util/fst/Util$FSTPath;->input:Lorg/apache/lucene/util/IntsRef;

    iget-object v2, p2, Lorg/apache/lucene/util/fst/Util$FSTPath;->input:Lorg/apache/lucene/util/IntsRef;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/IntsRef;->compareTo(Lorg/apache/lucene/util/IntsRef;)I

    move-result v0

    .line 274
    .end local v0    # "cmp":I
    :cond_0
    return v0
.end method
