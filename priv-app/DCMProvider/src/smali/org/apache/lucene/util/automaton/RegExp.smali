.class public Lorg/apache/lucene/util/automaton/RegExp;
.super Ljava/lang/Object;
.source "RegExp.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/automaton/RegExp$Kind;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$org$apache$lucene$util$automaton$RegExp$Kind:[I = null

.field public static final ALL:I = 0xffff

.field public static final ANYSTRING:I = 0x8

.field public static final AUTOMATON:I = 0x10

.field public static final COMPLEMENT:I = 0x2

.field public static final EMPTY:I = 0x4

.field public static final INTERSECTION:I = 0x1

.field public static final INTERVAL:I = 0x20

.field public static final NONE:I

.field private static allow_mutation:Z


# instance fields
.field b:Ljava/lang/String;

.field c:I

.field digits:I

.field exp1:Lorg/apache/lucene/util/automaton/RegExp;

.field exp2:Lorg/apache/lucene/util/automaton/RegExp;

.field flags:I

.field from:I

.field kind:Lorg/apache/lucene/util/automaton/RegExp$Kind;

.field max:I

.field min:I

.field pos:I

.field s:Ljava/lang/String;

.field to:I


# direct methods
.method static synthetic $SWITCH_TABLE$org$apache$lucene$util$automaton$RegExp$Kind()[I
    .locals 3

    .prologue
    .line 317
    sget-object v0, Lorg/apache/lucene/util/automaton/RegExp;->$SWITCH_TABLE$org$apache$lucene$util$automaton$RegExp$Kind:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lorg/apache/lucene/util/automaton/RegExp$Kind;->values()[Lorg/apache/lucene/util/automaton/RegExp$Kind;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_ANYCHAR:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    invoke-virtual {v1}, Lorg/apache/lucene/util/automaton/RegExp$Kind;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_f

    :goto_1
    :try_start_1
    sget-object v1, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_ANYSTRING:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    invoke-virtual {v1}, Lorg/apache/lucene/util/automaton/RegExp$Kind;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_e

    :goto_2
    :try_start_2
    sget-object v1, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_AUTOMATON:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    invoke-virtual {v1}, Lorg/apache/lucene/util/automaton/RegExp$Kind;->ordinal()I

    move-result v1

    const/16 v2, 0xf

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_d

    :goto_3
    :try_start_3
    sget-object v1, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_CHAR:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    invoke-virtual {v1}, Lorg/apache/lucene/util/automaton/RegExp$Kind;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_c

    :goto_4
    :try_start_4
    sget-object v1, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_CHAR_RANGE:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    invoke-virtual {v1}, Lorg/apache/lucene/util/automaton/RegExp$Kind;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_b

    :goto_5
    :try_start_5
    sget-object v1, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_COMPLEMENT:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    invoke-virtual {v1}, Lorg/apache/lucene/util/automaton/RegExp$Kind;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_a

    :goto_6
    :try_start_6
    sget-object v1, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_CONCATENATION:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    invoke-virtual {v1}, Lorg/apache/lucene/util/automaton/RegExp$Kind;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_9

    :goto_7
    :try_start_7
    sget-object v1, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_EMPTY:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    invoke-virtual {v1}, Lorg/apache/lucene/util/automaton/RegExp$Kind;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_8

    :goto_8
    :try_start_8
    sget-object v1, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_INTERSECTION:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    invoke-virtual {v1}, Lorg/apache/lucene/util/automaton/RegExp$Kind;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_7

    :goto_9
    :try_start_9
    sget-object v1, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_INTERVAL:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    invoke-virtual {v1}, Lorg/apache/lucene/util/automaton/RegExp$Kind;->ordinal()I

    move-result v1

    const/16 v2, 0x10

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_6

    :goto_a
    :try_start_a
    sget-object v1, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_OPTIONAL:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    invoke-virtual {v1}, Lorg/apache/lucene/util/automaton/RegExp$Kind;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_5

    :goto_b
    :try_start_b
    sget-object v1, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_REPEAT:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    invoke-virtual {v1}, Lorg/apache/lucene/util/automaton/RegExp$Kind;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_4

    :goto_c
    :try_start_c
    sget-object v1, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_REPEAT_MIN:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    invoke-virtual {v1}, Lorg/apache/lucene/util/automaton/RegExp$Kind;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_3

    :goto_d
    :try_start_d
    sget-object v1, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_REPEAT_MINMAX:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    invoke-virtual {v1}, Lorg/apache/lucene/util/automaton/RegExp$Kind;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_2

    :goto_e
    :try_start_e
    sget-object v1, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_STRING:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    invoke-virtual {v1}, Lorg/apache/lucene/util/automaton/RegExp$Kind;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_1

    :goto_f
    :try_start_f
    sget-object v1, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_UNION:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    invoke-virtual {v1}, Lorg/apache/lucene/util/automaton/RegExp$Kind;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_0

    :goto_10
    sput-object v0, Lorg/apache/lucene/util/automaton/RegExp;->$SWITCH_TABLE$org$apache$lucene$util$automaton$RegExp$Kind:[I

    goto/16 :goto_0

    :catch_0
    move-exception v1

    goto :goto_10

    :catch_1
    move-exception v1

    goto :goto_f

    :catch_2
    move-exception v1

    goto :goto_e

    :catch_3
    move-exception v1

    goto :goto_d

    :catch_4
    move-exception v1

    goto :goto_c

    :catch_5
    move-exception v1

    goto :goto_b

    :catch_6
    move-exception v1

    goto :goto_a

    :catch_7
    move-exception v1

    goto :goto_9

    :catch_8
    move-exception v1

    goto :goto_8

    :catch_9
    move-exception v1

    goto :goto_7

    :catch_a
    move-exception v1

    goto :goto_6

    :catch_b
    move-exception v1

    goto/16 :goto_5

    :catch_c
    move-exception v1

    goto/16 :goto_4

    :catch_d
    move-exception v1

    goto/16 :goto_3

    :catch_e
    move-exception v1

    goto/16 :goto_2

    :catch_f
    move-exception v1

    goto/16 :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 364
    const/4 v0, 0x0

    sput-boolean v0, Lorg/apache/lucene/util/automaton/RegExp;->allow_mutation:Z

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 377
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "s"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 388
    const v0, 0xffff

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/util/automaton/RegExp;-><init>(Ljava/lang/String;I)V

    .line 389
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 4
    .param p1, "s"    # Ljava/lang/String;
    .param p2, "syntax_flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 400
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 401
    iput-object p1, p0, Lorg/apache/lucene/util/automaton/RegExp;->b:Ljava/lang/String;

    .line 402
    iput p2, p0, Lorg/apache/lucene/util/automaton/RegExp;->flags:I

    .line 404
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ""

    invoke-static {v1}, Lorg/apache/lucene/util/automaton/RegExp;->makeString(Ljava/lang/String;)Lorg/apache/lucene/util/automaton/RegExp;

    move-result-object v0

    .line 410
    .local v0, "e":Lorg/apache/lucene/util/automaton/RegExp;
    :cond_0
    iget-object v1, v0, Lorg/apache/lucene/util/automaton/RegExp;->kind:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    iput-object v1, p0, Lorg/apache/lucene/util/automaton/RegExp;->kind:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    .line 411
    iget-object v1, v0, Lorg/apache/lucene/util/automaton/RegExp;->exp1:Lorg/apache/lucene/util/automaton/RegExp;

    iput-object v1, p0, Lorg/apache/lucene/util/automaton/RegExp;->exp1:Lorg/apache/lucene/util/automaton/RegExp;

    .line 412
    iget-object v1, v0, Lorg/apache/lucene/util/automaton/RegExp;->exp2:Lorg/apache/lucene/util/automaton/RegExp;

    iput-object v1, p0, Lorg/apache/lucene/util/automaton/RegExp;->exp2:Lorg/apache/lucene/util/automaton/RegExp;

    .line 413
    iget-object v1, v0, Lorg/apache/lucene/util/automaton/RegExp;->s:Ljava/lang/String;

    iput-object v1, p0, Lorg/apache/lucene/util/automaton/RegExp;->s:Ljava/lang/String;

    .line 414
    iget v1, v0, Lorg/apache/lucene/util/automaton/RegExp;->c:I

    iput v1, p0, Lorg/apache/lucene/util/automaton/RegExp;->c:I

    .line 415
    iget v1, v0, Lorg/apache/lucene/util/automaton/RegExp;->min:I

    iput v1, p0, Lorg/apache/lucene/util/automaton/RegExp;->min:I

    .line 416
    iget v1, v0, Lorg/apache/lucene/util/automaton/RegExp;->max:I

    iput v1, p0, Lorg/apache/lucene/util/automaton/RegExp;->max:I

    .line 417
    iget v1, v0, Lorg/apache/lucene/util/automaton/RegExp;->digits:I

    iput v1, p0, Lorg/apache/lucene/util/automaton/RegExp;->digits:I

    .line 418
    iget v1, v0, Lorg/apache/lucene/util/automaton/RegExp;->from:I

    iput v1, p0, Lorg/apache/lucene/util/automaton/RegExp;->from:I

    .line 419
    iget v1, v0, Lorg/apache/lucene/util/automaton/RegExp;->to:I

    iput v1, p0, Lorg/apache/lucene/util/automaton/RegExp;->to:I

    .line 420
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/lucene/util/automaton/RegExp;->b:Ljava/lang/String;

    .line 421
    return-void

    .line 406
    .end local v0    # "e":Lorg/apache/lucene/util/automaton/RegExp;
    :cond_1
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/RegExp;->parseUnionExp()Lorg/apache/lucene/util/automaton/RegExp;

    move-result-object v0

    .line 407
    .restart local v0    # "e":Lorg/apache/lucene/util/automaton/RegExp;
    iget v1, p0, Lorg/apache/lucene/util/automaton/RegExp;->pos:I

    iget-object v2, p0, Lorg/apache/lucene/util/automaton/RegExp;->b:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    .line 408
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "end-of-string expected at position "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lorg/apache/lucene/util/automaton/RegExp;->pos:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private check(I)Z
    .locals 1
    .param p1, "flag"    # I

    .prologue
    .line 863
    iget v0, p0, Lorg/apache/lucene/util/automaton/RegExp;->flags:I

    and-int/2addr v0, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private findLeaves(Lorg/apache/lucene/util/automaton/RegExp;Lorg/apache/lucene/util/automaton/RegExp$Kind;Ljava/util/List;Ljava/util/Map;Lorg/apache/lucene/util/automaton/AutomatonProvider;)V
    .locals 6
    .param p1, "exp"    # Lorg/apache/lucene/util/automaton/RegExp;
    .param p2, "kind"    # Lorg/apache/lucene/util/automaton/RegExp$Kind;
    .param p5, "automaton_provider"    # Lorg/apache/lucene/util/automaton/AutomatonProvider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/automaton/RegExp;",
            "Lorg/apache/lucene/util/automaton/RegExp$Kind;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/util/automaton/Automaton;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/util/automaton/Automaton;",
            ">;",
            "Lorg/apache/lucene/util/automaton/AutomatonProvider;",
            ")V"
        }
    .end annotation

    .prologue
    .line 568
    .local p3, "list":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/util/automaton/Automaton;>;"
    .local p4, "automata":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lorg/apache/lucene/util/automaton/Automaton;>;"
    iget-object v0, p1, Lorg/apache/lucene/util/automaton/RegExp;->kind:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    if-ne v0, p2, :cond_0

    .line 569
    iget-object v1, p1, Lorg/apache/lucene/util/automaton/RegExp;->exp1:Lorg/apache/lucene/util/automaton/RegExp;

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/util/automaton/RegExp;->findLeaves(Lorg/apache/lucene/util/automaton/RegExp;Lorg/apache/lucene/util/automaton/RegExp$Kind;Ljava/util/List;Ljava/util/Map;Lorg/apache/lucene/util/automaton/AutomatonProvider;)V

    .line 570
    iget-object v1, p1, Lorg/apache/lucene/util/automaton/RegExp;->exp2:Lorg/apache/lucene/util/automaton/RegExp;

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/util/automaton/RegExp;->findLeaves(Lorg/apache/lucene/util/automaton/RegExp;Lorg/apache/lucene/util/automaton/RegExp$Kind;Ljava/util/List;Ljava/util/Map;Lorg/apache/lucene/util/automaton/AutomatonProvider;)V

    .line 572
    :goto_0
    return-void

    .line 571
    :cond_0
    invoke-direct {p1, p4, p5}, Lorg/apache/lucene/util/automaton/RegExp;->toAutomaton(Ljava/util/Map;Lorg/apache/lucene/util/automaton/AutomatonProvider;)Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v0

    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method static makeAnyChar()Lorg/apache/lucene/util/automaton/RegExp;
    .locals 2

    .prologue
    .line 798
    new-instance v0, Lorg/apache/lucene/util/automaton/RegExp;

    invoke-direct {v0}, Lorg/apache/lucene/util/automaton/RegExp;-><init>()V

    .line 799
    .local v0, "r":Lorg/apache/lucene/util/automaton/RegExp;
    sget-object v1, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_ANYCHAR:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    iput-object v1, v0, Lorg/apache/lucene/util/automaton/RegExp;->kind:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    .line 800
    return-object v0
.end method

.method static makeAnyString()Lorg/apache/lucene/util/automaton/RegExp;
    .locals 2

    .prologue
    .line 817
    new-instance v0, Lorg/apache/lucene/util/automaton/RegExp;

    invoke-direct {v0}, Lorg/apache/lucene/util/automaton/RegExp;-><init>()V

    .line 818
    .local v0, "r":Lorg/apache/lucene/util/automaton/RegExp;
    sget-object v1, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_ANYSTRING:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    iput-object v1, v0, Lorg/apache/lucene/util/automaton/RegExp;->kind:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    .line 819
    return-object v0
.end method

.method static makeAutomaton(Ljava/lang/String;)Lorg/apache/lucene/util/automaton/RegExp;
    .locals 2
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 823
    new-instance v0, Lorg/apache/lucene/util/automaton/RegExp;

    invoke-direct {v0}, Lorg/apache/lucene/util/automaton/RegExp;-><init>()V

    .line 824
    .local v0, "r":Lorg/apache/lucene/util/automaton/RegExp;
    sget-object v1, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_AUTOMATON:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    iput-object v1, v0, Lorg/apache/lucene/util/automaton/RegExp;->kind:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    .line 825
    iput-object p0, v0, Lorg/apache/lucene/util/automaton/RegExp;->s:Ljava/lang/String;

    .line 826
    return-object v0
.end method

.method static makeChar(I)Lorg/apache/lucene/util/automaton/RegExp;
    .locals 2
    .param p0, "c"    # I

    .prologue
    .line 781
    new-instance v0, Lorg/apache/lucene/util/automaton/RegExp;

    invoke-direct {v0}, Lorg/apache/lucene/util/automaton/RegExp;-><init>()V

    .line 782
    .local v0, "r":Lorg/apache/lucene/util/automaton/RegExp;
    sget-object v1, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_CHAR:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    iput-object v1, v0, Lorg/apache/lucene/util/automaton/RegExp;->kind:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    .line 783
    iput p0, v0, Lorg/apache/lucene/util/automaton/RegExp;->c:I

    .line 784
    return-object v0
.end method

.method static makeCharRange(II)Lorg/apache/lucene/util/automaton/RegExp;
    .locals 4
    .param p0, "from"    # I
    .param p1, "to"    # I

    .prologue
    .line 788
    if-le p0, p1, :cond_0

    .line 789
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "invalid range: from ("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") cannot be > to ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 790
    :cond_0
    new-instance v0, Lorg/apache/lucene/util/automaton/RegExp;

    invoke-direct {v0}, Lorg/apache/lucene/util/automaton/RegExp;-><init>()V

    .line 791
    .local v0, "r":Lorg/apache/lucene/util/automaton/RegExp;
    sget-object v1, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_CHAR_RANGE:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    iput-object v1, v0, Lorg/apache/lucene/util/automaton/RegExp;->kind:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    .line 792
    iput p0, v0, Lorg/apache/lucene/util/automaton/RegExp;->from:I

    .line 793
    iput p1, v0, Lorg/apache/lucene/util/automaton/RegExp;->to:I

    .line 794
    return-object v0
.end method

.method static makeComplement(Lorg/apache/lucene/util/automaton/RegExp;)Lorg/apache/lucene/util/automaton/RegExp;
    .locals 2
    .param p0, "exp"    # Lorg/apache/lucene/util/automaton/RegExp;

    .prologue
    .line 774
    new-instance v0, Lorg/apache/lucene/util/automaton/RegExp;

    invoke-direct {v0}, Lorg/apache/lucene/util/automaton/RegExp;-><init>()V

    .line 775
    .local v0, "r":Lorg/apache/lucene/util/automaton/RegExp;
    sget-object v1, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_COMPLEMENT:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    iput-object v1, v0, Lorg/apache/lucene/util/automaton/RegExp;->kind:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    .line 776
    iput-object p0, v0, Lorg/apache/lucene/util/automaton/RegExp;->exp1:Lorg/apache/lucene/util/automaton/RegExp;

    .line 777
    return-object v0
.end method

.method static makeConcatenation(Lorg/apache/lucene/util/automaton/RegExp;Lorg/apache/lucene/util/automaton/RegExp;)Lorg/apache/lucene/util/automaton/RegExp;
    .locals 3
    .param p0, "exp1"    # Lorg/apache/lucene/util/automaton/RegExp;
    .param p1, "exp2"    # Lorg/apache/lucene/util/automaton/RegExp;

    .prologue
    .line 703
    iget-object v1, p0, Lorg/apache/lucene/util/automaton/RegExp;->kind:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    sget-object v2, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_CHAR:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/util/automaton/RegExp;->kind:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    sget-object v2, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_STRING:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    if-ne v1, v2, :cond_2

    .line 704
    :cond_0
    iget-object v1, p1, Lorg/apache/lucene/util/automaton/RegExp;->kind:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    sget-object v2, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_CHAR:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    if-eq v1, v2, :cond_1

    iget-object v1, p1, Lorg/apache/lucene/util/automaton/RegExp;->kind:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    sget-object v2, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_STRING:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    if-ne v1, v2, :cond_2

    :cond_1
    invoke-static {p0, p1}, Lorg/apache/lucene/util/automaton/RegExp;->makeString(Lorg/apache/lucene/util/automaton/RegExp;Lorg/apache/lucene/util/automaton/RegExp;)Lorg/apache/lucene/util/automaton/RegExp;

    move-result-object v0

    .line 722
    :goto_0
    return-object v0

    .line 706
    :cond_2
    new-instance v0, Lorg/apache/lucene/util/automaton/RegExp;

    invoke-direct {v0}, Lorg/apache/lucene/util/automaton/RegExp;-><init>()V

    .line 707
    .local v0, "r":Lorg/apache/lucene/util/automaton/RegExp;
    sget-object v1, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_CONCATENATION:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    iput-object v1, v0, Lorg/apache/lucene/util/automaton/RegExp;->kind:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    .line 708
    iget-object v1, p0, Lorg/apache/lucene/util/automaton/RegExp;->kind:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    sget-object v2, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_CONCATENATION:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    if-ne v1, v2, :cond_5

    .line 709
    iget-object v1, p0, Lorg/apache/lucene/util/automaton/RegExp;->exp2:Lorg/apache/lucene/util/automaton/RegExp;

    iget-object v1, v1, Lorg/apache/lucene/util/automaton/RegExp;->kind:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    sget-object v2, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_CHAR:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    if-eq v1, v2, :cond_3

    iget-object v1, p0, Lorg/apache/lucene/util/automaton/RegExp;->exp2:Lorg/apache/lucene/util/automaton/RegExp;

    iget-object v1, v1, Lorg/apache/lucene/util/automaton/RegExp;->kind:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    sget-object v2, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_STRING:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    if-ne v1, v2, :cond_5

    .line 710
    :cond_3
    iget-object v1, p1, Lorg/apache/lucene/util/automaton/RegExp;->kind:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    sget-object v2, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_CHAR:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    if-eq v1, v2, :cond_4

    iget-object v1, p1, Lorg/apache/lucene/util/automaton/RegExp;->kind:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    sget-object v2, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_STRING:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    if-ne v1, v2, :cond_5

    .line 711
    :cond_4
    iget-object v1, p0, Lorg/apache/lucene/util/automaton/RegExp;->exp1:Lorg/apache/lucene/util/automaton/RegExp;

    iput-object v1, v0, Lorg/apache/lucene/util/automaton/RegExp;->exp1:Lorg/apache/lucene/util/automaton/RegExp;

    .line 712
    iget-object v1, p0, Lorg/apache/lucene/util/automaton/RegExp;->exp2:Lorg/apache/lucene/util/automaton/RegExp;

    invoke-static {v1, p1}, Lorg/apache/lucene/util/automaton/RegExp;->makeString(Lorg/apache/lucene/util/automaton/RegExp;Lorg/apache/lucene/util/automaton/RegExp;)Lorg/apache/lucene/util/automaton/RegExp;

    move-result-object v1

    iput-object v1, v0, Lorg/apache/lucene/util/automaton/RegExp;->exp2:Lorg/apache/lucene/util/automaton/RegExp;

    goto :goto_0

    .line 713
    :cond_5
    iget-object v1, p0, Lorg/apache/lucene/util/automaton/RegExp;->kind:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    sget-object v2, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_CHAR:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    if-eq v1, v2, :cond_6

    iget-object v1, p0, Lorg/apache/lucene/util/automaton/RegExp;->kind:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    sget-object v2, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_STRING:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    if-ne v1, v2, :cond_8

    .line 714
    :cond_6
    iget-object v1, p1, Lorg/apache/lucene/util/automaton/RegExp;->kind:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    sget-object v2, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_CONCATENATION:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    if-ne v1, v2, :cond_8

    .line 715
    iget-object v1, p1, Lorg/apache/lucene/util/automaton/RegExp;->exp1:Lorg/apache/lucene/util/automaton/RegExp;

    iget-object v1, v1, Lorg/apache/lucene/util/automaton/RegExp;->kind:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    sget-object v2, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_CHAR:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    if-eq v1, v2, :cond_7

    iget-object v1, p1, Lorg/apache/lucene/util/automaton/RegExp;->exp1:Lorg/apache/lucene/util/automaton/RegExp;

    iget-object v1, v1, Lorg/apache/lucene/util/automaton/RegExp;->kind:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    sget-object v2, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_STRING:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    if-ne v1, v2, :cond_8

    .line 716
    :cond_7
    iget-object v1, p1, Lorg/apache/lucene/util/automaton/RegExp;->exp1:Lorg/apache/lucene/util/automaton/RegExp;

    invoke-static {p0, v1}, Lorg/apache/lucene/util/automaton/RegExp;->makeString(Lorg/apache/lucene/util/automaton/RegExp;Lorg/apache/lucene/util/automaton/RegExp;)Lorg/apache/lucene/util/automaton/RegExp;

    move-result-object v1

    iput-object v1, v0, Lorg/apache/lucene/util/automaton/RegExp;->exp1:Lorg/apache/lucene/util/automaton/RegExp;

    .line 717
    iget-object v1, p1, Lorg/apache/lucene/util/automaton/RegExp;->exp2:Lorg/apache/lucene/util/automaton/RegExp;

    iput-object v1, v0, Lorg/apache/lucene/util/automaton/RegExp;->exp2:Lorg/apache/lucene/util/automaton/RegExp;

    goto :goto_0

    .line 719
    :cond_8
    iput-object p0, v0, Lorg/apache/lucene/util/automaton/RegExp;->exp1:Lorg/apache/lucene/util/automaton/RegExp;

    .line 720
    iput-object p1, v0, Lorg/apache/lucene/util/automaton/RegExp;->exp2:Lorg/apache/lucene/util/automaton/RegExp;

    goto :goto_0
.end method

.method static makeEmpty()Lorg/apache/lucene/util/automaton/RegExp;
    .locals 2

    .prologue
    .line 804
    new-instance v0, Lorg/apache/lucene/util/automaton/RegExp;

    invoke-direct {v0}, Lorg/apache/lucene/util/automaton/RegExp;-><init>()V

    .line 805
    .local v0, "r":Lorg/apache/lucene/util/automaton/RegExp;
    sget-object v1, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_EMPTY:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    iput-object v1, v0, Lorg/apache/lucene/util/automaton/RegExp;->kind:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    .line 806
    return-object v0
.end method

.method static makeIntersection(Lorg/apache/lucene/util/automaton/RegExp;Lorg/apache/lucene/util/automaton/RegExp;)Lorg/apache/lucene/util/automaton/RegExp;
    .locals 2
    .param p0, "exp1"    # Lorg/apache/lucene/util/automaton/RegExp;
    .param p1, "exp2"    # Lorg/apache/lucene/util/automaton/RegExp;

    .prologue
    .line 735
    new-instance v0, Lorg/apache/lucene/util/automaton/RegExp;

    invoke-direct {v0}, Lorg/apache/lucene/util/automaton/RegExp;-><init>()V

    .line 736
    .local v0, "r":Lorg/apache/lucene/util/automaton/RegExp;
    sget-object v1, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_INTERSECTION:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    iput-object v1, v0, Lorg/apache/lucene/util/automaton/RegExp;->kind:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    .line 737
    iput-object p0, v0, Lorg/apache/lucene/util/automaton/RegExp;->exp1:Lorg/apache/lucene/util/automaton/RegExp;

    .line 738
    iput-object p1, v0, Lorg/apache/lucene/util/automaton/RegExp;->exp2:Lorg/apache/lucene/util/automaton/RegExp;

    .line 739
    return-object v0
.end method

.method static makeInterval(III)Lorg/apache/lucene/util/automaton/RegExp;
    .locals 2
    .param p0, "min"    # I
    .param p1, "max"    # I
    .param p2, "digits"    # I

    .prologue
    .line 830
    new-instance v0, Lorg/apache/lucene/util/automaton/RegExp;

    invoke-direct {v0}, Lorg/apache/lucene/util/automaton/RegExp;-><init>()V

    .line 831
    .local v0, "r":Lorg/apache/lucene/util/automaton/RegExp;
    sget-object v1, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_INTERVAL:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    iput-object v1, v0, Lorg/apache/lucene/util/automaton/RegExp;->kind:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    .line 832
    iput p0, v0, Lorg/apache/lucene/util/automaton/RegExp;->min:I

    .line 833
    iput p1, v0, Lorg/apache/lucene/util/automaton/RegExp;->max:I

    .line 834
    iput p2, v0, Lorg/apache/lucene/util/automaton/RegExp;->digits:I

    .line 835
    return-object v0
.end method

.method static makeOptional(Lorg/apache/lucene/util/automaton/RegExp;)Lorg/apache/lucene/util/automaton/RegExp;
    .locals 2
    .param p0, "exp"    # Lorg/apache/lucene/util/automaton/RegExp;

    .prologue
    .line 743
    new-instance v0, Lorg/apache/lucene/util/automaton/RegExp;

    invoke-direct {v0}, Lorg/apache/lucene/util/automaton/RegExp;-><init>()V

    .line 744
    .local v0, "r":Lorg/apache/lucene/util/automaton/RegExp;
    sget-object v1, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_OPTIONAL:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    iput-object v1, v0, Lorg/apache/lucene/util/automaton/RegExp;->kind:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    .line 745
    iput-object p0, v0, Lorg/apache/lucene/util/automaton/RegExp;->exp1:Lorg/apache/lucene/util/automaton/RegExp;

    .line 746
    return-object v0
.end method

.method static makeRepeat(Lorg/apache/lucene/util/automaton/RegExp;)Lorg/apache/lucene/util/automaton/RegExp;
    .locals 2
    .param p0, "exp"    # Lorg/apache/lucene/util/automaton/RegExp;

    .prologue
    .line 750
    new-instance v0, Lorg/apache/lucene/util/automaton/RegExp;

    invoke-direct {v0}, Lorg/apache/lucene/util/automaton/RegExp;-><init>()V

    .line 751
    .local v0, "r":Lorg/apache/lucene/util/automaton/RegExp;
    sget-object v1, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_REPEAT:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    iput-object v1, v0, Lorg/apache/lucene/util/automaton/RegExp;->kind:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    .line 752
    iput-object p0, v0, Lorg/apache/lucene/util/automaton/RegExp;->exp1:Lorg/apache/lucene/util/automaton/RegExp;

    .line 753
    return-object v0
.end method

.method static makeRepeat(Lorg/apache/lucene/util/automaton/RegExp;I)Lorg/apache/lucene/util/automaton/RegExp;
    .locals 2
    .param p0, "exp"    # Lorg/apache/lucene/util/automaton/RegExp;
    .param p1, "min"    # I

    .prologue
    .line 757
    new-instance v0, Lorg/apache/lucene/util/automaton/RegExp;

    invoke-direct {v0}, Lorg/apache/lucene/util/automaton/RegExp;-><init>()V

    .line 758
    .local v0, "r":Lorg/apache/lucene/util/automaton/RegExp;
    sget-object v1, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_REPEAT_MIN:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    iput-object v1, v0, Lorg/apache/lucene/util/automaton/RegExp;->kind:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    .line 759
    iput-object p0, v0, Lorg/apache/lucene/util/automaton/RegExp;->exp1:Lorg/apache/lucene/util/automaton/RegExp;

    .line 760
    iput p1, v0, Lorg/apache/lucene/util/automaton/RegExp;->min:I

    .line 761
    return-object v0
.end method

.method static makeRepeat(Lorg/apache/lucene/util/automaton/RegExp;II)Lorg/apache/lucene/util/automaton/RegExp;
    .locals 2
    .param p0, "exp"    # Lorg/apache/lucene/util/automaton/RegExp;
    .param p1, "min"    # I
    .param p2, "max"    # I

    .prologue
    .line 765
    new-instance v0, Lorg/apache/lucene/util/automaton/RegExp;

    invoke-direct {v0}, Lorg/apache/lucene/util/automaton/RegExp;-><init>()V

    .line 766
    .local v0, "r":Lorg/apache/lucene/util/automaton/RegExp;
    sget-object v1, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_REPEAT_MINMAX:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    iput-object v1, v0, Lorg/apache/lucene/util/automaton/RegExp;->kind:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    .line 767
    iput-object p0, v0, Lorg/apache/lucene/util/automaton/RegExp;->exp1:Lorg/apache/lucene/util/automaton/RegExp;

    .line 768
    iput p1, v0, Lorg/apache/lucene/util/automaton/RegExp;->min:I

    .line 769
    iput p2, v0, Lorg/apache/lucene/util/automaton/RegExp;->max:I

    .line 770
    return-object v0
.end method

.method static makeString(Ljava/lang/String;)Lorg/apache/lucene/util/automaton/RegExp;
    .locals 2
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 810
    new-instance v0, Lorg/apache/lucene/util/automaton/RegExp;

    invoke-direct {v0}, Lorg/apache/lucene/util/automaton/RegExp;-><init>()V

    .line 811
    .local v0, "r":Lorg/apache/lucene/util/automaton/RegExp;
    sget-object v1, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_STRING:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    iput-object v1, v0, Lorg/apache/lucene/util/automaton/RegExp;->kind:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    .line 812
    iput-object p0, v0, Lorg/apache/lucene/util/automaton/RegExp;->s:Ljava/lang/String;

    .line 813
    return-object v0
.end method

.method private static makeString(Lorg/apache/lucene/util/automaton/RegExp;Lorg/apache/lucene/util/automaton/RegExp;)Lorg/apache/lucene/util/automaton/RegExp;
    .locals 3
    .param p0, "exp1"    # Lorg/apache/lucene/util/automaton/RegExp;
    .param p1, "exp2"    # Lorg/apache/lucene/util/automaton/RegExp;

    .prologue
    .line 726
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 727
    .local v0, "b":Ljava/lang/StringBuilder;
    iget-object v1, p0, Lorg/apache/lucene/util/automaton/RegExp;->kind:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    sget-object v2, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_STRING:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/util/automaton/RegExp;->s:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 729
    :goto_0
    iget-object v1, p1, Lorg/apache/lucene/util/automaton/RegExp;->kind:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    sget-object v2, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_STRING:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    if-ne v1, v2, :cond_1

    iget-object v1, p1, Lorg/apache/lucene/util/automaton/RegExp;->s:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 731
    :goto_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/apache/lucene/util/automaton/RegExp;->makeString(Ljava/lang/String;)Lorg/apache/lucene/util/automaton/RegExp;

    move-result-object v1

    return-object v1

    .line 728
    :cond_0
    iget v1, p0, Lorg/apache/lucene/util/automaton/RegExp;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->appendCodePoint(I)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 730
    :cond_1
    iget v1, p1, Lorg/apache/lucene/util/automaton/RegExp;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->appendCodePoint(I)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method static makeUnion(Lorg/apache/lucene/util/automaton/RegExp;Lorg/apache/lucene/util/automaton/RegExp;)Lorg/apache/lucene/util/automaton/RegExp;
    .locals 2
    .param p0, "exp1"    # Lorg/apache/lucene/util/automaton/RegExp;
    .param p1, "exp2"    # Lorg/apache/lucene/util/automaton/RegExp;

    .prologue
    .line 695
    new-instance v0, Lorg/apache/lucene/util/automaton/RegExp;

    invoke-direct {v0}, Lorg/apache/lucene/util/automaton/RegExp;-><init>()V

    .line 696
    .local v0, "r":Lorg/apache/lucene/util/automaton/RegExp;
    sget-object v1, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_UNION:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    iput-object v1, v0, Lorg/apache/lucene/util/automaton/RegExp;->kind:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    .line 697
    iput-object p0, v0, Lorg/apache/lucene/util/automaton/RegExp;->exp1:Lorg/apache/lucene/util/automaton/RegExp;

    .line 698
    iput-object p1, v0, Lorg/apache/lucene/util/automaton/RegExp;->exp2:Lorg/apache/lucene/util/automaton/RegExp;

    .line 699
    return-object v0
.end method

.method private match(I)Z
    .locals 3
    .param p1, "c"    # I

    .prologue
    const/4 v0, 0x0

    .line 843
    iget v1, p0, Lorg/apache/lucene/util/automaton/RegExp;->pos:I

    iget-object v2, p0, Lorg/apache/lucene/util/automaton/RegExp;->b:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lt v1, v2, :cond_1

    .line 848
    :cond_0
    :goto_0
    return v0

    .line 844
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/util/automaton/RegExp;->b:Ljava/lang/String;

    iget v2, p0, Lorg/apache/lucene/util/automaton/RegExp;->pos:I

    invoke-virtual {v1, v2}, Ljava/lang/String;->codePointAt(I)I

    move-result v1

    if-ne v1, p1, :cond_0

    .line 845
    iget v0, p0, Lorg/apache/lucene/util/automaton/RegExp;->pos:I

    invoke-static {p1}, Ljava/lang/Character;->charCount(I)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lorg/apache/lucene/util/automaton/RegExp;->pos:I

    .line 846
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private more()Z
    .locals 2

    .prologue
    .line 852
    iget v0, p0, Lorg/apache/lucene/util/automaton/RegExp;->pos:I

    iget-object v1, p0, Lorg/apache/lucene/util/automaton/RegExp;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private next()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 856
    invoke-direct {p0}, Lorg/apache/lucene/util/automaton/RegExp;->more()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "unexpected end-of-string"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 857
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/util/automaton/RegExp;->b:Ljava/lang/String;

    iget v2, p0, Lorg/apache/lucene/util/automaton/RegExp;->pos:I

    invoke-virtual {v1, v2}, Ljava/lang/String;->codePointAt(I)I

    move-result v0

    .line 858
    .local v0, "ch":I
    iget v1, p0, Lorg/apache/lucene/util/automaton/RegExp;->pos:I

    invoke-static {v0}, Ljava/lang/Character;->charCount(I)I

    move-result v2

    add-int/2addr v1, v2

    iput v1, p0, Lorg/apache/lucene/util/automaton/RegExp;->pos:I

    .line 859
    return v0
.end method

.method private peek(Ljava/lang/String;)Z
    .locals 2
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 839
    invoke-direct {p0}, Lorg/apache/lucene/util/automaton/RegExp;->more()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/util/automaton/RegExp;->b:Ljava/lang/String;

    iget v1, p0, Lorg/apache/lucene/util/automaton/RegExp;->pos:I

    invoke-virtual {v0, v1}, Ljava/lang/String;->codePointAt(I)I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private toAutomaton(Ljava/util/Map;Lorg/apache/lucene/util/automaton/AutomatonProvider;)Lorg/apache/lucene/util/automaton/Automaton;
    .locals 9
    .param p2, "automaton_provider"    # Lorg/apache/lucene/util/automaton/AutomatonProvider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/util/automaton/Automaton;",
            ">;",
            "Lorg/apache/lucene/util/automaton/AutomatonProvider;",
            ")",
            "Lorg/apache/lucene/util/automaton/Automaton;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 486
    .local p1, "automata":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lorg/apache/lucene/util/automaton/Automaton;>;"
    const/4 v6, 0x0

    .line 487
    .local v6, "a":Lorg/apache/lucene/util/automaton/Automaton;
    invoke-static {}, Lorg/apache/lucene/util/automaton/RegExp;->$SWITCH_TABLE$org$apache$lucene$util$automaton$RegExp$Kind()[I

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/util/automaton/RegExp;->kind:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    invoke-virtual {v1}, Lorg/apache/lucene/util/automaton/RegExp$Kind;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 563
    :goto_0
    return-object v6

    .line 489
    :pswitch_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 490
    .local v3, "list":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/util/automaton/Automaton;>;"
    iget-object v1, p0, Lorg/apache/lucene/util/automaton/RegExp;->exp1:Lorg/apache/lucene/util/automaton/RegExp;

    sget-object v2, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_UNION:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    move-object v0, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/util/automaton/RegExp;->findLeaves(Lorg/apache/lucene/util/automaton/RegExp;Lorg/apache/lucene/util/automaton/RegExp$Kind;Ljava/util/List;Ljava/util/Map;Lorg/apache/lucene/util/automaton/AutomatonProvider;)V

    .line 491
    iget-object v1, p0, Lorg/apache/lucene/util/automaton/RegExp;->exp2:Lorg/apache/lucene/util/automaton/RegExp;

    sget-object v2, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_UNION:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    move-object v0, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/util/automaton/RegExp;->findLeaves(Lorg/apache/lucene/util/automaton/RegExp;Lorg/apache/lucene/util/automaton/RegExp$Kind;Ljava/util/List;Ljava/util/Map;Lorg/apache/lucene/util/automaton/AutomatonProvider;)V

    .line 492
    invoke-static {v3}, Lorg/apache/lucene/util/automaton/BasicOperations;->union(Ljava/util/Collection;)Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v6

    .line 493
    invoke-static {v6}, Lorg/apache/lucene/util/automaton/MinimizationOperations;->minimize(Lorg/apache/lucene/util/automaton/Automaton;)V

    goto :goto_0

    .line 496
    .end local v3    # "list":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/util/automaton/Automaton;>;"
    :pswitch_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 497
    .restart local v3    # "list":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/util/automaton/Automaton;>;"
    iget-object v1, p0, Lorg/apache/lucene/util/automaton/RegExp;->exp1:Lorg/apache/lucene/util/automaton/RegExp;

    sget-object v2, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_CONCATENATION:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    move-object v0, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/util/automaton/RegExp;->findLeaves(Lorg/apache/lucene/util/automaton/RegExp;Lorg/apache/lucene/util/automaton/RegExp$Kind;Ljava/util/List;Ljava/util/Map;Lorg/apache/lucene/util/automaton/AutomatonProvider;)V

    .line 499
    iget-object v1, p0, Lorg/apache/lucene/util/automaton/RegExp;->exp2:Lorg/apache/lucene/util/automaton/RegExp;

    sget-object v2, Lorg/apache/lucene/util/automaton/RegExp$Kind;->REGEXP_CONCATENATION:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    move-object v0, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/util/automaton/RegExp;->findLeaves(Lorg/apache/lucene/util/automaton/RegExp;Lorg/apache/lucene/util/automaton/RegExp$Kind;Ljava/util/List;Ljava/util/Map;Lorg/apache/lucene/util/automaton/AutomatonProvider;)V

    .line 501
    invoke-static {v3}, Lorg/apache/lucene/util/automaton/BasicOperations;->concatenate(Ljava/util/List;)Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v6

    .line 502
    invoke-static {v6}, Lorg/apache/lucene/util/automaton/MinimizationOperations;->minimize(Lorg/apache/lucene/util/automaton/Automaton;)V

    goto :goto_0

    .line 505
    .end local v3    # "list":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/util/automaton/Automaton;>;"
    :pswitch_2
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/RegExp;->exp1:Lorg/apache/lucene/util/automaton/RegExp;

    invoke-direct {v0, p1, p2}, Lorg/apache/lucene/util/automaton/RegExp;->toAutomaton(Ljava/util/Map;Lorg/apache/lucene/util/automaton/AutomatonProvider;)Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v0

    .line 506
    iget-object v1, p0, Lorg/apache/lucene/util/automaton/RegExp;->exp2:Lorg/apache/lucene/util/automaton/RegExp;

    invoke-direct {v1, p1, p2}, Lorg/apache/lucene/util/automaton/RegExp;->toAutomaton(Ljava/util/Map;Lorg/apache/lucene/util/automaton/AutomatonProvider;)Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v1

    .line 505
    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/automaton/Automaton;->intersection(Lorg/apache/lucene/util/automaton/Automaton;)Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v6

    .line 507
    invoke-static {v6}, Lorg/apache/lucene/util/automaton/MinimizationOperations;->minimize(Lorg/apache/lucene/util/automaton/Automaton;)V

    goto :goto_0

    .line 510
    :pswitch_3
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/RegExp;->exp1:Lorg/apache/lucene/util/automaton/RegExp;

    invoke-direct {v0, p1, p2}, Lorg/apache/lucene/util/automaton/RegExp;->toAutomaton(Ljava/util/Map;Lorg/apache/lucene/util/automaton/AutomatonProvider;)Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/util/automaton/Automaton;->optional()Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v6

    .line 511
    invoke-static {v6}, Lorg/apache/lucene/util/automaton/MinimizationOperations;->minimize(Lorg/apache/lucene/util/automaton/Automaton;)V

    goto :goto_0

    .line 514
    :pswitch_4
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/RegExp;->exp1:Lorg/apache/lucene/util/automaton/RegExp;

    invoke-direct {v0, p1, p2}, Lorg/apache/lucene/util/automaton/RegExp;->toAutomaton(Ljava/util/Map;Lorg/apache/lucene/util/automaton/AutomatonProvider;)Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/util/automaton/Automaton;->repeat()Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v6

    .line 515
    invoke-static {v6}, Lorg/apache/lucene/util/automaton/MinimizationOperations;->minimize(Lorg/apache/lucene/util/automaton/Automaton;)V

    goto :goto_0

    .line 518
    :pswitch_5
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/RegExp;->exp1:Lorg/apache/lucene/util/automaton/RegExp;

    invoke-direct {v0, p1, p2}, Lorg/apache/lucene/util/automaton/RegExp;->toAutomaton(Ljava/util/Map;Lorg/apache/lucene/util/automaton/AutomatonProvider;)Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/util/automaton/RegExp;->min:I

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/automaton/Automaton;->repeat(I)Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v6

    .line 519
    invoke-static {v6}, Lorg/apache/lucene/util/automaton/MinimizationOperations;->minimize(Lorg/apache/lucene/util/automaton/Automaton;)V

    goto/16 :goto_0

    .line 522
    :pswitch_6
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/RegExp;->exp1:Lorg/apache/lucene/util/automaton/RegExp;

    invoke-direct {v0, p1, p2}, Lorg/apache/lucene/util/automaton/RegExp;->toAutomaton(Ljava/util/Map;Lorg/apache/lucene/util/automaton/AutomatonProvider;)Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/util/automaton/RegExp;->min:I

    iget v2, p0, Lorg/apache/lucene/util/automaton/RegExp;->max:I

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/automaton/Automaton;->repeat(II)Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v6

    .line 523
    invoke-static {v6}, Lorg/apache/lucene/util/automaton/MinimizationOperations;->minimize(Lorg/apache/lucene/util/automaton/Automaton;)V

    goto/16 :goto_0

    .line 526
    :pswitch_7
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/RegExp;->exp1:Lorg/apache/lucene/util/automaton/RegExp;

    invoke-direct {v0, p1, p2}, Lorg/apache/lucene/util/automaton/RegExp;->toAutomaton(Ljava/util/Map;Lorg/apache/lucene/util/automaton/AutomatonProvider;)Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/util/automaton/Automaton;->complement()Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v6

    .line 527
    invoke-static {v6}, Lorg/apache/lucene/util/automaton/MinimizationOperations;->minimize(Lorg/apache/lucene/util/automaton/Automaton;)V

    goto/16 :goto_0

    .line 530
    :pswitch_8
    iget v0, p0, Lorg/apache/lucene/util/automaton/RegExp;->c:I

    invoke-static {v0}, Lorg/apache/lucene/util/automaton/BasicAutomata;->makeChar(I)Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v6

    .line 531
    goto/16 :goto_0

    .line 533
    :pswitch_9
    iget v0, p0, Lorg/apache/lucene/util/automaton/RegExp;->from:I

    iget v1, p0, Lorg/apache/lucene/util/automaton/RegExp;->to:I

    invoke-static {v0, v1}, Lorg/apache/lucene/util/automaton/BasicAutomata;->makeCharRange(II)Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v6

    .line 534
    goto/16 :goto_0

    .line 536
    :pswitch_a
    invoke-static {}, Lorg/apache/lucene/util/automaton/BasicAutomata;->makeAnyChar()Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v6

    .line 537
    goto/16 :goto_0

    .line 539
    :pswitch_b
    invoke-static {}, Lorg/apache/lucene/util/automaton/BasicAutomata;->makeEmpty()Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v6

    .line 540
    goto/16 :goto_0

    .line 542
    :pswitch_c
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/RegExp;->s:Ljava/lang/String;

    invoke-static {v0}, Lorg/apache/lucene/util/automaton/BasicAutomata;->makeString(Ljava/lang/String;)Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v6

    .line 543
    goto/16 :goto_0

    .line 545
    :pswitch_d
    invoke-static {}, Lorg/apache/lucene/util/automaton/BasicAutomata;->makeAnyString()Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v6

    .line 546
    goto/16 :goto_0

    .line 548
    :pswitch_e
    const/4 v7, 0x0

    .line 549
    .local v7, "aa":Lorg/apache/lucene/util/automaton/Automaton;
    if-eqz p1, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/util/automaton/RegExp;->s:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    .end local v7    # "aa":Lorg/apache/lucene/util/automaton/Automaton;
    check-cast v7, Lorg/apache/lucene/util/automaton/Automaton;

    .line 550
    .restart local v7    # "aa":Lorg/apache/lucene/util/automaton/Automaton;
    :cond_0
    if-nez v7, :cond_1

    if-eqz p2, :cond_1

    .line 551
    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/RegExp;->s:Ljava/lang/String;

    invoke-interface {p2, v0}, Lorg/apache/lucene/util/automaton/AutomatonProvider;->getAutomaton(Ljava/lang/String;)Lorg/apache/lucene/util/automaton/Automaton;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 555
    :cond_1
    if-nez v7, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "\'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/apache/lucene/util/automaton/RegExp;->s:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 556
    const-string v2, "\' not found"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 552
    :catch_0
    move-exception v8

    .line 553
    .local v8, "e":Ljava/io/IOException;
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    .line 557
    .end local v8    # "e":Ljava/io/IOException;
    :cond_2
    invoke-virtual {v7}, Lorg/apache/lucene/util/automaton/Automaton;->clone()Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v6

    .line 558
    goto/16 :goto_0

    .line 560
    .end local v7    # "aa":Lorg/apache/lucene/util/automaton/Automaton;
    :pswitch_f
    iget v0, p0, Lorg/apache/lucene/util/automaton/RegExp;->min:I

    iget v1, p0, Lorg/apache/lucene/util/automaton/RegExp;->max:I

    iget v2, p0, Lorg/apache/lucene/util/automaton/RegExp;->digits:I

    invoke-static {v0, v1, v2}, Lorg/apache/lucene/util/automaton/BasicAutomata;->makeInterval(III)Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v6

    goto/16 :goto_0

    .line 487
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method

.method private toAutomatonAllowMutate(Ljava/util/Map;Lorg/apache/lucene/util/automaton/AutomatonProvider;)Lorg/apache/lucene/util/automaton/Automaton;
    .locals 3
    .param p2, "automaton_provider"    # Lorg/apache/lucene/util/automaton/AutomatonProvider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/util/automaton/Automaton;",
            ">;",
            "Lorg/apache/lucene/util/automaton/AutomatonProvider;",
            ")",
            "Lorg/apache/lucene/util/automaton/Automaton;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 476
    .local p1, "automata":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lorg/apache/lucene/util/automaton/Automaton;>;"
    const/4 v1, 0x0

    .line 477
    .local v1, "b":Z
    sget-boolean v2, Lorg/apache/lucene/util/automaton/RegExp;->allow_mutation:Z

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-static {v2}, Lorg/apache/lucene/util/automaton/Automaton;->setAllowMutate(Z)Z

    move-result v1

    .line 478
    :cond_0
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/util/automaton/RegExp;->toAutomaton(Ljava/util/Map;Lorg/apache/lucene/util/automaton/AutomatonProvider;)Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v0

    .line 479
    .local v0, "a":Lorg/apache/lucene/util/automaton/Automaton;
    sget-boolean v2, Lorg/apache/lucene/util/automaton/RegExp;->allow_mutation:Z

    if-eqz v2, :cond_1

    invoke-static {v1}, Lorg/apache/lucene/util/automaton/Automaton;->setAllowMutate(Z)Z

    .line 480
    :cond_1
    return-object v0
.end method


# virtual methods
.method public getIdentifiers()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 667
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 668
    .local v0, "set":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-virtual {p0, v0}, Lorg/apache/lucene/util/automaton/RegExp;->getIdentifiers(Ljava/util/Set;)V

    .line 669
    return-object v0
.end method

.method getIdentifiers(Ljava/util/Set;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 673
    .local p1, "set":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-static {}, Lorg/apache/lucene/util/automaton/RegExp;->$SWITCH_TABLE$org$apache$lucene$util$automaton$RegExp$Kind()[I

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/util/automaton/RegExp;->kind:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    invoke-virtual {v1}, Lorg/apache/lucene/util/automaton/RegExp$Kind;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 692
    :goto_0
    :pswitch_0
    return-void

    .line 677
    :pswitch_1
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/RegExp;->exp1:Lorg/apache/lucene/util/automaton/RegExp;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/util/automaton/RegExp;->getIdentifiers(Ljava/util/Set;)V

    .line 678
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/RegExp;->exp2:Lorg/apache/lucene/util/automaton/RegExp;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/util/automaton/RegExp;->getIdentifiers(Ljava/util/Set;)V

    goto :goto_0

    .line 685
    :pswitch_2
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/RegExp;->exp1:Lorg/apache/lucene/util/automaton/RegExp;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/util/automaton/RegExp;->getIdentifiers(Ljava/util/Set;)V

    goto :goto_0

    .line 688
    :pswitch_3
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/RegExp;->s:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 673
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method final parseCharClass()Lorg/apache/lucene/util/automaton/RegExp;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 940
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/RegExp;->parseCharExp()I

    move-result v0

    .line 941
    .local v0, "c":I
    const/16 v1, 0x2d

    invoke-direct {p0, v1}, Lorg/apache/lucene/util/automaton/RegExp;->match(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/RegExp;->parseCharExp()I

    move-result v1

    invoke-static {v0, v1}, Lorg/apache/lucene/util/automaton/RegExp;->makeCharRange(II)Lorg/apache/lucene/util/automaton/RegExp;

    move-result-object v1

    .line 942
    :goto_0
    return-object v1

    :cond_0
    invoke-static {v0}, Lorg/apache/lucene/util/automaton/RegExp;->makeChar(I)Lorg/apache/lucene/util/automaton/RegExp;

    move-result-object v1

    goto :goto_0
.end method

.method final parseCharClassExp()Lorg/apache/lucene/util/automaton/RegExp;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 921
    const/16 v2, 0x5b

    invoke-direct {p0, v2}, Lorg/apache/lucene/util/automaton/RegExp;->match(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 922
    const/4 v1, 0x0

    .line 923
    .local v1, "negate":Z
    const/16 v2, 0x5e

    invoke-direct {p0, v2}, Lorg/apache/lucene/util/automaton/RegExp;->match(I)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 924
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/RegExp;->parseCharClasses()Lorg/apache/lucene/util/automaton/RegExp;

    move-result-object v0

    .line 925
    .local v0, "e":Lorg/apache/lucene/util/automaton/RegExp;
    if-eqz v1, :cond_1

    invoke-static {}, Lorg/apache/lucene/util/automaton/RegExp;->makeAnyChar()Lorg/apache/lucene/util/automaton/RegExp;

    move-result-object v2

    invoke-static {v0}, Lorg/apache/lucene/util/automaton/RegExp;->makeComplement(Lorg/apache/lucene/util/automaton/RegExp;)Lorg/apache/lucene/util/automaton/RegExp;

    move-result-object v3

    invoke-static {v2, v3}, Lorg/apache/lucene/util/automaton/RegExp;->makeIntersection(Lorg/apache/lucene/util/automaton/RegExp;Lorg/apache/lucene/util/automaton/RegExp;)Lorg/apache/lucene/util/automaton/RegExp;

    move-result-object v0

    .line 926
    :cond_1
    const/16 v2, 0x5d

    invoke-direct {p0, v2}, Lorg/apache/lucene/util/automaton/RegExp;->match(I)Z

    move-result v2

    if-nez v2, :cond_3

    new-instance v2, Ljava/lang/IllegalArgumentException;

    .line 927
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "expected \']\' at position "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lorg/apache/lucene/util/automaton/RegExp;->pos:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 929
    .end local v0    # "e":Lorg/apache/lucene/util/automaton/RegExp;
    .end local v1    # "negate":Z
    :cond_2
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/RegExp;->parseSimpleExp()Lorg/apache/lucene/util/automaton/RegExp;

    move-result-object v0

    :cond_3
    return-object v0
.end method

.method final parseCharClasses()Lorg/apache/lucene/util/automaton/RegExp;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 933
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/RegExp;->parseCharClass()Lorg/apache/lucene/util/automaton/RegExp;

    move-result-object v0

    .line 934
    .local v0, "e":Lorg/apache/lucene/util/automaton/RegExp;
    :goto_0
    invoke-direct {p0}, Lorg/apache/lucene/util/automaton/RegExp;->more()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "]"

    invoke-direct {p0, v1}, Lorg/apache/lucene/util/automaton/RegExp;->peek(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 936
    :cond_0
    return-object v0

    .line 935
    :cond_1
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/RegExp;->parseCharClass()Lorg/apache/lucene/util/automaton/RegExp;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/apache/lucene/util/automaton/RegExp;->makeUnion(Lorg/apache/lucene/util/automaton/RegExp;Lorg/apache/lucene/util/automaton/RegExp;)Lorg/apache/lucene/util/automaton/RegExp;

    move-result-object v0

    goto :goto_0
.end method

.method final parseCharExp()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 1001
    const/16 v0, 0x5c

    invoke-direct {p0, v0}, Lorg/apache/lucene/util/automaton/RegExp;->match(I)Z

    .line 1002
    invoke-direct {p0}, Lorg/apache/lucene/util/automaton/RegExp;->next()I

    move-result v0

    return v0
.end method

.method final parseComplExp()Lorg/apache/lucene/util/automaton/RegExp;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 916
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lorg/apache/lucene/util/automaton/RegExp;->check(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x7e

    invoke-direct {p0, v0}, Lorg/apache/lucene/util/automaton/RegExp;->match(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/RegExp;->parseComplExp()Lorg/apache/lucene/util/automaton/RegExp;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/lucene/util/automaton/RegExp;->makeComplement(Lorg/apache/lucene/util/automaton/RegExp;)Lorg/apache/lucene/util/automaton/RegExp;

    move-result-object v0

    .line 917
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/RegExp;->parseCharClassExp()Lorg/apache/lucene/util/automaton/RegExp;

    move-result-object v0

    goto :goto_0
.end method

.method final parseConcatExp()Lorg/apache/lucene/util/automaton/RegExp;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 880
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/RegExp;->parseRepeatExp()Lorg/apache/lucene/util/automaton/RegExp;

    move-result-object v0

    .line 881
    .local v0, "e":Lorg/apache/lucene/util/automaton/RegExp;
    invoke-direct {p0}, Lorg/apache/lucene/util/automaton/RegExp;->more()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, ")|"

    invoke-direct {p0, v1}, Lorg/apache/lucene/util/automaton/RegExp;->peek(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lorg/apache/lucene/util/automaton/RegExp;->check(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "&"

    invoke-direct {p0, v1}, Lorg/apache/lucene/util/automaton/RegExp;->peek(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 882
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/RegExp;->parseConcatExp()Lorg/apache/lucene/util/automaton/RegExp;

    move-result-object v1

    .line 881
    invoke-static {v0, v1}, Lorg/apache/lucene/util/automaton/RegExp;->makeConcatenation(Lorg/apache/lucene/util/automaton/RegExp;Lorg/apache/lucene/util/automaton/RegExp;)Lorg/apache/lucene/util/automaton/RegExp;

    move-result-object v0

    .line 883
    :cond_1
    return-object v0
.end method

.method final parseInterExp()Lorg/apache/lucene/util/automaton/RegExp;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 873
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/RegExp;->parseConcatExp()Lorg/apache/lucene/util/automaton/RegExp;

    move-result-object v0

    .line 874
    .local v0, "e":Lorg/apache/lucene/util/automaton/RegExp;
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lorg/apache/lucene/util/automaton/RegExp;->check(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x26

    invoke-direct {p0, v1}, Lorg/apache/lucene/util/automaton/RegExp;->match(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 875
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/RegExp;->parseInterExp()Lorg/apache/lucene/util/automaton/RegExp;

    move-result-object v1

    .line 874
    invoke-static {v0, v1}, Lorg/apache/lucene/util/automaton/RegExp;->makeIntersection(Lorg/apache/lucene/util/automaton/RegExp;Lorg/apache/lucene/util/automaton/RegExp;)Lorg/apache/lucene/util/automaton/RegExp;

    move-result-object v0

    .line 876
    :cond_0
    return-object v0
.end method

.method final parseRepeatExp()Lorg/apache/lucene/util/automaton/RegExp;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 887
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/RegExp;->parseComplExp()Lorg/apache/lucene/util/automaton/RegExp;

    move-result-object v0

    .line 888
    .local v0, "e":Lorg/apache/lucene/util/automaton/RegExp;
    :cond_0
    :goto_0
    const-string v4, "?*+{"

    invoke-direct {p0, v4}, Lorg/apache/lucene/util/automaton/RegExp;->peek(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 912
    return-object v0

    .line 889
    :cond_1
    const/16 v4, 0x3f

    invoke-direct {p0, v4}, Lorg/apache/lucene/util/automaton/RegExp;->match(I)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-static {v0}, Lorg/apache/lucene/util/automaton/RegExp;->makeOptional(Lorg/apache/lucene/util/automaton/RegExp;)Lorg/apache/lucene/util/automaton/RegExp;

    move-result-object v0

    goto :goto_0

    .line 890
    :cond_2
    const/16 v4, 0x2a

    invoke-direct {p0, v4}, Lorg/apache/lucene/util/automaton/RegExp;->match(I)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-static {v0}, Lorg/apache/lucene/util/automaton/RegExp;->makeRepeat(Lorg/apache/lucene/util/automaton/RegExp;)Lorg/apache/lucene/util/automaton/RegExp;

    move-result-object v0

    goto :goto_0

    .line 891
    :cond_3
    const/16 v4, 0x2b

    invoke-direct {p0, v4}, Lorg/apache/lucene/util/automaton/RegExp;->match(I)Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v4, 0x1

    invoke-static {v0, v4}, Lorg/apache/lucene/util/automaton/RegExp;->makeRepeat(Lorg/apache/lucene/util/automaton/RegExp;I)Lorg/apache/lucene/util/automaton/RegExp;

    move-result-object v0

    goto :goto_0

    .line 892
    :cond_4
    const/16 v4, 0x7b

    invoke-direct {p0, v4}, Lorg/apache/lucene/util/automaton/RegExp;->match(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 893
    iget v3, p0, Lorg/apache/lucene/util/automaton/RegExp;->pos:I

    .line 894
    .local v3, "start":I
    :goto_1
    const-string v4, "0123456789"

    invoke-direct {p0, v4}, Lorg/apache/lucene/util/automaton/RegExp;->peek(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 896
    iget v4, p0, Lorg/apache/lucene/util/automaton/RegExp;->pos:I

    if-ne v3, v4, :cond_6

    new-instance v4, Ljava/lang/IllegalArgumentException;

    .line 897
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "integer expected at position "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, Lorg/apache/lucene/util/automaton/RegExp;->pos:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 895
    :cond_5
    invoke-direct {p0}, Lorg/apache/lucene/util/automaton/RegExp;->next()I

    goto :goto_1

    .line 898
    :cond_6
    iget-object v4, p0, Lorg/apache/lucene/util/automaton/RegExp;->b:Ljava/lang/String;

    iget v5, p0, Lorg/apache/lucene/util/automaton/RegExp;->pos:I

    invoke-virtual {v4, v3, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 899
    .local v2, "n":I
    const/4 v1, -0x1

    .line 900
    .local v1, "m":I
    const/16 v4, 0x2c

    invoke-direct {p0, v4}, Lorg/apache/lucene/util/automaton/RegExp;->match(I)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 901
    iget v3, p0, Lorg/apache/lucene/util/automaton/RegExp;->pos:I

    .line 902
    :goto_2
    const-string v4, "0123456789"

    invoke-direct {p0, v4}, Lorg/apache/lucene/util/automaton/RegExp;->peek(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_8

    .line 904
    iget v4, p0, Lorg/apache/lucene/util/automaton/RegExp;->pos:I

    if-eq v3, v4, :cond_7

    iget-object v4, p0, Lorg/apache/lucene/util/automaton/RegExp;->b:Ljava/lang/String;

    iget v5, p0, Lorg/apache/lucene/util/automaton/RegExp;->pos:I

    invoke-virtual {v4, v3, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 906
    :cond_7
    :goto_3
    const/16 v4, 0x7d

    invoke-direct {p0, v4}, Lorg/apache/lucene/util/automaton/RegExp;->match(I)Z

    move-result v4

    if-nez v4, :cond_a

    new-instance v4, Ljava/lang/IllegalArgumentException;

    .line 907
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "expected \'}\' at position "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, Lorg/apache/lucene/util/automaton/RegExp;->pos:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 903
    :cond_8
    invoke-direct {p0}, Lorg/apache/lucene/util/automaton/RegExp;->next()I

    goto :goto_2

    .line 905
    :cond_9
    move v1, v2

    goto :goto_3

    .line 908
    :cond_a
    const/4 v4, -0x1

    if-ne v1, v4, :cond_b

    invoke-static {v0, v2}, Lorg/apache/lucene/util/automaton/RegExp;->makeRepeat(Lorg/apache/lucene/util/automaton/RegExp;I)Lorg/apache/lucene/util/automaton/RegExp;

    move-result-object v0

    goto/16 :goto_0

    .line 909
    :cond_b
    invoke-static {v0, v2, v1}, Lorg/apache/lucene/util/automaton/RegExp;->makeRepeat(Lorg/apache/lucene/util/automaton/RegExp;II)Lorg/apache/lucene/util/automaton/RegExp;

    move-result-object v0

    goto/16 :goto_0
.end method

.method final parseSimpleExp()Lorg/apache/lucene/util/automaton/RegExp;
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 946
    const/16 v10, 0x2e

    invoke-direct {p0, v10}, Lorg/apache/lucene/util/automaton/RegExp;->match(I)Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-static {}, Lorg/apache/lucene/util/automaton/RegExp;->makeAnyChar()Lorg/apache/lucene/util/automaton/RegExp;

    move-result-object v1

    .line 997
    :cond_0
    :goto_0
    return-object v1

    .line 947
    :cond_1
    const/4 v10, 0x4

    invoke-direct {p0, v10}, Lorg/apache/lucene/util/automaton/RegExp;->check(I)Z

    move-result v10

    if-eqz v10, :cond_2

    const/16 v10, 0x23

    invoke-direct {p0, v10}, Lorg/apache/lucene/util/automaton/RegExp;->match(I)Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-static {}, Lorg/apache/lucene/util/automaton/RegExp;->makeEmpty()Lorg/apache/lucene/util/automaton/RegExp;

    move-result-object v1

    goto :goto_0

    .line 948
    :cond_2
    const/16 v10, 0x8

    invoke-direct {p0, v10}, Lorg/apache/lucene/util/automaton/RegExp;->check(I)Z

    move-result v10

    if-eqz v10, :cond_3

    const/16 v10, 0x40

    invoke-direct {p0, v10}, Lorg/apache/lucene/util/automaton/RegExp;->match(I)Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-static {}, Lorg/apache/lucene/util/automaton/RegExp;->makeAnyString()Lorg/apache/lucene/util/automaton/RegExp;

    move-result-object v1

    goto :goto_0

    .line 949
    :cond_3
    const/16 v10, 0x22

    invoke-direct {p0, v10}, Lorg/apache/lucene/util/automaton/RegExp;->match(I)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 950
    iget v8, p0, Lorg/apache/lucene/util/automaton/RegExp;->pos:I

    .line 951
    .local v8, "start":I
    :goto_1
    invoke-direct {p0}, Lorg/apache/lucene/util/automaton/RegExp;->more()Z

    move-result v10

    if-eqz v10, :cond_4

    const-string v10, "\""

    invoke-direct {p0, v10}, Lorg/apache/lucene/util/automaton/RegExp;->peek(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 953
    :cond_4
    const/16 v10, 0x22

    invoke-direct {p0, v10}, Lorg/apache/lucene/util/automaton/RegExp;->match(I)Z

    move-result v10

    if-nez v10, :cond_6

    new-instance v10, Ljava/lang/IllegalArgumentException;

    .line 954
    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "expected \'\"\' at position "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v12, p0, Lorg/apache/lucene/util/automaton/RegExp;->pos:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 952
    :cond_5
    invoke-direct {p0}, Lorg/apache/lucene/util/automaton/RegExp;->next()I

    goto :goto_1

    .line 955
    :cond_6
    iget-object v10, p0, Lorg/apache/lucene/util/automaton/RegExp;->b:Ljava/lang/String;

    iget v11, p0, Lorg/apache/lucene/util/automaton/RegExp;->pos:I

    add-int/lit8 v11, v11, -0x1

    invoke-virtual {v10, v8, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lorg/apache/lucene/util/automaton/RegExp;->makeString(Ljava/lang/String;)Lorg/apache/lucene/util/automaton/RegExp;

    move-result-object v1

    goto :goto_0

    .line 956
    .end local v8    # "start":I
    :cond_7
    const/16 v10, 0x28

    invoke-direct {p0, v10}, Lorg/apache/lucene/util/automaton/RegExp;->match(I)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 957
    const/16 v10, 0x29

    invoke-direct {p0, v10}, Lorg/apache/lucene/util/automaton/RegExp;->match(I)Z

    move-result v10

    if-eqz v10, :cond_8

    const-string v10, ""

    invoke-static {v10}, Lorg/apache/lucene/util/automaton/RegExp;->makeString(Ljava/lang/String;)Lorg/apache/lucene/util/automaton/RegExp;

    move-result-object v1

    goto/16 :goto_0

    .line 958
    :cond_8
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/RegExp;->parseUnionExp()Lorg/apache/lucene/util/automaton/RegExp;

    move-result-object v1

    .line 959
    .local v1, "e":Lorg/apache/lucene/util/automaton/RegExp;
    const/16 v10, 0x29

    invoke-direct {p0, v10}, Lorg/apache/lucene/util/automaton/RegExp;->match(I)Z

    move-result v10

    if-nez v10, :cond_0

    new-instance v10, Ljava/lang/IllegalArgumentException;

    .line 960
    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "expected \')\' at position "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v12, p0, Lorg/apache/lucene/util/automaton/RegExp;->pos:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 962
    .end local v1    # "e":Lorg/apache/lucene/util/automaton/RegExp;
    :cond_9
    const/16 v10, 0x10

    invoke-direct {p0, v10}, Lorg/apache/lucene/util/automaton/RegExp;->check(I)Z

    move-result v10

    if-nez v10, :cond_a

    const/16 v10, 0x20

    invoke-direct {p0, v10}, Lorg/apache/lucene/util/automaton/RegExp;->check(I)Z

    move-result v10

    if-eqz v10, :cond_15

    :cond_a
    const/16 v10, 0x3c

    invoke-direct {p0, v10}, Lorg/apache/lucene/util/automaton/RegExp;->match(I)Z

    move-result v10

    if-eqz v10, :cond_15

    .line 963
    iget v8, p0, Lorg/apache/lucene/util/automaton/RegExp;->pos:I

    .line 964
    .restart local v8    # "start":I
    :goto_2
    invoke-direct {p0}, Lorg/apache/lucene/util/automaton/RegExp;->more()Z

    move-result v10

    if-eqz v10, :cond_b

    const-string v10, ">"

    invoke-direct {p0, v10}, Lorg/apache/lucene/util/automaton/RegExp;->peek(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_c

    .line 966
    :cond_b
    const/16 v10, 0x3e

    invoke-direct {p0, v10}, Lorg/apache/lucene/util/automaton/RegExp;->match(I)Z

    move-result v10

    if-nez v10, :cond_d

    new-instance v10, Ljava/lang/IllegalArgumentException;

    .line 967
    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "expected \'>\' at position "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v12, p0, Lorg/apache/lucene/util/automaton/RegExp;->pos:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 965
    :cond_c
    invoke-direct {p0}, Lorg/apache/lucene/util/automaton/RegExp;->next()I

    goto :goto_2

    .line 968
    :cond_d
    iget-object v10, p0, Lorg/apache/lucene/util/automaton/RegExp;->b:Ljava/lang/String;

    iget v11, p0, Lorg/apache/lucene/util/automaton/RegExp;->pos:I

    add-int/lit8 v11, v11, -0x1

    invoke-virtual {v10, v8, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 969
    .local v5, "s":Ljava/lang/String;
    const/16 v10, 0x2d

    invoke-virtual {v5, v10}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    .line 970
    .local v2, "i":I
    const/4 v10, -0x1

    if-ne v2, v10, :cond_f

    .line 971
    const/16 v10, 0x10

    invoke-direct {p0, v10}, Lorg/apache/lucene/util/automaton/RegExp;->check(I)Z

    move-result v10

    if-nez v10, :cond_e

    new-instance v10, Ljava/lang/IllegalArgumentException;

    .line 972
    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "interval syntax error at position "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v12, p0, Lorg/apache/lucene/util/automaton/RegExp;->pos:I

    add-int/lit8 v12, v12, -0x1

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 973
    :cond_e
    invoke-static {v5}, Lorg/apache/lucene/util/automaton/RegExp;->makeAutomaton(Ljava/lang/String;)Lorg/apache/lucene/util/automaton/RegExp;

    move-result-object v1

    goto/16 :goto_0

    .line 975
    :cond_f
    const/16 v10, 0x20

    invoke-direct {p0, v10}, Lorg/apache/lucene/util/automaton/RegExp;->check(I)Z

    move-result v10

    if-nez v10, :cond_10

    new-instance v10, Ljava/lang/IllegalArgumentException;

    .line 976
    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "illegal identifier at position "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v12, p0, Lorg/apache/lucene/util/automaton/RegExp;->pos:I

    add-int/lit8 v12, v12, -0x1

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 978
    :cond_10
    if-eqz v2, :cond_11

    :try_start_0
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    if-eq v2, v10, :cond_11

    const/16 v10, 0x2d

    invoke-virtual {v5, v10}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v10

    if-eq v2, v10, :cond_12

    :cond_11
    new-instance v10, Ljava/lang/NumberFormatException;

    invoke-direct {v10}, Ljava/lang/NumberFormatException;-><init>()V

    throw v10
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 992
    :catch_0
    move-exception v1

    .line 993
    .local v1, "e":Ljava/lang/NumberFormatException;
    new-instance v10, Ljava/lang/IllegalArgumentException;

    .line 994
    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "interval syntax error at position "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v12, p0, Lorg/apache/lucene/util/automaton/RegExp;->pos:I

    add-int/lit8 v12, v12, -0x1

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 993
    invoke-direct {v10, v11}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 979
    .end local v1    # "e":Ljava/lang/NumberFormatException;
    :cond_12
    const/4 v10, 0x0

    :try_start_1
    invoke-virtual {v5, v10, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    .line 980
    .local v7, "smin":Ljava/lang/String;
    add-int/lit8 v10, v2, 0x1

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v11

    invoke-virtual {v5, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 981
    .local v6, "smax":Ljava/lang/String;
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 982
    .local v4, "imin":I
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 984
    .local v3, "imax":I
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v10

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v11

    if-ne v10, v11, :cond_14

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v0

    .line 986
    .local v0, "digits":I
    :goto_3
    if-le v4, v3, :cond_13

    .line 987
    move v9, v4

    .line 988
    .local v9, "t":I
    move v4, v3

    .line 989
    move v3, v9

    .line 991
    .end local v9    # "t":I
    :cond_13
    invoke-static {v4, v3, v0}, Lorg/apache/lucene/util/automaton/RegExp;->makeInterval(III)Lorg/apache/lucene/util/automaton/RegExp;
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v1

    goto/16 :goto_0

    .line 985
    .end local v0    # "digits":I
    :cond_14
    const/4 v0, 0x0

    .restart local v0    # "digits":I
    goto :goto_3

    .line 997
    .end local v0    # "digits":I
    .end local v2    # "i":I
    .end local v3    # "imax":I
    .end local v4    # "imin":I
    .end local v5    # "s":Ljava/lang/String;
    .end local v6    # "smax":Ljava/lang/String;
    .end local v7    # "smin":Ljava/lang/String;
    .end local v8    # "start":I
    :cond_15
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/RegExp;->parseCharExp()I

    move-result v10

    invoke-static {v10}, Lorg/apache/lucene/util/automaton/RegExp;->makeChar(I)Lorg/apache/lucene/util/automaton/RegExp;

    move-result-object v1

    goto/16 :goto_0
.end method

.method final parseUnionExp()Lorg/apache/lucene/util/automaton/RegExp;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 867
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/RegExp;->parseInterExp()Lorg/apache/lucene/util/automaton/RegExp;

    move-result-object v0

    .line 868
    .local v0, "e":Lorg/apache/lucene/util/automaton/RegExp;
    const/16 v1, 0x7c

    invoke-direct {p0, v1}, Lorg/apache/lucene/util/automaton/RegExp;->match(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/RegExp;->parseUnionExp()Lorg/apache/lucene/util/automaton/RegExp;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/apache/lucene/util/automaton/RegExp;->makeUnion(Lorg/apache/lucene/util/automaton/RegExp;Lorg/apache/lucene/util/automaton/RegExp;)Lorg/apache/lucene/util/automaton/RegExp;

    move-result-object v0

    .line 869
    :cond_0
    return-object v0
.end method

.method public setAllowMutate(Z)Z
    .locals 1
    .param p1, "flag"    # Z

    .prologue
    .line 469
    sget-boolean v0, Lorg/apache/lucene/util/automaton/RegExp;->allow_mutation:Z

    .line 470
    .local v0, "b":Z
    sput-boolean p1, Lorg/apache/lucene/util/automaton/RegExp;->allow_mutation:Z

    .line 471
    return v0
.end method

.method public toAutomaton()Lorg/apache/lucene/util/automaton/Automaton;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 428
    invoke-direct {p0, v0, v0}, Lorg/apache/lucene/util/automaton/RegExp;->toAutomatonAllowMutate(Ljava/util/Map;Lorg/apache/lucene/util/automaton/AutomatonProvider;)Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v0

    return-object v0
.end method

.method public toAutomaton(Ljava/util/Map;)Lorg/apache/lucene/util/automaton/Automaton;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/util/automaton/Automaton;",
            ">;)",
            "Lorg/apache/lucene/util/automaton/Automaton;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 457
    .local p1, "automata":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lorg/apache/lucene/util/automaton/Automaton;>;"
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/util/automaton/RegExp;->toAutomatonAllowMutate(Ljava/util/Map;Lorg/apache/lucene/util/automaton/AutomatonProvider;)Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v0

    return-object v0
.end method

.method public toAutomaton(Lorg/apache/lucene/util/automaton/AutomatonProvider;)Lorg/apache/lucene/util/automaton/Automaton;
    .locals 1
    .param p1, "automaton_provider"    # Lorg/apache/lucene/util/automaton/AutomatonProvider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 442
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lorg/apache/lucene/util/automaton/RegExp;->toAutomatonAllowMutate(Ljava/util/Map;Lorg/apache/lucene/util/automaton/AutomatonProvider;)Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 579
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/lucene/util/automaton/RegExp;->toStringBuilder(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method toStringBuilder(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;
    .locals 6
    .param p1, "b"    # Ljava/lang/StringBuilder;

    .prologue
    const/16 v5, 0x30

    .line 583
    invoke-static {}, Lorg/apache/lucene/util/automaton/RegExp;->$SWITCH_TABLE$org$apache$lucene$util$automaton$RegExp$Kind()[I

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/util/automaton/RegExp;->kind:Lorg/apache/lucene/util/automaton/RegExp$Kind;

    invoke-virtual {v4}, Lorg/apache/lucene/util/automaton/RegExp$Kind;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 660
    :goto_0
    return-object p1

    .line 585
    :pswitch_0
    const-string v3, "("

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 586
    iget-object v3, p0, Lorg/apache/lucene/util/automaton/RegExp;->exp1:Lorg/apache/lucene/util/automaton/RegExp;

    invoke-virtual {v3, p1}, Lorg/apache/lucene/util/automaton/RegExp;->toStringBuilder(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    .line 587
    const-string/jumbo v3, "|"

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 588
    iget-object v3, p0, Lorg/apache/lucene/util/automaton/RegExp;->exp2:Lorg/apache/lucene/util/automaton/RegExp;

    invoke-virtual {v3, p1}, Lorg/apache/lucene/util/automaton/RegExp;->toStringBuilder(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    .line 589
    const-string v3, ")"

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 592
    :pswitch_1
    iget-object v3, p0, Lorg/apache/lucene/util/automaton/RegExp;->exp1:Lorg/apache/lucene/util/automaton/RegExp;

    invoke-virtual {v3, p1}, Lorg/apache/lucene/util/automaton/RegExp;->toStringBuilder(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    .line 593
    iget-object v3, p0, Lorg/apache/lucene/util/automaton/RegExp;->exp2:Lorg/apache/lucene/util/automaton/RegExp;

    invoke-virtual {v3, p1}, Lorg/apache/lucene/util/automaton/RegExp;->toStringBuilder(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 596
    :pswitch_2
    const-string v3, "("

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 597
    iget-object v3, p0, Lorg/apache/lucene/util/automaton/RegExp;->exp1:Lorg/apache/lucene/util/automaton/RegExp;

    invoke-virtual {v3, p1}, Lorg/apache/lucene/util/automaton/RegExp;->toStringBuilder(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    .line 598
    const-string v3, "&"

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 599
    iget-object v3, p0, Lorg/apache/lucene/util/automaton/RegExp;->exp2:Lorg/apache/lucene/util/automaton/RegExp;

    invoke-virtual {v3, p1}, Lorg/apache/lucene/util/automaton/RegExp;->toStringBuilder(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    .line 600
    const-string v3, ")"

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 603
    :pswitch_3
    const-string v3, "("

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 604
    iget-object v3, p0, Lorg/apache/lucene/util/automaton/RegExp;->exp1:Lorg/apache/lucene/util/automaton/RegExp;

    invoke-virtual {v3, p1}, Lorg/apache/lucene/util/automaton/RegExp;->toStringBuilder(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    .line 605
    const-string v3, ")?"

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 608
    :pswitch_4
    const-string v3, "("

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 609
    iget-object v3, p0, Lorg/apache/lucene/util/automaton/RegExp;->exp1:Lorg/apache/lucene/util/automaton/RegExp;

    invoke-virtual {v3, p1}, Lorg/apache/lucene/util/automaton/RegExp;->toStringBuilder(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    .line 610
    const-string v3, ")*"

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 613
    :pswitch_5
    const-string v3, "("

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 614
    iget-object v3, p0, Lorg/apache/lucene/util/automaton/RegExp;->exp1:Lorg/apache/lucene/util/automaton/RegExp;

    invoke-virtual {v3, p1}, Lorg/apache/lucene/util/automaton/RegExp;->toStringBuilder(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    .line 615
    const-string v3, "){"

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lorg/apache/lucene/util/automaton/RegExp;->min:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ",}"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 618
    :pswitch_6
    const-string v3, "("

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 619
    iget-object v3, p0, Lorg/apache/lucene/util/automaton/RegExp;->exp1:Lorg/apache/lucene/util/automaton/RegExp;

    invoke-virtual {v3, p1}, Lorg/apache/lucene/util/automaton/RegExp;->toStringBuilder(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    .line 620
    const-string v3, "){"

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lorg/apache/lucene/util/automaton/RegExp;->min:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lorg/apache/lucene/util/automaton/RegExp;->max:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "}"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 623
    :pswitch_7
    const-string/jumbo v3, "~("

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 624
    iget-object v3, p0, Lorg/apache/lucene/util/automaton/RegExp;->exp1:Lorg/apache/lucene/util/automaton/RegExp;

    invoke-virtual {v3, p1}, Lorg/apache/lucene/util/automaton/RegExp;->toStringBuilder(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    .line 625
    const-string v3, ")"

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 628
    :pswitch_8
    const-string v3, "\\"

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lorg/apache/lucene/util/automaton/RegExp;->c:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->appendCodePoint(I)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 631
    :pswitch_9
    const-string v3, "[\\"

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lorg/apache/lucene/util/automaton/RegExp;->from:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->appendCodePoint(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "-\\"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lorg/apache/lucene/util/automaton/RegExp;->to:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->appendCodePoint(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 634
    :pswitch_a
    const-string v3, "."

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 637
    :pswitch_b
    const-string v3, "#"

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 640
    :pswitch_c
    const-string v3, "\""

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/util/automaton/RegExp;->s:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 643
    :pswitch_d
    const-string v3, "@"

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 646
    :pswitch_e
    const-string v3, "<"

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/util/automaton/RegExp;->s:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ">"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 649
    :pswitch_f
    iget v3, p0, Lorg/apache/lucene/util/automaton/RegExp;->min:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    .line 650
    .local v1, "s1":Ljava/lang/String;
    iget v3, p0, Lorg/apache/lucene/util/automaton/RegExp;->max:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    .line 651
    .local v2, "s2":Ljava/lang/String;
    const-string v3, "<"

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 652
    iget v3, p0, Lorg/apache/lucene/util/automaton/RegExp;->digits:I

    if-lez v3, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    .local v0, "i":I
    :goto_1
    iget v3, p0, Lorg/apache/lucene/util/automaton/RegExp;->digits:I

    if-lt v0, v3, :cond_2

    .line 654
    .end local v0    # "i":I
    :cond_0
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 655
    iget v3, p0, Lorg/apache/lucene/util/automaton/RegExp;->digits:I

    if-lez v3, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    .restart local v0    # "i":I
    :goto_2
    iget v3, p0, Lorg/apache/lucene/util/automaton/RegExp;->digits:I

    if-lt v0, v3, :cond_3

    .line 657
    .end local v0    # "i":I
    :cond_1
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ">"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 653
    .restart local v0    # "i":I
    :cond_2
    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 652
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 656
    :cond_3
    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 655
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 583
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method
