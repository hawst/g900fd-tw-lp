.class public final Lorg/apache/lucene/util/packed/AppendingLongBuffer;
.super Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;
.source "AppendingLongBuffer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/packed/AppendingLongBuffer$Iterator;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;-><init>(I)V

    .line 33
    return-void
.end method


# virtual methods
.method get(II)J
    .locals 4
    .param p1, "block"    # I
    .param p2, "element"    # I

    .prologue
    .line 37
    iget v0, p0, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->valuesOff:I

    if-ne p1, v0, :cond_0

    .line 38
    iget-object v0, p0, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->pending:[J

    aget-wide v0, v0, p2

    .line 42
    :goto_0
    return-wide v0

    .line 39
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->deltas:[Lorg/apache/lucene/util/packed/PackedInts$Reader;

    aget-object v0, v0, p1

    if-nez v0, :cond_1

    .line 40
    iget-object v0, p0, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->minValues:[J

    aget-wide v0, v0, p1

    goto :goto_0

    .line 42
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->minValues:[J

    aget-wide v0, v0, p1

    iget-object v2, p0, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->deltas:[Lorg/apache/lucene/util/packed/PackedInts$Reader;

    aget-object v2, v2, p1

    invoke-interface {v2, p2}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I)J

    move-result-wide v2

    add-long/2addr v0, v2

    goto :goto_0
.end method

.method public bridge synthetic iterator()Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer$Iterator;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->iterator()Lorg/apache/lucene/util/packed/AppendingLongBuffer$Iterator;

    move-result-object v0

    return-object v0
.end method

.method public iterator()Lorg/apache/lucene/util/packed/AppendingLongBuffer$Iterator;
    .locals 2

    .prologue
    .line 75
    new-instance v0, Lorg/apache/lucene/util/packed/AppendingLongBuffer$Iterator;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/apache/lucene/util/packed/AppendingLongBuffer$Iterator;-><init>(Lorg/apache/lucene/util/packed/AppendingLongBuffer;Lorg/apache/lucene/util/packed/AppendingLongBuffer$Iterator;)V

    return-object v0
.end method

.method packPendingValues()V
    .locals 14

    .prologue
    const-wide/16 v12, 0x0

    const/4 v11, 0x0

    .line 47
    sget-boolean v9, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->$assertionsDisabled:Z

    if-nez v9, :cond_0

    iget v9, p0, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->pendingOff:I

    const/16 v10, 0x400

    if-eq v9, v10, :cond_0

    new-instance v9, Ljava/lang/AssertionError;

    invoke-direct {v9}, Ljava/lang/AssertionError;-><init>()V

    throw v9

    .line 50
    :cond_0
    iget-object v9, p0, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->pending:[J

    aget-wide v6, v9, v11

    .line 51
    .local v6, "minValue":J
    iget-object v9, p0, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->pending:[J

    aget-wide v4, v9, v11

    .line 52
    .local v4, "maxValue":J
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_0
    iget v9, p0, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->pendingOff:I

    if-lt v1, v9, :cond_2

    .line 56
    sub-long v2, v4, v6

    .line 58
    .local v2, "delta":J
    iget-object v9, p0, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->minValues:[J

    iget v10, p0, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->valuesOff:I

    aput-wide v6, v9, v10

    .line 59
    cmp-long v9, v2, v12

    if-eqz v9, :cond_1

    .line 61
    cmp-long v9, v2, v12

    if-gez v9, :cond_3

    const/16 v0, 0x40

    .line 62
    .local v0, "bitsRequired":I
    :goto_1
    const/4 v1, 0x0

    :goto_2
    iget v9, p0, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->pendingOff:I

    if-lt v1, v9, :cond_4

    .line 65
    iget v9, p0, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->pendingOff:I

    const/4 v10, 0x0

    invoke-static {v9, v0, v10}, Lorg/apache/lucene/util/packed/PackedInts;->getMutable(IIF)Lorg/apache/lucene/util/packed/PackedInts$Mutable;

    move-result-object v8

    .line 66
    .local v8, "mutable":Lorg/apache/lucene/util/packed/PackedInts$Mutable;
    const/4 v1, 0x0

    :goto_3
    iget v9, p0, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->pendingOff:I

    if-lt v1, v9, :cond_5

    .line 69
    iget-object v9, p0, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->deltas:[Lorg/apache/lucene/util/packed/PackedInts$Reader;

    iget v10, p0, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->valuesOff:I

    aput-object v8, v9, v10

    .line 71
    .end local v0    # "bitsRequired":I
    .end local v8    # "mutable":Lorg/apache/lucene/util/packed/PackedInts$Mutable;
    :cond_1
    return-void

    .line 53
    .end local v2    # "delta":J
    :cond_2
    iget-object v9, p0, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->pending:[J

    aget-wide v10, v9, v1

    invoke-static {v6, v7, v10, v11}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v6

    .line 54
    iget-object v9, p0, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->pending:[J

    aget-wide v10, v9, v1

    invoke-static {v4, v5, v10, v11}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    .line 52
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 61
    .restart local v2    # "delta":J
    :cond_3
    invoke-static {v2, v3}, Lorg/apache/lucene/util/packed/PackedInts;->bitsRequired(J)I

    move-result v0

    goto :goto_1

    .line 63
    .restart local v0    # "bitsRequired":I
    :cond_4
    iget-object v9, p0, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->pending:[J

    aget-wide v10, v9, v1

    sub-long/2addr v10, v6

    aput-wide v10, v9, v1

    .line 62
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 67
    .restart local v8    # "mutable":Lorg/apache/lucene/util/packed/PackedInts$Mutable;
    :cond_5
    iget-object v9, p0, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->pending:[J

    iget v10, p0, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->pendingOff:I

    sub-int/2addr v10, v1

    invoke-interface {v8, v1, v9, v1, v10}, Lorg/apache/lucene/util/packed/PackedInts$Mutable;->set(I[JII)I

    move-result v9

    add-int/2addr v1, v9

    goto :goto_3
.end method

.method public bridge synthetic ramBytesUsed()J
    .locals 2

    .prologue
    .line 1
    invoke-super {p0}, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;->ramBytesUsed()J

    move-result-wide v0

    return-wide v0
.end method
