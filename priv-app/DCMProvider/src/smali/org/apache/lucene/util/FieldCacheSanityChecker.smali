.class public final Lorg/apache/lucene/util/FieldCacheSanityChecker;
.super Ljava/lang/Object;
.source "FieldCacheSanityChecker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/FieldCacheSanityChecker$Insanity;,
        Lorg/apache/lucene/util/FieldCacheSanityChecker$InsanityType;,
        Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;
    }
.end annotation


# instance fields
.field private estimateRam:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    return-void
.end method

.method public static checkSanity(Lorg/apache/lucene/search/FieldCache;)[Lorg/apache/lucene/util/FieldCacheSanityChecker$Insanity;
    .locals 1
    .param p0, "cache"    # Lorg/apache/lucene/search/FieldCache;

    .prologue
    .line 76
    invoke-interface {p0}, Lorg/apache/lucene/search/FieldCache;->getCacheEntries()[Lorg/apache/lucene/search/FieldCache$CacheEntry;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/lucene/util/FieldCacheSanityChecker;->checkSanity([Lorg/apache/lucene/search/FieldCache$CacheEntry;)[Lorg/apache/lucene/util/FieldCacheSanityChecker$Insanity;

    move-result-object v0

    return-object v0
.end method

.method public static varargs checkSanity([Lorg/apache/lucene/search/FieldCache$CacheEntry;)[Lorg/apache/lucene/util/FieldCacheSanityChecker$Insanity;
    .locals 2
    .param p0, "cacheEntries"    # [Lorg/apache/lucene/search/FieldCache$CacheEntry;

    .prologue
    .line 85
    new-instance v0, Lorg/apache/lucene/util/FieldCacheSanityChecker;

    invoke-direct {v0}, Lorg/apache/lucene/util/FieldCacheSanityChecker;-><init>()V

    .line 86
    .local v0, "sanityChecker":Lorg/apache/lucene/util/FieldCacheSanityChecker;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/FieldCacheSanityChecker;->setRamUsageEstimator(Z)V

    .line 87
    invoke-virtual {v0, p0}, Lorg/apache/lucene/util/FieldCacheSanityChecker;->check([Lorg/apache/lucene/search/FieldCache$CacheEntry;)[Lorg/apache/lucene/util/FieldCacheSanityChecker$Insanity;

    move-result-object v1

    return-object v1
.end method

.method private checkSubreaders(Lorg/apache/lucene/util/MapOfSets;Lorg/apache/lucene/util/MapOfSets;)Ljava/util/Collection;
    .locals 24
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/MapOfSets",
            "<",
            "Ljava/lang/Integer;",
            "Lorg/apache/lucene/search/FieldCache$CacheEntry;",
            ">;",
            "Lorg/apache/lucene/util/MapOfSets",
            "<",
            "Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/util/FieldCacheSanityChecker$Insanity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 206
    .local p1, "valIdToItems":Lorg/apache/lucene/util/MapOfSets;, "Lorg/apache/lucene/util/MapOfSets<Ljava/lang/Integer;Lorg/apache/lucene/search/FieldCache$CacheEntry;>;"
    .local p2, "readerFieldToValIds":Lorg/apache/lucene/util/MapOfSets;, "Lorg/apache/lucene/util/MapOfSets<Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;Ljava/lang/Integer;>;"
    new-instance v7, Ljava/util/ArrayList;

    const/16 v19, 0x17

    move/from16 v0, v19

    invoke-direct {v7, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 208
    .local v7, "insanity":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/util/FieldCacheSanityChecker$Insanity;>;"
    new-instance v3, Ljava/util/HashMap;

    const/16 v19, 0x11

    move/from16 v0, v19

    invoke-direct {v3, v0}, Ljava/util/HashMap;-><init>(I)V

    .line 209
    .local v3, "badChildren":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;Ljava/util/Set<Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;>;>;"
    new-instance v5, Lorg/apache/lucene/util/MapOfSets;

    invoke-direct {v5, v3}, Lorg/apache/lucene/util/MapOfSets;-><init>(Ljava/util/Map;)V

    .line 211
    .local v5, "badKids":Lorg/apache/lucene/util/MapOfSets;, "Lorg/apache/lucene/util/MapOfSets<Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;>;"
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/util/MapOfSets;->getMap()Ljava/util/Map;

    move-result-object v18

    .line 212
    .local v18, "viToItemSets":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/util/Set<Lorg/apache/lucene/search/FieldCache$CacheEntry;>;>;"
    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/util/MapOfSets;->getMap()Ljava/util/Map;

    move-result-object v15

    .line 214
    .local v15, "rfToValIdSets":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;Ljava/util/Set<Ljava/lang/Integer;>;>;"
    new-instance v16, Ljava/util/HashSet;

    const/16 v19, 0x11

    move-object/from16 v0, v16

    move/from16 v1, v19

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    .line 216
    .local v16, "seen":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;>;"
    invoke-interface {v15}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v13

    .line 217
    .local v13, "readerFields":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;>;"
    invoke-interface {v13}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v20

    :cond_0
    :goto_0
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-nez v19, :cond_1

    .line 242
    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v20

    :goto_1
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-nez v19, :cond_5

    .line 270
    return-object v7

    .line 217
    :cond_1
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;

    .line 219
    .local v14, "rf":Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;
    move-object/from16 v0, v16

    invoke-interface {v0, v14}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_0

    .line 221
    iget-object v0, v14, Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;->readerKey:Ljava/lang/Object;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/FieldCacheSanityChecker;->getAllDescendantReaderKeys(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v10

    .line 222
    .local v10, "kids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v21

    :goto_2
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-nez v19, :cond_2

    .line 238
    move-object/from16 v0, v16

    invoke-interface {v0, v14}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 222
    :cond_2
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    .line 223
    .local v9, "kidKey":Ljava/lang/Object;
    new-instance v8, Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;

    iget-object v0, v14, Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;->fieldName:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-direct {v8, v9, v0}, Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;-><init>(Ljava/lang/Object;Ljava/lang/String;)V

    .line 225
    .local v8, "kid":Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;
    invoke-interface {v3, v8}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_4

    .line 228
    invoke-virtual {v5, v14, v8}, Lorg/apache/lucene/util/MapOfSets;->put(Ljava/lang/Object;Ljava/lang/Object;)I

    .line 229
    invoke-interface {v3, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/util/Collection;

    move-object/from16 v0, v19

    invoke-virtual {v5, v14, v0}, Lorg/apache/lucene/util/MapOfSets;->putAll(Ljava/lang/Object;Ljava/util/Collection;)I

    .line 230
    invoke-interface {v3, v8}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 236
    :cond_3
    :goto_3
    move-object/from16 v0, v16

    invoke-interface {v0, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 232
    :cond_4
    invoke-interface {v15, v8}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_3

    .line 234
    invoke-virtual {v5, v14, v8}, Lorg/apache/lucene/util/MapOfSets;->put(Ljava/lang/Object;Ljava/lang/Object;)I

    goto :goto_3

    .line 242
    .end local v8    # "kid":Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;
    .end local v9    # "kidKey":Ljava/lang/Object;
    .end local v10    # "kids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    .end local v14    # "rf":Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;
    :cond_5
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;

    .line 243
    .local v12, "parent":Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;
    invoke-interface {v3, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/util/Set;

    .line 245
    .local v11, "kids":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-interface {v11}, Ljava/util/Set;->size()I

    move-result v19

    mul-int/lit8 v19, v19, 0x2

    move/from16 v0, v19

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 249
    .local v4, "badEntries":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/FieldCache$CacheEntry;>;"
    invoke-interface {v15, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/util/Set;

    invoke-interface/range {v19 .. v19}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v21

    :goto_4
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-nez v19, :cond_7

    .line 255
    invoke-interface {v11}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v21

    :cond_6
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-nez v19, :cond_8

    .line 261
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v19

    move/from16 v0, v19

    new-array v6, v0, [Lorg/apache/lucene/search/FieldCache$CacheEntry;

    .line 262
    .local v6, "badness":[Lorg/apache/lucene/search/FieldCache$CacheEntry;
    invoke-interface {v4, v6}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "badness":[Lorg/apache/lucene/search/FieldCache$CacheEntry;
    check-cast v6, [Lorg/apache/lucene/search/FieldCache$CacheEntry;

    .line 264
    .restart local v6    # "badness":[Lorg/apache/lucene/search/FieldCache$CacheEntry;
    new-instance v19, Lorg/apache/lucene/util/FieldCacheSanityChecker$Insanity;

    sget-object v21, Lorg/apache/lucene/util/FieldCacheSanityChecker$InsanityType;->SUBREADER:Lorg/apache/lucene/util/FieldCacheSanityChecker$InsanityType;

    .line 265
    new-instance v22, Ljava/lang/StringBuilder;

    const-string v23, "Found caches for descendants of "

    invoke-direct/range {v22 .. v23}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 266
    invoke-virtual {v12}, Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    .line 265
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    .line 267
    move-object/from16 v0, v19

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2, v6}, Lorg/apache/lucene/util/FieldCacheSanityChecker$Insanity;-><init>(Lorg/apache/lucene/util/FieldCacheSanityChecker$InsanityType;Ljava/lang/String;[Lorg/apache/lucene/search/FieldCache$CacheEntry;)V

    .line 264
    move-object/from16 v0, v19

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 249
    .end local v6    # "badness":[Lorg/apache/lucene/search/FieldCache$CacheEntry;
    :cond_7
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Integer;

    .line 250
    .local v17, "value":Ljava/lang/Integer;
    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/util/Collection;

    move-object/from16 v0, v19

    invoke-interface {v4, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_4

    .line 255
    .end local v17    # "value":Ljava/lang/Integer;
    :cond_8
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;

    .line 256
    .restart local v8    # "kid":Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;
    invoke-interface {v15, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/util/Set;

    invoke-interface/range {v19 .. v19}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v22

    :goto_5
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_6

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Integer;

    .line 257
    .restart local v17    # "value":Ljava/lang/Integer;
    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/util/Collection;

    move-object/from16 v0, v19

    invoke-interface {v4, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_5
.end method

.method private checkValueMismatch(Lorg/apache/lucene/util/MapOfSets;Lorg/apache/lucene/util/MapOfSets;Ljava/util/Set;)Ljava/util/Collection;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/MapOfSets",
            "<",
            "Ljava/lang/Integer;",
            "Lorg/apache/lucene/search/FieldCache$CacheEntry;",
            ">;",
            "Lorg/apache/lucene/util/MapOfSets",
            "<",
            "Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;",
            ">;)",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/util/FieldCacheSanityChecker$Insanity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 169
    .local p1, "valIdToItems":Lorg/apache/lucene/util/MapOfSets;, "Lorg/apache/lucene/util/MapOfSets<Ljava/lang/Integer;Lorg/apache/lucene/search/FieldCache$CacheEntry;>;"
    .local p2, "readerFieldToValIds":Lorg/apache/lucene/util/MapOfSets;, "Lorg/apache/lucene/util/MapOfSets<Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;Ljava/lang/Integer;>;"
    .local p3, "valMismatchKeys":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface/range {p3 .. p3}, Ljava/util/Set;->size()I

    move-result v8

    mul-int/lit8 v8, v8, 0x3

    invoke-direct {v3, v8}, Ljava/util/ArrayList;-><init>(I)V

    .line 171
    .local v3, "insanity":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/util/FieldCacheSanityChecker$Insanity;>;"
    invoke-interface/range {p3 .. p3}, Ljava/util/Set;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_0

    .line 174
    invoke-virtual {p2}, Lorg/apache/lucene/util/MapOfSets;->getMap()Ljava/util/Map;

    move-result-object v5

    .line 175
    .local v5, "rfMap":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;Ljava/util/Set<Ljava/lang/Integer;>;>;"
    invoke-virtual {p1}, Lorg/apache/lucene/util/MapOfSets;->getMap()Ljava/util/Map;

    move-result-object v6

    .line 176
    .local v6, "valMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/util/Set<Lorg/apache/lucene/search/FieldCache$CacheEntry;>;>;"
    invoke-interface/range {p3 .. p3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_1

    .line 192
    .end local v5    # "rfMap":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;Ljava/util/Set<Ljava/lang/Integer;>;>;"
    .end local v6    # "valMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/util/Set<Lorg/apache/lucene/search/FieldCache$CacheEntry;>;>;"
    :cond_0
    return-object v3

    .line 176
    .restart local v5    # "rfMap":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;Ljava/util/Set<Ljava/lang/Integer;>;>;"
    .restart local v6    # "valMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/util/Set<Lorg/apache/lucene/search/FieldCache$CacheEntry;>;>;"
    :cond_1
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;

    .line 177
    .local v4, "rf":Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface/range {p3 .. p3}, Ljava/util/Set;->size()I

    move-result v8

    mul-int/lit8 v8, v8, 0x2

    invoke-direct {v0, v8}, Ljava/util/ArrayList;-><init>(I)V

    .line 178
    .local v0, "badEntries":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/FieldCache$CacheEntry;>;"
    invoke-interface {v5, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/Set;

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_3

    .line 184
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v8

    new-array v1, v8, [Lorg/apache/lucene/search/FieldCache$CacheEntry;

    .line 185
    .local v1, "badness":[Lorg/apache/lucene/search/FieldCache$CacheEntry;
    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "badness":[Lorg/apache/lucene/search/FieldCache$CacheEntry;
    check-cast v1, [Lorg/apache/lucene/search/FieldCache$CacheEntry;

    .line 187
    .restart local v1    # "badness":[Lorg/apache/lucene/search/FieldCache$CacheEntry;
    new-instance v8, Lorg/apache/lucene/util/FieldCacheSanityChecker$Insanity;

    sget-object v10, Lorg/apache/lucene/util/FieldCacheSanityChecker$InsanityType;->VALUEMISMATCH:Lorg/apache/lucene/util/FieldCacheSanityChecker$InsanityType;

    .line 188
    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "Multiple distinct value objects for "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 189
    invoke-virtual {v4}, Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 188
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 189
    invoke-direct {v8, v10, v11, v1}, Lorg/apache/lucene/util/FieldCacheSanityChecker$Insanity;-><init>(Lorg/apache/lucene/util/FieldCacheSanityChecker$InsanityType;Ljava/lang/String;[Lorg/apache/lucene/search/FieldCache$CacheEntry;)V

    .line 187
    invoke-interface {v3, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 178
    .end local v1    # "badness":[Lorg/apache/lucene/search/FieldCache$CacheEntry;
    :cond_3
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    .line 179
    .local v7, "value":Ljava/lang/Integer;
    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/Set;

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/search/FieldCache$CacheEntry;

    .line 180
    .local v2, "cacheEntry":Lorg/apache/lucene/search/FieldCache$CacheEntry;
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private getAllDescendantReaderKeys(Ljava/lang/Object;)Ljava/util/List;
    .locals 7
    .param p1, "seed"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 280
    new-instance v0, Ljava/util/ArrayList;

    const/16 v5, 0x11

    invoke-direct {v0, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 281
    .local v0, "all":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 282
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    if-lt v3, v5, :cond_0

    .line 301
    const/4 v5, 0x1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v6

    invoke-interface {v0, v5, v6}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v5

    return-object v5

    .line 283
    :cond_0
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    .line 286
    .local v4, "obj":Ljava/lang/Object;
    instance-of v5, v4, Lorg/apache/lucene/index/IndexReader;

    if-eqz v5, :cond_1

    .line 289
    :try_start_0
    check-cast v4, Lorg/apache/lucene/index/IndexReader;

    .end local v4    # "obj":Ljava/lang/Object;
    invoke-virtual {v4}, Lorg/apache/lucene/index/IndexReader;->getContext()Lorg/apache/lucene/index/IndexReaderContext;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/lucene/index/IndexReaderContext;->children()Ljava/util/List;

    move-result-object v1

    .line 290
    .local v1, "childs":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/IndexReaderContext;>;"
    if-eqz v1, :cond_1

    .line 291
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_2

    .line 282
    .end local v1    # "childs":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/IndexReaderContext;>;"
    :cond_1
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 291
    .restart local v1    # "childs":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/IndexReaderContext;>;"
    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/index/IndexReaderContext;

    .line 292
    .local v2, "ctx":Lorg/apache/lucene/index/IndexReaderContext;
    invoke-virtual {v2}, Lorg/apache/lucene/index/IndexReaderContext;->reader()Lorg/apache/lucene/index/IndexReader;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/lucene/index/IndexReader;->getCoreCacheKey()Ljava/lang/Object;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/apache/lucene/store/AlreadyClosedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 295
    .end local v1    # "childs":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/IndexReaderContext;>;"
    .end local v2    # "ctx":Lorg/apache/lucene/index/IndexReaderContext;
    :catch_0
    move-exception v5

    goto :goto_2
.end method


# virtual methods
.method public varargs check([Lorg/apache/lucene/search/FieldCache$CacheEntry;)[Lorg/apache/lucene/util/FieldCacheSanityChecker$Insanity;
    .locals 11
    .param p1, "cacheEntries"    # [Lorg/apache/lucene/search/FieldCache$CacheEntry;

    .prologue
    const/16 v10, 0x11

    .line 99
    if-eqz p1, :cond_0

    array-length v9, p1

    if-nez v9, :cond_1

    .line 100
    :cond_0
    const/4 v9, 0x0

    new-array v9, v9, [Lorg/apache/lucene/util/FieldCacheSanityChecker$Insanity;

    .line 155
    :goto_0
    return-object v9

    .line 102
    :cond_1
    iget-boolean v9, p0, Lorg/apache/lucene/util/FieldCacheSanityChecker;->estimateRam:Z

    if-eqz v9, :cond_2

    .line 103
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v9, p1

    if-lt v0, v9, :cond_3

    .line 112
    .end local v0    # "i":I
    :cond_2
    new-instance v7, Lorg/apache/lucene/util/MapOfSets;

    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9, v10}, Ljava/util/HashMap;-><init>(I)V

    invoke-direct {v7, v9}, Lorg/apache/lucene/util/MapOfSets;-><init>(Ljava/util/Map;)V

    .line 114
    .local v7, "valIdToItems":Lorg/apache/lucene/util/MapOfSets;, "Lorg/apache/lucene/util/MapOfSets<Ljava/lang/Integer;Lorg/apache/lucene/search/FieldCache$CacheEntry;>;"
    new-instance v3, Lorg/apache/lucene/util/MapOfSets;

    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9, v10}, Ljava/util/HashMap;-><init>(I)V

    invoke-direct {v3, v9}, Lorg/apache/lucene/util/MapOfSets;-><init>(Ljava/util/Map;)V

    .line 118
    .local v3, "readerFieldToValIds":Lorg/apache/lucene/util/MapOfSets;, "Lorg/apache/lucene/util/MapOfSets<Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;Ljava/lang/Integer;>;"
    new-instance v8, Ljava/util/HashSet;

    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    .line 121
    .local v8, "valMismatchKeys":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;>;"
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_2
    array-length v9, p1

    if-lt v0, v9, :cond_4

    .line 147
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v8}, Ljava/util/Set;->size()I

    move-result v9

    mul-int/lit8 v9, v9, 0x3

    invoke-direct {v1, v9}, Ljava/util/ArrayList;-><init>(I)V

    .line 149
    .local v1, "insanity":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/util/FieldCacheSanityChecker$Insanity;>;"
    invoke-direct {p0, v7, v3, v8}, Lorg/apache/lucene/util/FieldCacheSanityChecker;->checkValueMismatch(Lorg/apache/lucene/util/MapOfSets;Lorg/apache/lucene/util/MapOfSets;Ljava/util/Set;)Ljava/util/Collection;

    move-result-object v9

    invoke-interface {v1, v9}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 152
    invoke-direct {p0, v7, v3}, Lorg/apache/lucene/util/FieldCacheSanityChecker;->checkSubreaders(Lorg/apache/lucene/util/MapOfSets;Lorg/apache/lucene/util/MapOfSets;)Ljava/util/Collection;

    move-result-object v9

    invoke-interface {v1, v9}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 155
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v9

    new-array v9, v9, [Lorg/apache/lucene/util/FieldCacheSanityChecker$Insanity;

    invoke-interface {v1, v9}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Lorg/apache/lucene/util/FieldCacheSanityChecker$Insanity;

    goto :goto_0

    .line 104
    .end local v1    # "insanity":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/util/FieldCacheSanityChecker$Insanity;>;"
    .end local v3    # "readerFieldToValIds":Lorg/apache/lucene/util/MapOfSets;, "Lorg/apache/lucene/util/MapOfSets<Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;Ljava/lang/Integer;>;"
    .end local v7    # "valIdToItems":Lorg/apache/lucene/util/MapOfSets;, "Lorg/apache/lucene/util/MapOfSets<Ljava/lang/Integer;Lorg/apache/lucene/search/FieldCache$CacheEntry;>;"
    .end local v8    # "valMismatchKeys":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;>;"
    :cond_3
    aget-object v9, p1, v0

    invoke-virtual {v9}, Lorg/apache/lucene/search/FieldCache$CacheEntry;->estimateSize()V

    .line 103
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 122
    .restart local v3    # "readerFieldToValIds":Lorg/apache/lucene/util/MapOfSets;, "Lorg/apache/lucene/util/MapOfSets<Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;Ljava/lang/Integer;>;"
    .restart local v7    # "valIdToItems":Lorg/apache/lucene/util/MapOfSets;, "Lorg/apache/lucene/util/MapOfSets<Ljava/lang/Integer;Lorg/apache/lucene/search/FieldCache$CacheEntry;>;"
    .restart local v8    # "valMismatchKeys":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;>;"
    :cond_4
    aget-object v2, p1, v0

    .line 123
    .local v2, "item":Lorg/apache/lucene/search/FieldCache$CacheEntry;
    invoke-virtual {v2}, Lorg/apache/lucene/search/FieldCache$CacheEntry;->getValue()Ljava/lang/Object;

    move-result-object v5

    .line 128
    .local v5, "val":Ljava/lang/Object;
    instance-of v9, v5, Lorg/apache/lucene/util/Bits;

    if-eqz v9, :cond_6

    .line 121
    :cond_5
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 132
    :cond_6
    instance-of v9, v5, Lorg/apache/lucene/search/FieldCache$CreationPlaceholder;

    if-nez v9, :cond_5

    .line 135
    new-instance v4, Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;

    invoke-virtual {v2}, Lorg/apache/lucene/search/FieldCache$CacheEntry;->getReaderKey()Ljava/lang/Object;

    move-result-object v9

    .line 136
    invoke-virtual {v2}, Lorg/apache/lucene/search/FieldCache$CacheEntry;->getFieldName()Ljava/lang/String;

    move-result-object v10

    .line 135
    invoke-direct {v4, v9, v10}, Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;-><init>(Ljava/lang/Object;Ljava/lang/String;)V

    .line 138
    .local v4, "rf":Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;
    invoke-static {v5}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 141
    .local v6, "valId":Ljava/lang/Integer;
    invoke-virtual {v7, v6, v2}, Lorg/apache/lucene/util/MapOfSets;->put(Ljava/lang/Object;Ljava/lang/Object;)I

    .line 142
    const/4 v9, 0x1

    invoke-virtual {v3, v4, v6}, Lorg/apache/lucene/util/MapOfSets;->put(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v10

    if-ge v9, v10, :cond_5

    .line 143
    invoke-interface {v8, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_3
.end method

.method public setRamUsageEstimator(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 67
    iput-boolean p1, p0, Lorg/apache/lucene/util/FieldCacheSanityChecker;->estimateRam:Z

    .line 68
    return-void
.end method
