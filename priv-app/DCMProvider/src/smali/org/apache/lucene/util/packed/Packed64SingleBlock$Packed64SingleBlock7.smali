.class Lorg/apache/lucene/util/packed/Packed64SingleBlock$Packed64SingleBlock7;
.super Lorg/apache/lucene/util/packed/Packed64SingleBlock;
.source "Packed64SingleBlock.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/packed/Packed64SingleBlock;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Packed64SingleBlock7"
.end annotation


# direct methods
.method constructor <init>(I)V
    .locals 1
    .param p1, "valueCount"    # I

    .prologue
    .line 400
    const/4 v0, 0x7

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/util/packed/Packed64SingleBlock;-><init>(II)V

    .line 401
    return-void
.end method


# virtual methods
.method public get(I)J
    .locals 8
    .param p1, "index"    # I

    .prologue
    .line 405
    div-int/lit8 v1, p1, 0x9

    .line 406
    .local v1, "o":I
    rem-int/lit8 v0, p1, 0x9

    .line 407
    .local v0, "b":I
    mul-int/lit8 v2, v0, 0x7

    .line 408
    .local v2, "shift":I
    iget-object v3, p0, Lorg/apache/lucene/util/packed/Packed64SingleBlock$Packed64SingleBlock7;->blocks:[J

    aget-wide v4, v3, v1

    ushr-long/2addr v4, v2

    const-wide/16 v6, 0x7f

    and-long/2addr v4, v6

    return-wide v4
.end method

.method public set(IJ)V
    .locals 10
    .param p1, "index"    # I
    .param p2, "value"    # J

    .prologue
    .line 413
    div-int/lit8 v1, p1, 0x9

    .line 414
    .local v1, "o":I
    rem-int/lit8 v0, p1, 0x9

    .line 415
    .local v0, "b":I
    mul-int/lit8 v2, v0, 0x7

    .line 416
    .local v2, "shift":I
    iget-object v3, p0, Lorg/apache/lucene/util/packed/Packed64SingleBlock$Packed64SingleBlock7;->blocks:[J

    iget-object v4, p0, Lorg/apache/lucene/util/packed/Packed64SingleBlock$Packed64SingleBlock7;->blocks:[J

    aget-wide v4, v4, v1

    const-wide/16 v6, 0x7f

    shl-long/2addr v6, v2

    const-wide/16 v8, -0x1

    xor-long/2addr v6, v8

    and-long/2addr v4, v6

    shl-long v6, p2, v2

    or-long/2addr v4, v6

    aput-wide v4, v3, v1

    .line 417
    return-void
.end method
