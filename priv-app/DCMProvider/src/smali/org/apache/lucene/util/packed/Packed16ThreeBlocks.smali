.class final Lorg/apache/lucene/util/packed/Packed16ThreeBlocks;
.super Lorg/apache/lucene/util/packed/PackedInts$MutableImpl;
.source "Packed16ThreeBlocks.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final MAX_SIZE:I = 0x2aaaaaaa


# instance fields
.field final blocks:[S


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lorg/apache/lucene/util/packed/Packed16ThreeBlocks;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/packed/Packed16ThreeBlocks;->$assertionsDisabled:Z

    .line 35
    return-void

    .line 32
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(I)V
    .locals 2
    .param p1, "valueCount"    # I

    .prologue
    .line 38
    const/16 v0, 0x30

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/util/packed/PackedInts$MutableImpl;-><init>(II)V

    .line 39
    const v0, 0x2aaaaaaa

    if-le p1, v0, :cond_0

    .line 40
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "MAX_SIZE exceeded"

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 42
    :cond_0
    mul-int/lit8 v0, p1, 0x3

    new-array v0, v0, [S

    iput-object v0, p0, Lorg/apache/lucene/util/packed/Packed16ThreeBlocks;->blocks:[S

    .line 43
    return-void
.end method

.method constructor <init>(ILorg/apache/lucene/store/DataInput;I)V
    .locals 8
    .param p1, "packedIntsVersion"    # I
    .param p2, "in"    # Lorg/apache/lucene/store/DataInput;
    .param p3, "valueCount"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46
    invoke-direct {p0, p3}, Lorg/apache/lucene/util/packed/Packed16ThreeBlocks;-><init>(I)V

    .line 47
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    mul-int/lit8 v2, p3, 0x3

    if-lt v0, v2, :cond_0

    .line 51
    sget-object v2, Lorg/apache/lucene/util/packed/PackedInts$Format;->PACKED:Lorg/apache/lucene/util/packed/PackedInts$Format;

    const/16 v3, 0x30

    invoke-virtual {v2, p1, p3, v3}, Lorg/apache/lucene/util/packed/PackedInts$Format;->byteCount(III)J

    move-result-wide v2

    const-wide/16 v4, 0x3

    int-to-long v6, p3

    mul-long/2addr v4, v6

    const-wide/16 v6, 0x2

    mul-long/2addr v4, v6

    sub-long/2addr v2, v4

    long-to-int v1, v2

    .line 52
    .local v1, "remaining":I
    const/4 v0, 0x0

    :goto_1
    if-lt v0, v1, :cond_1

    .line 55
    return-void

    .line 48
    .end local v1    # "remaining":I
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/util/packed/Packed16ThreeBlocks;->blocks:[S

    invoke-virtual {p2}, Lorg/apache/lucene/store/DataInput;->readShort()S

    move-result v3

    aput-short v3, v2, v0

    .line 47
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 53
    .restart local v1    # "remaining":I
    :cond_1
    invoke-virtual {p2}, Lorg/apache/lucene/store/DataInput;->readByte()B

    .line 52
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method


# virtual methods
.method public clear()V
    .locals 2

    .prologue
    .line 114
    iget-object v0, p0, Lorg/apache/lucene/util/packed/Packed16ThreeBlocks;->blocks:[S

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([SS)V

    .line 115
    return-void
.end method

.method public fill(IIJ)V
    .locals 9
    .param p1, "fromIndex"    # I
    .param p2, "toIndex"    # I
    .param p3, "val"    # J

    .prologue
    .line 102
    const/16 v5, 0x20

    ushr-long v6, p3, v5

    long-to-int v5, v6

    int-to-short v0, v5

    .line 103
    .local v0, "block1":S
    const/16 v5, 0x10

    ushr-long v6, p3, v5

    long-to-int v5, v6

    int-to-short v1, v5

    .line 104
    .local v1, "block2":S
    long-to-int v5, p3

    int-to-short v2, v5

    .line 105
    .local v2, "block3":S
    mul-int/lit8 v4, p1, 0x3

    .local v4, "i":I
    mul-int/lit8 v3, p2, 0x3

    .local v3, "end":I
    :goto_0
    if-lt v4, v3, :cond_0

    .line 110
    return-void

    .line 106
    :cond_0
    iget-object v5, p0, Lorg/apache/lucene/util/packed/Packed16ThreeBlocks;->blocks:[S

    aput-short v0, v5, v4

    .line 107
    iget-object v5, p0, Lorg/apache/lucene/util/packed/Packed16ThreeBlocks;->blocks:[S

    add-int/lit8 v6, v4, 0x1

    aput-short v1, v5, v6

    .line 108
    iget-object v5, p0, Lorg/apache/lucene/util/packed/Packed16ThreeBlocks;->blocks:[S

    add-int/lit8 v6, v4, 0x2

    aput-short v2, v5, v6

    .line 105
    add-int/lit8 v4, v4, 0x3

    goto :goto_0
.end method

.method public get(I[JII)I
    .locals 10
    .param p1, "index"    # I
    .param p2, "arr"    # [J
    .param p3, "off"    # I
    .param p4, "len"    # I

    .prologue
    .line 65
    sget-boolean v4, Lorg/apache/lucene/util/packed/Packed16ThreeBlocks;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    if-gtz p4, :cond_0

    new-instance v4, Ljava/lang/AssertionError;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "len must be > 0 (got "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    .line 66
    :cond_0
    sget-boolean v4, Lorg/apache/lucene/util/packed/Packed16ThreeBlocks;->$assertionsDisabled:Z

    if-nez v4, :cond_2

    if-ltz p1, :cond_1

    iget v4, p0, Lorg/apache/lucene/util/packed/Packed16ThreeBlocks;->valueCount:I

    if-lt p1, v4, :cond_2

    :cond_1
    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 67
    :cond_2
    sget-boolean v4, Lorg/apache/lucene/util/packed/Packed16ThreeBlocks;->$assertionsDisabled:Z

    if-nez v4, :cond_3

    add-int v4, p3, p4

    array-length v5, p2

    if-le v4, v5, :cond_3

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 69
    :cond_3
    iget v4, p0, Lorg/apache/lucene/util/packed/Packed16ThreeBlocks;->valueCount:I

    sub-int/2addr v4, p1

    invoke-static {v4, p4}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 70
    .local v1, "gets":I
    mul-int/lit8 v2, p1, 0x3

    .local v2, "i":I
    add-int v4, p1, v1

    mul-int/lit8 v0, v4, 0x3

    .local v0, "end":I
    move v3, p3

    .end local p3    # "off":I
    .local v3, "off":I
    :goto_0
    if-lt v2, v0, :cond_4

    .line 73
    return v1

    .line 71
    :cond_4
    add-int/lit8 p3, v3, 0x1

    .end local v3    # "off":I
    .restart local p3    # "off":I
    iget-object v4, p0, Lorg/apache/lucene/util/packed/Packed16ThreeBlocks;->blocks:[S

    aget-short v4, v4, v2

    int-to-long v4, v4

    const-wide/32 v6, 0xffff

    and-long/2addr v4, v6

    const/16 v6, 0x20

    shl-long/2addr v4, v6

    iget-object v6, p0, Lorg/apache/lucene/util/packed/Packed16ThreeBlocks;->blocks:[S

    add-int/lit8 v7, v2, 0x1

    aget-short v6, v6, v7

    int-to-long v6, v6

    const-wide/32 v8, 0xffff

    and-long/2addr v6, v8

    const/16 v8, 0x10

    shl-long/2addr v6, v8

    or-long/2addr v4, v6

    iget-object v6, p0, Lorg/apache/lucene/util/packed/Packed16ThreeBlocks;->blocks:[S

    add-int/lit8 v7, v2, 0x2

    aget-short v6, v6, v7

    int-to-long v6, v6

    const-wide/32 v8, 0xffff

    and-long/2addr v6, v8

    or-long/2addr v4, v6

    aput-wide v4, p2, v3

    .line 70
    add-int/lit8 v2, v2, 0x3

    move v3, p3

    .end local p3    # "off":I
    .restart local v3    # "off":I
    goto :goto_0
.end method

.method public get(I)J
    .locals 8
    .param p1, "index"    # I

    .prologue
    const-wide/32 v6, 0xffff

    .line 59
    mul-int/lit8 v0, p1, 0x3

    .line 60
    .local v0, "o":I
    iget-object v1, p0, Lorg/apache/lucene/util/packed/Packed16ThreeBlocks;->blocks:[S

    aget-short v1, v1, v0

    int-to-long v2, v1

    and-long/2addr v2, v6

    const/16 v1, 0x20

    shl-long/2addr v2, v1

    iget-object v1, p0, Lorg/apache/lucene/util/packed/Packed16ThreeBlocks;->blocks:[S

    add-int/lit8 v4, v0, 0x1

    aget-short v1, v1, v4

    int-to-long v4, v1

    and-long/2addr v4, v6

    const/16 v1, 0x10

    shl-long/2addr v4, v1

    or-long/2addr v2, v4

    iget-object v1, p0, Lorg/apache/lucene/util/packed/Packed16ThreeBlocks;->blocks:[S

    add-int/lit8 v4, v0, 0x2

    aget-short v1, v1, v4

    int-to-long v4, v1

    and-long/2addr v4, v6

    or-long/2addr v2, v4

    return-wide v2
.end method

.method public ramBytesUsed()J
    .locals 4

    .prologue
    .line 120
    sget v0, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_HEADER:I

    add-int/lit8 v0, v0, 0x8

    .line 122
    sget v1, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_REF:I

    .line 120
    add-int/2addr v0, v1

    int-to-long v0, v0

    .line 119
    invoke-static {v0, v1}, Lorg/apache/lucene/util/RamUsageEstimator;->alignObjectSize(J)J

    move-result-wide v0

    .line 123
    iget-object v2, p0, Lorg/apache/lucene/util/packed/Packed16ThreeBlocks;->blocks:[S

    invoke-static {v2}, Lorg/apache/lucene/util/RamUsageEstimator;->sizeOf([S)J

    move-result-wide v2

    .line 119
    add-long/2addr v0, v2

    return-wide v0
.end method

.method public set(I[JII)I
    .locals 10
    .param p1, "index"    # I
    .param p2, "arr"    # [J
    .param p3, "off"    # I
    .param p4, "len"    # I

    .prologue
    .line 86
    sget-boolean v5, Lorg/apache/lucene/util/packed/Packed16ThreeBlocks;->$assertionsDisabled:Z

    if-nez v5, :cond_0

    if-gtz p4, :cond_0

    new-instance v5, Ljava/lang/AssertionError;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "len must be > 0 (got "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ")"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v8}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v5

    .line 87
    :cond_0
    sget-boolean v5, Lorg/apache/lucene/util/packed/Packed16ThreeBlocks;->$assertionsDisabled:Z

    if-nez v5, :cond_2

    if-ltz p1, :cond_1

    iget v5, p0, Lorg/apache/lucene/util/packed/Packed16ThreeBlocks;->valueCount:I

    if-lt p1, v5, :cond_2

    :cond_1
    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 88
    :cond_2
    sget-boolean v5, Lorg/apache/lucene/util/packed/Packed16ThreeBlocks;->$assertionsDisabled:Z

    if-nez v5, :cond_3

    add-int v5, p3, p4

    array-length v8, p2

    if-le v5, v8, :cond_3

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 90
    :cond_3
    iget v5, p0, Lorg/apache/lucene/util/packed/Packed16ThreeBlocks;->valueCount:I

    sub-int/2addr v5, p1

    invoke-static {v5, p4}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 91
    .local v4, "sets":I
    move v1, p3

    .local v1, "i":I
    mul-int/lit8 v2, p1, 0x3

    .local v2, "o":I
    add-int v0, p3, v4

    .local v0, "end":I
    move v3, v2

    .end local v2    # "o":I
    .local v3, "o":I
    :goto_0
    if-lt v1, v0, :cond_4

    .line 97
    return v4

    .line 92
    :cond_4
    aget-wide v6, p2, v1

    .line 93
    .local v6, "value":J
    iget-object v5, p0, Lorg/apache/lucene/util/packed/Packed16ThreeBlocks;->blocks:[S

    add-int/lit8 v2, v3, 0x1

    .end local v3    # "o":I
    .restart local v2    # "o":I
    const/16 v8, 0x20

    ushr-long v8, v6, v8

    long-to-int v8, v8

    int-to-short v8, v8

    aput-short v8, v5, v3

    .line 94
    iget-object v5, p0, Lorg/apache/lucene/util/packed/Packed16ThreeBlocks;->blocks:[S

    add-int/lit8 v3, v2, 0x1

    .end local v2    # "o":I
    .restart local v3    # "o":I
    const/16 v8, 0x10

    ushr-long v8, v6, v8

    long-to-int v8, v8

    int-to-short v8, v8

    aput-short v8, v5, v2

    .line 95
    iget-object v5, p0, Lorg/apache/lucene/util/packed/Packed16ThreeBlocks;->blocks:[S

    add-int/lit8 v2, v3, 0x1

    .end local v3    # "o":I
    .restart local v2    # "o":I
    long-to-int v8, v6

    int-to-short v8, v8

    aput-short v8, v5, v3

    .line 91
    add-int/lit8 v1, v1, 0x1

    move v3, v2

    .end local v2    # "o":I
    .restart local v3    # "o":I
    goto :goto_0
.end method

.method public set(IJ)V
    .locals 6
    .param p1, "index"    # I
    .param p2, "value"    # J

    .prologue
    .line 78
    mul-int/lit8 v0, p1, 0x3

    .line 79
    .local v0, "o":I
    iget-object v1, p0, Lorg/apache/lucene/util/packed/Packed16ThreeBlocks;->blocks:[S

    const/16 v2, 0x20

    ushr-long v2, p2, v2

    long-to-int v2, v2

    int-to-short v2, v2

    aput-short v2, v1, v0

    .line 80
    iget-object v1, p0, Lorg/apache/lucene/util/packed/Packed16ThreeBlocks;->blocks:[S

    add-int/lit8 v2, v0, 0x1

    const/16 v3, 0x10

    ushr-long v4, p2, v3

    long-to-int v3, v4

    int-to-short v3, v3

    aput-short v3, v1, v2

    .line 81
    iget-object v1, p0, Lorg/apache/lucene/util/packed/Packed16ThreeBlocks;->blocks:[S

    add-int/lit8 v2, v0, 0x2

    long-to-int v3, p2

    int-to-short v3, v3

    aput-short v3, v1, v2

    .line 82
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 128
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "(bitsPerValue="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/util/packed/Packed16ThreeBlocks;->bitsPerValue:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 129
    const-string v1, ", size="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/lucene/util/packed/Packed16ThreeBlocks;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", elements.length="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/util/packed/Packed16ThreeBlocks;->blocks:[S

    array-length v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 128
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
