.class public final Lorg/apache/lucene/util/UnicodeUtil;
.super Ljava/lang/Object;
.source "UnicodeUtil.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final BIG_TERM:Lorg/apache/lucene/util/BytesRef;

.field private static final HALF_MASK:J = 0x3ffL

.field private static final HALF_SHIFT:J = 0xaL

.field private static final LEAD_SURROGATE_MIN_VALUE:I = 0xd800

.field private static final LEAD_SURROGATE_OFFSET_:I = 0xd7c0

.field private static final LEAD_SURROGATE_SHIFT_:I = 0xa

.field private static final SUPPLEMENTARY_MIN_VALUE:I = 0x10000

.field private static final SURROGATE_OFFSET:I = -0x35fdc00

.field private static final TRAIL_SURROGATE_MASK_:I = 0x3ff

.field private static final TRAIL_SURROGATE_MIN_VALUE:I = 0xdc00

.field private static final UNI_MAX_BMP:J = 0xffffL

.field public static final UNI_REPLACEMENT_CHAR:I = 0xfffd

.field public static final UNI_SUR_HIGH_END:I = 0xdbff

.field public static final UNI_SUR_HIGH_START:I = 0xd800

.field public static final UNI_SUR_LOW_END:I = 0xdfff

.field public static final UNI_SUR_LOW_START:I = 0xdc00

.field static final utf8CodeLength:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 96
    const-class v1, Lorg/apache/lucene/util/UnicodeUtil;

    invoke-virtual {v1}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    sput-boolean v1, Lorg/apache/lucene/util/UnicodeUtil;->$assertionsDisabled:Z

    .line 103
    new-instance v1, Lorg/apache/lucene/util/BytesRef;

    .line 104
    const/16 v2, 0xa

    new-array v2, v2, [B

    fill-array-data v2, :array_0

    .line 103
    invoke-direct {v1, v2}, Lorg/apache/lucene/util/BytesRef;-><init>([B)V

    sput-object v1, Lorg/apache/lucene/util/UnicodeUtil;->BIG_TERM:Lorg/apache/lucene/util/BytesRef;

    .line 400
    const/high16 v0, -0x80000000

    .line 401
    .local v0, "v":I
    const/16 v1, 0xf8

    new-array v1, v1, [I

    fill-array-data v1, :array_1

    sput-object v1, Lorg/apache/lucene/util/UnicodeUtil;->utf8CodeLength:[I

    .line 526
    return-void

    .line 96
    .end local v0    # "v":I
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 104
    nop

    :array_0
    .array-data 1
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
    .end array-data

    .line 401
    nop

    :array_1
    .array-data 4
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        -0x80000000
        0x2
        0x2
        0x2
        0x2
        0x2
        0x2
        0x2
        0x2
        0x2
        0x2
        0x2
        0x2
        0x2
        0x2
        0x2
        0x2
        0x2
        0x2
        0x2
        0x2
        0x2
        0x2
        0x2
        0x2
        0x2
        0x2
        0x2
        0x2
        0x2
        0x2
        0x2
        0x2
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x4
        0x4
        0x4
        0x4
        0x4
        0x4
        0x4
        0x4
    .end array-data
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static UTF16toUTF8(Ljava/lang/CharSequence;IILorg/apache/lucene/util/BytesRef;)V
    .locals 12
    .param p0, "s"    # Ljava/lang/CharSequence;
    .param p1, "offset"    # I
    .param p2, "length"    # I
    .param p3, "result"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    const v11, 0xdfff

    const v10, 0xdc00

    .line 241
    add-int v1, p1, p2

    .line 243
    .local v1, "end":I
    iget-object v4, p3, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 244
    .local v4, "out":[B
    const/4 v8, 0x0

    iput v8, p3, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 246
    mul-int/lit8 v3, p2, 0x4

    .line 247
    .local v3, "maxLen":I
    array-length v8, v4

    if-ge v8, v3, :cond_0

    .line 248
    new-array v4, v3, [B

    .end local v4    # "out":[B
    iput-object v4, p3, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 250
    .restart local v4    # "out":[B
    :cond_0
    const/4 v5, 0x0

    .line 251
    .local v5, "upto":I
    move v2, p1

    .local v2, "i":I
    move v6, v5

    .end local v5    # "upto":I
    .local v6, "upto":I
    :goto_0
    if-lt v2, v1, :cond_1

    .line 287
    iput v6, p3, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 288
    return-void

    .line 252
    :cond_1
    invoke-interface {p0, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    .line 254
    .local v0, "code":I
    const/16 v8, 0x80

    if-ge v0, v8, :cond_2

    .line 255
    add-int/lit8 v5, v6, 0x1

    .end local v6    # "upto":I
    .restart local v5    # "upto":I
    int-to-byte v8, v0

    aput-byte v8, v4, v6

    .line 251
    :goto_1
    add-int/lit8 v2, v2, 0x1

    move v6, v5

    .end local v5    # "upto":I
    .restart local v6    # "upto":I
    goto :goto_0

    .line 256
    :cond_2
    const/16 v8, 0x800

    if-ge v0, v8, :cond_3

    .line 257
    add-int/lit8 v5, v6, 0x1

    .end local v6    # "upto":I
    .restart local v5    # "upto":I
    shr-int/lit8 v8, v0, 0x6

    or-int/lit16 v8, v8, 0xc0

    int-to-byte v8, v8

    aput-byte v8, v4, v6

    .line 258
    add-int/lit8 v6, v5, 0x1

    .end local v5    # "upto":I
    .restart local v6    # "upto":I
    and-int/lit8 v8, v0, 0x3f

    or-int/lit16 v8, v8, 0x80

    int-to-byte v8, v8

    aput-byte v8, v4, v5

    move v5, v6

    .line 259
    .end local v6    # "upto":I
    .restart local v5    # "upto":I
    goto :goto_1

    .end local v5    # "upto":I
    .restart local v6    # "upto":I
    :cond_3
    const v8, 0xd800

    if-lt v0, v8, :cond_4

    if-le v0, v11, :cond_5

    .line 260
    :cond_4
    add-int/lit8 v5, v6, 0x1

    .end local v6    # "upto":I
    .restart local v5    # "upto":I
    shr-int/lit8 v8, v0, 0xc

    or-int/lit16 v8, v8, 0xe0

    int-to-byte v8, v8

    aput-byte v8, v4, v6

    .line 261
    add-int/lit8 v6, v5, 0x1

    .end local v5    # "upto":I
    .restart local v6    # "upto":I
    shr-int/lit8 v8, v0, 0x6

    and-int/lit8 v8, v8, 0x3f

    or-int/lit16 v8, v8, 0x80

    int-to-byte v8, v8

    aput-byte v8, v4, v5

    .line 262
    add-int/lit8 v5, v6, 0x1

    .end local v6    # "upto":I
    .restart local v5    # "upto":I
    and-int/lit8 v8, v0, 0x3f

    or-int/lit16 v8, v8, 0x80

    int-to-byte v8, v8

    aput-byte v8, v4, v6

    goto :goto_1

    .line 266
    .end local v5    # "upto":I
    .restart local v6    # "upto":I
    :cond_5
    if-ge v0, v10, :cond_6

    add-int/lit8 v8, v1, -0x1

    if-ge v2, v8, :cond_6

    .line 267
    add-int/lit8 v8, v2, 0x1

    invoke-interface {p0, v8}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v7

    .line 269
    .local v7, "utf32":I
    if-lt v7, v10, :cond_6

    if-gt v7, v11, :cond_6

    .line 270
    shl-int/lit8 v8, v0, 0xa

    add-int/2addr v8, v7

    const v9, -0x35fdc00

    add-int v7, v8, v9

    .line 271
    add-int/lit8 v2, v2, 0x1

    .line 272
    add-int/lit8 v5, v6, 0x1

    .end local v6    # "upto":I
    .restart local v5    # "upto":I
    shr-int/lit8 v8, v7, 0x12

    or-int/lit16 v8, v8, 0xf0

    int-to-byte v8, v8

    aput-byte v8, v4, v6

    .line 273
    add-int/lit8 v6, v5, 0x1

    .end local v5    # "upto":I
    .restart local v6    # "upto":I
    shr-int/lit8 v8, v7, 0xc

    and-int/lit8 v8, v8, 0x3f

    or-int/lit16 v8, v8, 0x80

    int-to-byte v8, v8

    aput-byte v8, v4, v5

    .line 274
    add-int/lit8 v5, v6, 0x1

    .end local v6    # "upto":I
    .restart local v5    # "upto":I
    shr-int/lit8 v8, v7, 0x6

    and-int/lit8 v8, v8, 0x3f

    or-int/lit16 v8, v8, 0x80

    int-to-byte v8, v8

    aput-byte v8, v4, v6

    .line 275
    add-int/lit8 v6, v5, 0x1

    .end local v5    # "upto":I
    .restart local v6    # "upto":I
    and-int/lit8 v8, v7, 0x3f

    or-int/lit16 v8, v8, 0x80

    int-to-byte v8, v8

    aput-byte v8, v4, v5

    move v5, v6

    .line 276
    .end local v6    # "upto":I
    .restart local v5    # "upto":I
    goto/16 :goto_1

    .line 281
    .end local v5    # "upto":I
    .end local v7    # "utf32":I
    .restart local v6    # "upto":I
    :cond_6
    add-int/lit8 v5, v6, 0x1

    .end local v6    # "upto":I
    .restart local v5    # "upto":I
    const/16 v8, -0x11

    aput-byte v8, v4, v6

    .line 282
    add-int/lit8 v6, v5, 0x1

    .end local v5    # "upto":I
    .restart local v6    # "upto":I
    const/16 v8, -0x41

    aput-byte v8, v4, v5

    .line 283
    add-int/lit8 v5, v6, 0x1

    .end local v6    # "upto":I
    .restart local v5    # "upto":I
    const/16 v8, -0x43

    aput-byte v8, v4, v6

    goto/16 :goto_1
.end method

.method public static UTF16toUTF8([CIILorg/apache/lucene/util/BytesRef;)V
    .locals 11
    .param p0, "source"    # [C
    .param p1, "offset"    # I
    .param p2, "length"    # I
    .param p3, "result"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 186
    const/4 v6, 0x0

    .line 187
    .local v6, "upto":I
    move v2, p1

    .line 188
    .local v2, "i":I
    add-int v1, p1, p2

    .line 189
    .local v1, "end":I
    iget-object v5, p3, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 191
    .local v5, "out":[B
    mul-int/lit8 v4, p2, 0x4

    .line 192
    .local v4, "maxLen":I
    array-length v9, v5

    if-ge v9, v4, :cond_0

    .line 193
    new-array v5, v4, [B

    .end local v5    # "out":[B
    iput-object v5, p3, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 194
    .restart local v5    # "out":[B
    :cond_0
    const/4 v9, 0x0

    iput v9, p3, Lorg/apache/lucene/util/BytesRef;->offset:I

    move v3, v2

    .end local v2    # "i":I
    .local v3, "i":I
    move v7, v6

    .line 196
    .end local v6    # "upto":I
    .local v7, "upto":I
    :goto_0
    if-lt v3, v1, :cond_1

    .line 233
    iput v7, p3, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 234
    return-void

    .line 198
    :cond_1
    add-int/lit8 v2, v3, 0x1

    .end local v3    # "i":I
    .restart local v2    # "i":I
    aget-char v0, p0, v3

    .line 200
    .local v0, "code":I
    const/16 v9, 0x80

    if-ge v0, v9, :cond_2

    .line 201
    add-int/lit8 v6, v7, 0x1

    .end local v7    # "upto":I
    .restart local v6    # "upto":I
    int-to-byte v9, v0

    aput-byte v9, v5, v7

    move v3, v2

    .end local v2    # "i":I
    .restart local v3    # "i":I
    move v7, v6

    .end local v6    # "upto":I
    .restart local v7    # "upto":I
    goto :goto_0

    .line 202
    .end local v3    # "i":I
    .restart local v2    # "i":I
    :cond_2
    const/16 v9, 0x800

    if-ge v0, v9, :cond_3

    .line 203
    add-int/lit8 v6, v7, 0x1

    .end local v7    # "upto":I
    .restart local v6    # "upto":I
    shr-int/lit8 v9, v0, 0x6

    or-int/lit16 v9, v9, 0xc0

    int-to-byte v9, v9

    aput-byte v9, v5, v7

    .line 204
    add-int/lit8 v7, v6, 0x1

    .end local v6    # "upto":I
    .restart local v7    # "upto":I
    and-int/lit8 v9, v0, 0x3f

    or-int/lit16 v9, v9, 0x80

    int-to-byte v9, v9

    aput-byte v9, v5, v6

    move v3, v2

    .line 205
    .end local v2    # "i":I
    .restart local v3    # "i":I
    goto :goto_0

    .end local v3    # "i":I
    .restart local v2    # "i":I
    :cond_3
    const v9, 0xd800

    if-lt v0, v9, :cond_4

    const v9, 0xdfff

    if-le v0, v9, :cond_5

    .line 206
    :cond_4
    add-int/lit8 v6, v7, 0x1

    .end local v7    # "upto":I
    .restart local v6    # "upto":I
    shr-int/lit8 v9, v0, 0xc

    or-int/lit16 v9, v9, 0xe0

    int-to-byte v9, v9

    aput-byte v9, v5, v7

    .line 207
    add-int/lit8 v7, v6, 0x1

    .end local v6    # "upto":I
    .restart local v7    # "upto":I
    shr-int/lit8 v9, v0, 0x6

    and-int/lit8 v9, v9, 0x3f

    or-int/lit16 v9, v9, 0x80

    int-to-byte v9, v9

    aput-byte v9, v5, v6

    .line 208
    add-int/lit8 v6, v7, 0x1

    .end local v7    # "upto":I
    .restart local v6    # "upto":I
    and-int/lit8 v9, v0, 0x3f

    or-int/lit16 v9, v9, 0x80

    int-to-byte v9, v9

    aput-byte v9, v5, v7

    move v3, v2

    .end local v2    # "i":I
    .restart local v3    # "i":I
    move v7, v6

    .line 209
    .end local v6    # "upto":I
    .restart local v7    # "upto":I
    goto :goto_0

    .line 212
    .end local v3    # "i":I
    .restart local v2    # "i":I
    :cond_5
    const v9, 0xdc00

    if-ge v0, v9, :cond_6

    if-ge v2, v1, :cond_6

    .line 213
    aget-char v8, p0, v2

    .line 215
    .local v8, "utf32":I
    const v9, 0xdc00

    if-lt v8, v9, :cond_6

    const v9, 0xdfff

    if-gt v8, v9, :cond_6

    .line 216
    shl-int/lit8 v9, v0, 0xa

    add-int/2addr v9, v8

    const v10, -0x35fdc00

    add-int v8, v9, v10

    .line 217
    add-int/lit8 v2, v2, 0x1

    .line 218
    add-int/lit8 v6, v7, 0x1

    .end local v7    # "upto":I
    .restart local v6    # "upto":I
    shr-int/lit8 v9, v8, 0x12

    or-int/lit16 v9, v9, 0xf0

    int-to-byte v9, v9

    aput-byte v9, v5, v7

    .line 219
    add-int/lit8 v7, v6, 0x1

    .end local v6    # "upto":I
    .restart local v7    # "upto":I
    shr-int/lit8 v9, v8, 0xc

    and-int/lit8 v9, v9, 0x3f

    or-int/lit16 v9, v9, 0x80

    int-to-byte v9, v9

    aput-byte v9, v5, v6

    .line 220
    add-int/lit8 v6, v7, 0x1

    .end local v7    # "upto":I
    .restart local v6    # "upto":I
    shr-int/lit8 v9, v8, 0x6

    and-int/lit8 v9, v9, 0x3f

    or-int/lit16 v9, v9, 0x80

    int-to-byte v9, v9

    aput-byte v9, v5, v7

    .line 221
    add-int/lit8 v7, v6, 0x1

    .end local v6    # "upto":I
    .restart local v7    # "upto":I
    and-int/lit8 v9, v8, 0x3f

    or-int/lit16 v9, v9, 0x80

    int-to-byte v9, v9

    aput-byte v9, v5, v6

    move v3, v2

    .line 222
    .end local v2    # "i":I
    .restart local v3    # "i":I
    goto/16 :goto_0

    .line 227
    .end local v3    # "i":I
    .end local v8    # "utf32":I
    .restart local v2    # "i":I
    :cond_6
    add-int/lit8 v6, v7, 0x1

    .end local v7    # "upto":I
    .restart local v6    # "upto":I
    const/16 v9, -0x11

    aput-byte v9, v5, v7

    .line 228
    add-int/lit8 v7, v6, 0x1

    .end local v6    # "upto":I
    .restart local v7    # "upto":I
    const/16 v9, -0x41

    aput-byte v9, v5, v6

    .line 229
    add-int/lit8 v6, v7, 0x1

    .end local v7    # "upto":I
    .restart local v6    # "upto":I
    const/16 v9, -0x43

    aput-byte v9, v5, v7

    move v3, v2

    .end local v2    # "i":I
    .restart local v3    # "i":I
    move v7, v6

    .end local v6    # "upto":I
    .restart local v7    # "upto":I
    goto/16 :goto_0
.end method

.method public static UTF16toUTF8WithHash([CIILorg/apache/lucene/util/BytesRef;)I
    .locals 12
    .param p0, "source"    # [C
    .param p1, "offset"    # I
    .param p2, "length"    # I
    .param p3, "result"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 128
    const/4 v2, 0x0

    .line 129
    .local v2, "hash":I
    const/4 v7, 0x0

    .line 130
    .local v7, "upto":I
    move v3, p1

    .line 131
    .local v3, "i":I
    add-int v1, p1, p2

    .line 132
    .local v1, "end":I
    iget-object v6, p3, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 134
    .local v6, "out":[B
    mul-int/lit8 v5, p2, 0x4

    .line 135
    .local v5, "maxLen":I
    array-length v10, v6

    if-ge v10, v5, :cond_0

    .line 136
    const/4 v10, 0x1

    invoke-static {v5, v10}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v10

    new-array v6, v10, [B

    .end local v6    # "out":[B
    iput-object v6, p3, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 137
    .restart local v6    # "out":[B
    :cond_0
    const/4 v10, 0x0

    iput v10, p3, Lorg/apache/lucene/util/BytesRef;->offset:I

    move v4, v3

    .end local v3    # "i":I
    .local v4, "i":I
    move v8, v7

    .line 139
    .end local v7    # "upto":I
    .local v8, "upto":I
    :goto_0
    if-lt v4, v1, :cond_1

    .line 176
    iput v8, p3, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 177
    return v2

    .line 141
    :cond_1
    add-int/lit8 v3, v4, 0x1

    .end local v4    # "i":I
    .restart local v3    # "i":I
    aget-char v0, p0, v4

    .line 143
    .local v0, "code":I
    const/16 v10, 0x80

    if-ge v0, v10, :cond_2

    .line 144
    mul-int/lit8 v10, v2, 0x1f

    add-int/lit8 v7, v8, 0x1

    .end local v8    # "upto":I
    .restart local v7    # "upto":I
    int-to-byte v11, v0

    aput-byte v11, v6, v8

    add-int v2, v10, v11

    move v4, v3

    .end local v3    # "i":I
    .restart local v4    # "i":I
    move v8, v7

    .line 145
    .end local v7    # "upto":I
    .restart local v8    # "upto":I
    goto :goto_0

    .end local v4    # "i":I
    .restart local v3    # "i":I
    :cond_2
    const/16 v10, 0x800

    if-ge v0, v10, :cond_3

    .line 146
    mul-int/lit8 v10, v2, 0x1f

    add-int/lit8 v7, v8, 0x1

    .end local v8    # "upto":I
    .restart local v7    # "upto":I
    shr-int/lit8 v11, v0, 0x6

    or-int/lit16 v11, v11, 0xc0

    int-to-byte v11, v11

    aput-byte v11, v6, v8

    add-int v2, v10, v11

    .line 147
    mul-int/lit8 v10, v2, 0x1f

    add-int/lit8 v8, v7, 0x1

    .end local v7    # "upto":I
    .restart local v8    # "upto":I
    and-int/lit8 v11, v0, 0x3f

    or-int/lit16 v11, v11, 0x80

    int-to-byte v11, v11

    aput-byte v11, v6, v7

    add-int v2, v10, v11

    move v4, v3

    .line 148
    .end local v3    # "i":I
    .restart local v4    # "i":I
    goto :goto_0

    .end local v4    # "i":I
    .restart local v3    # "i":I
    :cond_3
    const v10, 0xd800

    if-lt v0, v10, :cond_4

    const v10, 0xdfff

    if-le v0, v10, :cond_5

    .line 149
    :cond_4
    mul-int/lit8 v10, v2, 0x1f

    add-int/lit8 v7, v8, 0x1

    .end local v8    # "upto":I
    .restart local v7    # "upto":I
    shr-int/lit8 v11, v0, 0xc

    or-int/lit16 v11, v11, 0xe0

    int-to-byte v11, v11

    aput-byte v11, v6, v8

    add-int v2, v10, v11

    .line 150
    mul-int/lit8 v10, v2, 0x1f

    add-int/lit8 v8, v7, 0x1

    .end local v7    # "upto":I
    .restart local v8    # "upto":I
    shr-int/lit8 v11, v0, 0x6

    and-int/lit8 v11, v11, 0x3f

    or-int/lit16 v11, v11, 0x80

    int-to-byte v11, v11

    aput-byte v11, v6, v7

    add-int v2, v10, v11

    .line 151
    mul-int/lit8 v10, v2, 0x1f

    add-int/lit8 v7, v8, 0x1

    .end local v8    # "upto":I
    .restart local v7    # "upto":I
    and-int/lit8 v11, v0, 0x3f

    or-int/lit16 v11, v11, 0x80

    int-to-byte v11, v11

    aput-byte v11, v6, v8

    add-int v2, v10, v11

    move v4, v3

    .end local v3    # "i":I
    .restart local v4    # "i":I
    move v8, v7

    .line 152
    .end local v7    # "upto":I
    .restart local v8    # "upto":I
    goto :goto_0

    .line 155
    .end local v4    # "i":I
    .restart local v3    # "i":I
    :cond_5
    const v10, 0xdc00

    if-ge v0, v10, :cond_6

    if-ge v3, v1, :cond_6

    .line 156
    aget-char v9, p0, v3

    .line 158
    .local v9, "utf32":I
    const v10, 0xdc00

    if-lt v9, v10, :cond_6

    const v10, 0xdfff

    if-gt v9, v10, :cond_6

    .line 159
    shl-int/lit8 v10, v0, 0xa

    add-int/2addr v10, v9

    const v11, -0x35fdc00

    add-int v9, v10, v11

    .line 160
    add-int/lit8 v3, v3, 0x1

    .line 161
    mul-int/lit8 v10, v2, 0x1f

    add-int/lit8 v7, v8, 0x1

    .end local v8    # "upto":I
    .restart local v7    # "upto":I
    shr-int/lit8 v11, v9, 0x12

    or-int/lit16 v11, v11, 0xf0

    int-to-byte v11, v11

    aput-byte v11, v6, v8

    add-int v2, v10, v11

    .line 162
    mul-int/lit8 v10, v2, 0x1f

    add-int/lit8 v8, v7, 0x1

    .end local v7    # "upto":I
    .restart local v8    # "upto":I
    shr-int/lit8 v11, v9, 0xc

    and-int/lit8 v11, v11, 0x3f

    or-int/lit16 v11, v11, 0x80

    int-to-byte v11, v11

    aput-byte v11, v6, v7

    add-int v2, v10, v11

    .line 163
    mul-int/lit8 v10, v2, 0x1f

    add-int/lit8 v7, v8, 0x1

    .end local v8    # "upto":I
    .restart local v7    # "upto":I
    shr-int/lit8 v11, v9, 0x6

    and-int/lit8 v11, v11, 0x3f

    or-int/lit16 v11, v11, 0x80

    int-to-byte v11, v11

    aput-byte v11, v6, v8

    add-int v2, v10, v11

    .line 164
    mul-int/lit8 v10, v2, 0x1f

    add-int/lit8 v8, v7, 0x1

    .end local v7    # "upto":I
    .restart local v8    # "upto":I
    and-int/lit8 v11, v9, 0x3f

    or-int/lit16 v11, v11, 0x80

    int-to-byte v11, v11

    aput-byte v11, v6, v7

    add-int v2, v10, v11

    move v4, v3

    .line 165
    .end local v3    # "i":I
    .restart local v4    # "i":I
    goto/16 :goto_0

    .line 170
    .end local v4    # "i":I
    .end local v9    # "utf32":I
    .restart local v3    # "i":I
    :cond_6
    mul-int/lit8 v10, v2, 0x1f

    add-int/lit8 v7, v8, 0x1

    .end local v8    # "upto":I
    .restart local v7    # "upto":I
    const/16 v11, -0x11

    aput-byte v11, v6, v8

    add-int/lit8 v2, v10, -0x11

    .line 171
    mul-int/lit8 v10, v2, 0x1f

    add-int/lit8 v8, v7, 0x1

    .end local v7    # "upto":I
    .restart local v8    # "upto":I
    const/16 v11, -0x41

    aput-byte v11, v6, v7

    add-int/lit8 v2, v10, -0x41

    .line 172
    mul-int/lit8 v10, v2, 0x1f

    add-int/lit8 v7, v8, 0x1

    .end local v8    # "upto":I
    .restart local v7    # "upto":I
    const/16 v11, -0x43

    aput-byte v11, v6, v8

    add-int/lit8 v2, v10, -0x43

    move v4, v3

    .end local v3    # "i":I
    .restart local v4    # "i":I
    move v8, v7

    .end local v7    # "upto":I
    .restart local v8    # "upto":I
    goto/16 :goto_0
.end method

.method public static UTF8toUTF16(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/CharsRef;)V
    .locals 3
    .param p0, "bytesRef"    # Lorg/apache/lucene/util/BytesRef;
    .param p1, "chars"    # Lorg/apache/lucene/util/CharsRef;

    .prologue
    .line 645
    iget-object v0, p0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v1, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget v2, p0, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-static {v0, v1, v2, p1}, Lorg/apache/lucene/util/UnicodeUtil;->UTF8toUTF16([BIILorg/apache/lucene/util/CharsRef;)V

    .line 646
    return-void
.end method

.method public static UTF8toUTF16([BIILorg/apache/lucene/util/CharsRef;)V
    .locals 12
    .param p0, "utf8"    # [B
    .param p1, "offset"    # I
    .param p2, "length"    # I
    .param p3, "chars"    # Lorg/apache/lucene/util/CharsRef;

    .prologue
    .line 611
    const/4 v6, 0x0

    iput v6, p3, Lorg/apache/lucene/util/CharsRef;->offset:I

    .line 612
    .local v6, "out_offset":I
    iget-object v8, p3, Lorg/apache/lucene/util/CharsRef;->chars:[C

    invoke-static {v8, p2}, Lorg/apache/lucene/util/ArrayUtil;->grow([CI)[C

    move-result-object v5

    iput-object v5, p3, Lorg/apache/lucene/util/CharsRef;->chars:[C

    .line 613
    .local v5, "out":[C
    add-int v3, p1, p2

    .local v3, "limit":I
    move v7, v6

    .end local v6    # "out_offset":I
    .local v7, "out_offset":I
    move v4, p1

    .line 614
    .end local p1    # "offset":I
    .local v4, "offset":I
    :goto_0
    if-lt v4, v3, :cond_0

    .line 637
    iget v8, p3, Lorg/apache/lucene/util/CharsRef;->offset:I

    sub-int v8, v7, v8

    iput v8, p3, Lorg/apache/lucene/util/CharsRef;->length:I

    .line 638
    return-void

    .line 615
    :cond_0
    add-int/lit8 p1, v4, 0x1

    .end local v4    # "offset":I
    .restart local p1    # "offset":I
    aget-byte v8, p0, v4

    and-int/lit16 v0, v8, 0xff

    .line 616
    .local v0, "b":I
    const/16 v8, 0xc0

    if-ge v0, v8, :cond_2

    .line 617
    sget-boolean v8, Lorg/apache/lucene/util/UnicodeUtil;->$assertionsDisabled:Z

    if-nez v8, :cond_1

    const/16 v8, 0x80

    if-lt v0, v8, :cond_1

    new-instance v8, Ljava/lang/AssertionError;

    invoke-direct {v8}, Ljava/lang/AssertionError;-><init>()V

    throw v8

    .line 618
    :cond_1
    add-int/lit8 v6, v7, 0x1

    .end local v7    # "out_offset":I
    .restart local v6    # "out_offset":I
    int-to-char v8, v0

    aput-char v8, v5, v7

    move v7, v6

    .end local v6    # "out_offset":I
    .restart local v7    # "out_offset":I
    move v4, p1

    .line 619
    .end local p1    # "offset":I
    .restart local v4    # "offset":I
    goto :goto_0

    .end local v4    # "offset":I
    .restart local p1    # "offset":I
    :cond_2
    const/16 v8, 0xe0

    if-ge v0, v8, :cond_3

    .line 620
    add-int/lit8 v6, v7, 0x1

    .end local v7    # "out_offset":I
    .restart local v6    # "out_offset":I
    and-int/lit8 v8, v0, 0x1f

    shl-int/lit8 v8, v8, 0x6

    add-int/lit8 v4, p1, 0x1

    .end local p1    # "offset":I
    .restart local v4    # "offset":I
    aget-byte v9, p0, p1

    and-int/lit8 v9, v9, 0x3f

    add-int/2addr v8, v9

    int-to-char v8, v8

    aput-char v8, v5, v7

    move v7, v6

    .line 621
    .end local v6    # "out_offset":I
    .restart local v7    # "out_offset":I
    goto :goto_0

    .end local v4    # "offset":I
    .restart local p1    # "offset":I
    :cond_3
    const/16 v8, 0xf0

    if-ge v0, v8, :cond_4

    .line 622
    add-int/lit8 v6, v7, 0x1

    .end local v7    # "out_offset":I
    .restart local v6    # "out_offset":I
    and-int/lit8 v8, v0, 0xf

    shl-int/lit8 v8, v8, 0xc

    aget-byte v9, p0, p1

    and-int/lit8 v9, v9, 0x3f

    shl-int/lit8 v9, v9, 0x6

    add-int/2addr v8, v9

    add-int/lit8 v9, p1, 0x1

    aget-byte v9, p0, v9

    and-int/lit8 v9, v9, 0x3f

    add-int/2addr v8, v9

    int-to-char v8, v8

    aput-char v8, v5, v7

    .line 623
    add-int/lit8 p1, p1, 0x2

    move v7, v6

    .end local v6    # "out_offset":I
    .restart local v7    # "out_offset":I
    move v4, p1

    .line 624
    .end local p1    # "offset":I
    .restart local v4    # "offset":I
    goto :goto_0

    .line 625
    .end local v4    # "offset":I
    .restart local p1    # "offset":I
    :cond_4
    sget-boolean v8, Lorg/apache/lucene/util/UnicodeUtil;->$assertionsDisabled:Z

    if-nez v8, :cond_5

    const/16 v8, 0xf8

    if-lt v0, v8, :cond_5

    new-instance v8, Ljava/lang/AssertionError;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "b = 0x"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v8

    .line 626
    :cond_5
    and-int/lit8 v8, v0, 0x7

    shl-int/lit8 v8, v8, 0x12

    aget-byte v9, p0, p1

    and-int/lit8 v9, v9, 0x3f

    shl-int/lit8 v9, v9, 0xc

    add-int/2addr v8, v9

    add-int/lit8 v9, p1, 0x1

    aget-byte v9, p0, v9

    and-int/lit8 v9, v9, 0x3f

    shl-int/lit8 v9, v9, 0x6

    add-int/2addr v8, v9

    add-int/lit8 v9, p1, 0x2

    aget-byte v9, p0, v9

    and-int/lit8 v9, v9, 0x3f

    add-int v1, v8, v9

    .line 627
    .local v1, "ch":I
    add-int/lit8 p1, p1, 0x3

    .line 628
    int-to-long v8, v1

    const-wide/32 v10, 0xffff

    cmp-long v8, v8, v10

    if-gez v8, :cond_6

    .line 629
    add-int/lit8 v6, v7, 0x1

    .end local v7    # "out_offset":I
    .restart local v6    # "out_offset":I
    int-to-char v8, v1

    aput-char v8, v5, v7

    move v7, v6

    .end local v6    # "out_offset":I
    .restart local v7    # "out_offset":I
    move v4, p1

    .line 630
    .end local p1    # "offset":I
    .restart local v4    # "offset":I
    goto/16 :goto_0

    .line 631
    .end local v4    # "offset":I
    .restart local p1    # "offset":I
    :cond_6
    const/high16 v8, 0x10000

    sub-int v2, v1, v8

    .line 632
    .local v2, "chHalf":I
    add-int/lit8 v6, v7, 0x1

    .end local v7    # "out_offset":I
    .restart local v6    # "out_offset":I
    shr-int/lit8 v8, v2, 0xa

    const v9, 0xd800

    add-int/2addr v8, v9

    int-to-char v8, v8

    aput-char v8, v5, v7

    .line 633
    add-int/lit8 v7, v6, 0x1

    .end local v6    # "out_offset":I
    .restart local v7    # "out_offset":I
    int-to-long v8, v2

    const-wide/16 v10, 0x3ff

    and-long/2addr v8, v10

    const-wide/32 v10, 0xdc00

    add-long/2addr v8, v10

    long-to-int v8, v8

    int-to-char v8, v8

    aput-char v8, v5, v6

    move v4, p1

    .end local p1    # "offset":I
    .restart local v4    # "offset":I
    goto/16 :goto_0
.end method

.method public static UTF8toUTF32(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/IntsRef;)V
    .locals 12
    .param p0, "utf8"    # Lorg/apache/lucene/util/BytesRef;
    .param p1, "utf32"    # Lorg/apache/lucene/util/IntsRef;

    .prologue
    .line 471
    iget-object v10, p1, Lorg/apache/lucene/util/IntsRef;->ints:[I

    if-eqz v10, :cond_0

    iget-object v10, p1, Lorg/apache/lucene/util/IntsRef;->ints:[I

    array-length v10, v10

    iget v11, p0, Lorg/apache/lucene/util/BytesRef;->length:I

    if-ge v10, v11, :cond_1

    .line 472
    :cond_0
    iget v10, p0, Lorg/apache/lucene/util/BytesRef;->length:I

    new-array v10, v10, [I

    iput-object v10, p1, Lorg/apache/lucene/util/IntsRef;->ints:[I

    .line 474
    :cond_1
    const/4 v4, 0x0

    .line 475
    .local v4, "utf32Count":I
    iget v7, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 476
    .local v7, "utf8Upto":I
    iget-object v1, p1, Lorg/apache/lucene/util/IntsRef;->ints:[I

    .line 477
    .local v1, "ints":[I
    iget-object v0, p0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 478
    .local v0, "bytes":[B
    iget v10, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget v11, p0, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int v6, v10, v11

    .local v6, "utf8Limit":I
    move v8, v7

    .end local v7    # "utf8Upto":I
    .local v8, "utf8Upto":I
    move v5, v4

    .line 479
    .end local v4    # "utf32Count":I
    .local v5, "utf32Count":I
    :goto_0
    if-lt v8, v6, :cond_2

    .line 510
    const/4 v10, 0x0

    iput v10, p1, Lorg/apache/lucene/util/IntsRef;->offset:I

    .line 511
    iput v5, p1, Lorg/apache/lucene/util/IntsRef;->length:I

    .line 512
    return-void

    .line 480
    :cond_2
    sget-object v10, Lorg/apache/lucene/util/UnicodeUtil;->utf8CodeLength:[I

    aget-byte v11, v0, v8

    and-int/lit16 v11, v11, 0xff

    aget v3, v10, v11

    .line 481
    .local v3, "numBytes":I
    const/4 v9, 0x0

    .line 482
    .local v9, "v":I
    packed-switch v3, :pswitch_data_0

    .line 499
    new-instance v10, Ljava/lang/IllegalArgumentException;

    const-string v11, "invalid utf8"

    invoke-direct {v10, v11}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 484
    :pswitch_0
    add-int/lit8 v4, v5, 0x1

    .end local v5    # "utf32Count":I
    .restart local v4    # "utf32Count":I
    add-int/lit8 v7, v8, 0x1

    .end local v8    # "utf8Upto":I
    .restart local v7    # "utf8Upto":I
    aget-byte v10, v0, v8

    aput v10, v1, v5

    move v8, v7

    .end local v7    # "utf8Upto":I
    .restart local v8    # "utf8Upto":I
    move v5, v4

    .line 485
    .end local v4    # "utf32Count":I
    .restart local v5    # "utf32Count":I
    goto :goto_0

    .line 488
    :pswitch_1
    add-int/lit8 v7, v8, 0x1

    .end local v8    # "utf8Upto":I
    .restart local v7    # "utf8Upto":I
    aget-byte v10, v0, v8

    and-int/lit8 v9, v10, 0x1f

    .line 503
    :goto_1
    add-int v10, v7, v3

    add-int/lit8 v2, v10, -0x1

    .local v2, "limit":I
    move v8, v7

    .line 504
    .end local v7    # "utf8Upto":I
    .restart local v8    # "utf8Upto":I
    :goto_2
    if-lt v8, v2, :cond_3

    .line 507
    add-int/lit8 v4, v5, 0x1

    .end local v5    # "utf32Count":I
    .restart local v4    # "utf32Count":I
    aput v9, v1, v5

    move v5, v4

    .end local v4    # "utf32Count":I
    .restart local v5    # "utf32Count":I
    goto :goto_0

    .line 492
    .end local v2    # "limit":I
    :pswitch_2
    add-int/lit8 v7, v8, 0x1

    .end local v8    # "utf8Upto":I
    .restart local v7    # "utf8Upto":I
    aget-byte v10, v0, v8

    and-int/lit8 v9, v10, 0xf

    .line 493
    goto :goto_1

    .line 496
    .end local v7    # "utf8Upto":I
    .restart local v8    # "utf8Upto":I
    :pswitch_3
    add-int/lit8 v7, v8, 0x1

    .end local v8    # "utf8Upto":I
    .restart local v7    # "utf8Upto":I
    aget-byte v10, v0, v8

    and-int/lit8 v9, v10, 0x7

    .line 497
    goto :goto_1

    .line 505
    .end local v7    # "utf8Upto":I
    .restart local v2    # "limit":I
    .restart local v8    # "utf8Upto":I
    :cond_3
    shl-int/lit8 v10, v9, 0x6

    add-int/lit8 v7, v8, 0x1

    .end local v8    # "utf8Upto":I
    .restart local v7    # "utf8Upto":I
    aget-byte v11, v0, v8

    and-int/lit8 v11, v11, 0x3f

    or-int v9, v10, v11

    move v8, v7

    .end local v7    # "utf8Upto":I
    .restart local v8    # "utf8Upto":I
    goto :goto_2

    .line 482
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static codePointCount(Lorg/apache/lucene/util/BytesRef;)I
    .locals 6
    .param p0, "utf8"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 433
    iget v3, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 434
    .local v3, "pos":I
    iget v5, p0, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int v2, v3, v5

    .line 435
    .local v2, "limit":I
    iget-object v0, p0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 437
    .local v0, "bytes":[B
    const/4 v1, 0x0

    .line 438
    .local v1, "codePointCount":I
    :goto_0
    if-lt v3, v2, :cond_0

    .line 453
    if-le v3, v2, :cond_5

    new-instance v5, Ljava/lang/IllegalArgumentException;

    invoke-direct {v5}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v5

    .line 439
    :cond_0
    aget-byte v5, v0, v3

    and-int/lit16 v4, v5, 0xff

    .line 440
    .local v4, "v":I
    const/16 v5, 0x80

    if-ge v4, v5, :cond_1

    add-int/lit8 v3, v3, 0x1

    .line 438
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 441
    :cond_1
    const/16 v5, 0xc0

    if-lt v4, v5, :cond_4

    .line 442
    const/16 v5, 0xe0

    if-ge v4, v5, :cond_2

    add-int/lit8 v3, v3, 0x2

    goto :goto_1

    .line 443
    :cond_2
    const/16 v5, 0xf0

    if-ge v4, v5, :cond_3

    add-int/lit8 v3, v3, 0x3

    goto :goto_1

    .line 444
    :cond_3
    const/16 v5, 0xf8

    if-ge v4, v5, :cond_4

    add-int/lit8 v3, v3, 0x4

    goto :goto_1

    .line 449
    :cond_4
    new-instance v5, Ljava/lang/IllegalArgumentException;

    invoke-direct {v5}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v5

    .line 455
    .end local v4    # "v":I
    :cond_5
    return v1
.end method

.method public static newString([III)Ljava/lang/String;
    .locals 13
    .param p0, "codePoints"    # [I
    .param p1, "offset"    # I
    .param p2, "count"    # I

    .prologue
    const/4 v12, 0x0

    .line 539
    if-gez p2, :cond_0

    .line 540
    new-instance v8, Ljava/lang/IllegalArgumentException;

    invoke-direct {v8}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v8

    .line 542
    :cond_0
    new-array v0, p2, [C

    .line 543
    .local v0, "chars":[C
    const/4 v7, 0x0

    .line 544
    .local v7, "w":I
    move v5, p1

    .local v5, "r":I
    add-int v2, p1, p2

    .local v2, "e":I
    :goto_0
    if-lt v5, v2, :cond_1

    .line 569
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v0, v12, v7}, Ljava/lang/String;-><init>([CII)V

    return-object v8

    .line 545
    :cond_1
    aget v1, p0, v5

    .line 546
    .local v1, "cp":I
    if-ltz v1, :cond_2

    const v8, 0x10ffff

    if-le v1, v8, :cond_3

    .line 547
    :cond_2
    new-instance v8, Ljava/lang/IllegalArgumentException;

    invoke-direct {v8}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v8

    .line 560
    :catch_0
    move-exception v3

    .line 561
    .local v3, "ex":Ljava/lang/IndexOutOfBoundsException;
    array-length v8, p0

    int-to-double v8, v8

    add-int/lit8 v10, v7, 0x2

    int-to-double v10, v10

    mul-double/2addr v8, v10

    .line 562
    sub-int v10, v5, p1

    add-int/lit8 v10, v10, 0x1

    int-to-double v10, v10

    .line 561
    div-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v8

    double-to-int v4, v8

    .line 563
    .local v4, "newlen":I
    new-array v6, v4, [C

    .line 564
    .local v6, "temp":[C
    invoke-static {v0, v12, v6, v12, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 565
    move-object v0, v6

    .line 551
    .end local v3    # "ex":Ljava/lang/IndexOutOfBoundsException;
    .end local v4    # "newlen":I
    .end local v6    # "temp":[C
    :cond_3
    const/high16 v8, 0x10000

    if-ge v1, v8, :cond_4

    .line 552
    int-to-char v8, v1

    :try_start_0
    aput-char v8, v0, v7

    .line 553
    add-int/lit8 v7, v7, 0x1

    .line 544
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 555
    :cond_4
    const v8, 0xd7c0

    shr-int/lit8 v9, v1, 0xa

    add-int/2addr v8, v9

    int-to-char v8, v8

    aput-char v8, v0, v7

    .line 556
    add-int/lit8 v8, v7, 0x1

    const v9, 0xdc00

    and-int/lit16 v10, v1, 0x3ff

    add-int/2addr v9, v10

    int-to-char v9, v9

    aput-char v9, v0, v8
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 557
    add-int/lit8 v7, v7, 0x2

    goto :goto_1
.end method

.method public static toHexString(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    const v5, 0xdfff

    .line 574
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 575
    .local v2, "sb":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-lt v1, v3, :cond_0

    .line 598
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 576
    :cond_0
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 577
    .local v0, "ch":C
    if-lez v1, :cond_1

    .line 578
    const/16 v3, 0x20

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 580
    :cond_1
    const/16 v3, 0x80

    if-ge v0, v3, :cond_2

    .line 581
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 575
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 583
    :cond_2
    const v3, 0xd800

    if-lt v0, v3, :cond_4

    const v3, 0xdbff

    if-gt v0, v3, :cond_4

    .line 584
    const-string v3, "H:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 595
    :cond_3
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "0x"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 585
    :cond_4
    const v3, 0xdc00

    if-lt v0, v3, :cond_5

    if-gt v0, v5, :cond_5

    .line 586
    const-string v3, "L:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 587
    :cond_5
    if-le v0, v5, :cond_3

    .line 588
    const v3, 0xffff

    if-ne v0, v3, :cond_6

    .line 589
    const-string v3, "F:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 591
    :cond_6
    const-string v3, "E:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2
.end method

.method public static validUTF16String(Ljava/lang/CharSequence;)Z
    .locals 8
    .param p0, "s"    # Ljava/lang/CharSequence;

    .prologue
    const v7, 0xdfff

    const v6, 0xdc00

    const/4 v4, 0x0

    .line 346
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v3

    .line 347
    .local v3, "size":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v3, :cond_1

    .line 366
    const/4 v4, 0x1

    :cond_0
    :goto_1
    return v4

    .line 348
    :cond_1
    invoke-interface {p0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    .line 349
    .local v0, "ch":C
    const v5, 0xd800

    if-lt v0, v5, :cond_3

    const v5, 0xdbff

    if-gt v0, v5, :cond_3

    .line 350
    add-int/lit8 v5, v3, -0x1

    if-ge v1, v5, :cond_0

    .line 351
    add-int/lit8 v1, v1, 0x1

    .line 352
    invoke-interface {p0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    .line 353
    .local v2, "nextCH":C
    if-lt v2, v6, :cond_0

    if-gt v2, v7, :cond_0

    .line 347
    .end local v2    # "nextCH":C
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 361
    :cond_3
    if-lt v0, v6, :cond_2

    if-gt v0, v7, :cond_2

    goto :goto_1
.end method

.method public static validUTF16String([CI)Z
    .locals 7
    .param p0, "s"    # [C
    .param p1, "size"    # I

    .prologue
    const v6, 0xdfff

    const v5, 0xdc00

    const/4 v3, 0x0

    .line 370
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, p1, :cond_1

    .line 387
    const/4 v3, 0x1

    :cond_0
    :goto_1
    return v3

    .line 371
    :cond_1
    aget-char v0, p0, v1

    .line 372
    .local v0, "ch":C
    const v4, 0xd800

    if-lt v0, v4, :cond_3

    const v4, 0xdbff

    if-gt v0, v4, :cond_3

    .line 373
    add-int/lit8 v4, p1, -0x1

    if-ge v1, v4, :cond_0

    .line 374
    add-int/lit8 v1, v1, 0x1

    .line 375
    aget-char v2, p0, v1

    .line 376
    .local v2, "nextCH":C
    if-lt v2, v5, :cond_0

    if-gt v2, v6, :cond_0

    .line 370
    .end local v2    # "nextCH":C
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 382
    :cond_3
    if-lt v0, v5, :cond_2

    if-gt v0, v6, :cond_2

    goto :goto_1
.end method
