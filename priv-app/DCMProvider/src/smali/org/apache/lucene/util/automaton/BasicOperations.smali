.class public final Lorg/apache/lucene/util/automaton/BasicOperations;
.super Ljava/lang/Object;
.source "BasicOperations.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;,
        Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;,
        Lorg/apache/lucene/util/automaton/BasicOperations$TransitionList;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const-class v0, Lorg/apache/lucene/util/automaton/BasicOperations;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/automaton/BasicOperations;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addEpsilons(Lorg/apache/lucene/util/automaton/Automaton;Ljava/util/Collection;)V
    .locals 14
    .param p0, "a"    # Lorg/apache/lucene/util/automaton/Automaton;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/automaton/Automaton;",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/util/automaton/StatePair;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 715
    .local p1, "pairs":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/util/automaton/StatePair;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->expandSingleton()V

    .line 716
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 717
    .local v1, "forward":Ljava/util/HashMap;, "Ljava/util/HashMap<Lorg/apache/lucene/util/automaton/State;Ljava/util/HashSet<Lorg/apache/lucene/util/automaton/State;>;>;"
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 718
    .local v0, "back":Ljava/util/HashMap;, "Ljava/util/HashMap<Lorg/apache/lucene/util/automaton/State;Ljava/util/HashSet<Lorg/apache/lucene/util/automaton/State;>;>;"
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_1

    .line 733
    new-instance v9, Ljava/util/LinkedList;

    invoke-direct {v9, p1}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    .line 734
    .local v9, "worklist":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lorg/apache/lucene/util/automaton/StatePair;>;"
    new-instance v10, Ljava/util/HashSet;

    invoke-direct {v10, p1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 735
    .local v10, "workset":Ljava/util/HashSet;, "Ljava/util/HashSet<Lorg/apache/lucene/util/automaton/StatePair;>;"
    :cond_0
    invoke-virtual {v9}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v11

    if-eqz v11, :cond_4

    .line 763
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_7

    .line 765
    const/4 v11, 0x0

    iput-boolean v11, p0, Lorg/apache/lucene/util/automaton/Automaton;->deterministic:Z

    .line 767
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->clearNumberedStates()V

    .line 768
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->checkMinimizeAlways()V

    .line 769
    return-void

    .line 718
    .end local v9    # "worklist":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lorg/apache/lucene/util/automaton/StatePair;>;"
    .end local v10    # "workset":Ljava/util/HashSet;, "Ljava/util/HashSet<Lorg/apache/lucene/util/automaton/StatePair;>;"
    :cond_1
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/util/automaton/StatePair;

    .line 719
    .local v3, "p":Lorg/apache/lucene/util/automaton/StatePair;
    iget-object v12, v3, Lorg/apache/lucene/util/automaton/StatePair;->s1:Lorg/apache/lucene/util/automaton/State;

    invoke-virtual {v1, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/HashSet;

    .line 720
    .local v8, "to":Ljava/util/HashSet;, "Ljava/util/HashSet<Lorg/apache/lucene/util/automaton/State;>;"
    if-nez v8, :cond_2

    .line 721
    new-instance v8, Ljava/util/HashSet;

    .end local v8    # "to":Ljava/util/HashSet;, "Ljava/util/HashSet<Lorg/apache/lucene/util/automaton/State;>;"
    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    .line 722
    .restart local v8    # "to":Ljava/util/HashSet;, "Ljava/util/HashSet<Lorg/apache/lucene/util/automaton/State;>;"
    iget-object v12, v3, Lorg/apache/lucene/util/automaton/StatePair;->s1:Lorg/apache/lucene/util/automaton/State;

    invoke-virtual {v1, v12, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 724
    :cond_2
    iget-object v12, v3, Lorg/apache/lucene/util/automaton/StatePair;->s2:Lorg/apache/lucene/util/automaton/State;

    invoke-virtual {v8, v12}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 725
    iget-object v12, v3, Lorg/apache/lucene/util/automaton/StatePair;->s2:Lorg/apache/lucene/util/automaton/State;

    invoke-virtual {v0, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/HashSet;

    .line 726
    .local v2, "from":Ljava/util/HashSet;, "Ljava/util/HashSet<Lorg/apache/lucene/util/automaton/State;>;"
    if-nez v2, :cond_3

    .line 727
    new-instance v2, Ljava/util/HashSet;

    .end local v2    # "from":Ljava/util/HashSet;, "Ljava/util/HashSet<Lorg/apache/lucene/util/automaton/State;>;"
    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 728
    .restart local v2    # "from":Ljava/util/HashSet;, "Ljava/util/HashSet<Lorg/apache/lucene/util/automaton/State;>;"
    iget-object v12, v3, Lorg/apache/lucene/util/automaton/StatePair;->s2:Lorg/apache/lucene/util/automaton/State;

    invoke-virtual {v0, v12, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 730
    :cond_3
    iget-object v12, v3, Lorg/apache/lucene/util/automaton/StatePair;->s1:Lorg/apache/lucene/util/automaton/State;

    invoke-virtual {v2, v12}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 736
    .end local v2    # "from":Ljava/util/HashSet;, "Ljava/util/HashSet<Lorg/apache/lucene/util/automaton/State;>;"
    .end local v3    # "p":Lorg/apache/lucene/util/automaton/StatePair;
    .end local v8    # "to":Ljava/util/HashSet;, "Ljava/util/HashSet<Lorg/apache/lucene/util/automaton/State;>;"
    .restart local v9    # "worklist":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lorg/apache/lucene/util/automaton/StatePair;>;"
    .restart local v10    # "workset":Ljava/util/HashSet;, "Ljava/util/HashSet<Lorg/apache/lucene/util/automaton/StatePair;>;"
    :cond_4
    invoke-virtual {v9}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/util/automaton/StatePair;

    .line 737
    .restart local v3    # "p":Lorg/apache/lucene/util/automaton/StatePair;
    invoke-virtual {v10, v3}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 738
    iget-object v11, v3, Lorg/apache/lucene/util/automaton/StatePair;->s2:Lorg/apache/lucene/util/automaton/State;

    invoke-virtual {v1, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/HashSet;

    .line 739
    .restart local v8    # "to":Ljava/util/HashSet;, "Ljava/util/HashSet<Lorg/apache/lucene/util/automaton/State;>;"
    iget-object v11, v3, Lorg/apache/lucene/util/automaton/StatePair;->s1:Lorg/apache/lucene/util/automaton/State;

    invoke-virtual {v0, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/HashSet;

    .line 740
    .restart local v2    # "from":Ljava/util/HashSet;, "Ljava/util/HashSet<Lorg/apache/lucene/util/automaton/State;>;"
    if-eqz v8, :cond_0

    .line 741
    invoke-virtual {v8}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_5
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/lucene/util/automaton/State;

    .line 742
    .local v7, "s":Lorg/apache/lucene/util/automaton/State;
    new-instance v4, Lorg/apache/lucene/util/automaton/StatePair;

    iget-object v11, v3, Lorg/apache/lucene/util/automaton/StatePair;->s1:Lorg/apache/lucene/util/automaton/State;

    invoke-direct {v4, v11, v7}, Lorg/apache/lucene/util/automaton/StatePair;-><init>(Lorg/apache/lucene/util/automaton/State;Lorg/apache/lucene/util/automaton/State;)V

    .line 743
    .local v4, "pp":Lorg/apache/lucene/util/automaton/StatePair;
    invoke-interface {p1, v4}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_5

    .line 744
    invoke-interface {p1, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 745
    iget-object v11, v3, Lorg/apache/lucene/util/automaton/StatePair;->s1:Lorg/apache/lucene/util/automaton/State;

    invoke-virtual {v1, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/util/HashSet;

    invoke-virtual {v11, v7}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 746
    invoke-virtual {v0, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/util/HashSet;

    iget-object v13, v3, Lorg/apache/lucene/util/automaton/StatePair;->s1:Lorg/apache/lucene/util/automaton/State;

    invoke-virtual {v11, v13}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 747
    invoke-virtual {v9, v4}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 748
    invoke-virtual {v10, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 749
    if-eqz v2, :cond_5

    .line 750
    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_6
    :goto_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_5

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/util/automaton/State;

    .line 751
    .local v5, "q":Lorg/apache/lucene/util/automaton/State;
    new-instance v6, Lorg/apache/lucene/util/automaton/StatePair;

    iget-object v13, v3, Lorg/apache/lucene/util/automaton/StatePair;->s1:Lorg/apache/lucene/util/automaton/State;

    invoke-direct {v6, v5, v13}, Lorg/apache/lucene/util/automaton/StatePair;-><init>(Lorg/apache/lucene/util/automaton/State;Lorg/apache/lucene/util/automaton/State;)V

    .line 752
    .local v6, "qq":Lorg/apache/lucene/util/automaton/StatePair;
    invoke-virtual {v10, v6}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_6

    .line 753
    invoke-virtual {v9, v6}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 754
    invoke-virtual {v10, v6}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 763
    .end local v2    # "from":Ljava/util/HashSet;, "Ljava/util/HashSet<Lorg/apache/lucene/util/automaton/State;>;"
    .end local v3    # "p":Lorg/apache/lucene/util/automaton/StatePair;
    .end local v4    # "pp":Lorg/apache/lucene/util/automaton/StatePair;
    .end local v5    # "q":Lorg/apache/lucene/util/automaton/State;
    .end local v6    # "qq":Lorg/apache/lucene/util/automaton/StatePair;
    .end local v7    # "s":Lorg/apache/lucene/util/automaton/State;
    .end local v8    # "to":Ljava/util/HashSet;, "Ljava/util/HashSet<Lorg/apache/lucene/util/automaton/State;>;"
    :cond_7
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/util/automaton/StatePair;

    .line 764
    .restart local v3    # "p":Lorg/apache/lucene/util/automaton/StatePair;
    iget-object v12, v3, Lorg/apache/lucene/util/automaton/StatePair;->s1:Lorg/apache/lucene/util/automaton/State;

    iget-object v13, v3, Lorg/apache/lucene/util/automaton/StatePair;->s2:Lorg/apache/lucene/util/automaton/State;

    invoke-virtual {v12, v13}, Lorg/apache/lucene/util/automaton/State;->addEpsilon(Lorg/apache/lucene/util/automaton/State;)V

    goto/16 :goto_1
.end method

.method public static complement(Lorg/apache/lucene/util/automaton/Automaton;)Lorg/apache/lucene/util/automaton/Automaton;
    .locals 6
    .param p0, "a"    # Lorg/apache/lucene/util/automaton/Automaton;

    .prologue
    const/4 v2, 0x0

    .line 243
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->cloneExpandedIfRequired()Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object p0

    .line 244
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->determinize()V

    .line 245
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->totalize()V

    .line 246
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->getNumberedStates()[Lorg/apache/lucene/util/automaton/State;

    move-result-object v4

    array-length v5, v4

    move v3, v2

    :goto_0
    if-lt v3, v5, :cond_0

    .line 248
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->removeDeadTransitions()V

    .line 249
    return-object p0

    .line 246
    :cond_0
    aget-object v0, v4, v3

    .line 247
    .local v0, "p":Lorg/apache/lucene/util/automaton/State;
    iget-boolean v1, v0, Lorg/apache/lucene/util/automaton/State;->accept:Z

    if-eqz v1, :cond_1

    move v1, v2

    :goto_1
    iput-boolean v1, v0, Lorg/apache/lucene/util/automaton/State;->accept:Z

    .line 246
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    .line 247
    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public static concatenate(Ljava/util/List;)Lorg/apache/lucene/util/automaton/Automaton;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/util/automaton/Automaton;",
            ">;)",
            "Lorg/apache/lucene/util/automaton/Automaton;"
        }
    .end annotation

    .prologue
    .local p0, "l":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/util/automaton/Automaton;>;"
    const/4 v10, 0x0

    .line 94
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-static {}, Lorg/apache/lucene/util/automaton/BasicAutomata;->makeEmptyString()Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v4

    .line 137
    :goto_0
    return-object v4

    .line 95
    :cond_0
    const/4 v3, 0x1

    .line 96
    .local v3, "all_singleton":Z
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_2

    .line 101
    :goto_1
    if-eqz v3, :cond_4

    .line 102
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 103
    .local v4, "b":Ljava/lang/StringBuilder;
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-nez v11, :cond_3

    .line 105
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lorg/apache/lucene/util/automaton/BasicAutomata;->makeString(Ljava/lang/String;)Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v4

    goto :goto_0

    .line 96
    .end local v4    # "b":Ljava/lang/StringBuilder;
    :cond_2
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/util/automaton/Automaton;

    .line 97
    .local v0, "a":Lorg/apache/lucene/util/automaton/Automaton;
    invoke-virtual {v0}, Lorg/apache/lucene/util/automaton/Automaton;->isSingleton()Z

    move-result v12

    if-nez v12, :cond_1

    .line 98
    const/4 v3, 0x0

    .line 99
    goto :goto_1

    .line 103
    .end local v0    # "a":Lorg/apache/lucene/util/automaton/Automaton;
    .restart local v4    # "b":Ljava/lang/StringBuilder;
    :cond_3
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/util/automaton/Automaton;

    .line 104
    .restart local v0    # "a":Lorg/apache/lucene/util/automaton/Automaton;
    iget-object v11, v0, Lorg/apache/lucene/util/automaton/Automaton;->singleton:Ljava/lang/String;

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 107
    .end local v0    # "a":Lorg/apache/lucene/util/automaton/Automaton;
    .end local v4    # "b":Ljava/lang/StringBuilder;
    :cond_4
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_5
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_7

    .line 109
    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    .line 110
    .local v7, "ids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_3
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_8

    .line 112
    invoke-interface {v7}, Ljava/util/Set;->size()I

    move-result v11

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v12

    if-eq v11, v12, :cond_9

    const/4 v6, 0x1

    .line 113
    .local v6, "has_aliases":Z
    :goto_4
    invoke-interface {p0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/util/automaton/Automaton;

    .line 114
    .local v4, "b":Lorg/apache/lucene/util/automaton/Automaton;
    if-eqz v6, :cond_a

    invoke-virtual {v4}, Lorg/apache/lucene/util/automaton/Automaton;->cloneExpanded()Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v4

    .line 116
    :goto_5
    invoke-virtual {v4}, Lorg/apache/lucene/util/automaton/Automaton;->getAcceptStates()Ljava/util/Set;

    move-result-object v2

    .line 117
    .local v2, "ac":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/util/automaton/State;>;"
    const/4 v5, 0x1

    .line 118
    .local v5, "first":Z
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_6
    :goto_6
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_b

    .line 133
    iput-boolean v10, v4, Lorg/apache/lucene/util/automaton/Automaton;->deterministic:Z

    .line 135
    invoke-virtual {v4}, Lorg/apache/lucene/util/automaton/Automaton;->clearNumberedStates()V

    .line 136
    invoke-virtual {v4}, Lorg/apache/lucene/util/automaton/Automaton;->checkMinimizeAlways()V

    goto/16 :goto_0

    .line 107
    .end local v2    # "ac":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/util/automaton/State;>;"
    .end local v4    # "b":Lorg/apache/lucene/util/automaton/Automaton;
    .end local v5    # "first":Z
    .end local v6    # "has_aliases":Z
    .end local v7    # "ids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    :cond_7
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/util/automaton/Automaton;

    .line 108
    .restart local v0    # "a":Lorg/apache/lucene/util/automaton/Automaton;
    invoke-static {v0}, Lorg/apache/lucene/util/automaton/BasicOperations;->isEmpty(Lorg/apache/lucene/util/automaton/Automaton;)Z

    move-result v12

    if-eqz v12, :cond_5

    invoke-static {}, Lorg/apache/lucene/util/automaton/BasicAutomata;->makeEmpty()Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v4

    goto/16 :goto_0

    .line 110
    .end local v0    # "a":Lorg/apache/lucene/util/automaton/Automaton;
    .restart local v7    # "ids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    :cond_8
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/util/automaton/Automaton;

    .line 111
    .restart local v0    # "a":Lorg/apache/lucene/util/automaton/Automaton;
    invoke-static {v0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-interface {v7, v12}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .end local v0    # "a":Lorg/apache/lucene/util/automaton/Automaton;
    :cond_9
    move v6, v10

    .line 112
    goto :goto_4

    .line 115
    .restart local v4    # "b":Lorg/apache/lucene/util/automaton/Automaton;
    .restart local v6    # "has_aliases":Z
    :cond_a
    invoke-virtual {v4}, Lorg/apache/lucene/util/automaton/Automaton;->cloneExpandedIfRequired()Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v4

    goto :goto_5

    .line 118
    .restart local v2    # "ac":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/util/automaton/State;>;"
    .restart local v5    # "first":Z
    :cond_b
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/util/automaton/Automaton;

    .line 119
    .restart local v0    # "a":Lorg/apache/lucene/util/automaton/Automaton;
    if-eqz v5, :cond_c

    const/4 v5, 0x0

    goto :goto_6

    .line 121
    :cond_c
    invoke-virtual {v0}, Lorg/apache/lucene/util/automaton/Automaton;->isEmptyString()Z

    move-result v12

    if-nez v12, :cond_6

    .line 122
    move-object v1, v0

    .line 123
    .local v1, "aa":Lorg/apache/lucene/util/automaton/Automaton;
    if-eqz v6, :cond_e

    invoke-virtual {v1}, Lorg/apache/lucene/util/automaton/Automaton;->cloneExpanded()Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v1

    .line 125
    :goto_7
    invoke-virtual {v1}, Lorg/apache/lucene/util/automaton/Automaton;->getAcceptStates()Ljava/util/Set;

    move-result-object v8

    .line 126
    .local v8, "ns":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/util/automaton/State;>;"
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_d
    :goto_8
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-nez v13, :cond_f

    .line 131
    move-object v2, v8

    goto :goto_6

    .line 124
    .end local v8    # "ns":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/util/automaton/State;>;"
    :cond_e
    invoke-virtual {v1}, Lorg/apache/lucene/util/automaton/Automaton;->cloneExpandedIfRequired()Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v1

    goto :goto_7

    .line 126
    .restart local v8    # "ns":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/util/automaton/State;>;"
    :cond_f
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/apache/lucene/util/automaton/State;

    .line 127
    .local v9, "s":Lorg/apache/lucene/util/automaton/State;
    iput-boolean v10, v9, Lorg/apache/lucene/util/automaton/State;->accept:Z

    .line 128
    iget-object v13, v1, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    invoke-virtual {v9, v13}, Lorg/apache/lucene/util/automaton/State;->addEpsilon(Lorg/apache/lucene/util/automaton/State;)V

    .line 129
    iget-boolean v13, v9, Lorg/apache/lucene/util/automaton/State;->accept:Z

    if-eqz v13, :cond_d

    invoke-interface {v8, v9}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_8
.end method

.method public static concatenate(Lorg/apache/lucene/util/automaton/Automaton;Lorg/apache/lucene/util/automaton/Automaton;)Lorg/apache/lucene/util/automaton/Automaton;
    .locals 5
    .param p0, "a1"    # Lorg/apache/lucene/util/automaton/Automaton;
    .param p1, "a2"    # Lorg/apache/lucene/util/automaton/Automaton;

    .prologue
    const/4 v2, 0x0

    .line 61
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->isSingleton()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p1}, Lorg/apache/lucene/util/automaton/Automaton;->isSingleton()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 62
    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lorg/apache/lucene/util/automaton/Automaton;->singleton:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p1, Lorg/apache/lucene/util/automaton/Automaton;->singleton:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lorg/apache/lucene/util/automaton/BasicAutomata;->makeString(Ljava/lang/String;)Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object p0

    .line 84
    .end local p0    # "a1":Lorg/apache/lucene/util/automaton/Automaton;
    :goto_0
    return-object p0

    .line 63
    .restart local p0    # "a1":Lorg/apache/lucene/util/automaton/Automaton;
    :cond_0
    invoke-static {p0}, Lorg/apache/lucene/util/automaton/BasicOperations;->isEmpty(Lorg/apache/lucene/util/automaton/Automaton;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-static {p1}, Lorg/apache/lucene/util/automaton/BasicOperations;->isEmpty(Lorg/apache/lucene/util/automaton/Automaton;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 64
    :cond_1
    invoke-static {}, Lorg/apache/lucene/util/automaton/BasicAutomata;->makeEmpty()Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object p0

    goto :goto_0

    .line 68
    :cond_2
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->isSingleton()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {p1}, Lorg/apache/lucene/util/automaton/Automaton;->isDeterministic()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v0, 0x1

    .line 69
    .local v0, "deterministic":Z
    :goto_1
    if-ne p0, p1, :cond_4

    .line 70
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->cloneExpanded()Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object p0

    .line 71
    invoke-virtual {p1}, Lorg/apache/lucene/util/automaton/Automaton;->cloneExpanded()Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object p1

    .line 76
    :goto_2
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->getAcceptStates()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_5

    .line 80
    iput-boolean v0, p0, Lorg/apache/lucene/util/automaton/Automaton;->deterministic:Z

    .line 82
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->clearNumberedStates()V

    .line 83
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->checkMinimizeAlways()V

    goto :goto_0

    .end local v0    # "deterministic":Z
    :cond_3
    move v0, v2

    .line 68
    goto :goto_1

    .line 73
    .restart local v0    # "deterministic":Z
    :cond_4
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->cloneExpandedIfRequired()Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object p0

    .line 74
    invoke-virtual {p1}, Lorg/apache/lucene/util/automaton/Automaton;->cloneExpandedIfRequired()Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object p1

    goto :goto_2

    .line 76
    :cond_5
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/util/automaton/State;

    .line 77
    .local v1, "s":Lorg/apache/lucene/util/automaton/State;
    iput-boolean v2, v1, Lorg/apache/lucene/util/automaton/State;->accept:Z

    .line 78
    iget-object v4, p1, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    invoke-virtual {v1, v4}, Lorg/apache/lucene/util/automaton/State;->addEpsilon(Lorg/apache/lucene/util/automaton/State;)V

    goto :goto_3
.end method

.method public static determinize(Lorg/apache/lucene/util/automaton/Automaton;)V
    .locals 31
    .param p0, "a"    # Lorg/apache/lucene/util/automaton/Automaton;

    .prologue
    .line 588
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/lucene/util/automaton/Automaton;->deterministic:Z

    move/from16 v28, v0

    if-nez v28, :cond_0

    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/util/automaton/Automaton;->isSingleton()Z

    move-result v28

    if-eqz v28, :cond_1

    .line 703
    :cond_0
    :goto_0
    return-void

    .line 592
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/util/automaton/Automaton;->getNumberedStates()[Lorg/apache/lucene/util/automaton/State;

    move-result-object v4

    .line 595
    .local v4, "allStates":[Lorg/apache/lucene/util/automaton/State;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-boolean v6, v0, Lorg/apache/lucene/util/automaton/State;->accept:Z

    .line 596
    .local v6, "initAccept":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v7, v0, Lorg/apache/lucene/util/automaton/State;->number:I

    .line 597
    .local v7, "initNumber":I
    new-instance v28, Lorg/apache/lucene/util/automaton/State;

    invoke-direct/range {v28 .. v28}, Lorg/apache/lucene/util/automaton/State;-><init>()V

    move-object/from16 v0, v28

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    .line 598
    new-instance v8, Lorg/apache/lucene/util/automaton/SortedIntSet$FrozenIntSet;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    invoke-direct {v8, v7, v0}, Lorg/apache/lucene/util/automaton/SortedIntSet$FrozenIntSet;-><init>(ILorg/apache/lucene/util/automaton/State;)V

    .line 600
    .local v8, "initialset":Lorg/apache/lucene/util/automaton/SortedIntSet$FrozenIntSet;
    new-instance v27, Ljava/util/LinkedList;

    invoke-direct/range {v27 .. v27}, Ljava/util/LinkedList;-><init>()V

    .line 601
    .local v27, "worklist":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lorg/apache/lucene/util/automaton/SortedIntSet$FrozenIntSet;>;"
    new-instance v15, Ljava/util/HashMap;

    invoke-direct {v15}, Ljava/util/HashMap;-><init>()V

    .line 603
    .local v15, "newstate":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/util/automaton/SortedIntSet$FrozenIntSet;Lorg/apache/lucene/util/automaton/State;>;"
    move-object/from16 v0, v27

    invoke-virtual {v0, v8}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 605
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iput-boolean v6, v0, Lorg/apache/lucene/util/automaton/State;->accept:Z

    .line 606
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    invoke-interface {v15, v8, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 608
    const/4 v13, 0x0

    .line 609
    .local v13, "newStateUpto":I
    const/16 v28, 0x5

    move/from16 v0, v28

    new-array v14, v0, [Lorg/apache/lucene/util/automaton/State;

    .line 610
    .local v14, "newStatesArray":[Lorg/apache/lucene/util/automaton/State;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    move-object/from16 v28, v0

    aput-object v28, v14, v13

    .line 611
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iput v13, v0, Lorg/apache/lucene/util/automaton/State;->number:I

    .line 612
    add-int/lit8 v13, v13, 0x1

    .line 615
    new-instance v19, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;

    const/16 v28, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v28

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;-><init>(Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;)V

    .line 618
    .local v19, "points":Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;
    new-instance v24, Lorg/apache/lucene/util/automaton/SortedIntSet;

    const/16 v28, 0x5

    move-object/from16 v0, v24

    move/from16 v1, v28

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/automaton/SortedIntSet;-><init>(I)V

    .line 620
    .local v24, "statesSet":Lorg/apache/lucene/util/automaton/SortedIntSet;
    :cond_2
    invoke-virtual/range {v27 .. v27}, Ljava/util/LinkedList;->size()I

    move-result v28

    if-gtz v28, :cond_3

    .line 701
    const/16 v28, 0x1

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lorg/apache/lucene/util/automaton/Automaton;->deterministic:Z

    .line 702
    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v13}, Lorg/apache/lucene/util/automaton/Automaton;->setNumberedStates([Lorg/apache/lucene/util/automaton/State;I)V

    goto/16 :goto_0

    .line 621
    :cond_3
    invoke-virtual/range {v27 .. v27}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lorg/apache/lucene/util/automaton/SortedIntSet$FrozenIntSet;

    .line 624
    .local v22, "s":Lorg/apache/lucene/util/automaton/SortedIntSet$FrozenIntSet;
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    move-object/from16 v0, v22

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/SortedIntSet$FrozenIntSet;->values:[I

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    array-length v0, v0

    move/from16 v28, v0

    move/from16 v0, v28

    if-lt v5, v0, :cond_4

    .line 631
    move-object/from16 v0, v19

    iget v0, v0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;->count:I

    move/from16 v28, v0

    if-eqz v28, :cond_2

    .line 636
    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;->sort()V

    .line 638
    const/4 v10, -0x1

    .line 639
    .local v10, "lastPoint":I
    const/4 v3, 0x0

    .line 641
    .local v3, "accCount":I
    move-object/from16 v0, v22

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/SortedIntSet$FrozenIntSet;->state:Lorg/apache/lucene/util/automaton/State;

    move-object/from16 v21, v0

    .line 642
    .local v21, "r":Lorg/apache/lucene/util/automaton/State;
    const/4 v5, 0x0

    :goto_2
    move-object/from16 v0, v19

    iget v0, v0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;->count:I

    move/from16 v28, v0

    move/from16 v0, v28

    if-lt v5, v0, :cond_6

    .line 698
    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;->reset()V

    .line 699
    sget-boolean v28, Lorg/apache/lucene/util/automaton/BasicOperations;->$assertionsDisabled:Z

    if-nez v28, :cond_2

    move-object/from16 v0, v24

    iget v0, v0, Lorg/apache/lucene/util/automaton/SortedIntSet;->upto:I

    move/from16 v28, v0

    if-eqz v28, :cond_2

    new-instance v28, Ljava/lang/AssertionError;

    new-instance v29, Ljava/lang/StringBuilder;

    const-string v30, "upto="

    invoke-direct/range {v29 .. v30}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v24

    iget v0, v0, Lorg/apache/lucene/util/automaton/SortedIntSet;->upto:I

    move/from16 v30, v0

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-direct/range {v28 .. v29}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v28

    .line 625
    .end local v3    # "accCount":I
    .end local v10    # "lastPoint":I
    .end local v21    # "r":Lorg/apache/lucene/util/automaton/State;
    :cond_4
    move-object/from16 v0, v22

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/SortedIntSet$FrozenIntSet;->values:[I

    move-object/from16 v28, v0

    aget v28, v28, v5

    aget-object v23, v4, v28

    .line 626
    .local v23, "s0":Lorg/apache/lucene/util/automaton/State;
    const/4 v9, 0x0

    .local v9, "j":I
    :goto_3
    move-object/from16 v0, v23

    iget v0, v0, Lorg/apache/lucene/util/automaton/State;->numTransitions:I

    move/from16 v28, v0

    move/from16 v0, v28

    if-lt v9, v0, :cond_5

    .line 624
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 627
    :cond_5
    move-object/from16 v0, v23

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/State;->transitionsArray:[Lorg/apache/lucene/util/automaton/Transition;

    move-object/from16 v28, v0

    aget-object v28, v28, v9

    move-object/from16 v0, v19

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;->add(Lorg/apache/lucene/util/automaton/Transition;)V

    .line 626
    add-int/lit8 v9, v9, 0x1

    goto :goto_3

    .line 644
    .end local v9    # "j":I
    .end local v23    # "s0":Lorg/apache/lucene/util/automaton/State;
    .restart local v3    # "accCount":I
    .restart local v10    # "lastPoint":I
    .restart local v21    # "r":Lorg/apache/lucene/util/automaton/State;
    :cond_6
    move-object/from16 v0, v19

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;->points:[Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;

    move-object/from16 v28, v0

    aget-object v28, v28, v5

    move-object/from16 v0, v28

    iget v0, v0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;->point:I

    move/from16 v18, v0

    .line 646
    .local v18, "point":I
    move-object/from16 v0, v24

    iget v0, v0, Lorg/apache/lucene/util/automaton/SortedIntSet;->upto:I

    move/from16 v28, v0

    if-lez v28, :cond_a

    .line 647
    sget-boolean v28, Lorg/apache/lucene/util/automaton/BasicOperations;->$assertionsDisabled:Z

    if-nez v28, :cond_7

    const/16 v28, -0x1

    move/from16 v0, v28

    if-ne v10, v0, :cond_7

    new-instance v28, Ljava/lang/AssertionError;

    invoke-direct/range {v28 .. v28}, Ljava/lang/AssertionError;-><init>()V

    throw v28

    .line 649
    :cond_7
    invoke-virtual/range {v24 .. v24}, Lorg/apache/lucene/util/automaton/SortedIntSet;->computeHash()V

    .line 651
    move-object/from16 v0, v24

    invoke-interface {v15, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lorg/apache/lucene/util/automaton/State;

    .line 652
    .local v20, "q":Lorg/apache/lucene/util/automaton/State;
    if-nez v20, :cond_c

    .line 653
    new-instance v20, Lorg/apache/lucene/util/automaton/State;

    .end local v20    # "q":Lorg/apache/lucene/util/automaton/State;
    invoke-direct/range {v20 .. v20}, Lorg/apache/lucene/util/automaton/State;-><init>()V

    .line 654
    .restart local v20    # "q":Lorg/apache/lucene/util/automaton/State;
    move-object/from16 v0, v24

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/automaton/SortedIntSet;->freeze(Lorg/apache/lucene/util/automaton/State;)Lorg/apache/lucene/util/automaton/SortedIntSet$FrozenIntSet;

    move-result-object v17

    .line 655
    .local v17, "p":Lorg/apache/lucene/util/automaton/SortedIntSet$FrozenIntSet;
    move-object/from16 v0, v27

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 656
    array-length v0, v14

    move/from16 v28, v0

    move/from16 v0, v28

    if-ne v13, v0, :cond_8

    .line 657
    add-int/lit8 v28, v13, 0x1

    sget v29, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_REF:I

    invoke-static/range {v28 .. v29}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v28

    move/from16 v0, v28

    new-array v12, v0, [Lorg/apache/lucene/util/automaton/State;

    .line 658
    .local v12, "newArray":[Lorg/apache/lucene/util/automaton/State;
    const/16 v28, 0x0

    const/16 v29, 0x0

    move/from16 v0, v28

    move/from16 v1, v29

    invoke-static {v14, v0, v12, v1, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 659
    move-object v14, v12

    .line 661
    .end local v12    # "newArray":[Lorg/apache/lucene/util/automaton/State;
    :cond_8
    aput-object v20, v14, v13

    .line 662
    move-object/from16 v0, v20

    iput v13, v0, Lorg/apache/lucene/util/automaton/State;->number:I

    .line 663
    add-int/lit8 v13, v13, 0x1

    .line 664
    if-lez v3, :cond_b

    const/16 v28, 0x1

    :goto_4
    move/from16 v0, v28

    move-object/from16 v1, v20

    iput-boolean v0, v1, Lorg/apache/lucene/util/automaton/State;->accept:Z

    .line 665
    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-interface {v15, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 670
    .end local v17    # "p":Lorg/apache/lucene/util/automaton/SortedIntSet$FrozenIntSet;
    :cond_9
    new-instance v28, Lorg/apache/lucene/util/automaton/Transition;

    add-int/lit8 v29, v18, -0x1

    move-object/from16 v0, v28

    move/from16 v1, v29

    move-object/from16 v2, v20

    invoke-direct {v0, v10, v1, v2}, Lorg/apache/lucene/util/automaton/Transition;-><init>(IILorg/apache/lucene/util/automaton/State;)V

    move-object/from16 v0, v21

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/automaton/State;->addTransition(Lorg/apache/lucene/util/automaton/Transition;)V

    .line 675
    .end local v20    # "q":Lorg/apache/lucene/util/automaton/State;
    :cond_a
    move-object/from16 v0, v19

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;->points:[Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;

    move-object/from16 v28, v0

    aget-object v28, v28, v5

    move-object/from16 v0, v28

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;->ends:Lorg/apache/lucene/util/automaton/BasicOperations$TransitionList;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/BasicOperations$TransitionList;->transitions:[Lorg/apache/lucene/util/automaton/Transition;

    move-object/from16 v26, v0

    .line 676
    .local v26, "transitions":[Lorg/apache/lucene/util/automaton/Transition;
    move-object/from16 v0, v19

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;->points:[Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;

    move-object/from16 v28, v0

    aget-object v28, v28, v5

    move-object/from16 v0, v28

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;->ends:Lorg/apache/lucene/util/automaton/BasicOperations$TransitionList;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v11, v0, Lorg/apache/lucene/util/automaton/BasicOperations$TransitionList;->count:I

    .line 677
    .local v11, "limit":I
    const/4 v9, 0x0

    .restart local v9    # "j":I
    :goto_5
    if-lt v9, v11, :cond_e

    .line 683
    move-object/from16 v0, v19

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;->points:[Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;

    move-object/from16 v28, v0

    aget-object v28, v28, v5

    move-object/from16 v0, v28

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;->ends:Lorg/apache/lucene/util/automaton/BasicOperations$TransitionList;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    move/from16 v0, v29

    move-object/from16 v1, v28

    iput v0, v1, Lorg/apache/lucene/util/automaton/BasicOperations$TransitionList;->count:I

    .line 687
    move-object/from16 v0, v19

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;->points:[Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;

    move-object/from16 v28, v0

    aget-object v28, v28, v5

    move-object/from16 v0, v28

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;->starts:Lorg/apache/lucene/util/automaton/BasicOperations$TransitionList;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/BasicOperations$TransitionList;->transitions:[Lorg/apache/lucene/util/automaton/Transition;

    move-object/from16 v26, v0

    .line 688
    move-object/from16 v0, v19

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;->points:[Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;

    move-object/from16 v28, v0

    aget-object v28, v28, v5

    move-object/from16 v0, v28

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;->starts:Lorg/apache/lucene/util/automaton/BasicOperations$TransitionList;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v11, v0, Lorg/apache/lucene/util/automaton/BasicOperations$TransitionList;->count:I

    .line 689
    const/4 v9, 0x0

    :goto_6
    if-lt v9, v11, :cond_10

    .line 695
    move/from16 v10, v18

    .line 696
    move-object/from16 v0, v19

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;->points:[Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;

    move-object/from16 v28, v0

    aget-object v28, v28, v5

    move-object/from16 v0, v28

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;->starts:Lorg/apache/lucene/util/automaton/BasicOperations$TransitionList;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    move/from16 v0, v29

    move-object/from16 v1, v28

    iput v0, v1, Lorg/apache/lucene/util/automaton/BasicOperations$TransitionList;->count:I

    .line 642
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_2

    .line 664
    .end local v9    # "j":I
    .end local v11    # "limit":I
    .end local v26    # "transitions":[Lorg/apache/lucene/util/automaton/Transition;
    .restart local v17    # "p":Lorg/apache/lucene/util/automaton/SortedIntSet$FrozenIntSet;
    .restart local v20    # "q":Lorg/apache/lucene/util/automaton/State;
    :cond_b
    const/16 v28, 0x0

    goto/16 :goto_4

    .line 667
    .end local v17    # "p":Lorg/apache/lucene/util/automaton/SortedIntSet$FrozenIntSet;
    :cond_c
    sget-boolean v28, Lorg/apache/lucene/util/automaton/BasicOperations;->$assertionsDisabled:Z

    if-nez v28, :cond_9

    if-lez v3, :cond_d

    const/16 v28, 0x1

    :goto_7
    move-object/from16 v0, v20

    iget-boolean v0, v0, Lorg/apache/lucene/util/automaton/State;->accept:Z

    move/from16 v29, v0

    move/from16 v0, v28

    move/from16 v1, v29

    if-eq v0, v1, :cond_9

    new-instance v28, Ljava/lang/AssertionError;

    new-instance v29, Ljava/lang/StringBuilder;

    const-string v30, "accCount="

    invoke-direct/range {v29 .. v30}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, " vs existing accept="

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v20

    iget-boolean v0, v0, Lorg/apache/lucene/util/automaton/State;->accept:Z

    move/from16 v30, v0

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, " states="

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-direct/range {v28 .. v29}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v28

    :cond_d
    const/16 v28, 0x0

    goto :goto_7

    .line 678
    .end local v20    # "q":Lorg/apache/lucene/util/automaton/State;
    .restart local v9    # "j":I
    .restart local v11    # "limit":I
    .restart local v26    # "transitions":[Lorg/apache/lucene/util/automaton/Transition;
    :cond_e
    aget-object v25, v26, v9

    .line 679
    .local v25, "t":Lorg/apache/lucene/util/automaton/Transition;
    move-object/from16 v0, v25

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/Transition;->to:Lorg/apache/lucene/util/automaton/State;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lorg/apache/lucene/util/automaton/State;->number:I

    move/from16 v28, v0

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    .line 680
    .local v16, "num":Ljava/lang/Integer;
    invoke-virtual/range {v16 .. v16}, Ljava/lang/Integer;->intValue()I

    move-result v28

    move-object/from16 v0, v24

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/automaton/SortedIntSet;->decr(I)V

    .line 681
    move-object/from16 v0, v25

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/Transition;->to:Lorg/apache/lucene/util/automaton/State;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-boolean v0, v0, Lorg/apache/lucene/util/automaton/State;->accept:Z

    move/from16 v28, v0

    if-eqz v28, :cond_f

    const/16 v28, 0x1

    :goto_8
    sub-int v3, v3, v28

    .line 677
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_5

    .line 681
    :cond_f
    const/16 v28, 0x0

    goto :goto_8

    .line 690
    .end local v16    # "num":Ljava/lang/Integer;
    .end local v25    # "t":Lorg/apache/lucene/util/automaton/Transition;
    :cond_10
    aget-object v25, v26, v9

    .line 691
    .restart local v25    # "t":Lorg/apache/lucene/util/automaton/Transition;
    move-object/from16 v0, v25

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/Transition;->to:Lorg/apache/lucene/util/automaton/State;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lorg/apache/lucene/util/automaton/State;->number:I

    move/from16 v28, v0

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    .line 692
    .restart local v16    # "num":Ljava/lang/Integer;
    invoke-virtual/range {v16 .. v16}, Ljava/lang/Integer;->intValue()I

    move-result v28

    move-object/from16 v0, v24

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/automaton/SortedIntSet;->incr(I)V

    .line 693
    move-object/from16 v0, v25

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/Transition;->to:Lorg/apache/lucene/util/automaton/State;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-boolean v0, v0, Lorg/apache/lucene/util/automaton/State;->accept:Z

    move/from16 v28, v0

    if-eqz v28, :cond_11

    const/16 v28, 0x1

    :goto_9
    add-int v3, v3, v28

    .line 689
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_6

    .line 693
    :cond_11
    const/16 v28, 0x0

    goto :goto_9
.end method

.method public static intersection(Lorg/apache/lucene/util/automaton/Automaton;Lorg/apache/lucene/util/automaton/Automaton;)Lorg/apache/lucene/util/automaton/Automaton;
    .locals 21
    .param p0, "a1"    # Lorg/apache/lucene/util/automaton/Automaton;
    .param p1, "a2"    # Lorg/apache/lucene/util/automaton/Automaton;

    .prologue
    .line 278
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/util/automaton/Automaton;->isSingleton()Z

    move-result v18

    if-eqz v18, :cond_1

    .line 279
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/Automaton;->singleton:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Lorg/apache/lucene/util/automaton/BasicOperations;->run(Lorg/apache/lucene/util/automaton/Automaton;Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_0

    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/util/automaton/Automaton;->cloneIfRequired()Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v4

    .line 322
    :goto_0
    return-object v4

    .line 280
    :cond_0
    invoke-static {}, Lorg/apache/lucene/util/automaton/BasicAutomata;->makeEmpty()Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v4

    goto :goto_0

    .line 282
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/util/automaton/Automaton;->isSingleton()Z

    move-result v18

    if-eqz v18, :cond_3

    .line 283
    move-object/from16 v0, p1

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/Automaton;->singleton:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Lorg/apache/lucene/util/automaton/BasicOperations;->run(Lorg/apache/lucene/util/automaton/Automaton;Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_2

    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/util/automaton/Automaton;->cloneIfRequired()Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v4

    goto :goto_0

    .line 284
    :cond_2
    invoke-static {}, Lorg/apache/lucene/util/automaton/BasicAutomata;->makeEmpty()Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v4

    goto :goto_0

    .line 286
    :cond_3
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    if-ne v0, v1, :cond_4

    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/util/automaton/Automaton;->cloneIfRequired()Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v4

    goto :goto_0

    .line 287
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/util/automaton/Automaton;->getSortedTransitions()[[Lorg/apache/lucene/util/automaton/Transition;

    move-result-object v15

    .line 288
    .local v15, "transitions1":[[Lorg/apache/lucene/util/automaton/Transition;
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/util/automaton/Automaton;->getSortedTransitions()[[Lorg/apache/lucene/util/automaton/Transition;

    move-result-object v16

    .line 289
    .local v16, "transitions2":[[Lorg/apache/lucene/util/automaton/Transition;
    new-instance v4, Lorg/apache/lucene/util/automaton/Automaton;

    invoke-direct {v4}, Lorg/apache/lucene/util/automaton/Automaton;-><init>()V

    .line 290
    .local v4, "c":Lorg/apache/lucene/util/automaton/Automaton;
    new-instance v17, Ljava/util/LinkedList;

    invoke-direct/range {v17 .. v17}, Ljava/util/LinkedList;-><init>()V

    .line 291
    .local v17, "worklist":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lorg/apache/lucene/util/automaton/StatePair;>;"
    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    .line 292
    .local v9, "newstates":Ljava/util/HashMap;, "Ljava/util/HashMap<Lorg/apache/lucene/util/automaton/StatePair;Lorg/apache/lucene/util/automaton/StatePair;>;"
    new-instance v10, Lorg/apache/lucene/util/automaton/StatePair;

    iget-object v0, v4, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    move-object/from16 v19, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    move-object/from16 v20, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-direct {v10, v0, v1, v2}, Lorg/apache/lucene/util/automaton/StatePair;-><init>(Lorg/apache/lucene/util/automaton/State;Lorg/apache/lucene/util/automaton/State;Lorg/apache/lucene/util/automaton/State;)V

    .line 293
    .local v10, "p":Lorg/apache/lucene/util/automaton/StatePair;
    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 294
    invoke-virtual {v9, v10, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 295
    :cond_5
    invoke-virtual/range {v17 .. v17}, Ljava/util/LinkedList;->size()I

    move-result v18

    if-gtz v18, :cond_6

    .line 319
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/lucene/util/automaton/Automaton;->deterministic:Z

    move/from16 v18, v0

    if-eqz v18, :cond_10

    move-object/from16 v0, p1

    iget-boolean v0, v0, Lorg/apache/lucene/util/automaton/Automaton;->deterministic:Z

    move/from16 v18, v0

    if-eqz v18, :cond_10

    const/16 v18, 0x1

    :goto_1
    move/from16 v0, v18

    iput-boolean v0, v4, Lorg/apache/lucene/util/automaton/Automaton;->deterministic:Z

    .line 320
    invoke-virtual {v4}, Lorg/apache/lucene/util/automaton/Automaton;->removeDeadTransitions()V

    .line 321
    invoke-virtual {v4}, Lorg/apache/lucene/util/automaton/Automaton;->checkMinimizeAlways()V

    goto/16 :goto_0

    .line 296
    :cond_6
    invoke-virtual/range {v17 .. v17}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v10

    .end local v10    # "p":Lorg/apache/lucene/util/automaton/StatePair;
    check-cast v10, Lorg/apache/lucene/util/automaton/StatePair;

    .line 297
    .restart local v10    # "p":Lorg/apache/lucene/util/automaton/StatePair;
    iget-object v0, v10, Lorg/apache/lucene/util/automaton/StatePair;->s:Lorg/apache/lucene/util/automaton/State;

    move-object/from16 v19, v0

    iget-object v0, v10, Lorg/apache/lucene/util/automaton/StatePair;->s1:Lorg/apache/lucene/util/automaton/State;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-boolean v0, v0, Lorg/apache/lucene/util/automaton/State;->accept:Z

    move/from16 v18, v0

    if-eqz v18, :cond_9

    iget-object v0, v10, Lorg/apache/lucene/util/automaton/StatePair;->s2:Lorg/apache/lucene/util/automaton/State;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-boolean v0, v0, Lorg/apache/lucene/util/automaton/State;->accept:Z

    move/from16 v18, v0

    if-eqz v18, :cond_9

    const/16 v18, 0x1

    :goto_2
    move/from16 v0, v18

    move-object/from16 v1, v19

    iput-boolean v0, v1, Lorg/apache/lucene/util/automaton/State;->accept:Z

    .line 298
    iget-object v0, v10, Lorg/apache/lucene/util/automaton/StatePair;->s1:Lorg/apache/lucene/util/automaton/State;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Lorg/apache/lucene/util/automaton/State;->number:I

    move/from16 v18, v0

    aget-object v13, v15, v18

    .line 299
    .local v13, "t1":[Lorg/apache/lucene/util/automaton/Transition;
    iget-object v0, v10, Lorg/apache/lucene/util/automaton/StatePair;->s2:Lorg/apache/lucene/util/automaton/State;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Lorg/apache/lucene/util/automaton/State;->number:I

    move/from16 v18, v0

    aget-object v14, v16, v18

    .line 300
    .local v14, "t2":[Lorg/apache/lucene/util/automaton/Transition;
    const/4 v7, 0x0

    .local v7, "n1":I
    const/4 v3, 0x0

    .local v3, "b2":I
    :goto_3
    array-length v0, v13

    move/from16 v18, v0

    move/from16 v0, v18

    if-ge v7, v0, :cond_5

    .line 301
    :goto_4
    array-length v0, v14

    move/from16 v18, v0

    move/from16 v0, v18

    if-ge v3, v0, :cond_7

    aget-object v18, v14, v3

    move-object/from16 v0, v18

    iget v0, v0, Lorg/apache/lucene/util/automaton/Transition;->max:I

    move/from16 v18, v0

    aget-object v19, v13, v7

    move-object/from16 v0, v19

    iget v0, v0, Lorg/apache/lucene/util/automaton/Transition;->min:I

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    if-lt v0, v1, :cond_a

    .line 303
    :cond_7
    move v8, v3

    .local v8, "n2":I
    :goto_5
    array-length v0, v14

    move/from16 v18, v0

    move/from16 v0, v18

    if-ge v8, v0, :cond_8

    aget-object v18, v13, v7

    move-object/from16 v0, v18

    iget v0, v0, Lorg/apache/lucene/util/automaton/Transition;->max:I

    move/from16 v18, v0

    aget-object v19, v14, v8

    move-object/from16 v0, v19

    iget v0, v0, Lorg/apache/lucene/util/automaton/Transition;->min:I

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_b

    .line 300
    :cond_8
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    .line 297
    .end local v3    # "b2":I
    .end local v7    # "n1":I
    .end local v8    # "n2":I
    .end local v13    # "t1":[Lorg/apache/lucene/util/automaton/Transition;
    .end local v14    # "t2":[Lorg/apache/lucene/util/automaton/Transition;
    :cond_9
    const/16 v18, 0x0

    goto :goto_2

    .line 302
    .restart local v3    # "b2":I
    .restart local v7    # "n1":I
    .restart local v13    # "t1":[Lorg/apache/lucene/util/automaton/Transition;
    .restart local v14    # "t2":[Lorg/apache/lucene/util/automaton/Transition;
    :cond_a
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 304
    .restart local v8    # "n2":I
    :cond_b
    aget-object v18, v14, v8

    move-object/from16 v0, v18

    iget v0, v0, Lorg/apache/lucene/util/automaton/Transition;->max:I

    move/from16 v18, v0

    aget-object v19, v13, v7

    move-object/from16 v0, v19

    iget v0, v0, Lorg/apache/lucene/util/automaton/Transition;->min:I

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    if-lt v0, v1, :cond_d

    .line 305
    new-instance v11, Lorg/apache/lucene/util/automaton/StatePair;

    aget-object v18, v13, v7

    move-object/from16 v0, v18

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/Transition;->to:Lorg/apache/lucene/util/automaton/State;

    move-object/from16 v18, v0

    aget-object v19, v14, v8

    move-object/from16 v0, v19

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/Transition;->to:Lorg/apache/lucene/util/automaton/State;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-direct {v11, v0, v1}, Lorg/apache/lucene/util/automaton/StatePair;-><init>(Lorg/apache/lucene/util/automaton/State;Lorg/apache/lucene/util/automaton/State;)V

    .line 306
    .local v11, "q":Lorg/apache/lucene/util/automaton/StatePair;
    invoke-virtual {v9, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lorg/apache/lucene/util/automaton/StatePair;

    .line 307
    .local v12, "r":Lorg/apache/lucene/util/automaton/StatePair;
    if-nez v12, :cond_c

    .line 308
    new-instance v18, Lorg/apache/lucene/util/automaton/State;

    invoke-direct/range {v18 .. v18}, Lorg/apache/lucene/util/automaton/State;-><init>()V

    move-object/from16 v0, v18

    iput-object v0, v11, Lorg/apache/lucene/util/automaton/StatePair;->s:Lorg/apache/lucene/util/automaton/State;

    .line 309
    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 310
    invoke-virtual {v9, v11, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 311
    move-object v12, v11

    .line 313
    :cond_c
    aget-object v18, v13, v7

    move-object/from16 v0, v18

    iget v0, v0, Lorg/apache/lucene/util/automaton/Transition;->min:I

    move/from16 v18, v0

    aget-object v19, v14, v8

    move-object/from16 v0, v19

    iget v0, v0, Lorg/apache/lucene/util/automaton/Transition;->min:I

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_e

    aget-object v18, v13, v7

    move-object/from16 v0, v18

    iget v6, v0, Lorg/apache/lucene/util/automaton/Transition;->min:I

    .line 314
    .local v6, "min":I
    :goto_6
    aget-object v18, v13, v7

    move-object/from16 v0, v18

    iget v0, v0, Lorg/apache/lucene/util/automaton/Transition;->max:I

    move/from16 v18, v0

    aget-object v19, v14, v8

    move-object/from16 v0, v19

    iget v0, v0, Lorg/apache/lucene/util/automaton/Transition;->max:I

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_f

    aget-object v18, v13, v7

    move-object/from16 v0, v18

    iget v5, v0, Lorg/apache/lucene/util/automaton/Transition;->max:I

    .line 315
    .local v5, "max":I
    :goto_7
    iget-object v0, v10, Lorg/apache/lucene/util/automaton/StatePair;->s:Lorg/apache/lucene/util/automaton/State;

    move-object/from16 v18, v0

    new-instance v19, Lorg/apache/lucene/util/automaton/Transition;

    iget-object v0, v12, Lorg/apache/lucene/util/automaton/StatePair;->s:Lorg/apache/lucene/util/automaton/State;

    move-object/from16 v20, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-direct {v0, v6, v5, v1}, Lorg/apache/lucene/util/automaton/Transition;-><init>(IILorg/apache/lucene/util/automaton/State;)V

    invoke-virtual/range {v18 .. v19}, Lorg/apache/lucene/util/automaton/State;->addTransition(Lorg/apache/lucene/util/automaton/Transition;)V

    .line 303
    .end local v5    # "max":I
    .end local v6    # "min":I
    .end local v11    # "q":Lorg/apache/lucene/util/automaton/StatePair;
    .end local v12    # "r":Lorg/apache/lucene/util/automaton/StatePair;
    :cond_d
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_5

    .line 313
    .restart local v11    # "q":Lorg/apache/lucene/util/automaton/StatePair;
    .restart local v12    # "r":Lorg/apache/lucene/util/automaton/StatePair;
    :cond_e
    aget-object v18, v14, v8

    move-object/from16 v0, v18

    iget v6, v0, Lorg/apache/lucene/util/automaton/Transition;->min:I

    goto :goto_6

    .line 314
    .restart local v6    # "min":I
    :cond_f
    aget-object v18, v14, v8

    move-object/from16 v0, v18

    iget v5, v0, Lorg/apache/lucene/util/automaton/Transition;->max:I

    goto :goto_7

    .line 319
    .end local v3    # "b2":I
    .end local v6    # "min":I
    .end local v7    # "n1":I
    .end local v8    # "n2":I
    .end local v11    # "q":Lorg/apache/lucene/util/automaton/StatePair;
    .end local v12    # "r":Lorg/apache/lucene/util/automaton/StatePair;
    .end local v13    # "t1":[Lorg/apache/lucene/util/automaton/Transition;
    .end local v14    # "t2":[Lorg/apache/lucene/util/automaton/Transition;
    :cond_10
    const/16 v18, 0x0

    goto/16 :goto_1
.end method

.method public static isEmpty(Lorg/apache/lucene/util/automaton/Automaton;)Z
    .locals 2
    .param p0, "a"    # Lorg/apache/lucene/util/automaton/Automaton;

    .prologue
    const/4 v0, 0x0

    .line 784
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->isSingleton()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 785
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    iget-boolean v1, v1, Lorg/apache/lucene/util/automaton/State;->accept:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    invoke-virtual {v1}, Lorg/apache/lucene/util/automaton/State;->numTransitions()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static isEmptyString(Lorg/apache/lucene/util/automaton/Automaton;)Z
    .locals 3
    .param p0, "a"    # Lorg/apache/lucene/util/automaton/Automaton;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 776
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->isSingleton()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lorg/apache/lucene/util/automaton/Automaton;->singleton:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 777
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 776
    goto :goto_0

    .line 777
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    iget-boolean v2, v2, Lorg/apache/lucene/util/automaton/State;->accept:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    invoke-virtual {v2}, Lorg/apache/lucene/util/automaton/State;->numTransitions()I

    move-result v2

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public static isTotal(Lorg/apache/lucene/util/automaton/Automaton;)Z
    .locals 5
    .param p0, "a"    # Lorg/apache/lucene/util/automaton/Automaton;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 792
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->isSingleton()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 798
    :cond_0
    :goto_0
    return v1

    .line 793
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    iget-boolean v3, v3, Lorg/apache/lucene/util/automaton/State;->accept:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    invoke-virtual {v3}, Lorg/apache/lucene/util/automaton/State;->numTransitions()I

    move-result v3

    if-ne v3, v2, :cond_0

    .line 794
    iget-object v3, p0, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    invoke-virtual {v3}, Lorg/apache/lucene/util/automaton/State;->getTransitions()Ljava/lang/Iterable;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/util/automaton/Transition;

    .line 795
    .local v0, "t":Lorg/apache/lucene/util/automaton/Transition;
    iget-object v3, v0, Lorg/apache/lucene/util/automaton/Transition;->to:Lorg/apache/lucene/util/automaton/State;

    iget-object v4, p0, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    if-ne v3, v4, :cond_0

    iget v3, v0, Lorg/apache/lucene/util/automaton/Transition;->min:I

    if-nez v3, :cond_0

    .line 796
    iget v3, v0, Lorg/apache/lucene/util/automaton/Transition;->max:I

    const v4, 0x10ffff

    if-ne v3, v4, :cond_0

    move v1, v2

    .line 795
    goto :goto_0
.end method

.method public static minus(Lorg/apache/lucene/util/automaton/Automaton;Lorg/apache/lucene/util/automaton/Automaton;)Lorg/apache/lucene/util/automaton/Automaton;
    .locals 1
    .param p0, "a1"    # Lorg/apache/lucene/util/automaton/Automaton;
    .param p1, "a2"    # Lorg/apache/lucene/util/automaton/Automaton;

    .prologue
    .line 261
    invoke-static {p0}, Lorg/apache/lucene/util/automaton/BasicOperations;->isEmpty(Lorg/apache/lucene/util/automaton/Automaton;)Z

    move-result v0

    if-nez v0, :cond_0

    if-ne p0, p1, :cond_1

    .line 262
    :cond_0
    invoke-static {}, Lorg/apache/lucene/util/automaton/BasicAutomata;->makeEmpty()Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v0

    .line 268
    :goto_0
    return-object v0

    .line 263
    :cond_1
    invoke-static {p1}, Lorg/apache/lucene/util/automaton/BasicOperations;->isEmpty(Lorg/apache/lucene/util/automaton/Automaton;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->cloneIfRequired()Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v0

    goto :goto_0

    .line 264
    :cond_2
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->isSingleton()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 265
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/Automaton;->singleton:Ljava/lang/String;

    invoke-static {p1, v0}, Lorg/apache/lucene/util/automaton/BasicOperations;->run(Lorg/apache/lucene/util/automaton/Automaton;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {}, Lorg/apache/lucene/util/automaton/BasicAutomata;->makeEmpty()Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v0

    goto :goto_0

    .line 266
    :cond_3
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->cloneIfRequired()Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v0

    goto :goto_0

    .line 268
    :cond_4
    invoke-virtual {p1}, Lorg/apache/lucene/util/automaton/Automaton;->complement()Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v0

    invoke-static {p0, v0}, Lorg/apache/lucene/util/automaton/BasicOperations;->intersection(Lorg/apache/lucene/util/automaton/Automaton;Lorg/apache/lucene/util/automaton/Automaton;)Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v0

    goto :goto_0
.end method

.method public static optional(Lorg/apache/lucene/util/automaton/Automaton;)Lorg/apache/lucene/util/automaton/Automaton;
    .locals 2
    .param p0, "a"    # Lorg/apache/lucene/util/automaton/Automaton;

    .prologue
    .line 148
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->cloneExpandedIfRequired()Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object p0

    .line 149
    new-instance v0, Lorg/apache/lucene/util/automaton/State;

    invoke-direct {v0}, Lorg/apache/lucene/util/automaton/State;-><init>()V

    .line 150
    .local v0, "s":Lorg/apache/lucene/util/automaton/State;
    iget-object v1, p0, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/automaton/State;->addEpsilon(Lorg/apache/lucene/util/automaton/State;)V

    .line 151
    const/4 v1, 0x1

    iput-boolean v1, v0, Lorg/apache/lucene/util/automaton/State;->accept:Z

    .line 152
    iput-object v0, p0, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    .line 153
    const/4 v1, 0x0

    iput-boolean v1, p0, Lorg/apache/lucene/util/automaton/Automaton;->deterministic:Z

    .line 155
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->clearNumberedStates()V

    .line 156
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->checkMinimizeAlways()V

    .line 157
    return-object p0
.end method

.method public static repeat(Lorg/apache/lucene/util/automaton/Automaton;)Lorg/apache/lucene/util/automaton/Automaton;
    .locals 4
    .param p0, "a"    # Lorg/apache/lucene/util/automaton/Automaton;

    .prologue
    .line 168
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->cloneExpanded()Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object p0

    .line 169
    new-instance v1, Lorg/apache/lucene/util/automaton/State;

    invoke-direct {v1}, Lorg/apache/lucene/util/automaton/State;-><init>()V

    .line 170
    .local v1, "s":Lorg/apache/lucene/util/automaton/State;
    const/4 v2, 0x1

    iput-boolean v2, v1, Lorg/apache/lucene/util/automaton/State;->accept:Z

    .line 171
    iget-object v2, p0, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/automaton/State;->addEpsilon(Lorg/apache/lucene/util/automaton/State;)V

    .line 172
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->getAcceptStates()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 174
    iput-object v1, p0, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    .line 175
    const/4 v2, 0x0

    iput-boolean v2, p0, Lorg/apache/lucene/util/automaton/Automaton;->deterministic:Z

    .line 177
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->clearNumberedStates()V

    .line 178
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->checkMinimizeAlways()V

    .line 179
    return-object p0

    .line 172
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/util/automaton/State;

    .line 173
    .local v0, "p":Lorg/apache/lucene/util/automaton/State;
    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/automaton/State;->addEpsilon(Lorg/apache/lucene/util/automaton/State;)V

    goto :goto_0
.end method

.method public static repeat(Lorg/apache/lucene/util/automaton/Automaton;I)Lorg/apache/lucene/util/automaton/Automaton;
    .locals 3
    .param p0, "a"    # Lorg/apache/lucene/util/automaton/Automaton;
    .param p1, "min"    # I

    .prologue
    .line 189
    if-nez p1, :cond_0

    invoke-static {p0}, Lorg/apache/lucene/util/automaton/BasicOperations;->repeat(Lorg/apache/lucene/util/automaton/Automaton;)Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v2

    .line 194
    :goto_0
    return-object v2

    .line 190
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .local v0, "as":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/util/automaton/Automaton;>;"
    move v1, p1

    .line 191
    .end local p1    # "min":I
    .local v1, "min":I
    :goto_1
    add-int/lit8 p1, v1, -0x1

    .end local v1    # "min":I
    .restart local p1    # "min":I
    if-gtz v1, :cond_1

    .line 193
    invoke-static {p0}, Lorg/apache/lucene/util/automaton/BasicOperations;->repeat(Lorg/apache/lucene/util/automaton/Automaton;)Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 194
    invoke-static {v0}, Lorg/apache/lucene/util/automaton/BasicOperations;->concatenate(Ljava/util/List;)Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v2

    goto :goto_0

    .line 192
    :cond_1
    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v1, p1

    .end local p1    # "min":I
    .restart local v1    # "min":I
    goto :goto_1
.end method

.method public static repeat(Lorg/apache/lucene/util/automaton/Automaton;II)Lorg/apache/lucene/util/automaton/Automaton;
    .locals 8
    .param p0, "a"    # Lorg/apache/lucene/util/automaton/Automaton;
    .param p1, "min"    # I
    .param p2, "max"    # I

    .prologue
    .line 206
    if-le p1, p2, :cond_1

    invoke-static {}, Lorg/apache/lucene/util/automaton/BasicAutomata;->makeEmpty()Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v1

    .line 233
    :cond_0
    :goto_0
    return-object v1

    .line 207
    :cond_1
    sub-int/2addr p2, p1

    .line 208
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->expandSingleton()V

    .line 210
    if-nez p1, :cond_2

    invoke-static {}, Lorg/apache/lucene/util/automaton/BasicAutomata;->makeEmptyString()Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v1

    .line 218
    .local v1, "b":Lorg/apache/lucene/util/automaton/Automaton;
    :goto_1
    if-lez p2, :cond_0

    .line 219
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->clone()Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v3

    .line 220
    .local v3, "d":Lorg/apache/lucene/util/automaton/Automaton;
    :goto_2
    add-int/lit8 p2, p2, -0x1

    if-gtz p2, :cond_5

    .line 226
    invoke-virtual {v1}, Lorg/apache/lucene/util/automaton/Automaton;->getAcceptStates()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_7

    .line 228
    const/4 v6, 0x0

    iput-boolean v6, v1, Lorg/apache/lucene/util/automaton/Automaton;->deterministic:Z

    .line 230
    invoke-virtual {v1}, Lorg/apache/lucene/util/automaton/Automaton;->clearNumberedStates()V

    .line 231
    invoke-virtual {v1}, Lorg/apache/lucene/util/automaton/Automaton;->checkMinimizeAlways()V

    goto :goto_0

    .line 211
    .end local v1    # "b":Lorg/apache/lucene/util/automaton/Automaton;
    .end local v3    # "d":Lorg/apache/lucene/util/automaton/Automaton;
    :cond_2
    const/4 v6, 0x1

    if-ne p1, v6, :cond_3

    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->clone()Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v1

    .restart local v1    # "b":Lorg/apache/lucene/util/automaton/Automaton;
    goto :goto_1

    .line 213
    .end local v1    # "b":Lorg/apache/lucene/util/automaton/Automaton;
    :cond_3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .local v0, "as":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/util/automaton/Automaton;>;"
    move v4, p1

    .line 214
    .end local p1    # "min":I
    .local v4, "min":I
    :goto_4
    add-int/lit8 p1, v4, -0x1

    .end local v4    # "min":I
    .restart local p1    # "min":I
    if-gtz v4, :cond_4

    .line 216
    invoke-static {v0}, Lorg/apache/lucene/util/automaton/BasicOperations;->concatenate(Ljava/util/List;)Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v1

    .restart local v1    # "b":Lorg/apache/lucene/util/automaton/Automaton;
    goto :goto_1

    .line 215
    .end local v1    # "b":Lorg/apache/lucene/util/automaton/Automaton;
    :cond_4
    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v4, p1

    .end local p1    # "min":I
    .restart local v4    # "min":I
    goto :goto_4

    .line 221
    .end local v0    # "as":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/util/automaton/Automaton;>;"
    .end local v4    # "min":I
    .restart local v1    # "b":Lorg/apache/lucene/util/automaton/Automaton;
    .restart local v3    # "d":Lorg/apache/lucene/util/automaton/Automaton;
    .restart local p1    # "min":I
    :cond_5
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->clone()Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v2

    .line 222
    .local v2, "c":Lorg/apache/lucene/util/automaton/Automaton;
    invoke-virtual {v2}, Lorg/apache/lucene/util/automaton/Automaton;->getAcceptStates()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_5
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_6

    .line 224
    move-object v3, v2

    goto :goto_2

    .line 222
    :cond_6
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/util/automaton/State;

    .line 223
    .local v5, "p":Lorg/apache/lucene/util/automaton/State;
    iget-object v7, v3, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    invoke-virtual {v5, v7}, Lorg/apache/lucene/util/automaton/State;->addEpsilon(Lorg/apache/lucene/util/automaton/State;)V

    goto :goto_5

    .line 226
    .end local v2    # "c":Lorg/apache/lucene/util/automaton/Automaton;
    .end local v5    # "p":Lorg/apache/lucene/util/automaton/State;
    :cond_7
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/util/automaton/State;

    .line 227
    .restart local v5    # "p":Lorg/apache/lucene/util/automaton/State;
    iget-object v7, v3, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    invoke-virtual {v5, v7}, Lorg/apache/lucene/util/automaton/State;->addEpsilon(Lorg/apache/lucene/util/automaton/State;)V

    goto :goto_3
.end method

.method public static run(Lorg/apache/lucene/util/automaton/Automaton;Ljava/lang/String;)Z
    .locals 18
    .param p0, "a"    # Lorg/apache/lucene/util/automaton/Automaton;
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 809
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/util/automaton/Automaton;->isSingleton()Z

    move-result v15

    if-eqz v15, :cond_1

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/util/automaton/Automaton;->singleton:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 850
    :cond_0
    :goto_0
    return v1

    .line 810
    :cond_1
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lorg/apache/lucene/util/automaton/Automaton;->deterministic:Z

    if-eqz v15, :cond_4

    .line 811
    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    .line 812
    .local v8, "p":Lorg/apache/lucene/util/automaton/State;
    const/4 v7, 0x0

    .local v7, "i":I
    const/4 v5, 0x0

    .local v5, "cp":I
    :goto_1
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v15

    if-lt v7, v15, :cond_2

    .line 817
    iget-boolean v1, v8, Lorg/apache/lucene/util/automaton/State;->accept:Z

    goto :goto_0

    .line 813
    :cond_2
    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Ljava/lang/String;->codePointAt(I)I

    move-result v5

    invoke-virtual {v8, v5}, Lorg/apache/lucene/util/automaton/State;->step(I)Lorg/apache/lucene/util/automaton/State;

    move-result-object v11

    .line 814
    .local v11, "q":Lorg/apache/lucene/util/automaton/State;
    if-nez v11, :cond_3

    const/4 v1, 0x0

    goto :goto_0

    .line 815
    :cond_3
    move-object v8, v11

    .line 812
    invoke-static {v5}, Ljava/lang/Character;->charCount(I)I

    move-result v15

    add-int/2addr v7, v15

    goto :goto_1

    .line 819
    .end local v5    # "cp":I
    .end local v7    # "i":I
    .end local v8    # "p":Lorg/apache/lucene/util/automaton/State;
    .end local v11    # "q":Lorg/apache/lucene/util/automaton/State;
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/util/automaton/Automaton;->getNumberedStates()[Lorg/apache/lucene/util/automaton/State;

    move-result-object v12

    .line 820
    .local v12, "states":[Lorg/apache/lucene/util/automaton/State;
    new-instance v9, Ljava/util/LinkedList;

    invoke-direct {v9}, Ljava/util/LinkedList;-><init>()V

    .line 821
    .local v9, "pp":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lorg/apache/lucene/util/automaton/State;>;"
    new-instance v10, Ljava/util/LinkedList;

    invoke-direct {v10}, Ljava/util/LinkedList;-><init>()V

    .line 822
    .local v10, "pp_other":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lorg/apache/lucene/util/automaton/State;>;"
    new-instance v2, Ljava/util/BitSet;

    array-length v15, v12

    invoke-direct {v2, v15}, Ljava/util/BitSet;-><init>(I)V

    .line 823
    .local v2, "bb":Ljava/util/BitSet;
    new-instance v3, Ljava/util/BitSet;

    array-length v15, v12

    invoke-direct {v3, v15}, Ljava/util/BitSet;-><init>(I)V

    .line 824
    .local v3, "bb_other":Ljava/util/BitSet;
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    invoke-virtual {v9, v15}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 825
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 826
    .local v6, "dest":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/util/automaton/State;>;"
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    iget-boolean v1, v15, Lorg/apache/lucene/util/automaton/State;->accept:Z

    .line 827
    .local v1, "accept":Z
    const/4 v7, 0x0

    .restart local v7    # "i":I
    const/4 v4, 0x0

    .local v4, "c":I
    :goto_2
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v15

    if-ge v7, v15, :cond_0

    .line 828
    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Ljava/lang/String;->codePointAt(I)I

    move-result v4

    .line 829
    const/4 v1, 0x0

    .line 830
    invoke-virtual {v10}, Ljava/util/LinkedList;->clear()V

    .line 831
    invoke-virtual {v3}, Ljava/util/BitSet;->clear()V

    .line 832
    invoke-virtual {v9}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :cond_5
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-nez v16, :cond_6

    .line 843
    move-object v14, v9

    .line 844
    .local v14, "tp":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lorg/apache/lucene/util/automaton/State;>;"
    move-object v9, v10

    .line 845
    move-object v10, v14

    .line 846
    move-object v13, v2

    .line 847
    .local v13, "tb":Ljava/util/BitSet;
    move-object v2, v3

    .line 848
    move-object v3, v13

    .line 827
    invoke-static {v4}, Ljava/lang/Character;->charCount(I)I

    move-result v15

    add-int/2addr v7, v15

    goto :goto_2

    .line 832
    .end local v13    # "tb":Ljava/util/BitSet;
    .end local v14    # "tp":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lorg/apache/lucene/util/automaton/State;>;"
    :cond_6
    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/apache/lucene/util/automaton/State;

    .line 833
    .restart local v8    # "p":Lorg/apache/lucene/util/automaton/State;
    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    .line 834
    invoke-virtual {v8, v4, v6}, Lorg/apache/lucene/util/automaton/State;->step(ILjava/util/Collection;)V

    .line 835
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :cond_7
    :goto_3
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_5

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lorg/apache/lucene/util/automaton/State;

    .line 836
    .restart local v11    # "q":Lorg/apache/lucene/util/automaton/State;
    iget-boolean v0, v11, Lorg/apache/lucene/util/automaton/State;->accept:Z

    move/from16 v17, v0

    if-eqz v17, :cond_8

    const/4 v1, 0x1

    .line 837
    :cond_8
    iget v0, v11, Lorg/apache/lucene/util/automaton/State;->number:I

    move/from16 v17, v0

    move/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v17

    if-nez v17, :cond_7

    .line 838
    iget v0, v11, Lorg/apache/lucene/util/automaton/State;->number:I

    move/from16 v17, v0

    move/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/util/BitSet;->set(I)V

    .line 839
    invoke-virtual {v10, v11}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_3
.end method

.method public static sameLanguage(Lorg/apache/lucene/util/automaton/Automaton;Lorg/apache/lucene/util/automaton/Automaton;)Z
    .locals 3
    .param p0, "a1"    # Lorg/apache/lucene/util/automaton/Automaton;
    .param p1, "a2"    # Lorg/apache/lucene/util/automaton/Automaton;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 330
    if-ne p0, p1, :cond_1

    .line 339
    :cond_0
    :goto_0
    return v0

    .line 333
    :cond_1
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->isSingleton()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p1}, Lorg/apache/lucene/util/automaton/Automaton;->isSingleton()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 334
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/Automaton;->singleton:Ljava/lang/String;

    iget-object v1, p1, Lorg/apache/lucene/util/automaton/Automaton;->singleton:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 335
    :cond_2
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->isSingleton()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 337
    invoke-static {p0, p1}, Lorg/apache/lucene/util/automaton/BasicOperations;->subsetOf(Lorg/apache/lucene/util/automaton/Automaton;Lorg/apache/lucene/util/automaton/Automaton;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static {p1, p0}, Lorg/apache/lucene/util/automaton/BasicOperations;->subsetOf(Lorg/apache/lucene/util/automaton/Automaton;Lorg/apache/lucene/util/automaton/Automaton;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    .line 339
    :cond_4
    invoke-static {p1, p0}, Lorg/apache/lucene/util/automaton/BasicOperations;->subsetOf(Lorg/apache/lucene/util/automaton/Automaton;Lorg/apache/lucene/util/automaton/Automaton;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-static {p0, p1}, Lorg/apache/lucene/util/automaton/BasicOperations;->subsetOf(Lorg/apache/lucene/util/automaton/Automaton;Lorg/apache/lucene/util/automaton/Automaton;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public static subsetOf(Lorg/apache/lucene/util/automaton/Automaton;Lorg/apache/lucene/util/automaton/Automaton;)Z
    .locals 17
    .param p0, "a1"    # Lorg/apache/lucene/util/automaton/Automaton;
    .param p1, "a2"    # Lorg/apache/lucene/util/automaton/Automaton;

    .prologue
    .line 351
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    if-ne v0, v1, :cond_0

    const/4 v15, 0x1

    .line 396
    :goto_0
    return v15

    .line 352
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/util/automaton/Automaton;->isSingleton()Z

    move-result v15

    if-eqz v15, :cond_2

    .line 353
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/util/automaton/Automaton;->isSingleton()Z

    move-result v15

    if-eqz v15, :cond_1

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/util/automaton/Automaton;->singleton:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/Automaton;->singleton:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    goto :goto_0

    .line 354
    :cond_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/util/automaton/Automaton;->singleton:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-static {v0, v15}, Lorg/apache/lucene/util/automaton/BasicOperations;->run(Lorg/apache/lucene/util/automaton/Automaton;Ljava/lang/String;)Z

    move-result v15

    goto :goto_0

    .line 356
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/util/automaton/Automaton;->determinize()V

    .line 357
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/util/automaton/Automaton;->getSortedTransitions()[[Lorg/apache/lucene/util/automaton/Transition;

    move-result-object v11

    .line 358
    .local v11, "transitions1":[[Lorg/apache/lucene/util/automaton/Transition;
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/util/automaton/Automaton;->getSortedTransitions()[[Lorg/apache/lucene/util/automaton/Transition;

    move-result-object v12

    .line 359
    .local v12, "transitions2":[[Lorg/apache/lucene/util/automaton/Transition;
    new-instance v14, Ljava/util/LinkedList;

    invoke-direct {v14}, Ljava/util/LinkedList;-><init>()V

    .line 360
    .local v14, "worklist":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lorg/apache/lucene/util/automaton/StatePair;>;"
    new-instance v13, Ljava/util/HashSet;

    invoke-direct {v13}, Ljava/util/HashSet;-><init>()V

    .line 361
    .local v13, "visited":Ljava/util/HashSet;, "Ljava/util/HashSet<Lorg/apache/lucene/util/automaton/StatePair;>;"
    new-instance v7, Lorg/apache/lucene/util/automaton/StatePair;

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    move-object/from16 v0, p1

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-direct {v7, v15, v0}, Lorg/apache/lucene/util/automaton/StatePair;-><init>(Lorg/apache/lucene/util/automaton/State;Lorg/apache/lucene/util/automaton/State;)V

    .line 362
    .local v7, "p":Lorg/apache/lucene/util/automaton/StatePair;
    invoke-virtual {v14, v7}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 363
    invoke-virtual {v13, v7}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 364
    :cond_3
    invoke-virtual {v14}, Ljava/util/LinkedList;->size()I

    move-result v15

    if-gtz v15, :cond_4

    .line 396
    const/4 v15, 0x1

    goto :goto_0

    .line 365
    :cond_4
    invoke-virtual {v14}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v7

    .end local v7    # "p":Lorg/apache/lucene/util/automaton/StatePair;
    check-cast v7, Lorg/apache/lucene/util/automaton/StatePair;

    .line 366
    .restart local v7    # "p":Lorg/apache/lucene/util/automaton/StatePair;
    iget-object v15, v7, Lorg/apache/lucene/util/automaton/StatePair;->s1:Lorg/apache/lucene/util/automaton/State;

    iget-boolean v15, v15, Lorg/apache/lucene/util/automaton/State;->accept:Z

    if-eqz v15, :cond_5

    iget-object v15, v7, Lorg/apache/lucene/util/automaton/StatePair;->s2:Lorg/apache/lucene/util/automaton/State;

    iget-boolean v15, v15, Lorg/apache/lucene/util/automaton/State;->accept:Z

    if-nez v15, :cond_5

    .line 367
    const/4 v15, 0x0

    goto :goto_0

    .line 369
    :cond_5
    iget-object v15, v7, Lorg/apache/lucene/util/automaton/StatePair;->s1:Lorg/apache/lucene/util/automaton/State;

    iget v15, v15, Lorg/apache/lucene/util/automaton/State;->number:I

    aget-object v9, v11, v15

    .line 370
    .local v9, "t1":[Lorg/apache/lucene/util/automaton/Transition;
    iget-object v15, v7, Lorg/apache/lucene/util/automaton/StatePair;->s2:Lorg/apache/lucene/util/automaton/State;

    iget v15, v15, Lorg/apache/lucene/util/automaton/State;->number:I

    aget-object v10, v12, v15

    .line 371
    .local v10, "t2":[Lorg/apache/lucene/util/automaton/Transition;
    const/4 v5, 0x0

    .local v5, "n1":I
    const/4 v2, 0x0

    .local v2, "b2":I
    :goto_1
    array-length v15, v9

    if-ge v5, v15, :cond_3

    .line 372
    :goto_2
    array-length v15, v10

    if-ge v2, v15, :cond_6

    aget-object v15, v10, v2

    iget v15, v15, Lorg/apache/lucene/util/automaton/Transition;->max:I

    aget-object v16, v9, v5

    move-object/from16 v0, v16

    iget v0, v0, Lorg/apache/lucene/util/automaton/Transition;->min:I

    move/from16 v16, v0

    move/from16 v0, v16

    if-lt v15, v0, :cond_8

    .line 374
    :cond_6
    aget-object v15, v9, v5

    iget v4, v15, Lorg/apache/lucene/util/automaton/Transition;->min:I

    .local v4, "min1":I
    aget-object v15, v9, v5

    iget v3, v15, Lorg/apache/lucene/util/automaton/Transition;->max:I

    .line 376
    .local v3, "max1":I
    move v6, v2

    .local v6, "n2":I
    :goto_3
    array-length v15, v10

    if-ge v6, v15, :cond_7

    aget-object v15, v9, v5

    iget v15, v15, Lorg/apache/lucene/util/automaton/Transition;->max:I

    aget-object v16, v10, v6

    move-object/from16 v0, v16

    iget v0, v0, Lorg/apache/lucene/util/automaton/Transition;->min:I

    move/from16 v16, v0

    move/from16 v0, v16

    if-ge v15, v0, :cond_9

    .line 391
    :cond_7
    if-gt v4, v3, :cond_d

    .line 392
    const/4 v15, 0x0

    goto/16 :goto_0

    .line 373
    .end local v3    # "max1":I
    .end local v4    # "min1":I
    .end local v6    # "n2":I
    :cond_8
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 377
    .restart local v3    # "max1":I
    .restart local v4    # "min1":I
    .restart local v6    # "n2":I
    :cond_9
    aget-object v15, v10, v6

    iget v15, v15, Lorg/apache/lucene/util/automaton/Transition;->min:I

    if-le v15, v4, :cond_a

    .line 378
    const/4 v15, 0x0

    goto/16 :goto_0

    .line 380
    :cond_a
    aget-object v15, v10, v6

    iget v15, v15, Lorg/apache/lucene/util/automaton/Transition;->max:I

    const v16, 0x10ffff

    move/from16 v0, v16

    if-ge v15, v0, :cond_c

    aget-object v15, v10, v6

    iget v15, v15, Lorg/apache/lucene/util/automaton/Transition;->max:I

    add-int/lit8 v4, v15, 0x1

    .line 385
    :goto_4
    new-instance v8, Lorg/apache/lucene/util/automaton/StatePair;

    aget-object v15, v9, v5

    iget-object v15, v15, Lorg/apache/lucene/util/automaton/Transition;->to:Lorg/apache/lucene/util/automaton/State;

    aget-object v16, v10, v6

    move-object/from16 v0, v16

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/Transition;->to:Lorg/apache/lucene/util/automaton/State;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-direct {v8, v15, v0}, Lorg/apache/lucene/util/automaton/StatePair;-><init>(Lorg/apache/lucene/util/automaton/State;Lorg/apache/lucene/util/automaton/State;)V

    .line 386
    .local v8, "q":Lorg/apache/lucene/util/automaton/StatePair;
    invoke-virtual {v13, v8}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_b

    .line 387
    invoke-virtual {v14, v8}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 388
    invoke-virtual {v13, v8}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 376
    :cond_b
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 382
    .end local v8    # "q":Lorg/apache/lucene/util/automaton/StatePair;
    :cond_c
    const v4, 0x10ffff

    .line 383
    const/4 v3, 0x0

    goto :goto_4

    .line 371
    :cond_d
    add-int/lit8 v5, v5, 0x1

    goto :goto_1
.end method

.method public static union(Ljava/util/Collection;)Lorg/apache/lucene/util/automaton/Automaton;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/util/automaton/Automaton;",
            ">;)",
            "Lorg/apache/lucene/util/automaton/Automaton;"
        }
    .end annotation

    .prologue
    .local p0, "l":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/util/automaton/Automaton;>;"
    const/4 v6, 0x0

    .line 434
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 435
    .local v4, "ids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_1

    .line 437
    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v7

    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v8

    if-eq v7, v8, :cond_2

    const/4 v3, 0x1

    .line 438
    .local v3, "has_aliases":Z
    :goto_1
    new-instance v5, Lorg/apache/lucene/util/automaton/State;

    invoke-direct {v5}, Lorg/apache/lucene/util/automaton/State;-><init>()V

    .line 439
    .local v5, "s":Lorg/apache/lucene/util/automaton/State;
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_3

    .line 446
    new-instance v0, Lorg/apache/lucene/util/automaton/Automaton;

    invoke-direct {v0}, Lorg/apache/lucene/util/automaton/Automaton;-><init>()V

    .line 447
    .local v0, "a":Lorg/apache/lucene/util/automaton/Automaton;
    iput-object v5, v0, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    .line 448
    iput-boolean v6, v0, Lorg/apache/lucene/util/automaton/Automaton;->deterministic:Z

    .line 450
    invoke-virtual {v0}, Lorg/apache/lucene/util/automaton/Automaton;->clearNumberedStates()V

    .line 451
    invoke-virtual {v0}, Lorg/apache/lucene/util/automaton/Automaton;->checkMinimizeAlways()V

    .line 452
    return-object v0

    .line 435
    .end local v0    # "a":Lorg/apache/lucene/util/automaton/Automaton;
    .end local v3    # "has_aliases":Z
    .end local v5    # "s":Lorg/apache/lucene/util/automaton/State;
    :cond_1
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/util/automaton/Automaton;

    .line 436
    .restart local v0    # "a":Lorg/apache/lucene/util/automaton/Automaton;
    invoke-static {v0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v4, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .end local v0    # "a":Lorg/apache/lucene/util/automaton/Automaton;
    :cond_2
    move v3, v6

    .line 437
    goto :goto_1

    .line 439
    .restart local v3    # "has_aliases":Z
    .restart local v5    # "s":Lorg/apache/lucene/util/automaton/State;
    :cond_3
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/util/automaton/Automaton;

    .line 440
    .local v1, "b":Lorg/apache/lucene/util/automaton/Automaton;
    invoke-static {v1}, Lorg/apache/lucene/util/automaton/BasicOperations;->isEmpty(Lorg/apache/lucene/util/automaton/Automaton;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 441
    move-object v2, v1

    .line 442
    .local v2, "bb":Lorg/apache/lucene/util/automaton/Automaton;
    if-eqz v3, :cond_4

    invoke-virtual {v2}, Lorg/apache/lucene/util/automaton/Automaton;->cloneExpanded()Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v2

    .line 444
    :goto_3
    iget-object v8, v2, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    invoke-virtual {v5, v8}, Lorg/apache/lucene/util/automaton/State;->addEpsilon(Lorg/apache/lucene/util/automaton/State;)V

    goto :goto_2

    .line 443
    :cond_4
    invoke-virtual {v2}, Lorg/apache/lucene/util/automaton/Automaton;->cloneExpandedIfRequired()Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v2

    goto :goto_3
.end method

.method public static union(Lorg/apache/lucene/util/automaton/Automaton;Lorg/apache/lucene/util/automaton/Automaton;)Lorg/apache/lucene/util/automaton/Automaton;
    .locals 3
    .param p0, "a1"    # Lorg/apache/lucene/util/automaton/Automaton;
    .param p1, "a2"    # Lorg/apache/lucene/util/automaton/Automaton;

    .prologue
    .line 406
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->isSingleton()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lorg/apache/lucene/util/automaton/Automaton;->isSingleton()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/util/automaton/Automaton;->singleton:Ljava/lang/String;

    .line 407
    iget-object v2, p1, Lorg/apache/lucene/util/automaton/Automaton;->singleton:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 408
    :cond_0
    if-ne p0, p1, :cond_2

    :cond_1
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->cloneIfRequired()Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object p0

    .line 424
    .end local p0    # "a1":Lorg/apache/lucene/util/automaton/Automaton;
    :goto_0
    return-object p0

    .line 409
    .restart local p0    # "a1":Lorg/apache/lucene/util/automaton/Automaton;
    :cond_2
    if-ne p0, p1, :cond_3

    .line 410
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->cloneExpanded()Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object p0

    .line 411
    invoke-virtual {p1}, Lorg/apache/lucene/util/automaton/Automaton;->cloneExpanded()Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object p1

    .line 416
    :goto_1
    new-instance v0, Lorg/apache/lucene/util/automaton/State;

    invoke-direct {v0}, Lorg/apache/lucene/util/automaton/State;-><init>()V

    .line 417
    .local v0, "s":Lorg/apache/lucene/util/automaton/State;
    iget-object v1, p0, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/automaton/State;->addEpsilon(Lorg/apache/lucene/util/automaton/State;)V

    .line 418
    iget-object v1, p1, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/automaton/State;->addEpsilon(Lorg/apache/lucene/util/automaton/State;)V

    .line 419
    iput-object v0, p0, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    .line 420
    const/4 v1, 0x0

    iput-boolean v1, p0, Lorg/apache/lucene/util/automaton/Automaton;->deterministic:Z

    .line 422
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->clearNumberedStates()V

    .line 423
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->checkMinimizeAlways()V

    goto :goto_0

    .line 413
    .end local v0    # "s":Lorg/apache/lucene/util/automaton/State;
    :cond_3
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->cloneExpandedIfRequired()Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object p0

    .line 414
    invoke-virtual {p1}, Lorg/apache/lucene/util/automaton/Automaton;->cloneExpandedIfRequired()Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object p1

    goto :goto_1
.end method
