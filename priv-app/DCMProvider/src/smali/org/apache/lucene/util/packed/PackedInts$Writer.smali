.class public abstract Lorg/apache/lucene/util/packed/PackedInts$Writer;
.super Ljava/lang/Object;
.source "PackedInts.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/packed/PackedInts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Writer"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field protected final bitsPerValue:I

.field protected final out:Lorg/apache/lucene/store/DataOutput;

.field protected final valueCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 734
    const-class v0, Lorg/apache/lucene/util/packed/PackedInts;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/packed/PackedInts$Writer;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected constructor <init>(Lorg/apache/lucene/store/DataOutput;II)V
    .locals 1
    .param p1, "out"    # Lorg/apache/lucene/store/DataOutput;
    .param p2, "valueCount"    # I
    .param p3, "bitsPerValue"    # I

    .prologue
    .line 739
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 740
    sget-boolean v0, Lorg/apache/lucene/util/packed/PackedInts$Writer;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    const/16 v0, 0x40

    if-le p3, v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 741
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/util/packed/PackedInts$Writer;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-gez p2, :cond_1

    const/4 v0, -0x1

    if-eq p2, v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 742
    :cond_1
    iput-object p1, p0, Lorg/apache/lucene/util/packed/PackedInts$Writer;->out:Lorg/apache/lucene/store/DataOutput;

    .line 743
    iput p2, p0, Lorg/apache/lucene/util/packed/PackedInts$Writer;->valueCount:I

    .line 744
    iput p3, p0, Lorg/apache/lucene/util/packed/PackedInts$Writer;->bitsPerValue:I

    .line 745
    return-void
.end method


# virtual methods
.method public abstract add(J)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public final bitsPerValue()I
    .locals 1

    .prologue
    .line 763
    iget v0, p0, Lorg/apache/lucene/util/packed/PackedInts$Writer;->bitsPerValue:I

    return v0
.end method

.method public abstract finish()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method protected abstract getFormat()Lorg/apache/lucene/util/packed/PackedInts$Format;
.end method

.method public abstract ord()I
.end method

.method writeHeader()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 748
    sget-boolean v0, Lorg/apache/lucene/util/packed/PackedInts$Writer;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/util/packed/PackedInts$Writer;->valueCount:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 749
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/util/packed/PackedInts$Writer;->out:Lorg/apache/lucene/store/DataOutput;

    const-string v1, "PackedInts"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lorg/apache/lucene/codecs/CodecUtil;->writeHeader(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;I)V

    .line 750
    iget-object v0, p0, Lorg/apache/lucene/util/packed/PackedInts$Writer;->out:Lorg/apache/lucene/store/DataOutput;

    iget v1, p0, Lorg/apache/lucene/util/packed/PackedInts$Writer;->bitsPerValue:I

    invoke-virtual {v0, v1}, Lorg/apache/lucene/store/DataOutput;->writeVInt(I)V

    .line 751
    iget-object v0, p0, Lorg/apache/lucene/util/packed/PackedInts$Writer;->out:Lorg/apache/lucene/store/DataOutput;

    iget v1, p0, Lorg/apache/lucene/util/packed/PackedInts$Writer;->valueCount:I

    invoke-virtual {v0, v1}, Lorg/apache/lucene/store/DataOutput;->writeVInt(I)V

    .line 752
    iget-object v0, p0, Lorg/apache/lucene/util/packed/PackedInts$Writer;->out:Lorg/apache/lucene/store/DataOutput;

    invoke-virtual {p0}, Lorg/apache/lucene/util/packed/PackedInts$Writer;->getFormat()Lorg/apache/lucene/util/packed/PackedInts$Format;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/lucene/util/packed/PackedInts$Format;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/store/DataOutput;->writeVInt(I)V

    .line 753
    return-void
.end method
