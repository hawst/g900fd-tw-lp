.class public final Lorg/apache/lucene/util/fst/IntsRefFSTEnum;
.super Lorg/apache/lucene/util/fst/FSTEnum;
.source "IntsRefFSTEnum.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/fst/IntsRefFSTEnum$InputOutput;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lorg/apache/lucene/util/fst/FSTEnum",
        "<TT;>;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final current:Lorg/apache/lucene/util/IntsRef;

.field private final result:Lorg/apache/lucene/util/fst/IntsRefFSTEnum$InputOutput;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/fst/IntsRefFSTEnum$InputOutput",
            "<TT;>;"
        }
    .end annotation
.end field

.field private target:Lorg/apache/lucene/util/IntsRef;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lorg/apache/lucene/util/fst/IntsRefFSTEnum;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/fst/IntsRefFSTEnum;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/util/fst/FST;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/FST",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 46
    .local p0, "this":Lorg/apache/lucene/util/fst/IntsRefFSTEnum;, "Lorg/apache/lucene/util/fst/IntsRefFSTEnum<TT;>;"
    .local p1, "fst":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/util/fst/FSTEnum;-><init>(Lorg/apache/lucene/util/fst/FST;)V

    .line 32
    new-instance v0, Lorg/apache/lucene/util/IntsRef;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/IntsRef;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/util/fst/IntsRefFSTEnum;->current:Lorg/apache/lucene/util/IntsRef;

    .line 33
    new-instance v0, Lorg/apache/lucene/util/fst/IntsRefFSTEnum$InputOutput;

    invoke-direct {v0}, Lorg/apache/lucene/util/fst/IntsRefFSTEnum$InputOutput;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/util/fst/IntsRefFSTEnum;->result:Lorg/apache/lucene/util/fst/IntsRefFSTEnum$InputOutput;

    .line 47
    iget-object v0, p0, Lorg/apache/lucene/util/fst/IntsRefFSTEnum;->result:Lorg/apache/lucene/util/fst/IntsRefFSTEnum$InputOutput;

    iget-object v1, p0, Lorg/apache/lucene/util/fst/IntsRefFSTEnum;->current:Lorg/apache/lucene/util/IntsRef;

    iput-object v1, v0, Lorg/apache/lucene/util/fst/IntsRefFSTEnum$InputOutput;->input:Lorg/apache/lucene/util/IntsRef;

    .line 48
    iget-object v0, p0, Lorg/apache/lucene/util/fst/IntsRefFSTEnum;->current:Lorg/apache/lucene/util/IntsRef;

    const/4 v1, 0x1

    iput v1, v0, Lorg/apache/lucene/util/IntsRef;->offset:I

    .line 49
    return-void
.end method

.method private setResult()Lorg/apache/lucene/util/fst/IntsRefFSTEnum$InputOutput;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/apache/lucene/util/fst/IntsRefFSTEnum$InputOutput",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 118
    .local p0, "this":Lorg/apache/lucene/util/fst/IntsRefFSTEnum;, "Lorg/apache/lucene/util/fst/IntsRefFSTEnum<TT;>;"
    iget v0, p0, Lorg/apache/lucene/util/fst/IntsRefFSTEnum;->upto:I

    if-nez v0, :cond_0

    .line 119
    const/4 v0, 0x0

    .line 123
    :goto_0
    return-object v0

    .line 121
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/util/fst/IntsRefFSTEnum;->current:Lorg/apache/lucene/util/IntsRef;

    iget v1, p0, Lorg/apache/lucene/util/fst/IntsRefFSTEnum;->upto:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Lorg/apache/lucene/util/IntsRef;->length:I

    .line 122
    iget-object v0, p0, Lorg/apache/lucene/util/fst/IntsRefFSTEnum;->result:Lorg/apache/lucene/util/fst/IntsRefFSTEnum$InputOutput;

    iget-object v1, p0, Lorg/apache/lucene/util/fst/IntsRefFSTEnum;->output:[Ljava/lang/Object;

    iget v2, p0, Lorg/apache/lucene/util/fst/IntsRefFSTEnum;->upto:I

    aget-object v1, v1, v2

    iput-object v1, v0, Lorg/apache/lucene/util/fst/IntsRefFSTEnum$InputOutput;->output:Ljava/lang/Object;

    .line 123
    iget-object v0, p0, Lorg/apache/lucene/util/fst/IntsRefFSTEnum;->result:Lorg/apache/lucene/util/fst/IntsRefFSTEnum$InputOutput;

    goto :goto_0
.end method


# virtual methods
.method public current()Lorg/apache/lucene/util/fst/IntsRefFSTEnum$InputOutput;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/apache/lucene/util/fst/IntsRefFSTEnum$InputOutput",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 52
    .local p0, "this":Lorg/apache/lucene/util/fst/IntsRefFSTEnum;, "Lorg/apache/lucene/util/fst/IntsRefFSTEnum<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/util/fst/IntsRefFSTEnum;->result:Lorg/apache/lucene/util/fst/IntsRefFSTEnum$InputOutput;

    return-object v0
.end method

.method protected getCurrentLabel()I
    .locals 2

    .prologue
    .line 104
    .local p0, "this":Lorg/apache/lucene/util/fst/IntsRefFSTEnum;, "Lorg/apache/lucene/util/fst/IntsRefFSTEnum<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/util/fst/IntsRefFSTEnum;->current:Lorg/apache/lucene/util/IntsRef;

    iget-object v0, v0, Lorg/apache/lucene/util/IntsRef;->ints:[I

    iget v1, p0, Lorg/apache/lucene/util/fst/IntsRefFSTEnum;->upto:I

    aget v0, v0, v1

    return v0
.end method

.method protected getTargetLabel()I
    .locals 3

    .prologue
    .line 94
    .local p0, "this":Lorg/apache/lucene/util/fst/IntsRefFSTEnum;, "Lorg/apache/lucene/util/fst/IntsRefFSTEnum<TT;>;"
    iget v0, p0, Lorg/apache/lucene/util/fst/IntsRefFSTEnum;->upto:I

    add-int/lit8 v0, v0, -0x1

    iget-object v1, p0, Lorg/apache/lucene/util/fst/IntsRefFSTEnum;->target:Lorg/apache/lucene/util/IntsRef;

    iget v1, v1, Lorg/apache/lucene/util/IntsRef;->length:I

    if-ne v0, v1, :cond_0

    .line 95
    const/4 v0, -0x1

    .line 97
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/util/fst/IntsRefFSTEnum;->target:Lorg/apache/lucene/util/IntsRef;

    iget-object v0, v0, Lorg/apache/lucene/util/IntsRef;->ints:[I

    iget-object v1, p0, Lorg/apache/lucene/util/fst/IntsRefFSTEnum;->target:Lorg/apache/lucene/util/IntsRef;

    iget v1, v1, Lorg/apache/lucene/util/IntsRef;->offset:I

    iget v2, p0, Lorg/apache/lucene/util/fst/IntsRefFSTEnum;->upto:I

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    goto :goto_0
.end method

.method protected grow()V
    .locals 3

    .prologue
    .line 114
    .local p0, "this":Lorg/apache/lucene/util/fst/IntsRefFSTEnum;, "Lorg/apache/lucene/util/fst/IntsRefFSTEnum<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/util/fst/IntsRefFSTEnum;->current:Lorg/apache/lucene/util/IntsRef;

    iget-object v1, p0, Lorg/apache/lucene/util/fst/IntsRefFSTEnum;->current:Lorg/apache/lucene/util/IntsRef;

    iget-object v1, v1, Lorg/apache/lucene/util/IntsRef;->ints:[I

    iget v2, p0, Lorg/apache/lucene/util/fst/IntsRefFSTEnum;->upto:I

    add-int/lit8 v2, v2, 0x1

    invoke-static {v1, v2}, Lorg/apache/lucene/util/ArrayUtil;->grow([II)[I

    move-result-object v1

    iput-object v1, v0, Lorg/apache/lucene/util/IntsRef;->ints:[I

    .line 115
    return-void
.end method

.method public next()Lorg/apache/lucene/util/fst/IntsRefFSTEnum$InputOutput;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/apache/lucene/util/fst/IntsRefFSTEnum$InputOutput",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 57
    .local p0, "this":Lorg/apache/lucene/util/fst/IntsRefFSTEnum;, "Lorg/apache/lucene/util/fst/IntsRefFSTEnum<TT;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/util/fst/IntsRefFSTEnum;->doNext()V

    .line 58
    invoke-direct {p0}, Lorg/apache/lucene/util/fst/IntsRefFSTEnum;->setResult()Lorg/apache/lucene/util/fst/IntsRefFSTEnum$InputOutput;

    move-result-object v0

    return-object v0
.end method

.method public seekCeil(Lorg/apache/lucene/util/IntsRef;)Lorg/apache/lucene/util/fst/IntsRefFSTEnum$InputOutput;
    .locals 1
    .param p1, "target"    # Lorg/apache/lucene/util/IntsRef;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/IntsRef;",
            ")",
            "Lorg/apache/lucene/util/fst/IntsRefFSTEnum$InputOutput",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 63
    .local p0, "this":Lorg/apache/lucene/util/fst/IntsRefFSTEnum;, "Lorg/apache/lucene/util/fst/IntsRefFSTEnum<TT;>;"
    iput-object p1, p0, Lorg/apache/lucene/util/fst/IntsRefFSTEnum;->target:Lorg/apache/lucene/util/IntsRef;

    .line 64
    iget v0, p1, Lorg/apache/lucene/util/IntsRef;->length:I

    iput v0, p0, Lorg/apache/lucene/util/fst/IntsRefFSTEnum;->targetLength:I

    .line 65
    invoke-super {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->doSeekCeil()V

    .line 66
    invoke-direct {p0}, Lorg/apache/lucene/util/fst/IntsRefFSTEnum;->setResult()Lorg/apache/lucene/util/fst/IntsRefFSTEnum$InputOutput;

    move-result-object v0

    return-object v0
.end method

.method public seekExact(Lorg/apache/lucene/util/IntsRef;)Lorg/apache/lucene/util/fst/IntsRefFSTEnum$InputOutput;
    .locals 2
    .param p1, "target"    # Lorg/apache/lucene/util/IntsRef;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/IntsRef;",
            ")",
            "Lorg/apache/lucene/util/fst/IntsRefFSTEnum$InputOutput",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 82
    .local p0, "this":Lorg/apache/lucene/util/fst/IntsRefFSTEnum;, "Lorg/apache/lucene/util/fst/IntsRefFSTEnum<TT;>;"
    iput-object p1, p0, Lorg/apache/lucene/util/fst/IntsRefFSTEnum;->target:Lorg/apache/lucene/util/IntsRef;

    .line 83
    iget v0, p1, Lorg/apache/lucene/util/IntsRef;->length:I

    iput v0, p0, Lorg/apache/lucene/util/fst/IntsRefFSTEnum;->targetLength:I

    .line 84
    invoke-super {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->doSeekExact()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 85
    sget-boolean v0, Lorg/apache/lucene/util/fst/IntsRefFSTEnum;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/util/fst/IntsRefFSTEnum;->upto:I

    iget v1, p1, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/lit8 v1, v1, 0x1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 86
    :cond_0
    invoke-direct {p0}, Lorg/apache/lucene/util/fst/IntsRefFSTEnum;->setResult()Lorg/apache/lucene/util/fst/IntsRefFSTEnum$InputOutput;

    move-result-object v0

    .line 88
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public seekFloor(Lorg/apache/lucene/util/IntsRef;)Lorg/apache/lucene/util/fst/IntsRefFSTEnum$InputOutput;
    .locals 1
    .param p1, "target"    # Lorg/apache/lucene/util/IntsRef;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/IntsRef;",
            ")",
            "Lorg/apache/lucene/util/fst/IntsRefFSTEnum$InputOutput",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 71
    .local p0, "this":Lorg/apache/lucene/util/fst/IntsRefFSTEnum;, "Lorg/apache/lucene/util/fst/IntsRefFSTEnum<TT;>;"
    iput-object p1, p0, Lorg/apache/lucene/util/fst/IntsRefFSTEnum;->target:Lorg/apache/lucene/util/IntsRef;

    .line 72
    iget v0, p1, Lorg/apache/lucene/util/IntsRef;->length:I

    iput v0, p0, Lorg/apache/lucene/util/fst/IntsRefFSTEnum;->targetLength:I

    .line 73
    invoke-super {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->doSeekFloor()V

    .line 74
    invoke-direct {p0}, Lorg/apache/lucene/util/fst/IntsRefFSTEnum;->setResult()Lorg/apache/lucene/util/fst/IntsRefFSTEnum$InputOutput;

    move-result-object v0

    return-object v0
.end method

.method protected setCurrentLabel(I)V
    .locals 2
    .param p1, "label"    # I

    .prologue
    .line 109
    .local p0, "this":Lorg/apache/lucene/util/fst/IntsRefFSTEnum;, "Lorg/apache/lucene/util/fst/IntsRefFSTEnum<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/util/fst/IntsRefFSTEnum;->current:Lorg/apache/lucene/util/IntsRef;

    iget-object v0, v0, Lorg/apache/lucene/util/IntsRef;->ints:[I

    iget v1, p0, Lorg/apache/lucene/util/fst/IntsRefFSTEnum;->upto:I

    aput p1, v0, v1

    .line 110
    return-void
.end method
