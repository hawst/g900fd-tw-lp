.class public abstract Lorg/apache/lucene/util/AttributeImpl;
.super Ljava/lang/Object;
.source "AttributeImpl.java"

# interfaces
.implements Ljava/lang/Cloneable;
.implements Lorg/apache/lucene/util/Attribute;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract clear()V
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/util/AttributeImpl;->clone()Lorg/apache/lucene/util/AttributeImpl;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lorg/apache/lucene/util/AttributeImpl;
    .locals 3

    .prologue
    .line 128
    const/4 v0, 0x0

    .line 130
    .local v0, "clone":Lorg/apache/lucene/util/AttributeImpl;
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "clone":Lorg/apache/lucene/util/AttributeImpl;
    check-cast v0, Lorg/apache/lucene/util/AttributeImpl;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 134
    .restart local v0    # "clone":Lorg/apache/lucene/util/AttributeImpl;
    return-object v0

    .line 131
    .end local v0    # "clone":Lorg/apache/lucene/util/AttributeImpl;
    :catch_0
    move-exception v1

    .line 132
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method public abstract copyTo(Lorg/apache/lucene/util/AttributeImpl;)V
.end method

.method public final reflectAsString(Z)Ljava/lang/String;
    .locals 2
    .param p1, "prependAttClass"    # Z

    .prologue
    .line 52
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 53
    .local v0, "buffer":Ljava/lang/StringBuilder;
    new-instance v1, Lorg/apache/lucene/util/AttributeImpl$1;

    invoke-direct {v1, p0, v0, p1}, Lorg/apache/lucene/util/AttributeImpl$1;-><init>(Lorg/apache/lucene/util/AttributeImpl;Ljava/lang/StringBuilder;Z)V

    invoke-virtual {p0, v1}, Lorg/apache/lucene/util/AttributeImpl;->reflectWith(Lorg/apache/lucene/util/AttributeReflector;)V

    .line 65
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public reflectWith(Lorg/apache/lucene/util/AttributeReflector;)V
    .locals 10
    .param p1, "reflector"    # Lorg/apache/lucene/util/AttributeReflector;

    .prologue
    const/4 v8, 0x1

    .line 93
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 94
    .local v0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/apache/lucene/util/AttributeImpl;>;"
    invoke-static {v0}, Lorg/apache/lucene/util/AttributeSource;->getAttributeInterfaces(Ljava/lang/Class;)Ljava/util/LinkedList;

    move-result-object v6

    .line 95
    .local v6, "interfaces":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/ref/WeakReference<Ljava/lang/Class<+Lorg/apache/lucene/util/Attribute;>;>;>;"
    invoke-virtual {v6}, Ljava/util/LinkedList;->size()I

    move-result v7

    if-eq v7, v8, :cond_0

    .line 96
    new-instance v7, Ljava/lang/UnsupportedOperationException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 97
    const-string v9, " implements more than one Attribute interface, the default reflectWith() implementation cannot handle this."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 96
    invoke-direct {v7, v8}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 99
    :cond_0
    invoke-virtual {v6}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/ref/WeakReference;

    invoke-virtual {v7}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Class;

    .line 100
    .local v5, "interf":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/apache/lucene/util/Attribute;>;"
    invoke-virtual {v0}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v3

    .line 102
    .local v3, "fields":[Ljava/lang/reflect/Field;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    :try_start_0
    array-length v7, v3

    if-lt v4, v7, :cond_1

    .line 113
    return-void

    .line 103
    :cond_1
    aget-object v2, v3, v4

    .line 104
    .local v2, "f":Ljava/lang/reflect/Field;
    invoke-virtual {v2}, Ljava/lang/reflect/Field;->getModifiers()I

    move-result v7

    invoke-static {v7}, Ljava/lang/reflect/Modifier;->isStatic(I)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 102
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 105
    :cond_2
    const/4 v7, 0x1

    invoke-virtual {v2, v7}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 106
    invoke-virtual {v2}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    invoke-interface {p1, v5, v7, v8}, Lorg/apache/lucene/util/AttributeReflector;->reflect(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 108
    .end local v2    # "f":Ljava/lang/reflect/Field;
    :catch_0
    move-exception v1

    .line 111
    .local v1, "e":Ljava/lang/IllegalAccessException;
    new-instance v7, Ljava/lang/RuntimeException;

    invoke-direct {v7, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v7
.end method
