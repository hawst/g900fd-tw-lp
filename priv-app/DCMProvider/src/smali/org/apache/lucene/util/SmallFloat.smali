.class public Lorg/apache/lucene/util/SmallFloat;
.super Ljava/lang/Object;
.source "SmallFloat.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static byte315ToFloat(B)F
    .locals 2
    .param p0, "b"    # B

    .prologue
    .line 94
    if-nez p0, :cond_0

    const/4 v1, 0x0

    .line 97
    :goto_0
    return v1

    .line 95
    :cond_0
    and-int/lit16 v1, p0, 0xff

    shl-int/lit8 v0, v1, 0x15

    .line 96
    .local v0, "bits":I
    const/high16 v1, 0x30000000

    add-int/2addr v0, v1

    .line 97
    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v1

    goto :goto_0
.end method

.method public static byte52ToFloat(B)F
    .locals 2
    .param p0, "b"    # B

    .prologue
    .line 122
    if-nez p0, :cond_0

    const/4 v1, 0x0

    .line 125
    :goto_0
    return v1

    .line 123
    :cond_0
    and-int/lit16 v1, p0, 0xff

    shl-int/lit8 v0, v1, 0x13

    .line 124
    .local v0, "bits":I
    const/high16 v1, 0x3d000000    # 0.03125f

    add-int/2addr v0, v1

    .line 125
    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v1

    goto :goto_0
.end method

.method public static byteToFloat(BII)F
    .locals 3
    .param p0, "b"    # B
    .param p1, "numMantissaBits"    # I
    .param p2, "zeroExp"    # I

    .prologue
    .line 60
    if-nez p0, :cond_0

    const/4 v1, 0x0

    .line 63
    :goto_0
    return v1

    .line 61
    :cond_0
    and-int/lit16 v1, p0, 0xff

    rsub-int/lit8 v2, p1, 0x18

    shl-int v0, v1, v2

    .line 62
    .local v0, "bits":I
    rsub-int/lit8 v1, p2, 0x3f

    shl-int/lit8 v1, v1, 0x18

    add-int/2addr v0, v1

    .line 63
    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v1

    goto :goto_0
.end method

.method public static floatToByte(FII)B
    .locals 4
    .param p0, "f"    # F
    .param p1, "numMantissaBits"    # I
    .param p2, "zeroExp"    # I

    .prologue
    .line 42
    rsub-int/lit8 v3, p2, 0x3f

    shl-int v1, v3, p1

    .line 43
    .local v1, "fzero":I
    invoke-static {p0}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    .line 44
    .local v0, "bits":I
    rsub-int/lit8 v3, p1, 0x18

    shr-int v2, v0, v3

    .line 45
    .local v2, "smallfloat":I
    if-gt v2, v1, :cond_1

    .line 46
    if-gtz v0, :cond_0

    .line 47
    const/4 v3, 0x0

    .line 52
    :goto_0
    return v3

    .line 48
    :cond_0
    const/4 v3, 0x1

    goto :goto_0

    .line 49
    :cond_1
    add-int/lit16 v3, v1, 0x100

    if-lt v2, v3, :cond_2

    .line 50
    const/4 v3, -0x1

    goto :goto_0

    .line 52
    :cond_2
    sub-int v3, v2, v1

    int-to-byte v3, v3

    goto :goto_0
.end method

.method public static floatToByte315(F)B
    .locals 3
    .param p0, "f"    # F

    .prologue
    .line 79
    invoke-static {p0}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    .line 80
    .local v0, "bits":I
    shr-int/lit8 v1, v0, 0x15

    .line 81
    .local v1, "smallfloat":I
    const/16 v2, 0x180

    if-gt v1, v2, :cond_1

    .line 82
    if-gtz v0, :cond_0

    const/4 v2, 0x0

    .line 87
    :goto_0
    return v2

    .line 82
    :cond_0
    const/4 v2, 0x1

    goto :goto_0

    .line 84
    :cond_1
    const/16 v2, 0x280

    if-lt v1, v2, :cond_2

    .line 85
    const/4 v2, -0x1

    goto :goto_0

    .line 87
    :cond_2
    add-int/lit16 v2, v1, -0x180

    int-to-byte v2, v2

    goto :goto_0
.end method

.method public static floatToByte52(F)B
    .locals 3
    .param p0, "f"    # F

    .prologue
    .line 107
    invoke-static {p0}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    .line 108
    .local v0, "bits":I
    shr-int/lit8 v1, v0, 0x13

    .line 109
    .local v1, "smallfloat":I
    const/16 v2, 0x7a0

    if-gt v1, v2, :cond_1

    .line 110
    if-gtz v0, :cond_0

    const/4 v2, 0x0

    .line 115
    :goto_0
    return v2

    .line 110
    :cond_0
    const/4 v2, 0x1

    goto :goto_0

    .line 112
    :cond_1
    const/16 v2, 0x8a0

    if-lt v1, v2, :cond_2

    .line 113
    const/4 v2, -0x1

    goto :goto_0

    .line 115
    :cond_2
    add-int/lit16 v2, v1, -0x7a0

    int-to-byte v2, v2

    goto :goto_0
.end method
