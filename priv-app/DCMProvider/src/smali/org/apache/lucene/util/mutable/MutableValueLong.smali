.class public Lorg/apache/lucene/util/mutable/MutableValueLong;
.super Lorg/apache/lucene/util/mutable/MutableValue;
.source "MutableValueLong.java"


# instance fields
.field public value:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lorg/apache/lucene/util/mutable/MutableValue;-><init>()V

    return-void
.end method


# virtual methods
.method public compareSameType(Ljava/lang/Object;)I
    .locals 8
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v4, -0x1

    .line 54
    move-object v0, p1

    check-cast v0, Lorg/apache/lucene/util/mutable/MutableValueLong;

    .line 55
    .local v0, "b":Lorg/apache/lucene/util/mutable/MutableValueLong;
    iget-wide v2, v0, Lorg/apache/lucene/util/mutable/MutableValueLong;->value:J

    .line 56
    .local v2, "bv":J
    iget-wide v6, p0, Lorg/apache/lucene/util/mutable/MutableValueLong;->value:J

    cmp-long v5, v6, v2

    if-gez v5, :cond_1

    move v1, v4

    .line 59
    :cond_0
    :goto_0
    return v1

    .line 57
    :cond_1
    iget-wide v6, p0, Lorg/apache/lucene/util/mutable/MutableValueLong;->value:J

    cmp-long v5, v6, v2

    if-gtz v5, :cond_0

    .line 58
    iget-boolean v5, p0, Lorg/apache/lucene/util/mutable/MutableValueLong;->exists:Z

    iget-boolean v6, v0, Lorg/apache/lucene/util/mutable/MutableValueLong;->exists:Z

    if-ne v5, v6, :cond_2

    const/4 v1, 0x0

    goto :goto_0

    .line 59
    :cond_2
    iget-boolean v5, p0, Lorg/apache/lucene/util/mutable/MutableValueLong;->exists:Z

    if-nez v5, :cond_0

    move v1, v4

    goto :goto_0
.end method

.method public copy(Lorg/apache/lucene/util/mutable/MutableValue;)V
    .locals 4
    .param p1, "source"    # Lorg/apache/lucene/util/mutable/MutableValue;

    .prologue
    .line 33
    move-object v0, p1

    check-cast v0, Lorg/apache/lucene/util/mutable/MutableValueLong;

    .line 34
    .local v0, "s":Lorg/apache/lucene/util/mutable/MutableValueLong;
    iget-boolean v1, v0, Lorg/apache/lucene/util/mutable/MutableValueLong;->exists:Z

    iput-boolean v1, p0, Lorg/apache/lucene/util/mutable/MutableValueLong;->exists:Z

    .line 35
    iget-wide v2, v0, Lorg/apache/lucene/util/mutable/MutableValueLong;->value:J

    iput-wide v2, p0, Lorg/apache/lucene/util/mutable/MutableValueLong;->value:J

    .line 36
    return-void
.end method

.method public duplicate()Lorg/apache/lucene/util/mutable/MutableValue;
    .locals 4

    .prologue
    .line 40
    new-instance v0, Lorg/apache/lucene/util/mutable/MutableValueLong;

    invoke-direct {v0}, Lorg/apache/lucene/util/mutable/MutableValueLong;-><init>()V

    .line 41
    .local v0, "v":Lorg/apache/lucene/util/mutable/MutableValueLong;
    iget-wide v2, p0, Lorg/apache/lucene/util/mutable/MutableValueLong;->value:J

    iput-wide v2, v0, Lorg/apache/lucene/util/mutable/MutableValueLong;->value:J

    .line 42
    iget-boolean v1, p0, Lorg/apache/lucene/util/mutable/MutableValueLong;->exists:Z

    iput-boolean v1, v0, Lorg/apache/lucene/util/mutable/MutableValueLong;->exists:Z

    .line 43
    return-object v0
.end method

.method public equalsSameType(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 48
    move-object v0, p1

    check-cast v0, Lorg/apache/lucene/util/mutable/MutableValueLong;

    .line 49
    .local v0, "b":Lorg/apache/lucene/util/mutable/MutableValueLong;
    iget-wide v2, p0, Lorg/apache/lucene/util/mutable/MutableValueLong;->value:J

    iget-wide v4, v0, Lorg/apache/lucene/util/mutable/MutableValueLong;->value:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lorg/apache/lucene/util/mutable/MutableValueLong;->exists:Z

    iget-boolean v2, v0, Lorg/apache/lucene/util/mutable/MutableValueLong;->exists:Z

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 65
    iget-wide v0, p0, Lorg/apache/lucene/util/mutable/MutableValueLong;->value:J

    long-to-int v0, v0

    iget-wide v2, p0, Lorg/apache/lucene/util/mutable/MutableValueLong;->value:J

    const/16 v1, 0x20

    shr-long/2addr v2, v1

    long-to-int v1, v2

    add-int/2addr v0, v1

    return v0
.end method

.method public toObject()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 28
    iget-boolean v0, p0, Lorg/apache/lucene/util/mutable/MutableValueLong;->exists:Z

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lorg/apache/lucene/util/mutable/MutableValueLong;->value:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
