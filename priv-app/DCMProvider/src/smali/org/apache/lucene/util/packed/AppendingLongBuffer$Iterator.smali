.class public final Lorg/apache/lucene/util/packed/AppendingLongBuffer$Iterator;
.super Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer$Iterator;
.source "AppendingLongBuffer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/packed/AppendingLongBuffer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "Iterator"
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/util/packed/AppendingLongBuffer;


# direct methods
.method private constructor <init>(Lorg/apache/lucene/util/packed/AppendingLongBuffer;)V
    .locals 0

    .prologue
    .line 81
    iput-object p1, p0, Lorg/apache/lucene/util/packed/AppendingLongBuffer$Iterator;->this$0:Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    .line 82
    invoke-direct {p0, p1}, Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer$Iterator;-><init>(Lorg/apache/lucene/util/packed/AbstractAppendingLongBuffer;)V

    .line 83
    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/util/packed/AppendingLongBuffer;Lorg/apache/lucene/util/packed/AppendingLongBuffer$Iterator;)V
    .locals 0

    .prologue
    .line 81
    invoke-direct {p0, p1}, Lorg/apache/lucene/util/packed/AppendingLongBuffer$Iterator;-><init>(Lorg/apache/lucene/util/packed/AppendingLongBuffer;)V

    return-void
.end method


# virtual methods
.method fillValues()V
    .locals 7

    .prologue
    const/16 v6, 0x400

    .line 86
    iget v1, p0, Lorg/apache/lucene/util/packed/AppendingLongBuffer$Iterator;->vOff:I

    iget-object v2, p0, Lorg/apache/lucene/util/packed/AppendingLongBuffer$Iterator;->this$0:Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    iget v2, v2, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->valuesOff:I

    if-ne v1, v2, :cond_1

    .line 87
    iget-object v1, p0, Lorg/apache/lucene/util/packed/AppendingLongBuffer$Iterator;->this$0:Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    iget-object v1, v1, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->pending:[J

    iput-object v1, p0, Lorg/apache/lucene/util/packed/AppendingLongBuffer$Iterator;->currentValues:[J

    .line 98
    :cond_0
    :goto_0
    return-void

    .line 88
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/util/packed/AppendingLongBuffer$Iterator;->this$0:Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    iget-object v1, v1, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->deltas:[Lorg/apache/lucene/util/packed/PackedInts$Reader;

    iget v2, p0, Lorg/apache/lucene/util/packed/AppendingLongBuffer$Iterator;->vOff:I

    aget-object v1, v1, v2

    if-nez v1, :cond_2

    .line 89
    iget-object v1, p0, Lorg/apache/lucene/util/packed/AppendingLongBuffer$Iterator;->currentValues:[J

    iget-object v2, p0, Lorg/apache/lucene/util/packed/AppendingLongBuffer$Iterator;->this$0:Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    iget-object v2, v2, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->minValues:[J

    iget v3, p0, Lorg/apache/lucene/util/packed/AppendingLongBuffer$Iterator;->vOff:I

    aget-wide v2, v2, v3

    invoke-static {v1, v2, v3}, Ljava/util/Arrays;->fill([JJ)V

    goto :goto_0

    .line 91
    :cond_2
    const/4 v0, 0x0

    .local v0, "k":I
    :goto_1
    if-lt v0, v6, :cond_3

    .line 94
    const/4 v0, 0x0

    :goto_2
    if-ge v0, v6, :cond_0

    .line 95
    iget-object v1, p0, Lorg/apache/lucene/util/packed/AppendingLongBuffer$Iterator;->currentValues:[J

    aget-wide v2, v1, v0

    iget-object v4, p0, Lorg/apache/lucene/util/packed/AppendingLongBuffer$Iterator;->this$0:Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    iget-object v4, v4, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->minValues:[J

    iget v5, p0, Lorg/apache/lucene/util/packed/AppendingLongBuffer$Iterator;->vOff:I

    aget-wide v4, v4, v5

    add-long/2addr v2, v4

    aput-wide v2, v1, v0

    .line 94
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 92
    :cond_3
    iget-object v1, p0, Lorg/apache/lucene/util/packed/AppendingLongBuffer$Iterator;->this$0:Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    iget-object v1, v1, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->deltas:[Lorg/apache/lucene/util/packed/PackedInts$Reader;

    iget v2, p0, Lorg/apache/lucene/util/packed/AppendingLongBuffer$Iterator;->vOff:I

    aget-object v1, v1, v2

    iget-object v2, p0, Lorg/apache/lucene/util/packed/AppendingLongBuffer$Iterator;->currentValues:[J

    rsub-int v3, v0, 0x400

    invoke-interface {v1, v0, v2, v0, v3}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I[JII)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_1
.end method
