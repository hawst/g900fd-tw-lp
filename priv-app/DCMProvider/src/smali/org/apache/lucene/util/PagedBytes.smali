.class public final Lorg/apache/lucene/util/PagedBytes;
.super Ljava/lang/Object;
.source "PagedBytes.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;,
        Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;,
        Lorg/apache/lucene/util/PagedBytes$Reader;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final EMPTY_BYTES:[B


# instance fields
.field private final blockBits:I

.field private final blockEnd:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final blockMask:I

.field private final blockSize:I

.field private final blocks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<[B>;"
        }
    .end annotation
.end field

.field private currentBlock:[B

.field private didSkipBytes:Z

.field private frozen:Z

.field private upto:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 37
    const-class v0, Lorg/apache/lucene/util/PagedBytes;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/PagedBytes;->$assertionsDisabled:Z

    .line 48
    new-array v0, v1, [B

    sput-object v0, Lorg/apache/lucene/util/PagedBytes;->EMPTY_BYTES:[B

    return-void

    :cond_0
    move v0, v1

    .line 37
    goto :goto_0
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "blockBits"    # I

    .prologue
    .line 131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/util/PagedBytes;->blocks:Ljava/util/List;

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/util/PagedBytes;->blockEnd:Ljava/util/List;

    .line 132
    sget-boolean v0, Lorg/apache/lucene/util/PagedBytes;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-lez p1, :cond_0

    const/16 v0, 0x1f

    if-le p1, v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, p1}, Ljava/lang/AssertionError;-><init>(I)V

    throw v0

    .line 133
    :cond_1
    const/4 v0, 0x1

    shl-int/2addr v0, p1

    iput v0, p0, Lorg/apache/lucene/util/PagedBytes;->blockSize:I

    .line 134
    iput p1, p0, Lorg/apache/lucene/util/PagedBytes;->blockBits:I

    .line 135
    iget v0, p0, Lorg/apache/lucene/util/PagedBytes;->blockSize:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/util/PagedBytes;->blockMask:I

    .line 136
    iget v0, p0, Lorg/apache/lucene/util/PagedBytes;->blockSize:I

    iput v0, p0, Lorg/apache/lucene/util/PagedBytes;->upto:I

    .line 137
    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/util/PagedBytes;)Ljava/util/List;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lorg/apache/lucene/util/PagedBytes;->blocks:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1(Lorg/apache/lucene/util/PagedBytes;)Ljava/util/List;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lorg/apache/lucene/util/PagedBytes;->blockEnd:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$2(Lorg/apache/lucene/util/PagedBytes;)I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lorg/apache/lucene/util/PagedBytes;->blockBits:I

    return v0
.end method

.method static synthetic access$3(Lorg/apache/lucene/util/PagedBytes;)I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lorg/apache/lucene/util/PagedBytes;->blockMask:I

    return v0
.end method

.method static synthetic access$4(Lorg/apache/lucene/util/PagedBytes;)I
    .locals 1

    .prologue
    .line 40
    iget v0, p0, Lorg/apache/lucene/util/PagedBytes;->blockSize:I

    return v0
.end method

.method static synthetic access$5(Lorg/apache/lucene/util/PagedBytes;)I
    .locals 1

    .prologue
    .line 45
    iget v0, p0, Lorg/apache/lucene/util/PagedBytes;->upto:I

    return v0
.end method

.method static synthetic access$6(Lorg/apache/lucene/util/PagedBytes;)[B
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lorg/apache/lucene/util/PagedBytes;->currentBlock:[B

    return-object v0
.end method

.method static synthetic access$7(Lorg/apache/lucene/util/PagedBytes;[B)V
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lorg/apache/lucene/util/PagedBytes;->currentBlock:[B

    return-void
.end method

.method static synthetic access$8(Lorg/apache/lucene/util/PagedBytes;I)V
    .locals 0

    .prologue
    .line 45
    iput p1, p0, Lorg/apache/lucene/util/PagedBytes;->upto:I

    return-void
.end method


# virtual methods
.method public copy(Lorg/apache/lucene/store/IndexInput;J)V
    .locals 6
    .param p1, "in"    # Lorg/apache/lucene/store/IndexInput;
    .param p2, "byteCount"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 141
    :goto_0
    const-wide/16 v2, 0x0

    cmp-long v1, p2, v2

    if-gtz v1, :cond_0

    .line 162
    :goto_1
    return-void

    .line 142
    :cond_0
    iget v1, p0, Lorg/apache/lucene/util/PagedBytes;->blockSize:I

    iget v2, p0, Lorg/apache/lucene/util/PagedBytes;->upto:I

    sub-int v0, v1, v2

    .line 143
    .local v0, "left":I
    if-nez v0, :cond_2

    .line 144
    iget-object v1, p0, Lorg/apache/lucene/util/PagedBytes;->currentBlock:[B

    if-eqz v1, :cond_1

    .line 145
    iget-object v1, p0, Lorg/apache/lucene/util/PagedBytes;->blocks:Ljava/util/List;

    iget-object v2, p0, Lorg/apache/lucene/util/PagedBytes;->currentBlock:[B

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 146
    iget-object v1, p0, Lorg/apache/lucene/util/PagedBytes;->blockEnd:Ljava/util/List;

    iget v2, p0, Lorg/apache/lucene/util/PagedBytes;->upto:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 148
    :cond_1
    iget v1, p0, Lorg/apache/lucene/util/PagedBytes;->blockSize:I

    new-array v1, v1, [B

    iput-object v1, p0, Lorg/apache/lucene/util/PagedBytes;->currentBlock:[B

    .line 149
    iput v4, p0, Lorg/apache/lucene/util/PagedBytes;->upto:I

    .line 150
    iget v0, p0, Lorg/apache/lucene/util/PagedBytes;->blockSize:I

    .line 152
    :cond_2
    int-to-long v2, v0

    cmp-long v1, v2, p2

    if-gez v1, :cond_3

    .line 153
    iget-object v1, p0, Lorg/apache/lucene/util/PagedBytes;->currentBlock:[B

    iget v2, p0, Lorg/apache/lucene/util/PagedBytes;->upto:I

    invoke-virtual {p1, v1, v2, v0, v4}, Lorg/apache/lucene/store/IndexInput;->readBytes([BIIZ)V

    .line 154
    iget v1, p0, Lorg/apache/lucene/util/PagedBytes;->blockSize:I

    iput v1, p0, Lorg/apache/lucene/util/PagedBytes;->upto:I

    .line 155
    int-to-long v2, v0

    sub-long/2addr p2, v2

    .line 156
    goto :goto_0

    .line 157
    :cond_3
    iget-object v1, p0, Lorg/apache/lucene/util/PagedBytes;->currentBlock:[B

    iget v2, p0, Lorg/apache/lucene/util/PagedBytes;->upto:I

    long-to-int v3, p2

    invoke-virtual {p1, v1, v2, v3, v4}, Lorg/apache/lucene/store/IndexInput;->readBytes([BIIZ)V

    .line 158
    iget v1, p0, Lorg/apache/lucene/util/PagedBytes;->upto:I

    int-to-long v2, v1

    add-long/2addr v2, p2

    long-to-int v1, v2

    iput v1, p0, Lorg/apache/lucene/util/PagedBytes;->upto:I

    goto :goto_1
.end method

.method public copy(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)V
    .locals 6
    .param p1, "bytes"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "out"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 168
    iget v1, p0, Lorg/apache/lucene/util/PagedBytes;->blockSize:I

    iget v2, p0, Lorg/apache/lucene/util/PagedBytes;->upto:I

    sub-int v0, v1, v2

    .line 169
    .local v0, "left":I
    iget v1, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    if-gt v1, v0, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/util/PagedBytes;->currentBlock:[B

    if-nez v1, :cond_2

    .line 170
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/util/PagedBytes;->currentBlock:[B

    if-eqz v1, :cond_1

    .line 171
    iget-object v1, p0, Lorg/apache/lucene/util/PagedBytes;->blocks:Ljava/util/List;

    iget-object v2, p0, Lorg/apache/lucene/util/PagedBytes;->currentBlock:[B

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 172
    iget-object v1, p0, Lorg/apache/lucene/util/PagedBytes;->blockEnd:Ljava/util/List;

    iget v2, p0, Lorg/apache/lucene/util/PagedBytes;->upto:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 173
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/apache/lucene/util/PagedBytes;->didSkipBytes:Z

    .line 175
    :cond_1
    iget v1, p0, Lorg/apache/lucene/util/PagedBytes;->blockSize:I

    new-array v1, v1, [B

    iput-object v1, p0, Lorg/apache/lucene/util/PagedBytes;->currentBlock:[B

    .line 176
    const/4 v1, 0x0

    iput v1, p0, Lorg/apache/lucene/util/PagedBytes;->upto:I

    .line 177
    iget v0, p0, Lorg/apache/lucene/util/PagedBytes;->blockSize:I

    .line 178
    sget-boolean v1, Lorg/apache/lucene/util/PagedBytes;->$assertionsDisabled:Z

    if-nez v1, :cond_2

    iget v1, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    iget v2, p0, Lorg/apache/lucene/util/PagedBytes;->blockSize:I

    if-le v1, v2, :cond_2

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 182
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/util/PagedBytes;->currentBlock:[B

    iput-object v1, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 183
    iget v1, p0, Lorg/apache/lucene/util/PagedBytes;->upto:I

    iput v1, p2, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 184
    iget v1, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    iput v1, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 186
    iget-object v1, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v2, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget-object v3, p0, Lorg/apache/lucene/util/PagedBytes;->currentBlock:[B

    iget v4, p0, Lorg/apache/lucene/util/PagedBytes;->upto:I

    iget v5, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-static {v1, v2, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 187
    iget v1, p0, Lorg/apache/lucene/util/PagedBytes;->upto:I

    iget v2, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/2addr v1, v2

    iput v1, p0, Lorg/apache/lucene/util/PagedBytes;->upto:I

    .line 188
    return-void
.end method

.method public copyUsingLengthPrefix(Lorg/apache/lucene/util/BytesRef;)J
    .locals 7
    .param p1, "bytes"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 225
    iget v2, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    const v3, 0x8000

    if-lt v2, v3, :cond_0

    .line 226
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "max length is 32767 (got "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 229
    :cond_0
    iget v2, p0, Lorg/apache/lucene/util/PagedBytes;->upto:I

    iget v3, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/2addr v2, v3

    add-int/lit8 v2, v2, 0x2

    iget v3, p0, Lorg/apache/lucene/util/PagedBytes;->blockSize:I

    if-le v2, v3, :cond_3

    .line 230
    iget v2, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v2, v2, 0x2

    iget v3, p0, Lorg/apache/lucene/util/PagedBytes;->blockSize:I

    if-le v2, v3, :cond_1

    .line 231
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "block size "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lorg/apache/lucene/util/PagedBytes;->blockSize:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is too small to store length "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " bytes"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 233
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/util/PagedBytes;->currentBlock:[B

    if-eqz v2, :cond_2

    .line 234
    iget-object v2, p0, Lorg/apache/lucene/util/PagedBytes;->blocks:Ljava/util/List;

    iget-object v3, p0, Lorg/apache/lucene/util/PagedBytes;->currentBlock:[B

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 235
    iget-object v2, p0, Lorg/apache/lucene/util/PagedBytes;->blockEnd:Ljava/util/List;

    iget v3, p0, Lorg/apache/lucene/util/PagedBytes;->upto:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 237
    :cond_2
    iget v2, p0, Lorg/apache/lucene/util/PagedBytes;->blockSize:I

    new-array v2, v2, [B

    iput-object v2, p0, Lorg/apache/lucene/util/PagedBytes;->currentBlock:[B

    .line 238
    const/4 v2, 0x0

    iput v2, p0, Lorg/apache/lucene/util/PagedBytes;->upto:I

    .line 241
    :cond_3
    invoke-virtual {p0}, Lorg/apache/lucene/util/PagedBytes;->getPointer()J

    move-result-wide v0

    .line 243
    .local v0, "pointer":J
    iget v2, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    const/16 v3, 0x80

    if-ge v2, v3, :cond_4

    .line 244
    iget-object v2, p0, Lorg/apache/lucene/util/PagedBytes;->currentBlock:[B

    iget v3, p0, Lorg/apache/lucene/util/PagedBytes;->upto:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lorg/apache/lucene/util/PagedBytes;->upto:I

    iget v4, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    .line 249
    :goto_0
    iget-object v2, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v3, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget-object v4, p0, Lorg/apache/lucene/util/PagedBytes;->currentBlock:[B

    iget v5, p0, Lorg/apache/lucene/util/PagedBytes;->upto:I

    iget v6, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-static {v2, v3, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 250
    iget v2, p0, Lorg/apache/lucene/util/PagedBytes;->upto:I

    iget v3, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/2addr v2, v3

    iput v2, p0, Lorg/apache/lucene/util/PagedBytes;->upto:I

    .line 252
    return-wide v0

    .line 246
    :cond_4
    iget-object v2, p0, Lorg/apache/lucene/util/PagedBytes;->currentBlock:[B

    iget v3, p0, Lorg/apache/lucene/util/PagedBytes;->upto:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lorg/apache/lucene/util/PagedBytes;->upto:I

    iget v4, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    shr-int/lit8 v4, v4, 0x8

    or-int/lit16 v4, v4, 0x80

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    .line 247
    iget-object v2, p0, Lorg/apache/lucene/util/PagedBytes;->currentBlock:[B

    iget v3, p0, Lorg/apache/lucene/util/PagedBytes;->upto:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lorg/apache/lucene/util/PagedBytes;->upto:I

    iget v4, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    and-int/lit16 v4, v4, 0xff

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    goto :goto_0
.end method

.method public freeze(Z)Lorg/apache/lucene/util/PagedBytes$Reader;
    .locals 5
    .param p1, "trim"    # Z

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 192
    iget-boolean v1, p0, Lorg/apache/lucene/util/PagedBytes;->frozen:Z

    if-eqz v1, :cond_0

    .line 193
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "already frozen"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 195
    :cond_0
    iget-boolean v1, p0, Lorg/apache/lucene/util/PagedBytes;->didSkipBytes:Z

    if-eqz v1, :cond_1

    .line 196
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "cannot freeze when copy(BytesRef, BytesRef) was used"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 198
    :cond_1
    if-eqz p1, :cond_2

    iget v1, p0, Lorg/apache/lucene/util/PagedBytes;->upto:I

    iget v2, p0, Lorg/apache/lucene/util/PagedBytes;->blockSize:I

    if-ge v1, v2, :cond_2

    .line 199
    iget v1, p0, Lorg/apache/lucene/util/PagedBytes;->upto:I

    new-array v0, v1, [B

    .line 200
    .local v0, "newBlock":[B
    iget-object v1, p0, Lorg/apache/lucene/util/PagedBytes;->currentBlock:[B

    iget v2, p0, Lorg/apache/lucene/util/PagedBytes;->upto:I

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 201
    iput-object v0, p0, Lorg/apache/lucene/util/PagedBytes;->currentBlock:[B

    .line 203
    .end local v0    # "newBlock":[B
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/util/PagedBytes;->currentBlock:[B

    if-nez v1, :cond_3

    .line 204
    sget-object v1, Lorg/apache/lucene/util/PagedBytes;->EMPTY_BYTES:[B

    iput-object v1, p0, Lorg/apache/lucene/util/PagedBytes;->currentBlock:[B

    .line 206
    :cond_3
    iget-object v1, p0, Lorg/apache/lucene/util/PagedBytes;->blocks:Ljava/util/List;

    iget-object v2, p0, Lorg/apache/lucene/util/PagedBytes;->currentBlock:[B

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 207
    iget-object v1, p0, Lorg/apache/lucene/util/PagedBytes;->blockEnd:Ljava/util/List;

    iget v2, p0, Lorg/apache/lucene/util/PagedBytes;->upto:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 208
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/apache/lucene/util/PagedBytes;->frozen:Z

    .line 209
    iput-object v4, p0, Lorg/apache/lucene/util/PagedBytes;->currentBlock:[B

    .line 210
    new-instance v1, Lorg/apache/lucene/util/PagedBytes$Reader;

    invoke-direct {v1, p0, v4}, Lorg/apache/lucene/util/PagedBytes$Reader;-><init>(Lorg/apache/lucene/util/PagedBytes;Lorg/apache/lucene/util/PagedBytes$Reader;)V

    return-object v1
.end method

.method public getDataInput()Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;
    .locals 2

    .prologue
    .line 382
    iget-boolean v0, p0, Lorg/apache/lucene/util/PagedBytes;->frozen:Z

    if-nez v0, :cond_0

    .line 383
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "must call freeze() before getDataInput"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 385
    :cond_0
    new-instance v0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;

    invoke-direct {v0, p0}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;-><init>(Lorg/apache/lucene/util/PagedBytes;)V

    return-object v0
.end method

.method public getDataOutput()Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;
    .locals 2

    .prologue
    .line 393
    iget-boolean v0, p0, Lorg/apache/lucene/util/PagedBytes;->frozen:Z

    if-eqz v0, :cond_0

    .line 394
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "cannot get DataOutput after freeze()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 396
    :cond_0
    new-instance v0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;

    invoke-direct {v0, p0}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;-><init>(Lorg/apache/lucene/util/PagedBytes;)V

    return-object v0
.end method

.method public getPointer()J
    .locals 4

    .prologue
    .line 214
    iget-object v0, p0, Lorg/apache/lucene/util/PagedBytes;->currentBlock:[B

    if-nez v0, :cond_0

    .line 215
    const-wide/16 v0, 0x0

    .line 217
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/util/PagedBytes;->blocks:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    int-to-long v0, v0

    iget v2, p0, Lorg/apache/lucene/util/PagedBytes;->blockSize:I

    int-to-long v2, v2

    mul-long/2addr v0, v2

    iget v2, p0, Lorg/apache/lucene/util/PagedBytes;->upto:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    goto :goto_0
.end method
