.class final Lorg/apache/lucene/util/fst/NodeHash;
.super Ljava/lang/Object;
.source "NodeHash.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private count:I

.field private final fst:Lorg/apache/lucene/util/fst/FST;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/fst/FST",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final in:Lorg/apache/lucene/util/fst/FST$BytesReader;

.field private mask:I

.field private final scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;"
        }
    .end annotation
.end field

.field private table:Lorg/apache/lucene/util/packed/GrowableWriter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lorg/apache/lucene/util/fst/NodeHash;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/fst/NodeHash;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/util/fst/FST;Lorg/apache/lucene/util/fst/FST$BytesReader;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/FST",
            "<TT;>;",
            "Lorg/apache/lucene/util/fst/FST$BytesReader;",
            ")V"
        }
    .end annotation

    .prologue
    .line 35
    .local p0, "this":Lorg/apache/lucene/util/fst/NodeHash;, "Lorg/apache/lucene/util/fst/NodeHash<TT;>;"
    .local p1, "fst":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    .local p2, "in":Lorg/apache/lucene/util/fst/FST$BytesReader;, "Lorg/apache/lucene/util/fst/FST$BytesReader;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-direct {v0}, Lorg/apache/lucene/util/fst/FST$Arc;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/util/fst/NodeHash;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    .line 36
    new-instance v0, Lorg/apache/lucene/util/packed/GrowableWriter;

    const/16 v1, 0x8

    const/16 v2, 0x10

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/util/packed/GrowableWriter;-><init>(IIF)V

    iput-object v0, p0, Lorg/apache/lucene/util/fst/NodeHash;->table:Lorg/apache/lucene/util/packed/GrowableWriter;

    .line 37
    const/16 v0, 0xf

    iput v0, p0, Lorg/apache/lucene/util/fst/NodeHash;->mask:I

    .line 38
    iput-object p1, p0, Lorg/apache/lucene/util/fst/NodeHash;->fst:Lorg/apache/lucene/util/fst/FST;

    .line 39
    iput-object p2, p0, Lorg/apache/lucene/util/fst/NodeHash;->in:Lorg/apache/lucene/util/fst/FST$BytesReader;

    .line 40
    return-void
.end method

.method private addNew(J)V
    .locals 7
    .param p1, "address"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 147
    .local p0, "this":Lorg/apache/lucene/util/fst/NodeHash;, "Lorg/apache/lucene/util/fst/NodeHash<TT;>;"
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/util/fst/NodeHash;->hash(J)I

    move-result v2

    iget v3, p0, Lorg/apache/lucene/util/fst/NodeHash;->mask:I

    and-int v1, v2, v3

    .line 148
    .local v1, "pos":I
    const/4 v0, 0x0

    .line 150
    .local v0, "c":I
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/util/fst/NodeHash;->table:Lorg/apache/lucene/util/packed/GrowableWriter;

    invoke-virtual {v2, v1}, Lorg/apache/lucene/util/packed/GrowableWriter;->get(I)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 151
    iget-object v2, p0, Lorg/apache/lucene/util/fst/NodeHash;->table:Lorg/apache/lucene/util/packed/GrowableWriter;

    invoke-virtual {v2, v1, p1, p2}, Lorg/apache/lucene/util/packed/GrowableWriter;->set(IJ)V

    .line 158
    return-void

    .line 156
    :cond_0
    add-int/lit8 v0, v0, 0x1

    add-int v2, v1, v0

    iget v3, p0, Lorg/apache/lucene/util/fst/NodeHash;->mask:I

    and-int v1, v2, v3

    .line 149
    goto :goto_0
.end method

.method private hash(J)I
    .locals 9
    .param p1, "node"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 95
    .local p0, "this":Lorg/apache/lucene/util/fst/NodeHash;, "Lorg/apache/lucene/util/fst/NodeHash<TT;>;"
    const/16 v0, 0x1f

    .line 97
    .local v0, "PRIME":I
    const/4 v1, 0x0

    .line 98
    .local v1, "h":I
    iget-object v2, p0, Lorg/apache/lucene/util/fst/NodeHash;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v3, p0, Lorg/apache/lucene/util/fst/NodeHash;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget-object v4, p0, Lorg/apache/lucene/util/fst/NodeHash;->in:Lorg/apache/lucene/util/fst/FST$BytesReader;

    invoke-virtual {v2, p1, p2, v3, v4}, Lorg/apache/lucene/util/fst/FST;->readFirstRealTargetArc(JLorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 101
    :goto_0
    mul-int/lit8 v2, v1, 0x1f

    iget-object v3, p0, Lorg/apache/lucene/util/fst/NodeHash;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget v3, v3, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    add-int v1, v2, v3

    .line 102
    mul-int/lit8 v2, v1, 0x1f

    iget-object v3, p0, Lorg/apache/lucene/util/fst/NodeHash;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget-wide v4, v3, Lorg/apache/lucene/util/fst/FST$Arc;->target:J

    iget-object v3, p0, Lorg/apache/lucene/util/fst/NodeHash;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget-wide v6, v3, Lorg/apache/lucene/util/fst/FST$Arc;->target:J

    const/16 v3, 0x20

    shr-long/2addr v6, v3

    xor-long/2addr v4, v6

    long-to-int v3, v4

    add-int v1, v2, v3

    .line 103
    mul-int/lit8 v2, v1, 0x1f

    iget-object v3, p0, Lorg/apache/lucene/util/fst/NodeHash;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget-object v3, v3, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    add-int v1, v2, v3

    .line 104
    mul-int/lit8 v2, v1, 0x1f

    iget-object v3, p0, Lorg/apache/lucene/util/fst/NodeHash;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget-object v3, v3, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    add-int v1, v2, v3

    .line 105
    iget-object v2, p0, Lorg/apache/lucene/util/fst/NodeHash;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-virtual {v2}, Lorg/apache/lucene/util/fst/FST$Arc;->isFinal()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 106
    add-int/lit8 v1, v1, 0x11

    .line 108
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/util/fst/NodeHash;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-virtual {v2}, Lorg/apache/lucene/util/fst/FST$Arc;->isLast()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 114
    const v2, 0x7fffffff

    and-int/2addr v2, v1

    return v2

    .line 111
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/util/fst/NodeHash;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v3, p0, Lorg/apache/lucene/util/fst/NodeHash;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget-object v4, p0, Lorg/apache/lucene/util/fst/NodeHash;->in:Lorg/apache/lucene/util/fst/FST$BytesReader;

    invoke-virtual {v2, v3, v4}, Lorg/apache/lucene/util/fst/FST;->readNextRealArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    goto :goto_0
.end method

.method private hash(Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;)I
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/Builder$UnCompiledNode",
            "<TT;>;)I"
        }
    .end annotation

    .prologue
    .line 73
    .local p0, "this":Lorg/apache/lucene/util/fst/NodeHash;, "Lorg/apache/lucene/util/fst/NodeHash<TT;>;"
    .local p1, "node":Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;, "Lorg/apache/lucene/util/fst/Builder$UnCompiledNode<TT;>;"
    const/16 v0, 0x1f

    .line 75
    .local v0, "PRIME":I
    const/4 v3, 0x0

    .line 77
    .local v3, "h":I
    const/4 v2, 0x0

    .local v2, "arcIdx":I
    :goto_0
    iget v6, p1, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    if-lt v2, v6, :cond_0

    .line 90
    const v6, 0x7fffffff

    and-int/2addr v6, v3

    return v6

    .line 78
    :cond_0
    iget-object v6, p1, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->arcs:[Lorg/apache/lucene/util/fst/Builder$Arc;

    aget-object v1, v6, v2

    .line 80
    .local v1, "arc":Lorg/apache/lucene/util/fst/Builder$Arc;, "Lorg/apache/lucene/util/fst/Builder$Arc<TT;>;"
    mul-int/lit8 v6, v3, 0x1f

    iget v7, v1, Lorg/apache/lucene/util/fst/Builder$Arc;->label:I

    add-int v3, v6, v7

    .line 81
    iget-object v6, v1, Lorg/apache/lucene/util/fst/Builder$Arc;->target:Lorg/apache/lucene/util/fst/Builder$Node;

    check-cast v6, Lorg/apache/lucene/util/fst/Builder$CompiledNode;

    iget-wide v4, v6, Lorg/apache/lucene/util/fst/Builder$CompiledNode;->node:J

    .line 82
    .local v4, "n":J
    mul-int/lit8 v6, v3, 0x1f

    const/16 v7, 0x20

    shr-long v8, v4, v7

    xor-long/2addr v8, v4

    long-to-int v7, v8

    add-int v3, v6, v7

    .line 83
    mul-int/lit8 v6, v3, 0x1f

    iget-object v7, v1, Lorg/apache/lucene/util/fst/Builder$Arc;->output:Ljava/lang/Object;

    invoke-virtual {v7}, Ljava/lang/Object;->hashCode()I

    move-result v7

    add-int v3, v6, v7

    .line 84
    mul-int/lit8 v6, v3, 0x1f

    iget-object v7, v1, Lorg/apache/lucene/util/fst/Builder$Arc;->nextFinalOutput:Ljava/lang/Object;

    invoke-virtual {v7}, Ljava/lang/Object;->hashCode()I

    move-result v7

    add-int v3, v6, v7

    .line 85
    iget-boolean v6, v1, Lorg/apache/lucene/util/fst/Builder$Arc;->isFinal:Z

    if-eqz v6, :cond_1

    .line 86
    add-int/lit8 v3, v3, 0x11

    .line 77
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private nodesEqual(Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;J)Z
    .locals 8
    .param p2, "address"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/Builder$UnCompiledNode",
            "<TT;>;J)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/util/fst/NodeHash;, "Lorg/apache/lucene/util/fst/NodeHash<TT;>;"
    .local p1, "node":Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;, "Lorg/apache/lucene/util/fst/Builder$UnCompiledNode<TT;>;"
    const/4 v3, 0x0

    .line 43
    iget-object v2, p0, Lorg/apache/lucene/util/fst/NodeHash;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v4, p0, Lorg/apache/lucene/util/fst/NodeHash;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget-object v5, p0, Lorg/apache/lucene/util/fst/NodeHash;->in:Lorg/apache/lucene/util/fst/FST$BytesReader;

    invoke-virtual {v2, p2, p3, v4, v5}, Lorg/apache/lucene/util/fst/FST;->readFirstRealTargetArc(JLorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 44
    iget-object v2, p0, Lorg/apache/lucene/util/fst/NodeHash;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget v2, v2, Lorg/apache/lucene/util/fst/FST$Arc;->bytesPerArc:I

    if-eqz v2, :cond_0

    iget v2, p1, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    iget-object v4, p0, Lorg/apache/lucene/util/fst/NodeHash;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget v4, v4, Lorg/apache/lucene/util/fst/FST$Arc;->numArcs:I

    if-eq v2, v4, :cond_0

    move v2, v3

    .line 67
    :goto_0
    return v2

    .line 47
    :cond_0
    const/4 v1, 0x0

    .local v1, "arcUpto":I
    :goto_1
    iget v2, p1, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    if-lt v1, v2, :cond_1

    move v2, v3

    .line 67
    goto :goto_0

    .line 48
    :cond_1
    iget-object v2, p1, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->arcs:[Lorg/apache/lucene/util/fst/Builder$Arc;

    aget-object v0, v2, v1

    .line 49
    .local v0, "arc":Lorg/apache/lucene/util/fst/Builder$Arc;, "Lorg/apache/lucene/util/fst/Builder$Arc<TT;>;"
    iget v2, v0, Lorg/apache/lucene/util/fst/Builder$Arc;->label:I

    iget-object v4, p0, Lorg/apache/lucene/util/fst/NodeHash;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget v4, v4, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    if-ne v2, v4, :cond_2

    .line 50
    iget-object v2, v0, Lorg/apache/lucene/util/fst/Builder$Arc;->output:Ljava/lang/Object;

    iget-object v4, p0, Lorg/apache/lucene/util/fst/NodeHash;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget-object v4, v4, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    invoke-virtual {v2, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 51
    iget-object v2, v0, Lorg/apache/lucene/util/fst/Builder$Arc;->target:Lorg/apache/lucene/util/fst/Builder$Node;

    check-cast v2, Lorg/apache/lucene/util/fst/Builder$CompiledNode;

    iget-wide v4, v2, Lorg/apache/lucene/util/fst/Builder$CompiledNode;->node:J

    iget-object v2, p0, Lorg/apache/lucene/util/fst/NodeHash;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget-wide v6, v2, Lorg/apache/lucene/util/fst/FST$Arc;->target:J

    cmp-long v2, v4, v6

    if-nez v2, :cond_2

    .line 52
    iget-object v2, v0, Lorg/apache/lucene/util/fst/Builder$Arc;->nextFinalOutput:Ljava/lang/Object;

    iget-object v4, p0, Lorg/apache/lucene/util/fst/NodeHash;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget-object v4, v4, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    invoke-virtual {v2, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 53
    iget-boolean v2, v0, Lorg/apache/lucene/util/fst/Builder$Arc;->isFinal:Z

    iget-object v4, p0, Lorg/apache/lucene/util/fst/NodeHash;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-virtual {v4}, Lorg/apache/lucene/util/fst/FST$Arc;->isFinal()Z

    move-result v4

    if-eq v2, v4, :cond_3

    :cond_2
    move v2, v3

    .line 54
    goto :goto_0

    .line 57
    :cond_3
    iget-object v2, p0, Lorg/apache/lucene/util/fst/NodeHash;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-virtual {v2}, Lorg/apache/lucene/util/fst/FST$Arc;->isLast()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 58
    iget v2, p1, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    add-int/lit8 v2, v2, -0x1

    if-ne v1, v2, :cond_4

    .line 59
    const/4 v2, 0x1

    goto :goto_0

    :cond_4
    move v2, v3

    .line 61
    goto :goto_0

    .line 64
    :cond_5
    iget-object v2, p0, Lorg/apache/lucene/util/fst/NodeHash;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v4, p0, Lorg/apache/lucene/util/fst/NodeHash;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget-object v5, p0, Lorg/apache/lucene/util/fst/NodeHash;->in:Lorg/apache/lucene/util/fst/FST$BytesReader;

    invoke-virtual {v2, v4, v5}, Lorg/apache/lucene/util/fst/FST;->readNextRealArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 47
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private rehash()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 161
    .local p0, "this":Lorg/apache/lucene/util/fst/NodeHash;, "Lorg/apache/lucene/util/fst/NodeHash<TT;>;"
    iget-object v3, p0, Lorg/apache/lucene/util/fst/NodeHash;->table:Lorg/apache/lucene/util/packed/GrowableWriter;

    .line 163
    .local v3, "oldTable":Lorg/apache/lucene/util/packed/GrowableWriter;
    invoke-virtual {v3}, Lorg/apache/lucene/util/packed/GrowableWriter;->size()I

    move-result v4

    const v5, 0x3fffffff    # 1.9999999f

    if-lt v4, v5, :cond_0

    .line 164
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "FST too large (> 2.1 GB)"

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 167
    :cond_0
    new-instance v4, Lorg/apache/lucene/util/packed/GrowableWriter;

    invoke-virtual {v3}, Lorg/apache/lucene/util/packed/GrowableWriter;->getBitsPerValue()I

    move-result v5

    invoke-virtual {v3}, Lorg/apache/lucene/util/packed/GrowableWriter;->size()I

    move-result v6

    mul-int/lit8 v6, v6, 0x2

    const/4 v7, 0x0

    invoke-direct {v4, v5, v6, v7}, Lorg/apache/lucene/util/packed/GrowableWriter;-><init>(IIF)V

    iput-object v4, p0, Lorg/apache/lucene/util/fst/NodeHash;->table:Lorg/apache/lucene/util/packed/GrowableWriter;

    .line 168
    iget-object v4, p0, Lorg/apache/lucene/util/fst/NodeHash;->table:Lorg/apache/lucene/util/packed/GrowableWriter;

    invoke-virtual {v4}, Lorg/apache/lucene/util/packed/GrowableWriter;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lorg/apache/lucene/util/fst/NodeHash;->mask:I

    .line 169
    const/4 v2, 0x0

    .local v2, "idx":I
    :goto_0
    invoke-virtual {v3}, Lorg/apache/lucene/util/packed/GrowableWriter;->size()I

    move-result v4

    if-lt v2, v4, :cond_1

    .line 175
    return-void

    .line 170
    :cond_1
    invoke-virtual {v3, v2}, Lorg/apache/lucene/util/packed/GrowableWriter;->get(I)J

    move-result-wide v0

    .line 171
    .local v0, "address":J
    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-eqz v4, :cond_2

    .line 172
    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/util/fst/NodeHash;->addNew(J)V

    .line 169
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method


# virtual methods
.method public add(Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;)J
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/Builder$UnCompiledNode",
            "<TT;>;)J"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 119
    .local p0, "this":Lorg/apache/lucene/util/fst/NodeHash;, "Lorg/apache/lucene/util/fst/NodeHash<TT;>;"
    .local p1, "nodeIn":Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;, "Lorg/apache/lucene/util/fst/Builder$UnCompiledNode<TT;>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/util/fst/NodeHash;->hash(Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;)I

    move-result v1

    .line 120
    .local v1, "h":I
    iget v5, p0, Lorg/apache/lucene/util/fst/NodeHash;->mask:I

    and-int v4, v1, v5

    .line 121
    .local v4, "pos":I
    const/4 v0, 0x0

    .line 123
    .local v0, "c":I
    :goto_0
    iget-object v5, p0, Lorg/apache/lucene/util/fst/NodeHash;->table:Lorg/apache/lucene/util/packed/GrowableWriter;

    invoke-virtual {v5, v4}, Lorg/apache/lucene/util/packed/GrowableWriter;->get(I)J

    move-result-wide v6

    .line 124
    .local v6, "v":J
    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-nez v5, :cond_2

    .line 126
    iget-object v5, p0, Lorg/apache/lucene/util/fst/NodeHash;->fst:Lorg/apache/lucene/util/fst/FST;

    invoke-virtual {v5, p1}, Lorg/apache/lucene/util/fst/FST;->addNode(Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;)J

    move-result-wide v2

    .line 128
    .local v2, "node":J
    sget-boolean v5, Lorg/apache/lucene/util/fst/NodeHash;->$assertionsDisabled:Z

    if-nez v5, :cond_0

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/util/fst/NodeHash;->hash(J)I

    move-result v5

    if-eq v5, v1, :cond_0

    new-instance v5, Ljava/lang/AssertionError;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "frozenHash="

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/util/fst/NodeHash;->hash(J)I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " vs h="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v8}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v5

    .line 129
    :cond_0
    iget v5, p0, Lorg/apache/lucene/util/fst/NodeHash;->count:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lorg/apache/lucene/util/fst/NodeHash;->count:I

    .line 130
    iget-object v5, p0, Lorg/apache/lucene/util/fst/NodeHash;->table:Lorg/apache/lucene/util/packed/GrowableWriter;

    invoke-virtual {v5, v4, v2, v3}, Lorg/apache/lucene/util/packed/GrowableWriter;->set(IJ)V

    .line 131
    iget-object v5, p0, Lorg/apache/lucene/util/fst/NodeHash;->table:Lorg/apache/lucene/util/packed/GrowableWriter;

    invoke-virtual {v5}, Lorg/apache/lucene/util/packed/GrowableWriter;->size()I

    move-result v5

    iget v8, p0, Lorg/apache/lucene/util/fst/NodeHash;->count:I

    mul-int/lit8 v8, v8, 0x2

    if-ge v5, v8, :cond_1

    .line 132
    invoke-direct {p0}, Lorg/apache/lucene/util/fst/NodeHash;->rehash()V

    .line 137
    .end local v2    # "node":J
    :cond_1
    :goto_1
    return-wide v2

    .line 135
    :cond_2
    invoke-direct {p0, p1, v6, v7}, Lorg/apache/lucene/util/fst/NodeHash;->nodesEqual(Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;J)Z

    move-result v5

    if-eqz v5, :cond_3

    move-wide v2, v6

    .line 137
    goto :goto_1

    .line 141
    :cond_3
    add-int/lit8 v0, v0, 0x1

    add-int v5, v4, v0

    iget v8, p0, Lorg/apache/lucene/util/fst/NodeHash;->mask:I

    and-int v4, v5, v8

    .line 122
    goto :goto_0
.end method

.method public count()I
    .locals 1

    .prologue
    .line 178
    .local p0, "this":Lorg/apache/lucene/util/fst/NodeHash;, "Lorg/apache/lucene/util/fst/NodeHash<TT;>;"
    iget v0, p0, Lorg/apache/lucene/util/fst/NodeHash;->count:I

    return v0
.end method
