.class public abstract Lorg/apache/lucene/util/fst/FST$BytesReader;
.super Lorg/apache/lucene/store/DataInput;
.source "FST.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/fst/FST;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "BytesReader"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1272
    invoke-direct {p0}, Lorg/apache/lucene/store/DataInput;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getPosition()J
.end method

.method public abstract reversed()Z
.end method

.method public abstract setPosition(J)V
.end method

.method public abstract skipBytes(I)V
.end method
