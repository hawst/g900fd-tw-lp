.class public final Lorg/apache/lucene/util/CollectionUtil;
.super Ljava/lang/Object;
.source "CollectionUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/CollectionUtil$ListMergeSorterTemplate;,
        Lorg/apache/lucene/util/CollectionUtil$ListSorterTemplate;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static binarySort(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/lang/Comparable",
            "<-TT;>;>(",
            "Ljava/util/List",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 307
    .local p0, "list":Ljava/util/List;, "Ljava/util/List<TT;>;"
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    .line 308
    .local v0, "size":I
    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    .line 310
    :goto_0
    return-void

    .line 309
    :cond_0
    invoke-static {p0}, Lorg/apache/lucene/util/CollectionUtil;->getSorter(Ljava/util/List;)Lorg/apache/lucene/util/SorterTemplate;

    move-result-object v1

    const/4 v2, 0x0

    add-int/lit8 v3, v0, -0x1

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/util/SorterTemplate;->binarySort(II)V

    goto :goto_0
.end method

.method public static binarySort(Ljava/util/List;Ljava/util/Comparator;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<TT;>;",
            "Ljava/util/Comparator",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 295
    .local p0, "list":Ljava/util/List;, "Ljava/util/List<TT;>;"
    .local p1, "comp":Ljava/util/Comparator;, "Ljava/util/Comparator<-TT;>;"
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    .line 296
    .local v0, "size":I
    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    .line 298
    :goto_0
    return-void

    .line 297
    :cond_0
    invoke-static {p0, p1}, Lorg/apache/lucene/util/CollectionUtil;->getSorter(Ljava/util/List;Ljava/util/Comparator;)Lorg/apache/lucene/util/SorterTemplate;

    move-result-object v1

    const/4 v2, 0x0

    add-int/lit8 v3, v0, -0x1

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/util/SorterTemplate;->binarySort(II)V

    goto :goto_0
.end method

.method private static getMergeSorter(Ljava/util/List;)Lorg/apache/lucene/util/SorterTemplate;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/lang/Comparable",
            "<-TT;>;>(",
            "Ljava/util/List",
            "<TT;>;)",
            "Lorg/apache/lucene/util/SorterTemplate;"
        }
    .end annotation

    .prologue
    .line 168
    .local p0, "list":Ljava/util/List;, "Ljava/util/List<TT;>;"
    instance-of v0, p0, Ljava/util/RandomAccess;

    if-nez v0, :cond_0

    .line 169
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "CollectionUtil can only sort random access lists in-place."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 170
    :cond_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    const/16 v1, 0x5dc

    if-ge v0, v1, :cond_1

    .line 171
    invoke-static {p0}, Lorg/apache/lucene/util/CollectionUtil;->getSorter(Ljava/util/List;)Lorg/apache/lucene/util/SorterTemplate;

    move-result-object v0

    .line 173
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lorg/apache/lucene/util/CollectionUtil$4;

    const v1, 0x3c23d70a    # 0.01f

    invoke-direct {v0, p0, v1}, Lorg/apache/lucene/util/CollectionUtil$4;-><init>(Ljava/util/List;F)V

    goto :goto_0
.end method

.method private static getMergeSorter(Ljava/util/List;Ljava/util/Comparator;)Lorg/apache/lucene/util/SorterTemplate;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<TT;>;",
            "Ljava/util/Comparator",
            "<-TT;>;)",
            "Lorg/apache/lucene/util/SorterTemplate;"
        }
    .end annotation

    .prologue
    .line 150
    .local p0, "list":Ljava/util/List;, "Ljava/util/List<TT;>;"
    .local p1, "comp":Ljava/util/Comparator;, "Ljava/util/Comparator<-TT;>;"
    instance-of v0, p0, Ljava/util/RandomAccess;

    if-nez v0, :cond_0

    .line 151
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "CollectionUtil can only sort random access lists in-place."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 152
    :cond_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    const/16 v1, 0x5dc

    if-ge v0, v1, :cond_1

    .line 153
    invoke-static {p0, p1}, Lorg/apache/lucene/util/CollectionUtil;->getSorter(Ljava/util/List;Ljava/util/Comparator;)Lorg/apache/lucene/util/SorterTemplate;

    move-result-object v0

    .line 155
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lorg/apache/lucene/util/CollectionUtil$3;

    const v1, 0x3c23d70a    # 0.01f

    invoke-direct {v0, p0, v1, p1}, Lorg/apache/lucene/util/CollectionUtil$3;-><init>(Ljava/util/List;FLjava/util/Comparator;)V

    goto :goto_0
.end method

.method private static getSorter(Ljava/util/List;)Lorg/apache/lucene/util/SorterTemplate;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/lang/Comparable",
            "<-TT;>;>(",
            "Ljava/util/List",
            "<TT;>;)",
            "Lorg/apache/lucene/util/SorterTemplate;"
        }
    .end annotation

    .prologue
    .line 136
    .local p0, "list":Ljava/util/List;, "Ljava/util/List<TT;>;"
    instance-of v0, p0, Ljava/util/RandomAccess;

    if-nez v0, :cond_0

    .line 137
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "CollectionUtil can only sort random access lists in-place."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 138
    :cond_0
    new-instance v0, Lorg/apache/lucene/util/CollectionUtil$2;

    invoke-direct {v0, p0}, Lorg/apache/lucene/util/CollectionUtil$2;-><init>(Ljava/util/List;)V

    return-object v0
.end method

.method private static getSorter(Ljava/util/List;Ljava/util/Comparator;)Lorg/apache/lucene/util/SorterTemplate;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<TT;>;",
            "Ljava/util/Comparator",
            "<-TT;>;)",
            "Lorg/apache/lucene/util/SorterTemplate;"
        }
    .end annotation

    .prologue
    .line 122
    .local p0, "list":Ljava/util/List;, "Ljava/util/List<TT;>;"
    .local p1, "comp":Ljava/util/Comparator;, "Ljava/util/Comparator<-TT;>;"
    instance-of v0, p0, Ljava/util/RandomAccess;

    if-nez v0, :cond_0

    .line 123
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "CollectionUtil can only sort random access lists in-place."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 124
    :cond_0
    new-instance v0, Lorg/apache/lucene/util/CollectionUtil$1;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/util/CollectionUtil$1;-><init>(Ljava/util/List;Ljava/util/Comparator;)V

    return-object v0
.end method

.method public static insertionSort(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/lang/Comparable",
            "<-TT;>;>(",
            "Ljava/util/List",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 281
    .local p0, "list":Ljava/util/List;, "Ljava/util/List<TT;>;"
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    .line 282
    .local v0, "size":I
    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    .line 284
    :goto_0
    return-void

    .line 283
    :cond_0
    invoke-static {p0}, Lorg/apache/lucene/util/CollectionUtil;->getSorter(Ljava/util/List;)Lorg/apache/lucene/util/SorterTemplate;

    move-result-object v1

    const/4 v2, 0x0

    add-int/lit8 v3, v0, -0x1

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/util/SorterTemplate;->insertionSort(II)V

    goto :goto_0
.end method

.method public static insertionSort(Ljava/util/List;Ljava/util/Comparator;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<TT;>;",
            "Ljava/util/Comparator",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 269
    .local p0, "list":Ljava/util/List;, "Ljava/util/List<TT;>;"
    .local p1, "comp":Ljava/util/Comparator;, "Ljava/util/Comparator<-TT;>;"
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    .line 270
    .local v0, "size":I
    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    .line 272
    :goto_0
    return-void

    .line 271
    :cond_0
    invoke-static {p0, p1}, Lorg/apache/lucene/util/CollectionUtil;->getSorter(Ljava/util/List;Ljava/util/Comparator;)Lorg/apache/lucene/util/SorterTemplate;

    move-result-object v1

    const/4 v2, 0x0

    add-int/lit8 v3, v0, -0x1

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/util/SorterTemplate;->insertionSort(II)V

    goto :goto_0
.end method

.method public static mergeSort(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/lang/Comparable",
            "<-TT;>;>(",
            "Ljava/util/List",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 229
    .local p0, "list":Ljava/util/List;, "Ljava/util/List<TT;>;"
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    .line 230
    .local v0, "size":I
    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    .line 232
    :goto_0
    return-void

    .line 231
    :cond_0
    invoke-static {p0}, Lorg/apache/lucene/util/CollectionUtil;->getMergeSorter(Ljava/util/List;)Lorg/apache/lucene/util/SorterTemplate;

    move-result-object v1

    const/4 v2, 0x0

    add-int/lit8 v3, v0, -0x1

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/util/SorterTemplate;->mergeSort(II)V

    goto :goto_0
.end method

.method public static mergeSort(Ljava/util/List;Ljava/util/Comparator;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<TT;>;",
            "Ljava/util/Comparator",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 217
    .local p0, "list":Ljava/util/List;, "Ljava/util/List<TT;>;"
    .local p1, "comp":Ljava/util/Comparator;, "Ljava/util/Comparator<-TT;>;"
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    .line 218
    .local v0, "size":I
    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    .line 220
    :goto_0
    return-void

    .line 219
    :cond_0
    invoke-static {p0, p1}, Lorg/apache/lucene/util/CollectionUtil;->getMergeSorter(Ljava/util/List;Ljava/util/Comparator;)Lorg/apache/lucene/util/SorterTemplate;

    move-result-object v1

    const/4 v2, 0x0

    add-int/lit8 v3, v0, -0x1

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/util/SorterTemplate;->mergeSort(II)V

    goto :goto_0
.end method

.method public static quickSort(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/lang/Comparable",
            "<-TT;>;>(",
            "Ljava/util/List",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 203
    .local p0, "list":Ljava/util/List;, "Ljava/util/List<TT;>;"
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    .line 204
    .local v0, "size":I
    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    .line 206
    :goto_0
    return-void

    .line 205
    :cond_0
    invoke-static {p0}, Lorg/apache/lucene/util/CollectionUtil;->getSorter(Ljava/util/List;)Lorg/apache/lucene/util/SorterTemplate;

    move-result-object v1

    const/4 v2, 0x0

    add-int/lit8 v3, v0, -0x1

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/util/SorterTemplate;->quickSort(II)V

    goto :goto_0
.end method

.method public static quickSort(Ljava/util/List;Ljava/util/Comparator;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<TT;>;",
            "Ljava/util/Comparator",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 191
    .local p0, "list":Ljava/util/List;, "Ljava/util/List<TT;>;"
    .local p1, "comp":Ljava/util/Comparator;, "Ljava/util/Comparator<-TT;>;"
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    .line 192
    .local v0, "size":I
    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    .line 194
    :goto_0
    return-void

    .line 193
    :cond_0
    invoke-static {p0, p1}, Lorg/apache/lucene/util/CollectionUtil;->getSorter(Ljava/util/List;Ljava/util/Comparator;)Lorg/apache/lucene/util/SorterTemplate;

    move-result-object v1

    const/4 v2, 0x0

    add-int/lit8 v3, v0, -0x1

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/util/SorterTemplate;->quickSort(II)V

    goto :goto_0
.end method

.method public static timSort(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/lang/Comparable",
            "<-TT;>;>(",
            "Ljava/util/List",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 255
    .local p0, "list":Ljava/util/List;, "Ljava/util/List<TT;>;"
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    .line 256
    .local v0, "size":I
    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    .line 258
    :goto_0
    return-void

    .line 257
    :cond_0
    invoke-static {p0}, Lorg/apache/lucene/util/CollectionUtil;->getMergeSorter(Ljava/util/List;)Lorg/apache/lucene/util/SorterTemplate;

    move-result-object v1

    const/4 v2, 0x0

    add-int/lit8 v3, v0, -0x1

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/util/SorterTemplate;->timSort(II)V

    goto :goto_0
.end method

.method public static timSort(Ljava/util/List;Ljava/util/Comparator;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<TT;>;",
            "Ljava/util/Comparator",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 243
    .local p0, "list":Ljava/util/List;, "Ljava/util/List<TT;>;"
    .local p1, "comp":Ljava/util/Comparator;, "Ljava/util/Comparator<-TT;>;"
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    .line 244
    .local v0, "size":I
    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    .line 246
    :goto_0
    return-void

    .line 245
    :cond_0
    invoke-static {p0, p1}, Lorg/apache/lucene/util/CollectionUtil;->getMergeSorter(Ljava/util/List;Ljava/util/Comparator;)Lorg/apache/lucene/util/SorterTemplate;

    move-result-object v1

    const/4 v2, 0x0

    add-int/lit8 v3, v0, -0x1

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/util/SorterTemplate;->timSort(II)V

    goto :goto_0
.end method
