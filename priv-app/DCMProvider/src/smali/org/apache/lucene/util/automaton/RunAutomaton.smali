.class public abstract Lorg/apache/lucene/util/automaton/RunAutomaton;
.super Ljava/lang/Object;
.source "RunAutomaton.java"


# instance fields
.field final accept:[Z

.field final classmap:[I

.field final initial:I

.field final maxInterval:I

.field final points:[I

.field final size:I

.field final transitions:[I


# direct methods
.method public constructor <init>(Lorg/apache/lucene/util/automaton/Automaton;IZ)V
    .locals 12
    .param p1, "a"    # Lorg/apache/lucene/util/automaton/Automaton;
    .param p2, "maxInterval"    # I
    .param p3, "tableize"    # Z

    .prologue
    .line 120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 121
    iput p2, p0, Lorg/apache/lucene/util/automaton/RunAutomaton;->maxInterval:I

    .line 122
    invoke-virtual {p1}, Lorg/apache/lucene/util/automaton/Automaton;->determinize()V

    .line 123
    invoke-virtual {p1}, Lorg/apache/lucene/util/automaton/Automaton;->getStartPoints()[I

    move-result-object v7

    iput-object v7, p0, Lorg/apache/lucene/util/automaton/RunAutomaton;->points:[I

    .line 124
    invoke-virtual {p1}, Lorg/apache/lucene/util/automaton/Automaton;->getNumberedStates()[Lorg/apache/lucene/util/automaton/State;

    move-result-object v6

    .line 125
    .local v6, "states":[Lorg/apache/lucene/util/automaton/State;
    iget-object v7, p1, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    iget v7, v7, Lorg/apache/lucene/util/automaton/State;->number:I

    iput v7, p0, Lorg/apache/lucene/util/automaton/RunAutomaton;->initial:I

    .line 126
    array-length v7, v6

    iput v7, p0, Lorg/apache/lucene/util/automaton/RunAutomaton;->size:I

    .line 127
    iget v7, p0, Lorg/apache/lucene/util/automaton/RunAutomaton;->size:I

    new-array v7, v7, [Z

    iput-object v7, p0, Lorg/apache/lucene/util/automaton/RunAutomaton;->accept:[Z

    .line 128
    iget v7, p0, Lorg/apache/lucene/util/automaton/RunAutomaton;->size:I

    iget-object v8, p0, Lorg/apache/lucene/util/automaton/RunAutomaton;->points:[I

    array-length v8, v8

    mul-int/2addr v7, v8

    new-array v7, v7, [I

    iput-object v7, p0, Lorg/apache/lucene/util/automaton/RunAutomaton;->transitions:[I

    .line 129
    const/4 v3, 0x0

    .local v3, "n":I
    :goto_0
    iget v7, p0, Lorg/apache/lucene/util/automaton/RunAutomaton;->size:I

    iget-object v8, p0, Lorg/apache/lucene/util/automaton/RunAutomaton;->points:[I

    array-length v8, v8

    mul-int/2addr v7, v8

    if-lt v3, v7, :cond_0

    .line 131
    array-length v8, v6

    const/4 v7, 0x0

    :goto_1
    if-lt v7, v8, :cond_1

    .line 142
    if-eqz p3, :cond_6

    .line 143
    add-int/lit8 v7, p2, 0x1

    new-array v7, v7, [I

    iput-object v7, p0, Lorg/apache/lucene/util/automaton/RunAutomaton;->classmap:[I

    .line 144
    const/4 v1, 0x0

    .line 145
    .local v1, "i":I
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_2
    if-le v2, p2, :cond_4

    .line 153
    .end local v1    # "i":I
    .end local v2    # "j":I
    :goto_3
    return-void

    .line 130
    :cond_0
    iget-object v7, p0, Lorg/apache/lucene/util/automaton/RunAutomaton;->transitions:[I

    const/4 v8, -0x1

    aput v8, v7, v3

    .line 129
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 131
    :cond_1
    aget-object v5, v6, v7

    .line 132
    .local v5, "s":Lorg/apache/lucene/util/automaton/State;
    iget v3, v5, Lorg/apache/lucene/util/automaton/State;->number:I

    .line 133
    iget-object v9, p0, Lorg/apache/lucene/util/automaton/RunAutomaton;->accept:[Z

    iget-boolean v10, v5, Lorg/apache/lucene/util/automaton/State;->accept:Z

    aput-boolean v10, v9, v3

    .line 134
    const/4 v0, 0x0

    .local v0, "c":I
    :goto_4
    iget-object v9, p0, Lorg/apache/lucene/util/automaton/RunAutomaton;->points:[I

    array-length v9, v9

    if-lt v0, v9, :cond_2

    .line 131
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 135
    :cond_2
    iget-object v9, p0, Lorg/apache/lucene/util/automaton/RunAutomaton;->points:[I

    aget v9, v9, v0

    invoke-virtual {v5, v9}, Lorg/apache/lucene/util/automaton/State;->step(I)Lorg/apache/lucene/util/automaton/State;

    move-result-object v4

    .line 136
    .local v4, "q":Lorg/apache/lucene/util/automaton/State;
    if-eqz v4, :cond_3

    iget-object v9, p0, Lorg/apache/lucene/util/automaton/RunAutomaton;->transitions:[I

    iget-object v10, p0, Lorg/apache/lucene/util/automaton/RunAutomaton;->points:[I

    array-length v10, v10

    mul-int/2addr v10, v3

    add-int/2addr v10, v0

    iget v11, v4, Lorg/apache/lucene/util/automaton/State;->number:I

    aput v11, v9, v10

    .line 134
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 146
    .end local v0    # "c":I
    .end local v4    # "q":Lorg/apache/lucene/util/automaton/State;
    .end local v5    # "s":Lorg/apache/lucene/util/automaton/State;
    .restart local v1    # "i":I
    .restart local v2    # "j":I
    :cond_4
    add-int/lit8 v7, v1, 0x1

    iget-object v8, p0, Lorg/apache/lucene/util/automaton/RunAutomaton;->points:[I

    array-length v8, v8

    if-ge v7, v8, :cond_5

    iget-object v7, p0, Lorg/apache/lucene/util/automaton/RunAutomaton;->points:[I

    add-int/lit8 v8, v1, 0x1

    aget v7, v7, v8

    if-ne v2, v7, :cond_5

    .line 147
    add-int/lit8 v1, v1, 0x1

    .line 148
    :cond_5
    iget-object v7, p0, Lorg/apache/lucene/util/automaton/RunAutomaton;->classmap:[I

    aput v1, v7, v2

    .line 145
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 151
    .end local v1    # "i":I
    .end local v2    # "j":I
    :cond_6
    const/4 v7, 0x0

    iput-object v7, p0, Lorg/apache/lucene/util/automaton/RunAutomaton;->classmap:[I

    goto :goto_3
.end method


# virtual methods
.method final getCharClass(I)I
    .locals 1
    .param p1, "c"    # I

    .prologue
    .line 111
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/RunAutomaton;->points:[I

    invoke-static {p1, v0}, Lorg/apache/lucene/util/automaton/SpecialOperations;->findIndex(I[I)I

    move-result v0

    return v0
.end method

.method public final getCharIntervals()[I
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/RunAutomaton;->points:[I

    invoke-virtual {v0}, [I->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    return-object v0
.end method

.method public final getInitialState()I
    .locals 1

    .prologue
    .line 96
    iget v0, p0, Lorg/apache/lucene/util/automaton/RunAutomaton;->initial:I

    return v0
.end method

.method public final getSize()I
    .locals 1

    .prologue
    .line 82
    iget v0, p0, Lorg/apache/lucene/util/automaton/RunAutomaton;->size:I

    return v0
.end method

.method public final isAccept(I)Z
    .locals 1
    .param p1, "state"    # I

    .prologue
    .line 89
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/RunAutomaton;->accept:[Z

    aget-boolean v0, v0, p1

    return v0
.end method

.method public final step(II)I
    .locals 3
    .param p1, "state"    # I
    .param p2, "c"    # I

    .prologue
    .line 163
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/RunAutomaton;->classmap:[I

    if-nez v0, :cond_0

    .line 164
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/RunAutomaton;->transitions:[I

    iget-object v1, p0, Lorg/apache/lucene/util/automaton/RunAutomaton;->points:[I

    array-length v1, v1

    mul-int/2addr v1, p1

    invoke-virtual {p0, p2}, Lorg/apache/lucene/util/automaton/RunAutomaton;->getCharClass(I)I

    move-result v2

    add-int/2addr v1, v2

    aget v0, v0, v1

    .line 166
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/RunAutomaton;->transitions:[I

    iget-object v1, p0, Lorg/apache/lucene/util/automaton/RunAutomaton;->points:[I

    array-length v1, v1

    mul-int/2addr v1, p1

    iget-object v2, p0, Lorg/apache/lucene/util/automaton/RunAutomaton;->classmap:[I

    aget v2, v2, p2

    add-int/2addr v1, v2

    aget v0, v0, v1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 8

    .prologue
    .line 52
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 53
    .local v0, "b":Ljava/lang/StringBuilder;
    const-string v6, "initial state: "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lorg/apache/lucene/util/automaton/RunAutomaton;->initial:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 54
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v6, p0, Lorg/apache/lucene/util/automaton/RunAutomaton;->size:I

    if-lt v1, v6, :cond_0

    .line 75
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6

    .line 55
    :cond_0
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "state "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 56
    iget-object v6, p0, Lorg/apache/lucene/util/automaton/RunAutomaton;->accept:[Z

    aget-boolean v6, v6, v1

    if-eqz v6, :cond_1

    const-string v6, " [accept]:\n"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 58
    :goto_1
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_2
    iget-object v6, p0, Lorg/apache/lucene/util/automaton/RunAutomaton;->points:[I

    array-length v6, v6

    if-lt v2, v6, :cond_2

    .line 54
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 57
    .end local v2    # "j":I
    :cond_1
    const-string v6, " [reject]:\n"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 59
    .restart local v2    # "j":I
    :cond_2
    iget-object v6, p0, Lorg/apache/lucene/util/automaton/RunAutomaton;->transitions:[I

    iget-object v7, p0, Lorg/apache/lucene/util/automaton/RunAutomaton;->points:[I

    array-length v7, v7

    mul-int/2addr v7, v1

    add-int/2addr v7, v2

    aget v3, v6, v7

    .line 60
    .local v3, "k":I
    const/4 v6, -0x1

    if-eq v3, v6, :cond_4

    .line 61
    iget-object v6, p0, Lorg/apache/lucene/util/automaton/RunAutomaton;->points:[I

    aget v5, v6, v2

    .line 63
    .local v5, "min":I
    add-int/lit8 v6, v2, 0x1

    iget-object v7, p0, Lorg/apache/lucene/util/automaton/RunAutomaton;->points:[I

    array-length v7, v7

    if-ge v6, v7, :cond_5

    iget-object v6, p0, Lorg/apache/lucene/util/automaton/RunAutomaton;->points:[I

    add-int/lit8 v7, v2, 0x1

    aget v6, v6, v7

    add-int/lit8 v4, v6, -0x1

    .line 65
    .local v4, "max":I
    :goto_3
    const-string v6, " "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 66
    invoke-static {v5, v0}, Lorg/apache/lucene/util/automaton/Transition;->appendCharString(ILjava/lang/StringBuilder;)V

    .line 67
    if-eq v5, v4, :cond_3

    .line 68
    const-string v6, "-"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 69
    invoke-static {v4, v0}, Lorg/apache/lucene/util/automaton/Transition;->appendCharString(ILjava/lang/StringBuilder;)V

    .line 71
    :cond_3
    const-string v6, " -> "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 58
    .end local v4    # "max":I
    .end local v5    # "min":I
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 64
    .restart local v5    # "min":I
    :cond_5
    iget v4, p0, Lorg/apache/lucene/util/automaton/RunAutomaton;->maxInterval:I

    .restart local v4    # "max":I
    goto :goto_3
.end method
