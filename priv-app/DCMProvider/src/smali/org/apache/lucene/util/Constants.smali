.class public final Lorg/apache/lucene/util/Constants;
.super Ljava/lang/Object;
.source "Constants.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final JAVA_VENDOR:Ljava/lang/String;

.field public static final JAVA_VERSION:Ljava/lang/String;

.field public static final JRE_IS_64BIT:Z

.field public static final JRE_IS_MINIMUM_JAVA6:Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final JRE_IS_MINIMUM_JAVA7:Z

.field public static final JRE_IS_MINIMUM_JAVA8:Z

.field public static final JVM_NAME:Ljava/lang/String;

.field public static final JVM_VENDOR:Ljava/lang/String;

.field public static final JVM_VERSION:Ljava/lang/String;

.field public static final LINUX:Z

.field public static final LUCENE_MAIN_VERSION:Ljava/lang/String;

.field public static final LUCENE_VERSION:Ljava/lang/String;

.field public static final MAC_OS_X:Z

.field public static final OS_ARCH:Ljava/lang/String;

.field public static final OS_NAME:Ljava/lang/String;

.field public static final OS_VERSION:Ljava/lang/String;

.field public static final SUN_OS:Z

.field public static final WINDOWS:Z


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .prologue
    .line 28
    const-class v13, Lorg/apache/lucene/util/Constants;

    invoke-virtual {v13}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v13

    if-nez v13, :cond_0

    const/4 v13, 0x1

    :goto_0
    sput-boolean v13, Lorg/apache/lucene/util/Constants;->$assertionsDisabled:Z

    .line 32
    const-string v13, "java.vm.vendor"

    invoke-static {v13}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    sput-object v13, Lorg/apache/lucene/util/Constants;->JVM_VENDOR:Ljava/lang/String;

    .line 33
    const-string v13, "java.vm.version"

    invoke-static {v13}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    sput-object v13, Lorg/apache/lucene/util/Constants;->JVM_VERSION:Ljava/lang/String;

    .line 34
    const-string v13, "java.vm.name"

    invoke-static {v13}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    sput-object v13, Lorg/apache/lucene/util/Constants;->JVM_NAME:Ljava/lang/String;

    .line 37
    const-string v13, "java.version"

    invoke-static {v13}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    sput-object v13, Lorg/apache/lucene/util/Constants;->JAVA_VERSION:Ljava/lang/String;

    .line 40
    const-string v13, "os.name"

    invoke-static {v13}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    sput-object v13, Lorg/apache/lucene/util/Constants;->OS_NAME:Ljava/lang/String;

    .line 42
    sget-object v13, Lorg/apache/lucene/util/Constants;->OS_NAME:Ljava/lang/String;

    const-string v14, "Linux"

    invoke-virtual {v13, v14}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v13

    sput-boolean v13, Lorg/apache/lucene/util/Constants;->LINUX:Z

    .line 44
    sget-object v13, Lorg/apache/lucene/util/Constants;->OS_NAME:Ljava/lang/String;

    const-string v14, "Windows"

    invoke-virtual {v13, v14}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v13

    sput-boolean v13, Lorg/apache/lucene/util/Constants;->WINDOWS:Z

    .line 46
    sget-object v13, Lorg/apache/lucene/util/Constants;->OS_NAME:Ljava/lang/String;

    const-string v14, "SunOS"

    invoke-virtual {v13, v14}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v13

    sput-boolean v13, Lorg/apache/lucene/util/Constants;->SUN_OS:Z

    .line 48
    sget-object v13, Lorg/apache/lucene/util/Constants;->OS_NAME:Ljava/lang/String;

    const-string v14, "Mac OS X"

    invoke-virtual {v13, v14}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v13

    sput-boolean v13, Lorg/apache/lucene/util/Constants;->MAC_OS_X:Z

    .line 50
    const-string v13, "os.arch"

    invoke-static {v13}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    sput-object v13, Lorg/apache/lucene/util/Constants;->OS_ARCH:Ljava/lang/String;

    .line 51
    const-string v13, "os.version"

    invoke-static {v13}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    sput-object v13, Lorg/apache/lucene/util/Constants;->OS_VERSION:Ljava/lang/String;

    .line 52
    const-string v13, "java.vendor"

    invoke-static {v13}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    sput-object v13, Lorg/apache/lucene/util/Constants;->JAVA_VENDOR:Ljava/lang/String;

    .line 57
    new-instance v13, Ljava/lang/Boolean;

    const/4 v14, 0x1

    invoke-direct {v13, v14}, Ljava/lang/Boolean;-><init>(Z)V

    invoke-virtual {v13}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v13

    .line 56
    sput-boolean v13, Lorg/apache/lucene/util/Constants;->JRE_IS_MINIMUM_JAVA6:Z

    .line 66
    const/4 v2, 0x0

    .line 68
    .local v2, "is64Bit":Z
    :try_start_0
    const-string v13, "sun.misc.Unsafe"

    invoke-static {v13}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v7

    .line 69
    .local v7, "unsafeClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v13, "theUnsafe"

    invoke-virtual {v7, v13}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v8

    .line 70
    .local v8, "unsafeField":Ljava/lang/reflect/Field;
    const/4 v13, 0x1

    invoke-virtual {v8, v13}, Ljava/lang/reflect/AccessibleObject;->setAccessible(Z)V

    .line 71
    const/4 v13, 0x0

    invoke-virtual {v8, v13}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .line 72
    .local v6, "unsafe":Ljava/lang/Object;
    const-string v13, "addressSize"

    const/4 v14, 0x0

    new-array v14, v14, [Ljava/lang/Class;

    invoke-virtual {v7, v13, v14}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v13

    .line 73
    const/4 v14, 0x0

    new-array v14, v14, [Ljava/lang/Object;

    invoke-virtual {v13, v6, v14}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    .line 72
    check-cast v13, Ljava/lang/Number;

    .line 73
    invoke-virtual {v13}, Ljava/lang/Number;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 75
    .local v0, "addressSize":I
    const/16 v13, 0x8

    if-lt v0, v13, :cond_1

    const/4 v2, 0x1

    .line 88
    .end local v0    # "addressSize":I
    .end local v6    # "unsafe":Ljava/lang/Object;
    .end local v7    # "unsafeClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v8    # "unsafeField":Ljava/lang/reflect/Field;
    :goto_1
    sput-boolean v2, Lorg/apache/lucene/util/Constants;->JRE_IS_64BIT:Z

    .line 91
    const/4 v10, 0x1

    .line 93
    .local v10, "v7":Z
    :try_start_1
    const-class v13, Ljava/lang/Throwable;

    const-string v14, "getSuppressed"

    const/4 v15, 0x0

    new-array v15, v15, [Ljava/lang/Class;

    invoke-virtual {v13, v14, v15}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_1

    .line 97
    :goto_2
    sput-boolean v10, Lorg/apache/lucene/util/Constants;->JRE_IS_MINIMUM_JAVA7:Z

    .line 99
    sget-boolean v13, Lorg/apache/lucene/util/Constants;->JRE_IS_MINIMUM_JAVA7:Z

    if-eqz v13, :cond_5

    .line 101
    const/4 v11, 0x1

    .line 103
    .local v11, "v8":Z
    :try_start_2
    const-class v13, Ljava/util/Collections;

    const-string v14, "emptySortedSet"

    const/4 v15, 0x0

    new-array v15, v15, [Ljava/lang/Class;

    invoke-virtual {v13, v14, v15}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_2 .. :try_end_2} :catch_2

    .line 107
    :goto_3
    sput-boolean v11, Lorg/apache/lucene/util/Constants;->JRE_IS_MINIMUM_JAVA8:Z

    .line 125
    .end local v11    # "v8":Z
    :goto_4
    const-string v13, "4.3"

    invoke-static {v13}, Lorg/apache/lucene/util/Constants;->ident(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    sput-object v13, Lorg/apache/lucene/util/Constants;->LUCENE_MAIN_VERSION:Ljava/lang/String;

    .line 132
    invoke-static {}, Lorg/apache/lucene/LucenePackage;->get()Ljava/lang/Package;

    move-result-object v5

    .line 133
    .local v5, "pkg":Ljava/lang/Package;
    if-nez v5, :cond_6

    const/4 v9, 0x0

    .line 134
    .local v9, "v":Ljava/lang/String;
    :goto_5
    if-nez v9, :cond_8

    .line 135
    sget-object v13, Lorg/apache/lucene/util/Constants;->LUCENE_MAIN_VERSION:Ljava/lang/String;

    const-string v14, "\\."

    invoke-virtual {v13, v14}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 136
    .local v4, "parts":[Ljava/lang/String;
    array-length v13, v4

    const/4 v14, 0x4

    if-ne v13, v14, :cond_9

    .line 138
    sget-boolean v13, Lorg/apache/lucene/util/Constants;->$assertionsDisabled:Z

    if-nez v13, :cond_7

    const/4 v13, 0x2

    aget-object v13, v4, v13

    const-string v14, "0"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_7

    new-instance v13, Ljava/lang/AssertionError;

    invoke-direct {v13}, Ljava/lang/AssertionError;-><init>()V

    throw v13

    .line 28
    .end local v2    # "is64Bit":Z
    .end local v4    # "parts":[Ljava/lang/String;
    .end local v5    # "pkg":Ljava/lang/Package;
    .end local v9    # "v":Ljava/lang/String;
    .end local v10    # "v7":Z
    :cond_0
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 75
    .restart local v0    # "addressSize":I
    .restart local v2    # "is64Bit":Z
    .restart local v6    # "unsafe":Ljava/lang/Object;
    .restart local v7    # "unsafeClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .restart local v8    # "unsafeField":Ljava/lang/reflect/Field;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 76
    .end local v0    # "addressSize":I
    .end local v6    # "unsafe":Ljava/lang/Object;
    .end local v7    # "unsafeClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v8    # "unsafeField":Ljava/lang/reflect/Field;
    :catch_0
    move-exception v1

    .line 77
    .local v1, "e":Ljava/lang/Exception;
    const-string v13, "sun.arch.data.model"

    invoke-static {v13}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 78
    .local v12, "x":Ljava/lang/String;
    if-eqz v12, :cond_3

    .line 79
    const-string v13, "64"

    invoke-virtual {v12, v13}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v13

    const/4 v14, -0x1

    if-eq v13, v14, :cond_2

    const/4 v2, 0x1

    .line 80
    :goto_6
    goto :goto_1

    .line 79
    :cond_2
    const/4 v2, 0x0

    goto :goto_6

    .line 81
    :cond_3
    sget-object v13, Lorg/apache/lucene/util/Constants;->OS_ARCH:Ljava/lang/String;

    if-eqz v13, :cond_4

    sget-object v13, Lorg/apache/lucene/util/Constants;->OS_ARCH:Ljava/lang/String;

    const-string v14, "64"

    invoke-virtual {v13, v14}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v13

    const/4 v14, -0x1

    if-eq v13, v14, :cond_4

    .line 82
    const/4 v2, 0x1

    .line 83
    goto :goto_1

    .line 84
    :cond_4
    const/4 v2, 0x0

    goto :goto_1

    .line 94
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v12    # "x":Ljava/lang/String;
    .restart local v10    # "v7":Z
    :catch_1
    move-exception v3

    .line 95
    .local v3, "nsme":Ljava/lang/NoSuchMethodException;
    const/4 v10, 0x0

    goto :goto_2

    .line 104
    .end local v3    # "nsme":Ljava/lang/NoSuchMethodException;
    .restart local v11    # "v8":Z
    :catch_2
    move-exception v3

    .line 105
    .restart local v3    # "nsme":Ljava/lang/NoSuchMethodException;
    const/4 v11, 0x0

    goto :goto_3

    .line 109
    .end local v3    # "nsme":Ljava/lang/NoSuchMethodException;
    .end local v11    # "v8":Z
    :cond_5
    const/4 v13, 0x0

    sput-boolean v13, Lorg/apache/lucene/util/Constants;->JRE_IS_MINIMUM_JAVA8:Z

    goto :goto_4

    .line 133
    .restart local v5    # "pkg":Ljava/lang/Package;
    :cond_6
    invoke-virtual {v5}, Ljava/lang/Package;->getImplementationVersion()Ljava/lang/String;

    move-result-object v9

    goto :goto_5

    .line 139
    .restart local v4    # "parts":[Ljava/lang/String;
    .restart local v9    # "v":Ljava/lang/String;
    :cond_7
    new-instance v13, Ljava/lang/StringBuilder;

    const/4 v14, 0x0

    aget-object v14, v4, v14

    invoke-static {v14}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v14, "."

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const/4 v14, 0x1

    aget-object v14, v4, v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "-SNAPSHOT"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 144
    .end local v4    # "parts":[Ljava/lang/String;
    :cond_8
    :goto_7
    invoke-static {v9}, Lorg/apache/lucene/util/Constants;->ident(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    sput-object v13, Lorg/apache/lucene/util/Constants;->LUCENE_VERSION:Ljava/lang/String;

    .line 145
    return-void

    .line 141
    .restart local v4    # "parts":[Ljava/lang/String;
    :cond_9
    new-instance v13, Ljava/lang/StringBuilder;

    sget-object v14, Lorg/apache/lucene/util/Constants;->LUCENE_MAIN_VERSION:Ljava/lang/String;

    invoke-static {v14}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v14, "-SNAPSHOT"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    goto :goto_7
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static ident(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 116
    invoke-virtual {p0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
