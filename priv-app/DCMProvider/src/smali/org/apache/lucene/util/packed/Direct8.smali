.class final Lorg/apache/lucene/util/packed/Direct8;
.super Lorg/apache/lucene/util/packed/PackedInts$MutableImpl;
.source "Direct8.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final values:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lorg/apache/lucene/util/packed/Direct8;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/packed/Direct8;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(I)V
    .locals 1
    .param p1, "valueCount"    # I

    .prologue
    .line 36
    const/16 v0, 0x8

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/util/packed/PackedInts$MutableImpl;-><init>(II)V

    .line 37
    new-array v0, p1, [B

    iput-object v0, p0, Lorg/apache/lucene/util/packed/Direct8;->values:[B

    .line 38
    return-void
.end method

.method constructor <init>(ILorg/apache/lucene/store/DataInput;I)V
    .locals 8
    .param p1, "packedIntsVersion"    # I
    .param p2, "in"    # Lorg/apache/lucene/store/DataInput;
    .param p3, "valueCount"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0, p3}, Lorg/apache/lucene/util/packed/Direct8;-><init>(I)V

    .line 42
    iget-object v2, p0, Lorg/apache/lucene/util/packed/Direct8;->values:[B

    const/4 v3, 0x0

    invoke-virtual {p2, v2, v3, p3}, Lorg/apache/lucene/store/DataInput;->readBytes([BII)V

    .line 44
    sget-object v2, Lorg/apache/lucene/util/packed/PackedInts$Format;->PACKED:Lorg/apache/lucene/util/packed/PackedInts$Format;

    const/16 v3, 0x8

    invoke-virtual {v2, p1, p3, v3}, Lorg/apache/lucene/util/packed/PackedInts$Format;->byteCount(III)J

    move-result-wide v2

    const-wide/16 v4, 0x1

    int-to-long v6, p3

    mul-long/2addr v4, v6

    sub-long/2addr v2, v4

    long-to-int v1, v2

    .line 45
    .local v1, "remaining":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v1, :cond_0

    .line 48
    return-void

    .line 46
    :cond_0
    invoke-virtual {p2}, Lorg/apache/lucene/store/DataInput;->readByte()B

    .line 45
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public clear()V
    .locals 2

    .prologue
    .line 71
    iget-object v0, p0, Lorg/apache/lucene/util/packed/Direct8;->values:[B

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([BB)V

    .line 72
    return-void
.end method

.method public fill(IIJ)V
    .locals 3
    .param p1, "fromIndex"    # I
    .param p2, "toIndex"    # I
    .param p3, "val"    # J

    .prologue
    .line 112
    sget-boolean v0, Lorg/apache/lucene/util/packed/Direct8;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    const-wide/16 v0, 0xff

    and-long/2addr v0, p3

    cmp-long v0, p3, v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 113
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/util/packed/Direct8;->values:[B

    long-to-int v1, p3

    int-to-byte v1, v1

    invoke-static {v0, p1, p2, v1}, Ljava/util/Arrays;->fill([BIIB)V

    .line 114
    return-void
.end method

.method public get(I[JII)I
    .locals 8
    .param p1, "index"    # I
    .param p2, "arr"    # [J
    .param p3, "off"    # I
    .param p4, "len"    # I

    .prologue
    .line 86
    sget-boolean v4, Lorg/apache/lucene/util/packed/Direct8;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    if-gtz p4, :cond_0

    new-instance v4, Ljava/lang/AssertionError;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "len must be > 0 (got "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    .line 87
    :cond_0
    sget-boolean v4, Lorg/apache/lucene/util/packed/Direct8;->$assertionsDisabled:Z

    if-nez v4, :cond_2

    if-ltz p1, :cond_1

    iget v4, p0, Lorg/apache/lucene/util/packed/Direct8;->valueCount:I

    if-lt p1, v4, :cond_2

    :cond_1
    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 88
    :cond_2
    sget-boolean v4, Lorg/apache/lucene/util/packed/Direct8;->$assertionsDisabled:Z

    if-nez v4, :cond_3

    add-int v4, p3, p4

    array-length v5, p2

    if-le v4, v5, :cond_3

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 90
    :cond_3
    iget v4, p0, Lorg/apache/lucene/util/packed/Direct8;->valueCount:I

    sub-int/2addr v4, p1

    invoke-static {v4, p4}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 91
    .local v1, "gets":I
    move v2, p1

    .local v2, "i":I
    move v3, p3

    .local v3, "o":I
    add-int v0, p1, v1

    .local v0, "end":I
    :goto_0
    if-lt v2, v0, :cond_4

    .line 94
    return v1

    .line 92
    :cond_4
    iget-object v4, p0, Lorg/apache/lucene/util/packed/Direct8;->values:[B

    aget-byte v4, v4, v2

    int-to-long v4, v4

    const-wide/16 v6, 0xff

    and-long/2addr v4, v6

    aput-wide v4, p2, v3

    .line 91
    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public get(I)J
    .locals 4
    .param p1, "index"    # I

    .prologue
    .line 52
    iget-object v0, p0, Lorg/apache/lucene/util/packed/Direct8;->values:[B

    aget-byte v0, v0, p1

    int-to-long v0, v0

    const-wide/16 v2, 0xff

    and-long/2addr v0, v2

    return-wide v0
.end method

.method public getArray()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lorg/apache/lucene/util/packed/Direct8;->values:[B

    return-object v0
.end method

.method public hasArray()Z
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x1

    return v0
.end method

.method public ramBytesUsed()J
    .locals 4

    .prologue
    .line 63
    sget v0, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_HEADER:I

    add-int/lit8 v0, v0, 0x8

    .line 65
    sget v1, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_REF:I

    .line 63
    add-int/2addr v0, v1

    int-to-long v0, v0

    .line 62
    invoke-static {v0, v1}, Lorg/apache/lucene/util/RamUsageEstimator;->alignObjectSize(J)J

    move-result-wide v0

    .line 66
    iget-object v2, p0, Lorg/apache/lucene/util/packed/Direct8;->values:[B

    invoke-static {v2}, Lorg/apache/lucene/util/RamUsageEstimator;->sizeOf([B)J

    move-result-wide v2

    .line 62
    add-long/2addr v0, v2

    return-wide v0
.end method

.method public set(I[JII)I
    .locals 8
    .param p1, "index"    # I
    .param p2, "arr"    # [J
    .param p3, "off"    # I
    .param p4, "len"    # I

    .prologue
    .line 99
    sget-boolean v4, Lorg/apache/lucene/util/packed/Direct8;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    if-gtz p4, :cond_0

    new-instance v4, Ljava/lang/AssertionError;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "len must be > 0 (got "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    .line 100
    :cond_0
    sget-boolean v4, Lorg/apache/lucene/util/packed/Direct8;->$assertionsDisabled:Z

    if-nez v4, :cond_2

    if-ltz p1, :cond_1

    iget v4, p0, Lorg/apache/lucene/util/packed/Direct8;->valueCount:I

    if-lt p1, v4, :cond_2

    :cond_1
    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 101
    :cond_2
    sget-boolean v4, Lorg/apache/lucene/util/packed/Direct8;->$assertionsDisabled:Z

    if-nez v4, :cond_3

    add-int v4, p3, p4

    array-length v5, p2

    if-le v4, v5, :cond_3

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 103
    :cond_3
    iget v4, p0, Lorg/apache/lucene/util/packed/Direct8;->valueCount:I

    sub-int/2addr v4, p1

    invoke-static {v4, p4}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 104
    .local v3, "sets":I
    move v1, p1

    .local v1, "i":I
    move v2, p3

    .local v2, "o":I
    add-int v0, p1, v3

    .local v0, "end":I
    :goto_0
    if-lt v1, v0, :cond_4

    .line 107
    return v3

    .line 105
    :cond_4
    iget-object v4, p0, Lorg/apache/lucene/util/packed/Direct8;->values:[B

    aget-wide v6, p2, v2

    long-to-int v5, v6

    int-to-byte v5, v5

    aput-byte v5, v4, v1

    .line 104
    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public set(IJ)V
    .locals 2
    .param p1, "index"    # I
    .param p2, "value"    # J

    .prologue
    .line 57
    iget-object v0, p0, Lorg/apache/lucene/util/packed/Direct8;->values:[B

    long-to-int v1, p2

    int-to-byte v1, v1

    aput-byte v1, v0, p1

    .line 58
    return-void
.end method
