.class Lorg/apache/lucene/util/DocIdBitSet$DocIdBitSetIterator;
.super Lorg/apache/lucene/search/DocIdSetIterator;
.source "DocIdBitSet.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/DocIdBitSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DocIdBitSetIterator"
.end annotation


# instance fields
.field private bitSet:Ljava/util/BitSet;

.field private docId:I


# direct methods
.method constructor <init>(Ljava/util/BitSet;)V
    .locals 1
    .param p1, "bitSet"    # Ljava/util/BitSet;

    .prologue
    .line 72
    invoke-direct {p0}, Lorg/apache/lucene/search/DocIdSetIterator;-><init>()V

    .line 73
    iput-object p1, p0, Lorg/apache/lucene/util/DocIdBitSet$DocIdBitSetIterator;->bitSet:Ljava/util/BitSet;

    .line 74
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/util/DocIdBitSet$DocIdBitSetIterator;->docId:I

    .line 75
    return-void
.end method


# virtual methods
.method public advance(I)I
    .locals 2
    .param p1, "target"    # I

    .prologue
    .line 93
    iget-object v1, p0, Lorg/apache/lucene/util/DocIdBitSet$DocIdBitSetIterator;->bitSet:Ljava/util/BitSet;

    invoke-virtual {v1, p1}, Ljava/util/BitSet;->nextSetBit(I)I

    move-result v0

    .line 95
    .local v0, "d":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const v0, 0x7fffffff

    .end local v0    # "d":I
    :cond_0
    iput v0, p0, Lorg/apache/lucene/util/DocIdBitSet$DocIdBitSetIterator;->docId:I

    .line 96
    iget v1, p0, Lorg/apache/lucene/util/DocIdBitSet$DocIdBitSetIterator;->docId:I

    return v1
.end method

.method public cost()J
    .locals 2

    .prologue
    .line 102
    iget-object v0, p0, Lorg/apache/lucene/util/DocIdBitSet$DocIdBitSetIterator;->bitSet:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->length()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 79
    iget v0, p0, Lorg/apache/lucene/util/DocIdBitSet$DocIdBitSetIterator;->docId:I

    return v0
.end method

.method public nextDoc()I
    .locals 3

    .prologue
    .line 85
    iget-object v1, p0, Lorg/apache/lucene/util/DocIdBitSet$DocIdBitSetIterator;->bitSet:Ljava/util/BitSet;

    iget v2, p0, Lorg/apache/lucene/util/DocIdBitSet$DocIdBitSetIterator;->docId:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->nextSetBit(I)I

    move-result v0

    .line 87
    .local v0, "d":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const v0, 0x7fffffff

    .end local v0    # "d":I
    :cond_0
    iput v0, p0, Lorg/apache/lucene/util/DocIdBitSet$DocIdBitSetIterator;->docId:I

    .line 88
    iget v1, p0, Lorg/apache/lucene/util/DocIdBitSet$DocIdBitSetIterator;->docId:I

    return v1
.end method
