.class public Lorg/apache/lucene/util/packed/PackedInts$Header;
.super Ljava/lang/Object;
.source "PackedInts.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/packed/PackedInts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Header"
.end annotation


# instance fields
.field private final bitsPerValue:I

.field private final format:Lorg/apache/lucene/util/packed/PackedInts$Format;

.field private final valueCount:I

.field private final version:I


# direct methods
.method public constructor <init>(Lorg/apache/lucene/util/packed/PackedInts$Format;III)V
    .locals 0
    .param p1, "format"    # Lorg/apache/lucene/util/packed/PackedInts$Format;
    .param p2, "valueCount"    # I
    .param p3, "bitsPerValue"    # I
    .param p4, "version"    # I

    .prologue
    .line 1256
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1257
    iput-object p1, p0, Lorg/apache/lucene/util/packed/PackedInts$Header;->format:Lorg/apache/lucene/util/packed/PackedInts$Format;

    .line 1258
    iput p2, p0, Lorg/apache/lucene/util/packed/PackedInts$Header;->valueCount:I

    .line 1259
    iput p3, p0, Lorg/apache/lucene/util/packed/PackedInts$Header;->bitsPerValue:I

    .line 1260
    iput p4, p0, Lorg/apache/lucene/util/packed/PackedInts$Header;->version:I

    .line 1261
    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/util/packed/PackedInts$Header;)Lorg/apache/lucene/util/packed/PackedInts$Format;
    .locals 1

    .prologue
    .line 1251
    iget-object v0, p0, Lorg/apache/lucene/util/packed/PackedInts$Header;->format:Lorg/apache/lucene/util/packed/PackedInts$Format;

    return-object v0
.end method

.method static synthetic access$1(Lorg/apache/lucene/util/packed/PackedInts$Header;)I
    .locals 1

    .prologue
    .line 1254
    iget v0, p0, Lorg/apache/lucene/util/packed/PackedInts$Header;->version:I

    return v0
.end method

.method static synthetic access$2(Lorg/apache/lucene/util/packed/PackedInts$Header;)I
    .locals 1

    .prologue
    .line 1252
    iget v0, p0, Lorg/apache/lucene/util/packed/PackedInts$Header;->valueCount:I

    return v0
.end method

.method static synthetic access$3(Lorg/apache/lucene/util/packed/PackedInts$Header;)I
    .locals 1

    .prologue
    .line 1253
    iget v0, p0, Lorg/apache/lucene/util/packed/PackedInts$Header;->bitsPerValue:I

    return v0
.end method
