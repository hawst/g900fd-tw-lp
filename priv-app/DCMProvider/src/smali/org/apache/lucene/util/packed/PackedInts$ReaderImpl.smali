.class abstract Lorg/apache/lucene/util/packed/PackedInts$ReaderImpl;
.super Ljava/lang/Object;
.source "PackedInts.java"

# interfaces
.implements Lorg/apache/lucene/util/packed/PackedInts$Reader;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/packed/PackedInts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x408
    name = "ReaderImpl"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field protected final bitsPerValue:I

.field protected final valueCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 595
    const-class v0, Lorg/apache/lucene/util/packed/PackedInts;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/packed/PackedInts$ReaderImpl;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected constructor <init>(II)V
    .locals 3
    .param p1, "valueCount"    # I
    .param p2, "bitsPerValue"    # I

    .prologue
    .line 599
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 600
    iput p2, p0, Lorg/apache/lucene/util/packed/PackedInts$ReaderImpl;->bitsPerValue:I

    .line 601
    sget-boolean v0, Lorg/apache/lucene/util/packed/PackedInts$ReaderImpl;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-lez p2, :cond_0

    const/16 v0, 0x40

    if-le p2, v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "bitsPerValue="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 602
    :cond_1
    iput p1, p0, Lorg/apache/lucene/util/packed/PackedInts$ReaderImpl;->valueCount:I

    .line 603
    return-void
.end method


# virtual methods
.method public get(I[JII)I
    .locals 7
    .param p1, "index"    # I
    .param p2, "arr"    # [J
    .param p3, "off"    # I
    .param p4, "len"    # I

    .prologue
    .line 627
    sget-boolean v4, Lorg/apache/lucene/util/packed/PackedInts$ReaderImpl;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    if-gtz p4, :cond_0

    new-instance v4, Ljava/lang/AssertionError;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "len must be > 0 (got "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    .line 628
    :cond_0
    sget-boolean v4, Lorg/apache/lucene/util/packed/PackedInts$ReaderImpl;->$assertionsDisabled:Z

    if-nez v4, :cond_2

    if-ltz p1, :cond_1

    iget v4, p0, Lorg/apache/lucene/util/packed/PackedInts$ReaderImpl;->valueCount:I

    if-lt p1, v4, :cond_2

    :cond_1
    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 629
    :cond_2
    sget-boolean v4, Lorg/apache/lucene/util/packed/PackedInts$ReaderImpl;->$assertionsDisabled:Z

    if-nez v4, :cond_3

    add-int v4, p3, p4

    array-length v5, p2

    if-le v4, v5, :cond_3

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 631
    :cond_3
    iget v4, p0, Lorg/apache/lucene/util/packed/PackedInts$ReaderImpl;->valueCount:I

    sub-int/2addr v4, p1

    invoke-static {v4, p4}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 632
    .local v1, "gets":I
    move v2, p1

    .local v2, "i":I
    move v3, p3

    .local v3, "o":I
    add-int v0, p1, v1

    .local v0, "end":I
    :goto_0
    if-lt v2, v0, :cond_4

    .line 635
    return v1

    .line 633
    :cond_4
    invoke-virtual {p0, v2}, Lorg/apache/lucene/util/packed/PackedInts$ReaderImpl;->get(I)J

    move-result-wide v4

    aput-wide v4, p2, v3

    .line 632
    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public getArray()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 617
    const/4 v0, 0x0

    return-object v0
.end method

.method public getBitsPerValue()I
    .locals 1

    .prologue
    .line 607
    iget v0, p0, Lorg/apache/lucene/util/packed/PackedInts$ReaderImpl;->bitsPerValue:I

    return v0
.end method

.method public hasArray()Z
    .locals 1

    .prologue
    .line 622
    const/4 v0, 0x0

    return v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 612
    iget v0, p0, Lorg/apache/lucene/util/packed/PackedInts$ReaderImpl;->valueCount:I

    return v0
.end method
