.class Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;
.super Ljava/lang/Object;
.source "UTF32ToUTF8.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/automaton/UTF32ToUTF8;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "UTF8Sequence"
.end annotation


# instance fields
.field private final bytes:[Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Byte;

.field private len:I


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x4

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    new-array v1, v4, [Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Byte;

    iput-object v1, p0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->bytes:[Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Byte;

    .line 64
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v4, :cond_0

    .line 67
    return-void

    .line 65
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->bytes:[Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Byte;

    new-instance v2, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Byte;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Byte;-><init>(Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Byte;)V

    aput-object v2, v1, v0

    .line 64
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method static synthetic access$0(Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;I)V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0, p1}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->set(I)V

    return-void
.end method

.method static synthetic access$1(Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;)I
    .locals 1

    .prologue
    .line 60
    iget v0, p0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->len:I

    return v0
.end method

.method private set(I)V
    .locals 7
    .param p1, "code"    # I

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x3

    const/4 v2, 0x0

    .line 78
    const/16 v0, 0x80

    if-ge p1, v0, :cond_0

    .line 80
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->bytes:[Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Byte;

    aget-object v0, v0, v2

    iput p1, v0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Byte;->value:I

    .line 81
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->bytes:[Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Byte;

    aget-object v0, v0, v2

    const/4 v1, 0x7

    iput-byte v1, v0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Byte;->bits:B

    .line 82
    iput v4, p0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->len:I

    .line 102
    :goto_0
    return-void

    .line 83
    :cond_0
    const/16 v0, 0x800

    if-ge p1, v0, :cond_1

    .line 85
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->bytes:[Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Byte;

    aget-object v0, v0, v2

    shr-int/lit8 v1, p1, 0x6

    or-int/lit16 v1, v1, 0xc0

    iput v1, v0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Byte;->value:I

    .line 86
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->bytes:[Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Byte;

    aget-object v0, v0, v2

    const/4 v1, 0x5

    iput-byte v1, v0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Byte;->bits:B

    .line 87
    invoke-direct {p0, p1, v4}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->setRest(II)V

    .line 88
    iput v5, p0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->len:I

    goto :goto_0

    .line 89
    :cond_1
    const/high16 v0, 0x10000

    if-ge p1, v0, :cond_2

    .line 91
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->bytes:[Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Byte;

    aget-object v0, v0, v2

    shr-int/lit8 v1, p1, 0xc

    or-int/lit16 v1, v1, 0xe0

    iput v1, v0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Byte;->value:I

    .line 92
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->bytes:[Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Byte;

    aget-object v0, v0, v2

    iput-byte v6, v0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Byte;->bits:B

    .line 93
    invoke-direct {p0, p1, v5}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->setRest(II)V

    .line 94
    iput v3, p0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->len:I

    goto :goto_0

    .line 97
    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->bytes:[Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Byte;

    aget-object v0, v0, v2

    shr-int/lit8 v1, p1, 0x12

    or-int/lit16 v1, v1, 0xf0

    iput v1, v0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Byte;->value:I

    .line 98
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->bytes:[Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Byte;

    aget-object v0, v0, v2

    iput-byte v3, v0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Byte;->bits:B

    .line 99
    invoke-direct {p0, p1, v3}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->setRest(II)V

    .line 100
    iput v6, p0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->len:I

    goto :goto_0
.end method

.method private setRest(II)V
    .locals 4
    .param p1, "code"    # I
    .param p2, "numBytes"    # I

    .prologue
    .line 105
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, p2, :cond_0

    .line 110
    return-void

    .line 106
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->bytes:[Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Byte;

    sub-int v2, p2, v0

    aget-object v1, v1, v2

    sget-object v2, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->MASKS:[I

    const/4 v3, 0x5

    aget v2, v2, v3

    and-int/2addr v2, p1

    or-int/lit16 v2, v2, 0x80

    iput v2, v1, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Byte;->value:I

    .line 107
    iget-object v1, p0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->bytes:[Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Byte;

    sub-int v2, p2, v0

    aget-object v1, v1, v2

    const/4 v2, 0x6

    iput-byte v2, v1, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Byte;->bits:B

    .line 108
    shr-int/lit8 p1, p1, 0x6

    .line 105
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public byteAt(I)I
    .locals 1
    .param p1, "idx"    # I

    .prologue
    .line 70
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->bytes:[Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Byte;

    aget-object v0, v0, p1

    iget v0, v0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Byte;->value:I

    return v0
.end method

.method public numBits(I)I
    .locals 1
    .param p1, "idx"    # I

    .prologue
    .line 74
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->bytes:[Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Byte;

    aget-object v0, v0, p1

    iget-byte v0, v0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Byte;->bits:B

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 114
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 115
    .local v0, "b":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v2, p0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->len:I

    if-lt v1, v2, :cond_0

    .line 121
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 116
    :cond_0
    if-lez v1, :cond_1

    .line 117
    const/16 v2, 0x20

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 119
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Sequence;->bytes:[Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Byte;

    aget-object v2, v2, v1

    iget v2, v2, Lorg/apache/lucene/util/automaton/UTF32ToUTF8$UTF8Byte;->value:I

    invoke-static {v2}, Ljava/lang/Integer;->toBinaryString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 115
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
