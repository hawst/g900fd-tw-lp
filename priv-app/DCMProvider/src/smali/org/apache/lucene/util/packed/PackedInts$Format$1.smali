.class enum Lorg/apache/lucene/util/packed/PackedInts$Format$1;
.super Lorg/apache/lucene/util/packed/PackedInts$Format;
.source "PackedInts.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/packed/PackedInts$Format;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4000
    name = null
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;II)V
    .locals 1
    .param p3, "$anonymous0"    # I

    .prologue
    .line 88
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lorg/apache/lucene/util/packed/PackedInts$Format;-><init>(Ljava/lang/String;IILorg/apache/lucene/util/packed/PackedInts$Format;)V

    .line 1
    return-void
.end method


# virtual methods
.method public byteCount(III)J
    .locals 6
    .param p1, "packedIntsVersion"    # I
    .param p2, "valueCount"    # I
    .param p3, "bitsPerValue"    # I

    .prologue
    .line 92
    const/4 v0, 0x1

    if-ge p1, v0, :cond_0

    .line 93
    const-wide/16 v0, 0x8

    int-to-double v2, p2

    int-to-double v4, p3

    mul-double/2addr v2, v4

    const-wide/high16 v4, 0x4050000000000000L    # 64.0

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-long v2, v2

    mul-long/2addr v0, v2

    .line 95
    :goto_0
    return-wide v0

    :cond_0
    int-to-double v0, p2

    int-to-double v2, p3

    mul-double/2addr v0, v2

    const-wide/high16 v2, 0x4020000000000000L    # 8.0

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-long v0, v0

    goto :goto_0
.end method
