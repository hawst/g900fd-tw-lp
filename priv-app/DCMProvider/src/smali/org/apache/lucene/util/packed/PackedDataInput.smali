.class public final Lorg/apache/lucene/util/packed/PackedDataInput;
.super Ljava/lang/Object;
.source "PackedDataInput.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field current:J

.field final in:Lorg/apache/lucene/store/DataInput;

.field remainingBits:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lorg/apache/lucene/util/packed/PackedDataInput;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/packed/PackedDataInput;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/store/DataInput;)V
    .locals 0
    .param p1, "in"    # Lorg/apache/lucene/store/DataInput;

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lorg/apache/lucene/util/packed/PackedDataInput;->in:Lorg/apache/lucene/store/DataInput;

    .line 42
    invoke-virtual {p0}, Lorg/apache/lucene/util/packed/PackedDataInput;->skipToNextByte()V

    .line 43
    return-void
.end method


# virtual methods
.method public readLong(I)J
    .locals 12
    .param p1, "bitsPerValue"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v10, 0x1

    .line 49
    sget-boolean v1, Lorg/apache/lucene/util/packed/PackedDataInput;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    if-lez p1, :cond_0

    const/16 v1, 0x40

    if-le p1, v1, :cond_1

    :cond_0
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, p1}, Ljava/lang/AssertionError;-><init>(I)V

    throw v1

    .line 50
    :cond_1
    const-wide/16 v2, 0x0

    .line 51
    .local v2, "r":J
    :goto_0
    if-gtz p1, :cond_2

    .line 61
    return-wide v2

    .line 52
    :cond_2
    iget v1, p0, Lorg/apache/lucene/util/packed/PackedDataInput;->remainingBits:I

    if-nez v1, :cond_3

    .line 53
    iget-object v1, p0, Lorg/apache/lucene/util/packed/PackedDataInput;->in:Lorg/apache/lucene/store/DataInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    int-to-long v4, v1

    iput-wide v4, p0, Lorg/apache/lucene/util/packed/PackedDataInput;->current:J

    .line 54
    const/16 v1, 0x8

    iput v1, p0, Lorg/apache/lucene/util/packed/PackedDataInput;->remainingBits:I

    .line 56
    :cond_3
    iget v1, p0, Lorg/apache/lucene/util/packed/PackedDataInput;->remainingBits:I

    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 57
    .local v0, "bits":I
    shl-long v4, v2, v0

    iget-wide v6, p0, Lorg/apache/lucene/util/packed/PackedDataInput;->current:J

    iget v1, p0, Lorg/apache/lucene/util/packed/PackedDataInput;->remainingBits:I

    sub-int/2addr v1, v0

    ushr-long/2addr v6, v1

    shl-long v8, v10, v0

    sub-long/2addr v8, v10

    and-long/2addr v6, v8

    or-long v2, v4, v6

    .line 58
    sub-int/2addr p1, v0

    .line 59
    iget v1, p0, Lorg/apache/lucene/util/packed/PackedDataInput;->remainingBits:I

    sub-int/2addr v1, v0

    iput v1, p0, Lorg/apache/lucene/util/packed/PackedDataInput;->remainingBits:I

    goto :goto_0
.end method

.method public skipToNextByte()V
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/util/packed/PackedDataInput;->remainingBits:I

    .line 70
    return-void
.end method
