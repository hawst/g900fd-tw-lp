.class Lorg/apache/lucene/util/SorterTemplate$TimSort;
.super Ljava/lang/Object;
.source "SorterTemplate.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/SorterTemplate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TimSort"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final hi:I

.field final minRun:I

.field final runEnds:[I

.field stackSize:I

.field final synthetic this$0:Lorg/apache/lucene/util/SorterTemplate;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 166
    const-class v0, Lorg/apache/lucene/util/SorterTemplate;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/SorterTemplate$TimSort;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/util/SorterTemplate;II)V
    .locals 2
    .param p2, "lo"    # I
    .param p3, "hi"    # I

    .prologue
    const/4 v1, 0x0

    .line 173
    iput-object p1, p0, Lorg/apache/lucene/util/SorterTemplate$TimSort;->this$0:Lorg/apache/lucene/util/SorterTemplate;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 174
    sget-boolean v0, Lorg/apache/lucene/util/SorterTemplate$TimSort;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-gt p3, p2, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 176
    :cond_0
    const/16 v0, 0x29

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/apache/lucene/util/SorterTemplate$TimSort;->runEnds:[I

    .line 177
    iget-object v0, p0, Lorg/apache/lucene/util/SorterTemplate$TimSort;->runEnds:[I

    aput p2, v0, v1

    .line 178
    iput v1, p0, Lorg/apache/lucene/util/SorterTemplate$TimSort;->stackSize:I

    .line 179
    iput p3, p0, Lorg/apache/lucene/util/SorterTemplate$TimSort;->hi:I

    .line 180
    sub-int v0, p3, p2

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lorg/apache/lucene/util/SorterTemplate$TimSort;->minRun(I)I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/util/SorterTemplate$TimSort;->minRun:I

    .line 181
    return-void
.end method


# virtual methods
.method ensureInvariants()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 268
    :goto_0
    iget v3, p0, Lorg/apache/lucene/util/SorterTemplate$TimSort;->stackSize:I

    if-gt v3, v5, :cond_1

    .line 293
    :cond_0
    return-void

    .line 269
    :cond_1
    invoke-virtual {p0, v4}, Lorg/apache/lucene/util/SorterTemplate$TimSort;->runLen(I)I

    move-result v0

    .line 270
    .local v0, "runLen0":I
    invoke-virtual {p0, v5}, Lorg/apache/lucene/util/SorterTemplate$TimSort;->runLen(I)I

    move-result v1

    .line 272
    .local v1, "runLen1":I
    iget v3, p0, Lorg/apache/lucene/util/SorterTemplate$TimSort;->stackSize:I

    if-le v3, v6, :cond_3

    .line 273
    invoke-virtual {p0, v6}, Lorg/apache/lucene/util/SorterTemplate$TimSort;->runLen(I)I

    move-result v2

    .line 275
    .local v2, "runLen2":I
    add-int v3, v1, v0

    if-gt v2, v3, :cond_3

    .line 277
    if-ge v2, v0, :cond_2

    .line 278
    invoke-virtual {p0, v5}, Lorg/apache/lucene/util/SorterTemplate$TimSort;->mergeAt(I)V

    goto :goto_0

    .line 280
    :cond_2
    invoke-virtual {p0, v4}, Lorg/apache/lucene/util/SorterTemplate$TimSort;->mergeAt(I)V

    goto :goto_0

    .line 286
    .end local v2    # "runLen2":I
    :cond_3
    if-gt v1, v0, :cond_0

    .line 287
    invoke-virtual {p0, v4}, Lorg/apache/lucene/util/SorterTemplate$TimSort;->mergeAt(I)V

    goto :goto_0
.end method

.method exhaustStack()V
    .locals 2

    .prologue
    .line 296
    :goto_0
    iget v0, p0, Lorg/apache/lucene/util/SorterTemplate$TimSort;->stackSize:I

    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    .line 299
    return-void

    .line 297
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/util/SorterTemplate$TimSort;->mergeAt(I)V

    goto :goto_0
.end method

.method mergeAt(I)V
    .locals 7
    .param p1, "i"    # I

    .prologue
    .line 221
    sget-boolean v0, Lorg/apache/lucene/util/SorterTemplate$TimSort;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/util/SorterTemplate$TimSort;->stackSize:I

    add-int/lit8 v4, p1, 0x1

    if-gt v0, v4, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 222
    :cond_0
    add-int/lit8 v0, p1, 0x1

    invoke-virtual {p0, v0}, Lorg/apache/lucene/util/SorterTemplate$TimSort;->runBase(I)I

    move-result v1

    .line 223
    .local v1, "l":I
    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/SorterTemplate$TimSort;->runBase(I)I

    move-result v2

    .line 224
    .local v2, "pivot":I
    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/SorterTemplate$TimSort;->runEnd(I)I

    move-result v3

    .line 225
    .local v3, "h":I
    iget-object v0, p0, Lorg/apache/lucene/util/SorterTemplate$TimSort;->this$0:Lorg/apache/lucene/util/SorterTemplate;

    sub-int v4, v2, v1

    sub-int v5, v3, v2

    # invokes: Lorg/apache/lucene/util/SorterTemplate;->runMerge(IIIII)V
    invoke-static/range {v0 .. v5}, Lorg/apache/lucene/util/SorterTemplate;->access$0(Lorg/apache/lucene/util/SorterTemplate;IIIII)V

    .line 226
    add-int/lit8 v6, p1, 0x1

    .local v6, "j":I
    :goto_0
    if-gtz v6, :cond_1

    .line 229
    iget v0, p0, Lorg/apache/lucene/util/SorterTemplate$TimSort;->stackSize:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/util/SorterTemplate$TimSort;->stackSize:I

    .line 230
    return-void

    .line 227
    :cond_1
    add-int/lit8 v0, v6, -0x1

    invoke-virtual {p0, v0}, Lorg/apache/lucene/util/SorterTemplate$TimSort;->runEnd(I)I

    move-result v0

    invoke-virtual {p0, v6, v0}, Lorg/apache/lucene/util/SorterTemplate$TimSort;->setRunEnd(II)V

    .line 226
    add-int/lit8 v6, v6, -0x1

    goto :goto_0
.end method

.method minRun(I)I
    .locals 6
    .param p1, "length"    # I

    .prologue
    const/16 v5, 0x40

    const/16 v4, 0x20

    .line 185
    sget-boolean v3, Lorg/apache/lucene/util/SorterTemplate$TimSort;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    if-ge p1, v4, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 186
    :cond_0
    move v1, p1

    .line 187
    .local v1, "n":I
    const/4 v2, 0x0

    .line 188
    .local v2, "r":I
    :goto_0
    if-ge v1, v5, :cond_2

    .line 192
    add-int v0, v1, v2

    .line 193
    .local v0, "minRun":I
    sget-boolean v3, Lorg/apache/lucene/util/SorterTemplate$TimSort;->$assertionsDisabled:Z

    if-nez v3, :cond_3

    if-lt v0, v4, :cond_1

    if-le v0, v5, :cond_3

    :cond_1
    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 189
    .end local v0    # "minRun":I
    :cond_2
    and-int/lit8 v3, v1, 0x1

    or-int/2addr v2, v3

    .line 190
    ushr-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 194
    .restart local v0    # "minRun":I
    :cond_3
    return v0
.end method

.method nextRun()I
    .locals 7

    .prologue
    .line 235
    const/4 v4, 0x0

    invoke-virtual {p0, v4}, Lorg/apache/lucene/util/SorterTemplate$TimSort;->runEnd(I)I

    move-result v3

    .line 236
    .local v3, "runBase":I
    iget v4, p0, Lorg/apache/lucene/util/SorterTemplate$TimSort;->hi:I

    if-ne v3, v4, :cond_1

    .line 237
    const/4 v2, 0x1

    .line 264
    :cond_0
    :goto_0
    return v2

    .line 239
    :cond_1
    const/4 v2, 0x1

    .line 240
    .local v2, "l":I
    iget-object v4, p0, Lorg/apache/lucene/util/SorterTemplate$TimSort;->this$0:Lorg/apache/lucene/util/SorterTemplate;

    add-int/lit8 v5, v3, 0x1

    invoke-virtual {v4, v3, v5}, Lorg/apache/lucene/util/SorterTemplate;->compare(II)I

    move-result v4

    if-lez v4, :cond_6

    .line 242
    :goto_1
    add-int v4, v3, v2

    iget v5, p0, Lorg/apache/lucene/util/SorterTemplate$TimSort;->hi:I

    if-gt v4, v5, :cond_2

    iget-object v4, p0, Lorg/apache/lucene/util/SorterTemplate$TimSort;->this$0:Lorg/apache/lucene/util/SorterTemplate;

    add-int v5, v3, v2

    add-int/lit8 v5, v5, -0x1

    add-int v6, v3, v2

    invoke-virtual {v4, v5, v6}, Lorg/apache/lucene/util/SorterTemplate;->compare(II)I

    move-result v4

    if-gtz v4, :cond_3

    .line 245
    :cond_2
    iget v4, p0, Lorg/apache/lucene/util/SorterTemplate$TimSort;->minRun:I

    if-ge v2, v4, :cond_4

    add-int v4, v3, v2

    iget v5, p0, Lorg/apache/lucene/util/SorterTemplate$TimSort;->hi:I

    if-gt v4, v5, :cond_4

    .line 246
    iget v4, p0, Lorg/apache/lucene/util/SorterTemplate$TimSort;->hi:I

    sub-int/2addr v4, v3

    add-int/lit8 v4, v4, 0x1

    iget v5, p0, Lorg/apache/lucene/util/SorterTemplate$TimSort;->minRun:I

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 247
    iget-object v4, p0, Lorg/apache/lucene/util/SorterTemplate$TimSort;->this$0:Lorg/apache/lucene/util/SorterTemplate;

    add-int v5, v3, v2

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v4, v3, v5}, Lorg/apache/lucene/util/SorterTemplate;->binarySort(II)V

    goto :goto_0

    .line 243
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 250
    :cond_4
    const/4 v1, 0x0

    .local v1, "i":I
    ushr-int/lit8 v0, v2, 0x1

    .local v0, "halfL":I
    :goto_2
    if-ge v1, v0, :cond_0

    .line 251
    iget-object v4, p0, Lorg/apache/lucene/util/SorterTemplate$TimSort;->this$0:Lorg/apache/lucene/util/SorterTemplate;

    add-int v5, v3, v1

    add-int v6, v3, v2

    sub-int/2addr v6, v1

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v4, v5, v6}, Lorg/apache/lucene/util/SorterTemplate;->swap(II)V

    .line 250
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 257
    .end local v0    # "halfL":I
    .end local v1    # "i":I
    :cond_5
    add-int/lit8 v2, v2, 0x1

    .line 256
    :cond_6
    add-int v4, v3, v2

    iget v5, p0, Lorg/apache/lucene/util/SorterTemplate$TimSort;->hi:I

    if-gt v4, v5, :cond_7

    iget-object v4, p0, Lorg/apache/lucene/util/SorterTemplate$TimSort;->this$0:Lorg/apache/lucene/util/SorterTemplate;

    add-int v5, v3, v2

    add-int/lit8 v5, v5, -0x1

    add-int v6, v3, v2

    invoke-virtual {v4, v5, v6}, Lorg/apache/lucene/util/SorterTemplate;->compare(II)I

    move-result v4

    if-lez v4, :cond_5

    .line 259
    :cond_7
    iget v4, p0, Lorg/apache/lucene/util/SorterTemplate$TimSort;->minRun:I

    if-ge v2, v4, :cond_0

    add-int v4, v3, v2

    iget v5, p0, Lorg/apache/lucene/util/SorterTemplate$TimSort;->hi:I

    if-gt v4, v5, :cond_0

    .line 260
    iget v4, p0, Lorg/apache/lucene/util/SorterTemplate$TimSort;->hi:I

    sub-int/2addr v4, v3

    add-int/lit8 v4, v4, 0x1

    iget v5, p0, Lorg/apache/lucene/util/SorterTemplate$TimSort;->minRun:I

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 261
    iget-object v4, p0, Lorg/apache/lucene/util/SorterTemplate$TimSort;->this$0:Lorg/apache/lucene/util/SorterTemplate;

    add-int v5, v3, v2

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v4, v3, v5}, Lorg/apache/lucene/util/SorterTemplate;->binarySort(II)V

    goto/16 :goto_0
.end method

.method pushRunLen(I)V
    .locals 4
    .param p1, "len"    # I

    .prologue
    .line 215
    iget-object v0, p0, Lorg/apache/lucene/util/SorterTemplate$TimSort;->runEnds:[I

    iget v1, p0, Lorg/apache/lucene/util/SorterTemplate$TimSort;->stackSize:I

    add-int/lit8 v1, v1, 0x1

    iget-object v2, p0, Lorg/apache/lucene/util/SorterTemplate$TimSort;->runEnds:[I

    iget v3, p0, Lorg/apache/lucene/util/SorterTemplate$TimSort;->stackSize:I

    aget v2, v2, v3

    add-int/2addr v2, p1

    aput v2, v0, v1

    .line 216
    iget v0, p0, Lorg/apache/lucene/util/SorterTemplate$TimSort;->stackSize:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/util/SorterTemplate$TimSort;->stackSize:I

    .line 217
    return-void
.end method

.method runBase(I)I
    .locals 2
    .param p1, "i"    # I

    .prologue
    .line 203
    iget-object v0, p0, Lorg/apache/lucene/util/SorterTemplate$TimSort;->runEnds:[I

    iget v1, p0, Lorg/apache/lucene/util/SorterTemplate$TimSort;->stackSize:I

    sub-int/2addr v1, p1

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    return v0
.end method

.method runEnd(I)I
    .locals 2
    .param p1, "i"    # I

    .prologue
    .line 207
    iget-object v0, p0, Lorg/apache/lucene/util/SorterTemplate$TimSort;->runEnds:[I

    iget v1, p0, Lorg/apache/lucene/util/SorterTemplate$TimSort;->stackSize:I

    sub-int/2addr v1, p1

    aget v0, v0, v1

    return v0
.end method

.method runLen(I)I
    .locals 4
    .param p1, "i"    # I

    .prologue
    .line 198
    iget v1, p0, Lorg/apache/lucene/util/SorterTemplate$TimSort;->stackSize:I

    sub-int v0, v1, p1

    .line 199
    .local v0, "off":I
    iget-object v1, p0, Lorg/apache/lucene/util/SorterTemplate$TimSort;->runEnds:[I

    aget v1, v1, v0

    iget-object v2, p0, Lorg/apache/lucene/util/SorterTemplate$TimSort;->runEnds:[I

    add-int/lit8 v3, v0, -0x1

    aget v2, v2, v3

    sub-int/2addr v1, v2

    return v1
.end method

.method setRunEnd(II)V
    .locals 2
    .param p1, "i"    # I
    .param p2, "runEnd"    # I

    .prologue
    .line 211
    iget-object v0, p0, Lorg/apache/lucene/util/SorterTemplate$TimSort;->runEnds:[I

    iget v1, p0, Lorg/apache/lucene/util/SorterTemplate$TimSort;->stackSize:I

    sub-int/2addr v1, p1

    aput p2, v0, v1

    .line 212
    return-void
.end method

.method sort()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 303
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/util/SorterTemplate$TimSort;->ensureInvariants()V

    .line 306
    invoke-virtual {p0}, Lorg/apache/lucene/util/SorterTemplate$TimSort;->nextRun()I

    move-result v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/util/SorterTemplate$TimSort;->pushRunLen(I)V

    .line 308
    invoke-virtual {p0, v2}, Lorg/apache/lucene/util/SorterTemplate$TimSort;->runEnd(I)I

    move-result v0

    iget v1, p0, Lorg/apache/lucene/util/SorterTemplate$TimSort;->hi:I

    .line 302
    if-le v0, v1, :cond_0

    .line 310
    invoke-virtual {p0}, Lorg/apache/lucene/util/SorterTemplate$TimSort;->exhaustStack()V

    .line 311
    sget-boolean v0, Lorg/apache/lucene/util/SorterTemplate$TimSort;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    invoke-virtual {p0, v2}, Lorg/apache/lucene/util/SorterTemplate$TimSort;->runEnd(I)I

    move-result v0

    iget v1, p0, Lorg/apache/lucene/util/SorterTemplate$TimSort;->hi:I

    add-int/lit8 v1, v1, 0x1

    if-eq v0, v1, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 312
    :cond_1
    return-void
.end method
