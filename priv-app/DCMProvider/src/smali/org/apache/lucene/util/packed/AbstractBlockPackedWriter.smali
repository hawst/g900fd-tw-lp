.class abstract Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;
.super Ljava/lang/Object;
.source "AbstractBlockPackedWriter.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field static final BPV_SHIFT:I = 0x1

.field static final MAX_BLOCK_SIZE:I = 0x8000000

.field static final MIN_VALUE_EQUALS_0:I = 0x1


# instance fields
.field protected blocks:[B

.field protected finished:Z

.field protected off:I

.field protected ord:J

.field protected out:Lorg/apache/lucene/store/DataOutput;

.field protected final values:[J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->$assertionsDisabled:Z

    .line 29
    return-void

    .line 25
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/store/DataOutput;I)V
    .locals 1
    .param p1, "out"    # Lorg/apache/lucene/store/DataOutput;
    .param p2, "blockSize"    # I

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    invoke-static {p2}, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->checkBlockSize(I)V

    .line 70
    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->reset(Lorg/apache/lucene/store/DataOutput;)V

    .line 71
    new-array v0, p2, [J

    iput-object v0, p0, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->values:[J

    .line 72
    return-void
.end method

.method static checkBlockSize(I)V
    .locals 3
    .param p0, "blockSize"    # I

    .prologue
    .line 32
    if-lez p0, :cond_0

    const/high16 v0, 0x8000000

    if-le p0, v0, :cond_1

    .line 33
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "blockSize must be > 0 and < 134217728, got "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 35
    :cond_1
    const/16 v0, 0x40

    if-ge p0, v0, :cond_2

    .line 36
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "blockSize must be >= 64, got "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 38
    :cond_2
    add-int/lit8 v0, p0, -0x1

    and-int/2addr v0, p0

    if-eqz v0, :cond_3

    .line 39
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "blockSize must be a power of two, got "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 41
    :cond_3
    return-void
.end method

.method private checkNotFinished()V
    .locals 2

    .prologue
    .line 84
    iget-boolean v0, p0, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->finished:Z

    if-eqz v0, :cond_0

    .line 85
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already finished"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 87
    :cond_0
    return-void
.end method

.method static writeVLong(Lorg/apache/lucene/store/DataOutput;J)V
    .locals 7
    .param p0, "out"    # Lorg/apache/lucene/store/DataOutput;
    .param p1, "i"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 49
    const/4 v0, 0x0

    .local v0, "k":I
    move v1, v0

    .line 50
    .end local v0    # "k":I
    .local v1, "k":I
    :goto_0
    const-wide/16 v2, -0x80

    and-long/2addr v2, p1

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    add-int/lit8 v0, v1, 0x1

    .end local v1    # "k":I
    .restart local v0    # "k":I
    const/16 v2, 0x8

    if-lt v1, v2, :cond_0

    .line 54
    :goto_1
    long-to-int v2, p1

    int-to-byte v2, v2

    invoke-virtual {p0, v2}, Lorg/apache/lucene/store/DataOutput;->writeByte(B)V

    .line 55
    return-void

    .line 51
    :cond_0
    const-wide/16 v2, 0x7f

    and-long/2addr v2, p1

    const-wide/16 v4, 0x80

    or-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    invoke-virtual {p0, v2}, Lorg/apache/lucene/store/DataOutput;->writeByte(B)V

    .line 52
    const/4 v2, 0x7

    ushr-long/2addr p1, v2

    move v1, v0

    .end local v0    # "k":I
    .restart local v1    # "k":I
    goto :goto_0

    :cond_1
    move v0, v1

    .end local v1    # "k":I
    .restart local v0    # "k":I
    goto :goto_1
.end method

.method static zigZagEncode(J)J
    .locals 4
    .param p0, "n"    # J

    .prologue
    .line 44
    const/16 v0, 0x3f

    shr-long v0, p0, v0

    const/4 v2, 0x1

    shl-long v2, p0, v2

    xor-long/2addr v0, v2

    return-wide v0
.end method


# virtual methods
.method public add(J)V
    .locals 5
    .param p1, "l"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 91
    invoke-direct {p0}, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->checkNotFinished()V

    .line 92
    iget v0, p0, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->off:I

    iget-object v1, p0, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->values:[J

    array-length v1, v1

    if-ne v0, v1, :cond_0

    .line 93
    invoke-virtual {p0}, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->flush()V

    .line 95
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->values:[J

    iget v1, p0, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->off:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->off:I

    aput-wide p1, v0, v1

    .line 96
    iget-wide v0, p0, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->ord:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->ord:J

    .line 97
    return-void
.end method

.method addBlockOfZeros()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 101
    invoke-direct {p0}, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->checkNotFinished()V

    .line 102
    iget v0, p0, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->off:I

    if-eqz v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->off:I

    iget-object v1, p0, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->values:[J

    array-length v1, v1

    if-eq v0, v1, :cond_0

    .line 103
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->off:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 105
    :cond_0
    iget v0, p0, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->off:I

    iget-object v1, p0, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->values:[J

    array-length v1, v1

    if-ne v0, v1, :cond_1

    .line 106
    invoke-virtual {p0}, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->flush()V

    .line 108
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->values:[J

    const-wide/16 v2, 0x0

    invoke-static {v0, v2, v3}, Ljava/util/Arrays;->fill([JJ)V

    .line 109
    iget-object v0, p0, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->values:[J

    array-length v0, v0

    iput v0, p0, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->off:I

    .line 110
    iget-wide v0, p0, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->ord:J

    iget-object v2, p0, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->values:[J

    array-length v2, v2

    int-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->ord:J

    .line 111
    return-void
.end method

.method public finish()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 117
    invoke-direct {p0}, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->checkNotFinished()V

    .line 118
    iget v0, p0, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->off:I

    if-lez v0, :cond_0

    .line 119
    invoke-virtual {p0}, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->flush()V

    .line 121
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->finished:Z

    .line 122
    return-void
.end method

.method protected abstract flush()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public ord()J
    .locals 2

    .prologue
    .line 126
    iget-wide v0, p0, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->ord:J

    return-wide v0
.end method

.method public reset(Lorg/apache/lucene/store/DataOutput;)V
    .locals 3
    .param p1, "out"    # Lorg/apache/lucene/store/DataOutput;

    .prologue
    const/4 v2, 0x0

    .line 76
    sget-boolean v0, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 77
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->out:Lorg/apache/lucene/store/DataOutput;

    .line 78
    iput v2, p0, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->off:I

    .line 79
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->ord:J

    .line 80
    iput-boolean v2, p0, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->finished:Z

    .line 81
    return-void
.end method

.method protected final writeValues(I)V
    .locals 11
    .param p1, "bitsRequired"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x1

    const/4 v2, 0x0

    .line 132
    sget-object v1, Lorg/apache/lucene/util/packed/PackedInts$Format;->PACKED:Lorg/apache/lucene/util/packed/PackedInts$Format;

    invoke-static {v1, v10, p1}, Lorg/apache/lucene/util/packed/PackedInts;->getEncoder(Lorg/apache/lucene/util/packed/PackedInts$Format;II)Lorg/apache/lucene/util/packed/PackedInts$Encoder;

    move-result-object v0

    .line 133
    .local v0, "encoder":Lorg/apache/lucene/util/packed/PackedInts$Encoder;
    iget-object v1, p0, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->values:[J

    array-length v1, v1

    invoke-interface {v0}, Lorg/apache/lucene/util/packed/PackedInts$Encoder;->byteValueCount()I

    move-result v3

    div-int v5, v1, v3

    .line 134
    .local v5, "iterations":I
    invoke-interface {v0}, Lorg/apache/lucene/util/packed/PackedInts$Encoder;->byteBlockCount()I

    move-result v1

    mul-int v7, v1, v5

    .line 135
    .local v7, "blockSize":I
    iget-object v1, p0, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->blocks:[B

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->blocks:[B

    array-length v1, v1

    if-ge v1, v7, :cond_1

    .line 136
    :cond_0
    new-array v1, v7, [B

    iput-object v1, p0, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->blocks:[B

    .line 138
    :cond_1
    iget v1, p0, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->off:I

    iget-object v3, p0, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->values:[J

    array-length v3, v3

    if-ge v1, v3, :cond_2

    .line 139
    iget-object v1, p0, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->values:[J

    iget v3, p0, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->off:I

    iget-object v4, p0, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->values:[J

    array-length v4, v4

    const-wide/16 v8, 0x0

    invoke-static {v1, v3, v4, v8, v9}, Ljava/util/Arrays;->fill([JIIJ)V

    .line 141
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->values:[J

    iget-object v3, p0, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->blocks:[B

    move v4, v2

    invoke-interface/range {v0 .. v5}, Lorg/apache/lucene/util/packed/PackedInts$Encoder;->encode([JI[BII)V

    .line 142
    sget-object v1, Lorg/apache/lucene/util/packed/PackedInts$Format;->PACKED:Lorg/apache/lucene/util/packed/PackedInts$Format;

    iget v2, p0, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->off:I

    invoke-virtual {v1, v10, v2, p1}, Lorg/apache/lucene/util/packed/PackedInts$Format;->byteCount(III)J

    move-result-wide v2

    long-to-int v6, v2

    .line 143
    .local v6, "blockCount":I
    iget-object v1, p0, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->out:Lorg/apache/lucene/store/DataOutput;

    iget-object v2, p0, Lorg/apache/lucene/util/packed/AbstractBlockPackedWriter;->blocks:[B

    invoke-virtual {v1, v2, v6}, Lorg/apache/lucene/store/DataOutput;->writeBytes([BI)V

    .line 144
    return-void
.end method
