.class public interface abstract Lorg/apache/lucene/util/packed/PackedInts$Mutable;
.super Ljava/lang/Object;
.source "PackedInts.java"

# interfaces
.implements Lorg/apache/lucene/util/packed/PackedInts$Reader;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/packed/PackedInts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Mutable"
.end annotation


# virtual methods
.method public abstract clear()V
.end method

.method public abstract fill(IIJ)V
.end method

.method public abstract save(Lorg/apache/lucene/store/DataOutput;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract set(I[JII)I
.end method

.method public abstract set(IJ)V
.end method
