.class final Lorg/apache/lucene/util/WeakIdentityMap$IdentityWeakReference;
.super Ljava/lang/ref/WeakReference;
.source "WeakIdentityMap.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/WeakIdentityMap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "IdentityWeakReference"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/ref/WeakReference",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field private final hash:I


# direct methods
.method constructor <init>(Ljava/lang/Object;Ljava/lang/ref/ReferenceQueue;)V
    .locals 1
    .param p1, "obj"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/lang/ref/ReferenceQueue",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 249
    .local p2, "queue":Ljava/lang/ref/ReferenceQueue;, "Ljava/lang/ref/ReferenceQueue<Ljava/lang/Object;>;"
    if-nez p1, :cond_0

    sget-object v0, Lorg/apache/lucene/util/WeakIdentityMap;->NULL:Ljava/lang/Object;

    :goto_0
    invoke-direct {p0, v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;Ljava/lang/ref/ReferenceQueue;)V

    .line 250
    invoke-static {p1}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/util/WeakIdentityMap$IdentityWeakReference;->hash:I

    .line 251
    return-void

    :cond_0
    move-object v0, p1

    .line 249
    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    .line 260
    if-ne p0, p1, :cond_1

    .line 269
    :cond_0
    :goto_0
    return v1

    .line 263
    :cond_1
    instance-of v2, p1, Lorg/apache/lucene/util/WeakIdentityMap$IdentityWeakReference;

    if-eqz v2, :cond_2

    move-object v0, p1

    .line 264
    check-cast v0, Lorg/apache/lucene/util/WeakIdentityMap$IdentityWeakReference;

    .line 265
    .local v0, "ref":Lorg/apache/lucene/util/WeakIdentityMap$IdentityWeakReference;, "Lorg/apache/lucene/util/WeakIdentityMap$IdentityWeakReference;"
    invoke-virtual {p0}, Lorg/apache/lucene/util/WeakIdentityMap$IdentityWeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0}, Lorg/apache/lucene/util/WeakIdentityMap$IdentityWeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    if-eq v2, v3, :cond_0

    .line 269
    .end local v0    # "ref":Lorg/apache/lucene/util/WeakIdentityMap$IdentityWeakReference;, "Lorg/apache/lucene/util/WeakIdentityMap$IdentityWeakReference;"
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 255
    iget v0, p0, Lorg/apache/lucene/util/WeakIdentityMap$IdentityWeakReference;->hash:I

    return v0
.end method
