.class final Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder;
.super Ljava/lang/Object;
.source "DaciukMihovAutomatonBuilder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final comparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/CharsRef;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private previous:Lorg/apache/lucene/util/CharsRef;

.field private root:Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;

.field private stateRegistry:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;",
            "Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder;->$assertionsDisabled:Z

    .line 206
    invoke-static {}, Lorg/apache/lucene/util/CharsRef;->getUTF16SortedAsUTF8Comparator()Ljava/util/Comparator;

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder;->comparator:Ljava/util/Comparator;

    return-void

    .line 34
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>()V
    .locals 2

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 190
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder;->stateRegistry:Ljava/util/HashMap;

    .line 195
    new-instance v0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;-><init>(Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;)V

    iput-object v0, p0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder;->root:Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;

    .line 34
    return-void
.end method

.method private addSuffix(Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;Ljava/lang/CharSequence;I)V
    .locals 3
    .param p1, "state"    # Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;
    .param p2, "current"    # Ljava/lang/CharSequence;
    .param p3, "fromIndex"    # I

    .prologue
    .line 324
    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v1

    .line 325
    .local v1, "len":I
    :goto_0
    if-lt p3, v1, :cond_0

    .line 330
    const/4 v2, 0x1

    iput-boolean v2, p1, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;->is_final:Z

    .line 331
    return-void

    .line 326
    :cond_0
    invoke-static {p2, p3}, Ljava/lang/Character;->codePointAt(Ljava/lang/CharSequence;I)I

    move-result v0

    .line 327
    .local v0, "cp":I
    invoke-virtual {p1, v0}, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;->newState(I)Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;

    move-result-object p1

    .line 328
    invoke-static {v0}, Ljava/lang/Character;->charCount(I)I

    move-result v2

    add-int/2addr p3, v2

    goto :goto_0
.end method

.method public static build(Ljava/util/Collection;)Lorg/apache/lucene/util/automaton/Automaton;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;)",
            "Lorg/apache/lucene/util/automaton/Automaton;"
        }
    .end annotation

    .prologue
    .line 276
    .local p0, "input":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/util/BytesRef;>;"
    new-instance v2, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder;

    invoke-direct {v2}, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder;-><init>()V

    .line 278
    .local v2, "builder":Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder;
    new-instance v3, Lorg/apache/lucene/util/CharsRef;

    invoke-direct {v3}, Lorg/apache/lucene/util/CharsRef;-><init>()V

    .line 279
    .local v3, "scratch":Lorg/apache/lucene/util/CharsRef;
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_0

    .line 284
    new-instance v0, Lorg/apache/lucene/util/automaton/Automaton;

    invoke-direct {v0}, Lorg/apache/lucene/util/automaton/Automaton;-><init>()V

    .line 286
    .local v0, "a":Lorg/apache/lucene/util/automaton/Automaton;
    invoke-virtual {v2}, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder;->complete()Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;

    move-result-object v4

    .line 287
    new-instance v5, Ljava/util/IdentityHashMap;

    invoke-direct {v5}, Ljava/util/IdentityHashMap;-><init>()V

    .line 285
    invoke-static {v4, v5}, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder;->convert(Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;Ljava/util/IdentityHashMap;)Lorg/apache/lucene/util/automaton/State;

    move-result-object v4

    iput-object v4, v0, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    .line 288
    const/4 v4, 0x1

    iput-boolean v4, v0, Lorg/apache/lucene/util/automaton/Automaton;->deterministic:Z

    .line 289
    return-object v0

    .line 279
    .end local v0    # "a":Lorg/apache/lucene/util/automaton/Automaton;
    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/util/BytesRef;

    .line 280
    .local v1, "b":Lorg/apache/lucene/util/BytesRef;
    invoke-static {v1, v3}, Lorg/apache/lucene/util/UnicodeUtil;->UTF8toUTF16(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/CharsRef;)V

    .line 281
    invoke-virtual {v2, v3}, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder;->add(Lorg/apache/lucene/util/CharsRef;)V

    goto :goto_0
.end method

.method private static convert(Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;Ljava/util/IdentityHashMap;)Lorg/apache/lucene/util/automaton/State;
    .locals 12
    .param p0, "s"    # Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;",
            "Ljava/util/IdentityHashMap",
            "<",
            "Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;",
            "Lorg/apache/lucene/util/automaton/State;",
            ">;)",
            "Lorg/apache/lucene/util/automaton/State;"
        }
    .end annotation

    .prologue
    .line 254
    .local p1, "visited":Ljava/util/IdentityHashMap;, "Ljava/util/IdentityHashMap<Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;Lorg/apache/lucene/util/automaton/State;>;"
    invoke-virtual {p1, p0}, Ljava/util/IdentityHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/util/automaton/State;

    .line 255
    .local v0, "converted":Lorg/apache/lucene/util/automaton/State;
    if-eqz v0, :cond_0

    move-object v1, v0

    .line 268
    .end local v0    # "converted":Lorg/apache/lucene/util/automaton/State;
    .local v1, "converted":Ljava/lang/Object;
    :goto_0
    return-object v1

    .line 257
    .end local v1    # "converted":Ljava/lang/Object;
    .restart local v0    # "converted":Lorg/apache/lucene/util/automaton/State;
    :cond_0
    new-instance v0, Lorg/apache/lucene/util/automaton/State;

    .end local v0    # "converted":Lorg/apache/lucene/util/automaton/State;
    invoke-direct {v0}, Lorg/apache/lucene/util/automaton/State;-><init>()V

    .line 258
    .restart local v0    # "converted":Lorg/apache/lucene/util/automaton/State;
    iget-boolean v6, p0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;->is_final:Z

    invoke-virtual {v0, v6}, Lorg/apache/lucene/util/automaton/State;->setAccept(Z)V

    .line 260
    invoke-virtual {p1, p0, v0}, Ljava/util/IdentityHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 261
    const/4 v2, 0x0

    .line 262
    .local v2, "i":I
    iget-object v4, p0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;->labels:[I

    .line 263
    .local v4, "labels":[I
    iget-object v7, p0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;->states:[Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;

    array-length v8, v7

    const/4 v6, 0x0

    move v3, v2

    .end local v2    # "i":I
    .local v3, "i":I
    :goto_1
    if-lt v6, v8, :cond_1

    move-object v1, v0

    .line 268
    .restart local v1    # "converted":Ljava/lang/Object;
    goto :goto_0

    .line 263
    .end local v1    # "converted":Ljava/lang/Object;
    :cond_1
    aget-object v5, v7, v6

    .line 265
    .local v5, "target":Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;
    new-instance v9, Lorg/apache/lucene/util/automaton/Transition;

    add-int/lit8 v2, v3, 0x1

    .end local v3    # "i":I
    .restart local v2    # "i":I
    aget v10, v4, v3

    invoke-static {v5, p1}, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder;->convert(Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;Ljava/util/IdentityHashMap;)Lorg/apache/lucene/util/automaton/State;

    move-result-object v11

    invoke-direct {v9, v10, v11}, Lorg/apache/lucene/util/automaton/Transition;-><init>(ILorg/apache/lucene/util/automaton/State;)V

    .line 264
    invoke-virtual {v0, v9}, Lorg/apache/lucene/util/automaton/State;->addTransition(Lorg/apache/lucene/util/automaton/Transition;)V

    .line 263
    add-int/lit8 v6, v6, 0x1

    move v3, v2

    .end local v2    # "i":I
    .restart local v3    # "i":I
    goto :goto_1
.end method

.method private replaceOrRegister(Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;)V
    .locals 3
    .param p1, "state"    # Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;

    .prologue
    .line 307
    invoke-virtual {p1}, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;->lastChild()Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;

    move-result-object v0

    .line 309
    .local v0, "child":Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;
    invoke-virtual {v0}, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;->hasChildren()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-direct {p0, v0}, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder;->replaceOrRegister(Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;)V

    .line 311
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder;->stateRegistry:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;

    .line 312
    .local v1, "registered":Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;
    if-eqz v1, :cond_1

    .line 313
    invoke-virtual {p1, v1}, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;->replaceLastChild(Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;)V

    .line 317
    :goto_0
    return-void

    .line 315
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder;->stateRegistry:Ljava/util/HashMap;

    invoke-virtual {v2, v0, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private setPrevious(Lorg/apache/lucene/util/CharsRef;)Z
    .locals 1
    .param p1, "current"    # Lorg/apache/lucene/util/CharsRef;

    .prologue
    .line 298
    invoke-static {p1}, Lorg/apache/lucene/util/CharsRef;->deepCopyOf(Lorg/apache/lucene/util/CharsRef;)Lorg/apache/lucene/util/CharsRef;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder;->previous:Lorg/apache/lucene/util/CharsRef;

    .line 299
    const/4 v0, 0x1

    return v0
.end method


# virtual methods
.method public add(Lorg/apache/lucene/util/CharsRef;)V
    .locals 7
    .param p1, "current"    # Lorg/apache/lucene/util/CharsRef;

    .prologue
    .line 214
    sget-boolean v4, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    iget-object v4, p0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder;->stateRegistry:Ljava/util/HashMap;

    if-nez v4, :cond_0

    new-instance v4, Ljava/lang/AssertionError;

    const-string v5, "Automaton already built."

    invoke-direct {v4, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    .line 215
    :cond_0
    sget-boolean v4, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    iget-object v4, p0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder;->previous:Lorg/apache/lucene/util/CharsRef;

    if-eqz v4, :cond_1

    .line 216
    sget-object v4, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder;->comparator:Ljava/util/Comparator;

    iget-object v5, p0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder;->previous:Lorg/apache/lucene/util/CharsRef;

    invoke-interface {v4, v5, p1}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v4

    if-lez v4, :cond_1

    new-instance v4, Ljava/lang/AssertionError;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Input must be in sorted UTF-8 order: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 217
    iget-object v6, p0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder;->previous:Lorg/apache/lucene/util/CharsRef;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " >= "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    .line 218
    :cond_1
    sget-boolean v4, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder;->$assertionsDisabled:Z

    if-nez v4, :cond_2

    invoke-direct {p0, p1}, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder;->setPrevious(Lorg/apache/lucene/util/CharsRef;)Z

    move-result v4

    if-nez v4, :cond_2

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 221
    :cond_2
    const/4 v2, 0x0

    .local v2, "pos":I
    invoke-virtual {p1}, Lorg/apache/lucene/util/CharsRef;->length()I

    move-result v0

    .line 222
    .local v0, "max":I
    iget-object v3, p0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder;->root:Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;

    .line 223
    .local v3, "state":Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;
    :goto_0
    if-ge v2, v0, :cond_3

    invoke-static {p1, v2}, Ljava/lang/Character;->codePointAt(Ljava/lang/CharSequence;I)I

    move-result v4

    invoke-virtual {v3, v4}, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;->lastChild(I)Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;

    move-result-object v1

    .local v1, "next":Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;
    if-nez v1, :cond_5

    .line 229
    .end local v1    # "next":Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;
    :cond_3
    invoke-virtual {v3}, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;->hasChildren()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-direct {p0, v3}, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder;->replaceOrRegister(Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;)V

    .line 231
    :cond_4
    invoke-direct {p0, v3, p1, v2}, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder;->addSuffix(Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;Ljava/lang/CharSequence;I)V

    .line 232
    return-void

    .line 224
    .restart local v1    # "next":Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;
    :cond_5
    move-object v3, v1

    .line 226
    invoke-static {p1, v2}, Ljava/lang/Character;->codePointAt(Ljava/lang/CharSequence;I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Character;->charCount(I)I

    move-result v4

    add-int/2addr v2, v4

    goto :goto_0
.end method

.method public complete()Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;
    .locals 1

    .prologue
    .line 241
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder;->stateRegistry:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 243
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder;->root:Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;

    invoke-virtual {v0}, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;->hasChildren()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder;->root:Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;

    invoke-direct {p0, v0}, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder;->replaceOrRegister(Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;)V

    .line 245
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder;->stateRegistry:Ljava/util/HashMap;

    .line 246
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder;->root:Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;

    return-object v0
.end method
