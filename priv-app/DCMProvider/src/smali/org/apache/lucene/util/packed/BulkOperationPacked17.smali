.class final Lorg/apache/lucene/util/packed/BulkOperationPacked17;
.super Lorg/apache/lucene/util/packed/BulkOperationPacked;
.source "BulkOperationPacked17.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    const/16 v0, 0x11

    invoke-direct {p0, v0}, Lorg/apache/lucene/util/packed/BulkOperationPacked;-><init>(I)V

    .line 29
    return-void
.end method


# virtual methods
.method public decode([BI[III)V
    .locals 24
    .param p1, "blocks"    # [B
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [I
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 120
    const/16 v20, 0x0

    .local v20, "i":I
    move/from16 v21, p4

    .end local p4    # "valuesOffset":I
    .local v21, "valuesOffset":I
    move/from16 v2, p2

    .end local p2    # "blocksOffset":I
    .local v2, "blocksOffset":I
    :goto_0
    move/from16 v0, v20

    move/from16 v1, p5

    if-lt v0, v1, :cond_0

    .line 147
    return-void

    .line 121
    :cond_0
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v22, p1, v2

    move/from16 v0, v22

    and-int/lit16 v3, v0, 0xff

    .line 122
    .local v3, "byte0":I
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v22, p1, p2

    move/from16 v0, v22

    and-int/lit16 v4, v0, 0xff

    .line 123
    .local v4, "byte1":I
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v22, p1, v2

    move/from16 v0, v22

    and-int/lit16 v12, v0, 0xff

    .line 124
    .local v12, "byte2":I
    add-int/lit8 p4, v21, 0x1

    .end local v21    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    shl-int/lit8 v22, v3, 0x9

    shl-int/lit8 v23, v4, 0x1

    or-int v22, v22, v23

    ushr-int/lit8 v23, v12, 0x7

    or-int v22, v22, v23

    aput v22, p3, v21

    .line 125
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v22, p1, p2

    move/from16 v0, v22

    and-int/lit16 v13, v0, 0xff

    .line 126
    .local v13, "byte3":I
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v22, p1, v2

    move/from16 v0, v22

    and-int/lit16 v14, v0, 0xff

    .line 127
    .local v14, "byte4":I
    add-int/lit8 v21, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v21    # "valuesOffset":I
    and-int/lit8 v22, v12, 0x7f

    shl-int/lit8 v22, v22, 0xa

    shl-int/lit8 v23, v13, 0x2

    or-int v22, v22, v23

    ushr-int/lit8 v23, v14, 0x6

    or-int v22, v22, v23

    aput v22, p3, p4

    .line 128
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v22, p1, p2

    move/from16 v0, v22

    and-int/lit16 v15, v0, 0xff

    .line 129
    .local v15, "byte5":I
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v22, p1, v2

    move/from16 v0, v22

    and-int/lit16 v0, v0, 0xff

    move/from16 v16, v0

    .line 130
    .local v16, "byte6":I
    add-int/lit8 p4, v21, 0x1

    .end local v21    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    and-int/lit8 v22, v14, 0x3f

    shl-int/lit8 v22, v22, 0xb

    shl-int/lit8 v23, v15, 0x3

    or-int v22, v22, v23

    ushr-int/lit8 v23, v16, 0x5

    or-int v22, v22, v23

    aput v22, p3, v21

    .line 131
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v22, p1, p2

    move/from16 v0, v22

    and-int/lit16 v0, v0, 0xff

    move/from16 v17, v0

    .line 132
    .local v17, "byte7":I
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v22, p1, v2

    move/from16 v0, v22

    and-int/lit16 v0, v0, 0xff

    move/from16 v18, v0

    .line 133
    .local v18, "byte8":I
    add-int/lit8 v21, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v21    # "valuesOffset":I
    and-int/lit8 v22, v16, 0x1f

    shl-int/lit8 v22, v22, 0xc

    shl-int/lit8 v23, v17, 0x4

    or-int v22, v22, v23

    ushr-int/lit8 v23, v18, 0x4

    or-int v22, v22, v23

    aput v22, p3, p4

    .line 134
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v22, p1, p2

    move/from16 v0, v22

    and-int/lit16 v0, v0, 0xff

    move/from16 v19, v0

    .line 135
    .local v19, "byte9":I
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v22, p1, v2

    move/from16 v0, v22

    and-int/lit16 v5, v0, 0xff

    .line 136
    .local v5, "byte10":I
    add-int/lit8 p4, v21, 0x1

    .end local v21    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    and-int/lit8 v22, v18, 0xf

    shl-int/lit8 v22, v22, 0xd

    shl-int/lit8 v23, v19, 0x5

    or-int v22, v22, v23

    ushr-int/lit8 v23, v5, 0x3

    or-int v22, v22, v23

    aput v22, p3, v21

    .line 137
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v22, p1, p2

    move/from16 v0, v22

    and-int/lit16 v6, v0, 0xff

    .line 138
    .local v6, "byte11":I
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v22, p1, v2

    move/from16 v0, v22

    and-int/lit16 v7, v0, 0xff

    .line 139
    .local v7, "byte12":I
    add-int/lit8 v21, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v21    # "valuesOffset":I
    and-int/lit8 v22, v5, 0x7

    shl-int/lit8 v22, v22, 0xe

    shl-int/lit8 v23, v6, 0x6

    or-int v22, v22, v23

    ushr-int/lit8 v23, v7, 0x2

    or-int v22, v22, v23

    aput v22, p3, p4

    .line 140
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v22, p1, p2

    move/from16 v0, v22

    and-int/lit16 v8, v0, 0xff

    .line 141
    .local v8, "byte13":I
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v22, p1, v2

    move/from16 v0, v22

    and-int/lit16 v9, v0, 0xff

    .line 142
    .local v9, "byte14":I
    add-int/lit8 p4, v21, 0x1

    .end local v21    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    and-int/lit8 v22, v7, 0x3

    shl-int/lit8 v22, v22, 0xf

    shl-int/lit8 v23, v8, 0x7

    or-int v22, v22, v23

    ushr-int/lit8 v23, v9, 0x1

    or-int v22, v22, v23

    aput v22, p3, v21

    .line 143
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v22, p1, p2

    move/from16 v0, v22

    and-int/lit16 v10, v0, 0xff

    .line 144
    .local v10, "byte15":I
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v22, p1, v2

    move/from16 v0, v22

    and-int/lit16 v11, v0, 0xff

    .line 145
    .local v11, "byte16":I
    add-int/lit8 v21, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v21    # "valuesOffset":I
    and-int/lit8 v22, v9, 0x1

    shl-int/lit8 v22, v22, 0x10

    shl-int/lit8 v23, v10, 0x8

    or-int v22, v22, v23

    or-int v22, v22, v11

    aput v22, p3, p4

    .line 120
    add-int/lit8 v20, v20, 0x1

    move/from16 v2, p2

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    goto/16 :goto_0
.end method

.method public decode([BI[JII)V
    .locals 44
    .param p1, "blocks"    # [B
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [J
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 238
    const/4 v3, 0x0

    .local v3, "i":I
    move/from16 v38, p4

    .end local p4    # "valuesOffset":I
    .local v38, "valuesOffset":I
    move/from16 v2, p2

    .end local p2    # "blocksOffset":I
    .local v2, "blocksOffset":I
    :goto_0
    move/from16 v0, p5

    if-lt v3, v0, :cond_0

    .line 265
    return-void

    .line 239
    :cond_0
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v39, p1, v2

    move/from16 v0, v39

    and-int/lit16 v0, v0, 0xff

    move/from16 v39, v0

    move/from16 v0, v39

    int-to-long v4, v0

    .line 240
    .local v4, "byte0":J
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v39, p1, p2

    move/from16 v0, v39

    and-int/lit16 v0, v0, 0xff

    move/from16 v39, v0

    move/from16 v0, v39

    int-to-long v6, v0

    .line 241
    .local v6, "byte1":J
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v39, p1, v2

    move/from16 v0, v39

    and-int/lit16 v0, v0, 0xff

    move/from16 v39, v0

    move/from16 v0, v39

    int-to-long v0, v0

    move-wide/from16 v22, v0

    .line 242
    .local v22, "byte2":J
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v39, 0x9

    shl-long v40, v4, v39

    const/16 v39, 0x1

    shl-long v42, v6, v39

    or-long v40, v40, v42

    const/16 v39, 0x7

    ushr-long v42, v22, v39

    or-long v40, v40, v42

    aput-wide v40, p3, v38

    .line 243
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v39, p1, p2

    move/from16 v0, v39

    and-int/lit16 v0, v0, 0xff

    move/from16 v39, v0

    move/from16 v0, v39

    int-to-long v0, v0

    move-wide/from16 v24, v0

    .line 244
    .local v24, "byte3":J
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v39, p1, v2

    move/from16 v0, v39

    and-int/lit16 v0, v0, 0xff

    move/from16 v39, v0

    move/from16 v0, v39

    int-to-long v0, v0

    move-wide/from16 v26, v0

    .line 245
    .local v26, "byte4":J
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const-wide/16 v40, 0x7f

    and-long v40, v40, v22

    const/16 v39, 0xa

    shl-long v40, v40, v39

    const/16 v39, 0x2

    shl-long v42, v24, v39

    or-long v40, v40, v42

    const/16 v39, 0x6

    ushr-long v42, v26, v39

    or-long v40, v40, v42

    aput-wide v40, p3, p4

    .line 246
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v39, p1, p2

    move/from16 v0, v39

    and-int/lit16 v0, v0, 0xff

    move/from16 v39, v0

    move/from16 v0, v39

    int-to-long v0, v0

    move-wide/from16 v28, v0

    .line 247
    .local v28, "byte5":J
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v39, p1, v2

    move/from16 v0, v39

    and-int/lit16 v0, v0, 0xff

    move/from16 v39, v0

    move/from16 v0, v39

    int-to-long v0, v0

    move-wide/from16 v30, v0

    .line 248
    .local v30, "byte6":J
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v40, 0x3f

    and-long v40, v40, v26

    const/16 v39, 0xb

    shl-long v40, v40, v39

    const/16 v39, 0x3

    shl-long v42, v28, v39

    or-long v40, v40, v42

    const/16 v39, 0x5

    ushr-long v42, v30, v39

    or-long v40, v40, v42

    aput-wide v40, p3, v38

    .line 249
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v39, p1, p2

    move/from16 v0, v39

    and-int/lit16 v0, v0, 0xff

    move/from16 v39, v0

    move/from16 v0, v39

    int-to-long v0, v0

    move-wide/from16 v32, v0

    .line 250
    .local v32, "byte7":J
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v39, p1, v2

    move/from16 v0, v39

    and-int/lit16 v0, v0, 0xff

    move/from16 v39, v0

    move/from16 v0, v39

    int-to-long v0, v0

    move-wide/from16 v34, v0

    .line 251
    .local v34, "byte8":J
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const-wide/16 v40, 0x1f

    and-long v40, v40, v30

    const/16 v39, 0xc

    shl-long v40, v40, v39

    const/16 v39, 0x4

    shl-long v42, v32, v39

    or-long v40, v40, v42

    const/16 v39, 0x4

    ushr-long v42, v34, v39

    or-long v40, v40, v42

    aput-wide v40, p3, p4

    .line 252
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v39, p1, p2

    move/from16 v0, v39

    and-int/lit16 v0, v0, 0xff

    move/from16 v39, v0

    move/from16 v0, v39

    int-to-long v0, v0

    move-wide/from16 v36, v0

    .line 253
    .local v36, "byte9":J
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v39, p1, v2

    move/from16 v0, v39

    and-int/lit16 v0, v0, 0xff

    move/from16 v39, v0

    move/from16 v0, v39

    int-to-long v8, v0

    .line 254
    .local v8, "byte10":J
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v40, 0xf

    and-long v40, v40, v34

    const/16 v39, 0xd

    shl-long v40, v40, v39

    const/16 v39, 0x5

    shl-long v42, v36, v39

    or-long v40, v40, v42

    const/16 v39, 0x3

    ushr-long v42, v8, v39

    or-long v40, v40, v42

    aput-wide v40, p3, v38

    .line 255
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v39, p1, p2

    move/from16 v0, v39

    and-int/lit16 v0, v0, 0xff

    move/from16 v39, v0

    move/from16 v0, v39

    int-to-long v10, v0

    .line 256
    .local v10, "byte11":J
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v39, p1, v2

    move/from16 v0, v39

    and-int/lit16 v0, v0, 0xff

    move/from16 v39, v0

    move/from16 v0, v39

    int-to-long v12, v0

    .line 257
    .local v12, "byte12":J
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const-wide/16 v40, 0x7

    and-long v40, v40, v8

    const/16 v39, 0xe

    shl-long v40, v40, v39

    const/16 v39, 0x6

    shl-long v42, v10, v39

    or-long v40, v40, v42

    const/16 v39, 0x2

    ushr-long v42, v12, v39

    or-long v40, v40, v42

    aput-wide v40, p3, p4

    .line 258
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v39, p1, p2

    move/from16 v0, v39

    and-int/lit16 v0, v0, 0xff

    move/from16 v39, v0

    move/from16 v0, v39

    int-to-long v14, v0

    .line 259
    .local v14, "byte13":J
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v39, p1, v2

    move/from16 v0, v39

    and-int/lit16 v0, v0, 0xff

    move/from16 v39, v0

    move/from16 v0, v39

    int-to-long v0, v0

    move-wide/from16 v16, v0

    .line 260
    .local v16, "byte14":J
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v40, 0x3

    and-long v40, v40, v12

    const/16 v39, 0xf

    shl-long v40, v40, v39

    const/16 v39, 0x7

    shl-long v42, v14, v39

    or-long v40, v40, v42

    const/16 v39, 0x1

    ushr-long v42, v16, v39

    or-long v40, v40, v42

    aput-wide v40, p3, v38

    .line 261
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v39, p1, p2

    move/from16 v0, v39

    and-int/lit16 v0, v0, 0xff

    move/from16 v39, v0

    move/from16 v0, v39

    int-to-long v0, v0

    move-wide/from16 v18, v0

    .line 262
    .local v18, "byte15":J
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v39, p1, v2

    move/from16 v0, v39

    and-int/lit16 v0, v0, 0xff

    move/from16 v39, v0

    move/from16 v0, v39

    int-to-long v0, v0

    move-wide/from16 v20, v0

    .line 263
    .local v20, "byte16":J
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const-wide/16 v40, 0x1

    and-long v40, v40, v16

    const/16 v39, 0x10

    shl-long v40, v40, v39

    const/16 v39, 0x8

    shl-long v42, v18, v39

    or-long v40, v40, v42

    or-long v40, v40, v20

    aput-wide v40, p3, p4

    .line 238
    add-int/lit8 v3, v3, 0x1

    move/from16 v2, p2

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    goto/16 :goto_0
.end method

.method public decode([JI[III)V
    .locals 44
    .param p1, "blocks"    # [J
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [I
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 33
    const/16 v37, 0x0

    .local v37, "i":I
    move/from16 v38, p4

    .end local p4    # "valuesOffset":I
    .local v38, "valuesOffset":I
    move/from16 v36, p2

    .end local p2    # "blocksOffset":I
    .local v36, "blocksOffset":I
    :goto_0
    move/from16 v0, v37

    move/from16 v1, p5

    if-lt v0, v1, :cond_0

    .line 116
    return-void

    .line 34
    :cond_0
    add-int/lit8 p2, v36, 0x1

    .end local v36    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v2, p1, v36

    .line 35
    .local v2, "block0":J
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v39, 0x2f

    ushr-long v40, v2, v39

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, v38

    .line 36
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const/16 v39, 0x1e

    ushr-long v40, v2, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, p4

    .line 37
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v39, 0xd

    ushr-long v40, v2, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, v38

    .line 38
    add-int/lit8 v36, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v36    # "blocksOffset":I
    aget-wide v4, p1, p2

    .line 39
    .local v4, "block1":J
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const-wide/16 v40, 0x1fff

    and-long v40, v40, v2

    const/16 v39, 0x4

    shl-long v40, v40, v39

    const/16 v39, 0x3c

    ushr-long v42, v4, v39

    or-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, p4

    .line 40
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v39, 0x2b

    ushr-long v40, v4, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, v38

    .line 41
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const/16 v39, 0x1a

    ushr-long v40, v4, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, p4

    .line 42
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v39, 0x9

    ushr-long v40, v4, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, v38

    .line 43
    add-int/lit8 p2, v36, 0x1

    .end local v36    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v20, p1, v36

    .line 44
    .local v20, "block2":J
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const-wide/16 v40, 0x1ff

    and-long v40, v40, v4

    const/16 v39, 0x8

    shl-long v40, v40, v39

    const/16 v39, 0x38

    ushr-long v42, v20, v39

    or-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, p4

    .line 45
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v39, 0x27

    ushr-long v40, v20, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, v38

    .line 46
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const/16 v39, 0x16

    ushr-long v40, v20, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, p4

    .line 47
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v39, 0x5

    ushr-long v40, v20, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, v38

    .line 48
    add-int/lit8 v36, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v36    # "blocksOffset":I
    aget-wide v22, p1, p2

    .line 49
    .local v22, "block3":J
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const-wide/16 v40, 0x1f

    and-long v40, v40, v20

    const/16 v39, 0xc

    shl-long v40, v40, v39

    const/16 v39, 0x34

    ushr-long v42, v22, v39

    or-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, p4

    .line 50
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v39, 0x23

    ushr-long v40, v22, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, v38

    .line 51
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const/16 v39, 0x12

    ushr-long v40, v22, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, p4

    .line 52
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v39, 0x1

    ushr-long v40, v22, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, v38

    .line 53
    add-int/lit8 p2, v36, 0x1

    .end local v36    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v24, p1, v36

    .line 54
    .local v24, "block4":J
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const-wide/16 v40, 0x1

    and-long v40, v40, v22

    const/16 v39, 0x10

    shl-long v40, v40, v39

    const/16 v39, 0x30

    ushr-long v42, v24, v39

    or-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, p4

    .line 55
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v39, 0x1f

    ushr-long v40, v24, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, v38

    .line 56
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const/16 v39, 0xe

    ushr-long v40, v24, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, p4

    .line 57
    add-int/lit8 v36, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v36    # "blocksOffset":I
    aget-wide v26, p1, p2

    .line 58
    .local v26, "block5":J
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v40, 0x3fff

    and-long v40, v40, v24

    const/16 v39, 0x3

    shl-long v40, v40, v39

    const/16 v39, 0x3d

    ushr-long v42, v26, v39

    or-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, v38

    .line 59
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const/16 v39, 0x2c

    ushr-long v40, v26, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, p4

    .line 60
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v39, 0x1b

    ushr-long v40, v26, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, v38

    .line 61
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const/16 v39, 0xa

    ushr-long v40, v26, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, p4

    .line 62
    add-int/lit8 p2, v36, 0x1

    .end local v36    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v28, p1, v36

    .line 63
    .local v28, "block6":J
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v40, 0x3ff

    and-long v40, v40, v26

    const/16 v39, 0x7

    shl-long v40, v40, v39

    const/16 v39, 0x39

    ushr-long v42, v28, v39

    or-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, v38

    .line 64
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const/16 v39, 0x28

    ushr-long v40, v28, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, p4

    .line 65
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v39, 0x17

    ushr-long v40, v28, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, v38

    .line 66
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const/16 v39, 0x6

    ushr-long v40, v28, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, p4

    .line 67
    add-int/lit8 v36, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v36    # "blocksOffset":I
    aget-wide v30, p1, p2

    .line 68
    .local v30, "block7":J
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v40, 0x3f

    and-long v40, v40, v28

    const/16 v39, 0xb

    shl-long v40, v40, v39

    const/16 v39, 0x35

    ushr-long v42, v30, v39

    or-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, v38

    .line 69
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const/16 v39, 0x24

    ushr-long v40, v30, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, p4

    .line 70
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v39, 0x13

    ushr-long v40, v30, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, v38

    .line 71
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const/16 v39, 0x2

    ushr-long v40, v30, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, p4

    .line 72
    add-int/lit8 p2, v36, 0x1

    .end local v36    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v32, p1, v36

    .line 73
    .local v32, "block8":J
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v40, 0x3

    and-long v40, v40, v30

    const/16 v39, 0xf

    shl-long v40, v40, v39

    const/16 v39, 0x31

    ushr-long v42, v32, v39

    or-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, v38

    .line 74
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const/16 v39, 0x20

    ushr-long v40, v32, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, p4

    .line 75
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v39, 0xf

    ushr-long v40, v32, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, v38

    .line 76
    add-int/lit8 v36, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v36    # "blocksOffset":I
    aget-wide v34, p1, p2

    .line 77
    .local v34, "block9":J
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const-wide/16 v40, 0x7fff

    and-long v40, v40, v32

    const/16 v39, 0x2

    shl-long v40, v40, v39

    const/16 v39, 0x3e

    ushr-long v42, v34, v39

    or-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, p4

    .line 78
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v39, 0x2d

    ushr-long v40, v34, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, v38

    .line 79
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const/16 v39, 0x1c

    ushr-long v40, v34, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, p4

    .line 80
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v39, 0xb

    ushr-long v40, v34, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, v38

    .line 81
    add-int/lit8 p2, v36, 0x1

    .end local v36    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v6, p1, v36

    .line 82
    .local v6, "block10":J
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const-wide/16 v40, 0x7ff

    and-long v40, v40, v34

    const/16 v39, 0x6

    shl-long v40, v40, v39

    const/16 v39, 0x3a

    ushr-long v42, v6, v39

    or-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, p4

    .line 83
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v39, 0x29

    ushr-long v40, v6, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, v38

    .line 84
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const/16 v39, 0x18

    ushr-long v40, v6, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, p4

    .line 85
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v39, 0x7

    ushr-long v40, v6, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, v38

    .line 86
    add-int/lit8 v36, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v36    # "blocksOffset":I
    aget-wide v8, p1, p2

    .line 87
    .local v8, "block11":J
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const-wide/16 v40, 0x7f

    and-long v40, v40, v6

    const/16 v39, 0xa

    shl-long v40, v40, v39

    const/16 v39, 0x36

    ushr-long v42, v8, v39

    or-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, p4

    .line 88
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v39, 0x25

    ushr-long v40, v8, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, v38

    .line 89
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const/16 v39, 0x14

    ushr-long v40, v8, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, p4

    .line 90
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v39, 0x3

    ushr-long v40, v8, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, v38

    .line 91
    add-int/lit8 p2, v36, 0x1

    .end local v36    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v10, p1, v36

    .line 92
    .local v10, "block12":J
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const-wide/16 v40, 0x7

    and-long v40, v40, v8

    const/16 v39, 0xe

    shl-long v40, v40, v39

    const/16 v39, 0x32

    ushr-long v42, v10, v39

    or-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, p4

    .line 93
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v39, 0x21

    ushr-long v40, v10, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, v38

    .line 94
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const/16 v39, 0x10

    ushr-long v40, v10, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, p4

    .line 95
    add-int/lit8 v36, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v36    # "blocksOffset":I
    aget-wide v12, p1, p2

    .line 96
    .local v12, "block13":J
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/32 v40, 0xffff

    and-long v40, v40, v10

    const/16 v39, 0x1

    shl-long v40, v40, v39

    const/16 v39, 0x3f

    ushr-long v42, v12, v39

    or-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, v38

    .line 97
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const/16 v39, 0x2e

    ushr-long v40, v12, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, p4

    .line 98
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v39, 0x1d

    ushr-long v40, v12, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, v38

    .line 99
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const/16 v39, 0xc

    ushr-long v40, v12, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, p4

    .line 100
    add-int/lit8 p2, v36, 0x1

    .end local v36    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v14, p1, v36

    .line 101
    .local v14, "block14":J
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v40, 0xfff

    and-long v40, v40, v12

    const/16 v39, 0x5

    shl-long v40, v40, v39

    const/16 v39, 0x3b

    ushr-long v42, v14, v39

    or-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, v38

    .line 102
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const/16 v39, 0x2a

    ushr-long v40, v14, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, p4

    .line 103
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v39, 0x19

    ushr-long v40, v14, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, v38

    .line 104
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const/16 v39, 0x8

    ushr-long v40, v14, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, p4

    .line 105
    add-int/lit8 v36, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v36    # "blocksOffset":I
    aget-wide v16, p1, p2

    .line 106
    .local v16, "block15":J
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v40, 0xff

    and-long v40, v40, v14

    const/16 v39, 0x9

    shl-long v40, v40, v39

    const/16 v39, 0x37

    ushr-long v42, v16, v39

    or-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, v38

    .line 107
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const/16 v39, 0x26

    ushr-long v40, v16, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, p4

    .line 108
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v39, 0x15

    ushr-long v40, v16, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, v38

    .line 109
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const/16 v39, 0x4

    ushr-long v40, v16, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, p4

    .line 110
    add-int/lit8 p2, v36, 0x1

    .end local v36    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v18, p1, v36

    .line 111
    .local v18, "block16":J
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v40, 0xf

    and-long v40, v40, v16

    const/16 v39, 0xd

    shl-long v40, v40, v39

    const/16 v39, 0x33

    ushr-long v42, v18, v39

    or-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, v38

    .line 112
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const/16 v39, 0x22

    ushr-long v40, v18, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, p4

    .line 113
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v39, 0x11

    ushr-long v40, v18, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, v38

    .line 114
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const-wide/32 v40, 0x1ffff

    and-long v40, v40, v18

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v39, v0

    aput v39, p3, p4

    .line 33
    add-int/lit8 v37, v37, 0x1

    move/from16 v36, p2

    .end local p2    # "blocksOffset":I
    .restart local v36    # "blocksOffset":I
    goto/16 :goto_0
.end method

.method public decode([JI[JII)V
    .locals 44
    .param p1, "blocks"    # [J
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [J
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 151
    const/16 v37, 0x0

    .local v37, "i":I
    move/from16 v38, p4

    .end local p4    # "valuesOffset":I
    .local v38, "valuesOffset":I
    move/from16 v36, p2

    .end local p2    # "blocksOffset":I
    .local v36, "blocksOffset":I
    :goto_0
    move/from16 v0, v37

    move/from16 v1, p5

    if-lt v0, v1, :cond_0

    .line 234
    return-void

    .line 152
    :cond_0
    add-int/lit8 p2, v36, 0x1

    .end local v36    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v2, p1, v36

    .line 153
    .local v2, "block0":J
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v39, 0x2f

    ushr-long v40, v2, v39

    aput-wide v40, p3, v38

    .line 154
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const/16 v39, 0x1e

    ushr-long v40, v2, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    aput-wide v40, p3, p4

    .line 155
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v39, 0xd

    ushr-long v40, v2, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    aput-wide v40, p3, v38

    .line 156
    add-int/lit8 v36, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v36    # "blocksOffset":I
    aget-wide v4, p1, p2

    .line 157
    .local v4, "block1":J
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const-wide/16 v40, 0x1fff

    and-long v40, v40, v2

    const/16 v39, 0x4

    shl-long v40, v40, v39

    const/16 v39, 0x3c

    ushr-long v42, v4, v39

    or-long v40, v40, v42

    aput-wide v40, p3, p4

    .line 158
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v39, 0x2b

    ushr-long v40, v4, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    aput-wide v40, p3, v38

    .line 159
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const/16 v39, 0x1a

    ushr-long v40, v4, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    aput-wide v40, p3, p4

    .line 160
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v39, 0x9

    ushr-long v40, v4, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    aput-wide v40, p3, v38

    .line 161
    add-int/lit8 p2, v36, 0x1

    .end local v36    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v20, p1, v36

    .line 162
    .local v20, "block2":J
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const-wide/16 v40, 0x1ff

    and-long v40, v40, v4

    const/16 v39, 0x8

    shl-long v40, v40, v39

    const/16 v39, 0x38

    ushr-long v42, v20, v39

    or-long v40, v40, v42

    aput-wide v40, p3, p4

    .line 163
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v39, 0x27

    ushr-long v40, v20, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    aput-wide v40, p3, v38

    .line 164
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const/16 v39, 0x16

    ushr-long v40, v20, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    aput-wide v40, p3, p4

    .line 165
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v39, 0x5

    ushr-long v40, v20, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    aput-wide v40, p3, v38

    .line 166
    add-int/lit8 v36, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v36    # "blocksOffset":I
    aget-wide v22, p1, p2

    .line 167
    .local v22, "block3":J
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const-wide/16 v40, 0x1f

    and-long v40, v40, v20

    const/16 v39, 0xc

    shl-long v40, v40, v39

    const/16 v39, 0x34

    ushr-long v42, v22, v39

    or-long v40, v40, v42

    aput-wide v40, p3, p4

    .line 168
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v39, 0x23

    ushr-long v40, v22, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    aput-wide v40, p3, v38

    .line 169
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const/16 v39, 0x12

    ushr-long v40, v22, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    aput-wide v40, p3, p4

    .line 170
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v39, 0x1

    ushr-long v40, v22, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    aput-wide v40, p3, v38

    .line 171
    add-int/lit8 p2, v36, 0x1

    .end local v36    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v24, p1, v36

    .line 172
    .local v24, "block4":J
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const-wide/16 v40, 0x1

    and-long v40, v40, v22

    const/16 v39, 0x10

    shl-long v40, v40, v39

    const/16 v39, 0x30

    ushr-long v42, v24, v39

    or-long v40, v40, v42

    aput-wide v40, p3, p4

    .line 173
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v39, 0x1f

    ushr-long v40, v24, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    aput-wide v40, p3, v38

    .line 174
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const/16 v39, 0xe

    ushr-long v40, v24, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    aput-wide v40, p3, p4

    .line 175
    add-int/lit8 v36, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v36    # "blocksOffset":I
    aget-wide v26, p1, p2

    .line 176
    .local v26, "block5":J
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v40, 0x3fff

    and-long v40, v40, v24

    const/16 v39, 0x3

    shl-long v40, v40, v39

    const/16 v39, 0x3d

    ushr-long v42, v26, v39

    or-long v40, v40, v42

    aput-wide v40, p3, v38

    .line 177
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const/16 v39, 0x2c

    ushr-long v40, v26, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    aput-wide v40, p3, p4

    .line 178
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v39, 0x1b

    ushr-long v40, v26, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    aput-wide v40, p3, v38

    .line 179
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const/16 v39, 0xa

    ushr-long v40, v26, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    aput-wide v40, p3, p4

    .line 180
    add-int/lit8 p2, v36, 0x1

    .end local v36    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v28, p1, v36

    .line 181
    .local v28, "block6":J
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v40, 0x3ff

    and-long v40, v40, v26

    const/16 v39, 0x7

    shl-long v40, v40, v39

    const/16 v39, 0x39

    ushr-long v42, v28, v39

    or-long v40, v40, v42

    aput-wide v40, p3, v38

    .line 182
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const/16 v39, 0x28

    ushr-long v40, v28, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    aput-wide v40, p3, p4

    .line 183
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v39, 0x17

    ushr-long v40, v28, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    aput-wide v40, p3, v38

    .line 184
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const/16 v39, 0x6

    ushr-long v40, v28, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    aput-wide v40, p3, p4

    .line 185
    add-int/lit8 v36, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v36    # "blocksOffset":I
    aget-wide v30, p1, p2

    .line 186
    .local v30, "block7":J
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v40, 0x3f

    and-long v40, v40, v28

    const/16 v39, 0xb

    shl-long v40, v40, v39

    const/16 v39, 0x35

    ushr-long v42, v30, v39

    or-long v40, v40, v42

    aput-wide v40, p3, v38

    .line 187
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const/16 v39, 0x24

    ushr-long v40, v30, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    aput-wide v40, p3, p4

    .line 188
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v39, 0x13

    ushr-long v40, v30, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    aput-wide v40, p3, v38

    .line 189
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const/16 v39, 0x2

    ushr-long v40, v30, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    aput-wide v40, p3, p4

    .line 190
    add-int/lit8 p2, v36, 0x1

    .end local v36    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v32, p1, v36

    .line 191
    .local v32, "block8":J
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v40, 0x3

    and-long v40, v40, v30

    const/16 v39, 0xf

    shl-long v40, v40, v39

    const/16 v39, 0x31

    ushr-long v42, v32, v39

    or-long v40, v40, v42

    aput-wide v40, p3, v38

    .line 192
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const/16 v39, 0x20

    ushr-long v40, v32, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    aput-wide v40, p3, p4

    .line 193
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v39, 0xf

    ushr-long v40, v32, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    aput-wide v40, p3, v38

    .line 194
    add-int/lit8 v36, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v36    # "blocksOffset":I
    aget-wide v34, p1, p2

    .line 195
    .local v34, "block9":J
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const-wide/16 v40, 0x7fff

    and-long v40, v40, v32

    const/16 v39, 0x2

    shl-long v40, v40, v39

    const/16 v39, 0x3e

    ushr-long v42, v34, v39

    or-long v40, v40, v42

    aput-wide v40, p3, p4

    .line 196
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v39, 0x2d

    ushr-long v40, v34, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    aput-wide v40, p3, v38

    .line 197
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const/16 v39, 0x1c

    ushr-long v40, v34, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    aput-wide v40, p3, p4

    .line 198
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v39, 0xb

    ushr-long v40, v34, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    aput-wide v40, p3, v38

    .line 199
    add-int/lit8 p2, v36, 0x1

    .end local v36    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v6, p1, v36

    .line 200
    .local v6, "block10":J
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const-wide/16 v40, 0x7ff

    and-long v40, v40, v34

    const/16 v39, 0x6

    shl-long v40, v40, v39

    const/16 v39, 0x3a

    ushr-long v42, v6, v39

    or-long v40, v40, v42

    aput-wide v40, p3, p4

    .line 201
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v39, 0x29

    ushr-long v40, v6, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    aput-wide v40, p3, v38

    .line 202
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const/16 v39, 0x18

    ushr-long v40, v6, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    aput-wide v40, p3, p4

    .line 203
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v39, 0x7

    ushr-long v40, v6, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    aput-wide v40, p3, v38

    .line 204
    add-int/lit8 v36, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v36    # "blocksOffset":I
    aget-wide v8, p1, p2

    .line 205
    .local v8, "block11":J
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const-wide/16 v40, 0x7f

    and-long v40, v40, v6

    const/16 v39, 0xa

    shl-long v40, v40, v39

    const/16 v39, 0x36

    ushr-long v42, v8, v39

    or-long v40, v40, v42

    aput-wide v40, p3, p4

    .line 206
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v39, 0x25

    ushr-long v40, v8, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    aput-wide v40, p3, v38

    .line 207
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const/16 v39, 0x14

    ushr-long v40, v8, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    aput-wide v40, p3, p4

    .line 208
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v39, 0x3

    ushr-long v40, v8, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    aput-wide v40, p3, v38

    .line 209
    add-int/lit8 p2, v36, 0x1

    .end local v36    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v10, p1, v36

    .line 210
    .local v10, "block12":J
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const-wide/16 v40, 0x7

    and-long v40, v40, v8

    const/16 v39, 0xe

    shl-long v40, v40, v39

    const/16 v39, 0x32

    ushr-long v42, v10, v39

    or-long v40, v40, v42

    aput-wide v40, p3, p4

    .line 211
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v39, 0x21

    ushr-long v40, v10, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    aput-wide v40, p3, v38

    .line 212
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const/16 v39, 0x10

    ushr-long v40, v10, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    aput-wide v40, p3, p4

    .line 213
    add-int/lit8 v36, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v36    # "blocksOffset":I
    aget-wide v12, p1, p2

    .line 214
    .local v12, "block13":J
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/32 v40, 0xffff

    and-long v40, v40, v10

    const/16 v39, 0x1

    shl-long v40, v40, v39

    const/16 v39, 0x3f

    ushr-long v42, v12, v39

    or-long v40, v40, v42

    aput-wide v40, p3, v38

    .line 215
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const/16 v39, 0x2e

    ushr-long v40, v12, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    aput-wide v40, p3, p4

    .line 216
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v39, 0x1d

    ushr-long v40, v12, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    aput-wide v40, p3, v38

    .line 217
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const/16 v39, 0xc

    ushr-long v40, v12, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    aput-wide v40, p3, p4

    .line 218
    add-int/lit8 p2, v36, 0x1

    .end local v36    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v14, p1, v36

    .line 219
    .local v14, "block14":J
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v40, 0xfff

    and-long v40, v40, v12

    const/16 v39, 0x5

    shl-long v40, v40, v39

    const/16 v39, 0x3b

    ushr-long v42, v14, v39

    or-long v40, v40, v42

    aput-wide v40, p3, v38

    .line 220
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const/16 v39, 0x2a

    ushr-long v40, v14, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    aput-wide v40, p3, p4

    .line 221
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v39, 0x19

    ushr-long v40, v14, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    aput-wide v40, p3, v38

    .line 222
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const/16 v39, 0x8

    ushr-long v40, v14, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    aput-wide v40, p3, p4

    .line 223
    add-int/lit8 v36, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v36    # "blocksOffset":I
    aget-wide v16, p1, p2

    .line 224
    .local v16, "block15":J
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v40, 0xff

    and-long v40, v40, v14

    const/16 v39, 0x9

    shl-long v40, v40, v39

    const/16 v39, 0x37

    ushr-long v42, v16, v39

    or-long v40, v40, v42

    aput-wide v40, p3, v38

    .line 225
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const/16 v39, 0x26

    ushr-long v40, v16, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    aput-wide v40, p3, p4

    .line 226
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v39, 0x15

    ushr-long v40, v16, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    aput-wide v40, p3, v38

    .line 227
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const/16 v39, 0x4

    ushr-long v40, v16, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    aput-wide v40, p3, p4

    .line 228
    add-int/lit8 p2, v36, 0x1

    .end local v36    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v18, p1, v36

    .line 229
    .local v18, "block16":J
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v40, 0xf

    and-long v40, v40, v16

    const/16 v39, 0xd

    shl-long v40, v40, v39

    const/16 v39, 0x33

    ushr-long v42, v18, v39

    or-long v40, v40, v42

    aput-wide v40, p3, v38

    .line 230
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const/16 v39, 0x22

    ushr-long v40, v18, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    aput-wide v40, p3, p4

    .line 231
    add-int/lit8 p4, v38, 0x1

    .end local v38    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v39, 0x11

    ushr-long v40, v18, v39

    const-wide/32 v42, 0x1ffff

    and-long v40, v40, v42

    aput-wide v40, p3, v38

    .line 232
    add-int/lit8 v38, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v38    # "valuesOffset":I
    const-wide/32 v40, 0x1ffff

    and-long v40, v40, v18

    aput-wide v40, p3, p4

    .line 151
    add-int/lit8 v37, v37, 0x1

    move/from16 v36, p2

    .end local p2    # "blocksOffset":I
    .restart local v36    # "blocksOffset":I
    goto/16 :goto_0
.end method
