.class final Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;
.super Ljava/lang/Object;
.source "BasicOperations.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/automaton/BasicOperations;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "PointTransitions"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;",
        ">;"
    }
.end annotation


# instance fields
.field final ends:Lorg/apache/lucene/util/automaton/BasicOperations$TransitionList;

.field point:I

.field final starts:Lorg/apache/lucene/util/automaton/BasicOperations$TransitionList;


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 472
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 474
    new-instance v0, Lorg/apache/lucene/util/automaton/BasicOperations$TransitionList;

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/automaton/BasicOperations$TransitionList;-><init>(Lorg/apache/lucene/util/automaton/BasicOperations$TransitionList;)V

    iput-object v0, p0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;->ends:Lorg/apache/lucene/util/automaton/BasicOperations$TransitionList;

    .line 475
    new-instance v0, Lorg/apache/lucene/util/automaton/BasicOperations$TransitionList;

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/automaton/BasicOperations$TransitionList;-><init>(Lorg/apache/lucene/util/automaton/BasicOperations$TransitionList;)V

    iput-object v0, p0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;->starts:Lorg/apache/lucene/util/automaton/BasicOperations$TransitionList;

    .line 472
    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;)V
    .locals 0

    .prologue
    .line 472
    invoke-direct {p0}, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;

    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;->compareTo(Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;)I
    .locals 2
    .param p1, "other"    # Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;

    .prologue
    .line 478
    iget v0, p0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;->point:I

    iget v1, p1, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;->point:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 489
    check-cast p1, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;

    .end local p1    # "other":Ljava/lang/Object;
    iget v0, p1, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;->point:I

    iget v1, p0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;->point:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 494
    iget v0, p0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;->point:I

    return v0
.end method

.method public reset(I)V
    .locals 2
    .param p1, "point"    # I

    .prologue
    const/4 v1, 0x0

    .line 482
    iput p1, p0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;->point:I

    .line 483
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;->ends:Lorg/apache/lucene/util/automaton/BasicOperations$TransitionList;

    iput v1, v0, Lorg/apache/lucene/util/automaton/BasicOperations$TransitionList;->count:I

    .line 484
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;->starts:Lorg/apache/lucene/util/automaton/BasicOperations$TransitionList;

    iput v1, v0, Lorg/apache/lucene/util/automaton/BasicOperations$TransitionList;->count:I

    .line 485
    return-void
.end method
