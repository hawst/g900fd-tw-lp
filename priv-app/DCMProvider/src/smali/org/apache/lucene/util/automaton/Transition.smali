.class public Lorg/apache/lucene/util/automaton/Transition;
.super Ljava/lang/Object;
.source "Transition.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/automaton/Transition$CompareByDestThenMinMaxSingle;,
        Lorg/apache/lucene/util/automaton/Transition$CompareByMinMaxThenDestSingle;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final CompareByDestThenMinMax:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/automaton/Transition;",
            ">;"
        }
    .end annotation
.end field

.field public static final CompareByMinMaxThenDest:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/automaton/Transition;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final max:I

.field final min:I

.field final to:Lorg/apache/lucene/util/automaton/State;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 42
    const-class v0, Lorg/apache/lucene/util/automaton/Transition;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/automaton/Transition;->$assertionsDisabled:Z

    .line 196
    new-instance v0, Lorg/apache/lucene/util/automaton/Transition$CompareByDestThenMinMaxSingle;

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/automaton/Transition$CompareByDestThenMinMaxSingle;-><init>(Lorg/apache/lucene/util/automaton/Transition$CompareByDestThenMinMaxSingle;)V

    sput-object v0, Lorg/apache/lucene/util/automaton/Transition;->CompareByDestThenMinMax:Ljava/util/Comparator;

    .line 213
    new-instance v0, Lorg/apache/lucene/util/automaton/Transition$CompareByMinMaxThenDestSingle;

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/automaton/Transition$CompareByMinMaxThenDestSingle;-><init>(Lorg/apache/lucene/util/automaton/Transition$CompareByMinMaxThenDestSingle;)V

    sput-object v0, Lorg/apache/lucene/util/automaton/Transition;->CompareByMinMaxThenDest:Ljava/util/Comparator;

    return-void

    .line 42
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(IILorg/apache/lucene/util/automaton/State;)V
    .locals 2
    .param p1, "min"    # I
    .param p2, "max"    # I
    .param p3, "to"    # Lorg/apache/lucene/util/automaton/State;

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    sget-boolean v1, Lorg/apache/lucene/util/automaton/Transition;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-gez p1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 73
    :cond_0
    sget-boolean v1, Lorg/apache/lucene/util/automaton/Transition;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    if-gez p2, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 74
    :cond_1
    if-ge p2, p1, :cond_2

    .line 75
    move v0, p2

    .line 76
    .local v0, "t":I
    move p2, p1

    .line 77
    move p1, v0

    .line 79
    .end local v0    # "t":I
    :cond_2
    iput p1, p0, Lorg/apache/lucene/util/automaton/Transition;->min:I

    .line 80
    iput p2, p0, Lorg/apache/lucene/util/automaton/Transition;->max:I

    .line 81
    iput-object p3, p0, Lorg/apache/lucene/util/automaton/Transition;->to:Lorg/apache/lucene/util/automaton/State;

    .line 82
    return-void
.end method

.method public constructor <init>(ILorg/apache/lucene/util/automaton/State;)V
    .locals 1
    .param p1, "c"    # I
    .param p2, "to"    # Lorg/apache/lucene/util/automaton/State;

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    sget-boolean v0, Lorg/apache/lucene/util/automaton/Transition;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-gez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 60
    :cond_0
    iput p1, p0, Lorg/apache/lucene/util/automaton/Transition;->max:I

    iput p1, p0, Lorg/apache/lucene/util/automaton/Transition;->min:I

    .line 61
    iput-object p2, p0, Lorg/apache/lucene/util/automaton/Transition;->to:Lorg/apache/lucene/util/automaton/State;

    .line 62
    return-void
.end method

.method static appendCharString(ILjava/lang/StringBuilder;)V
    .locals 2
    .param p0, "c"    # I
    .param p1, "b"    # Ljava/lang/StringBuilder;

    .prologue
    .line 140
    const/16 v1, 0x21

    if-lt p0, v1, :cond_0

    const/16 v1, 0x7e

    if-gt p0, v1, :cond_0

    const/16 v1, 0x5c

    if-eq p0, v1, :cond_0

    const/16 v1, 0x22

    if-eq p0, v1, :cond_0

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->appendCodePoint(I)Ljava/lang/StringBuilder;

    .line 153
    :goto_0
    return-void

    .line 142
    :cond_0
    const-string v1, "\\\\U"

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 143
    invoke-static {p0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    .line 144
    .local v0, "s":Ljava/lang/String;
    const/16 v1, 0x10

    if-ge p0, v1, :cond_1

    const-string v1, "0000000"

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 145
    :cond_1
    const/16 v1, 0x100

    if-ge p0, v1, :cond_2

    const-string v1, "000000"

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 146
    :cond_2
    const/16 v1, 0x1000

    if-ge p0, v1, :cond_3

    const-string v1, "00000"

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 147
    :cond_3
    const/high16 v1, 0x10000

    if-ge p0, v1, :cond_4

    const-string v1, "0000"

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 148
    :cond_4
    const/high16 v1, 0x100000

    if-ge p0, v1, :cond_5

    const-string v1, "000"

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 149
    :cond_5
    const/high16 v1, 0x1000000

    if-ge p0, v1, :cond_6

    const-string v1, "00"

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 150
    :cond_6
    const/high16 v1, 0x10000000

    if-ge p0, v1, :cond_7

    const-string v1, "0"

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 151
    :cond_7
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method


# virtual methods
.method appendDot(Ljava/lang/StringBuilder;)V
    .locals 2
    .param p1, "b"    # Ljava/lang/StringBuilder;

    .prologue
    .line 172
    const-string v0, " -> "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/util/automaton/Transition;->to:Lorg/apache/lucene/util/automaton/State;

    iget v1, v1, Lorg/apache/lucene/util/automaton/State;->number:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " [label=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 173
    iget v0, p0, Lorg/apache/lucene/util/automaton/Transition;->min:I

    invoke-static {v0, p1}, Lorg/apache/lucene/util/automaton/Transition;->appendCharString(ILjava/lang/StringBuilder;)V

    .line 174
    iget v0, p0, Lorg/apache/lucene/util/automaton/Transition;->min:I

    iget v1, p0, Lorg/apache/lucene/util/automaton/Transition;->max:I

    if-eq v0, v1, :cond_0

    .line 175
    const-string v0, "-"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 176
    iget v0, p0, Lorg/apache/lucene/util/automaton/Transition;->max:I

    invoke-static {v0, p1}, Lorg/apache/lucene/util/automaton/Transition;->appendCharString(ILjava/lang/StringBuilder;)V

    .line 178
    :cond_0
    const-string v0, "\"]\n"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 179
    return-void
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Transition;->clone()Lorg/apache/lucene/util/automaton/Transition;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lorg/apache/lucene/util/automaton/Transition;
    .locals 2

    .prologue
    .line 133
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/util/automaton/Transition;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 134
    :catch_0
    move-exception v0

    .line 135
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 108
    instance-of v2, p1, Lorg/apache/lucene/util/automaton/Transition;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 109
    check-cast v0, Lorg/apache/lucene/util/automaton/Transition;

    .line 110
    .local v0, "t":Lorg/apache/lucene/util/automaton/Transition;
    iget v2, v0, Lorg/apache/lucene/util/automaton/Transition;->min:I

    iget v3, p0, Lorg/apache/lucene/util/automaton/Transition;->min:I

    if-ne v2, v3, :cond_0

    iget v2, v0, Lorg/apache/lucene/util/automaton/Transition;->max:I

    iget v3, p0, Lorg/apache/lucene/util/automaton/Transition;->max:I

    if-ne v2, v3, :cond_0

    iget-object v2, v0, Lorg/apache/lucene/util/automaton/Transition;->to:Lorg/apache/lucene/util/automaton/State;

    iget-object v3, p0, Lorg/apache/lucene/util/automaton/Transition;->to:Lorg/apache/lucene/util/automaton/State;

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    .line 111
    .end local v0    # "t":Lorg/apache/lucene/util/automaton/Transition;
    :cond_0
    return v1
.end method

.method public getDest()Lorg/apache/lucene/util/automaton/State;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/Transition;->to:Lorg/apache/lucene/util/automaton/State;

    return-object v0
.end method

.method public getMax()I
    .locals 1

    .prologue
    .line 91
    iget v0, p0, Lorg/apache/lucene/util/automaton/Transition;->max:I

    return v0
.end method

.method public getMin()I
    .locals 1

    .prologue
    .line 86
    iget v0, p0, Lorg/apache/lucene/util/automaton/Transition;->min:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 122
    iget v0, p0, Lorg/apache/lucene/util/automaton/Transition;->min:I

    mul-int/lit8 v0, v0, 0x2

    iget v1, p0, Lorg/apache/lucene/util/automaton/Transition;->max:I

    mul-int/lit8 v1, v1, 0x3

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 161
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 162
    .local v0, "b":Ljava/lang/StringBuilder;
    iget v1, p0, Lorg/apache/lucene/util/automaton/Transition;->min:I

    invoke-static {v1, v0}, Lorg/apache/lucene/util/automaton/Transition;->appendCharString(ILjava/lang/StringBuilder;)V

    .line 163
    iget v1, p0, Lorg/apache/lucene/util/automaton/Transition;->min:I

    iget v2, p0, Lorg/apache/lucene/util/automaton/Transition;->max:I

    if-eq v1, v2, :cond_0

    .line 164
    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 165
    iget v1, p0, Lorg/apache/lucene/util/automaton/Transition;->max:I

    invoke-static {v1, v0}, Lorg/apache/lucene/util/automaton/Transition;->appendCharString(ILjava/lang/StringBuilder;)V

    .line 167
    :cond_0
    const-string v1, " -> "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/util/automaton/Transition;->to:Lorg/apache/lucene/util/automaton/State;

    iget v2, v2, Lorg/apache/lucene/util/automaton/State;->number:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 168
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
