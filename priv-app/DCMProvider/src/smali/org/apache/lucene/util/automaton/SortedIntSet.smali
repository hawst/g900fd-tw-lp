.class final Lorg/apache/lucene/util/automaton/SortedIntSet;
.super Ljava/lang/Object;
.source "SortedIntSet.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/automaton/SortedIntSet$FrozenIntSet;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final TREE_MAP_CUTOVER:I = 0x1e


# instance fields
.field counts:[I

.field private hashCode:I

.field private final map:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field state:Lorg/apache/lucene/util/automaton/State;

.field upto:I

.field private useTreeMap:Z

.field values:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lorg/apache/lucene/util/automaton/SortedIntSet;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/automaton/SortedIntSet;->$assertionsDisabled:Z

    .line 36
    return-void

    .line 28
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "capacity"    # I

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->map:Ljava/util/Map;

    .line 45
    new-array v0, p1, [I

    iput-object v0, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->values:[I

    .line 46
    new-array v0, p1, [I

    iput-object v0, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->counts:[I

    .line 47
    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/util/automaton/SortedIntSet;)I
    .locals 1

    .prologue
    .line 32
    iget v0, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->hashCode:I

    return v0
.end method


# virtual methods
.method public computeHash()V
    .locals 7

    .prologue
    .line 136
    iget-boolean v3, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->useTreeMap:Z

    if-eqz v3, :cond_3

    .line 137
    iget-object v3, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->map:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v3

    iget-object v4, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->values:[I

    array-length v4, v4

    if-le v3, v4, :cond_0

    .line 138
    iget-object v3, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->map:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v3

    const/4 v4, 0x4

    invoke-static {v3, v4}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v1

    .line 139
    .local v1, "size":I
    new-array v3, v1, [I

    iput-object v3, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->values:[I

    .line 140
    new-array v3, v1, [I

    iput-object v3, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->counts:[I

    .line 142
    .end local v1    # "size":I
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->map:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v3

    iput v3, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->hashCode:I

    .line 143
    const/4 v3, 0x0

    iput v3, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->upto:I

    .line 144
    iget-object v3, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->map:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 154
    :cond_1
    return-void

    .line 144
    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 145
    .local v2, "state":I
    iget v3, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->hashCode:I

    mul-int/lit16 v3, v3, 0x2ab

    add-int/2addr v3, v2

    iput v3, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->hashCode:I

    .line 146
    iget-object v3, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->values:[I

    iget v5, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->upto:I

    add-int/lit8 v6, v5, 0x1

    iput v6, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->upto:I

    aput v2, v3, v5

    goto :goto_0

    .line 149
    .end local v2    # "state":I
    :cond_3
    iget v3, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->upto:I

    iput v3, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->hashCode:I

    .line 150
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget v3, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->upto:I

    if-ge v0, v3, :cond_1

    .line 151
    iget v3, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->hashCode:I

    mul-int/lit16 v3, v3, 0x2ab

    iget-object v4, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->values:[I

    aget v4, v4, v0

    add-int/2addr v3, v4

    iput v3, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->hashCode:I

    .line 150
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public decr(I)V
    .locals 7
    .param p1, "num"    # I

    .prologue
    const/4 v6, 0x0

    .line 102
    iget-boolean v3, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->useTreeMap:Z

    if-eqz v3, :cond_2

    .line 103
    iget-object v3, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->map:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 104
    .local v0, "count":I
    const/4 v3, 0x1

    if-ne v0, v3, :cond_1

    .line 105
    iget-object v3, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->map:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    :goto_0
    iget-object v3, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->map:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v3

    if-nez v3, :cond_0

    .line 111
    iput-boolean v6, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->useTreeMap:Z

    .line 112
    iput v6, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->upto:I

    .line 133
    .end local v0    # "count":I
    :cond_0
    :goto_1
    return-void

    .line 107
    .restart local v0    # "count":I
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->map:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    add-int/lit8 v5, v0, -0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 117
    .end local v0    # "count":I
    :cond_2
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    iget v3, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->upto:I

    if-lt v1, v3, :cond_3

    .line 132
    sget-boolean v3, Lorg/apache/lucene/util/automaton/SortedIntSet;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 118
    :cond_3
    iget-object v3, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->values:[I

    aget v3, v3, v1

    if-ne v3, p1, :cond_5

    .line 119
    iget-object v3, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->counts:[I

    aget v4, v3, v1

    add-int/lit8 v4, v4, -0x1

    aput v4, v3, v1

    .line 120
    iget-object v3, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->counts:[I

    aget v3, v3, v1

    if-nez v3, :cond_0

    .line 121
    iget v3, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->upto:I

    add-int/lit8 v2, v3, -0x1

    .line 122
    .local v2, "limit":I
    :goto_3
    if-lt v1, v2, :cond_4

    .line 127
    iput v2, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->upto:I

    goto :goto_1

    .line 123
    :cond_4
    iget-object v3, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->values:[I

    iget-object v4, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->values:[I

    add-int/lit8 v5, v1, 0x1

    aget v4, v4, v5

    aput v4, v3, v1

    .line 124
    iget-object v3, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->counts:[I

    iget-object v4, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->counts:[I

    add-int/lit8 v5, v1, 0x1

    aget v4, v4, v5

    aput v4, v3, v1

    .line 125
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 117
    .end local v2    # "limit":I
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "_other"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x0

    .line 169
    if-nez p1, :cond_1

    .line 188
    :cond_0
    :goto_0
    return v2

    .line 172
    :cond_1
    instance-of v3, p1, Lorg/apache/lucene/util/automaton/SortedIntSet$FrozenIntSet;

    if-eqz v3, :cond_0

    move-object v1, p1

    .line 175
    check-cast v1, Lorg/apache/lucene/util/automaton/SortedIntSet$FrozenIntSet;

    .line 176
    .local v1, "other":Lorg/apache/lucene/util/automaton/SortedIntSet$FrozenIntSet;
    iget v3, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->hashCode:I

    iget v4, v1, Lorg/apache/lucene/util/automaton/SortedIntSet$FrozenIntSet;->hashCode:I

    if-ne v3, v4, :cond_0

    .line 179
    iget-object v3, v1, Lorg/apache/lucene/util/automaton/SortedIntSet$FrozenIntSet;->values:[I

    array-length v3, v3

    iget v4, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->upto:I

    if-ne v3, v4, :cond_0

    .line 182
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget v3, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->upto:I

    if-lt v0, v3, :cond_2

    .line 188
    const/4 v2, 0x1

    goto :goto_0

    .line 183
    :cond_2
    iget-object v3, v1, Lorg/apache/lucene/util/automaton/SortedIntSet$FrozenIntSet;->values:[I

    aget v3, v3, v0

    iget-object v4, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->values:[I

    aget v4, v4, v0

    if-ne v3, v4, :cond_0

    .line 182
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public freeze(Lorg/apache/lucene/util/automaton/State;)Lorg/apache/lucene/util/automaton/SortedIntSet$FrozenIntSet;
    .locals 4
    .param p1, "state"    # Lorg/apache/lucene/util/automaton/State;

    .prologue
    const/4 v3, 0x0

    .line 157
    iget v1, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->upto:I

    new-array v0, v1, [I

    .line 158
    .local v0, "c":[I
    iget-object v1, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->values:[I

    iget v2, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->upto:I

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 159
    new-instance v1, Lorg/apache/lucene/util/automaton/SortedIntSet$FrozenIntSet;

    iget v2, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->hashCode:I

    invoke-direct {v1, v0, v2, p1}, Lorg/apache/lucene/util/automaton/SortedIntSet$FrozenIntSet;-><init>([IILorg/apache/lucene/util/automaton/State;)V

    return-object v1
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 164
    iget v0, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->hashCode:I

    return v0
.end method

.method public incr(I)V
    .locals 8
    .param p1, "num"    # I

    .prologue
    const/4 v7, 0x1

    .line 51
    iget-boolean v4, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->useTreeMap:Z

    if-eqz v4, :cond_2

    .line 52
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 53
    .local v2, "key":Ljava/lang/Integer;
    iget-object v4, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->map:Ljava/util/Map;

    invoke-interface {v4, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 54
    .local v3, "val":Ljava/lang/Integer;
    if-nez v3, :cond_1

    .line 55
    iget-object v4, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->map:Ljava/util/Map;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v2, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    .end local v2    # "key":Ljava/lang/Integer;
    .end local v3    # "val":Ljava/lang/Integer;
    :cond_0
    :goto_0
    return-void

    .line 57
    .restart local v2    # "key":Ljava/lang/Integer;
    .restart local v3    # "val":Ljava/lang/Integer;
    :cond_1
    iget-object v4, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->map:Ljava/util/Map;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v2, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 62
    .end local v2    # "key":Ljava/lang/Integer;
    .end local v3    # "val":Ljava/lang/Integer;
    :cond_2
    iget v4, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->upto:I

    iget-object v5, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->values:[I

    array-length v5, v5

    if-ne v4, v5, :cond_3

    .line 63
    iget-object v4, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->values:[I

    iget v5, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->upto:I

    add-int/lit8 v5, v5, 0x1

    invoke-static {v4, v5}, Lorg/apache/lucene/util/ArrayUtil;->grow([II)[I

    move-result-object v4

    iput-object v4, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->values:[I

    .line 64
    iget-object v4, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->counts:[I

    iget v5, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->upto:I

    add-int/lit8 v5, v5, 0x1

    invoke-static {v4, v5}, Lorg/apache/lucene/util/ArrayUtil;->grow([II)[I

    move-result-object v4

    iput-object v4, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->counts:[I

    .line 67
    :cond_3
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget v4, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->upto:I

    if-lt v0, v4, :cond_4

    .line 87
    iget-object v4, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->values:[I

    iget v5, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->upto:I

    aput p1, v4, v5

    .line 88
    iget-object v4, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->counts:[I

    iget v5, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->upto:I

    aput v7, v4, v5

    .line 89
    iget v4, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->upto:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->upto:I

    .line 91
    iget v4, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->upto:I

    const/16 v5, 0x1e

    if-ne v4, v5, :cond_0

    .line 92
    iput-boolean v7, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->useTreeMap:Z

    .line 93
    const/4 v0, 0x0

    :goto_2
    iget v4, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->upto:I

    if-ge v0, v4, :cond_0

    .line 94
    iget-object v4, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->map:Ljava/util/Map;

    iget-object v5, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->values:[I

    aget v5, v5, v0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iget-object v6, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->counts:[I

    aget v6, v6, v0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 68
    :cond_4
    iget-object v4, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->values:[I

    aget v4, v4, v0

    if-ne v4, p1, :cond_5

    .line 69
    iget-object v4, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->counts:[I

    aget v5, v4, v0

    add-int/lit8 v5, v5, 0x1

    aput v5, v4, v0

    goto :goto_0

    .line 71
    :cond_5
    iget-object v4, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->values:[I

    aget v4, v4, v0

    if-ge p1, v4, :cond_7

    .line 73
    iget v4, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->upto:I

    add-int/lit8 v1, v4, -0x1

    .line 74
    .local v1, "j":I
    :goto_3
    if-ge v1, v0, :cond_6

    .line 79
    iget-object v4, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->values:[I

    aput p1, v4, v0

    .line 80
    iget-object v4, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->counts:[I

    aput v7, v4, v0

    .line 81
    iget v4, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->upto:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->upto:I

    goto/16 :goto_0

    .line 75
    :cond_6
    iget-object v4, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->values:[I

    add-int/lit8 v5, v1, 0x1

    iget-object v6, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->values:[I

    aget v6, v6, v1

    aput v6, v4, v5

    .line 76
    iget-object v4, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->counts:[I

    add-int/lit8 v5, v1, 0x1

    iget-object v6, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->counts:[I

    aget v6, v6, v1

    aput v6, v4, v5

    .line 77
    add-int/lit8 v1, v1, -0x1

    goto :goto_3

    .line 67
    .end local v1    # "j":I
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 193
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v3, 0x5b

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 194
    .local v1, "sb":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->upto:I

    if-lt v0, v2, :cond_0

    .line 200
    const/16 v2, 0x5d

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 201
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 195
    :cond_0
    if-lez v0, :cond_1

    .line 196
    const/16 v2, 0x20

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 198
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->values:[I

    aget v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x3a

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/util/automaton/SortedIntSet;->counts:[I

    aget v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 194
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
