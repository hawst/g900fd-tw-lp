.class public final Lorg/apache/lucene/util/IndexableBinaryStringTools;
.super Ljava/lang/Object;
.source "IndexableBinaryStringTools.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final CODING_CASES:[Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v6, 0x3

    const/4 v7, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 45
    const-class v0, Lorg/apache/lucene/util/IndexableBinaryStringTools;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/IndexableBinaryStringTools;->$assertionsDisabled:Z

    .line 47
    const/16 v0, 0x8

    new-array v0, v0, [Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;

    .line 49
    new-instance v3, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;

    const/4 v4, 0x7

    invoke-direct {v3, v4, v1}, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;-><init>(II)V

    aput-object v3, v0, v2

    .line 51
    new-instance v3, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;

    const/16 v4, 0xe

    const/4 v5, 0x6

    invoke-direct {v3, v4, v5, v7}, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;-><init>(III)V

    aput-object v3, v0, v1

    .line 52
    new-instance v3, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;

    const/16 v4, 0xd

    const/4 v5, 0x5

    invoke-direct {v3, v4, v5, v6}, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;-><init>(III)V

    aput-object v3, v0, v7

    .line 53
    new-instance v3, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;

    const/16 v4, 0xc

    invoke-direct {v3, v4, v8, v8}, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;-><init>(III)V

    aput-object v3, v0, v6

    .line 54
    new-instance v3, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;

    const/16 v4, 0xb

    const/4 v5, 0x5

    invoke-direct {v3, v4, v6, v5}, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;-><init>(III)V

    aput-object v3, v0, v8

    const/4 v3, 0x5

    .line 55
    new-instance v4, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;

    const/16 v5, 0xa

    const/4 v6, 0x6

    invoke-direct {v4, v5, v7, v6}, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;-><init>(III)V

    aput-object v4, v0, v3

    const/4 v3, 0x6

    .line 56
    new-instance v4, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;

    const/16 v5, 0x9

    const/4 v6, 0x7

    invoke-direct {v4, v5, v1, v6}, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;-><init>(III)V

    aput-object v4, v0, v3

    const/4 v1, 0x7

    .line 57
    new-instance v3, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;

    const/16 v4, 0x8

    invoke-direct {v3, v4, v2}, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;-><init>(II)V

    aput-object v3, v0, v1

    .line 47
    sput-object v0, Lorg/apache/lucene/util/IndexableBinaryStringTools;->CODING_CASES:[Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;

    .line 58
    return-void

    :cond_0
    move v0, v2

    .line 45
    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static decode([CII[BII)V
    .locals 12
    .param p0, "inputArray"    # [C
    .param p1, "inputOffset"    # I
    .param p2, "inputLength"    # I
    .param p3, "outputArray"    # [B
    .param p4, "outputOffset"    # I
    .param p5, "outputLength"    # I

    .prologue
    .line 166
    sget-boolean v9, Lorg/apache/lucene/util/IndexableBinaryStringTools;->$assertionsDisabled:Z

    if-nez v9, :cond_0

    .line 167
    invoke-static {p0, p1, p2}, Lorg/apache/lucene/util/IndexableBinaryStringTools;->getDecodedLength([CII)I

    move-result v9

    move/from16 v0, p5

    if-eq v0, v9, :cond_0

    new-instance v9, Ljava/lang/AssertionError;

    invoke-direct {v9}, Ljava/lang/AssertionError;-><init>()V

    throw v9

    .line 168
    :cond_0
    add-int/lit8 v6, p2, -0x1

    .line 169
    .local v6, "numInputChars":I
    move/from16 v7, p5

    .line 171
    .local v7, "numOutputBytes":I
    if-lez v7, :cond_2

    .line 172
    const/4 v2, 0x0

    .line 173
    .local v2, "caseNum":I
    move/from16 v8, p4

    .line 174
    .local v8, "outputByteNum":I
    move v5, p1

    .line 177
    .local v5, "inputCharNum":I
    :goto_0
    add-int/lit8 v9, v6, -0x1

    if-lt v5, v9, :cond_3

    .line 198
    aget-char v9, p0, v5

    int-to-short v4, v9

    .line 199
    .local v4, "inputChar":S
    sget-object v9, Lorg/apache/lucene/util/IndexableBinaryStringTools;->CODING_CASES:[Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;

    aget-object v3, v9, v2

    .line 200
    .local v3, "codingCase":Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;
    if-nez v2, :cond_1

    .line 201
    const/4 v9, 0x0

    aput-byte v9, p3, v8

    .line 203
    :cond_1
    aget-byte v9, p3, v8

    iget v10, v3, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->initialShift:I

    ushr-int v10, v4, v10

    int-to-byte v10, v10

    add-int/2addr v9, v10

    int-to-byte v9, v9

    aput-byte v9, p3, v8

    .line 204
    sub-int v1, v7, v8

    .line 205
    .local v1, "bytesLeft":I
    const/4 v9, 0x1

    if-le v1, v9, :cond_2

    .line 206
    const/4 v9, 0x2

    iget v10, v3, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->numBytes:I

    if-ne v9, v10, :cond_7

    .line 207
    add-int/lit8 v9, v8, 0x1

    iget-short v10, v3, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->finalMask:S

    and-int/2addr v10, v4

    iget v11, v3, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->finalShift:I

    ushr-int/2addr v10, v11

    int-to-byte v10, v10

    aput-byte v10, p3, v9

    .line 216
    .end local v1    # "bytesLeft":I
    .end local v2    # "caseNum":I
    .end local v3    # "codingCase":Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;
    .end local v4    # "inputChar":S
    .end local v5    # "inputCharNum":I
    .end local v8    # "outputByteNum":I
    :cond_2
    :goto_1
    return-void

    .line 178
    .restart local v2    # "caseNum":I
    .restart local v5    # "inputCharNum":I
    .restart local v8    # "outputByteNum":I
    :cond_3
    sget-object v9, Lorg/apache/lucene/util/IndexableBinaryStringTools;->CODING_CASES:[Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;

    aget-object v3, v9, v2

    .line 179
    .restart local v3    # "codingCase":Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;
    aget-char v9, p0, v5

    int-to-short v4, v9

    .line 180
    .restart local v4    # "inputChar":S
    const/4 v9, 0x2

    iget v10, v3, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->numBytes:I

    if-ne v9, v10, :cond_6

    .line 181
    if-nez v2, :cond_5

    .line 182
    iget v9, v3, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->initialShift:I

    ushr-int v9, v4, v9

    int-to-byte v9, v9

    aput-byte v9, p3, v8

    .line 186
    :goto_2
    add-int/lit8 v9, v8, 0x1

    iget-short v10, v3, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->finalMask:S

    and-int/2addr v10, v4

    iget v11, v3, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->finalShift:I

    shl-int/2addr v10, v11

    int-to-byte v10, v10

    aput-byte v10, p3, v9

    .line 192
    :goto_3
    iget v9, v3, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->advanceBytes:I

    add-int/2addr v8, v9

    .line 193
    add-int/lit8 v2, v2, 0x1

    sget-object v9, Lorg/apache/lucene/util/IndexableBinaryStringTools;->CODING_CASES:[Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;

    array-length v9, v9

    if-ne v2, v9, :cond_4

    .line 194
    const/4 v2, 0x0

    .line 177
    :cond_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 184
    :cond_5
    aget-byte v9, p3, v8

    iget v10, v3, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->initialShift:I

    ushr-int v10, v4, v10

    int-to-byte v10, v10

    add-int/2addr v9, v10

    int-to-byte v9, v9

    aput-byte v9, p3, v8

    goto :goto_2

    .line 188
    :cond_6
    aget-byte v9, p3, v8

    iget v10, v3, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->initialShift:I

    ushr-int v10, v4, v10

    int-to-byte v10, v10

    add-int/2addr v9, v10

    int-to-byte v9, v9

    aput-byte v9, p3, v8

    .line 189
    add-int/lit8 v9, v8, 0x1

    iget-short v10, v3, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->middleMask:S

    and-int/2addr v10, v4

    iget v11, v3, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->middleShift:I

    ushr-int/2addr v10, v11

    int-to-byte v10, v10

    aput-byte v10, p3, v9

    .line 190
    add-int/lit8 v9, v8, 0x2

    iget-short v10, v3, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->finalMask:S

    and-int/2addr v10, v4

    iget v11, v3, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->finalShift:I

    shl-int/2addr v10, v11

    int-to-byte v10, v10

    aput-byte v10, p3, v9

    goto :goto_3

    .line 209
    .restart local v1    # "bytesLeft":I
    :cond_7
    add-int/lit8 v9, v8, 0x1

    iget-short v10, v3, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->middleMask:S

    and-int/2addr v10, v4

    iget v11, v3, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->middleShift:I

    ushr-int/2addr v10, v11

    int-to-byte v10, v10

    aput-byte v10, p3, v9

    .line 210
    const/4 v9, 0x2

    if-le v1, v9, :cond_2

    .line 211
    add-int/lit8 v9, v8, 0x2

    iget-short v10, v3, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->finalMask:S

    and-int/2addr v10, v4

    iget v11, v3, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->finalShift:I

    shl-int/2addr v10, v11

    int-to-byte v10, v10

    aput-byte v10, p3, v9

    goto :goto_1
.end method

.method public static encode([BII[CII)V
    .locals 9
    .param p0, "inputArray"    # [B
    .param p1, "inputOffset"    # I
    .param p2, "inputLength"    # I
    .param p3, "outputArray"    # [C
    .param p4, "outputOffset"    # I
    .param p5, "outputLength"    # I

    .prologue
    const/4 v5, 0x1

    .line 111
    sget-boolean v6, Lorg/apache/lucene/util/IndexableBinaryStringTools;->$assertionsDisabled:Z

    if-nez v6, :cond_0

    .line 112
    invoke-static {p0, p1, p2}, Lorg/apache/lucene/util/IndexableBinaryStringTools;->getEncodedLength([BII)I

    move-result v6

    if-eq p5, v6, :cond_0

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 113
    :cond_0
    if-lez p2, :cond_1

    .line 114
    move v2, p1

    .line 115
    .local v2, "inputByteNum":I
    const/4 v0, 0x0

    .line 116
    .local v0, "caseNum":I
    move v3, p4

    .line 118
    .local v3, "outputCharNum":I
    :goto_0
    sget-object v6, Lorg/apache/lucene/util/IndexableBinaryStringTools;->CODING_CASES:[Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;

    aget-object v6, v6, v0

    iget v6, v6, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->numBytes:I

    add-int/2addr v6, v2

    if-le v6, p2, :cond_2

    .line 134
    sget-object v6, Lorg/apache/lucene/util/IndexableBinaryStringTools;->CODING_CASES:[Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;

    aget-object v1, v6, v0

    .line 136
    .local v1, "codingCase":Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;
    add-int/lit8 v6, v2, 0x1

    if-ge v6, p2, :cond_5

    .line 137
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "outputCharNum":I
    .local v4, "outputCharNum":I
    aget-byte v6, p0, v2

    and-int/lit16 v6, v6, 0xff

    iget v7, v1, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->initialShift:I

    shl-int/2addr v6, v7

    add-int/lit8 v7, v2, 0x1

    aget-byte v7, p0, v7

    and-int/lit16 v7, v7, 0xff

    iget v8, v1, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->middleShift:I

    shl-int/2addr v7, v8

    add-int/2addr v6, v7

    and-int/lit16 v6, v6, 0x7fff

    int-to-char v6, v6

    aput-char v6, p3, v3

    .line 139
    add-int/lit8 v3, v4, 0x1

    .end local v4    # "outputCharNum":I
    .restart local v3    # "outputCharNum":I
    aput-char v5, p3, v4

    .line 149
    .end local v0    # "caseNum":I
    .end local v1    # "codingCase":Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;
    .end local v2    # "inputByteNum":I
    .end local v3    # "outputCharNum":I
    :cond_1
    :goto_1
    return-void

    .line 119
    .restart local v0    # "caseNum":I
    .restart local v2    # "inputByteNum":I
    .restart local v3    # "outputCharNum":I
    :cond_2
    sget-object v6, Lorg/apache/lucene/util/IndexableBinaryStringTools;->CODING_CASES:[Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;

    aget-object v1, v6, v0

    .line 120
    .restart local v1    # "codingCase":Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;
    const/4 v6, 0x2

    iget v7, v1, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->numBytes:I

    if-ne v6, v7, :cond_4

    .line 121
    aget-byte v6, p0, v2

    and-int/lit16 v6, v6, 0xff

    iget v7, v1, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->initialShift:I

    shl-int/2addr v6, v7

    .line 122
    add-int/lit8 v7, v2, 0x1

    aget-byte v7, p0, v7

    and-int/lit16 v7, v7, 0xff

    iget v8, v1, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->finalShift:I

    ushr-int/2addr v7, v8

    iget-short v8, v1, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->finalMask:S

    and-int/2addr v7, v8

    add-int/2addr v6, v7

    and-int/lit16 v6, v6, 0x7fff

    int-to-char v6, v6

    .line 121
    aput-char v6, p3, v3

    .line 128
    :goto_2
    iget v6, v1, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->advanceBytes:I

    add-int/2addr v2, v6

    .line 129
    add-int/lit8 v0, v0, 0x1

    sget-object v6, Lorg/apache/lucene/util/IndexableBinaryStringTools;->CODING_CASES:[Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;

    array-length v6, v6

    if-ne v0, v6, :cond_3

    .line 130
    const/4 v0, 0x0

    .line 118
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 124
    :cond_4
    aget-byte v6, p0, v2

    and-int/lit16 v6, v6, 0xff

    iget v7, v1, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->initialShift:I

    shl-int/2addr v6, v7

    .line 125
    add-int/lit8 v7, v2, 0x1

    aget-byte v7, p0, v7

    and-int/lit16 v7, v7, 0xff

    iget v8, v1, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->middleShift:I

    shl-int/2addr v7, v8

    add-int/2addr v6, v7

    .line 126
    add-int/lit8 v7, v2, 0x2

    aget-byte v7, p0, v7

    and-int/lit16 v7, v7, 0xff

    iget v8, v1, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->finalShift:I

    ushr-int/2addr v7, v8

    iget-short v8, v1, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->finalMask:S

    and-int/2addr v7, v8

    add-int/2addr v6, v7

    and-int/lit16 v6, v6, 0x7fff

    int-to-char v6, v6

    .line 124
    aput-char v6, p3, v3

    goto :goto_2

    .line 140
    :cond_5
    if-ge v2, p2, :cond_7

    .line 141
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "outputCharNum":I
    .restart local v4    # "outputCharNum":I
    aget-byte v6, p0, v2

    and-int/lit16 v6, v6, 0xff

    iget v7, v1, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->initialShift:I

    shl-int/2addr v6, v7

    and-int/lit16 v6, v6, 0x7fff

    int-to-char v6, v6

    aput-char v6, p3, v3

    .line 143
    add-int/lit8 v3, v4, 0x1

    .end local v4    # "outputCharNum":I
    .restart local v3    # "outputCharNum":I
    if-nez v0, :cond_6

    :goto_3
    aput-char v5, p3, v4

    goto :goto_1

    :cond_6
    const/4 v5, 0x0

    goto :goto_3

    .line 146
    :cond_7
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "outputCharNum":I
    .restart local v4    # "outputCharNum":I
    aput-char v5, p3, v3

    goto :goto_1
.end method

.method public static getDecodedLength([CII)I
    .locals 10
    .param p0, "encoded"    # [C
    .param p1, "offset"    # I
    .param p2, "length"    # I

    .prologue
    .line 86
    add-int/lit8 v0, p2, -0x1

    .line 87
    .local v0, "numChars":I
    if-gtz v0, :cond_0

    .line 88
    const/4 v1, 0x0

    .line 93
    :goto_0
    return v1

    .line 91
    :cond_0
    add-int v1, p1, p2

    add-int/lit8 v1, v1, -0x1

    aget-char v1, p0, v1

    int-to-long v4, v1

    .line 92
    .local v4, "numFullBytesInFinalChar":J
    add-int/lit8 v1, v0, -0x1

    int-to-long v2, v1

    .line 93
    .local v2, "numEncodedChars":J
    const-wide/16 v6, 0xf

    mul-long/2addr v6, v2

    const-wide/16 v8, 0x7

    add-long/2addr v6, v8

    const-wide/16 v8, 0x8

    div-long/2addr v6, v8

    add-long/2addr v6, v4

    long-to-int v1, v6

    goto :goto_0
.end method

.method public static getEncodedLength([BII)I
    .locals 4
    .param p0, "inputArray"    # [B
    .param p1, "inputOffset"    # I
    .param p2, "inputLength"    # I

    .prologue
    .line 74
    const-wide/16 v0, 0x8

    int-to-long v2, p2

    mul-long/2addr v0, v2

    const-wide/16 v2, 0xe

    add-long/2addr v0, v2

    const-wide/16 v2, 0xf

    div-long/2addr v0, v2

    long-to-int v0, v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method
