.class final Lorg/apache/lucene/util/packed/BulkOperationPacked21;
.super Lorg/apache/lucene/util/packed/BulkOperationPacked;
.source "BulkOperationPacked21.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    const/16 v0, 0x15

    invoke-direct {p0, v0}, Lorg/apache/lucene/util/packed/BulkOperationPacked;-><init>(I)V

    .line 29
    return-void
.end method


# virtual methods
.method public decode([BI[III)V
    .locals 28
    .param p1, "blocks"    # [B
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [I
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 124
    const/16 v24, 0x0

    .local v24, "i":I
    move/from16 v25, p4

    .end local p4    # "valuesOffset":I
    .local v25, "valuesOffset":I
    move/from16 v2, p2

    .end local p2    # "blocksOffset":I
    .local v2, "blocksOffset":I
    :goto_0
    move/from16 v0, v24

    move/from16 v1, p5

    if-lt v0, v1, :cond_0

    .line 155
    return-void

    .line 125
    :cond_0
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v26, p1, v2

    move/from16 v0, v26

    and-int/lit16 v3, v0, 0xff

    .line 126
    .local v3, "byte0":I
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v26, p1, p2

    move/from16 v0, v26

    and-int/lit16 v4, v0, 0xff

    .line 127
    .local v4, "byte1":I
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v26, p1, v2

    move/from16 v0, v26

    and-int/lit16 v15, v0, 0xff

    .line 128
    .local v15, "byte2":I
    add-int/lit8 p4, v25, 0x1

    .end local v25    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    shl-int/lit8 v26, v3, 0xd

    shl-int/lit8 v27, v4, 0x5

    or-int v26, v26, v27

    ushr-int/lit8 v27, v15, 0x3

    or-int v26, v26, v27

    aput v26, p3, v25

    .line 129
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v26, p1, p2

    move/from16 v0, v26

    and-int/lit16 v0, v0, 0xff

    move/from16 v17, v0

    .line 130
    .local v17, "byte3":I
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v26, p1, v2

    move/from16 v0, v26

    and-int/lit16 v0, v0, 0xff

    move/from16 v18, v0

    .line 131
    .local v18, "byte4":I
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v26, p1, p2

    move/from16 v0, v26

    and-int/lit16 v0, v0, 0xff

    move/from16 v19, v0

    .line 132
    .local v19, "byte5":I
    add-int/lit8 v25, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v25    # "valuesOffset":I
    and-int/lit8 v26, v15, 0x7

    shl-int/lit8 v26, v26, 0x12

    shl-int/lit8 v27, v17, 0xa

    or-int v26, v26, v27

    shl-int/lit8 v27, v18, 0x2

    or-int v26, v26, v27

    ushr-int/lit8 v27, v19, 0x6

    or-int v26, v26, v27

    aput v26, p3, p4

    .line 133
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v26, p1, v2

    move/from16 v0, v26

    and-int/lit16 v0, v0, 0xff

    move/from16 v20, v0

    .line 134
    .local v20, "byte6":I
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v26, p1, p2

    move/from16 v0, v26

    and-int/lit16 v0, v0, 0xff

    move/from16 v21, v0

    .line 135
    .local v21, "byte7":I
    add-int/lit8 p4, v25, 0x1

    .end local v25    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    and-int/lit8 v26, v19, 0x3f

    shl-int/lit8 v26, v26, 0xf

    shl-int/lit8 v27, v20, 0x7

    or-int v26, v26, v27

    ushr-int/lit8 v27, v21, 0x1

    or-int v26, v26, v27

    aput v26, p3, v25

    .line 136
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v26, p1, v2

    move/from16 v0, v26

    and-int/lit16 v0, v0, 0xff

    move/from16 v22, v0

    .line 137
    .local v22, "byte8":I
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v26, p1, p2

    move/from16 v0, v26

    and-int/lit16 v0, v0, 0xff

    move/from16 v23, v0

    .line 138
    .local v23, "byte9":I
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v26, p1, v2

    move/from16 v0, v26

    and-int/lit16 v5, v0, 0xff

    .line 139
    .local v5, "byte10":I
    add-int/lit8 v25, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v25    # "valuesOffset":I
    and-int/lit8 v26, v21, 0x1

    shl-int/lit8 v26, v26, 0x14

    shl-int/lit8 v27, v22, 0xc

    or-int v26, v26, v27

    shl-int/lit8 v27, v23, 0x4

    or-int v26, v26, v27

    ushr-int/lit8 v27, v5, 0x4

    or-int v26, v26, v27

    aput v26, p3, p4

    .line 140
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v26, p1, p2

    move/from16 v0, v26

    and-int/lit16 v6, v0, 0xff

    .line 141
    .local v6, "byte11":I
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v26, p1, v2

    move/from16 v0, v26

    and-int/lit16 v7, v0, 0xff

    .line 142
    .local v7, "byte12":I
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v26, p1, p2

    move/from16 v0, v26

    and-int/lit16 v8, v0, 0xff

    .line 143
    .local v8, "byte13":I
    add-int/lit8 p4, v25, 0x1

    .end local v25    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    and-int/lit8 v26, v5, 0xf

    shl-int/lit8 v26, v26, 0x11

    shl-int/lit8 v27, v6, 0x9

    or-int v26, v26, v27

    shl-int/lit8 v27, v7, 0x1

    or-int v26, v26, v27

    ushr-int/lit8 v27, v8, 0x7

    or-int v26, v26, v27

    aput v26, p3, v25

    .line 144
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v26, p1, v2

    move/from16 v0, v26

    and-int/lit16 v9, v0, 0xff

    .line 145
    .local v9, "byte14":I
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v26, p1, p2

    move/from16 v0, v26

    and-int/lit16 v10, v0, 0xff

    .line 146
    .local v10, "byte15":I
    add-int/lit8 v25, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v25    # "valuesOffset":I
    and-int/lit8 v26, v8, 0x7f

    shl-int/lit8 v26, v26, 0xe

    shl-int/lit8 v27, v9, 0x6

    or-int v26, v26, v27

    ushr-int/lit8 v27, v10, 0x2

    or-int v26, v26, v27

    aput v26, p3, p4

    .line 147
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v26, p1, v2

    move/from16 v0, v26

    and-int/lit16 v11, v0, 0xff

    .line 148
    .local v11, "byte16":I
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v26, p1, p2

    move/from16 v0, v26

    and-int/lit16 v12, v0, 0xff

    .line 149
    .local v12, "byte17":I
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v26, p1, v2

    move/from16 v0, v26

    and-int/lit16 v13, v0, 0xff

    .line 150
    .local v13, "byte18":I
    add-int/lit8 p4, v25, 0x1

    .end local v25    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    and-int/lit8 v26, v10, 0x3

    shl-int/lit8 v26, v26, 0x13

    shl-int/lit8 v27, v11, 0xb

    or-int v26, v26, v27

    shl-int/lit8 v27, v12, 0x3

    or-int v26, v26, v27

    ushr-int/lit8 v27, v13, 0x5

    or-int v26, v26, v27

    aput v26, p3, v25

    .line 151
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v26, p1, p2

    move/from16 v0, v26

    and-int/lit16 v14, v0, 0xff

    .line 152
    .local v14, "byte19":I
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v26, p1, v2

    move/from16 v0, v26

    and-int/lit16 v0, v0, 0xff

    move/from16 v16, v0

    .line 153
    .local v16, "byte20":I
    add-int/lit8 v25, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v25    # "valuesOffset":I
    and-int/lit8 v26, v13, 0x1f

    shl-int/lit8 v26, v26, 0x10

    shl-int/lit8 v27, v14, 0x8

    or-int v26, v26, v27

    or-int v26, v26, v16

    aput v26, p3, p4

    .line 124
    add-int/lit8 v24, v24, 0x1

    move/from16 v2, p2

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    goto/16 :goto_0
.end method

.method public decode([BI[JII)V
    .locals 52
    .param p1, "blocks"    # [B
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [J
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 250
    const/4 v3, 0x0

    .local v3, "i":I
    move/from16 v46, p4

    .end local p4    # "valuesOffset":I
    .local v46, "valuesOffset":I
    move/from16 v2, p2

    .end local p2    # "blocksOffset":I
    .local v2, "blocksOffset":I
    :goto_0
    move/from16 v0, p5

    if-lt v3, v0, :cond_0

    .line 281
    return-void

    .line 251
    :cond_0
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v47, p1, v2

    move/from16 v0, v47

    and-int/lit16 v0, v0, 0xff

    move/from16 v47, v0

    move/from16 v0, v47

    int-to-long v4, v0

    .line 252
    .local v4, "byte0":J
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v47, p1, p2

    move/from16 v0, v47

    and-int/lit16 v0, v0, 0xff

    move/from16 v47, v0

    move/from16 v0, v47

    int-to-long v6, v0

    .line 253
    .local v6, "byte1":J
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v47, p1, v2

    move/from16 v0, v47

    and-int/lit16 v0, v0, 0xff

    move/from16 v47, v0

    move/from16 v0, v47

    int-to-long v0, v0

    move-wide/from16 v28, v0

    .line 254
    .local v28, "byte2":J
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v47, 0xd

    shl-long v48, v4, v47

    const/16 v47, 0x5

    shl-long v50, v6, v47

    or-long v48, v48, v50

    const/16 v47, 0x3

    ushr-long v50, v28, v47

    or-long v48, v48, v50

    aput-wide v48, p3, v46

    .line 255
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v47, p1, p2

    move/from16 v0, v47

    and-int/lit16 v0, v0, 0xff

    move/from16 v47, v0

    move/from16 v0, v47

    int-to-long v0, v0

    move-wide/from16 v32, v0

    .line 256
    .local v32, "byte3":J
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v47, p1, v2

    move/from16 v0, v47

    and-int/lit16 v0, v0, 0xff

    move/from16 v47, v0

    move/from16 v0, v47

    int-to-long v0, v0

    move-wide/from16 v34, v0

    .line 257
    .local v34, "byte4":J
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v47, p1, p2

    move/from16 v0, v47

    and-int/lit16 v0, v0, 0xff

    move/from16 v47, v0

    move/from16 v0, v47

    int-to-long v0, v0

    move-wide/from16 v36, v0

    .line 258
    .local v36, "byte5":J
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const-wide/16 v48, 0x7

    and-long v48, v48, v28

    const/16 v47, 0x12

    shl-long v48, v48, v47

    const/16 v47, 0xa

    shl-long v50, v32, v47

    or-long v48, v48, v50

    const/16 v47, 0x2

    shl-long v50, v34, v47

    or-long v48, v48, v50

    const/16 v47, 0x6

    ushr-long v50, v36, v47

    or-long v48, v48, v50

    aput-wide v48, p3, p4

    .line 259
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v47, p1, v2

    move/from16 v0, v47

    and-int/lit16 v0, v0, 0xff

    move/from16 v47, v0

    move/from16 v0, v47

    int-to-long v0, v0

    move-wide/from16 v38, v0

    .line 260
    .local v38, "byte6":J
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v47, p1, p2

    move/from16 v0, v47

    and-int/lit16 v0, v0, 0xff

    move/from16 v47, v0

    move/from16 v0, v47

    int-to-long v0, v0

    move-wide/from16 v40, v0

    .line 261
    .local v40, "byte7":J
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v48, 0x3f

    and-long v48, v48, v36

    const/16 v47, 0xf

    shl-long v48, v48, v47

    const/16 v47, 0x7

    shl-long v50, v38, v47

    or-long v48, v48, v50

    const/16 v47, 0x1

    ushr-long v50, v40, v47

    or-long v48, v48, v50

    aput-wide v48, p3, v46

    .line 262
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v47, p1, v2

    move/from16 v0, v47

    and-int/lit16 v0, v0, 0xff

    move/from16 v47, v0

    move/from16 v0, v47

    int-to-long v0, v0

    move-wide/from16 v42, v0

    .line 263
    .local v42, "byte8":J
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v47, p1, p2

    move/from16 v0, v47

    and-int/lit16 v0, v0, 0xff

    move/from16 v47, v0

    move/from16 v0, v47

    int-to-long v0, v0

    move-wide/from16 v44, v0

    .line 264
    .local v44, "byte9":J
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v47, p1, v2

    move/from16 v0, v47

    and-int/lit16 v0, v0, 0xff

    move/from16 v47, v0

    move/from16 v0, v47

    int-to-long v8, v0

    .line 265
    .local v8, "byte10":J
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const-wide/16 v48, 0x1

    and-long v48, v48, v40

    const/16 v47, 0x14

    shl-long v48, v48, v47

    const/16 v47, 0xc

    shl-long v50, v42, v47

    or-long v48, v48, v50

    const/16 v47, 0x4

    shl-long v50, v44, v47

    or-long v48, v48, v50

    const/16 v47, 0x4

    ushr-long v50, v8, v47

    or-long v48, v48, v50

    aput-wide v48, p3, p4

    .line 266
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v47, p1, p2

    move/from16 v0, v47

    and-int/lit16 v0, v0, 0xff

    move/from16 v47, v0

    move/from16 v0, v47

    int-to-long v10, v0

    .line 267
    .local v10, "byte11":J
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v47, p1, v2

    move/from16 v0, v47

    and-int/lit16 v0, v0, 0xff

    move/from16 v47, v0

    move/from16 v0, v47

    int-to-long v12, v0

    .line 268
    .local v12, "byte12":J
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v47, p1, p2

    move/from16 v0, v47

    and-int/lit16 v0, v0, 0xff

    move/from16 v47, v0

    move/from16 v0, v47

    int-to-long v14, v0

    .line 269
    .local v14, "byte13":J
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v48, 0xf

    and-long v48, v48, v8

    const/16 v47, 0x11

    shl-long v48, v48, v47

    const/16 v47, 0x9

    shl-long v50, v10, v47

    or-long v48, v48, v50

    const/16 v47, 0x1

    shl-long v50, v12, v47

    or-long v48, v48, v50

    const/16 v47, 0x7

    ushr-long v50, v14, v47

    or-long v48, v48, v50

    aput-wide v48, p3, v46

    .line 270
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v47, p1, v2

    move/from16 v0, v47

    and-int/lit16 v0, v0, 0xff

    move/from16 v47, v0

    move/from16 v0, v47

    int-to-long v0, v0

    move-wide/from16 v16, v0

    .line 271
    .local v16, "byte14":J
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v47, p1, p2

    move/from16 v0, v47

    and-int/lit16 v0, v0, 0xff

    move/from16 v47, v0

    move/from16 v0, v47

    int-to-long v0, v0

    move-wide/from16 v18, v0

    .line 272
    .local v18, "byte15":J
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const-wide/16 v48, 0x7f

    and-long v48, v48, v14

    const/16 v47, 0xe

    shl-long v48, v48, v47

    const/16 v47, 0x6

    shl-long v50, v16, v47

    or-long v48, v48, v50

    const/16 v47, 0x2

    ushr-long v50, v18, v47

    or-long v48, v48, v50

    aput-wide v48, p3, p4

    .line 273
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v47, p1, v2

    move/from16 v0, v47

    and-int/lit16 v0, v0, 0xff

    move/from16 v47, v0

    move/from16 v0, v47

    int-to-long v0, v0

    move-wide/from16 v20, v0

    .line 274
    .local v20, "byte16":J
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v47, p1, p2

    move/from16 v0, v47

    and-int/lit16 v0, v0, 0xff

    move/from16 v47, v0

    move/from16 v0, v47

    int-to-long v0, v0

    move-wide/from16 v22, v0

    .line 275
    .local v22, "byte17":J
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v47, p1, v2

    move/from16 v0, v47

    and-int/lit16 v0, v0, 0xff

    move/from16 v47, v0

    move/from16 v0, v47

    int-to-long v0, v0

    move-wide/from16 v24, v0

    .line 276
    .local v24, "byte18":J
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v48, 0x3

    and-long v48, v48, v18

    const/16 v47, 0x13

    shl-long v48, v48, v47

    const/16 v47, 0xb

    shl-long v50, v20, v47

    or-long v48, v48, v50

    const/16 v47, 0x3

    shl-long v50, v22, v47

    or-long v48, v48, v50

    const/16 v47, 0x5

    ushr-long v50, v24, v47

    or-long v48, v48, v50

    aput-wide v48, p3, v46

    .line 277
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v47, p1, p2

    move/from16 v0, v47

    and-int/lit16 v0, v0, 0xff

    move/from16 v47, v0

    move/from16 v0, v47

    int-to-long v0, v0

    move-wide/from16 v26, v0

    .line 278
    .local v26, "byte19":J
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v47, p1, v2

    move/from16 v0, v47

    and-int/lit16 v0, v0, 0xff

    move/from16 v47, v0

    move/from16 v0, v47

    int-to-long v0, v0

    move-wide/from16 v30, v0

    .line 279
    .local v30, "byte20":J
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const-wide/16 v48, 0x1f

    and-long v48, v48, v24

    const/16 v47, 0x10

    shl-long v48, v48, v47

    const/16 v47, 0x8

    shl-long v50, v26, v47

    or-long v48, v48, v50

    or-long v48, v48, v30

    aput-wide v48, p3, p4

    .line 250
    add-int/lit8 v3, v3, 0x1

    move/from16 v2, p2

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    goto/16 :goto_0
.end method

.method public decode([JI[III)V
    .locals 52
    .param p1, "blocks"    # [J
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [I
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 33
    const/16 v45, 0x0

    .local v45, "i":I
    move/from16 v46, p4

    .end local p4    # "valuesOffset":I
    .local v46, "valuesOffset":I
    move/from16 v44, p2

    .end local p2    # "blocksOffset":I
    .local v44, "blocksOffset":I
    :goto_0
    move/from16 v0, v45

    move/from16 v1, p5

    if-lt v0, v1, :cond_0

    .line 120
    return-void

    .line 34
    :cond_0
    add-int/lit8 p2, v44, 0x1

    .end local v44    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v2, p1, v44

    .line 35
    .local v2, "block0":J
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v47, 0x2b

    ushr-long v48, v2, v47

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, v46

    .line 36
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const/16 v47, 0x16

    ushr-long v48, v2, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, p4

    .line 37
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v47, 0x1

    ushr-long v48, v2, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, v46

    .line 38
    add-int/lit8 v44, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v44    # "blocksOffset":I
    aget-wide v4, p1, p2

    .line 39
    .local v4, "block1":J
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const-wide/16 v48, 0x1

    and-long v48, v48, v2

    const/16 v47, 0x14

    shl-long v48, v48, v47

    const/16 v47, 0x2c

    ushr-long v50, v4, v47

    or-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, p4

    .line 40
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v47, 0x17

    ushr-long v48, v4, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, v46

    .line 41
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const/16 v47, 0x2

    ushr-long v48, v4, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, p4

    .line 42
    add-int/lit8 p2, v44, 0x1

    .end local v44    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v26, p1, v44

    .line 43
    .local v26, "block2":J
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v48, 0x3

    and-long v48, v48, v4

    const/16 v47, 0x13

    shl-long v48, v48, v47

    const/16 v47, 0x2d

    ushr-long v50, v26, v47

    or-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, v46

    .line 44
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const/16 v47, 0x18

    ushr-long v48, v26, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, p4

    .line 45
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v47, 0x3

    ushr-long v48, v26, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, v46

    .line 46
    add-int/lit8 v44, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v44    # "blocksOffset":I
    aget-wide v30, p1, p2

    .line 47
    .local v30, "block3":J
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const-wide/16 v48, 0x7

    and-long v48, v48, v26

    const/16 v47, 0x12

    shl-long v48, v48, v47

    const/16 v47, 0x2e

    ushr-long v50, v30, v47

    or-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, p4

    .line 48
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v47, 0x19

    ushr-long v48, v30, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, v46

    .line 49
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const/16 v47, 0x4

    ushr-long v48, v30, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, p4

    .line 50
    add-int/lit8 p2, v44, 0x1

    .end local v44    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v32, p1, v44

    .line 51
    .local v32, "block4":J
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v48, 0xf

    and-long v48, v48, v30

    const/16 v47, 0x11

    shl-long v48, v48, v47

    const/16 v47, 0x2f

    ushr-long v50, v32, v47

    or-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, v46

    .line 52
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const/16 v47, 0x1a

    ushr-long v48, v32, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, p4

    .line 53
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v47, 0x5

    ushr-long v48, v32, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, v46

    .line 54
    add-int/lit8 v44, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v44    # "blocksOffset":I
    aget-wide v34, p1, p2

    .line 55
    .local v34, "block5":J
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const-wide/16 v48, 0x1f

    and-long v48, v48, v32

    const/16 v47, 0x10

    shl-long v48, v48, v47

    const/16 v47, 0x30

    ushr-long v50, v34, v47

    or-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, p4

    .line 56
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v47, 0x1b

    ushr-long v48, v34, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, v46

    .line 57
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const/16 v47, 0x6

    ushr-long v48, v34, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, p4

    .line 58
    add-int/lit8 p2, v44, 0x1

    .end local v44    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v36, p1, v44

    .line 59
    .local v36, "block6":J
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v48, 0x3f

    and-long v48, v48, v34

    const/16 v47, 0xf

    shl-long v48, v48, v47

    const/16 v47, 0x31

    ushr-long v50, v36, v47

    or-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, v46

    .line 60
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const/16 v47, 0x1c

    ushr-long v48, v36, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, p4

    .line 61
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v47, 0x7

    ushr-long v48, v36, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, v46

    .line 62
    add-int/lit8 v44, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v44    # "blocksOffset":I
    aget-wide v38, p1, p2

    .line 63
    .local v38, "block7":J
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const-wide/16 v48, 0x7f

    and-long v48, v48, v36

    const/16 v47, 0xe

    shl-long v48, v48, v47

    const/16 v47, 0x32

    ushr-long v50, v38, v47

    or-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, p4

    .line 64
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v47, 0x1d

    ushr-long v48, v38, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, v46

    .line 65
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const/16 v47, 0x8

    ushr-long v48, v38, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, p4

    .line 66
    add-int/lit8 p2, v44, 0x1

    .end local v44    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v40, p1, v44

    .line 67
    .local v40, "block8":J
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v48, 0xff

    and-long v48, v48, v38

    const/16 v47, 0xd

    shl-long v48, v48, v47

    const/16 v47, 0x33

    ushr-long v50, v40, v47

    or-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, v46

    .line 68
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const/16 v47, 0x1e

    ushr-long v48, v40, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, p4

    .line 69
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v47, 0x9

    ushr-long v48, v40, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, v46

    .line 70
    add-int/lit8 v44, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v44    # "blocksOffset":I
    aget-wide v42, p1, p2

    .line 71
    .local v42, "block9":J
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const-wide/16 v48, 0x1ff

    and-long v48, v48, v40

    const/16 v47, 0xc

    shl-long v48, v48, v47

    const/16 v47, 0x34

    ushr-long v50, v42, v47

    or-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, p4

    .line 72
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v47, 0x1f

    ushr-long v48, v42, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, v46

    .line 73
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const/16 v47, 0xa

    ushr-long v48, v42, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, p4

    .line 74
    add-int/lit8 p2, v44, 0x1

    .end local v44    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v6, p1, v44

    .line 75
    .local v6, "block10":J
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v48, 0x3ff

    and-long v48, v48, v42

    const/16 v47, 0xb

    shl-long v48, v48, v47

    const/16 v47, 0x35

    ushr-long v50, v6, v47

    or-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, v46

    .line 76
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const/16 v47, 0x20

    ushr-long v48, v6, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, p4

    .line 77
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v47, 0xb

    ushr-long v48, v6, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, v46

    .line 78
    add-int/lit8 v44, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v44    # "blocksOffset":I
    aget-wide v8, p1, p2

    .line 79
    .local v8, "block11":J
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const-wide/16 v48, 0x7ff

    and-long v48, v48, v6

    const/16 v47, 0xa

    shl-long v48, v48, v47

    const/16 v47, 0x36

    ushr-long v50, v8, v47

    or-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, p4

    .line 80
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v47, 0x21

    ushr-long v48, v8, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, v46

    .line 81
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const/16 v47, 0xc

    ushr-long v48, v8, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, p4

    .line 82
    add-int/lit8 p2, v44, 0x1

    .end local v44    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v10, p1, v44

    .line 83
    .local v10, "block12":J
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v48, 0xfff

    and-long v48, v48, v8

    const/16 v47, 0x9

    shl-long v48, v48, v47

    const/16 v47, 0x37

    ushr-long v50, v10, v47

    or-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, v46

    .line 84
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const/16 v47, 0x22

    ushr-long v48, v10, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, p4

    .line 85
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v47, 0xd

    ushr-long v48, v10, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, v46

    .line 86
    add-int/lit8 v44, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v44    # "blocksOffset":I
    aget-wide v12, p1, p2

    .line 87
    .local v12, "block13":J
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const-wide/16 v48, 0x1fff

    and-long v48, v48, v10

    const/16 v47, 0x8

    shl-long v48, v48, v47

    const/16 v47, 0x38

    ushr-long v50, v12, v47

    or-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, p4

    .line 88
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v47, 0x23

    ushr-long v48, v12, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, v46

    .line 89
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const/16 v47, 0xe

    ushr-long v48, v12, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, p4

    .line 90
    add-int/lit8 p2, v44, 0x1

    .end local v44    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v14, p1, v44

    .line 91
    .local v14, "block14":J
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v48, 0x3fff

    and-long v48, v48, v12

    const/16 v47, 0x7

    shl-long v48, v48, v47

    const/16 v47, 0x39

    ushr-long v50, v14, v47

    or-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, v46

    .line 92
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const/16 v47, 0x24

    ushr-long v48, v14, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, p4

    .line 93
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v47, 0xf

    ushr-long v48, v14, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, v46

    .line 94
    add-int/lit8 v44, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v44    # "blocksOffset":I
    aget-wide v16, p1, p2

    .line 95
    .local v16, "block15":J
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const-wide/16 v48, 0x7fff

    and-long v48, v48, v14

    const/16 v47, 0x6

    shl-long v48, v48, v47

    const/16 v47, 0x3a

    ushr-long v50, v16, v47

    or-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, p4

    .line 96
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v47, 0x25

    ushr-long v48, v16, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, v46

    .line 97
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const/16 v47, 0x10

    ushr-long v48, v16, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, p4

    .line 98
    add-int/lit8 p2, v44, 0x1

    .end local v44    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v18, p1, v44

    .line 99
    .local v18, "block16":J
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/32 v48, 0xffff

    and-long v48, v48, v16

    const/16 v47, 0x5

    shl-long v48, v48, v47

    const/16 v47, 0x3b

    ushr-long v50, v18, v47

    or-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, v46

    .line 100
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const/16 v47, 0x26

    ushr-long v48, v18, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, p4

    .line 101
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v47, 0x11

    ushr-long v48, v18, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, v46

    .line 102
    add-int/lit8 v44, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v44    # "blocksOffset":I
    aget-wide v20, p1, p2

    .line 103
    .local v20, "block17":J
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const-wide/32 v48, 0x1ffff

    and-long v48, v48, v18

    const/16 v47, 0x4

    shl-long v48, v48, v47

    const/16 v47, 0x3c

    ushr-long v50, v20, v47

    or-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, p4

    .line 104
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v47, 0x27

    ushr-long v48, v20, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, v46

    .line 105
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const/16 v47, 0x12

    ushr-long v48, v20, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, p4

    .line 106
    add-int/lit8 p2, v44, 0x1

    .end local v44    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v22, p1, v44

    .line 107
    .local v22, "block18":J
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/32 v48, 0x3ffff

    and-long v48, v48, v20

    const/16 v47, 0x3

    shl-long v48, v48, v47

    const/16 v47, 0x3d

    ushr-long v50, v22, v47

    or-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, v46

    .line 108
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const/16 v47, 0x28

    ushr-long v48, v22, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, p4

    .line 109
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v47, 0x13

    ushr-long v48, v22, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, v46

    .line 110
    add-int/lit8 v44, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v44    # "blocksOffset":I
    aget-wide v24, p1, p2

    .line 111
    .local v24, "block19":J
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const-wide/32 v48, 0x7ffff

    and-long v48, v48, v22

    const/16 v47, 0x2

    shl-long v48, v48, v47

    const/16 v47, 0x3e

    ushr-long v50, v24, v47

    or-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, p4

    .line 112
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v47, 0x29

    ushr-long v48, v24, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, v46

    .line 113
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const/16 v47, 0x14

    ushr-long v48, v24, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, p4

    .line 114
    add-int/lit8 p2, v44, 0x1

    .end local v44    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v28, p1, v44

    .line 115
    .local v28, "block20":J
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/32 v48, 0xfffff

    and-long v48, v48, v24

    const/16 v47, 0x1

    shl-long v48, v48, v47

    const/16 v47, 0x3f

    ushr-long v50, v28, v47

    or-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, v46

    .line 116
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const/16 v47, 0x2a

    ushr-long v48, v28, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, p4

    .line 117
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v47, 0x15

    ushr-long v48, v28, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, v46

    .line 118
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const-wide/32 v48, 0x1fffff

    and-long v48, v48, v28

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v47, v0

    aput v47, p3, p4

    .line 33
    add-int/lit8 v45, v45, 0x1

    move/from16 v44, p2

    .end local p2    # "blocksOffset":I
    .restart local v44    # "blocksOffset":I
    goto/16 :goto_0
.end method

.method public decode([JI[JII)V
    .locals 52
    .param p1, "blocks"    # [J
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [J
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 159
    const/16 v45, 0x0

    .local v45, "i":I
    move/from16 v46, p4

    .end local p4    # "valuesOffset":I
    .local v46, "valuesOffset":I
    move/from16 v44, p2

    .end local p2    # "blocksOffset":I
    .local v44, "blocksOffset":I
    :goto_0
    move/from16 v0, v45

    move/from16 v1, p5

    if-lt v0, v1, :cond_0

    .line 246
    return-void

    .line 160
    :cond_0
    add-int/lit8 p2, v44, 0x1

    .end local v44    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v2, p1, v44

    .line 161
    .local v2, "block0":J
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v47, 0x2b

    ushr-long v48, v2, v47

    aput-wide v48, p3, v46

    .line 162
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const/16 v47, 0x16

    ushr-long v48, v2, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    aput-wide v48, p3, p4

    .line 163
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v47, 0x1

    ushr-long v48, v2, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    aput-wide v48, p3, v46

    .line 164
    add-int/lit8 v44, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v44    # "blocksOffset":I
    aget-wide v4, p1, p2

    .line 165
    .local v4, "block1":J
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const-wide/16 v48, 0x1

    and-long v48, v48, v2

    const/16 v47, 0x14

    shl-long v48, v48, v47

    const/16 v47, 0x2c

    ushr-long v50, v4, v47

    or-long v48, v48, v50

    aput-wide v48, p3, p4

    .line 166
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v47, 0x17

    ushr-long v48, v4, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    aput-wide v48, p3, v46

    .line 167
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const/16 v47, 0x2

    ushr-long v48, v4, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    aput-wide v48, p3, p4

    .line 168
    add-int/lit8 p2, v44, 0x1

    .end local v44    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v26, p1, v44

    .line 169
    .local v26, "block2":J
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v48, 0x3

    and-long v48, v48, v4

    const/16 v47, 0x13

    shl-long v48, v48, v47

    const/16 v47, 0x2d

    ushr-long v50, v26, v47

    or-long v48, v48, v50

    aput-wide v48, p3, v46

    .line 170
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const/16 v47, 0x18

    ushr-long v48, v26, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    aput-wide v48, p3, p4

    .line 171
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v47, 0x3

    ushr-long v48, v26, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    aput-wide v48, p3, v46

    .line 172
    add-int/lit8 v44, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v44    # "blocksOffset":I
    aget-wide v30, p1, p2

    .line 173
    .local v30, "block3":J
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const-wide/16 v48, 0x7

    and-long v48, v48, v26

    const/16 v47, 0x12

    shl-long v48, v48, v47

    const/16 v47, 0x2e

    ushr-long v50, v30, v47

    or-long v48, v48, v50

    aput-wide v48, p3, p4

    .line 174
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v47, 0x19

    ushr-long v48, v30, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    aput-wide v48, p3, v46

    .line 175
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const/16 v47, 0x4

    ushr-long v48, v30, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    aput-wide v48, p3, p4

    .line 176
    add-int/lit8 p2, v44, 0x1

    .end local v44    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v32, p1, v44

    .line 177
    .local v32, "block4":J
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v48, 0xf

    and-long v48, v48, v30

    const/16 v47, 0x11

    shl-long v48, v48, v47

    const/16 v47, 0x2f

    ushr-long v50, v32, v47

    or-long v48, v48, v50

    aput-wide v48, p3, v46

    .line 178
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const/16 v47, 0x1a

    ushr-long v48, v32, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    aput-wide v48, p3, p4

    .line 179
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v47, 0x5

    ushr-long v48, v32, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    aput-wide v48, p3, v46

    .line 180
    add-int/lit8 v44, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v44    # "blocksOffset":I
    aget-wide v34, p1, p2

    .line 181
    .local v34, "block5":J
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const-wide/16 v48, 0x1f

    and-long v48, v48, v32

    const/16 v47, 0x10

    shl-long v48, v48, v47

    const/16 v47, 0x30

    ushr-long v50, v34, v47

    or-long v48, v48, v50

    aput-wide v48, p3, p4

    .line 182
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v47, 0x1b

    ushr-long v48, v34, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    aput-wide v48, p3, v46

    .line 183
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const/16 v47, 0x6

    ushr-long v48, v34, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    aput-wide v48, p3, p4

    .line 184
    add-int/lit8 p2, v44, 0x1

    .end local v44    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v36, p1, v44

    .line 185
    .local v36, "block6":J
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v48, 0x3f

    and-long v48, v48, v34

    const/16 v47, 0xf

    shl-long v48, v48, v47

    const/16 v47, 0x31

    ushr-long v50, v36, v47

    or-long v48, v48, v50

    aput-wide v48, p3, v46

    .line 186
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const/16 v47, 0x1c

    ushr-long v48, v36, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    aput-wide v48, p3, p4

    .line 187
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v47, 0x7

    ushr-long v48, v36, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    aput-wide v48, p3, v46

    .line 188
    add-int/lit8 v44, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v44    # "blocksOffset":I
    aget-wide v38, p1, p2

    .line 189
    .local v38, "block7":J
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const-wide/16 v48, 0x7f

    and-long v48, v48, v36

    const/16 v47, 0xe

    shl-long v48, v48, v47

    const/16 v47, 0x32

    ushr-long v50, v38, v47

    or-long v48, v48, v50

    aput-wide v48, p3, p4

    .line 190
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v47, 0x1d

    ushr-long v48, v38, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    aput-wide v48, p3, v46

    .line 191
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const/16 v47, 0x8

    ushr-long v48, v38, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    aput-wide v48, p3, p4

    .line 192
    add-int/lit8 p2, v44, 0x1

    .end local v44    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v40, p1, v44

    .line 193
    .local v40, "block8":J
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v48, 0xff

    and-long v48, v48, v38

    const/16 v47, 0xd

    shl-long v48, v48, v47

    const/16 v47, 0x33

    ushr-long v50, v40, v47

    or-long v48, v48, v50

    aput-wide v48, p3, v46

    .line 194
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const/16 v47, 0x1e

    ushr-long v48, v40, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    aput-wide v48, p3, p4

    .line 195
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v47, 0x9

    ushr-long v48, v40, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    aput-wide v48, p3, v46

    .line 196
    add-int/lit8 v44, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v44    # "blocksOffset":I
    aget-wide v42, p1, p2

    .line 197
    .local v42, "block9":J
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const-wide/16 v48, 0x1ff

    and-long v48, v48, v40

    const/16 v47, 0xc

    shl-long v48, v48, v47

    const/16 v47, 0x34

    ushr-long v50, v42, v47

    or-long v48, v48, v50

    aput-wide v48, p3, p4

    .line 198
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v47, 0x1f

    ushr-long v48, v42, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    aput-wide v48, p3, v46

    .line 199
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const/16 v47, 0xa

    ushr-long v48, v42, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    aput-wide v48, p3, p4

    .line 200
    add-int/lit8 p2, v44, 0x1

    .end local v44    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v6, p1, v44

    .line 201
    .local v6, "block10":J
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v48, 0x3ff

    and-long v48, v48, v42

    const/16 v47, 0xb

    shl-long v48, v48, v47

    const/16 v47, 0x35

    ushr-long v50, v6, v47

    or-long v48, v48, v50

    aput-wide v48, p3, v46

    .line 202
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const/16 v47, 0x20

    ushr-long v48, v6, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    aput-wide v48, p3, p4

    .line 203
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v47, 0xb

    ushr-long v48, v6, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    aput-wide v48, p3, v46

    .line 204
    add-int/lit8 v44, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v44    # "blocksOffset":I
    aget-wide v8, p1, p2

    .line 205
    .local v8, "block11":J
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const-wide/16 v48, 0x7ff

    and-long v48, v48, v6

    const/16 v47, 0xa

    shl-long v48, v48, v47

    const/16 v47, 0x36

    ushr-long v50, v8, v47

    or-long v48, v48, v50

    aput-wide v48, p3, p4

    .line 206
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v47, 0x21

    ushr-long v48, v8, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    aput-wide v48, p3, v46

    .line 207
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const/16 v47, 0xc

    ushr-long v48, v8, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    aput-wide v48, p3, p4

    .line 208
    add-int/lit8 p2, v44, 0x1

    .end local v44    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v10, p1, v44

    .line 209
    .local v10, "block12":J
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v48, 0xfff

    and-long v48, v48, v8

    const/16 v47, 0x9

    shl-long v48, v48, v47

    const/16 v47, 0x37

    ushr-long v50, v10, v47

    or-long v48, v48, v50

    aput-wide v48, p3, v46

    .line 210
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const/16 v47, 0x22

    ushr-long v48, v10, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    aput-wide v48, p3, p4

    .line 211
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v47, 0xd

    ushr-long v48, v10, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    aput-wide v48, p3, v46

    .line 212
    add-int/lit8 v44, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v44    # "blocksOffset":I
    aget-wide v12, p1, p2

    .line 213
    .local v12, "block13":J
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const-wide/16 v48, 0x1fff

    and-long v48, v48, v10

    const/16 v47, 0x8

    shl-long v48, v48, v47

    const/16 v47, 0x38

    ushr-long v50, v12, v47

    or-long v48, v48, v50

    aput-wide v48, p3, p4

    .line 214
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v47, 0x23

    ushr-long v48, v12, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    aput-wide v48, p3, v46

    .line 215
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const/16 v47, 0xe

    ushr-long v48, v12, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    aput-wide v48, p3, p4

    .line 216
    add-int/lit8 p2, v44, 0x1

    .end local v44    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v14, p1, v44

    .line 217
    .local v14, "block14":J
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v48, 0x3fff

    and-long v48, v48, v12

    const/16 v47, 0x7

    shl-long v48, v48, v47

    const/16 v47, 0x39

    ushr-long v50, v14, v47

    or-long v48, v48, v50

    aput-wide v48, p3, v46

    .line 218
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const/16 v47, 0x24

    ushr-long v48, v14, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    aput-wide v48, p3, p4

    .line 219
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v47, 0xf

    ushr-long v48, v14, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    aput-wide v48, p3, v46

    .line 220
    add-int/lit8 v44, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v44    # "blocksOffset":I
    aget-wide v16, p1, p2

    .line 221
    .local v16, "block15":J
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const-wide/16 v48, 0x7fff

    and-long v48, v48, v14

    const/16 v47, 0x6

    shl-long v48, v48, v47

    const/16 v47, 0x3a

    ushr-long v50, v16, v47

    or-long v48, v48, v50

    aput-wide v48, p3, p4

    .line 222
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v47, 0x25

    ushr-long v48, v16, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    aput-wide v48, p3, v46

    .line 223
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const/16 v47, 0x10

    ushr-long v48, v16, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    aput-wide v48, p3, p4

    .line 224
    add-int/lit8 p2, v44, 0x1

    .end local v44    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v18, p1, v44

    .line 225
    .local v18, "block16":J
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/32 v48, 0xffff

    and-long v48, v48, v16

    const/16 v47, 0x5

    shl-long v48, v48, v47

    const/16 v47, 0x3b

    ushr-long v50, v18, v47

    or-long v48, v48, v50

    aput-wide v48, p3, v46

    .line 226
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const/16 v47, 0x26

    ushr-long v48, v18, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    aput-wide v48, p3, p4

    .line 227
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v47, 0x11

    ushr-long v48, v18, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    aput-wide v48, p3, v46

    .line 228
    add-int/lit8 v44, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v44    # "blocksOffset":I
    aget-wide v20, p1, p2

    .line 229
    .local v20, "block17":J
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const-wide/32 v48, 0x1ffff

    and-long v48, v48, v18

    const/16 v47, 0x4

    shl-long v48, v48, v47

    const/16 v47, 0x3c

    ushr-long v50, v20, v47

    or-long v48, v48, v50

    aput-wide v48, p3, p4

    .line 230
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v47, 0x27

    ushr-long v48, v20, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    aput-wide v48, p3, v46

    .line 231
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const/16 v47, 0x12

    ushr-long v48, v20, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    aput-wide v48, p3, p4

    .line 232
    add-int/lit8 p2, v44, 0x1

    .end local v44    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v22, p1, v44

    .line 233
    .local v22, "block18":J
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/32 v48, 0x3ffff

    and-long v48, v48, v20

    const/16 v47, 0x3

    shl-long v48, v48, v47

    const/16 v47, 0x3d

    ushr-long v50, v22, v47

    or-long v48, v48, v50

    aput-wide v48, p3, v46

    .line 234
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const/16 v47, 0x28

    ushr-long v48, v22, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    aput-wide v48, p3, p4

    .line 235
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v47, 0x13

    ushr-long v48, v22, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    aput-wide v48, p3, v46

    .line 236
    add-int/lit8 v44, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v44    # "blocksOffset":I
    aget-wide v24, p1, p2

    .line 237
    .local v24, "block19":J
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const-wide/32 v48, 0x7ffff

    and-long v48, v48, v22

    const/16 v47, 0x2

    shl-long v48, v48, v47

    const/16 v47, 0x3e

    ushr-long v50, v24, v47

    or-long v48, v48, v50

    aput-wide v48, p3, p4

    .line 238
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v47, 0x29

    ushr-long v48, v24, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    aput-wide v48, p3, v46

    .line 239
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const/16 v47, 0x14

    ushr-long v48, v24, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    aput-wide v48, p3, p4

    .line 240
    add-int/lit8 p2, v44, 0x1

    .end local v44    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v28, p1, v44

    .line 241
    .local v28, "block20":J
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/32 v48, 0xfffff

    and-long v48, v48, v24

    const/16 v47, 0x1

    shl-long v48, v48, v47

    const/16 v47, 0x3f

    ushr-long v50, v28, v47

    or-long v48, v48, v50

    aput-wide v48, p3, v46

    .line 242
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const/16 v47, 0x2a

    ushr-long v48, v28, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    aput-wide v48, p3, p4

    .line 243
    add-int/lit8 p4, v46, 0x1

    .end local v46    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v47, 0x15

    ushr-long v48, v28, v47

    const-wide/32 v50, 0x1fffff

    and-long v48, v48, v50

    aput-wide v48, p3, v46

    .line 244
    add-int/lit8 v46, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v46    # "valuesOffset":I
    const-wide/32 v48, 0x1fffff

    and-long v48, v48, v28

    aput-wide v48, p3, p4

    .line 159
    add-int/lit8 v45, v45, 0x1

    move/from16 v44, p2

    .end local p2    # "blocksOffset":I
    .restart local v44    # "blocksOffset":I
    goto/16 :goto_0
.end method
