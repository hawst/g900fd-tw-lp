.class final Lorg/apache/lucene/util/packed/BulkOperationPacked13;
.super Lorg/apache/lucene/util/packed/BulkOperationPacked;
.source "BulkOperationPacked13.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    const/16 v0, 0xd

    invoke-direct {p0, v0}, Lorg/apache/lucene/util/packed/BulkOperationPacked;-><init>(I)V

    .line 29
    return-void
.end method


# virtual methods
.method public decode([BI[III)V
    .locals 19
    .param p1, "blocks"    # [B
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [I
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 116
    const/4 v15, 0x0

    .local v15, "i":I
    move/from16 v16, p4

    .end local p4    # "valuesOffset":I
    .local v16, "valuesOffset":I
    move/from16 v1, p2

    .end local p2    # "blocksOffset":I
    .local v1, "blocksOffset":I
    :goto_0
    move/from16 v0, p5

    if-lt v15, v0, :cond_0

    .line 139
    return-void

    .line 117
    :cond_0
    add-int/lit8 p2, v1, 0x1

    .end local v1    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v17, p1, v1

    move/from16 v0, v17

    and-int/lit16 v2, v0, 0xff

    .line 118
    .local v2, "byte0":I
    add-int/lit8 v1, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v1    # "blocksOffset":I
    aget-byte v17, p1, p2

    move/from16 v0, v17

    and-int/lit16 v3, v0, 0xff

    .line 119
    .local v3, "byte1":I
    add-int/lit8 p4, v16, 0x1

    .end local v16    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    shl-int/lit8 v17, v2, 0x5

    ushr-int/lit8 v18, v3, 0x3

    or-int v17, v17, v18

    aput v17, p3, v16

    .line 120
    add-int/lit8 p2, v1, 0x1

    .end local v1    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v17, p1, v1

    move/from16 v0, v17

    and-int/lit16 v7, v0, 0xff

    .line 121
    .local v7, "byte2":I
    add-int/lit8 v1, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v1    # "blocksOffset":I
    aget-byte v17, p1, p2

    move/from16 v0, v17

    and-int/lit16 v8, v0, 0xff

    .line 122
    .local v8, "byte3":I
    add-int/lit8 v16, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v16    # "valuesOffset":I
    and-int/lit8 v17, v3, 0x7

    shl-int/lit8 v17, v17, 0xa

    shl-int/lit8 v18, v7, 0x2

    or-int v17, v17, v18

    ushr-int/lit8 v18, v8, 0x6

    or-int v17, v17, v18

    aput v17, p3, p4

    .line 123
    add-int/lit8 p2, v1, 0x1

    .end local v1    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v17, p1, v1

    move/from16 v0, v17

    and-int/lit16 v9, v0, 0xff

    .line 124
    .local v9, "byte4":I
    add-int/lit8 p4, v16, 0x1

    .end local v16    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    and-int/lit8 v17, v8, 0x3f

    shl-int/lit8 v17, v17, 0x7

    ushr-int/lit8 v18, v9, 0x1

    or-int v17, v17, v18

    aput v17, p3, v16

    .line 125
    add-int/lit8 v1, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v1    # "blocksOffset":I
    aget-byte v17, p1, p2

    move/from16 v0, v17

    and-int/lit16 v10, v0, 0xff

    .line 126
    .local v10, "byte5":I
    add-int/lit8 p2, v1, 0x1

    .end local v1    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v17, p1, v1

    move/from16 v0, v17

    and-int/lit16 v11, v0, 0xff

    .line 127
    .local v11, "byte6":I
    add-int/lit8 v16, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v16    # "valuesOffset":I
    and-int/lit8 v17, v9, 0x1

    shl-int/lit8 v17, v17, 0xc

    shl-int/lit8 v18, v10, 0x4

    or-int v17, v17, v18

    ushr-int/lit8 v18, v11, 0x4

    or-int v17, v17, v18

    aput v17, p3, p4

    .line 128
    add-int/lit8 v1, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v1    # "blocksOffset":I
    aget-byte v17, p1, p2

    move/from16 v0, v17

    and-int/lit16 v12, v0, 0xff

    .line 129
    .local v12, "byte7":I
    add-int/lit8 p2, v1, 0x1

    .end local v1    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v17, p1, v1

    move/from16 v0, v17

    and-int/lit16 v13, v0, 0xff

    .line 130
    .local v13, "byte8":I
    add-int/lit8 p4, v16, 0x1

    .end local v16    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    and-int/lit8 v17, v11, 0xf

    shl-int/lit8 v17, v17, 0x9

    shl-int/lit8 v18, v12, 0x1

    or-int v17, v17, v18

    ushr-int/lit8 v18, v13, 0x7

    or-int v17, v17, v18

    aput v17, p3, v16

    .line 131
    add-int/lit8 v1, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v1    # "blocksOffset":I
    aget-byte v17, p1, p2

    move/from16 v0, v17

    and-int/lit16 v14, v0, 0xff

    .line 132
    .local v14, "byte9":I
    add-int/lit8 v16, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v16    # "valuesOffset":I
    and-int/lit8 v17, v13, 0x7f

    shl-int/lit8 v17, v17, 0x6

    ushr-int/lit8 v18, v14, 0x2

    or-int v17, v17, v18

    aput v17, p3, p4

    .line 133
    add-int/lit8 p2, v1, 0x1

    .end local v1    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v17, p1, v1

    move/from16 v0, v17

    and-int/lit16 v4, v0, 0xff

    .line 134
    .local v4, "byte10":I
    add-int/lit8 v1, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v1    # "blocksOffset":I
    aget-byte v17, p1, p2

    move/from16 v0, v17

    and-int/lit16 v5, v0, 0xff

    .line 135
    .local v5, "byte11":I
    add-int/lit8 p4, v16, 0x1

    .end local v16    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    and-int/lit8 v17, v14, 0x3

    shl-int/lit8 v17, v17, 0xb

    shl-int/lit8 v18, v4, 0x3

    or-int v17, v17, v18

    ushr-int/lit8 v18, v5, 0x5

    or-int v17, v17, v18

    aput v17, p3, v16

    .line 136
    add-int/lit8 p2, v1, 0x1

    .end local v1    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v17, p1, v1

    move/from16 v0, v17

    and-int/lit16 v6, v0, 0xff

    .line 137
    .local v6, "byte12":I
    add-int/lit8 v16, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v16    # "valuesOffset":I
    and-int/lit8 v17, v5, 0x1f

    shl-int/lit8 v17, v17, 0x8

    or-int v17, v17, v6

    aput v17, p3, p4

    .line 116
    add-int/lit8 v15, v15, 0x1

    move/from16 v1, p2

    .end local p2    # "blocksOffset":I
    .restart local v1    # "blocksOffset":I
    goto/16 :goto_0
.end method

.method public decode([BI[JII)V
    .locals 36
    .param p1, "blocks"    # [B
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [J
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 226
    const/4 v3, 0x0

    .local v3, "i":I
    move/from16 v30, p4

    .end local p4    # "valuesOffset":I
    .local v30, "valuesOffset":I
    move/from16 v2, p2

    .end local p2    # "blocksOffset":I
    .local v2, "blocksOffset":I
    :goto_0
    move/from16 v0, p5

    if-lt v3, v0, :cond_0

    .line 249
    return-void

    .line 227
    :cond_0
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v31, p1, v2

    move/from16 v0, v31

    and-int/lit16 v0, v0, 0xff

    move/from16 v31, v0

    move/from16 v0, v31

    int-to-long v4, v0

    .line 228
    .local v4, "byte0":J
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v31, p1, p2

    move/from16 v0, v31

    and-int/lit16 v0, v0, 0xff

    move/from16 v31, v0

    move/from16 v0, v31

    int-to-long v6, v0

    .line 229
    .local v6, "byte1":J
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v31, 0x5

    shl-long v32, v4, v31

    const/16 v31, 0x3

    ushr-long v34, v6, v31

    or-long v32, v32, v34

    aput-wide v32, p3, v30

    .line 230
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v31, p1, v2

    move/from16 v0, v31

    and-int/lit16 v0, v0, 0xff

    move/from16 v31, v0

    move/from16 v0, v31

    int-to-long v14, v0

    .line 231
    .local v14, "byte2":J
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v31, p1, p2

    move/from16 v0, v31

    and-int/lit16 v0, v0, 0xff

    move/from16 v31, v0

    move/from16 v0, v31

    int-to-long v0, v0

    move-wide/from16 v16, v0

    .line 232
    .local v16, "byte3":J
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const-wide/16 v32, 0x7

    and-long v32, v32, v6

    const/16 v31, 0xa

    shl-long v32, v32, v31

    const/16 v31, 0x2

    shl-long v34, v14, v31

    or-long v32, v32, v34

    const/16 v31, 0x6

    ushr-long v34, v16, v31

    or-long v32, v32, v34

    aput-wide v32, p3, p4

    .line 233
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v31, p1, v2

    move/from16 v0, v31

    and-int/lit16 v0, v0, 0xff

    move/from16 v31, v0

    move/from16 v0, v31

    int-to-long v0, v0

    move-wide/from16 v18, v0

    .line 234
    .local v18, "byte4":J
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v32, 0x3f

    and-long v32, v32, v16

    const/16 v31, 0x7

    shl-long v32, v32, v31

    const/16 v31, 0x1

    ushr-long v34, v18, v31

    or-long v32, v32, v34

    aput-wide v32, p3, v30

    .line 235
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v31, p1, p2

    move/from16 v0, v31

    and-int/lit16 v0, v0, 0xff

    move/from16 v31, v0

    move/from16 v0, v31

    int-to-long v0, v0

    move-wide/from16 v20, v0

    .line 236
    .local v20, "byte5":J
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v31, p1, v2

    move/from16 v0, v31

    and-int/lit16 v0, v0, 0xff

    move/from16 v31, v0

    move/from16 v0, v31

    int-to-long v0, v0

    move-wide/from16 v22, v0

    .line 237
    .local v22, "byte6":J
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const-wide/16 v32, 0x1

    and-long v32, v32, v18

    const/16 v31, 0xc

    shl-long v32, v32, v31

    const/16 v31, 0x4

    shl-long v34, v20, v31

    or-long v32, v32, v34

    const/16 v31, 0x4

    ushr-long v34, v22, v31

    or-long v32, v32, v34

    aput-wide v32, p3, p4

    .line 238
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v31, p1, p2

    move/from16 v0, v31

    and-int/lit16 v0, v0, 0xff

    move/from16 v31, v0

    move/from16 v0, v31

    int-to-long v0, v0

    move-wide/from16 v24, v0

    .line 239
    .local v24, "byte7":J
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v31, p1, v2

    move/from16 v0, v31

    and-int/lit16 v0, v0, 0xff

    move/from16 v31, v0

    move/from16 v0, v31

    int-to-long v0, v0

    move-wide/from16 v26, v0

    .line 240
    .local v26, "byte8":J
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v32, 0xf

    and-long v32, v32, v22

    const/16 v31, 0x9

    shl-long v32, v32, v31

    const/16 v31, 0x1

    shl-long v34, v24, v31

    or-long v32, v32, v34

    const/16 v31, 0x7

    ushr-long v34, v26, v31

    or-long v32, v32, v34

    aput-wide v32, p3, v30

    .line 241
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v31, p1, p2

    move/from16 v0, v31

    and-int/lit16 v0, v0, 0xff

    move/from16 v31, v0

    move/from16 v0, v31

    int-to-long v0, v0

    move-wide/from16 v28, v0

    .line 242
    .local v28, "byte9":J
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const-wide/16 v32, 0x7f

    and-long v32, v32, v26

    const/16 v31, 0x6

    shl-long v32, v32, v31

    const/16 v31, 0x2

    ushr-long v34, v28, v31

    or-long v32, v32, v34

    aput-wide v32, p3, p4

    .line 243
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v31, p1, v2

    move/from16 v0, v31

    and-int/lit16 v0, v0, 0xff

    move/from16 v31, v0

    move/from16 v0, v31

    int-to-long v8, v0

    .line 244
    .local v8, "byte10":J
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    aget-byte v31, p1, p2

    move/from16 v0, v31

    and-int/lit16 v0, v0, 0xff

    move/from16 v31, v0

    move/from16 v0, v31

    int-to-long v10, v0

    .line 245
    .local v10, "byte11":J
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v32, 0x3

    and-long v32, v32, v28

    const/16 v31, 0xb

    shl-long v32, v32, v31

    const/16 v31, 0x3

    shl-long v34, v8, v31

    or-long v32, v32, v34

    const/16 v31, 0x5

    ushr-long v34, v10, v31

    or-long v32, v32, v34

    aput-wide v32, p3, v30

    .line 246
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-byte v31, p1, v2

    move/from16 v0, v31

    and-int/lit16 v0, v0, 0xff

    move/from16 v31, v0

    move/from16 v0, v31

    int-to-long v12, v0

    .line 247
    .local v12, "byte12":J
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const-wide/16 v32, 0x1f

    and-long v32, v32, v10

    const/16 v31, 0x8

    shl-long v32, v32, v31

    or-long v32, v32, v12

    aput-wide v32, p3, p4

    .line 226
    add-int/lit8 v3, v3, 0x1

    move/from16 v2, p2

    .end local p2    # "blocksOffset":I
    .restart local v2    # "blocksOffset":I
    goto/16 :goto_0
.end method

.method public decode([JI[III)V
    .locals 36
    .param p1, "blocks"    # [J
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [I
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 33
    const/16 v29, 0x0

    .local v29, "i":I
    move/from16 v30, p4

    .end local p4    # "valuesOffset":I
    .local v30, "valuesOffset":I
    move/from16 v28, p2

    .end local p2    # "blocksOffset":I
    .local v28, "blocksOffset":I
    :goto_0
    move/from16 v0, v29

    move/from16 v1, p5

    if-lt v0, v1, :cond_0

    .line 112
    return-void

    .line 34
    :cond_0
    add-int/lit8 p2, v28, 0x1

    .end local v28    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v2, p1, v28

    .line 35
    .local v2, "block0":J
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v31, 0x33

    ushr-long v32, v2, v31

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, v30

    .line 36
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const/16 v31, 0x26

    ushr-long v32, v2, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, p4

    .line 37
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v31, 0x19

    ushr-long v32, v2, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, v30

    .line 38
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const/16 v31, 0xc

    ushr-long v32, v2, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, p4

    .line 39
    add-int/lit8 v28, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v28    # "blocksOffset":I
    aget-wide v4, p1, p2

    .line 40
    .local v4, "block1":J
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v32, 0xfff

    and-long v32, v32, v2

    const/16 v31, 0x1

    shl-long v32, v32, v31

    const/16 v31, 0x3f

    ushr-long v34, v4, v31

    or-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, v30

    .line 41
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const/16 v31, 0x32

    ushr-long v32, v4, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, p4

    .line 42
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v31, 0x25

    ushr-long v32, v4, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, v30

    .line 43
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const/16 v31, 0x18

    ushr-long v32, v4, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, p4

    .line 44
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v31, 0xb

    ushr-long v32, v4, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, v30

    .line 45
    add-int/lit8 p2, v28, 0x1

    .end local v28    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v12, p1, v28

    .line 46
    .local v12, "block2":J
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const-wide/16 v32, 0x7ff

    and-long v32, v32, v4

    const/16 v31, 0x2

    shl-long v32, v32, v31

    const/16 v31, 0x3e

    ushr-long v34, v12, v31

    or-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, p4

    .line 47
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v31, 0x31

    ushr-long v32, v12, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, v30

    .line 48
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const/16 v31, 0x24

    ushr-long v32, v12, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, p4

    .line 49
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v31, 0x17

    ushr-long v32, v12, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, v30

    .line 50
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const/16 v31, 0xa

    ushr-long v32, v12, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, p4

    .line 51
    add-int/lit8 v28, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v28    # "blocksOffset":I
    aget-wide v14, p1, p2

    .line 52
    .local v14, "block3":J
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v32, 0x3ff

    and-long v32, v32, v12

    const/16 v31, 0x3

    shl-long v32, v32, v31

    const/16 v31, 0x3d

    ushr-long v34, v14, v31

    or-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, v30

    .line 53
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const/16 v31, 0x30

    ushr-long v32, v14, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, p4

    .line 54
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v31, 0x23

    ushr-long v32, v14, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, v30

    .line 55
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const/16 v31, 0x16

    ushr-long v32, v14, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, p4

    .line 56
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v31, 0x9

    ushr-long v32, v14, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, v30

    .line 57
    add-int/lit8 p2, v28, 0x1

    .end local v28    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v16, p1, v28

    .line 58
    .local v16, "block4":J
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const-wide/16 v32, 0x1ff

    and-long v32, v32, v14

    const/16 v31, 0x4

    shl-long v32, v32, v31

    const/16 v31, 0x3c

    ushr-long v34, v16, v31

    or-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, p4

    .line 59
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v31, 0x2f

    ushr-long v32, v16, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, v30

    .line 60
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const/16 v31, 0x22

    ushr-long v32, v16, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, p4

    .line 61
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v31, 0x15

    ushr-long v32, v16, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, v30

    .line 62
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const/16 v31, 0x8

    ushr-long v32, v16, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, p4

    .line 63
    add-int/lit8 v28, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v28    # "blocksOffset":I
    aget-wide v18, p1, p2

    .line 64
    .local v18, "block5":J
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v32, 0xff

    and-long v32, v32, v16

    const/16 v31, 0x5

    shl-long v32, v32, v31

    const/16 v31, 0x3b

    ushr-long v34, v18, v31

    or-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, v30

    .line 65
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const/16 v31, 0x2e

    ushr-long v32, v18, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, p4

    .line 66
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v31, 0x21

    ushr-long v32, v18, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, v30

    .line 67
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const/16 v31, 0x14

    ushr-long v32, v18, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, p4

    .line 68
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v31, 0x7

    ushr-long v32, v18, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, v30

    .line 69
    add-int/lit8 p2, v28, 0x1

    .end local v28    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v20, p1, v28

    .line 70
    .local v20, "block6":J
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const-wide/16 v32, 0x7f

    and-long v32, v32, v18

    const/16 v31, 0x6

    shl-long v32, v32, v31

    const/16 v31, 0x3a

    ushr-long v34, v20, v31

    or-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, p4

    .line 71
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v31, 0x2d

    ushr-long v32, v20, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, v30

    .line 72
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const/16 v31, 0x20

    ushr-long v32, v20, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, p4

    .line 73
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v31, 0x13

    ushr-long v32, v20, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, v30

    .line 74
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const/16 v31, 0x6

    ushr-long v32, v20, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, p4

    .line 75
    add-int/lit8 v28, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v28    # "blocksOffset":I
    aget-wide v22, p1, p2

    .line 76
    .local v22, "block7":J
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v32, 0x3f

    and-long v32, v32, v20

    const/16 v31, 0x7

    shl-long v32, v32, v31

    const/16 v31, 0x39

    ushr-long v34, v22, v31

    or-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, v30

    .line 77
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const/16 v31, 0x2c

    ushr-long v32, v22, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, p4

    .line 78
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v31, 0x1f

    ushr-long v32, v22, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, v30

    .line 79
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const/16 v31, 0x12

    ushr-long v32, v22, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, p4

    .line 80
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v31, 0x5

    ushr-long v32, v22, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, v30

    .line 81
    add-int/lit8 p2, v28, 0x1

    .end local v28    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v24, p1, v28

    .line 82
    .local v24, "block8":J
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const-wide/16 v32, 0x1f

    and-long v32, v32, v22

    const/16 v31, 0x8

    shl-long v32, v32, v31

    const/16 v31, 0x38

    ushr-long v34, v24, v31

    or-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, p4

    .line 83
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v31, 0x2b

    ushr-long v32, v24, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, v30

    .line 84
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const/16 v31, 0x1e

    ushr-long v32, v24, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, p4

    .line 85
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v31, 0x11

    ushr-long v32, v24, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, v30

    .line 86
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const/16 v31, 0x4

    ushr-long v32, v24, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, p4

    .line 87
    add-int/lit8 v28, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v28    # "blocksOffset":I
    aget-wide v26, p1, p2

    .line 88
    .local v26, "block9":J
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v32, 0xf

    and-long v32, v32, v24

    const/16 v31, 0x9

    shl-long v32, v32, v31

    const/16 v31, 0x37

    ushr-long v34, v26, v31

    or-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, v30

    .line 89
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const/16 v31, 0x2a

    ushr-long v32, v26, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, p4

    .line 90
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v31, 0x1d

    ushr-long v32, v26, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, v30

    .line 91
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const/16 v31, 0x10

    ushr-long v32, v26, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, p4

    .line 92
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v31, 0x3

    ushr-long v32, v26, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, v30

    .line 93
    add-int/lit8 p2, v28, 0x1

    .end local v28    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v6, p1, v28

    .line 94
    .local v6, "block10":J
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const-wide/16 v32, 0x7

    and-long v32, v32, v26

    const/16 v31, 0xa

    shl-long v32, v32, v31

    const/16 v31, 0x36

    ushr-long v34, v6, v31

    or-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, p4

    .line 95
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v31, 0x29

    ushr-long v32, v6, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, v30

    .line 96
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const/16 v31, 0x1c

    ushr-long v32, v6, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, p4

    .line 97
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v31, 0xf

    ushr-long v32, v6, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, v30

    .line 98
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const/16 v31, 0x2

    ushr-long v32, v6, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, p4

    .line 99
    add-int/lit8 v28, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v28    # "blocksOffset":I
    aget-wide v8, p1, p2

    .line 100
    .local v8, "block11":J
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v32, 0x3

    and-long v32, v32, v6

    const/16 v31, 0xb

    shl-long v32, v32, v31

    const/16 v31, 0x35

    ushr-long v34, v8, v31

    or-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, v30

    .line 101
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const/16 v31, 0x28

    ushr-long v32, v8, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, p4

    .line 102
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v31, 0x1b

    ushr-long v32, v8, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, v30

    .line 103
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const/16 v31, 0xe

    ushr-long v32, v8, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, p4

    .line 104
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v31, 0x1

    ushr-long v32, v8, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, v30

    .line 105
    add-int/lit8 p2, v28, 0x1

    .end local v28    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v10, p1, v28

    .line 106
    .local v10, "block12":J
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const-wide/16 v32, 0x1

    and-long v32, v32, v8

    const/16 v31, 0xc

    shl-long v32, v32, v31

    const/16 v31, 0x34

    ushr-long v34, v10, v31

    or-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, p4

    .line 107
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v31, 0x27

    ushr-long v32, v10, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, v30

    .line 108
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const/16 v31, 0x1a

    ushr-long v32, v10, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, p4

    .line 109
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v31, 0xd

    ushr-long v32, v10, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, v30

    .line 110
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const-wide/16 v32, 0x1fff

    and-long v32, v32, v10

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    aput v31, p3, p4

    .line 33
    add-int/lit8 v29, v29, 0x1

    move/from16 v28, p2

    .end local p2    # "blocksOffset":I
    .restart local v28    # "blocksOffset":I
    goto/16 :goto_0
.end method

.method public decode([JI[JII)V
    .locals 36
    .param p1, "blocks"    # [J
    .param p2, "blocksOffset"    # I
    .param p3, "values"    # [J
    .param p4, "valuesOffset"    # I
    .param p5, "iterations"    # I

    .prologue
    .line 143
    const/16 v29, 0x0

    .local v29, "i":I
    move/from16 v30, p4

    .end local p4    # "valuesOffset":I
    .local v30, "valuesOffset":I
    move/from16 v28, p2

    .end local p2    # "blocksOffset":I
    .local v28, "blocksOffset":I
    :goto_0
    move/from16 v0, v29

    move/from16 v1, p5

    if-lt v0, v1, :cond_0

    .line 222
    return-void

    .line 144
    :cond_0
    add-int/lit8 p2, v28, 0x1

    .end local v28    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v2, p1, v28

    .line 145
    .local v2, "block0":J
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v31, 0x33

    ushr-long v32, v2, v31

    aput-wide v32, p3, v30

    .line 146
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const/16 v31, 0x26

    ushr-long v32, v2, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    aput-wide v32, p3, p4

    .line 147
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v31, 0x19

    ushr-long v32, v2, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    aput-wide v32, p3, v30

    .line 148
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const/16 v31, 0xc

    ushr-long v32, v2, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    aput-wide v32, p3, p4

    .line 149
    add-int/lit8 v28, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v28    # "blocksOffset":I
    aget-wide v4, p1, p2

    .line 150
    .local v4, "block1":J
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v32, 0xfff

    and-long v32, v32, v2

    const/16 v31, 0x1

    shl-long v32, v32, v31

    const/16 v31, 0x3f

    ushr-long v34, v4, v31

    or-long v32, v32, v34

    aput-wide v32, p3, v30

    .line 151
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const/16 v31, 0x32

    ushr-long v32, v4, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    aput-wide v32, p3, p4

    .line 152
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v31, 0x25

    ushr-long v32, v4, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    aput-wide v32, p3, v30

    .line 153
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const/16 v31, 0x18

    ushr-long v32, v4, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    aput-wide v32, p3, p4

    .line 154
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v31, 0xb

    ushr-long v32, v4, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    aput-wide v32, p3, v30

    .line 155
    add-int/lit8 p2, v28, 0x1

    .end local v28    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v12, p1, v28

    .line 156
    .local v12, "block2":J
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const-wide/16 v32, 0x7ff

    and-long v32, v32, v4

    const/16 v31, 0x2

    shl-long v32, v32, v31

    const/16 v31, 0x3e

    ushr-long v34, v12, v31

    or-long v32, v32, v34

    aput-wide v32, p3, p4

    .line 157
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v31, 0x31

    ushr-long v32, v12, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    aput-wide v32, p3, v30

    .line 158
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const/16 v31, 0x24

    ushr-long v32, v12, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    aput-wide v32, p3, p4

    .line 159
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v31, 0x17

    ushr-long v32, v12, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    aput-wide v32, p3, v30

    .line 160
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const/16 v31, 0xa

    ushr-long v32, v12, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    aput-wide v32, p3, p4

    .line 161
    add-int/lit8 v28, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v28    # "blocksOffset":I
    aget-wide v14, p1, p2

    .line 162
    .local v14, "block3":J
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v32, 0x3ff

    and-long v32, v32, v12

    const/16 v31, 0x3

    shl-long v32, v32, v31

    const/16 v31, 0x3d

    ushr-long v34, v14, v31

    or-long v32, v32, v34

    aput-wide v32, p3, v30

    .line 163
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const/16 v31, 0x30

    ushr-long v32, v14, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    aput-wide v32, p3, p4

    .line 164
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v31, 0x23

    ushr-long v32, v14, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    aput-wide v32, p3, v30

    .line 165
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const/16 v31, 0x16

    ushr-long v32, v14, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    aput-wide v32, p3, p4

    .line 166
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v31, 0x9

    ushr-long v32, v14, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    aput-wide v32, p3, v30

    .line 167
    add-int/lit8 p2, v28, 0x1

    .end local v28    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v16, p1, v28

    .line 168
    .local v16, "block4":J
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const-wide/16 v32, 0x1ff

    and-long v32, v32, v14

    const/16 v31, 0x4

    shl-long v32, v32, v31

    const/16 v31, 0x3c

    ushr-long v34, v16, v31

    or-long v32, v32, v34

    aput-wide v32, p3, p4

    .line 169
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v31, 0x2f

    ushr-long v32, v16, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    aput-wide v32, p3, v30

    .line 170
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const/16 v31, 0x22

    ushr-long v32, v16, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    aput-wide v32, p3, p4

    .line 171
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v31, 0x15

    ushr-long v32, v16, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    aput-wide v32, p3, v30

    .line 172
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const/16 v31, 0x8

    ushr-long v32, v16, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    aput-wide v32, p3, p4

    .line 173
    add-int/lit8 v28, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v28    # "blocksOffset":I
    aget-wide v18, p1, p2

    .line 174
    .local v18, "block5":J
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v32, 0xff

    and-long v32, v32, v16

    const/16 v31, 0x5

    shl-long v32, v32, v31

    const/16 v31, 0x3b

    ushr-long v34, v18, v31

    or-long v32, v32, v34

    aput-wide v32, p3, v30

    .line 175
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const/16 v31, 0x2e

    ushr-long v32, v18, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    aput-wide v32, p3, p4

    .line 176
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v31, 0x21

    ushr-long v32, v18, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    aput-wide v32, p3, v30

    .line 177
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const/16 v31, 0x14

    ushr-long v32, v18, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    aput-wide v32, p3, p4

    .line 178
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v31, 0x7

    ushr-long v32, v18, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    aput-wide v32, p3, v30

    .line 179
    add-int/lit8 p2, v28, 0x1

    .end local v28    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v20, p1, v28

    .line 180
    .local v20, "block6":J
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const-wide/16 v32, 0x7f

    and-long v32, v32, v18

    const/16 v31, 0x6

    shl-long v32, v32, v31

    const/16 v31, 0x3a

    ushr-long v34, v20, v31

    or-long v32, v32, v34

    aput-wide v32, p3, p4

    .line 181
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v31, 0x2d

    ushr-long v32, v20, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    aput-wide v32, p3, v30

    .line 182
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const/16 v31, 0x20

    ushr-long v32, v20, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    aput-wide v32, p3, p4

    .line 183
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v31, 0x13

    ushr-long v32, v20, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    aput-wide v32, p3, v30

    .line 184
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const/16 v31, 0x6

    ushr-long v32, v20, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    aput-wide v32, p3, p4

    .line 185
    add-int/lit8 v28, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v28    # "blocksOffset":I
    aget-wide v22, p1, p2

    .line 186
    .local v22, "block7":J
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v32, 0x3f

    and-long v32, v32, v20

    const/16 v31, 0x7

    shl-long v32, v32, v31

    const/16 v31, 0x39

    ushr-long v34, v22, v31

    or-long v32, v32, v34

    aput-wide v32, p3, v30

    .line 187
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const/16 v31, 0x2c

    ushr-long v32, v22, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    aput-wide v32, p3, p4

    .line 188
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v31, 0x1f

    ushr-long v32, v22, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    aput-wide v32, p3, v30

    .line 189
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const/16 v31, 0x12

    ushr-long v32, v22, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    aput-wide v32, p3, p4

    .line 190
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v31, 0x5

    ushr-long v32, v22, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    aput-wide v32, p3, v30

    .line 191
    add-int/lit8 p2, v28, 0x1

    .end local v28    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v24, p1, v28

    .line 192
    .local v24, "block8":J
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const-wide/16 v32, 0x1f

    and-long v32, v32, v22

    const/16 v31, 0x8

    shl-long v32, v32, v31

    const/16 v31, 0x38

    ushr-long v34, v24, v31

    or-long v32, v32, v34

    aput-wide v32, p3, p4

    .line 193
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v31, 0x2b

    ushr-long v32, v24, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    aput-wide v32, p3, v30

    .line 194
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const/16 v31, 0x1e

    ushr-long v32, v24, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    aput-wide v32, p3, p4

    .line 195
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v31, 0x11

    ushr-long v32, v24, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    aput-wide v32, p3, v30

    .line 196
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const/16 v31, 0x4

    ushr-long v32, v24, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    aput-wide v32, p3, p4

    .line 197
    add-int/lit8 v28, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v28    # "blocksOffset":I
    aget-wide v26, p1, p2

    .line 198
    .local v26, "block9":J
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v32, 0xf

    and-long v32, v32, v24

    const/16 v31, 0x9

    shl-long v32, v32, v31

    const/16 v31, 0x37

    ushr-long v34, v26, v31

    or-long v32, v32, v34

    aput-wide v32, p3, v30

    .line 199
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const/16 v31, 0x2a

    ushr-long v32, v26, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    aput-wide v32, p3, p4

    .line 200
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v31, 0x1d

    ushr-long v32, v26, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    aput-wide v32, p3, v30

    .line 201
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const/16 v31, 0x10

    ushr-long v32, v26, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    aput-wide v32, p3, p4

    .line 202
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v31, 0x3

    ushr-long v32, v26, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    aput-wide v32, p3, v30

    .line 203
    add-int/lit8 p2, v28, 0x1

    .end local v28    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v6, p1, v28

    .line 204
    .local v6, "block10":J
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const-wide/16 v32, 0x7

    and-long v32, v32, v26

    const/16 v31, 0xa

    shl-long v32, v32, v31

    const/16 v31, 0x36

    ushr-long v34, v6, v31

    or-long v32, v32, v34

    aput-wide v32, p3, p4

    .line 205
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v31, 0x29

    ushr-long v32, v6, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    aput-wide v32, p3, v30

    .line 206
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const/16 v31, 0x1c

    ushr-long v32, v6, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    aput-wide v32, p3, p4

    .line 207
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v31, 0xf

    ushr-long v32, v6, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    aput-wide v32, p3, v30

    .line 208
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const/16 v31, 0x2

    ushr-long v32, v6, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    aput-wide v32, p3, p4

    .line 209
    add-int/lit8 v28, p2, 0x1

    .end local p2    # "blocksOffset":I
    .restart local v28    # "blocksOffset":I
    aget-wide v8, p1, p2

    .line 210
    .local v8, "block11":J
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const-wide/16 v32, 0x3

    and-long v32, v32, v6

    const/16 v31, 0xb

    shl-long v32, v32, v31

    const/16 v31, 0x35

    ushr-long v34, v8, v31

    or-long v32, v32, v34

    aput-wide v32, p3, v30

    .line 211
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const/16 v31, 0x28

    ushr-long v32, v8, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    aput-wide v32, p3, p4

    .line 212
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v31, 0x1b

    ushr-long v32, v8, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    aput-wide v32, p3, v30

    .line 213
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const/16 v31, 0xe

    ushr-long v32, v8, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    aput-wide v32, p3, p4

    .line 214
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v31, 0x1

    ushr-long v32, v8, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    aput-wide v32, p3, v30

    .line 215
    add-int/lit8 p2, v28, 0x1

    .end local v28    # "blocksOffset":I
    .restart local p2    # "blocksOffset":I
    aget-wide v10, p1, v28

    .line 216
    .local v10, "block12":J
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const-wide/16 v32, 0x1

    and-long v32, v32, v8

    const/16 v31, 0xc

    shl-long v32, v32, v31

    const/16 v31, 0x34

    ushr-long v34, v10, v31

    or-long v32, v32, v34

    aput-wide v32, p3, p4

    .line 217
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v31, 0x27

    ushr-long v32, v10, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    aput-wide v32, p3, v30

    .line 218
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const/16 v31, 0x1a

    ushr-long v32, v10, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    aput-wide v32, p3, p4

    .line 219
    add-int/lit8 p4, v30, 0x1

    .end local v30    # "valuesOffset":I
    .restart local p4    # "valuesOffset":I
    const/16 v31, 0xd

    ushr-long v32, v10, v31

    const-wide/16 v34, 0x1fff

    and-long v32, v32, v34

    aput-wide v32, p3, v30

    .line 220
    add-int/lit8 v30, p4, 0x1

    .end local p4    # "valuesOffset":I
    .restart local v30    # "valuesOffset":I
    const-wide/16 v32, 0x1fff

    and-long v32, v32, v10

    aput-wide v32, p3, p4

    .line 143
    add-int/lit8 v29, v29, 0x1

    move/from16 v28, p2

    .end local p2    # "blocksOffset":I
    .restart local v28    # "blocksOffset":I
    goto/16 :goto_0
.end method
