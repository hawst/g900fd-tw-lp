.class public final Lorg/apache/lucene/util/BitUtil;
.super Ljava/lang/Object;
.source "BitUtil.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static nextHighestPowerOfTwo(I)I
    .locals 1
    .param p0, "v"    # I

    .prologue
    .line 82
    add-int/lit8 p0, p0, -0x1

    .line 83
    shr-int/lit8 v0, p0, 0x1

    or-int/2addr p0, v0

    .line 84
    shr-int/lit8 v0, p0, 0x2

    or-int/2addr p0, v0

    .line 85
    shr-int/lit8 v0, p0, 0x4

    or-int/2addr p0, v0

    .line 86
    shr-int/lit8 v0, p0, 0x8

    or-int/2addr p0, v0

    .line 87
    shr-int/lit8 v0, p0, 0x10

    or-int/2addr p0, v0

    .line 88
    add-int/lit8 p0, p0, 0x1

    .line 89
    return p0
.end method

.method public static nextHighestPowerOfTwo(J)J
    .locals 4
    .param p0, "v"    # J

    .prologue
    const-wide/16 v2, 0x1

    .line 94
    sub-long/2addr p0, v2

    .line 95
    const/4 v0, 0x1

    shr-long v0, p0, v0

    or-long/2addr p0, v0

    .line 96
    const/4 v0, 0x2

    shr-long v0, p0, v0

    or-long/2addr p0, v0

    .line 97
    const/4 v0, 0x4

    shr-long v0, p0, v0

    or-long/2addr p0, v0

    .line 98
    const/16 v0, 0x8

    shr-long v0, p0, v0

    or-long/2addr p0, v0

    .line 99
    const/16 v0, 0x10

    shr-long v0, p0, v0

    or-long/2addr p0, v0

    .line 100
    const/16 v0, 0x20

    shr-long v0, p0, v0

    or-long/2addr p0, v0

    .line 101
    add-long/2addr p0, v2

    .line 102
    return-wide p0
.end method

.method public static pop_andnot([J[JII)J
    .locals 10
    .param p0, "arr1"    # [J
    .param p1, "arr2"    # [J
    .param p2, "wordOffset"    # I
    .param p3, "numWords"    # I

    .prologue
    .line 63
    const-wide/16 v2, 0x0

    .line 64
    .local v2, "popCount":J
    move v1, p2

    .local v1, "i":I
    add-int v0, p2, p3

    .local v0, "end":I
    :goto_0
    if-lt v1, v0, :cond_0

    .line 67
    return-wide v2

    .line 65
    :cond_0
    aget-wide v4, p0, v1

    aget-wide v6, p1, v1

    const-wide/16 v8, -0x1

    xor-long/2addr v6, v8

    and-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->bitCount(J)I

    move-result v4

    int-to-long v4, v4

    add-long/2addr v2, v4

    .line 64
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static pop_array([JII)J
    .locals 6
    .param p0, "arr"    # [J
    .param p1, "wordOffset"    # I
    .param p2, "numWords"    # I

    .prologue
    .line 33
    const-wide/16 v2, 0x0

    .line 34
    .local v2, "popCount":J
    move v1, p1

    .local v1, "i":I
    add-int v0, p1, p2

    .local v0, "end":I
    :goto_0
    if-lt v1, v0, :cond_0

    .line 37
    return-wide v2

    .line 35
    :cond_0
    aget-wide v4, p0, v1

    invoke-static {v4, v5}, Ljava/lang/Long;->bitCount(J)I

    move-result v4

    int-to-long v4, v4

    add-long/2addr v2, v4

    .line 34
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static pop_intersect([J[JII)J
    .locals 8
    .param p0, "arr1"    # [J
    .param p1, "arr2"    # [J
    .param p2, "wordOffset"    # I
    .param p3, "numWords"    # I

    .prologue
    .line 43
    const-wide/16 v2, 0x0

    .line 44
    .local v2, "popCount":J
    move v1, p2

    .local v1, "i":I
    add-int v0, p2, p3

    .local v0, "end":I
    :goto_0
    if-lt v1, v0, :cond_0

    .line 47
    return-wide v2

    .line 45
    :cond_0
    aget-wide v4, p0, v1

    aget-wide v6, p1, v1

    and-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->bitCount(J)I

    move-result v4

    int-to-long v4, v4

    add-long/2addr v2, v4

    .line 44
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static pop_union([J[JII)J
    .locals 8
    .param p0, "arr1"    # [J
    .param p1, "arr2"    # [J
    .param p2, "wordOffset"    # I
    .param p3, "numWords"    # I

    .prologue
    .line 53
    const-wide/16 v2, 0x0

    .line 54
    .local v2, "popCount":J
    move v1, p2

    .local v1, "i":I
    add-int v0, p2, p3

    .local v0, "end":I
    :goto_0
    if-lt v1, v0, :cond_0

    .line 57
    return-wide v2

    .line 55
    :cond_0
    aget-wide v4, p0, v1

    aget-wide v6, p1, v1

    or-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->bitCount(J)I

    move-result v4

    int-to-long v4, v4

    add-long/2addr v2, v4

    .line 54
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static pop_xor([J[JII)J
    .locals 8
    .param p0, "arr1"    # [J
    .param p1, "arr2"    # [J
    .param p2, "wordOffset"    # I
    .param p3, "numWords"    # I

    .prologue
    .line 73
    const-wide/16 v2, 0x0

    .line 74
    .local v2, "popCount":J
    move v1, p2

    .local v1, "i":I
    add-int v0, p2, p3

    .local v0, "end":I
    :goto_0
    if-lt v1, v0, :cond_0

    .line 77
    return-wide v2

    .line 75
    :cond_0
    aget-wide v4, p0, v1

    aget-wide v6, p1, v1

    xor-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->bitCount(J)I

    move-result v4

    int-to-long v4, v4

    add-long/2addr v2, v4

    .line 74
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
