.class public abstract Lorg/apache/lucene/util/NumericUtils$LongRangeBuilder;
.super Ljava/lang/Object;
.source "NumericUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/NumericUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "LongRangeBuilder"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 409
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public addRange(JJI)V
    .locals 3
    .param p1, "min"    # J
    .param p3, "max"    # J
    .param p5, "shift"    # I

    .prologue
    const/16 v2, 0xb

    .line 424
    new-instance v1, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v1, v2}, Lorg/apache/lucene/util/BytesRef;-><init>(I)V

    .local v1, "minBytes":Lorg/apache/lucene/util/BytesRef;
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0, v2}, Lorg/apache/lucene/util/BytesRef;-><init>(I)V

    .line 425
    .local v0, "maxBytes":Lorg/apache/lucene/util/BytesRef;
    invoke-static {p1, p2, p5, v1}, Lorg/apache/lucene/util/NumericUtils;->longToPrefixCodedBytes(JILorg/apache/lucene/util/BytesRef;)V

    .line 426
    invoke-static {p3, p4, p5, v0}, Lorg/apache/lucene/util/NumericUtils;->longToPrefixCodedBytes(JILorg/apache/lucene/util/BytesRef;)V

    .line 427
    invoke-virtual {p0, v1, v0}, Lorg/apache/lucene/util/NumericUtils$LongRangeBuilder;->addRange(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)V

    .line 428
    return-void
.end method

.method public addRange(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)V
    .locals 1
    .param p1, "minPrefixCoded"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "maxPrefixCoded"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 416
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
