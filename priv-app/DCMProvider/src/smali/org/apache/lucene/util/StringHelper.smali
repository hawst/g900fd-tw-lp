.class public abstract Lorg/apache/lucene/util/StringHelper;
.super Ljava/lang/Object;
.source "StringHelper.java"


# static fields
.field private static versionComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    new-instance v0, Lorg/apache/lucene/util/StringHelper$1;

    invoke-direct {v0}, Lorg/apache/lucene/util/StringHelper$1;-><init>()V

    sput-object v0, Lorg/apache/lucene/util/StringHelper;->versionComparator:Ljava/util/Comparator;

    .line 91
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    return-void
.end method

.method public static bytesDifference(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)I
    .locals 8
    .param p0, "left"    # Lorg/apache/lucene/util/BytesRef;
    .param p1, "right"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 40
    iget v6, p0, Lorg/apache/lucene/util/BytesRef;->length:I

    iget v7, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    if-ge v6, v7, :cond_1

    iget v3, p0, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 41
    .local v3, "len":I
    :goto_0
    iget-object v0, p0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 42
    .local v0, "bytesLeft":[B
    iget v4, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 43
    .local v4, "offLeft":I
    iget-object v1, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 44
    .local v1, "bytesRight":[B
    iget v5, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 45
    .local v5, "offRight":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-lt v2, v3, :cond_2

    move v2, v3

    .line 48
    .end local v2    # "i":I
    :cond_0
    return v2

    .line 40
    .end local v0    # "bytesLeft":[B
    .end local v1    # "bytesRight":[B
    .end local v3    # "len":I
    .end local v4    # "offLeft":I
    .end local v5    # "offRight":I
    :cond_1
    iget v3, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    goto :goto_0

    .line 46
    .restart local v0    # "bytesLeft":[B
    .restart local v1    # "bytesRight":[B
    .restart local v2    # "i":I
    .restart local v3    # "len":I
    .restart local v4    # "offLeft":I
    .restart local v5    # "offRight":I
    :cond_2
    add-int v6, v2, v4

    aget-byte v6, v0, v6

    add-int v7, v2, v5

    aget-byte v7, v1, v7

    if-ne v6, v7, :cond_0

    .line 45
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public static endsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z
    .locals 2
    .param p0, "ref"    # Lorg/apache/lucene/util/BytesRef;
    .param p1, "suffix"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 128
    iget v0, p0, Lorg/apache/lucene/util/BytesRef;->length:I

    iget v1, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    sub-int/2addr v0, v1

    invoke-static {p0, p1, v0}, Lorg/apache/lucene/util/StringHelper;->sliceEquals(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;I)Z

    move-result v0

    return v0
.end method

.method public static equals(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p0, "s1"    # Ljava/lang/String;
    .param p1, "s2"    # Ljava/lang/String;

    .prologue
    .line 94
    if-nez p0, :cond_1

    .line 95
    if-nez p1, :cond_0

    const/4 v0, 0x1

    .line 97
    :goto_0
    return v0

    .line 95
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 97
    :cond_1
    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public static getVersionComparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59
    sget-object v0, Lorg/apache/lucene/util/StringHelper;->versionComparator:Ljava/util/Comparator;

    return-object v0
.end method

.method private static sliceEquals(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;I)Z
    .locals 8
    .param p0, "sliceToTest"    # Lorg/apache/lucene/util/BytesRef;
    .param p1, "other"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "pos"    # I

    .prologue
    const/4 v5, 0x0

    .line 132
    if-ltz p2, :cond_0

    iget v6, p0, Lorg/apache/lucene/util/BytesRef;->length:I

    sub-int/2addr v6, p2

    iget v7, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    if-ge v6, v7, :cond_1

    .line 145
    :cond_0
    :goto_0
    return v5

    .line 135
    :cond_1
    iget v6, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    add-int v0, v6, p2

    .line 136
    .local v0, "i":I
    iget v2, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 137
    .local v2, "j":I
    iget v6, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget v7, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int v4, v6, v7

    .local v4, "k":I
    move v3, v2

    .end local v2    # "j":I
    .local v3, "j":I
    move v1, v0

    .line 139
    .end local v0    # "i":I
    .local v1, "i":I
    :goto_1
    if-lt v3, v4, :cond_2

    .line 145
    const/4 v5, 0x1

    goto :goto_0

    .line 140
    :cond_2
    iget-object v6, p0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    add-int/lit8 v0, v1, 0x1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    aget-byte v6, v6, v1

    iget-object v7, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    add-int/lit8 v2, v3, 0x1

    .end local v3    # "j":I
    .restart local v2    # "j":I
    aget-byte v7, v7, v3

    if-ne v6, v7, :cond_0

    move v3, v2

    .end local v2    # "j":I
    .restart local v3    # "j":I
    move v1, v0

    .end local v0    # "i":I
    .restart local v1    # "i":I
    goto :goto_1
.end method

.method public static startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z
    .locals 1
    .param p0, "ref"    # Lorg/apache/lucene/util/BytesRef;
    .param p1, "prefix"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 113
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lorg/apache/lucene/util/StringHelper;->sliceEquals(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;I)Z

    move-result v0

    return v0
.end method
