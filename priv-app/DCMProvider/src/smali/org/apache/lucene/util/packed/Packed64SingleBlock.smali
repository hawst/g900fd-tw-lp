.class abstract Lorg/apache/lucene/util/packed/Packed64SingleBlock;
.super Lorg/apache/lucene/util/packed/PackedInts$MutableImpl;
.source "Packed64SingleBlock.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/packed/Packed64SingleBlock$Packed64SingleBlock1;,
        Lorg/apache/lucene/util/packed/Packed64SingleBlock$Packed64SingleBlock10;,
        Lorg/apache/lucene/util/packed/Packed64SingleBlock$Packed64SingleBlock12;,
        Lorg/apache/lucene/util/packed/Packed64SingleBlock$Packed64SingleBlock16;,
        Lorg/apache/lucene/util/packed/Packed64SingleBlock$Packed64SingleBlock2;,
        Lorg/apache/lucene/util/packed/Packed64SingleBlock$Packed64SingleBlock21;,
        Lorg/apache/lucene/util/packed/Packed64SingleBlock$Packed64SingleBlock3;,
        Lorg/apache/lucene/util/packed/Packed64SingleBlock$Packed64SingleBlock32;,
        Lorg/apache/lucene/util/packed/Packed64SingleBlock$Packed64SingleBlock4;,
        Lorg/apache/lucene/util/packed/Packed64SingleBlock$Packed64SingleBlock5;,
        Lorg/apache/lucene/util/packed/Packed64SingleBlock$Packed64SingleBlock6;,
        Lorg/apache/lucene/util/packed/Packed64SingleBlock$Packed64SingleBlock7;,
        Lorg/apache/lucene/util/packed/Packed64SingleBlock$Packed64SingleBlock8;,
        Lorg/apache/lucene/util/packed/Packed64SingleBlock$Packed64SingleBlock9;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final MAX_SUPPORTED_BITS_PER_VALUE:I = 0x20

.field private static final SUPPORTED_BITS_PER_VALUE:[I


# instance fields
.field final blocks:[J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lorg/apache/lucene/util/packed/Packed64SingleBlock;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/packed/Packed64SingleBlock;->$assertionsDisabled:Z

    .line 36
    const/16 v0, 0xe

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lorg/apache/lucene/util/packed/Packed64SingleBlock;->SUPPORTED_BITS_PER_VALUE:[I

    return-void

    .line 33
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 36
    nop

    :array_0
    .array-data 4
        0x1
        0x2
        0x3
        0x4
        0x5
        0x6
        0x7
        0x8
        0x9
        0xa
        0xc
        0x10
        0x15
        0x20
    .end array-data
.end method

.method constructor <init>(II)V
    .locals 2
    .param p1, "valueCount"    # I
    .param p2, "bitsPerValue"    # I

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/util/packed/PackedInts$MutableImpl;-><init>(II)V

    .line 51
    sget-boolean v1, Lorg/apache/lucene/util/packed/Packed64SingleBlock;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    invoke-static {p2}, Lorg/apache/lucene/util/packed/Packed64SingleBlock;->isSupported(I)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 52
    :cond_0
    const/16 v1, 0x40

    div-int v0, v1, p2

    .line 53
    .local v0, "valuesPerBlock":I
    invoke-static {p1, v0}, Lorg/apache/lucene/util/packed/Packed64SingleBlock;->requiredCapacity(II)I

    move-result v1

    new-array v1, v1, [J

    iput-object v1, p0, Lorg/apache/lucene/util/packed/Packed64SingleBlock;->blocks:[J

    .line 54
    return-void
.end method

.method public static create(II)Lorg/apache/lucene/util/packed/Packed64SingleBlock;
    .locals 2
    .param p0, "valueCount"    # I
    .param p1, "bitsPerValue"    # I

    .prologue
    .line 219
    packed-switch p1, :pswitch_data_0

    .line 249
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unsupported number of bits per value: 32"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 221
    :pswitch_1
    new-instance v0, Lorg/apache/lucene/util/packed/Packed64SingleBlock$Packed64SingleBlock1;

    invoke-direct {v0, p0}, Lorg/apache/lucene/util/packed/Packed64SingleBlock$Packed64SingleBlock1;-><init>(I)V

    .line 247
    :goto_0
    return-object v0

    .line 223
    :pswitch_2
    new-instance v0, Lorg/apache/lucene/util/packed/Packed64SingleBlock$Packed64SingleBlock2;

    invoke-direct {v0, p0}, Lorg/apache/lucene/util/packed/Packed64SingleBlock$Packed64SingleBlock2;-><init>(I)V

    goto :goto_0

    .line 225
    :pswitch_3
    new-instance v0, Lorg/apache/lucene/util/packed/Packed64SingleBlock$Packed64SingleBlock3;

    invoke-direct {v0, p0}, Lorg/apache/lucene/util/packed/Packed64SingleBlock$Packed64SingleBlock3;-><init>(I)V

    goto :goto_0

    .line 227
    :pswitch_4
    new-instance v0, Lorg/apache/lucene/util/packed/Packed64SingleBlock$Packed64SingleBlock4;

    invoke-direct {v0, p0}, Lorg/apache/lucene/util/packed/Packed64SingleBlock$Packed64SingleBlock4;-><init>(I)V

    goto :goto_0

    .line 229
    :pswitch_5
    new-instance v0, Lorg/apache/lucene/util/packed/Packed64SingleBlock$Packed64SingleBlock5;

    invoke-direct {v0, p0}, Lorg/apache/lucene/util/packed/Packed64SingleBlock$Packed64SingleBlock5;-><init>(I)V

    goto :goto_0

    .line 231
    :pswitch_6
    new-instance v0, Lorg/apache/lucene/util/packed/Packed64SingleBlock$Packed64SingleBlock6;

    invoke-direct {v0, p0}, Lorg/apache/lucene/util/packed/Packed64SingleBlock$Packed64SingleBlock6;-><init>(I)V

    goto :goto_0

    .line 233
    :pswitch_7
    new-instance v0, Lorg/apache/lucene/util/packed/Packed64SingleBlock$Packed64SingleBlock7;

    invoke-direct {v0, p0}, Lorg/apache/lucene/util/packed/Packed64SingleBlock$Packed64SingleBlock7;-><init>(I)V

    goto :goto_0

    .line 235
    :pswitch_8
    new-instance v0, Lorg/apache/lucene/util/packed/Packed64SingleBlock$Packed64SingleBlock8;

    invoke-direct {v0, p0}, Lorg/apache/lucene/util/packed/Packed64SingleBlock$Packed64SingleBlock8;-><init>(I)V

    goto :goto_0

    .line 237
    :pswitch_9
    new-instance v0, Lorg/apache/lucene/util/packed/Packed64SingleBlock$Packed64SingleBlock9;

    invoke-direct {v0, p0}, Lorg/apache/lucene/util/packed/Packed64SingleBlock$Packed64SingleBlock9;-><init>(I)V

    goto :goto_0

    .line 239
    :pswitch_a
    new-instance v0, Lorg/apache/lucene/util/packed/Packed64SingleBlock$Packed64SingleBlock10;

    invoke-direct {v0, p0}, Lorg/apache/lucene/util/packed/Packed64SingleBlock$Packed64SingleBlock10;-><init>(I)V

    goto :goto_0

    .line 241
    :pswitch_b
    new-instance v0, Lorg/apache/lucene/util/packed/Packed64SingleBlock$Packed64SingleBlock12;

    invoke-direct {v0, p0}, Lorg/apache/lucene/util/packed/Packed64SingleBlock$Packed64SingleBlock12;-><init>(I)V

    goto :goto_0

    .line 243
    :pswitch_c
    new-instance v0, Lorg/apache/lucene/util/packed/Packed64SingleBlock$Packed64SingleBlock16;

    invoke-direct {v0, p0}, Lorg/apache/lucene/util/packed/Packed64SingleBlock$Packed64SingleBlock16;-><init>(I)V

    goto :goto_0

    .line 245
    :pswitch_d
    new-instance v0, Lorg/apache/lucene/util/packed/Packed64SingleBlock$Packed64SingleBlock21;

    invoke-direct {v0, p0}, Lorg/apache/lucene/util/packed/Packed64SingleBlock$Packed64SingleBlock21;-><init>(I)V

    goto :goto_0

    .line 247
    :pswitch_e
    new-instance v0, Lorg/apache/lucene/util/packed/Packed64SingleBlock$Packed64SingleBlock32;

    invoke-direct {v0, p0}, Lorg/apache/lucene/util/packed/Packed64SingleBlock$Packed64SingleBlock32;-><init>(I)V

    goto :goto_0

    .line 219
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_0
        :pswitch_b
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_c
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_d
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_e
    .end packed-switch
.end method

.method public static create(Lorg/apache/lucene/store/DataInput;II)Lorg/apache/lucene/util/packed/Packed64SingleBlock;
    .locals 6
    .param p0, "in"    # Lorg/apache/lucene/store/DataInput;
    .param p1, "valueCount"    # I
    .param p2, "bitsPerValue"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 211
    invoke-static {p1, p2}, Lorg/apache/lucene/util/packed/Packed64SingleBlock;->create(II)Lorg/apache/lucene/util/packed/Packed64SingleBlock;

    move-result-object v1

    .line 212
    .local v1, "reader":Lorg/apache/lucene/util/packed/Packed64SingleBlock;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, v1, Lorg/apache/lucene/util/packed/Packed64SingleBlock;->blocks:[J

    array-length v2, v2

    if-lt v0, v2, :cond_0

    .line 215
    return-object v1

    .line 213
    :cond_0
    iget-object v2, v1, Lorg/apache/lucene/util/packed/Packed64SingleBlock;->blocks:[J

    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readLong()J

    move-result-wide v4

    aput-wide v4, v2, v0

    .line 212
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static isSupported(I)Z
    .locals 1
    .param p0, "bitsPerValue"    # I

    .prologue
    .line 39
    sget-object v0, Lorg/apache/lucene/util/packed/Packed64SingleBlock;->SUPPORTED_BITS_PER_VALUE:[I

    invoke-static {v0, p0}, Ljava/util/Arrays;->binarySearch([II)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static requiredCapacity(II)I
    .locals 2
    .param p0, "valueCount"    # I
    .param p1, "valuesPerBlock"    # I

    .prologue
    .line 43
    div-int v1, p0, p1

    .line 44
    rem-int v0, p0, p1

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 43
    :goto_0
    add-int/2addr v0, v1

    return v0

    .line 44
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public clear()V
    .locals 4

    .prologue
    .line 58
    iget-object v0, p0, Lorg/apache/lucene/util/packed/Packed64SingleBlock;->blocks:[J

    const-wide/16 v2, 0x0

    invoke-static {v0, v2, v3}, Ljava/util/Arrays;->fill([JJ)V

    .line 59
    return-void
.end method

.method public fill(IIJ)V
    .locals 11
    .param p1, "fromIndex"    # I
    .param p2, "toIndex"    # I
    .param p3, "val"    # J

    .prologue
    .line 160
    sget-boolean v8, Lorg/apache/lucene/util/packed/Packed64SingleBlock;->$assertionsDisabled:Z

    if-nez v8, :cond_0

    if-gez p1, :cond_0

    new-instance v8, Ljava/lang/AssertionError;

    invoke-direct {v8}, Ljava/lang/AssertionError;-><init>()V

    throw v8

    .line 161
    :cond_0
    sget-boolean v8, Lorg/apache/lucene/util/packed/Packed64SingleBlock;->$assertionsDisabled:Z

    if-nez v8, :cond_1

    if-le p1, p2, :cond_1

    new-instance v8, Ljava/lang/AssertionError;

    invoke-direct {v8}, Ljava/lang/AssertionError;-><init>()V

    throw v8

    .line 162
    :cond_1
    sget-boolean v8, Lorg/apache/lucene/util/packed/Packed64SingleBlock;->$assertionsDisabled:Z

    if-nez v8, :cond_2

    invoke-static {p3, p4}, Lorg/apache/lucene/util/packed/PackedInts;->bitsRequired(J)I

    move-result v8

    iget v9, p0, Lorg/apache/lucene/util/packed/Packed64SingleBlock;->bitsPerValue:I

    if-le v8, v9, :cond_2

    new-instance v8, Ljava/lang/AssertionError;

    invoke-direct {v8}, Ljava/lang/AssertionError;-><init>()V

    throw v8

    .line 164
    :cond_2
    const/16 v8, 0x40

    iget v9, p0, Lorg/apache/lucene/util/packed/Packed64SingleBlock;->bitsPerValue:I

    div-int v7, v8, v9

    .line 165
    .local v7, "valuesPerBlock":I
    sub-int v8, p2, p1

    shl-int/lit8 v9, v7, 0x1

    if-gt v8, v9, :cond_4

    .line 168
    invoke-super {p0, p1, p2, p3, p4}, Lorg/apache/lucene/util/packed/PackedInts$MutableImpl;->fill(IIJ)V

    .line 196
    :cond_3
    return-void

    .line 173
    :cond_4
    rem-int v4, p1, v7

    .line 174
    .local v4, "fromOffsetInBlock":I
    if-eqz v4, :cond_7

    .line 175
    move v5, v4

    .local v5, "i":I
    move v3, p1

    .end local p1    # "fromIndex":I
    .local v3, "fromIndex":I
    :goto_0
    if-lt v5, v7, :cond_5

    .line 178
    sget-boolean v8, Lorg/apache/lucene/util/packed/Packed64SingleBlock;->$assertionsDisabled:Z

    if-nez v8, :cond_6

    rem-int v8, v3, v7

    if-eqz v8, :cond_6

    new-instance v8, Ljava/lang/AssertionError;

    invoke-direct {v8}, Ljava/lang/AssertionError;-><init>()V

    throw v8

    .line 176
    :cond_5
    add-int/lit8 p1, v3, 0x1

    .end local v3    # "fromIndex":I
    .restart local p1    # "fromIndex":I
    invoke-virtual {p0, v3, p3, p4}, Lorg/apache/lucene/util/packed/Packed64SingleBlock;->set(IJ)V

    .line 175
    add-int/lit8 v5, v5, 0x1

    move v3, p1

    .end local p1    # "fromIndex":I
    .restart local v3    # "fromIndex":I
    goto :goto_0

    :cond_6
    move p1, v3

    .line 182
    .end local v3    # "fromIndex":I
    .end local v5    # "i":I
    .restart local p1    # "fromIndex":I
    :cond_7
    div-int v2, p1, v7

    .line 183
    .local v2, "fromBlock":I
    div-int v6, p2, v7

    .line 184
    .local v6, "toBlock":I
    sget-boolean v8, Lorg/apache/lucene/util/packed/Packed64SingleBlock;->$assertionsDisabled:Z

    if-nez v8, :cond_8

    mul-int v8, v2, v7

    if-eq v8, p1, :cond_8

    new-instance v8, Ljava/lang/AssertionError;

    invoke-direct {v8}, Ljava/lang/AssertionError;-><init>()V

    throw v8

    .line 186
    :cond_8
    const-wide/16 v0, 0x0

    .line 187
    .local v0, "blockValue":J
    const/4 v5, 0x0

    .restart local v5    # "i":I
    :goto_1
    if-lt v5, v7, :cond_9

    .line 190
    iget-object v8, p0, Lorg/apache/lucene/util/packed/Packed64SingleBlock;->blocks:[J

    invoke-static {v8, v2, v6, v0, v1}, Ljava/util/Arrays;->fill([JIIJ)V

    .line 193
    mul-int v5, v7, v6

    :goto_2
    if-ge v5, p2, :cond_3

    .line 194
    invoke-virtual {p0, v5, p3, p4}, Lorg/apache/lucene/util/packed/Packed64SingleBlock;->set(IJ)V

    .line 193
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 188
    :cond_9
    iget v8, p0, Lorg/apache/lucene/util/packed/Packed64SingleBlock;->bitsPerValue:I

    mul-int/2addr v8, v5

    shl-long v8, p3, v8

    or-long/2addr v0, v8

    .line 187
    add-int/lit8 v5, v5, 0x1

    goto :goto_1
.end method

.method public get(I[JII)I
    .locals 18
    .param p1, "index"    # I
    .param p2, "arr"    # [J
    .param p3, "off"    # I
    .param p4, "len"    # I

    .prologue
    .line 72
    sget-boolean v3, Lorg/apache/lucene/util/packed/Packed64SingleBlock;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    if-gtz p4, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "len must be > 0 (got "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p4

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 73
    :cond_0
    sget-boolean v3, Lorg/apache/lucene/util/packed/Packed64SingleBlock;->$assertionsDisabled:Z

    if-nez v3, :cond_2

    if-ltz p1, :cond_1

    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/util/packed/Packed64SingleBlock;->valueCount:I

    move/from16 v0, p1

    if-lt v0, v3, :cond_2

    :cond_1
    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 74
    :cond_2
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/util/packed/Packed64SingleBlock;->valueCount:I

    sub-int v3, v3, p1

    move/from16 v0, p4

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result p4

    .line 75
    sget-boolean v3, Lorg/apache/lucene/util/packed/Packed64SingleBlock;->$assertionsDisabled:Z

    if-nez v3, :cond_3

    add-int v3, p3, p4

    move-object/from16 v0, p2

    array-length v5, v0

    if-le v3, v5, :cond_3

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 77
    :cond_3
    move/from16 v13, p1

    .line 80
    .local v13, "originalIndex":I
    const/16 v3, 0x40

    move-object/from16 v0, p0

    iget v5, v0, Lorg/apache/lucene/util/packed/Packed64SingleBlock;->bitsPerValue:I

    div-int v14, v3, v5

    .line 81
    .local v14, "valuesPerBlock":I
    rem-int v12, p1, v14

    .line 82
    .local v12, "offsetInBlock":I
    if-eqz v12, :cond_7

    .line 83
    move v9, v12

    .local v9, "i":I
    move/from16 v11, p3

    .end local p3    # "off":I
    .local v11, "off":I
    move/from16 v10, p1

    .end local p1    # "index":I
    .local v10, "index":I
    :goto_0
    if-ge v9, v14, :cond_4

    if-gtz p4, :cond_5

    .line 87
    :cond_4
    if-nez p4, :cond_6

    .line 88
    sub-int v3, v10, v13

    move/from16 p3, v11

    .end local v11    # "off":I
    .restart local p3    # "off":I
    move/from16 p1, v10

    .line 110
    .end local v9    # "i":I
    .end local v10    # "index":I
    .restart local p1    # "index":I
    :goto_1
    return v3

    .line 84
    .end local p1    # "index":I
    .end local p3    # "off":I
    .restart local v9    # "i":I
    .restart local v10    # "index":I
    .restart local v11    # "off":I
    :cond_5
    add-int/lit8 p3, v11, 0x1

    .end local v11    # "off":I
    .restart local p3    # "off":I
    add-int/lit8 p1, v10, 0x1

    .end local v10    # "index":I
    .restart local p1    # "index":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lorg/apache/lucene/util/packed/Packed64SingleBlock;->get(I)J

    move-result-wide v16

    aput-wide v16, p2, v11

    .line 85
    add-int/lit8 p4, p4, -0x1

    .line 83
    add-int/lit8 v9, v9, 0x1

    move/from16 v11, p3

    .end local p3    # "off":I
    .restart local v11    # "off":I
    move/from16 v10, p1

    .end local p1    # "index":I
    .restart local v10    # "index":I
    goto :goto_0

    :cond_6
    move/from16 p3, v11

    .end local v11    # "off":I
    .restart local p3    # "off":I
    move/from16 p1, v10

    .line 93
    .end local v9    # "i":I
    .end local v10    # "index":I
    .restart local p1    # "index":I
    :cond_7
    sget-boolean v3, Lorg/apache/lucene/util/packed/Packed64SingleBlock;->$assertionsDisabled:Z

    if-nez v3, :cond_8

    rem-int v3, p1, v14

    if-eqz v3, :cond_8

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 94
    :cond_8
    sget-object v3, Lorg/apache/lucene/util/packed/PackedInts$Format;->PACKED_SINGLE_BLOCK:Lorg/apache/lucene/util/packed/PackedInts$Format;

    move-object/from16 v0, p0

    iget v5, v0, Lorg/apache/lucene/util/packed/Packed64SingleBlock;->bitsPerValue:I

    invoke-static {v3, v5}, Lorg/apache/lucene/util/packed/BulkOperation;->of(Lorg/apache/lucene/util/packed/PackedInts$Format;I)Lorg/apache/lucene/util/packed/BulkOperation;

    move-result-object v2

    .line 95
    .local v2, "decoder":Lorg/apache/lucene/util/packed/PackedInts$Decoder;
    sget-boolean v3, Lorg/apache/lucene/util/packed/Packed64SingleBlock;->$assertionsDisabled:Z

    if-nez v3, :cond_9

    invoke-interface {v2}, Lorg/apache/lucene/util/packed/PackedInts$Decoder;->longBlockCount()I

    move-result v3

    const/4 v5, 0x1

    if-eq v3, v5, :cond_9

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 96
    :cond_9
    sget-boolean v3, Lorg/apache/lucene/util/packed/Packed64SingleBlock;->$assertionsDisabled:Z

    if-nez v3, :cond_a

    invoke-interface {v2}, Lorg/apache/lucene/util/packed/PackedInts$Decoder;->longValueCount()I

    move-result v3

    if-eq v3, v14, :cond_a

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 97
    :cond_a
    div-int v4, p1, v14

    .line 98
    .local v4, "blockIndex":I
    add-int v3, p1, p4

    div-int/2addr v3, v14

    sub-int v7, v3, v4

    .line 99
    .local v7, "nblocks":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/util/packed/Packed64SingleBlock;->blocks:[J

    move-object/from16 v5, p2

    move/from16 v6, p3

    invoke-interface/range {v2 .. v7}, Lorg/apache/lucene/util/packed/PackedInts$Decoder;->decode([JI[JII)V

    .line 100
    mul-int v8, v7, v14

    .line 101
    .local v8, "diff":I
    add-int p1, p1, v8

    sub-int p4, p4, v8

    .line 103
    move/from16 v0, p1

    if-le v0, v13, :cond_b

    .line 105
    sub-int v3, p1, v13

    goto :goto_1

    .line 109
    :cond_b
    sget-boolean v3, Lorg/apache/lucene/util/packed/Packed64SingleBlock;->$assertionsDisabled:Z

    if-nez v3, :cond_c

    move/from16 v0, p1

    if-eq v0, v13, :cond_c

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 110
    :cond_c
    invoke-super/range {p0 .. p4}, Lorg/apache/lucene/util/packed/PackedInts$MutableImpl;->get(I[JII)I

    move-result v3

    goto/16 :goto_1
.end method

.method protected getFormat()Lorg/apache/lucene/util/packed/PackedInts$Format;
    .locals 1

    .prologue
    .line 200
    sget-object v0, Lorg/apache/lucene/util/packed/PackedInts$Format;->PACKED_SINGLE_BLOCK:Lorg/apache/lucene/util/packed/PackedInts$Format;

    return-object v0
.end method

.method public ramBytesUsed()J
    .locals 4

    .prologue
    .line 64
    sget v0, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_HEADER:I

    add-int/lit8 v0, v0, 0x8

    .line 66
    sget v1, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_REF:I

    .line 64
    add-int/2addr v0, v1

    int-to-long v0, v0

    .line 63
    invoke-static {v0, v1}, Lorg/apache/lucene/util/RamUsageEstimator;->alignObjectSize(J)J

    move-result-wide v0

    .line 67
    iget-object v2, p0, Lorg/apache/lucene/util/packed/Packed64SingleBlock;->blocks:[J

    invoke-static {v2}, Lorg/apache/lucene/util/RamUsageEstimator;->sizeOf([J)J

    move-result-wide v2

    .line 63
    add-long/2addr v0, v2

    return-wide v0
.end method

.method public set(I[JII)I
    .locals 15
    .param p1, "index"    # I
    .param p2, "arr"    # [J
    .param p3, "off"    # I
    .param p4, "len"    # I

    .prologue
    .line 116
    sget-boolean v3, Lorg/apache/lucene/util/packed/Packed64SingleBlock;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    if-gtz p4, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "len must be > 0 (got "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 117
    :cond_0
    sget-boolean v3, Lorg/apache/lucene/util/packed/Packed64SingleBlock;->$assertionsDisabled:Z

    if-nez v3, :cond_2

    if-ltz p1, :cond_1

    iget v3, p0, Lorg/apache/lucene/util/packed/Packed64SingleBlock;->valueCount:I

    move/from16 v0, p1

    if-lt v0, v3, :cond_2

    :cond_1
    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 118
    :cond_2
    iget v3, p0, Lorg/apache/lucene/util/packed/Packed64SingleBlock;->valueCount:I

    sub-int v3, v3, p1

    move/from16 v0, p4

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result p4

    .line 119
    sget-boolean v3, Lorg/apache/lucene/util/packed/Packed64SingleBlock;->$assertionsDisabled:Z

    if-nez v3, :cond_3

    add-int v3, p3, p4

    move-object/from16 v0, p2

    array-length v4, v0

    if-le v3, v4, :cond_3

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 121
    :cond_3
    move/from16 v13, p1

    .line 124
    .local v13, "originalIndex":I
    const/16 v3, 0x40

    iget v4, p0, Lorg/apache/lucene/util/packed/Packed64SingleBlock;->bitsPerValue:I

    div-int v14, v3, v4

    .line 125
    .local v14, "valuesPerBlock":I
    rem-int v12, p1, v14

    .line 126
    .local v12, "offsetInBlock":I
    if-eqz v12, :cond_7

    .line 127
    move v9, v12

    .local v9, "i":I
    move/from16 v11, p3

    .end local p3    # "off":I
    .local v11, "off":I
    move/from16 v10, p1

    .end local p1    # "index":I
    .local v10, "index":I
    :goto_0
    if-ge v9, v14, :cond_4

    if-gtz p4, :cond_5

    .line 131
    :cond_4
    if-nez p4, :cond_6

    .line 132
    sub-int v3, v10, v13

    move/from16 p3, v11

    .end local v11    # "off":I
    .restart local p3    # "off":I
    move/from16 p1, v10

    .line 154
    .end local v9    # "i":I
    .end local v10    # "index":I
    .restart local p1    # "index":I
    :goto_1
    return v3

    .line 128
    .end local p1    # "index":I
    .end local p3    # "off":I
    .restart local v9    # "i":I
    .restart local v10    # "index":I
    .restart local v11    # "off":I
    :cond_5
    add-int/lit8 p1, v10, 0x1

    .end local v10    # "index":I
    .restart local p1    # "index":I
    add-int/lit8 p3, v11, 0x1

    .end local v11    # "off":I
    .restart local p3    # "off":I
    aget-wide v4, p2, v11

    invoke-virtual {p0, v10, v4, v5}, Lorg/apache/lucene/util/packed/Packed64SingleBlock;->set(IJ)V

    .line 129
    add-int/lit8 p4, p4, -0x1

    .line 127
    add-int/lit8 v9, v9, 0x1

    move/from16 v11, p3

    .end local p3    # "off":I
    .restart local v11    # "off":I
    move/from16 v10, p1

    .end local p1    # "index":I
    .restart local v10    # "index":I
    goto :goto_0

    :cond_6
    move/from16 p3, v11

    .end local v11    # "off":I
    .restart local p3    # "off":I
    move/from16 p1, v10

    .line 137
    .end local v9    # "i":I
    .end local v10    # "index":I
    .restart local p1    # "index":I
    :cond_7
    sget-boolean v3, Lorg/apache/lucene/util/packed/Packed64SingleBlock;->$assertionsDisabled:Z

    if-nez v3, :cond_8

    rem-int v3, p1, v14

    if-eqz v3, :cond_8

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 138
    :cond_8
    sget-object v3, Lorg/apache/lucene/util/packed/PackedInts$Format;->PACKED_SINGLE_BLOCK:Lorg/apache/lucene/util/packed/PackedInts$Format;

    iget v4, p0, Lorg/apache/lucene/util/packed/Packed64SingleBlock;->bitsPerValue:I

    invoke-static {v3, v4}, Lorg/apache/lucene/util/packed/BulkOperation;->of(Lorg/apache/lucene/util/packed/PackedInts$Format;I)Lorg/apache/lucene/util/packed/BulkOperation;

    move-result-object v2

    .line 139
    .local v2, "op":Lorg/apache/lucene/util/packed/BulkOperation;
    sget-boolean v3, Lorg/apache/lucene/util/packed/Packed64SingleBlock;->$assertionsDisabled:Z

    if-nez v3, :cond_9

    invoke-virtual {v2}, Lorg/apache/lucene/util/packed/BulkOperation;->longBlockCount()I

    move-result v3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_9

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 140
    :cond_9
    sget-boolean v3, Lorg/apache/lucene/util/packed/Packed64SingleBlock;->$assertionsDisabled:Z

    if-nez v3, :cond_a

    invoke-virtual {v2}, Lorg/apache/lucene/util/packed/BulkOperation;->longValueCount()I

    move-result v3

    if-eq v3, v14, :cond_a

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 141
    :cond_a
    div-int v6, p1, v14

    .line 142
    .local v6, "blockIndex":I
    add-int v3, p1, p4

    div-int/2addr v3, v14

    sub-int v7, v3, v6

    .line 143
    .local v7, "nblocks":I
    iget-object v5, p0, Lorg/apache/lucene/util/packed/Packed64SingleBlock;->blocks:[J

    move-object/from16 v3, p2

    move/from16 v4, p3

    invoke-virtual/range {v2 .. v7}, Lorg/apache/lucene/util/packed/BulkOperation;->encode([JI[JII)V

    .line 144
    mul-int v8, v7, v14

    .line 145
    .local v8, "diff":I
    add-int p1, p1, v8

    sub-int p4, p4, v8

    .line 147
    move/from16 v0, p1

    if-le v0, v13, :cond_b

    .line 149
    sub-int v3, p1, v13

    goto :goto_1

    .line 153
    :cond_b
    sget-boolean v3, Lorg/apache/lucene/util/packed/Packed64SingleBlock;->$assertionsDisabled:Z

    if-nez v3, :cond_c

    move/from16 v0, p1

    if-eq v0, v13, :cond_c

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 154
    :cond_c
    invoke-super/range {p0 .. p4}, Lorg/apache/lucene/util/packed/PackedInts$MutableImpl;->set(I[JII)I

    move-result v3

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 205
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "(bitsPerValue="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/util/packed/Packed64SingleBlock;->bitsPerValue:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 206
    const-string v1, ", size="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/lucene/util/packed/Packed64SingleBlock;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", elements.length="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/util/packed/Packed64SingleBlock;->blocks:[J

    array-length v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 205
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
