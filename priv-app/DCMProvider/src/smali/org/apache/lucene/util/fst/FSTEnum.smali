.class abstract Lorg/apache/lucene/util/fst/FSTEnum;
.super Ljava/lang/Object;
.source "FSTEnum.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field protected final NO_OUTPUT:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field protected arcs:[Lorg/apache/lucene/util/fst/FST$Arc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;"
        }
    .end annotation
.end field

.field protected final fst:Lorg/apache/lucene/util/fst/FST;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/fst/FST",
            "<TT;>;"
        }
    .end annotation
.end field

.field protected final fstReader:Lorg/apache/lucene/util/fst/FST$BytesReader;

.field protected output:[Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[TT;"
        }
    .end annotation
.end field

.field protected final scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;"
        }
    .end annotation
.end field

.field protected targetLength:I

.field protected upto:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lorg/apache/lucene/util/fst/FSTEnum;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/fst/FSTEnum;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected constructor <init>(Lorg/apache/lucene/util/fst/FST;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/FST",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/util/fst/FSTEnum;, "Lorg/apache/lucene/util/fst/FSTEnum<TT;>;"
    .local p1, "fst":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    const/16 v1, 0xa

    const/4 v2, 0x0

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-array v0, v1, [Lorg/apache/lucene/util/fst/FST$Arc;

    iput-object v0, p0, Lorg/apache/lucene/util/fst/FSTEnum;->arcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    .line 35
    new-array v0, v1, [Ljava/lang/Object;

    iput-object v0, p0, Lorg/apache/lucene/util/fst/FSTEnum;->output:[Ljava/lang/Object;

    .line 39
    new-instance v0, Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-direct {v0}, Lorg/apache/lucene/util/fst/FST$Arc;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/util/fst/FSTEnum;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    .line 48
    iput-object p1, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    .line 49
    invoke-virtual {p1}, Lorg/apache/lucene/util/fst/FST;->getBytesReader()Lorg/apache/lucene/util/fst/FST$BytesReader;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fstReader:Lorg/apache/lucene/util/fst/FST$BytesReader;

    .line 50
    iget-object v0, p1, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    invoke-virtual {v0}, Lorg/apache/lucene/util/fst/Outputs;->getNoOutput()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/util/fst/FSTEnum;->NO_OUTPUT:Ljava/lang/Object;

    .line 51
    invoke-direct {p0, v2}, Lorg/apache/lucene/util/fst/FSTEnum;->getArc(I)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/apache/lucene/util/fst/FST;->getFirstArc(Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 52
    iget-object v0, p0, Lorg/apache/lucene/util/fst/FSTEnum;->output:[Ljava/lang/Object;

    iget-object v1, p0, Lorg/apache/lucene/util/fst/FSTEnum;->NO_OUTPUT:Ljava/lang/Object;

    aput-object v1, v0, v2

    .line 53
    return-void
.end method

.method private getArc(I)Lorg/apache/lucene/util/fst/FST$Arc;
    .locals 2
    .param p1, "idx"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 524
    .local p0, "this":Lorg/apache/lucene/util/fst/FSTEnum;, "Lorg/apache/lucene/util/fst/FSTEnum<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/util/fst/FSTEnum;->arcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    aget-object v0, v0, p1

    if-nez v0, :cond_0

    .line 525
    iget-object v0, p0, Lorg/apache/lucene/util/fst/FSTEnum;->arcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    new-instance v1, Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-direct {v1}, Lorg/apache/lucene/util/fst/FST$Arc;-><init>()V

    aput-object v1, v0, p1

    .line 527
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/util/fst/FSTEnum;->arcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    aget-object v0, v0, p1

    return-object v0
.end method

.method private incr()V
    .locals 5

    .prologue
    .local p0, "this":Lorg/apache/lucene/util/fst/FSTEnum;, "Lorg/apache/lucene/util/fst/FSTEnum<TT;>;"
    const/4 v4, 0x0

    .line 464
    iget v2, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    .line 465
    invoke-virtual {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->grow()V

    .line 466
    iget-object v2, p0, Lorg/apache/lucene/util/fst/FSTEnum;->arcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    array-length v2, v2

    iget v3, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    if-gt v2, v3, :cond_0

    .line 468
    iget v2, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    add-int/lit8 v2, v2, 0x1

    sget v3, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_REF:I

    invoke-static {v2, v3}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v2

    new-array v0, v2, [Lorg/apache/lucene/util/fst/FST$Arc;

    .line 469
    .local v0, "newArcs":[Lorg/apache/lucene/util/fst/FST$Arc;
    iget-object v2, p0, Lorg/apache/lucene/util/fst/FSTEnum;->arcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    iget-object v3, p0, Lorg/apache/lucene/util/fst/FSTEnum;->arcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    array-length v3, v3

    invoke-static {v2, v4, v0, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 470
    iput-object v0, p0, Lorg/apache/lucene/util/fst/FSTEnum;->arcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    .line 472
    .end local v0    # "newArcs":[Lorg/apache/lucene/util/fst/FST$Arc;
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/util/fst/FSTEnum;->output:[Ljava/lang/Object;

    array-length v2, v2

    iget v3, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    if-gt v2, v3, :cond_1

    .line 474
    iget v2, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    add-int/lit8 v2, v2, 0x1

    sget v3, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_REF:I

    invoke-static {v2, v3}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v2

    new-array v1, v2, [Ljava/lang/Object;

    .line 475
    .local v1, "newOutput":[Ljava/lang/Object;
    iget-object v2, p0, Lorg/apache/lucene/util/fst/FSTEnum;->output:[Ljava/lang/Object;

    iget-object v3, p0, Lorg/apache/lucene/util/fst/FSTEnum;->output:[Ljava/lang/Object;

    array-length v3, v3

    invoke-static {v2, v4, v1, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 476
    iput-object v1, p0, Lorg/apache/lucene/util/fst/FSTEnum;->output:[Ljava/lang/Object;

    .line 478
    .end local v1    # "newOutput":[Ljava/lang/Object;
    :cond_1
    return-void
.end method

.method private pushFirst()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 484
    .local p0, "this":Lorg/apache/lucene/util/fst/FSTEnum;, "Lorg/apache/lucene/util/fst/FSTEnum<TT;>;"
    iget-object v2, p0, Lorg/apache/lucene/util/fst/FSTEnum;->arcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    iget v3, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    aget-object v0, v2, v3

    .line 485
    .local v0, "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    sget-boolean v2, Lorg/apache/lucene/util/fst/FSTEnum;->$assertionsDisabled:Z

    if-nez v2, :cond_1

    if-nez v0, :cond_1

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 494
    :cond_0
    iget v2, v0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    invoke-virtual {p0, v2}, Lorg/apache/lucene/util/fst/FSTEnum;->setCurrentLabel(I)V

    .line 495
    invoke-direct {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->incr()V

    .line 497
    iget v2, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    invoke-direct {p0, v2}, Lorg/apache/lucene/util/fst/FSTEnum;->getArc(I)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v1

    .line 498
    .local v1, "nextArc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    iget-object v2, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v3, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fstReader:Lorg/apache/lucene/util/fst/FST$BytesReader;

    invoke-virtual {v2, v0, v1, v3}, Lorg/apache/lucene/util/fst/FST;->readFirstTargetArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 499
    move-object v0, v1

    .line 488
    .end local v1    # "nextArc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/util/fst/FSTEnum;->output:[Ljava/lang/Object;

    iget v3, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    iget-object v4, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v4, v4, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    iget-object v5, p0, Lorg/apache/lucene/util/fst/FSTEnum;->output:[Ljava/lang/Object;

    iget v6, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    add-int/lit8 v6, v6, -0x1

    aget-object v5, v5, v6

    iget-object v6, v0, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    invoke-virtual {v4, v5, v6}, Lorg/apache/lucene/util/fst/Outputs;->add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v2, v3

    .line 489
    iget v2, v0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    .line 501
    return-void
.end method

.method private pushLast()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 507
    .local p0, "this":Lorg/apache/lucene/util/fst/FSTEnum;, "Lorg/apache/lucene/util/fst/FSTEnum<TT;>;"
    iget-object v1, p0, Lorg/apache/lucene/util/fst/FSTEnum;->arcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    iget v2, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    aget-object v0, v1, v2

    .line 508
    .local v0, "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    sget-boolean v1, Lorg/apache/lucene/util/fst/FSTEnum;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    if-nez v0, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 517
    :cond_0
    invoke-direct {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->incr()V

    .line 519
    iget-object v1, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    iget v2, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    invoke-direct {p0, v2}, Lorg/apache/lucene/util/fst/FSTEnum;->getArc(I)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fstReader:Lorg/apache/lucene/util/fst/FST$BytesReader;

    invoke-virtual {v1, v0, v2, v3}, Lorg/apache/lucene/util/fst/FST;->readLastTargetArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v0

    .line 511
    :cond_1
    iget v1, v0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    invoke-virtual {p0, v1}, Lorg/apache/lucene/util/fst/FSTEnum;->setCurrentLabel(I)V

    .line 512
    iget-object v1, p0, Lorg/apache/lucene/util/fst/FSTEnum;->output:[Ljava/lang/Object;

    iget v2, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    iget-object v3, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v3, v3, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    iget-object v4, p0, Lorg/apache/lucene/util/fst/FSTEnum;->output:[Ljava/lang/Object;

    iget v5, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    add-int/lit8 v5, v5, -0x1

    aget-object v4, v4, v5

    iget-object v5, v0, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    invoke-virtual {v3, v4, v5}, Lorg/apache/lucene/util/fst/Outputs;->add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v1, v2

    .line 513
    iget v1, v0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 521
    return-void
.end method


# virtual methods
.method protected doNext()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/util/fst/FSTEnum;, "Lorg/apache/lucene/util/fst/FSTEnum<TT;>;"
    const/4 v2, 0x1

    .line 94
    iget v0, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    if-nez v0, :cond_2

    .line 96
    iput v2, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    .line 97
    iget-object v0, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lorg/apache/lucene/util/fst/FSTEnum;->getArc(I)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v1

    invoke-direct {p0, v2}, Lorg/apache/lucene/util/fst/FSTEnum;->getArc(I)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fstReader:Lorg/apache/lucene/util/fst/FST$BytesReader;

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/lucene/util/fst/FST;->readFirstTargetArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 111
    :goto_0
    invoke-direct {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->pushFirst()V

    .line 112
    :cond_0
    return-void

    .line 102
    :cond_1
    iget v0, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    .line 103
    iget v0, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    if-eqz v0, :cond_0

    .line 101
    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/util/fst/FSTEnum;->arcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    iget v1, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lorg/apache/lucene/util/fst/FST$Arc;->isLast()Z

    move-result v0

    if-nez v0, :cond_1

    .line 108
    iget-object v0, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v1, p0, Lorg/apache/lucene/util/fst/FSTEnum;->arcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    iget v2, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    aget-object v1, v1, v2

    iget-object v2, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fstReader:Lorg/apache/lucene/util/fst/FST$BytesReader;

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/fst/FST;->readNextArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    goto :goto_0
.end method

.method protected doSeekCeil()V
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 131
    .local p0, "this":Lorg/apache/lucene/util/fst/FSTEnum;, "Lorg/apache/lucene/util/fst/FSTEnum<TT;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->rewindPrefix()V

    .line 134
    iget v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    invoke-direct {p0, v10}, Lorg/apache/lucene/util/fst/FSTEnum;->getArc(I)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v0

    .line 135
    .local v0, "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->getTargetLabel()I

    move-result v9

    .line 143
    .local v9, "targetLabel":I
    :goto_0
    iget v10, v0, Lorg/apache/lucene/util/fst/FST$Arc;->bytesPerArc:I

    if-eqz v10, :cond_d

    iget v10, v0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    const/4 v11, -0x1

    if-eq v10, v11, :cond_d

    .line 148
    iget-object v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    invoke-virtual {v10}, Lorg/apache/lucene/util/fst/FST;->getBytesReader()Lorg/apache/lucene/util/fst/FST$BytesReader;

    move-result-object v4

    .line 149
    .local v4, "in":Lorg/apache/lucene/util/fst/FST$BytesReader;, "Lorg/apache/lucene/util/fst/FST$BytesReader;"
    iget v5, v0, Lorg/apache/lucene/util/fst/FST$Arc;->arcIdx:I

    .line 150
    .local v5, "low":I
    iget v10, v0, Lorg/apache/lucene/util/fst/FST$Arc;->numArcs:I

    add-int/lit8 v3, v10, -0x1

    .line 151
    .local v3, "high":I
    const/4 v6, 0x0

    .line 153
    .local v6, "mid":I
    const/4 v2, 0x0

    .line 154
    .local v2, "found":Z
    :goto_1
    if-le v5, v3, :cond_0

    .line 173
    :goto_2
    if-eqz v2, :cond_7

    .line 175
    add-int/lit8 v10, v6, -0x1

    iput v10, v0, Lorg/apache/lucene/util/fst/FST$Arc;->arcIdx:I

    .line 176
    iget-object v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    invoke-virtual {v10, v0, v4}, Lorg/apache/lucene/util/fst/FST;->readNextRealArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 177
    sget-boolean v10, Lorg/apache/lucene/util/fst/FSTEnum;->$assertionsDisabled:Z

    if-nez v10, :cond_3

    iget v10, v0, Lorg/apache/lucene/util/fst/FST$Arc;->arcIdx:I

    if-eq v10, v6, :cond_3

    new-instance v10, Ljava/lang/AssertionError;

    invoke-direct {v10}, Ljava/lang/AssertionError;-><init>()V

    throw v10

    .line 155
    :cond_0
    add-int v10, v5, v3

    ushr-int/lit8 v6, v10, 0x1

    .line 156
    iget-wide v10, v0, Lorg/apache/lucene/util/fst/FST$Arc;->posArcsStart:J

    invoke-virtual {v4, v10, v11}, Lorg/apache/lucene/util/fst/FST$BytesReader;->setPosition(J)V

    .line 157
    iget v10, v0, Lorg/apache/lucene/util/fst/FST$Arc;->bytesPerArc:I

    mul-int/2addr v10, v6

    add-int/lit8 v10, v10, 0x1

    invoke-virtual {v4, v10}, Lorg/apache/lucene/util/fst/FST$BytesReader;->skipBytes(I)V

    .line 158
    iget-object v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    invoke-virtual {v10, v4}, Lorg/apache/lucene/util/fst/FST;->readLabel(Lorg/apache/lucene/store/DataInput;)I

    move-result v7

    .line 159
    .local v7, "midLabel":I
    sub-int v1, v7, v9

    .line 161
    .local v1, "cmp":I
    if-gez v1, :cond_1

    .line 162
    add-int/lit8 v5, v6, 0x1

    goto :goto_1

    .line 163
    :cond_1
    if-lez v1, :cond_2

    .line 164
    add-int/lit8 v3, v6, -0x1

    goto :goto_1

    .line 166
    :cond_2
    const/4 v2, 0x1

    .line 167
    goto :goto_2

    .line 178
    .end local v1    # "cmp":I
    .end local v7    # "midLabel":I
    :cond_3
    sget-boolean v10, Lorg/apache/lucene/util/fst/FSTEnum;->$assertionsDisabled:Z

    if-nez v10, :cond_4

    iget v10, v0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    if-eq v10, v9, :cond_4

    new-instance v10, Ljava/lang/AssertionError;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "arc.label="

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v12, v0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " vs targetLabel="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " mid="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v10

    .line 179
    :cond_4
    iget-object v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->output:[Ljava/lang/Object;

    iget v11, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    iget-object v12, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v12, v12, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    iget-object v13, p0, Lorg/apache/lucene/util/fst/FSTEnum;->output:[Ljava/lang/Object;

    iget v14, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    add-int/lit8 v14, v14, -0x1

    aget-object v13, v13, v14

    iget-object v14, v0, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    invoke-virtual {v12, v13, v14}, Lorg/apache/lucene/util/fst/Outputs;->add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    aput-object v12, v10, v11

    .line 180
    const/4 v10, -0x1

    if-ne v9, v10, :cond_6

    .line 244
    .end local v2    # "found":Z
    .end local v3    # "high":I
    .end local v4    # "in":Lorg/apache/lucene/util/fst/FST$BytesReader;, "Lorg/apache/lucene/util/fst/FST$BytesReader;"
    .end local v5    # "low":I
    .end local v6    # "mid":I
    :cond_5
    :goto_3
    return-void

    .line 183
    .restart local v2    # "found":Z
    .restart local v3    # "high":I
    .restart local v4    # "in":Lorg/apache/lucene/util/fst/FST$BytesReader;, "Lorg/apache/lucene/util/fst/FST$BytesReader;"
    .restart local v5    # "low":I
    .restart local v6    # "mid":I
    :cond_6
    iget v10, v0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    invoke-virtual {p0, v10}, Lorg/apache/lucene/util/fst/FSTEnum;->setCurrentLabel(I)V

    .line 184
    invoke-direct {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->incr()V

    .line 185
    iget-object v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    iget v11, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    invoke-direct {p0, v11}, Lorg/apache/lucene/util/fst/FSTEnum;->getArc(I)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v11

    iget-object v12, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fstReader:Lorg/apache/lucene/util/fst/FST$BytesReader;

    invoke-virtual {v10, v0, v11, v12}, Lorg/apache/lucene/util/fst/FST;->readFirstTargetArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v0

    .line 186
    invoke-virtual {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->getTargetLabel()I

    move-result v9

    .line 187
    goto/16 :goto_0

    .line 188
    :cond_7
    iget v10, v0, Lorg/apache/lucene/util/fst/FST$Arc;->numArcs:I

    if-ne v5, v10, :cond_a

    .line 190
    iget v10, v0, Lorg/apache/lucene/util/fst/FST$Arc;->numArcs:I

    add-int/lit8 v10, v10, -0x2

    iput v10, v0, Lorg/apache/lucene/util/fst/FST$Arc;->arcIdx:I

    .line 191
    iget-object v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    invoke-virtual {v10, v0, v4}, Lorg/apache/lucene/util/fst/FST;->readNextRealArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 192
    sget-boolean v10, Lorg/apache/lucene/util/fst/FSTEnum;->$assertionsDisabled:Z

    if-nez v10, :cond_8

    invoke-virtual {v0}, Lorg/apache/lucene/util/fst/FST$Arc;->isLast()Z

    move-result v10

    if-nez v10, :cond_8

    new-instance v10, Ljava/lang/AssertionError;

    invoke-direct {v10}, Ljava/lang/AssertionError;-><init>()V

    throw v10

    .line 195
    :cond_8
    iget v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    add-int/lit8 v10, v10, -0x1

    iput v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    .line 197
    :goto_4
    iget v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    if-eqz v10, :cond_5

    .line 200
    iget v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    invoke-direct {p0, v10}, Lorg/apache/lucene/util/fst/FSTEnum;->getArc(I)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v8

    .line 202
    .local v8, "prevArc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    invoke-virtual {v8}, Lorg/apache/lucene/util/fst/FST$Arc;->isLast()Z

    move-result v10

    if-nez v10, :cond_9

    .line 203
    iget-object v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v11, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fstReader:Lorg/apache/lucene/util/fst/FST$BytesReader;

    invoke-virtual {v10, v8, v11}, Lorg/apache/lucene/util/fst/FST;->readNextArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 204
    invoke-direct {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->pushFirst()V

    goto :goto_3

    .line 207
    :cond_9
    iget v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    add-int/lit8 v10, v10, -0x1

    iput v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    goto :goto_4

    .line 210
    .end local v8    # "prevArc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    :cond_a
    if-le v5, v3, :cond_b

    .end local v5    # "low":I
    :goto_5
    add-int/lit8 v10, v5, -0x1

    iput v10, v0, Lorg/apache/lucene/util/fst/FST$Arc;->arcIdx:I

    .line 211
    iget-object v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    invoke-virtual {v10, v0, v4}, Lorg/apache/lucene/util/fst/FST;->readNextRealArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 212
    sget-boolean v10, Lorg/apache/lucene/util/fst/FSTEnum;->$assertionsDisabled:Z

    if-nez v10, :cond_c

    iget v10, v0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    if-gt v10, v9, :cond_c

    new-instance v10, Ljava/lang/AssertionError;

    invoke-direct {v10}, Ljava/lang/AssertionError;-><init>()V

    throw v10

    .restart local v5    # "low":I
    :cond_b
    move v5, v3

    .line 210
    goto :goto_5

    .line 213
    .end local v5    # "low":I
    :cond_c
    invoke-direct {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->pushFirst()V

    goto/16 :goto_3

    .line 218
    .end local v2    # "found":Z
    .end local v3    # "high":I
    .end local v4    # "in":Lorg/apache/lucene/util/fst/FST$BytesReader;, "Lorg/apache/lucene/util/fst/FST$BytesReader;"
    .end local v6    # "mid":I
    :cond_d
    iget v10, v0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    if-ne v10, v9, :cond_e

    .line 220
    iget-object v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->output:[Ljava/lang/Object;

    iget v11, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    iget-object v12, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v12, v12, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    iget-object v13, p0, Lorg/apache/lucene/util/fst/FSTEnum;->output:[Ljava/lang/Object;

    iget v14, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    add-int/lit8 v14, v14, -0x1

    aget-object v13, v13, v14

    iget-object v14, v0, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    invoke-virtual {v12, v13, v14}, Lorg/apache/lucene/util/fst/Outputs;->add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    aput-object v12, v10, v11

    .line 221
    const/4 v10, -0x1

    if-eq v9, v10, :cond_5

    .line 224
    iget v10, v0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    invoke-virtual {p0, v10}, Lorg/apache/lucene/util/fst/FSTEnum;->setCurrentLabel(I)V

    .line 225
    invoke-direct {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->incr()V

    .line 226
    iget-object v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    iget v11, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    invoke-direct {p0, v11}, Lorg/apache/lucene/util/fst/FSTEnum;->getArc(I)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v11

    iget-object v12, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fstReader:Lorg/apache/lucene/util/fst/FST$BytesReader;

    invoke-virtual {v10, v0, v11, v12}, Lorg/apache/lucene/util/fst/FST;->readFirstTargetArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v0

    .line 227
    invoke-virtual {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->getTargetLabel()I

    move-result v9

    .line 228
    goto/16 :goto_0

    :cond_e
    iget v10, v0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    if-le v10, v9, :cond_f

    .line 229
    invoke-direct {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->pushFirst()V

    goto/16 :goto_3

    .line 231
    :cond_f
    invoke-virtual {v0}, Lorg/apache/lucene/util/fst/FST$Arc;->isLast()Z

    move-result v10

    if-eqz v10, :cond_11

    .line 234
    iget v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    add-int/lit8 v10, v10, -0x1

    iput v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    .line 236
    :goto_6
    iget v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    if-eqz v10, :cond_5

    .line 239
    iget v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    invoke-direct {p0, v10}, Lorg/apache/lucene/util/fst/FSTEnum;->getArc(I)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v8

    .line 241
    .restart local v8    # "prevArc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    invoke-virtual {v8}, Lorg/apache/lucene/util/fst/FST$Arc;->isLast()Z

    move-result v10

    if-nez v10, :cond_10

    .line 242
    iget-object v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v11, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fstReader:Lorg/apache/lucene/util/fst/FST$BytesReader;

    invoke-virtual {v10, v8, v11}, Lorg/apache/lucene/util/fst/FST;->readNextArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 243
    invoke-direct {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->pushFirst()V

    goto/16 :goto_3

    .line 246
    :cond_10
    iget v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    add-int/lit8 v10, v10, -0x1

    iput v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    goto :goto_6

    .line 251
    .end local v8    # "prevArc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    :cond_11
    iget-object v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v11, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fstReader:Lorg/apache/lucene/util/fst/FST$BytesReader;

    invoke-virtual {v10, v0, v11}, Lorg/apache/lucene/util/fst/FST;->readNextArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    goto/16 :goto_0
.end method

.method protected doSeekExact()Z
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 431
    .local p0, "this":Lorg/apache/lucene/util/fst/FSTEnum;, "Lorg/apache/lucene/util/fst/FSTEnum<TT;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->rewindPrefix()V

    .line 434
    iget v4, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    add-int/lit8 v4, v4, -0x1

    invoke-direct {p0, v4}, Lorg/apache/lucene/util/fst/FSTEnum;->getArc(I)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v0

    .line 435
    .local v0, "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->getTargetLabel()I

    move-result v3

    .line 437
    .local v3, "targetLabel":I
    iget-object v4, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    invoke-virtual {v4}, Lorg/apache/lucene/util/fst/FST;->getBytesReader()Lorg/apache/lucene/util/fst/FST$BytesReader;

    move-result-object v1

    .line 441
    .local v1, "fstReader":Lorg/apache/lucene/util/fst/FST$BytesReader;, "Lorg/apache/lucene/util/fst/FST$BytesReader;"
    :goto_0
    iget-object v4, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    iget v5, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    invoke-direct {p0, v5}, Lorg/apache/lucene/util/fst/FSTEnum;->getArc(I)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v5

    invoke-virtual {v4, v3, v0, v5, v1}, Lorg/apache/lucene/util/fst/FST;->findTargetArc(ILorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v2

    .line 442
    .local v2, "nextArc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    if-nez v2, :cond_0

    .line 446
    iget-object v4, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    iget v5, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    invoke-direct {p0, v5}, Lorg/apache/lucene/util/fst/FSTEnum;->getArc(I)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v5

    invoke-virtual {v4, v0, v5, v1}, Lorg/apache/lucene/util/fst/FST;->readFirstTargetArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 448
    const/4 v4, 0x0

    .line 454
    :goto_1
    return v4

    .line 451
    :cond_0
    iget-object v4, p0, Lorg/apache/lucene/util/fst/FSTEnum;->output:[Ljava/lang/Object;

    iget v5, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    iget-object v6, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v6, v6, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    iget-object v7, p0, Lorg/apache/lucene/util/fst/FSTEnum;->output:[Ljava/lang/Object;

    iget v8, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    add-int/lit8 v8, v8, -0x1

    aget-object v7, v7, v8

    iget-object v8, v2, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    invoke-virtual {v6, v7, v8}, Lorg/apache/lucene/util/fst/Outputs;->add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    aput-object v6, v4, v5

    .line 452
    const/4 v4, -0x1

    if-ne v3, v4, :cond_1

    .line 454
    const/4 v4, 0x1

    goto :goto_1

    .line 456
    :cond_1
    invoke-virtual {p0, v3}, Lorg/apache/lucene/util/fst/FSTEnum;->setCurrentLabel(I)V

    .line 457
    invoke-direct {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->incr()V

    .line 458
    invoke-virtual {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->getTargetLabel()I

    move-result v3

    .line 459
    move-object v0, v2

    .line 439
    goto :goto_0
.end method

.method protected doSeekFloor()V
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/util/fst/FSTEnum;, "Lorg/apache/lucene/util/fst/FSTEnum<TT;>;"
    const/4 v14, -0x1

    .line 270
    invoke-virtual {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->rewindPrefix()V

    .line 274
    iget v9, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    invoke-direct {p0, v9}, Lorg/apache/lucene/util/fst/FSTEnum;->getArc(I)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v0

    .line 275
    .local v0, "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->getTargetLabel()I

    move-result v8

    .line 283
    .local v8, "targetLabel":I
    :goto_0
    iget v9, v0, Lorg/apache/lucene/util/fst/FST$Arc;->bytesPerArc:I

    if-eqz v9, :cond_f

    iget v9, v0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    if-eq v9, v14, :cond_f

    .line 287
    iget-object v9, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    invoke-virtual {v9}, Lorg/apache/lucene/util/fst/FST;->getBytesReader()Lorg/apache/lucene/util/fst/FST$BytesReader;

    move-result-object v4

    .line 288
    .local v4, "in":Lorg/apache/lucene/util/fst/FST$BytesReader;, "Lorg/apache/lucene/util/fst/FST$BytesReader;"
    iget v5, v0, Lorg/apache/lucene/util/fst/FST$Arc;->arcIdx:I

    .line 289
    .local v5, "low":I
    iget v9, v0, Lorg/apache/lucene/util/fst/FST$Arc;->numArcs:I

    add-int/lit8 v3, v9, -0x1

    .line 290
    .local v3, "high":I
    const/4 v6, 0x0

    .line 292
    .local v6, "mid":I
    const/4 v2, 0x0

    .line 293
    .local v2, "found":Z
    :goto_1
    if-le v5, v3, :cond_0

    .line 312
    :goto_2
    if-eqz v2, :cond_7

    .line 315
    add-int/lit8 v9, v6, -0x1

    iput v9, v0, Lorg/apache/lucene/util/fst/FST$Arc;->arcIdx:I

    .line 316
    iget-object v9, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    invoke-virtual {v9, v0, v4}, Lorg/apache/lucene/util/fst/FST;->readNextRealArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 317
    sget-boolean v9, Lorg/apache/lucene/util/fst/FSTEnum;->$assertionsDisabled:Z

    if-nez v9, :cond_3

    iget v9, v0, Lorg/apache/lucene/util/fst/FST$Arc;->arcIdx:I

    if-eq v9, v6, :cond_3

    new-instance v9, Ljava/lang/AssertionError;

    invoke-direct {v9}, Ljava/lang/AssertionError;-><init>()V

    throw v9

    .line 294
    :cond_0
    add-int v9, v5, v3

    ushr-int/lit8 v6, v9, 0x1

    .line 295
    iget-wide v10, v0, Lorg/apache/lucene/util/fst/FST$Arc;->posArcsStart:J

    invoke-virtual {v4, v10, v11}, Lorg/apache/lucene/util/fst/FST$BytesReader;->setPosition(J)V

    .line 296
    iget v9, v0, Lorg/apache/lucene/util/fst/FST$Arc;->bytesPerArc:I

    mul-int/2addr v9, v6

    add-int/lit8 v9, v9, 0x1

    invoke-virtual {v4, v9}, Lorg/apache/lucene/util/fst/FST$BytesReader;->skipBytes(I)V

    .line 297
    iget-object v9, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    invoke-virtual {v9, v4}, Lorg/apache/lucene/util/fst/FST;->readLabel(Lorg/apache/lucene/store/DataInput;)I

    move-result v7

    .line 298
    .local v7, "midLabel":I
    sub-int v1, v7, v8

    .line 300
    .local v1, "cmp":I
    if-gez v1, :cond_1

    .line 301
    add-int/lit8 v5, v6, 0x1

    .line 302
    goto :goto_1

    :cond_1
    if-lez v1, :cond_2

    .line 303
    add-int/lit8 v3, v6, -0x1

    .line 304
    goto :goto_1

    .line 305
    :cond_2
    const/4 v2, 0x1

    .line 306
    goto :goto_2

    .line 318
    .end local v1    # "cmp":I
    .end local v7    # "midLabel":I
    :cond_3
    sget-boolean v9, Lorg/apache/lucene/util/fst/FSTEnum;->$assertionsDisabled:Z

    if-nez v9, :cond_4

    iget v9, v0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    if-eq v9, v8, :cond_4

    new-instance v9, Ljava/lang/AssertionError;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "arc.label="

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v11, v0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " vs targetLabel="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " mid="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v9

    .line 319
    :cond_4
    iget-object v9, p0, Lorg/apache/lucene/util/fst/FSTEnum;->output:[Ljava/lang/Object;

    iget v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    iget-object v11, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v11, v11, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    iget-object v12, p0, Lorg/apache/lucene/util/fst/FSTEnum;->output:[Ljava/lang/Object;

    iget v13, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    add-int/lit8 v13, v13, -0x1

    aget-object v12, v12, v13

    iget-object v13, v0, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    invoke-virtual {v11, v12, v13}, Lorg/apache/lucene/util/fst/Outputs;->add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    aput-object v11, v9, v10

    .line 320
    if-ne v8, v14, :cond_6

    .line 413
    .end local v2    # "found":Z
    .end local v3    # "high":I
    .end local v4    # "in":Lorg/apache/lucene/util/fst/FST$BytesReader;, "Lorg/apache/lucene/util/fst/FST$BytesReader;"
    .end local v5    # "low":I
    .end local v6    # "mid":I
    :cond_5
    :goto_3
    return-void

    .line 323
    .restart local v2    # "found":Z
    .restart local v3    # "high":I
    .restart local v4    # "in":Lorg/apache/lucene/util/fst/FST$BytesReader;, "Lorg/apache/lucene/util/fst/FST$BytesReader;"
    .restart local v5    # "low":I
    .restart local v6    # "mid":I
    :cond_6
    iget v9, v0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    invoke-virtual {p0, v9}, Lorg/apache/lucene/util/fst/FSTEnum;->setCurrentLabel(I)V

    .line 324
    invoke-direct {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->incr()V

    .line 325
    iget-object v9, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    iget v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    invoke-direct {p0, v10}, Lorg/apache/lucene/util/fst/FSTEnum;->getArc(I)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v10

    iget-object v11, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fstReader:Lorg/apache/lucene/util/fst/FST$BytesReader;

    invoke-virtual {v9, v0, v10, v11}, Lorg/apache/lucene/util/fst/FST;->readFirstTargetArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v0

    .line 326
    invoke-virtual {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->getTargetLabel()I

    move-result v8

    .line 327
    goto/16 :goto_0

    .line 328
    :cond_7
    if-ne v3, v14, :cond_b

    .line 338
    :goto_4
    iget-object v9, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    iget v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    add-int/lit8 v10, v10, -0x1

    invoke-direct {p0, v10}, Lorg/apache/lucene/util/fst/FSTEnum;->getArc(I)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v10

    iget-object v11, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fstReader:Lorg/apache/lucene/util/fst/FST$BytesReader;

    invoke-virtual {v9, v10, v0, v11}, Lorg/apache/lucene/util/fst/FST;->readFirstTargetArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 339
    iget v9, v0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    if-ge v9, v8, :cond_a

    .line 342
    :goto_5
    invoke-virtual {v0}, Lorg/apache/lucene/util/fst/FST$Arc;->isLast()Z

    move-result v9

    if-nez v9, :cond_8

    iget-object v9, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    invoke-virtual {v9, v0, v4}, Lorg/apache/lucene/util/fst/FST;->readNextArcLabel(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)I

    move-result v9

    if-lt v9, v8, :cond_9

    .line 345
    :cond_8
    invoke-direct {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->pushLast()V

    goto :goto_3

    .line 343
    :cond_9
    iget-object v9, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fstReader:Lorg/apache/lucene/util/fst/FST$BytesReader;

    invoke-virtual {v9, v0, v10}, Lorg/apache/lucene/util/fst/FST;->readNextArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    goto :goto_5

    .line 348
    :cond_a
    iget v9, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    add-int/lit8 v9, v9, -0x1

    iput v9, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    .line 349
    iget v9, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    if-eqz v9, :cond_5

    .line 352
    invoke-virtual {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->getTargetLabel()I

    move-result v8

    .line 353
    iget v9, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    invoke-direct {p0, v9}, Lorg/apache/lucene/util/fst/FSTEnum;->getArc(I)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v0

    .line 335
    goto :goto_4

    .line 357
    :cond_b
    if-le v5, v3, :cond_c

    .end local v3    # "high":I
    :goto_6
    add-int/lit8 v9, v3, -0x1

    iput v9, v0, Lorg/apache/lucene/util/fst/FST$Arc;->arcIdx:I

    .line 359
    iget-object v9, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    invoke-virtual {v9, v0, v4}, Lorg/apache/lucene/util/fst/FST;->readNextRealArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 360
    sget-boolean v9, Lorg/apache/lucene/util/fst/FSTEnum;->$assertionsDisabled:Z

    if-nez v9, :cond_d

    invoke-virtual {v0}, Lorg/apache/lucene/util/fst/FST$Arc;->isLast()Z

    move-result v9

    if-nez v9, :cond_d

    iget-object v9, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    invoke-virtual {v9, v0, v4}, Lorg/apache/lucene/util/fst/FST;->readNextArcLabel(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)I

    move-result v9

    if-gt v9, v8, :cond_d

    new-instance v9, Ljava/lang/AssertionError;

    invoke-direct {v9}, Ljava/lang/AssertionError;-><init>()V

    throw v9

    .restart local v3    # "high":I
    :cond_c
    move v3, v5

    .line 357
    goto :goto_6

    .line 361
    .end local v3    # "high":I
    :cond_d
    sget-boolean v9, Lorg/apache/lucene/util/fst/FSTEnum;->$assertionsDisabled:Z

    if-nez v9, :cond_e

    iget v9, v0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    if-lt v9, v8, :cond_e

    new-instance v9, Ljava/lang/AssertionError;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "arc.label="

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v11, v0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " vs targetLabel="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v9

    .line 362
    :cond_e
    invoke-direct {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->pushLast()V

    goto/16 :goto_3

    .line 367
    .end local v2    # "found":Z
    .end local v4    # "in":Lorg/apache/lucene/util/fst/FST$BytesReader;, "Lorg/apache/lucene/util/fst/FST$BytesReader;"
    .end local v5    # "low":I
    .end local v6    # "mid":I
    :cond_f
    iget v9, v0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    if-ne v9, v8, :cond_10

    .line 369
    iget-object v9, p0, Lorg/apache/lucene/util/fst/FSTEnum;->output:[Ljava/lang/Object;

    iget v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    iget-object v11, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v11, v11, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    iget-object v12, p0, Lorg/apache/lucene/util/fst/FSTEnum;->output:[Ljava/lang/Object;

    iget v13, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    add-int/lit8 v13, v13, -0x1

    aget-object v12, v12, v13

    iget-object v13, v0, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    invoke-virtual {v11, v12, v13}, Lorg/apache/lucene/util/fst/Outputs;->add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    aput-object v11, v9, v10

    .line 370
    if-eq v8, v14, :cond_5

    .line 373
    iget v9, v0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    invoke-virtual {p0, v9}, Lorg/apache/lucene/util/fst/FSTEnum;->setCurrentLabel(I)V

    .line 374
    invoke-direct {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->incr()V

    .line 375
    iget-object v9, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    iget v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    invoke-direct {p0, v10}, Lorg/apache/lucene/util/fst/FSTEnum;->getArc(I)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v10

    iget-object v11, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fstReader:Lorg/apache/lucene/util/fst/FST$BytesReader;

    invoke-virtual {v9, v0, v10, v11}, Lorg/apache/lucene/util/fst/FST;->readFirstTargetArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v0

    .line 376
    invoke-virtual {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->getTargetLabel()I

    move-result v8

    .line 377
    goto/16 :goto_0

    :cond_10
    iget v9, v0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    if-le v9, v8, :cond_14

    .line 385
    :goto_7
    iget-object v9, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    iget v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    add-int/lit8 v10, v10, -0x1

    invoke-direct {p0, v10}, Lorg/apache/lucene/util/fst/FSTEnum;->getArc(I)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v10

    iget-object v11, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fstReader:Lorg/apache/lucene/util/fst/FST$BytesReader;

    invoke-virtual {v9, v10, v0, v11}, Lorg/apache/lucene/util/fst/FST;->readFirstTargetArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 386
    iget v9, v0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    if-ge v9, v8, :cond_13

    .line 389
    :goto_8
    invoke-virtual {v0}, Lorg/apache/lucene/util/fst/FST$Arc;->isLast()Z

    move-result v9

    if-nez v9, :cond_11

    iget-object v9, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fstReader:Lorg/apache/lucene/util/fst/FST$BytesReader;

    invoke-virtual {v9, v0, v10}, Lorg/apache/lucene/util/fst/FST;->readNextArcLabel(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)I

    move-result v9

    if-lt v9, v8, :cond_12

    .line 392
    :cond_11
    invoke-direct {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->pushLast()V

    goto/16 :goto_3

    .line 390
    :cond_12
    iget-object v9, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fstReader:Lorg/apache/lucene/util/fst/FST$BytesReader;

    invoke-virtual {v9, v0, v10}, Lorg/apache/lucene/util/fst/FST;->readNextArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    goto :goto_8

    .line 395
    :cond_13
    iget v9, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    add-int/lit8 v9, v9, -0x1

    iput v9, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    .line 396
    iget v9, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    if-eqz v9, :cond_5

    .line 399
    invoke-virtual {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->getTargetLabel()I

    move-result v8

    .line 400
    iget v9, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    invoke-direct {p0, v9}, Lorg/apache/lucene/util/fst/FSTEnum;->getArc(I)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v0

    .line 382
    goto :goto_7

    .line 402
    :cond_14
    invoke-virtual {v0}, Lorg/apache/lucene/util/fst/FST$Arc;->isLast()Z

    move-result v9

    if-nez v9, :cond_16

    .line 404
    iget-object v9, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fstReader:Lorg/apache/lucene/util/fst/FST$BytesReader;

    invoke-virtual {v9, v0, v10}, Lorg/apache/lucene/util/fst/FST;->readNextArcLabel(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)I

    move-result v9

    if-le v9, v8, :cond_15

    .line 405
    invoke-direct {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->pushLast()V

    goto/16 :goto_3

    .line 409
    :cond_15
    iget-object v9, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fstReader:Lorg/apache/lucene/util/fst/FST$BytesReader;

    invoke-virtual {v9, v0, v10}, Lorg/apache/lucene/util/fst/FST;->readNextArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    goto/16 :goto_0

    .line 412
    :cond_16
    invoke-direct {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->pushLast()V

    goto/16 :goto_3
.end method

.method protected abstract getCurrentLabel()I
.end method

.method protected abstract getTargetLabel()I
.end method

.method protected abstract grow()V
.end method

.method protected final rewindPrefix()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/util/fst/FSTEnum;, "Lorg/apache/lucene/util/fst/FSTEnum<TT;>;"
    const/4 v5, 0x1

    .line 64
    iget v3, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    if-nez v3, :cond_1

    .line 66
    iput v5, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    .line 67
    iget-object v3, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    const/4 v4, 0x0

    invoke-direct {p0, v4}, Lorg/apache/lucene/util/fst/FSTEnum;->getArc(I)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v4

    invoke-direct {p0, v5}, Lorg/apache/lucene/util/fst/FSTEnum;->getArc(I)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v5

    iget-object v6, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fstReader:Lorg/apache/lucene/util/fst/FST$BytesReader;

    invoke-virtual {v3, v4, v5, v6}, Lorg/apache/lucene/util/fst/FST;->readFirstTargetArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 90
    :cond_0
    :goto_0
    return-void

    .line 72
    :cond_1
    iget v2, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    .line 73
    .local v2, "currentLimit":I
    iput v5, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    .line 74
    :goto_1
    iget v3, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    if-ge v3, v2, :cond_0

    iget v3, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    iget v4, p0, Lorg/apache/lucene/util/fst/FSTEnum;->targetLength:I

    add-int/lit8 v4, v4, 0x1

    if-gt v3, v4, :cond_0

    .line 75
    invoke-virtual {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->getCurrentLabel()I

    move-result v3

    invoke-virtual {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->getTargetLabel()I

    move-result v4

    sub-int v1, v3, v4

    .line 76
    .local v1, "cmp":I
    if-ltz v1, :cond_0

    .line 80
    if-lez v1, :cond_2

    .line 82
    iget v3, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    invoke-direct {p0, v3}, Lorg/apache/lucene/util/fst/FSTEnum;->getArc(I)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v0

    .line 83
    .local v0, "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    iget-object v3, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    iget v4, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    add-int/lit8 v4, v4, -0x1

    invoke-direct {p0, v4}, Lorg/apache/lucene/util/fst/FSTEnum;->getArc(I)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fstReader:Lorg/apache/lucene/util/fst/FST$BytesReader;

    invoke-virtual {v3, v4, v0, v5}, Lorg/apache/lucene/util/fst/FST;->readFirstTargetArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    goto :goto_0

    .line 87
    .end local v0    # "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    :cond_2
    iget v3, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    goto :goto_1
.end method

.method protected abstract setCurrentLabel(I)V
.end method
