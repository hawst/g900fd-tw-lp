.class Lorg/apache/lucene/util/packed/DirectPackedReader;
.super Lorg/apache/lucene/util/packed/PackedInts$ReaderImpl;
.source "DirectPackedReader.java"


# instance fields
.field private final in:Lorg/apache/lucene/store/IndexInput;

.field private final startPointer:J


# direct methods
.method public constructor <init>(IILorg/apache/lucene/store/IndexInput;)V
    .locals 2
    .param p1, "bitsPerValue"    # I
    .param p2, "valueCount"    # I
    .param p3, "in"    # Lorg/apache/lucene/store/IndexInput;

    .prologue
    .line 30
    invoke-direct {p0, p2, p1}, Lorg/apache/lucene/util/packed/PackedInts$ReaderImpl;-><init>(II)V

    .line 31
    iput-object p3, p0, Lorg/apache/lucene/util/packed/DirectPackedReader;->in:Lorg/apache/lucene/store/IndexInput;

    .line 33
    invoke-virtual {p3}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/lucene/util/packed/DirectPackedReader;->startPointer:J

    .line 34
    return-void
.end method


# virtual methods
.method public get(I)J
    .locals 18
    .param p1, "index"    # I

    .prologue
    .line 38
    move/from16 v0, p1

    int-to-long v12, v0

    move-object/from16 v0, p0

    iget v14, v0, Lorg/apache/lucene/util/packed/DirectPackedReader;->bitsPerValue:I

    int-to-long v14, v14

    mul-long v8, v12, v14

    .line 39
    .local v8, "majorBitPos":J
    const/4 v12, 0x3

    ushr-long v4, v8, v12

    .line 41
    .local v4, "elementPos":J
    :try_start_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/util/packed/DirectPackedReader;->in:Lorg/apache/lucene/store/IndexInput;

    move-object/from16 v0, p0

    iget-wide v14, v0, Lorg/apache/lucene/util/packed/DirectPackedReader;->startPointer:J

    add-long/2addr v14, v4

    invoke-virtual {v12, v14, v15}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 43
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/util/packed/DirectPackedReader;->in:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v12}, Lorg/apache/lucene/store/IndexInput;->readByte()B

    move-result v2

    .line 44
    .local v2, "b0":B
    const-wide/16 v12, 0x7

    and-long/2addr v12, v8

    long-to-int v3, v12

    .line 45
    .local v3, "bitPos":I
    move-object/from16 v0, p0

    iget v12, v0, Lorg/apache/lucene/util/packed/DirectPackedReader;->bitsPerValue:I

    add-int/2addr v12, v3

    const/16 v13, 0x8

    if-gt v12, v13, :cond_1

    .line 47
    int-to-long v12, v2

    const-wide/16 v14, 0x1

    rsub-int/lit8 v16, v3, 0x8

    shl-long v14, v14, v16

    const-wide/16 v16, 0x1

    sub-long v14, v14, v16

    and-long/2addr v12, v14

    rsub-int/lit8 v14, v3, 0x8

    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/util/packed/DirectPackedReader;->bitsPerValue:I

    sub-int/2addr v14, v15

    ushr-long v10, v12, v14

    .line 65
    :cond_0
    :goto_0
    return-wide v10

    .line 51
    :cond_1
    move-object/from16 v0, p0

    iget v12, v0, Lorg/apache/lucene/util/packed/DirectPackedReader;->bitsPerValue:I

    add-int/lit8 v12, v12, -0x8

    add-int v7, v12, v3

    .line 52
    .local v7, "remainingBits":I
    int-to-long v12, v2

    const-wide/16 v14, 0x1

    rsub-int/lit8 v16, v3, 0x8

    shl-long v14, v14, v16

    const-wide/16 v16, 0x1

    sub-long v14, v14, v16

    and-long/2addr v12, v14

    shl-long v10, v12, v7

    .line 55
    .local v10, "result":J
    :goto_1
    const/16 v12, 0x8

    if-ge v7, v12, :cond_2

    .line 61
    if-lez v7, :cond_0

    .line 62
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/util/packed/DirectPackedReader;->in:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v12}, Lorg/apache/lucene/store/IndexInput;->readByte()B

    move-result v12

    int-to-long v12, v12

    const-wide/16 v14, 0xff

    and-long/2addr v12, v14

    rsub-int/lit8 v14, v7, 0x8

    ushr-long/2addr v12, v14

    or-long/2addr v10, v12

    goto :goto_0

    .line 56
    :cond_2
    add-int/lit8 v7, v7, -0x8

    .line 57
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/util/packed/DirectPackedReader;->in:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v12}, Lorg/apache/lucene/store/IndexInput;->readByte()B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v12

    int-to-long v12, v12

    const-wide/16 v14, 0xff

    and-long/2addr v12, v14

    shl-long/2addr v12, v7

    or-long/2addr v10, v12

    goto :goto_1

    .line 66
    .end local v2    # "b0":B
    .end local v3    # "bitPos":I
    .end local v7    # "remainingBits":I
    .end local v10    # "result":J
    :catch_0
    move-exception v6

    .line 67
    .local v6, "ioe":Ljava/io/IOException;
    new-instance v12, Ljava/lang/IllegalStateException;

    const-string v13, "failed"

    invoke-direct {v12, v13, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v12
.end method

.method public ramBytesUsed()J
    .locals 2

    .prologue
    .line 73
    const-wide/16 v0, 0x0

    return-wide v0
.end method
