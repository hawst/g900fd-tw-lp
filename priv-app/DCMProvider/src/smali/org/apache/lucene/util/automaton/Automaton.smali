.class public Lorg/apache/lucene/util/automaton/Automaton;
.super Ljava/lang/Object;
.source "Automaton.java"

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final MINIMIZE_HOPCROFT:I = 0x2

.field static allow_mutation:Z

.field static minimization:I

.field static minimize_always:Z


# instance fields
.field deterministic:Z

.field transient info:Ljava/lang/Object;

.field initial:Lorg/apache/lucene/util/automaton/State;

.field private numberedStates:[Lorg/apache/lucene/util/automaton/State;

.field singleton:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 77
    const-class v0, Lorg/apache/lucene/util/automaton/Automaton;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/automaton/Automaton;->$assertionsDisabled:Z

    .line 88
    const/4 v0, 0x2

    sput v0, Lorg/apache/lucene/util/automaton/Automaton;->minimization:I

    .line 111
    sput-boolean v1, Lorg/apache/lucene/util/automaton/Automaton;->minimize_always:Z

    .line 117
    sput-boolean v1, Lorg/apache/lucene/util/automaton/Automaton;->allow_mutation:Z

    return-void

    :cond_0
    move v0, v1

    .line 77
    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 134
    new-instance v0, Lorg/apache/lucene/util/automaton/State;

    invoke-direct {v0}, Lorg/apache/lucene/util/automaton/State;-><init>()V

    invoke-direct {p0, v0}, Lorg/apache/lucene/util/automaton/Automaton;-><init>(Lorg/apache/lucene/util/automaton/State;)V

    .line 135
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/automaton/State;)V
    .locals 1
    .param p1, "initial"    # Lorg/apache/lucene/util/automaton/State;

    .prologue
    .line 127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 128
    iput-object p1, p0, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    .line 129
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/util/automaton/Automaton;->deterministic:Z

    .line 130
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/util/automaton/Automaton;->singleton:Ljava/lang/String;

    .line 131
    return-void
.end method

.method public static concatenate(Ljava/util/List;)Lorg/apache/lucene/util/automaton/Automaton;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/util/automaton/Automaton;",
            ">;)",
            "Lorg/apache/lucene/util/automaton/Automaton;"
        }
    .end annotation

    .prologue
    .line 681
    .local p0, "l":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/util/automaton/Automaton;>;"
    invoke-static {p0}, Lorg/apache/lucene/util/automaton/BasicOperations;->concatenate(Ljava/util/List;)Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v0

    return-object v0
.end method

.method static getAllowMutate()Z
    .locals 1

    .prologue
    .line 182
    sget-boolean v0, Lorg/apache/lucene/util/automaton/Automaton;->allow_mutation:Z

    return v0
.end method

.method private getLiveStates()[Lorg/apache/lucene/util/automaton/State;
    .locals 12

    .prologue
    const/4 v8, 0x0

    .line 425
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->getNumberedStates()[Lorg/apache/lucene/util/automaton/State;

    move-result-object v6

    .line 426
    .local v6, "states":[Lorg/apache/lucene/util/automaton/State;
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 427
    .local v1, "live":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/util/automaton/State;>;"
    array-length v10, v6

    move v9, v8

    :goto_0
    if-lt v9, v10, :cond_1

    .line 433
    array-length v9, v6

    new-array v2, v9, [Ljava/util/Set;

    .line 434
    .local v2, "map":[Ljava/util/Set;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v9, v2

    if-lt v0, v9, :cond_3

    .line 436
    array-length v9, v6

    :goto_2
    if-lt v8, v9, :cond_4

    .line 441
    new-instance v7, Ljava/util/LinkedList;

    invoke-direct {v7, v1}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    .line 442
    .local v7, "worklist":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lorg/apache/lucene/util/automaton/State;>;"
    :cond_0
    invoke-virtual {v7}, Ljava/util/LinkedList;->size()I

    move-result v8

    if-gtz v8, :cond_6

    .line 451
    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v8

    new-array v8, v8, [Lorg/apache/lucene/util/automaton/State;

    invoke-interface {v1, v8}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [Lorg/apache/lucene/util/automaton/State;

    return-object v8

    .line 427
    .end local v0    # "i":I
    .end local v2    # "map":[Ljava/util/Set;
    .end local v7    # "worklist":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lorg/apache/lucene/util/automaton/State;>;"
    :cond_1
    aget-object v4, v6, v9

    .line 428
    .local v4, "q":Lorg/apache/lucene/util/automaton/State;
    invoke-virtual {v4}, Lorg/apache/lucene/util/automaton/State;->isAccept()Z

    move-result v11

    if-eqz v11, :cond_2

    .line 429
    invoke-interface {v1, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 427
    :cond_2
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 435
    .end local v4    # "q":Lorg/apache/lucene/util/automaton/State;
    .restart local v0    # "i":I
    .restart local v2    # "map":[Ljava/util/Set;
    :cond_3
    new-instance v9, Ljava/util/HashSet;

    invoke-direct {v9}, Ljava/util/HashSet;-><init>()V

    aput-object v9, v2, v0

    .line 434
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 436
    :cond_4
    aget-object v5, v6, v8

    .line 437
    .local v5, "s":Lorg/apache/lucene/util/automaton/State;
    const/4 v0, 0x0

    :goto_3
    iget v10, v5, Lorg/apache/lucene/util/automaton/State;->numTransitions:I

    if-lt v0, v10, :cond_5

    .line 436
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 438
    :cond_5
    iget-object v10, v5, Lorg/apache/lucene/util/automaton/State;->transitionsArray:[Lorg/apache/lucene/util/automaton/Transition;

    aget-object v10, v10, v0

    iget-object v10, v10, Lorg/apache/lucene/util/automaton/Transition;->to:Lorg/apache/lucene/util/automaton/State;

    iget v10, v10, Lorg/apache/lucene/util/automaton/State;->number:I

    aget-object v10, v2, v10

    invoke-interface {v10, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 437
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 443
    .end local v5    # "s":Lorg/apache/lucene/util/automaton/State;
    .restart local v7    # "worklist":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lorg/apache/lucene/util/automaton/State;>;"
    :cond_6
    invoke-virtual {v7}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/util/automaton/State;

    .line 444
    .restart local v5    # "s":Lorg/apache/lucene/util/automaton/State;
    iget v8, v5, Lorg/apache/lucene/util/automaton/State;->number:I

    aget-object v8, v2, v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_7
    :goto_4
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/util/automaton/State;

    .line 445
    .local v3, "p":Lorg/apache/lucene/util/automaton/State;
    invoke-interface {v1, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_7

    .line 446
    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 447
    invoke-virtual {v7, v3}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_4
.end method

.method public static minimize(Lorg/apache/lucene/util/automaton/Automaton;)Lorg/apache/lucene/util/automaton/Automaton;
    .locals 0
    .param p0, "a"    # Lorg/apache/lucene/util/automaton/Automaton;

    .prologue
    .line 773
    invoke-static {p0}, Lorg/apache/lucene/util/automaton/MinimizationOperations;->minimize(Lorg/apache/lucene/util/automaton/Automaton;)V

    .line 774
    return-object p0
.end method

.method public static setAllowMutate(Z)Z
    .locals 1
    .param p0, "flag"    # Z

    .prologue
    .line 168
    sget-boolean v0, Lorg/apache/lucene/util/automaton/Automaton;->allow_mutation:Z

    .line 169
    .local v0, "b":Z
    sput-boolean p0, Lorg/apache/lucene/util/automaton/Automaton;->allow_mutation:Z

    .line 170
    return v0
.end method

.method public static setMinimization(I)V
    .locals 0
    .param p0, "algorithm"    # I

    .prologue
    .line 143
    sput p0, Lorg/apache/lucene/util/automaton/Automaton;->minimization:I

    .line 144
    return-void
.end method

.method public static setMinimizeAlways(Z)V
    .locals 0
    .param p0, "flag"    # Z

    .prologue
    .line 155
    sput-boolean p0, Lorg/apache/lucene/util/automaton/Automaton;->minimize_always:Z

    .line 156
    return-void
.end method

.method public static union(Ljava/util/Collection;)Lorg/apache/lucene/util/automaton/Automaton;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/util/automaton/Automaton;",
            ">;)",
            "Lorg/apache/lucene/util/automaton/Automaton;"
        }
    .end annotation

    .prologue
    .line 751
    .local p0, "l":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/util/automaton/Automaton;>;"
    invoke-static {p0}, Lorg/apache/lucene/util/automaton/BasicOperations;->union(Ljava/util/Collection;)Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method checkMinimizeAlways()V
    .locals 1

    .prologue
    .line 186
    sget-boolean v0, Lorg/apache/lucene/util/automaton/Automaton;->minimize_always:Z

    if-eqz v0, :cond_0

    invoke-static {p0}, Lorg/apache/lucene/util/automaton/MinimizationOperations;->minimize(Lorg/apache/lucene/util/automaton/Automaton;)V

    .line 187
    :cond_0
    return-void
.end method

.method public clearNumberedStates()V
    .locals 1

    .prologue
    .line 326
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/util/automaton/Automaton;->numberedStates:[Lorg/apache/lucene/util/automaton/State;

    .line 327
    return-void
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->clone()Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lorg/apache/lucene/util/automaton/Automaton;
    .locals 14

    .prologue
    const/4 v7, 0x0

    .line 640
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/util/automaton/Automaton;

    .line 641
    .local v0, "a":Lorg/apache/lucene/util/automaton/Automaton;
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->isSingleton()Z

    move-result v8

    if-nez v8, :cond_0

    .line 642
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 643
    .local v2, "m":Ljava/util/HashMap;, "Ljava/util/HashMap<Lorg/apache/lucene/util/automaton/State;Lorg/apache/lucene/util/automaton/State;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->getNumberedStates()[Lorg/apache/lucene/util/automaton/State;

    move-result-object v5

    .line 644
    .local v5, "states":[Lorg/apache/lucene/util/automaton/State;
    array-length v9, v5

    move v8, v7

    :goto_0
    if-lt v8, v9, :cond_1

    .line 646
    array-length v9, v5

    move v8, v7

    :goto_1
    if-lt v8, v9, :cond_2

    .line 654
    .end local v2    # "m":Ljava/util/HashMap;, "Ljava/util/HashMap<Lorg/apache/lucene/util/automaton/State;Lorg/apache/lucene/util/automaton/State;>;"
    .end local v5    # "states":[Lorg/apache/lucene/util/automaton/State;
    :cond_0
    invoke-virtual {v0}, Lorg/apache/lucene/util/automaton/Automaton;->clearNumberedStates()V

    .line 655
    return-object v0

    .line 644
    .restart local v2    # "m":Ljava/util/HashMap;, "Ljava/util/HashMap<Lorg/apache/lucene/util/automaton/State;Lorg/apache/lucene/util/automaton/State;>;"
    .restart local v5    # "states":[Lorg/apache/lucene/util/automaton/State;
    :cond_1
    aget-object v4, v5, v8

    .line 645
    .local v4, "s":Lorg/apache/lucene/util/automaton/State;
    new-instance v10, Lorg/apache/lucene/util/automaton/State;

    invoke-direct {v10}, Lorg/apache/lucene/util/automaton/State;-><init>()V

    invoke-virtual {v2, v4, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 644
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 646
    .end local v4    # "s":Lorg/apache/lucene/util/automaton/State;
    :cond_2
    aget-object v4, v5, v8

    .line 647
    .restart local v4    # "s":Lorg/apache/lucene/util/automaton/State;
    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/util/automaton/State;

    .line 648
    .local v3, "p":Lorg/apache/lucene/util/automaton/State;
    iget-boolean v7, v4, Lorg/apache/lucene/util/automaton/State;->accept:Z

    iput-boolean v7, v3, Lorg/apache/lucene/util/automaton/State;->accept:Z

    .line 649
    iget-object v7, p0, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    if-ne v4, v7, :cond_3

    iput-object v3, v0, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    .line 650
    :cond_3
    invoke-virtual {v4}, Lorg/apache/lucene/util/automaton/State;->getTransitions()Ljava/lang/Iterable;

    move-result-object v7

    invoke-interface {v7}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_4

    .line 646
    add-int/lit8 v7, v8, 0x1

    move v8, v7

    goto :goto_1

    .line 650
    :cond_4
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/lucene/util/automaton/Transition;

    .line 651
    .local v6, "t":Lorg/apache/lucene/util/automaton/Transition;
    new-instance v11, Lorg/apache/lucene/util/automaton/Transition;

    iget v12, v6, Lorg/apache/lucene/util/automaton/Transition;->min:I

    iget v13, v6, Lorg/apache/lucene/util/automaton/Transition;->max:I

    iget-object v7, v6, Lorg/apache/lucene/util/automaton/Transition;->to:Lorg/apache/lucene/util/automaton/State;

    invoke-virtual {v2, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/lucene/util/automaton/State;

    invoke-direct {v11, v12, v13, v7}, Lorg/apache/lucene/util/automaton/Transition;-><init>(IILorg/apache/lucene/util/automaton/State;)V

    invoke-virtual {v3, v11}, Lorg/apache/lucene/util/automaton/State;->addTransition(Lorg/apache/lucene/util/automaton/Transition;)V
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 656
    .end local v0    # "a":Lorg/apache/lucene/util/automaton/Automaton;
    .end local v2    # "m":Ljava/util/HashMap;, "Ljava/util/HashMap<Lorg/apache/lucene/util/automaton/State;Lorg/apache/lucene/util/automaton/State;>;"
    .end local v3    # "p":Lorg/apache/lucene/util/automaton/State;
    .end local v4    # "s":Lorg/apache/lucene/util/automaton/State;
    .end local v5    # "states":[Lorg/apache/lucene/util/automaton/State;
    .end local v6    # "t":Lorg/apache/lucene/util/automaton/Transition;
    :catch_0
    move-exception v1

    .line 657
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v7, Ljava/lang/RuntimeException;

    invoke-direct {v7, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v7
.end method

.method cloneExpanded()Lorg/apache/lucene/util/automaton/Automaton;
    .locals 1

    .prologue
    .line 618
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->clone()Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v0

    .line 619
    .local v0, "a":Lorg/apache/lucene/util/automaton/Automaton;
    invoke-virtual {v0}, Lorg/apache/lucene/util/automaton/Automaton;->expandSingleton()V

    .line 620
    return-object v0
.end method

.method cloneExpandedIfRequired()Lorg/apache/lucene/util/automaton/Automaton;
    .locals 1

    .prologue
    .line 628
    sget-boolean v0, Lorg/apache/lucene/util/automaton/Automaton;->allow_mutation:Z

    if-eqz v0, :cond_0

    .line 629
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->expandSingleton()V

    .line 631
    .end local p0    # "this":Lorg/apache/lucene/util/automaton/Automaton;
    :goto_0
    return-object p0

    .restart local p0    # "this":Lorg/apache/lucene/util/automaton/Automaton;
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->cloneExpanded()Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object p0

    goto :goto_0
.end method

.method cloneIfRequired()Lorg/apache/lucene/util/automaton/Automaton;
    .locals 1

    .prologue
    .line 666
    sget-boolean v0, Lorg/apache/lucene/util/automaton/Automaton;->allow_mutation:Z

    if-eqz v0, :cond_0

    .line 667
    .end local p0    # "this":Lorg/apache/lucene/util/automaton/Automaton;
    :goto_0
    return-object p0

    .restart local p0    # "this":Lorg/apache/lucene/util/automaton/Automaton;
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->clone()Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object p0

    goto :goto_0
.end method

.method public complement()Lorg/apache/lucene/util/automaton/Automaton;
    .locals 1

    .prologue
    .line 716
    invoke-static {p0}, Lorg/apache/lucene/util/automaton/BasicOperations;->complement(Lorg/apache/lucene/util/automaton/Automaton;)Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v0

    return-object v0
.end method

.method public concatenate(Lorg/apache/lucene/util/automaton/Automaton;)Lorg/apache/lucene/util/automaton/Automaton;
    .locals 1
    .param p1, "a"    # Lorg/apache/lucene/util/automaton/Automaton;

    .prologue
    .line 674
    invoke-static {p0, p1}, Lorg/apache/lucene/util/automaton/BasicOperations;->concatenate(Lorg/apache/lucene/util/automaton/Automaton;Lorg/apache/lucene/util/automaton/Automaton;)Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v0

    return-object v0
.end method

.method public determinize()V
    .locals 0

    .prologue
    .line 758
    invoke-static {p0}, Lorg/apache/lucene/util/automaton/BasicOperations;->determinize(Lorg/apache/lucene/util/automaton/Automaton;)V

    .line 759
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 549
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "use BasicOperations.sameLanguage instead"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public expandSingleton()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 513
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->isSingleton()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 514
    new-instance v2, Lorg/apache/lucene/util/automaton/State;

    invoke-direct {v2}, Lorg/apache/lucene/util/automaton/State;-><init>()V

    .line 515
    .local v2, "p":Lorg/apache/lucene/util/automaton/State;
    iput-object v2, p0, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    .line 516
    const/4 v1, 0x0

    .local v1, "i":I
    const/4 v0, 0x0

    .local v0, "cp":I
    :goto_0
    iget-object v4, p0, Lorg/apache/lucene/util/automaton/Automaton;->singleton:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-lt v1, v4, :cond_1

    .line 521
    iput-boolean v6, v2, Lorg/apache/lucene/util/automaton/State;->accept:Z

    .line 522
    iput-boolean v6, p0, Lorg/apache/lucene/util/automaton/Automaton;->deterministic:Z

    .line 523
    const/4 v4, 0x0

    iput-object v4, p0, Lorg/apache/lucene/util/automaton/Automaton;->singleton:Ljava/lang/String;

    .line 525
    .end local v0    # "cp":I
    .end local v1    # "i":I
    .end local v2    # "p":Lorg/apache/lucene/util/automaton/State;
    :cond_0
    return-void

    .line 517
    .restart local v0    # "cp":I
    .restart local v1    # "i":I
    .restart local v2    # "p":Lorg/apache/lucene/util/automaton/State;
    :cond_1
    new-instance v3, Lorg/apache/lucene/util/automaton/State;

    invoke-direct {v3}, Lorg/apache/lucene/util/automaton/State;-><init>()V

    .line 518
    .local v3, "q":Lorg/apache/lucene/util/automaton/State;
    new-instance v4, Lorg/apache/lucene/util/automaton/Transition;

    iget-object v5, p0, Lorg/apache/lucene/util/automaton/Automaton;->singleton:Ljava/lang/String;

    invoke-virtual {v5, v1}, Ljava/lang/String;->codePointAt(I)I

    move-result v0

    invoke-direct {v4, v0, v3}, Lorg/apache/lucene/util/automaton/Transition;-><init>(ILorg/apache/lucene/util/automaton/State;)V

    invoke-virtual {v2, v4}, Lorg/apache/lucene/util/automaton/State;->addTransition(Lorg/apache/lucene/util/automaton/Transition;)V

    .line 519
    move-object v2, v3

    .line 516
    invoke-static {v0}, Ljava/lang/Character;->charCount(I)I

    move-result v4

    add-int/2addr v1, v4

    goto :goto_0
.end method

.method public getAcceptStates()Ljava/util/Set;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lorg/apache/lucene/util/automaton/State;",
            ">;"
        }
    .end annotation

    .prologue
    .line 335
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->expandSingleton()V

    .line 336
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 337
    .local v0, "accepts":Ljava/util/HashSet;, "Ljava/util/HashSet<Lorg/apache/lucene/util/automaton/State;>;"
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 338
    .local v3, "visited":Ljava/util/HashSet;, "Ljava/util/HashSet<Lorg/apache/lucene/util/automaton/State;>;"
    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    .line 339
    .local v4, "worklist":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lorg/apache/lucene/util/automaton/State;>;"
    iget-object v5, p0, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    invoke-virtual {v4, v5}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 340
    iget-object v5, p0, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    invoke-virtual {v3, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 341
    :cond_0
    invoke-virtual {v4}, Ljava/util/LinkedList;->size()I

    move-result v5

    if-gtz v5, :cond_1

    .line 350
    return-object v0

    .line 342
    :cond_1
    invoke-virtual {v4}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/util/automaton/State;

    .line 343
    .local v1, "s":Lorg/apache/lucene/util/automaton/State;
    iget-boolean v5, v1, Lorg/apache/lucene/util/automaton/State;->accept:Z

    if-eqz v5, :cond_2

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 344
    :cond_2
    invoke-virtual {v1}, Lorg/apache/lucene/util/automaton/State;->getTransitions()Ljava/lang/Iterable;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_3
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/util/automaton/Transition;

    .line 345
    .local v2, "t":Lorg/apache/lucene/util/automaton/Transition;
    iget-object v6, v2, Lorg/apache/lucene/util/automaton/Transition;->to:Lorg/apache/lucene/util/automaton/State;

    invoke-virtual {v3, v6}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 346
    iget-object v6, v2, Lorg/apache/lucene/util/automaton/Transition;->to:Lorg/apache/lucene/util/automaton/State;

    invoke-virtual {v3, v6}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 347
    iget-object v6, v2, Lorg/apache/lucene/util/automaton/Transition;->to:Lorg/apache/lucene/util/automaton/State;

    invoke-virtual {v4, v6}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public getInfo()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/Automaton;->info:Ljava/lang/Object;

    return-object v0
.end method

.method public getInitialState()Lorg/apache/lucene/util/automaton/State;
    .locals 1

    .prologue
    .line 222
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->expandSingleton()V

    .line 223
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    return-object v0
.end method

.method public getNumberOfStates()I
    .locals 3

    .prologue
    .line 531
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->isSingleton()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/util/automaton/Automaton;->singleton:Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lorg/apache/lucene/util/automaton/Automaton;->singleton:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->codePointCount(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 532
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->getNumberedStates()[Lorg/apache/lucene/util/automaton/State;

    move-result-object v0

    array-length v0, v0

    goto :goto_0
.end method

.method public getNumberOfTransitions()I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 540
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->isSingleton()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lorg/apache/lucene/util/automaton/Automaton;->singleton:Ljava/lang/String;

    iget-object v4, p0, Lorg/apache/lucene/util/automaton/Automaton;->singleton:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v3, v2, v4}, Ljava/lang/String;->codePointCount(II)I

    move-result v0

    .line 544
    :cond_0
    return v0

    .line 541
    :cond_1
    const/4 v0, 0x0

    .line 542
    .local v0, "c":I
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->getNumberedStates()[Lorg/apache/lucene/util/automaton/State;

    move-result-object v3

    array-length v4, v3

    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v1, v3, v2

    .line 543
    .local v1, "s":Lorg/apache/lucene/util/automaton/State;
    invoke-virtual {v1}, Lorg/apache/lucene/util/automaton/State;->numTransitions()I

    move-result v5

    add-int/2addr v0, v5

    .line 542
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public getNumberedStates()[Lorg/apache/lucene/util/automaton/State;
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 270
    iget-object v7, p0, Lorg/apache/lucene/util/automaton/Automaton;->numberedStates:[Lorg/apache/lucene/util/automaton/State;

    if-nez v7, :cond_1

    .line 271
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->expandSingleton()V

    .line 272
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    .line 273
    .local v5, "visited":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/util/automaton/State;>;"
    new-instance v6, Ljava/util/LinkedList;

    invoke-direct {v6}, Ljava/util/LinkedList;-><init>()V

    .line 274
    .local v6, "worklist":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lorg/apache/lucene/util/automaton/State;>;"
    const/4 v7, 0x4

    new-array v7, v7, [Lorg/apache/lucene/util/automaton/State;

    iput-object v7, p0, Lorg/apache/lucene/util/automaton/Automaton;->numberedStates:[Lorg/apache/lucene/util/automaton/State;

    .line 275
    const/4 v4, 0x0

    .line 276
    .local v4, "upto":I
    iget-object v7, p0, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    invoke-virtual {v6, v7}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 277
    iget-object v7, p0, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    invoke-interface {v5, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 278
    iget-object v7, p0, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    iput v4, v7, Lorg/apache/lucene/util/automaton/State;->number:I

    .line 279
    iget-object v7, p0, Lorg/apache/lucene/util/automaton/Automaton;->numberedStates:[Lorg/apache/lucene/util/automaton/State;

    iget-object v8, p0, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    aput-object v8, v7, v4

    .line 280
    add-int/lit8 v4, v4, 0x1

    .line 281
    :cond_0
    invoke-virtual {v6}, Ljava/util/LinkedList;->size()I

    move-result v7

    if-gtz v7, :cond_2

    .line 299
    iget-object v7, p0, Lorg/apache/lucene/util/automaton/Automaton;->numberedStates:[Lorg/apache/lucene/util/automaton/State;

    array-length v7, v7

    if-eq v7, v4, :cond_1

    .line 300
    new-array v1, v4, [Lorg/apache/lucene/util/automaton/State;

    .line 301
    .local v1, "newArray":[Lorg/apache/lucene/util/automaton/State;
    iget-object v7, p0, Lorg/apache/lucene/util/automaton/Automaton;->numberedStates:[Lorg/apache/lucene/util/automaton/State;

    invoke-static {v7, v9, v1, v9, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 302
    iput-object v1, p0, Lorg/apache/lucene/util/automaton/Automaton;->numberedStates:[Lorg/apache/lucene/util/automaton/State;

    .line 306
    .end local v1    # "newArray":[Lorg/apache/lucene/util/automaton/State;
    .end local v4    # "upto":I
    .end local v5    # "visited":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/util/automaton/State;>;"
    .end local v6    # "worklist":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lorg/apache/lucene/util/automaton/State;>;"
    :cond_1
    iget-object v7, p0, Lorg/apache/lucene/util/automaton/Automaton;->numberedStates:[Lorg/apache/lucene/util/automaton/State;

    return-object v7

    .line 282
    .restart local v4    # "upto":I
    .restart local v5    # "visited":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/util/automaton/State;>;"
    .restart local v6    # "worklist":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lorg/apache/lucene/util/automaton/State;>;"
    :cond_2
    invoke-virtual {v6}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/util/automaton/State;

    .line 283
    .local v2, "s":Lorg/apache/lucene/util/automaton/State;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v7, v2, Lorg/apache/lucene/util/automaton/State;->numTransitions:I

    if-ge v0, v7, :cond_0

    .line 284
    iget-object v7, v2, Lorg/apache/lucene/util/automaton/State;->transitionsArray:[Lorg/apache/lucene/util/automaton/Transition;

    aget-object v3, v7, v0

    .line 285
    .local v3, "t":Lorg/apache/lucene/util/automaton/Transition;
    iget-object v7, v3, Lorg/apache/lucene/util/automaton/Transition;->to:Lorg/apache/lucene/util/automaton/State;

    invoke-interface {v5, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_4

    .line 286
    iget-object v7, v3, Lorg/apache/lucene/util/automaton/Transition;->to:Lorg/apache/lucene/util/automaton/State;

    invoke-interface {v5, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 287
    iget-object v7, v3, Lorg/apache/lucene/util/automaton/Transition;->to:Lorg/apache/lucene/util/automaton/State;

    invoke-virtual {v6, v7}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 288
    iget-object v7, v3, Lorg/apache/lucene/util/automaton/Transition;->to:Lorg/apache/lucene/util/automaton/State;

    iput v4, v7, Lorg/apache/lucene/util/automaton/State;->number:I

    .line 289
    iget-object v7, p0, Lorg/apache/lucene/util/automaton/Automaton;->numberedStates:[Lorg/apache/lucene/util/automaton/State;

    array-length v7, v7

    if-ne v4, v7, :cond_3

    .line 290
    add-int/lit8 v7, v4, 0x1

    sget v8, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_REF:I

    invoke-static {v7, v8}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v7

    new-array v1, v7, [Lorg/apache/lucene/util/automaton/State;

    .line 291
    .restart local v1    # "newArray":[Lorg/apache/lucene/util/automaton/State;
    iget-object v7, p0, Lorg/apache/lucene/util/automaton/Automaton;->numberedStates:[Lorg/apache/lucene/util/automaton/State;

    invoke-static {v7, v9, v1, v9, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 292
    iput-object v1, p0, Lorg/apache/lucene/util/automaton/Automaton;->numberedStates:[Lorg/apache/lucene/util/automaton/State;

    .line 294
    .end local v1    # "newArray":[Lorg/apache/lucene/util/automaton/State;
    :cond_3
    iget-object v7, p0, Lorg/apache/lucene/util/automaton/Automaton;->numberedStates:[Lorg/apache/lucene/util/automaton/State;

    iget-object v8, v3, Lorg/apache/lucene/util/automaton/Transition;->to:Lorg/apache/lucene/util/automaton/State;

    aput-object v8, v7, v4

    .line 295
    add-int/lit8 v4, v4, 0x1

    .line 283
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getSingleton()Ljava/lang/String;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/Automaton;->singleton:Ljava/lang/String;

    return-object v0
.end method

.method public getSortedTransitions()[[Lorg/apache/lucene/util/automaton/Transition;
    .locals 7

    .prologue
    .line 497
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->getNumberedStates()[Lorg/apache/lucene/util/automaton/State;

    move-result-object v1

    .line 498
    .local v1, "states":[Lorg/apache/lucene/util/automaton/State;
    array-length v3, v1

    new-array v2, v3, [[Lorg/apache/lucene/util/automaton/Transition;

    .line 499
    .local v2, "transitions":[[Lorg/apache/lucene/util/automaton/Transition;
    array-length v4, v1

    const/4 v3, 0x0

    :goto_0
    if-lt v3, v4, :cond_0

    .line 505
    return-object v2

    .line 499
    :cond_0
    aget-object v0, v1, v3

    .line 500
    .local v0, "s":Lorg/apache/lucene/util/automaton/State;
    sget-object v5, Lorg/apache/lucene/util/automaton/Transition;->CompareByMinMaxThenDest:Ljava/util/Comparator;

    invoke-virtual {v0, v5}, Lorg/apache/lucene/util/automaton/State;->sortTransitions(Ljava/util/Comparator;)V

    .line 501
    invoke-virtual {v0}, Lorg/apache/lucene/util/automaton/State;->trimTransitionsArray()V

    .line 502
    iget v5, v0, Lorg/apache/lucene/util/automaton/State;->number:I

    iget-object v6, v0, Lorg/apache/lucene/util/automaton/State;->transitionsArray:[Lorg/apache/lucene/util/automaton/Transition;

    aput-object v6, v2, v5

    .line 503
    sget-boolean v5, Lorg/apache/lucene/util/automaton/Automaton;->$assertionsDisabled:Z

    if-nez v5, :cond_1

    iget-object v5, v0, Lorg/apache/lucene/util/automaton/State;->transitionsArray:[Lorg/apache/lucene/util/automaton/Transition;

    if-nez v5, :cond_1

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 499
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method getStartPoints()[I
    .locals 13

    .prologue
    const/4 v8, 0x0

    .line 401
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->getNumberedStates()[Lorg/apache/lucene/util/automaton/State;

    move-result-object v6

    .line 402
    .local v6, "states":[Lorg/apache/lucene/util/automaton/State;
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 403
    .local v4, "pointset":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v4, v9}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 404
    array-length v9, v6

    :goto_0
    if-lt v8, v9, :cond_0

    .line 410
    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v8

    new-array v3, v8, [I

    .line 411
    .local v3, "points":[I
    const/4 v1, 0x0

    .line 412
    .local v1, "n":I
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-nez v9, :cond_3

    .line 414
    invoke-static {v3}, Ljava/util/Arrays;->sort([I)V

    .line 415
    return-object v3

    .line 404
    .end local v1    # "n":I
    .end local v3    # "points":[I
    :cond_0
    aget-object v5, v6, v8

    .line 405
    .local v5, "s":Lorg/apache/lucene/util/automaton/State;
    invoke-virtual {v5}, Lorg/apache/lucene/util/automaton/State;->getTransitions()Ljava/lang/Iterable;

    move-result-object v10

    invoke-interface {v10}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_1
    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-nez v11, :cond_2

    .line 404
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 405
    :cond_2
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/lucene/util/automaton/Transition;

    .line 406
    .local v7, "t":Lorg/apache/lucene/util/automaton/Transition;
    iget v11, v7, Lorg/apache/lucene/util/automaton/Transition;->min:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-interface {v4, v11}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 407
    iget v11, v7, Lorg/apache/lucene/util/automaton/Transition;->max:I

    const v12, 0x10ffff

    if-ge v11, v12, :cond_1

    iget v11, v7, Lorg/apache/lucene/util/automaton/Transition;->max:I

    add-int/lit8 v11, v11, 0x1

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-interface {v4, v11}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 412
    .end local v5    # "s":Lorg/apache/lucene/util/automaton/State;
    .end local v7    # "t":Lorg/apache/lucene/util/automaton/Transition;
    .restart local v1    # "n":I
    .restart local v3    # "points":[I
    :cond_3
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 413
    .local v0, "m":Ljava/lang/Integer;
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "n":I
    .local v2, "n":I
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v9

    aput v9, v3, v1

    move v1, v2

    .end local v2    # "n":I
    .restart local v1    # "n":I
    goto :goto_1
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 554
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public intersection(Lorg/apache/lucene/util/automaton/Automaton;)Lorg/apache/lucene/util/automaton/Automaton;
    .locals 1
    .param p1, "a"    # Lorg/apache/lucene/util/automaton/Automaton;

    .prologue
    .line 730
    invoke-static {p0, p1}, Lorg/apache/lucene/util/automaton/BasicOperations;->intersection(Lorg/apache/lucene/util/automaton/Automaton;Lorg/apache/lucene/util/automaton/Automaton;)Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v0

    return-object v0
.end method

.method public isDeterministic()Z
    .locals 1

    .prologue
    .line 233
    iget-boolean v0, p0, Lorg/apache/lucene/util/automaton/Automaton;->deterministic:Z

    return v0
.end method

.method public isEmptyString()Z
    .locals 1

    .prologue
    .line 765
    invoke-static {p0}, Lorg/apache/lucene/util/automaton/BasicOperations;->isEmptyString(Lorg/apache/lucene/util/automaton/Automaton;)Z

    move-result v0

    return v0
.end method

.method isSingleton()Z
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/Automaton;->singleton:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public minus(Lorg/apache/lucene/util/automaton/Automaton;)Lorg/apache/lucene/util/automaton/Automaton;
    .locals 1
    .param p1, "a"    # Lorg/apache/lucene/util/automaton/Automaton;

    .prologue
    .line 723
    invoke-static {p0, p1}, Lorg/apache/lucene/util/automaton/BasicOperations;->minus(Lorg/apache/lucene/util/automaton/Automaton;Lorg/apache/lucene/util/automaton/Automaton;)Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v0

    return-object v0
.end method

.method public optional()Lorg/apache/lucene/util/automaton/Automaton;
    .locals 1

    .prologue
    .line 688
    invoke-static {p0}, Lorg/apache/lucene/util/automaton/BasicOperations;->optional(Lorg/apache/lucene/util/automaton/Automaton;)Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v0

    return-object v0
.end method

.method public reduce()V
    .locals 4

    .prologue
    .line 391
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->getNumberedStates()[Lorg/apache/lucene/util/automaton/State;

    move-result-object v1

    .line 392
    .local v1, "states":[Lorg/apache/lucene/util/automaton/State;
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->isSingleton()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 395
    :cond_0
    return-void

    .line 393
    :cond_1
    array-length v3, v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v0, v1, v2

    .line 394
    .local v0, "s":Lorg/apache/lucene/util/automaton/State;
    invoke-virtual {v0}, Lorg/apache/lucene/util/automaton/State;->reduce()V

    .line 393
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public removeDeadTransitions()V
    .locals 12

    .prologue
    const/4 v8, 0x0

    .line 460
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->getNumberedStates()[Lorg/apache/lucene/util/automaton/State;

    move-result-object v4

    .line 462
    .local v4, "states":[Lorg/apache/lucene/util/automaton/State;
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->isSingleton()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 490
    :goto_0
    return-void

    .line 463
    :cond_0
    invoke-direct {p0}, Lorg/apache/lucene/util/automaton/Automaton;->getLiveStates()[Lorg/apache/lucene/util/automaton/State;

    move-result-object v1

    .line 465
    .local v1, "live":[Lorg/apache/lucene/util/automaton/State;
    new-instance v2, Ljava/util/BitSet;

    array-length v9, v4

    invoke-direct {v2, v9}, Ljava/util/BitSet;-><init>(I)V

    .line 466
    .local v2, "liveSet":Ljava/util/BitSet;
    array-length v10, v1

    move v9, v8

    :goto_1
    if-lt v9, v10, :cond_1

    .line 469
    array-length v9, v4

    :goto_2
    if-lt v8, v9, :cond_2

    .line 480
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_3
    array-length v8, v1

    if-lt v0, v8, :cond_5

    .line 483
    array-length v8, v1

    if-lez v8, :cond_6

    .line 484
    invoke-virtual {p0, v1}, Lorg/apache/lucene/util/automaton/Automaton;->setNumberedStates([Lorg/apache/lucene/util/automaton/State;)V

    .line 489
    :goto_4
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->reduce()V

    goto :goto_0

    .line 466
    .end local v0    # "i":I
    :cond_1
    aget-object v3, v1, v9

    .line 467
    .local v3, "s":Lorg/apache/lucene/util/automaton/State;
    iget v11, v3, Lorg/apache/lucene/util/automaton/State;->number:I

    invoke-virtual {v2, v11}, Ljava/util/BitSet;->set(I)V

    .line 466
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 469
    .end local v3    # "s":Lorg/apache/lucene/util/automaton/State;
    :cond_2
    aget-object v3, v4, v8

    .line 471
    .restart local v3    # "s":Lorg/apache/lucene/util/automaton/State;
    const/4 v6, 0x0

    .line 472
    .local v6, "upto":I
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_5
    iget v10, v3, Lorg/apache/lucene/util/automaton/State;->numTransitions:I

    if-lt v0, v10, :cond_3

    .line 478
    iput v6, v3, Lorg/apache/lucene/util/automaton/State;->numTransitions:I

    .line 469
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 473
    :cond_3
    iget-object v10, v3, Lorg/apache/lucene/util/automaton/State;->transitionsArray:[Lorg/apache/lucene/util/automaton/Transition;

    aget-object v5, v10, v0

    .line 474
    .local v5, "t":Lorg/apache/lucene/util/automaton/Transition;
    iget-object v10, v5, Lorg/apache/lucene/util/automaton/Transition;->to:Lorg/apache/lucene/util/automaton/State;

    iget v10, v10, Lorg/apache/lucene/util/automaton/State;->number:I

    invoke-virtual {v2, v10}, Ljava/util/BitSet;->get(I)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 475
    iget-object v10, v3, Lorg/apache/lucene/util/automaton/State;->transitionsArray:[Lorg/apache/lucene/util/automaton/Transition;

    add-int/lit8 v7, v6, 0x1

    .end local v6    # "upto":I
    .local v7, "upto":I
    iget-object v11, v3, Lorg/apache/lucene/util/automaton/State;->transitionsArray:[Lorg/apache/lucene/util/automaton/Transition;

    aget-object v11, v11, v0

    aput-object v11, v10, v6

    move v6, v7

    .line 472
    .end local v7    # "upto":I
    .restart local v6    # "upto":I
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 481
    .end local v3    # "s":Lorg/apache/lucene/util/automaton/State;
    .end local v5    # "t":Lorg/apache/lucene/util/automaton/Transition;
    .end local v6    # "upto":I
    :cond_5
    aget-object v8, v1, v0

    iput v0, v8, Lorg/apache/lucene/util/automaton/State;->number:I

    .line 480
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 487
    :cond_6
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->clearNumberedStates()V

    goto :goto_4
.end method

.method public repeat()Lorg/apache/lucene/util/automaton/Automaton;
    .locals 1

    .prologue
    .line 695
    invoke-static {p0}, Lorg/apache/lucene/util/automaton/BasicOperations;->repeat(Lorg/apache/lucene/util/automaton/Automaton;)Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v0

    return-object v0
.end method

.method public repeat(I)Lorg/apache/lucene/util/automaton/Automaton;
    .locals 1
    .param p1, "min"    # I

    .prologue
    .line 702
    invoke-static {p0, p1}, Lorg/apache/lucene/util/automaton/BasicOperations;->repeat(Lorg/apache/lucene/util/automaton/Automaton;I)Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v0

    return-object v0
.end method

.method public repeat(II)Lorg/apache/lucene/util/automaton/Automaton;
    .locals 1
    .param p1, "min"    # I
    .param p2, "max"    # I

    .prologue
    .line 709
    invoke-static {p0, p1, p2}, Lorg/apache/lucene/util/automaton/BasicOperations;->repeat(Lorg/apache/lucene/util/automaton/Automaton;II)Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v0

    return-object v0
.end method

.method public restoreInvariant()V
    .locals 0

    .prologue
    .line 383
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->removeDeadTransitions()V

    .line 384
    return-void
.end method

.method public setDeterministic(Z)V
    .locals 0
    .param p1, "deterministic"    # Z

    .prologue
    .line 244
    iput-boolean p1, p0, Lorg/apache/lucene/util/automaton/Automaton;->deterministic:Z

    .line 245
    return-void
.end method

.method public setInfo(Ljava/lang/Object;)V
    .locals 0
    .param p1, "info"    # Ljava/lang/Object;

    .prologue
    .line 253
    iput-object p1, p0, Lorg/apache/lucene/util/automaton/Automaton;->info:Ljava/lang/Object;

    .line 254
    return-void
.end method

.method public setNumberedStates([Lorg/apache/lucene/util/automaton/State;)V
    .locals 1
    .param p1, "states"    # [Lorg/apache/lucene/util/automaton/State;

    .prologue
    .line 310
    array-length v0, p1

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/util/automaton/Automaton;->setNumberedStates([Lorg/apache/lucene/util/automaton/State;I)V

    .line 311
    return-void
.end method

.method public setNumberedStates([Lorg/apache/lucene/util/automaton/State;I)V
    .locals 3
    .param p1, "states"    # [Lorg/apache/lucene/util/automaton/State;
    .param p2, "count"    # I

    .prologue
    const/4 v2, 0x0

    .line 314
    sget-boolean v1, Lorg/apache/lucene/util/automaton/Automaton;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    array-length v1, p1

    if-le p2, v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 316
    :cond_0
    array-length v1, p1

    if-ge p2, v1, :cond_1

    .line 317
    new-array v0, p2, [Lorg/apache/lucene/util/automaton/State;

    .line 318
    .local v0, "newArray":[Lorg/apache/lucene/util/automaton/State;
    invoke-static {p1, v2, v0, v2, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 319
    iput-object v0, p0, Lorg/apache/lucene/util/automaton/Automaton;->numberedStates:[Lorg/apache/lucene/util/automaton/State;

    .line 323
    .end local v0    # "newArray":[Lorg/apache/lucene/util/automaton/State;
    :goto_0
    return-void

    .line 321
    :cond_1
    iput-object p1, p0, Lorg/apache/lucene/util/automaton/Automaton;->numberedStates:[Lorg/apache/lucene/util/automaton/State;

    goto :goto_0
.end method

.method public subsetOf(Lorg/apache/lucene/util/automaton/Automaton;)Z
    .locals 1
    .param p1, "a"    # Lorg/apache/lucene/util/automaton/Automaton;

    .prologue
    .line 737
    invoke-static {p0, p1}, Lorg/apache/lucene/util/automaton/BasicOperations;->subsetOf(Lorg/apache/lucene/util/automaton/Automaton;Lorg/apache/lucene/util/automaton/Automaton;)Z

    move-result v0

    return v0
.end method

.method public toDot()Ljava/lang/String;
    .locals 9

    .prologue
    .line 595
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "digraph Automaton {\n"

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 596
    .local v0, "b":Ljava/lang/StringBuilder;
    const-string v4, "  rankdir = LR;\n"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 597
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->getNumberedStates()[Lorg/apache/lucene/util/automaton/State;

    move-result-object v2

    .line 598
    .local v2, "states":[Lorg/apache/lucene/util/automaton/State;
    array-length v5, v2

    const/4 v4, 0x0

    :goto_0
    if-lt v4, v5, :cond_0

    .line 611
    const-string/jumbo v4, "}\n"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4

    .line 598
    :cond_0
    aget-object v1, v2, v4

    .line 599
    .local v1, "s":Lorg/apache/lucene/util/automaton/State;
    const-string v6, "  "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v1, Lorg/apache/lucene/util/automaton/State;->number:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 600
    iget-boolean v6, v1, Lorg/apache/lucene/util/automaton/State;->accept:Z

    if-eqz v6, :cond_2

    const-string v6, " [shape=doublecircle,label=\"\"];\n"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 602
    :goto_1
    iget-object v6, p0, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    if-ne v1, v6, :cond_1

    .line 603
    const-string v6, "  initial [shape=plaintext,label=\"\"];\n"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 604
    const-string v6, "  initial -> "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v1, Lorg/apache/lucene/util/automaton/State;->number:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 606
    :cond_1
    invoke-virtual {v1}, Lorg/apache/lucene/util/automaton/State;->getTransitions()Ljava/lang/Iterable;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_3

    .line 598
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 601
    :cond_2
    const-string v6, " [shape=circle,label=\"\"];\n"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 606
    :cond_3
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/util/automaton/Transition;

    .line 607
    .local v3, "t":Lorg/apache/lucene/util/automaton/Transition;
    const-string v7, "  "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v1, Lorg/apache/lucene/util/automaton/State;->number:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 608
    invoke-virtual {v3, v0}, Lorg/apache/lucene/util/automaton/Transition;->appendDot(Ljava/lang/StringBuilder;)V

    goto :goto_2
.end method

.method public toString()Ljava/lang/String;
    .locals 13

    .prologue
    const/4 v10, 0x0

    .line 571
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 572
    .local v0, "b":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->isSingleton()Z

    move-result v11

    if-eqz v11, :cond_3

    .line 573
    const-string v11, "singleton: "

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 574
    iget-object v11, p0, Lorg/apache/lucene/util/automaton/Automaton;->singleton:Ljava/lang/String;

    iget-object v12, p0, Lorg/apache/lucene/util/automaton/Automaton;->singleton:Ljava/lang/String;

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    invoke-virtual {v11, v10, v12}, Ljava/lang/String;->codePointCount(II)I

    move-result v7

    .line 575
    .local v7, "length":I
    new-array v2, v7, [I

    .line 576
    .local v2, "codepoints":[I
    const/4 v4, 0x0

    .local v4, "i":I
    const/4 v5, 0x0

    .local v5, "j":I
    const/4 v3, 0x0

    .local v3, "cp":I
    :goto_0
    iget-object v11, p0, Lorg/apache/lucene/util/automaton/Automaton;->singleton:Ljava/lang/String;

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    if-lt v4, v11, :cond_1

    .line 578
    array-length v11, v2

    :goto_1
    if-lt v10, v11, :cond_2

    .line 580
    const-string v10, "\n"

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 587
    .end local v2    # "codepoints":[I
    .end local v3    # "cp":I
    .end local v4    # "i":I
    .end local v5    # "j":I
    .end local v7    # "length":I
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    return-object v10

    .line 577
    .restart local v2    # "codepoints":[I
    .restart local v3    # "cp":I
    .restart local v4    # "i":I
    .restart local v5    # "j":I
    .restart local v7    # "length":I
    :cond_1
    add-int/lit8 v6, v5, 0x1

    .end local v5    # "j":I
    .local v6, "j":I
    iget-object v11, p0, Lorg/apache/lucene/util/automaton/Automaton;->singleton:Ljava/lang/String;

    invoke-virtual {v11, v4}, Ljava/lang/String;->codePointAt(I)I

    move-result v3

    aput v3, v2, v5

    .line 576
    invoke-static {v3}, Ljava/lang/Character;->charCount(I)I

    move-result v11

    add-int/2addr v4, v11

    move v5, v6

    .end local v6    # "j":I
    .restart local v5    # "j":I
    goto :goto_0

    .line 578
    :cond_2
    aget v1, v2, v10

    .line 579
    .local v1, "c":I
    invoke-static {v1, v0}, Lorg/apache/lucene/util/automaton/Transition;->appendCharString(ILjava/lang/StringBuilder;)V

    .line 578
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 582
    .end local v1    # "c":I
    .end local v2    # "codepoints":[I
    .end local v3    # "cp":I
    .end local v4    # "i":I
    .end local v5    # "j":I
    .end local v7    # "length":I
    :cond_3
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->getNumberedStates()[Lorg/apache/lucene/util/automaton/State;

    move-result-object v9

    .line 583
    .local v9, "states":[Lorg/apache/lucene/util/automaton/State;
    const-string v11, "initial state: "

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    iget v12, v12, Lorg/apache/lucene/util/automaton/State;->number:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "\n"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 584
    array-length v11, v9

    :goto_2
    if-ge v10, v11, :cond_0

    aget-object v8, v9, v10

    .line 585
    .local v8, "s":Lorg/apache/lucene/util/automaton/State;
    invoke-virtual {v8}, Lorg/apache/lucene/util/automaton/State;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 584
    add-int/lit8 v10, v10, 0x1

    goto :goto_2
.end method

.method totalize()V
    .locals 11

    .prologue
    const/4 v4, 0x0

    const v10, 0x10ffff

    .line 358
    new-instance v2, Lorg/apache/lucene/util/automaton/State;

    invoke-direct {v2}, Lorg/apache/lucene/util/automaton/State;-><init>()V

    .line 359
    .local v2, "s":Lorg/apache/lucene/util/automaton/State;
    new-instance v5, Lorg/apache/lucene/util/automaton/Transition;

    .line 360
    invoke-direct {v5, v4, v10, v2}, Lorg/apache/lucene/util/automaton/Transition;-><init>(IILorg/apache/lucene/util/automaton/State;)V

    .line 359
    invoke-virtual {v2, v5}, Lorg/apache/lucene/util/automaton/State;->addTransition(Lorg/apache/lucene/util/automaton/Transition;)V

    .line 361
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->getNumberedStates()[Lorg/apache/lucene/util/automaton/State;

    move-result-object v5

    array-length v6, v5

    :goto_0
    if-lt v4, v6, :cond_0

    .line 372
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->clearNumberedStates()V

    .line 373
    return-void

    .line 361
    :cond_0
    aget-object v1, v5, v4

    .line 362
    .local v1, "p":Lorg/apache/lucene/util/automaton/State;
    const/4 v0, 0x0

    .line 363
    .local v0, "maxi":I
    sget-object v7, Lorg/apache/lucene/util/automaton/Transition;->CompareByMinMaxThenDest:Ljava/util/Comparator;

    invoke-virtual {v1, v7}, Lorg/apache/lucene/util/automaton/State;->sortTransitions(Ljava/util/Comparator;)V

    .line 364
    invoke-virtual {v1}, Lorg/apache/lucene/util/automaton/State;->getTransitions()Ljava/lang/Iterable;

    move-result-object v7

    invoke-interface {v7}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_1
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_3

    .line 369
    if-gt v0, v10, :cond_2

    new-instance v7, Lorg/apache/lucene/util/automaton/Transition;

    .line 370
    invoke-direct {v7, v0, v10, v2}, Lorg/apache/lucene/util/automaton/Transition;-><init>(IILorg/apache/lucene/util/automaton/State;)V

    .line 369
    invoke-virtual {v1, v7}, Lorg/apache/lucene/util/automaton/State;->addTransition(Lorg/apache/lucene/util/automaton/Transition;)V

    .line 361
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 364
    :cond_3
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/util/automaton/Transition;

    .line 365
    .local v3, "t":Lorg/apache/lucene/util/automaton/Transition;
    iget v8, v3, Lorg/apache/lucene/util/automaton/Transition;->min:I

    if-le v8, v0, :cond_4

    new-instance v8, Lorg/apache/lucene/util/automaton/Transition;

    .line 366
    iget v9, v3, Lorg/apache/lucene/util/automaton/Transition;->min:I

    add-int/lit8 v9, v9, -0x1

    invoke-direct {v8, v0, v9, v2}, Lorg/apache/lucene/util/automaton/Transition;-><init>(IILorg/apache/lucene/util/automaton/State;)V

    .line 365
    invoke-virtual {v1, v8}, Lorg/apache/lucene/util/automaton/State;->addTransition(Lorg/apache/lucene/util/automaton/Transition;)V

    .line 367
    :cond_4
    iget v8, v3, Lorg/apache/lucene/util/automaton/Transition;->max:I

    add-int/lit8 v8, v8, 0x1

    if-le v8, v0, :cond_1

    iget v8, v3, Lorg/apache/lucene/util/automaton/Transition;->max:I

    add-int/lit8 v0, v8, 0x1

    goto :goto_1
.end method

.method public union(Lorg/apache/lucene/util/automaton/Automaton;)Lorg/apache/lucene/util/automaton/Automaton;
    .locals 1
    .param p1, "a"    # Lorg/apache/lucene/util/automaton/Automaton;

    .prologue
    .line 744
    invoke-static {p0, p1}, Lorg/apache/lucene/util/automaton/BasicOperations;->union(Lorg/apache/lucene/util/automaton/Automaton;Lorg/apache/lucene/util/automaton/Automaton;)Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v0

    return-object v0
.end method
