.class public Lorg/apache/lucene/util/fst/Util$TopNSearcher;
.super Ljava/lang/Object;
.source "Util.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/fst/Util;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TopNSearcher"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final bytesReader:Lorg/apache/lucene/util/fst/FST$BytesReader;

.field final comparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final fst:Lorg/apache/lucene/util/fst/FST;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/fst/FST",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final maxQueueDepth:I

.field queue:Ljava/util/TreeSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeSet",
            "<",
            "Lorg/apache/lucene/util/fst/Util$FSTPath",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field private final scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final topN:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 281
    const-class v0, Lorg/apache/lucene/util/fst/Util;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/util/fst/FST;IILjava/util/Comparator;)V
    .locals 2
    .param p2, "topN"    # I
    .param p3, "maxQueueDepth"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/FST",
            "<TT;>;II",
            "Ljava/util/Comparator",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 294
    .local p0, "this":Lorg/apache/lucene/util/fst/Util$TopNSearcher;, "Lorg/apache/lucene/util/fst/Util$TopNSearcher<TT;>;"
    .local p1, "fst":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    .local p4, "comparator":Ljava/util/Comparator;, "Ljava/util/Comparator<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 288
    new-instance v0, Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-direct {v0}, Lorg/apache/lucene/util/fst/FST$Arc;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    .line 292
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->queue:Ljava/util/TreeSet;

    .line 295
    iput-object p1, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->fst:Lorg/apache/lucene/util/fst/FST;

    .line 296
    invoke-virtual {p1}, Lorg/apache/lucene/util/fst/FST;->getBytesReader()Lorg/apache/lucene/util/fst/FST$BytesReader;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->bytesReader:Lorg/apache/lucene/util/fst/FST$BytesReader;

    .line 297
    iput p2, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->topN:I

    .line 298
    iput p3, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->maxQueueDepth:I

    .line 299
    iput-object p4, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->comparator:Ljava/util/Comparator;

    .line 301
    new-instance v0, Ljava/util/TreeSet;

    new-instance v1, Lorg/apache/lucene/util/fst/Util$TieBreakByInputComparator;

    invoke-direct {v1, p4}, Lorg/apache/lucene/util/fst/Util$TieBreakByInputComparator;-><init>(Ljava/util/Comparator;)V

    invoke-direct {v0, v1}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    iput-object v0, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->queue:Ljava/util/TreeSet;

    .line 302
    return-void
.end method

.method private addIfCompetitive(Lorg/apache/lucene/util/fst/Util$FSTPath;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/Util$FSTPath",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/util/fst/Util$TopNSearcher;, "Lorg/apache/lucene/util/fst/Util$TopNSearcher<TT;>;"
    .local p1, "path":Lorg/apache/lucene/util/fst/Util$FSTPath;, "Lorg/apache/lucene/util/fst/Util$FSTPath<TT;>;"
    const/4 v10, 0x0

    .line 307
    sget-boolean v6, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->$assertionsDisabled:Z

    if-nez v6, :cond_0

    iget-object v6, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->queue:Ljava/util/TreeSet;

    if-nez v6, :cond_0

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 309
    :cond_0
    iget-object v6, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v6, v6, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    iget-object v7, p1, Lorg/apache/lucene/util/fst/Util$FSTPath;->cost:Ljava/lang/Object;

    iget-object v8, p1, Lorg/apache/lucene/util/fst/Util$FSTPath;->arc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget-object v8, v8, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    invoke-virtual {v6, v7, v8}, Lorg/apache/lucene/util/fst/Outputs;->add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 312
    .local v3, "cost":Ljava/lang/Object;, "TT;"
    iget-object v6, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->queue:Ljava/util/TreeSet;

    invoke-virtual {v6}, Ljava/util/TreeSet;->size()I

    move-result v6

    iget v7, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->maxQueueDepth:I

    if-ne v6, v7, :cond_4

    .line 313
    iget-object v6, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->queue:Ljava/util/TreeSet;

    invoke-virtual {v6}, Ljava/util/TreeSet;->last()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/util/fst/Util$FSTPath;

    .line 314
    .local v0, "bottom":Lorg/apache/lucene/util/fst/Util$FSTPath;, "Lorg/apache/lucene/util/fst/Util$FSTPath<TT;>;"
    iget-object v6, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->comparator:Ljava/util/Comparator;

    iget-object v7, v0, Lorg/apache/lucene/util/fst/Util$FSTPath;->cost:Ljava/lang/Object;

    invoke-interface {v6, v3, v7}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v2

    .line 315
    .local v2, "comp":I
    if-lez v2, :cond_2

    .line 351
    .end local v0    # "bottom":Lorg/apache/lucene/util/fst/Util$FSTPath;, "Lorg/apache/lucene/util/fst/Util$FSTPath<TT;>;"
    .end local v2    # "comp":I
    :cond_1
    :goto_0
    return-void

    .line 318
    .restart local v0    # "bottom":Lorg/apache/lucene/util/fst/Util$FSTPath;, "Lorg/apache/lucene/util/fst/Util$FSTPath<TT;>;"
    .restart local v2    # "comp":I
    :cond_2
    if-nez v2, :cond_4

    .line 320
    iget-object v6, p1, Lorg/apache/lucene/util/fst/Util$FSTPath;->input:Lorg/apache/lucene/util/IntsRef;

    iget-object v7, p1, Lorg/apache/lucene/util/fst/Util$FSTPath;->input:Lorg/apache/lucene/util/IntsRef;

    iget v7, v7, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/lit8 v7, v7, 0x1

    invoke-virtual {v6, v7}, Lorg/apache/lucene/util/IntsRef;->grow(I)V

    .line 321
    iget-object v6, p1, Lorg/apache/lucene/util/fst/Util$FSTPath;->input:Lorg/apache/lucene/util/IntsRef;

    iget-object v6, v6, Lorg/apache/lucene/util/IntsRef;->ints:[I

    iget-object v7, p1, Lorg/apache/lucene/util/fst/Util$FSTPath;->input:Lorg/apache/lucene/util/IntsRef;

    iget v8, v7, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/lit8 v9, v8, 0x1

    iput v9, v7, Lorg/apache/lucene/util/IntsRef;->length:I

    iget-object v7, p1, Lorg/apache/lucene/util/fst/Util$FSTPath;->arc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget v7, v7, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    aput v7, v6, v8

    .line 322
    iget-object v6, v0, Lorg/apache/lucene/util/fst/Util$FSTPath;->input:Lorg/apache/lucene/util/IntsRef;

    iget-object v7, p1, Lorg/apache/lucene/util/fst/Util$FSTPath;->input:Lorg/apache/lucene/util/IntsRef;

    invoke-virtual {v6, v7}, Lorg/apache/lucene/util/IntsRef;->compareTo(Lorg/apache/lucene/util/IntsRef;)I

    move-result v1

    .line 323
    .local v1, "cmp":I
    iget-object v6, p1, Lorg/apache/lucene/util/fst/Util$FSTPath;->input:Lorg/apache/lucene/util/IntsRef;

    iget v7, v6, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/lit8 v7, v7, -0x1

    iput v7, v6, Lorg/apache/lucene/util/IntsRef;->length:I

    .line 326
    sget-boolean v6, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->$assertionsDisabled:Z

    if-nez v6, :cond_3

    if-nez v1, :cond_3

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 328
    :cond_3
    if-ltz v1, :cond_1

    .line 340
    .end local v0    # "bottom":Lorg/apache/lucene/util/fst/Util$FSTPath;, "Lorg/apache/lucene/util/fst/Util$FSTPath<TT;>;"
    .end local v1    # "cmp":I
    .end local v2    # "comp":I
    :cond_4
    new-instance v4, Lorg/apache/lucene/util/IntsRef;

    iget-object v6, p1, Lorg/apache/lucene/util/fst/Util$FSTPath;->input:Lorg/apache/lucene/util/IntsRef;

    iget v6, v6, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/lit8 v6, v6, 0x1

    invoke-direct {v4, v6}, Lorg/apache/lucene/util/IntsRef;-><init>(I)V

    .line 341
    .local v4, "newInput":Lorg/apache/lucene/util/IntsRef;
    iget-object v6, p1, Lorg/apache/lucene/util/fst/Util$FSTPath;->input:Lorg/apache/lucene/util/IntsRef;

    iget-object v6, v6, Lorg/apache/lucene/util/IntsRef;->ints:[I

    iget-object v7, v4, Lorg/apache/lucene/util/IntsRef;->ints:[I

    iget-object v8, p1, Lorg/apache/lucene/util/fst/Util$FSTPath;->input:Lorg/apache/lucene/util/IntsRef;

    iget v8, v8, Lorg/apache/lucene/util/IntsRef;->length:I

    invoke-static {v6, v10, v7, v10, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 342
    iget-object v6, v4, Lorg/apache/lucene/util/IntsRef;->ints:[I

    iget-object v7, p1, Lorg/apache/lucene/util/fst/Util$FSTPath;->input:Lorg/apache/lucene/util/IntsRef;

    iget v7, v7, Lorg/apache/lucene/util/IntsRef;->length:I

    iget-object v8, p1, Lorg/apache/lucene/util/fst/Util$FSTPath;->arc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget v8, v8, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    aput v8, v6, v7

    .line 343
    iget-object v6, p1, Lorg/apache/lucene/util/fst/Util$FSTPath;->input:Lorg/apache/lucene/util/IntsRef;

    iget v6, v6, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/lit8 v6, v6, 0x1

    iput v6, v4, Lorg/apache/lucene/util/IntsRef;->length:I

    .line 344
    new-instance v5, Lorg/apache/lucene/util/fst/Util$FSTPath;

    iget-object v6, p1, Lorg/apache/lucene/util/fst/Util$FSTPath;->arc:Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-direct {v5, v3, v6, v4}, Lorg/apache/lucene/util/fst/Util$FSTPath;-><init>(Ljava/lang/Object;Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/IntsRef;)V

    .line 346
    .local v5, "newPath":Lorg/apache/lucene/util/fst/Util$FSTPath;, "Lorg/apache/lucene/util/fst/Util$FSTPath<TT;>;"
    iget-object v6, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->queue:Ljava/util/TreeSet;

    invoke-virtual {v6, v5}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 348
    iget-object v6, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->queue:Ljava/util/TreeSet;

    invoke-virtual {v6}, Ljava/util/TreeSet;->size()I

    move-result v6

    iget v7, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->maxQueueDepth:I

    add-int/lit8 v7, v7, 0x1

    if-ne v6, v7, :cond_1

    .line 349
    iget-object v6, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->queue:Ljava/util/TreeSet;

    invoke-virtual {v6}, Ljava/util/TreeSet;->pollLast()Ljava/lang/Object;

    goto/16 :goto_0
.end method


# virtual methods
.method protected acceptResult(Lorg/apache/lucene/util/IntsRef;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "input"    # Lorg/apache/lucene/util/IntsRef;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/IntsRef;",
            "TT;)Z"
        }
    .end annotation

    .prologue
    .line 504
    .local p0, "this":Lorg/apache/lucene/util/fst/Util$TopNSearcher;, "Lorg/apache/lucene/util/fst/Util$TopNSearcher<TT;>;"
    .local p2, "output":Ljava/lang/Object;, "TT;"
    const/4 v0, 0x1

    return v0
.end method

.method public addStartPaths(Lorg/apache/lucene/util/fst/FST$Arc;Ljava/lang/Object;ZLorg/apache/lucene/util/IntsRef;)V
    .locals 4
    .param p3, "allowEmptyString"    # Z
    .param p4, "input"    # Lorg/apache/lucene/util/IntsRef;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;TT;Z",
            "Lorg/apache/lucene/util/IntsRef;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 358
    .local p0, "this":Lorg/apache/lucene/util/fst/Util$TopNSearcher;, "Lorg/apache/lucene/util/fst/Util$TopNSearcher<TT;>;"
    .local p1, "node":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    .local p2, "startOutput":Ljava/lang/Object;, "TT;"
    iget-object v1, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v1, v1, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    invoke-virtual {v1}, Lorg/apache/lucene/util/fst/Outputs;->getNoOutput()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 359
    iget-object v1, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v1, v1, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    invoke-virtual {v1}, Lorg/apache/lucene/util/fst/Outputs;->getNoOutput()Ljava/lang/Object;

    move-result-object p2

    .line 362
    :cond_0
    new-instance v0, Lorg/apache/lucene/util/fst/Util$FSTPath;

    invoke-direct {v0, p2, p1, p4}, Lorg/apache/lucene/util/fst/Util$FSTPath;-><init>(Ljava/lang/Object;Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/IntsRef;)V

    .line 363
    .local v0, "path":Lorg/apache/lucene/util/fst/Util$FSTPath;, "Lorg/apache/lucene/util/fst/Util$FSTPath<TT;>;"
    iget-object v1, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v2, v0, Lorg/apache/lucene/util/fst/Util$FSTPath;->arc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget-object v3, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->bytesReader:Lorg/apache/lucene/util/fst/FST$BytesReader;

    invoke-virtual {v1, p1, v2, v3}, Lorg/apache/lucene/util/fst/FST;->readFirstTargetArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 369
    :goto_0
    if-nez p3, :cond_1

    iget-object v1, v0, Lorg/apache/lucene/util/fst/Util$FSTPath;->arc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget v1, v1, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_2

    .line 370
    :cond_1
    invoke-direct {p0, v0}, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->addIfCompetitive(Lorg/apache/lucene/util/fst/Util$FSTPath;)V

    .line 372
    :cond_2
    iget-object v1, v0, Lorg/apache/lucene/util/fst/Util$FSTPath;->arc:Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-virtual {v1}, Lorg/apache/lucene/util/fst/FST$Arc;->isLast()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 377
    return-void

    .line 375
    :cond_3
    iget-object v1, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v2, v0, Lorg/apache/lucene/util/fst/Util$FSTPath;->arc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget-object v3, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->bytesReader:Lorg/apache/lucene/util/fst/FST$BytesReader;

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/util/fst/FST;->readNextArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    goto :goto_0
.end method

.method public search()[Lorg/apache/lucene/util/fst/Util$MinResult;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()[",
            "Lorg/apache/lucene/util/fst/Util$MinResult",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/util/fst/Util$TopNSearcher;, "Lorg/apache/lucene/util/fst/Util$TopNSearcher<TT;>;"
    const/4 v11, -0x1

    .line 381
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 385
    .local v7, "results":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/util/fst/Util$MinResult<TT;>;>;"
    iget-object v8, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->fst:Lorg/apache/lucene/util/fst/FST;

    invoke-virtual {v8}, Lorg/apache/lucene/util/fst/FST;->getBytesReader()Lorg/apache/lucene/util/fst/FST$BytesReader;

    move-result-object v4

    .line 386
    .local v4, "fstReader":Lorg/apache/lucene/util/fst/FST$BytesReader;, "Lorg/apache/lucene/util/fst/FST$BytesReader;"
    iget-object v8, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v8, v8, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    invoke-virtual {v8}, Lorg/apache/lucene/util/fst/Outputs;->getNoOutput()Ljava/lang/Object;

    move-result-object v0

    .line 394
    .local v0, "NO_OUTPUT":Ljava/lang/Object;, "TT;"
    const/4 v6, 0x0

    .line 397
    .local v6, "rejectCount":I
    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v8

    iget v9, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->topN:I

    if-lt v8, v9, :cond_2

    .line 499
    :cond_1
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v8

    new-array v1, v8, [Lorg/apache/lucene/util/fst/Util$MinResult;

    .line 500
    .local v1, "arr":[Lorg/apache/lucene/util/fst/Util$MinResult;
    invoke-interface {v7, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [Lorg/apache/lucene/util/fst/Util$MinResult;

    return-object v8

    .line 402
    .end local v1    # "arr":[Lorg/apache/lucene/util/fst/Util$MinResult;
    :cond_2
    iget-object v8, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->queue:Ljava/util/TreeSet;

    if-eqz v8, :cond_1

    .line 409
    iget-object v8, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->queue:Ljava/util/TreeSet;

    invoke-virtual {v8}, Ljava/util/TreeSet;->pollFirst()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/util/fst/Util$FSTPath;

    .line 411
    .local v5, "path":Lorg/apache/lucene/util/fst/Util$FSTPath;, "Lorg/apache/lucene/util/fst/Util$FSTPath<TT;>;"
    if-eqz v5, :cond_1

    .line 416
    iget-object v8, v5, Lorg/apache/lucene/util/fst/Util$FSTPath;->arc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget v8, v8, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    if-ne v8, v11, :cond_3

    .line 419
    iget-object v8, v5, Lorg/apache/lucene/util/fst/Util$FSTPath;->input:Lorg/apache/lucene/util/IntsRef;

    iget v9, v8, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/lit8 v9, v9, -0x1

    iput v9, v8, Lorg/apache/lucene/util/IntsRef;->length:I

    .line 420
    new-instance v8, Lorg/apache/lucene/util/fst/Util$MinResult;

    iget-object v9, v5, Lorg/apache/lucene/util/fst/Util$FSTPath;->input:Lorg/apache/lucene/util/IntsRef;

    iget-object v10, v5, Lorg/apache/lucene/util/fst/Util$FSTPath;->cost:Ljava/lang/Object;

    invoke-direct {v8, v9, v10}, Lorg/apache/lucene/util/fst/Util$MinResult;-><init>(Lorg/apache/lucene/util/IntsRef;Ljava/lang/Object;)V

    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 424
    :cond_3
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v8

    iget v9, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->topN:I

    add-int/lit8 v9, v9, -0x1

    if-ne v8, v9, :cond_4

    iget v8, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->maxQueueDepth:I

    iget v9, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->topN:I

    if-ne v8, v9, :cond_4

    .line 426
    const/4 v8, 0x0

    iput-object v8, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->queue:Ljava/util/TreeSet;

    .line 441
    :cond_4
    :goto_1
    iget-object v8, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v9, v5, Lorg/apache/lucene/util/fst/Util$FSTPath;->arc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget-object v10, v5, Lorg/apache/lucene/util/fst/Util$FSTPath;->arc:Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-virtual {v8, v9, v10, v4}, Lorg/apache/lucene/util/fst/FST;->readFirstTargetArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 444
    const/4 v3, 0x0

    .line 449
    .local v3, "foundZero":Z
    :goto_2
    iget-object v8, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->comparator:Ljava/util/Comparator;

    iget-object v9, v5, Lorg/apache/lucene/util/fst/Util$FSTPath;->arc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget-object v9, v9, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    invoke-interface {v8, v0, v9}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v8

    if-nez v8, :cond_9

    .line 450
    iget-object v8, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->queue:Ljava/util/TreeSet;

    if-nez v8, :cond_6

    .line 451
    const/4 v3, 0x1

    .line 468
    :cond_5
    sget-boolean v8, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->$assertionsDisabled:Z

    if-nez v8, :cond_a

    if-nez v3, :cond_a

    new-instance v8, Ljava/lang/AssertionError;

    invoke-direct {v8}, Ljava/lang/AssertionError;-><init>()V

    throw v8

    .line 453
    :cond_6
    if-nez v3, :cond_8

    .line 454
    iget-object v8, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget-object v9, v5, Lorg/apache/lucene/util/fst/Util$FSTPath;->arc:Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-virtual {v8, v9}, Lorg/apache/lucene/util/fst/FST$Arc;->copyFrom(Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 455
    const/4 v3, 0x1

    .line 462
    :cond_7
    :goto_3
    iget-object v8, v5, Lorg/apache/lucene/util/fst/Util$FSTPath;->arc:Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-virtual {v8}, Lorg/apache/lucene/util/fst/FST$Arc;->isLast()Z

    move-result v8

    if-nez v8, :cond_5

    .line 465
    iget-object v8, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v9, v5, Lorg/apache/lucene/util/fst/Util$FSTPath;->arc:Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-virtual {v8, v9, v4}, Lorg/apache/lucene/util/fst/FST;->readNextArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    goto :goto_2

    .line 457
    :cond_8
    invoke-direct {p0, v5}, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->addIfCompetitive(Lorg/apache/lucene/util/fst/Util$FSTPath;)V

    goto :goto_3

    .line 459
    :cond_9
    iget-object v8, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->queue:Ljava/util/TreeSet;

    if-eqz v8, :cond_7

    .line 460
    invoke-direct {p0, v5}, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->addIfCompetitive(Lorg/apache/lucene/util/fst/Util$FSTPath;)V

    goto :goto_3

    .line 470
    :cond_a
    iget-object v8, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->queue:Ljava/util/TreeSet;

    if-eqz v8, :cond_b

    .line 475
    iget-object v8, v5, Lorg/apache/lucene/util/fst/Util$FSTPath;->arc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget-object v9, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-virtual {v8, v9}, Lorg/apache/lucene/util/fst/FST$Arc;->copyFrom(Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 478
    :cond_b
    iget-object v8, v5, Lorg/apache/lucene/util/fst/Util$FSTPath;->arc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget v8, v8, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    if-ne v8, v11, :cond_d

    .line 481
    iget-object v8, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v8, v8, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    iget-object v9, v5, Lorg/apache/lucene/util/fst/Util$FSTPath;->cost:Ljava/lang/Object;

    iget-object v10, v5, Lorg/apache/lucene/util/fst/Util$FSTPath;->arc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget-object v10, v10, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    invoke-virtual {v8, v9, v10}, Lorg/apache/lucene/util/fst/Outputs;->add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 482
    .local v2, "finalOutput":Ljava/lang/Object;, "TT;"
    iget-object v8, v5, Lorg/apache/lucene/util/fst/Util$FSTPath;->input:Lorg/apache/lucene/util/IntsRef;

    invoke-virtual {p0, v8, v2}, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->acceptResult(Lorg/apache/lucene/util/IntsRef;Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_c

    .line 483
    new-instance v8, Lorg/apache/lucene/util/fst/Util$MinResult;

    iget-object v9, v5, Lorg/apache/lucene/util/fst/Util$FSTPath;->input:Lorg/apache/lucene/util/IntsRef;

    invoke-direct {v8, v9, v2}, Lorg/apache/lucene/util/fst/Util$MinResult;-><init>(Lorg/apache/lucene/util/IntsRef;Ljava/lang/Object;)V

    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 485
    :cond_c
    add-int/lit8 v6, v6, 0x1

    .line 486
    sget-boolean v8, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->$assertionsDisabled:Z

    if-nez v8, :cond_0

    iget v8, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->topN:I

    add-int/2addr v8, v6

    iget v9, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->maxQueueDepth:I

    if-le v8, v9, :cond_0

    new-instance v8, Ljava/lang/AssertionError;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "maxQueueDepth ("

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v10, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->maxQueueDepth:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ") is too small for topN ("

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->topN:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "): rejected "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " paths"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v8

    .line 490
    .end local v2    # "finalOutput":Ljava/lang/Object;, "TT;"
    :cond_d
    iget-object v8, v5, Lorg/apache/lucene/util/fst/Util$FSTPath;->input:Lorg/apache/lucene/util/IntsRef;

    iget-object v9, v5, Lorg/apache/lucene/util/fst/Util$FSTPath;->input:Lorg/apache/lucene/util/IntsRef;

    iget v9, v9, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/lit8 v9, v9, 0x1

    invoke-virtual {v8, v9}, Lorg/apache/lucene/util/IntsRef;->grow(I)V

    .line 491
    iget-object v8, v5, Lorg/apache/lucene/util/fst/Util$FSTPath;->input:Lorg/apache/lucene/util/IntsRef;

    iget-object v8, v8, Lorg/apache/lucene/util/IntsRef;->ints:[I

    iget-object v9, v5, Lorg/apache/lucene/util/fst/Util$FSTPath;->input:Lorg/apache/lucene/util/IntsRef;

    iget v9, v9, Lorg/apache/lucene/util/IntsRef;->length:I

    iget-object v10, v5, Lorg/apache/lucene/util/fst/Util$FSTPath;->arc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget v10, v10, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    aput v10, v8, v9

    .line 492
    iget-object v8, v5, Lorg/apache/lucene/util/fst/Util$FSTPath;->input:Lorg/apache/lucene/util/IntsRef;

    iget v9, v8, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/lit8 v9, v9, 0x1

    iput v9, v8, Lorg/apache/lucene/util/IntsRef;->length:I

    .line 493
    iget-object v8, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v8, v8, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    iget-object v9, v5, Lorg/apache/lucene/util/fst/Util$FSTPath;->cost:Ljava/lang/Object;

    iget-object v10, v5, Lorg/apache/lucene/util/fst/Util$FSTPath;->arc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget-object v10, v10, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    invoke-virtual {v8, v9, v10}, Lorg/apache/lucene/util/fst/Outputs;->add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    iput-object v8, v5, Lorg/apache/lucene/util/fst/Util$FSTPath;->cost:Ljava/lang/Object;

    goto/16 :goto_1
.end method
