.class public final Lorg/apache/lucene/util/BytesRefHash;
.super Ljava/lang/Object;
.source "BytesRefHash.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/BytesRefHash$BytesStartArray;,
        Lorg/apache/lucene/util/BytesRefHash$DirectBytesStartArray;,
        Lorg/apache/lucene/util/BytesRefHash$MaxBytesLengthExceededException;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final DEFAULT_CAPACITY:I = 0x10


# instance fields
.field bytesStart:[I

.field private final bytesStartArray:Lorg/apache/lucene/util/BytesRefHash$BytesStartArray;

.field private bytesUsed:Lorg/apache/lucene/util/Counter;

.field private count:I

.field private hashHalfSize:I

.field private hashMask:I

.field private hashSize:I

.field private ids:[I

.field private lastCount:I

.field final pool:Lorg/apache/lucene/util/ByteBlockPool;

.field private final scratch1:Lorg/apache/lucene/util/BytesRef;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-class v0, Lorg/apache/lucene/util/BytesRefHash;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/BytesRefHash;->$assertionsDisabled:Z

    .line 48
    return-void

    .line 46
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 70
    new-instance v0, Lorg/apache/lucene/util/ByteBlockPool;

    new-instance v1, Lorg/apache/lucene/util/ByteBlockPool$DirectAllocator;

    invoke-direct {v1}, Lorg/apache/lucene/util/ByteBlockPool$DirectAllocator;-><init>()V

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/ByteBlockPool;-><init>(Lorg/apache/lucene/util/ByteBlockPool$Allocator;)V

    invoke-direct {p0, v0}, Lorg/apache/lucene/util/BytesRefHash;-><init>(Lorg/apache/lucene/util/ByteBlockPool;)V

    .line 71
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/ByteBlockPool;)V
    .locals 2
    .param p1, "pool"    # Lorg/apache/lucene/util/ByteBlockPool;

    .prologue
    const/16 v1, 0x10

    .line 77
    new-instance v0, Lorg/apache/lucene/util/BytesRefHash$DirectBytesStartArray;

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRefHash$DirectBytesStartArray;-><init>(I)V

    invoke-direct {p0, p1, v1, v0}, Lorg/apache/lucene/util/BytesRefHash;-><init>(Lorg/apache/lucene/util/ByteBlockPool;ILorg/apache/lucene/util/BytesRefHash$BytesStartArray;)V

    .line 78
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/ByteBlockPool;ILorg/apache/lucene/util/BytesRefHash$BytesStartArray;)V
    .locals 4
    .param p1, "pool"    # Lorg/apache/lucene/util/ByteBlockPool;
    .param p2, "capacity"    # I
    .param p3, "bytesStartArray"    # Lorg/apache/lucene/util/BytesRefHash$BytesStartArray;

    .prologue
    const/4 v1, -0x1

    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/util/BytesRefHash;->scratch1:Lorg/apache/lucene/util/BytesRef;

    .line 60
    iput v1, p0, Lorg/apache/lucene/util/BytesRefHash;->lastCount:I

    .line 85
    iput p2, p0, Lorg/apache/lucene/util/BytesRefHash;->hashSize:I

    .line 86
    iget v0, p0, Lorg/apache/lucene/util/BytesRefHash;->hashSize:I

    shr-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/util/BytesRefHash;->hashHalfSize:I

    .line 87
    iget v0, p0, Lorg/apache/lucene/util/BytesRefHash;->hashSize:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/util/BytesRefHash;->hashMask:I

    .line 88
    iput-object p1, p0, Lorg/apache/lucene/util/BytesRefHash;->pool:Lorg/apache/lucene/util/ByteBlockPool;

    .line 89
    iget v0, p0, Lorg/apache/lucene/util/BytesRefHash;->hashSize:I

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/apache/lucene/util/BytesRefHash;->ids:[I

    .line 90
    iget-object v0, p0, Lorg/apache/lucene/util/BytesRefHash;->ids:[I

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 91
    iput-object p3, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStartArray:Lorg/apache/lucene/util/BytesRefHash$BytesStartArray;

    .line 92
    invoke-virtual {p3}, Lorg/apache/lucene/util/BytesRefHash$BytesStartArray;->init()[I

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    .line 93
    invoke-virtual {p3}, Lorg/apache/lucene/util/BytesRefHash$BytesStartArray;->bytesUsed()Lorg/apache/lucene/util/Counter;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lorg/apache/lucene/util/Counter;->newCounter()Lorg/apache/lucene/util/Counter;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesUsed:Lorg/apache/lucene/util/Counter;

    .line 94
    iget-object v0, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesUsed:Lorg/apache/lucene/util/Counter;

    iget v1, p0, Lorg/apache/lucene/util/BytesRefHash;->hashSize:I

    mul-int/lit8 v1, v1, 0x4

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/util/Counter;->addAndGet(J)J

    .line 95
    return-void

    .line 93
    :cond_0
    invoke-virtual {p3}, Lorg/apache/lucene/util/BytesRefHash$BytesStartArray;->bytesUsed()Lorg/apache/lucene/util/Counter;

    move-result-object v0

    goto :goto_0
.end method

.method private equals(ILorg/apache/lucene/util/BytesRef;)Z
    .locals 3
    .param p1, "id"    # I
    .param p2, "b"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 205
    iget-object v0, p0, Lorg/apache/lucene/util/BytesRefHash;->pool:Lorg/apache/lucene/util/ByteBlockPool;

    iget-object v1, p0, Lorg/apache/lucene/util/BytesRefHash;->scratch1:Lorg/apache/lucene/util/BytesRef;

    iget-object v2, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    aget v2, v2, p1

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/ByteBlockPool;->setBytesRef(Lorg/apache/lucene/util/BytesRef;I)V

    .line 206
    iget-object v0, p0, Lorg/apache/lucene/util/BytesRefHash;->scratch1:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v0, p2}, Lorg/apache/lucene/util/BytesRef;->bytesEquals(Lorg/apache/lucene/util/BytesRef;)Z

    move-result v0

    return v0
.end method

.method private final findHash(Lorg/apache/lucene/util/BytesRef;I)I
    .locals 5
    .param p1, "bytes"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "code"    # I

    .prologue
    const/4 v4, -0x1

    .line 387
    sget-boolean v3, Lorg/apache/lucene/util/BytesRefHash;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    if-nez v3, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    const-string v4, "bytesStart is null - not initialized"

    invoke-direct {v3, v4}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 389
    :cond_0
    iget v3, p0, Lorg/apache/lucene/util/BytesRefHash;->hashMask:I

    and-int v1, p2, v3

    .line 390
    .local v1, "hashPos":I
    iget-object v3, p0, Lorg/apache/lucene/util/BytesRefHash;->ids:[I

    aget v0, v3, v1

    .line 391
    .local v0, "e":I
    if-eq v0, v4, :cond_2

    invoke-direct {p0, v0, p1}, Lorg/apache/lucene/util/BytesRefHash;->equals(ILorg/apache/lucene/util/BytesRef;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 394
    shr-int/lit8 v3, p2, 0x8

    add-int/2addr v3, p2

    or-int/lit8 v2, v3, 0x1

    .line 396
    .local v2, "inc":I
    :cond_1
    add-int/2addr p2, v2

    .line 397
    iget v3, p0, Lorg/apache/lucene/util/BytesRefHash;->hashMask:I

    and-int v1, p2, v3

    .line 398
    iget-object v3, p0, Lorg/apache/lucene/util/BytesRefHash;->ids:[I

    aget v0, v3, v1

    .line 399
    if-eq v0, v4, :cond_2

    invoke-direct {p0, v0, p1}, Lorg/apache/lucene/util/BytesRefHash;->equals(ILorg/apache/lucene/util/BytesRef;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 402
    .end local v2    # "inc":I
    :cond_2
    return v1
.end method

.method private rehash(IZ)V
    .locals 22
    .param p1, "newSize"    # I
    .param p2, "hashOnData"    # Z

    .prologue
    .line 452
    add-int/lit8 v13, p1, -0x1

    .line 453
    .local v13, "newMask":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/BytesRefHash;->bytesUsed:Lorg/apache/lucene/util/Counter;

    move-object/from16 v18, v0

    mul-int/lit8 v19, p1, 0x4

    move/from16 v0, v19

    int-to-long v0, v0

    move-wide/from16 v20, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/Counter;->addAndGet(J)J

    .line 454
    move/from16 v0, p1

    new-array v12, v0, [I

    .line 455
    .local v12, "newHash":[I
    const/16 v18, -0x1

    move/from16 v0, v18

    invoke-static {v12, v0}, Ljava/util/Arrays;->fill([II)V

    .line 456
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/util/BytesRefHash;->hashSize:I

    move/from16 v18, v0

    move/from16 v0, v18

    if-lt v9, v0, :cond_0

    .line 497
    move-object/from16 v0, p0

    iput v13, v0, Lorg/apache/lucene/util/BytesRefHash;->hashMask:I

    .line 498
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/BytesRefHash;->bytesUsed:Lorg/apache/lucene/util/Counter;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/BytesRefHash;->ids:[I

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    neg-int v0, v0

    move/from16 v19, v0

    mul-int/lit8 v19, v19, 0x4

    move/from16 v0, v19

    int-to-long v0, v0

    move-wide/from16 v20, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/Counter;->addAndGet(J)J

    .line 499
    move-object/from16 v0, p0

    iput-object v12, v0, Lorg/apache/lucene/util/BytesRefHash;->ids:[I

    .line 500
    move/from16 v0, p1

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/util/BytesRefHash;->hashSize:I

    .line 501
    div-int/lit8 v18, p1, 0x2

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/util/BytesRefHash;->hashHalfSize:I

    .line 502
    return-void

    .line 457
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/BytesRefHash;->ids:[I

    move-object/from16 v18, v0

    aget v6, v18, v9

    .line 458
    .local v6, "e0":I
    const/16 v18, -0x1

    move/from16 v0, v18

    if-eq v6, v0, :cond_7

    .line 460
    if-eqz p2, :cond_3

    .line 461
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    move-object/from16 v18, v0

    aget v14, v18, v6

    .line 462
    .local v14, "off":I
    and-int/lit16 v0, v14, 0x7fff

    move/from16 v17, v0

    .line 463
    .local v17, "start":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/BytesRefHash;->pool:Lorg/apache/lucene/util/ByteBlockPool;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lorg/apache/lucene/util/ByteBlockPool;->buffers:[[B

    move-object/from16 v18, v0

    shr-int/lit8 v19, v14, 0xf

    aget-object v4, v18, v19

    .line 464
    .local v4, "bytes":[B
    const/4 v5, 0x0

    .line 467
    .local v5, "code":I
    aget-byte v18, v4, v17

    move/from16 v0, v18

    and-int/lit16 v0, v0, 0x80

    move/from16 v18, v0

    if-nez v18, :cond_1

    .line 469
    aget-byte v11, v4, v17

    .line 470
    .local v11, "len":I
    add-int/lit8 v15, v17, 0x1

    .line 476
    .local v15, "pos":I
    :goto_1
    add-int v7, v15, v11

    .local v7, "endPos":I
    move/from16 v16, v15

    .line 477
    .end local v15    # "pos":I
    .local v16, "pos":I
    :goto_2
    move/from16 v0, v16

    if-lt v0, v7, :cond_2

    .line 484
    .end local v4    # "bytes":[B
    .end local v7    # "endPos":I
    .end local v11    # "len":I
    .end local v14    # "off":I
    .end local v16    # "pos":I
    .end local v17    # "start":I
    :goto_3
    and-int v8, v5, v13

    .line 485
    .local v8, "hashPos":I
    sget-boolean v18, Lorg/apache/lucene/util/BytesRefHash;->$assertionsDisabled:Z

    if-nez v18, :cond_4

    if-gez v8, :cond_4

    new-instance v18, Ljava/lang/AssertionError;

    invoke-direct/range {v18 .. v18}, Ljava/lang/AssertionError;-><init>()V

    throw v18

    .line 472
    .end local v8    # "hashPos":I
    .restart local v4    # "bytes":[B
    .restart local v14    # "off":I
    .restart local v17    # "start":I
    :cond_1
    aget-byte v18, v4, v17

    and-int/lit8 v18, v18, 0x7f

    add-int/lit8 v19, v17, 0x1

    aget-byte v19, v4, v19

    move/from16 v0, v19

    and-int/lit16 v0, v0, 0xff

    move/from16 v19, v0

    shl-int/lit8 v19, v19, 0x7

    add-int v11, v18, v19

    .line 473
    .restart local v11    # "len":I
    add-int/lit8 v15, v17, 0x2

    .restart local v15    # "pos":I
    goto :goto_1

    .line 478
    .end local v15    # "pos":I
    .restart local v7    # "endPos":I
    .restart local v16    # "pos":I
    :cond_2
    mul-int/lit8 v18, v5, 0x1f

    add-int/lit8 v15, v16, 0x1

    .end local v16    # "pos":I
    .restart local v15    # "pos":I
    aget-byte v19, v4, v16

    add-int v5, v18, v19

    move/from16 v16, v15

    .end local v15    # "pos":I
    .restart local v16    # "pos":I
    goto :goto_2

    .line 481
    .end local v4    # "bytes":[B
    .end local v5    # "code":I
    .end local v7    # "endPos":I
    .end local v11    # "len":I
    .end local v14    # "off":I
    .end local v16    # "pos":I
    .end local v17    # "start":I
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    move-object/from16 v18, v0

    aget v5, v18, v6

    .restart local v5    # "code":I
    goto :goto_3

    .line 486
    .restart local v8    # "hashPos":I
    :cond_4
    aget v18, v12, v8

    const/16 v19, -0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-eq v0, v1, :cond_6

    .line 487
    shr-int/lit8 v18, v5, 0x8

    add-int v18, v18, v5

    or-int/lit8 v10, v18, 0x1

    .line 489
    .local v10, "inc":I
    :cond_5
    add-int/2addr v5, v10

    .line 490
    and-int v8, v5, v13

    .line 491
    aget v18, v12, v8

    const/16 v19, -0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_5

    .line 493
    .end local v10    # "inc":I
    :cond_6
    aput v6, v12, v8

    .line 456
    .end local v5    # "code":I
    .end local v8    # "hashPos":I
    :cond_7
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_0
.end method

.method private shrink(I)Z
    .locals 4
    .param p1, "targetSize"    # I

    .prologue
    .line 212
    iget v0, p0, Lorg/apache/lucene/util/BytesRefHash;->hashSize:I

    .line 213
    .local v0, "newSize":I
    :goto_0
    const/16 v1, 0x8

    if-lt v0, v1, :cond_0

    div-int/lit8 v1, v0, 0x4

    if-gt v1, p1, :cond_1

    .line 216
    :cond_0
    iget v1, p0, Lorg/apache/lucene/util/BytesRefHash;->hashSize:I

    if-eq v0, v1, :cond_2

    .line 217
    iget-object v1, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesUsed:Lorg/apache/lucene/util/Counter;

    iget v2, p0, Lorg/apache/lucene/util/BytesRefHash;->hashSize:I

    sub-int/2addr v2, v0

    neg-int v2, v2

    mul-int/lit8 v2, v2, 0x4

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/util/Counter;->addAndGet(J)J

    .line 218
    iput v0, p0, Lorg/apache/lucene/util/BytesRefHash;->hashSize:I

    .line 219
    iget v1, p0, Lorg/apache/lucene/util/BytesRefHash;->hashSize:I

    new-array v1, v1, [I

    iput-object v1, p0, Lorg/apache/lucene/util/BytesRefHash;->ids:[I

    .line 220
    iget-object v1, p0, Lorg/apache/lucene/util/BytesRefHash;->ids:[I

    const/4 v2, -0x1

    invoke-static {v1, v2}, Ljava/util/Arrays;->fill([II)V

    .line 221
    div-int/lit8 v1, v0, 0x2

    iput v1, p0, Lorg/apache/lucene/util/BytesRefHash;->hashHalfSize:I

    .line 222
    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lorg/apache/lucene/util/BytesRefHash;->hashMask:I

    .line 223
    const/4 v1, 0x1

    .line 225
    :goto_1
    return v1

    .line 214
    :cond_1
    div-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 225
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method


# virtual methods
.method public add(Lorg/apache/lucene/util/BytesRef;)I
    .locals 1
    .param p1, "bytes"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 274
    invoke-virtual {p1}, Lorg/apache/lucene/util/BytesRef;->hashCode()I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/util/BytesRefHash;->add(Lorg/apache/lucene/util/BytesRef;I)I

    move-result v0

    return v0
.end method

.method public add(Lorg/apache/lucene/util/BytesRef;I)I
    .locals 10
    .param p1, "bytes"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "code"    # I

    .prologue
    const v7, 0x8000

    const/4 v9, -0x1

    .line 305
    sget-boolean v6, Lorg/apache/lucene/util/BytesRefHash;->$assertionsDisabled:Z

    if-nez v6, :cond_0

    iget-object v6, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    if-nez v6, :cond_0

    new-instance v6, Ljava/lang/AssertionError;

    const-string v7, "Bytesstart is null - not initialized"

    invoke-direct {v6, v7}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v6

    .line 306
    :cond_0
    iget v5, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 308
    .local v5, "length":I
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/util/BytesRefHash;->findHash(Lorg/apache/lucene/util/BytesRef;I)I

    move-result v3

    .line 309
    .local v3, "hashPos":I
    iget-object v6, p0, Lorg/apache/lucene/util/BytesRefHash;->ids:[I

    aget v2, v6, v3

    .line 311
    .local v2, "e":I
    if-ne v2, v9, :cond_8

    .line 313
    iget v6, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v4, v6, 0x2

    .line 314
    .local v4, "len2":I
    iget-object v6, p0, Lorg/apache/lucene/util/BytesRefHash;->pool:Lorg/apache/lucene/util/ByteBlockPool;

    iget v6, v6, Lorg/apache/lucene/util/ByteBlockPool;->byteUpto:I

    add-int/2addr v6, v4

    if-le v6, v7, :cond_2

    .line 315
    if-le v4, v7, :cond_1

    .line 316
    new-instance v6, Lorg/apache/lucene/util/BytesRefHash$MaxBytesLengthExceededException;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "bytes can be at most 32766 in length; got "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 317
    iget v8, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 316
    invoke-direct {v6, v7}, Lorg/apache/lucene/util/BytesRefHash$MaxBytesLengthExceededException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 319
    :cond_1
    iget-object v6, p0, Lorg/apache/lucene/util/BytesRefHash;->pool:Lorg/apache/lucene/util/ByteBlockPool;

    invoke-virtual {v6}, Lorg/apache/lucene/util/ByteBlockPool;->nextBuffer()V

    .line 321
    :cond_2
    iget-object v6, p0, Lorg/apache/lucene/util/BytesRefHash;->pool:Lorg/apache/lucene/util/ByteBlockPool;

    iget-object v0, v6, Lorg/apache/lucene/util/ByteBlockPool;->buffer:[B

    .line 322
    .local v0, "buffer":[B
    iget-object v6, p0, Lorg/apache/lucene/util/BytesRefHash;->pool:Lorg/apache/lucene/util/ByteBlockPool;

    iget v1, v6, Lorg/apache/lucene/util/ByteBlockPool;->byteUpto:I

    .line 323
    .local v1, "bufferUpto":I
    iget v6, p0, Lorg/apache/lucene/util/BytesRefHash;->count:I

    iget-object v7, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    array-length v7, v7

    if-lt v6, v7, :cond_3

    .line 324
    iget-object v6, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStartArray:Lorg/apache/lucene/util/BytesRefHash$BytesStartArray;

    invoke-virtual {v6}, Lorg/apache/lucene/util/BytesRefHash$BytesStartArray;->grow()[I

    move-result-object v6

    iput-object v6, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    .line 325
    sget-boolean v6, Lorg/apache/lucene/util/BytesRefHash;->$assertionsDisabled:Z

    if-nez v6, :cond_3

    iget v6, p0, Lorg/apache/lucene/util/BytesRefHash;->count:I

    iget-object v7, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    array-length v7, v7

    add-int/lit8 v7, v7, 0x1

    if-lt v6, v7, :cond_3

    new-instance v6, Ljava/lang/AssertionError;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "count: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v8, p0, Lorg/apache/lucene/util/BytesRefHash;->count:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " len: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 326
    iget-object v8, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    array-length v8, v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v6

    .line 328
    :cond_3
    iget v2, p0, Lorg/apache/lucene/util/BytesRefHash;->count:I

    .end local v2    # "e":I
    add-int/lit8 v6, v2, 0x1

    iput v6, p0, Lorg/apache/lucene/util/BytesRefHash;->count:I

    .line 330
    .restart local v2    # "e":I
    iget-object v6, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    iget-object v7, p0, Lorg/apache/lucene/util/BytesRefHash;->pool:Lorg/apache/lucene/util/ByteBlockPool;

    iget v7, v7, Lorg/apache/lucene/util/ByteBlockPool;->byteOffset:I

    add-int/2addr v7, v1

    aput v7, v6, v2

    .line 336
    const/16 v6, 0x80

    if-ge v5, v6, :cond_5

    .line 338
    int-to-byte v6, v5

    aput-byte v6, v0, v1

    .line 339
    iget-object v6, p0, Lorg/apache/lucene/util/BytesRefHash;->pool:Lorg/apache/lucene/util/ByteBlockPool;

    iget v7, v6, Lorg/apache/lucene/util/ByteBlockPool;->byteUpto:I

    add-int/lit8 v8, v5, 0x1

    add-int/2addr v7, v8

    iput v7, v6, Lorg/apache/lucene/util/ByteBlockPool;->byteUpto:I

    .line 340
    sget-boolean v6, Lorg/apache/lucene/util/BytesRefHash;->$assertionsDisabled:Z

    if-nez v6, :cond_4

    if-gez v5, :cond_4

    new-instance v6, Ljava/lang/AssertionError;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Length must be positive: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v6

    .line 341
    :cond_4
    iget-object v6, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v7, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    add-int/lit8 v8, v1, 0x1

    invoke-static {v6, v7, v0, v8, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 351
    :goto_0
    sget-boolean v6, Lorg/apache/lucene/util/BytesRefHash;->$assertionsDisabled:Z

    if-nez v6, :cond_6

    iget-object v6, p0, Lorg/apache/lucene/util/BytesRefHash;->ids:[I

    aget v6, v6, v3

    if-eq v6, v9, :cond_6

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 345
    :cond_5
    and-int/lit8 v6, v5, 0x7f

    or-int/lit16 v6, v6, 0x80

    int-to-byte v6, v6

    aput-byte v6, v0, v1

    .line 346
    add-int/lit8 v6, v1, 0x1

    shr-int/lit8 v7, v5, 0x7

    and-int/lit16 v7, v7, 0xff

    int-to-byte v7, v7

    aput-byte v7, v0, v6

    .line 347
    iget-object v6, p0, Lorg/apache/lucene/util/BytesRefHash;->pool:Lorg/apache/lucene/util/ByteBlockPool;

    iget v7, v6, Lorg/apache/lucene/util/ByteBlockPool;->byteUpto:I

    add-int/lit8 v8, v5, 0x2

    add-int/2addr v7, v8

    iput v7, v6, Lorg/apache/lucene/util/ByteBlockPool;->byteUpto:I

    .line 348
    iget-object v6, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v7, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    add-int/lit8 v8, v1, 0x2

    invoke-static {v6, v7, v0, v8, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0

    .line 352
    :cond_6
    iget-object v6, p0, Lorg/apache/lucene/util/BytesRefHash;->ids:[I

    aput v2, v6, v3

    .line 354
    iget v6, p0, Lorg/apache/lucene/util/BytesRefHash;->count:I

    iget v7, p0, Lorg/apache/lucene/util/BytesRefHash;->hashHalfSize:I

    if-ne v6, v7, :cond_7

    .line 355
    iget v6, p0, Lorg/apache/lucene/util/BytesRefHash;->hashSize:I

    mul-int/lit8 v6, v6, 0x2

    const/4 v7, 0x1

    invoke-direct {p0, v6, v7}, Lorg/apache/lucene/util/BytesRefHash;->rehash(IZ)V

    :cond_7
    move v6, v2

    .line 359
    .end local v0    # "buffer":[B
    .end local v1    # "bufferUpto":I
    .end local v4    # "len2":I
    :goto_1
    return v6

    :cond_8
    add-int/lit8 v6, v2, 0x1

    neg-int v6, v6

    goto :goto_1
.end method

.method public addByPoolOffset(I)I
    .locals 7
    .param p1, "offset"    # I

    .prologue
    const/4 v6, -0x1

    .line 412
    sget-boolean v4, Lorg/apache/lucene/util/BytesRefHash;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    iget-object v4, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    if-nez v4, :cond_0

    new-instance v4, Ljava/lang/AssertionError;

    const-string v5, "Bytesstart is null - not initialized"

    invoke-direct {v4, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    .line 414
    :cond_0
    move v0, p1

    .line 415
    .local v0, "code":I
    iget v4, p0, Lorg/apache/lucene/util/BytesRefHash;->hashMask:I

    and-int v2, p1, v4

    .line 416
    .local v2, "hashPos":I
    iget-object v4, p0, Lorg/apache/lucene/util/BytesRefHash;->ids:[I

    aget v1, v4, v2

    .line 417
    .local v1, "e":I
    if-eq v1, v6, :cond_2

    iget-object v4, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    aget v4, v4, v1

    if-eq v4, p1, :cond_2

    .line 420
    shr-int/lit8 v4, v0, 0x8

    add-int/2addr v4, v0

    or-int/lit8 v3, v4, 0x1

    .line 422
    .local v3, "inc":I
    :cond_1
    add-int/2addr v0, v3

    .line 423
    iget v4, p0, Lorg/apache/lucene/util/BytesRefHash;->hashMask:I

    and-int v2, v0, v4

    .line 424
    iget-object v4, p0, Lorg/apache/lucene/util/BytesRefHash;->ids:[I

    aget v1, v4, v2

    .line 425
    if-eq v1, v6, :cond_2

    iget-object v4, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    aget v4, v4, v1

    if-ne v4, p1, :cond_1

    .line 427
    .end local v3    # "inc":I
    :cond_2
    if-ne v1, v6, :cond_6

    .line 429
    iget v4, p0, Lorg/apache/lucene/util/BytesRefHash;->count:I

    iget-object v5, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    array-length v5, v5

    if-lt v4, v5, :cond_3

    .line 430
    iget-object v4, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStartArray:Lorg/apache/lucene/util/BytesRefHash$BytesStartArray;

    invoke-virtual {v4}, Lorg/apache/lucene/util/BytesRefHash$BytesStartArray;->grow()[I

    move-result-object v4

    iput-object v4, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    .line 431
    sget-boolean v4, Lorg/apache/lucene/util/BytesRefHash;->$assertionsDisabled:Z

    if-nez v4, :cond_3

    iget v4, p0, Lorg/apache/lucene/util/BytesRefHash;->count:I

    iget-object v5, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    array-length v5, v5

    add-int/lit8 v5, v5, 0x1

    if-lt v4, v5, :cond_3

    new-instance v4, Ljava/lang/AssertionError;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "count: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, Lorg/apache/lucene/util/BytesRefHash;->count:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " len: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 432
    iget-object v6, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    array-length v6, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    .line 434
    :cond_3
    iget v1, p0, Lorg/apache/lucene/util/BytesRefHash;->count:I

    .end local v1    # "e":I
    add-int/lit8 v4, v1, 0x1

    iput v4, p0, Lorg/apache/lucene/util/BytesRefHash;->count:I

    .line 435
    .restart local v1    # "e":I
    iget-object v4, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    aput p1, v4, v1

    .line 436
    sget-boolean v4, Lorg/apache/lucene/util/BytesRefHash;->$assertionsDisabled:Z

    if-nez v4, :cond_4

    iget-object v4, p0, Lorg/apache/lucene/util/BytesRefHash;->ids:[I

    aget v4, v4, v2

    if-eq v4, v6, :cond_4

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 437
    :cond_4
    iget-object v4, p0, Lorg/apache/lucene/util/BytesRefHash;->ids:[I

    aput v1, v4, v2

    .line 439
    iget v4, p0, Lorg/apache/lucene/util/BytesRefHash;->count:I

    iget v5, p0, Lorg/apache/lucene/util/BytesRefHash;->hashHalfSize:I

    if-ne v4, v5, :cond_5

    .line 440
    iget v4, p0, Lorg/apache/lucene/util/BytesRefHash;->hashSize:I

    mul-int/lit8 v4, v4, 0x2

    const/4 v5, 0x0

    invoke-direct {p0, v4, v5}, Lorg/apache/lucene/util/BytesRefHash;->rehash(IZ)V

    :cond_5
    move v4, v1

    .line 444
    :goto_0
    return v4

    :cond_6
    add-int/lit8 v4, v1, 0x1

    neg-int v4, v4

    goto :goto_0
.end method

.method public byteStart(I)I
    .locals 2
    .param p1, "bytesID"    # I

    .prologue
    .line 530
    sget-boolean v0, Lorg/apache/lucene/util/BytesRefHash;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "bytesStart is null - not initialized"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 531
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/util/BytesRefHash;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    if-ltz p1, :cond_1

    iget v0, p0, Lorg/apache/lucene/util/BytesRefHash;->count:I

    if-lt p1, v0, :cond_2

    :cond_1
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, p1}, Ljava/lang/AssertionError;-><init>(I)V

    throw v0

    .line 532
    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    aget v0, v0, p1

    return v0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 247
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lorg/apache/lucene/util/BytesRefHash;->clear(Z)V

    .line 248
    return-void
.end method

.method public clear(Z)V
    .locals 3
    .param p1, "resetPool"    # Z

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 233
    iget v0, p0, Lorg/apache/lucene/util/BytesRefHash;->count:I

    iput v0, p0, Lorg/apache/lucene/util/BytesRefHash;->lastCount:I

    .line 234
    iput v1, p0, Lorg/apache/lucene/util/BytesRefHash;->count:I

    .line 235
    if-eqz p1, :cond_0

    .line 236
    iget-object v0, p0, Lorg/apache/lucene/util/BytesRefHash;->pool:Lorg/apache/lucene/util/ByteBlockPool;

    invoke-virtual {v0, v1, v1}, Lorg/apache/lucene/util/ByteBlockPool;->reset(ZZ)V

    .line 238
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStartArray:Lorg/apache/lucene/util/BytesRefHash$BytesStartArray;

    invoke-virtual {v0}, Lorg/apache/lucene/util/BytesRefHash$BytesStartArray;->clear()[I

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    .line 239
    iget v0, p0, Lorg/apache/lucene/util/BytesRefHash;->lastCount:I

    if-eq v0, v2, :cond_1

    iget v0, p0, Lorg/apache/lucene/util/BytesRefHash;->lastCount:I

    invoke-direct {p0, v0}, Lorg/apache/lucene/util/BytesRefHash;->shrink(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 244
    :goto_0
    return-void

    .line 243
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/util/BytesRefHash;->ids:[I

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([II)V

    goto :goto_0
.end method

.method public close()V
    .locals 4

    .prologue
    .line 254
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lorg/apache/lucene/util/BytesRefHash;->clear(Z)V

    .line 255
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/util/BytesRefHash;->ids:[I

    .line 256
    iget-object v0, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesUsed:Lorg/apache/lucene/util/Counter;

    iget v1, p0, Lorg/apache/lucene/util/BytesRefHash;->hashSize:I

    neg-int v1, v1

    mul-int/lit8 v1, v1, 0x4

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/util/Counter;->addAndGet(J)J

    .line 257
    return-void
.end method

.method compact()[I
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 137
    sget-boolean v2, Lorg/apache/lucene/util/BytesRefHash;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    const-string v3, "bytesStart is null - not initialized"

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 138
    :cond_0
    const/4 v1, 0x0

    .line 139
    .local v1, "upto":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lorg/apache/lucene/util/BytesRefHash;->hashSize:I

    if-lt v0, v2, :cond_1

    .line 149
    sget-boolean v2, Lorg/apache/lucene/util/BytesRefHash;->$assertionsDisabled:Z

    if-nez v2, :cond_4

    iget v2, p0, Lorg/apache/lucene/util/BytesRefHash;->count:I

    if-eq v1, v2, :cond_4

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 140
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/util/BytesRefHash;->ids:[I

    aget v2, v2, v0

    if-eq v2, v4, :cond_3

    .line 141
    if-ge v1, v0, :cond_2

    .line 142
    iget-object v2, p0, Lorg/apache/lucene/util/BytesRefHash;->ids:[I

    iget-object v3, p0, Lorg/apache/lucene/util/BytesRefHash;->ids:[I

    aget v3, v3, v0

    aput v3, v2, v1

    .line 143
    iget-object v2, p0, Lorg/apache/lucene/util/BytesRefHash;->ids:[I

    aput v4, v2, v0

    .line 145
    :cond_2
    add-int/lit8 v1, v1, 0x1

    .line 139
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 150
    :cond_4
    iget v2, p0, Lorg/apache/lucene/util/BytesRefHash;->count:I

    iput v2, p0, Lorg/apache/lucene/util/BytesRefHash;->lastCount:I

    .line 151
    iget-object v2, p0, Lorg/apache/lucene/util/BytesRefHash;->ids:[I

    return-object v2
.end method

.method public find(Lorg/apache/lucene/util/BytesRef;)I
    .locals 1
    .param p1, "bytes"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 368
    invoke-virtual {p1}, Lorg/apache/lucene/util/BytesRef;->hashCode()I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/util/BytesRefHash;->find(Lorg/apache/lucene/util/BytesRef;I)I

    move-result v0

    return v0
.end method

.method public find(Lorg/apache/lucene/util/BytesRef;I)I
    .locals 2
    .param p1, "bytes"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "code"    # I

    .prologue
    .line 383
    iget-object v0, p0, Lorg/apache/lucene/util/BytesRefHash;->ids:[I

    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/util/BytesRefHash;->findHash(Lorg/apache/lucene/util/BytesRef;I)I

    move-result v1

    aget v0, v0, v1

    return v0
.end method

.method public get(ILorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/BytesRef;
    .locals 3
    .param p1, "bytesID"    # I
    .param p2, "ref"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 122
    sget-boolean v0, Lorg/apache/lucene/util/BytesRefHash;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "bytesStart is null - not initialized"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 123
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/util/BytesRefHash;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    array-length v0, v0

    if-lt p1, v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "bytesID exceeds byteStart len: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 124
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/util/BytesRefHash;->pool:Lorg/apache/lucene/util/ByteBlockPool;

    iget-object v1, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    aget v1, v1, p1

    invoke-virtual {v0, p2, v1}, Lorg/apache/lucene/util/ByteBlockPool;->setBytesRef(Lorg/apache/lucene/util/BytesRef;I)V

    .line 125
    return-object p2
.end method

.method public reinit()V
    .locals 4

    .prologue
    .line 510
    iget-object v0, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    if-nez v0, :cond_0

    .line 511
    iget-object v0, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStartArray:Lorg/apache/lucene/util/BytesRefHash$BytesStartArray;

    invoke-virtual {v0}, Lorg/apache/lucene/util/BytesRefHash$BytesStartArray;->init()[I

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    .line 514
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/util/BytesRefHash;->ids:[I

    if-nez v0, :cond_1

    .line 515
    iget v0, p0, Lorg/apache/lucene/util/BytesRefHash;->hashSize:I

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/apache/lucene/util/BytesRefHash;->ids:[I

    .line 516
    iget-object v0, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesUsed:Lorg/apache/lucene/util/Counter;

    iget v1, p0, Lorg/apache/lucene/util/BytesRefHash;->hashSize:I

    mul-int/lit8 v1, v1, 0x4

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/util/Counter;->addAndGet(J)J

    .line 518
    :cond_1
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 103
    iget v0, p0, Lorg/apache/lucene/util/BytesRefHash;->count:I

    return v0
.end method

.method public sort(Ljava/util/Comparator;)[I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;)[I"
        }
    .end annotation

    .prologue
    .line 165
    .local p1, "comp":Ljava/util/Comparator;, "Ljava/util/Comparator<Lorg/apache/lucene/util/BytesRef;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/util/BytesRefHash;->compact()[I

    move-result-object v0

    .line 166
    .local v0, "compact":[I
    new-instance v1, Lorg/apache/lucene/util/BytesRefHash$1;

    invoke-direct {v1, p0, v0, p1}, Lorg/apache/lucene/util/BytesRefHash$1;-><init>(Lorg/apache/lucene/util/BytesRefHash;[ILjava/util/Comparator;)V

    .line 200
    const/4 v2, 0x0

    iget v3, p0, Lorg/apache/lucene/util/BytesRefHash;->count:I

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/util/BytesRefHash$1;->quickSort(II)V

    .line 201
    return-object v0
.end method
