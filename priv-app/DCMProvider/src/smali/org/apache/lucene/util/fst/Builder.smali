.class public Lorg/apache/lucene/util/fst/Builder;
.super Ljava/lang/Object;
.source "Builder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/fst/Builder$Arc;,
        Lorg/apache/lucene/util/fst/Builder$CompiledNode;,
        Lorg/apache/lucene/util/fst/Builder$FreezeTail;,
        Lorg/apache/lucene/util/fst/Builder$Node;,
        Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final NO_OUTPUT:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final acceptableOverheadRatio:F

.field private final dedupHash:Lorg/apache/lucene/util/fst/NodeHash;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/fst/NodeHash",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final doPackFST:Z

.field private final doShareNonSingletonNodes:Z

.field private final freezeTail:Lorg/apache/lucene/util/fst/Builder$FreezeTail;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/fst/Builder$FreezeTail",
            "<TT;>;"
        }
    .end annotation
.end field

.field private frontier:[Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lorg/apache/lucene/util/fst/Builder$UnCompiledNode",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final fst:Lorg/apache/lucene/util/fst/FST;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/fst/FST",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final lastInput:Lorg/apache/lucene/util/IntsRef;

.field private final minSuffixCount1:I

.field private final minSuffixCount2:I

.field private final shareMaxTailLength:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const-class v0, Lorg/apache/lucene/util/fst/Builder;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/fst/Builder;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;IIZZILorg/apache/lucene/util/fst/Outputs;Lorg/apache/lucene/util/fst/Builder$FreezeTail;ZFZI)V
    .locals 10
    .param p2, "minSuffixCount1"    # I
    .param p3, "minSuffixCount2"    # I
    .param p4, "doShareSuffix"    # Z
    .param p5, "doShareNonSingletonNodes"    # Z
    .param p6, "shareMaxTailLength"    # I
    .param p9, "doPackFST"    # Z
    .param p10, "acceptableOverheadRatio"    # F
    .param p11, "allowArrayArcs"    # Z
    .param p12, "bytesPageBits"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;",
            "IIZZI",
            "Lorg/apache/lucene/util/fst/Outputs",
            "<TT;>;",
            "Lorg/apache/lucene/util/fst/Builder$FreezeTail",
            "<TT;>;ZFZI)V"
        }
    .end annotation

    .prologue
    .line 152
    .local p0, "this":Lorg/apache/lucene/util/fst/Builder;, "Lorg/apache/lucene/util/fst/Builder<TT;>;"
    .local p1, "inputType":Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;, "Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;"
    .local p7, "outputs":Lorg/apache/lucene/util/fst/Outputs;, "Lorg/apache/lucene/util/fst/Outputs<TT;>;"
    .local p8, "freezeTail":Lorg/apache/lucene/util/fst/Builder$FreezeTail;, "Lorg/apache/lucene/util/fst/Builder$FreezeTail<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    new-instance v1, Lorg/apache/lucene/util/IntsRef;

    invoke-direct {v1}, Lorg/apache/lucene/util/IntsRef;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/util/fst/Builder;->lastInput:Lorg/apache/lucene/util/IntsRef;

    .line 156
    iput p2, p0, Lorg/apache/lucene/util/fst/Builder;->minSuffixCount1:I

    .line 157
    iput p3, p0, Lorg/apache/lucene/util/fst/Builder;->minSuffixCount2:I

    .line 158
    move-object/from16 v0, p8

    iput-object v0, p0, Lorg/apache/lucene/util/fst/Builder;->freezeTail:Lorg/apache/lucene/util/fst/Builder$FreezeTail;

    .line 159
    iput-boolean p5, p0, Lorg/apache/lucene/util/fst/Builder;->doShareNonSingletonNodes:Z

    .line 160
    move/from16 v0, p6

    iput v0, p0, Lorg/apache/lucene/util/fst/Builder;->shareMaxTailLength:I

    .line 161
    move/from16 v0, p9

    iput-boolean v0, p0, Lorg/apache/lucene/util/fst/Builder;->doPackFST:Z

    .line 162
    move/from16 v0, p10

    iput v0, p0, Lorg/apache/lucene/util/fst/Builder;->acceptableOverheadRatio:F

    .line 163
    new-instance v1, Lorg/apache/lucene/util/fst/FST;

    move-object v2, p1

    move-object/from16 v3, p7

    move/from16 v4, p9

    move/from16 v5, p10

    move/from16 v6, p11

    move/from16 v7, p12

    invoke-direct/range {v1 .. v7}, Lorg/apache/lucene/util/fst/FST;-><init>(Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;Lorg/apache/lucene/util/fst/Outputs;ZFZI)V

    iput-object v1, p0, Lorg/apache/lucene/util/fst/Builder;->fst:Lorg/apache/lucene/util/fst/FST;

    .line 164
    if-eqz p4, :cond_0

    .line 165
    new-instance v1, Lorg/apache/lucene/util/fst/NodeHash;

    iget-object v2, p0, Lorg/apache/lucene/util/fst/Builder;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v3, p0, Lorg/apache/lucene/util/fst/Builder;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v3, v3, Lorg/apache/lucene/util/fst/FST;->bytes:Lorg/apache/lucene/util/fst/BytesStore;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lorg/apache/lucene/util/fst/BytesStore;->getReverseReader(Z)Lorg/apache/lucene/util/fst/FST$BytesReader;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/lucene/util/fst/NodeHash;-><init>(Lorg/apache/lucene/util/fst/FST;Lorg/apache/lucene/util/fst/FST$BytesReader;)V

    iput-object v1, p0, Lorg/apache/lucene/util/fst/Builder;->dedupHash:Lorg/apache/lucene/util/fst/NodeHash;

    .line 169
    :goto_0
    invoke-virtual/range {p7 .. p7}, Lorg/apache/lucene/util/fst/Outputs;->getNoOutput()Ljava/lang/Object;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/util/fst/Builder;->NO_OUTPUT:Ljava/lang/Object;

    .line 172
    const/16 v1, 0xa

    new-array v8, v1, [Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;

    .line 173
    .local v8, "f":[Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;
    iput-object v8, p0, Lorg/apache/lucene/util/fst/Builder;->frontier:[Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;

    .line 174
    const/4 v9, 0x0

    .local v9, "idx":I
    :goto_1
    iget-object v1, p0, Lorg/apache/lucene/util/fst/Builder;->frontier:[Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;

    array-length v1, v1

    if-lt v9, v1, :cond_1

    .line 177
    return-void

    .line 167
    .end local v8    # "f":[Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;
    .end local v9    # "idx":I
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/lucene/util/fst/Builder;->dedupHash:Lorg/apache/lucene/util/fst/NodeHash;

    goto :goto_0

    .line 175
    .restart local v8    # "f":[Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;
    .restart local v9    # "idx":I
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/util/fst/Builder;->frontier:[Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;

    new-instance v2, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;

    invoke-direct {v2, p0, v9}, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;-><init>(Lorg/apache/lucene/util/fst/Builder;I)V

    aput-object v2, v1, v9

    .line 174
    add-int/lit8 v9, v9, 0x1

    goto :goto_1
.end method

.method public constructor <init>(Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;Lorg/apache/lucene/util/fst/Outputs;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;",
            "Lorg/apache/lucene/util/fst/Outputs",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/util/fst/Builder;, "Lorg/apache/lucene/util/fst/Builder<TT;>;"
    .local p1, "inputType":Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;, "Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;"
    .local p2, "outputs":Lorg/apache/lucene/util/fst/Outputs;, "Lorg/apache/lucene/util/fst/Outputs<TT;>;"
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 98
    const v6, 0x7fffffff

    const/4 v8, 0x0

    const/4 v10, 0x0

    const/16 v12, 0xf

    move-object v0, p0

    move-object v1, p1

    move v3, v2

    move v5, v4

    move-object v7, p2

    move v9, v2

    move v11, v4

    invoke-direct/range {v0 .. v12}, Lorg/apache/lucene/util/fst/Builder;-><init>(Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;IIZZILorg/apache/lucene/util/fst/Outputs;Lorg/apache/lucene/util/fst/Builder$FreezeTail;ZFZI)V

    .line 99
    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/util/fst/Builder;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lorg/apache/lucene/util/fst/Builder;->NO_OUTPUT:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1(Lorg/apache/lucene/util/fst/Builder;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 452
    invoke-direct {p0, p1}, Lorg/apache/lucene/util/fst/Builder;->validOutput(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$2(Lorg/apache/lucene/util/fst/Builder;)Lorg/apache/lucene/util/fst/FST;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lorg/apache/lucene/util/fst/Builder;->fst:Lorg/apache/lucene/util/fst/FST;

    return-object v0
.end method

.method private compileAllTargets(Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;I)V
    .locals 4
    .param p2, "tailLength"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/Builder$UnCompiledNode",
            "<TT;>;I)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 487
    .local p0, "this":Lorg/apache/lucene/util/fst/Builder;, "Lorg/apache/lucene/util/fst/Builder<TT;>;"
    .local p1, "node":Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;, "Lorg/apache/lucene/util/fst/Builder$UnCompiledNode<TT;>;"
    const/4 v1, 0x0

    .local v1, "arcIdx":I
    :goto_0
    iget v3, p1, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    if-lt v1, v3, :cond_0

    .line 499
    return-void

    .line 488
    :cond_0
    iget-object v3, p1, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->arcs:[Lorg/apache/lucene/util/fst/Builder$Arc;

    aget-object v0, v3, v1

    .line 489
    .local v0, "arc":Lorg/apache/lucene/util/fst/Builder$Arc;, "Lorg/apache/lucene/util/fst/Builder$Arc<TT;>;"
    iget-object v3, v0, Lorg/apache/lucene/util/fst/Builder$Arc;->target:Lorg/apache/lucene/util/fst/Builder$Node;

    invoke-interface {v3}, Lorg/apache/lucene/util/fst/Builder$Node;->isCompiled()Z

    move-result v3

    if-nez v3, :cond_2

    .line 491
    iget-object v2, v0, Lorg/apache/lucene/util/fst/Builder$Arc;->target:Lorg/apache/lucene/util/fst/Builder$Node;

    check-cast v2, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;

    .line 492
    .local v2, "n":Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;, "Lorg/apache/lucene/util/fst/Builder$UnCompiledNode<TT;>;"
    iget v3, v2, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    if-nez v3, :cond_1

    .line 494
    const/4 v3, 0x1

    iput-boolean v3, v2, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->isFinal:Z

    iput-boolean v3, v0, Lorg/apache/lucene/util/fst/Builder$Arc;->isFinal:Z

    .line 496
    :cond_1
    add-int/lit8 v3, p2, -0x1

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/util/fst/Builder;->compileNode(Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;I)Lorg/apache/lucene/util/fst/Builder$CompiledNode;

    move-result-object v3

    iput-object v3, v0, Lorg/apache/lucene/util/fst/Builder$Arc;->target:Lorg/apache/lucene/util/fst/Builder$Node;

    .line 487
    .end local v2    # "n":Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;, "Lorg/apache/lucene/util/fst/Builder$UnCompiledNode<TT;>;"
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private compileNode(Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;I)Lorg/apache/lucene/util/fst/Builder$CompiledNode;
    .locals 6
    .param p2, "tailLength"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/Builder$UnCompiledNode",
            "<TT;>;I)",
            "Lorg/apache/lucene/util/fst/Builder$CompiledNode;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 193
    .local p0, "this":Lorg/apache/lucene/util/fst/Builder;, "Lorg/apache/lucene/util/fst/Builder<TT;>;"
    .local p1, "nodeIn":Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;, "Lorg/apache/lucene/util/fst/Builder$UnCompiledNode<TT;>;"
    iget-object v1, p0, Lorg/apache/lucene/util/fst/Builder;->dedupHash:Lorg/apache/lucene/util/fst/NodeHash;

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lorg/apache/lucene/util/fst/Builder;->doShareNonSingletonNodes:Z

    if-nez v1, :cond_0

    iget v1, p1, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    const/4 v4, 0x1

    if-gt v1, v4, :cond_2

    :cond_0
    iget v1, p0, Lorg/apache/lucene/util/fst/Builder;->shareMaxTailLength:I

    if-gt p2, v1, :cond_2

    .line 194
    iget v1, p1, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    if-nez v1, :cond_1

    .line 195
    iget-object v1, p0, Lorg/apache/lucene/util/fst/Builder;->fst:Lorg/apache/lucene/util/fst/FST;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/util/fst/FST;->addNode(Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;)J

    move-result-wide v2

    .line 202
    .local v2, "node":J
    :goto_0
    sget-boolean v1, Lorg/apache/lucene/util/fst/Builder;->$assertionsDisabled:Z

    if-nez v1, :cond_3

    const-wide/16 v4, -0x2

    cmp-long v1, v2, v4

    if-nez v1, :cond_3

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 197
    .end local v2    # "node":J
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/util/fst/Builder;->dedupHash:Lorg/apache/lucene/util/fst/NodeHash;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/util/fst/NodeHash;->add(Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;)J

    move-result-wide v2

    .line 199
    .restart local v2    # "node":J
    goto :goto_0

    .line 200
    .end local v2    # "node":J
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/util/fst/Builder;->fst:Lorg/apache/lucene/util/fst/FST;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/util/fst/FST;->addNode(Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;)J

    move-result-wide v2

    .restart local v2    # "node":J
    goto :goto_0

    .line 204
    :cond_3
    invoke-virtual {p1}, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->clear()V

    .line 206
    new-instance v0, Lorg/apache/lucene/util/fst/Builder$CompiledNode;

    invoke-direct {v0}, Lorg/apache/lucene/util/fst/Builder$CompiledNode;-><init>()V

    .line 207
    .local v0, "fn":Lorg/apache/lucene/util/fst/Builder$CompiledNode;, "Lorg/apache/lucene/util/fst/Builder$CompiledNode;"
    iput-wide v2, v0, Lorg/apache/lucene/util/fst/Builder$CompiledNode;->node:J

    .line 208
    return-object v0
.end method

.method private freezeTail(I)V
    .locals 14
    .param p1, "prefixLenPlus1"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 212
    .local p0, "this":Lorg/apache/lucene/util/fst/Builder;, "Lorg/apache/lucene/util/fst/Builder<TT;>;"
    iget-object v10, p0, Lorg/apache/lucene/util/fst/Builder;->freezeTail:Lorg/apache/lucene/util/fst/Builder$FreezeTail;

    if-eqz v10, :cond_1

    .line 214
    iget-object v10, p0, Lorg/apache/lucene/util/fst/Builder;->freezeTail:Lorg/apache/lucene/util/fst/Builder$FreezeTail;

    iget-object v11, p0, Lorg/apache/lucene/util/fst/Builder;->frontier:[Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;

    iget-object v12, p0, Lorg/apache/lucene/util/fst/Builder;->lastInput:Lorg/apache/lucene/util/IntsRef;

    invoke-virtual {v10, v11, p1, v12}, Lorg/apache/lucene/util/fst/Builder$FreezeTail;->freeze([Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;ILorg/apache/lucene/util/IntsRef;)V

    .line 309
    :cond_0
    return-void

    .line 217
    :cond_1
    const/4 v10, 0x1

    invoke-static {v10, p1}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 218
    .local v3, "downTo":I
    iget-object v10, p0, Lorg/apache/lucene/util/fst/Builder;->lastInput:Lorg/apache/lucene/util/IntsRef;

    iget v4, v10, Lorg/apache/lucene/util/IntsRef;->length:I

    .local v4, "idx":I
    :goto_0
    if-lt v4, v3, :cond_0

    .line 220
    const/4 v2, 0x0

    .line 221
    .local v2, "doPrune":Z
    const/4 v1, 0x0

    .line 223
    .local v1, "doCompile":Z
    iget-object v10, p0, Lorg/apache/lucene/util/fst/Builder;->frontier:[Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;

    aget-object v7, v10, v4

    .line 224
    .local v7, "node":Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;, "Lorg/apache/lucene/util/fst/Builder$UnCompiledNode<TT;>;"
    iget-object v10, p0, Lorg/apache/lucene/util/fst/Builder;->frontier:[Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;

    add-int/lit8 v11, v4, -0x1

    aget-object v8, v10, v11

    .line 226
    .local v8, "parent":Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;, "Lorg/apache/lucene/util/fst/Builder$UnCompiledNode<TT;>;"
    iget-wide v10, v7, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->inputCount:J

    iget v12, p0, Lorg/apache/lucene/util/fst/Builder;->minSuffixCount1:I

    int-to-long v12, v12

    cmp-long v10, v10, v12

    if-gez v10, :cond_4

    .line 227
    const/4 v2, 0x1

    .line 228
    const/4 v1, 0x1

    .line 257
    :goto_1
    iget-wide v10, v7, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->inputCount:J

    iget v12, p0, Lorg/apache/lucene/util/fst/Builder;->minSuffixCount2:I

    int-to-long v12, v12

    cmp-long v10, v10, v12

    if-ltz v10, :cond_2

    iget v10, p0, Lorg/apache/lucene/util/fst/Builder;->minSuffixCount2:I

    const/4 v11, 0x1

    if-ne v10, v11, :cond_3

    iget-wide v10, v7, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->inputCount:J

    const-wide/16 v12, 0x1

    cmp-long v10, v10, v12

    if-nez v10, :cond_3

    const/4 v10, 0x1

    if-le v4, v10, :cond_3

    .line 259
    :cond_2
    const/4 v0, 0x0

    .local v0, "arcIdx":I
    :goto_2
    iget v10, v7, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    if-lt v0, v10, :cond_9

    .line 264
    const/4 v10, 0x0

    iput v10, v7, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    .line 267
    .end local v0    # "arcIdx":I
    :cond_3
    if-eqz v2, :cond_a

    .line 269
    invoke-virtual {v7}, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->clear()V

    .line 270
    iget-object v10, p0, Lorg/apache/lucene/util/fst/Builder;->lastInput:Lorg/apache/lucene/util/IntsRef;

    iget-object v10, v10, Lorg/apache/lucene/util/IntsRef;->ints:[I

    iget-object v11, p0, Lorg/apache/lucene/util/fst/Builder;->lastInput:Lorg/apache/lucene/util/IntsRef;

    iget v11, v11, Lorg/apache/lucene/util/IntsRef;->offset:I

    add-int/2addr v11, v4

    add-int/lit8 v11, v11, -0x1

    aget v10, v10, v11

    invoke-virtual {v8, v10, v7}, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->deleteLast(ILorg/apache/lucene/util/fst/Builder$Node;)V

    .line 218
    :goto_3
    add-int/lit8 v4, v4, -0x1

    goto :goto_0

    .line 229
    :cond_4
    if-le v4, p1, :cond_7

    .line 231
    iget-wide v10, v8, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->inputCount:J

    iget v12, p0, Lorg/apache/lucene/util/fst/Builder;->minSuffixCount2:I

    int-to-long v12, v12

    cmp-long v10, v10, v12

    if-ltz v10, :cond_5

    iget v10, p0, Lorg/apache/lucene/util/fst/Builder;->minSuffixCount2:I

    const/4 v11, 0x1

    if-ne v10, v11, :cond_6

    iget-wide v10, v8, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->inputCount:J

    const-wide/16 v12, 0x1

    cmp-long v10, v10, v12

    if-nez v10, :cond_6

    const/4 v10, 0x1

    if-le v4, v10, :cond_6

    .line 242
    :cond_5
    const/4 v2, 0x1

    .line 248
    :goto_4
    const/4 v1, 0x1

    .line 249
    goto :goto_1

    .line 246
    :cond_6
    const/4 v2, 0x0

    goto :goto_4

    .line 252
    :cond_7
    iget v10, p0, Lorg/apache/lucene/util/fst/Builder;->minSuffixCount2:I

    if-nez v10, :cond_8

    const/4 v1, 0x1

    :goto_5
    goto :goto_1

    :cond_8
    const/4 v1, 0x0

    goto :goto_5

    .line 261
    .restart local v0    # "arcIdx":I
    :cond_9
    iget-object v10, v7, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->arcs:[Lorg/apache/lucene/util/fst/Builder$Arc;

    aget-object v10, v10, v0

    iget-object v9, v10, Lorg/apache/lucene/util/fst/Builder$Arc;->target:Lorg/apache/lucene/util/fst/Builder$Node;

    check-cast v9, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;

    .line 262
    .local v9, "target":Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;, "Lorg/apache/lucene/util/fst/Builder$UnCompiledNode<TT;>;"
    invoke-virtual {v9}, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->clear()V

    .line 259
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 273
    .end local v0    # "arcIdx":I
    .end local v9    # "target":Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;, "Lorg/apache/lucene/util/fst/Builder$UnCompiledNode<TT;>;"
    :cond_a
    iget v10, p0, Lorg/apache/lucene/util/fst/Builder;->minSuffixCount2:I

    if-eqz v10, :cond_b

    .line 274
    iget-object v10, p0, Lorg/apache/lucene/util/fst/Builder;->lastInput:Lorg/apache/lucene/util/IntsRef;

    iget v10, v10, Lorg/apache/lucene/util/IntsRef;->length:I

    sub-int/2addr v10, v4

    invoke-direct {p0, v7, v10}, Lorg/apache/lucene/util/fst/Builder;->compileAllTargets(Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;I)V

    .line 276
    :cond_b
    iget-object v6, v7, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->output:Ljava/lang/Object;

    .line 283
    .local v6, "nextFinalOutput":Ljava/lang/Object;, "TT;"
    iget-boolean v10, v7, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->isFinal:Z

    if-nez v10, :cond_c

    iget v10, v7, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    if-eqz v10, :cond_c

    const/4 v5, 0x0

    .line 285
    .local v5, "isFinal":Z
    :goto_6
    if-eqz v1, :cond_d

    .line 289
    iget-object v10, p0, Lorg/apache/lucene/util/fst/Builder;->lastInput:Lorg/apache/lucene/util/IntsRef;

    iget-object v10, v10, Lorg/apache/lucene/util/IntsRef;->ints:[I

    iget-object v11, p0, Lorg/apache/lucene/util/fst/Builder;->lastInput:Lorg/apache/lucene/util/IntsRef;

    iget v11, v11, Lorg/apache/lucene/util/IntsRef;->offset:I

    add-int/2addr v11, v4

    add-int/lit8 v11, v11, -0x1

    aget v10, v10, v11

    .line 290
    iget-object v11, p0, Lorg/apache/lucene/util/fst/Builder;->lastInput:Lorg/apache/lucene/util/IntsRef;

    iget v11, v11, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/lit8 v11, v11, 0x1

    sub-int/2addr v11, v4

    invoke-direct {p0, v7, v11}, Lorg/apache/lucene/util/fst/Builder;->compileNode(Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;I)Lorg/apache/lucene/util/fst/Builder$CompiledNode;

    move-result-object v11

    .line 289
    invoke-virtual {v8, v10, v11, v6, v5}, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->replaceLast(ILorg/apache/lucene/util/fst/Builder$Node;Ljava/lang/Object;Z)V

    goto :goto_3

    .line 283
    .end local v5    # "isFinal":Z
    :cond_c
    const/4 v5, 0x1

    goto :goto_6

    .line 296
    .restart local v5    # "isFinal":Z
    :cond_d
    iget-object v10, p0, Lorg/apache/lucene/util/fst/Builder;->lastInput:Lorg/apache/lucene/util/IntsRef;

    iget-object v10, v10, Lorg/apache/lucene/util/IntsRef;->ints:[I

    iget-object v11, p0, Lorg/apache/lucene/util/fst/Builder;->lastInput:Lorg/apache/lucene/util/IntsRef;

    iget v11, v11, Lorg/apache/lucene/util/IntsRef;->offset:I

    add-int/2addr v11, v4

    add-int/lit8 v11, v11, -0x1

    aget v10, v10, v11

    invoke-virtual {v8, v10, v7, v6, v5}, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->replaceLast(ILorg/apache/lucene/util/fst/Builder$Node;Ljava/lang/Object;Z)V

    .line 304
    iget-object v10, p0, Lorg/apache/lucene/util/fst/Builder;->frontier:[Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;

    new-instance v11, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;

    invoke-direct {v11, p0, v4}, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;-><init>(Lorg/apache/lucene/util/fst/Builder;I)V

    aput-object v11, v10, v4

    goto/16 :goto_3
.end method

.method private validOutput(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .prologue
    .line 453
    .local p0, "this":Lorg/apache/lucene/util/fst/Builder;, "Lorg/apache/lucene/util/fst/Builder<TT;>;"
    .local p1, "output":Ljava/lang/Object;, "TT;"
    iget-object v0, p0, Lorg/apache/lucene/util/fst/Builder;->NO_OUTPUT:Ljava/lang/Object;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/util/fst/Builder;->NO_OUTPUT:Ljava/lang/Object;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public add(Lorg/apache/lucene/util/IntsRef;Ljava/lang/Object;)V
    .locals 20
    .param p1, "input"    # Lorg/apache/lucene/util/IntsRef;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/IntsRef;",
            "TT;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 347
    .local p0, "this":Lorg/apache/lucene/util/fst/Builder;, "Lorg/apache/lucene/util/fst/Builder<TT;>;"
    .local p2, "output":Ljava/lang/Object;, "TT;"
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/util/fst/Builder;->NO_OUTPUT:Ljava/lang/Object;

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_0

    .line 348
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/Builder;->NO_OUTPUT:Ljava/lang/Object;

    move-object/from16 p2, v0

    .line 351
    :cond_0
    sget-boolean v14, Lorg/apache/lucene/util/fst/Builder;->$assertionsDisabled:Z

    if-nez v14, :cond_1

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/util/fst/Builder;->lastInput:Lorg/apache/lucene/util/IntsRef;

    iget v14, v14, Lorg/apache/lucene/util/IntsRef;->length:I

    if-eqz v14, :cond_1

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/util/fst/Builder;->lastInput:Lorg/apache/lucene/util/IntsRef;

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lorg/apache/lucene/util/IntsRef;->compareTo(Lorg/apache/lucene/util/IntsRef;)I

    move-result v14

    if-gez v14, :cond_1

    new-instance v14, Ljava/lang/AssertionError;

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "inputs are added out of order lastInput="

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/Builder;->lastInput:Lorg/apache/lucene/util/IntsRef;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " vs input="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-direct {v14, v15}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v14

    .line 352
    :cond_1
    sget-boolean v14, Lorg/apache/lucene/util/fst/Builder;->$assertionsDisabled:Z

    if-nez v14, :cond_2

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/fst/Builder;->validOutput(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_2

    new-instance v14, Ljava/lang/AssertionError;

    invoke-direct {v14}, Ljava/lang/AssertionError;-><init>()V

    throw v14

    .line 355
    :cond_2
    move-object/from16 v0, p1

    iget v14, v0, Lorg/apache/lucene/util/IntsRef;->length:I

    if-nez v14, :cond_3

    .line 361
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/util/fst/Builder;->frontier:[Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;

    const/4 v15, 0x0

    aget-object v14, v14, v15

    iget-wide v0, v14, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->inputCount:J

    move-wide/from16 v16, v0

    const-wide/16 v18, 0x1

    add-long v16, v16, v18

    move-wide/from16 v0, v16

    iput-wide v0, v14, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->inputCount:J

    .line 362
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/util/fst/Builder;->frontier:[Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;

    const/4 v15, 0x0

    aget-object v14, v14, v15

    const/4 v15, 0x1

    iput-boolean v15, v14, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->isFinal:Z

    .line 363
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/util/fst/Builder;->fst:Lorg/apache/lucene/util/fst/FST;

    move-object/from16 v0, p2

    invoke-virtual {v14, v0}, Lorg/apache/lucene/util/fst/FST;->setEmptyOutput(Ljava/lang/Object;)V

    .line 450
    :goto_0
    return-void

    .line 368
    :cond_3
    const/4 v9, 0x0

    .line 369
    .local v9, "pos1":I
    move-object/from16 v0, p1

    iget v11, v0, Lorg/apache/lucene/util/IntsRef;->offset:I

    .line 370
    .local v11, "pos2":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/util/fst/Builder;->lastInput:Lorg/apache/lucene/util/IntsRef;

    iget v14, v14, Lorg/apache/lucene/util/IntsRef;->length:I

    move-object/from16 v0, p1

    iget v15, v0, Lorg/apache/lucene/util/IntsRef;->length:I

    invoke-static {v14, v15}, Ljava/lang/Math;->min(II)I

    move-result v10

    .line 372
    .local v10, "pos1Stop":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/util/fst/Builder;->frontier:[Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;

    aget-object v14, v14, v9

    iget-wide v0, v14, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->inputCount:J

    move-wide/from16 v16, v0

    const-wide/16 v18, 0x1

    add-long v16, v16, v18

    move-wide/from16 v0, v16

    iput-wide v0, v14, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->inputCount:J

    .line 374
    if-ge v9, v10, :cond_4

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/util/fst/Builder;->lastInput:Lorg/apache/lucene/util/IntsRef;

    iget-object v14, v14, Lorg/apache/lucene/util/IntsRef;->ints:[I

    aget v14, v14, v9

    move-object/from16 v0, p1

    iget-object v15, v0, Lorg/apache/lucene/util/IntsRef;->ints:[I

    aget v15, v15, v11

    if-eq v14, v15, :cond_8

    .line 380
    :cond_4
    add-int/lit8 v12, v9, 0x1

    .line 382
    .local v12, "prefixLenPlus1":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/util/fst/Builder;->frontier:[Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;

    array-length v14, v14

    move-object/from16 v0, p1

    iget v15, v0, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/lit8 v15, v15, 0x1

    if-ge v14, v15, :cond_5

    .line 384
    move-object/from16 v0, p1

    iget v14, v0, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/lit8 v14, v14, 0x1

    sget v15, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_REF:I

    invoke-static {v14, v15}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v14

    new-array v6, v14, [Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;

    .line 385
    .local v6, "next":[Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/util/fst/Builder;->frontier:[Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/Builder;->frontier:[Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v17, v0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-static {v14, v15, v6, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 386
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/util/fst/Builder;->frontier:[Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;

    array-length v3, v14

    .local v3, "idx":I
    :goto_2
    array-length v14, v6

    if-lt v3, v14, :cond_9

    .line 389
    move-object/from16 v0, p0

    iput-object v6, v0, Lorg/apache/lucene/util/fst/Builder;->frontier:[Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;

    .line 394
    .end local v3    # "idx":I
    .end local v6    # "next":[Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;
    :cond_5
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lorg/apache/lucene/util/fst/Builder;->freezeTail(I)V

    .line 397
    move v3, v12

    .restart local v3    # "idx":I
    :goto_3
    move-object/from16 v0, p1

    iget v14, v0, Lorg/apache/lucene/util/IntsRef;->length:I

    if-le v3, v14, :cond_a

    .line 403
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/util/fst/Builder;->frontier:[Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;

    move-object/from16 v0, p1

    iget v15, v0, Lorg/apache/lucene/util/IntsRef;->length:I

    aget-object v4, v14, v15

    .line 404
    .local v4, "lastNode":Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;, "Lorg/apache/lucene/util/fst/Builder$UnCompiledNode<TT;>;"
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/util/fst/Builder;->lastInput:Lorg/apache/lucene/util/IntsRef;

    iget v14, v14, Lorg/apache/lucene/util/IntsRef;->length:I

    move-object/from16 v0, p1

    iget v15, v0, Lorg/apache/lucene/util/IntsRef;->length:I

    if-ne v14, v15, :cond_6

    move-object/from16 v0, p1

    iget v14, v0, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/lit8 v14, v14, 0x1

    if-eq v12, v14, :cond_7

    .line 405
    :cond_6
    const/4 v14, 0x1

    iput-boolean v14, v4, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->isFinal:Z

    .line 406
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/util/fst/Builder;->NO_OUTPUT:Ljava/lang/Object;

    iput-object v14, v4, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->output:Ljava/lang/Object;

    .line 411
    :cond_7
    const/4 v3, 0x1

    :goto_4
    if-lt v3, v12, :cond_b

    .line 436
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/util/fst/Builder;->lastInput:Lorg/apache/lucene/util/IntsRef;

    iget v14, v14, Lorg/apache/lucene/util/IntsRef;->length:I

    move-object/from16 v0, p1

    iget v15, v0, Lorg/apache/lucene/util/IntsRef;->length:I

    if-ne v14, v15, :cond_11

    move-object/from16 v0, p1

    iget v14, v0, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/lit8 v14, v14, 0x1

    if-ne v12, v14, :cond_11

    .line 439
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/util/fst/Builder;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v14, v14, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    iget-object v15, v4, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->output:Ljava/lang/Object;

    move-object/from16 v0, p2

    invoke-virtual {v14, v15, v0}, Lorg/apache/lucene/util/fst/Outputs;->merge(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    iput-object v14, v4, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->output:Ljava/lang/Object;

    .line 447
    :goto_5
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/util/fst/Builder;->lastInput:Lorg/apache/lucene/util/IntsRef;

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Lorg/apache/lucene/util/IntsRef;->copyInts(Lorg/apache/lucene/util/IntsRef;)V

    goto/16 :goto_0

    .line 377
    .end local v3    # "idx":I
    .end local v4    # "lastNode":Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;, "Lorg/apache/lucene/util/fst/Builder$UnCompiledNode<TT;>;"
    .end local v12    # "prefixLenPlus1":I
    :cond_8
    add-int/lit8 v9, v9, 0x1

    .line 378
    add-int/lit8 v11, v11, 0x1

    .line 371
    goto/16 :goto_1

    .line 387
    .restart local v3    # "idx":I
    .restart local v6    # "next":[Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;
    .restart local v12    # "prefixLenPlus1":I
    :cond_9
    new-instance v14, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;

    move-object/from16 v0, p0

    invoke-direct {v14, v0, v3}, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;-><init>(Lorg/apache/lucene/util/fst/Builder;I)V

    aput-object v14, v6, v3

    .line 386
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 398
    .end local v6    # "next":[Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;
    :cond_a
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/util/fst/Builder;->frontier:[Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;

    add-int/lit8 v15, v3, -0x1

    aget-object v14, v14, v15

    move-object/from16 v0, p1

    iget-object v15, v0, Lorg/apache/lucene/util/IntsRef;->ints:[I

    move-object/from16 v0, p1

    iget v0, v0, Lorg/apache/lucene/util/IntsRef;->offset:I

    move/from16 v16, v0

    add-int v16, v16, v3

    add-int/lit8 v16, v16, -0x1

    aget v15, v15, v16

    .line 399
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/Builder;->frontier:[Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;

    move-object/from16 v16, v0

    aget-object v16, v16, v3

    .line 398
    invoke-virtual/range {v14 .. v16}, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->addArc(ILorg/apache/lucene/util/fst/Builder$Node;)V

    .line 400
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/util/fst/Builder;->frontier:[Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;

    aget-object v14, v14, v3

    iget-wide v0, v14, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->inputCount:J

    move-wide/from16 v16, v0

    const-wide/16 v18, 0x1

    add-long v16, v16, v18

    move-wide/from16 v0, v16

    iput-wide v0, v14, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->inputCount:J

    .line 397
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    .line 412
    .restart local v4    # "lastNode":Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;, "Lorg/apache/lucene/util/fst/Builder$UnCompiledNode<TT;>;"
    :cond_b
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/util/fst/Builder;->frontier:[Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;

    aget-object v7, v14, v3

    .line 413
    .local v7, "node":Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;, "Lorg/apache/lucene/util/fst/Builder$UnCompiledNode<TT;>;"
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/util/fst/Builder;->frontier:[Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;

    add-int/lit8 v15, v3, -0x1

    aget-object v8, v14, v15

    .line 415
    .local v8, "parentNode":Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;, "Lorg/apache/lucene/util/fst/Builder$UnCompiledNode<TT;>;"
    move-object/from16 v0, p1

    iget-object v14, v0, Lorg/apache/lucene/util/IntsRef;->ints:[I

    move-object/from16 v0, p1

    iget v15, v0, Lorg/apache/lucene/util/IntsRef;->offset:I

    add-int/2addr v15, v3

    add-int/lit8 v15, v15, -0x1

    aget v14, v14, v15

    invoke-virtual {v8, v14}, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->getLastOutput(I)Ljava/lang/Object;

    move-result-object v5

    .line 416
    .local v5, "lastOutput":Ljava/lang/Object;, "TT;"
    sget-boolean v14, Lorg/apache/lucene/util/fst/Builder;->$assertionsDisabled:Z

    if-nez v14, :cond_c

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lorg/apache/lucene/util/fst/Builder;->validOutput(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_c

    new-instance v14, Ljava/lang/AssertionError;

    invoke-direct {v14}, Ljava/lang/AssertionError;-><init>()V

    throw v14

    .line 421
    :cond_c
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/util/fst/Builder;->NO_OUTPUT:Ljava/lang/Object;

    if-eq v5, v14, :cond_f

    .line 422
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/util/fst/Builder;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v14, v14, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    move-object/from16 v0, p2

    invoke-virtual {v14, v0, v5}, Lorg/apache/lucene/util/fst/Outputs;->common(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 423
    .local v2, "commonOutputPrefix":Ljava/lang/Object;, "TT;"
    sget-boolean v14, Lorg/apache/lucene/util/fst/Builder;->$assertionsDisabled:Z

    if-nez v14, :cond_d

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lorg/apache/lucene/util/fst/Builder;->validOutput(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_d

    new-instance v14, Ljava/lang/AssertionError;

    invoke-direct {v14}, Ljava/lang/AssertionError;-><init>()V

    throw v14

    .line 424
    :cond_d
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/util/fst/Builder;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v14, v14, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    invoke-virtual {v14, v5, v2}, Lorg/apache/lucene/util/fst/Outputs;->subtract(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    .line 425
    .local v13, "wordSuffix":Ljava/lang/Object;, "TT;"
    sget-boolean v14, Lorg/apache/lucene/util/fst/Builder;->$assertionsDisabled:Z

    if-nez v14, :cond_e

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lorg/apache/lucene/util/fst/Builder;->validOutput(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_e

    new-instance v14, Ljava/lang/AssertionError;

    invoke-direct {v14}, Ljava/lang/AssertionError;-><init>()V

    throw v14

    .line 426
    :cond_e
    move-object/from16 v0, p1

    iget-object v14, v0, Lorg/apache/lucene/util/IntsRef;->ints:[I

    move-object/from16 v0, p1

    iget v15, v0, Lorg/apache/lucene/util/IntsRef;->offset:I

    add-int/2addr v15, v3

    add-int/lit8 v15, v15, -0x1

    aget v14, v14, v15

    invoke-virtual {v8, v14, v2}, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->setLastOutput(ILjava/lang/Object;)V

    .line 427
    invoke-virtual {v7, v13}, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->prependOutput(Ljava/lang/Object;)V

    .line 432
    :goto_6
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/util/fst/Builder;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v14, v14, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    move-object/from16 v0, p2

    invoke-virtual {v14, v0, v2}, Lorg/apache/lucene/util/fst/Outputs;->subtract(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    .line 433
    sget-boolean v14, Lorg/apache/lucene/util/fst/Builder;->$assertionsDisabled:Z

    if-nez v14, :cond_10

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/fst/Builder;->validOutput(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_10

    new-instance v14, Ljava/lang/AssertionError;

    invoke-direct {v14}, Ljava/lang/AssertionError;-><init>()V

    throw v14

    .line 429
    .end local v2    # "commonOutputPrefix":Ljava/lang/Object;, "TT;"
    .end local v13    # "wordSuffix":Ljava/lang/Object;, "TT;"
    :cond_f
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/util/fst/Builder;->NO_OUTPUT:Ljava/lang/Object;

    .restart local v13    # "wordSuffix":Ljava/lang/Object;, "TT;"
    move-object v2, v13

    .restart local v2    # "commonOutputPrefix":Ljava/lang/Object;, "TT;"
    goto :goto_6

    .line 411
    :cond_10
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_4

    .line 443
    .end local v2    # "commonOutputPrefix":Ljava/lang/Object;, "TT;"
    .end local v5    # "lastOutput":Ljava/lang/Object;, "TT;"
    .end local v7    # "node":Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;, "Lorg/apache/lucene/util/fst/Builder$UnCompiledNode<TT;>;"
    .end local v8    # "parentNode":Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;, "Lorg/apache/lucene/util/fst/Builder$UnCompiledNode<TT;>;"
    .end local v13    # "wordSuffix":Ljava/lang/Object;, "TT;"
    :cond_11
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/util/fst/Builder;->frontier:[Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;

    add-int/lit8 v15, v12, -0x1

    aget-object v14, v14, v15

    move-object/from16 v0, p1

    iget-object v15, v0, Lorg/apache/lucene/util/IntsRef;->ints:[I

    move-object/from16 v0, p1

    iget v0, v0, Lorg/apache/lucene/util/IntsRef;->offset:I

    move/from16 v16, v0

    add-int v16, v16, v12

    add-int/lit8 v16, v16, -0x1

    aget v15, v15, v16

    move-object/from16 v0, p2

    invoke-virtual {v14, v15, v0}, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->setLastOutput(ILjava/lang/Object;)V

    goto/16 :goto_5
.end method

.method public finish()Lorg/apache/lucene/util/fst/FST;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/apache/lucene/util/fst/FST",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/util/fst/Builder;, "Lorg/apache/lucene/util/fst/Builder<TT;>;"
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 460
    iget-object v2, p0, Lorg/apache/lucene/util/fst/Builder;->frontier:[Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;

    aget-object v0, v2, v3

    .line 463
    .local v0, "root":Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;, "Lorg/apache/lucene/util/fst/Builder$UnCompiledNode<TT;>;"
    invoke-direct {p0, v3}, Lorg/apache/lucene/util/fst/Builder;->freezeTail(I)V

    .line 464
    iget-wide v2, v0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->inputCount:J

    iget v4, p0, Lorg/apache/lucene/util/fst/Builder;->minSuffixCount1:I

    int-to-long v4, v4

    cmp-long v2, v2, v4

    if-ltz v2, :cond_0

    iget-wide v2, v0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->inputCount:J

    iget v4, p0, Lorg/apache/lucene/util/fst/Builder;->minSuffixCount2:I

    int-to-long v4, v4

    cmp-long v2, v2, v4

    if-ltz v2, :cond_0

    iget v2, v0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    if-nez v2, :cond_4

    .line 465
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/util/fst/Builder;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v2, v2, Lorg/apache/lucene/util/fst/FST;->emptyOutput:Ljava/lang/Object;

    if-nez v2, :cond_2

    .line 482
    :cond_1
    :goto_0
    return-object v1

    .line 467
    :cond_2
    iget v2, p0, Lorg/apache/lucene/util/fst/Builder;->minSuffixCount1:I

    if-gtz v2, :cond_1

    iget v2, p0, Lorg/apache/lucene/util/fst/Builder;->minSuffixCount2:I

    if-gtz v2, :cond_1

    .line 477
    :cond_3
    :goto_1
    iget-object v1, p0, Lorg/apache/lucene/util/fst/Builder;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v2, p0, Lorg/apache/lucene/util/fst/Builder;->lastInput:Lorg/apache/lucene/util/IntsRef;

    iget v2, v2, Lorg/apache/lucene/util/IntsRef;->length:I

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/util/fst/Builder;->compileNode(Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;I)Lorg/apache/lucene/util/fst/Builder$CompiledNode;

    move-result-object v2

    iget-wide v2, v2, Lorg/apache/lucene/util/fst/Builder$CompiledNode;->node:J

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/util/fst/FST;->finish(J)V

    .line 479
    iget-boolean v1, p0, Lorg/apache/lucene/util/fst/Builder;->doPackFST:Z

    if-eqz v1, :cond_5

    .line 480
    iget-object v1, p0, Lorg/apache/lucene/util/fst/Builder;->fst:Lorg/apache/lucene/util/fst/FST;

    const/4 v2, 0x3

    const/16 v3, 0xa

    iget-object v4, p0, Lorg/apache/lucene/util/fst/Builder;->fst:Lorg/apache/lucene/util/fst/FST;

    invoke-virtual {v4}, Lorg/apache/lucene/util/fst/FST;->getNodeCount()J

    move-result-wide v4

    const-wide/16 v6, 0x4

    div-long/2addr v4, v6

    long-to-int v4, v4

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    iget v4, p0, Lorg/apache/lucene/util/fst/Builder;->acceptableOverheadRatio:F

    invoke-virtual {v1, v2, v3, v4}, Lorg/apache/lucene/util/fst/FST;->pack(IIF)Lorg/apache/lucene/util/fst/FST;

    move-result-object v1

    goto :goto_0

    .line 472
    :cond_4
    iget v1, p0, Lorg/apache/lucene/util/fst/Builder;->minSuffixCount2:I

    if-eqz v1, :cond_3

    .line 473
    iget-object v1, p0, Lorg/apache/lucene/util/fst/Builder;->lastInput:Lorg/apache/lucene/util/IntsRef;

    iget v1, v1, Lorg/apache/lucene/util/IntsRef;->length:I

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/util/fst/Builder;->compileAllTargets(Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;I)V

    goto :goto_1

    .line 482
    :cond_5
    iget-object v1, p0, Lorg/apache/lucene/util/fst/Builder;->fst:Lorg/apache/lucene/util/fst/FST;

    goto :goto_0
.end method

.method public fstSizeInBytes()J
    .locals 2

    .prologue
    .line 519
    .local p0, "this":Lorg/apache/lucene/util/fst/Builder;, "Lorg/apache/lucene/util/fst/Builder<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/util/fst/Builder;->fst:Lorg/apache/lucene/util/fst/FST;

    invoke-virtual {v0}, Lorg/apache/lucene/util/fst/FST;->sizeInBytes()J

    move-result-wide v0

    return-wide v0
.end method

.method public getMappedStateCount()J
    .locals 2

    .prologue
    .line 188
    .local p0, "this":Lorg/apache/lucene/util/fst/Builder;, "Lorg/apache/lucene/util/fst/Builder<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/util/fst/Builder;->dedupHash:Lorg/apache/lucene/util/fst/NodeHash;

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/util/fst/Builder;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-wide v0, v0, Lorg/apache/lucene/util/fst/FST;->nodeCount:J

    goto :goto_0
.end method

.method public getTermCount()J
    .locals 2

    .prologue
    .line 184
    .local p0, "this":Lorg/apache/lucene/util/fst/Builder;, "Lorg/apache/lucene/util/fst/Builder<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/util/fst/Builder;->frontier:[Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    iget-wide v0, v0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->inputCount:J

    return-wide v0
.end method

.method public getTotStateCount()J
    .locals 2

    .prologue
    .line 180
    .local p0, "this":Lorg/apache/lucene/util/fst/Builder;, "Lorg/apache/lucene/util/fst/Builder<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/util/fst/Builder;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-wide v0, v0, Lorg/apache/lucene/util/fst/FST;->nodeCount:J

    return-wide v0
.end method
