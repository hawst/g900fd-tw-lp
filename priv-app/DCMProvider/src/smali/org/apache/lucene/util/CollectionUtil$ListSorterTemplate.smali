.class abstract Lorg/apache/lucene/util/CollectionUtil$ListSorterTemplate;
.super Lorg/apache/lucene/util/SorterTemplate;
.source "CollectionUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/CollectionUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "ListSorterTemplate"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lorg/apache/lucene/util/SorterTemplate;"
    }
.end annotation


# instance fields
.field protected final list:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation
.end field

.field private pivot:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 45
    .local p0, "this":Lorg/apache/lucene/util/CollectionUtil$ListSorterTemplate;, "Lorg/apache/lucene/util/CollectionUtil$ListSorterTemplate<TT;>;"
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<TT;>;"
    invoke-direct {p0}, Lorg/apache/lucene/util/SorterTemplate;-><init>()V

    .line 46
    iput-object p1, p0, Lorg/apache/lucene/util/CollectionUtil$ListSorterTemplate;->list:Ljava/util/List;

    .line 47
    return-void
.end method


# virtual methods
.method protected compare(II)I
    .locals 2
    .param p1, "i"    # I
    .param p2, "j"    # I

    .prologue
    .line 58
    .local p0, "this":Lorg/apache/lucene/util/CollectionUtil$ListSorterTemplate;, "Lorg/apache/lucene/util/CollectionUtil$ListSorterTemplate<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/util/CollectionUtil$ListSorterTemplate;->list:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/util/CollectionUtil$ListSorterTemplate;->list:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/util/CollectionUtil$ListSorterTemplate;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method protected abstract compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TT;)I"
        }
    .end annotation
.end method

.method protected comparePivot(I)I
    .locals 2
    .param p1, "j"    # I

    .prologue
    .line 68
    .local p0, "this":Lorg/apache/lucene/util/CollectionUtil$ListSorterTemplate;, "Lorg/apache/lucene/util/CollectionUtil$ListSorterTemplate<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/util/CollectionUtil$ListSorterTemplate;->pivot:Ljava/lang/Object;

    iget-object v1, p0, Lorg/apache/lucene/util/CollectionUtil$ListSorterTemplate;->list:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/util/CollectionUtil$ListSorterTemplate;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method protected setPivot(I)V
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 63
    .local p0, "this":Lorg/apache/lucene/util/CollectionUtil$ListSorterTemplate;, "Lorg/apache/lucene/util/CollectionUtil$ListSorterTemplate<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/util/CollectionUtil$ListSorterTemplate;->list:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/util/CollectionUtil$ListSorterTemplate;->pivot:Ljava/lang/Object;

    .line 64
    return-void
.end method

.method protected swap(II)V
    .locals 1
    .param p1, "i"    # I
    .param p2, "j"    # I

    .prologue
    .line 53
    .local p0, "this":Lorg/apache/lucene/util/CollectionUtil$ListSorterTemplate;, "Lorg/apache/lucene/util/CollectionUtil$ListSorterTemplate<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/util/CollectionUtil$ListSorterTemplate;->list:Ljava/util/List;

    invoke-static {v0, p1, p2}, Ljava/util/Collections;->swap(Ljava/util/List;II)V

    .line 54
    return-void
.end method
