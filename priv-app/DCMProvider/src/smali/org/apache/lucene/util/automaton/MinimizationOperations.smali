.class public final Lorg/apache/lucene/util/automaton/MinimizationOperations;
.super Ljava/lang/Object;
.source "MinimizationOperations.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/automaton/MinimizationOperations$IntPair;,
        Lorg/apache/lucene/util/automaton/MinimizationOperations$StateList;,
        Lorg/apache/lucene/util/automaton/MinimizationOperations$StateListNode;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static minimize(Lorg/apache/lucene/util/automaton/Automaton;)V
    .locals 1
    .param p0, "a"    # Lorg/apache/lucene/util/automaton/Automaton;

    .prologue
    .line 53
    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/Automaton;->isSingleton()Z

    move-result v0

    if-nez v0, :cond_0

    .line 54
    invoke-static {p0}, Lorg/apache/lucene/util/automaton/MinimizationOperations;->minimizeHopcroft(Lorg/apache/lucene/util/automaton/Automaton;)V

    .line 59
    :cond_0
    return-void
.end method

.method public static minimizeHopcroft(Lorg/apache/lucene/util/automaton/Automaton;)V
    .locals 45
    .param p0, "a"    # Lorg/apache/lucene/util/automaton/Automaton;

    .prologue
    .line 65
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/util/automaton/Automaton;->determinize()V

    .line 66
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Lorg/apache/lucene/util/automaton/State;->numTransitions:I

    move/from16 v40, v0

    const/16 v41, 0x1

    move/from16 v0, v40

    move/from16 v1, v41

    if-ne v0, v1, :cond_0

    .line 67
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/State;->transitionsArray:[Lorg/apache/lucene/util/automaton/Transition;

    move-object/from16 v40, v0

    const/16 v41, 0x0

    aget-object v38, v40, v41

    .line 68
    .local v38, "t":Lorg/apache/lucene/util/automaton/Transition;
    move-object/from16 v0, v38

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/Transition;->to:Lorg/apache/lucene/util/automaton/State;

    move-object/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    move-object/from16 v41, v0

    move-object/from16 v0, v40

    move-object/from16 v1, v41

    if-ne v0, v1, :cond_0

    move-object/from16 v0, v38

    iget v0, v0, Lorg/apache/lucene/util/automaton/Transition;->min:I

    move/from16 v40, v0

    if-nez v40, :cond_0

    .line 69
    move-object/from16 v0, v38

    iget v0, v0, Lorg/apache/lucene/util/automaton/Transition;->max:I

    move/from16 v40, v0

    const v41, 0x10ffff

    move/from16 v0, v40

    move/from16 v1, v41

    if-ne v0, v1, :cond_0

    .line 210
    .end local v38    # "t":Lorg/apache/lucene/util/automaton/Transition;
    :goto_0
    return-void

    .line 71
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/util/automaton/Automaton;->totalize()V

    .line 74
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/util/automaton/Automaton;->getStartPoints()[I

    move-result-object v31

    .line 75
    .local v31, "sigma":[I
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/util/automaton/Automaton;->getNumberedStates()[Lorg/apache/lucene/util/automaton/State;

    move-result-object v36

    .line 76
    .local v36, "states":[Lorg/apache/lucene/util/automaton/State;
    move-object/from16 v0, v31

    array-length v0, v0

    move/from16 v32, v0

    .local v32, "sigmaLen":I
    move-object/from16 v0, v36

    array-length v0, v0

    move/from16 v37, v0

    .line 78
    .local v37, "statesLen":I
    move/from16 v0, v37

    move/from16 v1, v32

    filled-new-array {v0, v1}, [I

    move-result-object v40

    const-class v41, Ljava/util/ArrayList;

    move-object/from16 v0, v41

    move-object/from16 v1, v40

    invoke-static {v0, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v28

    check-cast v28, [[Ljava/util/ArrayList;

    .line 80
    .local v28, "reverse":[[Ljava/util/ArrayList;
    move/from16 v0, v37

    new-array v0, v0, [Ljava/util/HashSet;

    move-object/from16 v19, v0

    .line 82
    .local v19, "partition":[Ljava/util/HashSet;
    move/from16 v0, v37

    new-array v0, v0, [Ljava/util/ArrayList;

    move-object/from16 v35, v0

    .line 83
    .local v35, "splitblock":[Ljava/util/ArrayList;
    move/from16 v0, v37

    new-array v8, v0, [I

    .line 84
    .local v8, "block":[I
    move/from16 v0, v37

    move/from16 v1, v32

    filled-new-array {v0, v1}, [I

    move-result-object v40

    const-class v41, Lorg/apache/lucene/util/automaton/MinimizationOperations$StateList;

    move-object/from16 v0, v41

    move-object/from16 v1, v40

    invoke-static {v0, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [[Lorg/apache/lucene/util/automaton/MinimizationOperations$StateList;

    .line 85
    .local v2, "active":[[Lorg/apache/lucene/util/automaton/MinimizationOperations$StateList;
    move/from16 v0, v37

    move/from16 v1, v32

    filled-new-array {v0, v1}, [I

    move-result-object v40

    const-class v41, Lorg/apache/lucene/util/automaton/MinimizationOperations$StateListNode;

    move-object/from16 v0, v41

    move-object/from16 v1, v40

    invoke-static {v0, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [[Lorg/apache/lucene/util/automaton/MinimizationOperations$StateListNode;

    .line 86
    .local v3, "active2":[[Lorg/apache/lucene/util/automaton/MinimizationOperations$StateListNode;
    new-instance v20, Ljava/util/LinkedList;

    invoke-direct/range {v20 .. v20}, Ljava/util/LinkedList;-><init>()V

    .line 87
    .local v20, "pending":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lorg/apache/lucene/util/automaton/MinimizationOperations$IntPair;>;"
    new-instance v21, Ljava/util/BitSet;

    mul-int v40, v32, v37

    move-object/from16 v0, v21

    move/from16 v1, v40

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    .line 88
    .local v21, "pending2":Ljava/util/BitSet;
    new-instance v34, Ljava/util/BitSet;

    move-object/from16 v0, v34

    move/from16 v1, v37

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    .line 89
    .local v34, "split":Ljava/util/BitSet;
    new-instance v26, Ljava/util/BitSet;

    move-object/from16 v0, v26

    move/from16 v1, v37

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    .local v26, "refine":Ljava/util/BitSet;
    new-instance v27, Ljava/util/BitSet;

    move-object/from16 v0, v27

    move/from16 v1, v37

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    .line 90
    .local v27, "refine2":Ljava/util/BitSet;
    const/16 v22, 0x0

    .local v22, "q":I
    :goto_1
    move/from16 v0, v22

    move/from16 v1, v37

    if-lt v0, v1, :cond_1

    .line 98
    const/16 v22, 0x0

    :goto_2
    move/from16 v0, v22

    move/from16 v1, v37

    if-lt v0, v1, :cond_3

    .line 112
    const/4 v12, 0x0

    .local v12, "j":I
    :goto_3
    const/16 v40, 0x1

    move/from16 v0, v40

    if-le v12, v0, :cond_7

    .line 121
    const/16 v39, 0x0

    .local v39, "x":I
    :goto_4
    move/from16 v0, v39

    move/from16 v1, v32

    if-lt v0, v1, :cond_b

    .line 127
    const/4 v13, 0x2

    .line 128
    .local v13, "k":I
    :goto_5
    invoke-virtual/range {v20 .. v20}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v40

    if-eqz v40, :cond_d

    .line 190
    new-array v0, v13, [Lorg/apache/lucene/util/automaton/State;

    move-object/from16 v16, v0

    .line 191
    .local v16, "newstates":[Lorg/apache/lucene/util/automaton/State;
    const/4 v15, 0x0

    .end local v22    # "q":I
    .local v15, "n":I
    :goto_6
    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v40, v0

    move/from16 v0, v40

    if-lt v15, v0, :cond_1a

    .line 202
    const/4 v15, 0x0

    :goto_7
    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v40, v0

    move/from16 v0, v40

    if-lt v15, v0, :cond_1d

    .line 208
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/util/automaton/Automaton;->clearNumberedStates()V

    .line 209
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/util/automaton/Automaton;->removeDeadTransitions()V

    goto/16 :goto_0

    .line 91
    .end local v12    # "j":I
    .end local v13    # "k":I
    .end local v15    # "n":I
    .end local v16    # "newstates":[Lorg/apache/lucene/util/automaton/State;
    .end local v39    # "x":I
    .restart local v22    # "q":I
    :cond_1
    new-instance v40, Ljava/util/ArrayList;

    invoke-direct/range {v40 .. v40}, Ljava/util/ArrayList;-><init>()V

    aput-object v40, v35, v22

    .line 92
    new-instance v40, Ljava/util/HashSet;

    invoke-direct/range {v40 .. v40}, Ljava/util/HashSet;-><init>()V

    aput-object v40, v19, v22

    .line 93
    const/16 v39, 0x0

    .restart local v39    # "x":I
    :goto_8
    move/from16 v0, v39

    move/from16 v1, v32

    if-lt v0, v1, :cond_2

    .line 90
    add-int/lit8 v22, v22, 0x1

    goto :goto_1

    .line 94
    :cond_2
    aget-object v40, v2, v22

    new-instance v41, Lorg/apache/lucene/util/automaton/MinimizationOperations$StateList;

    invoke-direct/range {v41 .. v41}, Lorg/apache/lucene/util/automaton/MinimizationOperations$StateList;-><init>()V

    aput-object v41, v40, v39

    .line 93
    add-int/lit8 v39, v39, 0x1

    goto :goto_8

    .line 99
    .end local v39    # "x":I
    :cond_3
    aget-object v23, v36, v22

    .line 100
    .local v23, "qq":Lorg/apache/lucene/util/automaton/State;
    move-object/from16 v0, v23

    iget-boolean v0, v0, Lorg/apache/lucene/util/automaton/State;->accept:Z

    move/from16 v40, v0

    if-eqz v40, :cond_4

    const/4 v12, 0x0

    .line 101
    .restart local v12    # "j":I
    :goto_9
    aget-object v40, v19, v12

    move-object/from16 v0, v40

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 102
    aput v12, v8, v22

    .line 103
    const/16 v39, 0x0

    .restart local v39    # "x":I
    :goto_a
    move/from16 v0, v39

    move/from16 v1, v32

    if-lt v0, v1, :cond_5

    .line 98
    add-int/lit8 v22, v22, 0x1

    goto/16 :goto_2

    .line 100
    .end local v12    # "j":I
    .end local v39    # "x":I
    :cond_4
    const/4 v12, 0x1

    goto :goto_9

    .line 105
    .restart local v12    # "j":I
    .restart local v39    # "x":I
    :cond_5
    aget v40, v31, v39

    move-object/from16 v0, v23

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/automaton/State;->step(I)Lorg/apache/lucene/util/automaton/State;

    move-result-object v40

    move-object/from16 v0, v40

    iget v0, v0, Lorg/apache/lucene/util/automaton/State;->number:I

    move/from16 v40, v0

    aget-object v24, v28, v40

    .line 106
    .local v24, "r":[Ljava/util/ArrayList;
    aget-object v40, v24, v39

    if-nez v40, :cond_6

    .line 107
    new-instance v40, Ljava/util/ArrayList;

    invoke-direct/range {v40 .. v40}, Ljava/util/ArrayList;-><init>()V

    aput-object v40, v24, v39

    .line 108
    :cond_6
    aget-object v40, v24, v39

    move-object/from16 v0, v40

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 103
    add-int/lit8 v39, v39, 0x1

    goto :goto_a

    .line 113
    .end local v23    # "qq":Lorg/apache/lucene/util/automaton/State;
    .end local v24    # "r":[Ljava/util/ArrayList;
    .end local v39    # "x":I
    :cond_7
    const/16 v39, 0x0

    .restart local v39    # "x":I
    :goto_b
    move/from16 v0, v39

    move/from16 v1, v32

    if-lt v0, v1, :cond_8

    .line 112
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_3

    .line 114
    :cond_8
    aget-object v40, v19, v12

    invoke-virtual/range {v40 .. v40}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :cond_9
    :goto_c
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v41

    if-nez v41, :cond_a

    .line 113
    add-int/lit8 v39, v39, 0x1

    goto :goto_b

    .line 114
    :cond_a
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Lorg/apache/lucene/util/automaton/State;

    .line 115
    .restart local v23    # "qq":Lorg/apache/lucene/util/automaton/State;
    move-object/from16 v0, v23

    iget v0, v0, Lorg/apache/lucene/util/automaton/State;->number:I

    move/from16 v41, v0

    aget-object v41, v28, v41

    aget-object v41, v41, v39

    if-eqz v41, :cond_9

    .line 116
    move-object/from16 v0, v23

    iget v0, v0, Lorg/apache/lucene/util/automaton/State;->number:I

    move/from16 v41, v0

    aget-object v41, v3, v41

    aget-object v42, v2, v12

    aget-object v42, v42, v39

    move-object/from16 v0, v42

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/automaton/MinimizationOperations$StateList;->add(Lorg/apache/lucene/util/automaton/State;)Lorg/apache/lucene/util/automaton/MinimizationOperations$StateListNode;

    move-result-object v42

    aput-object v42, v41, v39

    goto :goto_c

    .line 122
    .end local v23    # "qq":Lorg/apache/lucene/util/automaton/State;
    :cond_b
    const/16 v40, 0x0

    aget-object v40, v2, v40

    aget-object v40, v40, v39

    move-object/from16 v0, v40

    iget v0, v0, Lorg/apache/lucene/util/automaton/MinimizationOperations$StateList;->size:I

    move/from16 v40, v0

    const/16 v41, 0x1

    aget-object v41, v2, v41

    aget-object v41, v41, v39

    move-object/from16 v0, v41

    iget v0, v0, Lorg/apache/lucene/util/automaton/MinimizationOperations$StateList;->size:I

    move/from16 v41, v0

    move/from16 v0, v40

    move/from16 v1, v41

    if-gt v0, v1, :cond_c

    const/4 v12, 0x0

    .line 123
    :goto_d
    new-instance v40, Lorg/apache/lucene/util/automaton/MinimizationOperations$IntPair;

    move-object/from16 v0, v40

    move/from16 v1, v39

    invoke-direct {v0, v12, v1}, Lorg/apache/lucene/util/automaton/MinimizationOperations$IntPair;-><init>(II)V

    move-object/from16 v0, v20

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 124
    mul-int v40, v39, v37

    add-int v40, v40, v12

    move-object/from16 v0, v21

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 121
    add-int/lit8 v39, v39, 0x1

    goto/16 :goto_4

    .line 122
    :cond_c
    const/4 v12, 0x1

    goto :goto_d

    .line 129
    .restart local v13    # "k":I
    :cond_d
    invoke-virtual/range {v20 .. v20}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lorg/apache/lucene/util/automaton/MinimizationOperations$IntPair;

    .line 130
    .local v11, "ip":Lorg/apache/lucene/util/automaton/MinimizationOperations$IntPair;
    iget v0, v11, Lorg/apache/lucene/util/automaton/MinimizationOperations$IntPair;->n1:I

    move/from16 v18, v0

    .line 131
    .local v18, "p":I
    iget v0, v11, Lorg/apache/lucene/util/automaton/MinimizationOperations$IntPair;->n2:I

    move/from16 v39, v0

    .line 132
    mul-int v40, v39, v37

    add-int v40, v40, v18

    move-object/from16 v0, v21

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->clear(I)V

    .line 134
    aget-object v40, v2, v18

    aget-object v40, v40, v39

    move-object/from16 v0, v40

    iget-object v14, v0, Lorg/apache/lucene/util/automaton/MinimizationOperations$StateList;->first:Lorg/apache/lucene/util/automaton/MinimizationOperations$StateListNode;

    .local v14, "m":Lorg/apache/lucene/util/automaton/MinimizationOperations$StateListNode;
    :goto_e
    if-nez v14, :cond_e

    .line 150
    const/16 v40, 0x0

    move-object/from16 v0, v26

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->nextSetBit(I)I

    move-result v12

    :goto_f
    if-gez v12, :cond_12

    .line 187
    invoke-virtual/range {v26 .. v26}, Ljava/util/BitSet;->clear()V

    goto/16 :goto_5

    .line 135
    :cond_e
    iget-object v0, v14, Lorg/apache/lucene/util/automaton/MinimizationOperations$StateListNode;->q:Lorg/apache/lucene/util/automaton/State;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Lorg/apache/lucene/util/automaton/State;->number:I

    move/from16 v40, v0

    aget-object v40, v28, v40

    aget-object v25, v40, v39

    .line 136
    .local v25, "r":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/util/automaton/State;>;"
    if-eqz v25, :cond_10

    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :cond_f
    :goto_10
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v41

    if-nez v41, :cond_11

    .line 134
    :cond_10
    iget-object v14, v14, Lorg/apache/lucene/util/automaton/MinimizationOperations$StateListNode;->next:Lorg/apache/lucene/util/automaton/MinimizationOperations$StateListNode;

    goto :goto_e

    .line 136
    :cond_11
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Lorg/apache/lucene/util/automaton/State;

    .line 137
    .local v29, "s":Lorg/apache/lucene/util/automaton/State;
    move-object/from16 v0, v29

    iget v10, v0, Lorg/apache/lucene/util/automaton/State;->number:I

    .line 138
    .local v10, "i":I
    move-object/from16 v0, v34

    invoke-virtual {v0, v10}, Ljava/util/BitSet;->get(I)Z

    move-result v41

    if-nez v41, :cond_f

    .line 139
    move-object/from16 v0, v34

    invoke-virtual {v0, v10}, Ljava/util/BitSet;->set(I)V

    .line 140
    aget v12, v8, v10

    .line 141
    aget-object v41, v35, v12

    move-object/from16 v0, v41

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 142
    move-object/from16 v0, v27

    invoke-virtual {v0, v12}, Ljava/util/BitSet;->get(I)Z

    move-result v41

    if-nez v41, :cond_f

    .line 143
    move-object/from16 v0, v27

    invoke-virtual {v0, v12}, Ljava/util/BitSet;->set(I)V

    .line 144
    move-object/from16 v0, v26

    invoke-virtual {v0, v12}, Ljava/util/BitSet;->set(I)V

    goto :goto_10

    .line 151
    .end local v10    # "i":I
    .end local v25    # "r":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/util/automaton/State;>;"
    .end local v29    # "s":Lorg/apache/lucene/util/automaton/State;
    :cond_12
    aget-object v30, v35, v12

    .line 152
    .local v30, "sb":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/util/automaton/State;>;"
    invoke-virtual/range {v30 .. v30}, Ljava/util/ArrayList;->size()I

    move-result v40

    aget-object v41, v19, v12

    invoke-virtual/range {v41 .. v41}, Ljava/util/HashSet;->size()I

    move-result v41

    move/from16 v0, v40

    move/from16 v1, v41

    if-ge v0, v1, :cond_14

    .line 153
    aget-object v6, v19, v12

    .line 154
    .local v6, "b1":Ljava/util/HashSet;, "Ljava/util/HashSet<Lorg/apache/lucene/util/automaton/State;>;"
    aget-object v7, v19, v13

    .line 155
    .local v7, "b2":Ljava/util/HashSet;, "Ljava/util/HashSet<Lorg/apache/lucene/util/automaton/State;>;"
    invoke-virtual/range {v30 .. v30}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :cond_13
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v41

    if-nez v41, :cond_15

    .line 168
    const/4 v9, 0x0

    .local v9, "c":I
    :goto_11
    move/from16 v0, v32

    if-lt v9, v0, :cond_17

    .line 180
    add-int/lit8 v13, v13, 0x1

    .line 182
    .end local v6    # "b1":Ljava/util/HashSet;, "Ljava/util/HashSet<Lorg/apache/lucene/util/automaton/State;>;"
    .end local v7    # "b2":Ljava/util/HashSet;, "Ljava/util/HashSet<Lorg/apache/lucene/util/automaton/State;>;"
    .end local v9    # "c":I
    :cond_14
    move-object/from16 v0, v27

    invoke-virtual {v0, v12}, Ljava/util/BitSet;->clear(I)V

    .line 183
    invoke-virtual/range {v30 .. v30}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_12
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v41

    if-nez v41, :cond_19

    .line 185
    invoke-virtual/range {v30 .. v30}, Ljava/util/ArrayList;->clear()V

    .line 150
    add-int/lit8 v40, v12, 0x1

    move-object/from16 v0, v26

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->nextSetBit(I)I

    move-result v12

    goto/16 :goto_f

    .line 155
    .restart local v6    # "b1":Ljava/util/HashSet;, "Ljava/util/HashSet<Lorg/apache/lucene/util/automaton/State;>;"
    .restart local v7    # "b2":Ljava/util/HashSet;, "Ljava/util/HashSet<Lorg/apache/lucene/util/automaton/State;>;"
    :cond_15
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Lorg/apache/lucene/util/automaton/State;

    .line 156
    .restart local v29    # "s":Lorg/apache/lucene/util/automaton/State;
    move-object/from16 v0, v29

    invoke-virtual {v6, v0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 157
    move-object/from16 v0, v29

    invoke-virtual {v7, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 158
    move-object/from16 v0, v29

    iget v0, v0, Lorg/apache/lucene/util/automaton/State;->number:I

    move/from16 v41, v0

    aput v13, v8, v41

    .line 159
    const/4 v9, 0x0

    .restart local v9    # "c":I
    :goto_13
    move/from16 v0, v32

    if-ge v9, v0, :cond_13

    .line 160
    move-object/from16 v0, v29

    iget v0, v0, Lorg/apache/lucene/util/automaton/State;->number:I

    move/from16 v41, v0

    aget-object v41, v3, v41

    aget-object v33, v41, v9

    .line 161
    .local v33, "sn":Lorg/apache/lucene/util/automaton/MinimizationOperations$StateListNode;
    if-eqz v33, :cond_16

    move-object/from16 v0, v33

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/MinimizationOperations$StateListNode;->sl:Lorg/apache/lucene/util/automaton/MinimizationOperations$StateList;

    move-object/from16 v41, v0

    aget-object v42, v2, v12

    aget-object v42, v42, v9

    move-object/from16 v0, v41

    move-object/from16 v1, v42

    if-ne v0, v1, :cond_16

    .line 162
    invoke-virtual/range {v33 .. v33}, Lorg/apache/lucene/util/automaton/MinimizationOperations$StateListNode;->remove()V

    .line 163
    move-object/from16 v0, v29

    iget v0, v0, Lorg/apache/lucene/util/automaton/State;->number:I

    move/from16 v41, v0

    aget-object v41, v3, v41

    aget-object v42, v2, v13

    aget-object v42, v42, v9

    move-object/from16 v0, v42

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/automaton/MinimizationOperations$StateList;->add(Lorg/apache/lucene/util/automaton/State;)Lorg/apache/lucene/util/automaton/MinimizationOperations$StateListNode;

    move-result-object v42

    aput-object v42, v41, v9

    .line 159
    :cond_16
    add-int/lit8 v9, v9, 0x1

    goto :goto_13

    .line 169
    .end local v29    # "s":Lorg/apache/lucene/util/automaton/State;
    .end local v33    # "sn":Lorg/apache/lucene/util/automaton/MinimizationOperations$StateListNode;
    :cond_17
    aget-object v40, v2, v12

    aget-object v40, v40, v9

    move-object/from16 v0, v40

    iget v4, v0, Lorg/apache/lucene/util/automaton/MinimizationOperations$StateList;->size:I

    .line 170
    .local v4, "aj":I
    aget-object v40, v2, v13

    aget-object v40, v40, v9

    move-object/from16 v0, v40

    iget v5, v0, Lorg/apache/lucene/util/automaton/MinimizationOperations$StateList;->size:I

    .line 171
    .local v5, "ak":I
    mul-int v17, v9, v37

    .line 172
    .local v17, "ofs":I
    add-int v40, v17, v12

    move-object/from16 v0, v21

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v40

    if-nez v40, :cond_18

    if-lez v4, :cond_18

    if-gt v4, v5, :cond_18

    .line 173
    add-int v40, v17, v12

    move-object/from16 v0, v21

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 174
    new-instance v40, Lorg/apache/lucene/util/automaton/MinimizationOperations$IntPair;

    move-object/from16 v0, v40

    invoke-direct {v0, v12, v9}, Lorg/apache/lucene/util/automaton/MinimizationOperations$IntPair;-><init>(II)V

    move-object/from16 v0, v20

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 168
    :goto_14
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_11

    .line 176
    :cond_18
    add-int v40, v17, v13

    move-object/from16 v0, v21

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 177
    new-instance v40, Lorg/apache/lucene/util/automaton/MinimizationOperations$IntPair;

    move-object/from16 v0, v40

    invoke-direct {v0, v13, v9}, Lorg/apache/lucene/util/automaton/MinimizationOperations$IntPair;-><init>(II)V

    move-object/from16 v0, v20

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_14

    .line 183
    .end local v4    # "aj":I
    .end local v5    # "ak":I
    .end local v6    # "b1":Ljava/util/HashSet;, "Ljava/util/HashSet<Lorg/apache/lucene/util/automaton/State;>;"
    .end local v7    # "b2":Ljava/util/HashSet;, "Ljava/util/HashSet<Lorg/apache/lucene/util/automaton/State;>;"
    .end local v9    # "c":I
    .end local v17    # "ofs":I
    :cond_19
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Lorg/apache/lucene/util/automaton/State;

    .line 184
    .restart local v29    # "s":Lorg/apache/lucene/util/automaton/State;
    move-object/from16 v0, v29

    iget v0, v0, Lorg/apache/lucene/util/automaton/State;->number:I

    move/from16 v41, v0

    move-object/from16 v0, v34

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->clear(I)V

    goto/16 :goto_12

    .line 192
    .end local v11    # "ip":Lorg/apache/lucene/util/automaton/MinimizationOperations$IntPair;
    .end local v14    # "m":Lorg/apache/lucene/util/automaton/MinimizationOperations$StateListNode;
    .end local v18    # "p":I
    .end local v22    # "q":I
    .end local v29    # "s":Lorg/apache/lucene/util/automaton/State;
    .end local v30    # "sb":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/util/automaton/State;>;"
    .restart local v15    # "n":I
    .restart local v16    # "newstates":[Lorg/apache/lucene/util/automaton/State;
    :cond_1a
    new-instance v29, Lorg/apache/lucene/util/automaton/State;

    invoke-direct/range {v29 .. v29}, Lorg/apache/lucene/util/automaton/State;-><init>()V

    .line 193
    .restart local v29    # "s":Lorg/apache/lucene/util/automaton/State;
    aput-object v29, v16, v15

    .line 194
    aget-object v40, v19, v15

    invoke-virtual/range {v40 .. v40}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_15
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v41

    if-nez v41, :cond_1b

    .line 191
    add-int/lit8 v15, v15, 0x1

    goto/16 :goto_6

    .line 194
    :cond_1b
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lorg/apache/lucene/util/automaton/State;

    .line 195
    .local v22, "q":Lorg/apache/lucene/util/automaton/State;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    move-object/from16 v41, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v41

    if-ne v0, v1, :cond_1c

    move-object/from16 v0, v29

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/util/automaton/Automaton;->initial:Lorg/apache/lucene/util/automaton/State;

    .line 196
    :cond_1c
    move-object/from16 v0, v22

    iget-boolean v0, v0, Lorg/apache/lucene/util/automaton/State;->accept:Z

    move/from16 v41, v0

    move/from16 v0, v41

    move-object/from16 v1, v29

    iput-boolean v0, v1, Lorg/apache/lucene/util/automaton/State;->accept:Z

    .line 197
    move-object/from16 v0, v22

    iget v0, v0, Lorg/apache/lucene/util/automaton/State;->number:I

    move/from16 v41, v0

    move/from16 v0, v41

    move-object/from16 v1, v29

    iput v0, v1, Lorg/apache/lucene/util/automaton/State;->number:I

    .line 198
    move-object/from16 v0, v22

    iput v15, v0, Lorg/apache/lucene/util/automaton/State;->number:I

    goto :goto_15

    .line 203
    .end local v22    # "q":Lorg/apache/lucene/util/automaton/State;
    .end local v29    # "s":Lorg/apache/lucene/util/automaton/State;
    :cond_1d
    aget-object v29, v16, v15

    .line 204
    .restart local v29    # "s":Lorg/apache/lucene/util/automaton/State;
    move-object/from16 v0, v29

    iget v0, v0, Lorg/apache/lucene/util/automaton/State;->number:I

    move/from16 v40, v0

    aget-object v40, v36, v40

    move-object/from16 v0, v40

    iget-boolean v0, v0, Lorg/apache/lucene/util/automaton/State;->accept:Z

    move/from16 v40, v0

    move/from16 v0, v40

    move-object/from16 v1, v29

    iput-boolean v0, v1, Lorg/apache/lucene/util/automaton/State;->accept:Z

    .line 205
    move-object/from16 v0, v29

    iget v0, v0, Lorg/apache/lucene/util/automaton/State;->number:I

    move/from16 v40, v0

    aget-object v40, v36, v40

    invoke-virtual/range {v40 .. v40}, Lorg/apache/lucene/util/automaton/State;->getTransitions()Ljava/lang/Iterable;

    move-result-object v40

    invoke-interface/range {v40 .. v40}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_16
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v41

    if-nez v41, :cond_1e

    .line 202
    add-int/lit8 v15, v15, 0x1

    goto/16 :goto_7

    .line 205
    :cond_1e
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v38

    check-cast v38, Lorg/apache/lucene/util/automaton/Transition;

    .line 206
    .restart local v38    # "t":Lorg/apache/lucene/util/automaton/Transition;
    new-instance v41, Lorg/apache/lucene/util/automaton/Transition;

    move-object/from16 v0, v38

    iget v0, v0, Lorg/apache/lucene/util/automaton/Transition;->min:I

    move/from16 v42, v0

    move-object/from16 v0, v38

    iget v0, v0, Lorg/apache/lucene/util/automaton/Transition;->max:I

    move/from16 v43, v0

    move-object/from16 v0, v38

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/Transition;->to:Lorg/apache/lucene/util/automaton/State;

    move-object/from16 v44, v0

    move-object/from16 v0, v44

    iget v0, v0, Lorg/apache/lucene/util/automaton/State;->number:I

    move/from16 v44, v0

    aget-object v44, v16, v44

    invoke-direct/range {v41 .. v44}, Lorg/apache/lucene/util/automaton/Transition;-><init>(IILorg/apache/lucene/util/automaton/State;)V

    move-object/from16 v0, v29

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/automaton/State;->addTransition(Lorg/apache/lucene/util/automaton/Transition;)V

    goto :goto_16
.end method
