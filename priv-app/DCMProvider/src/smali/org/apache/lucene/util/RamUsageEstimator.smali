.class public final Lorg/apache/lucene/util/RamUsageEstimator;
.super Ljava/lang/Object;
.source "RamUsageEstimator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/RamUsageEstimator$ClassCache;,
        Lorg/apache/lucene/util/RamUsageEstimator$DummyOneFieldObject;,
        Lorg/apache/lucene/util/RamUsageEstimator$DummyTwoLongObject;,
        Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;,
        Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;
    }
.end annotation


# static fields
.field public static final JVM_INFO_STRING:Ljava/lang/String;

.field public static final NUM_BYTES_ARRAY_HEADER:I

.field public static final NUM_BYTES_BOOLEAN:I = 0x1

.field public static final NUM_BYTES_BYTE:I = 0x1

.field public static final NUM_BYTES_CHAR:I = 0x2

.field public static final NUM_BYTES_DOUBLE:I = 0x8

.field public static final NUM_BYTES_FLOAT:I = 0x4

.field public static final NUM_BYTES_INT:I = 0x4

.field public static final NUM_BYTES_LONG:I = 0x8

.field public static final NUM_BYTES_OBJECT_ALIGNMENT:I

.field public static final NUM_BYTES_OBJECT_HEADER:I

.field public static final NUM_BYTES_OBJECT_REF:I

.field public static final NUM_BYTES_SHORT:I = 0x2

.field public static final ONE_GB:J = 0x40000000L

.field public static final ONE_KB:J = 0x400L

.field public static final ONE_MB:J = 0x100000L

.field private static final objectFieldOffsetMethod:Ljava/lang/reflect/Method;

.field private static final primitiveSizes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final supportedFeatures:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;",
            ">;"
        }
    .end annotation
.end field

.field private static final theUnsafe:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 23

    .prologue
    .line 111
    new-instance v19, Ljava/util/IdentityHashMap;

    invoke-direct/range {v19 .. v19}, Ljava/util/IdentityHashMap;-><init>()V

    sput-object v19, Lorg/apache/lucene/util/RamUsageEstimator;->primitiveSizes:Ljava/util/Map;

    .line 112
    sget-object v19, Lorg/apache/lucene/util/RamUsageEstimator;->primitiveSizes:Ljava/util/Map;

    sget-object v20, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const/16 v21, 0x1

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-interface/range {v19 .. v21}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    sget-object v19, Lorg/apache/lucene/util/RamUsageEstimator;->primitiveSizes:Ljava/util/Map;

    sget-object v20, Ljava/lang/Byte;->TYPE:Ljava/lang/Class;

    const/16 v21, 0x1

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-interface/range {v19 .. v21}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    sget-object v19, Lorg/apache/lucene/util/RamUsageEstimator;->primitiveSizes:Ljava/util/Map;

    sget-object v20, Ljava/lang/Character;->TYPE:Ljava/lang/Class;

    const/16 v21, 0x2

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-interface/range {v19 .. v21}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    sget-object v19, Lorg/apache/lucene/util/RamUsageEstimator;->primitiveSizes:Ljava/util/Map;

    sget-object v20, Ljava/lang/Short;->TYPE:Ljava/lang/Class;

    const/16 v21, 0x2

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-interface/range {v19 .. v21}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    sget-object v19, Lorg/apache/lucene/util/RamUsageEstimator;->primitiveSizes:Ljava/util/Map;

    sget-object v20, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/16 v21, 0x4

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-interface/range {v19 .. v21}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    sget-object v19, Lorg/apache/lucene/util/RamUsageEstimator;->primitiveSizes:Ljava/util/Map;

    sget-object v20, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    const/16 v21, 0x4

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-interface/range {v19 .. v21}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    sget-object v19, Lorg/apache/lucene/util/RamUsageEstimator;->primitiveSizes:Ljava/util/Map;

    sget-object v20, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    const/16 v21, 0x8

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-interface/range {v19 .. v21}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    sget-object v19, Lorg/apache/lucene/util/RamUsageEstimator;->primitiveSizes:Ljava/util/Map;

    sget-object v20, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    const/16 v21, 0x8

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-interface/range {v19 .. v21}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    sget-boolean v19, Lorg/apache/lucene/util/Constants;->JRE_IS_64BIT:Z

    if-eqz v19, :cond_1

    const/16 v14, 0x8

    .line 144
    .local v14, "referenceSize":I
    :goto_0
    sget-boolean v19, Lorg/apache/lucene/util/Constants;->JRE_IS_64BIT:Z

    if-eqz v19, :cond_2

    const/16 v11, 0x10

    .line 147
    .local v11, "objectHeader":I
    :goto_1
    sget-boolean v19, Lorg/apache/lucene/util/Constants;->JRE_IS_64BIT:Z

    if-eqz v19, :cond_3

    const/16 v4, 0x18

    .line 149
    .local v4, "arrayHeader":I
    :goto_2
    const-class v19, Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;

    invoke-static/range {v19 .. v19}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v19

    sput-object v19, Lorg/apache/lucene/util/RamUsageEstimator;->supportedFeatures:Ljava/util/EnumSet;

    .line 151
    const/16 v17, 0x0

    .line 152
    .local v17, "unsafeClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/16 v16, 0x0

    .line 154
    .local v16, "tempTheUnsafe":Ljava/lang/Object;
    :try_start_0
    const-string v19, "sun.misc.Unsafe"

    invoke-static/range {v19 .. v19}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v17

    .line 155
    const-string v19, "theUnsafe"

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v18

    .line 156
    .local v18, "unsafeField":Ljava/lang/reflect/Field;
    const/16 v19, 0x1

    invoke-virtual/range {v18 .. v19}, Ljava/lang/reflect/AccessibleObject;->setAccessible(Z)V

    .line 157
    const/16 v19, 0x0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v16

    .line 161
    .end local v16    # "tempTheUnsafe":Ljava/lang/Object;
    .end local v18    # "unsafeField":Ljava/lang/reflect/Field;
    :goto_3
    sput-object v16, Lorg/apache/lucene/util/RamUsageEstimator;->theUnsafe:Ljava/lang/Object;

    .line 165
    :try_start_1
    const-string v19, "arrayIndexScale"

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Class;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const-class v22, Ljava/lang/Class;

    aput-object v22, v20, v21

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    .line 166
    .local v5, "arrayIndexScaleM":Ljava/lang/reflect/Method;
    sget-object v19, Lorg/apache/lucene/util/RamUsageEstimator;->theUnsafe:Ljava/lang/Object;

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const-class v22, [Ljava/lang/Object;

    aput-object v22, v20, v21

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v5, v0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Number;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Number;->intValue()I

    move-result v14

    .line 167
    sget-object v19, Lorg/apache/lucene/util/RamUsageEstimator;->supportedFeatures:Ljava/util/EnumSet;

    sget-object v20, Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;->OBJECT_REFERENCE_SIZE:Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;

    invoke-virtual/range {v19 .. v20}, Ljava/util/AbstractCollection;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 174
    .end local v5    # "arrayIndexScaleM":Ljava/lang/reflect/Method;
    :goto_4
    sget-boolean v19, Lorg/apache/lucene/util/Constants;->JRE_IS_64BIT:Z

    if-eqz v19, :cond_4

    add-int/lit8 v11, v14, 0x8

    .line 175
    :goto_5
    sget-boolean v19, Lorg/apache/lucene/util/Constants;->JRE_IS_64BIT:Z

    if-eqz v19, :cond_5

    mul-int/lit8 v19, v14, 0x2

    add-int/lit8 v4, v19, 0x8

    .line 182
    :goto_6
    const/4 v15, 0x0

    .line 184
    .local v15, "tempObjectFieldOffsetMethod":Ljava/lang/reflect/Method;
    :try_start_2
    const-string v19, "objectFieldOffset"

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Class;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const-class v22, Ljava/lang/reflect/Field;

    aput-object v22, v20, v21

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v10

    .line 185
    .local v10, "objectFieldOffsetM":Ljava/lang/reflect/Method;
    const-class v19, Lorg/apache/lucene/util/RamUsageEstimator$DummyTwoLongObject;

    const-string v20, "dummy1"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v7

    .line 186
    .local v7, "dummy1Field":Ljava/lang/reflect/Field;
    sget-object v19, Lorg/apache/lucene/util/RamUsageEstimator;->theUnsafe:Ljava/lang/Object;

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aput-object v7, v20, v21

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v10, v0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Number;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Number;->intValue()I

    move-result v12

    .line 187
    .local v12, "ofs1":I
    const-class v19, Lorg/apache/lucene/util/RamUsageEstimator$DummyTwoLongObject;

    const-string v20, "dummy2"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v8

    .line 188
    .local v8, "dummy2Field":Ljava/lang/reflect/Field;
    sget-object v19, Lorg/apache/lucene/util/RamUsageEstimator;->theUnsafe:Ljava/lang/Object;

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aput-object v8, v20, v21

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v10, v0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Number;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Number;->intValue()I

    move-result v13

    .line 189
    .local v13, "ofs2":I
    sub-int v19, v13, v12

    invoke-static/range {v19 .. v19}, Ljava/lang/Math;->abs(I)I

    move-result v19

    const/16 v20, 0x8

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_0

    .line 190
    const-class v19, Lorg/apache/lucene/util/RamUsageEstimator$DummyOneFieldObject;

    const-string v20, "base"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v6

    .line 191
    .local v6, "baseField":Ljava/lang/reflect/Field;
    sget-object v19, Lorg/apache/lucene/util/RamUsageEstimator;->theUnsafe:Ljava/lang/Object;

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aput-object v6, v20, v21

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v10, v0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Number;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Number;->intValue()I

    move-result v11

    .line 192
    sget-object v19, Lorg/apache/lucene/util/RamUsageEstimator;->supportedFeatures:Ljava/util/EnumSet;

    sget-object v20, Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;->FIELD_OFFSETS:Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;

    invoke-virtual/range {v19 .. v20}, Ljava/util/AbstractCollection;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 193
    move-object v15, v10

    .line 198
    .end local v6    # "baseField":Ljava/lang/reflect/Field;
    .end local v7    # "dummy1Field":Ljava/lang/reflect/Field;
    .end local v8    # "dummy2Field":Ljava/lang/reflect/Field;
    .end local v10    # "objectFieldOffsetM":Ljava/lang/reflect/Method;
    .end local v12    # "ofs1":I
    .end local v13    # "ofs2":I
    :cond_0
    :goto_7
    sput-object v15, Lorg/apache/lucene/util/RamUsageEstimator;->objectFieldOffsetMethod:Ljava/lang/reflect/Method;

    .line 203
    :try_start_3
    const-string v19, "arrayBaseOffset"

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Class;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const-class v22, Ljava/lang/Class;

    aput-object v22, v20, v21

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 205
    .local v3, "arrayBaseOffsetM":Ljava/lang/reflect/Method;
    sget-object v19, Lorg/apache/lucene/util/RamUsageEstimator;->theUnsafe:Ljava/lang/Object;

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const-class v22, [B

    aput-object v22, v20, v21

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v3, v0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Number;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Number;->intValue()I

    move-result v4

    .line 206
    sget-object v19, Lorg/apache/lucene/util/RamUsageEstimator;->supportedFeatures:Ljava/util/EnumSet;

    sget-object v20, Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;->ARRAY_HEADER_SIZE:Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;

    invoke-virtual/range {v19 .. v20}, Ljava/util/AbstractCollection;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    .line 211
    .end local v3    # "arrayBaseOffsetM":Ljava/lang/reflect/Method;
    :goto_8
    sput v14, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_REF:I

    .line 212
    sput v11, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_HEADER:I

    .line 213
    sput v4, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_ARRAY_HEADER:I

    .line 217
    const/16 v9, 0x8

    .line 239
    .local v9, "objectAlignment":I
    sput v9, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_ALIGNMENT:I

    .line 241
    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "[JVM: "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 242
    sget-object v20, Lorg/apache/lucene/util/Constants;->JVM_NAME:Ljava/lang/String;

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ", "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    sget-object v20, Lorg/apache/lucene/util/Constants;->JVM_VERSION:Ljava/lang/String;

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ", "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    sget-object v20, Lorg/apache/lucene/util/Constants;->JVM_VENDOR:Ljava/lang/String;

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ", "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    .line 243
    sget-object v20, Lorg/apache/lucene/util/Constants;->JAVA_VENDOR:Ljava/lang/String;

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ", "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    sget-object v20, Lorg/apache/lucene/util/Constants;->JAVA_VERSION:Ljava/lang/String;

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "]"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    .line 241
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    sput-object v19, Lorg/apache/lucene/util/RamUsageEstimator;->JVM_INFO_STRING:Ljava/lang/String;

    .line 244
    return-void

    .line 143
    .end local v4    # "arrayHeader":I
    .end local v9    # "objectAlignment":I
    .end local v11    # "objectHeader":I
    .end local v14    # "referenceSize":I
    .end local v15    # "tempObjectFieldOffsetMethod":Ljava/lang/reflect/Method;
    .end local v17    # "unsafeClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_1
    const/4 v14, 0x4

    goto/16 :goto_0

    .line 144
    .restart local v14    # "referenceSize":I
    :cond_2
    const/16 v11, 0x8

    goto/16 :goto_1

    .line 147
    .restart local v11    # "objectHeader":I
    :cond_3
    const/16 v4, 0xc

    goto/16 :goto_2

    .line 174
    .restart local v4    # "arrayHeader":I
    .restart local v17    # "unsafeClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_4
    const/16 v11, 0x8

    goto/16 :goto_5

    .line 175
    :cond_5
    const/16 v4, 0xc

    goto/16 :goto_6

    .line 207
    .restart local v15    # "tempObjectFieldOffsetMethod":Ljava/lang/reflect/Method;
    :catch_0
    move-exception v19

    goto :goto_8

    .line 195
    :catch_1
    move-exception v19

    goto/16 :goto_7

    .line 168
    .end local v15    # "tempObjectFieldOffsetMethod":Ljava/lang/reflect/Method;
    :catch_2
    move-exception v19

    goto/16 :goto_4

    .line 158
    .restart local v16    # "tempTheUnsafe":Ljava/lang/Object;
    :catch_3
    move-exception v19

    goto/16 :goto_3
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static adjustForField(JLjava/lang/reflect/Field;)J
    .locals 12
    .param p0, "sizeSoFar"    # J
    .param p2, "f"    # Ljava/lang/reflect/Field;

    .prologue
    .line 526
    invoke-virtual {p2}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v6

    .line 527
    .local v6, "type":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {v6}, Ljava/lang/Class;->isPrimitive()Z

    move-result v7

    if-eqz v7, :cond_0

    sget-object v7, Lorg/apache/lucene/util/RamUsageEstimator;->primitiveSizes:Ljava/util/Map;

    invoke-interface {v7, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 528
    .local v2, "fsize":I
    :goto_0
    sget-object v7, Lorg/apache/lucene/util/RamUsageEstimator;->objectFieldOffsetMethod:Ljava/lang/reflect/Method;

    if-eqz v7, :cond_3

    .line 531
    :try_start_0
    sget-object v7, Lorg/apache/lucene/util/RamUsageEstimator;->objectFieldOffsetMethod:Ljava/lang/reflect/Method;

    sget-object v8, Lorg/apache/lucene/util/RamUsageEstimator;->theUnsafe:Ljava/lang/Object;

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object p2, v9, v10

    invoke-virtual {v7, v8, v9}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Number;

    invoke-virtual {v7}, Ljava/lang/Number;->longValue()J

    move-result-wide v8

    int-to-long v10, v2

    add-long v4, v8, v10

    .line 532
    .local v4, "offsetPlusSize":J
    invoke-static {p0, p1, v4, v5}, Ljava/lang/Math;->max(JJ)J
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-wide v8

    .line 549
    .end local v4    # "offsetPlusSize":J
    :goto_1
    return-wide v8

    .line 527
    .end local v2    # "fsize":I
    :cond_0
    sget v2, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_REF:I

    goto :goto_0

    .line 533
    .restart local v2    # "fsize":I
    :catch_0
    move-exception v1

    .line 534
    .local v1, "ex":Ljava/lang/IllegalAccessException;
    new-instance v7, Ljava/lang/RuntimeException;

    const-string v8, "Access problem with sun.misc.Unsafe"

    invoke-direct {v7, v8, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v7

    .line 535
    .end local v1    # "ex":Ljava/lang/IllegalAccessException;
    :catch_1
    move-exception v3

    .line 536
    .local v3, "ite":Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v3}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    .line 537
    .local v0, "cause":Ljava/lang/Throwable;
    instance-of v7, v0, Ljava/lang/RuntimeException;

    if-eqz v7, :cond_1

    .line 538
    check-cast v0, Ljava/lang/RuntimeException;

    .end local v0    # "cause":Ljava/lang/Throwable;
    throw v0

    .line 539
    .restart local v0    # "cause":Ljava/lang/Throwable;
    :cond_1
    instance-of v7, v0, Ljava/lang/Error;

    if-eqz v7, :cond_2

    .line 540
    check-cast v0, Ljava/lang/Error;

    .end local v0    # "cause":Ljava/lang/Throwable;
    throw v0

    .line 543
    .restart local v0    # "cause":Ljava/lang/Throwable;
    :cond_2
    new-instance v7, Ljava/lang/RuntimeException;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Call to Unsafe\'s objectFieldOffset() throwed checked Exception when accessing field "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 545
    invoke-virtual {p2}, Ljava/lang/reflect/Field;->getDeclaringClass()Ljava/lang/Class;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "#"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p2}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 543
    invoke-direct {v7, v8, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v7

    .line 549
    .end local v0    # "cause":Ljava/lang/Throwable;
    .end local v3    # "ite":Ljava/lang/reflect/InvocationTargetException;
    :cond_3
    int-to-long v8, v2

    add-long/2addr v8, p0

    goto :goto_1
.end method

.method public static alignObjectSize(J)J
    .locals 4
    .param p0, "size"    # J

    .prologue
    .line 287
    sget v0, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_ALIGNMENT:I

    int-to-long v0, v0

    const-wide/16 v2, 0x1

    sub-long/2addr v0, v2

    add-long/2addr p0, v0

    .line 288
    sget v0, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_ALIGNMENT:I

    int-to-long v0, v0

    rem-long v0, p0, v0

    sub-long v0, p0, v0

    return-wide v0
.end method

.method private static createCacheEntry(Ljava/lang/Class;)Lorg/apache/lucene/util/RamUsageEstimator$ClassCache;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Lorg/apache/lucene/util/RamUsageEstimator$ClassCache;"
        }
    .end annotation

    .prologue
    .line 496
    .local p0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    sget v5, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_HEADER:I

    int-to-long v6, v5

    .line 497
    .local v6, "shallowInstanceSize":J
    new-instance v4, Ljava/util/ArrayList;

    const/16 v5, 0x20

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 498
    .local v4, "referenceFields":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/reflect/Field;>;"
    move-object v0, p0

    .local v0, "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :goto_0
    if-nez v0, :cond_0

    .line 512
    new-instance v1, Lorg/apache/lucene/util/RamUsageEstimator$ClassCache;

    .line 513
    invoke-static {v6, v7}, Lorg/apache/lucene/util/RamUsageEstimator;->alignObjectSize(J)J

    move-result-wide v8

    .line 514
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    new-array v5, v5, [Ljava/lang/reflect/Field;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Ljava/lang/reflect/Field;

    .line 512
    invoke-direct {v1, v8, v9, v5}, Lorg/apache/lucene/util/RamUsageEstimator$ClassCache;-><init>(J[Ljava/lang/reflect/Field;)V

    .line 515
    .local v1, "cachedInfo":Lorg/apache/lucene/util/RamUsageEstimator$ClassCache;
    return-object v1

    .line 499
    .end local v1    # "cachedInfo":Lorg/apache/lucene/util/RamUsageEstimator$ClassCache;
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v3

    .line 500
    .local v3, "fields":[Ljava/lang/reflect/Field;
    array-length v8, v3

    const/4 v5, 0x0

    :goto_1
    if-lt v5, v8, :cond_1

    .line 498
    invoke-virtual {v0}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v0

    goto :goto_0

    .line 500
    :cond_1
    aget-object v2, v3, v5

    .line 501
    .local v2, "f":Ljava/lang/reflect/Field;
    invoke-virtual {v2}, Ljava/lang/reflect/Field;->getModifiers()I

    move-result v9

    invoke-static {v9}, Ljava/lang/reflect/Modifier;->isStatic(I)Z

    move-result v9

    if-nez v9, :cond_2

    .line 502
    invoke-static {v6, v7, v2}, Lorg/apache/lucene/util/RamUsageEstimator;->adjustForField(JLjava/lang/reflect/Field;)J

    move-result-wide v6

    .line 504
    invoke-virtual {v2}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Class;->isPrimitive()Z

    move-result v9

    if-nez v9, :cond_2

    .line 505
    const/4 v9, 0x1

    invoke-virtual {v2, v9}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 506
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 500
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1
.end method

.method public static getSupportedFeatures()Ljava/util/EnumSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumSet",
            "<",
            "Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;",
            ">;"
        }
    .end annotation

    .prologue
    .line 562
    sget-object v0, Lorg/apache/lucene/util/RamUsageEstimator;->supportedFeatures:Ljava/util/EnumSet;

    invoke-static {v0}, Ljava/util/EnumSet;->copyOf(Ljava/util/EnumSet;)Ljava/util/EnumSet;

    move-result-object v0

    return-object v0
.end method

.method public static getUnsupportedFeatures()Ljava/util/EnumSet;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumSet",
            "<",
            "Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;",
            ">;"
        }
    .end annotation

    .prologue
    .line 555
    const-class v1, Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;

    invoke-static {v1}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    .line 556
    .local v0, "unsupported":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;>;"
    sget-object v1, Lorg/apache/lucene/util/RamUsageEstimator;->supportedFeatures:Ljava/util/EnumSet;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->removeAll(Ljava/util/Collection;)Z

    .line 557
    return-object v0
.end method

.method public static humanReadableUnits(J)Ljava/lang/String;
    .locals 4
    .param p0, "bytes"    # J

    .prologue
    .line 569
    .line 570
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "0.#"

    sget-object v2, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-static {v2}, Ljava/text/DecimalFormatSymbols;->getInstance(Ljava/util/Locale;)Ljava/text/DecimalFormatSymbols;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 569
    invoke-static {p0, p1, v0}, Lorg/apache/lucene/util/RamUsageEstimator;->humanReadableUnits(JLjava/text/DecimalFormat;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static humanReadableUnits(JLjava/text/DecimalFormat;)Ljava/lang/String;
    .locals 4
    .param p0, "bytes"    # J
    .param p2, "df"    # Ljava/text/DecimalFormat;

    .prologue
    const-wide/16 v2, 0x0

    .line 577
    const-wide/32 v0, 0x40000000

    div-long v0, p0, v0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 578
    new-instance v0, Ljava/lang/StringBuilder;

    long-to-float v1, p0

    const/high16 v2, 0x4e800000

    div-float/2addr v1, v2

    float-to-double v2, v1

    invoke-virtual {p2, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, " GB"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 584
    :goto_0
    return-object v0

    .line 579
    :cond_0
    const-wide/32 v0, 0x100000

    div-long v0, p0, v0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 580
    new-instance v0, Ljava/lang/StringBuilder;

    long-to-float v1, p0

    const/high16 v2, 0x49800000    # 1048576.0f

    div-float/2addr v1, v2

    float-to-double v2, v1

    invoke-virtual {p2, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, " MB"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 581
    :cond_1
    const-wide/16 v0, 0x400

    div-long v0, p0, v0

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    .line 582
    new-instance v0, Ljava/lang/StringBuilder;

    long-to-float v1, p0

    const/high16 v2, 0x44800000    # 1024.0f

    div-float/2addr v1, v2

    float-to-double v2, v1

    invoke-virtual {p2, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, " KB"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 584
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p0, p1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, " bytes"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static humanSizeOf(Ljava/lang/Object;)Ljava/lang/String;
    .locals 2
    .param p0, "object"    # Ljava/lang/Object;

    .prologue
    .line 594
    invoke-static {p0}, Lorg/apache/lucene/util/RamUsageEstimator;->sizeOf(Ljava/lang/Object;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lorg/apache/lucene/util/RamUsageEstimator;->humanReadableUnits(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static isSupportedJVM()Z
    .locals 2

    .prologue
    .line 280
    sget-object v0, Lorg/apache/lucene/util/RamUsageEstimator;->supportedFeatures:Ljava/util/EnumSet;

    invoke-virtual {v0}, Ljava/util/EnumSet;->size()I

    move-result v0

    invoke-static {}, Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;->values()[Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;

    move-result-object v1

    array-length v1, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static measureObjectSize(Ljava/lang/Object;)J
    .locals 22
    .param p0, "root"    # Ljava/lang/Object;

    .prologue
    .line 414
    new-instance v12, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;

    invoke-direct {v12}, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;-><init>()V

    .line 416
    .local v12, "seen":Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;, "Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet<Ljava/lang/Object;>;"
    new-instance v3, Ljava/util/IdentityHashMap;

    invoke-direct {v3}, Ljava/util/IdentityHashMap;-><init>()V

    .line 418
    .local v3, "classCache":Ljava/util/IdentityHashMap;, "Ljava/util/IdentityHashMap<Ljava/lang/Class<*>;Lorg/apache/lucene/util/RamUsageEstimator$ClassCache;>;"
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 419
    .local v13, "stack":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 421
    const-wide/16 v16, 0x0

    .line 422
    .local v16, "totalSize":J
    :cond_0
    :goto_0
    invoke-virtual {v13}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v18

    if-eqz v18, :cond_1

    .line 483
    invoke-virtual {v12}, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;->clear()V

    .line 484
    invoke-virtual {v13}, Ljava/util/ArrayList;->clear()V

    .line 485
    invoke-virtual {v3}, Ljava/util/IdentityHashMap;->clear()V

    .line 487
    return-wide v16

    .line 423
    :cond_1
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v18

    add-int/lit8 v18, v18, -0x1

    move/from16 v0, v18

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v10

    .line 425
    .local v10, "ob":Ljava/lang/Object;
    if-eqz v10, :cond_0

    invoke-virtual {v12, v10}, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;->contains(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_0

    .line 428
    invoke-virtual {v12, v10}, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;->add(Ljava/lang/Object;)Z

    .line 430
    invoke-virtual {v10}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v11

    .line 431
    .local v11, "obClazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {v11}, Ljava/lang/Class;->isArray()Z

    move-result v18

    if-eqz v18, :cond_5

    .line 436
    sget v18, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_ARRAY_HEADER:I

    move/from16 v0, v18

    int-to-long v14, v0

    .line 437
    .local v14, "size":J
    invoke-static {v10}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v8

    .line 438
    .local v8, "len":I
    if-lez v8, :cond_2

    .line 439
    invoke-virtual {v11}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v4

    .line 440
    .local v4, "componentClazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {v4}, Ljava/lang/Class;->isPrimitive()Z

    move-result v18

    if-eqz v18, :cond_3

    .line 441
    int-to-long v0, v8

    move-wide/from16 v20, v0

    sget-object v18, Lorg/apache/lucene/util/RamUsageEstimator;->primitiveSizes:Ljava/util/Map;

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/Integer;

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Integer;->intValue()I

    move-result v18

    move/from16 v0, v18

    int-to-long v0, v0

    move-wide/from16 v18, v0

    mul-long v18, v18, v20

    add-long v14, v14, v18

    .line 454
    .end local v4    # "componentClazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_2
    invoke-static {v14, v15}, Lorg/apache/lucene/util/RamUsageEstimator;->alignObjectSize(J)J

    move-result-wide v18

    add-long v16, v16, v18

    .line 455
    goto :goto_0

    .line 443
    .restart local v4    # "componentClazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_3
    sget v18, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_REF:I

    move/from16 v0, v18

    int-to-long v0, v0

    move-wide/from16 v18, v0

    int-to-long v0, v8

    move-wide/from16 v20, v0

    mul-long v18, v18, v20

    add-long v14, v14, v18

    .line 446
    move v7, v8

    .local v7, "i":I
    :cond_4
    :goto_1
    add-int/lit8 v7, v7, -0x1

    if-ltz v7, :cond_2

    .line 447
    invoke-static {v10, v7}, Ljava/lang/reflect/Array;->get(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v9

    .line 448
    .local v9, "o":Ljava/lang/Object;
    if-eqz v9, :cond_4

    invoke-virtual {v12, v9}, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;->contains(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_4

    .line 449
    invoke-virtual {v13, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 461
    .end local v4    # "componentClazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v7    # "i":I
    .end local v8    # "len":I
    .end local v9    # "o":Ljava/lang/Object;
    .end local v14    # "size":J
    :cond_5
    :try_start_0
    invoke-virtual {v3, v11}, Ljava/util/IdentityHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/util/RamUsageEstimator$ClassCache;

    .line 462
    .local v2, "cachedInfo":Lorg/apache/lucene/util/RamUsageEstimator$ClassCache;
    if-nez v2, :cond_6

    .line 463
    invoke-static {v11}, Lorg/apache/lucene/util/RamUsageEstimator;->createCacheEntry(Ljava/lang/Class;)Lorg/apache/lucene/util/RamUsageEstimator$ClassCache;

    move-result-object v2

    invoke-virtual {v3, v11, v2}, Ljava/util/IdentityHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 466
    :cond_6
    iget-object v0, v2, Lorg/apache/lucene/util/RamUsageEstimator$ClassCache;->referenceFields:[Ljava/lang/reflect/Field;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v20, v0

    const/16 v18, 0x0

    :goto_2
    move/from16 v0, v18

    move/from16 v1, v20

    if-lt v0, v1, :cond_7

    .line 474
    iget-wide v0, v2, Lorg/apache/lucene/util/RamUsageEstimator$ClassCache;->alignedShallowInstanceSize:J

    move-wide/from16 v18, v0

    add-long v16, v16, v18

    goto/16 :goto_0

    .line 466
    :cond_7
    aget-object v6, v19, v18

    .line 468
    .local v6, "f":Ljava/lang/reflect/Field;
    invoke-virtual {v6, v10}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    .line 469
    .restart local v9    # "o":Ljava/lang/Object;
    if-eqz v9, :cond_8

    invoke-virtual {v12, v9}, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;->contains(Ljava/lang/Object;)Z

    move-result v21

    if-nez v21, :cond_8

    .line 470
    invoke-virtual {v13, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    .line 466
    :cond_8
    add-int/lit8 v18, v18, 0x1

    goto :goto_2

    .line 475
    .end local v2    # "cachedInfo":Lorg/apache/lucene/util/RamUsageEstimator$ClassCache;
    .end local v6    # "f":Ljava/lang/reflect/Field;
    .end local v9    # "o":Ljava/lang/Object;
    :catch_0
    move-exception v5

    .line 477
    .local v5, "e":Ljava/lang/IllegalAccessException;
    new-instance v18, Ljava/lang/RuntimeException;

    const-string v19, "Reflective field access failed?"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v18
.end method

.method public static shallowSizeOf(Ljava/lang/Object;)J
    .locals 4
    .param p0, "obj"    # Ljava/lang/Object;

    .prologue
    .line 352
    if-nez p0, :cond_0

    const-wide/16 v2, 0x0

    .line 357
    :goto_0
    return-wide v2

    .line 353
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 354
    .local v0, "clz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {v0}, Ljava/lang/Class;->isArray()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 355
    invoke-static {p0}, Lorg/apache/lucene/util/RamUsageEstimator;->shallowSizeOfArray(Ljava/lang/Object;)J

    move-result-wide v2

    goto :goto_0

    .line 357
    :cond_1
    invoke-static {v0}, Lorg/apache/lucene/util/RamUsageEstimator;->shallowSizeOfInstance(Ljava/lang/Class;)J

    move-result-wide v2

    goto :goto_0
.end method

.method private static shallowSizeOfArray(Ljava/lang/Object;)J
    .locals 8
    .param p0, "array"    # Ljava/lang/Object;

    .prologue
    .line 393
    sget v4, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_ARRAY_HEADER:I

    int-to-long v2, v4

    .line 394
    .local v2, "size":J
    invoke-static {p0}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v1

    .line 395
    .local v1, "len":I
    if-lez v1, :cond_0

    .line 396
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v0

    .line 397
    .local v0, "arrayElementClazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {v0}, Ljava/lang/Class;->isPrimitive()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 398
    int-to-long v6, v1

    sget-object v4, Lorg/apache/lucene/util/RamUsageEstimator;->primitiveSizes:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    int-to-long v4, v4

    mul-long/2addr v4, v6

    add-long/2addr v2, v4

    .line 403
    .end local v0    # "arrayElementClazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_0
    :goto_0
    invoke-static {v2, v3}, Lorg/apache/lucene/util/RamUsageEstimator;->alignObjectSize(J)J

    move-result-wide v4

    return-wide v4

    .line 400
    .restart local v0    # "arrayElementClazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_1
    sget v4, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_REF:I

    int-to-long v4, v4

    int-to-long v6, v1

    mul-long/2addr v4, v6

    add-long/2addr v2, v4

    goto :goto_0
.end method

.method public static shallowSizeOfInstance(Ljava/lang/Class;)J
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)J"
        }
    .end annotation

    .prologue
    .line 370
    .local p0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {p0}, Ljava/lang/Class;->isArray()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 371
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "This method does not work with array classes."

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 372
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Class;->isPrimitive()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 373
    sget-object v4, Lorg/apache/lucene/util/RamUsageEstimator;->primitiveSizes:Ljava/util/Map;

    invoke-interface {v4, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    int-to-long v4, v4

    .line 386
    :goto_0
    return-wide v4

    .line 375
    :cond_1
    sget v4, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_HEADER:I

    int-to-long v2, v4

    .line 378
    .local v2, "size":J
    :goto_1
    if-nez p0, :cond_2

    .line 386
    invoke-static {v2, v3}, Lorg/apache/lucene/util/RamUsageEstimator;->alignObjectSize(J)J

    move-result-wide v4

    goto :goto_0

    .line 379
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v1

    .line 380
    .local v1, "fields":[Ljava/lang/reflect/Field;
    array-length v5, v1

    const/4 v4, 0x0

    :goto_2
    if-lt v4, v5, :cond_3

    .line 378
    invoke-virtual {p0}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object p0

    goto :goto_1

    .line 380
    :cond_3
    aget-object v0, v1, v4

    .line 381
    .local v0, "f":Ljava/lang/reflect/Field;
    invoke-virtual {v0}, Ljava/lang/reflect/Field;->getModifiers()I

    move-result v6

    invoke-static {v6}, Ljava/lang/reflect/Modifier;->isStatic(I)Z

    move-result v6

    if-nez v6, :cond_4

    .line 382
    invoke-static {v2, v3, v0}, Lorg/apache/lucene/util/RamUsageEstimator;->adjustForField(JLjava/lang/reflect/Field;)J

    move-result-wide v2

    .line 380
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_2
.end method

.method public static sizeOf(Ljava/lang/Object;)J
    .locals 2
    .param p0, "obj"    # Ljava/lang/Object;

    .prologue
    .line 341
    invoke-static {p0}, Lorg/apache/lucene/util/RamUsageEstimator;->measureObjectSize(Ljava/lang/Object;)J

    move-result-wide v0

    return-wide v0
.end method

.method public static sizeOf([B)J
    .locals 4
    .param p0, "arr"    # [B

    .prologue
    .line 293
    sget v0, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_ARRAY_HEADER:I

    int-to-long v0, v0

    array-length v2, p0

    int-to-long v2, v2

    add-long/2addr v0, v2

    invoke-static {v0, v1}, Lorg/apache/lucene/util/RamUsageEstimator;->alignObjectSize(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static sizeOf([C)J
    .locals 6
    .param p0, "arr"    # [C

    .prologue
    .line 303
    sget v0, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_ARRAY_HEADER:I

    int-to-long v0, v0

    const-wide/16 v2, 0x2

    array-length v4, p0

    int-to-long v4, v4

    mul-long/2addr v2, v4

    add-long/2addr v0, v2

    invoke-static {v0, v1}, Lorg/apache/lucene/util/RamUsageEstimator;->alignObjectSize(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static sizeOf([D)J
    .locals 6
    .param p0, "arr"    # [D

    .prologue
    .line 328
    sget v0, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_ARRAY_HEADER:I

    int-to-long v0, v0

    const-wide/16 v2, 0x8

    array-length v4, p0

    int-to-long v4, v4

    mul-long/2addr v2, v4

    add-long/2addr v0, v2

    invoke-static {v0, v1}, Lorg/apache/lucene/util/RamUsageEstimator;->alignObjectSize(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static sizeOf([F)J
    .locals 6
    .param p0, "arr"    # [F

    .prologue
    .line 318
    sget v0, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_ARRAY_HEADER:I

    int-to-long v0, v0

    const-wide/16 v2, 0x4

    array-length v4, p0

    int-to-long v4, v4

    mul-long/2addr v2, v4

    add-long/2addr v0, v2

    invoke-static {v0, v1}, Lorg/apache/lucene/util/RamUsageEstimator;->alignObjectSize(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static sizeOf([I)J
    .locals 6
    .param p0, "arr"    # [I

    .prologue
    .line 313
    sget v0, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_ARRAY_HEADER:I

    int-to-long v0, v0

    const-wide/16 v2, 0x4

    array-length v4, p0

    int-to-long v4, v4

    mul-long/2addr v2, v4

    add-long/2addr v0, v2

    invoke-static {v0, v1}, Lorg/apache/lucene/util/RamUsageEstimator;->alignObjectSize(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static sizeOf([J)J
    .locals 6
    .param p0, "arr"    # [J

    .prologue
    .line 323
    sget v0, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_ARRAY_HEADER:I

    int-to-long v0, v0

    const-wide/16 v2, 0x8

    array-length v4, p0

    int-to-long v4, v4

    mul-long/2addr v2, v4

    add-long/2addr v0, v2

    invoke-static {v0, v1}, Lorg/apache/lucene/util/RamUsageEstimator;->alignObjectSize(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static sizeOf([S)J
    .locals 6
    .param p0, "arr"    # [S

    .prologue
    .line 308
    sget v0, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_ARRAY_HEADER:I

    int-to-long v0, v0

    const-wide/16 v2, 0x2

    array-length v4, p0

    int-to-long v4, v4

    mul-long/2addr v2, v4

    add-long/2addr v0, v2

    invoke-static {v0, v1}, Lorg/apache/lucene/util/RamUsageEstimator;->alignObjectSize(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static sizeOf([Z)J
    .locals 4
    .param p0, "arr"    # [Z

    .prologue
    .line 298
    sget v0, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_ARRAY_HEADER:I

    int-to-long v0, v0

    array-length v2, p0

    int-to-long v2, v2

    add-long/2addr v0, v2

    invoke-static {v0, v1}, Lorg/apache/lucene/util/RamUsageEstimator;->alignObjectSize(J)J

    move-result-wide v0

    return-wide v0
.end method
