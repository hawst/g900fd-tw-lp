.class public final Lorg/apache/lucene/util/RecyclingByteBlockAllocator;
.super Lorg/apache/lucene/util/ByteBlockPool$Allocator;
.source "RecyclingByteBlockAllocator.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final DEFAULT_BUFFERED_BLOCKS:I = 0x40


# instance fields
.field private final bytesUsed:Lorg/apache/lucene/util/Counter;

.field private freeBlocks:I

.field private freeByteBlocks:[[B

.field private final maxBufferedBlocks:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->$assertionsDisabled:Z

    .line 36
    return-void

    .line 31
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 75
    const v0, 0x8000

    const/16 v1, 0x40

    const/4 v2, 0x0

    invoke-static {v2}, Lorg/apache/lucene/util/Counter;->newCounter(Z)Lorg/apache/lucene/util/Counter;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;-><init>(IILorg/apache/lucene/util/Counter;)V

    .line 76
    return-void
.end method

.method public constructor <init>(II)V
    .locals 1
    .param p1, "blockSize"    # I
    .param p2, "maxBufferedBlocks"    # I

    .prologue
    .line 65
    const/4 v0, 0x0

    invoke-static {v0}, Lorg/apache/lucene/util/Counter;->newCounter(Z)Lorg/apache/lucene/util/Counter;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;-><init>(IILorg/apache/lucene/util/Counter;)V

    .line 66
    return-void
.end method

.method public constructor <init>(IILorg/apache/lucene/util/Counter;)V
    .locals 1
    .param p1, "blockSize"    # I
    .param p2, "maxBufferedBlocks"    # I
    .param p3, "bytesUsed"    # Lorg/apache/lucene/util/Counter;

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lorg/apache/lucene/util/ByteBlockPool$Allocator;-><init>(I)V

    .line 34
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->freeBlocks:I

    .line 51
    new-array v0, p2, [[B

    iput-object v0, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->freeByteBlocks:[[B

    .line 52
    iput p2, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->maxBufferedBlocks:I

    .line 53
    iput-object p3, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->bytesUsed:Lorg/apache/lucene/util/Counter;

    .line 54
    return-void
.end method


# virtual methods
.method public bytesUsed()J
    .locals 2

    .prologue
    .line 122
    iget-object v0, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->bytesUsed:Lorg/apache/lucene/util/Counter;

    invoke-virtual {v0}, Lorg/apache/lucene/util/Counter;->get()J

    move-result-wide v0

    return-wide v0
.end method

.method public freeBlocks(I)I
    .locals 6
    .param p1, "num"    # I

    .prologue
    .line 140
    sget-boolean v2, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    if-gez p1, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "free blocks must be >= 0 but was: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 143
    :cond_0
    iget v2, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->freeBlocks:I

    if-le p1, v2, :cond_1

    .line 144
    const/4 v1, 0x0

    .line 145
    .local v1, "stop":I
    iget v0, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->freeBlocks:I

    .line 150
    .local v0, "count":I
    :goto_0
    iget v2, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->freeBlocks:I

    if-gt v2, v1, :cond_2

    .line 153
    iget-object v2, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->bytesUsed:Lorg/apache/lucene/util/Counter;

    neg-int v3, v0

    iget v4, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->blockSize:I

    mul-int/2addr v3, v4

    int-to-long v4, v3

    invoke-virtual {v2, v4, v5}, Lorg/apache/lucene/util/Counter;->addAndGet(J)J

    .line 154
    sget-boolean v2, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->$assertionsDisabled:Z

    if-nez v2, :cond_3

    iget-object v2, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->bytesUsed:Lorg/apache/lucene/util/Counter;

    invoke-virtual {v2}, Lorg/apache/lucene/util/Counter;->get()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-gez v2, :cond_3

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 147
    .end local v0    # "count":I
    .end local v1    # "stop":I
    :cond_1
    iget v2, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->freeBlocks:I

    sub-int v1, v2, p1

    .line 148
    .restart local v1    # "stop":I
    move v0, p1

    .line 150
    .restart local v0    # "count":I
    goto :goto_0

    .line 151
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->freeByteBlocks:[[B

    iget v3, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->freeBlocks:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->freeBlocks:I

    const/4 v4, 0x0

    aput-object v4, v2, v3

    goto :goto_0

    .line 155
    :cond_3
    return v0
.end method

.method public getByteBlock()[B
    .locals 4

    .prologue
    .line 80
    iget v1, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->freeBlocks:I

    if-nez v1, :cond_0

    .line 81
    iget-object v1, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->bytesUsed:Lorg/apache/lucene/util/Counter;

    iget v2, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->blockSize:I

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/util/Counter;->addAndGet(J)J

    .line 82
    iget v1, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->blockSize:I

    new-array v0, v1, [B

    .line 86
    :goto_0
    return-object v0

    .line 84
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->freeByteBlocks:[[B

    iget v2, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->freeBlocks:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->freeBlocks:I

    aget-object v0, v1, v2

    .line 85
    .local v0, "b":[B
    iget-object v1, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->freeByteBlocks:[[B

    iget v2, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->freeBlocks:I

    const/4 v3, 0x0

    aput-object v3, v1, v2

    goto :goto_0
.end method

.method public maxBufferedBlocks()I
    .locals 1

    .prologue
    .line 129
    iget v0, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->maxBufferedBlocks:I

    return v0
.end method

.method public numBufferedBlocks()I
    .locals 1

    .prologue
    .line 115
    iget v0, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->freeBlocks:I

    return v0
.end method

.method public recycleByteBlocks([[BII)V
    .locals 10
    .param p1, "blocks"    # [[B
    .param p2, "start"    # I
    .param p3, "end"    # I

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 91
    iget v5, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->maxBufferedBlocks:I

    iget v6, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->freeBlocks:I

    sub-int/2addr v5, v6

    sub-int v6, p3, p2

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 92
    .local v2, "numBlocks":I
    iget v5, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->freeBlocks:I

    add-int v3, v5, v2

    .line 93
    .local v3, "size":I
    iget-object v5, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->freeByteBlocks:[[B

    array-length v5, v5

    if-lt v3, v5, :cond_0

    .line 95
    sget v5, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_REF:I

    .line 94
    invoke-static {v3, v5}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v5

    new-array v1, v5, [[B

    .line 96
    .local v1, "newBlocks":[[B
    iget-object v5, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->freeByteBlocks:[[B

    iget v6, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->freeBlocks:I

    invoke-static {v5, v7, v1, v7, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 97
    iput-object v1, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->freeByteBlocks:[[B

    .line 99
    .end local v1    # "newBlocks":[[B
    :cond_0
    add-int v4, p2, v2

    .line 100
    .local v4, "stop":I
    move v0, p2

    .local v0, "i":I
    :goto_0
    if-lt v0, v4, :cond_1

    .line 104
    move v0, v4

    :goto_1
    if-lt v0, p3, :cond_2

    .line 107
    iget-object v5, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->bytesUsed:Lorg/apache/lucene/util/Counter;

    sub-int v6, p3, v4

    neg-int v6, v6

    iget v7, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->blockSize:I

    mul-int/2addr v6, v7

    int-to-long v6, v6

    invoke-virtual {v5, v6, v7}, Lorg/apache/lucene/util/Counter;->addAndGet(J)J

    .line 108
    sget-boolean v5, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->$assertionsDisabled:Z

    if-nez v5, :cond_3

    iget-object v5, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->bytesUsed:Lorg/apache/lucene/util/Counter;

    invoke-virtual {v5}, Lorg/apache/lucene/util/Counter;->get()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-gez v5, :cond_3

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 101
    :cond_1
    iget-object v5, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->freeByteBlocks:[[B

    iget v6, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->freeBlocks:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->freeBlocks:I

    aget-object v7, p1, v0

    aput-object v7, v5, v6

    .line 102
    aput-object v8, p1, v0

    .line 100
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 105
    :cond_2
    aput-object v8, p1, v0

    .line 104
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 109
    :cond_3
    return-void
.end method
