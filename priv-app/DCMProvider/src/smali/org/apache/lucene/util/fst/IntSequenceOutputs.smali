.class public final Lorg/apache/lucene/util/fst/IntSequenceOutputs;
.super Lorg/apache/lucene/util/fst/Outputs;
.source "IntSequenceOutputs.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/util/fst/Outputs",
        "<",
        "Lorg/apache/lucene/util/IntsRef;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final NO_OUTPUT:Lorg/apache/lucene/util/IntsRef;

.field private static final singleton:Lorg/apache/lucene/util/fst/IntSequenceOutputs;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lorg/apache/lucene/util/fst/IntSequenceOutputs;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/fst/IntSequenceOutputs;->$assertionsDisabled:Z

    .line 35
    new-instance v0, Lorg/apache/lucene/util/IntsRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/IntsRef;-><init>()V

    sput-object v0, Lorg/apache/lucene/util/fst/IntSequenceOutputs;->NO_OUTPUT:Lorg/apache/lucene/util/IntsRef;

    .line 36
    new-instance v0, Lorg/apache/lucene/util/fst/IntSequenceOutputs;

    invoke-direct {v0}, Lorg/apache/lucene/util/fst/IntSequenceOutputs;-><init>()V

    sput-object v0, Lorg/apache/lucene/util/fst/IntSequenceOutputs;->singleton:Lorg/apache/lucene/util/fst/IntSequenceOutputs;

    return-void

    .line 33
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lorg/apache/lucene/util/fst/Outputs;-><init>()V

    .line 39
    return-void
.end method

.method public static getSingleton()Lorg/apache/lucene/util/fst/IntSequenceOutputs;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lorg/apache/lucene/util/fst/IntSequenceOutputs;->singleton:Lorg/apache/lucene/util/fst/IntSequenceOutputs;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/util/IntsRef;

    check-cast p2, Lorg/apache/lucene/util/IntsRef;

    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/util/fst/IntSequenceOutputs;->add(Lorg/apache/lucene/util/IntsRef;Lorg/apache/lucene/util/IntsRef;)Lorg/apache/lucene/util/IntsRef;

    move-result-object v0

    return-object v0
.end method

.method public add(Lorg/apache/lucene/util/IntsRef;Lorg/apache/lucene/util/IntsRef;)Lorg/apache/lucene/util/IntsRef;
    .locals 6
    .param p1, "prefix"    # Lorg/apache/lucene/util/IntsRef;
    .param p2, "output"    # Lorg/apache/lucene/util/IntsRef;

    .prologue
    .line 94
    sget-boolean v1, Lorg/apache/lucene/util/fst/IntSequenceOutputs;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 95
    :cond_0
    sget-boolean v1, Lorg/apache/lucene/util/fst/IntSequenceOutputs;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    if-nez p2, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 96
    :cond_1
    sget-object v1, Lorg/apache/lucene/util/fst/IntSequenceOutputs;->NO_OUTPUT:Lorg/apache/lucene/util/IntsRef;

    if-ne p1, v1, :cond_2

    .line 107
    .end local p2    # "output":Lorg/apache/lucene/util/IntsRef;
    :goto_0
    return-object p2

    .line 98
    .restart local p2    # "output":Lorg/apache/lucene/util/IntsRef;
    :cond_2
    sget-object v1, Lorg/apache/lucene/util/fst/IntSequenceOutputs;->NO_OUTPUT:Lorg/apache/lucene/util/IntsRef;

    if-ne p2, v1, :cond_3

    move-object p2, p1

    .line 99
    goto :goto_0

    .line 101
    :cond_3
    sget-boolean v1, Lorg/apache/lucene/util/fst/IntSequenceOutputs;->$assertionsDisabled:Z

    if-nez v1, :cond_4

    iget v1, p1, Lorg/apache/lucene/util/IntsRef;->length:I

    if-gtz v1, :cond_4

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 102
    :cond_4
    sget-boolean v1, Lorg/apache/lucene/util/fst/IntSequenceOutputs;->$assertionsDisabled:Z

    if-nez v1, :cond_5

    iget v1, p2, Lorg/apache/lucene/util/IntsRef;->length:I

    if-gtz v1, :cond_5

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 103
    :cond_5
    new-instance v0, Lorg/apache/lucene/util/IntsRef;

    iget v1, p1, Lorg/apache/lucene/util/IntsRef;->length:I

    iget v2, p2, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/2addr v1, v2

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/IntsRef;-><init>(I)V

    .line 104
    .local v0, "result":Lorg/apache/lucene/util/IntsRef;
    iget-object v1, p1, Lorg/apache/lucene/util/IntsRef;->ints:[I

    iget v2, p1, Lorg/apache/lucene/util/IntsRef;->offset:I

    iget-object v3, v0, Lorg/apache/lucene/util/IntsRef;->ints:[I

    const/4 v4, 0x0

    iget v5, p1, Lorg/apache/lucene/util/IntsRef;->length:I

    invoke-static {v1, v2, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 105
    iget-object v1, p2, Lorg/apache/lucene/util/IntsRef;->ints:[I

    iget v2, p2, Lorg/apache/lucene/util/IntsRef;->offset:I

    iget-object v3, v0, Lorg/apache/lucene/util/IntsRef;->ints:[I

    iget v4, p1, Lorg/apache/lucene/util/IntsRef;->length:I

    iget v5, p2, Lorg/apache/lucene/util/IntsRef;->length:I

    invoke-static {v1, v2, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 106
    iget v1, p1, Lorg/apache/lucene/util/IntsRef;->length:I

    iget v2, p2, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/2addr v1, v2

    iput v1, v0, Lorg/apache/lucene/util/IntsRef;->length:I

    move-object p2, v0

    .line 107
    goto :goto_0
.end method

.method public bridge synthetic common(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/util/IntsRef;

    check-cast p2, Lorg/apache/lucene/util/IntsRef;

    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/util/fst/IntSequenceOutputs;->common(Lorg/apache/lucene/util/IntsRef;Lorg/apache/lucene/util/IntsRef;)Lorg/apache/lucene/util/IntsRef;

    move-result-object v0

    return-object v0
.end method

.method public common(Lorg/apache/lucene/util/IntsRef;Lorg/apache/lucene/util/IntsRef;)Lorg/apache/lucene/util/IntsRef;
    .locals 6
    .param p1, "output1"    # Lorg/apache/lucene/util/IntsRef;
    .param p2, "output2"    # Lorg/apache/lucene/util/IntsRef;

    .prologue
    .line 47
    sget-boolean v3, Lorg/apache/lucene/util/fst/IntSequenceOutputs;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    if-nez p1, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 48
    :cond_0
    sget-boolean v3, Lorg/apache/lucene/util/fst/IntSequenceOutputs;->$assertionsDisabled:Z

    if-nez v3, :cond_1

    if-nez p2, :cond_1

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 50
    :cond_1
    iget v0, p1, Lorg/apache/lucene/util/IntsRef;->offset:I

    .line 51
    .local v0, "pos1":I
    iget v1, p2, Lorg/apache/lucene/util/IntsRef;->offset:I

    .line 52
    .local v1, "pos2":I
    iget v3, p1, Lorg/apache/lucene/util/IntsRef;->length:I

    iget v4, p2, Lorg/apache/lucene/util/IntsRef;->length:I

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    add-int v2, v0, v3

    .line 53
    .local v2, "stopAt1":I
    :goto_0
    if-lt v0, v2, :cond_4

    .line 61
    :cond_2
    iget v3, p1, Lorg/apache/lucene/util/IntsRef;->offset:I

    if-ne v0, v3, :cond_5

    .line 63
    sget-object p1, Lorg/apache/lucene/util/fst/IntSequenceOutputs;->NO_OUTPUT:Lorg/apache/lucene/util/IntsRef;

    .line 71
    .end local p1    # "output1":Lorg/apache/lucene/util/IntsRef;
    .end local p2    # "output2":Lorg/apache/lucene/util/IntsRef;
    :cond_3
    :goto_1
    return-object p1

    .line 54
    .restart local p1    # "output1":Lorg/apache/lucene/util/IntsRef;
    .restart local p2    # "output2":Lorg/apache/lucene/util/IntsRef;
    :cond_4
    iget-object v3, p1, Lorg/apache/lucene/util/IntsRef;->ints:[I

    aget v3, v3, v0

    iget-object v4, p2, Lorg/apache/lucene/util/IntsRef;->ints:[I

    aget v4, v4, v1

    if-ne v3, v4, :cond_2

    .line 57
    add-int/lit8 v0, v0, 0x1

    .line 58
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 64
    :cond_5
    iget v3, p1, Lorg/apache/lucene/util/IntsRef;->offset:I

    iget v4, p1, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/2addr v3, v4

    if-eq v0, v3, :cond_3

    .line 67
    iget v3, p2, Lorg/apache/lucene/util/IntsRef;->offset:I

    iget v4, p2, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/2addr v3, v4

    if-ne v1, v3, :cond_6

    move-object p1, p2

    .line 69
    goto :goto_1

    .line 71
    :cond_6
    new-instance p2, Lorg/apache/lucene/util/IntsRef;

    .end local p2    # "output2":Lorg/apache/lucene/util/IntsRef;
    iget-object v3, p1, Lorg/apache/lucene/util/IntsRef;->ints:[I

    iget v4, p1, Lorg/apache/lucene/util/IntsRef;->offset:I

    iget v5, p1, Lorg/apache/lucene/util/IntsRef;->offset:I

    sub-int v5, v0, v5

    invoke-direct {p2, v3, v4, v5}, Lorg/apache/lucene/util/IntsRef;-><init>([III)V

    move-object p1, p2

    goto :goto_1
.end method

.method public bridge synthetic getNoOutput()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/util/fst/IntSequenceOutputs;->getNoOutput()Lorg/apache/lucene/util/IntsRef;

    move-result-object v0

    return-object v0
.end method

.method public getNoOutput()Lorg/apache/lucene/util/IntsRef;
    .locals 1

    .prologue
    .line 137
    sget-object v0, Lorg/apache/lucene/util/fst/IntSequenceOutputs;->NO_OUTPUT:Lorg/apache/lucene/util/IntsRef;

    return-object v0
.end method

.method public bridge synthetic outputToString(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/util/IntsRef;

    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/fst/IntSequenceOutputs;->outputToString(Lorg/apache/lucene/util/IntsRef;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public outputToString(Lorg/apache/lucene/util/IntsRef;)Ljava/lang/String;
    .locals 1
    .param p1, "output"    # Lorg/apache/lucene/util/IntsRef;

    .prologue
    .line 142
    invoke-virtual {p1}, Lorg/apache/lucene/util/IntsRef;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic read(Lorg/apache/lucene/store/DataInput;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/fst/IntSequenceOutputs;->read(Lorg/apache/lucene/store/DataInput;)Lorg/apache/lucene/util/IntsRef;

    move-result-object v0

    return-object v0
.end method

.method public read(Lorg/apache/lucene/store/DataInput;)Lorg/apache/lucene/util/IntsRef;
    .locals 5
    .param p1, "in"    # Lorg/apache/lucene/store/DataInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 122
    invoke-virtual {p1}, Lorg/apache/lucene/store/DataInput;->readVInt()I

    move-result v1

    .line 123
    .local v1, "len":I
    if-nez v1, :cond_0

    .line 124
    sget-object v2, Lorg/apache/lucene/util/fst/IntSequenceOutputs;->NO_OUTPUT:Lorg/apache/lucene/util/IntsRef;

    .line 131
    :goto_0
    return-object v2

    .line 126
    :cond_0
    new-instance v2, Lorg/apache/lucene/util/IntsRef;

    invoke-direct {v2, v1}, Lorg/apache/lucene/util/IntsRef;-><init>(I)V

    .line 127
    .local v2, "output":Lorg/apache/lucene/util/IntsRef;
    const/4 v0, 0x0

    .local v0, "idx":I
    :goto_1
    if-lt v0, v1, :cond_1

    .line 130
    iput v1, v2, Lorg/apache/lucene/util/IntsRef;->length:I

    goto :goto_0

    .line 128
    :cond_1
    iget-object v3, v2, Lorg/apache/lucene/util/IntsRef;->ints:[I

    invoke-virtual {p1}, Lorg/apache/lucene/store/DataInput;->readVInt()I

    move-result v4

    aput v4, v3, v0

    .line 127
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public bridge synthetic subtract(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/util/IntsRef;

    check-cast p2, Lorg/apache/lucene/util/IntsRef;

    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/util/fst/IntSequenceOutputs;->subtract(Lorg/apache/lucene/util/IntsRef;Lorg/apache/lucene/util/IntsRef;)Lorg/apache/lucene/util/IntsRef;

    move-result-object v0

    return-object v0
.end method

.method public subtract(Lorg/apache/lucene/util/IntsRef;Lorg/apache/lucene/util/IntsRef;)Lorg/apache/lucene/util/IntsRef;
    .locals 5
    .param p1, "output"    # Lorg/apache/lucene/util/IntsRef;
    .param p2, "inc"    # Lorg/apache/lucene/util/IntsRef;

    .prologue
    .line 77
    sget-boolean v0, Lorg/apache/lucene/util/fst/IntSequenceOutputs;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 78
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/util/fst/IntSequenceOutputs;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 79
    :cond_1
    sget-object v0, Lorg/apache/lucene/util/fst/IntSequenceOutputs;->NO_OUTPUT:Lorg/apache/lucene/util/IntsRef;

    if-ne p2, v0, :cond_2

    .line 88
    .end local p1    # "output":Lorg/apache/lucene/util/IntsRef;
    :goto_0
    return-object p1

    .line 82
    .restart local p1    # "output":Lorg/apache/lucene/util/IntsRef;
    :cond_2
    iget v0, p2, Lorg/apache/lucene/util/IntsRef;->length:I

    iget v1, p1, Lorg/apache/lucene/util/IntsRef;->length:I

    if-ne v0, v1, :cond_3

    .line 84
    sget-object p1, Lorg/apache/lucene/util/fst/IntSequenceOutputs;->NO_OUTPUT:Lorg/apache/lucene/util/IntsRef;

    goto :goto_0

    .line 86
    :cond_3
    sget-boolean v0, Lorg/apache/lucene/util/fst/IntSequenceOutputs;->$assertionsDisabled:Z

    if-nez v0, :cond_4

    iget v0, p2, Lorg/apache/lucene/util/IntsRef;->length:I

    iget v1, p1, Lorg/apache/lucene/util/IntsRef;->length:I

    if-lt v0, v1, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "inc.length="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p2, Lorg/apache/lucene/util/IntsRef;->length:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " vs output.length="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lorg/apache/lucene/util/IntsRef;->length:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 87
    :cond_4
    sget-boolean v0, Lorg/apache/lucene/util/fst/IntSequenceOutputs;->$assertionsDisabled:Z

    if-nez v0, :cond_5

    iget v0, p2, Lorg/apache/lucene/util/IntsRef;->length:I

    if-gtz v0, :cond_5

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 88
    :cond_5
    new-instance v0, Lorg/apache/lucene/util/IntsRef;

    iget-object v1, p1, Lorg/apache/lucene/util/IntsRef;->ints:[I

    iget v2, p1, Lorg/apache/lucene/util/IntsRef;->offset:I

    iget v3, p2, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/2addr v2, v3

    iget v3, p1, Lorg/apache/lucene/util/IntsRef;->length:I

    iget v4, p2, Lorg/apache/lucene/util/IntsRef;->length:I

    sub-int/2addr v3, v4

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/util/IntsRef;-><init>([III)V

    move-object p1, v0

    goto :goto_0
.end method

.method public bridge synthetic write(Ljava/lang/Object;Lorg/apache/lucene/store/DataOutput;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/util/IntsRef;

    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/util/fst/IntSequenceOutputs;->write(Lorg/apache/lucene/util/IntsRef;Lorg/apache/lucene/store/DataOutput;)V

    return-void
.end method

.method public write(Lorg/apache/lucene/util/IntsRef;Lorg/apache/lucene/store/DataOutput;)V
    .locals 3
    .param p1, "prefix"    # Lorg/apache/lucene/util/IntsRef;
    .param p2, "out"    # Lorg/apache/lucene/store/DataOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 113
    sget-boolean v1, Lorg/apache/lucene/util/fst/IntSequenceOutputs;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 114
    :cond_0
    iget v1, p1, Lorg/apache/lucene/util/IntsRef;->length:I

    invoke-virtual {p2, v1}, Lorg/apache/lucene/store/DataOutput;->writeVInt(I)V

    .line 115
    const/4 v0, 0x0

    .local v0, "idx":I
    :goto_0
    iget v1, p1, Lorg/apache/lucene/util/IntsRef;->length:I

    if-lt v0, v1, :cond_1

    .line 118
    return-void

    .line 116
    :cond_1
    iget-object v1, p1, Lorg/apache/lucene/util/IntsRef;->ints:[I

    iget v2, p1, Lorg/apache/lucene/util/IntsRef;->offset:I

    add-int/2addr v2, v0

    aget v1, v1, v2

    invoke-virtual {p2, v1}, Lorg/apache/lucene/store/DataOutput;->writeVInt(I)V

    .line 115
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
