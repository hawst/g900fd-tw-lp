.class public final Lorg/apache/lucene/util/FixedBitSet;
.super Lorg/apache/lucene/search/DocIdSet;
.source "FixedBitSet.java"

# interfaces
.implements Lorg/apache/lucene/util/Bits;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final bits:[J

.field private final numBits:I

.field private final wordLength:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lorg/apache/lucene/util/FixedBitSet;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/FixedBitSet;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "numBits"    # I

    .prologue
    .line 53
    invoke-direct {p0}, Lorg/apache/lucene/search/DocIdSet;-><init>()V

    .line 54
    iput p1, p0, Lorg/apache/lucene/util/FixedBitSet;->numBits:I

    .line 55
    invoke-static {p1}, Lorg/apache/lucene/util/FixedBitSet;->bits2words(I)I

    move-result v0

    new-array v0, v0, [J

    iput-object v0, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    .line 56
    iget-object v0, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    array-length v0, v0

    iput v0, p0, Lorg/apache/lucene/util/FixedBitSet;->wordLength:I

    .line 57
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/FixedBitSet;)V
    .locals 4
    .param p1, "other"    # Lorg/apache/lucene/util/FixedBitSet;

    .prologue
    const/4 v3, 0x0

    .line 70
    invoke-direct {p0}, Lorg/apache/lucene/search/DocIdSet;-><init>()V

    .line 71
    iget v0, p1, Lorg/apache/lucene/util/FixedBitSet;->wordLength:I

    new-array v0, v0, [J

    iput-object v0, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    .line 72
    iget-object v0, p1, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    iget-object v1, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    iget v2, p1, Lorg/apache/lucene/util/FixedBitSet;->wordLength:I

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 73
    iget v0, p1, Lorg/apache/lucene/util/FixedBitSet;->numBits:I

    iput v0, p0, Lorg/apache/lucene/util/FixedBitSet;->numBits:I

    .line 74
    iget v0, p1, Lorg/apache/lucene/util/FixedBitSet;->wordLength:I

    iput v0, p0, Lorg/apache/lucene/util/FixedBitSet;->wordLength:I

    .line 75
    return-void
.end method

.method public constructor <init>([JI)V
    .locals 3
    .param p1, "storedBits"    # [J
    .param p2, "numBits"    # I

    .prologue
    .line 59
    invoke-direct {p0}, Lorg/apache/lucene/search/DocIdSet;-><init>()V

    .line 60
    invoke-static {p2}, Lorg/apache/lucene/util/FixedBitSet;->bits2words(I)I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/util/FixedBitSet;->wordLength:I

    .line 61
    iget v0, p0, Lorg/apache/lucene/util/FixedBitSet;->wordLength:I

    array-length v1, p1

    if-le v0, v1, :cond_0

    .line 62
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The given long array is too small  to hold "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " bits"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 64
    :cond_0
    iput p2, p0, Lorg/apache/lucene/util/FixedBitSet;->numBits:I

    .line 65
    iput-object p1, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    .line 66
    return-void
.end method

.method private and([JI)V
    .locals 6
    .param p1, "otherArr"    # [J
    .param p2, "otherLen"    # I

    .prologue
    .line 262
    iget-object v1, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    .line 263
    .local v1, "thisArr":[J
    iget v2, p0, Lorg/apache/lucene/util/FixedBitSet;->wordLength:I

    invoke-static {v2, p2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 264
    .local v0, "pos":I
    :goto_0
    add-int/lit8 v0, v0, -0x1

    if-gez v0, :cond_1

    .line 267
    iget v2, p0, Lorg/apache/lucene/util/FixedBitSet;->wordLength:I

    if-le v2, p2, :cond_0

    .line 268
    iget v2, p0, Lorg/apache/lucene/util/FixedBitSet;->wordLength:I

    const-wide/16 v4, 0x0

    invoke-static {v1, p2, v2, v4, v5}, Ljava/util/Arrays;->fill([JIIJ)V

    .line 270
    :cond_0
    return-void

    .line 265
    :cond_1
    aget-wide v2, v1, v0

    aget-wide v4, p1, v0

    and-long/2addr v2, v4

    aput-wide v2, v1, v0

    goto :goto_0
.end method

.method private andNot([JI)V
    .locals 8
    .param p1, "otherArr"    # [J
    .param p2, "otherLen"    # I

    .prologue
    .line 295
    iget-object v1, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    .line 296
    .local v1, "thisArr":[J
    iget v2, p0, Lorg/apache/lucene/util/FixedBitSet;->wordLength:I

    invoke-static {v2, p2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 297
    .local v0, "pos":I
    :goto_0
    add-int/lit8 v0, v0, -0x1

    if-gez v0, :cond_0

    .line 300
    return-void

    .line 298
    :cond_0
    aget-wide v2, v1, v0

    aget-wide v4, p1, v0

    const-wide/16 v6, -0x1

    xor-long/2addr v4, v6

    and-long/2addr v2, v4

    aput-wide v2, v1, v0

    goto :goto_0
.end method

.method public static bits2words(I)I
    .locals 2
    .param p0, "numBits"    # I

    .prologue
    .line 46
    ushr-int/lit8 v0, p0, 0x6

    .line 47
    .local v0, "numLong":I
    and-int/lit8 v1, p0, 0x3f

    if-eqz v1, :cond_0

    .line 48
    add-int/lit8 v0, v0, 0x1

    .line 50
    :cond_0
    return v0
.end method

.method private or([JI)V
    .locals 6
    .param p1, "otherArr"    # [J
    .param p2, "otherLen"    # I

    .prologue
    .line 226
    iget-object v1, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    .line 227
    .local v1, "thisArr":[J
    iget v2, p0, Lorg/apache/lucene/util/FixedBitSet;->wordLength:I

    invoke-static {v2, p2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 228
    .local v0, "pos":I
    :goto_0
    add-int/lit8 v0, v0, -0x1

    if-gez v0, :cond_0

    .line 231
    return-void

    .line 229
    :cond_0
    aget-wide v2, v1, v0

    aget-wide v4, p1, v0

    or-long/2addr v2, v4

    aput-wide v2, v1, v0

    goto :goto_0
.end method


# virtual methods
.method public and(Lorg/apache/lucene/search/DocIdSetIterator;)V
    .locals 5
    .param p1, "iter"    # Lorg/apache/lucene/search/DocIdSetIterator;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, -0x1

    .line 236
    instance-of v4, p1, Lorg/apache/lucene/util/OpenBitSetIterator;

    if-eqz v4, :cond_1

    invoke-virtual {p1}, Lorg/apache/lucene/search/DocIdSetIterator;->docID()I

    move-result v4

    if-ne v4, v3, :cond_1

    move-object v2, p1

    .line 237
    check-cast v2, Lorg/apache/lucene/util/OpenBitSetIterator;

    .line 238
    .local v2, "obs":Lorg/apache/lucene/util/OpenBitSetIterator;
    iget-object v3, v2, Lorg/apache/lucene/util/OpenBitSetIterator;->arr:[J

    iget v4, v2, Lorg/apache/lucene/util/OpenBitSetIterator;->words:I

    invoke-direct {p0, v3, v4}, Lorg/apache/lucene/util/FixedBitSet;->and([JI)V

    .line 241
    iget v3, p0, Lorg/apache/lucene/util/FixedBitSet;->numBits:I

    invoke-virtual {v2, v3}, Lorg/apache/lucene/util/OpenBitSetIterator;->advance(I)I

    .line 254
    .end local v2    # "obs":Lorg/apache/lucene/util/OpenBitSetIterator;
    :cond_0
    :goto_0
    return-void

    .line 243
    :cond_1
    iget v4, p0, Lorg/apache/lucene/util/FixedBitSet;->numBits:I

    if-eqz v4, :cond_0

    .line 244
    const/4 v4, 0x0

    invoke-virtual {p0, v4}, Lorg/apache/lucene/util/FixedBitSet;->nextSetBit(I)I

    move-result v0

    .line 245
    .local v0, "bitSetDoc":I
    :goto_1
    if-eq v0, v3, :cond_2

    invoke-virtual {p1, v0}, Lorg/apache/lucene/search/DocIdSetIterator;->advance(I)I

    move-result v1

    .local v1, "disiDoc":I
    iget v4, p0, Lorg/apache/lucene/util/FixedBitSet;->numBits:I

    if-lt v1, v4, :cond_3

    .line 250
    .end local v1    # "disiDoc":I
    :cond_2
    if-eq v0, v3, :cond_0

    .line 251
    iget v3, p0, Lorg/apache/lucene/util/FixedBitSet;->numBits:I

    invoke-virtual {p0, v0, v3}, Lorg/apache/lucene/util/FixedBitSet;->clear(II)V

    goto :goto_0

    .line 246
    .restart local v1    # "disiDoc":I
    :cond_3
    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/util/FixedBitSet;->clear(II)V

    .line 247
    add-int/lit8 v1, v1, 0x1

    .line 248
    iget v4, p0, Lorg/apache/lucene/util/FixedBitSet;->numBits:I

    if-ge v1, v4, :cond_4

    invoke-virtual {p0, v1}, Lorg/apache/lucene/util/FixedBitSet;->nextSetBit(I)I

    move-result v0

    :goto_2
    goto :goto_1

    :cond_4
    move v0, v3

    goto :goto_2
.end method

.method public and(Lorg/apache/lucene/util/FixedBitSet;)V
    .locals 2
    .param p1, "other"    # Lorg/apache/lucene/util/FixedBitSet;

    .prologue
    .line 258
    iget-object v0, p1, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    iget v1, p1, Lorg/apache/lucene/util/FixedBitSet;->wordLength:I

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/util/FixedBitSet;->and([JI)V

    .line 259
    return-void
.end method

.method public andNot(Lorg/apache/lucene/search/DocIdSetIterator;)V
    .locals 4
    .param p1, "iter"    # Lorg/apache/lucene/search/DocIdSetIterator;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 275
    instance-of v2, p1, Lorg/apache/lucene/util/OpenBitSetIterator;

    if-eqz v2, :cond_1

    invoke-virtual {p1}, Lorg/apache/lucene/search/DocIdSetIterator;->docID()I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_1

    move-object v1, p1

    .line 276
    check-cast v1, Lorg/apache/lucene/util/OpenBitSetIterator;

    .line 277
    .local v1, "obs":Lorg/apache/lucene/util/OpenBitSetIterator;
    iget-object v2, v1, Lorg/apache/lucene/util/OpenBitSetIterator;->arr:[J

    iget v3, v1, Lorg/apache/lucene/util/OpenBitSetIterator;->words:I

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/util/FixedBitSet;->andNot([JI)V

    .line 280
    iget v2, p0, Lorg/apache/lucene/util/FixedBitSet;->numBits:I

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/OpenBitSetIterator;->advance(I)I

    .line 287
    .end local v1    # "obs":Lorg/apache/lucene/util/OpenBitSetIterator;
    :goto_0
    return-void

    .line 284
    .local v0, "doc":I
    :cond_0
    invoke-virtual {p0, v0}, Lorg/apache/lucene/util/FixedBitSet;->clear(I)V

    .line 283
    .end local v0    # "doc":I
    :cond_1
    invoke-virtual {p1}, Lorg/apache/lucene/search/DocIdSetIterator;->nextDoc()I

    move-result v0

    .restart local v0    # "doc":I
    iget v2, p0, Lorg/apache/lucene/util/FixedBitSet;->numBits:I

    if-lt v0, v2, :cond_0

    goto :goto_0
.end method

.method public andNot(Lorg/apache/lucene/util/FixedBitSet;)V
    .locals 2
    .param p1, "other"    # Lorg/apache/lucene/util/FixedBitSet;

    .prologue
    .line 291
    iget-object v0, p1, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    iget-object v1, p1, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    array-length v1, v1

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/util/FixedBitSet;->andNot([JI)V

    .line 292
    return-void
.end method

.method public bits()Lorg/apache/lucene/util/Bits;
    .locals 0

    .prologue
    .line 84
    return-object p0
.end method

.method public cardinality()I
    .locals 3

    .prologue
    .line 107
    iget-object v0, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    const/4 v1, 0x0

    iget-object v2, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    array-length v2, v2

    invoke-static {v0, v1, v2}, Lorg/apache/lucene/util/BitUtil;->pop_array([JII)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public clear(I)V
    .locals 10
    .param p1, "index"    # I

    .prologue
    .line 140
    sget-boolean v4, Lorg/apache/lucene/util/FixedBitSet;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    if-ltz p1, :cond_0

    iget v4, p0, Lorg/apache/lucene/util/FixedBitSet;->numBits:I

    if-lt p1, v4, :cond_1

    :cond_0
    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 141
    :cond_1
    shr-int/lit8 v1, p1, 0x6

    .line 142
    .local v1, "wordNum":I
    and-int/lit8 v0, p1, 0x3f

    .line 143
    .local v0, "bit":I
    const-wide/16 v4, 0x1

    shl-long v2, v4, v0

    .line 144
    .local v2, "bitmask":J
    iget-object v4, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    aget-wide v6, v4, v1

    const-wide/16 v8, -0x1

    xor-long/2addr v8, v2

    and-long/2addr v6, v8

    aput-wide v6, v4, v1

    .line 145
    return-void
.end method

.method public clear(II)V
    .locals 12
    .param p1, "startIndex"    # I
    .param p2, "endIndex"    # I

    .prologue
    const-wide/16 v8, -0x1

    .line 379
    sget-boolean v6, Lorg/apache/lucene/util/FixedBitSet;->$assertionsDisabled:Z

    if-nez v6, :cond_1

    if-ltz p1, :cond_0

    iget v6, p0, Lorg/apache/lucene/util/FixedBitSet;->numBits:I

    if-lt p1, v6, :cond_1

    :cond_0
    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 380
    :cond_1
    sget-boolean v6, Lorg/apache/lucene/util/FixedBitSet;->$assertionsDisabled:Z

    if-nez v6, :cond_3

    if-ltz p2, :cond_2

    iget v6, p0, Lorg/apache/lucene/util/FixedBitSet;->numBits:I

    if-le p2, v6, :cond_3

    :cond_2
    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 381
    :cond_3
    if-gt p2, p1, :cond_4

    .line 403
    :goto_0
    return-void

    .line 385
    :cond_4
    shr-int/lit8 v1, p1, 0x6

    .line 386
    .local v1, "startWord":I
    add-int/lit8 v6, p2, -0x1

    shr-int/lit8 v0, v6, 0x6

    .line 388
    .local v0, "endWord":I
    shl-long v4, v8, p1

    .line 389
    .local v4, "startmask":J
    neg-int v6, p2

    ushr-long v2, v8, v6

    .line 392
    .local v2, "endmask":J
    xor-long/2addr v4, v8

    .line 393
    xor-long/2addr v2, v8

    .line 395
    if-ne v1, v0, :cond_5

    .line 396
    iget-object v6, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    aget-wide v8, v6, v1

    or-long v10, v4, v2

    and-long/2addr v8, v10

    aput-wide v8, v6, v1

    goto :goto_0

    .line 400
    :cond_5
    iget-object v6, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    aget-wide v8, v6, v1

    and-long/2addr v8, v4

    aput-wide v8, v6, v1

    .line 401
    iget-object v6, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    add-int/lit8 v7, v1, 0x1

    const-wide/16 v8, 0x0

    invoke-static {v6, v7, v0, v8, v9}, Ljava/util/Arrays;->fill([JIIJ)V

    .line 402
    iget-object v6, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    aget-wide v8, v6, v0

    and-long/2addr v8, v2

    aput-wide v8, v6, v0

    goto :goto_0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/util/FixedBitSet;->clone()Lorg/apache/lucene/util/FixedBitSet;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lorg/apache/lucene/util/FixedBitSet;
    .locals 1

    .prologue
    .line 407
    new-instance v0, Lorg/apache/lucene/util/FixedBitSet;

    invoke-direct {v0, p0}, Lorg/apache/lucene/util/FixedBitSet;-><init>(Lorg/apache/lucene/util/FixedBitSet;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 413
    if-ne p0, p1, :cond_1

    .line 414
    const/4 v1, 0x1

    .line 423
    :cond_0
    :goto_0
    return v1

    .line 416
    :cond_1
    instance-of v2, p1, Lorg/apache/lucene/util/FixedBitSet;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 419
    check-cast v0, Lorg/apache/lucene/util/FixedBitSet;

    .line 420
    .local v0, "other":Lorg/apache/lucene/util/FixedBitSet;
    iget v2, p0, Lorg/apache/lucene/util/FixedBitSet;->numBits:I

    invoke-virtual {v0}, Lorg/apache/lucene/util/FixedBitSet;->length()I

    move-result v3

    if-ne v2, v3, :cond_0

    .line 423
    iget-object v1, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    iget-object v2, v0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([J[J)Z

    move-result v1

    goto :goto_0
.end method

.method public flip(II)V
    .locals 12
    .param p1, "startIndex"    # I
    .param p2, "endIndex"    # I

    .prologue
    const-wide/16 v10, -0x1

    .line 312
    sget-boolean v5, Lorg/apache/lucene/util/FixedBitSet;->$assertionsDisabled:Z

    if-nez v5, :cond_1

    if-ltz p1, :cond_0

    iget v5, p0, Lorg/apache/lucene/util/FixedBitSet;->numBits:I

    if-lt p1, v5, :cond_1

    :cond_0
    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 313
    :cond_1
    sget-boolean v5, Lorg/apache/lucene/util/FixedBitSet;->$assertionsDisabled:Z

    if-nez v5, :cond_3

    if-ltz p2, :cond_2

    iget v5, p0, Lorg/apache/lucene/util/FixedBitSet;->numBits:I

    if-le p2, v5, :cond_3

    :cond_2
    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 314
    :cond_3
    if-gt p2, p1, :cond_4

    .line 343
    :goto_0
    return-void

    .line 318
    :cond_4
    shr-int/lit8 v4, p1, 0x6

    .line 319
    .local v4, "startWord":I
    add-int/lit8 v5, p2, -0x1

    shr-int/lit8 v0, v5, 0x6

    .line 328
    .local v0, "endWord":I
    shl-long v6, v10, p1

    .line 329
    .local v6, "startmask":J
    neg-int v5, p2

    ushr-long v2, v10, v5

    .line 331
    .local v2, "endmask":J
    if-ne v4, v0, :cond_5

    .line 332
    iget-object v5, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    aget-wide v8, v5, v4

    and-long v10, v6, v2

    xor-long/2addr v8, v10

    aput-wide v8, v5, v4

    goto :goto_0

    .line 336
    :cond_5
    iget-object v5, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    aget-wide v8, v5, v4

    xor-long/2addr v8, v6

    aput-wide v8, v5, v4

    .line 338
    add-int/lit8 v1, v4, 0x1

    .local v1, "i":I
    :goto_1
    if-lt v1, v0, :cond_6

    .line 342
    iget-object v5, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    aget-wide v8, v5, v0

    xor-long/2addr v8, v2

    aput-wide v8, v5, v0

    goto :goto_0

    .line 339
    :cond_6
    iget-object v5, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    iget-object v8, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    aget-wide v8, v8, v1

    xor-long/2addr v8, v10

    aput-wide v8, v5, v1

    .line 338
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public get(I)Z
    .locals 8
    .param p1, "index"    # I

    .prologue
    .line 112
    sget-boolean v4, Lorg/apache/lucene/util/FixedBitSet;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    if-ltz p1, :cond_0

    iget v4, p0, Lorg/apache/lucene/util/FixedBitSet;->numBits:I

    if-lt p1, v4, :cond_1

    :cond_0
    new-instance v4, Ljava/lang/AssertionError;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "index="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    .line 113
    :cond_1
    shr-int/lit8 v1, p1, 0x6

    .line 116
    .local v1, "i":I
    and-int/lit8 v0, p1, 0x3f

    .line 117
    .local v0, "bit":I
    const-wide/16 v4, 0x1

    shl-long v2, v4, v0

    .line 118
    .local v2, "bitmask":J
    iget-object v4, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    aget-wide v4, v4, v1

    and-long/2addr v4, v2

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_2

    const/4 v4, 0x1

    :goto_0
    return v4

    :cond_2
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public getAndClear(I)Z
    .locals 10
    .param p1, "index"    # I

    .prologue
    .line 148
    sget-boolean v5, Lorg/apache/lucene/util/FixedBitSet;->$assertionsDisabled:Z

    if-nez v5, :cond_1

    if-ltz p1, :cond_0

    iget v5, p0, Lorg/apache/lucene/util/FixedBitSet;->numBits:I

    if-lt p1, v5, :cond_1

    :cond_0
    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 149
    :cond_1
    shr-int/lit8 v4, p1, 0x6

    .line 150
    .local v4, "wordNum":I
    and-int/lit8 v0, p1, 0x3f

    .line 151
    .local v0, "bit":I
    const-wide/16 v6, 0x1

    shl-long v2, v6, v0

    .line 152
    .local v2, "bitmask":J
    iget-object v5, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    aget-wide v6, v5, v4

    and-long/2addr v6, v2

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-eqz v5, :cond_2

    const/4 v1, 0x1

    .line 153
    .local v1, "val":Z
    :goto_0
    iget-object v5, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    aget-wide v6, v5, v4

    const-wide/16 v8, -0x1

    xor-long/2addr v8, v2

    and-long/2addr v6, v8

    aput-wide v6, v5, v4

    .line 154
    return v1

    .line 152
    .end local v1    # "val":Z
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getAndSet(I)Z
    .locals 10
    .param p1, "index"    # I

    .prologue
    .line 130
    sget-boolean v5, Lorg/apache/lucene/util/FixedBitSet;->$assertionsDisabled:Z

    if-nez v5, :cond_1

    if-ltz p1, :cond_0

    iget v5, p0, Lorg/apache/lucene/util/FixedBitSet;->numBits:I

    if-lt p1, v5, :cond_1

    :cond_0
    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 131
    :cond_1
    shr-int/lit8 v4, p1, 0x6

    .line 132
    .local v4, "wordNum":I
    and-int/lit8 v0, p1, 0x3f

    .line 133
    .local v0, "bit":I
    const-wide/16 v6, 0x1

    shl-long v2, v6, v0

    .line 134
    .local v2, "bitmask":J
    iget-object v5, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    aget-wide v6, v5, v4

    and-long/2addr v6, v2

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-eqz v5, :cond_2

    const/4 v1, 0x1

    .line 135
    .local v1, "val":Z
    :goto_0
    iget-object v5, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    aget-wide v6, v5, v4

    or-long/2addr v6, v2

    aput-wide v6, v5, v4

    .line 136
    return v1

    .line 134
    .end local v1    # "val":Z
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getBits()[J
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    return-object v0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    .line 428
    const-wide/16 v0, 0x0

    .line 429
    .local v0, "h":J
    iget v2, p0, Lorg/apache/lucene/util/FixedBitSet;->wordLength:I

    .local v2, "i":I
    :goto_0
    add-int/lit8 v2, v2, -0x1

    if-gez v2, :cond_0

    .line 435
    const/16 v3, 0x20

    shr-long v4, v0, v3

    xor-long/2addr v4, v0

    long-to-int v3, v4

    const v4, -0x6789edcc

    add-int/2addr v3, v4

    return v3

    .line 430
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    aget-wide v4, v3, v2

    xor-long/2addr v0, v4

    .line 431
    const/4 v3, 0x1

    shl-long v4, v0, v3

    const/16 v3, 0x3f

    ushr-long v6, v0, v3

    or-long v0, v4, v6

    goto :goto_0
.end method

.method public isCacheable()Z
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x1

    return v0
.end method

.method public iterator()Lorg/apache/lucene/search/DocIdSetIterator;
    .locals 3

    .prologue
    .line 79
    new-instance v0, Lorg/apache/lucene/util/OpenBitSetIterator;

    iget-object v1, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    iget v2, p0, Lorg/apache/lucene/util/FixedBitSet;->wordLength:I

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/util/OpenBitSetIterator;-><init>([JI)V

    return-object v0
.end method

.method public length()I
    .locals 1

    .prologue
    .line 89
    iget v0, p0, Lorg/apache/lucene/util/FixedBitSet;->numBits:I

    return v0
.end method

.method public nextSetBit(I)I
    .locals 8
    .param p1, "index"    # I

    .prologue
    const-wide/16 v6, 0x0

    .line 161
    sget-boolean v4, Lorg/apache/lucene/util/FixedBitSet;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    if-ltz p1, :cond_0

    iget v4, p0, Lorg/apache/lucene/util/FixedBitSet;->numBits:I

    if-lt p1, v4, :cond_1

    :cond_0
    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 162
    :cond_1
    shr-int/lit8 v0, p1, 0x6

    .line 163
    .local v0, "i":I
    and-int/lit8 v1, p1, 0x3f

    .line 164
    .local v1, "subIndex":I
    iget-object v4, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    aget-wide v4, v4, v0

    shr-long v2, v4, v1

    .line 166
    .local v2, "word":J
    cmp-long v4, v2, v6

    if-eqz v4, :cond_3

    .line 167
    shl-int/lit8 v4, v0, 0x6

    add-int/2addr v4, v1

    invoke-static {v2, v3}, Ljava/lang/Long;->numberOfTrailingZeros(J)I

    move-result v5

    add-int/2addr v4, v5

    .line 177
    :goto_0
    return v4

    .line 171
    :cond_2
    iget-object v4, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    aget-wide v2, v4, v0

    .line 172
    cmp-long v4, v2, v6

    if-eqz v4, :cond_3

    .line 173
    shl-int/lit8 v4, v0, 0x6

    invoke-static {v2, v3}, Ljava/lang/Long;->numberOfTrailingZeros(J)I

    move-result v5

    add-int/2addr v4, v5

    goto :goto_0

    .line 170
    :cond_3
    add-int/lit8 v0, v0, 0x1

    iget v4, p0, Lorg/apache/lucene/util/FixedBitSet;->wordLength:I

    if-lt v0, v4, :cond_2

    .line 177
    const/4 v4, -0x1

    goto :goto_0
.end method

.method public or(Lorg/apache/lucene/search/DocIdSetIterator;)V
    .locals 4
    .param p1, "iter"    # Lorg/apache/lucene/search/DocIdSetIterator;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 206
    instance-of v2, p1, Lorg/apache/lucene/util/OpenBitSetIterator;

    if-eqz v2, :cond_1

    invoke-virtual {p1}, Lorg/apache/lucene/search/DocIdSetIterator;->docID()I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_1

    move-object v1, p1

    .line 207
    check-cast v1, Lorg/apache/lucene/util/OpenBitSetIterator;

    .line 208
    .local v1, "obs":Lorg/apache/lucene/util/OpenBitSetIterator;
    iget-object v2, v1, Lorg/apache/lucene/util/OpenBitSetIterator;->arr:[J

    iget v3, v1, Lorg/apache/lucene/util/OpenBitSetIterator;->words:I

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/util/FixedBitSet;->or([JI)V

    .line 211
    iget v2, p0, Lorg/apache/lucene/util/FixedBitSet;->numBits:I

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/OpenBitSetIterator;->advance(I)I

    .line 218
    .end local v1    # "obs":Lorg/apache/lucene/util/OpenBitSetIterator;
    :goto_0
    return-void

    .line 215
    .local v0, "doc":I
    :cond_0
    invoke-virtual {p0, v0}, Lorg/apache/lucene/util/FixedBitSet;->set(I)V

    .line 214
    .end local v0    # "doc":I
    :cond_1
    invoke-virtual {p1}, Lorg/apache/lucene/search/DocIdSetIterator;->nextDoc()I

    move-result v0

    .restart local v0    # "doc":I
    iget v2, p0, Lorg/apache/lucene/util/FixedBitSet;->numBits:I

    if-lt v0, v2, :cond_0

    goto :goto_0
.end method

.method public or(Lorg/apache/lucene/util/FixedBitSet;)V
    .locals 2
    .param p1, "other"    # Lorg/apache/lucene/util/FixedBitSet;

    .prologue
    .line 222
    iget-object v0, p1, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    iget v1, p1, Lorg/apache/lucene/util/FixedBitSet;->wordLength:I

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/util/FixedBitSet;->or([JI)V

    .line 223
    return-void
.end method

.method public prevSetBit(I)I
    .locals 10
    .param p1, "index"    # I

    .prologue
    const-wide/16 v8, 0x0

    .line 184
    sget-boolean v4, Lorg/apache/lucene/util/FixedBitSet;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    if-ltz p1, :cond_0

    iget v4, p0, Lorg/apache/lucene/util/FixedBitSet;->numBits:I

    if-lt p1, v4, :cond_1

    :cond_0
    new-instance v4, Ljava/lang/AssertionError;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "index="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " numBits="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lorg/apache/lucene/util/FixedBitSet;->numBits:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    .line 185
    :cond_1
    shr-int/lit8 v0, p1, 0x6

    .line 186
    .local v0, "i":I
    and-int/lit8 v1, p1, 0x3f

    .line 187
    .local v1, "subIndex":I
    iget-object v4, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    aget-wide v4, v4, v0

    rsub-int/lit8 v6, v1, 0x3f

    shl-long v2, v4, v6

    .line 189
    .local v2, "word":J
    cmp-long v4, v2, v8

    if-eqz v4, :cond_3

    .line 190
    shl-int/lit8 v4, v0, 0x6

    add-int/2addr v4, v1

    invoke-static {v2, v3}, Ljava/lang/Long;->numberOfLeadingZeros(J)I

    move-result v5

    sub-int/2addr v4, v5

    .line 200
    :goto_0
    return v4

    .line 194
    :cond_2
    iget-object v4, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    aget-wide v2, v4, v0

    .line 195
    cmp-long v4, v2, v8

    if-eqz v4, :cond_3

    .line 196
    shl-int/lit8 v4, v0, 0x6

    add-int/lit8 v4, v4, 0x3f

    invoke-static {v2, v3}, Ljava/lang/Long;->numberOfLeadingZeros(J)I

    move-result v5

    sub-int/2addr v4, v5

    goto :goto_0

    .line 193
    :cond_3
    add-int/lit8 v0, v0, -0x1

    if-gez v0, :cond_2

    .line 200
    const/4 v4, -0x1

    goto :goto_0
.end method

.method public set(I)V
    .locals 8
    .param p1, "index"    # I

    .prologue
    .line 122
    sget-boolean v4, Lorg/apache/lucene/util/FixedBitSet;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    if-ltz p1, :cond_0

    iget v4, p0, Lorg/apache/lucene/util/FixedBitSet;->numBits:I

    if-lt p1, v4, :cond_1

    :cond_0
    new-instance v4, Ljava/lang/AssertionError;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "index="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " numBits="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lorg/apache/lucene/util/FixedBitSet;->numBits:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    .line 123
    :cond_1
    shr-int/lit8 v1, p1, 0x6

    .line 124
    .local v1, "wordNum":I
    and-int/lit8 v0, p1, 0x3f

    .line 125
    .local v0, "bit":I
    const-wide/16 v4, 0x1

    shl-long v2, v4, v0

    .line 126
    .local v2, "bitmask":J
    iget-object v4, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    aget-wide v6, v4, v1

    or-long/2addr v6, v2

    aput-wide v6, v4, v1

    .line 127
    return-void
.end method

.method public set(II)V
    .locals 12
    .param p1, "startIndex"    # I
    .param p2, "endIndex"    # I

    .prologue
    const-wide/16 v10, -0x1

    .line 351
    sget-boolean v6, Lorg/apache/lucene/util/FixedBitSet;->$assertionsDisabled:Z

    if-nez v6, :cond_1

    if-ltz p1, :cond_0

    iget v6, p0, Lorg/apache/lucene/util/FixedBitSet;->numBits:I

    if-lt p1, v6, :cond_1

    :cond_0
    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 352
    :cond_1
    sget-boolean v6, Lorg/apache/lucene/util/FixedBitSet;->$assertionsDisabled:Z

    if-nez v6, :cond_3

    if-ltz p2, :cond_2

    iget v6, p0, Lorg/apache/lucene/util/FixedBitSet;->numBits:I

    if-le p2, v6, :cond_3

    :cond_2
    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 353
    :cond_3
    if-gt p2, p1, :cond_4

    .line 371
    :goto_0
    return-void

    .line 357
    :cond_4
    shr-int/lit8 v1, p1, 0x6

    .line 358
    .local v1, "startWord":I
    add-int/lit8 v6, p2, -0x1

    shr-int/lit8 v0, v6, 0x6

    .line 360
    .local v0, "endWord":I
    shl-long v4, v10, p1

    .line 361
    .local v4, "startmask":J
    neg-int v6, p2

    ushr-long v2, v10, v6

    .line 363
    .local v2, "endmask":J
    if-ne v1, v0, :cond_5

    .line 364
    iget-object v6, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    aget-wide v8, v6, v1

    and-long v10, v4, v2

    or-long/2addr v8, v10

    aput-wide v8, v6, v1

    goto :goto_0

    .line 368
    :cond_5
    iget-object v6, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    aget-wide v8, v6, v1

    or-long/2addr v8, v4

    aput-wide v8, v6, v1

    .line 369
    iget-object v6, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    add-int/lit8 v7, v1, 0x1

    invoke-static {v6, v7, v0, v10, v11}, Ljava/util/Arrays;->fill([JIIJ)V

    .line 370
    iget-object v6, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    aget-wide v8, v6, v0

    or-long/2addr v8, v2

    aput-wide v8, v6, v0

    goto :goto_0
.end method
