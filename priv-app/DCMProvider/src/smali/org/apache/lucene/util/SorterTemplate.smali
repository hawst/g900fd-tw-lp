.class public abstract Lorg/apache/lucene/util/SorterTemplate;
.super Ljava/lang/Object;
.source "SorterTemplate.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/SorterTemplate$TimSort;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final MERGESORT_THRESHOLD:I = 0xc

.field private static final QUICKSORT_THRESHOLD:I = 0x7

.field private static final TIMSORT_MINRUN:I = 0x20

.field private static final TIMSORT_STACKSIZE:I = 0x28

.field private static final TIMSORT_THRESHOLD:I = 0x40


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .prologue
    const-wide/16 v10, 0x1

    const/16 v8, 0x28

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 31
    const-class v2, Lorg/apache/lucene/util/SorterTemplate;

    invoke-virtual {v2}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v2

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    sput-boolean v2, Lorg/apache/lucene/util/SorterTemplate;->$assertionsDisabled:Z

    .line 43
    new-array v1, v8, [J

    .line 44
    .local v1, "lengths":[J
    const-wide/16 v6, 0x20

    aput-wide v6, v1, v4

    .line 45
    aget-wide v4, v1, v4

    add-long/2addr v4, v10

    aput-wide v4, v1, v3

    .line 46
    const/4 v0, 0x2

    .local v0, "i":I
    :goto_1
    if-lt v0, v8, :cond_1

    .line 49
    const/16 v2, 0x27

    aget-wide v2, v1, v2

    const-wide/32 v4, 0x7fffffff

    cmp-long v2, v2, v4

    if-gez v2, :cond_2

    .line 50
    new-instance v2, Ljava/lang/Error;

    const-string v3, "TIMSORT_STACKSIZE is too small"

    invoke-direct {v2, v3}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    throw v2

    .end local v0    # "i":I
    .end local v1    # "lengths":[J
    :cond_0
    move v2, v4

    .line 31
    goto :goto_0

    .line 47
    .restart local v0    # "i":I
    .restart local v1    # "lengths":[J
    :cond_1
    add-int/lit8 v2, v0, -0x2

    aget-wide v2, v1, v2

    add-int/lit8 v4, v0, -0x1

    aget-wide v4, v1, v4

    add-long/2addr v2, v4

    add-long/2addr v2, v10

    aput-wide v2, v1, v0

    .line 46
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 52
    :cond_2
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/util/SorterTemplate;IIIII)V
    .locals 0

    .prologue
    .line 348
    invoke-direct/range {p0 .. p5}, Lorg/apache/lucene/util/SorterTemplate;->runMerge(IIIII)V

    return-void
.end method

.method private lower(III)I
    .locals 4
    .param p1, "lo"    # I
    .param p2, "hi"    # I
    .param p3, "val"    # I

    .prologue
    .line 416
    sub-int v1, p2, p1

    .line 417
    .local v1, "len":I
    :goto_0
    if-gtz v1, :cond_0

    .line 427
    return p1

    .line 418
    :cond_0
    ushr-int/lit8 v0, v1, 0x1

    .line 419
    .local v0, "half":I
    add-int v2, p1, v0

    .line 420
    .local v2, "mid":I
    invoke-virtual {p0, v2, p3}, Lorg/apache/lucene/util/SorterTemplate;->compare(II)I

    move-result v3

    if-gez v3, :cond_1

    .line 421
    add-int/lit8 p1, v2, 0x1

    .line 422
    sub-int v3, v1, v0

    add-int/lit8 v1, v3, -0x1

    .line 423
    goto :goto_0

    .line 424
    :cond_1
    move v1, v0

    goto :goto_0
.end method

.method private quickSort(III)V
    .locals 5
    .param p1, "lo"    # I
    .param p2, "hi"    # I
    .param p3, "maxDepth"    # I

    .prologue
    .line 114
    sub-int v0, p2, p1

    .line 115
    .local v0, "diff":I
    const/4 v4, 0x7

    if-gt v0, v4, :cond_0

    .line 116
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/util/SorterTemplate;->insertionSort(II)V

    .line 160
    :goto_0
    return-void

    .line 121
    :cond_0
    add-int/lit8 p3, p3, -0x1

    if-nez p3, :cond_1

    .line 122
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/util/SorterTemplate;->mergeSort(II)V

    goto :goto_0

    .line 126
    :cond_1
    ushr-int/lit8 v4, v0, 0x1

    add-int v2, p1, v4

    .line 128
    .local v2, "mid":I
    invoke-virtual {p0, p1, v2}, Lorg/apache/lucene/util/SorterTemplate;->compare(II)I

    move-result v4

    if-lez v4, :cond_2

    .line 129
    invoke-virtual {p0, p1, v2}, Lorg/apache/lucene/util/SorterTemplate;->swap(II)V

    .line 132
    :cond_2
    invoke-virtual {p0, v2, p2}, Lorg/apache/lucene/util/SorterTemplate;->compare(II)I

    move-result v4

    if-lez v4, :cond_3

    .line 133
    invoke-virtual {p0, v2, p2}, Lorg/apache/lucene/util/SorterTemplate;->swap(II)V

    .line 134
    invoke-virtual {p0, p1, v2}, Lorg/apache/lucene/util/SorterTemplate;->compare(II)I

    move-result v4

    if-lez v4, :cond_3

    .line 135
    invoke-virtual {p0, p1, v2}, Lorg/apache/lucene/util/SorterTemplate;->swap(II)V

    .line 139
    :cond_3
    add-int/lit8 v1, p1, 0x1

    .line 140
    .local v1, "left":I
    add-int/lit8 v3, p2, -0x1

    .line 142
    .local v3, "right":I
    invoke-virtual {p0, v2}, Lorg/apache/lucene/util/SorterTemplate;->setPivot(I)V

    .line 144
    :goto_1
    invoke-virtual {p0, v3}, Lorg/apache/lucene/util/SorterTemplate;->comparePivot(I)I

    move-result v4

    if-ltz v4, :cond_5

    .line 147
    :goto_2
    if-ge v1, v3, :cond_4

    invoke-virtual {p0, v1}, Lorg/apache/lucene/util/SorterTemplate;->comparePivot(I)I

    move-result v4

    if-gez v4, :cond_6

    .line 150
    :cond_4
    if-ge v1, v3, :cond_7

    .line 151
    invoke-virtual {p0, v1, v3}, Lorg/apache/lucene/util/SorterTemplate;->swap(II)V

    .line 152
    add-int/lit8 v3, v3, -0x1

    .line 143
    goto :goto_1

    .line 145
    :cond_5
    add-int/lit8 v3, v3, -0x1

    goto :goto_1

    .line 148
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 158
    :cond_7
    invoke-direct {p0, p1, v1, p3}, Lorg/apache/lucene/util/SorterTemplate;->quickSort(III)V

    .line 159
    add-int/lit8 v4, v1, 0x1

    invoke-direct {p0, v4, p2, p3}, Lorg/apache/lucene/util/SorterTemplate;->quickSort(III)V

    goto :goto_0
.end method

.method private rotate(III)V
    .locals 4
    .param p1, "lo"    # I
    .param p2, "mid"    # I
    .param p3, "hi"    # I

    .prologue
    .line 400
    move v2, p1

    .line 401
    .local v2, "lot":I
    add-int/lit8 v0, p2, -0x1

    .local v0, "hit":I
    move v1, v0

    .end local v0    # "hit":I
    .local v1, "hit":I
    move v3, v2

    .line 402
    .end local v2    # "lot":I
    .local v3, "lot":I
    :goto_0
    if-lt v3, v1, :cond_0

    .line 405
    move v2, p2

    .end local v3    # "lot":I
    .restart local v2    # "lot":I
    add-int/lit8 v0, p3, -0x1

    .end local v1    # "hit":I
    .restart local v0    # "hit":I
    move v1, v0

    .end local v0    # "hit":I
    .restart local v1    # "hit":I
    move v3, v2

    .line 406
    .end local v2    # "lot":I
    .restart local v3    # "lot":I
    :goto_1
    if-lt v3, v1, :cond_1

    .line 409
    move v2, p1

    .end local v3    # "lot":I
    .restart local v2    # "lot":I
    add-int/lit8 v0, p3, -0x1

    .end local v1    # "hit":I
    .restart local v0    # "hit":I
    move v1, v0

    .end local v0    # "hit":I
    .restart local v1    # "hit":I
    move v3, v2

    .line 410
    .end local v2    # "lot":I
    .restart local v3    # "lot":I
    :goto_2
    if-lt v3, v1, :cond_2

    .line 413
    return-void

    .line 403
    :cond_0
    add-int/lit8 v2, v3, 0x1

    .end local v3    # "lot":I
    .restart local v2    # "lot":I
    add-int/lit8 v0, v1, -0x1

    .end local v1    # "hit":I
    .restart local v0    # "hit":I
    invoke-virtual {p0, v3, v1}, Lorg/apache/lucene/util/SorterTemplate;->swap(II)V

    move v1, v0

    .end local v0    # "hit":I
    .restart local v1    # "hit":I
    move v3, v2

    .end local v2    # "lot":I
    .restart local v3    # "lot":I
    goto :goto_0

    .line 407
    :cond_1
    add-int/lit8 v2, v3, 0x1

    .end local v3    # "lot":I
    .restart local v2    # "lot":I
    add-int/lit8 v0, v1, -0x1

    .end local v1    # "hit":I
    .restart local v0    # "hit":I
    invoke-virtual {p0, v3, v1}, Lorg/apache/lucene/util/SorterTemplate;->swap(II)V

    move v1, v0

    .end local v0    # "hit":I
    .restart local v1    # "hit":I
    move v3, v2

    .end local v2    # "lot":I
    .restart local v3    # "lot":I
    goto :goto_1

    .line 411
    :cond_2
    add-int/lit8 v2, v3, 0x1

    .end local v3    # "lot":I
    .restart local v2    # "lot":I
    add-int/lit8 v0, v1, -0x1

    .end local v1    # "hit":I
    .restart local v0    # "hit":I
    invoke-virtual {p0, v3, v1}, Lorg/apache/lucene/util/SorterTemplate;->swap(II)V

    move v1, v0

    .end local v0    # "hit":I
    .restart local v1    # "hit":I
    move v3, v2

    .end local v2    # "lot":I
    .restart local v3    # "lot":I
    goto :goto_2
.end method

.method private runMerge(IIIII)V
    .locals 2
    .param p1, "lo"    # I
    .param p2, "pivot"    # I
    .param p3, "hi"    # I
    .param p4, "len1"    # I
    .param p5, "len2"    # I

    .prologue
    .line 349
    if-eqz p4, :cond_0

    if-nez p5, :cond_1

    .line 374
    :cond_0
    :goto_0
    return-void

    .line 352
    :cond_1
    add-int/lit8 v0, p2, -0x1

    invoke-virtual {p0, v0}, Lorg/apache/lucene/util/SorterTemplate;->setPivot(I)V

    .line 353
    invoke-virtual {p0, p2}, Lorg/apache/lucene/util/SorterTemplate;->comparePivot(I)I

    move-result v0

    if-lez v0, :cond_0

    .line 358
    :goto_1
    add-int/lit8 v0, p3, -0x1

    invoke-virtual {p0, v0}, Lorg/apache/lucene/util/SorterTemplate;->comparePivot(I)I

    move-result v0

    if-lez v0, :cond_2

    .line 362
    invoke-virtual {p0, p2}, Lorg/apache/lucene/util/SorterTemplate;->setPivot(I)V

    .line 363
    :goto_2
    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/SorterTemplate;->comparePivot(I)I

    move-result v0

    if-gez v0, :cond_3

    .line 367
    add-int v0, p4, p5

    const/4 v1, 0x2

    if-ne v0, v1, :cond_6

    .line 368
    sget-boolean v0, Lorg/apache/lucene/util/SorterTemplate;->$assertionsDisabled:Z

    if-nez v0, :cond_4

    if-eq p4, p5, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 359
    :cond_2
    add-int/lit8 p3, p3, -0x1

    .line 360
    add-int/lit8 p5, p5, -0x1

    goto :goto_1

    .line 364
    :cond_3
    add-int/lit8 p1, p1, 0x1

    .line 365
    add-int/lit8 p4, p4, -0x1

    goto :goto_2

    .line 369
    :cond_4
    sget-boolean v0, Lorg/apache/lucene/util/SorterTemplate;->$assertionsDisabled:Z

    if-nez v0, :cond_5

    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/util/SorterTemplate;->compare(II)I

    move-result v0

    if-gtz v0, :cond_5

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 370
    :cond_5
    invoke-virtual {p0, p2, p1}, Lorg/apache/lucene/util/SorterTemplate;->swap(II)V

    goto :goto_0

    .line 373
    :cond_6
    invoke-virtual/range {p0 .. p5}, Lorg/apache/lucene/util/SorterTemplate;->merge(IIIII)V

    goto :goto_0
.end method

.method private upper(III)I
    .locals 4
    .param p1, "lo"    # I
    .param p2, "hi"    # I
    .param p3, "val"    # I

    .prologue
    .line 431
    sub-int v1, p2, p1

    .line 432
    .local v1, "len":I
    :goto_0
    if-gtz v1, :cond_0

    .line 442
    return p1

    .line 433
    :cond_0
    ushr-int/lit8 v0, v1, 0x1

    .line 434
    .local v0, "half":I
    add-int v2, p1, v0

    .line 435
    .local v2, "mid":I
    invoke-virtual {p0, p3, v2}, Lorg/apache/lucene/util/SorterTemplate;->compare(II)I

    move-result v3

    if-gez v3, :cond_1

    .line 436
    move v1, v0

    .line 437
    goto :goto_0

    .line 438
    :cond_1
    add-int/lit8 p1, v2, 0x1

    .line 439
    sub-int v3, v1, v0

    add-int/lit8 v1, v3, -0x1

    goto :goto_0
.end method


# virtual methods
.method public final binarySort(II)V
    .locals 7
    .param p1, "lo"    # I
    .param p2, "hi"    # I

    .prologue
    .line 85
    add-int/lit8 v2, p1, 0x1

    .local v2, "i":I
    :goto_0
    if-le v2, p2, :cond_0

    .line 102
    return-void

    .line 86
    :cond_0
    move v4, p1

    .line 87
    .local v4, "l":I
    add-int/lit8 v1, v2, -0x1

    .line 88
    .local v1, "h":I
    invoke-virtual {p0, v2}, Lorg/apache/lucene/util/SorterTemplate;->setPivot(I)V

    .line 89
    :goto_1
    if-le v4, v1, :cond_1

    .line 98
    move v3, v2

    .local v3, "j":I
    :goto_2
    if-gt v3, v4, :cond_3

    .line 85
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 90
    .end local v3    # "j":I
    :cond_1
    add-int v6, v4, v1

    ushr-int/lit8 v5, v6, 0x1

    .line 91
    .local v5, "mid":I
    invoke-virtual {p0, v5}, Lorg/apache/lucene/util/SorterTemplate;->comparePivot(I)I

    move-result v0

    .line 92
    .local v0, "cmp":I
    if-gez v0, :cond_2

    .line 93
    add-int/lit8 v1, v5, -0x1

    .line 94
    goto :goto_1

    .line 95
    :cond_2
    add-int/lit8 v4, v5, 0x1

    goto :goto_1

    .line 99
    .end local v0    # "cmp":I
    .end local v5    # "mid":I
    .restart local v3    # "j":I
    :cond_3
    add-int/lit8 v6, v3, -0x1

    invoke-virtual {p0, v6, v3}, Lorg/apache/lucene/util/SorterTemplate;->swap(II)V

    .line 98
    add-int/lit8 v3, v3, -0x1

    goto :goto_2
.end method

.method protected abstract compare(II)I
.end method

.method protected abstract comparePivot(I)I
.end method

.method public final insertionSort(II)V
    .locals 3
    .param p1, "lo"    # I
    .param p2, "hi"    # I

    .prologue
    .line 71
    add-int/lit8 v0, p1, 0x1

    .local v0, "i":I
    :goto_0
    if-le v0, p2, :cond_0

    .line 80
    return-void

    .line 72
    :cond_0
    move v1, v0

    .local v1, "j":I
    :goto_1
    if-gt v1, p1, :cond_2

    .line 71
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 73
    :cond_2
    add-int/lit8 v2, v1, -0x1

    invoke-virtual {p0, v2, v1}, Lorg/apache/lucene/util/SorterTemplate;->compare(II)I

    move-result v2

    if-lez v2, :cond_1

    .line 74
    add-int/lit8 v2, v1, -0x1

    invoke-virtual {p0, v2, v1}, Lorg/apache/lucene/util/SorterTemplate;->swap(II)V

    .line 72
    add-int/lit8 v1, v1, -0x1

    goto :goto_1
.end method

.method protected merge(IIIII)V
    .locals 14
    .param p1, "lo"    # I
    .param p2, "pivot"    # I
    .param p3, "hi"    # I
    .param p4, "len1"    # I
    .param p5, "len2"    # I

    .prologue
    .line 382
    move/from16 v0, p4

    move/from16 v1, p5

    if-le v0, v1, :cond_0

    .line 383
    ushr-int/lit8 v6, p4, 0x1

    .line 384
    .local v6, "len11":I
    add-int v4, p1, v6

    .line 385
    .local v4, "first_cut":I
    move/from16 v0, p2

    move/from16 v1, p3

    invoke-direct {p0, v0, v1, v4}, Lorg/apache/lucene/util/SorterTemplate;->lower(III)I

    move-result v10

    .line 386
    .local v10, "second_cut":I
    sub-int v7, v10, p2

    .line 393
    .local v7, "len22":I
    :goto_0
    move/from16 v0, p2

    invoke-direct {p0, v4, v0, v10}, Lorg/apache/lucene/util/SorterTemplate;->rotate(III)V

    .line 394
    add-int v5, v4, v7

    .local v5, "new_mid":I
    move-object v2, p0

    move v3, p1

    .line 395
    invoke-direct/range {v2 .. v7}, Lorg/apache/lucene/util/SorterTemplate;->runMerge(IIIII)V

    .line 396
    sub-int v12, p4, v6

    sub-int v13, p5, v7

    move-object v8, p0

    move v9, v5

    move/from16 v11, p3

    invoke-direct/range {v8 .. v13}, Lorg/apache/lucene/util/SorterTemplate;->runMerge(IIIII)V

    .line 397
    return-void

    .line 388
    .end local v4    # "first_cut":I
    .end local v5    # "new_mid":I
    .end local v6    # "len11":I
    .end local v7    # "len22":I
    .end local v10    # "second_cut":I
    :cond_0
    ushr-int/lit8 v7, p5, 0x1

    .line 389
    .restart local v7    # "len22":I
    add-int v10, p2, v7

    .line 390
    .restart local v10    # "second_cut":I
    move/from16 v0, p2

    invoke-direct {p0, p1, v0, v10}, Lorg/apache/lucene/util/SorterTemplate;->upper(III)I

    move-result v4

    .line 391
    .restart local v4    # "first_cut":I
    sub-int v6, v4, p1

    .restart local v6    # "len11":I
    goto :goto_0
.end method

.method public final mergeSort(II)V
    .locals 7
    .param p1, "lo"    # I
    .param p2, "hi"    # I

    .prologue
    .line 333
    sub-int v6, p2, p1

    .line 334
    .local v6, "diff":I
    const/16 v0, 0xc

    if-gt v6, v0, :cond_0

    .line 335
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/util/SorterTemplate;->insertionSort(II)V

    .line 344
    :goto_0
    return-void

    .line 339
    :cond_0
    ushr-int/lit8 v0, v6, 0x1

    add-int v2, p1, v0

    .line 341
    .local v2, "mid":I
    invoke-virtual {p0, p1, v2}, Lorg/apache/lucene/util/SorterTemplate;->mergeSort(II)V

    .line 342
    invoke-virtual {p0, v2, p2}, Lorg/apache/lucene/util/SorterTemplate;->mergeSort(II)V

    .line 343
    sub-int v4, v2, p1

    sub-int v5, p2, v2

    move-object v0, p0

    move v1, p1

    move v3, p2

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/util/SorterTemplate;->runMerge(IIIII)V

    goto :goto_0
.end method

.method public final quickSort(II)V
    .locals 1
    .param p1, "lo"    # I
    .param p2, "hi"    # I

    .prologue
    .line 107
    if-gt p2, p1, :cond_0

    .line 110
    :goto_0
    return-void

    .line 109
    :cond_0
    sub-int v0, p2, p1

    invoke-static {v0}, Ljava/lang/Integer;->numberOfLeadingZeros(I)I

    move-result v0

    rsub-int/lit8 v0, v0, 0x20

    shl-int/lit8 v0, v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/util/SorterTemplate;->quickSort(III)V

    goto :goto_0
.end method

.method protected abstract setPivot(I)V
.end method

.method protected abstract swap(II)V
.end method

.method public final timSort(II)V
    .locals 2
    .param p1, "lo"    # I
    .param p2, "hi"    # I

    .prologue
    .line 322
    sub-int v0, p2, p1

    const/16 v1, 0x40

    if-gt v0, v1, :cond_0

    .line 323
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/util/SorterTemplate;->binarySort(II)V

    .line 328
    :goto_0
    return-void

    .line 327
    :cond_0
    new-instance v0, Lorg/apache/lucene/util/SorterTemplate$TimSort;

    invoke-direct {v0, p0, p1, p2}, Lorg/apache/lucene/util/SorterTemplate$TimSort;-><init>(Lorg/apache/lucene/util/SorterTemplate;II)V

    invoke-virtual {v0}, Lorg/apache/lucene/util/SorterTemplate$TimSort;->sort()V

    goto :goto_0
.end method
