.class public Lorg/apache/lucene/util/AttributeSource;
.super Ljava/lang/Object;
.source "AttributeSource.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/AttributeSource$AttributeFactory;,
        Lorg/apache/lucene/util/AttributeSource$State;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final knownImplClasses:Lorg/apache/lucene/util/WeakIdentityMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/WeakIdentityMap",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lorg/apache/lucene/util/AttributeImpl;",
            ">;",
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lorg/apache/lucene/util/Attribute;",
            ">;>;>;>;"
        }
    .end annotation
.end field


# instance fields
.field private final attributeImpls:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lorg/apache/lucene/util/AttributeImpl;",
            ">;",
            "Lorg/apache/lucene/util/AttributeImpl;",
            ">;"
        }
    .end annotation
.end field

.field private final attributes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lorg/apache/lucene/util/Attribute;",
            ">;",
            "Lorg/apache/lucene/util/AttributeImpl;",
            ">;"
        }
    .end annotation
.end field

.field private final currentState:[Lorg/apache/lucene/util/AttributeSource$State;

.field private final factory:Lorg/apache/lucene/util/AttributeSource$AttributeFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 40
    const-class v0, Lorg/apache/lucene/util/AttributeSource;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/AttributeSource;->$assertionsDisabled:Z

    .line 204
    invoke-static {v1}, Lorg/apache/lucene/util/WeakIdentityMap;->newConcurrentHashMap(Z)Lorg/apache/lucene/util/WeakIdentityMap;

    move-result-object v0

    .line 203
    sput-object v0, Lorg/apache/lucene/util/AttributeSource;->knownImplClasses:Lorg/apache/lucene/util/WeakIdentityMap;

    .line 204
    return-void

    :cond_0
    move v0, v1

    .line 40
    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 128
    sget-object v0, Lorg/apache/lucene/util/AttributeSource$AttributeFactory;->DEFAULT_ATTRIBUTE_FACTORY:Lorg/apache/lucene/util/AttributeSource$AttributeFactory;

    invoke-direct {p0, v0}, Lorg/apache/lucene/util/AttributeSource;-><init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;)V

    .line 129
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;)V
    .locals 1
    .param p1, "factory"    # Lorg/apache/lucene/util/AttributeSource$AttributeFactory;

    .prologue
    .line 147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 148
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/util/AttributeSource;->attributes:Ljava/util/Map;

    .line 149
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/util/AttributeSource;->attributeImpls:Ljava/util/Map;

    .line 150
    const/4 v0, 0x1

    new-array v0, v0, [Lorg/apache/lucene/util/AttributeSource$State;

    iput-object v0, p0, Lorg/apache/lucene/util/AttributeSource;->currentState:[Lorg/apache/lucene/util/AttributeSource$State;

    .line 151
    iput-object p1, p0, Lorg/apache/lucene/util/AttributeSource;->factory:Lorg/apache/lucene/util/AttributeSource$AttributeFactory;

    .line 152
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/AttributeSource;)V
    .locals 2
    .param p1, "input"    # Lorg/apache/lucene/util/AttributeSource;

    .prologue
    .line 134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 135
    if-nez p1, :cond_0

    .line 136
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "input AttributeSource must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 138
    :cond_0
    iget-object v0, p1, Lorg/apache/lucene/util/AttributeSource;->attributes:Ljava/util/Map;

    iput-object v0, p0, Lorg/apache/lucene/util/AttributeSource;->attributes:Ljava/util/Map;

    .line 139
    iget-object v0, p1, Lorg/apache/lucene/util/AttributeSource;->attributeImpls:Ljava/util/Map;

    iput-object v0, p0, Lorg/apache/lucene/util/AttributeSource;->attributeImpls:Ljava/util/Map;

    .line 140
    iget-object v0, p1, Lorg/apache/lucene/util/AttributeSource;->currentState:[Lorg/apache/lucene/util/AttributeSource$State;

    iput-object v0, p0, Lorg/apache/lucene/util/AttributeSource;->currentState:[Lorg/apache/lucene/util/AttributeSource$State;

    .line 141
    iget-object v0, p1, Lorg/apache/lucene/util/AttributeSource;->factory:Lorg/apache/lucene/util/AttributeSource$AttributeFactory;

    iput-object v0, p0, Lorg/apache/lucene/util/AttributeSource;->factory:Lorg/apache/lucene/util/AttributeSource$AttributeFactory;

    .line 142
    return-void
.end method

.method static getAttributeInterfaces(Ljava/lang/Class;)Ljava/util/LinkedList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lorg/apache/lucene/util/AttributeImpl;",
            ">;)",
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lorg/apache/lucene/util/Attribute;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 207
    .local p0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/apache/lucene/util/AttributeImpl;>;"
    sget-object v3, Lorg/apache/lucene/util/AttributeSource;->knownImplClasses:Lorg/apache/lucene/util/WeakIdentityMap;

    invoke-virtual {v3, p0}, Lorg/apache/lucene/util/WeakIdentityMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/LinkedList;

    .line 208
    .local v2, "foundInterfaces":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/ref/WeakReference<Ljava/lang/Class<+Lorg/apache/lucene/util/Attribute;>;>;>;"
    if-nez v2, :cond_1

    .line 210
    new-instance v2, Ljava/util/LinkedList;

    .end local v2    # "foundInterfaces":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/ref/WeakReference<Ljava/lang/Class<+Lorg/apache/lucene/util/Attribute;>;>;>;"
    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    .line 213
    .restart local v2    # "foundInterfaces":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/ref/WeakReference<Ljava/lang/Class<+Lorg/apache/lucene/util/Attribute;>;>;>;"
    move-object v0, p0

    .line 215
    .local v0, "actClazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Class;->getInterfaces()[Ljava/lang/Class;

    move-result-object v4

    array-length v5, v4

    const/4 v3, 0x0

    :goto_0
    if-lt v3, v5, :cond_2

    .line 220
    invoke-virtual {v0}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v0

    .line 221
    if-nez v0, :cond_0

    .line 222
    sget-object v3, Lorg/apache/lucene/util/AttributeSource;->knownImplClasses:Lorg/apache/lucene/util/WeakIdentityMap;

    invoke-virtual {v3, p0, v2}, Lorg/apache/lucene/util/WeakIdentityMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 224
    .end local v0    # "actClazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_1
    return-object v2

    .line 215
    .restart local v0    # "actClazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_2
    aget-object v1, v4, v3

    .line 216
    .local v1, "curInterface":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-class v6, Lorg/apache/lucene/util/Attribute;

    if-eq v1, v6, :cond_3

    const-class v6, Lorg/apache/lucene/util/Attribute;

    invoke-virtual {v6, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 217
    new-instance v6, Ljava/lang/ref/WeakReference;

    const-class v7, Lorg/apache/lucene/util/Attribute;

    invoke-virtual {v1, v7}, Ljava/lang/Class;->asSubclass(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v2, v6}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 215
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method private getCurrentState()Lorg/apache/lucene/util/AttributeSource$State;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 309
    iget-object v4, p0, Lorg/apache/lucene/util/AttributeSource;->currentState:[Lorg/apache/lucene/util/AttributeSource$State;

    aget-object v2, v4, v5

    .line 310
    .local v2, "s":Lorg/apache/lucene/util/AttributeSource$State;
    if-nez v2, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/util/AttributeSource;->hasAttributes()Z

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    move-object v3, v2

    .line 321
    .end local v2    # "s":Lorg/apache/lucene/util/AttributeSource$State;
    .local v3, "s":Ljava/lang/Object;
    :goto_0
    return-object v3

    .line 313
    .end local v3    # "s":Ljava/lang/Object;
    .restart local v2    # "s":Lorg/apache/lucene/util/AttributeSource$State;
    :cond_1
    iget-object v4, p0, Lorg/apache/lucene/util/AttributeSource;->currentState:[Lorg/apache/lucene/util/AttributeSource$State;

    new-instance v2, Lorg/apache/lucene/util/AttributeSource$State;

    .end local v2    # "s":Lorg/apache/lucene/util/AttributeSource$State;
    invoke-direct {v2}, Lorg/apache/lucene/util/AttributeSource$State;-><init>()V

    aput-object v2, v4, v5

    .restart local v2    # "s":Lorg/apache/lucene/util/AttributeSource$State;
    move-object v0, v2

    .line 314
    .local v0, "c":Lorg/apache/lucene/util/AttributeSource$State;
    iget-object v4, p0, Lorg/apache/lucene/util/AttributeSource;->attributeImpls:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 315
    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/util/AttributeImpl;>;"
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/util/AttributeImpl;

    iput-object v4, v0, Lorg/apache/lucene/util/AttributeSource$State;->attribute:Lorg/apache/lucene/util/AttributeImpl;

    .line 316
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_2

    move-object v3, v2

    .line 321
    .restart local v3    # "s":Ljava/lang/Object;
    goto :goto_0

    .line 317
    .end local v3    # "s":Ljava/lang/Object;
    :cond_2
    new-instance v4, Lorg/apache/lucene/util/AttributeSource$State;

    invoke-direct {v4}, Lorg/apache/lucene/util/AttributeSource$State;-><init>()V

    iput-object v4, v0, Lorg/apache/lucene/util/AttributeSource$State;->next:Lorg/apache/lucene/util/AttributeSource$State;

    .line 318
    iget-object v0, v0, Lorg/apache/lucene/util/AttributeSource$State;->next:Lorg/apache/lucene/util/AttributeSource$State;

    .line 319
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/util/AttributeImpl;

    iput-object v4, v0, Lorg/apache/lucene/util/AttributeSource$State;->attribute:Lorg/apache/lucene/util/AttributeImpl;

    goto :goto_1
.end method


# virtual methods
.method public final addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A::",
            "Lorg/apache/lucene/util/Attribute;",
            ">(",
            "Ljava/lang/Class",
            "<TA;>;)TA;"
        }
    .end annotation

    .prologue
    .line 263
    .local p1, "attClass":Ljava/lang/Class;, "Ljava/lang/Class<TA;>;"
    iget-object v1, p0, Lorg/apache/lucene/util/AttributeSource;->attributes:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/util/AttributeImpl;

    .line 264
    .local v0, "attImpl":Lorg/apache/lucene/util/AttributeImpl;
    if-nez v0, :cond_2

    .line 265
    invoke-virtual {p1}, Ljava/lang/Class;->isInterface()Z

    move-result v1

    if-eqz v1, :cond_0

    const-class v1, Lorg/apache/lucene/util/Attribute;

    invoke-virtual {v1, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 266
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    .line 267
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "addAttribute() only accepts an interface that extends Attribute, but "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 268
    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " does not fulfil this contract."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 267
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 266
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 271
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/util/AttributeSource;->factory:Lorg/apache/lucene/util/AttributeSource$AttributeFactory;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/util/AttributeSource$AttributeFactory;->createAttributeInstance(Ljava/lang/Class;)Lorg/apache/lucene/util/AttributeImpl;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/util/AttributeSource;->addAttributeImpl(Lorg/apache/lucene/util/AttributeImpl;)V

    .line 273
    :cond_2
    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/util/Attribute;

    return-object v1
.end method

.method public final addAttributeImpl(Lorg/apache/lucene/util/AttributeImpl;)V
    .locals 8
    .param p1, "att"    # Lorg/apache/lucene/util/AttributeImpl;

    .prologue
    .line 236
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 237
    .local v0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/apache/lucene/util/AttributeImpl;>;"
    iget-object v4, p0, Lorg/apache/lucene/util/AttributeSource;->attributeImpls:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 254
    :cond_0
    return-void

    .line 239
    :cond_1
    invoke-static {v0}, Lorg/apache/lucene/util/AttributeSource;->getAttributeInterfaces(Ljava/lang/Class;)Ljava/util/LinkedList;

    move-result-object v3

    .line 242
    .local v3, "foundInterfaces":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/ref/WeakReference<Ljava/lang/Class<+Lorg/apache/lucene/util/Attribute;>;>;>;"
    invoke-virtual {v3}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/ref/WeakReference;

    .line 243
    .local v2, "curInterfaceRef":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Ljava/lang/Class<+Lorg/apache/lucene/util/Attribute;>;>;"
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Class;

    .line 244
    .local v1, "curInterface":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/apache/lucene/util/Attribute;>;"
    sget-boolean v5, Lorg/apache/lucene/util/AttributeSource;->$assertionsDisabled:Z

    if-nez v5, :cond_3

    if-nez v1, :cond_3

    new-instance v4, Ljava/lang/AssertionError;

    .line 245
    const-string v5, "We have a strong reference on the class holding the interfaces, so they should never get evicted"

    invoke-direct {v4, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    .line 247
    :cond_3
    iget-object v5, p0, Lorg/apache/lucene/util/AttributeSource;->attributes:Ljava/util/Map;

    invoke-interface {v5, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 249
    iget-object v5, p0, Lorg/apache/lucene/util/AttributeSource;->currentState:[Lorg/apache/lucene/util/AttributeSource$State;

    const/4 v6, 0x0

    const/4 v7, 0x0

    aput-object v7, v5, v6

    .line 250
    iget-object v5, p0, Lorg/apache/lucene/util/AttributeSource;->attributes:Ljava/util/Map;

    invoke-interface {v5, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 251
    iget-object v5, p0, Lorg/apache/lucene/util/AttributeSource;->attributeImpls:Ljava/util/Map;

    invoke-interface {v5, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final captureState()Lorg/apache/lucene/util/AttributeSource$State;
    .locals 2

    .prologue
    .line 339
    invoke-direct {p0}, Lorg/apache/lucene/util/AttributeSource;->getCurrentState()Lorg/apache/lucene/util/AttributeSource$State;

    move-result-object v0

    .line 340
    .local v0, "state":Lorg/apache/lucene/util/AttributeSource$State;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lorg/apache/lucene/util/AttributeSource$State;->clone()Lorg/apache/lucene/util/AttributeSource$State;

    move-result-object v1

    goto :goto_0
.end method

.method public final clearAttributes()V
    .locals 2

    .prologue
    .line 329
    invoke-direct {p0}, Lorg/apache/lucene/util/AttributeSource;->getCurrentState()Lorg/apache/lucene/util/AttributeSource$State;

    move-result-object v0

    .local v0, "state":Lorg/apache/lucene/util/AttributeSource$State;
    :goto_0
    if-nez v0, :cond_0

    .line 332
    return-void

    .line 330
    :cond_0
    iget-object v1, v0, Lorg/apache/lucene/util/AttributeSource$State;->attribute:Lorg/apache/lucene/util/AttributeImpl;

    invoke-virtual {v1}, Lorg/apache/lucene/util/AttributeImpl;->clear()V

    .line 329
    iget-object v0, v0, Lorg/apache/lucene/util/AttributeSource$State;->next:Lorg/apache/lucene/util/AttributeSource$State;

    goto :goto_0
.end method

.method public final cloneAttributes()Lorg/apache/lucene/util/AttributeSource;
    .locals 8

    .prologue
    .line 468
    new-instance v0, Lorg/apache/lucene/util/AttributeSource;

    iget-object v3, p0, Lorg/apache/lucene/util/AttributeSource;->factory:Lorg/apache/lucene/util/AttributeSource$AttributeFactory;

    invoke-direct {v0, v3}, Lorg/apache/lucene/util/AttributeSource;-><init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;)V

    .line 470
    .local v0, "clone":Lorg/apache/lucene/util/AttributeSource;
    invoke-virtual {p0}, Lorg/apache/lucene/util/AttributeSource;->hasAttributes()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 472
    invoke-direct {p0}, Lorg/apache/lucene/util/AttributeSource;->getCurrentState()Lorg/apache/lucene/util/AttributeSource$State;

    move-result-object v2

    .local v2, "state":Lorg/apache/lucene/util/AttributeSource$State;
    :goto_0
    if-nez v2, :cond_1

    .line 477
    iget-object v3, p0, Lorg/apache/lucene/util/AttributeSource;->attributes:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 482
    .end local v2    # "state":Lorg/apache/lucene/util/AttributeSource$State;
    :cond_0
    return-object v0

    .line 473
    .restart local v2    # "state":Lorg/apache/lucene/util/AttributeSource$State;
    :cond_1
    iget-object v3, v0, Lorg/apache/lucene/util/AttributeSource;->attributeImpls:Ljava/util/Map;

    iget-object v4, v2, Lorg/apache/lucene/util/AttributeSource$State;->attribute:Lorg/apache/lucene/util/AttributeImpl;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    iget-object v5, v2, Lorg/apache/lucene/util/AttributeSource$State;->attribute:Lorg/apache/lucene/util/AttributeImpl;

    invoke-virtual {v5}, Lorg/apache/lucene/util/AttributeImpl;->clone()Lorg/apache/lucene/util/AttributeImpl;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 472
    iget-object v2, v2, Lorg/apache/lucene/util/AttributeSource$State;->next:Lorg/apache/lucene/util/AttributeSource$State;

    goto :goto_0

    .line 477
    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 478
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Class<+Lorg/apache/lucene/util/Attribute;>;Lorg/apache/lucene/util/AttributeImpl;>;"
    iget-object v6, v0, Lorg/apache/lucene/util/AttributeSource;->attributes:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Class;

    iget-object v7, v0, Lorg/apache/lucene/util/AttributeSource;->attributeImpls:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/util/AttributeImpl;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-interface {v7, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/util/AttributeImpl;

    invoke-interface {v6, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public final copyTo(Lorg/apache/lucene/util/AttributeSource;)V
    .locals 5
    .param p1, "target"    # Lorg/apache/lucene/util/AttributeSource;

    .prologue
    .line 494
    invoke-direct {p0}, Lorg/apache/lucene/util/AttributeSource;->getCurrentState()Lorg/apache/lucene/util/AttributeSource$State;

    move-result-object v0

    .local v0, "state":Lorg/apache/lucene/util/AttributeSource$State;
    :goto_0
    if-nez v0, :cond_0

    .line 502
    return-void

    .line 495
    :cond_0
    iget-object v2, p1, Lorg/apache/lucene/util/AttributeSource;->attributeImpls:Ljava/util/Map;

    iget-object v3, v0, Lorg/apache/lucene/util/AttributeSource$State;->attribute:Lorg/apache/lucene/util/AttributeImpl;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/util/AttributeImpl;

    .line 496
    .local v1, "targetImpl":Lorg/apache/lucene/util/AttributeImpl;
    if-nez v1, :cond_1

    .line 497
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "This AttributeSource contains AttributeImpl of type "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 498
    iget-object v4, v0, Lorg/apache/lucene/util/AttributeSource$State;->attribute:Lorg/apache/lucene/util/AttributeImpl;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " that is not in the target"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 497
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 500
    :cond_1
    iget-object v2, v0, Lorg/apache/lucene/util/AttributeSource$State;->attribute:Lorg/apache/lucene/util/AttributeImpl;

    invoke-virtual {v2, v1}, Lorg/apache/lucene/util/AttributeImpl;->copyTo(Lorg/apache/lucene/util/AttributeImpl;)V

    .line 494
    iget-object v0, v0, Lorg/apache/lucene/util/AttributeSource$State;->next:Lorg/apache/lucene/util/AttributeSource$State;

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 383
    if-ne p1, p0, :cond_1

    move v3, v4

    .line 414
    :cond_0
    :goto_0
    return v3

    .line 387
    :cond_1
    instance-of v5, p1, Lorg/apache/lucene/util/AttributeSource;

    if-eqz v5, :cond_0

    move-object v0, p1

    .line 388
    check-cast v0, Lorg/apache/lucene/util/AttributeSource;

    .line 390
    .local v0, "other":Lorg/apache/lucene/util/AttributeSource;
    invoke-virtual {p0}, Lorg/apache/lucene/util/AttributeSource;->hasAttributes()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 391
    invoke-virtual {v0}, Lorg/apache/lucene/util/AttributeSource;->hasAttributes()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 395
    iget-object v5, p0, Lorg/apache/lucene/util/AttributeSource;->attributeImpls:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->size()I

    move-result v5

    iget-object v6, v0, Lorg/apache/lucene/util/AttributeSource;->attributeImpls:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->size()I

    move-result v6

    if-ne v5, v6, :cond_0

    .line 400
    invoke-direct {p0}, Lorg/apache/lucene/util/AttributeSource;->getCurrentState()Lorg/apache/lucene/util/AttributeSource$State;

    move-result-object v2

    .line 401
    .local v2, "thisState":Lorg/apache/lucene/util/AttributeSource$State;
    invoke-direct {v0}, Lorg/apache/lucene/util/AttributeSource;->getCurrentState()Lorg/apache/lucene/util/AttributeSource$State;

    move-result-object v1

    .line 402
    .local v1, "otherState":Lorg/apache/lucene/util/AttributeSource$State;
    :goto_1
    if-eqz v2, :cond_2

    if-nez v1, :cond_3

    :cond_2
    move v3, v4

    .line 409
    goto :goto_0

    .line 403
    :cond_3
    iget-object v5, v1, Lorg/apache/lucene/util/AttributeSource$State;->attribute:Lorg/apache/lucene/util/AttributeImpl;

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    iget-object v6, v2, Lorg/apache/lucene/util/AttributeSource$State;->attribute:Lorg/apache/lucene/util/AttributeImpl;

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    if-ne v5, v6, :cond_0

    iget-object v5, v1, Lorg/apache/lucene/util/AttributeSource$State;->attribute:Lorg/apache/lucene/util/AttributeImpl;

    iget-object v6, v2, Lorg/apache/lucene/util/AttributeSource$State;->attribute:Lorg/apache/lucene/util/AttributeImpl;

    invoke-virtual {v5, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 406
    iget-object v2, v2, Lorg/apache/lucene/util/AttributeSource$State;->next:Lorg/apache/lucene/util/AttributeSource$State;

    .line 407
    iget-object v1, v1, Lorg/apache/lucene/util/AttributeSource$State;->next:Lorg/apache/lucene/util/AttributeSource$State;

    goto :goto_1

    .line 411
    .end local v1    # "otherState":Lorg/apache/lucene/util/AttributeSource$State;
    .end local v2    # "thisState":Lorg/apache/lucene/util/AttributeSource$State;
    :cond_4
    invoke-virtual {v0}, Lorg/apache/lucene/util/AttributeSource;->hasAttributes()Z

    move-result v5

    if-nez v5, :cond_0

    move v3, v4

    goto :goto_0
.end method

.method public final getAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A::",
            "Lorg/apache/lucene/util/Attribute;",
            ">(",
            "Ljava/lang/Class",
            "<TA;>;)TA;"
        }
    .end annotation

    .prologue
    .line 301
    .local p1, "attClass":Ljava/lang/Class;, "Ljava/lang/Class<TA;>;"
    iget-object v1, p0, Lorg/apache/lucene/util/AttributeSource;->attributes:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/util/AttributeImpl;

    .line 302
    .local v0, "attImpl":Lorg/apache/lucene/util/AttributeImpl;
    if-nez v0, :cond_0

    .line 303
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "This AttributeSource does not have the attribute \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 305
    :cond_0
    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/util/Attribute;

    return-object v1
.end method

.method public final getAttributeClassesIterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lorg/apache/lucene/util/Attribute;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 165
    iget-object v0, p0, Lorg/apache/lucene/util/AttributeSource;->attributes:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public final getAttributeFactory()Lorg/apache/lucene/util/AttributeSource$AttributeFactory;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lorg/apache/lucene/util/AttributeSource;->factory:Lorg/apache/lucene/util/AttributeSource$AttributeFactory;

    return-object v0
.end method

.method public final getAttributeImplsIterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lorg/apache/lucene/util/AttributeImpl;",
            ">;"
        }
    .end annotation

    .prologue
    .line 173
    invoke-direct {p0}, Lorg/apache/lucene/util/AttributeSource;->getCurrentState()Lorg/apache/lucene/util/AttributeSource$State;

    move-result-object v0

    .line 174
    .local v0, "initState":Lorg/apache/lucene/util/AttributeSource$State;
    if-eqz v0, :cond_0

    .line 175
    new-instance v1, Lorg/apache/lucene/util/AttributeSource$1;

    invoke-direct {v1, p0, v0}, Lorg/apache/lucene/util/AttributeSource$1;-><init>(Lorg/apache/lucene/util/AttributeSource;Lorg/apache/lucene/util/AttributeSource$State;)V

    .line 198
    :goto_0
    return-object v1

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    goto :goto_0
.end method

.method public final hasAttribute(Ljava/lang/Class;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lorg/apache/lucene/util/Attribute;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 286
    .local p1, "attClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/apache/lucene/util/Attribute;>;"
    iget-object v0, p0, Lorg/apache/lucene/util/AttributeSource;->attributes:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final hasAttributes()Z
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Lorg/apache/lucene/util/AttributeSource;->attributes:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 374
    const/4 v0, 0x0

    .line 375
    .local v0, "code":I
    invoke-direct {p0}, Lorg/apache/lucene/util/AttributeSource;->getCurrentState()Lorg/apache/lucene/util/AttributeSource$State;

    move-result-object v1

    .local v1, "state":Lorg/apache/lucene/util/AttributeSource$State;
    :goto_0
    if-nez v1, :cond_0

    .line 378
    return v0

    .line 376
    :cond_0
    mul-int/lit8 v2, v0, 0x1f

    iget-object v3, v1, Lorg/apache/lucene/util/AttributeSource$State;->attribute:Lorg/apache/lucene/util/AttributeImpl;

    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    add-int v0, v2, v3

    .line 375
    iget-object v1, v1, Lorg/apache/lucene/util/AttributeSource$State;->next:Lorg/apache/lucene/util/AttributeSource$State;

    goto :goto_0
.end method

.method public final reflectAsString(Z)Ljava/lang/String;
    .locals 2
    .param p1, "prependAttClass"    # Z

    .prologue
    .line 429
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 430
    .local v0, "buffer":Ljava/lang/StringBuilder;
    new-instance v1, Lorg/apache/lucene/util/AttributeSource$2;

    invoke-direct {v1, p0, v0, p1}, Lorg/apache/lucene/util/AttributeSource$2;-><init>(Lorg/apache/lucene/util/AttributeSource;Ljava/lang/StringBuilder;Z)V

    invoke-virtual {p0, v1}, Lorg/apache/lucene/util/AttributeSource;->reflectWith(Lorg/apache/lucene/util/AttributeReflector;)V

    .line 442
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public final reflectWith(Lorg/apache/lucene/util/AttributeReflector;)V
    .locals 2
    .param p1, "reflector"    # Lorg/apache/lucene/util/AttributeReflector;

    .prologue
    .line 455
    invoke-direct {p0}, Lorg/apache/lucene/util/AttributeSource;->getCurrentState()Lorg/apache/lucene/util/AttributeSource$State;

    move-result-object v0

    .local v0, "state":Lorg/apache/lucene/util/AttributeSource$State;
    :goto_0
    if-nez v0, :cond_0

    .line 458
    return-void

    .line 456
    :cond_0
    iget-object v1, v0, Lorg/apache/lucene/util/AttributeSource$State;->attribute:Lorg/apache/lucene/util/AttributeImpl;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/util/AttributeImpl;->reflectWith(Lorg/apache/lucene/util/AttributeReflector;)V

    .line 455
    iget-object v0, v0, Lorg/apache/lucene/util/AttributeSource$State;->next:Lorg/apache/lucene/util/AttributeSource$State;

    goto :goto_0
.end method

.method public final restoreState(Lorg/apache/lucene/util/AttributeSource$State;)V
    .locals 4
    .param p1, "state"    # Lorg/apache/lucene/util/AttributeSource$State;

    .prologue
    .line 359
    if-nez p1, :cond_0

    .line 370
    :goto_0
    return-void

    .line 362
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/util/AttributeSource;->attributeImpls:Ljava/util/Map;

    iget-object v2, p1, Lorg/apache/lucene/util/AttributeSource$State;->attribute:Lorg/apache/lucene/util/AttributeImpl;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/util/AttributeImpl;

    .line 363
    .local v0, "targetImpl":Lorg/apache/lucene/util/AttributeImpl;
    if-nez v0, :cond_1

    .line 364
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "State contains AttributeImpl of type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 365
    iget-object v3, p1, Lorg/apache/lucene/util/AttributeSource$State;->attribute:Lorg/apache/lucene/util/AttributeImpl;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " that is not in in this AttributeSource"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 364
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 367
    :cond_1
    iget-object v1, p1, Lorg/apache/lucene/util/AttributeSource$State;->attribute:Lorg/apache/lucene/util/AttributeImpl;

    invoke-virtual {v1, v0}, Lorg/apache/lucene/util/AttributeImpl;->copyTo(Lorg/apache/lucene/util/AttributeImpl;)V

    .line 368
    iget-object p1, p1, Lorg/apache/lucene/util/AttributeSource$State;->next:Lorg/apache/lucene/util/AttributeSource$State;

    .line 369
    if-nez p1, :cond_0

    goto :goto_0
.end method
