.class public final Lorg/apache/lucene/util/ByteBlockPool$DirectAllocator;
.super Lorg/apache/lucene/util/ByteBlockPool$Allocator;
.source "ByteBlockPool.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/ByteBlockPool;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DirectAllocator"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 74
    const v0, 0x8000

    invoke-direct {p0, v0}, Lorg/apache/lucene/util/ByteBlockPool$DirectAllocator;-><init>(I)V

    .line 75
    return-void
.end method

.method public constructor <init>(I)V
    .locals 0
    .param p1, "blockSize"    # I

    .prologue
    .line 78
    invoke-direct {p0, p1}, Lorg/apache/lucene/util/ByteBlockPool$Allocator;-><init>(I)V

    .line 79
    return-void
.end method


# virtual methods
.method public recycleByteBlocks([[BII)V
    .locals 0
    .param p1, "blocks"    # [[B
    .param p2, "start"    # I
    .param p3, "end"    # I

    .prologue
    .line 83
    return-void
.end method
