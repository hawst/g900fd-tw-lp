.class public final Lorg/apache/lucene/util/fst/NoOutputs;
.super Lorg/apache/lucene/util/fst/Outputs;
.source "NoOutputs.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/util/fst/Outputs",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field static final NO_OUTPUT:Ljava/lang/Object;

.field private static final singleton:Lorg/apache/lucene/util/fst/NoOutputs;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lorg/apache/lucene/util/fst/NoOutputs;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/fst/NoOutputs;->$assertionsDisabled:Z

    .line 32
    new-instance v0, Lorg/apache/lucene/util/fst/NoOutputs$1;

    invoke-direct {v0}, Lorg/apache/lucene/util/fst/NoOutputs$1;-><init>()V

    sput-object v0, Lorg/apache/lucene/util/fst/NoOutputs;->NO_OUTPUT:Ljava/lang/Object;

    .line 46
    new-instance v0, Lorg/apache/lucene/util/fst/NoOutputs;

    invoke-direct {v0}, Lorg/apache/lucene/util/fst/NoOutputs;-><init>()V

    sput-object v0, Lorg/apache/lucene/util/fst/NoOutputs;->singleton:Lorg/apache/lucene/util/fst/NoOutputs;

    return-void

    .line 30
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lorg/apache/lucene/util/fst/Outputs;-><init>()V

    .line 49
    return-void
.end method

.method public static getSingleton()Lorg/apache/lucene/util/fst/NoOutputs;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lorg/apache/lucene/util/fst/NoOutputs;->singleton:Lorg/apache/lucene/util/fst/NoOutputs;

    return-object v0
.end method


# virtual methods
.method public add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1, "prefix"    # Ljava/lang/Object;
    .param p2, "output"    # Ljava/lang/Object;

    .prologue
    .line 71
    sget-boolean v0, Lorg/apache/lucene/util/fst/NoOutputs;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    sget-object v0, Lorg/apache/lucene/util/fst/NoOutputs;->NO_OUTPUT:Ljava/lang/Object;

    if-eq p1, v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "got "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 72
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/util/fst/NoOutputs;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    sget-object v0, Lorg/apache/lucene/util/fst/NoOutputs;->NO_OUTPUT:Ljava/lang/Object;

    if-eq p2, v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 73
    :cond_1
    sget-object v0, Lorg/apache/lucene/util/fst/NoOutputs;->NO_OUTPUT:Ljava/lang/Object;

    return-object v0
.end method

.method public common(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "output1"    # Ljava/lang/Object;
    .param p2, "output2"    # Ljava/lang/Object;

    .prologue
    .line 57
    sget-boolean v0, Lorg/apache/lucene/util/fst/NoOutputs;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    sget-object v0, Lorg/apache/lucene/util/fst/NoOutputs;->NO_OUTPUT:Ljava/lang/Object;

    if-eq p1, v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 58
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/util/fst/NoOutputs;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    sget-object v0, Lorg/apache/lucene/util/fst/NoOutputs;->NO_OUTPUT:Ljava/lang/Object;

    if-eq p2, v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 59
    :cond_1
    sget-object v0, Lorg/apache/lucene/util/fst/NoOutputs;->NO_OUTPUT:Ljava/lang/Object;

    return-object v0
.end method

.method public getNoOutput()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 97
    sget-object v0, Lorg/apache/lucene/util/fst/NoOutputs;->NO_OUTPUT:Ljava/lang/Object;

    return-object v0
.end method

.method public merge(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "first"    # Ljava/lang/Object;
    .param p2, "second"    # Ljava/lang/Object;

    .prologue
    .line 78
    sget-boolean v0, Lorg/apache/lucene/util/fst/NoOutputs;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    sget-object v0, Lorg/apache/lucene/util/fst/NoOutputs;->NO_OUTPUT:Ljava/lang/Object;

    if-eq p1, v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 79
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/util/fst/NoOutputs;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    sget-object v0, Lorg/apache/lucene/util/fst/NoOutputs;->NO_OUTPUT:Ljava/lang/Object;

    if-eq p2, v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 80
    :cond_1
    sget-object v0, Lorg/apache/lucene/util/fst/NoOutputs;->NO_OUTPUT:Ljava/lang/Object;

    return-object v0
.end method

.method public outputToString(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p1, "output"    # Ljava/lang/Object;

    .prologue
    .line 102
    const-string v0, ""

    return-object v0
.end method

.method public read(Lorg/apache/lucene/store/DataInput;)Ljava/lang/Object;
    .locals 1
    .param p1, "in"    # Lorg/apache/lucene/store/DataInput;

    .prologue
    .line 92
    sget-object v0, Lorg/apache/lucene/util/fst/NoOutputs;->NO_OUTPUT:Ljava/lang/Object;

    return-object v0
.end method

.method public subtract(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "output"    # Ljava/lang/Object;
    .param p2, "inc"    # Ljava/lang/Object;

    .prologue
    .line 64
    sget-boolean v0, Lorg/apache/lucene/util/fst/NoOutputs;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    sget-object v0, Lorg/apache/lucene/util/fst/NoOutputs;->NO_OUTPUT:Ljava/lang/Object;

    if-eq p1, v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 65
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/util/fst/NoOutputs;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    sget-object v0, Lorg/apache/lucene/util/fst/NoOutputs;->NO_OUTPUT:Ljava/lang/Object;

    if-eq p2, v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 66
    :cond_1
    sget-object v0, Lorg/apache/lucene/util/fst/NoOutputs;->NO_OUTPUT:Ljava/lang/Object;

    return-object v0
.end method

.method public write(Ljava/lang/Object;Lorg/apache/lucene/store/DataOutput;)V
    .locals 0
    .param p1, "prefix"    # Ljava/lang/Object;
    .param p2, "out"    # Lorg/apache/lucene/store/DataOutput;

    .prologue
    .line 86
    return-void
.end method
