.class public Lorg/apache/lucene/util/ByteBlockPool$DirectTrackingAllocator;
.super Lorg/apache/lucene/util/ByteBlockPool$Allocator;
.source "ByteBlockPool.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/ByteBlockPool;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DirectTrackingAllocator"
.end annotation


# instance fields
.field private final bytesUsed:Lorg/apache/lucene/util/Counter;


# direct methods
.method public constructor <init>(ILorg/apache/lucene/util/Counter;)V
    .locals 0
    .param p1, "blockSize"    # I
    .param p2, "bytesUsed"    # Lorg/apache/lucene/util/Counter;

    .prologue
    .line 96
    invoke-direct {p0, p1}, Lorg/apache/lucene/util/ByteBlockPool$Allocator;-><init>(I)V

    .line 97
    iput-object p2, p0, Lorg/apache/lucene/util/ByteBlockPool$DirectTrackingAllocator;->bytesUsed:Lorg/apache/lucene/util/Counter;

    .line 98
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Counter;)V
    .locals 1
    .param p1, "bytesUsed"    # Lorg/apache/lucene/util/Counter;

    .prologue
    .line 92
    const v0, 0x8000

    invoke-direct {p0, v0, p1}, Lorg/apache/lucene/util/ByteBlockPool$DirectTrackingAllocator;-><init>(ILorg/apache/lucene/util/Counter;)V

    .line 93
    return-void
.end method


# virtual methods
.method public getByteBlock()[B
    .locals 4

    .prologue
    .line 102
    iget-object v0, p0, Lorg/apache/lucene/util/ByteBlockPool$DirectTrackingAllocator;->bytesUsed:Lorg/apache/lucene/util/Counter;

    iget v1, p0, Lorg/apache/lucene/util/ByteBlockPool$DirectTrackingAllocator;->blockSize:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/util/Counter;->addAndGet(J)J

    .line 103
    iget v0, p0, Lorg/apache/lucene/util/ByteBlockPool$DirectTrackingAllocator;->blockSize:I

    new-array v0, v0, [B

    return-object v0
.end method

.method public recycleByteBlocks([[BII)V
    .locals 4
    .param p1, "blocks"    # [[B
    .param p2, "start"    # I
    .param p3, "end"    # I

    .prologue
    .line 108
    iget-object v1, p0, Lorg/apache/lucene/util/ByteBlockPool$DirectTrackingAllocator;->bytesUsed:Lorg/apache/lucene/util/Counter;

    sub-int v2, p3, p2

    iget v3, p0, Lorg/apache/lucene/util/ByteBlockPool$DirectTrackingAllocator;->blockSize:I

    mul-int/2addr v2, v3

    neg-int v2, v2

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/util/Counter;->addAndGet(J)J

    .line 109
    move v0, p2

    .local v0, "i":I
    :goto_0
    if-lt v0, p3, :cond_0

    .line 112
    return-void

    .line 110
    :cond_0
    const/4 v1, 0x0

    aput-object v1, p1, v0

    .line 109
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
