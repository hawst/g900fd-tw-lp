.class final Lorg/apache/lucene/util/fst/ReverseBytesReader;
.super Lorg/apache/lucene/util/fst/FST$BytesReader;
.source "ReverseBytesReader.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/util/fst/FST$BytesReader;"
    }
.end annotation


# instance fields
.field private final bytes:[B

.field private pos:I


# direct methods
.method public constructor <init>([B)V
    .locals 0
    .param p1, "bytes"    # [B

    .prologue
    .line 25
    invoke-direct {p0}, Lorg/apache/lucene/util/fst/FST$BytesReader;-><init>()V

    .line 26
    iput-object p1, p0, Lorg/apache/lucene/util/fst/ReverseBytesReader;->bytes:[B

    .line 27
    return-void
.end method


# virtual methods
.method public getPosition()J
    .locals 2

    .prologue
    .line 48
    iget v0, p0, Lorg/apache/lucene/util/fst/ReverseBytesReader;->pos:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public readByte()B
    .locals 3

    .prologue
    .line 31
    iget-object v0, p0, Lorg/apache/lucene/util/fst/ReverseBytesReader;->bytes:[B

    iget v1, p0, Lorg/apache/lucene/util/fst/ReverseBytesReader;->pos:I

    add-int/lit8 v2, v1, -0x1

    iput v2, p0, Lorg/apache/lucene/util/fst/ReverseBytesReader;->pos:I

    aget-byte v0, v0, v1

    return v0
.end method

.method public readBytes([BII)V
    .locals 5
    .param p1, "b"    # [B
    .param p2, "offset"    # I
    .param p3, "len"    # I

    .prologue
    .line 36
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, p3, :cond_0

    .line 39
    return-void

    .line 37
    :cond_0
    add-int v1, p2, v0

    iget-object v2, p0, Lorg/apache/lucene/util/fst/ReverseBytesReader;->bytes:[B

    iget v3, p0, Lorg/apache/lucene/util/fst/ReverseBytesReader;->pos:I

    add-int/lit8 v4, v3, -0x1

    iput v4, p0, Lorg/apache/lucene/util/fst/ReverseBytesReader;->pos:I

    aget-byte v2, v2, v3

    aput-byte v2, p1, v1

    .line 36
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public reversed()Z
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x1

    return v0
.end method

.method public setPosition(J)V
    .locals 1
    .param p1, "pos"    # J

    .prologue
    .line 53
    long-to-int v0, p1

    iput v0, p0, Lorg/apache/lucene/util/fst/ReverseBytesReader;->pos:I

    .line 54
    return-void
.end method

.method public skipBytes(I)V
    .locals 1
    .param p1, "count"    # I

    .prologue
    .line 43
    iget v0, p0, Lorg/apache/lucene/util/fst/ReverseBytesReader;->pos:I

    sub-int/2addr v0, p1

    iput v0, p0, Lorg/apache/lucene/util/fst/ReverseBytesReader;->pos:I

    .line 44
    return-void
.end method
