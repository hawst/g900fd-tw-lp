.class final Lorg/apache/lucene/util/packed/DirectPacked64SingleBlockReader;
.super Lorg/apache/lucene/util/packed/PackedInts$ReaderImpl;
.source "DirectPacked64SingleBlockReader.java"


# instance fields
.field private final in:Lorg/apache/lucene/store/IndexInput;

.field private final mask:J

.field private final startPointer:J

.field private final valuesPerBlock:I


# direct methods
.method constructor <init>(IILorg/apache/lucene/store/IndexInput;)V
    .locals 4
    .param p1, "bitsPerValue"    # I
    .param p2, "valueCount"    # I
    .param p3, "in"    # Lorg/apache/lucene/store/IndexInput;

    .prologue
    const-wide/16 v2, -0x1

    .line 33
    invoke-direct {p0, p2, p1}, Lorg/apache/lucene/util/packed/PackedInts$ReaderImpl;-><init>(II)V

    .line 34
    iput-object p3, p0, Lorg/apache/lucene/util/packed/DirectPacked64SingleBlockReader;->in:Lorg/apache/lucene/store/IndexInput;

    .line 35
    invoke-virtual {p3}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/lucene/util/packed/DirectPacked64SingleBlockReader;->startPointer:J

    .line 36
    const/16 v0, 0x40

    div-int/2addr v0, p1

    iput v0, p0, Lorg/apache/lucene/util/packed/DirectPacked64SingleBlockReader;->valuesPerBlock:I

    .line 37
    shl-long v0, v2, p1

    xor-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/lucene/util/packed/DirectPacked64SingleBlockReader;->mask:J

    .line 38
    return-void
.end method


# virtual methods
.method public get(I)J
    .locals 12
    .param p1, "index"    # I

    .prologue
    .line 42
    iget v5, p0, Lorg/apache/lucene/util/packed/DirectPacked64SingleBlockReader;->valuesPerBlock:I

    div-int v2, p1, v5

    .line 43
    .local v2, "blockOffset":I
    int-to-long v8, v2

    const/4 v5, 0x3

    shl-long v6, v8, v5

    .line 45
    .local v6, "skip":J
    :try_start_0
    iget-object v5, p0, Lorg/apache/lucene/util/packed/DirectPacked64SingleBlockReader;->in:Lorg/apache/lucene/store/IndexInput;

    iget-wide v8, p0, Lorg/apache/lucene/util/packed/DirectPacked64SingleBlockReader;->startPointer:J

    add-long/2addr v8, v6

    invoke-virtual {v5, v8, v9}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 47
    iget-object v5, p0, Lorg/apache/lucene/util/packed/DirectPacked64SingleBlockReader;->in:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v5}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v0

    .line 48
    .local v0, "block":J
    iget v5, p0, Lorg/apache/lucene/util/packed/DirectPacked64SingleBlockReader;->valuesPerBlock:I

    rem-int v4, p1, v5

    .line 49
    .local v4, "offsetInBlock":I
    iget v5, p0, Lorg/apache/lucene/util/packed/DirectPacked64SingleBlockReader;->bitsPerValue:I

    mul-int/2addr v5, v4

    ushr-long v8, v0, v5

    iget-wide v10, p0, Lorg/apache/lucene/util/packed/DirectPacked64SingleBlockReader;->mask:J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    and-long/2addr v8, v10

    return-wide v8

    .line 50
    .end local v0    # "block":J
    .end local v4    # "offsetInBlock":I
    :catch_0
    move-exception v3

    .line 51
    .local v3, "e":Ljava/io/IOException;
    new-instance v5, Ljava/lang/IllegalStateException;

    const-string v8, "failed"

    invoke-direct {v5, v8, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5
.end method

.method public ramBytesUsed()J
    .locals 2

    .prologue
    .line 57
    const-wide/16 v0, 0x0

    return-wide v0
.end method
