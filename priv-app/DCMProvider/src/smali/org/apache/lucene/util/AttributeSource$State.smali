.class public final Lorg/apache/lucene/util/AttributeSource$State;
.super Ljava/lang/Object;
.source "AttributeSource.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/AttributeSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "State"
.end annotation


# instance fields
.field attribute:Lorg/apache/lucene/util/AttributeImpl;

.field next:Lorg/apache/lucene/util/AttributeSource$State;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/util/AttributeSource$State;->clone()Lorg/apache/lucene/util/AttributeSource$State;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lorg/apache/lucene/util/AttributeSource$State;
    .locals 2

    .prologue
    .line 105
    new-instance v0, Lorg/apache/lucene/util/AttributeSource$State;

    invoke-direct {v0}, Lorg/apache/lucene/util/AttributeSource$State;-><init>()V

    .line 106
    .local v0, "clone":Lorg/apache/lucene/util/AttributeSource$State;
    iget-object v1, p0, Lorg/apache/lucene/util/AttributeSource$State;->attribute:Lorg/apache/lucene/util/AttributeImpl;

    invoke-virtual {v1}, Lorg/apache/lucene/util/AttributeImpl;->clone()Lorg/apache/lucene/util/AttributeImpl;

    move-result-object v1

    iput-object v1, v0, Lorg/apache/lucene/util/AttributeSource$State;->attribute:Lorg/apache/lucene/util/AttributeImpl;

    .line 108
    iget-object v1, p0, Lorg/apache/lucene/util/AttributeSource$State;->next:Lorg/apache/lucene/util/AttributeSource$State;

    if-eqz v1, :cond_0

    .line 109
    iget-object v1, p0, Lorg/apache/lucene/util/AttributeSource$State;->next:Lorg/apache/lucene/util/AttributeSource$State;

    invoke-virtual {v1}, Lorg/apache/lucene/util/AttributeSource$State;->clone()Lorg/apache/lucene/util/AttributeSource$State;

    move-result-object v1

    iput-object v1, v0, Lorg/apache/lucene/util/AttributeSource$State;->next:Lorg/apache/lucene/util/AttributeSource$State;

    .line 112
    :cond_0
    return-object v0
.end method
