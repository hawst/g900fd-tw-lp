.class abstract Lorg/apache/lucene/util/automaton/LevenshteinAutomata$ParametricDescription;
.super Ljava/lang/Object;
.source "LevenshteinAutomata.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/automaton/LevenshteinAutomata;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x408
    name = "ParametricDescription"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final MASKS:[J


# instance fields
.field private final minErrors:[I

.field protected final n:I

.field protected final w:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 208
    const-class v0, Lorg/apache/lucene/util/automaton/LevenshteinAutomata;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/automaton/LevenshteinAutomata$ParametricDescription;->$assertionsDisabled:Z

    .line 251
    const/16 v0, 0x3f

    new-array v0, v0, [J

    fill-array-data v0, :array_0

    sput-object v0, Lorg/apache/lucene/util/automaton/LevenshteinAutomata$ParametricDescription;->MASKS:[J

    .line 266
    return-void

    .line 208
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 251
    nop

    :array_0
    .array-data 8
        0x1
        0x3
        0x7
        0xf
        0x1f
        0x3f
        0x7f
        0xff
        0x1ff
        0x3ff
        0x7ff
        0xfff
        0x1fff
        0x3fff
        0x7fff
        0xffff
        0x1ffff
        0x3ffff
        0x7ffff
        0xfffff
        0x1fffff
        0x3fffff
        0x7fffff
        0xffffff
        0x1ffffff
        0x3ffffff
        0x7ffffff
        0xfffffff
        0x1fffffff
        0x3fffffff
        0x7fffffff
        0xffffffffL
        0x1ffffffffL
        0x3ffffffffL
        0x7ffffffffL
        0xfffffffffL
        0x1fffffffffL
        0x3fffffffffL
        0x7fffffffffL
        0xffffffffffL
        0x1ffffffffffL
        0x3ffffffffffL
        0x7ffffffffffL
        0xfffffffffffL
        0x1fffffffffffL
        0x3fffffffffffL
        0x7fffffffffffL
        0xffffffffffffL
        0x1ffffffffffffL
        0x3ffffffffffffL    # 5.562684646268E-309
        0x7ffffffffffffL
        0xfffffffffffffL
        0x1fffffffffffffL
        0x3fffffffffffffL
        0x7fffffffffffffL
        0xffffffffffffffL
        0x1ffffffffffffffL    # 4.77830972673648E-299
        0x3ffffffffffffffL
        0x7ffffffffffffffL
        0xfffffffffffffffL
        0x1fffffffffffffffL
        0x3fffffffffffffffL    # 1.9999999999999998
        0x7fffffffffffffffL
    .end array-data
.end method

.method constructor <init>(II[I)V
    .locals 0
    .param p1, "w"    # I
    .param p2, "n"    # I
    .param p3, "minErrors"    # [I

    .prologue
    .line 213
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 214
    iput p1, p0, Lorg/apache/lucene/util/automaton/LevenshteinAutomata$ParametricDescription;->w:I

    .line 215
    iput p2, p0, Lorg/apache/lucene/util/automaton/LevenshteinAutomata$ParametricDescription;->n:I

    .line 216
    iput-object p3, p0, Lorg/apache/lucene/util/automaton/LevenshteinAutomata$ParametricDescription;->minErrors:[I

    .line 217
    return-void
.end method


# virtual methods
.method getPosition(I)I
    .locals 1
    .param p1, "absState"    # I

    .prologue
    .line 242
    iget v0, p0, Lorg/apache/lucene/util/automaton/LevenshteinAutomata$ParametricDescription;->w:I

    add-int/lit8 v0, v0, 0x1

    rem-int v0, p1, v0

    return v0
.end method

.method isAccept(I)Z
    .locals 4
    .param p1, "absState"    # I

    .prologue
    .line 231
    iget v2, p0, Lorg/apache/lucene/util/automaton/LevenshteinAutomata$ParametricDescription;->w:I

    add-int/lit8 v2, v2, 0x1

    div-int v1, p1, v2

    .line 232
    .local v1, "state":I
    iget v2, p0, Lorg/apache/lucene/util/automaton/LevenshteinAutomata$ParametricDescription;->w:I

    add-int/lit8 v2, v2, 0x1

    rem-int v0, p1, v2

    .line 233
    .local v0, "offset":I
    sget-boolean v2, Lorg/apache/lucene/util/automaton/LevenshteinAutomata$ParametricDescription;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    if-gez v0, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 234
    :cond_0
    iget v2, p0, Lorg/apache/lucene/util/automaton/LevenshteinAutomata$ParametricDescription;->w:I

    sub-int/2addr v2, v0

    iget-object v3, p0, Lorg/apache/lucene/util/automaton/LevenshteinAutomata$ParametricDescription;->minErrors:[I

    aget v3, v3, v1

    add-int/2addr v2, v3

    iget v3, p0, Lorg/apache/lucene/util/automaton/LevenshteinAutomata$ParametricDescription;->n:I

    if-gt v2, v3, :cond_1

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method size()I
    .locals 2

    .prologue
    .line 223
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/LevenshteinAutomata$ParametricDescription;->minErrors:[I

    array-length v0, v0

    iget v1, p0, Lorg/apache/lucene/util/automaton/LevenshteinAutomata$ParametricDescription;->w:I

    add-int/lit8 v1, v1, 0x1

    mul-int/2addr v0, v1

    return v0
.end method

.method abstract transition(III)I
.end method

.method protected unpack([JII)I
    .locals 12
    .param p1, "data"    # [J
    .param p2, "index"    # I
    .param p3, "bitsPerValue"    # I

    .prologue
    .line 269
    mul-int v5, p3, p2

    int-to-long v0, v5

    .line 270
    .local v0, "bitLoc":J
    const/4 v5, 0x6

    shr-long v6, v0, v5

    long-to-int v3, v6

    .line 271
    .local v3, "dataLoc":I
    const-wide/16 v6, 0x3f

    and-long/2addr v6, v0

    long-to-int v2, v6

    .line 273
    .local v2, "bitStart":I
    add-int v5, v2, p3

    const/16 v6, 0x40

    if-gt v5, v6, :cond_0

    .line 275
    aget-wide v6, p1, v3

    shr-long/2addr v6, v2

    sget-object v5, Lorg/apache/lucene/util/automaton/LevenshteinAutomata$ParametricDescription;->MASKS:[J

    add-int/lit8 v8, p3, -0x1

    aget-wide v8, v5, v8

    and-long/2addr v6, v8

    long-to-int v5, v6

    .line 279
    :goto_0
    return v5

    .line 278
    :cond_0
    rsub-int/lit8 v4, v2, 0x40

    .line 279
    .local v4, "part":I
    aget-wide v6, p1, v3

    shr-long/2addr v6, v2

    sget-object v5, Lorg/apache/lucene/util/automaton/LevenshteinAutomata$ParametricDescription;->MASKS:[J

    add-int/lit8 v8, v4, -0x1

    aget-wide v8, v5, v8

    and-long/2addr v6, v8

    .line 280
    add-int/lit8 v5, v3, 0x1

    aget-wide v8, p1, v5

    sget-object v5, Lorg/apache/lucene/util/automaton/LevenshteinAutomata$ParametricDescription;->MASKS:[J

    sub-int v10, p3, v4

    add-int/lit8 v10, v10, -0x1

    aget-wide v10, v5, v10

    and-long/2addr v8, v10

    shl-long/2addr v8, v4

    .line 279
    add-long/2addr v6, v8

    long-to-int v5, v6

    goto :goto_0
.end method
