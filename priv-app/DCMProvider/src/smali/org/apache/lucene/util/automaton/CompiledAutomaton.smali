.class public Lorg/apache/lucene/util/automaton/CompiledAutomaton;
.super Ljava/lang/Object;
.source "CompiledAutomaton.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$org$apache$lucene$util$automaton$CompiledAutomaton$AUTOMATON_TYPE:[I

.field static final synthetic $assertionsDisabled:Z


# instance fields
.field public final commonSuffixRef:Lorg/apache/lucene/util/BytesRef;

.field public final finite:Ljava/lang/Boolean;

.field public final runAutomaton:Lorg/apache/lucene/util/automaton/ByteRunAutomaton;

.field public final sortedTransitions:[[Lorg/apache/lucene/util/automaton/Transition;

.field public final term:Lorg/apache/lucene/util/BytesRef;

.field public final type:Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;


# direct methods
.method static synthetic $SWITCH_TABLE$org$apache$lucene$util$automaton$CompiledAutomaton$AUTOMATON_TYPE()[I
    .locals 3

    .prologue
    .line 37
    sget-object v0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->$SWITCH_TABLE$org$apache$lucene$util$automaton$CompiledAutomaton$AUTOMATON_TYPE:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;->values()[Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;->ALL:Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;

    invoke-virtual {v1}, Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_4

    :goto_1
    :try_start_1
    sget-object v1, Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;->NONE:Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;

    invoke-virtual {v1}, Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_3

    :goto_2
    :try_start_2
    sget-object v1, Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;->NORMAL:Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;

    invoke-virtual {v1}, Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :goto_3
    :try_start_3
    sget-object v1, Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;->PREFIX:Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;

    invoke-virtual {v1}, Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_1

    :goto_4
    :try_start_4
    sget-object v1, Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;->SINGLE:Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;

    invoke-virtual {v1}, Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_0

    :goto_5
    sput-object v0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->$SWITCH_TABLE$org$apache$lucene$util$automaton$CompiledAutomaton$AUTOMATON_TYPE:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_5

    :catch_1
    move-exception v1

    goto :goto_4

    :catch_2
    move-exception v1

    goto :goto_3

    :catch_3
    move-exception v1

    goto :goto_2

    :catch_4
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/util/automaton/Automaton;)V
    .locals 2
    .param p1, "automaton"    # Lorg/apache/lucene/util/automaton/Automaton;

    .prologue
    .line 90
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Lorg/apache/lucene/util/automaton/CompiledAutomaton;-><init>(Lorg/apache/lucene/util/automaton/Automaton;Ljava/lang/Boolean;Z)V

    .line 91
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/automaton/Automaton;Ljava/lang/Boolean;Z)V
    .locals 6
    .param p1, "automaton"    # Lorg/apache/lucene/util/automaton/Automaton;
    .param p2, "finite"    # Ljava/lang/Boolean;
    .param p3, "simplify"    # Z

    .prologue
    const/4 v5, 0x0

    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    if-eqz p3, :cond_5

    .line 99
    invoke-static {p1}, Lorg/apache/lucene/util/automaton/BasicOperations;->isEmpty(Lorg/apache/lucene/util/automaton/Automaton;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 101
    sget-object v3, Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;->NONE:Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;

    iput-object v3, p0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->type:Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;

    .line 102
    iput-object v5, p0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->term:Lorg/apache/lucene/util/BytesRef;

    .line 103
    iput-object v5, p0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->commonSuffixRef:Lorg/apache/lucene/util/BytesRef;

    .line 104
    iput-object v5, p0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->runAutomaton:Lorg/apache/lucene/util/automaton/ByteRunAutomaton;

    .line 105
    iput-object v5, p0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->sortedTransitions:[[Lorg/apache/lucene/util/automaton/Transition;

    .line 106
    iput-object v5, p0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->finite:Ljava/lang/Boolean;

    .line 171
    :goto_0
    return-void

    .line 108
    :cond_0
    invoke-static {p1}, Lorg/apache/lucene/util/automaton/BasicOperations;->isTotal(Lorg/apache/lucene/util/automaton/Automaton;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 110
    sget-object v3, Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;->ALL:Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;

    iput-object v3, p0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->type:Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;

    .line 111
    iput-object v5, p0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->term:Lorg/apache/lucene/util/BytesRef;

    .line 112
    iput-object v5, p0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->commonSuffixRef:Lorg/apache/lucene/util/BytesRef;

    .line 113
    iput-object v5, p0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->runAutomaton:Lorg/apache/lucene/util/automaton/ByteRunAutomaton;

    .line 114
    iput-object v5, p0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->sortedTransitions:[[Lorg/apache/lucene/util/automaton/Transition;

    .line 115
    iput-object v5, p0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->finite:Ljava/lang/Boolean;

    goto :goto_0

    .line 120
    :cond_1
    invoke-virtual {p1}, Lorg/apache/lucene/util/automaton/Automaton;->getSingleton()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_3

    .line 121
    invoke-static {p1}, Lorg/apache/lucene/util/automaton/SpecialOperations;->getCommonPrefix(Lorg/apache/lucene/util/automaton/Automaton;)Ljava/lang/String;

    move-result-object v0

    .line 122
    .local v0, "commonPrefix":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_2

    invoke-static {v0}, Lorg/apache/lucene/util/automaton/BasicAutomata;->makeString(Ljava/lang/String;)Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v3

    invoke-static {p1, v3}, Lorg/apache/lucene/util/automaton/BasicOperations;->sameLanguage(Lorg/apache/lucene/util/automaton/Automaton;Lorg/apache/lucene/util/automaton/Automaton;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 123
    move-object v1, v0

    .line 132
    .local v1, "singleton":Ljava/lang/String;
    :goto_1
    if-eqz v1, :cond_4

    .line 135
    sget-object v3, Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;->SINGLE:Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;

    iput-object v3, p0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->type:Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;

    .line 136
    new-instance v3, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v3, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    iput-object v3, p0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->term:Lorg/apache/lucene/util/BytesRef;

    .line 137
    iput-object v5, p0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->commonSuffixRef:Lorg/apache/lucene/util/BytesRef;

    .line 138
    iput-object v5, p0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->runAutomaton:Lorg/apache/lucene/util/automaton/ByteRunAutomaton;

    .line 139
    iput-object v5, p0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->sortedTransitions:[[Lorg/apache/lucene/util/automaton/Transition;

    .line 140
    iput-object v5, p0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->finite:Ljava/lang/Boolean;

    goto :goto_0

    .line 125
    .end local v1    # "singleton":Ljava/lang/String;
    :cond_2
    const/4 v1, 0x0

    .line 127
    .restart local v1    # "singleton":Ljava/lang/String;
    goto :goto_1

    .line 128
    .end local v0    # "commonPrefix":Ljava/lang/String;
    .end local v1    # "singleton":Ljava/lang/String;
    :cond_3
    const/4 v0, 0x0

    .line 129
    .restart local v0    # "commonPrefix":Ljava/lang/String;
    invoke-virtual {p1}, Lorg/apache/lucene/util/automaton/Automaton;->getSingleton()Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "singleton":Ljava/lang/String;
    goto :goto_1

    .line 143
    :cond_4
    invoke-static {v0}, Lorg/apache/lucene/util/automaton/BasicAutomata;->makeString(Ljava/lang/String;)Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v3

    invoke-static {}, Lorg/apache/lucene/util/automaton/BasicAutomata;->makeAnyString()Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v4

    .line 142
    invoke-static {v3, v4}, Lorg/apache/lucene/util/automaton/BasicOperations;->concatenate(Lorg/apache/lucene/util/automaton/Automaton;Lorg/apache/lucene/util/automaton/Automaton;)Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v3

    invoke-static {p1, v3}, Lorg/apache/lucene/util/automaton/BasicOperations;->sameLanguage(Lorg/apache/lucene/util/automaton/Automaton;Lorg/apache/lucene/util/automaton/Automaton;)Z

    move-result v3

    .line 143
    if-eqz v3, :cond_5

    .line 145
    sget-object v3, Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;->PREFIX:Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;

    iput-object v3, p0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->type:Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;

    .line 146
    new-instance v3, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v3, v0}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    iput-object v3, p0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->term:Lorg/apache/lucene/util/BytesRef;

    .line 147
    iput-object v5, p0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->commonSuffixRef:Lorg/apache/lucene/util/BytesRef;

    .line 148
    iput-object v5, p0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->runAutomaton:Lorg/apache/lucene/util/automaton/ByteRunAutomaton;

    .line 149
    iput-object v5, p0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->sortedTransitions:[[Lorg/apache/lucene/util/automaton/Transition;

    .line 150
    iput-object v5, p0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->finite:Ljava/lang/Boolean;

    goto :goto_0

    .line 156
    .end local v0    # "commonPrefix":Ljava/lang/String;
    .end local v1    # "singleton":Ljava/lang/String;
    :cond_5
    sget-object v3, Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;->NORMAL:Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;

    iput-object v3, p0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->type:Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;

    .line 157
    iput-object v5, p0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->term:Lorg/apache/lucene/util/BytesRef;

    .line 158
    if-nez p2, :cond_6

    .line 159
    invoke-static {p1}, Lorg/apache/lucene/util/automaton/SpecialOperations;->isFinite(Lorg/apache/lucene/util/automaton/Automaton;)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->finite:Ljava/lang/Boolean;

    .line 163
    :goto_2
    new-instance v3, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;

    invoke-direct {v3}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;-><init>()V

    invoke-virtual {v3, p1}, Lorg/apache/lucene/util/automaton/UTF32ToUTF8;->convert(Lorg/apache/lucene/util/automaton/Automaton;)Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v2

    .line 164
    .local v2, "utf8":Lorg/apache/lucene/util/automaton/Automaton;
    iget-object v3, p0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->finite:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 165
    iput-object v5, p0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->commonSuffixRef:Lorg/apache/lucene/util/BytesRef;

    .line 169
    :goto_3
    new-instance v3, Lorg/apache/lucene/util/automaton/ByteRunAutomaton;

    const/4 v4, 0x1

    invoke-direct {v3, v2, v4}, Lorg/apache/lucene/util/automaton/ByteRunAutomaton;-><init>(Lorg/apache/lucene/util/automaton/Automaton;Z)V

    iput-object v3, p0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->runAutomaton:Lorg/apache/lucene/util/automaton/ByteRunAutomaton;

    .line 170
    invoke-virtual {v2}, Lorg/apache/lucene/util/automaton/Automaton;->getSortedTransitions()[[Lorg/apache/lucene/util/automaton/Transition;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->sortedTransitions:[[Lorg/apache/lucene/util/automaton/Transition;

    goto/16 :goto_0

    .line 161
    .end local v2    # "utf8":Lorg/apache/lucene/util/automaton/Automaton;
    :cond_6
    iput-object p2, p0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->finite:Ljava/lang/Boolean;

    goto :goto_2

    .line 167
    .restart local v2    # "utf8":Lorg/apache/lucene/util/automaton/Automaton;
    :cond_7
    invoke-static {v2}, Lorg/apache/lucene/util/automaton/SpecialOperations;->getCommonSuffixBytesRef(Lorg/apache/lucene/util/automaton/Automaton;)Lorg/apache/lucene/util/BytesRef;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->commonSuffixRef:Lorg/apache/lucene/util/BytesRef;

    goto :goto_3
.end method

.method private addTail(ILorg/apache/lucene/util/BytesRef;II)Lorg/apache/lucene/util/BytesRef;
    .locals 9
    .param p1, "state"    # I
    .param p2, "term"    # Lorg/apache/lucene/util/BytesRef;
    .param p3, "idx"    # I
    .param p4, "leadLabel"    # I

    .prologue
    .line 179
    const/4 v2, 0x0

    .line 180
    .local v2, "maxTransition":Lorg/apache/lucene/util/automaton/Transition;
    iget-object v5, p0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->sortedTransitions:[[Lorg/apache/lucene/util/automaton/Transition;

    aget-object v6, v5, p1

    array-length v7, v6

    const/4 v5, 0x0

    :goto_0
    if-lt v5, v7, :cond_0

    .line 186
    sget-boolean v5, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->$assertionsDisabled:Z

    if-nez v5, :cond_2

    if-nez v2, :cond_2

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 180
    :cond_0
    aget-object v3, v6, v5

    .line 181
    .local v3, "transition":Lorg/apache/lucene/util/automaton/Transition;
    iget v8, v3, Lorg/apache/lucene/util/automaton/Transition;->min:I

    if-ge v8, p4, :cond_1

    .line 182
    move-object v2, v3

    .line 180
    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 190
    .end local v3    # "transition":Lorg/apache/lucene/util/automaton/Transition;
    :cond_2
    iget v5, v2, Lorg/apache/lucene/util/automaton/Transition;->max:I

    add-int/lit8 v6, p4, -0x1

    if-le v5, v6, :cond_4

    .line 191
    add-int/lit8 v0, p4, -0x1

    .line 195
    .local v0, "floorLabel":I
    :goto_1
    iget-object v5, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    array-length v5, v5

    if-lt p3, v5, :cond_3

    .line 196
    add-int/lit8 v5, p3, 0x1

    invoke-virtual {p2, v5}, Lorg/apache/lucene/util/BytesRef;->grow(I)V

    .line 199
    :cond_3
    iget-object v5, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    int-to-byte v6, v0

    aput-byte v6, v5, p3

    .line 201
    iget-object v5, v2, Lorg/apache/lucene/util/automaton/Transition;->to:Lorg/apache/lucene/util/automaton/State;

    invoke-virtual {v5}, Lorg/apache/lucene/util/automaton/State;->getNumber()I

    move-result p1

    .line 202
    add-int/lit8 p3, p3, 0x1

    .line 206
    :goto_2
    iget-object v5, p0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->sortedTransitions:[[Lorg/apache/lucene/util/automaton/Transition;

    aget-object v4, v5, p1

    .line 207
    .local v4, "transitions":[Lorg/apache/lucene/util/automaton/Transition;
    array-length v5, v4

    if-nez v5, :cond_6

    .line 208
    sget-boolean v5, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->$assertionsDisabled:Z

    if-nez v5, :cond_5

    iget-object v5, p0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->runAutomaton:Lorg/apache/lucene/util/automaton/ByteRunAutomaton;

    invoke-virtual {v5, p1}, Lorg/apache/lucene/util/automaton/ByteRunAutomaton;->isAccept(I)Z

    move-result v5

    if-nez v5, :cond_5

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 193
    .end local v0    # "floorLabel":I
    .end local v4    # "transitions":[Lorg/apache/lucene/util/automaton/Transition;
    :cond_4
    iget v0, v2, Lorg/apache/lucene/util/automaton/Transition;->max:I

    .restart local v0    # "floorLabel":I
    goto :goto_1

    .line 209
    .restart local v4    # "transitions":[Lorg/apache/lucene/util/automaton/Transition;
    :cond_5
    iput p3, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 211
    return-object p2

    .line 215
    :cond_6
    sget-boolean v5, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->$assertionsDisabled:Z

    if-nez v5, :cond_7

    array-length v5, v4

    if-nez v5, :cond_7

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 216
    :cond_7
    array-length v5, v4

    add-int/lit8 v5, v5, -0x1

    aget-object v1, v4, v5

    .line 217
    .local v1, "lastTransition":Lorg/apache/lucene/util/automaton/Transition;
    iget-object v5, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    array-length v5, v5

    if-lt p3, v5, :cond_8

    .line 218
    add-int/lit8 v5, p3, 0x1

    invoke-virtual {p2, v5}, Lorg/apache/lucene/util/BytesRef;->grow(I)V

    .line 221
    :cond_8
    iget-object v5, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v6, v1, Lorg/apache/lucene/util/automaton/Transition;->max:I

    int-to-byte v6, v6

    aput-byte v6, v5, p3

    .line 222
    iget-object v5, v1, Lorg/apache/lucene/util/automaton/Transition;->to:Lorg/apache/lucene/util/automaton/State;

    invoke-virtual {v5}, Lorg/apache/lucene/util/automaton/State;->getNumber()I

    move-result p1

    .line 223
    add-int/lit8 p3, p3, 0x1

    .line 205
    goto :goto_2
.end method


# virtual methods
.method public floor(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/BytesRef;
    .locals 11
    .param p1, "input"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "output"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    const/4 v7, 0x0

    const/4 v10, -0x1

    const/4 v9, 0x0

    .line 260
    iput v9, p2, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 263
    iget-object v6, p0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->runAutomaton:Lorg/apache/lucene/util/automaton/ByteRunAutomaton;

    invoke-virtual {v6}, Lorg/apache/lucene/util/automaton/ByteRunAutomaton;->getInitialState()I

    move-result v4

    .line 266
    .local v4, "state":I
    iget v6, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    if-nez v6, :cond_1

    .line 267
    iget-object v6, p0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->runAutomaton:Lorg/apache/lucene/util/automaton/ByteRunAutomaton;

    invoke-virtual {v6, v4}, Lorg/apache/lucene/util/automaton/ByteRunAutomaton;->isAccept(I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 268
    iput v9, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 335
    .end local p2    # "output":Lorg/apache/lucene/util/BytesRef;
    :goto_0
    return-object p2

    .restart local p2    # "output":Lorg/apache/lucene/util/BytesRef;
    :cond_0
    move-object p2, v7

    .line 271
    goto :goto_0

    .line 275
    :cond_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 277
    .local v3, "stack":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    const/4 v0, 0x0

    .line 279
    .local v0, "idx":I
    :goto_1
    iget-object v6, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v8, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    add-int/2addr v8, v0

    aget-byte v6, v6, v8

    and-int/lit16 v1, v6, 0xff

    .line 280
    .local v1, "label":I
    iget-object v6, p0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->runAutomaton:Lorg/apache/lucene/util/automaton/ByteRunAutomaton;

    invoke-virtual {v6, v4, v1}, Lorg/apache/lucene/util/automaton/ByteRunAutomaton;->step(II)I

    move-result v2

    .line 283
    .local v2, "nextState":I
    iget v6, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v6, v6, -0x1

    if-ne v0, v6, :cond_4

    .line 284
    if-eq v2, v10, :cond_3

    iget-object v6, p0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->runAutomaton:Lorg/apache/lucene/util/automaton/ByteRunAutomaton;

    invoke-virtual {v6, v2}, Lorg/apache/lucene/util/automaton/ByteRunAutomaton;->isAccept(I)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 286
    iget-object v6, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    array-length v6, v6

    if-lt v0, v6, :cond_2

    .line 287
    add-int/lit8 v6, v0, 0x1

    invoke-virtual {p2, v6}, Lorg/apache/lucene/util/BytesRef;->grow(I)V

    .line 289
    :cond_2
    iget-object v6, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    int-to-byte v7, v1

    aput-byte v7, v6, v0

    .line 290
    iget v6, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    iput v6, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    goto :goto_0

    .line 294
    :cond_3
    const/4 v2, -0x1

    .line 298
    :cond_4
    if-ne v2, v10, :cond_a

    .line 303
    :goto_2
    iget-object v6, p0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->sortedTransitions:[[Lorg/apache/lucene/util/automaton/Transition;

    aget-object v5, v6, v4

    .line 304
    .local v5, "transitions":[Lorg/apache/lucene/util/automaton/Transition;
    array-length v6, v5

    if-nez v6, :cond_6

    .line 305
    sget-boolean v6, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->$assertionsDisabled:Z

    if-nez v6, :cond_5

    iget-object v6, p0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->runAutomaton:Lorg/apache/lucene/util/automaton/ByteRunAutomaton;

    invoke-virtual {v6, v4}, Lorg/apache/lucene/util/automaton/ByteRunAutomaton;->isAccept(I)Z

    move-result v6

    if-nez v6, :cond_5

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 306
    :cond_5
    iput v0, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    goto :goto_0

    .line 309
    :cond_6
    add-int/lit8 v6, v1, -0x1

    aget-object v8, v5, v9

    iget v8, v8, Lorg/apache/lucene/util/automaton/Transition;->min:I

    if-ge v6, v8, :cond_9

    .line 311
    iget-object v6, p0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->runAutomaton:Lorg/apache/lucene/util/automaton/ByteRunAutomaton;

    invoke-virtual {v6, v4}, Lorg/apache/lucene/util/automaton/ByteRunAutomaton;->isAccept(I)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 312
    iput v0, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    goto :goto_0

    .line 317
    :cond_7
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v6

    if-nez v6, :cond_8

    move-object p2, v7

    .line 319
    goto :goto_0

    .line 321
    :cond_8
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-interface {v3, v6}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 322
    add-int/lit8 v0, v0, -0x1

    .line 324
    iget-object v6, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v8, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    add-int/2addr v8, v0

    aget-byte v6, v6, v8

    and-int/lit16 v1, v6, 0xff

    .line 302
    goto :goto_2

    .line 335
    :cond_9
    invoke-direct {p0, v4, p2, v0, v1}, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->addTail(ILorg/apache/lucene/util/BytesRef;II)Lorg/apache/lucene/util/BytesRef;

    move-result-object p2

    goto/16 :goto_0

    .line 338
    .end local v5    # "transitions":[Lorg/apache/lucene/util/automaton/Transition;
    :cond_a
    iget-object v6, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    array-length v6, v6

    if-lt v0, v6, :cond_b

    .line 339
    add-int/lit8 v6, v0, 0x1

    invoke-virtual {p2, v6}, Lorg/apache/lucene/util/BytesRef;->grow(I)V

    .line 341
    :cond_b
    iget-object v6, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    int-to-byte v8, v1

    aput-byte v8, v6, v0

    .line 342
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 343
    move v4, v2

    .line 344
    add-int/lit8 v0, v0, 0x1

    .line 278
    goto/16 :goto_1
.end method

.method public getTermsEnum(Lorg/apache/lucene/index/Terms;)Lorg/apache/lucene/index/TermsEnum;
    .locals 3
    .param p1, "terms"    # Lorg/apache/lucene/index/Terms;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 232
    invoke-static {}, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->$SWITCH_TABLE$org$apache$lucene$util$automaton$CompiledAutomaton$AUTOMATON_TYPE()[I

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->type:Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;

    invoke-virtual {v1}, Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 247
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "unhandled case"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 234
    :pswitch_0
    sget-object v0, Lorg/apache/lucene/index/TermsEnum;->EMPTY:Lorg/apache/lucene/index/TermsEnum;

    .line 244
    :goto_0
    return-object v0

    .line 236
    :pswitch_1
    invoke-virtual {p1, v2}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v0

    goto :goto_0

    .line 238
    :pswitch_2
    new-instance v0, Lorg/apache/lucene/index/SingleTermsEnum;

    invoke-virtual {p1, v2}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->term:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/index/SingleTermsEnum;-><init>(Lorg/apache/lucene/index/TermsEnum;Lorg/apache/lucene/util/BytesRef;)V

    goto :goto_0

    .line 242
    :pswitch_3
    new-instance v0, Lorg/apache/lucene/search/PrefixTermsEnum;

    invoke-virtual {p1, v2}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->term:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/search/PrefixTermsEnum;-><init>(Lorg/apache/lucene/index/TermsEnum;Lorg/apache/lucene/util/BytesRef;)V

    goto :goto_0

    .line 244
    :pswitch_4
    invoke-virtual {p1, p0, v2}, Lorg/apache/lucene/index/Terms;->intersect(Lorg/apache/lucene/util/automaton/CompiledAutomaton;Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v0

    goto :goto_0

    .line 232
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
