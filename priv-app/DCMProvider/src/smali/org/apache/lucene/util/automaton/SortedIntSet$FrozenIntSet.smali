.class public final Lorg/apache/lucene/util/automaton/SortedIntSet$FrozenIntSet;
.super Ljava/lang/Object;
.source "SortedIntSet.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/automaton/SortedIntSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FrozenIntSet"
.end annotation


# instance fields
.field final hashCode:I

.field final state:Lorg/apache/lucene/util/automaton/State;

.field final values:[I


# direct methods
.method public constructor <init>(ILorg/apache/lucene/util/automaton/State;)V
    .locals 2
    .param p1, "num"    # I
    .param p2, "state"    # Lorg/apache/lucene/util/automaton/State;

    .prologue
    .line 215
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 216
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    aput p1, v0, v1

    iput-object v0, p0, Lorg/apache/lucene/util/automaton/SortedIntSet$FrozenIntSet;->values:[I

    .line 217
    iput-object p2, p0, Lorg/apache/lucene/util/automaton/SortedIntSet$FrozenIntSet;->state:Lorg/apache/lucene/util/automaton/State;

    .line 218
    add-int/lit16 v0, p1, 0x2ab

    iput v0, p0, Lorg/apache/lucene/util/automaton/SortedIntSet$FrozenIntSet;->hashCode:I

    .line 219
    return-void
.end method

.method public constructor <init>([IILorg/apache/lucene/util/automaton/State;)V
    .locals 0
    .param p1, "values"    # [I
    .param p2, "hashCode"    # I
    .param p3, "state"    # Lorg/apache/lucene/util/automaton/State;

    .prologue
    .line 209
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 210
    iput-object p1, p0, Lorg/apache/lucene/util/automaton/SortedIntSet$FrozenIntSet;->values:[I

    .line 211
    iput p2, p0, Lorg/apache/lucene/util/automaton/SortedIntSet$FrozenIntSet;->hashCode:I

    .line 212
    iput-object p3, p0, Lorg/apache/lucene/util/automaton/SortedIntSet$FrozenIntSet;->state:Lorg/apache/lucene/util/automaton/State;

    .line 213
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "_other"    # Ljava/lang/Object;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 228
    if-nez p1, :cond_1

    .line 261
    :cond_0
    :goto_0
    return v2

    .line 231
    :cond_1
    instance-of v4, p1, Lorg/apache/lucene/util/automaton/SortedIntSet$FrozenIntSet;

    if-eqz v4, :cond_3

    move-object v1, p1

    .line 232
    check-cast v1, Lorg/apache/lucene/util/automaton/SortedIntSet$FrozenIntSet;

    .line 233
    .local v1, "other":Lorg/apache/lucene/util/automaton/SortedIntSet$FrozenIntSet;
    iget v4, p0, Lorg/apache/lucene/util/automaton/SortedIntSet$FrozenIntSet;->hashCode:I

    iget v5, v1, Lorg/apache/lucene/util/automaton/SortedIntSet$FrozenIntSet;->hashCode:I

    if-ne v4, v5, :cond_0

    .line 236
    iget-object v4, v1, Lorg/apache/lucene/util/automaton/SortedIntSet$FrozenIntSet;->values:[I

    array-length v4, v4

    iget-object v5, p0, Lorg/apache/lucene/util/automaton/SortedIntSet$FrozenIntSet;->values:[I

    array-length v5, v5

    if-ne v4, v5, :cond_0

    .line 239
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v4, p0, Lorg/apache/lucene/util/automaton/SortedIntSet$FrozenIntSet;->values:[I

    array-length v4, v4

    if-lt v0, v4, :cond_2

    move v2, v3

    .line 244
    goto :goto_0

    .line 240
    :cond_2
    iget-object v4, v1, Lorg/apache/lucene/util/automaton/SortedIntSet$FrozenIntSet;->values:[I

    aget v4, v4, v0

    iget-object v5, p0, Lorg/apache/lucene/util/automaton/SortedIntSet$FrozenIntSet;->values:[I

    aget v5, v5, v0

    if-ne v4, v5, :cond_0

    .line 239
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 245
    .end local v0    # "i":I
    .end local v1    # "other":Lorg/apache/lucene/util/automaton/SortedIntSet$FrozenIntSet;
    :cond_3
    instance-of v4, p1, Lorg/apache/lucene/util/automaton/SortedIntSet;

    if-eqz v4, :cond_0

    move-object v1, p1

    .line 246
    check-cast v1, Lorg/apache/lucene/util/automaton/SortedIntSet;

    .line 247
    .local v1, "other":Lorg/apache/lucene/util/automaton/SortedIntSet;
    iget v4, p0, Lorg/apache/lucene/util/automaton/SortedIntSet$FrozenIntSet;->hashCode:I

    # getter for: Lorg/apache/lucene/util/automaton/SortedIntSet;->hashCode:I
    invoke-static {v1}, Lorg/apache/lucene/util/automaton/SortedIntSet;->access$0(Lorg/apache/lucene/util/automaton/SortedIntSet;)I

    move-result v5

    if-ne v4, v5, :cond_0

    .line 250
    iget-object v4, v1, Lorg/apache/lucene/util/automaton/SortedIntSet;->values:[I

    array-length v4, v4

    iget-object v5, p0, Lorg/apache/lucene/util/automaton/SortedIntSet$FrozenIntSet;->values:[I

    array-length v5, v5

    if-ne v4, v5, :cond_0

    .line 253
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_2
    iget-object v4, p0, Lorg/apache/lucene/util/automaton/SortedIntSet$FrozenIntSet;->values:[I

    array-length v4, v4

    if-lt v0, v4, :cond_4

    move v2, v3

    .line 258
    goto :goto_0

    .line 254
    :cond_4
    iget-object v4, v1, Lorg/apache/lucene/util/automaton/SortedIntSet;->values:[I

    aget v4, v4, v0

    iget-object v5, p0, Lorg/apache/lucene/util/automaton/SortedIntSet$FrozenIntSet;->values:[I

    aget v5, v5, v0

    if-ne v4, v5, :cond_0

    .line 253
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 223
    iget v0, p0, Lorg/apache/lucene/util/automaton/SortedIntSet$FrozenIntSet;->hashCode:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 266
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v3, 0x5b

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 267
    .local v1, "sb":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/util/automaton/SortedIntSet$FrozenIntSet;->values:[I

    array-length v2, v2

    if-lt v0, v2, :cond_0

    .line 273
    const/16 v2, 0x5d

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 274
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 268
    :cond_0
    if-lez v0, :cond_1

    .line 269
    const/16 v2, 0x20

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 271
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/util/automaton/SortedIntSet$FrozenIntSet;->values:[I

    aget v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 267
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
