.class abstract Lorg/apache/lucene/util/CollectionUtil$ListMergeSorterTemplate;
.super Lorg/apache/lucene/util/CollectionUtil$ListSorterTemplate;
.source "CollectionUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/CollectionUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "ListMergeSorterTemplate"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lorg/apache/lucene/util/CollectionUtil$ListSorterTemplate",
        "<TT;>;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final threshold:I

.field private final tmp:[Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[TT;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 76
    const-class v0, Lorg/apache/lucene/util/CollectionUtil;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/CollectionUtil$ListMergeSorterTemplate;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Ljava/util/List;F)V
    .locals 2
    .param p2, "overheadRatio"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TT;>;F)V"
        }
    .end annotation

    .prologue
    .line 82
    .local p0, "this":Lorg/apache/lucene/util/CollectionUtil$ListMergeSorterTemplate;, "Lorg/apache/lucene/util/CollectionUtil$ListMergeSorterTemplate<TT;>;"
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<TT;>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/util/CollectionUtil$ListSorterTemplate;-><init>(Ljava/util/List;)V

    .line 83
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, p2

    float-to-int v1, v1

    iput v1, p0, Lorg/apache/lucene/util/CollectionUtil$ListMergeSorterTemplate;->threshold:I

    .line 85
    iget v1, p0, Lorg/apache/lucene/util/CollectionUtil$ListMergeSorterTemplate;->threshold:I

    new-array v0, v1, [Ljava/lang/Object;

    .line 86
    .local v0, "tmpBuf":[Ljava/lang/Object;
    iput-object v0, p0, Lorg/apache/lucene/util/CollectionUtil$ListMergeSorterTemplate;->tmp:[Ljava/lang/Object;

    .line 87
    return-void
.end method

.method private mergeWithExtraMemory(IIIII)V
    .locals 9
    .param p1, "lo"    # I
    .param p2, "pivot"    # I
    .param p3, "hi"    # I
    .param p4, "len1"    # I
    .param p5, "len2"    # I

    .prologue
    .line 90
    .local p0, "this":Lorg/apache/lucene/util/CollectionUtil$ListMergeSorterTemplate;, "Lorg/apache/lucene/util/CollectionUtil$ListMergeSorterTemplate<TT;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-lt v2, p4, :cond_1

    .line 93
    const/4 v2, 0x0

    move v4, p2

    .local v4, "j":I
    move v0, p1

    .local v0, "dest":I
    move v1, v0

    .end local v0    # "dest":I
    .local v1, "dest":I
    move v5, v4

    .end local v4    # "j":I
    .local v5, "j":I
    move v3, v2

    .line 94
    .end local v2    # "i":I
    .local v3, "i":I
    :goto_1
    if-ge v3, p4, :cond_0

    if-lt v5, p3, :cond_2

    .line 101
    :cond_0
    :goto_2
    if-lt v3, p4, :cond_4

    .line 104
    sget-boolean v6, Lorg/apache/lucene/util/CollectionUtil$ListMergeSorterTemplate;->$assertionsDisabled:Z

    if-nez v6, :cond_5

    if-eq v5, v1, :cond_5

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 91
    .end local v1    # "dest":I
    .end local v3    # "i":I
    .end local v5    # "j":I
    .restart local v2    # "i":I
    :cond_1
    iget-object v6, p0, Lorg/apache/lucene/util/CollectionUtil$ListMergeSorterTemplate;->tmp:[Ljava/lang/Object;

    iget-object v7, p0, Lorg/apache/lucene/util/CollectionUtil$ListMergeSorterTemplate;->list:Ljava/util/List;

    add-int v8, p1, v2

    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    aput-object v7, v6, v2

    .line 90
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 95
    .end local v2    # "i":I
    .restart local v1    # "dest":I
    .restart local v3    # "i":I
    .restart local v5    # "j":I
    :cond_2
    iget-object v6, p0, Lorg/apache/lucene/util/CollectionUtil$ListMergeSorterTemplate;->tmp:[Ljava/lang/Object;

    aget-object v6, v6, v3

    iget-object v7, p0, Lorg/apache/lucene/util/CollectionUtil$ListMergeSorterTemplate;->list:Ljava/util/List;

    invoke-interface {v7, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {p0, v6, v7}, Lorg/apache/lucene/util/CollectionUtil$ListMergeSorterTemplate;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v6

    if-gtz v6, :cond_3

    .line 96
    iget-object v6, p0, Lorg/apache/lucene/util/CollectionUtil$ListMergeSorterTemplate;->list:Ljava/util/List;

    add-int/lit8 v0, v1, 0x1

    .end local v1    # "dest":I
    .restart local v0    # "dest":I
    iget-object v7, p0, Lorg/apache/lucene/util/CollectionUtil$ListMergeSorterTemplate;->tmp:[Ljava/lang/Object;

    add-int/lit8 v2, v3, 0x1

    .end local v3    # "i":I
    .restart local v2    # "i":I
    aget-object v7, v7, v3

    invoke-interface {v6, v1, v7}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move v1, v0

    .end local v0    # "dest":I
    .restart local v1    # "dest":I
    move v3, v2

    .line 97
    .end local v2    # "i":I
    .restart local v3    # "i":I
    goto :goto_1

    .line 98
    :cond_3
    iget-object v6, p0, Lorg/apache/lucene/util/CollectionUtil$ListMergeSorterTemplate;->list:Ljava/util/List;

    add-int/lit8 v0, v1, 0x1

    .end local v1    # "dest":I
    .restart local v0    # "dest":I
    iget-object v7, p0, Lorg/apache/lucene/util/CollectionUtil$ListMergeSorterTemplate;->list:Ljava/util/List;

    add-int/lit8 v4, v5, 0x1

    .end local v5    # "j":I
    .restart local v4    # "j":I
    invoke-interface {v7, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    invoke-interface {v6, v1, v7}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move v1, v0

    .end local v0    # "dest":I
    .restart local v1    # "dest":I
    move v5, v4

    .end local v4    # "j":I
    .restart local v5    # "j":I
    goto :goto_1

    .line 102
    :cond_4
    iget-object v6, p0, Lorg/apache/lucene/util/CollectionUtil$ListMergeSorterTemplate;->list:Ljava/util/List;

    add-int/lit8 v0, v1, 0x1

    .end local v1    # "dest":I
    .restart local v0    # "dest":I
    iget-object v7, p0, Lorg/apache/lucene/util/CollectionUtil$ListMergeSorterTemplate;->tmp:[Ljava/lang/Object;

    add-int/lit8 v2, v3, 0x1

    .end local v3    # "i":I
    .restart local v2    # "i":I
    aget-object v7, v7, v3

    invoke-interface {v6, v1, v7}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move v1, v0

    .end local v0    # "dest":I
    .restart local v1    # "dest":I
    move v3, v2

    .end local v2    # "i":I
    .restart local v3    # "i":I
    goto :goto_2

    .line 105
    :cond_5
    return-void
.end method


# virtual methods
.method protected merge(IIIII)V
    .locals 1
    .param p1, "lo"    # I
    .param p2, "pivot"    # I
    .param p3, "hi"    # I
    .param p4, "len1"    # I
    .param p5, "len2"    # I

    .prologue
    .line 109
    .local p0, "this":Lorg/apache/lucene/util/CollectionUtil$ListMergeSorterTemplate;, "Lorg/apache/lucene/util/CollectionUtil$ListMergeSorterTemplate<TT;>;"
    iget v0, p0, Lorg/apache/lucene/util/CollectionUtil$ListMergeSorterTemplate;->threshold:I

    if-gt p4, v0, :cond_0

    .line 110
    invoke-direct/range {p0 .. p5}, Lorg/apache/lucene/util/CollectionUtil$ListMergeSorterTemplate;->mergeWithExtraMemory(IIIII)V

    .line 116
    :goto_0
    return-void

    .line 114
    :cond_0
    invoke-super/range {p0 .. p5}, Lorg/apache/lucene/util/CollectionUtil$ListSorterTemplate;->merge(IIIII)V

    goto :goto_0
.end method
