.class final Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;
.super Ljava/lang/Object;
.source "BasicOperations.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/automaton/BasicOperations;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "PointTransitionSet"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final HASHMAP_CUTOVER:I = 0x1e


# instance fields
.field count:I

.field private final map:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;",
            ">;"
        }
    .end annotation
.end field

.field points:[Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;

.field private useHash:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 498
    const-class v0, Lorg/apache/lucene/util/automaton/BasicOperations;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;->$assertionsDisabled:Z

    .line 502
    return-void

    .line 498
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 498
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 500
    const/4 v0, 0x5

    new-array v0, v0, [Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;

    iput-object v0, p0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;->points:[Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;

    .line 503
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;->map:Ljava/util/HashMap;

    .line 504
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;->useHash:Z

    .line 498
    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;)V
    .locals 0

    .prologue
    .line 498
    invoke-direct {p0}, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;-><init>()V

    return-void
.end method

.method private find(I)Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;
    .locals 6
    .param p1, "point"    # I

    .prologue
    .line 523
    iget-boolean v3, p0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;->useHash:Z

    if-eqz v3, :cond_1

    .line 524
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 525
    .local v2, "pi":Ljava/lang/Integer;
    iget-object v3, p0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;->map:Ljava/util/HashMap;

    invoke-virtual {v3, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;

    .line 526
    .local v1, "p":Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;
    if-nez v1, :cond_0

    .line 527
    invoke-direct {p0, p1}, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;->next(I)Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;

    move-result-object v1

    .line 528
    iget-object v3, p0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;->map:Ljava/util/HashMap;

    invoke-virtual {v3, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 547
    .end local v1    # "p":Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;
    .end local v2    # "pi":Ljava/lang/Integer;
    :cond_0
    :goto_0
    return-object v1

    .line 532
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget v3, p0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;->count:I

    if-lt v0, v3, :cond_2

    .line 538
    invoke-direct {p0, p1}, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;->next(I)Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;

    move-result-object v1

    .line 539
    .restart local v1    # "p":Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;
    iget v3, p0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;->count:I

    const/16 v4, 0x1e

    if-ne v3, v4, :cond_0

    .line 541
    sget-boolean v3, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;->$assertionsDisabled:Z

    if-nez v3, :cond_4

    iget-object v3, p0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;->map:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->size()I

    move-result v3

    if-eqz v3, :cond_4

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 533
    .end local v1    # "p":Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;
    :cond_2
    iget-object v3, p0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;->points:[Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;

    aget-object v3, v3, v0

    iget v3, v3, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;->point:I

    if-ne v3, p1, :cond_3

    .line 534
    iget-object v3, p0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;->points:[Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;

    aget-object v1, v3, v0

    goto :goto_0

    .line 532
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 542
    .restart local v1    # "p":Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;
    :cond_4
    const/4 v0, 0x0

    :goto_2
    iget v3, p0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;->count:I

    if-lt v0, v3, :cond_5

    .line 545
    const/4 v3, 0x1

    iput-boolean v3, p0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;->useHash:Z

    goto :goto_0

    .line 543
    :cond_5
    iget-object v3, p0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;->map:Ljava/util/HashMap;

    iget-object v4, p0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;->points:[Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;

    aget-object v4, v4, v0

    iget v4, v4, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;->point:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;->points:[Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;

    aget-object v5, v5, v0

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 542
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method private next(I)Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;
    .locals 5
    .param p1, "point"    # I

    .prologue
    const/4 v4, 0x0

    .line 508
    iget v2, p0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;->count:I

    iget-object v3, p0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;->points:[Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;

    array-length v3, v3

    if-ne v2, v3, :cond_0

    .line 509
    iget v2, p0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;->count:I

    add-int/lit8 v2, v2, 0x1

    sget v3, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_REF:I

    invoke-static {v2, v3}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v2

    new-array v0, v2, [Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;

    .line 510
    .local v0, "newArray":[Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;
    iget-object v2, p0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;->points:[Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;

    iget v3, p0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;->count:I

    invoke-static {v2, v4, v0, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 511
    iput-object v0, p0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;->points:[Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;

    .line 513
    .end local v0    # "newArray":[Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;->points:[Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;

    iget v3, p0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;->count:I

    aget-object v1, v2, v3

    .line 514
    .local v1, "points0":Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;
    if-nez v1, :cond_1

    .line 515
    iget-object v2, p0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;->points:[Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;

    iget v3, p0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;->count:I

    new-instance v1, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;

    .end local v1    # "points0":Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;
    const/4 v4, 0x0

    invoke-direct {v1, v4}, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;-><init>(Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;)V

    aput-object v1, v2, v3

    .line 517
    .restart local v1    # "points0":Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;
    :cond_1
    invoke-virtual {v1, p1}, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;->reset(I)V

    .line 518
    iget v2, p0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;->count:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;->count:I

    .line 519
    return-object v1
.end method


# virtual methods
.method public add(Lorg/apache/lucene/util/automaton/Transition;)V
    .locals 1
    .param p1, "t"    # Lorg/apache/lucene/util/automaton/Transition;

    .prologue
    .line 565
    iget v0, p1, Lorg/apache/lucene/util/automaton/Transition;->min:I

    invoke-direct {p0, v0}, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;->find(I)Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;

    move-result-object v0

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;->starts:Lorg/apache/lucene/util/automaton/BasicOperations$TransitionList;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/util/automaton/BasicOperations$TransitionList;->add(Lorg/apache/lucene/util/automaton/Transition;)V

    .line 566
    iget v0, p1, Lorg/apache/lucene/util/automaton/Transition;->max:I

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;->find(I)Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;

    move-result-object v0

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;->ends:Lorg/apache/lucene/util/automaton/BasicOperations$TransitionList;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/util/automaton/BasicOperations$TransitionList;->add(Lorg/apache/lucene/util/automaton/Transition;)V

    .line 567
    return-void
.end method

.method public reset()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 552
    iget-boolean v0, p0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;->useHash:Z

    if-eqz v0, :cond_0

    .line 553
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;->map:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 554
    iput-boolean v1, p0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;->useHash:Z

    .line 556
    :cond_0
    iput v1, p0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;->count:I

    .line 557
    return-void
.end method

.method public sort()V
    .locals 3

    .prologue
    .line 561
    iget v0, p0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;->count:I

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;->points:[Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;

    const/4 v1, 0x0

    iget v2, p0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;->count:I

    invoke-static {v0, v1, v2}, Lorg/apache/lucene/util/ArrayUtil;->mergeSort([Ljava/lang/Comparable;II)V

    .line 562
    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 571
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 572
    .local v1, "s":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;->count:I

    if-lt v0, v2, :cond_0

    .line 578
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 573
    :cond_0
    if-lez v0, :cond_1

    .line 574
    const/16 v2, 0x20

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 576
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;->points:[Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;

    aget-object v2, v2, v0

    iget v2, v2, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;->point:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x3a

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;->points:[Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;

    aget-object v3, v3, v0

    iget-object v3, v3, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;->starts:Lorg/apache/lucene/util/automaton/BasicOperations$TransitionList;

    iget v3, v3, Lorg/apache/lucene/util/automaton/BasicOperations$TransitionList;->count:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x2c

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitionSet;->points:[Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;

    aget-object v3, v3, v0

    iget-object v3, v3, Lorg/apache/lucene/util/automaton/BasicOperations$PointTransitions;->ends:Lorg/apache/lucene/util/automaton/BasicOperations$TransitionList;

    iget v3, v3, Lorg/apache/lucene/util/automaton/BasicOperations$TransitionList;->count:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 572
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
