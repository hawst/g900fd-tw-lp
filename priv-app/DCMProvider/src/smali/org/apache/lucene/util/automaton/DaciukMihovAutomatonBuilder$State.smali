.class final Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;
.super Ljava/lang/Object;
.source "DaciukMihovAutomatonBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "State"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final NO_LABELS:[I

.field private static final NO_STATES:[Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;


# instance fields
.field is_final:Z

.field labels:[I

.field states:[Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 38
    const-class v0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;->$assertionsDisabled:Z

    .line 41
    new-array v0, v1, [I

    sput-object v0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;->NO_LABELS:[I

    .line 44
    new-array v0, v1, [Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;

    sput-object v0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;->NO_STATES:[Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;

    return-void

    :cond_0
    move v0, v1

    .line 38
    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    sget-object v0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;->NO_LABELS:[I

    iput-object v0, p0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;->labels:[I

    .line 56
    sget-object v0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;->NO_STATES:[Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;

    iput-object v0, p0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;->states:[Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;

    .line 38
    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;-><init>()V

    return-void
.end method

.method private static referenceEquals([Ljava/lang/Object;[Ljava/lang/Object;)Z
    .locals 4
    .param p0, "a1"    # [Ljava/lang/Object;
    .param p1, "a2"    # [Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 173
    array-length v2, p0

    array-length v3, p1

    if-eq v2, v3, :cond_1

    .line 183
    :cond_0
    :goto_0
    return v1

    .line 177
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v2, p0

    if-lt v0, v2, :cond_2

    .line 183
    const/4 v1, 0x1

    goto :goto_0

    .line 178
    :cond_2
    aget-object v2, p0, v0

    aget-object v3, p1, v0

    if-ne v2, v3, :cond_0

    .line 177
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 85
    move-object v0, p1

    check-cast v0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;

    .line 86
    .local v0, "other":Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;
    iget-boolean v1, p0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;->is_final:Z

    iget-boolean v2, v0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;->is_final:Z

    if-ne v1, v2, :cond_0

    .line 87
    iget-object v1, p0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;->labels:[I

    iget-object v2, v0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;->labels:[I

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 88
    iget-object v1, p0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;->states:[Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;

    iget-object v2, v0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;->states:[Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;

    invoke-static {v1, v2}, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;->referenceEquals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 86
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method getState(I)Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;
    .locals 2
    .param p1, "label"    # I

    .prologue
    .line 70
    iget-object v1, p0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;->labels:[I

    invoke-static {v1, p1}, Ljava/util/Arrays;->binarySearch([II)I

    move-result v0

    .line 71
    .local v0, "index":I
    if-ltz v0, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;->states:[Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;

    aget-object v1, v1, v0

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method hasChildren()Z
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;->labels:[I

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 96
    iget-boolean v4, p0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;->is_final:Z

    if-eqz v4, :cond_0

    const/4 v1, 0x1

    .line 98
    .local v1, "hash":I
    :goto_0
    mul-int/lit8 v4, v1, 0x1f

    iget-object v5, p0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;->labels:[I

    array-length v5, v5

    add-int/2addr v4, v5

    xor-int/2addr v1, v4

    .line 99
    iget-object v5, p0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;->labels:[I

    array-length v6, v5

    move v4, v3

    :goto_1
    if-lt v4, v6, :cond_1

    .line 108
    iget-object v4, p0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;->states:[Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;

    array-length v5, v4

    :goto_2
    if-lt v3, v5, :cond_2

    .line 112
    return v1

    .end local v1    # "hash":I
    :cond_0
    move v1, v3

    .line 96
    goto :goto_0

    .line 99
    .restart local v1    # "hash":I
    :cond_1
    aget v0, v5, v4

    .line 100
    .local v0, "c":I
    mul-int/lit8 v7, v1, 0x1f

    add-int/2addr v7, v0

    xor-int/2addr v1, v7

    .line 99
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 108
    .end local v0    # "c":I
    :cond_2
    aget-object v2, v4, v3

    .line 109
    .local v2, "s":Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;
    invoke-static {v2}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v6

    xor-int/2addr v1, v6

    .line 108
    add-int/lit8 v3, v3, 0x1

    goto :goto_2
.end method

.method lastChild()Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;
    .locals 2

    .prologue
    .line 142
    sget-boolean v0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;->hasChildren()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "No outgoing transitions."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 143
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;->states:[Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;

    iget-object v1, p0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;->states:[Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    return-object v0
.end method

.method lastChild(I)Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;
    .locals 3
    .param p1, "label"    # I

    .prologue
    .line 151
    iget-object v2, p0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;->labels:[I

    array-length v2, v2

    add-int/lit8 v0, v2, -0x1

    .line 152
    .local v0, "index":I
    const/4 v1, 0x0

    .line 153
    .local v1, "s":Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;
    if-ltz v0, :cond_0

    iget-object v2, p0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;->labels:[I

    aget v2, v2, v0

    if-ne v2, p1, :cond_0

    .line 154
    iget-object v2, p0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;->states:[Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;

    aget-object v1, v2, v0

    .line 156
    :cond_0
    sget-boolean v2, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;->$assertionsDisabled:Z

    if-nez v2, :cond_1

    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;->getState(I)Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;

    move-result-object v2

    if-eq v1, v2, :cond_1

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 157
    :cond_1
    return-object v1
.end method

.method newState(I)Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;
    .locals 3
    .param p1, "label"    # I

    .prologue
    .line 128
    sget-boolean v0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;->labels:[I

    invoke-static {v0, p1}, Ljava/util/Arrays;->binarySearch([II)I

    move-result v0

    if-ltz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "State already has transition labeled: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 129
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 131
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;->labels:[I

    iget-object v1, p0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;->labels:[I

    array-length v1, v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;->labels:[I

    .line 132
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;->states:[Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;

    iget-object v1, p0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;->states:[Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;

    array-length v1, v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;

    iput-object v0, p0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;->states:[Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;

    .line 134
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;->labels:[I

    iget-object v1, p0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;->labels:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aput p1, v0, v1

    .line 135
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;->states:[Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;

    iget-object v1, p0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;->states:[Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    new-instance v2, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;

    invoke-direct {v2}, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;-><init>()V

    aput-object v2, v0, v1

    return-object v2
.end method

.method replaceLastChild(Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;)V
    .locals 2
    .param p1, "state"    # Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;

    .prologue
    .line 165
    sget-boolean v0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;->hasChildren()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "No outgoing transitions."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 166
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;->states:[Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;

    iget-object v1, p0, Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;->states:[Lorg/apache/lucene/util/automaton/DaciukMihovAutomatonBuilder$State;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aput-object p1, v0, v1

    .line 167
    return-void
.end method
