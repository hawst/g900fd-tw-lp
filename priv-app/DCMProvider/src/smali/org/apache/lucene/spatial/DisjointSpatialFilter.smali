.class public Lorg/apache/lucene/spatial/DisjointSpatialFilter;
.super Lorg/apache/lucene/search/Filter;
.source "DisjointSpatialFilter.java"


# instance fields
.field private final field:Ljava/lang/String;

.field private final intersectsFilter:Lorg/apache/lucene/search/Filter;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/spatial/SpatialStrategy;Lorg/apache/lucene/spatial/query/SpatialArgs;Ljava/lang/String;)V
    .locals 2
    .param p1, "strategy"    # Lorg/apache/lucene/spatial/SpatialStrategy;
    .param p2, "args"    # Lorg/apache/lucene/spatial/query/SpatialArgs;
    .param p3, "field"    # Ljava/lang/String;

    .prologue
    .line 54
    invoke-direct {p0}, Lorg/apache/lucene/search/Filter;-><init>()V

    .line 55
    iput-object p3, p0, Lorg/apache/lucene/spatial/DisjointSpatialFilter;->field:Ljava/lang/String;

    .line 58
    invoke-virtual {p2}, Lorg/apache/lucene/spatial/query/SpatialArgs;->getOperation()Lorg/apache/lucene/spatial/query/SpatialOperation;

    move-result-object v0

    .line 59
    .local v0, "origOp":Lorg/apache/lucene/spatial/query/SpatialOperation;
    sget-object v1, Lorg/apache/lucene/spatial/query/SpatialOperation;->Intersects:Lorg/apache/lucene/spatial/query/SpatialOperation;

    invoke-virtual {p2, v1}, Lorg/apache/lucene/spatial/query/SpatialArgs;->setOperation(Lorg/apache/lucene/spatial/query/SpatialOperation;)V

    .line 60
    invoke-virtual {p1, p2}, Lorg/apache/lucene/spatial/SpatialStrategy;->makeFilter(Lorg/apache/lucene/spatial/query/SpatialArgs;)Lorg/apache/lucene/search/Filter;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/spatial/DisjointSpatialFilter;->intersectsFilter:Lorg/apache/lucene/search/Filter;

    .line 61
    invoke-virtual {p2, v0}, Lorg/apache/lucene/spatial/query/SpatialArgs;->setOperation(Lorg/apache/lucene/spatial/query/SpatialOperation;)V

    .line 62
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 66
    if-ne p0, p1, :cond_1

    .line 75
    :cond_0
    :goto_0
    return v1

    .line 67
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 69
    check-cast v0, Lorg/apache/lucene/spatial/DisjointSpatialFilter;

    .line 71
    .local v0, "that":Lorg/apache/lucene/spatial/DisjointSpatialFilter;
    iget-object v3, p0, Lorg/apache/lucene/spatial/DisjointSpatialFilter;->field:Ljava/lang/String;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lorg/apache/lucene/spatial/DisjointSpatialFilter;->field:Ljava/lang/String;

    iget-object v4, v0, Lorg/apache/lucene/spatial/DisjointSpatialFilter;->field:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    :cond_4
    move v1, v2

    .line 72
    goto :goto_0

    .line 71
    :cond_5
    iget-object v3, v0, Lorg/apache/lucene/spatial/DisjointSpatialFilter;->field:Ljava/lang/String;

    if-nez v3, :cond_4

    .line 73
    :cond_6
    iget-object v3, p0, Lorg/apache/lucene/spatial/DisjointSpatialFilter;->intersectsFilter:Lorg/apache/lucene/search/Filter;

    iget-object v4, v0, Lorg/apache/lucene/spatial/DisjointSpatialFilter;->intersectsFilter:Lorg/apache/lucene/search/Filter;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public getDocIdSet(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/DocIdSet;
    .locals 7
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "acceptDocs"    # Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 88
    iget-object v3, p0, Lorg/apache/lucene/spatial/DisjointSpatialFilter;->field:Ljava/lang/String;

    if-nez v3, :cond_1

    .line 89
    const/4 v1, 0x0

    .line 109
    .local v1, "docsWithField":Lorg/apache/lucene/util/Bits;
    :cond_0
    :goto_0
    new-instance v3, Lorg/apache/lucene/queries/ChainedFilter;

    const/4 v4, 0x1

    new-array v4, v4, [Lorg/apache/lucene/search/Filter;

    const/4 v5, 0x0

    iget-object v6, p0, Lorg/apache/lucene/spatial/DisjointSpatialFilter;->intersectsFilter:Lorg/apache/lucene/search/Filter;

    aput-object v6, v4, v5

    const/4 v5, 0x2

    invoke-direct {v3, v4, v5}, Lorg/apache/lucene/queries/ChainedFilter;-><init>([Lorg/apache/lucene/search/Filter;I)V

    invoke-virtual {v3, p1, p2}, Lorg/apache/lucene/queries/ChainedFilter;->getDocIdSet(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/DocIdSet;

    move-result-object v0

    .line 110
    .local v0, "docIdSet":Lorg/apache/lucene/search/DocIdSet;
    invoke-static {v0, v1}, Lorg/apache/lucene/search/BitsFilteredDocIdSet;->wrap(Lorg/apache/lucene/search/DocIdSet;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/DocIdSet;

    move-result-object v3

    .end local v0    # "docIdSet":Lorg/apache/lucene/search/DocIdSet;
    :goto_1
    return-object v3

    .line 95
    .end local v1    # "docsWithField":Lorg/apache/lucene/util/Bits;
    :cond_1
    sget-object v3, Lorg/apache/lucene/search/FieldCache;->DEFAULT:Lorg/apache/lucene/search/FieldCache;

    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/lucene/spatial/DisjointSpatialFilter;->field:Ljava/lang/String;

    invoke-interface {v3, v4, v5}, Lorg/apache/lucene/search/FieldCache;->getDocsWithField(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;)Lorg/apache/lucene/util/Bits;

    move-result-object v1

    .line 97
    .restart local v1    # "docsWithField":Lorg/apache/lucene/util/Bits;
    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v2

    .line 98
    .local v2, "maxDoc":I
    invoke-interface {v1}, Lorg/apache/lucene/util/Bits;->length()I

    move-result v3

    if-eq v3, v2, :cond_2

    .line 99
    new-instance v3, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Bits length should be maxDoc ("

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") but wasn\'t: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 101
    :cond_2
    instance-of v3, v1, Lorg/apache/lucene/util/Bits$MatchNoBits;

    if-eqz v3, :cond_3

    .line 102
    const/4 v3, 0x0

    goto :goto_1

    .line 103
    :cond_3
    instance-of v3, v1, Lorg/apache/lucene/util/Bits$MatchAllBits;

    if-eqz v3, :cond_0

    .line 104
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 80
    iget-object v1, p0, Lorg/apache/lucene/spatial/DisjointSpatialFilter;->field:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/spatial/DisjointSpatialFilter;->field:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 81
    .local v0, "result":I
    :goto_0
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lorg/apache/lucene/spatial/DisjointSpatialFilter;->intersectsFilter:Lorg/apache/lucene/search/Filter;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    add-int v0, v1, v2

    .line 82
    return v0

    .line 80
    .end local v0    # "result":I
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
