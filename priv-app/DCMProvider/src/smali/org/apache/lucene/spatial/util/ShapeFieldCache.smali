.class public Lorg/apache/lucene/spatial/util/ShapeFieldCache;
.super Ljava/lang/Object;
.source "ShapeFieldCache.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/spatial4j/core/shape/Shape;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private cache:[Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation
.end field

.field public defaultLength:I


# direct methods
.method public constructor <init>(II)V
    .locals 1
    .param p1, "length"    # I
    .param p2, "defaultLength"    # I

    .prologue
    .line 39
    .local p0, "this":Lorg/apache/lucene/spatial/util/ShapeFieldCache;, "Lorg/apache/lucene/spatial/util/ShapeFieldCache<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-array v0, p1, [Ljava/util/List;

    iput-object v0, p0, Lorg/apache/lucene/spatial/util/ShapeFieldCache;->cache:[Ljava/util/List;

    .line 41
    iput p2, p0, Lorg/apache/lucene/spatial/util/ShapeFieldCache;->defaultLength:I

    .line 42
    return-void
.end method


# virtual methods
.method public add(ILcom/spatial4j/core/shape/Shape;)V
    .locals 3
    .param p1, "docid"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITT;)V"
        }
    .end annotation

    .prologue
    .line 45
    .local p0, "this":Lorg/apache/lucene/spatial/util/ShapeFieldCache;, "Lorg/apache/lucene/spatial/util/ShapeFieldCache<TT;>;"
    .local p2, "s":Lcom/spatial4j/core/shape/Shape;, "TT;"
    iget-object v1, p0, Lorg/apache/lucene/spatial/util/ShapeFieldCache;->cache:[Ljava/util/List;

    aget-object v0, v1, p1

    .line 46
    .local v0, "list":Ljava/util/List;, "Ljava/util/List<TT;>;"
    if-nez v0, :cond_0

    .line 47
    iget-object v1, p0, Lorg/apache/lucene/spatial/util/ShapeFieldCache;->cache:[Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    .end local v0    # "list":Ljava/util/List;, "Ljava/util/List<TT;>;"
    iget v2, p0, Lorg/apache/lucene/spatial/util/ShapeFieldCache;->defaultLength:I

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    aput-object v0, v1, p1

    .line 49
    .restart local v0    # "list":Ljava/util/List;, "Ljava/util/List<TT;>;"
    :cond_0
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 50
    return-void
.end method

.method public getShapes(I)Ljava/util/List;
    .locals 1
    .param p1, "docid"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 53
    .local p0, "this":Lorg/apache/lucene/spatial/util/ShapeFieldCache;, "Lorg/apache/lucene/spatial/util/ShapeFieldCache<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/spatial/util/ShapeFieldCache;->cache:[Ljava/util/List;

    aget-object v0, v0, p1

    return-object v0
.end method
