.class public abstract Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;
.super Ljava/lang/Object;
.source "SpatialPrefixTree.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field protected static final UTF8:Ljava/nio/charset/Charset;


# instance fields
.field protected final ctx:Lcom/spatial4j/core/context/SpatialContext;

.field protected final maxLevels:I

.field private transient worldCell:Lorg/apache/lucene/spatial/prefix/tree/Cell;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-class v0, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;->$assertionsDisabled:Z

    .line 44
    const-string v0, "UTF-8"

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;->UTF8:Ljava/nio/charset/Charset;

    return-void

    .line 42
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/spatial4j/core/context/SpatialContext;I)V
    .locals 1
    .param p1, "ctx"    # Lcom/spatial4j/core/context/SpatialContext;
    .param p2, "maxLevels"    # I

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    sget-boolean v0, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-gtz p2, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 52
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;->ctx:Lcom/spatial4j/core/context/SpatialContext;

    .line 53
    iput p2, p0, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;->maxLevels:I

    .line 54
    return-void
.end method

.method public static cellsToTokenStrings(Ljava/util/Collection;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/spatial/prefix/tree/Cell;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 241
    .local p0, "cells":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/spatial/prefix/tree/Cell;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 242
    .local v2, "tokens":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_0

    .line 250
    return-object v2

    .line 242
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/spatial/prefix/tree/Cell;

    .line 243
    .local v0, "cell":Lorg/apache/lucene/spatial/prefix/tree/Cell;
    invoke-virtual {v0}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->getTokenString()Ljava/lang/String;

    move-result-object v1

    .line 244
    .local v1, "token":Ljava/lang/String;
    invoke-virtual {v0}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->isLeaf()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 245
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v5, 0x2b

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 247
    :cond_1
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private recursiveGetCells(Lorg/apache/lucene/spatial/prefix/tree/Cell;Lcom/spatial4j/core/shape/Shape;IZZLjava/util/List;)Z
    .locals 11
    .param p1, "cell"    # Lorg/apache/lucene/spatial/prefix/tree/Cell;
    .param p2, "shape"    # Lcom/spatial4j/core/shape/Shape;
    .param p3, "detailLevel"    # I
    .param p4, "inclParents"    # Z
    .param p5, "simplify"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/spatial/prefix/tree/Cell;",
            "Lcom/spatial4j/core/shape/Shape;",
            "IZZ",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/spatial/prefix/tree/Cell;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 179
    .local p6, "result":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/spatial/prefix/tree/Cell;>;"
    invoke-virtual {p1}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->getLevel()I

    move-result v1

    if-ne v1, p3, :cond_0

    .line 180
    invoke-virtual {p1}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->setLeaf()V

    .line 182
    :cond_0
    invoke-virtual {p1}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->isLeaf()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 183
    move-object/from16 v0, p6

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 184
    const/4 v1, 0x1

    .line 210
    :goto_0
    return v1

    .line 186
    :cond_1
    if-eqz p4, :cond_2

    invoke-virtual {p1}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->getLevel()I

    move-result v1

    if-eqz v1, :cond_2

    .line 187
    move-object/from16 v0, p6

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 189
    :cond_2
    invoke-virtual {p1, p2}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->getSubCells(Lcom/spatial4j/core/shape/Shape;)Ljava/util/Collection;

    move-result-object v9

    .line 190
    .local v9, "subCells":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/spatial/prefix/tree/Cell;>;"
    const/4 v8, 0x0

    .line 191
    .local v8, "leaves":I
    invoke-interface {v9}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_3
    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_6

    .line 196
    if-eqz p5, :cond_7

    invoke-virtual {p1}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->getSubCellsSize()I

    move-result v1

    if-ne v8, v1, :cond_7

    invoke-virtual {p1}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->getLevel()I

    move-result v1

    if-eqz v1, :cond_7

    .line 202
    :cond_4
    invoke-interface/range {p6 .. p6}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move-object/from16 v0, p6

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 203
    add-int/lit8 v8, v8, -0x1

    .line 201
    if-gtz v8, :cond_4

    .line 205
    invoke-virtual {p1}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->setLeaf()V

    .line 206
    if-nez p4, :cond_5

    .line 207
    move-object/from16 v0, p6

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 208
    :cond_5
    const/4 v1, 0x1

    goto :goto_0

    .line 191
    :cond_6
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/spatial/prefix/tree/Cell;

    .local v2, "subCell":Lorg/apache/lucene/spatial/prefix/tree/Cell;
    move-object v1, p0

    move-object v3, p2

    move v4, p3

    move v5, p4

    move/from16 v6, p5

    move-object/from16 v7, p6

    .line 192
    invoke-direct/range {v1 .. v7}, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;->recursiveGetCells(Lorg/apache/lucene/spatial/prefix/tree/Cell;Lcom/spatial4j/core/shape/Shape;IZZLjava/util/List;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 193
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 210
    .end local v2    # "subCell":Lorg/apache/lucene/spatial/prefix/tree/Cell;
    :cond_7
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected getCell(Lcom/spatial4j/core/shape/Point;I)Lorg/apache/lucene/spatial/prefix/tree/Cell;
    .locals 2
    .param p1, "p"    # Lcom/spatial4j/core/shape/Point;
    .param p2, "level"    # I

    .prologue
    const/4 v1, 0x0

    .line 138
    invoke-virtual {p0, p1, p2, v1}, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;->getCells(Lcom/spatial4j/core/shape/Point;IZ)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/spatial/prefix/tree/Cell;

    return-object v0
.end method

.method public abstract getCell(Ljava/lang/String;)Lorg/apache/lucene/spatial/prefix/tree/Cell;
.end method

.method public abstract getCell([BII)Lorg/apache/lucene/spatial/prefix/tree/Cell;
.end method

.method public final getCell([BIILorg/apache/lucene/spatial/prefix/tree/Cell;)Lorg/apache/lucene/spatial/prefix/tree/Cell;
    .locals 0
    .param p1, "bytes"    # [B
    .param p2, "offset"    # I
    .param p3, "len"    # I
    .param p4, "target"    # Lorg/apache/lucene/spatial/prefix/tree/Cell;

    .prologue
    .line 126
    if-nez p4, :cond_0

    .line 127
    invoke-virtual {p0, p1, p2, p3}, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;->getCell([BII)Lorg/apache/lucene/spatial/prefix/tree/Cell;

    move-result-object p4

    .line 131
    .end local p4    # "target":Lorg/apache/lucene/spatial/prefix/tree/Cell;
    :goto_0
    return-object p4

    .line 130
    .restart local p4    # "target":Lorg/apache/lucene/spatial/prefix/tree/Cell;
    :cond_0
    invoke-virtual {p4, p1, p2, p3}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->reset([BII)V

    goto :goto_0
.end method

.method public getCells(Lcom/spatial4j/core/shape/Point;IZ)Ljava/util/List;
    .locals 5
    .param p1, "p"    # Lcom/spatial4j/core/shape/Point;
    .param p2, "detailLevel"    # I
    .param p3, "inclParents"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/spatial4j/core/shape/Point;",
            "IZ)",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/spatial/prefix/tree/Cell;",
            ">;"
        }
    .end annotation

    .prologue
    .line 222
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;->getCell(Lcom/spatial4j/core/shape/Point;I)Lorg/apache/lucene/spatial/prefix/tree/Cell;

    move-result-object v0

    .line 223
    .local v0, "cell":Lorg/apache/lucene/spatial/prefix/tree/Cell;
    if-nez p3, :cond_0

    .line 224
    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 234
    :goto_0
    return-object v1

    .line 227
    :cond_0
    invoke-virtual {v0}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->getTokenString()Ljava/lang/String;

    move-result-object v2

    .line 228
    .local v2, "endToken":Ljava/lang/String;
    sget-boolean v4, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-eq v4, p2, :cond_1

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 229
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, p2}, Ljava/util/ArrayList;-><init>(I)V

    .line 230
    .local v1, "cells":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/spatial/prefix/tree/Cell;>;"
    const/4 v3, 0x1

    .local v3, "i":I
    :goto_1
    if-lt v3, p2, :cond_2

    .line 233
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 231
    :cond_2
    const/4 v4, 0x0

    invoke-virtual {v2, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;->getCell(Ljava/lang/String;)Lorg/apache/lucene/spatial/prefix/tree/Cell;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 230
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method public getCells(Lcom/spatial4j/core/shape/Shape;IZZ)Ljava/util/List;
    .locals 7
    .param p1, "shape"    # Lcom/spatial4j/core/shape/Shape;
    .param p2, "detailLevel"    # I
    .param p3, "inclParents"    # Z
    .param p4, "simplify"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/spatial4j/core/shape/Shape;",
            "IZZ)",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/spatial/prefix/tree/Cell;",
            ">;"
        }
    .end annotation

    .prologue
    .line 161
    iget v0, p0, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;->maxLevels:I

    if-le p2, v0, :cond_0

    .line 162
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "detailLevel > maxLevels"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 164
    :cond_0
    instance-of v0, p1, Lcom/spatial4j/core/shape/Point;

    if-eqz v0, :cond_1

    .line 165
    check-cast p1, Lcom/spatial4j/core/shape/Point;

    .end local p1    # "shape":Lcom/spatial4j/core/shape/Shape;
    invoke-virtual {p0, p1, p2, p3}, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;->getCells(Lcom/spatial4j/core/shape/Point;IZ)Ljava/util/List;

    move-result-object v6

    .line 169
    :goto_0
    return-object v6

    .line 167
    .restart local p1    # "shape":Lcom/spatial4j/core/shape/Shape;
    :cond_1
    new-instance v6, Ljava/util/ArrayList;

    if-eqz p3, :cond_2

    const/16 v0, 0x1000

    :goto_1
    invoke-direct {v6, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 168
    .local v6, "cells":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/spatial/prefix/tree/Cell;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;->getWorldCell()Lorg/apache/lucene/spatial/prefix/tree/Cell;

    move-result-object v1

    move-object v0, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;->recursiveGetCells(Lorg/apache/lucene/spatial/prefix/tree/Cell;Lcom/spatial4j/core/shape/Shape;IZZLjava/util/List;)Z

    goto :goto_0

    .line 167
    .end local v6    # "cells":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/spatial/prefix/tree/Cell;>;"
    :cond_2
    const/16 v0, 0x800

    goto :goto_1
.end method

.method public getDistanceForLevel(I)D
    .locals 10
    .param p1, "level"    # I

    .prologue
    .line 90
    const/4 v6, 0x1

    if-lt p1, v6, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;->getMaxLevels()I

    move-result v6

    if-le p1, v6, :cond_1

    .line 91
    :cond_0
    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string v7, "Level must be in 1 to maxLevels range"

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 93
    :cond_1
    iget-object v6, p0, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;->ctx:Lcom/spatial4j/core/context/SpatialContext;

    invoke-virtual {v6}, Lcom/spatial4j/core/context/SpatialContext;->getWorldBounds()Lcom/spatial4j/core/shape/Rectangle;

    move-result-object v6

    invoke-interface {v6}, Lcom/spatial4j/core/shape/Rectangle;->getCenter()Lcom/spatial4j/core/shape/Point;

    move-result-object v6

    invoke-virtual {p0, v6, p1}, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;->getCell(Lcom/spatial4j/core/shape/Point;I)Lorg/apache/lucene/spatial/prefix/tree/Cell;

    move-result-object v1

    .line 94
    .local v1, "cell":Lorg/apache/lucene/spatial/prefix/tree/Cell;
    invoke-virtual {v1}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->getShape()Lcom/spatial4j/core/shape/Shape;

    move-result-object v6

    invoke-interface {v6}, Lcom/spatial4j/core/shape/Shape;->getBoundingBox()Lcom/spatial4j/core/shape/Rectangle;

    move-result-object v0

    .line 95
    .local v0, "bbox":Lcom/spatial4j/core/shape/Rectangle;
    invoke-interface {v0}, Lcom/spatial4j/core/shape/Rectangle;->getWidth()D

    move-result-wide v4

    .line 96
    .local v4, "width":D
    invoke-interface {v0}, Lcom/spatial4j/core/shape/Rectangle;->getHeight()D

    move-result-wide v2

    .line 99
    .local v2, "height":D
    mul-double v6, v4, v4

    mul-double v8, v2, v2

    add-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    return-wide v6
.end method

.method public abstract getLevelForDistance(D)I
.end method

.method public getMaxLevels()I
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;->maxLevels:I

    return v0
.end method

.method public getSpatialContext()Lcom/spatial4j/core/context/SpatialContext;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;->ctx:Lcom/spatial4j/core/context/SpatialContext;

    return-object v0
.end method

.method public getWorldCell()Lorg/apache/lucene/spatial/prefix/tree/Cell;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;->worldCell:Lorg/apache/lucene/spatial/prefix/tree/Cell;

    if-nez v0, :cond_0

    .line 112
    const-string v0, ""

    invoke-virtual {p0, v0}, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;->getCell(Ljava/lang/String;)Lorg/apache/lucene/spatial/prefix/tree/Cell;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;->worldCell:Lorg/apache/lucene/spatial/prefix/tree/Cell;

    .line 114
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;->worldCell:Lorg/apache/lucene/spatial/prefix/tree/Cell;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 66
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "(maxLevels:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;->maxLevels:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",ctx:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;->ctx:Lcom/spatial4j/core/context/SpatialContext;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
