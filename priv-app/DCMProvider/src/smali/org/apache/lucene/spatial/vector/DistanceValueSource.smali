.class public Lorg/apache/lucene/spatial/vector/DistanceValueSource;
.super Lorg/apache/lucene/queries/function/ValueSource;
.source "DistanceValueSource.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final from:Lcom/spatial4j/core/shape/Point;

.field private strategy:Lorg/apache/lucene/spatial/vector/PointVectorStrategy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-class v0, Lorg/apache/lucene/spatial/vector/DistanceValueSource;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/spatial/vector/DistanceValueSource;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/spatial/vector/PointVectorStrategy;Lcom/spatial4j/core/shape/Point;)V
    .locals 0
    .param p1, "strategy"    # Lorg/apache/lucene/spatial/vector/PointVectorStrategy;
    .param p2, "from"    # Lcom/spatial4j/core/shape/Point;

    .prologue
    .line 46
    invoke-direct {p0}, Lorg/apache/lucene/queries/function/ValueSource;-><init>()V

    .line 47
    iput-object p1, p0, Lorg/apache/lucene/spatial/vector/DistanceValueSource;->strategy:Lorg/apache/lucene/spatial/vector/PointVectorStrategy;

    .line 48
    iput-object p2, p0, Lorg/apache/lucene/spatial/vector/DistanceValueSource;->from:Lcom/spatial4j/core/shape/Point;

    .line 49
    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/spatial/vector/DistanceValueSource;)Lcom/spatial4j/core/shape/Point;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lorg/apache/lucene/spatial/vector/DistanceValueSource;->from:Lcom/spatial4j/core/shape/Point;

    return-object v0
.end method

.method static synthetic access$1(Lorg/apache/lucene/spatial/vector/DistanceValueSource;)Lorg/apache/lucene/spatial/vector/PointVectorStrategy;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lorg/apache/lucene/spatial/vector/DistanceValueSource;->strategy:Lorg/apache/lucene/spatial/vector/PointVectorStrategy;

    return-object v0
.end method


# virtual methods
.method public description()Ljava/lang/String;
    .locals 2

    .prologue
    .line 56
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "DistanceValueSource("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/spatial/vector/DistanceValueSource;->strategy:Lorg/apache/lucene/spatial/vector/PointVectorStrategy;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/spatial/vector/DistanceValueSource;->from:Lcom/spatial4j/core/shape/Point;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 101
    if-ne p0, p1, :cond_1

    .line 109
    :cond_0
    :goto_0
    return v1

    .line 102
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 104
    check-cast v0, Lorg/apache/lucene/spatial/vector/DistanceValueSource;

    .line 106
    .local v0, "that":Lorg/apache/lucene/spatial/vector/DistanceValueSource;
    iget-object v3, p0, Lorg/apache/lucene/spatial/vector/DistanceValueSource;->from:Lcom/spatial4j/core/shape/Point;

    iget-object v4, v0, Lorg/apache/lucene/spatial/vector/DistanceValueSource;->from:Lcom/spatial4j/core/shape/Point;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    goto :goto_0

    .line 107
    :cond_4
    iget-object v3, p0, Lorg/apache/lucene/spatial/vector/DistanceValueSource;->strategy:Lorg/apache/lucene/spatial/vector/PointVectorStrategy;

    iget-object v4, v0, Lorg/apache/lucene/spatial/vector/DistanceValueSource;->strategy:Lorg/apache/lucene/spatial/vector/PointVectorStrategy;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
    .locals 8
    .param p1, "context"    # Ljava/util/Map;
    .param p2, "readerContext"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    .line 64
    invoke-virtual {p2}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v6

    .line 66
    .local v6, "reader":Lorg/apache/lucene/index/AtomicReader;
    sget-object v0, Lorg/apache/lucene/search/FieldCache;->DEFAULT:Lorg/apache/lucene/search/FieldCache;

    iget-object v1, p0, Lorg/apache/lucene/spatial/vector/DistanceValueSource;->strategy:Lorg/apache/lucene/spatial/vector/PointVectorStrategy;

    invoke-virtual {v1}, Lorg/apache/lucene/spatial/vector/PointVectorStrategy;->getFieldNameX()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v6, v1, v7}, Lorg/apache/lucene/search/FieldCache;->getDoubles(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;Z)Lorg/apache/lucene/search/FieldCache$Doubles;

    move-result-object v4

    .line 67
    .local v4, "ptX":Lorg/apache/lucene/search/FieldCache$Doubles;
    sget-object v0, Lorg/apache/lucene/search/FieldCache;->DEFAULT:Lorg/apache/lucene/search/FieldCache;

    iget-object v1, p0, Lorg/apache/lucene/spatial/vector/DistanceValueSource;->strategy:Lorg/apache/lucene/spatial/vector/PointVectorStrategy;

    invoke-virtual {v1}, Lorg/apache/lucene/spatial/vector/PointVectorStrategy;->getFieldNameY()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v6, v1, v7}, Lorg/apache/lucene/search/FieldCache;->getDoubles(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;Z)Lorg/apache/lucene/search/FieldCache$Doubles;

    move-result-object v5

    .line 68
    .local v5, "ptY":Lorg/apache/lucene/search/FieldCache$Doubles;
    sget-object v0, Lorg/apache/lucene/search/FieldCache;->DEFAULT:Lorg/apache/lucene/search/FieldCache;

    iget-object v1, p0, Lorg/apache/lucene/spatial/vector/DistanceValueSource;->strategy:Lorg/apache/lucene/spatial/vector/PointVectorStrategy;

    invoke-virtual {v1}, Lorg/apache/lucene/spatial/vector/PointVectorStrategy;->getFieldNameX()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v6, v1}, Lorg/apache/lucene/search/FieldCache;->getDocsWithField(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;)Lorg/apache/lucene/util/Bits;

    move-result-object v2

    .line 69
    .local v2, "validX":Lorg/apache/lucene/util/Bits;
    sget-object v0, Lorg/apache/lucene/search/FieldCache;->DEFAULT:Lorg/apache/lucene/search/FieldCache;

    iget-object v1, p0, Lorg/apache/lucene/spatial/vector/DistanceValueSource;->strategy:Lorg/apache/lucene/spatial/vector/PointVectorStrategy;

    invoke-virtual {v1}, Lorg/apache/lucene/spatial/vector/PointVectorStrategy;->getFieldNameY()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v6, v1}, Lorg/apache/lucene/search/FieldCache;->getDocsWithField(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;)Lorg/apache/lucene/util/Bits;

    move-result-object v3

    .line 71
    .local v3, "validY":Lorg/apache/lucene/util/Bits;
    new-instance v0, Lorg/apache/lucene/spatial/vector/DistanceValueSource$1;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/spatial/vector/DistanceValueSource$1;-><init>(Lorg/apache/lucene/spatial/vector/DistanceValueSource;Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/search/FieldCache$Doubles;Lorg/apache/lucene/search/FieldCache$Doubles;)V

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lorg/apache/lucene/spatial/vector/DistanceValueSource;->from:Lcom/spatial4j/core/shape/Point;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method
