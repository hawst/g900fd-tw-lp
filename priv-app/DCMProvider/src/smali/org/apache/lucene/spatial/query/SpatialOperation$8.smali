.class Lorg/apache/lucene/spatial/query/SpatialOperation$8;
.super Lorg/apache/lucene/spatial/query/SpatialOperation;
.source "SpatialOperation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/spatial/query/SpatialOperation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;ZZZ)V
    .locals 0
    .param p1, "$anonymous0"    # Ljava/lang/String;
    .param p2, "$anonymous1"    # Z
    .param p3, "$anonymous2"    # Z
    .param p4, "$anonymous3"    # Z

    .prologue
    .line 93
    invoke-direct {p0, p1, p2, p3, p4}, Lorg/apache/lucene/spatial/query/SpatialOperation;-><init>(Ljava/lang/String;ZZZ)V

    .line 1
    return-void
.end method


# virtual methods
.method public evaluate(Lcom/spatial4j/core/shape/Shape;Lcom/spatial4j/core/shape/Shape;)Z
    .locals 1
    .param p1, "indexedShape"    # Lcom/spatial4j/core/shape/Shape;
    .param p2, "queryShape"    # Lcom/spatial4j/core/shape/Shape;

    .prologue
    .line 96
    invoke-interface {p2}, Lcom/spatial4j/core/shape/Shape;->hasArea()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1, p2}, Lcom/spatial4j/core/shape/Shape;->relate(Lcom/spatial4j/core/shape/Shape;)Lcom/spatial4j/core/shape/SpatialRelation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/spatial4j/core/shape/SpatialRelation;->intersects()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
