.class public Lorg/apache/lucene/spatial/query/SpatialArgsParser;
.super Ljava/lang/Object;
.source "SpatialArgsParser.java"


# static fields
.field public static final DIST_ERR:Ljava/lang/String; = "distErr"

.field public static final DIST_ERR_PCT:Ljava/lang/String; = "distErrPct"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method protected static parseMap(Ljava/lang/String;)Ljava/util/Map;
    .locals 7
    .param p0, "body"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 118
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 119
    .local v3, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v4, Ljava/util/StringTokenizer;

    const-string v6, " \n\t"

    invoke-direct {v4, p0, v6}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    .local v4, "st":Ljava/util/StringTokenizer;
    :goto_0
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v6

    if-nez v6, :cond_0

    .line 131
    return-object v3

    .line 121
    :cond_0
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v0

    .line 122
    .local v0, "a":Ljava/lang/String;
    const/16 v6, 0x3d

    invoke-virtual {v0, v6}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 123
    .local v1, "idx":I
    if-lez v1, :cond_1

    .line 124
    const/4 v6, 0x0

    invoke-virtual {v0, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 125
    .local v2, "k":Ljava/lang/String;
    add-int/lit8 v6, v1, 0x1

    invoke-virtual {v0, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 126
    .local v5, "v":Ljava/lang/String;
    invoke-interface {v3, v2, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 128
    .end local v2    # "k":Ljava/lang/String;
    .end local v5    # "v":Ljava/lang/String;
    :cond_1
    invoke-interface {v3, v0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method protected static readBool(Ljava/lang/String;Z)Z
    .locals 0
    .param p0, "v"    # Ljava/lang/String;
    .param p1, "defaultValue"    # Z

    .prologue
    .line 112
    if-nez p0, :cond_0

    .end local p1    # "defaultValue":Z
    :goto_0
    return p1

    .restart local p1    # "defaultValue":Z
    :cond_0
    invoke-static {p0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result p1

    goto :goto_0
.end method

.method protected static readDouble(Ljava/lang/String;)Ljava/lang/Double;
    .locals 1
    .param p0, "v"    # Ljava/lang/String;

    .prologue
    .line 108
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v0

    goto :goto_0
.end method

.method static writeSpatialArgs(Lorg/apache/lucene/spatial/query/SpatialArgs;)Ljava/lang/String;
    .locals 10
    .param p0, "args"    # Lorg/apache/lucene/spatial/query/SpatialArgs;

    .prologue
    .line 53
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 54
    .local v0, "str":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lorg/apache/lucene/spatial/query/SpatialArgs;->getOperation()Lorg/apache/lucene/spatial/query/SpatialOperation;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/lucene/spatial/query/SpatialOperation;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 55
    const/16 v1, 0x28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 56
    invoke-virtual {p0}, Lorg/apache/lucene/spatial/query/SpatialArgs;->getShape()Lcom/spatial4j/core/shape/Shape;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 57
    invoke-virtual {p0}, Lorg/apache/lucene/spatial/query/SpatialArgs;->getDistErrPct()Ljava/lang/Double;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 58
    const-string v1, " distErrPct="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    const-string v3, "%.2f%%"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {p0}, Lorg/apache/lucene/spatial/query/SpatialArgs;->getDistErrPct()Ljava/lang/Double;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    const-wide/high16 v8, 0x4059000000000000L    # 100.0

    mul-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 59
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/spatial/query/SpatialArgs;->getDistErr()Ljava/lang/Double;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 60
    const-string v1, " distErr="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/lucene/spatial/query/SpatialArgs;->getDistErr()Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 61
    :cond_1
    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 62
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public parse(Ljava/lang/String;Lcom/spatial4j/core/context/SpatialContext;)Lorg/apache/lucene/spatial/query/SpatialArgs;
    .locals 11
    .param p1, "v"    # Ljava/lang/String;
    .param p2, "ctx"    # Lcom/spatial4j/core/context/SpatialContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Lcom/spatial4j/core/exception/InvalidShapeException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    .line 75
    const/16 v7, 0x28

    invoke-virtual {p1, v7}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    .line 76
    .local v4, "idx":I
    const/16 v7, 0x29

    invoke-virtual {p1, v7}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v3

    .line 78
    .local v3, "edx":I
    if-ltz v4, :cond_0

    if-le v4, v3, :cond_1

    .line 79
    :cond_0
    new-instance v7, Ljava/lang/IllegalArgumentException;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "missing parens: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v7

    .line 82
    :cond_1
    const/4 v7, 0x0

    invoke-virtual {p1, v7, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lorg/apache/lucene/spatial/query/SpatialOperation;->get(Ljava/lang/String;)Lorg/apache/lucene/spatial/query/SpatialOperation;

    move-result-object v5

    .line 84
    .local v5, "op":Lorg/apache/lucene/spatial/query/SpatialOperation;
    add-int/lit8 v7, v4, 0x1

    invoke-virtual {p1, v7, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 85
    .local v2, "body":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v7

    const/4 v8, 0x1

    if-ge v7, v8, :cond_2

    .line 86
    new-instance v7, Ljava/lang/IllegalArgumentException;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "missing body : "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v7

    .line 89
    :cond_2
    invoke-virtual {p2, v2}, Lcom/spatial4j/core/context/SpatialContext;->readShape(Ljava/lang/String;)Lcom/spatial4j/core/shape/Shape;

    move-result-object v6

    .line 90
    .local v6, "shape":Lcom/spatial4j/core/shape/Shape;
    new-instance v1, Lorg/apache/lucene/spatial/query/SpatialArgs;

    invoke-direct {v1, v5, v6}, Lorg/apache/lucene/spatial/query/SpatialArgs;-><init>(Lorg/apache/lucene/spatial/query/SpatialOperation;Lcom/spatial4j/core/shape/Shape;)V

    .line 92
    .local v1, "args":Lorg/apache/lucene/spatial/query/SpatialArgs;
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v8, v3, 0x1

    if-le v7, v8, :cond_3

    .line 93
    add-int/lit8 v7, v3, 0x1

    invoke-virtual {p1, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 94
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_3

    .line 95
    invoke-static {v2}, Lorg/apache/lucene/spatial/query/SpatialArgsParser;->parseMap(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    .line 96
    .local v0, "aa":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v7, "distErrPct"

    invoke-interface {v0, v7}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-static {v7}, Lorg/apache/lucene/spatial/query/SpatialArgsParser;->readDouble(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v7

    invoke-virtual {v1, v7}, Lorg/apache/lucene/spatial/query/SpatialArgs;->setDistErrPct(Ljava/lang/Double;)V

    .line 97
    const-string v7, "distErr"

    invoke-interface {v0, v7}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-static {v7}, Lorg/apache/lucene/spatial/query/SpatialArgsParser;->readDouble(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v7

    invoke-virtual {v1, v7}, Lorg/apache/lucene/spatial/query/SpatialArgs;->setDistErr(Ljava/lang/Double;)V

    .line 98
    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_3

    .line 99
    new-instance v7, Ljava/lang/IllegalArgumentException;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "unused parameters: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v7

    .line 103
    .end local v0    # "aa":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_3
    invoke-virtual {v1}, Lorg/apache/lucene/spatial/query/SpatialArgs;->validate()V

    .line 104
    return-object v1
.end method
