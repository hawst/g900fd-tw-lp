.class public Lorg/apache/lucene/spatial/prefix/RecursivePrefixTreeStrategy;
.super Lorg/apache/lucene/spatial/prefix/PrefixTreeStrategy;
.source "RecursivePrefixTreeStrategy.java"


# instance fields
.field private prefixGridScanLevel:I


# direct methods
.method public constructor <init>(Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;Ljava/lang/String;)V
    .locals 1
    .param p1, "grid"    # Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;
    .param p2, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 42
    .line 43
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/spatial/prefix/PrefixTreeStrategy;-><init>(Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;Ljava/lang/String;Z)V

    .line 44
    invoke-virtual {p1}, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;->getMaxLevels()I

    move-result v0

    add-int/lit8 v0, v0, -0x4

    iput v0, p0, Lorg/apache/lucene/spatial/prefix/RecursivePrefixTreeStrategy;->prefixGridScanLevel:I

    .line 45
    return-void
.end method


# virtual methods
.method public makeFilter(Lorg/apache/lucene/spatial/query/SpatialArgs;)Lorg/apache/lucene/search/Filter;
    .locals 10
    .param p1, "args"    # Lorg/apache/lucene/spatial/query/SpatialArgs;

    .prologue
    .line 66
    invoke-virtual {p1}, Lorg/apache/lucene/spatial/query/SpatialArgs;->getOperation()Lorg/apache/lucene/spatial/query/SpatialOperation;

    move-result-object v9

    .line 67
    .local v9, "op":Lorg/apache/lucene/spatial/query/SpatialOperation;
    sget-object v0, Lorg/apache/lucene/spatial/query/SpatialOperation;->IsDisjointTo:Lorg/apache/lucene/spatial/query/SpatialOperation;

    if-ne v9, v0, :cond_0

    .line 68
    new-instance v0, Lorg/apache/lucene/spatial/DisjointSpatialFilter;

    invoke-virtual {p0}, Lorg/apache/lucene/spatial/prefix/RecursivePrefixTreeStrategy;->getFieldName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p0, p1, v2}, Lorg/apache/lucene/spatial/DisjointSpatialFilter;-><init>(Lorg/apache/lucene/spatial/SpatialStrategy;Lorg/apache/lucene/spatial/query/SpatialArgs;Ljava/lang/String;)V

    .line 83
    :goto_0
    return-object v0

    .line 70
    :cond_0
    invoke-virtual {p1}, Lorg/apache/lucene/spatial/query/SpatialArgs;->getShape()Lcom/spatial4j/core/shape/Shape;

    move-result-object v1

    .line 71
    .local v1, "shape":Lcom/spatial4j/core/shape/Shape;
    iget-object v0, p0, Lorg/apache/lucene/spatial/prefix/RecursivePrefixTreeStrategy;->grid:Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;

    iget-object v2, p0, Lorg/apache/lucene/spatial/prefix/RecursivePrefixTreeStrategy;->ctx:Lcom/spatial4j/core/context/SpatialContext;

    iget-wide v6, p0, Lorg/apache/lucene/spatial/prefix/RecursivePrefixTreeStrategy;->distErrPct:D

    invoke-virtual {p1, v2, v6, v7}, Lorg/apache/lucene/spatial/query/SpatialArgs;->resolveDistErr(Lcom/spatial4j/core/context/SpatialContext;D)D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;->getLevelForDistance(D)I

    move-result v4

    .line 72
    .local v4, "detailLevel":I
    const/4 v8, 0x1

    .line 74
    .local v8, "hasIndexedLeaves":Z
    sget-object v0, Lorg/apache/lucene/spatial/query/SpatialOperation;->Intersects:Lorg/apache/lucene/spatial/query/SpatialOperation;

    if-ne v9, v0, :cond_1

    .line 75
    new-instance v0, Lorg/apache/lucene/spatial/prefix/IntersectsPrefixTreeFilter;

    .line 76
    invoke-virtual {p0}, Lorg/apache/lucene/spatial/prefix/RecursivePrefixTreeStrategy;->getFieldName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/spatial/prefix/RecursivePrefixTreeStrategy;->grid:Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;

    iget v5, p0, Lorg/apache/lucene/spatial/prefix/RecursivePrefixTreeStrategy;->prefixGridScanLevel:I

    .line 77
    const/4 v6, 0x1

    .line 75
    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/spatial/prefix/IntersectsPrefixTreeFilter;-><init>(Lcom/spatial4j/core/shape/Shape;Ljava/lang/String;Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;IIZ)V

    goto :goto_0

    .line 78
    :cond_1
    sget-object v0, Lorg/apache/lucene/spatial/query/SpatialOperation;->IsWithin:Lorg/apache/lucene/spatial/query/SpatialOperation;

    if-ne v9, v0, :cond_2

    .line 79
    new-instance v0, Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter;

    .line 80
    invoke-virtual {p0}, Lorg/apache/lucene/spatial/prefix/RecursivePrefixTreeStrategy;->getFieldName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/spatial/prefix/RecursivePrefixTreeStrategy;->grid:Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;

    iget v5, p0, Lorg/apache/lucene/spatial/prefix/RecursivePrefixTreeStrategy;->prefixGridScanLevel:I

    .line 81
    const-wide/high16 v6, -0x4010000000000000L    # -1.0

    .line 79
    invoke-direct/range {v0 .. v7}, Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter;-><init>(Lcom/spatial4j/core/shape/Shape;Ljava/lang/String;Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;IID)V

    goto :goto_0

    .line 82
    :cond_2
    sget-object v0, Lorg/apache/lucene/spatial/query/SpatialOperation;->Contains:Lorg/apache/lucene/spatial/query/SpatialOperation;

    if-ne v9, v0, :cond_3

    .line 83
    new-instance v0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter;

    invoke-virtual {p0}, Lorg/apache/lucene/spatial/prefix/RecursivePrefixTreeStrategy;->getFieldName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/spatial/prefix/RecursivePrefixTreeStrategy;->grid:Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;

    invoke-direct {v0, v1, v2, v3, v4}, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter;-><init>(Lcom/spatial4j/core/shape/Shape;Ljava/lang/String;Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;I)V

    goto :goto_0

    .line 85
    :cond_3
    new-instance v0, Lorg/apache/lucene/spatial/query/UnsupportedSpatialOperation;

    invoke-direct {v0, v9}, Lorg/apache/lucene/spatial/query/UnsupportedSpatialOperation;-><init>(Lorg/apache/lucene/spatial/query/SpatialOperation;)V

    throw v0
.end method

.method public setPrefixGridScanLevel(I)V
    .locals 0
    .param p1, "prefixGridScanLevel"    # I

    .prologue
    .line 56
    iput p1, p0, Lorg/apache/lucene/spatial/prefix/RecursivePrefixTreeStrategy;->prefixGridScanLevel:I

    .line 57
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 61
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "(prefixGridScanLevel:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/spatial/prefix/RecursivePrefixTreeStrategy;->prefixGridScanLevel:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",SPG:("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/spatial/prefix/RecursivePrefixTreeStrategy;->grid:Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "))"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
