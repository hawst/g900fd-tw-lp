.class Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$ContainsVisitor;
.super Lorg/apache/lucene/spatial/prefix/AbstractPrefixTreeFilter$BaseTermsEnumTraverser;
.source "ContainsPrefixTreeFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ContainsVisitor"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field nextCell:Lorg/apache/lucene/spatial/prefix/tree/Cell;

.field termBytes:Lorg/apache/lucene/util/BytesRef;

.field final synthetic this$0:Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const-class v0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$ContainsVisitor;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter;Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;)V
    .locals 1
    .param p2, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p3, "acceptDocs"    # Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55
    iput-object p1, p0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$ContainsVisitor;->this$0:Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter;

    .line 56
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/spatial/prefix/AbstractPrefixTreeFilter$BaseTermsEnumTraverser;-><init>(Lorg/apache/lucene/spatial/prefix/AbstractPrefixTreeFilter;Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;)V

    .line 59
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$ContainsVisitor;->termBytes:Lorg/apache/lucene/util/BytesRef;

    .line 57
    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$ContainsVisitor;Lorg/apache/lucene/spatial/prefix/tree/Cell;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$ContainsVisitor;->visit(Lorg/apache/lucene/spatial/prefix/tree/Cell;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;

    move-result-object v0

    return-object v0
.end method

.method private collectDocs(Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;
    .locals 6
    .param p1, "acceptContains"    # Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 129
    const/4 v1, 0x0

    .line 131
    .local v1, "set":Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;
    iget-object v3, p0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$ContainsVisitor;->termsEnum:Lorg/apache/lucene/index/TermsEnum;

    iget-object v4, p0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$ContainsVisitor;->docsEnum:Lorg/apache/lucene/index/DocsEnum;

    const/4 v5, 0x0

    invoke-virtual {v3, p1, v4, v5}, Lorg/apache/lucene/index/TermsEnum;->docs(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;I)Lorg/apache/lucene/index/DocsEnum;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$ContainsVisitor;->docsEnum:Lorg/apache/lucene/index/DocsEnum;

    .line 133
    :goto_0
    iget-object v3, p0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$ContainsVisitor;->docsEnum:Lorg/apache/lucene/index/DocsEnum;

    invoke-virtual {v3}, Lorg/apache/lucene/index/DocsEnum;->nextDoc()I

    move-result v0

    .local v0, "docid":I
    const v3, 0x7fffffff

    if-ne v0, v3, :cond_0

    .line 142
    return-object v1

    .line 134
    :cond_0
    if-nez v1, :cond_2

    .line 135
    iget-object v3, p0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$ContainsVisitor;->termsEnum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v3}, Lorg/apache/lucene/index/TermsEnum;->docFreq()I

    move-result v2

    .line 136
    .local v2, "size":I
    if-gtz v2, :cond_1

    .line 137
    const/16 v2, 0x10

    .line 138
    :cond_1
    new-instance v1, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;

    .end local v1    # "set":Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;
    invoke-direct {v1, v2}, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;-><init>(I)V

    .line 140
    .end local v2    # "size":I
    .restart local v1    # "set":Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;
    :cond_2
    invoke-virtual {v1, v0}, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;->set(I)V

    goto :goto_0
.end method

.method private getDocs(Lorg/apache/lucene/spatial/prefix/tree/Cell;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;
    .locals 2
    .param p1, "cell"    # Lorg/apache/lucene/spatial/prefix/tree/Cell;
    .param p2, "acceptContains"    # Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 107
    sget-boolean v0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$ContainsVisitor;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {p1}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->getTokenBytes()[B

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>([B)V

    iget-object v1, p0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$ContainsVisitor;->termBytes:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/BytesRef;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 109
    :cond_0
    invoke-direct {p0, p2}, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$ContainsVisitor;->collectDocs(Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;

    move-result-object v0

    return-object v0
.end method

.method private getLeafDocs(Lorg/apache/lucene/spatial/prefix/tree/Cell;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;
    .locals 7
    .param p1, "leafCell"    # Lorg/apache/lucene/spatial/prefix/tree/Cell;
    .param p2, "acceptContains"    # Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 113
    sget-boolean v2, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$ContainsVisitor;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    new-instance v2, Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {p1}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->getTokenBytes()[B

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/apache/lucene/util/BytesRef;-><init>([B)V

    iget-object v3, p0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$ContainsVisitor;->termBytes:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/util/BytesRef;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 115
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$ContainsVisitor;->termsEnum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v2}, Lorg/apache/lucene/index/TermsEnum;->next()Lorg/apache/lucene/util/BytesRef;

    move-result-object v0

    .line 116
    .local v0, "nextTerm":Lorg/apache/lucene/util/BytesRef;
    if-nez v0, :cond_2

    .line 117
    iput-object v1, p0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$ContainsVisitor;->termsEnum:Lorg/apache/lucene/index/TermsEnum;

    .line 124
    :cond_1
    :goto_0
    return-object v1

    .line 120
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$ContainsVisitor;->this$0:Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter;

    iget-object v2, v2, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter;->grid:Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;

    iget-object v3, v0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v4, v0, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget v5, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    iget-object v6, p0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$ContainsVisitor;->nextCell:Lorg/apache/lucene/spatial/prefix/tree/Cell;

    invoke-virtual {v2, v3, v4, v5, v6}, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;->getCell([BIILorg/apache/lucene/spatial/prefix/tree/Cell;)Lorg/apache/lucene/spatial/prefix/tree/Cell;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$ContainsVisitor;->nextCell:Lorg/apache/lucene/spatial/prefix/tree/Cell;

    .line 121
    iget-object v2, p0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$ContainsVisitor;->nextCell:Lorg/apache/lucene/spatial/prefix/tree/Cell;

    invoke-virtual {v2}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->getLevel()I

    move-result v2

    invoke-virtual {p1}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->getLevel()I

    move-result v3

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$ContainsVisitor;->nextCell:Lorg/apache/lucene/spatial/prefix/tree/Cell;

    invoke-virtual {v2}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->isLeaf()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 122
    invoke-direct {p0, p2}, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$ContainsVisitor;->collectDocs(Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;

    move-result-object v1

    goto :goto_0
.end method

.method private seekExact(Lorg/apache/lucene/spatial/prefix/tree/Cell;)Z
    .locals 4
    .param p1, "cell"    # Lorg/apache/lucene/spatial/prefix/tree/Cell;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 99
    sget-boolean v0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$ContainsVisitor;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {p1}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->getTokenBytes()[B

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>([B)V

    iget-object v1, p0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$ContainsVisitor;->termBytes:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/BytesRef;->compareTo(Lorg/apache/lucene/util/BytesRef;)I

    move-result v0

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 101
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$ContainsVisitor;->termBytes:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {p1}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->getTokenBytes()[B

    move-result-object v1

    iput-object v1, v0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 102
    iget-object v0, p0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$ContainsVisitor;->termBytes:Lorg/apache/lucene/util/BytesRef;

    iget-object v1, p0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$ContainsVisitor;->termBytes:Lorg/apache/lucene/util/BytesRef;

    iget-object v1, v1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    array-length v1, v1

    iput v1, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 103
    iget-object v1, p0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$ContainsVisitor;->termsEnum:Lorg/apache/lucene/index/TermsEnum;

    iget-object v2, p0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$ContainsVisitor;->termBytes:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {p1}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->getLevel()I

    move-result v0

    const/4 v3, 0x2

    if-gt v0, v3, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v2, v0}, Lorg/apache/lucene/index/TermsEnum;->seekExact(Lorg/apache/lucene/util/BytesRef;Z)Z

    move-result v0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private visit(Lorg/apache/lucene/spatial/prefix/tree/Cell;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;
    .locals 7
    .param p1, "cell"    # Lorg/apache/lucene/spatial/prefix/tree/Cell;
    .param p2, "acceptContains"    # Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 65
    iget-object v4, p0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$ContainsVisitor;->termsEnum:Lorg/apache/lucene/index/TermsEnum;

    if-nez v4, :cond_1

    .line 66
    const/4 v0, 0x0

    .line 95
    :cond_0
    :goto_0
    return-object v0

    .line 69
    :cond_1
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$ContainsVisitor;->getLeafDocs(Lorg/apache/lucene/spatial/prefix/tree/Cell;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;

    move-result-object v1

    .line 72
    .local v1, "leafDocs":Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;
    const/4 v0, 0x0

    .line 73
    .local v0, "combinedSubResults":Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;
    iget-object v4, p0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$ContainsVisitor;->this$0:Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter;

    iget-object v4, v4, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter;->queryShape:Lcom/spatial4j/core/shape/Shape;

    invoke-virtual {p1, v4}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->getSubCells(Lcom/spatial4j/core/shape/Shape;)Ljava/util/Collection;

    move-result-object v3

    .line 74
    .local v3, "subCells":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/spatial/prefix/tree/Cell;>;"
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_3

    .line 90
    :cond_2
    if-eqz v0, :cond_7

    .line 91
    if-eqz v1, :cond_0

    .line 93
    invoke-virtual {v1, v0}, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;->union(Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;)Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;

    move-result-object v0

    goto :goto_0

    .line 74
    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/spatial/prefix/tree/Cell;

    .line 75
    .local v2, "subCell":Lorg/apache/lucene/spatial/prefix/tree/Cell;
    invoke-direct {p0, v2}, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$ContainsVisitor;->seekExact(Lorg/apache/lucene/spatial/prefix/tree/Cell;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 76
    const/4 v0, 0x0

    .line 84
    :goto_2
    if-eqz v0, :cond_2

    .line 86
    move-object p2, v0

    goto :goto_1

    .line 77
    :cond_4
    invoke-virtual {v2}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->getLevel()I

    move-result v5

    iget-object v6, p0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$ContainsVisitor;->this$0:Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter;

    iget v6, v6, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter;->detailLevel:I

    if-ne v5, v6, :cond_5

    .line 78
    invoke-direct {p0, v2, p2}, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$ContainsVisitor;->getDocs(Lorg/apache/lucene/spatial/prefix/tree/Cell;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;

    move-result-object v0

    goto :goto_2

    .line 79
    :cond_5
    invoke-virtual {v2}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->getShapeRel()Lcom/spatial4j/core/shape/SpatialRelation;

    move-result-object v5

    sget-object v6, Lcom/spatial4j/core/shape/SpatialRelation;->WITHIN:Lcom/spatial4j/core/shape/SpatialRelation;

    if-ne v5, v6, :cond_6

    .line 80
    invoke-direct {p0, v2, p2}, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$ContainsVisitor;->getLeafDocs(Lorg/apache/lucene/spatial/prefix/tree/Cell;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;

    move-result-object v0

    goto :goto_2

    .line 82
    :cond_6
    invoke-direct {p0, v2, p2}, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$ContainsVisitor;->visit(Lorg/apache/lucene/spatial/prefix/tree/Cell;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;

    move-result-object v0

    goto :goto_2

    .end local v2    # "subCell":Lorg/apache/lucene/spatial/prefix/tree/Cell;
    :cond_7
    move-object v0, v1

    .line 95
    goto :goto_0
.end method
