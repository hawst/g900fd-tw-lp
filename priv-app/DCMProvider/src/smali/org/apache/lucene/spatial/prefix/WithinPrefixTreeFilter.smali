.class public Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter;
.super Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter;
.source "WithinPrefixTreeFilter.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final bufferedQueryShape:Lcom/spatial4j/core/shape/Shape;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const-class v0, Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/spatial4j/core/shape/Shape;Ljava/lang/String;Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;IID)V
    .locals 2
    .param p1, "queryShape"    # Lcom/spatial4j/core/shape/Shape;
    .param p2, "fieldName"    # Ljava/lang/String;
    .param p3, "grid"    # Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;
    .param p4, "detailLevel"    # I
    .param p5, "prefixGridScanLevel"    # I
    .param p6, "queryBuffer"    # D

    .prologue
    .line 67
    invoke-direct/range {p0 .. p5}, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter;-><init>(Lcom/spatial4j/core/shape/Shape;Ljava/lang/String;Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;II)V

    .line 68
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    cmpl-double v0, p6, v0

    if-nez v0, :cond_0

    .line 69
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter;->bufferedQueryShape:Lcom/spatial4j/core/shape/Shape;

    .line 72
    :goto_0
    return-void

    .line 71
    :cond_0
    invoke-virtual {p0, p1, p6, p7}, Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter;->bufferShape(Lcom/spatial4j/core/shape/Shape;D)Lcom/spatial4j/core/shape/Shape;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter;->bufferedQueryShape:Lcom/spatial4j/core/shape/Shape;

    goto :goto_0
.end method

.method static synthetic access$0(Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter;)Lcom/spatial4j/core/shape/Shape;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter;->bufferedQueryShape:Lcom/spatial4j/core/shape/Shape;

    return-object v0
.end method


# virtual methods
.method protected bufferShape(Lcom/spatial4j/core/shape/Shape;D)Lcom/spatial4j/core/shape/Shape;
    .locals 22
    .param p1, "shape"    # Lcom/spatial4j/core/shape/Shape;
    .param p2, "distErr"    # D

    .prologue
    .line 78
    const-wide/16 v18, 0x0

    cmpg-double v15, p2, v18

    if-gtz v15, :cond_0

    .line 79
    new-instance v15, Ljava/lang/IllegalArgumentException;

    const-string v18, "distErr must be > 0"

    move-object/from16 v0, v18

    invoke-direct {v15, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v15

    .line 80
    :cond_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter;->grid:Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;

    invoke-virtual {v15}, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;->getSpatialContext()Lcom/spatial4j/core/context/SpatialContext;

    move-result-object v5

    .line 81
    .local v5, "ctx":Lcom/spatial4j/core/context/SpatialContext;
    move-object/from16 v0, p1

    instance-of v15, v0, Lcom/spatial4j/core/shape/Point;

    if-eqz v15, :cond_1

    .line 82
    check-cast p1, Lcom/spatial4j/core/shape/Point;

    .end local p1    # "shape":Lcom/spatial4j/core/shape/Shape;
    move-object/from16 v0, p1

    move-wide/from16 v1, p2

    invoke-virtual {v5, v0, v1, v2}, Lcom/spatial4j/core/context/SpatialContext;->makeCircle(Lcom/spatial4j/core/shape/Point;D)Lcom/spatial4j/core/shape/Circle;

    move-result-object v15

    .line 114
    :goto_0
    return-object v15

    .line 83
    .restart local p1    # "shape":Lcom/spatial4j/core/shape/Shape;
    :cond_1
    move-object/from16 v0, p1

    instance-of v15, v0, Lcom/spatial4j/core/shape/Circle;

    if-eqz v15, :cond_3

    move-object/from16 v14, p1

    .line 84
    check-cast v14, Lcom/spatial4j/core/shape/Circle;

    .line 85
    .local v14, "circle":Lcom/spatial4j/core/shape/Circle;
    invoke-interface {v14}, Lcom/spatial4j/core/shape/Circle;->getRadius()D

    move-result-wide v18

    add-double v16, v18, p2

    .line 86
    .local v16, "newDist":D
    invoke-virtual {v5}, Lcom/spatial4j/core/context/SpatialContext;->isGeo()Z

    move-result v15

    if-eqz v15, :cond_2

    const-wide v18, 0x4066800000000000L    # 180.0

    cmpl-double v15, v16, v18

    if-lez v15, :cond_2

    .line 87
    const-wide v16, 0x4066800000000000L    # 180.0

    .line 88
    :cond_2
    invoke-interface {v14}, Lcom/spatial4j/core/shape/Circle;->getCenter()Lcom/spatial4j/core/shape/Point;

    move-result-object v15

    move-wide/from16 v0, v16

    invoke-virtual {v5, v15, v0, v1}, Lcom/spatial4j/core/context/SpatialContext;->makeCircle(Lcom/spatial4j/core/shape/Point;D)Lcom/spatial4j/core/shape/Circle;

    move-result-object v15

    goto :goto_0

    .line 90
    .end local v14    # "circle":Lcom/spatial4j/core/shape/Circle;
    .end local v16    # "newDist":D
    :cond_3
    invoke-interface/range {p1 .. p1}, Lcom/spatial4j/core/shape/Shape;->getBoundingBox()Lcom/spatial4j/core/shape/Rectangle;

    move-result-object v4

    .line 91
    .local v4, "bbox":Lcom/spatial4j/core/shape/Rectangle;
    invoke-interface {v4}, Lcom/spatial4j/core/shape/Rectangle;->getMinX()D

    move-result-wide v18

    sub-double v6, v18, p2

    .line 92
    .local v6, "newMinX":D
    invoke-interface {v4}, Lcom/spatial4j/core/shape/Rectangle;->getMaxX()D

    move-result-wide v18

    add-double v8, v18, p2

    .line 93
    .local v8, "newMaxX":D
    invoke-interface {v4}, Lcom/spatial4j/core/shape/Rectangle;->getMinY()D

    move-result-wide v18

    sub-double v10, v18, p2

    .line 94
    .local v10, "newMinY":D
    invoke-interface {v4}, Lcom/spatial4j/core/shape/Rectangle;->getMaxY()D

    move-result-wide v18

    add-double v12, v18, p2

    .line 95
    .local v12, "newMaxY":D
    invoke-virtual {v5}, Lcom/spatial4j/core/context/SpatialContext;->isGeo()Z

    move-result v15

    if-eqz v15, :cond_8

    .line 96
    const-wide v18, -0x3fa9800000000000L    # -90.0

    cmpg-double v15, v10, v18

    if-gez v15, :cond_4

    .line 97
    const-wide v10, -0x3fa9800000000000L    # -90.0

    .line 98
    :cond_4
    const-wide v18, 0x4056800000000000L    # 90.0

    cmpl-double v15, v12, v18

    if-lez v15, :cond_5

    .line 99
    const-wide v12, 0x4056800000000000L    # 90.0

    .line 100
    :cond_5
    const-wide v18, -0x3fa9800000000000L    # -90.0

    cmpl-double v15, v10, v18

    if-eqz v15, :cond_6

    const-wide v18, 0x4056800000000000L    # 90.0

    cmpl-double v15, v12, v18

    if-eqz v15, :cond_6

    invoke-interface {v4}, Lcom/spatial4j/core/shape/Rectangle;->getWidth()D

    move-result-wide v18

    const-wide/high16 v20, 0x4000000000000000L    # 2.0

    mul-double v20, v20, p2

    add-double v18, v18, v20

    const-wide v20, 0x4076800000000000L    # 360.0

    cmpl-double v15, v18, v20

    if-lez v15, :cond_7

    .line 101
    :cond_6
    const-wide v6, -0x3f99800000000000L    # -180.0

    .line 102
    const-wide v8, 0x4066800000000000L    # 180.0

    .line 114
    :goto_1
    invoke-virtual/range {v5 .. v13}, Lcom/spatial4j/core/context/SpatialContext;->makeRectangle(DDDD)Lcom/spatial4j/core/shape/Rectangle;

    move-result-object v15

    goto/16 :goto_0

    .line 104
    :cond_7
    invoke-static {v6, v7}, Lcom/spatial4j/core/distance/DistanceUtils;->normLonDEG(D)D

    move-result-wide v6

    .line 105
    invoke-static {v8, v9}, Lcom/spatial4j/core/distance/DistanceUtils;->normLonDEG(D)D

    move-result-wide v8

    .line 107
    goto :goto_1

    .line 109
    :cond_8
    invoke-virtual {v5}, Lcom/spatial4j/core/context/SpatialContext;->getWorldBounds()Lcom/spatial4j/core/shape/Rectangle;

    move-result-object v15

    invoke-interface {v15}, Lcom/spatial4j/core/shape/Rectangle;->getMinX()D

    move-result-wide v18

    move-wide/from16 v0, v18

    invoke-static {v6, v7, v0, v1}, Ljava/lang/Math;->max(DD)D

    move-result-wide v6

    .line 110
    invoke-virtual {v5}, Lcom/spatial4j/core/context/SpatialContext;->getWorldBounds()Lcom/spatial4j/core/shape/Rectangle;

    move-result-object v15

    invoke-interface {v15}, Lcom/spatial4j/core/shape/Rectangle;->getMaxX()D

    move-result-wide v18

    move-wide/from16 v0, v18

    invoke-static {v8, v9, v0, v1}, Ljava/lang/Math;->min(DD)D

    move-result-wide v8

    .line 111
    invoke-virtual {v5}, Lcom/spatial4j/core/context/SpatialContext;->getWorldBounds()Lcom/spatial4j/core/shape/Rectangle;

    move-result-object v15

    invoke-interface {v15}, Lcom/spatial4j/core/shape/Rectangle;->getMinY()D

    move-result-wide v18

    move-wide/from16 v0, v18

    invoke-static {v10, v11, v0, v1}, Ljava/lang/Math;->max(DD)D

    move-result-wide v10

    .line 112
    invoke-virtual {v5}, Lcom/spatial4j/core/context/SpatialContext;->getWorldBounds()Lcom/spatial4j/core/shape/Rectangle;

    move-result-object v15

    invoke-interface {v15}, Lcom/spatial4j/core/shape/Rectangle;->getMaxY()D

    move-result-wide v18

    move-wide/from16 v0, v18

    invoke-static {v12, v13, v0, v1}, Ljava/lang/Math;->min(DD)D

    move-result-wide v12

    goto :goto_1
.end method

.method public getDocIdSet(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/DocIdSet;
    .locals 6
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "acceptDocs"    # Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 121
    new-instance v0, Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter$1;

    const/4 v5, 0x1

    move-object v1, p0

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter$1;-><init>(Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter;Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter;Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;Z)V

    .line 205
    invoke-virtual {v0}, Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter$1;->getDocIdSet()Lorg/apache/lucene/search/DocIdSet;

    move-result-object v0

    .line 121
    return-object v0
.end method
