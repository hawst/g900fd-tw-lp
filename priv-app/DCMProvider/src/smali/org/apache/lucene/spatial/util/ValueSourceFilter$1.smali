.class Lorg/apache/lucene/spatial/util/ValueSourceFilter$1;
.super Lorg/apache/lucene/search/FilteredDocIdSet;
.source "ValueSourceFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/spatial/util/ValueSourceFilter;->getDocIdSet(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/DocIdSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/spatial/util/ValueSourceFilter;

.field private final synthetic val$values:Lorg/apache/lucene/queries/function/FunctionValues;


# direct methods
.method constructor <init>(Lorg/apache/lucene/spatial/util/ValueSourceFilter;Lorg/apache/lucene/search/DocIdSet;Lorg/apache/lucene/queries/function/FunctionValues;)V
    .locals 0
    .param p2, "$anonymous0"    # Lorg/apache/lucene/search/DocIdSet;

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/spatial/util/ValueSourceFilter$1;->this$0:Lorg/apache/lucene/spatial/util/ValueSourceFilter;

    iput-object p3, p0, Lorg/apache/lucene/spatial/util/ValueSourceFilter$1;->val$values:Lorg/apache/lucene/queries/function/FunctionValues;

    .line 57
    invoke-direct {p0, p2}, Lorg/apache/lucene/search/FilteredDocIdSet;-><init>(Lorg/apache/lucene/search/DocIdSet;)V

    return-void
.end method


# virtual methods
.method public match(I)Z
    .locals 4
    .param p1, "doc"    # I

    .prologue
    .line 60
    iget-object v2, p0, Lorg/apache/lucene/spatial/util/ValueSourceFilter$1;->val$values:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->doubleVal(I)D

    move-result-wide v0

    .line 61
    .local v0, "val":D
    iget-object v2, p0, Lorg/apache/lucene/spatial/util/ValueSourceFilter$1;->this$0:Lorg/apache/lucene/spatial/util/ValueSourceFilter;

    iget-wide v2, v2, Lorg/apache/lucene/spatial/util/ValueSourceFilter;->min:D

    cmpl-double v2, v0, v2

    if-ltz v2, :cond_0

    iget-object v2, p0, Lorg/apache/lucene/spatial/util/ValueSourceFilter$1;->this$0:Lorg/apache/lucene/spatial/util/ValueSourceFilter;

    iget-wide v2, v2, Lorg/apache/lucene/spatial/util/ValueSourceFilter;->max:D

    cmpg-double v2, v0, v2

    if-gtz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method
