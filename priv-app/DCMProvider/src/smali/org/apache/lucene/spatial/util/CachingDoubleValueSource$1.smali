.class Lorg/apache/lucene/spatial/util/CachingDoubleValueSource$1;
.super Lorg/apache/lucene/queries/function/FunctionValues;
.source "CachingDoubleValueSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/spatial/util/CachingDoubleValueSource;->getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/spatial/util/CachingDoubleValueSource;

.field private final synthetic val$base:I

.field private final synthetic val$vals:Lorg/apache/lucene/queries/function/FunctionValues;


# direct methods
.method constructor <init>(Lorg/apache/lucene/spatial/util/CachingDoubleValueSource;ILorg/apache/lucene/queries/function/FunctionValues;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/spatial/util/CachingDoubleValueSource$1;->this$0:Lorg/apache/lucene/spatial/util/CachingDoubleValueSource;

    iput p2, p0, Lorg/apache/lucene/spatial/util/CachingDoubleValueSource$1;->val$base:I

    iput-object p3, p0, Lorg/apache/lucene/spatial/util/CachingDoubleValueSource$1;->val$vals:Lorg/apache/lucene/queries/function/FunctionValues;

    .line 53
    invoke-direct {p0}, Lorg/apache/lucene/queries/function/FunctionValues;-><init>()V

    return-void
.end method


# virtual methods
.method public doubleVal(I)D
    .locals 4
    .param p1, "doc"    # I

    .prologue
    .line 57
    iget v2, p0, Lorg/apache/lucene/spatial/util/CachingDoubleValueSource$1;->val$base:I

    add-int/2addr v2, p1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 58
    .local v0, "key":Ljava/lang/Integer;
    iget-object v2, p0, Lorg/apache/lucene/spatial/util/CachingDoubleValueSource$1;->this$0:Lorg/apache/lucene/spatial/util/CachingDoubleValueSource;

    iget-object v2, v2, Lorg/apache/lucene/spatial/util/CachingDoubleValueSource;->cache:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Double;

    .line 59
    .local v1, "v":Ljava/lang/Double;
    if-nez v1, :cond_0

    .line 60
    iget-object v2, p0, Lorg/apache/lucene/spatial/util/CachingDoubleValueSource$1;->val$vals:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->doubleVal(I)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    .line 61
    iget-object v2, p0, Lorg/apache/lucene/spatial/util/CachingDoubleValueSource$1;->this$0:Lorg/apache/lucene/spatial/util/CachingDoubleValueSource;

    iget-object v2, v2, Lorg/apache/lucene/spatial/util/CachingDoubleValueSource;->cache:Ljava/util/Map;

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    :cond_0
    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    return-wide v2
.end method

.method public floatVal(I)F
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 68
    invoke-virtual {p0, p1}, Lorg/apache/lucene/spatial/util/CachingDoubleValueSource$1;->doubleVal(I)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method public toString(I)Ljava/lang/String;
    .locals 4
    .param p1, "doc"    # I

    .prologue
    .line 73
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0, p1}, Lorg/apache/lucene/spatial/util/CachingDoubleValueSource$1;->doubleVal(I)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
