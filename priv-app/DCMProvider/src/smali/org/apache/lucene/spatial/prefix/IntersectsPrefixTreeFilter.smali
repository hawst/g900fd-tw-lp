.class public Lorg/apache/lucene/spatial/prefix/IntersectsPrefixTreeFilter;
.super Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter;
.source "IntersectsPrefixTreeFilter.java"


# instance fields
.field private final hasIndexedLeaves:Z


# direct methods
.method public constructor <init>(Lcom/spatial4j/core/shape/Shape;Ljava/lang/String;Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;IIZ)V
    .locals 0
    .param p1, "queryShape"    # Lcom/spatial4j/core/shape/Shape;
    .param p2, "fieldName"    # Ljava/lang/String;
    .param p3, "grid"    # Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;
    .param p4, "detailLevel"    # I
    .param p5, "prefixGridScanLevel"    # I
    .param p6, "hasIndexedLeaves"    # Z

    .prologue
    .line 44
    invoke-direct/range {p0 .. p5}, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter;-><init>(Lcom/spatial4j/core/shape/Shape;Ljava/lang/String;Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;II)V

    .line 45
    iput-boolean p6, p0, Lorg/apache/lucene/spatial/prefix/IntersectsPrefixTreeFilter;->hasIndexedLeaves:Z

    .line 46
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 50
    invoke-super {p0, p1}, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lorg/apache/lucene/spatial/prefix/IntersectsPrefixTreeFilter;->hasIndexedLeaves:Z

    check-cast p1, Lorg/apache/lucene/spatial/prefix/IntersectsPrefixTreeFilter;

    .end local p1    # "o":Ljava/lang/Object;
    iget-boolean v1, p1, Lorg/apache/lucene/spatial/prefix/IntersectsPrefixTreeFilter;->hasIndexedLeaves:Z

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDocIdSet(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/DocIdSet;
    .locals 6
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "acceptDocs"    # Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55
    new-instance v0, Lorg/apache/lucene/spatial/prefix/IntersectsPrefixTreeFilter$1;

    iget-boolean v5, p0, Lorg/apache/lucene/spatial/prefix/IntersectsPrefixTreeFilter;->hasIndexedLeaves:Z

    move-object v1, p0

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/spatial/prefix/IntersectsPrefixTreeFilter$1;-><init>(Lorg/apache/lucene/spatial/prefix/IntersectsPrefixTreeFilter;Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter;Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;Z)V

    .line 96
    invoke-virtual {v0}, Lorg/apache/lucene/spatial/prefix/IntersectsPrefixTreeFilter$1;->getDocIdSet()Lorg/apache/lucene/search/DocIdSet;

    move-result-object v0

    .line 55
    return-object v0
.end method
