.class public Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree$Factory;
.super Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTreeFactory;
.source "QuadPrefixTree.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTreeFactory;-><init>()V

    return-void
.end method


# virtual methods
.method protected getLevelForDistance(D)I
    .locals 3
    .param p1, "degrees"    # D

    .prologue
    .line 49
    new-instance v0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;

    iget-object v1, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree$Factory;->ctx:Lcom/spatial4j/core/context/SpatialContext;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;-><init>(Lcom/spatial4j/core/context/SpatialContext;I)V

    .line 50
    .local v0, "grid":Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;
    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->getLevelForDistance(D)I

    move-result v1

    return v1
.end method

.method protected newSPT()Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;
    .locals 3

    .prologue
    .line 55
    new-instance v1, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;

    iget-object v2, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree$Factory;->ctx:Lcom/spatial4j/core/context/SpatialContext;

    .line 56
    iget-object v0, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree$Factory;->maxLevels:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree$Factory;->maxLevels:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 55
    :goto_0
    invoke-direct {v1, v2, v0}, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;-><init>(Lcom/spatial4j/core/context/SpatialContext;I)V

    return-object v1

    .line 56
    :cond_0
    const/16 v0, 0x32

    goto :goto_0
.end method
