.class public abstract Lorg/apache/lucene/spatial/prefix/PrefixTreeStrategy;
.super Lorg/apache/lucene/spatial/SpatialStrategy;
.source "PrefixTreeStrategy.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/spatial/prefix/PrefixTreeStrategy$CellTokenStream;
    }
.end annotation


# static fields
.field public static final FIELD_TYPE:Lorg/apache/lucene/document/FieldType;


# instance fields
.field protected defaultFieldValuesArrayLen:I

.field protected distErrPct:D

.field protected final grid:Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;

.field private final provider:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/spatial/prefix/PointPrefixTreeFieldCacheProvider;",
            ">;"
        }
    .end annotation
.end field

.field protected final simplifyIndexedCells:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 140
    new-instance v0, Lorg/apache/lucene/document/FieldType;

    invoke-direct {v0}, Lorg/apache/lucene/document/FieldType;-><init>()V

    sput-object v0, Lorg/apache/lucene/spatial/prefix/PrefixTreeStrategy;->FIELD_TYPE:Lorg/apache/lucene/document/FieldType;

    .line 143
    sget-object v0, Lorg/apache/lucene/spatial/prefix/PrefixTreeStrategy;->FIELD_TYPE:Lorg/apache/lucene/document/FieldType;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/FieldType;->setIndexed(Z)V

    .line 144
    sget-object v0, Lorg/apache/lucene/spatial/prefix/PrefixTreeStrategy;->FIELD_TYPE:Lorg/apache/lucene/document/FieldType;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/FieldType;->setTokenized(Z)V

    .line 145
    sget-object v0, Lorg/apache/lucene/spatial/prefix/PrefixTreeStrategy;->FIELD_TYPE:Lorg/apache/lucene/document/FieldType;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/FieldType;->setOmitNorms(Z)V

    .line 146
    sget-object v0, Lorg/apache/lucene/spatial/prefix/PrefixTreeStrategy;->FIELD_TYPE:Lorg/apache/lucene/document/FieldType;

    sget-object v1, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_ONLY:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/FieldType;->setIndexOptions(Lorg/apache/lucene/index/FieldInfo$IndexOptions;)V

    .line 147
    sget-object v0, Lorg/apache/lucene/spatial/prefix/PrefixTreeStrategy;->FIELD_TYPE:Lorg/apache/lucene/document/FieldType;

    invoke-virtual {v0}, Lorg/apache/lucene/document/FieldType;->freeze()V

    .line 148
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;Ljava/lang/String;Z)V
    .locals 2
    .param p1, "grid"    # Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;
    .param p2, "fieldName"    # Ljava/lang/String;
    .param p3, "simplifyIndexedCells"    # Z

    .prologue
    .line 87
    invoke-virtual {p1}, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;->getSpatialContext()Lcom/spatial4j/core/context/SpatialContext;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lorg/apache/lucene/spatial/SpatialStrategy;-><init>(Lcom/spatial4j/core/context/SpatialContext;Ljava/lang/String;)V

    .line 81
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/spatial/prefix/PrefixTreeStrategy;->provider:Ljava/util/Map;

    .line 83
    const/4 v0, 0x2

    iput v0, p0, Lorg/apache/lucene/spatial/prefix/PrefixTreeStrategy;->defaultFieldValuesArrayLen:I

    .line 84
    const-wide v0, 0x3f9999999999999aL    # 0.025

    iput-wide v0, p0, Lorg/apache/lucene/spatial/prefix/PrefixTreeStrategy;->distErrPct:D

    .line 88
    iput-object p1, p0, Lorg/apache/lucene/spatial/prefix/PrefixTreeStrategy;->grid:Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;

    .line 89
    iput-boolean p3, p0, Lorg/apache/lucene/spatial/prefix/PrefixTreeStrategy;->simplifyIndexedCells:Z

    .line 90
    return-void
.end method


# virtual methods
.method public createIndexableFields(Lcom/spatial4j/core/shape/Shape;)[Lorg/apache/lucene/document/Field;
    .locals 5
    .param p1, "shape"    # Lcom/spatial4j/core/shape/Shape;

    .prologue
    .line 123
    iget-wide v2, p0, Lorg/apache/lucene/spatial/prefix/PrefixTreeStrategy;->distErrPct:D

    iget-object v4, p0, Lorg/apache/lucene/spatial/prefix/PrefixTreeStrategy;->ctx:Lcom/spatial4j/core/context/SpatialContext;

    invoke-static {p1, v2, v3, v4}, Lorg/apache/lucene/spatial/query/SpatialArgs;->calcDistanceFromErrPct(Lcom/spatial4j/core/shape/Shape;DLcom/spatial4j/core/context/SpatialContext;)D

    move-result-wide v0

    .line 124
    .local v0, "distErr":D
    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/spatial/prefix/PrefixTreeStrategy;->createIndexableFields(Lcom/spatial4j/core/shape/Shape;D)[Lorg/apache/lucene/document/Field;

    move-result-object v2

    return-object v2
.end method

.method public createIndexableFields(Lcom/spatial4j/core/shape/Shape;D)[Lorg/apache/lucene/document/Field;
    .locals 8
    .param p1, "shape"    # Lcom/spatial4j/core/shape/Shape;
    .param p2, "distErr"    # D

    .prologue
    const/4 v6, 0x1

    .line 128
    iget-object v3, p0, Lorg/apache/lucene/spatial/prefix/PrefixTreeStrategy;->grid:Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;

    invoke-virtual {v3, p2, p3}, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;->getLevelForDistance(D)I

    move-result v1

    .line 129
    .local v1, "detailLevel":I
    iget-object v3, p0, Lorg/apache/lucene/spatial/prefix/PrefixTreeStrategy;->grid:Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;

    iget-boolean v4, p0, Lorg/apache/lucene/spatial/prefix/PrefixTreeStrategy;->simplifyIndexedCells:Z

    invoke-virtual {v3, p1, v1, v6, v4}, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;->getCells(Lcom/spatial4j/core/shape/Shape;IZZ)Ljava/util/List;

    move-result-object v0

    .line 134
    .local v0, "cells":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/spatial/prefix/tree/Cell;>;"
    new-instance v2, Lorg/apache/lucene/document/Field;

    invoke-virtual {p0}, Lorg/apache/lucene/spatial/prefix/PrefixTreeStrategy;->getFieldName()Ljava/lang/String;

    move-result-object v3

    .line 135
    new-instance v4, Lorg/apache/lucene/spatial/prefix/PrefixTreeStrategy$CellTokenStream;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/apache/lucene/spatial/prefix/PrefixTreeStrategy$CellTokenStream;-><init>(Ljava/util/Iterator;)V

    sget-object v5, Lorg/apache/lucene/spatial/prefix/PrefixTreeStrategy;->FIELD_TYPE:Lorg/apache/lucene/document/FieldType;

    .line 134
    invoke-direct {v2, v3, v4, v5}, Lorg/apache/lucene/document/Field;-><init>(Ljava/lang/String;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/document/FieldType;)V

    .line 136
    .local v2, "field":Lorg/apache/lucene/document/Field;
    new-array v3, v6, [Lorg/apache/lucene/document/Field;

    const/4 v4, 0x0

    aput-object v2, v3, v4

    return-object v3
.end method

.method public getDistErrPct()D
    .locals 2

    .prologue
    .line 103
    iget-wide v0, p0, Lorg/apache/lucene/spatial/prefix/PrefixTreeStrategy;->distErrPct:D

    return-wide v0
.end method

.method public getGrid()Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lorg/apache/lucene/spatial/prefix/PrefixTreeStrategy;->grid:Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;

    return-object v0
.end method

.method public makeDistanceValueSource(Lcom/spatial4j/core/shape/Point;)Lorg/apache/lucene/queries/function/ValueSource;
    .locals 6
    .param p1, "queryPoint"    # Lcom/spatial4j/core/shape/Point;

    .prologue
    .line 187
    iget-object v3, p0, Lorg/apache/lucene/spatial/prefix/PrefixTreeStrategy;->provider:Ljava/util/Map;

    invoke-virtual {p0}, Lorg/apache/lucene/spatial/prefix/PrefixTreeStrategy;->getFieldName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/spatial/prefix/PointPrefixTreeFieldCacheProvider;

    .line 188
    .local v1, "p":Lorg/apache/lucene/spatial/prefix/PointPrefixTreeFieldCacheProvider;
    if-nez v1, :cond_1

    .line 189
    monitor-enter p0

    .line 190
    :try_start_0
    iget-object v3, p0, Lorg/apache/lucene/spatial/prefix/PrefixTreeStrategy;->provider:Ljava/util/Map;

    invoke-virtual {p0}, Lorg/apache/lucene/spatial/prefix/PrefixTreeStrategy;->getFieldName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lorg/apache/lucene/spatial/prefix/PointPrefixTreeFieldCacheProvider;

    move-object v1, v0

    .line 191
    if-nez v1, :cond_0

    .line 192
    new-instance v2, Lorg/apache/lucene/spatial/prefix/PointPrefixTreeFieldCacheProvider;

    iget-object v3, p0, Lorg/apache/lucene/spatial/prefix/PrefixTreeStrategy;->grid:Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;

    invoke-virtual {p0}, Lorg/apache/lucene/spatial/prefix/PrefixTreeStrategy;->getFieldName()Ljava/lang/String;

    move-result-object v4

    iget v5, p0, Lorg/apache/lucene/spatial/prefix/PrefixTreeStrategy;->defaultFieldValuesArrayLen:I

    invoke-direct {v2, v3, v4, v5}, Lorg/apache/lucene/spatial/prefix/PointPrefixTreeFieldCacheProvider;-><init>(Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 193
    .end local v1    # "p":Lorg/apache/lucene/spatial/prefix/PointPrefixTreeFieldCacheProvider;
    .local v2, "p":Lorg/apache/lucene/spatial/prefix/PointPrefixTreeFieldCacheProvider;
    :try_start_1
    iget-object v3, p0, Lorg/apache/lucene/spatial/prefix/PrefixTreeStrategy;->provider:Ljava/util/Map;

    invoke-virtual {p0}, Lorg/apache/lucene/spatial/prefix/PrefixTreeStrategy;->getFieldName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object v1, v2

    .line 189
    .end local v2    # "p":Lorg/apache/lucene/spatial/prefix/PointPrefixTreeFieldCacheProvider;
    .restart local v1    # "p":Lorg/apache/lucene/spatial/prefix/PointPrefixTreeFieldCacheProvider;
    :cond_0
    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 198
    :cond_1
    new-instance v3, Lorg/apache/lucene/spatial/util/ShapeFieldCacheDistanceValueSource;

    iget-object v4, p0, Lorg/apache/lucene/spatial/prefix/PrefixTreeStrategy;->ctx:Lcom/spatial4j/core/context/SpatialContext;

    invoke-direct {v3, v4, v1, p1}, Lorg/apache/lucene/spatial/util/ShapeFieldCacheDistanceValueSource;-><init>(Lcom/spatial4j/core/context/SpatialContext;Lorg/apache/lucene/spatial/util/ShapeFieldCacheProvider;Lcom/spatial4j/core/shape/Point;)V

    return-object v3

    .line 189
    :catchall_0
    move-exception v3

    :goto_0
    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v3

    .end local v1    # "p":Lorg/apache/lucene/spatial/prefix/PointPrefixTreeFieldCacheProvider;
    .restart local v2    # "p":Lorg/apache/lucene/spatial/prefix/PointPrefixTreeFieldCacheProvider;
    :catchall_1
    move-exception v3

    move-object v1, v2

    .end local v2    # "p":Lorg/apache/lucene/spatial/prefix/PointPrefixTreeFieldCacheProvider;
    .restart local v1    # "p":Lorg/apache/lucene/spatial/prefix/PointPrefixTreeFieldCacheProvider;
    goto :goto_0
.end method

.method public setDefaultFieldValuesArrayLen(I)V
    .locals 0
    .param p1, "defaultFieldValuesArrayLen"    # I

    .prologue
    .line 99
    iput p1, p0, Lorg/apache/lucene/spatial/prefix/PrefixTreeStrategy;->defaultFieldValuesArrayLen:I

    .line 100
    return-void
.end method

.method public setDistErrPct(D)V
    .locals 1
    .param p1, "distErrPct"    # D

    .prologue
    .line 118
    iput-wide p1, p0, Lorg/apache/lucene/spatial/prefix/PrefixTreeStrategy;->distErrPct:D

    .line 119
    return-void
.end method
