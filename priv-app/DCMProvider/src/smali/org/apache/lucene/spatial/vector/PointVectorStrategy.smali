.class public Lorg/apache/lucene/spatial/vector/PointVectorStrategy;
.super Lorg/apache/lucene/spatial/SpatialStrategy;
.source "PointVectorStrategy.java"


# static fields
.field public static final SUFFIX_X:Ljava/lang/String; = "__x"

.field public static final SUFFIX_Y:Ljava/lang/String; = "__y"


# instance fields
.field private final fieldNameX:Ljava/lang/String;

.field private final fieldNameY:Ljava/lang/String;

.field public precisionStep:I


# direct methods
.method public constructor <init>(Lcom/spatial4j/core/context/SpatialContext;Ljava/lang/String;)V
    .locals 2
    .param p1, "ctx"    # Lcom/spatial4j/core/context/SpatialContext;
    .param p2, "fieldNamePrefix"    # Ljava/lang/String;

    .prologue
    .line 86
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/spatial/SpatialStrategy;-><init>(Lcom/spatial4j/core/context/SpatialContext;Ljava/lang/String;)V

    .line 83
    const/16 v0, 0x8

    iput v0, p0, Lorg/apache/lucene/spatial/vector/PointVectorStrategy;->precisionStep:I

    .line 87
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "__x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/spatial/vector/PointVectorStrategy;->fieldNameX:Ljava/lang/String;

    .line 88
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "__y"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/spatial/vector/PointVectorStrategy;->fieldNameY:Ljava/lang/String;

    .line 89
    return-void
.end method

.method private makeDisjoint(Lcom/spatial4j/core/shape/Rectangle;)Lorg/apache/lucene/search/Query;
    .locals 8
    .param p1, "bbox"    # Lcom/spatial4j/core/shape/Rectangle;

    .prologue
    .line 257
    invoke-interface {p1}, Lcom/spatial4j/core/shape/Rectangle;->getCrossesDateLine()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 258
    new-instance v3, Ljava/lang/UnsupportedOperationException;

    const-string v4, "makeDisjoint doesn\'t handle dateline cross"

    invoke-direct {v3, v4}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 259
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/spatial/vector/PointVectorStrategy;->fieldNameX:Ljava/lang/String;

    invoke-interface {p1}, Lcom/spatial4j/core/shape/Rectangle;->getMinX()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-interface {p1}, Lcom/spatial4j/core/shape/Rectangle;->getMaxX()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-direct {p0, v3, v4, v5}, Lorg/apache/lucene/spatial/vector/PointVectorStrategy;->rangeQuery(Ljava/lang/String;Ljava/lang/Double;Ljava/lang/Double;)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v1

    .line 260
    .local v1, "qX":Lorg/apache/lucene/search/Query;
    iget-object v3, p0, Lorg/apache/lucene/spatial/vector/PointVectorStrategy;->fieldNameY:Ljava/lang/String;

    invoke-interface {p1}, Lcom/spatial4j/core/shape/Rectangle;->getMinY()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-interface {p1}, Lcom/spatial4j/core/shape/Rectangle;->getMaxY()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-direct {p0, v3, v4, v5}, Lorg/apache/lucene/spatial/vector/PointVectorStrategy;->rangeQuery(Ljava/lang/String;Ljava/lang/Double;Ljava/lang/Double;)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v2

    .line 262
    .local v2, "qY":Lorg/apache/lucene/search/Query;
    new-instance v0, Lorg/apache/lucene/search/BooleanQuery;

    invoke-direct {v0}, Lorg/apache/lucene/search/BooleanQuery;-><init>()V

    .line 263
    .local v0, "bq":Lorg/apache/lucene/search/BooleanQuery;
    sget-object v3, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST_NOT:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v0, v1, v3}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 264
    sget-object v3, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST_NOT:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 265
    return-object v0
.end method

.method private makeWithin(Lcom/spatial4j/core/shape/Rectangle;)Lorg/apache/lucene/search/Query;
    .locals 7
    .param p1, "bbox"    # Lcom/spatial4j/core/shape/Rectangle;

    .prologue
    const/4 v6, 0x0

    .line 229
    new-instance v1, Lorg/apache/lucene/search/BooleanQuery;

    invoke-direct {v1}, Lorg/apache/lucene/search/BooleanQuery;-><init>()V

    .line 230
    .local v1, "bq":Lorg/apache/lucene/search/BooleanQuery;
    sget-object v0, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    .line 231
    .local v0, "MUST":Lorg/apache/lucene/search/BooleanClause$Occur;
    invoke-interface {p1}, Lcom/spatial4j/core/shape/Rectangle;->getCrossesDateLine()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 233
    iget-object v2, p0, Lorg/apache/lucene/spatial/vector/PointVectorStrategy;->fieldNameX:Ljava/lang/String;

    invoke-interface {p1}, Lcom/spatial4j/core/shape/Rectangle;->getMaxX()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-direct {p0, v2, v6, v3}, Lorg/apache/lucene/spatial/vector/PointVectorStrategy;->rangeQuery(Ljava/lang/String;Ljava/lang/Double;Ljava/lang/Double;)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v2

    sget-object v3, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 234
    iget-object v2, p0, Lorg/apache/lucene/spatial/vector/PointVectorStrategy;->fieldNameX:Ljava/lang/String;

    invoke-interface {p1}, Lcom/spatial4j/core/shape/Rectangle;->getMinX()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-direct {p0, v2, v3, v6}, Lorg/apache/lucene/spatial/vector/PointVectorStrategy;->rangeQuery(Ljava/lang/String;Ljava/lang/Double;Ljava/lang/Double;)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v2

    sget-object v3, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 235
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/search/BooleanQuery;->setMinimumNumberShouldMatch(I)V

    .line 239
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/spatial/vector/PointVectorStrategy;->fieldNameY:Ljava/lang/String;

    invoke-interface {p1}, Lcom/spatial4j/core/shape/Rectangle;->getMinY()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-interface {p1}, Lcom/spatial4j/core/shape/Rectangle;->getMaxY()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-direct {p0, v2, v3, v4}, Lorg/apache/lucene/spatial/vector/PointVectorStrategy;->rangeQuery(Ljava/lang/String;Ljava/lang/Double;Ljava/lang/Double;)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 240
    return-object v1

    .line 237
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/spatial/vector/PointVectorStrategy;->fieldNameX:Ljava/lang/String;

    invoke-interface {p1}, Lcom/spatial4j/core/shape/Rectangle;->getMinX()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-interface {p1}, Lcom/spatial4j/core/shape/Rectangle;->getMaxX()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-direct {p0, v2, v3, v4}, Lorg/apache/lucene/spatial/vector/PointVectorStrategy;->rangeQuery(Ljava/lang/String;Ljava/lang/Double;Ljava/lang/Double;)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    goto :goto_0
.end method

.method private rangeQuery(Ljava/lang/String;Ljava/lang/Double;Ljava/lang/Double;)Lorg/apache/lucene/search/NumericRangeQuery;
    .locals 6
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "min"    # Ljava/lang/Double;
    .param p3, "max"    # Ljava/lang/Double;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Double;",
            "Ljava/lang/Double;",
            ")",
            "Lorg/apache/lucene/search/NumericRangeQuery",
            "<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 245
    .line 246
    iget v1, p0, Lorg/apache/lucene/spatial/vector/PointVectorStrategy;->precisionStep:I

    move-object v0, p1

    move-object v2, p2

    move-object v3, p3

    move v5, v4

    .line 244
    invoke-static/range {v0 .. v5}, Lorg/apache/lucene/search/NumericRangeQuery;->newDoubleRange(Ljava/lang/String;ILjava/lang/Double;Ljava/lang/Double;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public createIndexableFields(Lcom/spatial4j/core/shape/Point;)[Lorg/apache/lucene/document/Field;
    .locals 8
    .param p1, "point"    # Lcom/spatial4j/core/shape/Point;

    .prologue
    .line 114
    new-instance v0, Lorg/apache/lucene/document/FieldType;

    sget-object v2, Lorg/apache/lucene/document/DoubleField;->TYPE_STORED:Lorg/apache/lucene/document/FieldType;

    invoke-direct {v0, v2}, Lorg/apache/lucene/document/FieldType;-><init>(Lorg/apache/lucene/document/FieldType;)V

    .line 115
    .local v0, "doubleFieldType":Lorg/apache/lucene/document/FieldType;
    iget v2, p0, Lorg/apache/lucene/spatial/vector/PointVectorStrategy;->precisionStep:I

    invoke-virtual {v0, v2}, Lorg/apache/lucene/document/FieldType;->setNumericPrecisionStep(I)V

    .line 116
    const/4 v2, 0x2

    new-array v1, v2, [Lorg/apache/lucene/document/Field;

    .line 117
    .local v1, "f":[Lorg/apache/lucene/document/Field;
    const/4 v2, 0x0

    new-instance v3, Lorg/apache/lucene/document/DoubleField;

    iget-object v4, p0, Lorg/apache/lucene/spatial/vector/PointVectorStrategy;->fieldNameX:Ljava/lang/String;

    invoke-interface {p1}, Lcom/spatial4j/core/shape/Point;->getX()D

    move-result-wide v6

    invoke-direct {v3, v4, v6, v7, v0}, Lorg/apache/lucene/document/DoubleField;-><init>(Ljava/lang/String;DLorg/apache/lucene/document/FieldType;)V

    aput-object v3, v1, v2

    .line 118
    const/4 v2, 0x1

    new-instance v3, Lorg/apache/lucene/document/DoubleField;

    iget-object v4, p0, Lorg/apache/lucene/spatial/vector/PointVectorStrategy;->fieldNameY:Ljava/lang/String;

    invoke-interface {p1}, Lcom/spatial4j/core/shape/Point;->getY()D

    move-result-wide v6

    invoke-direct {v3, v4, v6, v7, v0}, Lorg/apache/lucene/document/DoubleField;-><init>(Ljava/lang/String;DLorg/apache/lucene/document/FieldType;)V

    aput-object v3, v1, v2

    .line 119
    return-object v1
.end method

.method public createIndexableFields(Lcom/spatial4j/core/shape/Shape;)[Lorg/apache/lucene/document/Field;
    .locals 3
    .param p1, "shape"    # Lcom/spatial4j/core/shape/Shape;

    .prologue
    .line 107
    instance-of v0, p1, Lcom/spatial4j/core/shape/Point;

    if-eqz v0, :cond_0

    .line 108
    check-cast p1, Lcom/spatial4j/core/shape/Point;

    .end local p1    # "shape":Lcom/spatial4j/core/shape/Shape;
    invoke-virtual {p0, p1}, Lorg/apache/lucene/spatial/vector/PointVectorStrategy;->createIndexableFields(Lcom/spatial4j/core/shape/Point;)[Lorg/apache/lucene/document/Field;

    move-result-object v0

    return-object v0

    .line 109
    .restart local p1    # "shape":Lcom/spatial4j/core/shape/Shape;
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can only index Point, not "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method getFieldNameX()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lorg/apache/lucene/spatial/vector/PointVectorStrategy;->fieldNameX:Ljava/lang/String;

    return-object v0
.end method

.method getFieldNameY()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lorg/apache/lucene/spatial/vector/PointVectorStrategy;->fieldNameY:Ljava/lang/String;

    return-object v0
.end method

.method public makeDistanceValueSource(Lcom/spatial4j/core/shape/Point;)Lorg/apache/lucene/queries/function/ValueSource;
    .locals 1
    .param p1, "queryPoint"    # Lcom/spatial4j/core/shape/Point;

    .prologue
    .line 124
    new-instance v0, Lorg/apache/lucene/spatial/vector/DistanceValueSource;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/spatial/vector/DistanceValueSource;-><init>(Lorg/apache/lucene/spatial/vector/PointVectorStrategy;Lcom/spatial4j/core/shape/Point;)V

    return-object v0
.end method

.method public makeFilter(Lorg/apache/lucene/spatial/query/SpatialArgs;)Lorg/apache/lucene/search/Filter;
    .locals 3
    .param p1, "args"    # Lorg/apache/lucene/spatial/query/SpatialArgs;

    .prologue
    .line 130
    invoke-virtual {p0, p1}, Lorg/apache/lucene/spatial/vector/PointVectorStrategy;->makeQuery(Lorg/apache/lucene/spatial/query/SpatialArgs;)Lorg/apache/lucene/search/ConstantScoreQuery;

    move-result-object v0

    .line 131
    .local v0, "csq":Lorg/apache/lucene/search/ConstantScoreQuery;
    invoke-virtual {v0}, Lorg/apache/lucene/search/ConstantScoreQuery;->getFilter()Lorg/apache/lucene/search/Filter;

    move-result-object v1

    .line 132
    .local v1, "filter":Lorg/apache/lucene/search/Filter;
    if-eqz v1, :cond_0

    .line 135
    .end local v1    # "filter":Lorg/apache/lucene/search/Filter;
    :goto_0
    return-object v1

    .restart local v1    # "filter":Lorg/apache/lucene/search/Filter;
    :cond_0
    new-instance v1, Lorg/apache/lucene/search/QueryWrapperFilter;

    .end local v1    # "filter":Lorg/apache/lucene/search/Filter;
    invoke-virtual {v0}, Lorg/apache/lucene/search/ConstantScoreQuery;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/lucene/search/QueryWrapperFilter;-><init>(Lorg/apache/lucene/search/Query;)V

    goto :goto_0
.end method

.method public makeQuery(Lorg/apache/lucene/spatial/query/SpatialArgs;)Lorg/apache/lucene/search/ConstantScoreQuery;
    .locals 10
    .param p1, "args"    # Lorg/apache/lucene/spatial/query/SpatialArgs;

    .prologue
    .line 140
    invoke-virtual {p1}, Lorg/apache/lucene/spatial/query/SpatialArgs;->getOperation()Lorg/apache/lucene/spatial/query/SpatialOperation;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Lorg/apache/lucene/spatial/query/SpatialOperation;

    const/4 v4, 0x0

    .line 141
    sget-object v5, Lorg/apache/lucene/spatial/query/SpatialOperation;->Intersects:Lorg/apache/lucene/spatial/query/SpatialOperation;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    .line 142
    sget-object v5, Lorg/apache/lucene/spatial/query/SpatialOperation;->IsWithin:Lorg/apache/lucene/spatial/query/SpatialOperation;

    aput-object v5, v3, v4

    .line 140
    invoke-static {v2, v3}, Lorg/apache/lucene/spatial/query/SpatialOperation;->is(Lorg/apache/lucene/spatial/query/SpatialOperation;[Lorg/apache/lucene/spatial/query/SpatialOperation;)Z

    move-result v2

    .line 142
    if-nez v2, :cond_0

    .line 143
    new-instance v2, Lorg/apache/lucene/spatial/query/UnsupportedSpatialOperation;

    invoke-virtual {p1}, Lorg/apache/lucene/spatial/query/SpatialArgs;->getOperation()Lorg/apache/lucene/spatial/query/SpatialOperation;

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/apache/lucene/spatial/query/UnsupportedSpatialOperation;-><init>(Lorg/apache/lucene/spatial/query/SpatialOperation;)V

    throw v2

    .line 144
    :cond_0
    invoke-virtual {p1}, Lorg/apache/lucene/spatial/query/SpatialArgs;->getShape()Lcom/spatial4j/core/shape/Shape;

    move-result-object v9

    .line 145
    .local v9, "shape":Lcom/spatial4j/core/shape/Shape;
    instance-of v2, v9, Lcom/spatial4j/core/shape/Rectangle;

    if-eqz v2, :cond_1

    move-object v0, v9

    .line 146
    check-cast v0, Lcom/spatial4j/core/shape/Rectangle;

    .line 147
    .local v0, "bbox":Lcom/spatial4j/core/shape/Rectangle;
    new-instance v2, Lorg/apache/lucene/search/ConstantScoreQuery;

    invoke-direct {p0, v0}, Lorg/apache/lucene/spatial/vector/PointVectorStrategy;->makeWithin(Lcom/spatial4j/core/shape/Rectangle;)Lorg/apache/lucene/search/Query;

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/apache/lucene/search/ConstantScoreQuery;-><init>(Lorg/apache/lucene/search/Query;)V

    .line 156
    :goto_0
    return-object v2

    .line 148
    .end local v0    # "bbox":Lcom/spatial4j/core/shape/Rectangle;
    :cond_1
    instance-of v2, v9, Lcom/spatial4j/core/shape/Circle;

    if-eqz v2, :cond_2

    move-object v8, v9

    .line 149
    check-cast v8, Lcom/spatial4j/core/shape/Circle;

    .line 150
    .local v8, "circle":Lcom/spatial4j/core/shape/Circle;
    invoke-interface {v8}, Lcom/spatial4j/core/shape/Circle;->getBoundingBox()Lcom/spatial4j/core/shape/Rectangle;

    move-result-object v0

    .line 151
    .restart local v0    # "bbox":Lcom/spatial4j/core/shape/Rectangle;
    new-instance v1, Lorg/apache/lucene/spatial/util/ValueSourceFilter;

    .line 152
    new-instance v2, Lorg/apache/lucene/search/QueryWrapperFilter;

    invoke-direct {p0, v0}, Lorg/apache/lucene/spatial/vector/PointVectorStrategy;->makeWithin(Lcom/spatial4j/core/shape/Rectangle;)Lorg/apache/lucene/search/Query;

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/apache/lucene/search/QueryWrapperFilter;-><init>(Lorg/apache/lucene/search/Query;)V

    .line 153
    invoke-interface {v8}, Lcom/spatial4j/core/shape/Circle;->getCenter()Lcom/spatial4j/core/shape/Point;

    move-result-object v3

    invoke-virtual {p0, v3}, Lorg/apache/lucene/spatial/vector/PointVectorStrategy;->makeDistanceValueSource(Lcom/spatial4j/core/shape/Point;)Lorg/apache/lucene/queries/function/ValueSource;

    move-result-object v3

    .line 154
    const-wide/16 v4, 0x0

    .line 155
    invoke-interface {v8}, Lcom/spatial4j/core/shape/Circle;->getRadius()D

    move-result-wide v6

    .line 151
    invoke-direct/range {v1 .. v7}, Lorg/apache/lucene/spatial/util/ValueSourceFilter;-><init>(Lorg/apache/lucene/search/Filter;Lorg/apache/lucene/queries/function/ValueSource;DD)V

    .line 156
    .local v1, "vsf":Lorg/apache/lucene/spatial/util/ValueSourceFilter;
    new-instance v2, Lorg/apache/lucene/search/ConstantScoreQuery;

    invoke-direct {v2, v1}, Lorg/apache/lucene/search/ConstantScoreQuery;-><init>(Lorg/apache/lucene/search/Filter;)V

    goto :goto_0

    .line 158
    .end local v0    # "bbox":Lcom/spatial4j/core/shape/Rectangle;
    .end local v1    # "vsf":Lorg/apache/lucene/spatial/util/ValueSourceFilter;
    .end local v8    # "circle":Lcom/spatial4j/core/shape/Circle;
    :cond_2
    new-instance v2, Ljava/lang/UnsupportedOperationException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Only Rectangles and Circles are currently supported, found ["

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 159
    invoke-virtual {v9}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 158
    invoke-direct {v2, v3}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public bridge synthetic makeQuery(Lorg/apache/lucene/spatial/query/SpatialArgs;)Lorg/apache/lucene/search/Query;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/spatial/vector/PointVectorStrategy;->makeQuery(Lorg/apache/lucene/spatial/query/SpatialArgs;)Lorg/apache/lucene/search/ConstantScoreQuery;

    move-result-object v0

    return-object v0
.end method

.method public makeQueryDistanceScore(Lorg/apache/lucene/spatial/query/SpatialArgs;)Lorg/apache/lucene/search/Query;
    .locals 15
    .param p1, "args"    # Lorg/apache/lucene/spatial/query/SpatialArgs;

    .prologue
    .line 166
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/spatial/query/SpatialArgs;->getShape()Lcom/spatial4j/core/shape/Shape;

    move-result-object v11

    .line 167
    .local v11, "shape":Lcom/spatial4j/core/shape/Shape;
    instance-of v2, v11, Lcom/spatial4j/core/shape/Rectangle;

    if-nez v2, :cond_0

    instance-of v2, v11, Lcom/spatial4j/core/shape/Circle;

    if-nez v2, :cond_0

    .line 168
    new-instance v2, Ljava/lang/UnsupportedOperationException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Only Rectangles and Circles are currently supported, found ["

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 169
    invoke-virtual {v11}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 168
    invoke-direct {v2, v4}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 172
    :cond_0
    invoke-interface {v11}, Lcom/spatial4j/core/shape/Shape;->getBoundingBox()Lcom/spatial4j/core/shape/Rectangle;

    move-result-object v0

    .line 174
    .local v0, "bbox":Lcom/spatial4j/core/shape/Rectangle;
    invoke-interface {v0}, Lcom/spatial4j/core/shape/Rectangle;->getCrossesDateLine()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 175
    new-instance v2, Ljava/lang/UnsupportedOperationException;

    const-string v4, "Crossing dateline not yet supported"

    invoke-direct {v2, v4}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 178
    :cond_1
    const/4 v3, 0x0

    .line 180
    .local v3, "valueSource":Lorg/apache/lucene/queries/function/ValueSource;
    const/4 v12, 0x0

    .line 181
    .local v12, "spatial":Lorg/apache/lucene/search/Query;
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/spatial/query/SpatialArgs;->getOperation()Lorg/apache/lucene/spatial/query/SpatialOperation;

    move-result-object v10

    .line 183
    .local v10, "op":Lorg/apache/lucene/spatial/query/SpatialOperation;
    const/4 v2, 0x2

    new-array v2, v2, [Lorg/apache/lucene/spatial/query/SpatialOperation;

    const/4 v4, 0x0

    .line 184
    sget-object v5, Lorg/apache/lucene/spatial/query/SpatialOperation;->BBoxWithin:Lorg/apache/lucene/spatial/query/SpatialOperation;

    aput-object v5, v2, v4

    const/4 v4, 0x1

    .line 185
    sget-object v5, Lorg/apache/lucene/spatial/query/SpatialOperation;->BBoxIntersects:Lorg/apache/lucene/spatial/query/SpatialOperation;

    aput-object v5, v2, v4

    .line 183
    invoke-static {v10, v2}, Lorg/apache/lucene/spatial/query/SpatialOperation;->is(Lorg/apache/lucene/spatial/query/SpatialOperation;[Lorg/apache/lucene/spatial/query/SpatialOperation;)Z

    move-result v2

    .line 185
    if-eqz v2, :cond_2

    .line 186
    invoke-direct {p0, v0}, Lorg/apache/lucene/spatial/vector/PointVectorStrategy;->makeWithin(Lcom/spatial4j/core/shape/Rectangle;)Lorg/apache/lucene/search/Query;

    move-result-object v12

    move-object v14, v3

    .line 208
    .end local v3    # "valueSource":Lorg/apache/lucene/queries/function/ValueSource;
    .local v14, "valueSource":Lorg/apache/lucene/queries/function/ValueSource;
    :goto_0
    if-nez v12, :cond_4

    .line 209
    new-instance v2, Lorg/apache/lucene/spatial/query/UnsupportedSpatialOperation;

    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/spatial/query/SpatialArgs;->getOperation()Lorg/apache/lucene/spatial/query/SpatialOperation;

    move-result-object v4

    invoke-direct {v2, v4}, Lorg/apache/lucene/spatial/query/UnsupportedSpatialOperation;-><init>(Lorg/apache/lucene/spatial/query/SpatialOperation;)V

    throw v2

    .line 188
    .end local v14    # "valueSource":Lorg/apache/lucene/queries/function/ValueSource;
    .restart local v3    # "valueSource":Lorg/apache/lucene/queries/function/ValueSource;
    :cond_2
    const/4 v2, 0x2

    new-array v2, v2, [Lorg/apache/lucene/spatial/query/SpatialOperation;

    const/4 v4, 0x0

    .line 189
    sget-object v5, Lorg/apache/lucene/spatial/query/SpatialOperation;->Intersects:Lorg/apache/lucene/spatial/query/SpatialOperation;

    aput-object v5, v2, v4

    const/4 v4, 0x1

    .line 190
    sget-object v5, Lorg/apache/lucene/spatial/query/SpatialOperation;->IsWithin:Lorg/apache/lucene/spatial/query/SpatialOperation;

    aput-object v5, v2, v4

    .line 188
    invoke-static {v10, v2}, Lorg/apache/lucene/spatial/query/SpatialOperation;->is(Lorg/apache/lucene/spatial/query/SpatialOperation;[Lorg/apache/lucene/spatial/query/SpatialOperation;)Z

    move-result v2

    .line 190
    if-eqz v2, :cond_3

    .line 191
    invoke-direct {p0, v0}, Lorg/apache/lucene/spatial/vector/PointVectorStrategy;->makeWithin(Lcom/spatial4j/core/shape/Rectangle;)Lorg/apache/lucene/search/Query;

    move-result-object v12

    .line 192
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/spatial/query/SpatialArgs;->getShape()Lcom/spatial4j/core/shape/Shape;

    move-result-object v2

    instance-of v2, v2, Lcom/spatial4j/core/shape/Circle;

    if-eqz v2, :cond_6

    .line 193
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/spatial/query/SpatialArgs;->getShape()Lcom/spatial4j/core/shape/Shape;

    move-result-object v9

    check-cast v9, Lcom/spatial4j/core/shape/Circle;

    .line 196
    .local v9, "circle":Lcom/spatial4j/core/shape/Circle;
    invoke-interface {v11}, Lcom/spatial4j/core/shape/Shape;->getCenter()Lcom/spatial4j/core/shape/Point;

    move-result-object v2

    invoke-virtual {p0, v2}, Lorg/apache/lucene/spatial/vector/PointVectorStrategy;->makeDistanceValueSource(Lcom/spatial4j/core/shape/Point;)Lorg/apache/lucene/queries/function/ValueSource;

    move-result-object v3

    .line 198
    new-instance v1, Lorg/apache/lucene/spatial/util/ValueSourceFilter;

    .line 199
    new-instance v2, Lorg/apache/lucene/search/QueryWrapperFilter;

    invoke-direct {v2, v12}, Lorg/apache/lucene/search/QueryWrapperFilter;-><init>(Lorg/apache/lucene/search/Query;)V

    const-wide/16 v4, 0x0

    invoke-interface {v9}, Lcom/spatial4j/core/shape/Circle;->getRadius()D

    move-result-wide v6

    .line 198
    invoke-direct/range {v1 .. v7}, Lorg/apache/lucene/spatial/util/ValueSourceFilter;-><init>(Lorg/apache/lucene/search/Filter;Lorg/apache/lucene/queries/function/ValueSource;DD)V

    .line 201
    .local v1, "vsf":Lorg/apache/lucene/spatial/util/ValueSourceFilter;
    new-instance v12, Lorg/apache/lucene/search/FilteredQuery;

    .end local v12    # "spatial":Lorg/apache/lucene/search/Query;
    new-instance v2, Lorg/apache/lucene/search/MatchAllDocsQuery;

    invoke-direct {v2}, Lorg/apache/lucene/search/MatchAllDocsQuery;-><init>()V

    invoke-direct {v12, v2, v1}, Lorg/apache/lucene/search/FilteredQuery;-><init>(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;)V

    .restart local v12    # "spatial":Lorg/apache/lucene/search/Query;
    move-object v14, v3

    .line 203
    .end local v3    # "valueSource":Lorg/apache/lucene/queries/function/ValueSource;
    .restart local v14    # "valueSource":Lorg/apache/lucene/queries/function/ValueSource;
    goto :goto_0

    .line 204
    .end local v1    # "vsf":Lorg/apache/lucene/spatial/util/ValueSourceFilter;
    .end local v9    # "circle":Lcom/spatial4j/core/shape/Circle;
    .end local v14    # "valueSource":Lorg/apache/lucene/queries/function/ValueSource;
    .restart local v3    # "valueSource":Lorg/apache/lucene/queries/function/ValueSource;
    :cond_3
    sget-object v2, Lorg/apache/lucene/spatial/query/SpatialOperation;->IsDisjointTo:Lorg/apache/lucene/spatial/query/SpatialOperation;

    if-ne v10, v2, :cond_6

    .line 205
    invoke-direct {p0, v0}, Lorg/apache/lucene/spatial/vector/PointVectorStrategy;->makeDisjoint(Lcom/spatial4j/core/shape/Rectangle;)Lorg/apache/lucene/search/Query;

    move-result-object v12

    move-object v14, v3

    .end local v3    # "valueSource":Lorg/apache/lucene/queries/function/ValueSource;
    .restart local v14    # "valueSource":Lorg/apache/lucene/queries/function/ValueSource;
    goto :goto_0

    .line 212
    :cond_4
    if-eqz v14, :cond_5

    .line 213
    new-instance v3, Lorg/apache/lucene/spatial/util/CachingDoubleValueSource;

    invoke-direct {v3, v14}, Lorg/apache/lucene/spatial/util/CachingDoubleValueSource;-><init>(Lorg/apache/lucene/queries/function/ValueSource;)V

    .line 218
    .end local v14    # "valueSource":Lorg/apache/lucene/queries/function/ValueSource;
    .restart local v3    # "valueSource":Lorg/apache/lucene/queries/function/ValueSource;
    :goto_1
    new-instance v13, Lorg/apache/lucene/queries/function/FunctionQuery;

    invoke-direct {v13, v3}, Lorg/apache/lucene/queries/function/FunctionQuery;-><init>(Lorg/apache/lucene/queries/function/ValueSource;)V

    .line 219
    .local v13, "spatialRankingQuery":Lorg/apache/lucene/search/Query;
    new-instance v8, Lorg/apache/lucene/search/BooleanQuery;

    invoke-direct {v8}, Lorg/apache/lucene/search/BooleanQuery;-><init>()V

    .line 220
    .local v8, "bq":Lorg/apache/lucene/search/BooleanQuery;
    sget-object v2, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v8, v12, v2}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 221
    sget-object v2, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v8, v13, v2}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 222
    return-object v8

    .line 216
    .end local v3    # "valueSource":Lorg/apache/lucene/queries/function/ValueSource;
    .end local v8    # "bq":Lorg/apache/lucene/search/BooleanQuery;
    .end local v13    # "spatialRankingQuery":Lorg/apache/lucene/search/Query;
    .restart local v14    # "valueSource":Lorg/apache/lucene/queries/function/ValueSource;
    :cond_5
    invoke-interface {v11}, Lcom/spatial4j/core/shape/Shape;->getCenter()Lcom/spatial4j/core/shape/Point;

    move-result-object v2

    invoke-virtual {p0, v2}, Lorg/apache/lucene/spatial/vector/PointVectorStrategy;->makeDistanceValueSource(Lcom/spatial4j/core/shape/Point;)Lorg/apache/lucene/queries/function/ValueSource;

    move-result-object v3

    .end local v14    # "valueSource":Lorg/apache/lucene/queries/function/ValueSource;
    .restart local v3    # "valueSource":Lorg/apache/lucene/queries/function/ValueSource;
    goto :goto_1

    :cond_6
    move-object v14, v3

    .end local v3    # "valueSource":Lorg/apache/lucene/queries/function/ValueSource;
    .restart local v14    # "valueSource":Lorg/apache/lucene/queries/function/ValueSource;
    goto/16 :goto_0
.end method

.method public setPrecisionStep(I)V
    .locals 2
    .param p1, "p"    # I

    .prologue
    .line 92
    iput p1, p0, Lorg/apache/lucene/spatial/vector/PointVectorStrategy;->precisionStep:I

    .line 93
    iget v0, p0, Lorg/apache/lucene/spatial/vector/PointVectorStrategy;->precisionStep:I

    if-lez v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/spatial/vector/PointVectorStrategy;->precisionStep:I

    const/16 v1, 0x40

    if-lt v0, v1, :cond_1

    .line 94
    :cond_0
    const v0, 0x7fffffff

    iput v0, p0, Lorg/apache/lucene/spatial/vector/PointVectorStrategy;->precisionStep:I

    .line 95
    :cond_1
    return-void
.end method
