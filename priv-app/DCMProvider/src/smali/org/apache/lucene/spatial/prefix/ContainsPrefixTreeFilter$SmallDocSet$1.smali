.class Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet$1;
.super Lorg/apache/lucene/search/DocIdSetIterator;
.source "ContainsPrefixTreeFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;->iterator()Lorg/apache/lucene/search/DocIdSetIterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field idx:I

.field final synthetic this$1:Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;

.field private final synthetic val$docs:[I

.field private final synthetic val$size:I


# direct methods
.method constructor <init>(Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;I[I)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet$1;->this$1:Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;

    iput p2, p0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet$1;->val$size:I

    iput-object p3, p0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet$1;->val$docs:[I

    .line 225
    invoke-direct {p0}, Lorg/apache/lucene/search/DocIdSetIterator;-><init>()V

    .line 226
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet$1;->idx:I

    return-void
.end method


# virtual methods
.method public advance(I)I
    .locals 1
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 246
    invoke-virtual {p0, p1}, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet$1;->slowAdvance(I)I

    move-result v0

    return v0
.end method

.method public cost()J
    .locals 2

    .prologue
    .line 251
    iget v0, p0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet$1;->val$size:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public docID()I
    .locals 2

    .prologue
    .line 229
    iget v0, p0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet$1;->idx:I

    if-ltz v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet$1;->idx:I

    iget v1, p0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet$1;->val$size:I

    if-ge v0, v1, :cond_0

    .line 230
    iget-object v0, p0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet$1;->val$docs:[I

    iget v1, p0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet$1;->idx:I

    aget v0, v0, v1

    .line 232
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public nextDoc()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 237
    iget v0, p0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet$1;->idx:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet$1;->idx:I

    iget v1, p0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet$1;->val$size:I

    if-ge v0, v1, :cond_0

    .line 238
    iget-object v0, p0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet$1;->val$docs:[I

    iget v1, p0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet$1;->idx:I

    aget v0, v0, v1

    .line 239
    :goto_0
    return v0

    :cond_0
    const v0, 0x7fffffff

    goto :goto_0
.end method
