.class public abstract Lorg/apache/lucene/spatial/util/ShapeFieldCacheProvider;
.super Ljava/lang/Object;
.source "ShapeFieldCacheProvider.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/spatial4j/core/shape/Shape;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field protected final defaultSize:I

.field private log:Ljava/util/logging/Logger;

.field protected final shapeField:Ljava/lang/String;

.field sidx:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Lorg/apache/lucene/index/IndexReader;",
            "Lorg/apache/lucene/spatial/util/ShapeFieldCache",
            "<TT;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .param p1, "shapeField"    # Ljava/lang/String;
    .param p2, "defaultSize"    # I

    .prologue
    .line 47
    .local p0, "this":Lorg/apache/lucene/spatial/util/ShapeFieldCacheProvider;, "Lorg/apache/lucene/spatial/util/ShapeFieldCacheProvider<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/spatial/util/ShapeFieldCacheProvider;->log:Ljava/util/logging/Logger;

    .line 42
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/spatial/util/ShapeFieldCacheProvider;->sidx:Ljava/util/WeakHashMap;

    .line 48
    iput-object p1, p0, Lorg/apache/lucene/spatial/util/ShapeFieldCacheProvider;->shapeField:Ljava/lang/String;

    .line 49
    iput p2, p0, Lorg/apache/lucene/spatial/util/ShapeFieldCacheProvider;->defaultSize:I

    .line 50
    return-void
.end method


# virtual methods
.method public declared-synchronized getCache(Lorg/apache/lucene/index/AtomicReader;)Lorg/apache/lucene/spatial/util/ShapeFieldCache;
    .locals 18
    .param p1, "reader"    # Lorg/apache/lucene/index/AtomicReader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/AtomicReader;",
            ")",
            "Lorg/apache/lucene/spatial/util/ShapeFieldCache",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55
    .local p0, "this":Lorg/apache/lucene/spatial/util/ShapeFieldCacheProvider;, "Lorg/apache/lucene/spatial/util/ShapeFieldCacheProvider<TT;>;"
    monitor-enter p0

    :try_start_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/spatial/util/ShapeFieldCacheProvider;->sidx:Ljava/util/WeakHashMap;

    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/spatial/util/ShapeFieldCache;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 56
    .local v5, "idx":Lorg/apache/lucene/spatial/util/ShapeFieldCache;, "Lorg/apache/lucene/spatial/util/ShapeFieldCache<TT;>;"
    if-eqz v5, :cond_0

    move-object v8, v5

    .line 87
    .end local v5    # "idx":Lorg/apache/lucene/spatial/util/ShapeFieldCache;, "Lorg/apache/lucene/spatial/util/ShapeFieldCache<TT;>;"
    .local v8, "idx":Ljava/lang/Object;, "Lorg/apache/lucene/spatial/util/ShapeFieldCache<TT;>;"
    :goto_0
    monitor-exit p0

    return-object v8

    .line 59
    .end local v8    # "idx":Ljava/lang/Object;, "Lorg/apache/lucene/spatial/util/ShapeFieldCache<TT;>;"
    .restart local v5    # "idx":Lorg/apache/lucene/spatial/util/ShapeFieldCache;, "Lorg/apache/lucene/spatial/util/ShapeFieldCache<TT;>;"
    :cond_0
    :try_start_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 61
    .local v10, "startTime":J
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/spatial/util/ShapeFieldCacheProvider;->log:Ljava/util/logging/Logger;

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "Building Cache ["

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "]"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/util/logging/Logger;->fine(Ljava/lang/String;)V

    .line 62
    new-instance v5, Lorg/apache/lucene/spatial/util/ShapeFieldCache;

    .end local v5    # "idx":Lorg/apache/lucene/spatial/util/ShapeFieldCache;, "Lorg/apache/lucene/spatial/util/ShapeFieldCache<TT;>;"
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v15

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/spatial/util/ShapeFieldCacheProvider;->defaultSize:I

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-direct {v5, v15, v0}, Lorg/apache/lucene/spatial/util/ShapeFieldCache;-><init>(II)V

    .line 63
    .restart local v5    # "idx":Lorg/apache/lucene/spatial/util/ShapeFieldCache;, "Lorg/apache/lucene/spatial/util/ShapeFieldCache<TT;>;"
    const/4 v2, 0x0

    .line 64
    .local v2, "count":I
    const/4 v4, 0x0

    .line 65
    .local v4, "docs":Lorg/apache/lucene/index/DocsEnum;
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/spatial/util/ShapeFieldCacheProvider;->shapeField:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Lorg/apache/lucene/index/AtomicReader;->terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v14

    .line 66
    .local v14, "terms":Lorg/apache/lucene/index/Terms;
    const/4 v12, 0x0

    .line 67
    .local v12, "te":Lorg/apache/lucene/index/TermsEnum;
    if-eqz v14, :cond_1

    .line 68
    invoke-virtual {v14, v12}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v12

    .line 69
    invoke-virtual {v12}, Lorg/apache/lucene/index/TermsEnum;->next()Lorg/apache/lucene/util/BytesRef;

    move-result-object v13

    .line 70
    .local v13, "term":Lorg/apache/lucene/util/BytesRef;
    :goto_1
    if-nez v13, :cond_2

    .line 84
    .end local v13    # "term":Lorg/apache/lucene/util/BytesRef;
    :cond_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/spatial/util/ShapeFieldCacheProvider;->sidx:Ljava/util/WeakHashMap;

    move-object/from16 v0, p1

    invoke-virtual {v15, v0, v5}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    sub-long v6, v16, v10

    .line 86
    .local v6, "elapsed":J
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/spatial/util/ShapeFieldCacheProvider;->log:Ljava/util/logging/Logger;

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "Cached: ["

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, " in "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "ms] "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/util/logging/Logger;->fine(Ljava/lang/String;)V

    move-object v8, v5

    .line 87
    .restart local v8    # "idx":Ljava/lang/Object;, "Lorg/apache/lucene/spatial/util/ShapeFieldCache<TT;>;"
    goto/16 :goto_0

    .line 71
    .end local v6    # "elapsed":J
    .end local v8    # "idx":Ljava/lang/Object;, "Lorg/apache/lucene/spatial/util/ShapeFieldCache<TT;>;"
    .restart local v13    # "term":Lorg/apache/lucene/util/BytesRef;
    :cond_2
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lorg/apache/lucene/spatial/util/ShapeFieldCacheProvider;->readShape(Lorg/apache/lucene/util/BytesRef;)Lcom/spatial4j/core/shape/Shape;

    move-result-object v9

    .line 72
    .local v9, "shape":Lcom/spatial4j/core/shape/Shape;, "TT;"
    if-eqz v9, :cond_3

    .line 73
    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v12, v15, v4, v0}, Lorg/apache/lucene/index/TermsEnum;->docs(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;I)Lorg/apache/lucene/index/DocsEnum;

    move-result-object v4

    .line 74
    invoke-virtual {v4}, Lorg/apache/lucene/index/DocsEnum;->nextDoc()I

    move-result v15

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 75
    .local v3, "docid":Ljava/lang/Integer;
    :goto_2
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v15

    const v16, 0x7fffffff

    move/from16 v0, v16

    if-ne v15, v0, :cond_4

    .line 81
    .end local v3    # "docid":Ljava/lang/Integer;
    :cond_3
    invoke-virtual {v12}, Lorg/apache/lucene/index/TermsEnum;->next()Lorg/apache/lucene/util/BytesRef;

    move-result-object v13

    goto :goto_1

    .line 76
    .restart local v3    # "docid":Ljava/lang/Integer;
    :cond_4
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v15

    invoke-virtual {v5, v15, v9}, Lorg/apache/lucene/spatial/util/ShapeFieldCache;->add(ILcom/spatial4j/core/shape/Shape;)V

    .line 77
    invoke-virtual {v4}, Lorg/apache/lucene/index/DocsEnum;->nextDoc()I

    move-result v15

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    .line 78
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 55
    .end local v2    # "count":I
    .end local v3    # "docid":Ljava/lang/Integer;
    .end local v4    # "docs":Lorg/apache/lucene/index/DocsEnum;
    .end local v5    # "idx":Lorg/apache/lucene/spatial/util/ShapeFieldCache;, "Lorg/apache/lucene/spatial/util/ShapeFieldCache<TT;>;"
    .end local v9    # "shape":Lcom/spatial4j/core/shape/Shape;, "TT;"
    .end local v10    # "startTime":J
    .end local v12    # "te":Lorg/apache/lucene/index/TermsEnum;
    .end local v13    # "term":Lorg/apache/lucene/util/BytesRef;
    .end local v14    # "terms":Lorg/apache/lucene/index/Terms;
    :catchall_0
    move-exception v15

    monitor-exit p0

    throw v15
.end method

.method protected abstract readShape(Lorg/apache/lucene/util/BytesRef;)Lcom/spatial4j/core/shape/Shape;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/BytesRef;",
            ")TT;"
        }
    .end annotation
.end method
