.class public abstract Lorg/apache/lucene/spatial/SpatialStrategy;
.super Ljava/lang/Object;
.source "SpatialStrategy.java"


# instance fields
.field protected final ctx:Lcom/spatial4j/core/context/SpatialContext;

.field private final fieldName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/spatial4j/core/context/SpatialContext;Ljava/lang/String;)V
    .locals 2
    .param p1, "ctx"    # Lcom/spatial4j/core/context/SpatialContext;
    .param p2, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    if-nez p1, :cond_0

    .line 68
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "ctx is required"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 69
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/spatial/SpatialStrategy;->ctx:Lcom/spatial4j/core/context/SpatialContext;

    .line 70
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 71
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "fieldName is required"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 72
    :cond_2
    iput-object p2, p0, Lorg/apache/lucene/spatial/SpatialStrategy;->fieldName:Ljava/lang/String;

    .line 73
    return-void
.end method


# virtual methods
.method public abstract createIndexableFields(Lcom/spatial4j/core/shape/Shape;)[Lorg/apache/lucene/document/Field;
.end method

.method public getFieldName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lorg/apache/lucene/spatial/SpatialStrategy;->fieldName:Ljava/lang/String;

    return-object v0
.end method

.method public getSpatialContext()Lcom/spatial4j/core/context/SpatialContext;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lorg/apache/lucene/spatial/SpatialStrategy;->ctx:Lcom/spatial4j/core/context/SpatialContext;

    return-object v0
.end method

.method public abstract makeDistanceValueSource(Lcom/spatial4j/core/shape/Point;)Lorg/apache/lucene/queries/function/ValueSource;
.end method

.method public abstract makeFilter(Lorg/apache/lucene/spatial/query/SpatialArgs;)Lorg/apache/lucene/search/Filter;
.end method

.method public makeQuery(Lorg/apache/lucene/spatial/query/SpatialArgs;)Lorg/apache/lucene/search/Query;
    .locals 2
    .param p1, "args"    # Lorg/apache/lucene/spatial/query/SpatialArgs;

    .prologue
    .line 122
    new-instance v0, Lorg/apache/lucene/search/ConstantScoreQuery;

    invoke-virtual {p0, p1}, Lorg/apache/lucene/spatial/SpatialStrategy;->makeFilter(Lorg/apache/lucene/spatial/query/SpatialArgs;)Lorg/apache/lucene/search/Filter;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/lucene/search/ConstantScoreQuery;-><init>(Lorg/apache/lucene/search/Filter;)V

    return-object v0
.end method

.method public final makeRecipDistanceValueSource(Lcom/spatial4j/core/shape/Shape;)Lorg/apache/lucene/queries/function/ValueSource;
    .locals 12
    .param p1, "queryShape"    # Lcom/spatial4j/core/shape/Shape;

    .prologue
    .line 149
    invoke-interface {p1}, Lcom/spatial4j/core/shape/Shape;->getBoundingBox()Lcom/spatial4j/core/shape/Rectangle;

    move-result-object v6

    .line 150
    .local v6, "bbox":Lcom/spatial4j/core/shape/Rectangle;
    iget-object v0, p0, Lorg/apache/lucene/spatial/SpatialStrategy;->ctx:Lcom/spatial4j/core/context/SpatialContext;

    invoke-virtual {v0}, Lcom/spatial4j/core/context/SpatialContext;->getDistCalc()Lcom/spatial4j/core/distance/DistanceCalculator;

    move-result-object v0

    .line 151
    iget-object v1, p0, Lorg/apache/lucene/spatial/SpatialStrategy;->ctx:Lcom/spatial4j/core/context/SpatialContext;

    invoke-interface {v6}, Lcom/spatial4j/core/shape/Rectangle;->getMinX()D

    move-result-wide v2

    invoke-interface {v6}, Lcom/spatial4j/core/shape/Rectangle;->getMinY()D

    move-result-wide v4

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/spatial4j/core/context/SpatialContext;->makePoint(DD)Lcom/spatial4j/core/shape/Point;

    move-result-object v1

    invoke-interface {v6}, Lcom/spatial4j/core/shape/Rectangle;->getMaxX()D

    move-result-wide v2

    invoke-interface {v6}, Lcom/spatial4j/core/shape/Rectangle;->getMaxY()D

    move-result-wide v4

    .line 150
    invoke-interface/range {v0 .. v5}, Lcom/spatial4j/core/distance/DistanceCalculator;->distance(Lcom/spatial4j/core/shape/Point;DD)D

    move-result-wide v8

    .line 152
    .local v8, "diagonalDist":D
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    mul-double v10, v8, v0

    .line 153
    .local v10, "distToEdge":D
    double-to-float v0, v10

    const v1, 0x3dcccccd    # 0.1f

    mul-float v7, v0, v1

    .line 154
    .local v7, "c":F
    new-instance v0, Lorg/apache/lucene/queries/function/valuesource/ReciprocalFloatFunction;

    invoke-interface {p1}, Lcom/spatial4j/core/shape/Shape;->getCenter()Lcom/spatial4j/core/shape/Point;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/apache/lucene/spatial/SpatialStrategy;->makeDistanceValueSource(Lcom/spatial4j/core/shape/Point;)Lorg/apache/lucene/queries/function/ValueSource;

    move-result-object v1

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2, v7, v7}, Lorg/apache/lucene/queries/function/valuesource/ReciprocalFloatFunction;-><init>(Lorg/apache/lucene/queries/function/ValueSource;FFF)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 159
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, " field:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/spatial/SpatialStrategy;->fieldName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ctx="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/spatial/SpatialStrategy;->ctx:Lcom/spatial4j/core/context/SpatialContext;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
