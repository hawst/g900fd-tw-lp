.class public abstract Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;
.super Lorg/apache/lucene/spatial/prefix/AbstractPrefixTreeFilter$BaseTermsEnumTraverser;
.source "AbstractVisitingPrefixTreeFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401
    name = "VisitorTemplate"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate$VNodeCellIterator;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private curVNode:Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;

.field private curVNodeTerm:Lorg/apache/lucene/util/BytesRef;

.field protected final hasIndexedLeaves:Z

.field private scanCell:Lorg/apache/lucene/spatial/prefix/tree/Cell;

.field final synthetic this$0:Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter;

.field private thisTerm:Lorg/apache/lucene/util/BytesRef;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 98
    const-class v0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter;Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;Z)V
    .locals 1
    .param p2, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p3, "acceptDocs"    # Lorg/apache/lucene/util/Bits;
    .param p4, "hasIndexedLeaves"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 121
    iput-object p1, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->this$0:Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter;

    .line 122
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/spatial/prefix/AbstractPrefixTreeFilter$BaseTermsEnumTraverser;-><init>(Lorg/apache/lucene/spatial/prefix/AbstractPrefixTreeFilter;Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;)V

    .line 115
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->curVNodeTerm:Lorg/apache/lucene/util/BytesRef;

    .line 123
    iput-boolean p4, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->hasIndexedLeaves:Z

    .line 124
    return-void
.end method

.method private addIntersectingChildren()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 204
    sget-boolean v3, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->thisTerm:Lorg/apache/lucene/util/BytesRef;

    if-nez v3, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 205
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->curVNode:Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;

    iget-object v0, v3, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;->cell:Lorg/apache/lucene/spatial/prefix/tree/Cell;

    .line 206
    .local v0, "cell":Lorg/apache/lucene/spatial/prefix/tree/Cell;
    invoke-virtual {v0}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->getLevel()I

    move-result v3

    iget-object v4, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->this$0:Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter;

    iget v4, v4, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter;->detailLevel:I

    if-lt v3, v4, :cond_1

    .line 207
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "Spatial logic error"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 210
    :cond_1
    iget-boolean v3, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->hasIndexedLeaves:Z

    if-eqz v3, :cond_4

    invoke-virtual {v0}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->getLevel()I

    move-result v3

    if-eqz v3, :cond_4

    .line 213
    sget-boolean v3, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->$assertionsDisabled:Z

    if-nez v3, :cond_2

    iget-object v3, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->thisTerm:Lorg/apache/lucene/util/BytesRef;

    iget-object v4, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->curVNodeTerm:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v3, v4}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v3

    if-nez v3, :cond_2

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 214
    :cond_2
    iget-object v3, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->this$0:Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter;

    iget-object v3, v3, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter;->grid:Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;

    iget-object v4, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->thisTerm:Lorg/apache/lucene/util/BytesRef;

    iget-object v4, v4, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget-object v5, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->thisTerm:Lorg/apache/lucene/util/BytesRef;

    iget v5, v5, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget-object v6, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->thisTerm:Lorg/apache/lucene/util/BytesRef;

    iget v6, v6, Lorg/apache/lucene/util/BytesRef;->length:I

    iget-object v7, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->scanCell:Lorg/apache/lucene/spatial/prefix/tree/Cell;

    invoke-virtual {v3, v4, v5, v6, v7}, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;->getCell([BIILorg/apache/lucene/spatial/prefix/tree/Cell;)Lorg/apache/lucene/spatial/prefix/tree/Cell;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->scanCell:Lorg/apache/lucene/spatial/prefix/tree/Cell;

    .line 215
    iget-object v3, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->scanCell:Lorg/apache/lucene/spatial/prefix/tree/Cell;

    invoke-virtual {v3}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->getLevel()I

    move-result v3

    invoke-virtual {v0}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->getLevel()I

    move-result v4

    if-ne v3, v4, :cond_4

    iget-object v3, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->scanCell:Lorg/apache/lucene/spatial/prefix/tree/Cell;

    invoke-virtual {v3}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->isLeaf()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 216
    iget-object v3, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->scanCell:Lorg/apache/lucene/spatial/prefix/tree/Cell;

    invoke-virtual {p0, v3}, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->visitLeaf(Lorg/apache/lucene/spatial/prefix/tree/Cell;)V

    .line 218
    iget-object v3, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->termsEnum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v3}, Lorg/apache/lucene/index/TermsEnum;->next()Lorg/apache/lucene/util/BytesRef;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->thisTerm:Lorg/apache/lucene/util/BytesRef;

    if-nez v3, :cond_4

    .line 243
    :cond_3
    :goto_0
    return-void

    .line 228
    :cond_4
    invoke-virtual {v0}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->getLevel()I

    move-result v3

    iget-object v4, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->this$0:Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter;

    iget v4, v4, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter;->prefixGridScanLevel:I

    if-lt v3, v4, :cond_5

    const/4 v1, 0x1

    .line 230
    .local v1, "scan":Z
    :goto_1
    if-nez v1, :cond_6

    .line 233
    invoke-virtual {p0, v0}, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->findSubCellsToVisit(Lorg/apache/lucene/spatial/prefix/tree/Cell;)Ljava/util/Iterator;

    move-result-object v2

    .line 234
    .local v2, "subCellsIter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/spatial/prefix/tree/Cell;>;"
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 236
    iget-object v3, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->curVNode:Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;

    new-instance v4, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate$VNodeCellIterator;

    new-instance v5, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;

    iget-object v6, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->curVNode:Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;

    invoke-direct {v5, v6}, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;-><init>(Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;)V

    invoke-direct {v4, p0, v2, v5}, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate$VNodeCellIterator;-><init>(Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;Ljava/util/Iterator;Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;)V

    iput-object v4, v3, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;->children:Ljava/util/Iterator;

    goto :goto_0

    .line 228
    .end local v1    # "scan":Z
    .end local v2    # "subCellsIter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/spatial/prefix/tree/Cell;>;"
    :cond_5
    const/4 v1, 0x0

    goto :goto_1

    .line 241
    .restart local v1    # "scan":Z
    :cond_6
    iget-object v3, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->this$0:Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter;

    iget v3, v3, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter;->detailLevel:I

    invoke-virtual {p0, v3}, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->scan(I)V

    goto :goto_0
.end method


# virtual methods
.method protected findSubCellsToVisit(Lorg/apache/lucene/spatial/prefix/tree/Cell;)Ljava/util/Iterator;
    .locals 1
    .param p1, "cell"    # Lorg/apache/lucene/spatial/prefix/tree/Cell;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/spatial/prefix/tree/Cell;",
            ")",
            "Ljava/util/Iterator",
            "<",
            "Lorg/apache/lucene/spatial/prefix/tree/Cell;",
            ">;"
        }
    .end annotation

    .prologue
    .line 252
    iget-object v0, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->this$0:Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter;

    iget-object v0, v0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter;->queryShape:Lcom/spatial4j/core/shape/Shape;

    invoke-virtual {p1, v0}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->getSubCells(Lcom/spatial4j/core/shape/Shape;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method protected abstract finish()Lorg/apache/lucene/search/DocIdSet;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public getDocIdSet()Lorg/apache/lucene/search/DocIdSet;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 127
    sget-boolean v4, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    iget-object v4, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->curVNode:Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;

    if-eqz v4, :cond_0

    new-instance v4, Ljava/lang/AssertionError;

    const-string v5, "Called more than once?"

    invoke-direct {v4, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    .line 128
    :cond_0
    iget-object v4, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->termsEnum:Lorg/apache/lucene/index/TermsEnum;

    if-nez v4, :cond_1

    move-object v4, v5

    .line 198
    :goto_0
    return-object v4

    .line 131
    :cond_1
    iget-object v4, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->termsEnum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v4}, Lorg/apache/lucene/index/TermsEnum;->next()Lorg/apache/lucene/util/BytesRef;

    move-result-object v4

    iput-object v4, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->thisTerm:Lorg/apache/lucene/util/BytesRef;

    if-nez v4, :cond_2

    move-object v4, v5

    .line 132
    goto :goto_0

    .line 134
    :cond_2
    new-instance v4, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;

    invoke-direct {v4, v5}, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;-><init>(Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;)V

    iput-object v4, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->curVNode:Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;

    .line 135
    iget-object v4, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->curVNode:Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;

    iget-object v6, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->this$0:Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter;

    iget-object v6, v6, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter;->grid:Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;

    invoke-virtual {v6}, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;->getWorldCell()Lorg/apache/lucene/spatial/prefix/tree/Cell;

    move-result-object v6

    invoke-virtual {v4, v6}, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;->reset(Lorg/apache/lucene/spatial/prefix/tree/Cell;)V

    .line 137
    invoke-virtual {p0}, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->start()V

    .line 139
    invoke-direct {p0}, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->addIntersectingChildren()V

    .line 141
    :cond_3
    :goto_1
    iget-object v4, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->thisTerm:Lorg/apache/lucene/util/BytesRef;

    if-nez v4, :cond_5

    .line 198
    :cond_4
    invoke-virtual {p0}, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->finish()Lorg/apache/lucene/search/DocIdSet;

    move-result-object v4

    goto :goto_0

    .line 144
    :cond_5
    iget-object v4, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->curVNode:Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;

    iget-object v4, v4, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;->children:Ljava/util/Iterator;

    if-eqz v4, :cond_7

    .line 146
    sget-boolean v4, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->$assertionsDisabled:Z

    if-nez v4, :cond_6

    iget-object v4, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->curVNode:Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;

    iget-object v4, v4, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;->children:Ljava/util/Iterator;

    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_6

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 147
    :cond_6
    iget-object v4, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->curVNode:Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;

    invoke-virtual {p0, v4}, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->preSiblings(Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;)V

    .line 148
    iget-object v4, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->curVNode:Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;

    iget-object v4, v4, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;->children:Ljava/util/Iterator;

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;

    iput-object v4, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->curVNode:Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;

    .line 169
    :goto_2
    iget-object v4, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->curVNodeTerm:Lorg/apache/lucene/util/BytesRef;

    iget-object v6, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->curVNode:Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;

    iget-object v6, v6, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;->cell:Lorg/apache/lucene/spatial/prefix/tree/Cell;

    invoke-virtual {v6}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->getTokenBytes()[B

    move-result-object v6

    iput-object v6, v4, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 170
    iget-object v4, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->curVNodeTerm:Lorg/apache/lucene/util/BytesRef;

    iget-object v6, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->curVNodeTerm:Lorg/apache/lucene/util/BytesRef;

    iget-object v6, v6, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    array-length v6, v6

    iput v6, v4, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 171
    iget-object v4, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->termsEnum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v4}, Lorg/apache/lucene/index/TermsEnum;->getComparator()Ljava/util/Comparator;

    move-result-object v4

    iget-object v6, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->thisTerm:Lorg/apache/lucene/util/BytesRef;

    iget-object v7, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->curVNodeTerm:Lorg/apache/lucene/util/BytesRef;

    invoke-interface {v4, v6, v7}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    .line 172
    .local v0, "compare":I
    if-lez v0, :cond_9

    .line 174
    sget-boolean v4, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->$assertionsDisabled:Z

    if-nez v4, :cond_3

    iget-object v4, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->context:Lorg/apache/lucene/index/AtomicReaderContext;

    invoke-virtual {v4}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v4

    iget-object v6, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->this$0:Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter;

    iget-object v6, v6, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter;->fieldName:Ljava/lang/String;

    invoke-virtual {v4, v6}, Lorg/apache/lucene/index/AtomicReader;->terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v4

    invoke-virtual {v4, v5}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v4

    iget-object v6, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->curVNodeTerm:Lorg/apache/lucene/util/BytesRef;

    const/4 v7, 0x0

    invoke-virtual {v4, v6, v7}, Lorg/apache/lucene/index/TermsEnum;->seekExact(Lorg/apache/lucene/util/BytesRef;Z)Z

    move-result v4

    if-eqz v4, :cond_3

    new-instance v4, Ljava/lang/AssertionError;

    const-string v5, "should be absent"

    invoke-direct {v4, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    .line 151
    .end local v0    # "compare":I
    :cond_7
    iget-object v4, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->curVNode:Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;

    iget-object v2, v4, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;->parent:Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;

    .line 153
    .local v2, "parentVNode":Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;
    :goto_3
    if-eqz v2, :cond_4

    .line 155
    iget-object v4, v2, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;->children:Ljava/util/Iterator;

    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 157
    iget-object v4, v2, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;->children:Ljava/util/Iterator;

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;

    iput-object v4, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->curVNode:Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;

    goto :goto_2

    .line 161
    :cond_8
    invoke-virtual {p0, v2}, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->postSiblings(Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;)V

    .line 162
    iput-object v5, v2, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;->children:Ljava/util/Iterator;

    .line 163
    iget-object v2, v2, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;->parent:Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;

    .line 152
    goto :goto_3

    .line 176
    .end local v2    # "parentVNode":Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;
    .restart local v0    # "compare":I
    :cond_9
    if-gez v0, :cond_a

    .line 178
    iget-object v4, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->termsEnum:Lorg/apache/lucene/index/TermsEnum;

    iget-object v6, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->curVNodeTerm:Lorg/apache/lucene/util/BytesRef;

    const/4 v7, 0x1

    invoke-virtual {v4, v6, v7}, Lorg/apache/lucene/index/TermsEnum;->seekCeil(Lorg/apache/lucene/util/BytesRef;Z)Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    move-result-object v3

    .line 179
    .local v3, "seekStatus":Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    sget-object v4, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->END:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    if-eq v3, v4, :cond_4

    .line 181
    iget-object v4, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->termsEnum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v4}, Lorg/apache/lucene/index/TermsEnum;->term()Lorg/apache/lucene/util/BytesRef;

    move-result-object v4

    iput-object v4, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->thisTerm:Lorg/apache/lucene/util/BytesRef;

    .line 182
    sget-object v4, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->NOT_FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    if-eq v3, v4, :cond_3

    .line 187
    .end local v3    # "seekStatus":Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    :cond_a
    iget-object v4, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->curVNode:Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;

    iget-object v4, v4, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;->cell:Lorg/apache/lucene/spatial/prefix/tree/Cell;

    invoke-virtual {p0, v4}, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->visit(Lorg/apache/lucene/spatial/prefix/tree/Cell;)Z

    move-result v1

    .line 189
    .local v1, "descend":Z
    iget-object v4, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->termsEnum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v4}, Lorg/apache/lucene/index/TermsEnum;->next()Lorg/apache/lucene/util/BytesRef;

    move-result-object v4

    iput-object v4, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->thisTerm:Lorg/apache/lucene/util/BytesRef;

    if-eqz v4, :cond_4

    .line 191
    if-eqz v1, :cond_3

    .line 192
    invoke-direct {p0}, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->addIntersectingChildren()V

    goto/16 :goto_1
.end method

.method protected postSiblings(Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;)V
    .locals 0
    .param p1, "vNode"    # Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 338
    return-void
.end method

.method protected preSiblings(Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;)V
    .locals 0
    .param p1, "vNode"    # Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 335
    return-void
.end method

.method protected scan(I)V
    .locals 6
    .param p1, "scanDetailLevel"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 263
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->thisTerm:Lorg/apache/lucene/util/BytesRef;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->thisTerm:Lorg/apache/lucene/util/BytesRef;

    iget-object v2, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->curVNodeTerm:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v1, v2}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 274
    :cond_0
    return-void

    .line 265
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->this$0:Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter;

    iget-object v1, v1, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter;->grid:Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;

    iget-object v2, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->thisTerm:Lorg/apache/lucene/util/BytesRef;

    iget-object v2, v2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget-object v3, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->thisTerm:Lorg/apache/lucene/util/BytesRef;

    iget v3, v3, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget-object v4, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->thisTerm:Lorg/apache/lucene/util/BytesRef;

    iget v4, v4, Lorg/apache/lucene/util/BytesRef;->length:I

    iget-object v5, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->scanCell:Lorg/apache/lucene/spatial/prefix/tree/Cell;

    invoke-virtual {v1, v2, v3, v4, v5}, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;->getCell([BIILorg/apache/lucene/spatial/prefix/tree/Cell;)Lorg/apache/lucene/spatial/prefix/tree/Cell;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->scanCell:Lorg/apache/lucene/spatial/prefix/tree/Cell;

    .line 267
    iget-object v1, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->scanCell:Lorg/apache/lucene/spatial/prefix/tree/Cell;

    invoke-virtual {v1}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->getLevel()I

    move-result v0

    .line 268
    .local v0, "termLevel":I
    if-le v0, p1, :cond_3

    .line 264
    :cond_2
    :goto_1
    iget-object v1, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->termsEnum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v1}, Lorg/apache/lucene/index/TermsEnum;->next()Lorg/apache/lucene/util/BytesRef;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->thisTerm:Lorg/apache/lucene/util/BytesRef;

    goto :goto_0

    .line 270
    :cond_3
    if-eq v0, p1, :cond_4

    iget-object v1, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->scanCell:Lorg/apache/lucene/spatial/prefix/tree/Cell;

    invoke-virtual {v1}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->isLeaf()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 271
    :cond_4
    iget-object v1, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->scanCell:Lorg/apache/lucene/spatial/prefix/tree/Cell;

    invoke-virtual {p0, v1}, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;->visitScanned(Lorg/apache/lucene/spatial/prefix/tree/Cell;)V

    goto :goto_1
.end method

.method protected abstract start()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method protected abstract visit(Lorg/apache/lucene/spatial/prefix/tree/Cell;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method protected abstract visitLeaf(Lorg/apache/lucene/spatial/prefix/tree/Cell;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method protected abstract visitScanned(Lorg/apache/lucene/spatial/prefix/tree/Cell;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
