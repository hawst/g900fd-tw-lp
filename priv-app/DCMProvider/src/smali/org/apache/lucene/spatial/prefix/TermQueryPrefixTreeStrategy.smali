.class public Lorg/apache/lucene/spatial/prefix/TermQueryPrefixTreeStrategy;
.super Lorg/apache/lucene/spatial/prefix/PrefixTreeStrategy;
.source "TermQueryPrefixTreeStrategy.java"


# direct methods
.method public constructor <init>(Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;Ljava/lang/String;)V
    .locals 1
    .param p1, "grid"    # Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;
    .param p2, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 46
    .line 47
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/spatial/prefix/PrefixTreeStrategy;-><init>(Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;Ljava/lang/String;Z)V

    .line 48
    return-void
.end method


# virtual methods
.method public makeFilter(Lorg/apache/lucene/spatial/query/SpatialArgs;)Lorg/apache/lucene/search/Filter;
    .locals 12
    .param p1, "args"    # Lorg/apache/lucene/spatial/query/SpatialArgs;

    .prologue
    .line 52
    invoke-virtual {p1}, Lorg/apache/lucene/spatial/query/SpatialArgs;->getOperation()Lorg/apache/lucene/spatial/query/SpatialOperation;

    move-result-object v5

    .line 53
    .local v5, "op":Lorg/apache/lucene/spatial/query/SpatialOperation;
    sget-object v8, Lorg/apache/lucene/spatial/query/SpatialOperation;->Intersects:Lorg/apache/lucene/spatial/query/SpatialOperation;

    if-eq v5, v8, :cond_0

    .line 54
    new-instance v8, Lorg/apache/lucene/spatial/query/UnsupportedSpatialOperation;

    invoke-direct {v8, v5}, Lorg/apache/lucene/spatial/query/UnsupportedSpatialOperation;-><init>(Lorg/apache/lucene/spatial/query/SpatialOperation;)V

    throw v8

    .line 56
    :cond_0
    invoke-virtual {p1}, Lorg/apache/lucene/spatial/query/SpatialArgs;->getShape()Lcom/spatial4j/core/shape/Shape;

    move-result-object v6

    .line 57
    .local v6, "shape":Lcom/spatial4j/core/shape/Shape;
    iget-object v8, p0, Lorg/apache/lucene/spatial/prefix/TermQueryPrefixTreeStrategy;->grid:Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;

    iget-object v9, p0, Lorg/apache/lucene/spatial/prefix/TermQueryPrefixTreeStrategy;->ctx:Lcom/spatial4j/core/context/SpatialContext;

    iget-wide v10, p0, Lorg/apache/lucene/spatial/prefix/TermQueryPrefixTreeStrategy;->distErrPct:D

    invoke-virtual {p1, v9, v10, v11}, Lorg/apache/lucene/spatial/query/SpatialArgs;->resolveDistErr(Lcom/spatial4j/core/context/SpatialContext;D)D

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;->getLevelForDistance(D)I

    move-result v2

    .line 58
    .local v2, "detailLevel":I
    iget-object v8, p0, Lorg/apache/lucene/spatial/prefix/TermQueryPrefixTreeStrategy;->grid:Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;

    .line 59
    const/4 v9, 0x0

    .line 60
    const/4 v10, 0x1

    .line 58
    invoke-virtual {v8, v6, v2, v9, v10}, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;->getCells(Lcom/spatial4j/core/shape/Shape;IZZ)Ljava/util/List;

    move-result-object v1

    .line 61
    .local v1, "cells":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/spatial/prefix/tree/Cell;>;"
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v8

    new-array v7, v8, [Lorg/apache/lucene/util/BytesRef;

    .line 62
    .local v7, "terms":[Lorg/apache/lucene/util/BytesRef;
    const/4 v3, 0x0

    .line 63
    .local v3, "i":I
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-nez v9, :cond_1

    .line 66
    new-instance v8, Lorg/apache/lucene/queries/TermsFilter;

    invoke-virtual {p0}, Lorg/apache/lucene/spatial/prefix/TermQueryPrefixTreeStrategy;->getFieldName()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9, v7}, Lorg/apache/lucene/queries/TermsFilter;-><init>(Ljava/lang/String;[Lorg/apache/lucene/util/BytesRef;)V

    return-object v8

    .line 63
    :cond_1
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/spatial/prefix/tree/Cell;

    .line 64
    .local v0, "cell":Lorg/apache/lucene/spatial/prefix/tree/Cell;
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "i":I
    .local v4, "i":I
    new-instance v9, Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v0}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->getTokenString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    aput-object v9, v7, v3

    move v3, v4

    .end local v4    # "i":I
    .restart local v3    # "i":I
    goto :goto_0
.end method
