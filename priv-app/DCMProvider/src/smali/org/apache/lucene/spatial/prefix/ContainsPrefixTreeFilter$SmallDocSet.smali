.class Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;
.super Lorg/apache/lucene/search/DocIdSet;
.source "ContainsPrefixTreeFilter.java"

# interfaces
.implements Lorg/apache/lucene/util/Bits;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SmallDocSet"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final intSet:Lorg/apache/lucene/util/SentinelIntSet;

.field private maxInt:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 149
    const-class v0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(I)V
    .locals 2
    .param p1, "size"    # I

    .prologue
    .line 154
    invoke-direct {p0}, Lorg/apache/lucene/search/DocIdSet;-><init>()V

    .line 152
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;->maxInt:I

    .line 155
    new-instance v0, Lorg/apache/lucene/util/SentinelIntSet;

    const/4 v1, -0x1

    invoke-direct {v0, p1, v1}, Lorg/apache/lucene/util/SentinelIntSet;-><init>(II)V

    iput-object v0, p0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;->intSet:Lorg/apache/lucene/util/SentinelIntSet;

    .line 156
    return-void
.end method


# virtual methods
.method public bits()Lorg/apache/lucene/util/Bits;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 204
    invoke-virtual {p0}, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;->size()I

    move-result v0

    const/4 v1, 0x4

    if-le v0, v1, :cond_0

    .end local p0    # "this":Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;
    :goto_0
    return-object p0

    .restart local p0    # "this":Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;
    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public get(I)Z
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 160
    iget-object v0, p0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;->intSet:Lorg/apache/lucene/util/SentinelIntSet;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/util/SentinelIntSet;->exists(I)Z

    move-result v0

    return v0
.end method

.method public iterator()Lorg/apache/lucene/search/DocIdSetIterator;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 209
    invoke-virtual {p0}, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;->size()I

    move-result v5

    if-nez v5, :cond_0

    .line 210
    const/4 v5, 0x0

    .line 225
    :goto_0
    return-object v5

    .line 212
    :cond_0
    const/4 v0, 0x0

    .line 213
    .local v0, "d":I
    iget-object v5, p0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;->intSet:Lorg/apache/lucene/util/SentinelIntSet;

    invoke-virtual {v5}, Lorg/apache/lucene/util/SentinelIntSet;->size()I

    move-result v5

    new-array v2, v5, [I

    .line 214
    .local v2, "docs":[I
    iget-object v5, p0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;->intSet:Lorg/apache/lucene/util/SentinelIntSet;

    iget-object v7, v5, Lorg/apache/lucene/util/SentinelIntSet;->keys:[I

    array-length v8, v7

    move v5, v6

    move v1, v0

    .end local v0    # "d":I
    .local v1, "d":I
    :goto_1
    if-lt v5, v8, :cond_1

    .line 219
    sget-boolean v5, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;->$assertionsDisabled:Z

    if-nez v5, :cond_3

    iget-object v5, p0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;->intSet:Lorg/apache/lucene/util/SentinelIntSet;

    invoke-virtual {v5}, Lorg/apache/lucene/util/SentinelIntSet;->size()I

    move-result v5

    if-eq v1, v5, :cond_3

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 214
    :cond_1
    aget v4, v7, v5

    .line 215
    .local v4, "v":I
    iget-object v9, p0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;->intSet:Lorg/apache/lucene/util/SentinelIntSet;

    iget v9, v9, Lorg/apache/lucene/util/SentinelIntSet;->emptyVal:I

    if-ne v4, v9, :cond_2

    move v0, v1

    .line 214
    .end local v1    # "d":I
    .restart local v0    # "d":I
    :goto_2
    add-int/lit8 v5, v5, 0x1

    move v1, v0

    .end local v0    # "d":I
    .restart local v1    # "d":I
    goto :goto_1

    .line 217
    :cond_2
    add-int/lit8 v0, v1, 0x1

    .end local v1    # "d":I
    .restart local v0    # "d":I
    aput v4, v2, v1

    goto :goto_2

    .line 220
    .end local v0    # "d":I
    .end local v4    # "v":I
    .restart local v1    # "d":I
    :cond_3
    move v3, v1

    .line 223
    .local v3, "size":I
    invoke-static {v2, v6, v3}, Ljava/util/Arrays;->sort([III)V

    .line 225
    new-instance v5, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet$1;

    invoke-direct {v5, p0, v3, v2}, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet$1;-><init>(Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;I[I)V

    goto :goto_0
.end method

.method public length()I
    .locals 1

    .prologue
    .line 172
    iget v0, p0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;->maxInt:I

    return v0
.end method

.method public set(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 164
    iget-object v0, p0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;->intSet:Lorg/apache/lucene/util/SentinelIntSet;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/util/SentinelIntSet;->put(I)I

    .line 165
    iget v0, p0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;->maxInt:I

    if-le p1, v0, :cond_0

    .line 166
    iput p1, p0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;->maxInt:I

    .line 167
    :cond_0
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;->intSet:Lorg/apache/lucene/util/SentinelIntSet;

    invoke-virtual {v0}, Lorg/apache/lucene/util/SentinelIntSet;->size()I

    move-result v0

    return v0
.end method

.method public union(Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;)Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;
    .locals 7
    .param p1, "other"    # Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;

    .prologue
    .line 184
    iget-object v3, p1, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;->intSet:Lorg/apache/lucene/util/SentinelIntSet;

    invoke-virtual {v3}, Lorg/apache/lucene/util/SentinelIntSet;->size()I

    move-result v3

    iget-object v4, p0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;->intSet:Lorg/apache/lucene/util/SentinelIntSet;

    invoke-virtual {v4}, Lorg/apache/lucene/util/SentinelIntSet;->size()I

    move-result v4

    if-le v3, v4, :cond_0

    .line 185
    move-object v0, p1

    .line 186
    .local v0, "bigger":Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;
    move-object v1, p0

    .line 192
    .local v1, "smaller":Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;
    :goto_0
    iget-object v3, v1, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;->intSet:Lorg/apache/lucene/util/SentinelIntSet;

    iget-object v4, v3, Lorg/apache/lucene/util/SentinelIntSet;->keys:[I

    array-length v5, v4

    const/4 v3, 0x0

    :goto_1
    if-lt v3, v5, :cond_1

    .line 197
    return-object v0

    .line 188
    .end local v0    # "bigger":Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;
    .end local v1    # "smaller":Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;
    :cond_0
    move-object v0, p0

    .line 189
    .restart local v0    # "bigger":Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;
    move-object v1, p1

    .restart local v1    # "smaller":Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;
    goto :goto_0

    .line 192
    :cond_1
    aget v2, v4, v3

    .line 193
    .local v2, "v":I
    iget-object v6, v1, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;->intSet:Lorg/apache/lucene/util/SentinelIntSet;

    iget v6, v6, Lorg/apache/lucene/util/SentinelIntSet;->emptyVal:I

    if-ne v2, v6, :cond_2

    .line 192
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 195
    :cond_2
    invoke-virtual {v0, v2}, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;->set(I)V

    goto :goto_2
.end method
