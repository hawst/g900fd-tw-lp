.class final Lorg/apache/lucene/spatial/prefix/PrefixTreeStrategy$CellTokenStream;
.super Lorg/apache/lucene/analysis/TokenStream;
.source "PrefixTreeStrategy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/spatial/prefix/PrefixTreeStrategy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "CellTokenStream"
.end annotation


# instance fields
.field private iter:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<",
            "Lorg/apache/lucene/spatial/prefix/tree/Cell;",
            ">;"
        }
    .end annotation
.end field

.field nextTokenStringNeedingLeaf:Ljava/lang/CharSequence;

.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;


# direct methods
.method public constructor <init>(Ljava/util/Iterator;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator",
            "<",
            "Lorg/apache/lucene/spatial/prefix/tree/Cell;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "tokens":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/spatial/prefix/tree/Cell;>;"
    const/4 v1, 0x0

    .line 157
    invoke-direct {p0}, Lorg/apache/lucene/analysis/TokenStream;-><init>()V

    .line 153
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/spatial/prefix/PrefixTreeStrategy$CellTokenStream;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/spatial/prefix/PrefixTreeStrategy$CellTokenStream;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 155
    iput-object v1, p0, Lorg/apache/lucene/spatial/prefix/PrefixTreeStrategy$CellTokenStream;->iter:Ljava/util/Iterator;

    .line 161
    iput-object v1, p0, Lorg/apache/lucene/spatial/prefix/PrefixTreeStrategy$CellTokenStream;->nextTokenStringNeedingLeaf:Ljava/lang/CharSequence;

    .line 158
    iput-object p1, p0, Lorg/apache/lucene/spatial/prefix/PrefixTreeStrategy$CellTokenStream;->iter:Ljava/util/Iterator;

    .line 159
    return-void
.end method


# virtual methods
.method public incrementToken()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 165
    invoke-virtual {p0}, Lorg/apache/lucene/spatial/prefix/PrefixTreeStrategy$CellTokenStream;->clearAttributes()V

    .line 166
    iget-object v3, p0, Lorg/apache/lucene/spatial/prefix/PrefixTreeStrategy$CellTokenStream;->nextTokenStringNeedingLeaf:Ljava/lang/CharSequence;

    if-eqz v3, :cond_1

    .line 167
    iget-object v3, p0, Lorg/apache/lucene/spatial/prefix/PrefixTreeStrategy$CellTokenStream;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iget-object v4, p0, Lorg/apache/lucene/spatial/prefix/PrefixTreeStrategy$CellTokenStream;->nextTokenStringNeedingLeaf:Ljava/lang/CharSequence;

    invoke-interface {v3, v4}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->append(Ljava/lang/CharSequence;)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 168
    iget-object v3, p0, Lorg/apache/lucene/spatial/prefix/PrefixTreeStrategy$CellTokenStream;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    const/16 v4, 0x2b

    invoke-interface {v3, v4}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->append(C)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 169
    const/4 v3, 0x0

    iput-object v3, p0, Lorg/apache/lucene/spatial/prefix/PrefixTreeStrategy$CellTokenStream;->nextTokenStringNeedingLeaf:Ljava/lang/CharSequence;

    .line 180
    :cond_0
    :goto_0
    return v2

    .line 172
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/spatial/prefix/PrefixTreeStrategy$CellTokenStream;->iter:Ljava/util/Iterator;

    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 173
    iget-object v3, p0, Lorg/apache/lucene/spatial/prefix/PrefixTreeStrategy$CellTokenStream;->iter:Ljava/util/Iterator;

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/spatial/prefix/tree/Cell;

    .line 174
    .local v0, "cell":Lorg/apache/lucene/spatial/prefix/tree/Cell;
    invoke-virtual {v0}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->getTokenString()Ljava/lang/String;

    move-result-object v1

    .line 175
    .local v1, "token":Ljava/lang/CharSequence;
    iget-object v3, p0, Lorg/apache/lucene/spatial/prefix/PrefixTreeStrategy$CellTokenStream;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v3, v1}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->append(Ljava/lang/CharSequence;)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 176
    invoke-virtual {v0}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->isLeaf()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 177
    iput-object v1, p0, Lorg/apache/lucene/spatial/prefix/PrefixTreeStrategy$CellTokenStream;->nextTokenStringNeedingLeaf:Ljava/lang/CharSequence;

    goto :goto_0

    .line 180
    .end local v0    # "cell":Lorg/apache/lucene/spatial/prefix/tree/Cell;
    .end local v1    # "token":Ljava/lang/CharSequence;
    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method
