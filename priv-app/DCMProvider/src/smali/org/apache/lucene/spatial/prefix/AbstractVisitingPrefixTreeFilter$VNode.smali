.class public Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;
.super Ljava/lang/Object;
.source "AbstractVisitingPrefixTreeFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "VNode"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field cell:Lorg/apache/lucene/spatial/prefix/tree/Cell;

.field children:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<",
            "Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;",
            ">;"
        }
    .end annotation
.end field

.field final parent:Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 348
    const-class v0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;)V
    .locals 0
    .param p1, "parent"    # Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;

    .prologue
    .line 361
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 362
    iput-object p1, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;->parent:Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;

    .line 363
    return-void
.end method


# virtual methods
.method reset(Lorg/apache/lucene/spatial/prefix/tree/Cell;)V
    .locals 1
    .param p1, "cell"    # Lorg/apache/lucene/spatial/prefix/tree/Cell;

    .prologue
    .line 366
    sget-boolean v0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 367
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;->cell:Lorg/apache/lucene/spatial/prefix/tree/Cell;

    .line 368
    sget-boolean v0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;->children:Ljava/util/Iterator;

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 369
    :cond_1
    return-void
.end method
