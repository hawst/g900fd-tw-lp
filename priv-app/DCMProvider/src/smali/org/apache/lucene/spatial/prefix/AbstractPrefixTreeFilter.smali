.class public abstract Lorg/apache/lucene/spatial/prefix/AbstractPrefixTreeFilter;
.super Lorg/apache/lucene/search/Filter;
.source "AbstractPrefixTreeFilter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/spatial/prefix/AbstractPrefixTreeFilter$BaseTermsEnumTraverser;
    }
.end annotation


# instance fields
.field protected final detailLevel:I

.field protected final fieldName:Ljava/lang/String;

.field protected final grid:Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;

.field protected final queryShape:Lcom/spatial4j/core/shape/Shape;


# direct methods
.method public constructor <init>(Lcom/spatial4j/core/shape/Shape;Ljava/lang/String;Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;I)V
    .locals 0
    .param p1, "queryShape"    # Lcom/spatial4j/core/shape/Shape;
    .param p2, "fieldName"    # Ljava/lang/String;
    .param p3, "grid"    # Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;
    .param p4, "detailLevel"    # I

    .prologue
    .line 46
    invoke-direct {p0}, Lorg/apache/lucene/search/Filter;-><init>()V

    .line 47
    iput-object p1, p0, Lorg/apache/lucene/spatial/prefix/AbstractPrefixTreeFilter;->queryShape:Lcom/spatial4j/core/shape/Shape;

    .line 48
    iput-object p2, p0, Lorg/apache/lucene/spatial/prefix/AbstractPrefixTreeFilter;->fieldName:Ljava/lang/String;

    .line 49
    iput-object p3, p0, Lorg/apache/lucene/spatial/prefix/AbstractPrefixTreeFilter;->grid:Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;

    .line 50
    iput p4, p0, Lorg/apache/lucene/spatial/prefix/AbstractPrefixTreeFilter;->detailLevel:I

    .line 51
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 55
    if-ne p0, p1, :cond_1

    .line 64
    :cond_0
    :goto_0
    return v1

    .line 56
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 58
    check-cast v0, Lorg/apache/lucene/spatial/prefix/AbstractPrefixTreeFilter;

    .line 60
    .local v0, "that":Lorg/apache/lucene/spatial/prefix/AbstractPrefixTreeFilter;
    iget v3, p0, Lorg/apache/lucene/spatial/prefix/AbstractPrefixTreeFilter;->detailLevel:I

    iget v4, v0, Lorg/apache/lucene/spatial/prefix/AbstractPrefixTreeFilter;->detailLevel:I

    if-eq v3, v4, :cond_3

    move v1, v2

    goto :goto_0

    .line 61
    :cond_3
    iget-object v3, p0, Lorg/apache/lucene/spatial/prefix/AbstractPrefixTreeFilter;->fieldName:Ljava/lang/String;

    iget-object v4, v0, Lorg/apache/lucene/spatial/prefix/AbstractPrefixTreeFilter;->fieldName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    goto :goto_0

    .line 62
    :cond_4
    iget-object v3, p0, Lorg/apache/lucene/spatial/prefix/AbstractPrefixTreeFilter;->queryShape:Lcom/spatial4j/core/shape/Shape;

    iget-object v4, v0, Lorg/apache/lucene/spatial/prefix/AbstractPrefixTreeFilter;->queryShape:Lcom/spatial4j/core/shape/Shape;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 69
    iget-object v1, p0, Lorg/apache/lucene/spatial/prefix/AbstractPrefixTreeFilter;->queryShape:Lcom/spatial4j/core/shape/Shape;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    .line 70
    .local v0, "result":I
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lorg/apache/lucene/spatial/prefix/AbstractPrefixTreeFilter;->fieldName:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int v0, v1, v2

    .line 71
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lorg/apache/lucene/spatial/prefix/AbstractPrefixTreeFilter;->detailLevel:I

    add-int v0, v1, v2

    .line 72
    return v0
.end method
