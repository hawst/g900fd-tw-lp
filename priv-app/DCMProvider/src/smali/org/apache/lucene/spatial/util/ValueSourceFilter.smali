.class public Lorg/apache/lucene/spatial/util/ValueSourceFilter;
.super Lorg/apache/lucene/search/Filter;
.source "ValueSourceFilter.java"


# instance fields
.field final max:D

.field final min:D

.field final source:Lorg/apache/lucene/queries/function/ValueSource;

.field final startingFilter:Lorg/apache/lucene/search/Filter;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/Filter;Lorg/apache/lucene/queries/function/ValueSource;DD)V
    .locals 3
    .param p1, "startingFilter"    # Lorg/apache/lucene/search/Filter;
    .param p2, "source"    # Lorg/apache/lucene/queries/function/ValueSource;
    .param p3, "min"    # D
    .param p5, "max"    # D

    .prologue
    .line 43
    invoke-direct {p0}, Lorg/apache/lucene/search/Filter;-><init>()V

    .line 45
    if-nez p1, :cond_0

    .line 46
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "please provide a non-null startingFilter; you can use QueryWrapperFilter(MatchAllDocsQuery) as a no-op filter"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 48
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/spatial/util/ValueSourceFilter;->startingFilter:Lorg/apache/lucene/search/Filter;

    .line 49
    iput-object p2, p0, Lorg/apache/lucene/spatial/util/ValueSourceFilter;->source:Lorg/apache/lucene/queries/function/ValueSource;

    .line 50
    iput-wide p3, p0, Lorg/apache/lucene/spatial/util/ValueSourceFilter;->min:D

    .line 51
    iput-wide p5, p0, Lorg/apache/lucene/spatial/util/ValueSourceFilter;->max:D

    .line 52
    return-void
.end method


# virtual methods
.method public getDocIdSet(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/DocIdSet;
    .locals 3
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "acceptDocs"    # Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 56
    iget-object v1, p0, Lorg/apache/lucene/spatial/util/ValueSourceFilter;->source:Lorg/apache/lucene/queries/function/ValueSource;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, p1}, Lorg/apache/lucene/queries/function/ValueSource;->getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;

    move-result-object v0

    .line 57
    .local v0, "values":Lorg/apache/lucene/queries/function/FunctionValues;
    new-instance v1, Lorg/apache/lucene/spatial/util/ValueSourceFilter$1;

    iget-object v2, p0, Lorg/apache/lucene/spatial/util/ValueSourceFilter;->startingFilter:Lorg/apache/lucene/search/Filter;

    invoke-virtual {v2, p1, p2}, Lorg/apache/lucene/search/Filter;->getDocIdSet(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/DocIdSet;

    move-result-object v2

    invoke-direct {v1, p0, v2, v0}, Lorg/apache/lucene/spatial/util/ValueSourceFilter$1;-><init>(Lorg/apache/lucene/spatial/util/ValueSourceFilter;Lorg/apache/lucene/search/DocIdSet;Lorg/apache/lucene/queries/function/FunctionValues;)V

    return-object v1
.end method
