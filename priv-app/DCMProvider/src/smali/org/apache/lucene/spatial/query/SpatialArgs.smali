.class public Lorg/apache/lucene/spatial/query/SpatialArgs;
.super Ljava/lang/Object;
.source "SpatialArgs.java"


# static fields
.field public static final DEFAULT_DISTERRPCT:D = 0.025


# instance fields
.field private distErr:Ljava/lang/Double;

.field private distErrPct:Ljava/lang/Double;

.field private operation:Lorg/apache/lucene/spatial/query/SpatialOperation;

.field private shape:Lcom/spatial4j/core/shape/Shape;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/spatial/query/SpatialOperation;Lcom/spatial4j/core/shape/Shape;)V
    .locals 2
    .param p1, "operation"    # Lorg/apache/lucene/spatial/query/SpatialOperation;
    .param p2, "shape"    # Lcom/spatial4j/core/shape/Shape;

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 42
    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "operation and shape are required"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 43
    :cond_1
    iput-object p1, p0, Lorg/apache/lucene/spatial/query/SpatialArgs;->operation:Lorg/apache/lucene/spatial/query/SpatialOperation;

    .line 44
    iput-object p2, p0, Lorg/apache/lucene/spatial/query/SpatialArgs;->shape:Lcom/spatial4j/core/shape/Shape;

    .line 45
    return-void
.end method

.method public static calcDistanceFromErrPct(Lcom/spatial4j/core/shape/Shape;DLcom/spatial4j/core/context/SpatialContext;)D
    .locals 13
    .param p0, "shape"    # Lcom/spatial4j/core/shape/Shape;
    .param p1, "distErrPct"    # D
    .param p3, "ctx"    # Lcom/spatial4j/core/context/SpatialContext;

    .prologue
    const-wide/16 v2, 0x0

    .line 58
    cmpg-double v0, p1, v2

    if-ltz v0, :cond_0

    const-wide/high16 v10, 0x3fe0000000000000L    # 0.5

    cmpl-double v0, p1, v10

    if-lez v0, :cond_1

    .line 59
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "distErrPct "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " must be between [0 to 0.5]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 61
    :cond_1
    cmpl-double v0, p1, v2

    if-eqz v0, :cond_2

    instance-of v0, p0, Lcom/spatial4j/core/shape/Point;

    if-eqz v0, :cond_3

    .line 71
    :cond_2
    :goto_0
    return-wide v2

    .line 64
    :cond_3
    invoke-interface {p0}, Lcom/spatial4j/core/shape/Shape;->getBoundingBox()Lcom/spatial4j/core/shape/Rectangle;

    move-result-object v6

    .line 68
    .local v6, "bbox":Lcom/spatial4j/core/shape/Rectangle;
    invoke-interface {v6}, Lcom/spatial4j/core/shape/Rectangle;->getCenter()Lcom/spatial4j/core/shape/Point;

    move-result-object v1

    .line 69
    .local v1, "ctr":Lcom/spatial4j/core/shape/Point;
    invoke-interface {v1}, Lcom/spatial4j/core/shape/Point;->getY()D

    move-result-wide v10

    cmpl-double v0, v10, v2

    if-ltz v0, :cond_4

    invoke-interface {v6}, Lcom/spatial4j/core/shape/Rectangle;->getMaxY()D

    move-result-wide v4

    .line 70
    .local v4, "y":D
    :goto_1
    invoke-virtual/range {p3 .. p3}, Lcom/spatial4j/core/context/SpatialContext;->getDistCalc()Lcom/spatial4j/core/distance/DistanceCalculator;

    move-result-object v0

    invoke-interface {v6}, Lcom/spatial4j/core/shape/Rectangle;->getMaxX()D

    move-result-wide v2

    invoke-interface/range {v0 .. v5}, Lcom/spatial4j/core/distance/DistanceCalculator;->distance(Lcom/spatial4j/core/shape/Point;DD)D

    move-result-wide v8

    .line 71
    .local v8, "diagonalDist":D
    mul-double v2, v8, p1

    goto :goto_0

    .line 69
    .end local v4    # "y":D
    .end local v8    # "diagonalDist":D
    :cond_4
    invoke-interface {v6}, Lcom/spatial4j/core/shape/Rectangle;->getMinY()D

    move-result-wide v4

    goto :goto_1
.end method


# virtual methods
.method public getDistErr()Ljava/lang/Double;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lorg/apache/lucene/spatial/query/SpatialArgs;->distErr:Ljava/lang/Double;

    return-object v0
.end method

.method public getDistErrPct()Ljava/lang/Double;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lorg/apache/lucene/spatial/query/SpatialArgs;->distErrPct:Ljava/lang/Double;

    return-object v0
.end method

.method public getOperation()Lorg/apache/lucene/spatial/query/SpatialOperation;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lorg/apache/lucene/spatial/query/SpatialArgs;->operation:Lorg/apache/lucene/spatial/query/SpatialOperation;

    return-object v0
.end method

.method public getShape()Lcom/spatial4j/core/shape/Shape;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lorg/apache/lucene/spatial/query/SpatialArgs;->shape:Lcom/spatial4j/core/shape/Shape;

    return-object v0
.end method

.method public resolveDistErr(Lcom/spatial4j/core/context/SpatialContext;D)D
    .locals 4
    .param p1, "ctx"    # Lcom/spatial4j/core/context/SpatialContext;
    .param p2, "defaultDistErrPct"    # D

    .prologue
    .line 82
    iget-object v2, p0, Lorg/apache/lucene/spatial/query/SpatialArgs;->distErr:Ljava/lang/Double;

    if-eqz v2, :cond_0

    .line 83
    iget-object v2, p0, Lorg/apache/lucene/spatial/query/SpatialArgs;->distErr:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    .line 85
    :goto_0
    return-wide v2

    .line 84
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/spatial/query/SpatialArgs;->distErrPct:Ljava/lang/Double;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lorg/apache/lucene/spatial/query/SpatialArgs;->distErrPct:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    .line 85
    .local v0, "distErrPct":D
    :goto_1
    iget-object v2, p0, Lorg/apache/lucene/spatial/query/SpatialArgs;->shape:Lcom/spatial4j/core/shape/Shape;

    invoke-static {v2, v0, v1, p1}, Lorg/apache/lucene/spatial/query/SpatialArgs;->calcDistanceFromErrPct(Lcom/spatial4j/core/shape/Shape;DLcom/spatial4j/core/context/SpatialContext;)D

    move-result-wide v2

    goto :goto_0

    .end local v0    # "distErrPct":D
    :cond_1
    move-wide v0, p2

    .line 84
    goto :goto_1
.end method

.method public setDistErr(Ljava/lang/Double;)V
    .locals 0
    .param p1, "distErr"    # Ljava/lang/Double;

    .prologue
    .line 150
    iput-object p1, p0, Lorg/apache/lucene/spatial/query/SpatialArgs;->distErr:Ljava/lang/Double;

    .line 151
    return-void
.end method

.method public setDistErrPct(Ljava/lang/Double;)V
    .locals 0
    .param p1, "distErrPct"    # Ljava/lang/Double;

    .prologue
    .line 135
    if-eqz p1, :cond_0

    .line 136
    iput-object p1, p0, Lorg/apache/lucene/spatial/query/SpatialArgs;->distErrPct:Ljava/lang/Double;

    .line 137
    :cond_0
    return-void
.end method

.method public setOperation(Lorg/apache/lucene/spatial/query/SpatialOperation;)V
    .locals 0
    .param p1, "operation"    # Lorg/apache/lucene/spatial/query/SpatialOperation;

    .prologue
    .line 111
    iput-object p1, p0, Lorg/apache/lucene/spatial/query/SpatialArgs;->operation:Lorg/apache/lucene/spatial/query/SpatialOperation;

    .line 112
    return-void
.end method

.method public setShape(Lcom/spatial4j/core/shape/Shape;)V
    .locals 0
    .param p1, "shape"    # Lcom/spatial4j/core/shape/Shape;

    .prologue
    .line 119
    iput-object p1, p0, Lorg/apache/lucene/spatial/query/SpatialArgs;->shape:Lcom/spatial4j/core/shape/Shape;

    .line 120
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    invoke-static {p0}, Lorg/apache/lucene/spatial/query/SpatialArgsParser;->writeSpatialArgs(Lorg/apache/lucene/spatial/query/SpatialArgs;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public validate()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 90
    iget-object v0, p0, Lorg/apache/lucene/spatial/query/SpatialArgs;->operation:Lorg/apache/lucene/spatial/query/SpatialOperation;

    invoke-virtual {v0}, Lorg/apache/lucene/spatial/query/SpatialOperation;->isTargetNeedsArea()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/spatial/query/SpatialArgs;->shape:Lcom/spatial4j/core/shape/Shape;

    invoke-interface {v0}, Lcom/spatial4j/core/shape/Shape;->hasArea()Z

    move-result v0

    if-nez v0, :cond_0

    .line 91
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lorg/apache/lucene/spatial/query/SpatialArgs;->operation:Lorg/apache/lucene/spatial/query/SpatialOperation;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " only supports geometry with area"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 93
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/spatial/query/SpatialArgs;->distErr:Ljava/lang/Double;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/spatial/query/SpatialArgs;->distErrPct:Ljava/lang/Double;

    if-eqz v0, :cond_1

    .line 94
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Only distErr or distErrPct can be specified."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 95
    :cond_1
    return-void
.end method
