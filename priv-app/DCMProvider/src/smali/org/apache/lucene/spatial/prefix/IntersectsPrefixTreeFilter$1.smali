.class Lorg/apache/lucene/spatial/prefix/IntersectsPrefixTreeFilter$1;
.super Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;
.source "IntersectsPrefixTreeFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/spatial/prefix/IntersectsPrefixTreeFilter;->getDocIdSet(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/DocIdSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private results:Lorg/apache/lucene/util/FixedBitSet;

.field final synthetic this$0:Lorg/apache/lucene/spatial/prefix/IntersectsPrefixTreeFilter;


# direct methods
.method constructor <init>(Lorg/apache/lucene/spatial/prefix/IntersectsPrefixTreeFilter;Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter;Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;Z)V
    .locals 0
    .param p3, "$anonymous0"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p4, "$anonymous1"    # Lorg/apache/lucene/util/Bits;
    .param p5, "$anonymous2"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/spatial/prefix/IntersectsPrefixTreeFilter$1;->this$0:Lorg/apache/lucene/spatial/prefix/IntersectsPrefixTreeFilter;

    .line 55
    invoke-direct {p0, p2, p3, p4, p5}, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;-><init>(Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter;Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;Z)V

    return-void
.end method


# virtual methods
.method protected finish()Lorg/apache/lucene/search/DocIdSet;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lorg/apache/lucene/spatial/prefix/IntersectsPrefixTreeFilter$1;->results:Lorg/apache/lucene/util/FixedBitSet;

    return-object v0
.end method

.method protected start()V
    .locals 2

    .prologue
    .line 60
    new-instance v0, Lorg/apache/lucene/util/FixedBitSet;

    iget v1, p0, Lorg/apache/lucene/spatial/prefix/IntersectsPrefixTreeFilter$1;->maxDoc:I

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/FixedBitSet;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/spatial/prefix/IntersectsPrefixTreeFilter$1;->results:Lorg/apache/lucene/util/FixedBitSet;

    .line 61
    return-void
.end method

.method protected visit(Lorg/apache/lucene/spatial/prefix/tree/Cell;)Z
    .locals 2
    .param p1, "cell"    # Lorg/apache/lucene/spatial/prefix/tree/Cell;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 70
    invoke-virtual {p1}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->getShapeRel()Lcom/spatial4j/core/shape/SpatialRelation;

    move-result-object v0

    sget-object v1, Lcom/spatial4j/core/shape/SpatialRelation;->WITHIN:Lcom/spatial4j/core/shape/SpatialRelation;

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->getLevel()I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/spatial/prefix/IntersectsPrefixTreeFilter$1;->this$0:Lorg/apache/lucene/spatial/prefix/IntersectsPrefixTreeFilter;

    iget v1, v1, Lorg/apache/lucene/spatial/prefix/IntersectsPrefixTreeFilter;->detailLevel:I

    if-ne v0, v1, :cond_1

    .line 71
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/spatial/prefix/IntersectsPrefixTreeFilter$1;->results:Lorg/apache/lucene/util/FixedBitSet;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/spatial/prefix/IntersectsPrefixTreeFilter$1;->collectDocs(Lorg/apache/lucene/util/FixedBitSet;)V

    .line 72
    const/4 v0, 0x0

    .line 74
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected visitLeaf(Lorg/apache/lucene/spatial/prefix/tree/Cell;)V
    .locals 1
    .param p1, "cell"    # Lorg/apache/lucene/spatial/prefix/tree/Cell;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 79
    iget-object v0, p0, Lorg/apache/lucene/spatial/prefix/IntersectsPrefixTreeFilter$1;->results:Lorg/apache/lucene/util/FixedBitSet;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/spatial/prefix/IntersectsPrefixTreeFilter$1;->collectDocs(Lorg/apache/lucene/util/FixedBitSet;)V

    .line 80
    return-void
.end method

.method protected visitScanned(Lorg/apache/lucene/spatial/prefix/tree/Cell;)V
    .locals 3
    .param p1, "cell"    # Lorg/apache/lucene/spatial/prefix/tree/Cell;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 88
    invoke-virtual {p1}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->getLevel()I

    move-result v1

    iget-object v2, p0, Lorg/apache/lucene/spatial/prefix/IntersectsPrefixTreeFilter$1;->this$0:Lorg/apache/lucene/spatial/prefix/IntersectsPrefixTreeFilter;

    iget-object v2, v2, Lorg/apache/lucene/spatial/prefix/IntersectsPrefixTreeFilter;->grid:Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;

    invoke-virtual {v2}, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;->getMaxLevels()I

    move-result v2

    if-ne v1, v2, :cond_1

    invoke-virtual {p1}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->isLeaf()Z

    move-result v1

    if-nez v1, :cond_1

    .line 89
    invoke-virtual {p1}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->getCenter()Lcom/spatial4j/core/shape/Point;

    move-result-object v0

    .line 92
    .local v0, "cShape":Lcom/spatial4j/core/shape/Shape;
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/spatial/prefix/IntersectsPrefixTreeFilter$1;->this$0:Lorg/apache/lucene/spatial/prefix/IntersectsPrefixTreeFilter;

    iget-object v1, v1, Lorg/apache/lucene/spatial/prefix/IntersectsPrefixTreeFilter;->queryShape:Lcom/spatial4j/core/shape/Shape;

    invoke-interface {v1, v0}, Lcom/spatial4j/core/shape/Shape;->relate(Lcom/spatial4j/core/shape/Shape;)Lcom/spatial4j/core/shape/SpatialRelation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/spatial4j/core/shape/SpatialRelation;->intersects()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 93
    iget-object v1, p0, Lorg/apache/lucene/spatial/prefix/IntersectsPrefixTreeFilter$1;->results:Lorg/apache/lucene/util/FixedBitSet;

    invoke-virtual {p0, v1}, Lorg/apache/lucene/spatial/prefix/IntersectsPrefixTreeFilter$1;->collectDocs(Lorg/apache/lucene/util/FixedBitSet;)V

    .line 94
    :cond_0
    return-void

    .line 91
    .end local v0    # "cShape":Lcom/spatial4j/core/shape/Shape;
    :cond_1
    invoke-virtual {p1}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->getShape()Lcom/spatial4j/core/shape/Shape;

    move-result-object v0

    .restart local v0    # "cShape":Lcom/spatial4j/core/shape/Shape;
    goto :goto_0
.end method
