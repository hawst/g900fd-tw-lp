.class public Lorg/apache/lucene/spatial/prefix/PointPrefixTreeFieldCacheProvider;
.super Lorg/apache/lucene/spatial/util/ShapeFieldCacheProvider;
.source "PointPrefixTreeFieldCacheProvider.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/spatial/util/ShapeFieldCacheProvider",
        "<",
        "Lcom/spatial4j/core/shape/Point;",
        ">;"
    }
.end annotation


# instance fields
.field final grid:Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;

.field private scanCell:Lorg/apache/lucene/spatial/prefix/tree/Cell;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;Ljava/lang/String;I)V
    .locals 1
    .param p1, "grid"    # Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;
    .param p2, "shapeField"    # Ljava/lang/String;
    .param p3, "defaultSize"    # I

    .prologue
    .line 39
    invoke-direct {p0, p2, p3}, Lorg/apache/lucene/spatial/util/ShapeFieldCacheProvider;-><init>(Ljava/lang/String;I)V

    .line 43
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/spatial/prefix/PointPrefixTreeFieldCacheProvider;->scanCell:Lorg/apache/lucene/spatial/prefix/tree/Cell;

    .line 40
    iput-object p1, p0, Lorg/apache/lucene/spatial/prefix/PointPrefixTreeFieldCacheProvider;->grid:Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;

    .line 41
    return-void
.end method


# virtual methods
.method protected readShape(Lorg/apache/lucene/util/BytesRef;)Lcom/spatial4j/core/shape/Point;
    .locals 5
    .param p1, "term"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 47
    iget-object v0, p0, Lorg/apache/lucene/spatial/prefix/PointPrefixTreeFieldCacheProvider;->grid:Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;

    iget-object v1, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v2, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget v3, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    iget-object v4, p0, Lorg/apache/lucene/spatial/prefix/PointPrefixTreeFieldCacheProvider;->scanCell:Lorg/apache/lucene/spatial/prefix/tree/Cell;

    invoke-virtual {v0, v1, v2, v3, v4}, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;->getCell([BIILorg/apache/lucene/spatial/prefix/tree/Cell;)Lorg/apache/lucene/spatial/prefix/tree/Cell;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/spatial/prefix/PointPrefixTreeFieldCacheProvider;->scanCell:Lorg/apache/lucene/spatial/prefix/tree/Cell;

    .line 48
    iget-object v0, p0, Lorg/apache/lucene/spatial/prefix/PointPrefixTreeFieldCacheProvider;->scanCell:Lorg/apache/lucene/spatial/prefix/tree/Cell;

    invoke-virtual {v0}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->getLevel()I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/spatial/prefix/PointPrefixTreeFieldCacheProvider;->grid:Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;

    invoke-virtual {v1}, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;->getMaxLevels()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/spatial/prefix/PointPrefixTreeFieldCacheProvider;->scanCell:Lorg/apache/lucene/spatial/prefix/tree/Cell;

    invoke-virtual {v0}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->isLeaf()Z

    move-result v0

    if-nez v0, :cond_0

    .line 49
    iget-object v0, p0, Lorg/apache/lucene/spatial/prefix/PointPrefixTreeFieldCacheProvider;->scanCell:Lorg/apache/lucene/spatial/prefix/tree/Cell;

    invoke-virtual {v0}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->getCenter()Lcom/spatial4j/core/shape/Point;

    move-result-object v0

    .line 50
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected bridge synthetic readShape(Lorg/apache/lucene/util/BytesRef;)Lcom/spatial4j/core/shape/Shape;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/spatial/prefix/PointPrefixTreeFieldCacheProvider;->readShape(Lorg/apache/lucene/util/BytesRef;)Lcom/spatial4j/core/shape/Point;

    move-result-object v0

    return-object v0
.end method
