.class Lorg/apache/lucene/spatial/util/ShapeFieldCacheDistanceValueSource$1;
.super Lorg/apache/lucene/queries/function/FunctionValues;
.source "ShapeFieldCacheDistanceValueSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/spatial/util/ShapeFieldCacheDistanceValueSource;->getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final cache:Lorg/apache/lucene/spatial/util/ShapeFieldCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/spatial/util/ShapeFieldCache",
            "<",
            "Lcom/spatial4j/core/shape/Point;",
            ">;"
        }
    .end annotation
.end field

.field private final calculator:Lcom/spatial4j/core/distance/DistanceCalculator;

.field private final from:Lcom/spatial4j/core/shape/Point;

.field private final nullValue:D

.field final synthetic this$0:Lorg/apache/lucene/spatial/util/ShapeFieldCacheDistanceValueSource;


# direct methods
.method constructor <init>(Lorg/apache/lucene/spatial/util/ShapeFieldCacheDistanceValueSource;Lorg/apache/lucene/index/AtomicReaderContext;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/spatial/util/ShapeFieldCacheDistanceValueSource$1;->this$0:Lorg/apache/lucene/spatial/util/ShapeFieldCacheDistanceValueSource;

    .line 58
    invoke-direct {p0}, Lorg/apache/lucene/queries/function/FunctionValues;-><init>()V

    .line 60
    # getter for: Lorg/apache/lucene/spatial/util/ShapeFieldCacheDistanceValueSource;->provider:Lorg/apache/lucene/spatial/util/ShapeFieldCacheProvider;
    invoke-static {p1}, Lorg/apache/lucene/spatial/util/ShapeFieldCacheDistanceValueSource;->access$0(Lorg/apache/lucene/spatial/util/ShapeFieldCacheDistanceValueSource;)Lorg/apache/lucene/spatial/util/ShapeFieldCacheProvider;

    move-result-object v0

    invoke-virtual {p2}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/spatial/util/ShapeFieldCacheProvider;->getCache(Lorg/apache/lucene/index/AtomicReader;)Lorg/apache/lucene/spatial/util/ShapeFieldCache;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/spatial/util/ShapeFieldCacheDistanceValueSource$1;->cache:Lorg/apache/lucene/spatial/util/ShapeFieldCache;

    .line 61
    # getter for: Lorg/apache/lucene/spatial/util/ShapeFieldCacheDistanceValueSource;->from:Lcom/spatial4j/core/shape/Point;
    invoke-static {p1}, Lorg/apache/lucene/spatial/util/ShapeFieldCacheDistanceValueSource;->access$1(Lorg/apache/lucene/spatial/util/ShapeFieldCacheDistanceValueSource;)Lcom/spatial4j/core/shape/Point;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/spatial/util/ShapeFieldCacheDistanceValueSource$1;->from:Lcom/spatial4j/core/shape/Point;

    .line 62
    # getter for: Lorg/apache/lucene/spatial/util/ShapeFieldCacheDistanceValueSource;->ctx:Lcom/spatial4j/core/context/SpatialContext;
    invoke-static {p1}, Lorg/apache/lucene/spatial/util/ShapeFieldCacheDistanceValueSource;->access$2(Lorg/apache/lucene/spatial/util/ShapeFieldCacheDistanceValueSource;)Lcom/spatial4j/core/context/SpatialContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/spatial4j/core/context/SpatialContext;->getDistCalc()Lcom/spatial4j/core/distance/DistanceCalculator;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/spatial/util/ShapeFieldCacheDistanceValueSource$1;->calculator:Lcom/spatial4j/core/distance/DistanceCalculator;

    .line 63
    # getter for: Lorg/apache/lucene/spatial/util/ShapeFieldCacheDistanceValueSource;->ctx:Lcom/spatial4j/core/context/SpatialContext;
    invoke-static {p1}, Lorg/apache/lucene/spatial/util/ShapeFieldCacheDistanceValueSource;->access$2(Lorg/apache/lucene/spatial/util/ShapeFieldCacheDistanceValueSource;)Lcom/spatial4j/core/context/SpatialContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/spatial4j/core/context/SpatialContext;->isGeo()Z

    move-result v0

    if-eqz v0, :cond_0

    const-wide v0, 0x4066800000000000L    # 180.0

    :goto_0
    iput-wide v0, p0, Lorg/apache/lucene/spatial/util/ShapeFieldCacheDistanceValueSource$1;->nullValue:D

    return-void

    :cond_0
    const-wide v0, 0x7fefffffffffffffL    # Double.MAX_VALUE

    goto :goto_0
.end method


# virtual methods
.method public doubleVal(I)D
    .locals 7
    .param p1, "doc"    # I

    .prologue
    .line 72
    iget-object v4, p0, Lorg/apache/lucene/spatial/util/ShapeFieldCacheDistanceValueSource$1;->cache:Lorg/apache/lucene/spatial/util/ShapeFieldCache;

    invoke-virtual {v4, p1}, Lorg/apache/lucene/spatial/util/ShapeFieldCache;->getShapes(I)Ljava/util/List;

    move-result-object v1

    .line 73
    .local v1, "vals":Ljava/util/List;, "Ljava/util/List<Lcom/spatial4j/core/shape/Point;>;"
    if-eqz v1, :cond_1

    .line 74
    iget-object v5, p0, Lorg/apache/lucene/spatial/util/ShapeFieldCacheDistanceValueSource$1;->calculator:Lcom/spatial4j/core/distance/DistanceCalculator;

    iget-object v6, p0, Lorg/apache/lucene/spatial/util/ShapeFieldCacheDistanceValueSource$1;->from:Lcom/spatial4j/core/shape/Point;

    const/4 v4, 0x0

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/spatial4j/core/shape/Point;

    invoke-interface {v5, v6, v4}, Lcom/spatial4j/core/distance/DistanceCalculator;->distance(Lcom/spatial4j/core/shape/Point;Lcom/spatial4j/core/shape/Point;)D

    move-result-wide v2

    .line 75
    .local v2, "v":D
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    if-lt v0, v4, :cond_0

    .line 80
    .end local v0    # "i":I
    .end local v2    # "v":D
    :goto_1
    return-wide v2

    .line 76
    .restart local v0    # "i":I
    .restart local v2    # "v":D
    :cond_0
    iget-object v5, p0, Lorg/apache/lucene/spatial/util/ShapeFieldCacheDistanceValueSource$1;->calculator:Lcom/spatial4j/core/distance/DistanceCalculator;

    iget-object v6, p0, Lorg/apache/lucene/spatial/util/ShapeFieldCacheDistanceValueSource$1;->from:Lcom/spatial4j/core/shape/Point;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/spatial4j/core/shape/Point;

    invoke-interface {v5, v6, v4}, Lcom/spatial4j/core/distance/DistanceCalculator;->distance(Lcom/spatial4j/core/shape/Point;Lcom/spatial4j/core/shape/Point;)D

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(DD)D

    move-result-wide v2

    .line 75
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 80
    .end local v0    # "i":I
    .end local v2    # "v":D
    :cond_1
    iget-wide v2, p0, Lorg/apache/lucene/spatial/util/ShapeFieldCacheDistanceValueSource$1;->nullValue:D

    goto :goto_1
.end method

.method public floatVal(I)F
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 67
    invoke-virtual {p0, p1}, Lorg/apache/lucene/spatial/util/ShapeFieldCacheDistanceValueSource$1;->doubleVal(I)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method public toString(I)Ljava/lang/String;
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 85
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/lucene/spatial/util/ShapeFieldCacheDistanceValueSource$1;->this$0:Lorg/apache/lucene/spatial/util/ShapeFieldCacheDistanceValueSource;

    invoke-virtual {v1}, Lorg/apache/lucene/spatial/util/ShapeFieldCacheDistanceValueSource;->description()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, p1}, Lorg/apache/lucene/spatial/util/ShapeFieldCacheDistanceValueSource$1;->floatVal(I)F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
