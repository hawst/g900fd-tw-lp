.class public abstract Lorg/apache/lucene/spatial/query/SpatialOperation;
.super Ljava/lang/Object;
.source "SpatialOperation.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final BBoxIntersects:Lorg/apache/lucene/spatial/query/SpatialOperation;

.field public static final BBoxWithin:Lorg/apache/lucene/spatial/query/SpatialOperation;

.field public static final Contains:Lorg/apache/lucene/spatial/query/SpatialOperation;

.field public static final Intersects:Lorg/apache/lucene/spatial/query/SpatialOperation;

.field public static final IsDisjointTo:Lorg/apache/lucene/spatial/query/SpatialOperation;

.field public static final IsEqualTo:Lorg/apache/lucene/spatial/query/SpatialOperation;

.field public static final IsWithin:Lorg/apache/lucene/spatial/query/SpatialOperation;

.field public static final Overlaps:Lorg/apache/lucene/spatial/query/SpatialOperation;

.field private static final list:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/spatial/query/SpatialOperation;",
            ">;"
        }
    .end annotation
.end field

.field private static final registry:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/spatial/query/SpatialOperation;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final name:Ljava/lang/String;

.field private final scoreIsMeaningful:Z

.field private final sourceNeedsArea:Z

.field private final targetNeedsArea:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 43
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lorg/apache/lucene/spatial/query/SpatialOperation;->registry:Ljava/util/Map;

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lorg/apache/lucene/spatial/query/SpatialOperation;->list:Ljava/util/List;

    .line 49
    new-instance v0, Lorg/apache/lucene/spatial/query/SpatialOperation$1;

    const-string v1, "BBoxIntersects"

    invoke-direct {v0, v1, v3, v2, v2}, Lorg/apache/lucene/spatial/query/SpatialOperation$1;-><init>(Ljava/lang/String;ZZZ)V

    sput-object v0, Lorg/apache/lucene/spatial/query/SpatialOperation;->BBoxIntersects:Lorg/apache/lucene/spatial/query/SpatialOperation;

    .line 56
    new-instance v0, Lorg/apache/lucene/spatial/query/SpatialOperation$2;

    const-string v1, "BBoxWithin"

    invoke-direct {v0, v1, v3, v2, v2}, Lorg/apache/lucene/spatial/query/SpatialOperation$2;-><init>(Ljava/lang/String;ZZZ)V

    sput-object v0, Lorg/apache/lucene/spatial/query/SpatialOperation;->BBoxWithin:Lorg/apache/lucene/spatial/query/SpatialOperation;

    .line 63
    new-instance v0, Lorg/apache/lucene/spatial/query/SpatialOperation$3;

    const-string v1, "Contains"

    invoke-direct {v0, v1, v3, v3, v2}, Lorg/apache/lucene/spatial/query/SpatialOperation$3;-><init>(Ljava/lang/String;ZZZ)V

    sput-object v0, Lorg/apache/lucene/spatial/query/SpatialOperation;->Contains:Lorg/apache/lucene/spatial/query/SpatialOperation;

    .line 69
    new-instance v0, Lorg/apache/lucene/spatial/query/SpatialOperation$4;

    const-string v1, "Intersects"

    invoke-direct {v0, v1, v3, v2, v2}, Lorg/apache/lucene/spatial/query/SpatialOperation$4;-><init>(Ljava/lang/String;ZZZ)V

    sput-object v0, Lorg/apache/lucene/spatial/query/SpatialOperation;->Intersects:Lorg/apache/lucene/spatial/query/SpatialOperation;

    .line 75
    new-instance v0, Lorg/apache/lucene/spatial/query/SpatialOperation$5;

    const-string v1, "IsEqualTo"

    invoke-direct {v0, v1, v2, v2, v2}, Lorg/apache/lucene/spatial/query/SpatialOperation$5;-><init>(Ljava/lang/String;ZZZ)V

    sput-object v0, Lorg/apache/lucene/spatial/query/SpatialOperation;->IsEqualTo:Lorg/apache/lucene/spatial/query/SpatialOperation;

    .line 81
    new-instance v0, Lorg/apache/lucene/spatial/query/SpatialOperation$6;

    const-string v1, "IsDisjointTo"

    invoke-direct {v0, v1, v2, v2, v2}, Lorg/apache/lucene/spatial/query/SpatialOperation$6;-><init>(Ljava/lang/String;ZZZ)V

    sput-object v0, Lorg/apache/lucene/spatial/query/SpatialOperation;->IsDisjointTo:Lorg/apache/lucene/spatial/query/SpatialOperation;

    .line 87
    new-instance v0, Lorg/apache/lucene/spatial/query/SpatialOperation$7;

    const-string v1, "IsWithin"

    invoke-direct {v0, v1, v3, v2, v3}, Lorg/apache/lucene/spatial/query/SpatialOperation$7;-><init>(Ljava/lang/String;ZZZ)V

    sput-object v0, Lorg/apache/lucene/spatial/query/SpatialOperation;->IsWithin:Lorg/apache/lucene/spatial/query/SpatialOperation;

    .line 93
    new-instance v0, Lorg/apache/lucene/spatial/query/SpatialOperation$8;

    const-string v1, "Overlaps"

    invoke-direct {v0, v1, v3, v2, v3}, Lorg/apache/lucene/spatial/query/SpatialOperation$8;-><init>(Ljava/lang/String;ZZZ)V

    sput-object v0, Lorg/apache/lucene/spatial/query/SpatialOperation;->Overlaps:Lorg/apache/lucene/spatial/query/SpatialOperation;

    .line 98
    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;ZZZ)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "scoreIsMeaningful"    # Z
    .param p3, "sourceNeedsArea"    # Z
    .param p4, "targetNeedsArea"    # Z

    .prologue
    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107
    iput-object p1, p0, Lorg/apache/lucene/spatial/query/SpatialOperation;->name:Ljava/lang/String;

    .line 108
    iput-boolean p2, p0, Lorg/apache/lucene/spatial/query/SpatialOperation;->scoreIsMeaningful:Z

    .line 109
    iput-boolean p3, p0, Lorg/apache/lucene/spatial/query/SpatialOperation;->sourceNeedsArea:Z

    .line 110
    iput-boolean p4, p0, Lorg/apache/lucene/spatial/query/SpatialOperation;->targetNeedsArea:Z

    .line 111
    sget-object v0, Lorg/apache/lucene/spatial/query/SpatialOperation;->registry:Ljava/util/Map;

    invoke-interface {v0, p1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    sget-object v0, Lorg/apache/lucene/spatial/query/SpatialOperation;->registry:Ljava/util/Map;

    sget-object v1, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-virtual {p1, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    sget-object v0, Lorg/apache/lucene/spatial/query/SpatialOperation;->list:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 114
    return-void
.end method

.method public static get(Ljava/lang/String;)Lorg/apache/lucene/spatial/query/SpatialOperation;
    .locals 4
    .param p0, "v"    # Ljava/lang/String;

    .prologue
    .line 117
    sget-object v1, Lorg/apache/lucene/spatial/query/SpatialOperation;->registry:Ljava/util/Map;

    invoke-interface {v1, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/spatial/query/SpatialOperation;

    .line 118
    .local v0, "op":Lorg/apache/lucene/spatial/query/SpatialOperation;
    if-nez v0, :cond_0

    .line 119
    sget-object v1, Lorg/apache/lucene/spatial/query/SpatialOperation;->registry:Ljava/util/Map;

    sget-object v2, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-virtual {p0, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "op":Lorg/apache/lucene/spatial/query/SpatialOperation;
    check-cast v0, Lorg/apache/lucene/spatial/query/SpatialOperation;

    .line 121
    .restart local v0    # "op":Lorg/apache/lucene/spatial/query/SpatialOperation;
    :cond_0
    if-nez v0, :cond_1

    .line 122
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown Operation: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 124
    :cond_1
    return-object v0
.end method

.method public static varargs is(Lorg/apache/lucene/spatial/query/SpatialOperation;[Lorg/apache/lucene/spatial/query/SpatialOperation;)Z
    .locals 4
    .param p0, "op"    # Lorg/apache/lucene/spatial/query/SpatialOperation;
    .param p1, "tst"    # [Lorg/apache/lucene/spatial/query/SpatialOperation;

    .prologue
    const/4 v1, 0x0

    .line 132
    array-length v3, p1

    move v2, v1

    :goto_0
    if-lt v2, v3, :cond_0

    .line 137
    :goto_1
    return v1

    .line 132
    :cond_0
    aget-object v0, p1, v2

    .line 133
    .local v0, "t":Lorg/apache/lucene/spatial/query/SpatialOperation;
    if-ne p0, v0, :cond_1

    .line 134
    const/4 v1, 0x1

    goto :goto_1

    .line 132
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static values()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/spatial/query/SpatialOperation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 128
    sget-object v0, Lorg/apache/lucene/spatial/query/SpatialOperation;->list:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public abstract evaluate(Lcom/spatial4j/core/shape/Shape;Lcom/spatial4j/core/shape/Shape;)Z
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lorg/apache/lucene/spatial/query/SpatialOperation;->name:Ljava/lang/String;

    return-object v0
.end method

.method public isScoreIsMeaningful()Z
    .locals 1

    .prologue
    .line 149
    iget-boolean v0, p0, Lorg/apache/lucene/spatial/query/SpatialOperation;->scoreIsMeaningful:Z

    return v0
.end method

.method public isSourceNeedsArea()Z
    .locals 1

    .prologue
    .line 153
    iget-boolean v0, p0, Lorg/apache/lucene/spatial/query/SpatialOperation;->sourceNeedsArea:Z

    return v0
.end method

.method public isTargetNeedsArea()Z
    .locals 1

    .prologue
    .line 157
    iget-boolean v0, p0, Lorg/apache/lucene/spatial/query/SpatialOperation;->targetNeedsArea:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lorg/apache/lucene/spatial/query/SpatialOperation;->name:Ljava/lang/String;

    return-object v0
.end method
