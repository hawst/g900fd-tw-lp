.class Lorg/apache/lucene/spatial/prefix/tree/GeohashPrefixTree$GhCell;
.super Lorg/apache/lucene/spatial/prefix/tree/Cell;
.source "GeohashPrefixTree.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/spatial/prefix/tree/GeohashPrefixTree;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "GhCell"
.end annotation


# instance fields
.field private shape:Lcom/spatial4j/core/shape/Shape;

.field final synthetic this$0:Lorg/apache/lucene/spatial/prefix/tree/GeohashPrefixTree;


# direct methods
.method constructor <init>(Lorg/apache/lucene/spatial/prefix/tree/GeohashPrefixTree;Ljava/lang/String;)V
    .locals 0
    .param p2, "token"    # Ljava/lang/String;

    .prologue
    .line 97
    iput-object p1, p0, Lorg/apache/lucene/spatial/prefix/tree/GeohashPrefixTree$GhCell;->this$0:Lorg/apache/lucene/spatial/prefix/tree/GeohashPrefixTree;

    .line 98
    invoke-direct {p0, p2}, Lorg/apache/lucene/spatial/prefix/tree/Cell;-><init>(Ljava/lang/String;)V

    .line 99
    return-void
.end method

.method constructor <init>(Lorg/apache/lucene/spatial/prefix/tree/GeohashPrefixTree;[BII)V
    .locals 0
    .param p2, "bytes"    # [B
    .param p3, "off"    # I
    .param p4, "len"    # I

    .prologue
    .line 101
    iput-object p1, p0, Lorg/apache/lucene/spatial/prefix/tree/GeohashPrefixTree$GhCell;->this$0:Lorg/apache/lucene/spatial/prefix/tree/GeohashPrefixTree;

    .line 102
    invoke-direct {p0, p2, p3, p4}, Lorg/apache/lucene/spatial/prefix/tree/Cell;-><init>([BII)V

    .line 103
    return-void
.end method

.method private getGeohash()Ljava/lang/String;
    .locals 1

    .prologue
    .line 147
    invoke-virtual {p0}, Lorg/apache/lucene/spatial/prefix/tree/GeohashPrefixTree$GhCell;->getTokenString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getCenter()Lcom/spatial4j/core/shape/Point;
    .locals 2

    .prologue
    .line 143
    invoke-direct {p0}, Lorg/apache/lucene/spatial/prefix/tree/GeohashPrefixTree$GhCell;->getGeohash()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/spatial/prefix/tree/GeohashPrefixTree$GhCell;->this$0:Lorg/apache/lucene/spatial/prefix/tree/GeohashPrefixTree;

    iget-object v1, v1, Lorg/apache/lucene/spatial/prefix/tree/GeohashPrefixTree;->ctx:Lcom/spatial4j/core/context/SpatialContext;

    invoke-static {v0, v1}, Lcom/spatial4j/core/io/GeohashUtils;->decode(Ljava/lang/String;Lcom/spatial4j/core/context/SpatialContext;)Lcom/spatial4j/core/shape/Point;

    move-result-object v0

    return-object v0
.end method

.method public getShape()Lcom/spatial4j/core/shape/Shape;
    .locals 2

    .prologue
    .line 135
    iget-object v0, p0, Lorg/apache/lucene/spatial/prefix/tree/GeohashPrefixTree$GhCell;->shape:Lcom/spatial4j/core/shape/Shape;

    if-nez v0, :cond_0

    .line 136
    invoke-direct {p0}, Lorg/apache/lucene/spatial/prefix/tree/GeohashPrefixTree$GhCell;->getGeohash()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/spatial/prefix/tree/GeohashPrefixTree$GhCell;->this$0:Lorg/apache/lucene/spatial/prefix/tree/GeohashPrefixTree;

    iget-object v1, v1, Lorg/apache/lucene/spatial/prefix/tree/GeohashPrefixTree;->ctx:Lcom/spatial4j/core/context/SpatialContext;

    invoke-static {v0, v1}, Lcom/spatial4j/core/io/GeohashUtils;->decodeBoundary(Ljava/lang/String;Lcom/spatial4j/core/context/SpatialContext;)Lcom/spatial4j/core/shape/Rectangle;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/spatial/prefix/tree/GeohashPrefixTree$GhCell;->shape:Lcom/spatial4j/core/shape/Shape;

    .line 138
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/spatial/prefix/tree/GeohashPrefixTree$GhCell;->shape:Lcom/spatial4j/core/shape/Shape;

    return-object v0
.end method

.method public getSubCell(Lcom/spatial4j/core/shape/Point;)Lorg/apache/lucene/spatial/prefix/tree/Cell;
    .locals 2
    .param p1, "p"    # Lcom/spatial4j/core/shape/Point;

    .prologue
    .line 128
    iget-object v0, p0, Lorg/apache/lucene/spatial/prefix/tree/GeohashPrefixTree$GhCell;->this$0:Lorg/apache/lucene/spatial/prefix/tree/GeohashPrefixTree;

    invoke-virtual {p0}, Lorg/apache/lucene/spatial/prefix/tree/GeohashPrefixTree$GhCell;->getLevel()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, p1, v1}, Lorg/apache/lucene/spatial/prefix/tree/GeohashPrefixTree;->getCell(Lcom/spatial4j/core/shape/Point;I)Lorg/apache/lucene/spatial/prefix/tree/Cell;

    move-result-object v0

    return-object v0
.end method

.method public getSubCells()Ljava/util/Collection;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/spatial/prefix/tree/Cell;",
            ">;"
        }
    .end annotation

    .prologue
    .line 113
    invoke-direct {p0}, Lorg/apache/lucene/spatial/prefix/tree/GeohashPrefixTree$GhCell;->getGeohash()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/spatial4j/core/io/GeohashUtils;->getSubGeohashes(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 114
    .local v2, "hashes":[Ljava/lang/String;
    new-instance v0, Ljava/util/ArrayList;

    array-length v3, v2

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 115
    .local v0, "cells":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/spatial/prefix/tree/Cell;>;"
    array-length v4, v2

    const/4 v3, 0x0

    :goto_0
    if-lt v3, v4, :cond_0

    .line 118
    return-object v0

    .line 115
    :cond_0
    aget-object v1, v2, v3

    .line 116
    .local v1, "hash":Ljava/lang/String;
    new-instance v5, Lorg/apache/lucene/spatial/prefix/tree/GeohashPrefixTree$GhCell;

    iget-object v6, p0, Lorg/apache/lucene/spatial/prefix/tree/GeohashPrefixTree$GhCell;->this$0:Lorg/apache/lucene/spatial/prefix/tree/GeohashPrefixTree;

    invoke-direct {v5, v6, v1}, Lorg/apache/lucene/spatial/prefix/tree/GeohashPrefixTree$GhCell;-><init>(Lorg/apache/lucene/spatial/prefix/tree/GeohashPrefixTree;Ljava/lang/String;)V

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 115
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public getSubCellsSize()I
    .locals 1

    .prologue
    .line 123
    const/16 v0, 0x20

    return v0
.end method

.method public reset([BII)V
    .locals 1
    .param p1, "bytes"    # [B
    .param p2, "off"    # I
    .param p3, "len"    # I

    .prologue
    .line 107
    invoke-super {p0, p1, p2, p3}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->reset([BII)V

    .line 108
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/spatial/prefix/tree/GeohashPrefixTree$GhCell;->shape:Lcom/spatial4j/core/shape/Shape;

    .line 109
    return-void
.end method
