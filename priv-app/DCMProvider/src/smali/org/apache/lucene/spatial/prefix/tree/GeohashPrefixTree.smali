.class public Lorg/apache/lucene/spatial/prefix/tree/GeohashPrefixTree;
.super Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;
.source "GeohashPrefixTree.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/spatial/prefix/tree/GeohashPrefixTree$Factory;,
        Lorg/apache/lucene/spatial/prefix/tree/GeohashPrefixTree$GhCell;
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/spatial4j/core/context/SpatialContext;I)V
    .locals 6
    .param p1, "ctx"    # Lcom/spatial4j/core/context/SpatialContext;
    .param p2, "maxLevels"    # I

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;-><init>(Lcom/spatial4j/core/context/SpatialContext;I)V

    .line 60
    invoke-virtual {p1}, Lcom/spatial4j/core/context/SpatialContext;->getWorldBounds()Lcom/spatial4j/core/shape/Rectangle;

    move-result-object v1

    .line 61
    .local v1, "bounds":Lcom/spatial4j/core/shape/Rectangle;
    invoke-interface {v1}, Lcom/spatial4j/core/shape/Rectangle;->getMinX()D

    move-result-wide v2

    const-wide v4, -0x3f99800000000000L    # -180.0

    cmpl-double v2, v2, v4

    if-eqz v2, :cond_0

    .line 62
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Geohash only supports lat-lon world bounds. Got "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 63
    :cond_0
    invoke-static {}, Lorg/apache/lucene/spatial/prefix/tree/GeohashPrefixTree;->getMaxLevelsPossible()I

    move-result v0

    .line 64
    .local v0, "MAXP":I
    if-lez p2, :cond_1

    if-le p2, v0, :cond_2

    .line 65
    :cond_1
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "maxLen must be [1-"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] but got "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 66
    :cond_2
    return-void
.end method

.method public static getMaxLevelsPossible()I
    .locals 1

    .prologue
    .line 70
    const/16 v0, 0x18

    return v0
.end method


# virtual methods
.method public getCell(Lcom/spatial4j/core/shape/Point;I)Lorg/apache/lucene/spatial/prefix/tree/Cell;
    .locals 6
    .param p1, "p"    # Lcom/spatial4j/core/shape/Point;
    .param p2, "level"    # I

    .prologue
    .line 83
    new-instance v0, Lorg/apache/lucene/spatial/prefix/tree/GeohashPrefixTree$GhCell;

    invoke-interface {p1}, Lcom/spatial4j/core/shape/Point;->getY()D

    move-result-wide v2

    invoke-interface {p1}, Lcom/spatial4j/core/shape/Point;->getX()D

    move-result-wide v4

    invoke-static {v2, v3, v4, v5, p2}, Lcom/spatial4j/core/io/GeohashUtils;->encodeLatLon(DDI)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lorg/apache/lucene/spatial/prefix/tree/GeohashPrefixTree$GhCell;-><init>(Lorg/apache/lucene/spatial/prefix/tree/GeohashPrefixTree;Ljava/lang/String;)V

    return-object v0
.end method

.method public getCell(Ljava/lang/String;)Lorg/apache/lucene/spatial/prefix/tree/Cell;
    .locals 1
    .param p1, "token"    # Ljava/lang/String;

    .prologue
    .line 88
    new-instance v0, Lorg/apache/lucene/spatial/prefix/tree/GeohashPrefixTree$GhCell;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/spatial/prefix/tree/GeohashPrefixTree$GhCell;-><init>(Lorg/apache/lucene/spatial/prefix/tree/GeohashPrefixTree;Ljava/lang/String;)V

    return-object v0
.end method

.method public getCell([BII)Lorg/apache/lucene/spatial/prefix/tree/Cell;
    .locals 1
    .param p1, "bytes"    # [B
    .param p2, "offset"    # I
    .param p3, "len"    # I

    .prologue
    .line 93
    new-instance v0, Lorg/apache/lucene/spatial/prefix/tree/GeohashPrefixTree$GhCell;

    invoke-direct {v0, p0, p1, p2, p3}, Lorg/apache/lucene/spatial/prefix/tree/GeohashPrefixTree$GhCell;-><init>(Lorg/apache/lucene/spatial/prefix/tree/GeohashPrefixTree;[BII)V

    return-object v0
.end method

.method public getLevelForDistance(D)I
    .locals 5
    .param p1, "dist"    # D

    .prologue
    .line 75
    const-wide/16 v2, 0x0

    cmpl-double v1, p1, v2

    if-nez v1, :cond_0

    .line 76
    iget v1, p0, Lorg/apache/lucene/spatial/prefix/tree/GeohashPrefixTree;->maxLevels:I

    .line 78
    :goto_0
    return v1

    .line 77
    :cond_0
    invoke-static {p1, p2, p1, p2}, Lcom/spatial4j/core/io/GeohashUtils;->lookupHashLenForWidthHeight(DD)I

    move-result v0

    .line 78
    .local v0, "level":I
    iget v1, p0, Lorg/apache/lucene/spatial/prefix/tree/GeohashPrefixTree;->maxLevels:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    const/4 v2, 0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    goto :goto_0
.end method
