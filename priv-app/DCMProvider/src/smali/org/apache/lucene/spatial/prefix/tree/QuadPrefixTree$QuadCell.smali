.class Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree$QuadCell;
.super Lorg/apache/lucene/spatial/prefix/tree/Cell;
.source "QuadPrefixTree.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "QuadCell"
.end annotation


# instance fields
.field private shape:Lcom/spatial4j/core/shape/Shape;

.field final synthetic this$0:Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;Ljava/lang/String;)V
    .locals 0
    .param p2, "token"    # Ljava/lang/String;

    .prologue
    .line 222
    iput-object p1, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree$QuadCell;->this$0:Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;

    .line 223
    invoke-direct {p0, p2}, Lorg/apache/lucene/spatial/prefix/tree/Cell;-><init>(Ljava/lang/String;)V

    .line 224
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;Ljava/lang/String;Lcom/spatial4j/core/shape/SpatialRelation;)V
    .locals 0
    .param p2, "token"    # Ljava/lang/String;
    .param p3, "shapeRel"    # Lcom/spatial4j/core/shape/SpatialRelation;

    .prologue
    .line 226
    iput-object p1, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree$QuadCell;->this$0:Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;

    .line 227
    invoke-direct {p0, p2}, Lorg/apache/lucene/spatial/prefix/tree/Cell;-><init>(Ljava/lang/String;)V

    .line 228
    iput-object p3, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree$QuadCell;->shapeRel:Lcom/spatial4j/core/shape/SpatialRelation;

    .line 229
    return-void
.end method

.method constructor <init>(Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;[BII)V
    .locals 0
    .param p2, "bytes"    # [B
    .param p3, "off"    # I
    .param p4, "len"    # I

    .prologue
    .line 231
    iput-object p1, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree$QuadCell;->this$0:Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;

    .line 232
    invoke-direct {p0, p2, p3, p4}, Lorg/apache/lucene/spatial/prefix/tree/Cell;-><init>([BII)V

    .line 233
    return-void
.end method

.method private makeShape()Lcom/spatial4j/core/shape/Rectangle;
    .locals 20

    .prologue
    .line 271
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree$QuadCell;->getTokenString()Ljava/lang/String;

    move-result-object v16

    .line 272
    .local v16, "token":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree$QuadCell;->this$0:Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;

    # getter for: Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->xmin:D
    invoke-static {v3}, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->access$0(Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;)D

    move-result-wide v4

    .line 273
    .local v4, "xmin":D
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree$QuadCell;->this$0:Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;

    # getter for: Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->ymin:D
    invoke-static {v3}, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->access$1(Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;)D

    move-result-wide v8

    .line 275
    .local v8, "ymin":D
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_0
    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v3

    if-lt v14, v3, :cond_0

    .line 291
    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v15

    .line 293
    .local v15, "len":I
    if-lez v15, :cond_8

    .line 294
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree$QuadCell;->this$0:Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;

    iget-object v3, v3, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->levelW:[D

    add-int/lit8 v6, v15, -0x1

    aget-wide v18, v3, v6

    .line 295
    .local v18, "width":D
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree$QuadCell;->this$0:Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;

    iget-object v3, v3, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->levelH:[D

    add-int/lit8 v6, v15, -0x1

    aget-wide v12, v3, v6

    .line 300
    .local v12, "height":D
    :goto_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree$QuadCell;->this$0:Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;

    iget-object v3, v3, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->ctx:Lcom/spatial4j/core/context/SpatialContext;

    add-double v6, v4, v18

    add-double v10, v8, v12

    invoke-virtual/range {v3 .. v11}, Lcom/spatial4j/core/context/SpatialContext;->makeRectangle(DDDD)Lcom/spatial4j/core/shape/Rectangle;

    move-result-object v3

    return-object v3

    .line 276
    .end local v12    # "height":D
    .end local v15    # "len":I
    .end local v18    # "width":D
    :cond_0
    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 277
    .local v2, "c":C
    const/16 v3, 0x41

    if-eq v3, v2, :cond_1

    const/16 v3, 0x61

    if-ne v3, v2, :cond_3

    .line 278
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree$QuadCell;->this$0:Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;

    iget-object v3, v3, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->levelH:[D

    aget-wide v6, v3, v14

    add-double/2addr v8, v6

    .line 275
    :cond_2
    :goto_2
    add-int/lit8 v14, v14, 0x1

    goto :goto_0

    .line 279
    :cond_3
    const/16 v3, 0x42

    if-eq v3, v2, :cond_4

    const/16 v3, 0x62

    if-ne v3, v2, :cond_5

    .line 280
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree$QuadCell;->this$0:Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;

    iget-object v3, v3, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->levelW:[D

    aget-wide v6, v3, v14

    add-double/2addr v4, v6

    .line 281
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree$QuadCell;->this$0:Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;

    iget-object v3, v3, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->levelH:[D

    aget-wide v6, v3, v14

    add-double/2addr v8, v6

    .line 282
    goto :goto_2

    :cond_5
    const/16 v3, 0x43

    if-eq v3, v2, :cond_2

    const/16 v3, 0x63

    if-eq v3, v2, :cond_2

    .line 285
    const/16 v3, 0x44

    if-eq v3, v2, :cond_6

    const/16 v3, 0x64

    if-ne v3, v2, :cond_7

    .line 286
    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree$QuadCell;->this$0:Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;

    iget-object v3, v3, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->levelW:[D

    aget-wide v6, v3, v14

    add-double/2addr v4, v6

    .line 287
    goto :goto_2

    .line 288
    :cond_7
    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "unexpected char: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 297
    .end local v2    # "c":C
    .restart local v15    # "len":I
    :cond_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree$QuadCell;->this$0:Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;

    # getter for: Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->gridW:D
    invoke-static {v3}, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->access$2(Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;)D

    move-result-wide v18

    .line 298
    .restart local v18    # "width":D
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree$QuadCell;->this$0:Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;

    iget-wide v12, v3, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->gridH:D

    .restart local v12    # "height":D
    goto/16 :goto_1
.end method


# virtual methods
.method public getShape()Lcom/spatial4j/core/shape/Shape;
    .locals 1

    .prologue
    .line 265
    iget-object v0, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree$QuadCell;->shape:Lcom/spatial4j/core/shape/Shape;

    if-nez v0, :cond_0

    .line 266
    invoke-direct {p0}, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree$QuadCell;->makeShape()Lcom/spatial4j/core/shape/Rectangle;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree$QuadCell;->shape:Lcom/spatial4j/core/shape/Shape;

    .line 267
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree$QuadCell;->shape:Lcom/spatial4j/core/shape/Shape;

    return-object v0
.end method

.method public getSubCell(Lcom/spatial4j/core/shape/Point;)Lorg/apache/lucene/spatial/prefix/tree/Cell;
    .locals 2
    .param p1, "p"    # Lcom/spatial4j/core/shape/Point;

    .prologue
    .line 258
    iget-object v0, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree$QuadCell;->this$0:Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;

    invoke-virtual {p0}, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree$QuadCell;->getLevel()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, p1, v1}, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->getCell(Lcom/spatial4j/core/shape/Point;I)Lorg/apache/lucene/spatial/prefix/tree/Cell;

    move-result-object v0

    return-object v0
.end method

.method public getSubCells()Ljava/util/Collection;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/spatial/prefix/tree/Cell;",
            ">;"
        }
    .end annotation

    .prologue
    .line 243
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 244
    .local v0, "cells":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/spatial/prefix/tree/Cell;>;"
    new-instance v1, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree$QuadCell;

    iget-object v2, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree$QuadCell;->this$0:Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree$QuadCell;->getTokenString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "A"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree$QuadCell;-><init>(Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 245
    new-instance v1, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree$QuadCell;

    iget-object v2, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree$QuadCell;->this$0:Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree$QuadCell;->getTokenString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "B"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree$QuadCell;-><init>(Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 246
    new-instance v1, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree$QuadCell;

    iget-object v2, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree$QuadCell;->this$0:Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree$QuadCell;->getTokenString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "C"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree$QuadCell;-><init>(Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 247
    new-instance v1, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree$QuadCell;

    iget-object v2, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree$QuadCell;->this$0:Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree$QuadCell;->getTokenString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "D"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree$QuadCell;-><init>(Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 248
    return-object v0
.end method

.method public getSubCellsSize()I
    .locals 1

    .prologue
    .line 253
    const/4 v0, 0x4

    return v0
.end method

.method public reset([BII)V
    .locals 1
    .param p1, "bytes"    # [B
    .param p2, "off"    # I
    .param p3, "len"    # I

    .prologue
    .line 237
    invoke-super {p0, p1, p2, p3}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->reset([BII)V

    .line 238
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree$QuadCell;->shape:Lcom/spatial4j/core/shape/Shape;

    .line 239
    return-void
.end method
