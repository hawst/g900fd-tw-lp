.class public abstract Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter;
.super Lorg/apache/lucene/spatial/prefix/AbstractPrefixTreeFilter;
.source "AbstractVisitingPrefixTreeFilter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;,
        Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field protected final prefixGridScanLevel:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const-class v0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/spatial4j/core/shape/Shape;Ljava/lang/String;Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;II)V
    .locals 2
    .param p1, "queryShape"    # Lcom/spatial4j/core/shape/Shape;
    .param p2, "fieldName"    # Ljava/lang/String;
    .param p3, "grid"    # Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;
    .param p4, "detailLevel"    # I
    .param p5, "prefixGridScanLevel"    # I

    .prologue
    .line 54
    invoke-direct {p0, p1, p2, p3, p4}, Lorg/apache/lucene/spatial/prefix/AbstractPrefixTreeFilter;-><init>(Lcom/spatial4j/core/shape/Shape;Ljava/lang/String;Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;I)V

    .line 55
    const/4 v0, 0x0

    invoke-virtual {p3}, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;->getMaxLevels()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {p5, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter;->prefixGridScanLevel:I

    .line 56
    sget-boolean v0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p3}, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;->getMaxLevels()I

    move-result v0

    if-le p4, v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 57
    :cond_0
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 61
    invoke-super {p0, p1}, Lorg/apache/lucene/spatial/prefix/AbstractPrefixTreeFilter;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 67
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 63
    check-cast v0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter;

    .line 65
    .local v0, "that":Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter;
    iget v2, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter;->prefixGridScanLevel:I

    iget v3, v0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter;->prefixGridScanLevel:I

    if-ne v2, v3, :cond_0

    .line 67
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 72
    invoke-super {p0}, Lorg/apache/lucene/spatial/prefix/AbstractPrefixTreeFilter;->hashCode()I

    move-result v0

    .line 73
    .local v0, "result":I
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter;->prefixGridScanLevel:I

    add-int v0, v1, v2

    .line 74
    return v0
.end method
