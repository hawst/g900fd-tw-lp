.class public Lorg/apache/lucene/spatial/util/CachingDoubleValueSource;
.super Lorg/apache/lucene/queries/function/ValueSource;
.source "CachingDoubleValueSource.java"


# instance fields
.field final cache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation
.end field

.field final source:Lorg/apache/lucene/queries/function/ValueSource;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/queries/function/ValueSource;)V
    .locals 1
    .param p1, "source"    # Lorg/apache/lucene/queries/function/ValueSource;

    .prologue
    .line 38
    invoke-direct {p0}, Lorg/apache/lucene/queries/function/ValueSource;-><init>()V

    .line 40
    iput-object p1, p0, Lorg/apache/lucene/spatial/util/CachingDoubleValueSource;->source:Lorg/apache/lucene/queries/function/ValueSource;

    .line 41
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/spatial/util/CachingDoubleValueSource;->cache:Ljava/util/Map;

    .line 42
    return-void
.end method


# virtual methods
.method public description()Ljava/lang/String;
    .locals 2

    .prologue
    .line 46
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Cached["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/spatial/util/CachingDoubleValueSource;->source:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v1}, Lorg/apache/lucene/queries/function/ValueSource;->description()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 80
    if-ne p0, p1, :cond_1

    .line 87
    :cond_0
    :goto_0
    return v1

    .line 81
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 83
    check-cast v0, Lorg/apache/lucene/spatial/util/CachingDoubleValueSource;

    .line 85
    .local v0, "that":Lorg/apache/lucene/spatial/util/CachingDoubleValueSource;
    iget-object v3, p0, Lorg/apache/lucene/spatial/util/CachingDoubleValueSource;->source:Lorg/apache/lucene/queries/function/ValueSource;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lorg/apache/lucene/spatial/util/CachingDoubleValueSource;->source:Lorg/apache/lucene/queries/function/ValueSource;

    iget-object v4, v0, Lorg/apache/lucene/spatial/util/CachingDoubleValueSource;->source:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/queries/function/ValueSource;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :goto_1
    move v1, v2

    goto :goto_0

    :cond_4
    iget-object v3, v0, Lorg/apache/lucene/spatial/util/CachingDoubleValueSource;->source:Lorg/apache/lucene/queries/function/ValueSource;

    if-eqz v3, :cond_0

    goto :goto_1
.end method

.method public getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
    .locals 3
    .param p1, "context"    # Ljava/util/Map;
    .param p2, "readerContext"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 51
    iget v0, p2, Lorg/apache/lucene/index/AtomicReaderContext;->docBase:I

    .line 52
    .local v0, "base":I
    iget-object v2, p0, Lorg/apache/lucene/spatial/util/CachingDoubleValueSource;->source:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v2, p1, p2}, Lorg/apache/lucene/queries/function/ValueSource;->getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;

    move-result-object v1

    .line 53
    .local v1, "vals":Lorg/apache/lucene/queries/function/FunctionValues;
    new-instance v2, Lorg/apache/lucene/spatial/util/CachingDoubleValueSource$1;

    invoke-direct {v2, p0, v0, v1}, Lorg/apache/lucene/spatial/util/CachingDoubleValueSource$1;-><init>(Lorg/apache/lucene/spatial/util/CachingDoubleValueSource;ILorg/apache/lucene/queries/function/FunctionValues;)V

    return-object v2
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lorg/apache/lucene/spatial/util/CachingDoubleValueSource;->source:Lorg/apache/lucene/queries/function/ValueSource;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/spatial/util/CachingDoubleValueSource;->source:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v0}, Lorg/apache/lucene/queries/function/ValueSource;->hashCode()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
