.class public Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;
.super Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;
.source "QuadPrefixTree.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree$Factory;,
        Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree$QuadCell;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final DEFAULT_MAX_LEVELS:I = 0xc

.field public static final MAX_LEVELS_POSSIBLE:I = 0x32


# instance fields
.field public final gridH:D

.field private final gridW:D

.field final levelH:[D

.field final levelN:[I

.field final levelS:[I

.field final levelW:[D

.field private final xmax:D

.field private final xmid:D

.field private final xmin:D

.field private final ymax:D

.field private final ymid:D

.field private final ymin:D


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->$assertionsDisabled:Z

    .line 62
    return-void

    .line 40
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/spatial4j/core/context/SpatialContext;)V
    .locals 1
    .param p1, "ctx"    # Lcom/spatial4j/core/context/SpatialContext;

    .prologue
    .line 109
    const/16 v0, 0xc

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;-><init>(Lcom/spatial4j/core/context/SpatialContext;I)V

    .line 110
    return-void
.end method

.method public constructor <init>(Lcom/spatial4j/core/context/SpatialContext;I)V
    .locals 1
    .param p1, "ctx"    # Lcom/spatial4j/core/context/SpatialContext;
    .param p2, "maxLevels"    # I

    .prologue
    .line 114
    invoke-virtual {p1}, Lcom/spatial4j/core/context/SpatialContext;->getWorldBounds()Lcom/spatial4j/core/shape/Rectangle;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;-><init>(Lcom/spatial4j/core/context/SpatialContext;Lcom/spatial4j/core/shape/Rectangle;I)V

    .line 115
    return-void
.end method

.method public constructor <init>(Lcom/spatial4j/core/context/SpatialContext;Lcom/spatial4j/core/shape/Rectangle;I)V
    .locals 9
    .param p1, "ctx"    # Lcom/spatial4j/core/context/SpatialContext;
    .param p2, "bounds"    # Lcom/spatial4j/core/shape/Rectangle;
    .param p3, "maxLevels"    # I

    .prologue
    const/4 v8, 0x0

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    .line 80
    invoke-direct {p0, p1, p3}, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;-><init>(Lcom/spatial4j/core/context/SpatialContext;I)V

    .line 81
    invoke-interface {p2}, Lcom/spatial4j/core/shape/Rectangle;->getMinX()D

    move-result-wide v2

    iput-wide v2, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->xmin:D

    .line 82
    invoke-interface {p2}, Lcom/spatial4j/core/shape/Rectangle;->getMaxX()D

    move-result-wide v2

    iput-wide v2, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->xmax:D

    .line 83
    invoke-interface {p2}, Lcom/spatial4j/core/shape/Rectangle;->getMinY()D

    move-result-wide v2

    iput-wide v2, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->ymin:D

    .line 84
    invoke-interface {p2}, Lcom/spatial4j/core/shape/Rectangle;->getMaxY()D

    move-result-wide v2

    iput-wide v2, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->ymax:D

    .line 86
    new-array v1, p3, [D

    iput-object v1, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->levelW:[D

    .line 87
    new-array v1, p3, [D

    iput-object v1, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->levelH:[D

    .line 88
    new-array v1, p3, [I

    iput-object v1, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->levelS:[I

    .line 89
    new-array v1, p3, [I

    iput-object v1, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->levelN:[I

    .line 91
    iget-wide v2, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->xmax:D

    iget-wide v4, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->xmin:D

    sub-double/2addr v2, v4

    iput-wide v2, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->gridW:D

    .line 92
    iget-wide v2, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->ymax:D

    iget-wide v4, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->ymin:D

    sub-double/2addr v2, v4

    iput-wide v2, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->gridH:D

    .line 93
    iget-wide v2, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->xmin:D

    iget-wide v4, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->gridW:D

    div-double/2addr v4, v6

    add-double/2addr v2, v4

    iput-wide v2, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->xmid:D

    .line 94
    iget-wide v2, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->ymin:D

    iget-wide v4, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->gridH:D

    div-double/2addr v4, v6

    add-double/2addr v2, v4

    iput-wide v2, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->ymid:D

    .line 95
    iget-object v1, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->levelW:[D

    iget-wide v2, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->gridW:D

    div-double/2addr v2, v6

    aput-wide v2, v1, v8

    .line 96
    iget-object v1, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->levelH:[D

    iget-wide v2, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->gridH:D

    div-double/2addr v2, v6

    aput-wide v2, v1, v8

    .line 97
    iget-object v1, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->levelS:[I

    const/4 v2, 0x2

    aput v2, v1, v8

    .line 98
    iget-object v1, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->levelN:[I

    const/4 v2, 0x4

    aput v2, v1, v8

    .line 100
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->levelW:[D

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 106
    return-void

    .line 101
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->levelW:[D

    iget-object v2, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->levelW:[D

    add-int/lit8 v3, v0, -0x1

    aget-wide v2, v2, v3

    div-double/2addr v2, v6

    aput-wide v2, v1, v0

    .line 102
    iget-object v1, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->levelH:[D

    iget-object v2, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->levelH:[D

    add-int/lit8 v3, v0, -0x1

    aget-wide v2, v2, v3

    div-double/2addr v2, v6

    aput-wide v2, v1, v0

    .line 103
    iget-object v1, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->levelS:[I

    iget-object v2, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->levelS:[I

    add-int/lit8 v3, v0, -0x1

    aget v2, v2, v3

    mul-int/lit8 v2, v2, 0x2

    aput v2, v1, v0

    .line 104
    iget-object v1, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->levelN:[I

    iget-object v2, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->levelN:[I

    add-int/lit8 v3, v0, -0x1

    aget v2, v2, v3

    mul-int/lit8 v2, v2, 0x4

    aput v2, v1, v0

    .line 100
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method static synthetic access$0(Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;)D
    .locals 2

    .prologue
    .line 63
    iget-wide v0, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->xmin:D

    return-wide v0
.end method

.method static synthetic access$1(Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;)D
    .locals 2

    .prologue
    .line 65
    iget-wide v0, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->ymin:D

    return-wide v0
.end method

.method static synthetic access$2(Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;)D
    .locals 2

    .prologue
    .line 70
    iget-wide v0, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->gridW:D

    return-wide v0
.end method

.method private build(DDILjava/util/List;Ljava/lang/StringBuilder;Lcom/spatial4j/core/shape/Shape;I)V
    .locals 19
    .param p1, "x"    # D
    .param p3, "y"    # D
    .param p5, "level"    # I
    .param p7, "str"    # Ljava/lang/StringBuilder;
    .param p8, "shape"    # Lcom/spatial4j/core/shape/Shape;
    .param p9, "maxLevel"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(DDI",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/spatial/prefix/tree/Cell;",
            ">;",
            "Ljava/lang/StringBuilder;",
            "Lcom/spatial4j/core/shape/Shape;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 167
    .local p6, "matches":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/spatial/prefix/tree/Cell;>;"
    sget-boolean v2, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    invoke-virtual/range {p7 .. p7}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    move/from16 v0, p5

    if-eq v2, v0, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 168
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->levelW:[D

    aget-wide v2, v2, p5

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    div-double v16, v2, v4

    .line 169
    .local v16, "w":D
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->levelH:[D

    aget-wide v2, v2, p5

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    div-double v14, v2, v4

    .line 173
    .local v14, "h":D
    const/16 v3, 0x41

    sub-double v4, p1, v16

    add-double v6, p3, v14

    move-object/from16 v2, p0

    move/from16 v8, p5

    move-object/from16 v9, p6

    move-object/from16 v10, p7

    move-object/from16 v11, p8

    move/from16 v12, p9

    invoke-direct/range {v2 .. v12}, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->checkBattenberg(CDDILjava/util/List;Ljava/lang/StringBuilder;Lcom/spatial4j/core/shape/Shape;I)V

    .line 174
    const/16 v3, 0x42

    add-double v4, p1, v16

    add-double v6, p3, v14

    move-object/from16 v2, p0

    move/from16 v8, p5

    move-object/from16 v9, p6

    move-object/from16 v10, p7

    move-object/from16 v11, p8

    move/from16 v12, p9

    invoke-direct/range {v2 .. v12}, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->checkBattenberg(CDDILjava/util/List;Ljava/lang/StringBuilder;Lcom/spatial4j/core/shape/Shape;I)V

    .line 175
    const/16 v3, 0x43

    sub-double v4, p1, v16

    sub-double v6, p3, v14

    move-object/from16 v2, p0

    move/from16 v8, p5

    move-object/from16 v9, p6

    move-object/from16 v10, p7

    move-object/from16 v11, p8

    move/from16 v12, p9

    invoke-direct/range {v2 .. v12}, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->checkBattenberg(CDDILjava/util/List;Ljava/lang/StringBuilder;Lcom/spatial4j/core/shape/Shape;I)V

    .line 176
    const/16 v3, 0x44

    add-double v4, p1, v16

    sub-double v6, p3, v14

    move-object/from16 v2, p0

    move/from16 v8, p5

    move-object/from16 v9, p6

    move-object/from16 v10, p7

    move-object/from16 v11, p8

    move/from16 v12, p9

    invoke-direct/range {v2 .. v12}, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->checkBattenberg(CDDILjava/util/List;Ljava/lang/StringBuilder;Lcom/spatial4j/core/shape/Shape;I)V

    .line 182
    return-void
.end method

.method private checkBattenberg(CDDILjava/util/List;Ljava/lang/StringBuilder;Lcom/spatial4j/core/shape/Shape;I)V
    .locals 20
    .param p1, "c"    # C
    .param p2, "cx"    # D
    .param p4, "cy"    # D
    .param p6, "level"    # I
    .param p8, "str"    # Ljava/lang/StringBuilder;
    .param p9, "shape"    # Lcom/spatial4j/core/shape/Shape;
    .param p10, "maxLevel"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(CDDI",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/spatial/prefix/tree/Cell;",
            ">;",
            "Ljava/lang/StringBuilder;",
            "Lcom/spatial4j/core/shape/Shape;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 193
    .local p7, "matches":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/spatial/prefix/tree/Cell;>;"
    sget-boolean v3, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    invoke-virtual/range {p8 .. p8}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    move/from16 v0, p6

    if-eq v3, v0, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 194
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->levelW:[D

    aget-wide v4, v3, p6

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    div-double v18, v4, v6

    .line 195
    .local v18, "w":D
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->levelH:[D

    aget-wide v4, v3, p6

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    div-double v14, v4, v6

    .line 197
    .local v14, "h":D
    invoke-virtual/range {p8 .. p8}, Ljava/lang/StringBuilder;->length()I

    move-result v13

    .line 198
    .local v13, "strlen":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->ctx:Lcom/spatial4j/core/context/SpatialContext;

    sub-double v4, p2, v18

    add-double v6, p2, v18

    sub-double v8, p4, v14

    add-double v10, p4, v14

    invoke-virtual/range {v3 .. v11}, Lcom/spatial4j/core/context/SpatialContext;->makeRectangle(DDDD)Lcom/spatial4j/core/shape/Rectangle;

    move-result-object v2

    .line 199
    .local v2, "rectangle":Lcom/spatial4j/core/shape/Rectangle;
    move-object/from16 v0, p9

    invoke-interface {v0, v2}, Lcom/spatial4j/core/shape/Shape;->relate(Lcom/spatial4j/core/shape/Shape;)Lcom/spatial4j/core/shape/SpatialRelation;

    move-result-object v16

    .line 200
    .local v16, "v":Lcom/spatial4j/core/shape/SpatialRelation;
    sget-object v3, Lcom/spatial4j/core/shape/SpatialRelation;->CONTAINS:Lcom/spatial4j/core/shape/SpatialRelation;

    move-object/from16 v0, v16

    if-ne v3, v0, :cond_2

    .line 201
    move-object/from16 v0, p8

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 203
    new-instance v3, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree$QuadCell;

    invoke-virtual/range {p8 .. p8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v16 .. v16}, Lcom/spatial4j/core/shape/SpatialRelation;->transpose()Lcom/spatial4j/core/shape/SpatialRelation;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v4, v5}, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree$QuadCell;-><init>(Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;Ljava/lang/String;Lcom/spatial4j/core/shape/SpatialRelation;)V

    move-object/from16 v0, p7

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 217
    :cond_1
    :goto_0
    move-object/from16 v0, p8

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 218
    return-void

    .line 204
    :cond_2
    sget-object v3, Lcom/spatial4j/core/shape/SpatialRelation;->DISJOINT:Lcom/spatial4j/core/shape/SpatialRelation;

    move-object/from16 v0, v16

    if-eq v3, v0, :cond_1

    .line 207
    move-object/from16 v0, p8

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 209
    add-int/lit8 v8, p6, 0x1

    .line 210
    .local v8, "nextLevel":I
    move/from16 v0, p10

    if-lt v8, v0, :cond_3

    .line 212
    new-instance v3, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree$QuadCell;

    invoke-virtual/range {p8 .. p8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v16 .. v16}, Lcom/spatial4j/core/shape/SpatialRelation;->transpose()Lcom/spatial4j/core/shape/SpatialRelation;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v4, v5}, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree$QuadCell;-><init>(Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;Ljava/lang/String;Lcom/spatial4j/core/shape/SpatialRelation;)V

    move-object/from16 v0, p7

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    move-object/from16 v3, p0

    move-wide/from16 v4, p2

    move-wide/from16 v6, p4

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    move-object/from16 v11, p9

    move/from16 v12, p10

    .line 214
    invoke-direct/range {v3 .. v12}, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->build(DDILjava/util/List;Ljava/lang/StringBuilder;Lcom/spatial4j/core/shape/Shape;I)V

    goto :goto_0
.end method


# virtual methods
.method public getCell(Lcom/spatial4j/core/shape/Point;I)Lorg/apache/lucene/spatial/prefix/tree/Cell;
    .locals 14
    .param p1, "p"    # Lcom/spatial4j/core/shape/Point;
    .param p2, "level"    # I

    .prologue
    .line 144
    new-instance v7, Ljava/util/ArrayList;

    const/4 v0, 0x1

    invoke-direct {v7, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 145
    .local v7, "cells":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/spatial/prefix/tree/Cell;>;"
    iget-wide v2, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->xmid:D

    iget-wide v4, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->ymid:D

    const/4 v6, 0x0

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->ctx:Lcom/spatial4j/core/context/SpatialContext;

    invoke-interface {p1}, Lcom/spatial4j/core/shape/Point;->getX()D

    move-result-wide v10

    invoke-interface {p1}, Lcom/spatial4j/core/shape/Point;->getY()D

    move-result-wide v12

    invoke-virtual {v0, v10, v11, v12, v13}, Lcom/spatial4j/core/context/SpatialContext;->makePoint(DD)Lcom/spatial4j/core/shape/Point;

    move-result-object v9

    move-object v1, p0

    move/from16 v10, p2

    invoke-direct/range {v1 .. v10}, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->build(DDILjava/util/List;Ljava/lang/StringBuilder;Lcom/spatial4j/core/shape/Shape;I)V

    .line 146
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/spatial/prefix/tree/Cell;

    return-object v0
.end method

.method public getCell(Ljava/lang/String;)Lorg/apache/lucene/spatial/prefix/tree/Cell;
    .locals 1
    .param p1, "token"    # Ljava/lang/String;

    .prologue
    .line 151
    new-instance v0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree$QuadCell;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree$QuadCell;-><init>(Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;Ljava/lang/String;)V

    return-object v0
.end method

.method public getCell([BII)Lorg/apache/lucene/spatial/prefix/tree/Cell;
    .locals 1
    .param p1, "bytes"    # [B
    .param p2, "offset"    # I
    .param p3, "len"    # I

    .prologue
    .line 156
    new-instance v0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree$QuadCell;

    invoke-direct {v0, p0, p1, p2, p3}, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree$QuadCell;-><init>(Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;[BII)V

    return-object v0
.end method

.method public getLevelForDistance(D)I
    .locals 5
    .param p1, "dist"    # D

    .prologue
    .line 131
    const-wide/16 v2, 0x0

    cmpl-double v1, p1, v2

    if-nez v1, :cond_0

    .line 132
    iget v1, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->maxLevels:I

    .line 139
    :goto_0
    return v1

    .line 133
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget v1, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->maxLevels:I

    add-int/lit8 v1, v1, -0x1

    if-lt v0, v1, :cond_1

    .line 139
    iget v1, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->maxLevels:I

    goto :goto_0

    .line 135
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->levelW:[D

    aget-wide v2, v1, v0

    cmpl-double v1, p1, v2

    if-lez v1, :cond_2

    iget-object v1, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->levelH:[D

    aget-wide v2, v1, v0

    cmpl-double v1, p1, v2

    if-lez v1, :cond_2

    .line 136
    add-int/lit8 v1, v0, 0x1

    goto :goto_0

    .line 133
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public printInfo(Ljava/io/PrintStream;)V
    .locals 6
    .param p1, "out"    # Ljava/io/PrintStream;

    .prologue
    const/4 v3, 0x5

    .line 118
    sget-object v2, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-static {v2}, Ljava/text/NumberFormat;->getNumberInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v1

    .line 119
    .local v1, "nf":Ljava/text/NumberFormat;
    invoke-virtual {v1, v3}, Ljava/text/NumberFormat;->setMaximumFractionDigits(I)V

    .line 120
    invoke-virtual {v1, v3}, Ljava/text/NumberFormat;->setMinimumFractionDigits(I)V

    .line 121
    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Ljava/text/NumberFormat;->setMinimumIntegerDigits(I)V

    .line 123
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->maxLevels:I

    if-lt v0, v2, :cond_0

    .line 127
    return-void

    .line 124
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "]\t"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->levelW:[D

    aget-wide v4, v3, v0

    invoke-virtual {v1, v4, v5}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\t"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->levelH:[D

    aget-wide v4, v3, v0

    invoke-virtual {v1, v4, v5}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\t"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 125
    iget-object v3, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->levelS:[I

    aget v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\t"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->levelS:[I

    aget v3, v3, v0

    iget-object v4, p0, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree;->levelS:[I

    aget v4, v4, v0

    mul-int/2addr v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 124
    invoke-virtual {p1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 123
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
