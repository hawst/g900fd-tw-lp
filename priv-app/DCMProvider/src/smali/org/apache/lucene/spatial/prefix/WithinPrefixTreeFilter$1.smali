.class Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter$1;
.super Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;
.source "WithinPrefixTreeFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter;->getDocIdSet(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/DocIdSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private inside:Lorg/apache/lucene/util/FixedBitSet;

.field private outside:Lorg/apache/lucene/util/FixedBitSet;

.field final synthetic this$0:Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter;

.field private visitRelation:Lcom/spatial4j/core/shape/SpatialRelation;


# direct methods
.method constructor <init>(Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter;Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter;Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;Z)V
    .locals 0
    .param p3, "$anonymous0"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p4, "$anonymous1"    # Lorg/apache/lucene/util/Bits;
    .param p5, "$anonymous2"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter$1;->this$0:Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter;

    .line 121
    invoke-direct {p0, p2, p3, p4, p5}, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;-><init>(Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter;Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;Z)V

    return-void
.end method

.method private allCellsIntersectQuery(Lorg/apache/lucene/spatial/prefix/tree/Cell;Lcom/spatial4j/core/shape/SpatialRelation;)Z
    .locals 7
    .param p1, "cell"    # Lorg/apache/lucene/spatial/prefix/tree/Cell;
    .param p2, "relate"    # Lcom/spatial4j/core/shape/SpatialRelation;

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 177
    if-nez p2, :cond_0

    .line 178
    invoke-virtual {p1}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->getShape()Lcom/spatial4j/core/shape/Shape;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter$1;->this$0:Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter;

    iget-object v5, v5, Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter;->queryShape:Lcom/spatial4j/core/shape/Shape;

    invoke-interface {v4, v5}, Lcom/spatial4j/core/shape/Shape;->relate(Lcom/spatial4j/core/shape/Shape;)Lcom/spatial4j/core/shape/SpatialRelation;

    move-result-object p2

    .line 179
    :cond_0
    invoke-virtual {p1}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->getLevel()I

    move-result v4

    iget-object v5, p0, Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter$1;->this$0:Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter;

    iget v5, v5, Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter;->detailLevel:I

    if-ne v4, v5, :cond_2

    .line 180
    invoke-virtual {p2}, Lcom/spatial4j/core/shape/SpatialRelation;->intersects()Z

    move-result v2

    .line 193
    :cond_1
    :goto_0
    return v2

    .line 181
    :cond_2
    sget-object v4, Lcom/spatial4j/core/shape/SpatialRelation;->WITHIN:Lcom/spatial4j/core/shape/SpatialRelation;

    if-eq p2, v4, :cond_1

    .line 183
    sget-object v4, Lcom/spatial4j/core/shape/SpatialRelation;->DISJOINT:Lcom/spatial4j/core/shape/SpatialRelation;

    if-ne p2, v4, :cond_3

    move v2, v3

    .line 184
    goto :goto_0

    .line 188
    :cond_3
    invoke-virtual {p1, v6}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->getSubCells(Lcom/spatial4j/core/shape/Shape;)Ljava/util/Collection;

    move-result-object v1

    .line 189
    .local v1, "subCells":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/spatial/prefix/tree/Cell;>;"
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/spatial/prefix/tree/Cell;

    .line 190
    .local v0, "subCell":Lorg/apache/lucene/spatial/prefix/tree/Cell;
    invoke-direct {p0, v0, v6}, Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter$1;->allCellsIntersectQuery(Lorg/apache/lucene/spatial/prefix/tree/Cell;Lcom/spatial4j/core/shape/SpatialRelation;)Z

    move-result v5

    if-nez v5, :cond_4

    move v2, v3

    .line 191
    goto :goto_0
.end method


# virtual methods
.method protected findSubCellsToVisit(Lorg/apache/lucene/spatial/prefix/tree/Cell;)Ljava/util/Iterator;
    .locals 1
    .param p1, "cell"    # Lorg/apache/lucene/spatial/prefix/tree/Cell;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/spatial/prefix/tree/Cell;",
            ")",
            "Ljava/util/Iterator",
            "<",
            "Lorg/apache/lucene/spatial/prefix/tree/Cell;",
            ">;"
        }
    .end annotation

    .prologue
    .line 141
    iget-object v0, p0, Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter$1;->this$0:Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter;

    # getter for: Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter;->bufferedQueryShape:Lcom/spatial4j/core/shape/Shape;
    invoke-static {v0}, Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter;->access$0(Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter;)Lcom/spatial4j/core/shape/Shape;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->getSubCells(Lcom/spatial4j/core/shape/Shape;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method protected finish()Lorg/apache/lucene/search/DocIdSet;
    .locals 2

    .prologue
    .line 134
    iget-object v0, p0, Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter$1;->inside:Lorg/apache/lucene/util/FixedBitSet;

    iget-object v1, p0, Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter$1;->outside:Lorg/apache/lucene/util/FixedBitSet;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/FixedBitSet;->andNot(Lorg/apache/lucene/util/FixedBitSet;)V

    .line 135
    iget-object v0, p0, Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter$1;->inside:Lorg/apache/lucene/util/FixedBitSet;

    return-object v0
.end method

.method protected start()V
    .locals 2

    .prologue
    .line 128
    new-instance v0, Lorg/apache/lucene/util/FixedBitSet;

    iget v1, p0, Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter$1;->maxDoc:I

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/FixedBitSet;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter$1;->inside:Lorg/apache/lucene/util/FixedBitSet;

    .line 129
    new-instance v0, Lorg/apache/lucene/util/FixedBitSet;

    iget v1, p0, Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter$1;->maxDoc:I

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/FixedBitSet;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter$1;->outside:Lorg/apache/lucene/util/FixedBitSet;

    .line 130
    return-void
.end method

.method protected visit(Lorg/apache/lucene/spatial/prefix/tree/Cell;)Z
    .locals 3
    .param p1, "cell"    # Lorg/apache/lucene/spatial/prefix/tree/Cell;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 148
    invoke-virtual {p1}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->getShape()Lcom/spatial4j/core/shape/Shape;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter$1;->this$0:Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter;

    iget-object v2, v2, Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter;->queryShape:Lcom/spatial4j/core/shape/Shape;

    invoke-interface {v1, v2}, Lcom/spatial4j/core/shape/Shape;->relate(Lcom/spatial4j/core/shape/Shape;)Lcom/spatial4j/core/shape/SpatialRelation;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter$1;->visitRelation:Lcom/spatial4j/core/shape/SpatialRelation;

    .line 149
    iget-object v1, p0, Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter$1;->visitRelation:Lcom/spatial4j/core/shape/SpatialRelation;

    sget-object v2, Lcom/spatial4j/core/shape/SpatialRelation;->WITHIN:Lcom/spatial4j/core/shape/SpatialRelation;

    if-ne v1, v2, :cond_0

    .line 150
    iget-object v1, p0, Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter$1;->inside:Lorg/apache/lucene/util/FixedBitSet;

    invoke-virtual {p0, v1}, Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter$1;->collectDocs(Lorg/apache/lucene/util/FixedBitSet;)V

    .line 159
    :goto_0
    return v0

    .line 152
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter$1;->visitRelation:Lcom/spatial4j/core/shape/SpatialRelation;

    sget-object v2, Lcom/spatial4j/core/shape/SpatialRelation;->DISJOINT:Lcom/spatial4j/core/shape/SpatialRelation;

    if-ne v1, v2, :cond_1

    .line 153
    iget-object v1, p0, Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter$1;->outside:Lorg/apache/lucene/util/FixedBitSet;

    invoke-virtual {p0, v1}, Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter$1;->collectDocs(Lorg/apache/lucene/util/FixedBitSet;)V

    goto :goto_0

    .line 155
    :cond_1
    invoke-virtual {p1}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->getLevel()I

    move-result v1

    iget-object v2, p0, Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter$1;->this$0:Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter;

    iget v2, v2, Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter;->detailLevel:I

    if-ne v1, v2, :cond_2

    .line 156
    iget-object v1, p0, Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter$1;->inside:Lorg/apache/lucene/util/FixedBitSet;

    invoke-virtual {p0, v1}, Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter$1;->collectDocs(Lorg/apache/lucene/util/FixedBitSet;)V

    goto :goto_0

    .line 159
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected visitLeaf(Lorg/apache/lucene/spatial/prefix/tree/Cell;)V
    .locals 3
    .param p1, "cell"    # Lorg/apache/lucene/spatial/prefix/tree/Cell;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 165
    sget-boolean v0, Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter$1;->this$0:Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter;

    iget v0, v0, Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter;->detailLevel:I

    invoke-virtual {p1}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->getLevel()I

    move-result v1

    if-ne v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 166
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter$1;->visitRelation:Lcom/spatial4j/core/shape/SpatialRelation;

    invoke-virtual {p1}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->getShape()Lcom/spatial4j/core/shape/Shape;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter$1;->this$0:Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter;

    iget-object v2, v2, Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter;->queryShape:Lcom/spatial4j/core/shape/Shape;

    invoke-interface {v1, v2}, Lcom/spatial4j/core/shape/Shape;->relate(Lcom/spatial4j/core/shape/Shape;)Lcom/spatial4j/core/shape/SpatialRelation;

    move-result-object v1

    if-eq v0, v1, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 167
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter$1;->visitRelation:Lcom/spatial4j/core/shape/SpatialRelation;

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter$1;->allCellsIntersectQuery(Lorg/apache/lucene/spatial/prefix/tree/Cell;Lcom/spatial4j/core/shape/SpatialRelation;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 168
    iget-object v0, p0, Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter$1;->inside:Lorg/apache/lucene/util/FixedBitSet;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter$1;->collectDocs(Lorg/apache/lucene/util/FixedBitSet;)V

    .line 171
    :goto_0
    return-void

    .line 170
    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter$1;->outside:Lorg/apache/lucene/util/FixedBitSet;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter$1;->collectDocs(Lorg/apache/lucene/util/FixedBitSet;)V

    goto :goto_0
.end method

.method protected visitScanned(Lorg/apache/lucene/spatial/prefix/tree/Cell;)V
    .locals 1
    .param p1, "cell"    # Lorg/apache/lucene/spatial/prefix/tree/Cell;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 198
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter$1;->allCellsIntersectQuery(Lorg/apache/lucene/spatial/prefix/tree/Cell;Lcom/spatial4j/core/shape/SpatialRelation;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 199
    iget-object v0, p0, Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter$1;->inside:Lorg/apache/lucene/util/FixedBitSet;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter$1;->collectDocs(Lorg/apache/lucene/util/FixedBitSet;)V

    .line 203
    :goto_0
    return-void

    .line 201
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter$1;->outside:Lorg/apache/lucene/util/FixedBitSet;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/spatial/prefix/WithinPrefixTreeFilter$1;->collectDocs(Lorg/apache/lucene/util/FixedBitSet;)V

    goto :goto_0
.end method
