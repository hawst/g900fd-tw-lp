.class Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate$VNodeCellIterator;
.super Ljava/lang/Object;
.source "AbstractVisitingPrefixTreeFilter.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "VNodeCellIterator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final cellIter:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<",
            "Lorg/apache/lucene/spatial/prefix/tree/Cell;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$1:Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;

.field private final vNode:Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 277
    const-class v0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate$VNodeCellIterator;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;Ljava/util/Iterator;Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;)V
    .locals 0
    .param p3, "vNode"    # Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator",
            "<",
            "Lorg/apache/lucene/spatial/prefix/tree/Cell;",
            ">;",
            "Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;",
            ")V"
        }
    .end annotation

    .prologue
    .line 282
    .local p2, "cellIter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/spatial/prefix/tree/Cell;>;"
    iput-object p1, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate$VNodeCellIterator;->this$1:Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 283
    iput-object p2, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate$VNodeCellIterator;->cellIter:Ljava/util/Iterator;

    .line 284
    iput-object p3, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate$VNodeCellIterator;->vNode:Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;

    .line 285
    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 1

    .prologue
    .line 289
    iget-object v0, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate$VNodeCellIterator;->cellIter:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate$VNodeCellIterator;->next()Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;

    move-result-object v0

    return-object v0
.end method

.method public next()Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;
    .locals 2

    .prologue
    .line 294
    sget-boolean v0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate$VNodeCellIterator;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate$VNodeCellIterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 295
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate$VNodeCellIterator;->vNode:Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;

    iget-object v0, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate$VNodeCellIterator;->cellIter:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/spatial/prefix/tree/Cell;

    invoke-virtual {v1, v0}, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;->reset(Lorg/apache/lucene/spatial/prefix/tree/Cell;)V

    .line 296
    iget-object v0, p0, Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VisitorTemplate$VNodeCellIterator;->vNode:Lorg/apache/lucene/spatial/prefix/AbstractVisitingPrefixTreeFilter$VNode;

    return-object v0
.end method

.method public remove()V
    .locals 0

    .prologue
    .line 301
    return-void
.end method
