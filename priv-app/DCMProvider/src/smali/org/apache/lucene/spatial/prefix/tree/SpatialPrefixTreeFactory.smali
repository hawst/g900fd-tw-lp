.class public abstract Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTreeFactory;
.super Ljava/lang/Object;
.source "SpatialPrefixTreeFactory.java"


# static fields
.field private static final DEFAULT_GEO_MAX_DETAIL_KM:D = 0.001

.field public static final MAX_DIST_ERR:Ljava/lang/String; = "maxDistErr"

.field public static final MAX_LEVELS:Ljava/lang/String; = "maxLevels"

.field public static final PREFIX_TREE:Ljava/lang/String; = "prefixTree"


# instance fields
.field protected args:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected ctx:Lcom/spatial4j/core/context/SpatialContext;

.field protected maxLevels:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static makeSPT(Ljava/util/Map;Ljava/lang/ClassLoader;Lcom/spatial4j/core/context/SpatialContext;)Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;
    .locals 5
    .param p1, "classLoader"    # Ljava/lang/ClassLoader;
    .param p2, "ctx"    # Lcom/spatial4j/core/context/SpatialContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/ClassLoader;",
            "Lcom/spatial4j/core/context/SpatialContext;",
            ")",
            "Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;"
        }
    .end annotation

    .prologue
    .line 48
    .local p0, "args":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v4, "prefixTree"

    invoke-interface {p0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 49
    .local v1, "cname":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 50
    invoke-virtual {p2}, Lcom/spatial4j/core/context/SpatialContext;->isGeo()Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v1, "geohash"

    .line 51
    :cond_0
    :goto_0
    const-string v4, "geohash"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 52
    new-instance v3, Lorg/apache/lucene/spatial/prefix/tree/GeohashPrefixTree$Factory;

    invoke-direct {v3}, Lorg/apache/lucene/spatial/prefix/tree/GeohashPrefixTree$Factory;-><init>()V

    .line 63
    .local v3, "instance":Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTreeFactory;
    :goto_1
    invoke-virtual {v3, p0, p2}, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTreeFactory;->init(Ljava/util/Map;Lcom/spatial4j/core/context/SpatialContext;)V

    .line 64
    invoke-virtual {v3}, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTreeFactory;->newSPT()Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;

    move-result-object v4

    return-object v4

    .line 50
    .end local v3    # "instance":Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTreeFactory;
    :cond_1
    const-string v1, "quad"

    goto :goto_0

    .line 53
    :cond_2
    const-string v4, "quad"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 54
    new-instance v3, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree$Factory;

    invoke-direct {v3}, Lorg/apache/lucene/spatial/prefix/tree/QuadPrefixTree$Factory;-><init>()V

    .restart local v3    # "instance":Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTreeFactory;
    goto :goto_1

    .line 57
    .end local v3    # "instance":Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTreeFactory;
    :cond_3
    :try_start_0
    invoke-virtual {p1, v1}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 58
    .local v0, "c":Ljava/lang/Class;
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTreeFactory;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .restart local v3    # "instance":Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTreeFactory;
    goto :goto_1

    .line 59
    .end local v0    # "c":Ljava/lang/Class;
    .end local v3    # "instance":Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTreeFactory;
    :catch_0
    move-exception v2

    .line 60
    .local v2, "e":Ljava/lang/Exception;
    new-instance v4, Ljava/lang/RuntimeException;

    invoke-direct {v4, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v4
.end method


# virtual methods
.method protected abstract getLevelForDistance(D)I
.end method

.method protected init(Ljava/util/Map;Lcom/spatial4j/core/context/SpatialContext;)V
    .locals 0
    .param p2, "ctx"    # Lcom/spatial4j/core/context/SpatialContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/spatial4j/core/context/SpatialContext;",
            ")V"
        }
    .end annotation

    .prologue
    .line 68
    .local p1, "args":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iput-object p1, p0, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTreeFactory;->args:Ljava/util/Map;

    .line 69
    iput-object p2, p0, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTreeFactory;->ctx:Lcom/spatial4j/core/context/SpatialContext;

    .line 70
    invoke-virtual {p0}, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTreeFactory;->initMaxLevels()V

    .line 71
    return-void
.end method

.method protected initMaxLevels()V
    .locals 8

    .prologue
    .line 74
    iget-object v4, p0, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTreeFactory;->args:Ljava/util/Map;

    const-string v5, "maxLevels"

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 75
    .local v3, "mlStr":Ljava/lang/String;
    if-eqz v3, :cond_1

    .line 76
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    iput-object v4, p0, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTreeFactory;->maxLevels:Ljava/lang/Integer;

    .line 91
    :cond_0
    :goto_0
    return-void

    .line 81
    :cond_1
    iget-object v4, p0, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTreeFactory;->args:Ljava/util/Map;

    const-string v5, "maxDistErr"

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 82
    .local v2, "maxDetailDistStr":Ljava/lang/String;
    if-nez v2, :cond_2

    .line 83
    iget-object v4, p0, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTreeFactory;->ctx:Lcom/spatial4j/core/context/SpatialContext;

    invoke-virtual {v4}, Lcom/spatial4j/core/context/SpatialContext;->isGeo()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 86
    const-wide v4, 0x3f50624dd2f1a9fcL    # 0.001

    const-wide v6, 0x40b8e3023ed7ac24L    # 6371.0087714

    invoke-static {v4, v5, v6, v7}, Lcom/spatial4j/core/distance/DistanceUtils;->dist2Degrees(DD)D

    move-result-wide v0

    .line 90
    .local v0, "degrees":D
    :goto_1
    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTreeFactory;->getLevelForDistance(D)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iput-object v4, p0, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTreeFactory;->maxLevels:Ljava/lang/Integer;

    goto :goto_0

    .line 88
    .end local v0    # "degrees":D
    :cond_2
    invoke-static {v2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    .restart local v0    # "degrees":D
    goto :goto_1
.end method

.method protected abstract newSPT()Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;
.end method
