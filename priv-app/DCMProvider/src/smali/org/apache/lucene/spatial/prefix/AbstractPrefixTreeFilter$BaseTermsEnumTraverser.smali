.class public abstract Lorg/apache/lucene/spatial/prefix/AbstractPrefixTreeFilter$BaseTermsEnumTraverser;
.super Ljava/lang/Object;
.source "AbstractPrefixTreeFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/spatial/prefix/AbstractPrefixTreeFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401
    name = "BaseTermsEnumTraverser"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field protected acceptDocs:Lorg/apache/lucene/util/Bits;

.field protected final context:Lorg/apache/lucene/index/AtomicReaderContext;

.field protected docsEnum:Lorg/apache/lucene/index/DocsEnum;

.field protected final maxDoc:I

.field protected termsEnum:Lorg/apache/lucene/index/TermsEnum;

.field final synthetic this$0:Lorg/apache/lucene/spatial/prefix/AbstractPrefixTreeFilter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 77
    const-class v0, Lorg/apache/lucene/spatial/prefix/AbstractPrefixTreeFilter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/spatial/prefix/AbstractPrefixTreeFilter$BaseTermsEnumTraverser;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/spatial/prefix/AbstractPrefixTreeFilter;Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;)V
    .locals 3
    .param p2, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p3, "acceptDocs"    # Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 86
    iput-object p1, p0, Lorg/apache/lucene/spatial/prefix/AbstractPrefixTreeFilter$BaseTermsEnumTraverser;->this$0:Lorg/apache/lucene/spatial/prefix/AbstractPrefixTreeFilter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    iput-object p2, p0, Lorg/apache/lucene/spatial/prefix/AbstractPrefixTreeFilter$BaseTermsEnumTraverser;->context:Lorg/apache/lucene/index/AtomicReaderContext;

    .line 88
    invoke-virtual {p2}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v0

    .line 89
    .local v0, "reader":Lorg/apache/lucene/index/AtomicReader;
    iput-object p3, p0, Lorg/apache/lucene/spatial/prefix/AbstractPrefixTreeFilter$BaseTermsEnumTraverser;->acceptDocs:Lorg/apache/lucene/util/Bits;

    .line 90
    invoke-virtual {v0}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v2

    iput v2, p0, Lorg/apache/lucene/spatial/prefix/AbstractPrefixTreeFilter$BaseTermsEnumTraverser;->maxDoc:I

    .line 91
    iget-object v2, p1, Lorg/apache/lucene/spatial/prefix/AbstractPrefixTreeFilter;->fieldName:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lorg/apache/lucene/index/AtomicReader;->terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v1

    .line 92
    .local v1, "terms":Lorg/apache/lucene/index/Terms;
    if-eqz v1, :cond_0

    .line 93
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/lucene/spatial/prefix/AbstractPrefixTreeFilter$BaseTermsEnumTraverser;->termsEnum:Lorg/apache/lucene/index/TermsEnum;

    .line 94
    :cond_0
    return-void
.end method


# virtual methods
.method protected collectDocs(Lorg/apache/lucene/util/FixedBitSet;)V
    .locals 5
    .param p1, "bitSet"    # Lorg/apache/lucene/util/FixedBitSet;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 98
    sget-boolean v1, Lorg/apache/lucene/spatial/prefix/AbstractPrefixTreeFilter$BaseTermsEnumTraverser;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/spatial/prefix/AbstractPrefixTreeFilter$BaseTermsEnumTraverser;->termsEnum:Lorg/apache/lucene/index/TermsEnum;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 99
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/spatial/prefix/AbstractPrefixTreeFilter$BaseTermsEnumTraverser;->termsEnum:Lorg/apache/lucene/index/TermsEnum;

    iget-object v2, p0, Lorg/apache/lucene/spatial/prefix/AbstractPrefixTreeFilter$BaseTermsEnumTraverser;->acceptDocs:Lorg/apache/lucene/util/Bits;

    iget-object v3, p0, Lorg/apache/lucene/spatial/prefix/AbstractPrefixTreeFilter$BaseTermsEnumTraverser;->docsEnum:Lorg/apache/lucene/index/DocsEnum;

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lorg/apache/lucene/index/TermsEnum;->docs(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;I)Lorg/apache/lucene/index/DocsEnum;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/spatial/prefix/AbstractPrefixTreeFilter$BaseTermsEnumTraverser;->docsEnum:Lorg/apache/lucene/index/DocsEnum;

    .line 101
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/spatial/prefix/AbstractPrefixTreeFilter$BaseTermsEnumTraverser;->docsEnum:Lorg/apache/lucene/index/DocsEnum;

    invoke-virtual {v1}, Lorg/apache/lucene/index/DocsEnum;->nextDoc()I

    move-result v0

    .local v0, "docid":I
    const v1, 0x7fffffff

    if-ne v0, v1, :cond_1

    .line 104
    return-void

    .line 102
    :cond_1
    invoke-virtual {p1, v0}, Lorg/apache/lucene/util/FixedBitSet;->set(I)V

    goto :goto_0
.end method
