.class public Lorg/apache/lucene/spatial/util/ShapeFieldCacheDistanceValueSource;
.super Lorg/apache/lucene/queries/function/ValueSource;
.source "ShapeFieldCacheDistanceValueSource.java"


# instance fields
.field private final ctx:Lcom/spatial4j/core/context/SpatialContext;

.field private final from:Lcom/spatial4j/core/shape/Point;

.field private final provider:Lorg/apache/lucene/spatial/util/ShapeFieldCacheProvider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/spatial/util/ShapeFieldCacheProvider",
            "<",
            "Lcom/spatial4j/core/shape/Point;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/spatial4j/core/context/SpatialContext;Lorg/apache/lucene/spatial/util/ShapeFieldCacheProvider;Lcom/spatial4j/core/shape/Point;)V
    .locals 0
    .param p1, "ctx"    # Lcom/spatial4j/core/context/SpatialContext;
    .param p3, "from"    # Lcom/spatial4j/core/shape/Point;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/spatial4j/core/context/SpatialContext;",
            "Lorg/apache/lucene/spatial/util/ShapeFieldCacheProvider",
            "<",
            "Lcom/spatial4j/core/shape/Point;",
            ">;",
            "Lcom/spatial4j/core/shape/Point;",
            ")V"
        }
    .end annotation

    .prologue
    .line 45
    .local p2, "provider":Lorg/apache/lucene/spatial/util/ShapeFieldCacheProvider;, "Lorg/apache/lucene/spatial/util/ShapeFieldCacheProvider<Lcom/spatial4j/core/shape/Point;>;"
    invoke-direct {p0}, Lorg/apache/lucene/queries/function/ValueSource;-><init>()V

    .line 46
    iput-object p1, p0, Lorg/apache/lucene/spatial/util/ShapeFieldCacheDistanceValueSource;->ctx:Lcom/spatial4j/core/context/SpatialContext;

    .line 47
    iput-object p3, p0, Lorg/apache/lucene/spatial/util/ShapeFieldCacheDistanceValueSource;->from:Lcom/spatial4j/core/shape/Point;

    .line 48
    iput-object p2, p0, Lorg/apache/lucene/spatial/util/ShapeFieldCacheDistanceValueSource;->provider:Lorg/apache/lucene/spatial/util/ShapeFieldCacheProvider;

    .line 49
    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/spatial/util/ShapeFieldCacheDistanceValueSource;)Lorg/apache/lucene/spatial/util/ShapeFieldCacheProvider;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lorg/apache/lucene/spatial/util/ShapeFieldCacheDistanceValueSource;->provider:Lorg/apache/lucene/spatial/util/ShapeFieldCacheProvider;

    return-object v0
.end method

.method static synthetic access$1(Lorg/apache/lucene/spatial/util/ShapeFieldCacheDistanceValueSource;)Lcom/spatial4j/core/shape/Point;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lorg/apache/lucene/spatial/util/ShapeFieldCacheDistanceValueSource;->from:Lcom/spatial4j/core/shape/Point;

    return-object v0
.end method

.method static synthetic access$2(Lorg/apache/lucene/spatial/util/ShapeFieldCacheDistanceValueSource;)Lcom/spatial4j/core/context/SpatialContext;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lorg/apache/lucene/spatial/util/ShapeFieldCacheDistanceValueSource;->ctx:Lcom/spatial4j/core/context/SpatialContext;

    return-object v0
.end method


# virtual methods
.method public description()Ljava/lang/String;
    .locals 2

    .prologue
    .line 53
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/spatial/util/ShapeFieldCacheDistanceValueSource;->provider:Lorg/apache/lucene/spatial/util/ShapeFieldCacheProvider;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/spatial/util/ShapeFieldCacheDistanceValueSource;->from:Lcom/spatial4j/core/shape/Point;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 92
    if-ne p0, p1, :cond_1

    .line 101
    :cond_0
    :goto_0
    return v1

    .line 93
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 95
    check-cast v0, Lorg/apache/lucene/spatial/util/ShapeFieldCacheDistanceValueSource;

    .line 97
    .local v0, "that":Lorg/apache/lucene/spatial/util/ShapeFieldCacheDistanceValueSource;
    iget-object v3, p0, Lorg/apache/lucene/spatial/util/ShapeFieldCacheDistanceValueSource;->ctx:Lcom/spatial4j/core/context/SpatialContext;

    iget-object v4, v0, Lorg/apache/lucene/spatial/util/ShapeFieldCacheDistanceValueSource;->ctx:Lcom/spatial4j/core/context/SpatialContext;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    goto :goto_0

    .line 98
    :cond_4
    iget-object v3, p0, Lorg/apache/lucene/spatial/util/ShapeFieldCacheDistanceValueSource;->from:Lcom/spatial4j/core/shape/Point;

    iget-object v4, v0, Lorg/apache/lucene/spatial/util/ShapeFieldCacheDistanceValueSource;->from:Lcom/spatial4j/core/shape/Point;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    move v1, v2

    goto :goto_0

    .line 99
    :cond_5
    iget-object v3, p0, Lorg/apache/lucene/spatial/util/ShapeFieldCacheDistanceValueSource;->provider:Lorg/apache/lucene/spatial/util/ShapeFieldCacheProvider;

    iget-object v4, v0, Lorg/apache/lucene/spatial/util/ShapeFieldCacheDistanceValueSource;->provider:Lorg/apache/lucene/spatial/util/ShapeFieldCacheProvider;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
    .locals 1
    .param p1, "context"    # Ljava/util/Map;
    .param p2, "readerContext"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58
    new-instance v0, Lorg/apache/lucene/spatial/util/ShapeFieldCacheDistanceValueSource$1;

    invoke-direct {v0, p0, p2}, Lorg/apache/lucene/spatial/util/ShapeFieldCacheDistanceValueSource$1;-><init>(Lorg/apache/lucene/spatial/util/ShapeFieldCacheDistanceValueSource;Lorg/apache/lucene/index/AtomicReaderContext;)V

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lorg/apache/lucene/spatial/util/ShapeFieldCacheDistanceValueSource;->from:Lcom/spatial4j/core/shape/Point;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method
