.class Lorg/apache/lucene/spatial/vector/DistanceValueSource$1;
.super Lorg/apache/lucene/queries/function/FunctionValues;
.source "DistanceValueSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/spatial/vector/DistanceValueSource;->getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final calculator:Lcom/spatial4j/core/distance/DistanceCalculator;

.field private final from:Lcom/spatial4j/core/shape/Point;

.field private final nullValue:D

.field final synthetic this$0:Lorg/apache/lucene/spatial/vector/DistanceValueSource;

.field private final synthetic val$ptX:Lorg/apache/lucene/search/FieldCache$Doubles;

.field private final synthetic val$ptY:Lorg/apache/lucene/search/FieldCache$Doubles;

.field private final synthetic val$validX:Lorg/apache/lucene/util/Bits;

.field private final synthetic val$validY:Lorg/apache/lucene/util/Bits;


# direct methods
.method constructor <init>(Lorg/apache/lucene/spatial/vector/DistanceValueSource;Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/search/FieldCache$Doubles;Lorg/apache/lucene/search/FieldCache$Doubles;)V
    .locals 2

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/spatial/vector/DistanceValueSource$1;->this$0:Lorg/apache/lucene/spatial/vector/DistanceValueSource;

    iput-object p2, p0, Lorg/apache/lucene/spatial/vector/DistanceValueSource$1;->val$validX:Lorg/apache/lucene/util/Bits;

    iput-object p3, p0, Lorg/apache/lucene/spatial/vector/DistanceValueSource$1;->val$validY:Lorg/apache/lucene/util/Bits;

    iput-object p4, p0, Lorg/apache/lucene/spatial/vector/DistanceValueSource$1;->val$ptX:Lorg/apache/lucene/search/FieldCache$Doubles;

    iput-object p5, p0, Lorg/apache/lucene/spatial/vector/DistanceValueSource$1;->val$ptY:Lorg/apache/lucene/search/FieldCache$Doubles;

    .line 71
    invoke-direct {p0}, Lorg/apache/lucene/queries/function/FunctionValues;-><init>()V

    .line 73
    # getter for: Lorg/apache/lucene/spatial/vector/DistanceValueSource;->from:Lcom/spatial4j/core/shape/Point;
    invoke-static {p1}, Lorg/apache/lucene/spatial/vector/DistanceValueSource;->access$0(Lorg/apache/lucene/spatial/vector/DistanceValueSource;)Lcom/spatial4j/core/shape/Point;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/spatial/vector/DistanceValueSource$1;->from:Lcom/spatial4j/core/shape/Point;

    .line 74
    # getter for: Lorg/apache/lucene/spatial/vector/DistanceValueSource;->strategy:Lorg/apache/lucene/spatial/vector/PointVectorStrategy;
    invoke-static {p1}, Lorg/apache/lucene/spatial/vector/DistanceValueSource;->access$1(Lorg/apache/lucene/spatial/vector/DistanceValueSource;)Lorg/apache/lucene/spatial/vector/PointVectorStrategy;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/spatial/vector/PointVectorStrategy;->getSpatialContext()Lcom/spatial4j/core/context/SpatialContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/spatial4j/core/context/SpatialContext;->getDistCalc()Lcom/spatial4j/core/distance/DistanceCalculator;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/spatial/vector/DistanceValueSource$1;->calculator:Lcom/spatial4j/core/distance/DistanceCalculator;

    .line 75
    # getter for: Lorg/apache/lucene/spatial/vector/DistanceValueSource;->strategy:Lorg/apache/lucene/spatial/vector/PointVectorStrategy;
    invoke-static {p1}, Lorg/apache/lucene/spatial/vector/DistanceValueSource;->access$1(Lorg/apache/lucene/spatial/vector/DistanceValueSource;)Lorg/apache/lucene/spatial/vector/PointVectorStrategy;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/spatial/vector/PointVectorStrategy;->getSpatialContext()Lcom/spatial4j/core/context/SpatialContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/spatial4j/core/context/SpatialContext;->isGeo()Z

    move-result v0

    if-eqz v0, :cond_0

    const-wide v0, 0x4066800000000000L    # 180.0

    :goto_0
    iput-wide v0, p0, Lorg/apache/lucene/spatial/vector/DistanceValueSource$1;->nullValue:D

    return-void

    :cond_0
    const-wide v0, 0x7fefffffffffffffL    # Double.MAX_VALUE

    goto :goto_0
.end method


# virtual methods
.method public doubleVal(I)D
    .locals 6
    .param p1, "doc"    # I

    .prologue
    .line 85
    iget-object v0, p0, Lorg/apache/lucene/spatial/vector/DistanceValueSource$1;->val$validX:Lorg/apache/lucene/util/Bits;

    invoke-interface {v0, p1}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 86
    sget-boolean v0, Lorg/apache/lucene/spatial/vector/DistanceValueSource;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/spatial/vector/DistanceValueSource$1;->val$validY:Lorg/apache/lucene/util/Bits;

    invoke-interface {v0, p1}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 87
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/spatial/vector/DistanceValueSource$1;->calculator:Lcom/spatial4j/core/distance/DistanceCalculator;

    iget-object v1, p0, Lorg/apache/lucene/spatial/vector/DistanceValueSource$1;->from:Lcom/spatial4j/core/shape/Point;

    iget-object v2, p0, Lorg/apache/lucene/spatial/vector/DistanceValueSource$1;->val$ptX:Lorg/apache/lucene/search/FieldCache$Doubles;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/search/FieldCache$Doubles;->get(I)D

    move-result-wide v2

    iget-object v4, p0, Lorg/apache/lucene/spatial/vector/DistanceValueSource$1;->val$ptY:Lorg/apache/lucene/search/FieldCache$Doubles;

    invoke-virtual {v4, p1}, Lorg/apache/lucene/search/FieldCache$Doubles;->get(I)D

    move-result-wide v4

    invoke-interface/range {v0 .. v5}, Lcom/spatial4j/core/distance/DistanceCalculator;->distance(Lcom/spatial4j/core/shape/Point;DD)D

    move-result-wide v0

    .line 89
    :goto_0
    return-wide v0

    :cond_1
    iget-wide v0, p0, Lorg/apache/lucene/spatial/vector/DistanceValueSource$1;->nullValue:D

    goto :goto_0
.end method

.method public floatVal(I)F
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 79
    invoke-virtual {p0, p1}, Lorg/apache/lucene/spatial/vector/DistanceValueSource$1;->doubleVal(I)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method public toString(I)Ljava/lang/String;
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 94
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/lucene/spatial/vector/DistanceValueSource$1;->this$0:Lorg/apache/lucene/spatial/vector/DistanceValueSource;

    invoke-virtual {v1}, Lorg/apache/lucene/spatial/vector/DistanceValueSource;->description()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, p1}, Lorg/apache/lucene/spatial/vector/DistanceValueSource$1;->floatVal(I)F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
