.class public Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter;
.super Lorg/apache/lucene/spatial/prefix/AbstractPrefixTreeFilter;
.source "ContainsPrefixTreeFilter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$ContainsVisitor;,
        Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/spatial4j/core/shape/Shape;Ljava/lang/String;Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;I)V
    .locals 0
    .param p1, "queryShape"    # Lcom/spatial4j/core/shape/Shape;
    .param p2, "fieldName"    # Ljava/lang/String;
    .param p3, "grid"    # Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;
    .param p4, "detailLevel"    # I

    .prologue
    .line 45
    invoke-direct {p0, p1, p2, p3, p4}, Lorg/apache/lucene/spatial/prefix/AbstractPrefixTreeFilter;-><init>(Lcom/spatial4j/core/shape/Shape;Ljava/lang/String;Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;I)V

    .line 46
    return-void
.end method


# virtual methods
.method public getDocIdSet(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/DocIdSet;
    .locals 2
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "acceptDocs"    # Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50
    new-instance v0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$ContainsVisitor;

    invoke-direct {v0, p0, p1, p2}, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$ContainsVisitor;-><init>(Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter;Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;)V

    iget-object v1, p0, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter;->grid:Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;

    invoke-virtual {v1}, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;->getWorldCell()Lorg/apache/lucene/spatial/prefix/tree/Cell;

    move-result-object v1

    # invokes: Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$ContainsVisitor;->visit(Lorg/apache/lucene/spatial/prefix/tree/Cell;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;
    invoke-static {v0, v1, p2}, Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$ContainsVisitor;->access$0(Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$ContainsVisitor;Lorg/apache/lucene/spatial/prefix/tree/Cell;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/spatial/prefix/ContainsPrefixTreeFilter$SmallDocSet;

    move-result-object v0

    return-object v0
.end method
