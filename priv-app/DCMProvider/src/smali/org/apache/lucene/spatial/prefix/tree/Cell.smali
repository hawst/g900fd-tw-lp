.class public abstract Lorg/apache/lucene/spatial/prefix/tree/Cell;
.super Ljava/lang/Object;
.source "Cell.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lorg/apache/lucene/spatial/prefix/tree/Cell;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final LEAF_BYTE:B = 0x2bt


# instance fields
.field private b_len:I

.field private b_off:I

.field private bytes:[B

.field protected leaf:Z

.field protected shapeRel:Lcom/spatial4j/core/shape/SpatialRelation;

.field private token:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lorg/apache/lucene/spatial/prefix/tree/Cell;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/spatial/prefix/tree/Cell;->$assertionsDisabled:Z

    .line 36
    return-void

    .line 35
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "token"    # Ljava/lang/String;

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-object p1, p0, Lorg/apache/lucene/spatial/prefix/tree/Cell;->token:Ljava/lang/String;

    .line 63
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x2b

    if-ne v0, v1, :cond_0

    .line 64
    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/spatial/prefix/tree/Cell;->token:Ljava/lang/String;

    .line 65
    invoke-virtual {p0}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->setLeaf()V

    .line 68
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->getLevel()I

    move-result v0

    if-nez v0, :cond_1

    .line 69
    invoke-virtual {p0}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->getShape()Lcom/spatial4j/core/shape/Shape;

    .line 70
    :cond_1
    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 0
    .param p1, "bytes"    # [B
    .param p2, "off"    # I
    .param p3, "len"    # I

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    iput-object p1, p0, Lorg/apache/lucene/spatial/prefix/tree/Cell;->bytes:[B

    .line 74
    iput p2, p0, Lorg/apache/lucene/spatial/prefix/tree/Cell;->b_off:I

    .line 75
    iput p3, p0, Lorg/apache/lucene/spatial/prefix/tree/Cell;->b_len:I

    .line 76
    invoke-direct {p0}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->b_fixLeaf()V

    .line 77
    return-void
.end method

.method private b_fixLeaf()V
    .locals 3

    .prologue
    .line 91
    iget-object v0, p0, Lorg/apache/lucene/spatial/prefix/tree/Cell;->bytes:[B

    iget v1, p0, Lorg/apache/lucene/spatial/prefix/tree/Cell;->b_off:I

    iget v2, p0, Lorg/apache/lucene/spatial/prefix/tree/Cell;->b_len:I

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    aget-byte v0, v0, v1

    const/16 v1, 0x2b

    if-ne v0, v1, :cond_0

    .line 92
    iget v0, p0, Lorg/apache/lucene/spatial/prefix/tree/Cell;->b_len:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/spatial/prefix/tree/Cell;->b_len:I

    .line 93
    invoke-virtual {p0}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->setLeaf()V

    .line 97
    :goto_0
    return-void

    .line 95
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/spatial/prefix/tree/Cell;->leaf:Z

    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/spatial/prefix/tree/Cell;

    invoke-virtual {p0, p1}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->compareTo(Lorg/apache/lucene/spatial/prefix/tree/Cell;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lorg/apache/lucene/spatial/prefix/tree/Cell;)I
    .locals 2
    .param p1, "o"    # Lorg/apache/lucene/spatial/prefix/tree/Cell;

    .prologue
    .line 222
    invoke-virtual {p0}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->getTokenString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->getTokenString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 227
    if-eqz p1, :cond_0

    instance-of v0, p1, Lorg/apache/lucene/spatial/prefix/tree/Cell;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->getTokenString()Ljava/lang/String;

    move-result-object v0

    check-cast p1, Lorg/apache/lucene/spatial/prefix/tree/Cell;

    .end local p1    # "obj":Ljava/lang/Object;
    invoke-virtual {p1}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->getTokenString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCenter()Lcom/spatial4j/core/shape/Point;
    .locals 1

    .prologue
    .line 217
    invoke-virtual {p0}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->getShape()Lcom/spatial4j/core/shape/Shape;

    move-result-object v0

    invoke-interface {v0}, Lcom/spatial4j/core/shape/Shape;->getCenter()Lcom/spatial4j/core/shape/Point;

    move-result-object v0

    return-object v0
.end method

.method public getLevel()I
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lorg/apache/lucene/spatial/prefix/tree/Cell;->token:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/spatial/prefix/tree/Cell;->token:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lorg/apache/lucene/spatial/prefix/tree/Cell;->b_len:I

    goto :goto_0
.end method

.method public abstract getShape()Lcom/spatial4j/core/shape/Shape;
.end method

.method public getShapeRel()Lcom/spatial4j/core/shape/SpatialRelation;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lorg/apache/lucene/spatial/prefix/tree/Cell;->shapeRel:Lcom/spatial4j/core/shape/SpatialRelation;

    return-object v0
.end method

.method public abstract getSubCell(Lcom/spatial4j/core/shape/Point;)Lorg/apache/lucene/spatial/prefix/tree/Cell;
.end method

.method protected abstract getSubCells()Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/spatial/prefix/tree/Cell;",
            ">;"
        }
    .end annotation
.end method

.method public getSubCells(Lcom/spatial4j/core/shape/Shape;)Ljava/util/Collection;
    .locals 7
    .param p1, "shapeFilter"    # Lcom/spatial4j/core/shape/Shape;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/spatial4j/core/shape/Shape;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/spatial/prefix/tree/Cell;",
            ">;"
        }
    .end annotation

    .prologue
    .line 165
    instance-of v5, p1, Lcom/spatial4j/core/shape/Point;

    if-eqz v5, :cond_1

    .line 166
    check-cast p1, Lcom/spatial4j/core/shape/Point;

    .end local p1    # "shapeFilter":Lcom/spatial4j/core/shape/Shape;
    invoke-virtual {p0, p1}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->getSubCell(Lcom/spatial4j/core/shape/Point;)Lorg/apache/lucene/spatial/prefix/tree/Cell;

    move-result-object v4

    .line 167
    .local v4, "subCell":Lorg/apache/lucene/spatial/prefix/tree/Cell;
    sget-object v5, Lcom/spatial4j/core/shape/SpatialRelation;->CONTAINS:Lcom/spatial4j/core/shape/SpatialRelation;

    iput-object v5, v4, Lorg/apache/lucene/spatial/prefix/tree/Cell;->shapeRel:Lcom/spatial4j/core/shape/SpatialRelation;

    .line 168
    invoke-static {v4}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 187
    .end local v4    # "subCell":Lorg/apache/lucene/spatial/prefix/tree/Cell;
    :cond_0
    :goto_0
    return-object v1

    .line 170
    .restart local p1    # "shapeFilter":Lcom/spatial4j/core/shape/Shape;
    :cond_1
    invoke-virtual {p0}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->getSubCells()Ljava/util/Collection;

    move-result-object v1

    .line 172
    .local v1, "cells":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/spatial/prefix/tree/Cell;>;"
    if-eqz p1, :cond_0

    .line 177
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v5

    invoke-direct {v2, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 178
    .local v2, "copy":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/spatial/prefix/tree/Cell;>;"
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_3

    move-object v1, v2

    .line 187
    goto :goto_0

    .line 178
    :cond_3
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/spatial/prefix/tree/Cell;

    .line 179
    .local v0, "cell":Lorg/apache/lucene/spatial/prefix/tree/Cell;
    invoke-virtual {v0}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->getShape()Lcom/spatial4j/core/shape/Shape;

    move-result-object v6

    invoke-interface {v6, p1}, Lcom/spatial4j/core/shape/Shape;->relate(Lcom/spatial4j/core/shape/Shape;)Lcom/spatial4j/core/shape/SpatialRelation;

    move-result-object v3

    .line 180
    .local v3, "rel":Lcom/spatial4j/core/shape/SpatialRelation;
    sget-object v6, Lcom/spatial4j/core/shape/SpatialRelation;->DISJOINT:Lcom/spatial4j/core/shape/SpatialRelation;

    if-eq v3, v6, :cond_2

    .line 182
    iput-object v3, v0, Lorg/apache/lucene/spatial/prefix/tree/Cell;->shapeRel:Lcom/spatial4j/core/shape/SpatialRelation;

    .line 183
    sget-object v6, Lcom/spatial4j/core/shape/SpatialRelation;->WITHIN:Lcom/spatial4j/core/shape/SpatialRelation;

    if-ne v3, v6, :cond_4

    .line 184
    invoke-virtual {v0}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->setLeaf()V

    .line 185
    :cond_4
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public abstract getSubCellsSize()I
.end method

.method public getTokenBytes()[B
    .locals 2

    .prologue
    .line 131
    iget-object v0, p0, Lorg/apache/lucene/spatial/prefix/tree/Cell;->bytes:[B

    if-eqz v0, :cond_1

    .line 132
    iget v0, p0, Lorg/apache/lucene/spatial/prefix/tree/Cell;->b_off:I

    if-nez v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/spatial/prefix/tree/Cell;->b_len:I

    iget-object v1, p0, Lorg/apache/lucene/spatial/prefix/tree/Cell;->bytes:[B

    array-length v1, v1

    if-eq v0, v1, :cond_2

    .line 133
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not supported if byte[] needs to be recreated."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 136
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/spatial/prefix/tree/Cell;->token:Ljava/lang/String;

    sget-object v1, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;->UTF8:Ljava/nio/charset/Charset;

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/spatial/prefix/tree/Cell;->bytes:[B

    .line 137
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/spatial/prefix/tree/Cell;->b_off:I

    .line 138
    iget-object v0, p0, Lorg/apache/lucene/spatial/prefix/tree/Cell;->bytes:[B

    array-length v0, v0

    iput v0, p0, Lorg/apache/lucene/spatial/prefix/tree/Cell;->b_len:I

    .line 140
    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/spatial/prefix/tree/Cell;->bytes:[B

    return-object v0
.end method

.method public getTokenString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 121
    iget-object v0, p0, Lorg/apache/lucene/spatial/prefix/tree/Cell;->token:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 122
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/lucene/spatial/prefix/tree/Cell;->bytes:[B

    iget v2, p0, Lorg/apache/lucene/spatial/prefix/tree/Cell;->b_off:I

    iget v3, p0, Lorg/apache/lucene/spatial/prefix/tree/Cell;->b_len:I

    sget-object v4, Lorg/apache/lucene/spatial/prefix/tree/SpatialPrefixTree;->UTF8:Ljava/nio/charset/Charset;

    invoke-direct {v0, v1, v2, v3, v4}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    iput-object v0, p0, Lorg/apache/lucene/spatial/prefix/tree/Cell;->token:Ljava/lang/String;

    .line 124
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/spatial/prefix/tree/Cell;->token:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 232
    invoke-virtual {p0}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->getTokenString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public isLeaf()Z
    .locals 1

    .prologue
    .line 108
    iget-boolean v0, p0, Lorg/apache/lucene/spatial/prefix/tree/Cell;->leaf:Z

    return v0
.end method

.method public reset([BII)V
    .locals 2
    .param p1, "bytes"    # [B
    .param p2, "off"    # I
    .param p3, "len"    # I

    .prologue
    const/4 v1, 0x0

    .line 80
    sget-boolean v0, Lorg/apache/lucene/spatial/prefix/tree/Cell;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->getLevel()I

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 81
    :cond_0
    iput-object v1, p0, Lorg/apache/lucene/spatial/prefix/tree/Cell;->token:Ljava/lang/String;

    .line 82
    iput-object v1, p0, Lorg/apache/lucene/spatial/prefix/tree/Cell;->shapeRel:Lcom/spatial4j/core/shape/SpatialRelation;

    .line 83
    iput-object p1, p0, Lorg/apache/lucene/spatial/prefix/tree/Cell;->bytes:[B

    .line 84
    iput p2, p0, Lorg/apache/lucene/spatial/prefix/tree/Cell;->b_off:I

    .line 85
    iput p3, p0, Lorg/apache/lucene/spatial/prefix/tree/Cell;->b_len:I

    .line 86
    invoke-direct {p0}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->b_fixLeaf()V

    .line 87
    return-void
.end method

.method public setLeaf()V
    .locals 1

    .prologue
    .line 113
    sget-boolean v0, Lorg/apache/lucene/spatial/prefix/tree/Cell;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->getLevel()I

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 114
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/spatial/prefix/tree/Cell;->leaf:Z

    .line 115
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 237
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->getTokenString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/lucene/spatial/prefix/tree/Cell;->isLeaf()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x2b

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method
