.class final Lorg/apache/lucene/search/BooleanScorer$BucketScorer;
.super Lorg/apache/lucene/search/Scorer;
.source "BooleanScorer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/BooleanScorer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "BucketScorer"
.end annotation


# instance fields
.field doc:I

.field freq:I

.field score:D


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/Weight;)V
    .locals 1
    .param p1, "weight"    # Lorg/apache/lucene/search/Weight;

    .prologue
    .line 121
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/Scorer;-><init>(Lorg/apache/lucene/search/Weight;)V

    .line 118
    const v0, 0x7fffffff

    iput v0, p0, Lorg/apache/lucene/search/BooleanScorer$BucketScorer;->doc:I

    .line 121
    return-void
.end method


# virtual methods
.method public advance(I)I
    .locals 1
    .param p1, "target"    # I

    .prologue
    .line 124
    const v0, 0x7fffffff

    return v0
.end method

.method public cost()J
    .locals 2

    .prologue
    .line 139
    const-wide/16 v0, 0x1

    return-wide v0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 127
    iget v0, p0, Lorg/apache/lucene/search/BooleanScorer$BucketScorer;->doc:I

    return v0
.end method

.method public freq()I
    .locals 1

    .prologue
    .line 130
    iget v0, p0, Lorg/apache/lucene/search/BooleanScorer$BucketScorer;->freq:I

    return v0
.end method

.method public nextDoc()I
    .locals 1

    .prologue
    .line 133
    const v0, 0x7fffffff

    return v0
.end method

.method public score()F
    .locals 2

    .prologue
    .line 136
    iget-wide v0, p0, Lorg/apache/lucene/search/BooleanScorer$BucketScorer;->score:D

    double-to-float v0, v0

    return v0
.end method
