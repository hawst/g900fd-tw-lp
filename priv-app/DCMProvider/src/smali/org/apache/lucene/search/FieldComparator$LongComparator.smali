.class public final Lorg/apache/lucene/search/FieldComparator$LongComparator;
.super Lorg/apache/lucene/search/FieldComparator$NumericComparator;
.source "FieldComparator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FieldComparator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LongComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/FieldComparator$NumericComparator",
        "<",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field private bottom:J

.field private currentReaderValues:Lorg/apache/lucene/search/FieldCache$Longs;

.field private final parser:Lorg/apache/lucene/search/FieldCache$LongParser;

.field private final values:[J


# direct methods
.method constructor <init>(ILjava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Ljava/lang/Long;)V
    .locals 1
    .param p1, "numHits"    # I
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "parser"    # Lorg/apache/lucene/search/FieldCache$Parser;
    .param p4, "missingValue"    # Ljava/lang/Long;

    .prologue
    .line 627
    invoke-direct {p0, p2, p4}, Lorg/apache/lucene/search/FieldComparator$NumericComparator;-><init>(Ljava/lang/String;Ljava/lang/Number;)V

    .line 628
    new-array v0, p1, [J

    iput-object v0, p0, Lorg/apache/lucene/search/FieldComparator$LongComparator;->values:[J

    .line 629
    check-cast p3, Lorg/apache/lucene/search/FieldCache$LongParser;

    .end local p3    # "parser":Lorg/apache/lucene/search/FieldCache$Parser;
    iput-object p3, p0, Lorg/apache/lucene/search/FieldComparator$LongComparator;->parser:Lorg/apache/lucene/search/FieldCache$LongParser;

    .line 630
    return-void
.end method


# virtual methods
.method public compare(II)I
    .locals 5
    .param p1, "slot1"    # I
    .param p2, "slot2"    # I

    .prologue
    .line 636
    iget-object v4, p0, Lorg/apache/lucene/search/FieldComparator$LongComparator;->values:[J

    aget-wide v0, v4, p1

    .line 637
    .local v0, "v1":J
    iget-object v4, p0, Lorg/apache/lucene/search/FieldComparator$LongComparator;->values:[J

    aget-wide v2, v4, p2

    .line 638
    .local v2, "v2":J
    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    .line 639
    const/4 v4, 0x1

    .line 643
    :goto_0
    return v4

    .line 640
    :cond_0
    cmp-long v4, v0, v2

    if-gez v4, :cond_1

    .line 641
    const/4 v4, -0x1

    goto :goto_0

    .line 643
    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public compareBottom(I)I
    .locals 4
    .param p1, "doc"    # I

    .prologue
    .line 651
    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$LongComparator;->currentReaderValues:Lorg/apache/lucene/search/FieldCache$Longs;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/search/FieldCache$Longs;->get(I)J

    move-result-wide v0

    .line 654
    .local v0, "v2":J
    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$LongComparator;->docsWithField:Lorg/apache/lucene/util/Bits;

    if-eqz v2, :cond_0

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$LongComparator;->docsWithField:Lorg/apache/lucene/util/Bits;

    invoke-interface {v2, p1}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 655
    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$LongComparator;->missingValue:Ljava/lang/Number;

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 658
    :cond_0
    iget-wide v2, p0, Lorg/apache/lucene/search/FieldComparator$LongComparator;->bottom:J

    cmp-long v2, v2, v0

    if-lez v2, :cond_1

    .line 659
    const/4 v2, 0x1

    .line 663
    :goto_0
    return v2

    .line 660
    :cond_1
    iget-wide v2, p0, Lorg/apache/lucene/search/FieldComparator$LongComparator;->bottom:J

    cmp-long v2, v2, v0

    if-gez v2, :cond_2

    .line 661
    const/4 v2, -0x1

    goto :goto_0

    .line 663
    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public compareDocToValue(ILjava/lang/Long;)I
    .locals 6
    .param p1, "doc"    # I
    .param p2, "valueObj"    # Ljava/lang/Long;

    .prologue
    .line 699
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 700
    .local v2, "value":J
    iget-object v4, p0, Lorg/apache/lucene/search/FieldComparator$LongComparator;->currentReaderValues:Lorg/apache/lucene/search/FieldCache$Longs;

    invoke-virtual {v4, p1}, Lorg/apache/lucene/search/FieldCache$Longs;->get(I)J

    move-result-wide v0

    .line 703
    .local v0, "docValue":J
    iget-object v4, p0, Lorg/apache/lucene/search/FieldComparator$LongComparator;->docsWithField:Lorg/apache/lucene/util/Bits;

    if-eqz v4, :cond_0

    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lorg/apache/lucene/search/FieldComparator$LongComparator;->docsWithField:Lorg/apache/lucene/util/Bits;

    invoke-interface {v4, p1}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v4

    if-nez v4, :cond_0

    .line 704
    iget-object v4, p0, Lorg/apache/lucene/search/FieldComparator$LongComparator;->missingValue:Ljava/lang/Number;

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 706
    :cond_0
    cmp-long v4, v0, v2

    if-gez v4, :cond_1

    .line 707
    const/4 v4, -0x1

    .line 711
    :goto_0
    return v4

    .line 708
    :cond_1
    cmp-long v4, v0, v2

    if-lez v4, :cond_2

    .line 709
    const/4 v4, 0x1

    goto :goto_0

    .line 711
    :cond_2
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public bridge synthetic compareDocToValue(ILjava/lang/Object;)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/search/FieldComparator$LongComparator;->compareDocToValue(ILjava/lang/Long;)I

    move-result v0

    return v0
.end method

.method public copy(II)V
    .locals 4
    .param p1, "slot"    # I
    .param p2, "doc"    # I

    .prologue
    .line 669
    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$LongComparator;->currentReaderValues:Lorg/apache/lucene/search/FieldCache$Longs;

    invoke-virtual {v2, p2}, Lorg/apache/lucene/search/FieldCache$Longs;->get(I)J

    move-result-wide v0

    .line 672
    .local v0, "v2":J
    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$LongComparator;->docsWithField:Lorg/apache/lucene/util/Bits;

    if-eqz v2, :cond_0

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$LongComparator;->docsWithField:Lorg/apache/lucene/util/Bits;

    invoke-interface {v2, p2}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 673
    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$LongComparator;->missingValue:Ljava/lang/Number;

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 676
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$LongComparator;->values:[J

    aput-wide v0, v2, p1

    .line 677
    return-void
.end method

.method public setBottom(I)V
    .locals 2
    .param p1, "bottom"    # I

    .prologue
    .line 689
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$LongComparator;->values:[J

    aget-wide v0, v0, p1

    iput-wide v0, p0, Lorg/apache/lucene/search/FieldComparator$LongComparator;->bottom:J

    .line 690
    return-void
.end method

.method public setNextReader(Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/search/FieldComparator;
    .locals 5
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/AtomicReaderContext;",
            ")",
            "Lorg/apache/lucene/search/FieldComparator",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 683
    sget-object v1, Lorg/apache/lucene/search/FieldCache;->DEFAULT:Lorg/apache/lucene/search/FieldCache;

    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/search/FieldComparator$LongComparator;->field:Ljava/lang/String;

    iget-object v4, p0, Lorg/apache/lucene/search/FieldComparator$LongComparator;->parser:Lorg/apache/lucene/search/FieldCache$LongParser;

    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$LongComparator;->missingValue:Ljava/lang/Number;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v2, v3, v4, v0}, Lorg/apache/lucene/search/FieldCache;->getLongs(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$LongParser;Z)Lorg/apache/lucene/search/FieldCache$Longs;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/FieldComparator$LongComparator;->currentReaderValues:Lorg/apache/lucene/search/FieldCache$Longs;

    .line 684
    invoke-super {p0, p1}, Lorg/apache/lucene/search/FieldComparator$NumericComparator;->setNextReader(Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/search/FieldComparator;

    move-result-object v0

    return-object v0

    .line 683
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public value(I)Ljava/lang/Long;
    .locals 2
    .param p1, "slot"    # I

    .prologue
    .line 694
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$LongComparator;->values:[J

    aget-wide v0, v0, p1

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic value(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/FieldComparator$LongComparator;->value(I)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method
