.class final Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator$AnyOrdComparator;
.super Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator$PerSegmentComparator;
.source "FieldComparator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "AnyOrdComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator$PerSegmentComparator;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final docBase:I

.field private final termsIndex:Lorg/apache/lucene/index/SortedDocValues;

.field final synthetic this$1:Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1008
    const-class v0, Lorg/apache/lucene/search/FieldComparator;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator$AnyOrdComparator;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;Lorg/apache/lucene/index/SortedDocValues;I)V
    .locals 0
    .param p2, "termsIndex"    # Lorg/apache/lucene/index/SortedDocValues;
    .param p3, "docBase"    # I

    .prologue
    .line 1012
    iput-object p1, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator$AnyOrdComparator;->this$1:Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;

    invoke-direct {p0, p1}, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator$PerSegmentComparator;-><init>(Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;)V

    .line 1013
    iput-object p2, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator$AnyOrdComparator;->termsIndex:Lorg/apache/lucene/index/SortedDocValues;

    .line 1014
    iput p3, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator$AnyOrdComparator;->docBase:I

    .line 1015
    return-void
.end method


# virtual methods
.method public compareBottom(I)I
    .locals 3
    .param p1, "doc"    # I

    .prologue
    const/4 v1, -0x1

    .line 1019
    sget-boolean v2, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator$AnyOrdComparator;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator$AnyOrdComparator;->this$1:Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;

    iget v2, v2, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->bottomSlot:I

    if-ne v2, v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 1020
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator$AnyOrdComparator;->termsIndex:Lorg/apache/lucene/index/SortedDocValues;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/index/SortedDocValues;->getOrd(I)I

    move-result v0

    .line 1021
    .local v0, "docOrd":I
    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator$AnyOrdComparator;->this$1:Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;

    iget-boolean v2, v2, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->bottomSameReader:Z

    if-eqz v2, :cond_2

    .line 1023
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator$AnyOrdComparator;->this$1:Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;

    iget v1, v1, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->bottomOrd:I

    sub-int/2addr v1, v0

    .line 1030
    :cond_1
    :goto_0
    return v1

    .line 1024
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator$AnyOrdComparator;->this$1:Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;

    iget v2, v2, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->bottomOrd:I

    if-lt v2, v0, :cond_1

    .line 1028
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public copy(II)V
    .locals 3
    .param p1, "slot"    # I
    .param p2, "doc"    # I

    .prologue
    .line 1036
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator$AnyOrdComparator;->termsIndex:Lorg/apache/lucene/index/SortedDocValues;

    invoke-virtual {v1, p2}, Lorg/apache/lucene/index/SortedDocValues;->getOrd(I)I

    move-result v0

    .line 1037
    .local v0, "ord":I
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator$AnyOrdComparator;->this$1:Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;

    iget-object v1, v1, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->ords:[I

    aput v0, v1, p1

    .line 1038
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 1039
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator$AnyOrdComparator;->this$1:Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;

    iget-object v1, v1, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->values:[Lorg/apache/lucene/util/BytesRef;

    const/4 v2, 0x0

    aput-object v2, v1, p1

    .line 1047
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator$AnyOrdComparator;->this$1:Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;

    iget-object v1, v1, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->readerGen:[I

    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator$AnyOrdComparator;->this$1:Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;

    iget v2, v2, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->currentReaderGen:I

    aput v2, v1, p1

    .line 1048
    return-void

    .line 1041
    :cond_0
    sget-boolean v1, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator$AnyOrdComparator;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    if-gez v0, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 1042
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator$AnyOrdComparator;->this$1:Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;

    iget-object v1, v1, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->values:[Lorg/apache/lucene/util/BytesRef;

    aget-object v1, v1, p1

    if-nez v1, :cond_2

    .line 1043
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator$AnyOrdComparator;->this$1:Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;

    iget-object v1, v1, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->values:[Lorg/apache/lucene/util/BytesRef;

    new-instance v2, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v2}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    aput-object v2, v1, p1

    .line 1045
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator$AnyOrdComparator;->termsIndex:Lorg/apache/lucene/index/SortedDocValues;

    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator$AnyOrdComparator;->this$1:Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;

    iget-object v2, v2, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->values:[Lorg/apache/lucene/util/BytesRef;

    aget-object v2, v2, p1

    invoke-virtual {v1, v0, v2}, Lorg/apache/lucene/index/SortedDocValues;->lookupOrd(ILorg/apache/lucene/util/BytesRef;)V

    goto :goto_0
.end method
