.class public abstract Lorg/apache/lucene/search/similarities/SimilarityBase;
.super Lorg/apache/lucene/search/similarities/Similarity;
.source "SimilarityBase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/similarities/SimilarityBase$BasicExactDocScorer;,
        Lorg/apache/lucene/search/similarities/SimilarityBase$BasicSloppyDocScorer;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final LOG_2:D

.field private static final NORM_TABLE:[F


# instance fields
.field protected discountOverlaps:Z


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/16 v5, 0x100

    .line 46
    const-class v2, Lorg/apache/lucene/search/similarities/SimilarityBase;

    invoke-virtual {v2}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    sput-boolean v2, Lorg/apache/lucene/search/similarities/SimilarityBase;->$assertionsDisabled:Z

    .line 48
    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    sput-wide v2, Lorg/apache/lucene/search/similarities/SimilarityBase;->LOG_2:D

    .line 238
    new-array v2, v5, [F

    sput-object v2, Lorg/apache/lucene/search/similarities/SimilarityBase;->NORM_TABLE:[F

    .line 241
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-lt v1, v5, :cond_1

    .line 245
    return-void

    .line 46
    .end local v1    # "i":I
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 242
    .restart local v1    # "i":I
    :cond_1
    int-to-byte v2, v1

    invoke-static {v2}, Lorg/apache/lucene/util/SmallFloat;->byte315ToFloat(B)F

    move-result v0

    .line 243
    .local v0, "floatNorm":F
    sget-object v2, Lorg/apache/lucene/search/similarities/SimilarityBase;->NORM_TABLE:[F

    const/high16 v3, 0x3f800000    # 1.0f

    mul-float v4, v0, v0

    div-float/2addr v3, v4

    aput v3, v2, v1

    .line 241
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0}, Lorg/apache/lucene/search/similarities/Similarity;-><init>()V

    .line 54
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/search/similarities/SimilarityBase;->discountOverlaps:Z

    .line 60
    return-void
.end method

.method public static log2(D)D
    .locals 4
    .param p0, "x"    # D

    .prologue
    .line 275
    invoke-static {p0, p1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    sget-wide v2, Lorg/apache/lucene/search/similarities/SimilarityBase;->LOG_2:D

    div-double/2addr v0, v2

    return-wide v0
.end method


# virtual methods
.method public computeNorm(Lorg/apache/lucene/index/FieldInvertState;)J
    .locals 4
    .param p1, "state"    # Lorg/apache/lucene/index/FieldInvertState;

    .prologue
    .line 251
    iget-boolean v1, p0, Lorg/apache/lucene/search/similarities/SimilarityBase;->discountOverlaps:Z

    if-eqz v1, :cond_0

    .line 252
    invoke-virtual {p1}, Lorg/apache/lucene/index/FieldInvertState;->getLength()I

    move-result v1

    invoke-virtual {p1}, Lorg/apache/lucene/index/FieldInvertState;->getNumOverlap()I

    move-result v2

    sub-int/2addr v1, v2

    int-to-float v0, v1

    .line 255
    .local v0, "numTerms":F
    :goto_0
    invoke-virtual {p1}, Lorg/apache/lucene/index/FieldInvertState;->getBoost()F

    move-result v1

    invoke-virtual {p0, v1, v0}, Lorg/apache/lucene/search/similarities/SimilarityBase;->encodeNormValue(FF)B

    move-result v1

    int-to-long v2, v1

    return-wide v2

    .line 254
    .end local v0    # "numTerms":F
    :cond_0
    invoke-virtual {p1}, Lorg/apache/lucene/index/FieldInvertState;->getLength()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1}, Lorg/apache/lucene/index/FieldInvertState;->getBoost()F

    move-result v2

    div-float v0, v1, v2

    .restart local v0    # "numTerms":F
    goto :goto_0
.end method

.method public final varargs computeWeight(FLorg/apache/lucene/search/CollectionStatistics;[Lorg/apache/lucene/search/TermStatistics;)Lorg/apache/lucene/search/similarities/Similarity$SimWeight;
    .locals 4
    .param p1, "queryBoost"    # F
    .param p2, "collectionStats"    # Lorg/apache/lucene/search/CollectionStatistics;
    .param p3, "termStats"    # [Lorg/apache/lucene/search/TermStatistics;

    .prologue
    .line 85
    array-length v2, p3

    new-array v1, v2, [Lorg/apache/lucene/search/similarities/BasicStats;

    .line 86
    .local v1, "stats":[Lorg/apache/lucene/search/similarities/BasicStats;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p3

    if-lt v0, v2, :cond_0

    .line 90
    array-length v2, v1

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    const/4 v2, 0x0

    aget-object v2, v1, v2

    :goto_1
    return-object v2

    .line 87
    :cond_0
    invoke-virtual {p2}, Lorg/apache/lucene/search/CollectionStatistics;->field()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2, p1}, Lorg/apache/lucene/search/similarities/SimilarityBase;->newStats(Ljava/lang/String;F)Lorg/apache/lucene/search/similarities/BasicStats;

    move-result-object v2

    aput-object v2, v1, v0

    .line 88
    aget-object v2, v1, v0

    aget-object v3, p3, v0

    invoke-virtual {p0, v2, p2, v3}, Lorg/apache/lucene/search/similarities/SimilarityBase;->fillBasicStats(Lorg/apache/lucene/search/similarities/BasicStats;Lorg/apache/lucene/search/CollectionStatistics;Lorg/apache/lucene/search/TermStatistics;)V

    .line 86
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 90
    :cond_1
    new-instance v2, Lorg/apache/lucene/search/similarities/MultiSimilarity$MultiStats;

    invoke-direct {v2, v1}, Lorg/apache/lucene/search/similarities/MultiSimilarity$MultiStats;-><init>([Lorg/apache/lucene/search/similarities/Similarity$SimWeight;)V

    goto :goto_1
.end method

.method protected decodeNormValue(B)F
    .locals 2
    .param p1, "norm"    # B

    .prologue
    .line 262
    sget-object v0, Lorg/apache/lucene/search/similarities/SimilarityBase;->NORM_TABLE:[F

    and-int/lit16 v1, p1, 0xff

    aget v0, v0, v1

    return v0
.end method

.method protected encodeNormValue(FF)B
    .locals 2
    .param p1, "boost"    # F
    .param p2, "length"    # F

    .prologue
    .line 267
    float-to-double v0, p2

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    div-float v0, p1, v0

    invoke-static {v0}, Lorg/apache/lucene/util/SmallFloat;->floatToByte315(F)B

    move-result v0

    return v0
.end method

.method public exactSimScorer(Lorg/apache/lucene/search/similarities/Similarity$SimWeight;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;
    .locals 7
    .param p1, "stats"    # Lorg/apache/lucene/search/similarities/Similarity$SimWeight;
    .param p2, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 194
    instance-of v4, p1, Lorg/apache/lucene/search/similarities/MultiSimilarity$MultiStats;

    if-eqz v4, :cond_1

    .line 197
    check-cast p1, Lorg/apache/lucene/search/similarities/MultiSimilarity$MultiStats;

    .end local p1    # "stats":Lorg/apache/lucene/search/similarities/Similarity$SimWeight;
    iget-object v3, p1, Lorg/apache/lucene/search/similarities/MultiSimilarity$MultiStats;->subStats:[Lorg/apache/lucene/search/similarities/Similarity$SimWeight;

    .line 198
    .local v3, "subStats":[Lorg/apache/lucene/search/similarities/Similarity$SimWeight;
    array-length v4, v3

    new-array v2, v4, [Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;

    .line 199
    .local v2, "subScorers":[Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v4, v2

    if-lt v1, v4, :cond_0

    .line 203
    new-instance v4, Lorg/apache/lucene/search/similarities/MultiSimilarity$MultiExactDocScorer;

    invoke-direct {v4, v2}, Lorg/apache/lucene/search/similarities/MultiSimilarity$MultiExactDocScorer;-><init>([Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;)V

    .line 206
    .end local v1    # "i":I
    .end local v2    # "subScorers":[Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;
    .end local v3    # "subStats":[Lorg/apache/lucene/search/similarities/Similarity$SimWeight;
    :goto_1
    return-object v4

    .line 200
    .restart local v1    # "i":I
    .restart local v2    # "subScorers":[Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;
    .restart local v3    # "subStats":[Lorg/apache/lucene/search/similarities/Similarity$SimWeight;
    :cond_0
    aget-object v0, v3, v1

    check-cast v0, Lorg/apache/lucene/search/similarities/BasicStats;

    .line 201
    .local v0, "basicstats":Lorg/apache/lucene/search/similarities/BasicStats;
    new-instance v4, Lorg/apache/lucene/search/similarities/SimilarityBase$BasicExactDocScorer;

    invoke-virtual {p2}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v5

    iget-object v6, v0, Lorg/apache/lucene/search/similarities/BasicStats;->field:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lorg/apache/lucene/index/AtomicReader;->getNormValues(Ljava/lang/String;)Lorg/apache/lucene/index/NumericDocValues;

    move-result-object v5

    invoke-direct {v4, p0, v0, v5}, Lorg/apache/lucene/search/similarities/SimilarityBase$BasicExactDocScorer;-><init>(Lorg/apache/lucene/search/similarities/SimilarityBase;Lorg/apache/lucene/search/similarities/BasicStats;Lorg/apache/lucene/index/NumericDocValues;)V

    aput-object v4, v2, v1

    .line 199
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .end local v0    # "basicstats":Lorg/apache/lucene/search/similarities/BasicStats;
    .end local v1    # "i":I
    .end local v2    # "subScorers":[Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;
    .end local v3    # "subStats":[Lorg/apache/lucene/search/similarities/Similarity$SimWeight;
    .restart local p1    # "stats":Lorg/apache/lucene/search/similarities/Similarity$SimWeight;
    :cond_1
    move-object v0, p1

    .line 205
    check-cast v0, Lorg/apache/lucene/search/similarities/BasicStats;

    .line 206
    .restart local v0    # "basicstats":Lorg/apache/lucene/search/similarities/BasicStats;
    new-instance v4, Lorg/apache/lucene/search/similarities/SimilarityBase$BasicExactDocScorer;

    invoke-virtual {p2}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v5

    iget-object v6, v0, Lorg/apache/lucene/search/similarities/BasicStats;->field:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lorg/apache/lucene/index/AtomicReader;->getNormValues(Ljava/lang/String;)Lorg/apache/lucene/index/NumericDocValues;

    move-result-object v5

    invoke-direct {v4, p0, v0, v5}, Lorg/apache/lucene/search/similarities/SimilarityBase$BasicExactDocScorer;-><init>(Lorg/apache/lucene/search/similarities/SimilarityBase;Lorg/apache/lucene/search/similarities/BasicStats;Lorg/apache/lucene/index/NumericDocValues;)V

    goto :goto_1
.end method

.method protected explain(Lorg/apache/lucene/search/similarities/BasicStats;ILorg/apache/lucene/search/Explanation;F)Lorg/apache/lucene/search/Explanation;
    .locals 6
    .param p1, "stats"    # Lorg/apache/lucene/search/similarities/BasicStats;
    .param p2, "doc"    # I
    .param p3, "freq"    # Lorg/apache/lucene/search/Explanation;
    .param p4, "docLen"    # F

    .prologue
    .line 181
    new-instance v1, Lorg/apache/lucene/search/Explanation;

    invoke-direct {v1}, Lorg/apache/lucene/search/Explanation;-><init>()V

    .line 182
    .local v1, "result":Lorg/apache/lucene/search/Explanation;
    invoke-virtual {p3}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v0

    invoke-virtual {p0, p1, v0, p4}, Lorg/apache/lucene/search/similarities/SimilarityBase;->score(Lorg/apache/lucene/search/similarities/BasicStats;FF)F

    move-result v0

    invoke-virtual {v1, v0}, Lorg/apache/lucene/search/Explanation;->setValue(F)V

    .line 183
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "score("

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 184
    const-string v2, ", doc="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", freq="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p3}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "), computed from:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 183
    invoke-virtual {v1, v0}, Lorg/apache/lucene/search/Explanation;->setDescription(Ljava/lang/String;)V

    .line 185
    invoke-virtual {v1, p3}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 187
    invoke-virtual {p3}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v4

    move-object v0, p0

    move-object v2, p1

    move v3, p2

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lorg/apache/lucene/search/similarities/SimilarityBase;->explain(Lorg/apache/lucene/search/Explanation;Lorg/apache/lucene/search/similarities/BasicStats;IFF)V

    .line 189
    return-object v1
.end method

.method protected explain(Lorg/apache/lucene/search/Explanation;Lorg/apache/lucene/search/similarities/BasicStats;IFF)V
    .locals 0
    .param p1, "expl"    # Lorg/apache/lucene/search/Explanation;
    .param p2, "stats"    # Lorg/apache/lucene/search/similarities/BasicStats;
    .param p3, "doc"    # I
    .param p4, "freq"    # F
    .param p5, "docLen"    # F

    .prologue
    .line 162
    return-void
.end method

.method protected fillBasicStats(Lorg/apache/lucene/search/similarities/BasicStats;Lorg/apache/lucene/search/CollectionStatistics;Lorg/apache/lucene/search/TermStatistics;)V
    .locals 18
    .param p1, "stats"    # Lorg/apache/lucene/search/similarities/BasicStats;
    .param p2, "collectionStats"    # Lorg/apache/lucene/search/CollectionStatistics;
    .param p3, "termStats"    # Lorg/apache/lucene/search/TermStatistics;

    .prologue
    .line 102
    sget-boolean v3, Lorg/apache/lucene/search/similarities/SimilarityBase;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/search/CollectionStatistics;->sumTotalTermFreq()J

    move-result-wide v14

    const-wide/16 v16, -0x1

    cmp-long v3, v14, v16

    if-eqz v3, :cond_0

    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/search/CollectionStatistics;->sumTotalTermFreq()J

    move-result-wide v14

    invoke-virtual/range {p3 .. p3}, Lorg/apache/lucene/search/TermStatistics;->totalTermFreq()J

    move-result-wide v16

    cmp-long v3, v14, v16

    if-gez v3, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 103
    :cond_0
    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/search/CollectionStatistics;->maxDoc()J

    move-result-wide v6

    .line 105
    .local v6, "numberOfDocuments":J
    invoke-virtual/range {p3 .. p3}, Lorg/apache/lucene/search/TermStatistics;->docFreq()J

    move-result-wide v4

    .line 106
    .local v4, "docFreq":J
    invoke-virtual/range {p3 .. p3}, Lorg/apache/lucene/search/TermStatistics;->totalTermFreq()J

    move-result-wide v12

    .line 109
    .local v12, "totalTermFreq":J
    const-wide/16 v14, -0x1

    cmp-long v3, v12, v14

    if-nez v3, :cond_1

    .line 110
    move-wide v12, v4

    .line 116
    :cond_1
    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/search/CollectionStatistics;->sumTotalTermFreq()J

    move-result-wide v10

    .line 118
    .local v10, "sumTotalTermFreq":J
    const-wide/16 v14, 0x0

    cmp-long v3, v10, v14

    if-gtz v3, :cond_2

    .line 123
    move-wide v8, v4

    .line 124
    .local v8, "numberOfFieldTokens":J
    const/high16 v2, 0x3f800000    # 1.0f

    .line 131
    .local v2, "avgFieldLength":F
    :goto_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v7}, Lorg/apache/lucene/search/similarities/BasicStats;->setNumberOfDocuments(J)V

    .line 132
    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v9}, Lorg/apache/lucene/search/similarities/BasicStats;->setNumberOfFieldTokens(J)V

    .line 133
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/apache/lucene/search/similarities/BasicStats;->setAvgFieldLength(F)V

    .line 134
    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Lorg/apache/lucene/search/similarities/BasicStats;->setDocFreq(J)V

    .line 135
    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v13}, Lorg/apache/lucene/search/similarities/BasicStats;->setTotalTermFreq(J)V

    .line 136
    return-void

    .line 126
    .end local v2    # "avgFieldLength":F
    .end local v8    # "numberOfFieldTokens":J
    :cond_2
    move-wide v8, v10

    .line 127
    .restart local v8    # "numberOfFieldTokens":J
    long-to-float v3, v8

    long-to-float v14, v6

    div-float v2, v3, v14

    .restart local v2    # "avgFieldLength":F
    goto :goto_0
.end method

.method public getDiscountOverlaps()Z
    .locals 1

    .prologue
    .line 80
    iget-boolean v0, p0, Lorg/apache/lucene/search/similarities/SimilarityBase;->discountOverlaps:Z

    return v0
.end method

.method protected newStats(Ljava/lang/String;F)Lorg/apache/lucene/search/similarities/BasicStats;
    .locals 1
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "queryBoost"    # F

    .prologue
    .line 95
    new-instance v0, Lorg/apache/lucene/search/similarities/BasicStats;

    invoke-direct {v0, p1, p2}, Lorg/apache/lucene/search/similarities/BasicStats;-><init>(Ljava/lang/String;F)V

    return-object v0
.end method

.method protected abstract score(Lorg/apache/lucene/search/similarities/BasicStats;FF)F
.end method

.method public setDiscountOverlaps(Z)V
    .locals 0
    .param p1, "v"    # Z

    .prologue
    .line 72
    iput-boolean p1, p0, Lorg/apache/lucene/search/similarities/SimilarityBase;->discountOverlaps:Z

    .line 73
    return-void
.end method

.method public sloppySimScorer(Lorg/apache/lucene/search/similarities/Similarity$SimWeight;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;
    .locals 7
    .param p1, "stats"    # Lorg/apache/lucene/search/similarities/Similarity$SimWeight;
    .param p2, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 212
    instance-of v4, p1, Lorg/apache/lucene/search/similarities/MultiSimilarity$MultiStats;

    if-eqz v4, :cond_1

    .line 215
    check-cast p1, Lorg/apache/lucene/search/similarities/MultiSimilarity$MultiStats;

    .end local p1    # "stats":Lorg/apache/lucene/search/similarities/Similarity$SimWeight;
    iget-object v3, p1, Lorg/apache/lucene/search/similarities/MultiSimilarity$MultiStats;->subStats:[Lorg/apache/lucene/search/similarities/Similarity$SimWeight;

    .line 216
    .local v3, "subStats":[Lorg/apache/lucene/search/similarities/Similarity$SimWeight;
    array-length v4, v3

    new-array v2, v4, [Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;

    .line 217
    .local v2, "subScorers":[Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v4, v2

    if-lt v1, v4, :cond_0

    .line 221
    new-instance v4, Lorg/apache/lucene/search/similarities/MultiSimilarity$MultiSloppyDocScorer;

    invoke-direct {v4, v2}, Lorg/apache/lucene/search/similarities/MultiSimilarity$MultiSloppyDocScorer;-><init>([Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;)V

    .line 224
    .end local v1    # "i":I
    .end local v2    # "subScorers":[Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;
    .end local v3    # "subStats":[Lorg/apache/lucene/search/similarities/Similarity$SimWeight;
    :goto_1
    return-object v4

    .line 218
    .restart local v1    # "i":I
    .restart local v2    # "subScorers":[Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;
    .restart local v3    # "subStats":[Lorg/apache/lucene/search/similarities/Similarity$SimWeight;
    :cond_0
    aget-object v0, v3, v1

    check-cast v0, Lorg/apache/lucene/search/similarities/BasicStats;

    .line 219
    .local v0, "basicstats":Lorg/apache/lucene/search/similarities/BasicStats;
    new-instance v4, Lorg/apache/lucene/search/similarities/SimilarityBase$BasicSloppyDocScorer;

    invoke-virtual {p2}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v5

    iget-object v6, v0, Lorg/apache/lucene/search/similarities/BasicStats;->field:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lorg/apache/lucene/index/AtomicReader;->getNormValues(Ljava/lang/String;)Lorg/apache/lucene/index/NumericDocValues;

    move-result-object v5

    invoke-direct {v4, p0, v0, v5}, Lorg/apache/lucene/search/similarities/SimilarityBase$BasicSloppyDocScorer;-><init>(Lorg/apache/lucene/search/similarities/SimilarityBase;Lorg/apache/lucene/search/similarities/BasicStats;Lorg/apache/lucene/index/NumericDocValues;)V

    aput-object v4, v2, v1

    .line 217
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .end local v0    # "basicstats":Lorg/apache/lucene/search/similarities/BasicStats;
    .end local v1    # "i":I
    .end local v2    # "subScorers":[Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;
    .end local v3    # "subStats":[Lorg/apache/lucene/search/similarities/Similarity$SimWeight;
    .restart local p1    # "stats":Lorg/apache/lucene/search/similarities/Similarity$SimWeight;
    :cond_1
    move-object v0, p1

    .line 223
    check-cast v0, Lorg/apache/lucene/search/similarities/BasicStats;

    .line 224
    .restart local v0    # "basicstats":Lorg/apache/lucene/search/similarities/BasicStats;
    new-instance v4, Lorg/apache/lucene/search/similarities/SimilarityBase$BasicSloppyDocScorer;

    invoke-virtual {p2}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v5

    iget-object v6, v0, Lorg/apache/lucene/search/similarities/BasicStats;->field:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lorg/apache/lucene/index/AtomicReader;->getNormValues(Ljava/lang/String;)Lorg/apache/lucene/index/NumericDocValues;

    move-result-object v5

    invoke-direct {v4, p0, v0, v5}, Lorg/apache/lucene/search/similarities/SimilarityBase$BasicSloppyDocScorer;-><init>(Lorg/apache/lucene/search/similarities/SimilarityBase;Lorg/apache/lucene/search/similarities/BasicStats;Lorg/apache/lucene/index/NumericDocValues;)V

    goto :goto_1
.end method

.method public abstract toString()Ljava/lang/String;
.end method
