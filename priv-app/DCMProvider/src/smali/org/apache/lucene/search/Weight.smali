.class public abstract Lorg/apache/lucene/search/Weight;
.super Ljava/lang/Object;
.source "Weight.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract explain(Lorg/apache/lucene/index/AtomicReaderContext;I)Lorg/apache/lucene/search/Explanation;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getQuery()Lorg/apache/lucene/search/Query;
.end method

.method public abstract getValueForNormalization()F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract normalize(FF)V
.end method

.method public abstract scorer(Lorg/apache/lucene/index/AtomicReaderContext;ZZLorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/Scorer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public scoresDocsOutOfOrder()Z
    .locals 1

    .prologue
    .line 127
    const/4 v0, 0x0

    return v0
.end method
