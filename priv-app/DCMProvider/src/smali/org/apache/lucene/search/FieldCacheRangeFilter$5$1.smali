.class Lorg/apache/lucene/search/FieldCacheRangeFilter$5$1;
.super Lorg/apache/lucene/search/FieldCacheDocIdSet;
.source "FieldCacheRangeFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/FieldCacheRangeFilter$5;->getDocIdSet(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/DocIdSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lorg/apache/lucene/search/FieldCacheRangeFilter$5;

.field private final synthetic val$inclusiveLowerPoint:I

.field private final synthetic val$inclusiveUpperPoint:I

.field private final synthetic val$values:Lorg/apache/lucene/search/FieldCache$Ints;


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/FieldCacheRangeFilter$5;ILorg/apache/lucene/util/Bits;Lorg/apache/lucene/search/FieldCache$Ints;II)V
    .locals 0
    .param p2, "$anonymous0"    # I
    .param p3, "$anonymous1"    # Lorg/apache/lucene/util/Bits;

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$5$1;->this$1:Lorg/apache/lucene/search/FieldCacheRangeFilter$5;

    iput-object p4, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$5$1;->val$values:Lorg/apache/lucene/search/FieldCache$Ints;

    iput p5, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$5$1;->val$inclusiveLowerPoint:I

    iput p6, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$5$1;->val$inclusiveUpperPoint:I

    .line 339
    invoke-direct {p0, p2, p3}, Lorg/apache/lucene/search/FieldCacheDocIdSet;-><init>(ILorg/apache/lucene/util/Bits;)V

    return-void
.end method


# virtual methods
.method protected matchDoc(I)Z
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 342
    iget-object v1, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$5$1;->val$values:Lorg/apache/lucene/search/FieldCache$Ints;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/FieldCache$Ints;->get(I)I

    move-result v0

    .line 343
    .local v0, "value":I
    iget v1, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$5$1;->val$inclusiveLowerPoint:I

    if-lt v0, v1, :cond_0

    iget v1, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$5$1;->val$inclusiveUpperPoint:I

    if-gt v0, v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
