.class public Lorg/apache/lucene/search/similarities/NormalizationH2;
.super Lorg/apache/lucene/search/similarities/Normalization;
.source "NormalizationH2.java"


# instance fields
.field private final c:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 48
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-direct {p0, v0}, Lorg/apache/lucene/search/similarities/NormalizationH2;-><init>(F)V

    .line 49
    return-void
.end method

.method public constructor <init>(F)V
    .locals 0
    .param p1, "c"    # F

    .prologue
    .line 40
    invoke-direct {p0}, Lorg/apache/lucene/search/similarities/Normalization;-><init>()V

    .line 41
    iput p1, p0, Lorg/apache/lucene/search/similarities/NormalizationH2;->c:F

    .line 42
    return-void
.end method


# virtual methods
.method public getC()F
    .locals 1

    .prologue
    .line 66
    iget v0, p0, Lorg/apache/lucene/search/similarities/NormalizationH2;->c:F

    return v0
.end method

.method public final tfn(Lorg/apache/lucene/search/similarities/BasicStats;FF)F
    .locals 5
    .param p1, "stats"    # Lorg/apache/lucene/search/similarities/BasicStats;
    .param p2, "tf"    # F
    .param p3, "len"    # F

    .prologue
    .line 53
    float-to-double v0, p2

    const/high16 v2, 0x3f800000    # 1.0f

    iget v3, p0, Lorg/apache/lucene/search/similarities/NormalizationH2;->c:F

    invoke-virtual {p1}, Lorg/apache/lucene/search/similarities/BasicStats;->getAvgFieldLength()F

    move-result v4

    mul-float/2addr v3, v4

    div-float/2addr v3, p3

    add-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Lorg/apache/lucene/search/similarities/SimilarityBase;->log2(D)D

    move-result-wide v2

    mul-double/2addr v0, v2

    double-to-float v0, v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    const-string v0, "2"

    return-object v0
.end method
