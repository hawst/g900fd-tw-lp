.class abstract Lorg/apache/lucene/search/DisjunctionScorer;
.super Lorg/apache/lucene/search/Scorer;
.source "DisjunctionScorer.java"


# instance fields
.field protected numScorers:I

.field protected final subScorers:[Lorg/apache/lucene/search/Scorer;


# direct methods
.method protected constructor <init>(Lorg/apache/lucene/search/Weight;[Lorg/apache/lucene/search/Scorer;I)V
    .locals 0
    .param p1, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p2, "subScorers"    # [Lorg/apache/lucene/search/Scorer;
    .param p3, "numScorers"    # I

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/Scorer;-><init>(Lorg/apache/lucene/search/Weight;)V

    .line 33
    iput-object p2, p0, Lorg/apache/lucene/search/DisjunctionScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    .line 34
    iput p3, p0, Lorg/apache/lucene/search/DisjunctionScorer;->numScorers:I

    .line 35
    invoke-virtual {p0}, Lorg/apache/lucene/search/DisjunctionScorer;->heapify()V

    .line 36
    return-void
.end method


# virtual methods
.method public cost()J
    .locals 6

    .prologue
    .line 111
    const-wide/16 v2, 0x0

    .line 112
    .local v2, "sum":J
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lorg/apache/lucene/search/DisjunctionScorer;->numScorers:I

    if-lt v0, v1, :cond_0

    .line 115
    return-wide v2

    .line 113
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/DisjunctionScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lorg/apache/lucene/search/Scorer;->cost()J

    move-result-wide v4

    add-long/2addr v2, v4

    .line 112
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final getChildren()Ljava/util/Collection;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/search/Scorer$ChildScorer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 102
    new-instance v0, Ljava/util/ArrayList;

    iget v2, p0, Lorg/apache/lucene/search/DisjunctionScorer;->numScorers:I

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 103
    .local v0, "children":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/search/Scorer$ChildScorer;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v2, p0, Lorg/apache/lucene/search/DisjunctionScorer;->numScorers:I

    if-lt v1, v2, :cond_0

    .line 106
    return-object v0

    .line 104
    :cond_0
    new-instance v2, Lorg/apache/lucene/search/Scorer$ChildScorer;

    iget-object v3, p0, Lorg/apache/lucene/search/DisjunctionScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v3, v3, v1

    const-string v4, "SHOULD"

    invoke-direct {v2, v3, v4}, Lorg/apache/lucene/search/Scorer$ChildScorer;-><init>(Lorg/apache/lucene/search/Scorer;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 103
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method protected final heapAdjust(I)V
    .locals 10
    .param p1, "root"    # I

    .prologue
    .line 52
    iget-object v9, p0, Lorg/apache/lucene/search/DisjunctionScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v8, v9, p1

    .line 53
    .local v8, "scorer":Lorg/apache/lucene/search/Scorer;
    invoke-virtual {v8}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v0

    .line 54
    .local v0, "doc":I
    move v1, p1

    .line 55
    .local v1, "i":I
    :goto_0
    iget v9, p0, Lorg/apache/lucene/search/DisjunctionScorer;->numScorers:I

    shr-int/lit8 v9, v9, 0x1

    add-int/lit8 v9, v9, -0x1

    if-le v1, v9, :cond_1

    .line 83
    :cond_0
    return-void

    .line 56
    :cond_1
    shl-int/lit8 v9, v1, 0x1

    add-int/lit8 v2, v9, 0x1

    .line 57
    .local v2, "lchild":I
    iget-object v9, p0, Lorg/apache/lucene/search/DisjunctionScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v4, v9, v2

    .line 58
    .local v4, "lscorer":Lorg/apache/lucene/search/Scorer;
    invoke-virtual {v4}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v3

    .line 59
    .local v3, "ldoc":I
    const v6, 0x7fffffff

    .local v6, "rdoc":I
    shl-int/lit8 v9, v1, 0x1

    add-int/lit8 v5, v9, 0x2

    .line 60
    .local v5, "rchild":I
    const/4 v7, 0x0

    .line 61
    .local v7, "rscorer":Lorg/apache/lucene/search/Scorer;
    iget v9, p0, Lorg/apache/lucene/search/DisjunctionScorer;->numScorers:I

    if-ge v5, v9, :cond_2

    .line 62
    iget-object v9, p0, Lorg/apache/lucene/search/DisjunctionScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v7, v9, v5

    .line 63
    invoke-virtual {v7}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v6

    .line 65
    :cond_2
    if-ge v3, v0, :cond_4

    .line 66
    if-ge v6, v3, :cond_3

    .line 67
    iget-object v9, p0, Lorg/apache/lucene/search/DisjunctionScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aput-object v7, v9, v1

    .line 68
    iget-object v9, p0, Lorg/apache/lucene/search/DisjunctionScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aput-object v8, v9, v5

    .line 69
    move v1, v5

    .line 70
    goto :goto_0

    .line 71
    :cond_3
    iget-object v9, p0, Lorg/apache/lucene/search/DisjunctionScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aput-object v4, v9, v1

    .line 72
    iget-object v9, p0, Lorg/apache/lucene/search/DisjunctionScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aput-object v8, v9, v2

    .line 73
    move v1, v2

    .line 75
    goto :goto_0

    :cond_4
    if-ge v6, v0, :cond_0

    .line 76
    iget-object v9, p0, Lorg/apache/lucene/search/DisjunctionScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aput-object v7, v9, v1

    .line 77
    iget-object v9, p0, Lorg/apache/lucene/search/DisjunctionScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aput-object v8, v9, v5

    .line 78
    move v1, v5

    .line 79
    goto :goto_0
.end method

.method protected final heapRemoveRoot()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 89
    iget v0, p0, Lorg/apache/lucene/search/DisjunctionScorer;->numScorers:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 90
    iget-object v0, p0, Lorg/apache/lucene/search/DisjunctionScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aput-object v4, v0, v3

    .line 91
    iput v3, p0, Lorg/apache/lucene/search/DisjunctionScorer;->numScorers:I

    .line 98
    :goto_0
    return-void

    .line 93
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/DisjunctionScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    iget-object v1, p0, Lorg/apache/lucene/search/DisjunctionScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    iget v2, p0, Lorg/apache/lucene/search/DisjunctionScorer;->numScorers:I

    add-int/lit8 v2, v2, -0x1

    aget-object v1, v1, v2

    aput-object v1, v0, v3

    .line 94
    iget-object v0, p0, Lorg/apache/lucene/search/DisjunctionScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    iget v1, p0, Lorg/apache/lucene/search/DisjunctionScorer;->numScorers:I

    add-int/lit8 v1, v1, -0x1

    aput-object v4, v0, v1

    .line 95
    iget v0, p0, Lorg/apache/lucene/search/DisjunctionScorer;->numScorers:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/search/DisjunctionScorer;->numScorers:I

    .line 96
    invoke-virtual {p0, v3}, Lorg/apache/lucene/search/DisjunctionScorer;->heapAdjust(I)V

    goto :goto_0
.end method

.method protected final heapify()V
    .locals 2

    .prologue
    .line 42
    iget v1, p0, Lorg/apache/lucene/search/DisjunctionScorer;->numScorers:I

    shr-int/lit8 v1, v1, 0x1

    add-int/lit8 v0, v1, -0x1

    .local v0, "i":I
    :goto_0
    if-gez v0, :cond_0

    .line 45
    return-void

    .line 43
    :cond_0
    invoke-virtual {p0, v0}, Lorg/apache/lucene/search/DisjunctionScorer;->heapAdjust(I)V

    .line 42
    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method
