.class public Lorg/apache/lucene/search/FieldValueHitQueue$Entry;
.super Lorg/apache/lucene/search/ScoreDoc;
.source "FieldValueHitQueue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FieldValueHitQueue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Entry"
.end annotation


# instance fields
.field public slot:I


# direct methods
.method public constructor <init>(IIF)V
    .locals 0
    .param p1, "slot"    # I
    .param p2, "doc"    # I
    .param p3, "score"    # F

    .prologue
    .line 44
    invoke-direct {p0, p2, p3}, Lorg/apache/lucene/search/ScoreDoc;-><init>(IF)V

    .line 45
    iput p1, p0, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->slot:I

    .line 46
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 50
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "slot:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->slot:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-super {p0}, Lorg/apache/lucene/search/ScoreDoc;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
