.class Lorg/apache/lucene/search/FieldCacheRewriteMethod$MultiTermQueryFieldCacheWrapperFilter$2;
.super Lorg/apache/lucene/search/FieldCacheDocIdSet;
.source "FieldCacheRewriteMethod.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/FieldCacheRewriteMethod$MultiTermQueryFieldCacheWrapperFilter;->getDocIdSet(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/DocIdSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lorg/apache/lucene/search/FieldCacheRewriteMethod$MultiTermQueryFieldCacheWrapperFilter;

.field private final synthetic val$fcsi:Lorg/apache/lucene/index/SortedDocValues;

.field private final synthetic val$termSet:Lorg/apache/lucene/util/OpenBitSet;


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/FieldCacheRewriteMethod$MultiTermQueryFieldCacheWrapperFilter;ILorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/SortedDocValues;Lorg/apache/lucene/util/OpenBitSet;)V
    .locals 0
    .param p2, "$anonymous0"    # I
    .param p3, "$anonymous1"    # Lorg/apache/lucene/util/Bits;

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/search/FieldCacheRewriteMethod$MultiTermQueryFieldCacheWrapperFilter$2;->this$1:Lorg/apache/lucene/search/FieldCacheRewriteMethod$MultiTermQueryFieldCacheWrapperFilter;

    iput-object p4, p0, Lorg/apache/lucene/search/FieldCacheRewriteMethod$MultiTermQueryFieldCacheWrapperFilter$2;->val$fcsi:Lorg/apache/lucene/index/SortedDocValues;

    iput-object p5, p0, Lorg/apache/lucene/search/FieldCacheRewriteMethod$MultiTermQueryFieldCacheWrapperFilter$2;->val$termSet:Lorg/apache/lucene/util/OpenBitSet;

    .line 152
    invoke-direct {p0, p2, p3}, Lorg/apache/lucene/search/FieldCacheDocIdSet;-><init>(ILorg/apache/lucene/util/Bits;)V

    return-void
.end method


# virtual methods
.method protected final matchDoc(I)Z
    .locals 2
    .param p1, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ArrayIndexOutOfBoundsException;
        }
    .end annotation

    .prologue
    .line 155
    iget-object v1, p0, Lorg/apache/lucene/search/FieldCacheRewriteMethod$MultiTermQueryFieldCacheWrapperFilter$2;->val$fcsi:Lorg/apache/lucene/index/SortedDocValues;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/index/SortedDocValues;->getOrd(I)I

    move-result v0

    .line 156
    .local v0, "ord":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 157
    const/4 v1, 0x0

    .line 159
    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/FieldCacheRewriteMethod$MultiTermQueryFieldCacheWrapperFilter$2;->val$termSet:Lorg/apache/lucene/util/OpenBitSet;

    invoke-virtual {v1, v0}, Lorg/apache/lucene/util/OpenBitSet;->get(I)Z

    move-result v1

    goto :goto_0
.end method
