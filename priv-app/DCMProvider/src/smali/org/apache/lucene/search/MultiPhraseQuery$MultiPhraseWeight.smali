.class Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;
.super Lorg/apache/lucene/search/Weight;
.source "MultiPhraseQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/MultiPhraseQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MultiPhraseWeight"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final similarity:Lorg/apache/lucene/search/similarities/Similarity;

.field private final stats:Lorg/apache/lucene/search/similarities/Similarity$SimWeight;

.field private final termContexts:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lorg/apache/lucene/index/Term;",
            "Lorg/apache/lucene/index/TermContext;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lorg/apache/lucene/search/MultiPhraseQuery;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 136
    const-class v0, Lorg/apache/lucene/search/MultiPhraseQuery;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/search/MultiPhraseQuery;Lorg/apache/lucene/search/IndexSearcher;)V
    .locals 9
    .param p2, "searcher"    # Lorg/apache/lucene/search/IndexSearcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 142
    iput-object p1, p0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->this$0:Lorg/apache/lucene/search/MultiPhraseQuery;

    .line 141
    invoke-direct {p0}, Lorg/apache/lucene/search/Weight;-><init>()V

    .line 139
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    iput-object v5, p0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->termContexts:Ljava/util/Map;

    .line 143
    invoke-virtual {p2}, Lorg/apache/lucene/search/IndexSearcher;->getSimilarity()Lorg/apache/lucene/search/similarities/Similarity;

    move-result-object v5

    iput-object v5, p0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->similarity:Lorg/apache/lucene/search/similarities/Similarity;

    .line 144
    invoke-virtual {p2}, Lorg/apache/lucene/search/IndexSearcher;->getTopReaderContext()Lorg/apache/lucene/index/IndexReaderContext;

    move-result-object v1

    .line 147
    .local v1, "context":Lorg/apache/lucene/index/IndexReaderContext;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 148
    .local v0, "allTermStats":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/search/TermStatistics;>;"
    # getter for: Lorg/apache/lucene/search/MultiPhraseQuery;->termArrays:Ljava/util/ArrayList;
    invoke-static {p1}, Lorg/apache/lucene/search/MultiPhraseQuery;->access$0(Lorg/apache/lucene/search/MultiPhraseQuery;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_1

    .line 158
    iget-object v6, p0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->similarity:Lorg/apache/lucene/search/similarities/Similarity;

    invoke-virtual {p1}, Lorg/apache/lucene/search/MultiPhraseQuery;->getBoost()F

    move-result v7

    .line 159
    # getter for: Lorg/apache/lucene/search/MultiPhraseQuery;->field:Ljava/lang/String;
    invoke-static {p1}, Lorg/apache/lucene/search/MultiPhraseQuery;->access$1(Lorg/apache/lucene/search/MultiPhraseQuery;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2, v5}, Lorg/apache/lucene/search/IndexSearcher;->collectionStatistics(Ljava/lang/String;)Lorg/apache/lucene/search/CollectionStatistics;

    move-result-object v8

    .line 160
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    new-array v5, v5, [Lorg/apache/lucene/search/TermStatistics;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Lorg/apache/lucene/search/TermStatistics;

    .line 158
    invoke-virtual {v6, v7, v8, v5}, Lorg/apache/lucene/search/similarities/Similarity;->computeWeight(FLorg/apache/lucene/search/CollectionStatistics;[Lorg/apache/lucene/search/TermStatistics;)Lorg/apache/lucene/search/similarities/Similarity$SimWeight;

    move-result-object v5

    iput-object v5, p0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->stats:Lorg/apache/lucene/search/similarities/Similarity$SimWeight;

    .line 161
    return-void

    .line 148
    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Lorg/apache/lucene/index/Term;

    .line 149
    .local v4, "terms":[Lorg/apache/lucene/index/Term;
    array-length v7, v4

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v7, :cond_0

    aget-object v2, v4, v5

    .line 150
    .local v2, "term":Lorg/apache/lucene/index/Term;
    iget-object v8, p0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->termContexts:Ljava/util/Map;

    invoke-interface {v8, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/index/TermContext;

    .line 151
    .local v3, "termContext":Lorg/apache/lucene/index/TermContext;
    if-nez v3, :cond_2

    .line 152
    const/4 v8, 0x1

    invoke-static {v1, v2, v8}, Lorg/apache/lucene/index/TermContext;->build(Lorg/apache/lucene/index/IndexReaderContext;Lorg/apache/lucene/index/Term;Z)Lorg/apache/lucene/index/TermContext;

    move-result-object v3

    .line 153
    iget-object v8, p0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->termContexts:Ljava/util/Map;

    invoke-interface {v8, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 155
    :cond_2
    invoke-virtual {p2, v2, v3}, Lorg/apache/lucene/search/IndexSearcher;->termStatistics(Lorg/apache/lucene/index/Term;Lorg/apache/lucene/index/TermContext;)Lorg/apache/lucene/search/TermStatistics;

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 149
    add-int/lit8 v5, v5, 0x1

    goto :goto_0
.end method


# virtual methods
.method public explain(Lorg/apache/lucene/index/AtomicReaderContext;I)Lorg/apache/lucene/search/Explanation;
    .locals 10
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 261
    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/lucene/index/AtomicReader;->getLiveDocs()Lorg/apache/lucene/util/Bits;

    move-result-object v6

    invoke-virtual {p0, p1, v9, v8, v6}, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->scorer(Lorg/apache/lucene/index/AtomicReaderContext;ZZLorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/Scorer;

    move-result-object v5

    .line 262
    .local v5, "scorer":Lorg/apache/lucene/search/Scorer;
    if-eqz v5, :cond_1

    .line 263
    invoke-virtual {v5, p2}, Lorg/apache/lucene/search/Scorer;->advance(I)I

    move-result v2

    .line 264
    .local v2, "newDoc":I
    if-ne v2, p2, :cond_1

    .line 265
    iget-object v6, p0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->this$0:Lorg/apache/lucene/search/MultiPhraseQuery;

    # getter for: Lorg/apache/lucene/search/MultiPhraseQuery;->slop:I
    invoke-static {v6}, Lorg/apache/lucene/search/MultiPhraseQuery;->access$3(Lorg/apache/lucene/search/MultiPhraseQuery;)I

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {v5}, Lorg/apache/lucene/search/Scorer;->freq()I

    move-result v6

    int-to-float v1, v6

    .line 266
    .end local v5    # "scorer":Lorg/apache/lucene/search/Scorer;
    .local v1, "freq":F
    :goto_0
    iget-object v6, p0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->similarity:Lorg/apache/lucene/search/similarities/Similarity;

    iget-object v7, p0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->stats:Lorg/apache/lucene/search/similarities/Similarity$SimWeight;

    invoke-virtual {v6, v7, p1}, Lorg/apache/lucene/search/similarities/Similarity;->sloppySimScorer(Lorg/apache/lucene/search/similarities/Similarity$SimWeight;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;

    move-result-object v0

    .line 267
    .local v0, "docScorer":Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;
    new-instance v3, Lorg/apache/lucene/search/ComplexExplanation;

    invoke-direct {v3}, Lorg/apache/lucene/search/ComplexExplanation;-><init>()V

    .line 268
    .local v3, "result":Lorg/apache/lucene/search/ComplexExplanation;
    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "weight("

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " in "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ") ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->similarity:Lorg/apache/lucene/search/similarities/Similarity;

    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "], result of:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Lorg/apache/lucene/search/ComplexExplanation;->setDescription(Ljava/lang/String;)V

    .line 269
    new-instance v6, Lorg/apache/lucene/search/Explanation;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "phraseFreq="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v1, v7}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    invoke-virtual {v0, p2, v6}, Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;->explain(ILorg/apache/lucene/search/Explanation;)Lorg/apache/lucene/search/Explanation;

    move-result-object v4

    .line 270
    .local v4, "scoreExplanation":Lorg/apache/lucene/search/Explanation;
    invoke-virtual {v3, v4}, Lorg/apache/lucene/search/ComplexExplanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 271
    invoke-virtual {v4}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v6

    invoke-virtual {v3, v6}, Lorg/apache/lucene/search/ComplexExplanation;->setValue(F)V

    .line 272
    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v3, v6}, Lorg/apache/lucene/search/ComplexExplanation;->setMatch(Ljava/lang/Boolean;)V

    .line 277
    .end local v0    # "docScorer":Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;
    .end local v1    # "freq":F
    .end local v2    # "newDoc":I
    .end local v3    # "result":Lorg/apache/lucene/search/ComplexExplanation;
    .end local v4    # "scoreExplanation":Lorg/apache/lucene/search/Explanation;
    :goto_1
    return-object v3

    .line 265
    .restart local v2    # "newDoc":I
    .restart local v5    # "scorer":Lorg/apache/lucene/search/Scorer;
    :cond_0
    check-cast v5, Lorg/apache/lucene/search/SloppyPhraseScorer;

    .end local v5    # "scorer":Lorg/apache/lucene/search/Scorer;
    invoke-virtual {v5}, Lorg/apache/lucene/search/SloppyPhraseScorer;->sloppyFreq()F

    move-result v1

    goto :goto_0

    .line 277
    .end local v2    # "newDoc":I
    .restart local v5    # "scorer":Lorg/apache/lucene/search/Scorer;
    :cond_1
    new-instance v3, Lorg/apache/lucene/search/ComplexExplanation;

    const/4 v6, 0x0

    const-string v7, "no matching term"

    invoke-direct {v3, v8, v6, v7}, Lorg/apache/lucene/search/ComplexExplanation;-><init>(ZFLjava/lang/String;)V

    goto :goto_1
.end method

.method public getQuery()Lorg/apache/lucene/search/Query;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->this$0:Lorg/apache/lucene/search/MultiPhraseQuery;

    return-object v0
.end method

.method public getValueForNormalization()F
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->stats:Lorg/apache/lucene/search/similarities/Similarity$SimWeight;

    invoke-virtual {v0}, Lorg/apache/lucene/search/similarities/Similarity$SimWeight;->getValueForNormalization()F

    move-result v0

    return v0
.end method

.method public normalize(FF)V
    .locals 1
    .param p1, "queryNorm"    # F
    .param p2, "topLevelBoost"    # F

    .prologue
    .line 173
    iget-object v0, p0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->stats:Lorg/apache/lucene/search/similarities/Similarity$SimWeight;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/search/similarities/Similarity$SimWeight;->normalize(FF)V

    .line 174
    return-void
.end method

.method public scorer(Lorg/apache/lucene/index/AtomicReaderContext;ZZLorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/Scorer;
    .locals 18
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "scoreDocsInOrder"    # Z
    .param p3, "topScorer"    # Z
    .param p4, "acceptDocs"    # Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 179
    sget-boolean v4, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->this$0:Lorg/apache/lucene/search/MultiPhraseQuery;

    # getter for: Lorg/apache/lucene/search/MultiPhraseQuery;->termArrays:Ljava/util/ArrayList;
    invoke-static {v4}, Lorg/apache/lucene/search/MultiPhraseQuery;->access$0(Lorg/apache/lucene/search/MultiPhraseQuery;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_0

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 180
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v12

    .line 181
    .local v12, "reader":Lorg/apache/lucene/index/AtomicReader;
    move-object/from16 v3, p4

    .line 183
    .local v3, "liveDocs":Lorg/apache/lucene/util/Bits;
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->this$0:Lorg/apache/lucene/search/MultiPhraseQuery;

    # getter for: Lorg/apache/lucene/search/MultiPhraseQuery;->termArrays:Ljava/util/ArrayList;
    invoke-static {v4}, Lorg/apache/lucene/search/MultiPhraseQuery;->access$0(Lorg/apache/lucene/search/MultiPhraseQuery;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v11, v4, [Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;

    .line 185
    .local v11, "postingsFreqs":[Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->this$0:Lorg/apache/lucene/search/MultiPhraseQuery;

    # getter for: Lorg/apache/lucene/search/MultiPhraseQuery;->field:Ljava/lang/String;
    invoke-static {v4}, Lorg/apache/lucene/search/MultiPhraseQuery;->access$1(Lorg/apache/lucene/search/MultiPhraseQuery;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v12, v4}, Lorg/apache/lucene/index/AtomicReader;->terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v9

    .line 186
    .local v9, "fieldTerms":Lorg/apache/lucene/index/Terms;
    if-nez v9, :cond_2

    .line 187
    const/4 v13, 0x0

    .line 255
    :cond_1
    :goto_0
    return-object v13

    .line 191
    :cond_2
    const/4 v4, 0x0

    invoke-virtual {v9, v4}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v7

    .line 193
    .local v7, "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    const/4 v10, 0x0

    .local v10, "pos":I
    :goto_1
    array-length v4, v11

    if-lt v10, v4, :cond_4

    .line 243
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->this$0:Lorg/apache/lucene/search/MultiPhraseQuery;

    # getter for: Lorg/apache/lucene/search/MultiPhraseQuery;->slop:I
    invoke-static {v4}, Lorg/apache/lucene/search/MultiPhraseQuery;->access$3(Lorg/apache/lucene/search/MultiPhraseQuery;)I

    move-result v4

    if-nez v4, :cond_3

    .line 244
    invoke-static {v11}, Lorg/apache/lucene/util/ArrayUtil;->mergeSort([Ljava/lang/Comparable;)V

    .line 247
    :cond_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->this$0:Lorg/apache/lucene/search/MultiPhraseQuery;

    # getter for: Lorg/apache/lucene/search/MultiPhraseQuery;->slop:I
    invoke-static {v4}, Lorg/apache/lucene/search/MultiPhraseQuery;->access$3(Lorg/apache/lucene/search/MultiPhraseQuery;)I

    move-result v4

    if-nez v4, :cond_c

    .line 248
    new-instance v13, Lorg/apache/lucene/search/ExactPhraseScorer;

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->similarity:Lorg/apache/lucene/search/similarities/Similarity;

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->stats:Lorg/apache/lucene/search/similarities/Similarity$SimWeight;

    move-object/from16 v0, p1

    invoke-virtual {v4, v6, v0}, Lorg/apache/lucene/search/similarities/Similarity;->exactSimScorer(Lorg/apache/lucene/search/similarities/Similarity$SimWeight;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v13, v0, v11, v4}, Lorg/apache/lucene/search/ExactPhraseScorer;-><init>(Lorg/apache/lucene/search/Weight;[Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;)V

    .line 249
    .local v13, "s":Lorg/apache/lucene/search/ExactPhraseScorer;
    iget-boolean v4, v13, Lorg/apache/lucene/search/ExactPhraseScorer;->noDocs:Z

    if-eqz v4, :cond_1

    .line 250
    const/4 v13, 0x0

    goto :goto_0

    .line 194
    .end local v13    # "s":Lorg/apache/lucene/search/ExactPhraseScorer;
    :cond_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->this$0:Lorg/apache/lucene/search/MultiPhraseQuery;

    # getter for: Lorg/apache/lucene/search/MultiPhraseQuery;->termArrays:Ljava/util/ArrayList;
    invoke-static {v4}, Lorg/apache/lucene/search/MultiPhraseQuery;->access$0(Lorg/apache/lucene/search/MultiPhraseQuery;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Lorg/apache/lucene/index/Term;

    .line 199
    .local v5, "terms":[Lorg/apache/lucene/index/Term;
    array-length v4, v5

    const/4 v6, 0x1

    if-le v4, v6, :cond_7

    .line 200
    new-instance v2, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum;

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->termContexts:Ljava/util/Map;

    move-object/from16 v4, p1

    invoke-direct/range {v2 .. v7}, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum;-><init>(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/AtomicReaderContext;[Lorg/apache/lucene/index/Term;Ljava/util/Map;Lorg/apache/lucene/index/TermsEnum;)V

    .line 204
    .local v2, "postingsEnum":Lorg/apache/lucene/index/DocsAndPositionsEnum;
    const/4 v8, 0x0

    .line 205
    .local v8, "docFreq":I
    const/4 v15, 0x0

    .local v15, "termIdx":I
    :goto_2
    array-length v4, v5

    if-lt v15, v4, :cond_5

    .line 216
    if-nez v8, :cond_b

    .line 218
    const/4 v13, 0x0

    goto :goto_0

    .line 206
    :cond_5
    aget-object v14, v5, v15

    .line 207
    .local v14, "term":Lorg/apache/lucene/index/Term;
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->termContexts:Ljava/util/Map;

    invoke-interface {v4, v14}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/index/TermContext;

    move-object/from16 v0, p1

    iget v6, v0, Lorg/apache/lucene/index/AtomicReaderContext;->ord:I

    invoke-virtual {v4, v6}, Lorg/apache/lucene/index/TermContext;->get(I)Lorg/apache/lucene/index/TermState;

    move-result-object v16

    .line 208
    .local v16, "termState":Lorg/apache/lucene/index/TermState;
    if-nez v16, :cond_6

    .line 205
    :goto_3
    add-int/lit8 v15, v15, 0x1

    goto :goto_2

    .line 212
    :cond_6
    invoke-virtual {v14}, Lorg/apache/lucene/index/Term;->bytes()Lorg/apache/lucene/util/BytesRef;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-virtual {v7, v4, v0}, Lorg/apache/lucene/index/TermsEnum;->seekExact(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/index/TermState;)V

    .line 213
    invoke-virtual {v7}, Lorg/apache/lucene/index/TermsEnum;->docFreq()I

    move-result v4

    add-int/2addr v8, v4

    goto :goto_3

    .line 221
    .end local v2    # "postingsEnum":Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .end local v8    # "docFreq":I
    .end local v14    # "term":Lorg/apache/lucene/index/Term;
    .end local v15    # "termIdx":I
    .end local v16    # "termState":Lorg/apache/lucene/index/TermState;
    :cond_7
    const/4 v4, 0x0

    aget-object v14, v5, v4

    .line 222
    .restart local v14    # "term":Lorg/apache/lucene/index/Term;
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->termContexts:Ljava/util/Map;

    invoke-interface {v4, v14}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/index/TermContext;

    move-object/from16 v0, p1

    iget v6, v0, Lorg/apache/lucene/index/AtomicReaderContext;->ord:I

    invoke-virtual {v4, v6}, Lorg/apache/lucene/index/TermContext;->get(I)Lorg/apache/lucene/index/TermState;

    move-result-object v16

    .line 223
    .restart local v16    # "termState":Lorg/apache/lucene/index/TermState;
    if-nez v16, :cond_8

    .line 225
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 227
    :cond_8
    invoke-virtual {v14}, Lorg/apache/lucene/index/Term;->bytes()Lorg/apache/lucene/util/BytesRef;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-virtual {v7, v4, v0}, Lorg/apache/lucene/index/TermsEnum;->seekExact(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/index/TermState;)V

    .line 228
    const/4 v4, 0x0

    const/4 v6, 0x0

    invoke-virtual {v7, v3, v4, v6}, Lorg/apache/lucene/index/TermsEnum;->docsAndPositions(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsAndPositionsEnum;I)Lorg/apache/lucene/index/DocsAndPositionsEnum;

    move-result-object v2

    .line 230
    .restart local v2    # "postingsEnum":Lorg/apache/lucene/index/DocsAndPositionsEnum;
    if-nez v2, :cond_a

    .line 232
    sget-boolean v4, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->$assertionsDisabled:Z

    if-nez v4, :cond_9

    const/4 v4, 0x0

    const/4 v6, 0x0

    invoke-virtual {v7, v3, v4, v6}, Lorg/apache/lucene/index/TermsEnum;->docs(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;I)Lorg/apache/lucene/index/DocsEnum;

    move-result-object v4

    if-nez v4, :cond_9

    new-instance v4, Ljava/lang/AssertionError;

    const-string v6, "termstate found but no term exists in reader"

    invoke-direct {v4, v6}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    .line 233
    :cond_9
    new-instance v4, Ljava/lang/IllegalStateException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v17, "field \""

    move-object/from16 v0, v17

    invoke-direct {v6, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v14}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v17, "\" was indexed without position data; cannot run PhraseQuery (term="

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v14}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v17, ")"

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 236
    :cond_a
    invoke-virtual {v7}, Lorg/apache/lucene/index/TermsEnum;->docFreq()I

    move-result v8

    .line 239
    .end local v14    # "term":Lorg/apache/lucene/index/Term;
    .end local v16    # "termState":Lorg/apache/lucene/index/TermState;
    .restart local v8    # "docFreq":I
    :cond_b
    new-instance v6, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->this$0:Lorg/apache/lucene/search/MultiPhraseQuery;

    # getter for: Lorg/apache/lucene/search/MultiPhraseQuery;->positions:Ljava/util/ArrayList;
    invoke-static {v4}, Lorg/apache/lucene/search/MultiPhraseQuery;->access$2(Lorg/apache/lucene/search/MultiPhraseQuery;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-direct {v6, v2, v8, v4, v5}, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;-><init>(Lorg/apache/lucene/index/DocsAndPositionsEnum;II[Lorg/apache/lucene/index/Term;)V

    aput-object v6, v11, v10

    .line 193
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_1

    .line 255
    .end local v2    # "postingsEnum":Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .end local v5    # "terms":[Lorg/apache/lucene/index/Term;
    .end local v8    # "docFreq":I
    :cond_c
    new-instance v13, Lorg/apache/lucene/search/SloppyPhraseScorer;

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->this$0:Lorg/apache/lucene/search/MultiPhraseQuery;

    # getter for: Lorg/apache/lucene/search/MultiPhraseQuery;->slop:I
    invoke-static {v4}, Lorg/apache/lucene/search/MultiPhraseQuery;->access$3(Lorg/apache/lucene/search/MultiPhraseQuery;)I

    move-result v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->similarity:Lorg/apache/lucene/search/similarities/Similarity;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->stats:Lorg/apache/lucene/search/similarities/Similarity$SimWeight;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-virtual {v6, v0, v1}, Lorg/apache/lucene/search/similarities/Similarity;->sloppySimScorer(Lorg/apache/lucene/search/similarities/Similarity$SimWeight;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;

    move-result-object v6

    move-object/from16 v0, p0

    invoke-direct {v13, v0, v11, v4, v6}, Lorg/apache/lucene/search/SloppyPhraseScorer;-><init>(Lorg/apache/lucene/search/Weight;[Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;ILorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;)V

    goto/16 :goto_0
.end method
