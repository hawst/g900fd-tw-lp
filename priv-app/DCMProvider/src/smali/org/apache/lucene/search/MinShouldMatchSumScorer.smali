.class Lorg/apache/lucene/search/MinShouldMatchSumScorer;
.super Lorg/apache/lucene/search/Scorer;
.source "MinShouldMatchSumScorer.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private doc:I

.field private final mm:I

.field private final mmStack:[Lorg/apache/lucene/search/Scorer;

.field private nrInHeap:I

.field protected nrMatchers:I

.field private numScorers:I

.field private score:D

.field private final sortedSubScorers:[Lorg/apache/lucene/search/Scorer;

.field private sortedSubScorersIdx:I

.field private final subScorers:[Lorg/apache/lucene/search/Scorer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/search/Weight;Ljava/util/List;)V
    .locals 1
    .param p1, "weight"    # Lorg/apache/lucene/search/Weight;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/search/Weight;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/search/Scorer;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 117
    .local p2, "subScorers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/Scorer;>;"
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/search/MinShouldMatchSumScorer;-><init>(Lorg/apache/lucene/search/Weight;Ljava/util/List;I)V

    .line 118
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/search/Weight;Ljava/util/List;I)V
    .locals 4
    .param p1, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p3, "minimumNrMatchers"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/search/Weight;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/search/Scorer;",
            ">;I)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p2, "subScorers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/Scorer;>;"
    const/4 v2, -0x1

    .line 76
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/Scorer;-><init>(Lorg/apache/lucene/search/Weight;)V

    .line 46
    const/4 v1, 0x0

    iput v1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->sortedSubScorersIdx:I

    .line 58
    iput v2, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->doc:I

    .line 60
    iput v2, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->nrMatchers:I

    .line 61
    const-wide/high16 v2, 0x7ff8000000000000L    # NaN

    iput-wide v2, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->score:D

    .line 77
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->numScorers:I

    iput v1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->nrInHeap:I

    .line 79
    if-gtz p3, :cond_0

    .line 80
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Minimum nr of matchers must be positive"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 82
    :cond_0
    iget v1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->numScorers:I

    const/4 v2, 0x1

    if-gt v1, v2, :cond_1

    .line 83
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "There must be at least 2 subScorers"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 86
    :cond_1
    iput p3, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->mm:I

    .line 87
    iget v1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->numScorers:I

    new-array v1, v1, [Lorg/apache/lucene/search/Scorer;

    invoke-interface {p2, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lorg/apache/lucene/search/Scorer;

    iput-object v1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->sortedSubScorers:[Lorg/apache/lucene/search/Scorer;

    .line 90
    iget-object v1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->sortedSubScorers:[Lorg/apache/lucene/search/Scorer;

    new-instance v2, Lorg/apache/lucene/search/MinShouldMatchSumScorer$1;

    invoke-direct {v2, p0}, Lorg/apache/lucene/search/MinShouldMatchSumScorer$1;-><init>(Lorg/apache/lucene/search/MinShouldMatchSumScorer;)V

    invoke-static {v1, v2}, Lorg/apache/lucene/util/ArrayUtil;->mergeSort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 97
    iget v1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->mm:I

    add-int/lit8 v1, v1, -0x1

    new-array v1, v1, [Lorg/apache/lucene/search/Scorer;

    iput-object v1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->mmStack:[Lorg/apache/lucene/search/Scorer;

    .line 98
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->mm:I

    add-int/lit8 v1, v1, -0x1

    if-lt v0, v1, :cond_2

    .line 101
    iget v1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->nrInHeap:I

    iget v2, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->mm:I

    add-int/lit8 v2, v2, -0x1

    sub-int/2addr v1, v2

    iput v1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->nrInHeap:I

    .line 102
    iget v1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->mm:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->sortedSubScorersIdx:I

    .line 104
    iget v1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->nrInHeap:I

    new-array v1, v1, [Lorg/apache/lucene/search/Scorer;

    iput-object v1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    .line 105
    const/4 v0, 0x0

    :goto_1
    iget v1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->nrInHeap:I

    if-lt v0, v1, :cond_3

    .line 108
    invoke-virtual {p0}, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->minheapHeapify()V

    .line 109
    sget-boolean v1, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->$assertionsDisabled:Z

    if-nez v1, :cond_4

    invoke-virtual {p0}, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->minheapCheck()Z

    move-result v1

    if-nez v1, :cond_4

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 99
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->mmStack:[Lorg/apache/lucene/search/Scorer;

    iget-object v2, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->sortedSubScorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v2, v2, v0

    aput-object v2, v1, v0

    .line 98
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 106
    :cond_3
    iget-object v1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    iget-object v2, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->sortedSubScorers:[Lorg/apache/lucene/search/Scorer;

    iget v3, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->mm:I

    add-int/lit8 v3, v3, -0x1

    add-int/2addr v3, v0

    aget-object v2, v2, v3

    aput-object v2, v1, v0

    .line 105
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 110
    :cond_4
    return-void
.end method

.method private countMatches(I)V
    .locals 4
    .param p1, "root"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 212
    iget v0, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->nrInHeap:I

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v0

    iget v1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->doc:I

    if-ne v0, v1, :cond_0

    .line 213
    iget v0, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->nrMatchers:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->nrMatchers:I

    .line 214
    iget-wide v0, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->score:D

    iget-object v2, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v2, v2, p1

    invoke-virtual {v2}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v2

    float-to-double v2, v2

    add-double/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->score:D

    .line 215
    shl-int/lit8 v0, p1, 0x1

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->countMatches(I)V

    .line 216
    shl-int/lit8 v0, p1, 0x1

    add-int/lit8 v0, v0, 0x2

    invoke-direct {p0, v0}, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->countMatches(I)V

    .line 218
    :cond_0
    return-void
.end method

.method private evaluateSmallestDocInHeap()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    const v6, 0x7fffffff

    .line 158
    iget-object v1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v1, v1, v2

    invoke-virtual {v1}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->doc:I

    .line 159
    iget v1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->doc:I

    if-ne v1, v6, :cond_1

    .line 160
    iput v6, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->nrMatchers:I

    .line 204
    :cond_0
    :goto_0
    return-void

    .line 164
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v1, v1, v2

    invoke-virtual {v1}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v1

    float-to-double v2, v1

    iput-wide v2, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->score:D

    .line 165
    iput v4, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->nrMatchers:I

    .line 166
    invoke-direct {p0, v4}, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->countMatches(I)V

    .line 167
    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->countMatches(I)V

    .line 171
    iget v1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->mm:I

    add-int/lit8 v0, v1, -0x2

    .local v0, "i":I
    :goto_1
    if-ltz v0, :cond_0

    .line 172
    iget-object v1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->mmStack:[Lorg/apache/lucene/search/Scorer;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v1

    iget v2, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->doc:I

    if-ge v1, v2, :cond_2

    iget-object v1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->mmStack:[Lorg/apache/lucene/search/Scorer;

    aget-object v1, v1, v0

    iget v2, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->doc:I

    invoke-virtual {v1, v2}, Lorg/apache/lucene/search/Scorer;->advance(I)I

    move-result v1

    if-eq v1, v6, :cond_5

    .line 173
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->mmStack:[Lorg/apache/lucene/search/Scorer;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v1

    iget v2, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->doc:I

    if-ne v1, v2, :cond_4

    .line 174
    iget v1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->nrMatchers:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->nrMatchers:I

    .line 175
    iget-wide v2, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->score:D

    iget-object v1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->mmStack:[Lorg/apache/lucene/search/Scorer;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v1

    float-to-double v4, v1

    add-double/2addr v2, v4

    iput-wide v2, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->score:D

    .line 171
    :cond_3
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 177
    :cond_4
    iget v1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->nrMatchers:I

    add-int/2addr v1, v0

    iget v2, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->mm:I

    if-ge v1, v2, :cond_3

    goto :goto_0

    .line 182
    :cond_5
    iget v1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->numScorers:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->numScorers:I

    .line 183
    iget v1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->numScorers:I

    iget v2, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->mm:I

    if-ge v1, v2, :cond_6

    .line 184
    iput v6, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->doc:I

    .line 185
    iput v6, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->nrMatchers:I

    goto :goto_0

    .line 188
    :cond_6
    iget v1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->mm:I

    add-int/lit8 v1, v1, -0x2

    sub-int/2addr v1, v0

    if-lez v1, :cond_7

    .line 190
    iget-object v1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->mmStack:[Lorg/apache/lucene/search/Scorer;

    add-int/lit8 v2, v0, 0x1

    iget-object v3, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->mmStack:[Lorg/apache/lucene/search/Scorer;

    iget v4, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->mm:I

    add-int/lit8 v4, v4, -0x2

    sub-int/2addr v4, v0

    invoke-static {v1, v2, v3, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 193
    :cond_7
    iget-object v1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->sortedSubScorers:[Lorg/apache/lucene/search/Scorer;

    iget v2, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->sortedSubScorersIdx:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->sortedSubScorersIdx:I

    aget-object v1, v1, v2

    invoke-virtual {p0, v1}, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->minheapRemove(Lorg/apache/lucene/search/Scorer;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 197
    iget-object v1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->mmStack:[Lorg/apache/lucene/search/Scorer;

    iget v2, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->mm:I

    add-int/lit8 v2, v2, -0x2

    iget-object v3, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->sortedSubScorers:[Lorg/apache/lucene/search/Scorer;

    iget v4, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->sortedSubScorersIdx:I

    add-int/lit8 v4, v4, -0x1

    aget-object v3, v3, v4

    aput-object v3, v1, v2

    .line 199
    iget v1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->nrMatchers:I

    add-int/2addr v1, v0

    iget v2, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->mm:I

    if-ge v1, v2, :cond_3

    goto/16 :goto_0
.end method

.method private minheapCheck(I)Z
    .locals 6
    .param p1, "root"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 394
    iget v4, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->nrInHeap:I

    if-lt p1, v4, :cond_1

    .line 402
    :cond_0
    :goto_0
    return v2

    .line 396
    :cond_1
    shl-int/lit8 v4, p1, 0x1

    add-int/lit8 v0, v4, 0x1

    .line 397
    .local v0, "lchild":I
    shl-int/lit8 v4, p1, 0x1

    add-int/lit8 v1, v4, 0x2

    .line 398
    .local v1, "rchild":I
    iget v4, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->nrInHeap:I

    if-ge v0, v4, :cond_2

    iget-object v4, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v4, v4, p1

    invoke-virtual {v4}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v4

    iget-object v5, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v5, v5, v0

    invoke-virtual {v5}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v5

    if-le v4, v5, :cond_2

    move v2, v3

    .line 399
    goto :goto_0

    .line 400
    :cond_2
    iget v4, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->nrInHeap:I

    if-ge v1, v4, :cond_3

    iget-object v4, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v4, v4, p1

    invoke-virtual {v4}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v4

    iget-object v5, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v5, v5, v1

    invoke-virtual {v5}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v5

    if-le v4, v5, :cond_3

    move v2, v3

    .line 401
    goto :goto_0

    .line 402
    :cond_3
    invoke-direct {p0, v0}, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->minheapCheck(I)Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-direct {p0, v1}, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->minheapCheck(I)Z

    move-result v4

    if-nez v4, :cond_0

    :cond_4
    move v2, v3

    goto :goto_0
.end method


# virtual methods
.method public advance(I)I
    .locals 4
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const v0, 0x7fffffff

    const/4 v3, 0x0

    .line 250
    iget v1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->numScorers:I

    iget v2, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->mm:I

    if-ge v1, v2, :cond_1

    .line 251
    iput v0, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->doc:I

    .line 271
    :goto_0
    return v0

    .line 254
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v1, v1, v3

    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/Scorer;->advance(I)I

    move-result v1

    if-eq v1, v0, :cond_2

    .line 255
    invoke-virtual {p0, v3}, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->minheapSiftDown(I)V

    .line 253
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v1, v1, v3

    invoke-virtual {v1}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v1

    if-lt v1, p1, :cond_0

    .line 266
    invoke-direct {p0}, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->evaluateSmallestDocInHeap()V

    .line 268
    iget v0, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->nrMatchers:I

    iget v1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->mm:I

    if-lt v0, v1, :cond_3

    .line 269
    iget v0, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->doc:I

    goto :goto_0

    .line 257
    :cond_2
    invoke-virtual {p0}, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->minheapRemoveRoot()V

    .line 258
    iget v1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->numScorers:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->numScorers:I

    .line 259
    iget v1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->numScorers:I

    iget v2, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->mm:I

    if-ge v1, v2, :cond_1

    .line 260
    iput v0, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->doc:I

    goto :goto_0

    .line 271
    :cond_3
    invoke-virtual {p0}, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->nextDoc()I

    move-result v0

    goto :goto_0
.end method

.method public cost()J
    .locals 9

    .prologue
    const/high16 v8, 0x3f800000    # 1.0f

    .line 278
    const-wide/16 v2, 0x0

    .line 279
    .local v2, "costCandidateGeneration":J
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    iget v5, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->nrInHeap:I

    if-lt v4, v5, :cond_0

    .line 283
    const/high16 v0, 0x3f800000    # 1.0f

    .line 284
    .local v0, "c1":F
    const/high16 v1, 0x3f800000    # 1.0f

    .line 286
    .local v1, "c2":F
    long-to-float v5, v2

    mul-float/2addr v5, v8

    .line 287
    long-to-float v6, v2

    mul-float/2addr v6, v8

    iget v7, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->mm:I

    add-int/lit8 v7, v7, -0x1

    int-to-float v7, v7

    mul-float/2addr v6, v7

    .line 285
    add-float/2addr v5, v6

    float-to-long v6, v5

    return-wide v6

    .line 280
    .end local v0    # "c1":F
    .end local v1    # "c2":F
    :cond_0
    iget-object v5, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v5, v5, v4

    invoke-virtual {v5}, Lorg/apache/lucene/search/Scorer;->cost()J

    move-result-wide v6

    add-long/2addr v2, v6

    .line 279
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 231
    iget v0, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->doc:I

    return v0
.end method

.method public freq()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 236
    iget v0, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->nrMatchers:I

    return v0
.end method

.method public final getChildren()Ljava/util/Collection;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/search/Scorer$ChildScorer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 122
    new-instance v0, Ljava/util/ArrayList;

    iget v2, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->numScorers:I

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 123
    .local v0, "children":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/search/Scorer$ChildScorer;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v2, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->numScorers:I

    if-lt v1, v2, :cond_0

    .line 126
    return-object v0

    .line 124
    :cond_0
    new-instance v2, Lorg/apache/lucene/search/Scorer$ChildScorer;

    iget-object v3, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v3, v3, v1

    const-string v4, "SHOULD"

    invoke-direct {v2, v3, v4}, Lorg/apache/lucene/search/Scorer$ChildScorer;-><init>(Lorg/apache/lucene/search/Scorer;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 123
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method minheapCheck()Z
    .locals 1

    .prologue
    .line 391
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->minheapCheck(I)Z

    move-result v0

    return v0
.end method

.method protected final minheapHeapify()V
    .locals 2

    .prologue
    .line 295
    iget v1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->nrInHeap:I

    shr-int/lit8 v1, v1, 0x1

    add-int/lit8 v0, v1, -0x1

    .local v0, "i":I
    :goto_0
    if-gez v0, :cond_0

    .line 298
    return-void

    .line 296
    :cond_0
    invoke-virtual {p0, v0}, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->minheapSiftDown(I)V

    .line 295
    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method protected final minheapRemove(Lorg/apache/lucene/search/Scorer;)Z
    .locals 4
    .param p1, "scorer"    # Lorg/apache/lucene/search/Scorer;

    .prologue
    .line 378
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->nrInHeap:I

    if-lt v0, v1, :cond_0

    .line 387
    const/4 v1, 0x0

    :goto_1
    return v1

    .line 379
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v1, v1, v0

    if-ne v1, p1, :cond_1

    .line 380
    iget-object v1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    iget-object v2, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    iget v3, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->nrInHeap:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->nrInHeap:I

    aget-object v2, v2, v3

    aput-object v2, v1, v0

    .line 382
    invoke-virtual {p0, v0}, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->minheapSiftUp(I)V

    .line 383
    invoke-virtual {p0, v0}, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->minheapSiftDown(I)V

    .line 384
    const/4 v1, 0x1

    goto :goto_1

    .line 378
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected final minheapRemoveRoot()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 361
    iget v0, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->nrInHeap:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 363
    iput v3, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->nrInHeap:I

    .line 370
    :goto_0
    return-void

    .line 365
    :cond_0
    iget v0, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->nrInHeap:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->nrInHeap:I

    .line 366
    iget-object v0, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    iget-object v1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    iget v2, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->nrInHeap:I

    aget-object v1, v1, v2

    aput-object v1, v0, v3

    .line 368
    invoke-virtual {p0, v3}, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->minheapSiftDown(I)V

    goto :goto_0
.end method

.method protected final minheapSiftDown(I)V
    .locals 10
    .param p1, "root"    # I

    .prologue
    .line 306
    iget-object v9, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v8, v9, p1

    .line 307
    .local v8, "scorer":Lorg/apache/lucene/search/Scorer;
    invoke-virtual {v8}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v0

    .line 308
    .local v0, "doc":I
    move v1, p1

    .line 309
    .local v1, "i":I
    :goto_0
    iget v9, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->nrInHeap:I

    shr-int/lit8 v9, v9, 0x1

    add-int/lit8 v9, v9, -0x1

    if-le v1, v9, :cond_1

    .line 337
    :cond_0
    return-void

    .line 310
    :cond_1
    shl-int/lit8 v9, v1, 0x1

    add-int/lit8 v2, v9, 0x1

    .line 311
    .local v2, "lchild":I
    iget-object v9, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v4, v9, v2

    .line 312
    .local v4, "lscorer":Lorg/apache/lucene/search/Scorer;
    invoke-virtual {v4}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v3

    .line 313
    .local v3, "ldoc":I
    const v6, 0x7fffffff

    .local v6, "rdoc":I
    shl-int/lit8 v9, v1, 0x1

    add-int/lit8 v5, v9, 0x2

    .line 314
    .local v5, "rchild":I
    const/4 v7, 0x0

    .line 315
    .local v7, "rscorer":Lorg/apache/lucene/search/Scorer;
    iget v9, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->nrInHeap:I

    if-ge v5, v9, :cond_2

    .line 316
    iget-object v9, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v7, v9, v5

    .line 317
    invoke-virtual {v7}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v6

    .line 319
    :cond_2
    if-ge v3, v0, :cond_4

    .line 320
    if-ge v6, v3, :cond_3

    .line 321
    iget-object v9, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aput-object v7, v9, v1

    .line 322
    iget-object v9, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aput-object v8, v9, v5

    .line 323
    move v1, v5

    .line 324
    goto :goto_0

    .line 325
    :cond_3
    iget-object v9, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aput-object v4, v9, v1

    .line 326
    iget-object v9, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aput-object v8, v9, v2

    .line 327
    move v1, v2

    .line 329
    goto :goto_0

    :cond_4
    if-ge v6, v0, :cond_0

    .line 330
    iget-object v9, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aput-object v7, v9, v1

    .line 331
    iget-object v9, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aput-object v8, v9, v5

    .line 332
    move v1, v5

    .line 333
    goto :goto_0
.end method

.method protected final minheapSiftUp(I)V
    .locals 7
    .param p1, "i"    # I

    .prologue
    .line 340
    iget-object v5, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v4, v5, p1

    .line 341
    .local v4, "scorer":Lorg/apache/lucene/search/Scorer;
    invoke-virtual {v4}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v0

    .line 343
    .local v0, "doc":I
    :goto_0
    if-gtz p1, :cond_1

    .line 354
    :cond_0
    iget-object v5, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aput-object v4, v5, p1

    .line 355
    return-void

    .line 344
    :cond_1
    add-int/lit8 v5, p1, -0x1

    shr-int/lit8 v1, v5, 0x1

    .line 345
    .local v1, "parent":I
    iget-object v5, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v3, v5, v1

    .line 346
    .local v3, "pscorer":Lorg/apache/lucene/search/Scorer;
    invoke-virtual {v3}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v2

    .line 347
    .local v2, "pdoc":I
    if-le v2, v0, :cond_0

    .line 348
    iget-object v5, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    iget-object v6, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v6, v6, v1

    aput-object v6, v5, p1

    .line 349
    move p1, v1

    goto :goto_0
.end method

.method public nextDoc()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const v0, 0x7fffffff

    const/4 v3, 0x0

    .line 131
    sget-boolean v1, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    iget v1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->doc:I

    if-ne v1, v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 135
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v1, v1, v3

    invoke-virtual {v1}, Lorg/apache/lucene/search/Scorer;->nextDoc()I

    move-result v1

    if-eq v1, v0, :cond_2

    .line 136
    invoke-virtual {p0, v3}, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->minheapSiftDown(I)V

    .line 134
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v1, v1, v3

    invoke-virtual {v1}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v1

    iget v2, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->doc:I

    if-eq v1, v2, :cond_0

    .line 147
    invoke-direct {p0}, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->evaluateSmallestDocInHeap()V

    .line 149
    iget v1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->nrMatchers:I

    iget v2, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->mm:I

    if-lt v1, v2, :cond_1

    .line 153
    iget v0, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->doc:I

    :goto_0
    return v0

    .line 138
    :cond_2
    invoke-virtual {p0}, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->minheapRemoveRoot()V

    .line 139
    iget v1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->numScorers:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->numScorers:I

    .line 140
    iget v1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->numScorers:I

    iget v2, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->mm:I

    if-ge v1, v2, :cond_1

    .line 141
    iput v0, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->doc:I

    goto :goto_0
.end method

.method public score()F
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 226
    iget-wide v0, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->score:D

    double-to-float v0, v0

    return v0
.end method
