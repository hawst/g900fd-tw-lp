.class Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorNonScoringCollector;
.super Lorg/apache/lucene/search/TopFieldCollector;
.source "TopFieldCollector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/TopFieldCollector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MultiComparatorNonScoringCollector"
.end annotation


# instance fields
.field final comparators:[Lorg/apache/lucene/search/FieldComparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lorg/apache/lucene/search/FieldComparator",
            "<*>;"
        }
    .end annotation
.end field

.field final queue:Lorg/apache/lucene/search/FieldValueHitQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/search/FieldValueHitQueue",
            "<",
            "Lorg/apache/lucene/search/FieldValueHitQueue$Entry;",
            ">;"
        }
    .end annotation
.end field

.field final reverseMul:[I


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/FieldValueHitQueue;IZ)V
    .locals 1
    .param p2, "numHits"    # I
    .param p3, "fillFields"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/search/FieldValueHitQueue",
            "<",
            "Lorg/apache/lucene/search/FieldValueHitQueue$Entry;",
            ">;IZ)V"
        }
    .end annotation

    .prologue
    .line 389
    .local p1, "queue":Lorg/apache/lucene/search/FieldValueHitQueue;, "Lorg/apache/lucene/search/FieldValueHitQueue<Lorg/apache/lucene/search/FieldValueHitQueue$Entry;>;"
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lorg/apache/lucene/search/TopFieldCollector;-><init>(Lorg/apache/lucene/util/PriorityQueue;IZLorg/apache/lucene/search/TopFieldCollector;)V

    .line 390
    iput-object p1, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorNonScoringCollector;->queue:Lorg/apache/lucene/search/FieldValueHitQueue;

    .line 391
    invoke-virtual {p1}, Lorg/apache/lucene/search/FieldValueHitQueue;->getComparators()[Lorg/apache/lucene/search/FieldComparator;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorNonScoringCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    .line 392
    invoke-virtual {p1}, Lorg/apache/lucene/search/FieldValueHitQueue;->getReverseMul()[I

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorNonScoringCollector;->reverseMul:[I

    .line 393
    return-void
.end method


# virtual methods
.method public collect(I)V
    .locals 5
    .param p1, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 403
    iget v3, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorNonScoringCollector;->totalHits:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorNonScoringCollector;->totalHits:I

    .line 404
    iget-boolean v3, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorNonScoringCollector;->queueFull:Z

    if-eqz v3, :cond_4

    .line 406
    const/4 v1, 0x0

    .line 407
    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorNonScoringCollector;->reverseMul:[I

    aget v3, v3, v1

    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorNonScoringCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    aget-object v4, v4, v1

    invoke-virtual {v4, p1}, Lorg/apache/lucene/search/FieldComparator;->compareBottom(I)I

    move-result v4

    mul-int v0, v3, v4

    .line 408
    .local v0, "c":I
    if-gez v0, :cond_1

    .line 446
    .end local v0    # "c":I
    :cond_0
    return-void

    .line 411
    .restart local v0    # "c":I
    :cond_1
    if-lez v0, :cond_2

    .line 423
    const/4 v1, 0x0

    :goto_1
    iget-object v3, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorNonScoringCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    array-length v3, v3

    if-lt v1, v3, :cond_3

    .line 427
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorNonScoringCollector;->updateBottom(I)V

    .line 429
    const/4 v1, 0x0

    :goto_2
    iget-object v3, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorNonScoringCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    array-length v3, v3

    if-ge v1, v3, :cond_0

    .line 430
    iget-object v3, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorNonScoringCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    aget-object v3, v3, v1

    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorNonScoringCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iget v4, v4, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->slot:I

    invoke-virtual {v3, v4}, Lorg/apache/lucene/search/FieldComparator;->setBottom(I)V

    .line 429
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 414
    :cond_2
    iget-object v3, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorNonScoringCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    if-eq v1, v3, :cond_0

    .line 406
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 424
    :cond_3
    iget-object v3, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorNonScoringCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    aget-object v3, v3, v1

    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorNonScoringCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iget v4, v4, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->slot:I

    invoke-virtual {v3, v4, p1}, Lorg/apache/lucene/search/FieldComparator;->copy(II)V

    .line 423
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 434
    .end local v0    # "c":I
    .end local v1    # "i":I
    :cond_4
    iget v3, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorNonScoringCollector;->totalHits:I

    add-int/lit8 v2, v3, -0x1

    .line 436
    .local v2, "slot":I
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_3
    iget-object v3, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorNonScoringCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    array-length v3, v3

    if-lt v1, v3, :cond_5

    .line 439
    const/high16 v3, 0x7fc00000    # NaNf

    invoke-virtual {p0, v2, p1, v3}, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorNonScoringCollector;->add(IIF)V

    .line 440
    iget-boolean v3, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorNonScoringCollector;->queueFull:Z

    if-eqz v3, :cond_0

    .line 441
    const/4 v1, 0x0

    :goto_4
    iget-object v3, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorNonScoringCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    array-length v3, v3

    if-ge v1, v3, :cond_0

    .line 442
    iget-object v3, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorNonScoringCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    aget-object v3, v3, v1

    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorNonScoringCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iget v4, v4, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->slot:I

    invoke-virtual {v3, v4}, Lorg/apache/lucene/search/FieldComparator;->setBottom(I)V

    .line 441
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 437
    :cond_5
    iget-object v3, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorNonScoringCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    aget-object v3, v3, v1

    invoke-virtual {v3, v2, p1}, Lorg/apache/lucene/search/FieldComparator;->copy(II)V

    .line 436
    add-int/lit8 v1, v1, 0x1

    goto :goto_3
.end method

.method public setNextReader(Lorg/apache/lucene/index/AtomicReaderContext;)V
    .locals 3
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 450
    iget v1, p1, Lorg/apache/lucene/index/AtomicReaderContext;->docBase:I

    iput v1, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorNonScoringCollector;->docBase:I

    .line 451
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorNonScoringCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 454
    return-void

    .line 452
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorNonScoringCollector;->queue:Lorg/apache/lucene/search/FieldValueHitQueue;

    iget-object v2, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorNonScoringCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    aget-object v2, v2, v0

    invoke-virtual {v2, p1}, Lorg/apache/lucene/search/FieldComparator;->setNextReader(Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/search/FieldComparator;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lorg/apache/lucene/search/FieldValueHitQueue;->setComparator(ILorg/apache/lucene/search/FieldComparator;)V

    .line 451
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public setScorer(Lorg/apache/lucene/search/Scorer;)V
    .locals 2
    .param p1, "scorer"    # Lorg/apache/lucene/search/Scorer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 459
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorNonScoringCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 462
    return-void

    .line 460
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorNonScoringCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/FieldComparator;->setScorer(Lorg/apache/lucene/search/Scorer;)V

    .line 459
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method final updateBottom(I)V
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 397
    iget-object v0, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorNonScoringCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iget v1, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorNonScoringCollector;->docBase:I

    add-int/2addr v1, p1

    iput v1, v0, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->doc:I

    .line 398
    iget-object v0, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorNonScoringCollector;->pq:Lorg/apache/lucene/util/PriorityQueue;

    invoke-virtual {v0}, Lorg/apache/lucene/util/PriorityQueue;->updateTop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iput-object v0, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorNonScoringCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    .line 399
    return-void
.end method
