.class public Lorg/apache/lucene/search/similarities/BasicModelIn;
.super Lorg/apache/lucene/search/similarities/BasicModel;
.source "BasicModelIn.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lorg/apache/lucene/search/similarities/BasicModel;-><init>()V

    return-void
.end method


# virtual methods
.method public final explain(Lorg/apache/lucene/search/similarities/BasicStats;F)Lorg/apache/lucene/search/Explanation;
    .locals 4
    .param p1, "stats"    # Lorg/apache/lucene/search/similarities/BasicStats;
    .param p2, "tfn"    # F

    .prologue
    .line 41
    new-instance v0, Lorg/apache/lucene/search/Explanation;

    invoke-direct {v0}, Lorg/apache/lucene/search/Explanation;-><init>()V

    .line 42
    .local v0, "result":Lorg/apache/lucene/search/Explanation;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ", computed from: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/Explanation;->setDescription(Ljava/lang/String;)V

    .line 43
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/search/similarities/BasicModelIn;->score(Lorg/apache/lucene/search/similarities/BasicStats;F)F

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/Explanation;->setValue(F)V

    .line 44
    new-instance v1, Lorg/apache/lucene/search/Explanation;

    const-string v2, "tfn"

    invoke-direct {v1, p2, v2}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 46
    new-instance v1, Lorg/apache/lucene/search/Explanation;

    invoke-virtual {p1}, Lorg/apache/lucene/search/similarities/BasicStats;->getNumberOfDocuments()J

    move-result-wide v2

    long-to-float v2, v2

    const-string v3, "numberOfDocuments"

    invoke-direct {v1, v2, v3}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    .line 45
    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 48
    new-instance v1, Lorg/apache/lucene/search/Explanation;

    invoke-virtual {p1}, Lorg/apache/lucene/search/similarities/BasicStats;->getDocFreq()J

    move-result-wide v2

    long-to-float v2, v2

    const-string v3, "docFreq"

    invoke-direct {v1, v2, v3}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    .line 47
    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 49
    return-object v0
.end method

.method public final score(Lorg/apache/lucene/search/similarities/BasicStats;F)F
    .locals 10
    .param p1, "stats"    # Lorg/apache/lucene/search/similarities/BasicStats;
    .param p2, "tfn"    # F

    .prologue
    .line 34
    invoke-virtual {p1}, Lorg/apache/lucene/search/similarities/BasicStats;->getNumberOfDocuments()J

    move-result-wide v0

    .line 35
    .local v0, "N":J
    invoke-virtual {p1}, Lorg/apache/lucene/search/similarities/BasicStats;->getDocFreq()J

    move-result-wide v2

    .line 36
    .local v2, "n":J
    const-wide/16 v4, 0x1

    add-long/2addr v4, v0

    long-to-double v4, v4

    long-to-double v6, v2

    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    add-double/2addr v6, v8

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Lorg/apache/lucene/search/similarities/SimilarityBase;->log2(D)D

    move-result-wide v4

    double-to-float v4, v4

    mul-float/2addr v4, p2

    return v4
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    const-string v0, "I(n)"

    return-object v0
.end method
