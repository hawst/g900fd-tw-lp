.class public Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanWeight;
.super Lorg/apache/lucene/search/spans/SpanWeight;
.source "PayloadNearQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/payloads/PayloadNearQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "PayloadNearSpanWeight"
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/search/payloads/PayloadNearQuery;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/payloads/PayloadNearQuery;Lorg/apache/lucene/search/spans/SpanQuery;Lorg/apache/lucene/search/IndexSearcher;)V
    .locals 0
    .param p2, "query"    # Lorg/apache/lucene/search/spans/SpanQuery;
    .param p3, "searcher"    # Lorg/apache/lucene/search/IndexSearcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 146
    iput-object p1, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanWeight;->this$0:Lorg/apache/lucene/search/payloads/PayloadNearQuery;

    .line 147
    invoke-direct {p0, p2, p3}, Lorg/apache/lucene/search/spans/SpanWeight;-><init>(Lorg/apache/lucene/search/spans/SpanQuery;Lorg/apache/lucene/search/IndexSearcher;)V

    .line 148
    return-void
.end method


# virtual methods
.method public explain(Lorg/apache/lucene/index/AtomicReaderContext;I)Lorg/apache/lucene/search/Explanation;
    .locals 12
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v11, 0x0

    .line 159
    const/4 v9, 0x1

    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v10

    invoke-virtual {v10}, Lorg/apache/lucene/index/AtomicReader;->getLiveDocs()Lorg/apache/lucene/util/Bits;

    move-result-object v10

    invoke-virtual {p0, p1, v9, v11, v10}, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanWeight;->scorer(Lorg/apache/lucene/index/AtomicReaderContext;ZZLorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/Scorer;

    move-result-object v8

    check-cast v8, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;

    .line 160
    .local v8, "scorer":Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;
    if-eqz v8, :cond_0

    .line 161
    invoke-virtual {v8, p2}, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->advance(I)I

    move-result v4

    .line 162
    .local v4, "newDoc":I
    if-ne v4, p2, :cond_0

    .line 163
    invoke-virtual {v8}, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->freq()I

    move-result v9

    int-to-float v3, v9

    .line 164
    .local v3, "freq":F
    iget-object v9, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanWeight;->similarity:Lorg/apache/lucene/search/similarities/Similarity;

    iget-object v10, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanWeight;->stats:Lorg/apache/lucene/search/similarities/Similarity$SimWeight;

    invoke-virtual {v9, v10, p1}, Lorg/apache/lucene/search/similarities/Similarity;->sloppySimScorer(Lorg/apache/lucene/search/similarities/Similarity$SimWeight;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;

    move-result-object v0

    .line 165
    .local v0, "docScorer":Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;
    new-instance v1, Lorg/apache/lucene/search/Explanation;

    invoke-direct {v1}, Lorg/apache/lucene/search/Explanation;-><init>()V

    .line 166
    .local v1, "expl":Lorg/apache/lucene/search/Explanation;
    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "weight("

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanWeight;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " in "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ") ["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanWeight;->similarity:Lorg/apache/lucene/search/similarities/Similarity;

    invoke-virtual {v10}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "], result of:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v9}, Lorg/apache/lucene/search/Explanation;->setDescription(Ljava/lang/String;)V

    .line 167
    new-instance v9, Lorg/apache/lucene/search/Explanation;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "phraseFreq="

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v3, v10}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    invoke-virtual {v0, p2, v9}, Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;->explain(ILorg/apache/lucene/search/Explanation;)Lorg/apache/lucene/search/Explanation;

    move-result-object v7

    .line 168
    .local v7, "scoreExplanation":Lorg/apache/lucene/search/Explanation;
    invoke-virtual {v1, v7}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 169
    invoke-virtual {v7}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v9

    invoke-virtual {v1, v9}, Lorg/apache/lucene/search/Explanation;->setValue(F)V

    .line 170
    invoke-virtual {p0}, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanWeight;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v9

    check-cast v9, Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v9}, Lorg/apache/lucene/search/spans/SpanQuery;->getField()Ljava/lang/String;

    move-result-object v2

    .line 172
    .local v2, "field":Ljava/lang/String;
    iget-object v9, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanWeight;->this$0:Lorg/apache/lucene/search/payloads/PayloadNearQuery;

    iget-object v9, v9, Lorg/apache/lucene/search/payloads/PayloadNearQuery;->function:Lorg/apache/lucene/search/payloads/PayloadFunction;

    # getter for: Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->payloadsSeen:I
    invoke-static {v8}, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->access$0(Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;)I

    move-result v10

    iget v11, v8, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->payloadScore:F

    invoke-virtual {v9, p2, v2, v10, v11}, Lorg/apache/lucene/search/payloads/PayloadFunction;->explain(ILjava/lang/String;IF)Lorg/apache/lucene/search/Explanation;

    move-result-object v5

    .line 174
    .local v5, "payloadExpl":Lorg/apache/lucene/search/Explanation;
    new-instance v6, Lorg/apache/lucene/search/ComplexExplanation;

    invoke-direct {v6}, Lorg/apache/lucene/search/ComplexExplanation;-><init>()V

    .line 175
    .local v6, "result":Lorg/apache/lucene/search/ComplexExplanation;
    invoke-virtual {v6, v1}, Lorg/apache/lucene/search/ComplexExplanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 176
    invoke-virtual {v6, v5}, Lorg/apache/lucene/search/ComplexExplanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 177
    invoke-virtual {v1}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v9

    invoke-virtual {v5}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v10

    mul-float/2addr v9, v10

    invoke-virtual {v6, v9}, Lorg/apache/lucene/search/ComplexExplanation;->setValue(F)V

    .line 178
    const-string v9, "PayloadNearQuery, product of:"

    invoke-virtual {v6, v9}, Lorg/apache/lucene/search/ComplexExplanation;->setDescription(Ljava/lang/String;)V

    .line 183
    .end local v0    # "docScorer":Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;
    .end local v1    # "expl":Lorg/apache/lucene/search/Explanation;
    .end local v2    # "field":Ljava/lang/String;
    .end local v3    # "freq":F
    .end local v4    # "newDoc":I
    .end local v5    # "payloadExpl":Lorg/apache/lucene/search/Explanation;
    .end local v6    # "result":Lorg/apache/lucene/search/ComplexExplanation;
    .end local v7    # "scoreExplanation":Lorg/apache/lucene/search/Explanation;
    :goto_0
    return-object v6

    :cond_0
    new-instance v6, Lorg/apache/lucene/search/ComplexExplanation;

    const/4 v9, 0x0

    const-string v10, "no matching term"

    invoke-direct {v6, v11, v9, v10}, Lorg/apache/lucene/search/ComplexExplanation;-><init>(ZFLjava/lang/String;)V

    goto :goto_0
.end method

.method public scorer(Lorg/apache/lucene/index/AtomicReaderContext;ZZLorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/Scorer;
    .locals 6
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "scoreDocsInOrder"    # Z
    .param p3, "topScorer"    # Z
    .param p4, "acceptDocs"    # Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 153
    new-instance v0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;

    iget-object v1, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanWeight;->this$0:Lorg/apache/lucene/search/payloads/PayloadNearQuery;

    iget-object v2, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanWeight;->query:Lorg/apache/lucene/search/spans/SpanQuery;

    iget-object v3, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanWeight;->termContexts:Ljava/util/Map;

    invoke-virtual {v2, p1, p4, v3}, Lorg/apache/lucene/search/spans/SpanQuery;->getSpans(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;Ljava/util/Map;)Lorg/apache/lucene/search/spans/Spans;

    move-result-object v2

    .line 154
    iget-object v4, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanWeight;->similarity:Lorg/apache/lucene/search/similarities/Similarity;

    iget-object v3, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanWeight;->similarity:Lorg/apache/lucene/search/similarities/Similarity;

    iget-object v5, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanWeight;->stats:Lorg/apache/lucene/search/similarities/Similarity$SimWeight;

    invoke-virtual {v3, v5, p1}, Lorg/apache/lucene/search/similarities/Similarity;->sloppySimScorer(Lorg/apache/lucene/search/similarities/Similarity$SimWeight;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;

    move-result-object v5

    move-object v3, p0

    .line 153
    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;-><init>(Lorg/apache/lucene/search/payloads/PayloadNearQuery;Lorg/apache/lucene/search/spans/Spans;Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/similarities/Similarity;Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;)V

    return-object v0
.end method
