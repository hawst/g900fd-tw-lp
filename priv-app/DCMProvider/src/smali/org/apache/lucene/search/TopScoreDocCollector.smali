.class public abstract Lorg/apache/lucene/search/TopScoreDocCollector;
.super Lorg/apache/lucene/search/TopDocsCollector;
.source "TopScoreDocCollector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/TopScoreDocCollector$InOrderPagingScoreDocCollector;,
        Lorg/apache/lucene/search/TopScoreDocCollector$InOrderTopScoreDocCollector;,
        Lorg/apache/lucene/search/TopScoreDocCollector$OutOfOrderPagingScoreDocCollector;,
        Lorg/apache/lucene/search/TopScoreDocCollector$OutOfOrderTopScoreDocCollector;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/TopDocsCollector",
        "<",
        "Lorg/apache/lucene/search/ScoreDoc;",
        ">;"
    }
.end annotation


# instance fields
.field docBase:I

.field pqTop:Lorg/apache/lucene/search/ScoreDoc;

.field scorer:Lorg/apache/lucene/search/Scorer;


# direct methods
.method private constructor <init>(I)V
    .locals 2
    .param p1, "numHits"    # I

    .prologue
    .line 275
    new-instance v0, Lorg/apache/lucene/search/HitQueue;

    const/4 v1, 0x1

    invoke-direct {v0, p1, v1}, Lorg/apache/lucene/search/HitQueue;-><init>(IZ)V

    invoke-direct {p0, v0}, Lorg/apache/lucene/search/TopDocsCollector;-><init>(Lorg/apache/lucene/util/PriorityQueue;)V

    .line 270
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/search/TopScoreDocCollector;->docBase:I

    .line 278
    iget-object v0, p0, Lorg/apache/lucene/search/TopScoreDocCollector;->pq:Lorg/apache/lucene/util/PriorityQueue;

    invoke-virtual {v0}, Lorg/apache/lucene/util/PriorityQueue;->top()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/ScoreDoc;

    iput-object v0, p0, Lorg/apache/lucene/search/TopScoreDocCollector;->pqTop:Lorg/apache/lucene/search/ScoreDoc;

    .line 279
    return-void
.end method

.method synthetic constructor <init>(ILorg/apache/lucene/search/TopScoreDocCollector;)V
    .locals 0

    .prologue
    .line 274
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/TopScoreDocCollector;-><init>(I)V

    return-void
.end method

.method public static create(ILorg/apache/lucene/search/ScoreDoc;Z)Lorg/apache/lucene/search/TopScoreDocCollector;
    .locals 2
    .param p0, "numHits"    # I
    .param p1, "after"    # Lorg/apache/lucene/search/ScoreDoc;
    .param p2, "docsScoredInOrder"    # Z

    .prologue
    const/4 v1, 0x0

    .line 253
    if-gtz p0, :cond_0

    .line 254
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "numHits must be > 0; please use TotalHitCountCollector if you just need the total hit count"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 257
    :cond_0
    if-eqz p2, :cond_2

    .line 258
    if-nez p1, :cond_1

    .line 259
    new-instance v0, Lorg/apache/lucene/search/TopScoreDocCollector$InOrderTopScoreDocCollector;

    invoke-direct {v0, p0, v1}, Lorg/apache/lucene/search/TopScoreDocCollector$InOrderTopScoreDocCollector;-><init>(ILorg/apache/lucene/search/TopScoreDocCollector$InOrderTopScoreDocCollector;)V

    .line 262
    :goto_0
    return-object v0

    .line 260
    :cond_1
    new-instance v0, Lorg/apache/lucene/search/TopScoreDocCollector$InOrderPagingScoreDocCollector;

    invoke-direct {v0, p1, p0, v1}, Lorg/apache/lucene/search/TopScoreDocCollector$InOrderPagingScoreDocCollector;-><init>(Lorg/apache/lucene/search/ScoreDoc;ILorg/apache/lucene/search/TopScoreDocCollector$InOrderPagingScoreDocCollector;)V

    goto :goto_0

    .line 262
    :cond_2
    if-nez p1, :cond_3

    .line 263
    new-instance v0, Lorg/apache/lucene/search/TopScoreDocCollector$OutOfOrderTopScoreDocCollector;

    invoke-direct {v0, p0, v1}, Lorg/apache/lucene/search/TopScoreDocCollector$OutOfOrderTopScoreDocCollector;-><init>(ILorg/apache/lucene/search/TopScoreDocCollector$OutOfOrderTopScoreDocCollector;)V

    goto :goto_0

    .line 264
    :cond_3
    new-instance v0, Lorg/apache/lucene/search/TopScoreDocCollector$OutOfOrderPagingScoreDocCollector;

    invoke-direct {v0, p1, p0, v1}, Lorg/apache/lucene/search/TopScoreDocCollector$OutOfOrderPagingScoreDocCollector;-><init>(Lorg/apache/lucene/search/ScoreDoc;ILorg/apache/lucene/search/TopScoreDocCollector$OutOfOrderPagingScoreDocCollector;)V

    goto :goto_0
.end method

.method public static create(IZ)Lorg/apache/lucene/search/TopScoreDocCollector;
    .locals 1
    .param p0, "numHits"    # I
    .param p1, "docsScoredInOrder"    # Z

    .prologue
    .line 238
    const/4 v0, 0x0

    invoke-static {p0, v0, p1}, Lorg/apache/lucene/search/TopScoreDocCollector;->create(ILorg/apache/lucene/search/ScoreDoc;Z)Lorg/apache/lucene/search/TopScoreDocCollector;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected newTopDocs([Lorg/apache/lucene/search/ScoreDoc;I)Lorg/apache/lucene/search/TopDocs;
    .locals 4
    .param p1, "results"    # [Lorg/apache/lucene/search/ScoreDoc;
    .param p2, "start"    # I

    .prologue
    .line 283
    if-nez p1, :cond_0

    .line 284
    sget-object v2, Lorg/apache/lucene/search/TopScoreDocCollector;->EMPTY_TOPDOCS:Lorg/apache/lucene/search/TopDocs;

    .line 299
    :goto_0
    return-object v2

    .line 291
    :cond_0
    const/high16 v1, 0x7fc00000    # NaNf

    .line 292
    .local v1, "maxScore":F
    if-nez p2, :cond_1

    .line 293
    const/4 v2, 0x0

    aget-object v2, p1, v2

    iget v1, v2, Lorg/apache/lucene/search/ScoreDoc;->score:F

    .line 299
    :goto_1
    new-instance v2, Lorg/apache/lucene/search/TopDocs;

    iget v3, p0, Lorg/apache/lucene/search/TopScoreDocCollector;->totalHits:I

    invoke-direct {v2, v3, p1, v1}, Lorg/apache/lucene/search/TopDocs;-><init>(I[Lorg/apache/lucene/search/ScoreDoc;F)V

    goto :goto_0

    .line 295
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/search/TopScoreDocCollector;->pq:Lorg/apache/lucene/util/PriorityQueue;

    invoke-virtual {v2}, Lorg/apache/lucene/util/PriorityQueue;->size()I

    move-result v0

    .local v0, "i":I
    :goto_2
    const/4 v2, 0x1

    if-gt v0, v2, :cond_2

    .line 296
    iget-object v2, p0, Lorg/apache/lucene/search/TopScoreDocCollector;->pq:Lorg/apache/lucene/util/PriorityQueue;

    invoke-virtual {v2}, Lorg/apache/lucene/util/PriorityQueue;->pop()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/search/ScoreDoc;

    iget v1, v2, Lorg/apache/lucene/search/ScoreDoc;->score:F

    goto :goto_1

    .line 295
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/search/TopScoreDocCollector;->pq:Lorg/apache/lucene/util/PriorityQueue;

    invoke-virtual {v2}, Lorg/apache/lucene/util/PriorityQueue;->pop()Ljava/lang/Object;

    add-int/lit8 v0, v0, -0x1

    goto :goto_2
.end method

.method public setNextReader(Lorg/apache/lucene/index/AtomicReaderContext;)V
    .locals 1
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;

    .prologue
    .line 304
    iget v0, p1, Lorg/apache/lucene/index/AtomicReaderContext;->docBase:I

    iput v0, p0, Lorg/apache/lucene/search/TopScoreDocCollector;->docBase:I

    .line 305
    return-void
.end method

.method public setScorer(Lorg/apache/lucene/search/Scorer;)V
    .locals 0
    .param p1, "scorer"    # Lorg/apache/lucene/search/Scorer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 309
    iput-object p1, p0, Lorg/apache/lucene/search/TopScoreDocCollector;->scorer:Lorg/apache/lucene/search/Scorer;

    .line 310
    return-void
.end method
