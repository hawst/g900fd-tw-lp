.class final Lorg/apache/lucene/search/FieldCacheImpl$IntCache;
.super Lorg/apache/lucene/search/FieldCacheImpl$Cache;
.source "FieldCacheImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FieldCacheImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "IntCache"
.end annotation


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/FieldCacheImpl;)V
    .locals 0
    .param p1, "wrapper"    # Lorg/apache/lucene/search/FieldCacheImpl;

    .prologue
    .line 602
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/FieldCacheImpl$Cache;-><init>(Lorg/apache/lucene/search/FieldCacheImpl;)V

    .line 603
    return-void
.end method


# virtual methods
.method protected createValue(Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;Z)Ljava/lang/Object;
    .locals 8
    .param p1, "reader"    # Lorg/apache/lucene/index/AtomicReader;
    .param p2, "key"    # Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;
    .param p3, "setDocsWithField"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 609
    iget-object v1, p2, Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;->custom:Ljava/lang/Object;

    check-cast v1, Lorg/apache/lucene/search/FieldCache$IntParser;

    .line 610
    .local v1, "parser":Lorg/apache/lucene/search/FieldCache$IntParser;
    if-nez v1, :cond_0

    .line 617
    :try_start_0
    iget-object v5, p0, Lorg/apache/lucene/search/FieldCacheImpl$IntCache;->wrapper:Lorg/apache/lucene/search/FieldCacheImpl;

    iget-object v6, p2, Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;->field:Ljava/lang/String;

    sget-object v7, Lorg/apache/lucene/search/FieldCacheImpl;->DEFAULT_INT_PARSER:Lorg/apache/lucene/search/FieldCache$IntParser;

    invoke-virtual {v5, p1, v6, v7, p3}, Lorg/apache/lucene/search/FieldCacheImpl;->getInts(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$IntParser;Z)Lorg/apache/lucene/search/FieldCache$Ints;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 662
    :goto_0
    return-object v5

    .line 618
    :catch_0
    move-exception v0

    .line 619
    .local v0, "ne":Ljava/lang/NumberFormatException;
    iget-object v5, p0, Lorg/apache/lucene/search/FieldCacheImpl$IntCache;->wrapper:Lorg/apache/lucene/search/FieldCacheImpl;

    iget-object v6, p2, Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;->field:Ljava/lang/String;

    sget-object v7, Lorg/apache/lucene/search/FieldCacheImpl;->NUMERIC_UTILS_INT_PARSER:Lorg/apache/lucene/search/FieldCache$IntParser;

    invoke-virtual {v5, p1, v6, v7, p3}, Lorg/apache/lucene/search/FieldCacheImpl;->getInts(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$IntParser;Z)Lorg/apache/lucene/search/FieldCache$Ints;

    move-result-object v5

    goto :goto_0

    .line 623
    .end local v0    # "ne":Ljava/lang/NumberFormatException;
    :cond_0
    new-instance v4, Lorg/apache/lucene/search/FieldCacheImpl$HoldsOneThing;

    const/4 v5, 0x0

    invoke-direct {v4, v5}, Lorg/apache/lucene/search/FieldCacheImpl$HoldsOneThing;-><init>(Lorg/apache/lucene/search/FieldCacheImpl$HoldsOneThing;)V

    .line 625
    .local v4, "valuesRef":Lorg/apache/lucene/search/FieldCacheImpl$HoldsOneThing;, "Lorg/apache/lucene/search/FieldCacheImpl$HoldsOneThing<[I>;"
    new-instance v2, Lorg/apache/lucene/search/FieldCacheImpl$IntCache$1;

    invoke-direct {v2, p0, v1, p1, v4}, Lorg/apache/lucene/search/FieldCacheImpl$IntCache$1;-><init>(Lorg/apache/lucene/search/FieldCacheImpl$IntCache;Lorg/apache/lucene/search/FieldCache$IntParser;Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/search/FieldCacheImpl$HoldsOneThing;)V

    .line 653
    .local v2, "u":Lorg/apache/lucene/search/FieldCacheImpl$Uninvert;
    iget-object v5, p2, Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;->field:Ljava/lang/String;

    invoke-virtual {v2, p1, v5, p3}, Lorg/apache/lucene/search/FieldCacheImpl$Uninvert;->uninvert(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;Z)V

    .line 655
    if-eqz p3, :cond_1

    .line 656
    iget-object v5, p0, Lorg/apache/lucene/search/FieldCacheImpl$IntCache;->wrapper:Lorg/apache/lucene/search/FieldCacheImpl;

    iget-object v6, p2, Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;->field:Ljava/lang/String;

    iget-object v7, v2, Lorg/apache/lucene/search/FieldCacheImpl$Uninvert;->docsWithField:Lorg/apache/lucene/util/Bits;

    invoke-virtual {v5, p1, v6, v7}, Lorg/apache/lucene/search/FieldCacheImpl;->setDocsWithField(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;Lorg/apache/lucene/util/Bits;)V

    .line 658
    :cond_1
    invoke-virtual {v4}, Lorg/apache/lucene/search/FieldCacheImpl$HoldsOneThing;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [I

    .line 659
    .local v3, "values":[I
    if-nez v3, :cond_2

    .line 660
    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v5

    new-array v3, v5, [I

    .line 662
    :cond_2
    new-instance v5, Lorg/apache/lucene/search/FieldCacheImpl$IntsFromArray;

    invoke-direct {v5, v3}, Lorg/apache/lucene/search/FieldCacheImpl$IntsFromArray;-><init>([I)V

    goto :goto_0
.end method
