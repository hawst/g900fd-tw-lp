.class Lorg/apache/lucene/search/BooleanScorer2$SingleMatchScorer;
.super Lorg/apache/lucene/search/Scorer;
.source "BooleanScorer2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/BooleanScorer2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SingleMatchScorer"
.end annotation


# instance fields
.field private lastDocScore:F

.field private lastScoredDoc:I

.field private scorer:Lorg/apache/lucene/search/Scorer;

.field final synthetic this$0:Lorg/apache/lucene/search/BooleanScorer2;


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/BooleanScorer2;Lorg/apache/lucene/search/Scorer;)V
    .locals 1
    .param p2, "scorer"    # Lorg/apache/lucene/search/Scorer;

    .prologue
    .line 113
    iput-object p1, p0, Lorg/apache/lucene/search/BooleanScorer2$SingleMatchScorer;->this$0:Lorg/apache/lucene/search/BooleanScorer2;

    .line 114
    iget-object v0, p2, Lorg/apache/lucene/search/Scorer;->weight:Lorg/apache/lucene/search/Weight;

    invoke-direct {p0, v0}, Lorg/apache/lucene/search/Scorer;-><init>(Lorg/apache/lucene/search/Weight;)V

    .line 108
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/search/BooleanScorer2$SingleMatchScorer;->lastScoredDoc:I

    .line 111
    const/high16 v0, 0x7fc00000    # NaNf

    iput v0, p0, Lorg/apache/lucene/search/BooleanScorer2$SingleMatchScorer;->lastDocScore:F

    .line 115
    iput-object p2, p0, Lorg/apache/lucene/search/BooleanScorer2$SingleMatchScorer;->scorer:Lorg/apache/lucene/search/Scorer;

    .line 116
    return-void
.end method


# virtual methods
.method public advance(I)I
    .locals 1
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 148
    iget-object v0, p0, Lorg/apache/lucene/search/BooleanScorer2$SingleMatchScorer;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/Scorer;->advance(I)I

    move-result v0

    return v0
.end method

.method public cost()J
    .locals 2

    .prologue
    .line 153
    iget-object v0, p0, Lorg/apache/lucene/search/BooleanScorer2$SingleMatchScorer;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->cost()J

    move-result-wide v0

    return-wide v0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lorg/apache/lucene/search/BooleanScorer2$SingleMatchScorer;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v0

    return v0
.end method

.method public freq()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 133
    const/4 v0, 0x1

    return v0
.end method

.method public nextDoc()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 143
    iget-object v0, p0, Lorg/apache/lucene/search/BooleanScorer2$SingleMatchScorer;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->nextDoc()I

    move-result v0

    return v0
.end method

.method public score()F
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 120
    invoke-virtual {p0}, Lorg/apache/lucene/search/BooleanScorer2$SingleMatchScorer;->docID()I

    move-result v0

    .line 121
    .local v0, "doc":I
    iget v1, p0, Lorg/apache/lucene/search/BooleanScorer2$SingleMatchScorer;->lastScoredDoc:I

    if-lt v0, v1, :cond_1

    .line 122
    iget v1, p0, Lorg/apache/lucene/search/BooleanScorer2$SingleMatchScorer;->lastScoredDoc:I

    if-le v0, v1, :cond_0

    .line 123
    iget-object v1, p0, Lorg/apache/lucene/search/BooleanScorer2$SingleMatchScorer;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v1}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v1

    iput v1, p0, Lorg/apache/lucene/search/BooleanScorer2$SingleMatchScorer;->lastDocScore:F

    .line 124
    iput v0, p0, Lorg/apache/lucene/search/BooleanScorer2$SingleMatchScorer;->lastScoredDoc:I

    .line 126
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/BooleanScorer2$SingleMatchScorer;->this$0:Lorg/apache/lucene/search/BooleanScorer2;

    # getter for: Lorg/apache/lucene/search/BooleanScorer2;->coordinator:Lorg/apache/lucene/search/BooleanScorer2$Coordinator;
    invoke-static {v1}, Lorg/apache/lucene/search/BooleanScorer2;->access$2(Lorg/apache/lucene/search/BooleanScorer2;)Lorg/apache/lucene/search/BooleanScorer2$Coordinator;

    move-result-object v1

    iget v2, v1, Lorg/apache/lucene/search/BooleanScorer2$Coordinator;->nrMatchers:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lorg/apache/lucene/search/BooleanScorer2$Coordinator;->nrMatchers:I

    .line 128
    :cond_1
    iget v1, p0, Lorg/apache/lucene/search/BooleanScorer2$SingleMatchScorer;->lastDocScore:F

    return v1
.end method
