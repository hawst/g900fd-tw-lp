.class Lorg/apache/lucene/search/DocTermOrdsRewriteMethod$MultiTermQueryDocTermOrdsWrapperFilter;
.super Lorg/apache/lucene/search/Filter;
.source "DocTermOrdsRewriteMethod.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/DocTermOrdsRewriteMethod;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "MultiTermQueryDocTermOrdsWrapperFilter"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field protected final query:Lorg/apache/lucene/search/MultiTermQuery;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    const-class v0, Lorg/apache/lucene/search/DocTermOrdsRewriteMethod;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/DocTermOrdsRewriteMethod$MultiTermQueryDocTermOrdsWrapperFilter;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected constructor <init>(Lorg/apache/lucene/search/MultiTermQuery;)V
    .locals 0
    .param p1, "query"    # Lorg/apache/lucene/search/MultiTermQuery;

    .prologue
    .line 54
    invoke-direct {p0}, Lorg/apache/lucene/search/Filter;-><init>()V

    .line 55
    iput-object p1, p0, Lorg/apache/lucene/search/DocTermOrdsRewriteMethod$MultiTermQueryDocTermOrdsWrapperFilter;->query:Lorg/apache/lucene/search/MultiTermQuery;

    .line 56
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x0

    .line 66
    if-ne p1, p0, :cond_1

    const/4 v0, 0x1

    .line 71
    .end local p1    # "o":Ljava/lang/Object;
    :cond_0
    :goto_0
    return v0

    .line 67
    .restart local p1    # "o":Ljava/lang/Object;
    :cond_1
    if-eqz p1, :cond_0

    .line 68
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 69
    iget-object v0, p0, Lorg/apache/lucene/search/DocTermOrdsRewriteMethod$MultiTermQueryDocTermOrdsWrapperFilter;->query:Lorg/apache/lucene/search/MultiTermQuery;

    check-cast p1, Lorg/apache/lucene/search/DocTermOrdsRewriteMethod$MultiTermQueryDocTermOrdsWrapperFilter;

    .end local p1    # "o":Ljava/lang/Object;
    iget-object v1, p1, Lorg/apache/lucene/search/DocTermOrdsRewriteMethod$MultiTermQueryDocTermOrdsWrapperFilter;->query:Lorg/apache/lucene/search/MultiTermQuery;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/MultiTermQuery;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public getDocIdSet(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/DocIdSet;
    .locals 7
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "acceptDocs"    # Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 88
    sget-object v0, Lorg/apache/lucene/search/FieldCache;->DEFAULT:Lorg/apache/lucene/search/FieldCache;

    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/search/DocTermOrdsRewriteMethod$MultiTermQueryDocTermOrdsWrapperFilter;->query:Lorg/apache/lucene/search/MultiTermQuery;

    iget-object v2, v2, Lorg/apache/lucene/search/MultiTermQuery;->field:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lorg/apache/lucene/search/FieldCache;->getDocTermOrds(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;)Lorg/apache/lucene/index/SortedSetDocValues;

    move-result-object v4

    .line 90
    .local v4, "docTermOrds":Lorg/apache/lucene/index/SortedSetDocValues;
    new-instance v5, Lorg/apache/lucene/util/OpenBitSet;

    invoke-virtual {v4}, Lorg/apache/lucene/index/SortedSetDocValues;->getValueCount()J

    move-result-wide v0

    invoke-direct {v5, v0, v1}, Lorg/apache/lucene/util/OpenBitSet;-><init>(J)V

    .line 91
    .local v5, "termSet":Lorg/apache/lucene/util/OpenBitSet;
    iget-object v0, p0, Lorg/apache/lucene/search/DocTermOrdsRewriteMethod$MultiTermQueryDocTermOrdsWrapperFilter;->query:Lorg/apache/lucene/search/MultiTermQuery;

    new-instance v1, Lorg/apache/lucene/search/DocTermOrdsRewriteMethod$MultiTermQueryDocTermOrdsWrapperFilter$1;

    invoke-direct {v1, p0, v4}, Lorg/apache/lucene/search/DocTermOrdsRewriteMethod$MultiTermQueryDocTermOrdsWrapperFilter$1;-><init>(Lorg/apache/lucene/search/DocTermOrdsRewriteMethod$MultiTermQueryDocTermOrdsWrapperFilter;Lorg/apache/lucene/index/SortedSetDocValues;)V

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/MultiTermQuery;->getTermsEnum(Lorg/apache/lucene/index/Terms;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v6

    .line 139
    .local v6, "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    sget-boolean v0, Lorg/apache/lucene/search/DocTermOrdsRewriteMethod$MultiTermQueryDocTermOrdsWrapperFilter;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez v6, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 140
    :cond_0
    invoke-virtual {v6}, Lorg/apache/lucene/index/TermsEnum;->next()Lorg/apache/lucene/util/BytesRef;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 143
    :cond_1
    invoke-virtual {v6}, Lorg/apache/lucene/index/TermsEnum;->ord()J

    move-result-wide v0

    invoke-virtual {v5, v0, v1}, Lorg/apache/lucene/util/OpenBitSet;->set(J)V

    .line 144
    invoke-virtual {v6}, Lorg/apache/lucene/index/TermsEnum;->next()Lorg/apache/lucene/util/BytesRef;

    move-result-object v0

    if-nez v0, :cond_1

    .line 149
    new-instance v0, Lorg/apache/lucene/search/DocTermOrdsRewriteMethod$MultiTermQueryDocTermOrdsWrapperFilter$2;

    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v2

    move-object v1, p0

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/search/DocTermOrdsRewriteMethod$MultiTermQueryDocTermOrdsWrapperFilter$2;-><init>(Lorg/apache/lucene/search/DocTermOrdsRewriteMethod$MultiTermQueryDocTermOrdsWrapperFilter;ILorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/SortedSetDocValues;Lorg/apache/lucene/util/OpenBitSet;)V

    :goto_0
    return-object v0

    .line 146
    :cond_2
    sget-object v0, Lorg/apache/lucene/search/DocIdSet;->EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;

    goto :goto_0
.end method

.method public final getField()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lorg/apache/lucene/search/DocTermOrdsRewriteMethod$MultiTermQueryDocTermOrdsWrapperFilter;->query:Lorg/apache/lucene/search/MultiTermQuery;

    invoke-virtual {v0}, Lorg/apache/lucene/search/MultiTermQuery;->getField()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lorg/apache/lucene/search/DocTermOrdsRewriteMethod$MultiTermQueryDocTermOrdsWrapperFilter;->query:Lorg/apache/lucene/search/MultiTermQuery;

    invoke-virtual {v0}, Lorg/apache/lucene/search/MultiTermQuery;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lorg/apache/lucene/search/DocTermOrdsRewriteMethod$MultiTermQueryDocTermOrdsWrapperFilter;->query:Lorg/apache/lucene/search/MultiTermQuery;

    invoke-virtual {v0}, Lorg/apache/lucene/search/MultiTermQuery;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
