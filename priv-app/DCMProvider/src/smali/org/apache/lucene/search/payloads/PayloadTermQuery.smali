.class public Lorg/apache/lucene/search/payloads/PayloadTermQuery;
.super Lorg/apache/lucene/search/spans/SpanTermQuery;
.source "PayloadTermQuery.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;
    }
.end annotation


# instance fields
.field protected function:Lorg/apache/lucene/search/payloads/PayloadFunction;

.field private includeSpanScore:Z


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/Term;Lorg/apache/lucene/search/payloads/PayloadFunction;)V
    .locals 1
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .param p2, "function"    # Lorg/apache/lucene/search/payloads/PayloadFunction;

    .prologue
    .line 59
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/search/payloads/PayloadTermQuery;-><init>(Lorg/apache/lucene/index/Term;Lorg/apache/lucene/search/payloads/PayloadFunction;Z)V

    .line 60
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/index/Term;Lorg/apache/lucene/search/payloads/PayloadFunction;Z)V
    .locals 0
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .param p2, "function"    # Lorg/apache/lucene/search/payloads/PayloadFunction;
    .param p3, "includeSpanScore"    # Z

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/spans/SpanTermQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    .line 65
    iput-object p2, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery;->function:Lorg/apache/lucene/search/payloads/PayloadFunction;

    .line 66
    iput-boolean p3, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery;->includeSpanScore:Z

    .line 67
    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/search/payloads/PayloadTermQuery;)Lorg/apache/lucene/index/Term;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery;->term:Lorg/apache/lucene/index/Term;

    return-object v0
.end method

.method static synthetic access$1(Lorg/apache/lucene/search/payloads/PayloadTermQuery;)Z
    .locals 1

    .prologue
    .line 56
    iget-boolean v0, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery;->includeSpanScore:Z

    return v0
.end method


# virtual methods
.method public createWeight(Lorg/apache/lucene/search/IndexSearcher;)Lorg/apache/lucene/search/Weight;
    .locals 1
    .param p1, "searcher"    # Lorg/apache/lucene/search/IndexSearcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 71
    new-instance v0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;

    invoke-direct {v0, p0, p0, p1}, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;-><init>(Lorg/apache/lucene/search/payloads/PayloadTermQuery;Lorg/apache/lucene/search/payloads/PayloadTermQuery;Lorg/apache/lucene/search/IndexSearcher;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 231
    if-ne p0, p1, :cond_1

    .line 245
    :cond_0
    :goto_0
    return v1

    .line 233
    :cond_1
    invoke-super {p0, p1}, Lorg/apache/lucene/search/spans/SpanTermQuery;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    move v1, v2

    .line 234
    goto :goto_0

    .line 235
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 236
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 237
    check-cast v0, Lorg/apache/lucene/search/payloads/PayloadTermQuery;

    .line 238
    .local v0, "other":Lorg/apache/lucene/search/payloads/PayloadTermQuery;
    iget-object v3, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery;->function:Lorg/apache/lucene/search/payloads/PayloadFunction;

    if-nez v3, :cond_4

    .line 239
    iget-object v3, v0, Lorg/apache/lucene/search/payloads/PayloadTermQuery;->function:Lorg/apache/lucene/search/payloads/PayloadFunction;

    if-eqz v3, :cond_5

    move v1, v2

    .line 240
    goto :goto_0

    .line 241
    :cond_4
    iget-object v3, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery;->function:Lorg/apache/lucene/search/payloads/PayloadFunction;

    iget-object v4, v0, Lorg/apache/lucene/search/payloads/PayloadTermQuery;->function:Lorg/apache/lucene/search/payloads/PayloadFunction;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/search/payloads/PayloadFunction;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    move v1, v2

    .line 242
    goto :goto_0

    .line 243
    :cond_5
    iget-boolean v3, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery;->includeSpanScore:Z

    iget-boolean v4, v0, Lorg/apache/lucene/search/payloads/PayloadTermQuery;->includeSpanScore:Z

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 244
    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 222
    const/16 v0, 0x1f

    .line 223
    .local v0, "prime":I
    invoke-super {p0}, Lorg/apache/lucene/search/spans/SpanTermQuery;->hashCode()I

    move-result v1

    .line 224
    .local v1, "result":I
    mul-int/lit8 v3, v1, 0x1f

    iget-object v2, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery;->function:Lorg/apache/lucene/search/payloads/PayloadFunction;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    add-int v1, v3, v2

    .line 225
    mul-int/lit8 v3, v1, 0x1f

    iget-boolean v2, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery;->includeSpanScore:Z

    if-eqz v2, :cond_1

    const/16 v2, 0x4cf

    :goto_1
    add-int v1, v3, v2

    .line 226
    return v1

    .line 224
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery;->function:Lorg/apache/lucene/search/payloads/PayloadFunction;

    invoke-virtual {v2}, Lorg/apache/lucene/search/payloads/PayloadFunction;->hashCode()I

    move-result v2

    goto :goto_0

    .line 225
    :cond_1
    const/16 v2, 0x4d5

    goto :goto_1
.end method
