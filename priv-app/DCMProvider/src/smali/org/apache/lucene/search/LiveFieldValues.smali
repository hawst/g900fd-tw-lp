.class public abstract Lorg/apache/lucene/search/LiveFieldValues;
.super Ljava/lang/Object;
.source "LiveFieldValues.java"

# interfaces
.implements Ljava/io/Closeable;
.implements Lorg/apache/lucene/search/ReferenceManager$RefreshListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lorg/apache/lucene/search/ReferenceManager$RefreshListener;",
        "Ljava/io/Closeable;"
    }
.end annotation


# instance fields
.field private volatile current:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "TT;>;"
        }
    .end annotation
.end field

.field private final mgr:Lorg/apache/lucene/search/ReferenceManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/search/ReferenceManager",
            "<",
            "Lorg/apache/lucene/search/IndexSearcher;",
            ">;"
        }
    .end annotation
.end field

.field private final missingValue:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private volatile old:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/ReferenceManager;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/search/ReferenceManager",
            "<",
            "Lorg/apache/lucene/search/IndexSearcher;",
            ">;TT;)V"
        }
    .end annotation

    .prologue
    .line 43
    .local p0, "this":Lorg/apache/lucene/search/LiveFieldValues;, "Lorg/apache/lucene/search/LiveFieldValues<TT;>;"
    .local p1, "mgr":Lorg/apache/lucene/search/ReferenceManager;, "Lorg/apache/lucene/search/ReferenceManager<Lorg/apache/lucene/search/IndexSearcher;>;"
    .local p2, "missingValue":Ljava/lang/Object;, "TT;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/search/LiveFieldValues;->current:Ljava/util/Map;

    .line 39
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/search/LiveFieldValues;->old:Ljava/util/Map;

    .line 44
    iput-object p2, p0, Lorg/apache/lucene/search/LiveFieldValues;->missingValue:Ljava/lang/Object;

    .line 45
    iput-object p1, p0, Lorg/apache/lucene/search/LiveFieldValues;->mgr:Lorg/apache/lucene/search/ReferenceManager;

    .line 46
    invoke-virtual {p1, p0}, Lorg/apache/lucene/search/ReferenceManager;->addListener(Lorg/apache/lucene/search/ReferenceManager$RefreshListener;)V

    .line 47
    return-void
.end method


# virtual methods
.method public add(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1
    .param p1, "id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "TT;)V"
        }
    .end annotation

    .prologue
    .line 79
    .local p0, "this":Lorg/apache/lucene/search/LiveFieldValues;, "Lorg/apache/lucene/search/LiveFieldValues<TT;>;"
    .local p2, "value":Ljava/lang/Object;, "TT;"
    iget-object v0, p0, Lorg/apache/lucene/search/LiveFieldValues;->current:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    return-void
.end method

.method public afterRefresh(Z)V
    .locals 1
    .param p1, "didRefresh"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 72
    .local p0, "this":Lorg/apache/lucene/search/LiveFieldValues;, "Lorg/apache/lucene/search/LiveFieldValues<TT;>;"
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/search/LiveFieldValues;->old:Ljava/util/Map;

    .line 73
    return-void
.end method

.method public beforeRefresh()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 56
    .local p0, "this":Lorg/apache/lucene/search/LiveFieldValues;, "Lorg/apache/lucene/search/LiveFieldValues<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/LiveFieldValues;->current:Ljava/util/Map;

    iput-object v0, p0, Lorg/apache/lucene/search/LiveFieldValues;->old:Ljava/util/Map;

    .line 61
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/search/LiveFieldValues;->current:Ljava/util/Map;

    .line 62
    return-void
.end method

.method public close()V
    .locals 1

    .prologue
    .line 51
    .local p0, "this":Lorg/apache/lucene/search/LiveFieldValues;, "Lorg/apache/lucene/search/LiveFieldValues<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/LiveFieldValues;->mgr:Lorg/apache/lucene/search/ReferenceManager;

    invoke-virtual {v0, p0}, Lorg/apache/lucene/search/ReferenceManager;->removeListener(Lorg/apache/lucene/search/ReferenceManager$RefreshListener;)V

    .line 52
    return-void
.end method

.method public delete(Ljava/lang/String;)V
    .locals 2
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 85
    .local p0, "this":Lorg/apache/lucene/search/LiveFieldValues;, "Lorg/apache/lucene/search/LiveFieldValues<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/LiveFieldValues;->current:Ljava/util/Map;

    iget-object v1, p0, Lorg/apache/lucene/search/LiveFieldValues;->missingValue:Ljava/lang/Object;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    return-void
.end method

.method public get(Ljava/lang/String;)Ljava/lang/Object;
    .locals 4
    .param p1, "id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/search/LiveFieldValues;, "Lorg/apache/lucene/search/LiveFieldValues<TT;>;"
    const/4 v2, 0x0

    .line 98
    iget-object v3, p0, Lorg/apache/lucene/search/LiveFieldValues;->current:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 99
    .local v1, "value":Ljava/lang/Object;, "TT;"
    iget-object v3, p0, Lorg/apache/lucene/search/LiveFieldValues;->missingValue:Ljava/lang/Object;

    if-ne v1, v3, :cond_1

    .line 119
    :cond_0
    :goto_0
    return-object v2

    .line 103
    :cond_1
    if-eqz v1, :cond_2

    move-object v2, v1

    .line 104
    goto :goto_0

    .line 106
    :cond_2
    iget-object v3, p0, Lorg/apache/lucene/search/LiveFieldValues;->old:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 107
    iget-object v3, p0, Lorg/apache/lucene/search/LiveFieldValues;->missingValue:Ljava/lang/Object;

    if-eq v1, v3, :cond_0

    .line 111
    if-eqz v1, :cond_3

    move-object v2, v1

    .line 112
    goto :goto_0

    .line 117
    :cond_3
    iget-object v2, p0, Lorg/apache/lucene/search/LiveFieldValues;->mgr:Lorg/apache/lucene/search/ReferenceManager;

    invoke-virtual {v2}, Lorg/apache/lucene/search/ReferenceManager;->acquire()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/IndexSearcher;

    .line 119
    .local v0, "s":Lorg/apache/lucene/search/IndexSearcher;
    :try_start_0
    invoke-virtual {p0, v0, p1}, Lorg/apache/lucene/search/LiveFieldValues;->lookupFromSearcher(Lorg/apache/lucene/search/IndexSearcher;Ljava/lang/String;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 121
    iget-object v3, p0, Lorg/apache/lucene/search/LiveFieldValues;->mgr:Lorg/apache/lucene/search/ReferenceManager;

    invoke-virtual {v3, v0}, Lorg/apache/lucene/search/ReferenceManager;->release(Ljava/lang/Object;)V

    goto :goto_0

    .line 120
    :catchall_0
    move-exception v2

    .line 121
    iget-object v3, p0, Lorg/apache/lucene/search/LiveFieldValues;->mgr:Lorg/apache/lucene/search/ReferenceManager;

    invoke-virtual {v3, v0}, Lorg/apache/lucene/search/ReferenceManager;->release(Ljava/lang/Object;)V

    .line 122
    throw v2
.end method

.method protected abstract lookupFromSearcher(Lorg/apache/lucene/search/IndexSearcher;Ljava/lang/String;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/search/IndexSearcher;",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public size()I
    .locals 2

    .prologue
    .line 91
    .local p0, "this":Lorg/apache/lucene/search/LiveFieldValues;, "Lorg/apache/lucene/search/LiveFieldValues<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/LiveFieldValues;->current:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/search/LiveFieldValues;->old:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
