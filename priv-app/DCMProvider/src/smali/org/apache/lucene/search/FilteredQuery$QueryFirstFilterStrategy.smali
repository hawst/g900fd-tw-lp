.class final Lorg/apache/lucene/search/FilteredQuery$QueryFirstFilterStrategy;
.super Lorg/apache/lucene/search/FilteredQuery$FilterStrategy;
.source "FilteredQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FilteredQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "QueryFirstFilterStrategy"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 603
    invoke-direct {p0}, Lorg/apache/lucene/search/FilteredQuery$FilterStrategy;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/search/FilteredQuery$QueryFirstFilterStrategy;)V
    .locals 0

    .prologue
    .line 603
    invoke-direct {p0}, Lorg/apache/lucene/search/FilteredQuery$QueryFirstFilterStrategy;-><init>()V

    return-void
.end method


# virtual methods
.method public filteredScorer(Lorg/apache/lucene/index/AtomicReaderContext;ZZLorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/DocIdSet;)Lorg/apache/lucene/search/Scorer;
    .locals 8
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "scoreDocsInOrder"    # Z
    .param p3, "topScorer"    # Z
    .param p4, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p5, "docIdSet"    # Lorg/apache/lucene/search/DocIdSet;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 608
    invoke-virtual {p5}, Lorg/apache/lucene/search/DocIdSet;->bits()Lorg/apache/lucene/util/Bits;

    move-result-object v6

    .line 609
    .local v6, "filterAcceptDocs":Lorg/apache/lucene/util/Bits;
    if-nez v6, :cond_1

    .line 610
    sget-object v0, Lorg/apache/lucene/search/FilteredQuery;->LEAP_FROG_QUERY_FIRST_STRATEGY:Lorg/apache/lucene/search/FilteredQuery$FilterStrategy;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lorg/apache/lucene/search/FilteredQuery$FilterStrategy;->filteredScorer(Lorg/apache/lucene/index/AtomicReaderContext;ZZLorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/DocIdSet;)Lorg/apache/lucene/search/Scorer;

    move-result-object v0

    .line 613
    :cond_0
    :goto_0
    return-object v0

    .line 612
    :cond_1
    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p4, p1, v1, v2, v0}, Lorg/apache/lucene/search/Weight;->scorer(Lorg/apache/lucene/index/AtomicReaderContext;ZZLorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/Scorer;

    move-result-object v7

    .line 613
    .local v7, "scorer":Lorg/apache/lucene/search/Scorer;
    if-eqz v7, :cond_0

    new-instance v0, Lorg/apache/lucene/search/FilteredQuery$QueryFirstScorer;

    .line 614
    invoke-direct {v0, p4, v6, v7}, Lorg/apache/lucene/search/FilteredQuery$QueryFirstScorer;-><init>(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/search/Scorer;)V

    goto :goto_0
.end method
