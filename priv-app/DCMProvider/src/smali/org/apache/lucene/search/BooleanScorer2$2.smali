.class Lorg/apache/lucene/search/BooleanScorer2$2;
.super Lorg/apache/lucene/search/DisjunctionSumScorer;
.source "BooleanScorer2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/BooleanScorer2;->countingDisjunctionSumScorer(Ljava/util/List;I)Lorg/apache/lucene/search/Scorer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/search/BooleanScorer2;


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/BooleanScorer2;Lorg/apache/lucene/search/Weight;[Lorg/apache/lucene/search/Scorer;[F)V
    .locals 0
    .param p2, "$anonymous0"    # Lorg/apache/lucene/search/Weight;
    .param p3, "$anonymous1"    # [Lorg/apache/lucene/search/Scorer;
    .param p4, "$anonymous2"    # [F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/search/BooleanScorer2$2;->this$0:Lorg/apache/lucene/search/BooleanScorer2;

    .line 170
    invoke-direct {p0, p2, p3, p4}, Lorg/apache/lucene/search/DisjunctionSumScorer;-><init>(Lorg/apache/lucene/search/Weight;[Lorg/apache/lucene/search/Scorer;[F)V

    return-void
.end method


# virtual methods
.method public score()F
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 173
    iget-object v0, p0, Lorg/apache/lucene/search/BooleanScorer2$2;->this$0:Lorg/apache/lucene/search/BooleanScorer2;

    # getter for: Lorg/apache/lucene/search/BooleanScorer2;->coordinator:Lorg/apache/lucene/search/BooleanScorer2$Coordinator;
    invoke-static {v0}, Lorg/apache/lucene/search/BooleanScorer2;->access$2(Lorg/apache/lucene/search/BooleanScorer2;)Lorg/apache/lucene/search/BooleanScorer2$Coordinator;

    move-result-object v0

    iget v1, v0, Lorg/apache/lucene/search/BooleanScorer2$Coordinator;->nrMatchers:I

    iget v2, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->nrMatchers:I

    add-int/2addr v1, v2

    iput v1, v0, Lorg/apache/lucene/search/BooleanScorer2$Coordinator;->nrMatchers:I

    .line 174
    iget-wide v0, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->score:D

    double-to-float v0, v0

    return v0
.end method
