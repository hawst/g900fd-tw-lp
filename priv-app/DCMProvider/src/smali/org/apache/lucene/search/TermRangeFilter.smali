.class public Lorg/apache/lucene/search/TermRangeFilter;
.super Lorg/apache/lucene/search/MultiTermQueryWrapperFilter;
.source "TermRangeFilter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/MultiTermQueryWrapperFilter",
        "<",
        "Lorg/apache/lucene/search/TermRangeQuery;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;ZZ)V
    .locals 6
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "lowerTerm"    # Lorg/apache/lucene/util/BytesRef;
    .param p3, "upperTerm"    # Lorg/apache/lucene/util/BytesRef;
    .param p4, "includeLower"    # Z
    .param p5, "includeUpper"    # Z

    .prologue
    .line 49
    new-instance v0, Lorg/apache/lucene/search/TermRangeQuery;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/search/TermRangeQuery;-><init>(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;ZZ)V

    invoke-direct {p0, v0}, Lorg/apache/lucene/search/MultiTermQueryWrapperFilter;-><init>(Lorg/apache/lucene/search/MultiTermQuery;)V

    .line 50
    return-void
.end method

.method public static Less(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/search/TermRangeFilter;
    .locals 6
    .param p0, "fieldName"    # Ljava/lang/String;
    .param p1, "upperTerm"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 66
    new-instance v0, Lorg/apache/lucene/search/TermRangeFilter;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v1, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/search/TermRangeFilter;-><init>(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;ZZ)V

    return-object v0
.end method

.method public static More(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/search/TermRangeFilter;
    .locals 6
    .param p0, "fieldName"    # Ljava/lang/String;
    .param p1, "lowerTerm"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 74
    new-instance v0, Lorg/apache/lucene/search/TermRangeFilter;

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/search/TermRangeFilter;-><init>(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;ZZ)V

    return-object v0
.end method

.method public static newStringRange(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Lorg/apache/lucene/search/TermRangeFilter;
    .locals 6
    .param p0, "field"    # Ljava/lang/String;
    .param p1, "lowerTerm"    # Ljava/lang/String;
    .param p2, "upperTerm"    # Ljava/lang/String;
    .param p3, "includeLower"    # Z
    .param p4, "includeUpper"    # Z

    .prologue
    const/4 v3, 0x0

    .line 56
    if-nez p1, :cond_0

    move-object v2, v3

    .line 57
    .local v2, "lower":Lorg/apache/lucene/util/BytesRef;
    :goto_0
    if-nez p2, :cond_1

    .line 58
    .local v3, "upper":Lorg/apache/lucene/util/BytesRef;
    :goto_1
    new-instance v0, Lorg/apache/lucene/search/TermRangeFilter;

    move-object v1, p0

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/search/TermRangeFilter;-><init>(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;ZZ)V

    return-object v0

    .line 56
    .end local v2    # "lower":Lorg/apache/lucene/util/BytesRef;
    .end local v3    # "upper":Lorg/apache/lucene/util/BytesRef;
    :cond_0
    new-instance v2, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v2, p1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 57
    .restart local v2    # "lower":Lorg/apache/lucene/util/BytesRef;
    :cond_1
    new-instance v3, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v3, p2}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method


# virtual methods
.method public getLowerTerm()Lorg/apache/lucene/util/BytesRef;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lorg/apache/lucene/search/TermRangeFilter;->query:Lorg/apache/lucene/search/MultiTermQuery;

    check-cast v0, Lorg/apache/lucene/search/TermRangeQuery;

    invoke-virtual {v0}, Lorg/apache/lucene/search/TermRangeQuery;->getLowerTerm()Lorg/apache/lucene/util/BytesRef;

    move-result-object v0

    return-object v0
.end method

.method public getUpperTerm()Lorg/apache/lucene/util/BytesRef;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lorg/apache/lucene/search/TermRangeFilter;->query:Lorg/apache/lucene/search/MultiTermQuery;

    check-cast v0, Lorg/apache/lucene/search/TermRangeQuery;

    invoke-virtual {v0}, Lorg/apache/lucene/search/TermRangeQuery;->getUpperTerm()Lorg/apache/lucene/util/BytesRef;

    move-result-object v0

    return-object v0
.end method

.method public includesLower()Z
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lorg/apache/lucene/search/TermRangeFilter;->query:Lorg/apache/lucene/search/MultiTermQuery;

    check-cast v0, Lorg/apache/lucene/search/TermRangeQuery;

    invoke-virtual {v0}, Lorg/apache/lucene/search/TermRangeQuery;->includesLower()Z

    move-result v0

    return v0
.end method

.method public includesUpper()Z
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lorg/apache/lucene/search/TermRangeFilter;->query:Lorg/apache/lucene/search/MultiTermQuery;

    check-cast v0, Lorg/apache/lucene/search/TermRangeQuery;

    invoke-virtual {v0}, Lorg/apache/lucene/search/TermRangeQuery;->includesUpper()Z

    move-result v0

    return v0
.end method
