.class final Lorg/apache/lucene/search/FilteredQuery$PrimaryAdvancedLeapFrogScorer;
.super Lorg/apache/lucene/search/FilteredQuery$LeapFrogScorer;
.source "FilteredQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FilteredQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "PrimaryAdvancedLeapFrogScorer"
.end annotation


# instance fields
.field private final firstFilteredDoc:I


# direct methods
.method protected constructor <init>(Lorg/apache/lucene/search/Weight;ILorg/apache/lucene/search/DocIdSetIterator;Lorg/apache/lucene/search/Scorer;)V
    .locals 0
    .param p1, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p2, "firstFilteredDoc"    # I
    .param p3, "filterIter"    # Lorg/apache/lucene/search/DocIdSetIterator;
    .param p4, "other"    # Lorg/apache/lucene/search/Scorer;

    .prologue
    .line 324
    invoke-direct {p0, p1, p3, p4, p4}, Lorg/apache/lucene/search/FilteredQuery$LeapFrogScorer;-><init>(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/DocIdSetIterator;Lorg/apache/lucene/search/DocIdSetIterator;Lorg/apache/lucene/search/Scorer;)V

    .line 325
    iput p2, p0, Lorg/apache/lucene/search/FilteredQuery$PrimaryAdvancedLeapFrogScorer;->firstFilteredDoc:I

    .line 326
    iput p2, p0, Lorg/apache/lucene/search/FilteredQuery$PrimaryAdvancedLeapFrogScorer;->primaryDoc:I

    .line 327
    return-void
.end method


# virtual methods
.method protected primaryNext()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 331
    iget v0, p0, Lorg/apache/lucene/search/FilteredQuery$PrimaryAdvancedLeapFrogScorer;->secondaryDoc:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 332
    invoke-super {p0}, Lorg/apache/lucene/search/FilteredQuery$LeapFrogScorer;->primaryNext()I

    move-result v0

    .line 334
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lorg/apache/lucene/search/FilteredQuery$PrimaryAdvancedLeapFrogScorer;->firstFilteredDoc:I

    goto :goto_0
.end method
