.class public Lorg/apache/lucene/search/SearcherLifetimeManager;
.super Ljava/lang/Object;
.source "SearcherLifetimeManager.java"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/SearcherLifetimeManager$PruneByAge;,
        Lorg/apache/lucene/search/SearcherLifetimeManager$Pruner;,
        Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;
    }
.end annotation


# static fields
.field static final NANOS_PER_SEC:D = 1.0E9


# instance fields
.field private volatile closed:Z

.field private final searchers:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/Long;",
            "Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 136
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/search/SearcherLifetimeManager;->searchers:Ljava/util/concurrent/ConcurrentHashMap;

    .line 101
    return-void
.end method

.method private ensureOpen()V
    .locals 2

    .prologue
    .line 139
    iget-boolean v0, p0, Lorg/apache/lucene/search/SearcherLifetimeManager;->closed:Z

    if-eqz v0, :cond_0

    .line 140
    new-instance v0, Lorg/apache/lucene/store/AlreadyClosedException;

    const-string v1, "this SearcherLifetimeManager instance is closed"

    invoke-direct {v0, v1}, Lorg/apache/lucene/store/AlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 142
    :cond_0
    return-void
.end method


# virtual methods
.method public acquire(J)Lorg/apache/lucene/search/IndexSearcher;
    .locals 3
    .param p1, "version"    # J

    .prologue
    .line 192
    invoke-direct {p0}, Lorg/apache/lucene/search/SearcherLifetimeManager;->ensureOpen()V

    .line 193
    iget-object v1, p0, Lorg/apache/lucene/search/SearcherLifetimeManager;->searchers:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;

    .line 194
    .local v0, "tracker":Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;
    if-eqz v0, :cond_0

    .line 195
    iget-object v1, v0, Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;->searcher:Lorg/apache/lucene/search/IndexSearcher;

    invoke-virtual {v1}, Lorg/apache/lucene/search/IndexSearcher;->getIndexReader()Lorg/apache/lucene/index/IndexReader;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/lucene/index/IndexReader;->tryIncRef()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 196
    iget-object v1, v0, Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;->searcher:Lorg/apache/lucene/search/IndexSearcher;

    .line 199
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public declared-synchronized close()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 289
    monitor-enter p0

    const/4 v2, 0x1

    :try_start_0
    iput-boolean v2, p0, Lorg/apache/lucene/search/SearcherLifetimeManager;->closed:Z

    .line 290
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lorg/apache/lucene/search/SearcherLifetimeManager;->searchers:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 294
    .local v0, "toClose":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 298
    invoke-static {v0}, Lorg/apache/lucene/util/IOUtils;->close(Ljava/lang/Iterable;)V

    .line 301
    iget-object v2, p0, Lorg/apache/lucene/search/SearcherLifetimeManager;->searchers:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result v2

    if-eqz v2, :cond_1

    .line 302
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "another thread called record while this SearcherLifetimeManager instance was being closed; not all searchers were closed"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 289
    .end local v0    # "toClose":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;>;"
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 294
    .restart local v0    # "toClose":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;>;"
    :cond_0
    :try_start_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;

    .line 295
    .local v1, "tracker":Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;
    iget-object v3, p0, Lorg/apache/lucene/search/SearcherLifetimeManager;->searchers:Ljava/util/concurrent/ConcurrentHashMap;

    iget-wide v4, v1, Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;->version:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 304
    .end local v1    # "tracker":Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;
    :cond_1
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized prune(Lorg/apache/lucene/search/SearcherLifetimeManager$Pruner;)V
    .locals 12
    .param p1, "pruner"    # Lorg/apache/lucene/search/SearcherLifetimeManager$Pruner;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 251
    monitor-enter p0

    :try_start_0
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 252
    .local v7, "trackers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;>;"
    iget-object v8, p0, Lorg/apache/lucene/search/SearcherLifetimeManager;->searchers:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v8}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-nez v9, :cond_0

    .line 255
    invoke-static {v7}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 256
    const-wide/16 v2, 0x0

    .line 257
    .local v2, "lastRecordTimeSec":D
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v8

    long-to-double v8, v8

    const-wide v10, 0x41cdcd6500000000L    # 1.0E9

    div-double v4, v8, v10

    .line 258
    .local v4, "now":D
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v9

    if-nez v9, :cond_1

    .line 276
    monitor-exit p0

    return-void

    .line 252
    .end local v2    # "lastRecordTimeSec":D
    .end local v4    # "now":D
    :cond_0
    :try_start_1
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;

    .line 253
    .local v6, "tracker":Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;
    invoke-interface {v7, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 251
    .end local v6    # "tracker":Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;
    .end local v7    # "trackers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;>;"
    :catchall_0
    move-exception v8

    monitor-exit p0

    throw v8

    .line 258
    .restart local v2    # "lastRecordTimeSec":D
    .restart local v4    # "now":D
    .restart local v7    # "trackers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;>;"
    :cond_1
    :try_start_2
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;

    .line 260
    .restart local v6    # "tracker":Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;
    const-wide/16 v10, 0x0

    cmpl-double v9, v2, v10

    if-nez v9, :cond_3

    .line 261
    const-wide/16 v0, 0x0

    .line 269
    .local v0, "ageSec":D
    :goto_2
    iget-object v9, v6, Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;->searcher:Lorg/apache/lucene/search/IndexSearcher;

    invoke-interface {p1, v0, v1, v9}, Lorg/apache/lucene/search/SearcherLifetimeManager$Pruner;->doPrune(DLorg/apache/lucene/search/IndexSearcher;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 271
    iget-object v9, p0, Lorg/apache/lucene/search/SearcherLifetimeManager;->searchers:Ljava/util/concurrent/ConcurrentHashMap;

    iget-wide v10, v6, Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;->version:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 272
    invoke-virtual {v6}, Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;->close()V

    .line 274
    :cond_2
    iget-wide v2, v6, Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;->recordTimeSec:D
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 263
    .end local v0    # "ageSec":D
    :cond_3
    sub-double v0, v4, v2

    .restart local v0    # "ageSec":D
    goto :goto_2
.end method

.method public record(Lorg/apache/lucene/search/IndexSearcher;)J
    .locals 6
    .param p1, "searcher"    # Lorg/apache/lucene/search/IndexSearcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 158
    invoke-direct {p0}, Lorg/apache/lucene/search/SearcherLifetimeManager;->ensureOpen()V

    .line 162
    invoke-virtual {p1}, Lorg/apache/lucene/search/IndexSearcher;->getIndexReader()Lorg/apache/lucene/index/IndexReader;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/DirectoryReader;

    invoke-virtual {v1}, Lorg/apache/lucene/index/DirectoryReader;->getVersion()J

    move-result-wide v2

    .line 163
    .local v2, "version":J
    iget-object v1, p0, Lorg/apache/lucene/search/SearcherLifetimeManager;->searchers:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;

    .line 164
    .local v0, "tracker":Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;
    if-nez v0, :cond_1

    .line 166
    new-instance v0, Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;

    .end local v0    # "tracker":Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;
    invoke-direct {v0, p1}, Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;-><init>(Lorg/apache/lucene/search/IndexSearcher;)V

    .line 167
    .restart local v0    # "tracker":Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;
    iget-object v1, p0, Lorg/apache/lucene/search/SearcherLifetimeManager;->searchers:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1, v4, v0}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 170
    invoke-virtual {v0}, Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;->close()V

    .line 176
    :cond_0
    return-wide v2

    .line 172
    :cond_1
    iget-object v1, v0, Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;->searcher:Lorg/apache/lucene/search/IndexSearcher;

    if-eq v1, p1, :cond_0

    .line 173
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "the provided searcher has the same underlying reader version yet the searcher instance differs from before (new="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " vs old="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;->searcher:Lorg/apache/lucene/search/IndexSearcher;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public release(Lorg/apache/lucene/search/IndexSearcher;)V
    .locals 1
    .param p1, "s"    # Lorg/apache/lucene/search/IndexSearcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 207
    invoke-virtual {p1}, Lorg/apache/lucene/search/IndexSearcher;->getIndexReader()Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexReader;->decRef()V

    .line 208
    return-void
.end method
