.class public Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;
.super Lorg/apache/lucene/search/Scorer;
.source "ConstantScoreQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/ConstantScoreQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "ConstantScorer"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final docIdSetIterator:Lorg/apache/lucene/search/DocIdSetIterator;

.field final theScore:F

.field final synthetic this$0:Lorg/apache/lucene/search/ConstantScoreQuery;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 173
    const-class v0, Lorg/apache/lucene/search/ConstantScoreQuery;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/search/ConstantScoreQuery;Lorg/apache/lucene/search/DocIdSetIterator;Lorg/apache/lucene/search/Weight;F)V
    .locals 0
    .param p2, "docIdSetIterator"    # Lorg/apache/lucene/search/DocIdSetIterator;
    .param p3, "w"    # Lorg/apache/lucene/search/Weight;
    .param p4, "theScore"    # F

    .prologue
    .line 177
    iput-object p1, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;->this$0:Lorg/apache/lucene/search/ConstantScoreQuery;

    .line 178
    invoke-direct {p0, p3}, Lorg/apache/lucene/search/Scorer;-><init>(Lorg/apache/lucene/search/Weight;)V

    .line 179
    iput p4, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;->theScore:F

    .line 180
    iput-object p2, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;->docIdSetIterator:Lorg/apache/lucene/search/DocIdSetIterator;

    .line 181
    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;)Lorg/apache/lucene/search/ConstantScoreQuery;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;->this$0:Lorg/apache/lucene/search/ConstantScoreQuery;

    return-object v0
.end method

.method private wrapCollector(Lorg/apache/lucene/search/Collector;)Lorg/apache/lucene/search/Collector;
    .locals 1
    .param p1, "collector"    # Lorg/apache/lucene/search/Collector;

    .prologue
    .line 215
    new-instance v0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer$1;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer$1;-><init>(Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;Lorg/apache/lucene/search/Collector;)V

    return-object v0
.end method


# virtual methods
.method public advance(I)I
    .locals 1
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 206
    iget-object v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;->docIdSetIterator:Lorg/apache/lucene/search/DocIdSetIterator;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/DocIdSetIterator;->advance(I)I

    move-result v0

    return v0
.end method

.method public cost()J
    .locals 2

    .prologue
    .line 211
    iget-object v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;->docIdSetIterator:Lorg/apache/lucene/search/DocIdSetIterator;

    invoke-virtual {v0}, Lorg/apache/lucene/search/DocIdSetIterator;->cost()J

    move-result-wide v0

    return-wide v0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;->docIdSetIterator:Lorg/apache/lucene/search/DocIdSetIterator;

    invoke-virtual {v0}, Lorg/apache/lucene/search/DocIdSetIterator;->docID()I

    move-result v0

    return v0
.end method

.method public freq()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 201
    const/4 v0, 0x1

    return v0
.end method

.method public getChildren()Ljava/util/Collection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/search/Scorer$ChildScorer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 261
    iget-object v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;->docIdSetIterator:Lorg/apache/lucene/search/DocIdSetIterator;

    instance-of v0, v0, Lorg/apache/lucene/search/Scorer;

    if-eqz v0, :cond_0

    .line 262
    new-instance v1, Lorg/apache/lucene/search/Scorer$ChildScorer;

    iget-object v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;->docIdSetIterator:Lorg/apache/lucene/search/DocIdSetIterator;

    check-cast v0, Lorg/apache/lucene/search/Scorer;

    const-string v2, "constant"

    invoke-direct {v1, v0, v2}, Lorg/apache/lucene/search/Scorer$ChildScorer;-><init>(Lorg/apache/lucene/search/Scorer;Ljava/lang/String;)V

    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 264
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public nextDoc()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 185
    iget-object v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;->docIdSetIterator:Lorg/apache/lucene/search/DocIdSetIterator;

    invoke-virtual {v0}, Lorg/apache/lucene/search/DocIdSetIterator;->nextDoc()I

    move-result v0

    return v0
.end method

.method public score()F
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 195
    sget-boolean v0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;->docIdSetIterator:Lorg/apache/lucene/search/DocIdSetIterator;

    invoke-virtual {v0}, Lorg/apache/lucene/search/DocIdSetIterator;->docID()I

    move-result v0

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 196
    :cond_0
    iget v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;->theScore:F

    return v0
.end method

.method public score(Lorg/apache/lucene/search/Collector;)V
    .locals 2
    .param p1, "collector"    # Lorg/apache/lucene/search/Collector;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 242
    iget-object v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;->docIdSetIterator:Lorg/apache/lucene/search/DocIdSetIterator;

    instance-of v0, v0, Lorg/apache/lucene/search/Scorer;

    if-eqz v0, :cond_0

    .line 243
    iget-object v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;->docIdSetIterator:Lorg/apache/lucene/search/DocIdSetIterator;

    check-cast v0, Lorg/apache/lucene/search/Scorer;

    invoke-direct {p0, p1}, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;->wrapCollector(Lorg/apache/lucene/search/Collector;)Lorg/apache/lucene/search/Collector;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/Scorer;->score(Lorg/apache/lucene/search/Collector;)V

    .line 247
    :goto_0
    return-void

    .line 245
    :cond_0
    invoke-super {p0, p1}, Lorg/apache/lucene/search/Scorer;->score(Lorg/apache/lucene/search/Collector;)V

    goto :goto_0
.end method

.method public score(Lorg/apache/lucene/search/Collector;II)Z
    .locals 2
    .param p1, "collector"    # Lorg/apache/lucene/search/Collector;
    .param p2, "max"    # I
    .param p3, "firstDocID"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 252
    iget-object v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;->docIdSetIterator:Lorg/apache/lucene/search/DocIdSetIterator;

    instance-of v0, v0, Lorg/apache/lucene/search/Scorer;

    if-eqz v0, :cond_0

    .line 253
    iget-object v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;->docIdSetIterator:Lorg/apache/lucene/search/DocIdSetIterator;

    check-cast v0, Lorg/apache/lucene/search/Scorer;

    invoke-direct {p0, p1}, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;->wrapCollector(Lorg/apache/lucene/search/Collector;)Lorg/apache/lucene/search/Collector;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3}, Lorg/apache/lucene/search/Scorer;->score(Lorg/apache/lucene/search/Collector;II)Z

    move-result v0

    .line 255
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lorg/apache/lucene/search/Scorer;->score(Lorg/apache/lucene/search/Collector;II)Z

    move-result v0

    goto :goto_0
.end method
