.class Lorg/apache/lucene/search/DocTermOrdsRewriteMethod$MultiTermQueryDocTermOrdsWrapperFilter$2;
.super Lorg/apache/lucene/search/FieldCacheDocIdSet;
.source "DocTermOrdsRewriteMethod.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/DocTermOrdsRewriteMethod$MultiTermQueryDocTermOrdsWrapperFilter;->getDocIdSet(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/DocIdSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lorg/apache/lucene/search/DocTermOrdsRewriteMethod$MultiTermQueryDocTermOrdsWrapperFilter;

.field private final synthetic val$docTermOrds:Lorg/apache/lucene/index/SortedSetDocValues;

.field private final synthetic val$termSet:Lorg/apache/lucene/util/OpenBitSet;


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/DocTermOrdsRewriteMethod$MultiTermQueryDocTermOrdsWrapperFilter;ILorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/SortedSetDocValues;Lorg/apache/lucene/util/OpenBitSet;)V
    .locals 0
    .param p2, "$anonymous0"    # I
    .param p3, "$anonymous1"    # Lorg/apache/lucene/util/Bits;

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/search/DocTermOrdsRewriteMethod$MultiTermQueryDocTermOrdsWrapperFilter$2;->this$1:Lorg/apache/lucene/search/DocTermOrdsRewriteMethod$MultiTermQueryDocTermOrdsWrapperFilter;

    iput-object p4, p0, Lorg/apache/lucene/search/DocTermOrdsRewriteMethod$MultiTermQueryDocTermOrdsWrapperFilter$2;->val$docTermOrds:Lorg/apache/lucene/index/SortedSetDocValues;

    iput-object p5, p0, Lorg/apache/lucene/search/DocTermOrdsRewriteMethod$MultiTermQueryDocTermOrdsWrapperFilter$2;->val$termSet:Lorg/apache/lucene/util/OpenBitSet;

    .line 149
    invoke-direct {p0, p2, p3}, Lorg/apache/lucene/search/FieldCacheDocIdSet;-><init>(ILorg/apache/lucene/util/Bits;)V

    return-void
.end method


# virtual methods
.method protected final matchDoc(I)Z
    .locals 4
    .param p1, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ArrayIndexOutOfBoundsException;
        }
    .end annotation

    .prologue
    .line 152
    iget-object v2, p0, Lorg/apache/lucene/search/DocTermOrdsRewriteMethod$MultiTermQueryDocTermOrdsWrapperFilter$2;->val$docTermOrds:Lorg/apache/lucene/index/SortedSetDocValues;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/index/SortedSetDocValues;->setDocument(I)V

    .line 155
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/search/DocTermOrdsRewriteMethod$MultiTermQueryDocTermOrdsWrapperFilter$2;->val$docTermOrds:Lorg/apache/lucene/index/SortedSetDocValues;

    invoke-virtual {v2}, Lorg/apache/lucene/index/SortedSetDocValues;->nextOrd()J

    move-result-wide v0

    .local v0, "ord":J
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_1

    .line 160
    const/4 v2, 0x0

    :goto_0
    return v2

    .line 156
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/search/DocTermOrdsRewriteMethod$MultiTermQueryDocTermOrdsWrapperFilter$2;->val$termSet:Lorg/apache/lucene/util/OpenBitSet;

    invoke-virtual {v2, v0, v1}, Lorg/apache/lucene/util/OpenBitSet;->get(J)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 157
    const/4 v2, 0x1

    goto :goto_0
.end method
