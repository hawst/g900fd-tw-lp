.class Lorg/apache/lucene/search/DisjunctionMaxScorer;
.super Lorg/apache/lucene/search/DisjunctionScorer;
.source "DisjunctionMaxScorer.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private doc:I

.field private freq:I

.field private scoreMax:F

.field private scoreSum:F

.field private final tieBreakerMultiplier:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lorg/apache/lucene/search/DisjunctionMaxScorer;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/search/Weight;F[Lorg/apache/lucene/search/Scorer;I)V
    .locals 1
    .param p1, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p2, "tieBreakerMultiplier"    # F
    .param p3, "subScorers"    # [Lorg/apache/lucene/search/Scorer;
    .param p4, "numScorers"    # I

    .prologue
    const/4 v0, -0x1

    .line 53
    invoke-direct {p0, p1, p3, p4}, Lorg/apache/lucene/search/DisjunctionScorer;-><init>(Lorg/apache/lucene/search/Weight;[Lorg/apache/lucene/search/Scorer;I)V

    .line 30
    iput v0, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->doc:I

    .line 31
    iput v0, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->freq:I

    .line 54
    iput p2, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->tieBreakerMultiplier:F

    .line 55
    return-void
.end method

.method private afterNext()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 90
    iget-object v0, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v0, v0, v2

    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->doc:I

    .line 91
    iget v0, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->doc:I

    const v1, 0x7fffffff

    if-eq v0, v1, :cond_0

    .line 92
    iget-object v0, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v0, v0, v2

    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->scoreMax:F

    iput v0, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->scoreSum:F

    .line 93
    iput v3, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->freq:I

    .line 94
    invoke-direct {p0, v3}, Lorg/apache/lucene/search/DisjunctionMaxScorer;->scoreAll(I)V

    .line 95
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lorg/apache/lucene/search/DisjunctionMaxScorer;->scoreAll(I)V

    .line 97
    :cond_0
    return-void
.end method

.method private scoreAll(I)V
    .locals 3
    .param p1, "root"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 101
    iget v1, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->numScorers:I

    if-ge p1, v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v1, v1, p1

    invoke-virtual {v1}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v1

    iget v2, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->doc:I

    if-ne v1, v2, :cond_0

    .line 102
    iget-object v1, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v1, v1, p1

    invoke-virtual {v1}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v0

    .line 103
    .local v0, "sub":F
    iget v1, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->freq:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->freq:I

    .line 104
    iget v1, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->scoreSum:F

    add-float/2addr v1, v0

    iput v1, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->scoreSum:F

    .line 105
    iget v1, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->scoreMax:F

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v1

    iput v1, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->scoreMax:F

    .line 106
    shl-int/lit8 v1, p1, 0x1

    add-int/lit8 v1, v1, 0x1

    invoke-direct {p0, v1}, Lorg/apache/lucene/search/DisjunctionMaxScorer;->scoreAll(I)V

    .line 107
    shl-int/lit8 v1, p1, 0x1

    add-int/lit8 v1, v1, 0x2

    invoke-direct {p0, v1}, Lorg/apache/lucene/search/DisjunctionMaxScorer;->scoreAll(I)V

    .line 109
    .end local v0    # "sub":F
    :cond_0
    return-void
.end method


# virtual methods
.method public advance(I)I
    .locals 3
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const v0, 0x7fffffff

    const/4 v2, 0x0

    .line 118
    sget-boolean v1, Lorg/apache/lucene/search/DisjunctionMaxScorer;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget v1, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->doc:I

    if-ne v1, v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 120
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v1, v1, v2

    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/Scorer;->advance(I)I

    move-result v1

    if-eq v1, v0, :cond_2

    .line 121
    invoke-virtual {p0, v2}, Lorg/apache/lucene/search/DisjunctionMaxScorer;->heapAdjust(I)V

    .line 128
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v1, v1, v2

    invoke-virtual {v1}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v1

    if-lt v1, p1, :cond_0

    .line 129
    invoke-direct {p0}, Lorg/apache/lucene/search/DisjunctionMaxScorer;->afterNext()V

    .line 130
    iget v0, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->doc:I

    :goto_0
    return v0

    .line 123
    :cond_2
    invoke-virtual {p0}, Lorg/apache/lucene/search/DisjunctionMaxScorer;->heapRemoveRoot()V

    .line 124
    iget v1, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->numScorers:I

    if-nez v1, :cond_1

    .line 125
    iput v0, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->doc:I

    goto :goto_0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 78
    iget v0, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->doc:I

    return v0
.end method

.method public freq()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 113
    iget v0, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->freq:I

    return v0
.end method

.method public nextDoc()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const v0, 0x7fffffff

    const/4 v3, 0x0

    .line 59
    sget-boolean v1, Lorg/apache/lucene/search/DisjunctionMaxScorer;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget v1, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->doc:I

    if-ne v1, v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 61
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v1, v1, v3

    invoke-virtual {v1}, Lorg/apache/lucene/search/Scorer;->nextDoc()I

    move-result v1

    if-eq v1, v0, :cond_2

    .line 62
    invoke-virtual {p0, v3}, Lorg/apache/lucene/search/DisjunctionMaxScorer;->heapAdjust(I)V

    .line 69
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v1, v1, v3

    invoke-virtual {v1}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v1

    iget v2, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->doc:I

    if-eq v1, v2, :cond_0

    .line 70
    invoke-direct {p0}, Lorg/apache/lucene/search/DisjunctionMaxScorer;->afterNext()V

    .line 71
    iget v0, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->doc:I

    :goto_0
    return v0

    .line 64
    :cond_2
    invoke-virtual {p0}, Lorg/apache/lucene/search/DisjunctionMaxScorer;->heapRemoveRoot()V

    .line 65
    iget v1, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->numScorers:I

    if-nez v1, :cond_1

    .line 66
    iput v0, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->doc:I

    goto :goto_0
.end method

.method public score()F
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 86
    iget v0, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->scoreMax:F

    iget v1, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->scoreSum:F

    iget v2, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->scoreMax:F

    sub-float/2addr v1, v2

    iget v2, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->tieBreakerMultiplier:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    return v0
.end method
