.class public abstract Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;
.super Ljava/lang/Object;
.source "Similarity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/similarities/Similarity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "SloppySimScorer"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 238
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract computePayloadFactor(IIILorg/apache/lucene/util/BytesRef;)F
.end method

.method public abstract computeSlopFactor(I)F
.end method

.method public explain(ILorg/apache/lucene/search/Explanation;)Lorg/apache/lucene/search/Explanation;
    .locals 4
    .param p1, "doc"    # I
    .param p2, "freq"    # Lorg/apache/lucene/search/Explanation;

    .prologue
    .line 261
    new-instance v0, Lorg/apache/lucene/search/Explanation;

    invoke-virtual {p2}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v1

    invoke-virtual {p0, p1, v1}, Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;->score(IF)F

    move-result v1

    .line 262
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "score(doc="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",freq="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "), with freq of:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 261
    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    .line 263
    .local v0, "result":Lorg/apache/lucene/search/Explanation;
    invoke-virtual {v0, p2}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 264
    return-object v0
.end method

.method public abstract score(IF)F
.end method
