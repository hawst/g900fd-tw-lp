.class public abstract Lorg/apache/lucene/search/TopFieldCollector;
.super Lorg/apache/lucene/search/TopDocsCollector;
.source "TopFieldCollector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorNonScoringCollector;,
        Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringMaxScoreCollector;,
        Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringNoMaxScoreCollector;,
        Lorg/apache/lucene/search/TopFieldCollector$OneComparatorNonScoringCollector;,
        Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringMaxScoreCollector;,
        Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringNoMaxScoreCollector;,
        Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorNonScoringCollector;,
        Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringMaxScoreCollector;,
        Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringNoMaxScoreCollector;,
        Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderOneComparatorNonScoringCollector;,
        Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderOneComparatorScoringMaxScoreCollector;,
        Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderOneComparatorScoringNoMaxScoreCollector;,
        Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/TopDocsCollector",
        "<",
        "Lorg/apache/lucene/search/FieldValueHitQueue$Entry;",
        ">;"
    }
.end annotation


# static fields
.field private static final EMPTY_SCOREDOCS:[Lorg/apache/lucene/search/ScoreDoc;


# instance fields
.field bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

.field docBase:I

.field private final fillFields:Z

.field maxScore:F

.field final numHits:I

.field queueFull:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1003
    const/4 v0, 0x0

    new-array v0, v0, [Lorg/apache/lucene/search/ScoreDoc;

    sput-object v0, Lorg/apache/lucene/search/TopFieldCollector;->EMPTY_SCOREDOCS:[Lorg/apache/lucene/search/ScoreDoc;

    return-void
.end method

.method private constructor <init>(Lorg/apache/lucene/util/PriorityQueue;IZ)V
    .locals 1
    .param p2, "numHits"    # I
    .param p3, "fillFields"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/PriorityQueue",
            "<",
            "Lorg/apache/lucene/search/FieldValueHitQueue$Entry;",
            ">;IZ)V"
        }
    .end annotation

    .prologue
    .line 1024
    .local p1, "pq":Lorg/apache/lucene/util/PriorityQueue;, "Lorg/apache/lucene/util/PriorityQueue<Lorg/apache/lucene/search/FieldValueHitQueue$Entry;>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/TopDocsCollector;-><init>(Lorg/apache/lucene/util/PriorityQueue;)V

    .line 1011
    const/high16 v0, 0x7fc00000    # NaNf

    iput v0, p0, Lorg/apache/lucene/search/TopFieldCollector;->maxScore:F

    .line 1014
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/search/TopFieldCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    .line 1025
    iput p2, p0, Lorg/apache/lucene/search/TopFieldCollector;->numHits:I

    .line 1026
    iput-boolean p3, p0, Lorg/apache/lucene/search/TopFieldCollector;->fillFields:Z

    .line 1027
    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/util/PriorityQueue;IZLorg/apache/lucene/search/TopFieldCollector;)V
    .locals 0

    .prologue
    .line 1023
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/search/TopFieldCollector;-><init>(Lorg/apache/lucene/util/PriorityQueue;IZ)V

    return-void
.end method

.method public static create(Lorg/apache/lucene/search/Sort;ILorg/apache/lucene/search/FieldDoc;ZZZZ)Lorg/apache/lucene/search/TopFieldCollector;
    .locals 7
    .param p0, "sort"    # Lorg/apache/lucene/search/Sort;
    .param p1, "numHits"    # I
    .param p2, "after"    # Lorg/apache/lucene/search/FieldDoc;
    .param p3, "fillFields"    # Z
    .param p4, "trackDocScores"    # Z
    .param p5, "trackMaxScore"    # Z
    .param p6, "docsScoredInOrder"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1115
    iget-object v0, p0, Lorg/apache/lucene/search/Sort;->fields:[Lorg/apache/lucene/search/SortField;

    array-length v0, v0

    if-nez v0, :cond_0

    .line 1116
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v2, "Sort must contain at least one field"

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1119
    :cond_0
    if-gtz p1, :cond_1

    .line 1120
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v2, "numHits must be > 0; please use TotalHitCountCollector if you just need the total hit count"

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1123
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/search/Sort;->fields:[Lorg/apache/lucene/search/SortField;

    invoke-static {v0, p1}, Lorg/apache/lucene/search/FieldValueHitQueue;->create([Lorg/apache/lucene/search/SortField;I)Lorg/apache/lucene/search/FieldValueHitQueue;

    move-result-object v1

    .line 1125
    .local v1, "queue":Lorg/apache/lucene/search/FieldValueHitQueue;, "Lorg/apache/lucene/search/FieldValueHitQueue<Lorg/apache/lucene/search/FieldValueHitQueue$Entry;>;"
    if-nez p2, :cond_d

    .line 1126
    invoke-virtual {v1}, Lorg/apache/lucene/search/FieldValueHitQueue;->getComparators()[Lorg/apache/lucene/search/FieldComparator;

    move-result-object v0

    array-length v0, v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_7

    .line 1127
    if-eqz p6, :cond_4

    .line 1128
    if-eqz p5, :cond_2

    .line 1129
    new-instance v0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringMaxScoreCollector;

    invoke-direct {v0, v1, p1, p3}, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringMaxScoreCollector;-><init>(Lorg/apache/lucene/search/FieldValueHitQueue;IZ)V

    .line 1173
    :goto_0
    return-object v0

    .line 1130
    :cond_2
    if-eqz p4, :cond_3

    .line 1131
    new-instance v0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringNoMaxScoreCollector;

    invoke-direct {v0, v1, p1, p3}, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringNoMaxScoreCollector;-><init>(Lorg/apache/lucene/search/FieldValueHitQueue;IZ)V

    goto :goto_0

    .line 1133
    :cond_3
    new-instance v0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorNonScoringCollector;

    invoke-direct {v0, v1, p1, p3}, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorNonScoringCollector;-><init>(Lorg/apache/lucene/search/FieldValueHitQueue;IZ)V

    goto :goto_0

    .line 1136
    :cond_4
    if-eqz p5, :cond_5

    .line 1137
    new-instance v0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderOneComparatorScoringMaxScoreCollector;

    invoke-direct {v0, v1, p1, p3}, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderOneComparatorScoringMaxScoreCollector;-><init>(Lorg/apache/lucene/search/FieldValueHitQueue;IZ)V

    goto :goto_0

    .line 1138
    :cond_5
    if-eqz p4, :cond_6

    .line 1139
    new-instance v0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderOneComparatorScoringNoMaxScoreCollector;

    invoke-direct {v0, v1, p1, p3}, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderOneComparatorScoringNoMaxScoreCollector;-><init>(Lorg/apache/lucene/search/FieldValueHitQueue;IZ)V

    goto :goto_0

    .line 1141
    :cond_6
    new-instance v0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderOneComparatorNonScoringCollector;

    invoke-direct {v0, v1, p1, p3}, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderOneComparatorNonScoringCollector;-><init>(Lorg/apache/lucene/search/FieldValueHitQueue;IZ)V

    goto :goto_0

    .line 1147
    :cond_7
    if-eqz p6, :cond_a

    .line 1148
    if-eqz p5, :cond_8

    .line 1149
    new-instance v0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringMaxScoreCollector;

    invoke-direct {v0, v1, p1, p3}, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringMaxScoreCollector;-><init>(Lorg/apache/lucene/search/FieldValueHitQueue;IZ)V

    goto :goto_0

    .line 1150
    :cond_8
    if-eqz p4, :cond_9

    .line 1151
    new-instance v0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringNoMaxScoreCollector;

    invoke-direct {v0, v1, p1, p3}, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringNoMaxScoreCollector;-><init>(Lorg/apache/lucene/search/FieldValueHitQueue;IZ)V

    goto :goto_0

    .line 1153
    :cond_9
    new-instance v0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorNonScoringCollector;

    invoke-direct {v0, v1, p1, p3}, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorNonScoringCollector;-><init>(Lorg/apache/lucene/search/FieldValueHitQueue;IZ)V

    goto :goto_0

    .line 1156
    :cond_a
    if-eqz p5, :cond_b

    .line 1157
    new-instance v0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringMaxScoreCollector;

    invoke-direct {v0, v1, p1, p3}, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringMaxScoreCollector;-><init>(Lorg/apache/lucene/search/FieldValueHitQueue;IZ)V

    goto :goto_0

    .line 1158
    :cond_b
    if-eqz p4, :cond_c

    .line 1159
    new-instance v0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringNoMaxScoreCollector;

    invoke-direct {v0, v1, p1, p3}, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringNoMaxScoreCollector;-><init>(Lorg/apache/lucene/search/FieldValueHitQueue;IZ)V

    goto :goto_0

    .line 1161
    :cond_c
    new-instance v0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorNonScoringCollector;

    invoke-direct {v0, v1, p1, p3}, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorNonScoringCollector;-><init>(Lorg/apache/lucene/search/FieldValueHitQueue;IZ)V

    goto :goto_0

    .line 1165
    :cond_d
    iget-object v0, p2, Lorg/apache/lucene/search/FieldDoc;->fields:[Ljava/lang/Object;

    if-nez v0, :cond_e

    .line 1166
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v2, "after.fields wasn\'t set; you must pass fillFields=true for the previous search"

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1169
    :cond_e
    iget-object v0, p2, Lorg/apache/lucene/search/FieldDoc;->fields:[Ljava/lang/Object;

    array-length v0, v0

    invoke-virtual {p0}, Lorg/apache/lucene/search/Sort;->getSort()[Lorg/apache/lucene/search/SortField;

    move-result-object v2

    array-length v2, v2

    if-eq v0, v2, :cond_f

    .line 1170
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "after.fields has "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p2, Lorg/apache/lucene/search/FieldDoc;->fields:[Ljava/lang/Object;

    array-length v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " values but sort has "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lorg/apache/lucene/search/Sort;->getSort()[Lorg/apache/lucene/search/SortField;

    move-result-object v3

    array-length v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1173
    :cond_f
    new-instance v0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;

    move-object v2, p2

    move v3, p1

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;-><init>(Lorg/apache/lucene/search/FieldValueHitQueue;Lorg/apache/lucene/search/FieldDoc;IZZZ)V

    goto/16 :goto_0
.end method

.method public static create(Lorg/apache/lucene/search/Sort;IZZZZ)Lorg/apache/lucene/search/TopFieldCollector;
    .locals 7
    .param p0, "sort"    # Lorg/apache/lucene/search/Sort;
    .param p1, "numHits"    # I
    .param p2, "fillFields"    # Z
    .param p3, "trackDocScores"    # Z
    .param p4, "trackMaxScore"    # Z
    .param p5, "docsScoredInOrder"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1069
    const/4 v2, 0x0

    move-object v0, p0

    move v1, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-static/range {v0 .. v6}, Lorg/apache/lucene/search/TopFieldCollector;->create(Lorg/apache/lucene/search/Sort;ILorg/apache/lucene/search/FieldDoc;ZZZZ)Lorg/apache/lucene/search/TopFieldCollector;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public acceptsDocsOutOfOrder()Z
    .locals 1

    .prologue
    .line 1217
    const/4 v0, 0x0

    return v0
.end method

.method final add(IIF)V
    .locals 3
    .param p1, "slot"    # I
    .param p2, "doc"    # I
    .param p3, "score"    # F

    .prologue
    .line 1178
    iget-object v0, p0, Lorg/apache/lucene/search/TopFieldCollector;->pq:Lorg/apache/lucene/util/PriorityQueue;

    new-instance v1, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iget v2, p0, Lorg/apache/lucene/search/TopFieldCollector;->docBase:I

    add-int/2addr v2, p2

    invoke-direct {v1, p1, v2, p3}, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;-><init>(IIF)V

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/PriorityQueue;->add(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iput-object v0, p0, Lorg/apache/lucene/search/TopFieldCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    .line 1179
    iget v0, p0, Lorg/apache/lucene/search/TopFieldCollector;->totalHits:I

    iget v1, p0, Lorg/apache/lucene/search/TopFieldCollector;->numHits:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lorg/apache/lucene/search/TopFieldCollector;->queueFull:Z

    .line 1180
    return-void

    .line 1179
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected newTopDocs([Lorg/apache/lucene/search/ScoreDoc;I)Lorg/apache/lucene/search/TopDocs;
    .locals 4
    .param p1, "results"    # [Lorg/apache/lucene/search/ScoreDoc;
    .param p2, "start"    # I

    .prologue
    .line 1205
    if-nez p1, :cond_0

    .line 1206
    sget-object p1, Lorg/apache/lucene/search/TopFieldCollector;->EMPTY_SCOREDOCS:[Lorg/apache/lucene/search/ScoreDoc;

    .line 1208
    const/high16 v0, 0x7fc00000    # NaNf

    iput v0, p0, Lorg/apache/lucene/search/TopFieldCollector;->maxScore:F

    .line 1212
    :cond_0
    new-instance v1, Lorg/apache/lucene/search/TopFieldDocs;

    iget v2, p0, Lorg/apache/lucene/search/TopFieldCollector;->totalHits:I

    iget-object v0, p0, Lorg/apache/lucene/search/TopFieldCollector;->pq:Lorg/apache/lucene/util/PriorityQueue;

    check-cast v0, Lorg/apache/lucene/search/FieldValueHitQueue;

    invoke-virtual {v0}, Lorg/apache/lucene/search/FieldValueHitQueue;->getFields()[Lorg/apache/lucene/search/SortField;

    move-result-object v0

    iget v3, p0, Lorg/apache/lucene/search/TopFieldCollector;->maxScore:F

    invoke-direct {v1, v2, p1, v0, v3}, Lorg/apache/lucene/search/TopFieldDocs;-><init>(I[Lorg/apache/lucene/search/ScoreDoc;[Lorg/apache/lucene/search/SortField;F)V

    return-object v1
.end method

.method protected populateResults([Lorg/apache/lucene/search/ScoreDoc;I)V
    .locals 6
    .param p1, "results"    # [Lorg/apache/lucene/search/ScoreDoc;
    .param p2, "howMany"    # I

    .prologue
    .line 1189
    iget-boolean v3, p0, Lorg/apache/lucene/search/TopFieldCollector;->fillFields:Z

    if-eqz v3, :cond_2

    .line 1191
    iget-object v2, p0, Lorg/apache/lucene/search/TopFieldCollector;->pq:Lorg/apache/lucene/util/PriorityQueue;

    check-cast v2, Lorg/apache/lucene/search/FieldValueHitQueue;

    .line 1192
    .local v2, "queue":Lorg/apache/lucene/search/FieldValueHitQueue;, "Lorg/apache/lucene/search/FieldValueHitQueue<Lorg/apache/lucene/search/FieldValueHitQueue$Entry;>;"
    add-int/lit8 v1, p2, -0x1

    .local v1, "i":I
    :goto_0
    if-gez v1, :cond_1

    .line 1201
    .end local v2    # "queue":Lorg/apache/lucene/search/FieldValueHitQueue;, "Lorg/apache/lucene/search/FieldValueHitQueue<Lorg/apache/lucene/search/FieldValueHitQueue$Entry;>;"
    :cond_0
    return-void

    .line 1193
    .restart local v2    # "queue":Lorg/apache/lucene/search/FieldValueHitQueue;, "Lorg/apache/lucene/search/FieldValueHitQueue<Lorg/apache/lucene/search/FieldValueHitQueue$Entry;>;"
    :cond_1
    invoke-virtual {v2}, Lorg/apache/lucene/search/FieldValueHitQueue;->pop()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/search/FieldValueHitQueue;->fillFields(Lorg/apache/lucene/search/FieldValueHitQueue$Entry;)Lorg/apache/lucene/search/FieldDoc;

    move-result-object v3

    aput-object v3, p1, v1

    .line 1192
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 1196
    .end local v1    # "i":I
    .end local v2    # "queue":Lorg/apache/lucene/search/FieldValueHitQueue;, "Lorg/apache/lucene/search/FieldValueHitQueue<Lorg/apache/lucene/search/FieldValueHitQueue$Entry;>;"
    :cond_2
    add-int/lit8 v1, p2, -0x1

    .restart local v1    # "i":I
    :goto_1
    if-ltz v1, :cond_0

    .line 1197
    iget-object v3, p0, Lorg/apache/lucene/search/TopFieldCollector;->pq:Lorg/apache/lucene/util/PriorityQueue;

    invoke-virtual {v3}, Lorg/apache/lucene/util/PriorityQueue;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    .line 1198
    .local v0, "entry":Lorg/apache/lucene/search/FieldValueHitQueue$Entry;, "Lorg/apache/lucene/search/FieldValueHitQueue$Entry;"
    new-instance v3, Lorg/apache/lucene/search/FieldDoc;

    iget v4, v0, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->doc:I

    iget v5, v0, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->score:F

    invoke-direct {v3, v4, v5}, Lorg/apache/lucene/search/FieldDoc;-><init>(IF)V

    aput-object v3, p1, v1

    .line 1196
    add-int/lit8 v1, v1, -0x1

    goto :goto_1
.end method
