.class Lorg/apache/lucene/search/FuzzyTermsEnum$AutomatonFuzzyTermsEnum;
.super Lorg/apache/lucene/index/FilteredTermsEnum;
.source "FuzzyTermsEnum.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FuzzyTermsEnum;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AutomatonFuzzyTermsEnum"
.end annotation


# instance fields
.field private final boostAtt:Lorg/apache/lucene/search/BoostAttribute;

.field private final matchers:[Lorg/apache/lucene/util/automaton/ByteRunAutomaton;

.field private final termRef:Lorg/apache/lucene/util/BytesRef;

.field final synthetic this$0:Lorg/apache/lucene/search/FuzzyTermsEnum;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/FuzzyTermsEnum;Lorg/apache/lucene/index/TermsEnum;[Lorg/apache/lucene/util/automaton/CompiledAutomaton;)V
    .locals 3
    .param p2, "tenum"    # Lorg/apache/lucene/index/TermsEnum;
    .param p3, "compiled"    # [Lorg/apache/lucene/util/automaton/CompiledAutomaton;

    .prologue
    .line 340
    iput-object p1, p0, Lorg/apache/lucene/search/FuzzyTermsEnum$AutomatonFuzzyTermsEnum;->this$0:Lorg/apache/lucene/search/FuzzyTermsEnum;

    .line 341
    const/4 v1, 0x0

    invoke-direct {p0, p2, v1}, Lorg/apache/lucene/index/FilteredTermsEnum;-><init>(Lorg/apache/lucene/index/TermsEnum;Z)V

    .line 338
    invoke-virtual {p0}, Lorg/apache/lucene/search/FuzzyTermsEnum$AutomatonFuzzyTermsEnum;->attributes()Lorg/apache/lucene/util/AttributeSource;

    move-result-object v1

    const-class v2, Lorg/apache/lucene/search/BoostAttribute;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/AttributeSource;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/search/BoostAttribute;

    iput-object v1, p0, Lorg/apache/lucene/search/FuzzyTermsEnum$AutomatonFuzzyTermsEnum;->boostAtt:Lorg/apache/lucene/search/BoostAttribute;

    .line 342
    array-length v1, p3

    new-array v1, v1, [Lorg/apache/lucene/util/automaton/ByteRunAutomaton;

    iput-object v1, p0, Lorg/apache/lucene/search/FuzzyTermsEnum$AutomatonFuzzyTermsEnum;->matchers:[Lorg/apache/lucene/util/automaton/ByteRunAutomaton;

    .line 343
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p3

    if-lt v0, v1, :cond_0

    .line 345
    new-instance v1, Lorg/apache/lucene/util/BytesRef;

    # getter for: Lorg/apache/lucene/search/FuzzyTermsEnum;->term:Lorg/apache/lucene/index/Term;
    invoke-static {p1}, Lorg/apache/lucene/search/FuzzyTermsEnum;->access$0(Lorg/apache/lucene/search/FuzzyTermsEnum;)Lorg/apache/lucene/index/Term;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    iput-object v1, p0, Lorg/apache/lucene/search/FuzzyTermsEnum$AutomatonFuzzyTermsEnum;->termRef:Lorg/apache/lucene/util/BytesRef;

    .line 346
    return-void

    .line 344
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/FuzzyTermsEnum$AutomatonFuzzyTermsEnum;->matchers:[Lorg/apache/lucene/util/automaton/ByteRunAutomaton;

    aget-object v2, p3, v0

    iget-object v2, v2, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->runAutomaton:Lorg/apache/lucene/util/automaton/ByteRunAutomaton;

    aput-object v2, v1, v0

    .line 343
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method protected accept(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;
    .locals 6
    .param p1, "term"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    .line 352
    iget-object v3, p0, Lorg/apache/lucene/search/FuzzyTermsEnum$AutomatonFuzzyTermsEnum;->matchers:[Lorg/apache/lucene/util/automaton/ByteRunAutomaton;

    array-length v3, v3

    add-int/lit8 v1, v3, -0x1

    .line 357
    .local v1, "ed":I
    :goto_0
    if-gtz v1, :cond_1

    .line 367
    :cond_0
    if-nez v1, :cond_2

    .line 368
    iget-object v3, p0, Lorg/apache/lucene/search/FuzzyTermsEnum$AutomatonFuzzyTermsEnum;->boostAtt:Lorg/apache/lucene/search/BoostAttribute;

    invoke-interface {v3, v5}, Lorg/apache/lucene/search/BoostAttribute;->setBoost(F)V

    .line 370
    sget-object v3, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;->YES:Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

    .line 380
    :goto_1
    return-object v3

    .line 358
    :cond_1
    add-int/lit8 v3, v1, -0x1

    invoke-virtual {p0, p1, v3}, Lorg/apache/lucene/search/FuzzyTermsEnum$AutomatonFuzzyTermsEnum;->matches(Lorg/apache/lucene/util/BytesRef;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 359
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 372
    :cond_2
    invoke-static {p1}, Lorg/apache/lucene/util/UnicodeUtil;->codePointCount(Lorg/apache/lucene/util/BytesRef;)I

    move-result v0

    .line 373
    .local v0, "codePointCount":I
    int-to-float v3, v1

    .line 374
    iget-object v4, p0, Lorg/apache/lucene/search/FuzzyTermsEnum$AutomatonFuzzyTermsEnum;->this$0:Lorg/apache/lucene/search/FuzzyTermsEnum;

    iget v4, v4, Lorg/apache/lucene/search/FuzzyTermsEnum;->termLength:I

    invoke-static {v0, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    .line 373
    sub-float v2, v5, v3

    .line 375
    .local v2, "similarity":F
    iget-object v3, p0, Lorg/apache/lucene/search/FuzzyTermsEnum$AutomatonFuzzyTermsEnum;->this$0:Lorg/apache/lucene/search/FuzzyTermsEnum;

    iget v3, v3, Lorg/apache/lucene/search/FuzzyTermsEnum;->minSimilarity:F

    cmpl-float v3, v2, v3

    if-lez v3, :cond_3

    .line 376
    iget-object v3, p0, Lorg/apache/lucene/search/FuzzyTermsEnum$AutomatonFuzzyTermsEnum;->boostAtt:Lorg/apache/lucene/search/BoostAttribute;

    iget-object v4, p0, Lorg/apache/lucene/search/FuzzyTermsEnum$AutomatonFuzzyTermsEnum;->this$0:Lorg/apache/lucene/search/FuzzyTermsEnum;

    iget v4, v4, Lorg/apache/lucene/search/FuzzyTermsEnum;->minSimilarity:F

    sub-float v4, v2, v4

    iget-object v5, p0, Lorg/apache/lucene/search/FuzzyTermsEnum$AutomatonFuzzyTermsEnum;->this$0:Lorg/apache/lucene/search/FuzzyTermsEnum;

    iget v5, v5, Lorg/apache/lucene/search/FuzzyTermsEnum;->scale_factor:F

    mul-float/2addr v4, v5

    invoke-interface {v3, v4}, Lorg/apache/lucene/search/BoostAttribute;->setBoost(F)V

    .line 378
    sget-object v3, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;->YES:Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

    goto :goto_1

    .line 380
    :cond_3
    sget-object v3, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;->NO:Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

    goto :goto_1
.end method

.method final matches(Lorg/apache/lucene/util/BytesRef;I)Z
    .locals 4
    .param p1, "term"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "k"    # I

    .prologue
    .line 387
    if-nez p2, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/search/FuzzyTermsEnum$AutomatonFuzzyTermsEnum;->termRef:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {p1, v0}, Lorg/apache/lucene/util/BytesRef;->equals(Ljava/lang/Object;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/FuzzyTermsEnum$AutomatonFuzzyTermsEnum;->matchers:[Lorg/apache/lucene/util/automaton/ByteRunAutomaton;

    aget-object v0, v0, p2

    iget-object v1, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v2, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget v3, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/lucene/util/automaton/ByteRunAutomaton;->run([BII)Z

    move-result v0

    goto :goto_0
.end method
