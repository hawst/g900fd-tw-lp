.class final Lorg/apache/lucene/search/PhrasePositions;
.super Ljava/lang/Object;
.source "PhrasePositions.java"


# instance fields
.field count:I

.field doc:I

.field next:Lorg/apache/lucene/search/PhrasePositions;

.field offset:I

.field final ord:I

.field position:I

.field final postings:Lorg/apache/lucene/index/DocsAndPositionsEnum;

.field rptGroup:I

.field rptInd:I

.field final terms:[Lorg/apache/lucene/index/Term;


# direct methods
.method constructor <init>(Lorg/apache/lucene/index/DocsAndPositionsEnum;II[Lorg/apache/lucene/index/Term;)V
    .locals 1
    .param p1, "postings"    # Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .param p2, "o"    # I
    .param p3, "ord"    # I
    .param p4, "terms"    # [Lorg/apache/lucene/index/Term;

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/search/PhrasePositions;->rptGroup:I

    .line 39
    iput-object p1, p0, Lorg/apache/lucene/search/PhrasePositions;->postings:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    .line 40
    iput p2, p0, Lorg/apache/lucene/search/PhrasePositions;->offset:I

    .line 41
    iput p3, p0, Lorg/apache/lucene/search/PhrasePositions;->ord:I

    .line 42
    iput-object p4, p0, Lorg/apache/lucene/search/PhrasePositions;->terms:[Lorg/apache/lucene/index/Term;

    .line 43
    return-void
.end method


# virtual methods
.method final firstPosition()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 62
    iget-object v0, p0, Lorg/apache/lucene/search/PhrasePositions;->postings:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->freq()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/PhrasePositions;->count:I

    .line 63
    invoke-virtual {p0}, Lorg/apache/lucene/search/PhrasePositions;->nextPosition()Z

    .line 64
    return-void
.end method

.method final next()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46
    iget-object v0, p0, Lorg/apache/lucene/search/PhrasePositions;->postings:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->nextDoc()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/PhrasePositions;->doc:I

    .line 47
    iget v0, p0, Lorg/apache/lucene/search/PhrasePositions;->doc:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_0

    .line 48
    const/4 v0, 0x0

    .line 50
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method final nextPosition()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 73
    iget v0, p0, Lorg/apache/lucene/search/PhrasePositions;->count:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lorg/apache/lucene/search/PhrasePositions;->count:I

    if-lez v0, :cond_0

    .line 74
    iget-object v0, p0, Lorg/apache/lucene/search/PhrasePositions;->postings:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->nextPosition()I

    move-result v0

    iget v1, p0, Lorg/apache/lucene/search/PhrasePositions;->offset:I

    sub-int/2addr v0, v1

    iput v0, p0, Lorg/apache/lucene/search/PhrasePositions;->position:I

    .line 75
    const/4 v0, 0x1

    .line 77
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final skipTo(I)Z
    .locals 2
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 54
    iget-object v0, p0, Lorg/apache/lucene/search/PhrasePositions;->postings:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->advance(I)I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/PhrasePositions;->doc:I

    .line 55
    iget v0, p0, Lorg/apache/lucene/search/PhrasePositions;->doc:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_0

    .line 56
    const/4 v0, 0x0

    .line 58
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 83
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "d:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/lucene/search/PhrasePositions;->doc:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " o:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/search/PhrasePositions;->offset:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " p:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/search/PhrasePositions;->position:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " c:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/search/PhrasePositions;->count:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 84
    .local v0, "s":Ljava/lang/String;
    iget v1, p0, Lorg/apache/lucene/search/PhrasePositions;->rptGroup:I

    if-ltz v1, :cond_0

    .line 85
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " rpt:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/search/PhrasePositions;->rptGroup:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",i"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/search/PhrasePositions;->rptInd:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 87
    :cond_0
    return-object v0
.end method
