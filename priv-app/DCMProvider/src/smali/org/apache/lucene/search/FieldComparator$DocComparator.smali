.class public final Lorg/apache/lucene/search/FieldComparator$DocComparator;
.super Lorg/apache/lucene/search/FieldComparator;
.source "FieldComparator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FieldComparator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DocComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/FieldComparator",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field private bottom:I

.field private docBase:I

.field private final docIDs:[I


# direct methods
.method constructor <init>(I)V
    .locals 1
    .param p1, "numHits"    # I

    .prologue
    .line 799
    invoke-direct {p0}, Lorg/apache/lucene/search/FieldComparator;-><init>()V

    .line 800
    new-array v0, p1, [I

    iput-object v0, p0, Lorg/apache/lucene/search/FieldComparator$DocComparator;->docIDs:[I

    .line 801
    return-void
.end method


# virtual methods
.method public compare(II)I
    .locals 2
    .param p1, "slot1"    # I
    .param p2, "slot2"    # I

    .prologue
    .line 806
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$DocComparator;->docIDs:[I

    aget v0, v0, p1

    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$DocComparator;->docIDs:[I

    aget v1, v1, p2

    sub-int/2addr v0, v1

    return v0
.end method

.method public compareBottom(I)I
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 812
    iget v0, p0, Lorg/apache/lucene/search/FieldComparator$DocComparator;->bottom:I

    iget v1, p0, Lorg/apache/lucene/search/FieldComparator$DocComparator;->docBase:I

    add-int/2addr v1, p1

    sub-int/2addr v0, v1

    return v0
.end method

.method public compareDocToValue(ILjava/lang/Integer;)I
    .locals 3
    .param p1, "doc"    # I
    .param p2, "valueObj"    # Ljava/lang/Integer;

    .prologue
    .line 841
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 842
    .local v1, "value":I
    iget v2, p0, Lorg/apache/lucene/search/FieldComparator$DocComparator;->docBase:I

    add-int v0, v2, p1

    .line 843
    .local v0, "docValue":I
    if-ge v0, v1, :cond_0

    .line 844
    const/4 v2, -0x1

    .line 848
    :goto_0
    return v2

    .line 845
    :cond_0
    if-le v0, v1, :cond_1

    .line 846
    const/4 v2, 0x1

    goto :goto_0

    .line 848
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public bridge synthetic compareDocToValue(ILjava/lang/Object;)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/search/FieldComparator$DocComparator;->compareDocToValue(ILjava/lang/Integer;)I

    move-result v0

    return v0
.end method

.method public copy(II)V
    .locals 2
    .param p1, "slot"    # I
    .param p2, "doc"    # I

    .prologue
    .line 817
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$DocComparator;->docIDs:[I

    iget v1, p0, Lorg/apache/lucene/search/FieldComparator$DocComparator;->docBase:I

    add-int/2addr v1, p2

    aput v1, v0, p1

    .line 818
    return-void
.end method

.method public setBottom(I)V
    .locals 1
    .param p1, "bottom"    # I

    .prologue
    .line 831
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$DocComparator;->docIDs:[I

    aget v0, v0, p1

    iput v0, p0, Lorg/apache/lucene/search/FieldComparator$DocComparator;->bottom:I

    .line 832
    return-void
.end method

.method public setNextReader(Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/search/FieldComparator;
    .locals 1
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/AtomicReaderContext;",
            ")",
            "Lorg/apache/lucene/search/FieldComparator",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 825
    iget v0, p1, Lorg/apache/lucene/index/AtomicReaderContext;->docBase:I

    iput v0, p0, Lorg/apache/lucene/search/FieldComparator$DocComparator;->docBase:I

    .line 826
    return-object p0
.end method

.method public value(I)Ljava/lang/Integer;
    .locals 1
    .param p1, "slot"    # I

    .prologue
    .line 836
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$DocComparator;->docIDs:[I

    aget v0, v0, p1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic value(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/FieldComparator$DocComparator;->value(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method
