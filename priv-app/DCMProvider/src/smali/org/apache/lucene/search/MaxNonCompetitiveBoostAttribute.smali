.class public interface abstract Lorg/apache/lucene/search/MaxNonCompetitiveBoostAttribute;
.super Ljava/lang/Object;
.source "MaxNonCompetitiveBoostAttribute.java"

# interfaces
.implements Lorg/apache/lucene/util/Attribute;


# virtual methods
.method public abstract getCompetitiveTerm()Lorg/apache/lucene/util/BytesRef;
.end method

.method public abstract getMaxNonCompetitiveBoost()F
.end method

.method public abstract setCompetitiveTerm(Lorg/apache/lucene/util/BytesRef;)V
.end method

.method public abstract setMaxNonCompetitiveBoost(F)V
.end method
