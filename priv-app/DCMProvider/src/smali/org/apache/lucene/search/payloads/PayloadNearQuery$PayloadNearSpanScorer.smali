.class public Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;
.super Lorg/apache/lucene/search/spans/SpanScorer;
.source "PayloadNearQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/payloads/PayloadNearQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "PayloadNearSpanScorer"
.end annotation


# instance fields
.field protected payloadScore:F

.field private payloadsSeen:I

.field scratch:Lorg/apache/lucene/util/BytesRef;

.field spans:Lorg/apache/lucene/search/spans/Spans;

.field final synthetic this$0:Lorg/apache/lucene/search/payloads/PayloadNearQuery;


# direct methods
.method protected constructor <init>(Lorg/apache/lucene/search/payloads/PayloadNearQuery;Lorg/apache/lucene/search/spans/Spans;Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/similarities/Similarity;Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;)V
    .locals 1
    .param p2, "spans"    # Lorg/apache/lucene/search/spans/Spans;
    .param p3, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p4, "similarity"    # Lorg/apache/lucene/search/similarities/Similarity;
    .param p5, "docScorer"    # Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 193
    iput-object p1, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->this$0:Lorg/apache/lucene/search/payloads/PayloadNearQuery;

    .line 194
    invoke-direct {p0, p2, p3, p5}, Lorg/apache/lucene/search/spans/SpanScorer;-><init>(Lorg/apache/lucene/search/spans/Spans;Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;)V

    .line 218
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->scratch:Lorg/apache/lucene/util/BytesRef;

    .line 195
    iput-object p2, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->spans:Lorg/apache/lucene/search/spans/Spans;

    .line 196
    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;)I
    .locals 1

    .prologue
    .line 190
    iget v0, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->payloadsSeen:I

    return v0
.end method


# virtual methods
.method public getPayloads([Lorg/apache/lucene/search/spans/Spans;)V
    .locals 4
    .param p1, "subSpans"    # [Lorg/apache/lucene/search/spans/Spans;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 200
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-lt v0, v1, :cond_0

    .line 215
    return-void

    .line 201
    :cond_0
    aget-object v1, p1, v0

    instance-of v1, v1, Lorg/apache/lucene/search/spans/NearSpansOrdered;

    if-eqz v1, :cond_3

    .line 202
    aget-object v1, p1, v0

    check-cast v1, Lorg/apache/lucene/search/spans/NearSpansOrdered;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/NearSpansOrdered;->isPayloadAvailable()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 203
    aget-object v1, p1, v0

    check-cast v1, Lorg/apache/lucene/search/spans/NearSpansOrdered;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/NearSpansOrdered;->getPayload()Ljava/util/Collection;

    move-result-object v1

    .line 204
    aget-object v2, p1, v0

    invoke-virtual {v2}, Lorg/apache/lucene/search/spans/Spans;->start()I

    move-result v2

    aget-object v3, p1, v0

    invoke-virtual {v3}, Lorg/apache/lucene/search/spans/Spans;->end()I

    move-result v3

    .line 203
    invoke-virtual {p0, v1, v2, v3}, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->processPayloads(Ljava/util/Collection;II)V

    .line 206
    :cond_1
    aget-object v1, p1, v0

    check-cast v1, Lorg/apache/lucene/search/spans/NearSpansOrdered;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/NearSpansOrdered;->getSubSpans()[Lorg/apache/lucene/search/spans/Spans;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->getPayloads([Lorg/apache/lucene/search/spans/Spans;)V

    .line 200
    :cond_2
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 207
    :cond_3
    aget-object v1, p1, v0

    instance-of v1, v1, Lorg/apache/lucene/search/spans/NearSpansUnordered;

    if-eqz v1, :cond_2

    .line 208
    aget-object v1, p1, v0

    check-cast v1, Lorg/apache/lucene/search/spans/NearSpansUnordered;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->isPayloadAvailable()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 209
    aget-object v1, p1, v0

    check-cast v1, Lorg/apache/lucene/search/spans/NearSpansUnordered;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->getPayload()Ljava/util/Collection;

    move-result-object v1

    .line 210
    aget-object v2, p1, v0

    invoke-virtual {v2}, Lorg/apache/lucene/search/spans/Spans;->start()I

    move-result v2

    aget-object v3, p1, v0

    invoke-virtual {v3}, Lorg/apache/lucene/search/spans/Spans;->end()I

    move-result v3

    .line 209
    invoke-virtual {p0, v1, v2, v3}, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->processPayloads(Ljava/util/Collection;II)V

    .line 212
    :cond_4
    aget-object v1, p1, v0

    check-cast v1, Lorg/apache/lucene/search/spans/NearSpansUnordered;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->getSubSpans()[Lorg/apache/lucene/search/spans/Spans;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->getPayloads([Lorg/apache/lucene/search/spans/Spans;)V

    goto :goto_1
.end method

.method protected processPayloads(Ljava/util/Collection;II)V
    .locals 12
    .param p2, "start"    # I
    .param p3, "end"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<[B>;II)V"
        }
    .end annotation

    .prologue
    .line 231
    .local p1, "payLoads":Ljava/util/Collection;, "Ljava/util/Collection<[B>;"
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 240
    return-void

    .line 231
    :cond_0
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [B

    .line 232
    .local v8, "thePayload":[B
    iget-object v0, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->scratch:Lorg/apache/lucene/util/BytesRef;

    iput-object v8, v0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 233
    iget-object v0, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->scratch:Lorg/apache/lucene/util/BytesRef;

    const/4 v1, 0x0

    iput v1, v0, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 234
    iget-object v0, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->scratch:Lorg/apache/lucene/util/BytesRef;

    array-length v1, v8

    iput v1, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 235
    iget-object v0, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->this$0:Lorg/apache/lucene/search/payloads/PayloadNearQuery;

    iget-object v0, v0, Lorg/apache/lucene/search/payloads/PayloadNearQuery;->function:Lorg/apache/lucene/search/payloads/PayloadFunction;

    iget v1, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->doc:I

    iget-object v2, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->this$0:Lorg/apache/lucene/search/payloads/PayloadNearQuery;

    iget-object v2, v2, Lorg/apache/lucene/search/payloads/PayloadNearQuery;->fieldName:Ljava/lang/String;

    .line 236
    iget v5, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->payloadsSeen:I

    iget v6, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->payloadScore:F

    iget-object v3, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->docScorer:Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;

    iget v4, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->doc:I

    .line 237
    iget-object v7, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v7}, Lorg/apache/lucene/search/spans/Spans;->start()I

    move-result v7

    iget-object v10, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v10}, Lorg/apache/lucene/search/spans/Spans;->end()I

    move-result v10

    iget-object v11, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->scratch:Lorg/apache/lucene/util/BytesRef;

    .line 236
    invoke-virtual {v3, v4, v7, v10, v11}, Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;->computePayloadFactor(IIILorg/apache/lucene/util/BytesRef;)F

    move-result v7

    move v3, p2

    move v4, p3

    .line 235
    invoke-virtual/range {v0 .. v7}, Lorg/apache/lucene/search/payloads/PayloadFunction;->currentScore(ILjava/lang/String;IIIFF)F

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->payloadScore:F

    .line 238
    iget v0, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->payloadsSeen:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->payloadsSeen:I

    goto :goto_0
.end method

.method public score()F
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 266
    invoke-super {p0}, Lorg/apache/lucene/search/spans/SpanScorer;->score()F

    move-result v0

    .line 267
    iget-object v1, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->this$0:Lorg/apache/lucene/search/payloads/PayloadNearQuery;

    iget-object v1, v1, Lorg/apache/lucene/search/payloads/PayloadNearQuery;->function:Lorg/apache/lucene/search/payloads/PayloadFunction;

    iget v2, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->doc:I

    iget-object v3, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->this$0:Lorg/apache/lucene/search/payloads/PayloadNearQuery;

    iget-object v3, v3, Lorg/apache/lucene/search/payloads/PayloadNearQuery;->fieldName:Ljava/lang/String;

    iget v4, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->payloadsSeen:I

    iget v5, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->payloadScore:F

    invoke-virtual {v1, v2, v3, v4, v5}, Lorg/apache/lucene/search/payloads/PayloadFunction;->docScore(ILjava/lang/String;IF)F

    move-result v1

    .line 266
    mul-float/2addr v0, v1

    return v0
.end method

.method protected setFreqCurrentDoc()Z
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 245
    iget-boolean v4, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->more:Z

    if-nez v4, :cond_0

    .line 260
    :goto_0
    return v2

    .line 248
    :cond_0
    iget-object v4, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v4}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v4

    iput v4, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->doc:I

    .line 249
    iput v5, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->freq:F

    .line 250
    iput v5, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->payloadScore:F

    .line 251
    iput v2, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->payloadsSeen:I

    .line 253
    :cond_1
    iget-object v4, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v4}, Lorg/apache/lucene/search/spans/Spans;->end()I

    move-result v4

    iget-object v5, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v5}, Lorg/apache/lucene/search/spans/Spans;->start()I

    move-result v5

    sub-int v0, v4, v5

    .line 254
    .local v0, "matchLength":I
    iget v4, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->freq:F

    iget-object v5, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->docScorer:Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;

    invoke-virtual {v5, v0}, Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;->computeSlopFactor(I)F

    move-result v5

    add-float/2addr v4, v5

    iput v4, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->freq:F

    .line 255
    new-array v1, v3, [Lorg/apache/lucene/search/spans/Spans;

    .line 256
    .local v1, "spansArr":[Lorg/apache/lucene/search/spans/Spans;
    iget-object v4, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->spans:Lorg/apache/lucene/search/spans/Spans;

    aput-object v4, v1, v2

    .line 257
    invoke-virtual {p0, v1}, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->getPayloads([Lorg/apache/lucene/search/spans/Spans;)V

    .line 258
    iget-object v4, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v4}, Lorg/apache/lucene/search/spans/Spans;->next()Z

    move-result v4

    iput-boolean v4, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->more:Z

    .line 259
    iget-boolean v4, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->more:Z

    if-eqz v4, :cond_2

    iget v4, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->doc:I

    iget-object v5, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v5}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v5

    if-eq v4, v5, :cond_1

    :cond_2
    move v2, v3

    .line 260
    goto :goto_0
.end method
