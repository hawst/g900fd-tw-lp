.class public Lorg/apache/lucene/search/similarities/LMSimilarity$LMStats;
.super Lorg/apache/lucene/search/similarities/BasicStats;
.source "LMSimilarity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/similarities/LMSimilarity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LMStats"
.end annotation


# instance fields
.field private collectionProbability:F


# direct methods
.method public constructor <init>(Ljava/lang/String;F)V
    .locals 0
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "queryBoost"    # F

    .prologue
    .line 111
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/search/similarities/BasicStats;-><init>(Ljava/lang/String;F)V

    .line 112
    return-void
.end method


# virtual methods
.method public final getCollectionProbability()F
    .locals 1

    .prologue
    .line 119
    iget v0, p0, Lorg/apache/lucene/search/similarities/LMSimilarity$LMStats;->collectionProbability:F

    return v0
.end method

.method public final setCollectionProbability(F)V
    .locals 0
    .param p1, "collectionProbability"    # F

    .prologue
    .line 127
    iput p1, p0, Lorg/apache/lucene/search/similarities/LMSimilarity$LMStats;->collectionProbability:F

    .line 128
    return-void
.end method
