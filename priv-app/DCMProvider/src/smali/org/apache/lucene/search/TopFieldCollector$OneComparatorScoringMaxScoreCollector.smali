.class Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringMaxScoreCollector;
.super Lorg/apache/lucene/search/TopFieldCollector$OneComparatorNonScoringCollector;
.source "TopFieldCollector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/TopFieldCollector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "OneComparatorScoringMaxScoreCollector"
.end annotation


# instance fields
.field scorer:Lorg/apache/lucene/search/Scorer;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/FieldValueHitQueue;IZ)V
    .locals 1
    .param p2, "numHits"    # I
    .param p3, "fillFields"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/search/FieldValueHitQueue",
            "<",
            "Lorg/apache/lucene/search/FieldValueHitQueue$Entry;",
            ">;IZ)V"
        }
    .end annotation

    .prologue
    .line 278
    .local p1, "queue":Lorg/apache/lucene/search/FieldValueHitQueue;, "Lorg/apache/lucene/search/FieldValueHitQueue<Lorg/apache/lucene/search/FieldValueHitQueue$Entry;>;"
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorNonScoringCollector;-><init>(Lorg/apache/lucene/search/FieldValueHitQueue;IZ)V

    .line 280
    const/high16 v0, -0x800000    # Float.NEGATIVE_INFINITY

    iput v0, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringMaxScoreCollector;->maxScore:F

    .line 281
    return-void
.end method


# virtual methods
.method public collect(I)V
    .locals 4
    .param p1, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 291
    iget-object v2, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringMaxScoreCollector;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v2}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v0

    .line 292
    .local v0, "score":F
    iget v2, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringMaxScoreCollector;->maxScore:F

    cmpl-float v2, v0, v2

    if-lez v2, :cond_0

    .line 293
    iput v0, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringMaxScoreCollector;->maxScore:F

    .line 295
    :cond_0
    iget v2, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringMaxScoreCollector;->totalHits:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringMaxScoreCollector;->totalHits:I

    .line 296
    iget-boolean v2, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringMaxScoreCollector;->queueFull:Z

    if-eqz v2, :cond_3

    .line 297
    iget v2, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringMaxScoreCollector;->reverseMul:I

    iget-object v3, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringMaxScoreCollector;->comparator:Lorg/apache/lucene/search/FieldComparator;

    invoke-virtual {v3, p1}, Lorg/apache/lucene/search/FieldComparator;->compareBottom(I)I

    move-result v3

    mul-int/2addr v2, v3

    if-gtz v2, :cond_2

    .line 319
    :cond_1
    :goto_0
    return-void

    .line 305
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringMaxScoreCollector;->comparator:Lorg/apache/lucene/search/FieldComparator;

    iget-object v3, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringMaxScoreCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iget v3, v3, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->slot:I

    invoke-virtual {v2, v3, p1}, Lorg/apache/lucene/search/FieldComparator;->copy(II)V

    .line 306
    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringMaxScoreCollector;->updateBottom(IF)V

    .line 307
    iget-object v2, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringMaxScoreCollector;->comparator:Lorg/apache/lucene/search/FieldComparator;

    iget-object v3, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringMaxScoreCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iget v3, v3, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->slot:I

    invoke-virtual {v2, v3}, Lorg/apache/lucene/search/FieldComparator;->setBottom(I)V

    goto :goto_0

    .line 310
    :cond_3
    iget v2, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringMaxScoreCollector;->totalHits:I

    add-int/lit8 v1, v2, -0x1

    .line 312
    .local v1, "slot":I
    iget-object v2, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringMaxScoreCollector;->comparator:Lorg/apache/lucene/search/FieldComparator;

    invoke-virtual {v2, v1, p1}, Lorg/apache/lucene/search/FieldComparator;->copy(II)V

    .line 313
    invoke-virtual {p0, v1, p1, v0}, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringMaxScoreCollector;->add(IIF)V

    .line 314
    iget-boolean v2, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringMaxScoreCollector;->queueFull:Z

    if-eqz v2, :cond_1

    .line 315
    iget-object v2, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringMaxScoreCollector;->comparator:Lorg/apache/lucene/search/FieldComparator;

    iget-object v3, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringMaxScoreCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iget v3, v3, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->slot:I

    invoke-virtual {v2, v3}, Lorg/apache/lucene/search/FieldComparator;->setBottom(I)V

    goto :goto_0
.end method

.method public setScorer(Lorg/apache/lucene/search/Scorer;)V
    .locals 0
    .param p1, "scorer"    # Lorg/apache/lucene/search/Scorer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 323
    iput-object p1, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringMaxScoreCollector;->scorer:Lorg/apache/lucene/search/Scorer;

    .line 324
    invoke-super {p0, p1}, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorNonScoringCollector;->setScorer(Lorg/apache/lucene/search/Scorer;)V

    .line 325
    return-void
.end method

.method final updateBottom(IF)V
    .locals 2
    .param p1, "doc"    # I
    .param p2, "score"    # F

    .prologue
    .line 284
    iget-object v0, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringMaxScoreCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iget v1, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringMaxScoreCollector;->docBase:I

    add-int/2addr v1, p1

    iput v1, v0, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->doc:I

    .line 285
    iget-object v0, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringMaxScoreCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iput p2, v0, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->score:F

    .line 286
    iget-object v0, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringMaxScoreCollector;->pq:Lorg/apache/lucene/util/PriorityQueue;

    invoke-virtual {v0}, Lorg/apache/lucene/util/PriorityQueue;->updateTop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iput-object v0, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringMaxScoreCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    .line 287
    return-void
.end method
