.class public Lorg/apache/lucene/search/MultiTermQueryWrapperFilter;
.super Lorg/apache/lucene/search/Filter;
.source "MultiTermQueryWrapperFilter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Q:",
        "Lorg/apache/lucene/search/MultiTermQuery;",
        ">",
        "Lorg/apache/lucene/search/Filter;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field protected final query:Lorg/apache/lucene/search/MultiTermQuery;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TQ;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const-class v0, Lorg/apache/lucene/search/MultiTermQueryWrapperFilter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/MultiTermQueryWrapperFilter;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected constructor <init>(Lorg/apache/lucene/search/MultiTermQuery;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TQ;)V"
        }
    .end annotation

    .prologue
    .line 52
    .local p0, "this":Lorg/apache/lucene/search/MultiTermQueryWrapperFilter;, "Lorg/apache/lucene/search/MultiTermQueryWrapperFilter<TQ;>;"
    .local p1, "query":Lorg/apache/lucene/search/MultiTermQuery;, "TQ;"
    invoke-direct {p0}, Lorg/apache/lucene/search/Filter;-><init>()V

    .line 53
    iput-object p1, p0, Lorg/apache/lucene/search/MultiTermQueryWrapperFilter;->query:Lorg/apache/lucene/search/MultiTermQuery;

    .line 54
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .local p0, "this":Lorg/apache/lucene/search/MultiTermQueryWrapperFilter;, "Lorg/apache/lucene/search/MultiTermQueryWrapperFilter<TQ;>;"
    const/4 v0, 0x0

    .line 65
    if-ne p1, p0, :cond_1

    const/4 v0, 0x1

    .line 70
    .end local p1    # "o":Ljava/lang/Object;
    :cond_0
    :goto_0
    return v0

    .line 66
    .restart local p1    # "o":Ljava/lang/Object;
    :cond_1
    if-eqz p1, :cond_0

    .line 67
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 68
    iget-object v0, p0, Lorg/apache/lucene/search/MultiTermQueryWrapperFilter;->query:Lorg/apache/lucene/search/MultiTermQuery;

    check-cast p1, Lorg/apache/lucene/search/MultiTermQueryWrapperFilter;

    .end local p1    # "o":Ljava/lang/Object;
    iget-object v1, p1, Lorg/apache/lucene/search/MultiTermQueryWrapperFilter;->query:Lorg/apache/lucene/search/MultiTermQuery;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/MultiTermQuery;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public getDocIdSet(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/DocIdSet;
    .locals 8
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "acceptDocs"    # Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 87
    .local p0, "this":Lorg/apache/lucene/search/MultiTermQueryWrapperFilter;, "Lorg/apache/lucene/search/MultiTermQueryWrapperFilter<TQ;>;"
    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v4

    .line 88
    .local v4, "reader":Lorg/apache/lucene/index/AtomicReader;
    invoke-virtual {v4}, Lorg/apache/lucene/index/AtomicReader;->fields()Lorg/apache/lucene/index/Fields;

    move-result-object v3

    .line 89
    .local v3, "fields":Lorg/apache/lucene/index/Fields;
    if-nez v3, :cond_0

    .line 91
    sget-object v0, Lorg/apache/lucene/search/DocIdSet;->EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;

    .line 119
    :goto_0
    return-object v0

    .line 94
    :cond_0
    iget-object v7, p0, Lorg/apache/lucene/search/MultiTermQueryWrapperFilter;->query:Lorg/apache/lucene/search/MultiTermQuery;

    iget-object v7, v7, Lorg/apache/lucene/search/MultiTermQuery;->field:Ljava/lang/String;

    invoke-virtual {v3, v7}, Lorg/apache/lucene/index/Fields;->terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v5

    .line 95
    .local v5, "terms":Lorg/apache/lucene/index/Terms;
    if-nez v5, :cond_1

    .line 97
    sget-object v0, Lorg/apache/lucene/search/DocIdSet;->EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;

    goto :goto_0

    .line 100
    :cond_1
    iget-object v7, p0, Lorg/apache/lucene/search/MultiTermQueryWrapperFilter;->query:Lorg/apache/lucene/search/MultiTermQuery;

    invoke-virtual {v7, v5}, Lorg/apache/lucene/search/MultiTermQuery;->getTermsEnum(Lorg/apache/lucene/index/Terms;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v6

    .line 101
    .local v6, "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    sget-boolean v7, Lorg/apache/lucene/search/MultiTermQueryWrapperFilter;->$assertionsDisabled:Z

    if-nez v7, :cond_2

    if-nez v6, :cond_2

    new-instance v7, Ljava/lang/AssertionError;

    invoke-direct {v7}, Ljava/lang/AssertionError;-><init>()V

    throw v7

    .line 102
    :cond_2
    invoke-virtual {v6}, Lorg/apache/lucene/index/TermsEnum;->next()Lorg/apache/lucene/util/BytesRef;

    move-result-object v7

    if-eqz v7, :cond_5

    .line 104
    new-instance v0, Lorg/apache/lucene/util/FixedBitSet;

    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v7

    invoke-direct {v0, v7}, Lorg/apache/lucene/util/FixedBitSet;-><init>(I)V

    .line 105
    .local v0, "bitSet":Lorg/apache/lucene/util/FixedBitSet;
    const/4 v2, 0x0

    .line 109
    .local v2, "docsEnum":Lorg/apache/lucene/index/DocsEnum;
    :cond_3
    const/4 v7, 0x0

    invoke-virtual {v6, p2, v2, v7}, Lorg/apache/lucene/index/TermsEnum;->docs(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;I)Lorg/apache/lucene/index/DocsEnum;

    move-result-object v2

    .line 111
    :goto_1
    invoke-virtual {v2}, Lorg/apache/lucene/index/DocsEnum;->nextDoc()I

    move-result v1

    .local v1, "docid":I
    const v7, 0x7fffffff

    if-ne v1, v7, :cond_4

    .line 114
    invoke-virtual {v6}, Lorg/apache/lucene/index/TermsEnum;->next()Lorg/apache/lucene/util/BytesRef;

    move-result-object v7

    if-nez v7, :cond_3

    goto :goto_0

    .line 112
    :cond_4
    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/FixedBitSet;->set(I)V

    goto :goto_1

    .line 119
    .end local v0    # "bitSet":Lorg/apache/lucene/util/FixedBitSet;
    .end local v1    # "docid":I
    .end local v2    # "docsEnum":Lorg/apache/lucene/index/DocsEnum;
    :cond_5
    sget-object v0, Lorg/apache/lucene/search/DocIdSet;->EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;

    goto :goto_0
.end method

.method public final getField()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    .local p0, "this":Lorg/apache/lucene/search/MultiTermQueryWrapperFilter;, "Lorg/apache/lucene/search/MultiTermQueryWrapperFilter<TQ;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/MultiTermQueryWrapperFilter;->query:Lorg/apache/lucene/search/MultiTermQuery;

    invoke-virtual {v0}, Lorg/apache/lucene/search/MultiTermQuery;->getField()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 75
    .local p0, "this":Lorg/apache/lucene/search/MultiTermQueryWrapperFilter;, "Lorg/apache/lucene/search/MultiTermQueryWrapperFilter<TQ;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/MultiTermQueryWrapperFilter;->query:Lorg/apache/lucene/search/MultiTermQuery;

    invoke-virtual {v0}, Lorg/apache/lucene/search/MultiTermQuery;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    .local p0, "this":Lorg/apache/lucene/search/MultiTermQueryWrapperFilter;, "Lorg/apache/lucene/search/MultiTermQueryWrapperFilter<TQ;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/MultiTermQueryWrapperFilter;->query:Lorg/apache/lucene/search/MultiTermQuery;

    invoke-virtual {v0}, Lorg/apache/lucene/search/MultiTermQuery;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
