.class public Lorg/apache/lucene/search/similarities/DistributionLL;
.super Lorg/apache/lucene/search/similarities/Distribution;
.source "DistributionLL.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lorg/apache/lucene/search/similarities/Distribution;-><init>()V

    return-void
.end method


# virtual methods
.method public final score(Lorg/apache/lucene/search/similarities/BasicStats;FF)F
    .locals 2
    .param p1, "stats"    # Lorg/apache/lucene/search/similarities/BasicStats;
    .param p2, "tfn"    # F
    .param p3, "lambda"    # F

    .prologue
    .line 34
    add-float v0, p2, p3

    div-float v0, p3, v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    neg-double v0, v0

    double-to-float v0, v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    const-string v0, "LL"

    return-object v0
.end method
