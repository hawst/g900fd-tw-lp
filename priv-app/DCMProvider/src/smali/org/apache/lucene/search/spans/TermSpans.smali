.class public Lorg/apache/lucene/search/spans/TermSpans;
.super Lorg/apache/lucene/search/spans/Spans;
.source "TermSpans.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/spans/TermSpans$EmptyTermSpans;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final EMPTY_TERM_SPANS:Lorg/apache/lucene/search/spans/TermSpans;


# instance fields
.field protected count:I

.field protected doc:I

.field protected freq:I

.field protected position:I

.field protected final postings:Lorg/apache/lucene/index/DocsAndPositionsEnum;

.field protected readPayload:Z

.field protected final term:Lorg/apache/lucene/index/Term;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 32
    const-class v0, Lorg/apache/lucene/search/spans/TermSpans;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/spans/TermSpans;->$assertionsDisabled:Z

    .line 182
    new-instance v0, Lorg/apache/lucene/search/spans/TermSpans$EmptyTermSpans;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lorg/apache/lucene/search/spans/TermSpans$EmptyTermSpans;-><init>(Lorg/apache/lucene/search/spans/TermSpans$EmptyTermSpans;)V

    sput-object v0, Lorg/apache/lucene/search/spans/TermSpans;->EMPTY_TERM_SPANS:Lorg/apache/lucene/search/spans/TermSpans;

    return-void

    .line 32
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 48
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/Spans;-><init>()V

    .line 49
    iput-object v0, p0, Lorg/apache/lucene/search/spans/TermSpans;->term:Lorg/apache/lucene/index/Term;

    .line 50
    iput-object v0, p0, Lorg/apache/lucene/search/spans/TermSpans;->postings:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    .line 51
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/index/DocsAndPositionsEnum;Lorg/apache/lucene/index/Term;)V
    .locals 1
    .param p1, "postings"    # Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .param p2, "term"    # Lorg/apache/lucene/index/Term;

    .prologue
    .line 41
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/Spans;-><init>()V

    .line 42
    iput-object p1, p0, Lorg/apache/lucene/search/spans/TermSpans;->postings:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    .line 43
    iput-object p2, p0, Lorg/apache/lucene/search/spans/TermSpans;->term:Lorg/apache/lucene/index/Term;

    .line 44
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/search/spans/TermSpans;->doc:I

    .line 45
    return-void
.end method


# virtual methods
.method public cost()J
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lorg/apache/lucene/search/spans/TermSpans;->postings:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->cost()J

    move-result-wide v0

    return-wide v0
.end method

.method public doc()I
    .locals 1

    .prologue
    .line 90
    iget v0, p0, Lorg/apache/lucene/search/spans/TermSpans;->doc:I

    return v0
.end method

.method public end()I
    .locals 1

    .prologue
    .line 100
    iget v0, p0, Lorg/apache/lucene/search/spans/TermSpans;->position:I

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public getPayload()Ljava/util/Collection;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<[B>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 111
    iget-object v2, p0, Lorg/apache/lucene/search/spans/TermSpans;->postings:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v2}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->getPayload()Lorg/apache/lucene/util/BytesRef;

    move-result-object v1

    .line 112
    .local v1, "payload":Lorg/apache/lucene/util/BytesRef;
    const/4 v2, 0x1

    iput-boolean v2, p0, Lorg/apache/lucene/search/spans/TermSpans;->readPayload:Z

    .line 114
    if-eqz v1, :cond_0

    .line 115
    iget v2, v1, Lorg/apache/lucene/util/BytesRef;->length:I

    new-array v0, v2, [B

    .line 116
    .local v0, "bytes":[B
    iget-object v2, v1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v3, v1, Lorg/apache/lucene/util/BytesRef;->offset:I

    const/4 v4, 0x0

    iget v5, v1, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-static {v2, v3, v0, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 120
    :goto_0
    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    return-object v2

    .line 118
    .end local v0    # "bytes":[B
    :cond_0
    const/4 v0, 0x0

    .restart local v0    # "bytes":[B
    goto :goto_0
.end method

.method public getPostings()Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lorg/apache/lucene/search/spans/TermSpans;->postings:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    return-object v0
.end method

.method public isPayloadAvailable()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 126
    iget-boolean v0, p0, Lorg/apache/lucene/search/spans/TermSpans;->readPayload:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/search/spans/TermSpans;->postings:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->getPayload()Lorg/apache/lucene/util/BytesRef;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 55
    iget v1, p0, Lorg/apache/lucene/search/spans/TermSpans;->count:I

    iget v2, p0, Lorg/apache/lucene/search/spans/TermSpans;->freq:I

    if-ne v1, v2, :cond_2

    .line 56
    iget-object v1, p0, Lorg/apache/lucene/search/spans/TermSpans;->postings:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    if-nez v1, :cond_1

    .line 69
    :cond_0
    :goto_0
    return v0

    .line 59
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/search/spans/TermSpans;->postings:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v1}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->nextDoc()I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/search/spans/TermSpans;->doc:I

    .line 60
    iget v1, p0, Lorg/apache/lucene/search/spans/TermSpans;->doc:I

    const v2, 0x7fffffff

    if-eq v1, v2, :cond_0

    .line 63
    iget-object v1, p0, Lorg/apache/lucene/search/spans/TermSpans;->postings:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v1}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->freq()I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/search/spans/TermSpans;->freq:I

    .line 64
    iput v0, p0, Lorg/apache/lucene/search/spans/TermSpans;->count:I

    .line 66
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/search/spans/TermSpans;->postings:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v1}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->nextPosition()I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/search/spans/TermSpans;->position:I

    .line 67
    iget v1, p0, Lorg/apache/lucene/search/spans/TermSpans;->count:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/search/spans/TermSpans;->count:I

    .line 68
    iput-boolean v0, p0, Lorg/apache/lucene/search/spans/TermSpans;->readPayload:Z

    .line 69
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public skipTo(I)Z
    .locals 3
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 74
    sget-boolean v1, Lorg/apache/lucene/search/spans/TermSpans;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget v1, p0, Lorg/apache/lucene/search/spans/TermSpans;->doc:I

    if-gt p1, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 75
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/spans/TermSpans;->postings:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->advance(I)I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/search/spans/TermSpans;->doc:I

    .line 76
    iget v1, p0, Lorg/apache/lucene/search/spans/TermSpans;->doc:I

    const v2, 0x7fffffff

    if-ne v1, v2, :cond_1

    .line 85
    :goto_0
    return v0

    .line 80
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/search/spans/TermSpans;->postings:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v1}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->freq()I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/search/spans/TermSpans;->freq:I

    .line 81
    iput v0, p0, Lorg/apache/lucene/search/spans/TermSpans;->count:I

    .line 82
    iget-object v1, p0, Lorg/apache/lucene/search/spans/TermSpans;->postings:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v1}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->nextPosition()I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/search/spans/TermSpans;->position:I

    .line 83
    iget v1, p0, Lorg/apache/lucene/search/spans/TermSpans;->count:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/search/spans/TermSpans;->count:I

    .line 84
    iput-boolean v0, p0, Lorg/apache/lucene/search/spans/TermSpans;->readPayload:Z

    .line 85
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public start()I
    .locals 1

    .prologue
    .line 95
    iget v0, p0, Lorg/apache/lucene/search/spans/TermSpans;->position:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 131
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "spans("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/search/spans/TermSpans;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v1}, Lorg/apache/lucene/index/Term;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 132
    iget v0, p0, Lorg/apache/lucene/search/spans/TermSpans;->doc:I

    const/4 v2, -0x1

    if-ne v0, v2, :cond_0

    const-string v0, "START"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 131
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 132
    :cond_0
    iget v0, p0, Lorg/apache/lucene/search/spans/TermSpans;->doc:I

    const v2, 0x7fffffff

    if-ne v0, v2, :cond_1

    const-string v0, "END"

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    iget v2, p0, Lorg/apache/lucene/search/spans/TermSpans;->doc:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "-"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lorg/apache/lucene/search/spans/TermSpans;->position:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
