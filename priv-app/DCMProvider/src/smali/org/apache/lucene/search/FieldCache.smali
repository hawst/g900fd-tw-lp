.class public interface abstract Lorg/apache/lucene/search/FieldCache;
.super Ljava/lang/Object;
.source "FieldCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/FieldCache$ByteParser;,
        Lorg/apache/lucene/search/FieldCache$Bytes;,
        Lorg/apache/lucene/search/FieldCache$CacheEntry;,
        Lorg/apache/lucene/search/FieldCache$CreationPlaceholder;,
        Lorg/apache/lucene/search/FieldCache$DoubleParser;,
        Lorg/apache/lucene/search/FieldCache$Doubles;,
        Lorg/apache/lucene/search/FieldCache$FloatParser;,
        Lorg/apache/lucene/search/FieldCache$Floats;,
        Lorg/apache/lucene/search/FieldCache$IntParser;,
        Lorg/apache/lucene/search/FieldCache$Ints;,
        Lorg/apache/lucene/search/FieldCache$LongParser;,
        Lorg/apache/lucene/search/FieldCache$Longs;,
        Lorg/apache/lucene/search/FieldCache$Parser;,
        Lorg/apache/lucene/search/FieldCache$ShortParser;,
        Lorg/apache/lucene/search/FieldCache$Shorts;
    }
.end annotation


# static fields
.field public static final DEFAULT:Lorg/apache/lucene/search/FieldCache;

.field public static final DEFAULT_BYTE_PARSER:Lorg/apache/lucene/search/FieldCache$ByteParser;

.field public static final DEFAULT_DOUBLE_PARSER:Lorg/apache/lucene/search/FieldCache$DoubleParser;

.field public static final DEFAULT_FLOAT_PARSER:Lorg/apache/lucene/search/FieldCache$FloatParser;

.field public static final DEFAULT_INT_PARSER:Lorg/apache/lucene/search/FieldCache$IntParser;

.field public static final DEFAULT_LONG_PARSER:Lorg/apache/lucene/search/FieldCache$LongParser;

.field public static final DEFAULT_SHORT_PARSER:Lorg/apache/lucene/search/FieldCache$ShortParser;

.field public static final EMPTY_TERMSINDEX:Lorg/apache/lucene/index/SortedDocValues;

.field public static final NUMERIC_UTILS_DOUBLE_PARSER:Lorg/apache/lucene/search/FieldCache$DoubleParser;

.field public static final NUMERIC_UTILS_FLOAT_PARSER:Lorg/apache/lucene/search/FieldCache$FloatParser;

.field public static final NUMERIC_UTILS_INT_PARSER:Lorg/apache/lucene/search/FieldCache$IntParser;

.field public static final NUMERIC_UTILS_LONG_PARSER:Lorg/apache/lucene/search/FieldCache$LongParser;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 137
    new-instance v0, Lorg/apache/lucene/search/FieldCache$1;

    invoke-direct {v0}, Lorg/apache/lucene/search/FieldCache$1;-><init>()V

    sput-object v0, Lorg/apache/lucene/search/FieldCache;->EMPTY_TERMSINDEX:Lorg/apache/lucene/index/SortedDocValues;

    .line 230
    new-instance v0, Lorg/apache/lucene/search/FieldCacheImpl;

    invoke-direct {v0}, Lorg/apache/lucene/search/FieldCacheImpl;-><init>()V

    sput-object v0, Lorg/apache/lucene/search/FieldCache;->DEFAULT:Lorg/apache/lucene/search/FieldCache;

    .line 233
    new-instance v0, Lorg/apache/lucene/search/FieldCache$2;

    invoke-direct {v0}, Lorg/apache/lucene/search/FieldCache$2;-><init>()V

    sput-object v0, Lorg/apache/lucene/search/FieldCache;->DEFAULT_BYTE_PARSER:Lorg/apache/lucene/search/FieldCache$ByteParser;

    .line 253
    new-instance v0, Lorg/apache/lucene/search/FieldCache$3;

    invoke-direct {v0}, Lorg/apache/lucene/search/FieldCache$3;-><init>()V

    sput-object v0, Lorg/apache/lucene/search/FieldCache;->DEFAULT_SHORT_PARSER:Lorg/apache/lucene/search/FieldCache$ShortParser;

    .line 274
    new-instance v0, Lorg/apache/lucene/search/FieldCache$4;

    invoke-direct {v0}, Lorg/apache/lucene/search/FieldCache$4;-><init>()V

    sput-object v0, Lorg/apache/lucene/search/FieldCache;->DEFAULT_INT_PARSER:Lorg/apache/lucene/search/FieldCache$IntParser;

    .line 296
    new-instance v0, Lorg/apache/lucene/search/FieldCache$5;

    invoke-direct {v0}, Lorg/apache/lucene/search/FieldCache$5;-><init>()V

    sput-object v0, Lorg/apache/lucene/search/FieldCache;->DEFAULT_FLOAT_PARSER:Lorg/apache/lucene/search/FieldCache$FloatParser;

    .line 318
    new-instance v0, Lorg/apache/lucene/search/FieldCache$6;

    invoke-direct {v0}, Lorg/apache/lucene/search/FieldCache$6;-><init>()V

    sput-object v0, Lorg/apache/lucene/search/FieldCache;->DEFAULT_LONG_PARSER:Lorg/apache/lucene/search/FieldCache$LongParser;

    .line 340
    new-instance v0, Lorg/apache/lucene/search/FieldCache$7;

    invoke-direct {v0}, Lorg/apache/lucene/search/FieldCache$7;-><init>()V

    sput-object v0, Lorg/apache/lucene/search/FieldCache;->DEFAULT_DOUBLE_PARSER:Lorg/apache/lucene/search/FieldCache$DoubleParser;

    .line 365
    new-instance v0, Lorg/apache/lucene/search/FieldCache$8;

    invoke-direct {v0}, Lorg/apache/lucene/search/FieldCache$8;-><init>()V

    sput-object v0, Lorg/apache/lucene/search/FieldCache;->NUMERIC_UTILS_INT_PARSER:Lorg/apache/lucene/search/FieldCache$IntParser;

    .line 386
    new-instance v0, Lorg/apache/lucene/search/FieldCache$9;

    invoke-direct {v0}, Lorg/apache/lucene/search/FieldCache$9;-><init>()V

    sput-object v0, Lorg/apache/lucene/search/FieldCache;->NUMERIC_UTILS_FLOAT_PARSER:Lorg/apache/lucene/search/FieldCache$FloatParser;

    .line 406
    new-instance v0, Lorg/apache/lucene/search/FieldCache$10;

    invoke-direct {v0}, Lorg/apache/lucene/search/FieldCache$10;-><init>()V

    sput-object v0, Lorg/apache/lucene/search/FieldCache;->NUMERIC_UTILS_LONG_PARSER:Lorg/apache/lucene/search/FieldCache$LongParser;

    .line 426
    new-instance v0, Lorg/apache/lucene/search/FieldCache$11;

    invoke-direct {v0}, Lorg/apache/lucene/search/FieldCache$11;-><init>()V

    sput-object v0, Lorg/apache/lucene/search/FieldCache;->NUMERIC_UTILS_DOUBLE_PARSER:Lorg/apache/lucene/search/FieldCache$DoubleParser;

    .line 440
    return-void
.end method


# virtual methods
.method public abstract getBytes(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$ByteParser;Z)Lorg/apache/lucene/search/FieldCache$Bytes;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getBytes(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;Z)Lorg/apache/lucene/search/FieldCache$Bytes;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getCacheEntries()[Lorg/apache/lucene/search/FieldCache$CacheEntry;
.end method

.method public abstract getDocTermOrds(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;)Lorg/apache/lucene/index/SortedSetDocValues;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getDocsWithField(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;)Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getDoubles(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$DoubleParser;Z)Lorg/apache/lucene/search/FieldCache$Doubles;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getDoubles(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;Z)Lorg/apache/lucene/search/FieldCache$Doubles;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getFloats(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$FloatParser;Z)Lorg/apache/lucene/search/FieldCache$Floats;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getFloats(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;Z)Lorg/apache/lucene/search/FieldCache$Floats;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getInfoStream()Ljava/io/PrintStream;
.end method

.method public abstract getInts(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$IntParser;Z)Lorg/apache/lucene/search/FieldCache$Ints;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getInts(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;Z)Lorg/apache/lucene/search/FieldCache$Ints;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getLongs(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$LongParser;Z)Lorg/apache/lucene/search/FieldCache$Longs;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getLongs(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;Z)Lorg/apache/lucene/search/FieldCache$Longs;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getShorts(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$ShortParser;Z)Lorg/apache/lucene/search/FieldCache$Shorts;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getShorts(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;Z)Lorg/apache/lucene/search/FieldCache$Shorts;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getTerms(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;)Lorg/apache/lucene/index/BinaryDocValues;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getTerms(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;F)Lorg/apache/lucene/index/BinaryDocValues;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getTermsIndex(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;)Lorg/apache/lucene/index/SortedDocValues;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getTermsIndex(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;F)Lorg/apache/lucene/index/SortedDocValues;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract purge(Lorg/apache/lucene/index/AtomicReader;)V
.end method

.method public abstract purgeAllCaches()V
.end method

.method public abstract setInfoStream(Ljava/io/PrintStream;)V
.end method
