.class public final Lorg/apache/lucene/search/FieldComparator$ByteComparator;
.super Lorg/apache/lucene/search/FieldComparator$NumericComparator;
.source "FieldComparator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FieldComparator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ByteComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/FieldComparator$NumericComparator",
        "<",
        "Ljava/lang/Byte;",
        ">;"
    }
.end annotation


# instance fields
.field private bottom:B

.field private currentReaderValues:Lorg/apache/lucene/search/FieldCache$Bytes;

.field private final parser:Lorg/apache/lucene/search/FieldCache$ByteParser;

.field private final values:[B


# direct methods
.method constructor <init>(ILjava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Ljava/lang/Byte;)V
    .locals 1
    .param p1, "numHits"    # I
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "parser"    # Lorg/apache/lucene/search/FieldCache$Parser;
    .param p4, "missingValue"    # Ljava/lang/Byte;

    .prologue
    .line 230
    invoke-direct {p0, p2, p4}, Lorg/apache/lucene/search/FieldComparator$NumericComparator;-><init>(Ljava/lang/String;Ljava/lang/Number;)V

    .line 231
    new-array v0, p1, [B

    iput-object v0, p0, Lorg/apache/lucene/search/FieldComparator$ByteComparator;->values:[B

    .line 232
    check-cast p3, Lorg/apache/lucene/search/FieldCache$ByteParser;

    .end local p3    # "parser":Lorg/apache/lucene/search/FieldCache$Parser;
    iput-object p3, p0, Lorg/apache/lucene/search/FieldComparator$ByteComparator;->parser:Lorg/apache/lucene/search/FieldCache$ByteParser;

    .line 233
    return-void
.end method


# virtual methods
.method public compare(II)I
    .locals 2
    .param p1, "slot1"    # I
    .param p2, "slot2"    # I

    .prologue
    .line 237
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$ByteComparator;->values:[B

    aget-byte v0, v0, p1

    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$ByteComparator;->values:[B

    aget-byte v1, v1, p2

    sub-int/2addr v0, v1

    return v0
.end method

.method public compareBottom(I)I
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 242
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$ByteComparator;->currentReaderValues:Lorg/apache/lucene/search/FieldCache$Bytes;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/FieldCache$Bytes;->get(I)B

    move-result v0

    .line 245
    .local v0, "v2":B
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$ByteComparator;->docsWithField:Lorg/apache/lucene/util/Bits;

    if-eqz v1, :cond_0

    if-nez v0, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$ByteComparator;->docsWithField:Lorg/apache/lucene/util/Bits;

    invoke-interface {v1, p1}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 246
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$ByteComparator;->missingValue:Ljava/lang/Number;

    check-cast v1, Ljava/lang/Byte;

    invoke-virtual {v1}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    .line 249
    :cond_0
    iget-byte v1, p0, Lorg/apache/lucene/search/FieldComparator$ByteComparator;->bottom:B

    sub-int/2addr v1, v0

    return v1
.end method

.method public compareDocToValue(ILjava/lang/Byte;)I
    .locals 2
    .param p1, "doc"    # I
    .param p2, "value"    # Ljava/lang/Byte;

    .prologue
    .line 283
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$ByteComparator;->currentReaderValues:Lorg/apache/lucene/search/FieldCache$Bytes;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/FieldCache$Bytes;->get(I)B

    move-result v0

    .line 286
    .local v0, "docValue":B
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$ByteComparator;->docsWithField:Lorg/apache/lucene/util/Bits;

    if-eqz v1, :cond_0

    if-nez v0, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$ByteComparator;->docsWithField:Lorg/apache/lucene/util/Bits;

    invoke-interface {v1, p1}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 287
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$ByteComparator;->missingValue:Ljava/lang/Number;

    check-cast v1, Ljava/lang/Byte;

    invoke-virtual {v1}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    .line 289
    :cond_0
    invoke-virtual {p2}, Ljava/lang/Byte;->byteValue()B

    move-result v1

    sub-int v1, v0, v1

    return v1
.end method

.method public bridge synthetic compareDocToValue(ILjava/lang/Object;)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    check-cast p2, Ljava/lang/Byte;

    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/search/FieldComparator$ByteComparator;->compareDocToValue(ILjava/lang/Byte;)I

    move-result v0

    return v0
.end method

.method public copy(II)V
    .locals 2
    .param p1, "slot"    # I
    .param p2, "doc"    # I

    .prologue
    .line 254
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$ByteComparator;->currentReaderValues:Lorg/apache/lucene/search/FieldCache$Bytes;

    invoke-virtual {v1, p2}, Lorg/apache/lucene/search/FieldCache$Bytes;->get(I)B

    move-result v0

    .line 257
    .local v0, "v2":B
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$ByteComparator;->docsWithField:Lorg/apache/lucene/util/Bits;

    if-eqz v1, :cond_0

    if-nez v0, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$ByteComparator;->docsWithField:Lorg/apache/lucene/util/Bits;

    invoke-interface {v1, p2}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 258
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$ByteComparator;->missingValue:Ljava/lang/Number;

    check-cast v1, Ljava/lang/Byte;

    invoke-virtual {v1}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    .line 260
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$ByteComparator;->values:[B

    aput-byte v0, v1, p1

    .line 261
    return-void
.end method

.method public setBottom(I)V
    .locals 1
    .param p1, "bottom"    # I

    .prologue
    .line 273
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$ByteComparator;->values:[B

    aget-byte v0, v0, p1

    iput-byte v0, p0, Lorg/apache/lucene/search/FieldComparator$ByteComparator;->bottom:B

    .line 274
    return-void
.end method

.method public setNextReader(Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/search/FieldComparator;
    .locals 5
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/AtomicReaderContext;",
            ")",
            "Lorg/apache/lucene/search/FieldComparator",
            "<",
            "Ljava/lang/Byte;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 267
    sget-object v1, Lorg/apache/lucene/search/FieldCache;->DEFAULT:Lorg/apache/lucene/search/FieldCache;

    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/search/FieldComparator$ByteComparator;->field:Ljava/lang/String;

    iget-object v4, p0, Lorg/apache/lucene/search/FieldComparator$ByteComparator;->parser:Lorg/apache/lucene/search/FieldCache$ByteParser;

    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$ByteComparator;->missingValue:Ljava/lang/Number;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v2, v3, v4, v0}, Lorg/apache/lucene/search/FieldCache;->getBytes(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$ByteParser;Z)Lorg/apache/lucene/search/FieldCache$Bytes;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/FieldComparator$ByteComparator;->currentReaderValues:Lorg/apache/lucene/search/FieldCache$Bytes;

    .line 268
    invoke-super {p0, p1}, Lorg/apache/lucene/search/FieldComparator$NumericComparator;->setNextReader(Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/search/FieldComparator;

    move-result-object v0

    return-object v0

    .line 267
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public value(I)Ljava/lang/Byte;
    .locals 1
    .param p1, "slot"    # I

    .prologue
    .line 278
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$ByteComparator;->values:[B

    aget-byte v0, v0, p1

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic value(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/FieldComparator$ByteComparator;->value(I)Ljava/lang/Byte;

    move-result-object v0

    return-object v0
.end method
