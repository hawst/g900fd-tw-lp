.class Lorg/apache/lucene/search/ConjunctionScorer;
.super Lorg/apache/lucene/search/Scorer;
.source "ConjunctionScorer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;
    }
.end annotation


# instance fields
.field private final coord:F

.field protected final docsAndFreqs:[Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;

.field protected lastDoc:I

.field private final lead:Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/Weight;[Lorg/apache/lucene/search/Scorer;)V
    .locals 1
    .param p1, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p2, "scorers"    # [Lorg/apache/lucene/search/Scorer;

    .prologue
    .line 35
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/search/ConjunctionScorer;-><init>(Lorg/apache/lucene/search/Weight;[Lorg/apache/lucene/search/Scorer;F)V

    .line 36
    return-void
.end method

.method constructor <init>(Lorg/apache/lucene/search/Weight;[Lorg/apache/lucene/search/Scorer;F)V
    .locals 4
    .param p1, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p2, "scorers"    # [Lorg/apache/lucene/search/Scorer;
    .param p3, "coord"    # F

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/Scorer;-><init>(Lorg/apache/lucene/search/Weight;)V

    .line 29
    const/4 v1, -0x1

    iput v1, p0, Lorg/apache/lucene/search/ConjunctionScorer;->lastDoc:I

    .line 40
    iput p3, p0, Lorg/apache/lucene/search/ConjunctionScorer;->coord:F

    .line 41
    array-length v1, p2

    new-array v1, v1, [Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;

    iput-object v1, p0, Lorg/apache/lucene/search/ConjunctionScorer;->docsAndFreqs:[Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;

    .line 42
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p2

    if-lt v0, v1, :cond_0

    .line 47
    iget-object v1, p0, Lorg/apache/lucene/search/ConjunctionScorer;->docsAndFreqs:[Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;

    new-instance v2, Lorg/apache/lucene/search/ConjunctionScorer$1;

    invoke-direct {v2, p0}, Lorg/apache/lucene/search/ConjunctionScorer$1;-><init>(Lorg/apache/lucene/search/ConjunctionScorer;)V

    invoke-static {v1, v2}, Lorg/apache/lucene/util/ArrayUtil;->mergeSort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 54
    iget-object v1, p0, Lorg/apache/lucene/search/ConjunctionScorer;->docsAndFreqs:[Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    iput-object v1, p0, Lorg/apache/lucene/search/ConjunctionScorer;->lead:Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;

    .line 55
    return-void

    .line 43
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/ConjunctionScorer;->docsAndFreqs:[Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;

    new-instance v2, Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;

    aget-object v3, p2, v0

    invoke-direct {v2, v3}, Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;-><init>(Lorg/apache/lucene/search/Scorer;)V

    aput-object v2, v1, v0

    .line 42
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private doNext(I)I
    .locals 3
    .param p1, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 63
    :goto_0
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_1
    iget-object v1, p0, Lorg/apache/lucene/search/ConjunctionScorer;->docsAndFreqs:[Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 79
    return p1

    .line 68
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/ConjunctionScorer;->docsAndFreqs:[Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;

    aget-object v1, v1, v0

    iget v1, v1, Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;->doc:I

    if-ge v1, p1, :cond_1

    .line 69
    iget-object v1, p0, Lorg/apache/lucene/search/ConjunctionScorer;->docsAndFreqs:[Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;

    aget-object v1, v1, v0

    iget-object v2, p0, Lorg/apache/lucene/search/ConjunctionScorer;->docsAndFreqs:[Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;

    aget-object v2, v2, v0

    iget-object v2, v2, Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/search/Scorer;->advance(I)I

    move-result v2

    iput v2, v1, Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;->doc:I

    .line 71
    iget-object v1, p0, Lorg/apache/lucene/search/ConjunctionScorer;->docsAndFreqs:[Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;

    aget-object v1, v1, v0

    iget v1, v1, Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;->doc:I

    if-le v1, p1, :cond_1

    .line 73
    iget-object v1, p0, Lorg/apache/lucene/search/ConjunctionScorer;->docsAndFreqs:[Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;

    aget-object v1, v1, v0

    iget p1, v1, Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;->doc:I

    .line 82
    iget-object v1, p0, Lorg/apache/lucene/search/ConjunctionScorer;->lead:Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;

    iget-object v2, p0, Lorg/apache/lucene/search/ConjunctionScorer;->lead:Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;

    iget-object v2, v2, Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/search/Scorer;->advance(I)I

    move-result p1

    .end local p1    # "doc":I
    iput p1, v1, Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;->doc:I

    .line 58
    .restart local p1    # "doc":I
    goto :goto_0

    .line 63
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method


# virtual methods
.method public advance(I)I
    .locals 2
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 88
    iget-object v0, p0, Lorg/apache/lucene/search/ConjunctionScorer;->lead:Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;

    iget-object v1, p0, Lorg/apache/lucene/search/ConjunctionScorer;->lead:Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;

    iget-object v1, v1, Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/Scorer;->advance(I)I

    move-result v1

    iput v1, v0, Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;->doc:I

    .line 89
    iget-object v0, p0, Lorg/apache/lucene/search/ConjunctionScorer;->lead:Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;

    iget v0, v0, Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;->doc:I

    invoke-direct {p0, v0}, Lorg/apache/lucene/search/ConjunctionScorer;->doNext(I)I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/ConjunctionScorer;->lastDoc:I

    return v0
.end method

.method public cost()J
    .locals 2

    .prologue
    .line 120
    iget-object v0, p0, Lorg/apache/lucene/search/ConjunctionScorer;->lead:Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;

    iget-object v0, v0, Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->cost()J

    move-result-wide v0

    return-wide v0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lorg/apache/lucene/search/ConjunctionScorer;->lastDoc:I

    return v0
.end method

.method public freq()I
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lorg/apache/lucene/search/ConjunctionScorer;->docsAndFreqs:[Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;

    array-length v0, v0

    return v0
.end method

.method public getChildren()Ljava/util/Collection;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/search/Scorer$ChildScorer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 125
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lorg/apache/lucene/search/ConjunctionScorer;->docsAndFreqs:[Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;

    array-length v2, v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 126
    .local v0, "children":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/search/Scorer$ChildScorer;>;"
    iget-object v3, p0, Lorg/apache/lucene/search/ConjunctionScorer;->docsAndFreqs:[Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v4, :cond_0

    .line 129
    return-object v0

    .line 126
    :cond_0
    aget-object v1, v3, v2

    .line 127
    .local v1, "docs":Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;
    new-instance v5, Lorg/apache/lucene/search/Scorer$ChildScorer;

    iget-object v6, v1, Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;->scorer:Lorg/apache/lucene/search/Scorer;

    const-string v7, "MUST"

    invoke-direct {v5, v6, v7}, Lorg/apache/lucene/search/Scorer$ChildScorer;-><init>(Lorg/apache/lucene/search/Scorer;Ljava/lang/String;)V

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 126
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public nextDoc()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 99
    iget-object v0, p0, Lorg/apache/lucene/search/ConjunctionScorer;->lead:Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;

    iget-object v1, p0, Lorg/apache/lucene/search/ConjunctionScorer;->lead:Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;

    iget-object v1, v1, Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v1}, Lorg/apache/lucene/search/Scorer;->nextDoc()I

    move-result v1

    iput v1, v0, Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;->doc:I

    .line 100
    iget-object v0, p0, Lorg/apache/lucene/search/ConjunctionScorer;->lead:Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;

    iget v0, v0, Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;->doc:I

    invoke-direct {p0, v0}, Lorg/apache/lucene/search/ConjunctionScorer;->doNext(I)I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/ConjunctionScorer;->lastDoc:I

    return v0
.end method

.method public score()F
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 106
    const/4 v1, 0x0

    .line 107
    .local v1, "sum":F
    iget-object v3, p0, Lorg/apache/lucene/search/ConjunctionScorer;->docsAndFreqs:[Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v4, :cond_0

    .line 110
    iget v2, p0, Lorg/apache/lucene/search/ConjunctionScorer;->coord:F

    mul-float/2addr v2, v1

    return v2

    .line 107
    :cond_0
    aget-object v0, v3, v2

    .line 108
    .local v0, "docs":Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;
    iget-object v5, v0, Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v5}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v5

    add-float/2addr v1, v5

    .line 107
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method
