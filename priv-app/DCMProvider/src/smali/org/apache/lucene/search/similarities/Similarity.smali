.class public abstract Lorg/apache/lucene/search/similarities/Similarity;
.super Ljava/lang/Object;
.source "Similarity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;,
        Lorg/apache/lucene/search/similarities/Similarity$SimWeight;,
        Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract computeNorm(Lorg/apache/lucene/index/FieldInvertState;)J
.end method

.method public varargs abstract computeWeight(FLorg/apache/lucene/search/CollectionStatistics;[Lorg/apache/lucene/search/TermStatistics;)Lorg/apache/lucene/search/similarities/Similarity$SimWeight;
.end method

.method public coord(II)F
    .locals 1
    .param p1, "overlap"    # I
    .param p2, "maxOverlap"    # I

    .prologue
    .line 125
    const/high16 v0, 0x3f800000    # 1.0f

    return v0
.end method

.method public abstract exactSimScorer(Lorg/apache/lucene/search/similarities/Similarity$SimWeight;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public queryNorm(F)F
    .locals 1
    .param p1, "valueForNormalization"    # F

    .prologue
    .line 142
    const/high16 v0, 0x3f800000    # 1.0f

    return v0
.end method

.method public abstract sloppySimScorer(Lorg/apache/lucene/search/similarities/Similarity$SimWeight;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
