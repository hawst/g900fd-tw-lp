.class Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;
.super Ljava/lang/Object;
.source "SearcherLifetimeManager.java"

# interfaces
.implements Ljava/io/Closeable;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/SearcherLifetimeManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SearcherTracker"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;",
        ">;",
        "Ljava/io/Closeable;"
    }
.end annotation


# instance fields
.field public final recordTimeSec:D

.field public final searcher:Lorg/apache/lucene/search/IndexSearcher;

.field public final version:J


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/IndexSearcher;)V
    .locals 4
    .param p1, "searcher"    # Lorg/apache/lucene/search/IndexSearcher;

    .prologue
    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 111
    iput-object p1, p0, Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;->searcher:Lorg/apache/lucene/search/IndexSearcher;

    .line 112
    invoke-virtual {p1}, Lorg/apache/lucene/search/IndexSearcher;->getIndexReader()Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/DirectoryReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DirectoryReader;->getVersion()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;->version:J

    .line 113
    invoke-virtual {p1}, Lorg/apache/lucene/search/IndexSearcher;->getIndexReader()Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexReader;->incRef()V

    .line 116
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    long-to-double v0, v0

    const-wide v2, 0x41cdcd6500000000L    # 1.0E9

    div-double/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;->recordTimeSec:D

    .line 117
    return-void
.end method


# virtual methods
.method public declared-synchronized close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 127
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;->searcher:Lorg/apache/lucene/search/IndexSearcher;

    invoke-virtual {v0}, Lorg/apache/lucene/search/IndexSearcher;->getIndexReader()Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexReader;->decRef()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 128
    monitor-exit p0

    return-void

    .line 127
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;

    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;->compareTo(Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;)I
    .locals 4
    .param p1, "other"    # Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;

    .prologue
    .line 122
    iget-wide v0, p1, Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;->recordTimeSec:D

    iget-wide v2, p0, Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;->recordTimeSec:D

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Double;->compare(DD)I

    move-result v0

    return v0
.end method
