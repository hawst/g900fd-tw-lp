.class public Lorg/apache/lucene/search/FieldCacheImpl$SortedDocValuesImpl;
.super Lorg/apache/lucene/index/SortedDocValues;
.source "FieldCacheImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FieldCacheImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SortedDocValuesImpl"
.end annotation


# instance fields
.field private final bytes:Lorg/apache/lucene/util/PagedBytes$Reader;

.field private final docToTermOrd:Lorg/apache/lucene/util/packed/PackedInts$Reader;

.field private final numOrd:I

.field private final termOrdToBytesOffset:Lorg/apache/lucene/util/packed/PackedInts$Reader;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/util/PagedBytes$Reader;Lorg/apache/lucene/util/packed/PackedInts$Reader;Lorg/apache/lucene/util/packed/PackedInts$Reader;I)V
    .locals 0
    .param p1, "bytes"    # Lorg/apache/lucene/util/PagedBytes$Reader;
    .param p2, "termOrdToBytesOffset"    # Lorg/apache/lucene/util/packed/PackedInts$Reader;
    .param p3, "docToTermOrd"    # Lorg/apache/lucene/util/packed/PackedInts$Reader;
    .param p4, "numOrd"    # I

    .prologue
    .line 1076
    invoke-direct {p0}, Lorg/apache/lucene/index/SortedDocValues;-><init>()V

    .line 1077
    iput-object p1, p0, Lorg/apache/lucene/search/FieldCacheImpl$SortedDocValuesImpl;->bytes:Lorg/apache/lucene/util/PagedBytes$Reader;

    .line 1078
    iput-object p3, p0, Lorg/apache/lucene/search/FieldCacheImpl$SortedDocValuesImpl;->docToTermOrd:Lorg/apache/lucene/util/packed/PackedInts$Reader;

    .line 1079
    iput-object p2, p0, Lorg/apache/lucene/search/FieldCacheImpl$SortedDocValuesImpl;->termOrdToBytesOffset:Lorg/apache/lucene/util/packed/PackedInts$Reader;

    .line 1080
    iput p4, p0, Lorg/apache/lucene/search/FieldCacheImpl$SortedDocValuesImpl;->numOrd:I

    .line 1081
    return-void
.end method


# virtual methods
.method public getOrd(I)I
    .locals 2
    .param p1, "docID"    # I

    .prologue
    .line 1093
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl$SortedDocValuesImpl;->docToTermOrd:Lorg/apache/lucene/util/packed/PackedInts$Reader;

    invoke-interface {v0, p1}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I)J

    move-result-wide v0

    long-to-int v0, v0

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public getValueCount()I
    .locals 1

    .prologue
    .line 1085
    iget v0, p0, Lorg/apache/lucene/search/FieldCacheImpl$SortedDocValuesImpl;->numOrd:I

    return v0
.end method

.method public lookupOrd(ILorg/apache/lucene/util/BytesRef;)V
    .locals 4
    .param p1, "ord"    # I
    .param p2, "ret"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 1098
    if-gez p1, :cond_0

    .line 1099
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ord must be >=0 (got ord="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1101
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl$SortedDocValuesImpl;->bytes:Lorg/apache/lucene/util/PagedBytes$Reader;

    iget-object v1, p0, Lorg/apache/lucene/search/FieldCacheImpl$SortedDocValuesImpl;->termOrdToBytesOffset:Lorg/apache/lucene/util/packed/PackedInts$Reader;

    invoke-interface {v1, p1}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I)J

    move-result-wide v2

    invoke-virtual {v0, p2, v2, v3}, Lorg/apache/lucene/util/PagedBytes$Reader;->fill(Lorg/apache/lucene/util/BytesRef;J)V

    .line 1102
    return-void
.end method
