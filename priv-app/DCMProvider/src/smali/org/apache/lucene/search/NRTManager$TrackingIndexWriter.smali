.class public Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;
.super Ljava/lang/Object;
.source "NRTManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/NRTManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TrackingIndexWriter"
.end annotation


# instance fields
.field private final indexingGen:Ljava/util/concurrent/atomic/AtomicLong;

.field private final writer:Lorg/apache/lucene/index/IndexWriter;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/IndexWriter;)V
    .locals 4
    .param p1, "writer"    # Lorg/apache/lucene/index/IndexWriter;

    .prologue
    .line 152
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 150
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v2, 0x1

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->indexingGen:Ljava/util/concurrent/atomic/AtomicLong;

    .line 153
    iput-object p1, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->writer:Lorg/apache/lucene/index/IndexWriter;

    .line 154
    return-void
.end method


# virtual methods
.method public addDocument(Ljava/lang/Iterable;)J
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lorg/apache/lucene/index/IndexableField;",
            ">;)J"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 223
    .local p1, "d":Ljava/lang/Iterable;, "Ljava/lang/Iterable<+Lorg/apache/lucene/index/IndexableField;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/IndexWriter;->addDocument(Ljava/lang/Iterable;)V

    .line 225
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->indexingGen:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    return-wide v0
.end method

.method public addDocument(Ljava/lang/Iterable;Lorg/apache/lucene/analysis/Analyzer;)J
    .locals 2
    .param p2, "a"    # Lorg/apache/lucene/analysis/Analyzer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lorg/apache/lucene/index/IndexableField;",
            ">;",
            "Lorg/apache/lucene/analysis/Analyzer;",
            ")J"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 211
    .local p1, "d":Ljava/lang/Iterable;, "Ljava/lang/Iterable<+Lorg/apache/lucene/index/IndexableField;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/index/IndexWriter;->addDocument(Ljava/lang/Iterable;Lorg/apache/lucene/analysis/Analyzer;)V

    .line 213
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->indexingGen:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    return-wide v0
.end method

.method public addDocuments(Ljava/lang/Iterable;)J
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Ljava/lang/Iterable",
            "<+",
            "Lorg/apache/lucene/index/IndexableField;",
            ">;>;)J"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 229
    .local p1, "docs":Ljava/lang/Iterable;, "Ljava/lang/Iterable<+Ljava/lang/Iterable<+Lorg/apache/lucene/index/IndexableField;>;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/IndexWriter;->addDocuments(Ljava/lang/Iterable;)V

    .line 231
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->indexingGen:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    return-wide v0
.end method

.method public addDocuments(Ljava/lang/Iterable;Lorg/apache/lucene/analysis/Analyzer;)J
    .locals 2
    .param p2, "a"    # Lorg/apache/lucene/analysis/Analyzer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Ljava/lang/Iterable",
            "<+",
            "Lorg/apache/lucene/index/IndexableField;",
            ">;>;",
            "Lorg/apache/lucene/analysis/Analyzer;",
            ")J"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 217
    .local p1, "docs":Ljava/lang/Iterable;, "Ljava/lang/Iterable<+Ljava/lang/Iterable<+Lorg/apache/lucene/index/IndexableField;>;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/index/IndexWriter;->addDocuments(Ljava/lang/Iterable;Lorg/apache/lucene/analysis/Analyzer;)V

    .line 219
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->indexingGen:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    return-wide v0
.end method

.method public varargs addIndexes([Lorg/apache/lucene/index/IndexReader;)J
    .locals 2
    .param p1, "readers"    # [Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 241
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/IndexWriter;->addIndexes([Lorg/apache/lucene/index/IndexReader;)V

    .line 243
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->indexingGen:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    return-wide v0
.end method

.method public varargs addIndexes([Lorg/apache/lucene/store/Directory;)J
    .locals 2
    .param p1, "dirs"    # [Lorg/apache/lucene/store/Directory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 235
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/IndexWriter;->addIndexes([Lorg/apache/lucene/store/Directory;)V

    .line 237
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->indexingGen:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    return-wide v0
.end method

.method public deleteAll()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 205
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexWriter;->deleteAll()V

    .line 207
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->indexingGen:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    return-wide v0
.end method

.method public deleteDocuments(Lorg/apache/lucene/index/Term;)J
    .locals 2
    .param p1, "t"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 181
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/IndexWriter;->deleteDocuments(Lorg/apache/lucene/index/Term;)V

    .line 183
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->indexingGen:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    return-wide v0
.end method

.method public deleteDocuments(Lorg/apache/lucene/search/Query;)J
    .locals 2
    .param p1, "q"    # Lorg/apache/lucene/search/Query;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 193
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/IndexWriter;->deleteDocuments(Lorg/apache/lucene/search/Query;)V

    .line 195
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->indexingGen:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    return-wide v0
.end method

.method public varargs deleteDocuments([Lorg/apache/lucene/index/Term;)J
    .locals 2
    .param p1, "terms"    # [Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 187
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/IndexWriter;->deleteDocuments([Lorg/apache/lucene/index/Term;)V

    .line 189
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->indexingGen:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    return-wide v0
.end method

.method public varargs deleteDocuments([Lorg/apache/lucene/search/Query;)J
    .locals 2
    .param p1, "queries"    # [Lorg/apache/lucene/search/Query;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 199
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/IndexWriter;->deleteDocuments([Lorg/apache/lucene/search/Query;)V

    .line 201
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->indexingGen:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    return-wide v0
.end method

.method getAndIncrementGeneration()J
    .locals 2

    .prologue
    .line 255
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->indexingGen:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->getAndIncrement()J

    move-result-wide v0

    return-wide v0
.end method

.method public getGeneration()J
    .locals 2

    .prologue
    .line 247
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->indexingGen:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    return-wide v0
.end method

.method public getIndexWriter()Lorg/apache/lucene/index/IndexWriter;
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->writer:Lorg/apache/lucene/index/IndexWriter;

    return-object v0
.end method

.method public tryDeleteDocument(Lorg/apache/lucene/index/IndexReader;I)J
    .locals 2
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "docID"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 259
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/index/IndexWriter;->tryDeleteDocument(Lorg/apache/lucene/index/IndexReader;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 260
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->indexingGen:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    .line 262
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public updateDocument(Lorg/apache/lucene/index/Term;Ljava/lang/Iterable;)J
    .locals 2
    .param p1, "t"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/Term;",
            "Ljava/lang/Iterable",
            "<+",
            "Lorg/apache/lucene/index/IndexableField;",
            ">;)J"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 163
    .local p2, "d":Ljava/lang/Iterable;, "Ljava/lang/Iterable<+Lorg/apache/lucene/index/IndexableField;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/index/IndexWriter;->updateDocument(Lorg/apache/lucene/index/Term;Ljava/lang/Iterable;)V

    .line 165
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->indexingGen:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    return-wide v0
.end method

.method public updateDocument(Lorg/apache/lucene/index/Term;Ljava/lang/Iterable;Lorg/apache/lucene/analysis/Analyzer;)J
    .locals 2
    .param p1, "t"    # Lorg/apache/lucene/index/Term;
    .param p3, "a"    # Lorg/apache/lucene/analysis/Analyzer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/Term;",
            "Ljava/lang/Iterable",
            "<+",
            "Lorg/apache/lucene/index/IndexableField;",
            ">;",
            "Lorg/apache/lucene/analysis/Analyzer;",
            ")J"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 157
    .local p2, "d":Ljava/lang/Iterable;, "Ljava/lang/Iterable<+Lorg/apache/lucene/index/IndexableField;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/lucene/index/IndexWriter;->updateDocument(Lorg/apache/lucene/index/Term;Ljava/lang/Iterable;Lorg/apache/lucene/analysis/Analyzer;)V

    .line 159
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->indexingGen:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    return-wide v0
.end method

.method public updateDocuments(Lorg/apache/lucene/index/Term;Ljava/lang/Iterable;)J
    .locals 2
    .param p1, "t"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/Term;",
            "Ljava/lang/Iterable",
            "<+",
            "Ljava/lang/Iterable",
            "<+",
            "Lorg/apache/lucene/index/IndexableField;",
            ">;>;)J"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 175
    .local p2, "docs":Ljava/lang/Iterable;, "Ljava/lang/Iterable<+Ljava/lang/Iterable<+Lorg/apache/lucene/index/IndexableField;>;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/index/IndexWriter;->updateDocuments(Lorg/apache/lucene/index/Term;Ljava/lang/Iterable;)V

    .line 177
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->indexingGen:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    return-wide v0
.end method

.method public updateDocuments(Lorg/apache/lucene/index/Term;Ljava/lang/Iterable;Lorg/apache/lucene/analysis/Analyzer;)J
    .locals 2
    .param p1, "t"    # Lorg/apache/lucene/index/Term;
    .param p3, "a"    # Lorg/apache/lucene/analysis/Analyzer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/Term;",
            "Ljava/lang/Iterable",
            "<+",
            "Ljava/lang/Iterable",
            "<+",
            "Lorg/apache/lucene/index/IndexableField;",
            ">;>;",
            "Lorg/apache/lucene/analysis/Analyzer;",
            ")J"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 169
    .local p2, "docs":Ljava/lang/Iterable;, "Ljava/lang/Iterable<+Ljava/lang/Iterable<+Lorg/apache/lucene/index/IndexableField;>;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/lucene/index/IndexWriter;->updateDocuments(Lorg/apache/lucene/index/Term;Ljava/lang/Iterable;Lorg/apache/lucene/analysis/Analyzer;)V

    .line 171
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->indexingGen:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    return-wide v0
.end method
