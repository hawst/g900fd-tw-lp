.class final Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$IntQueue;
.super Ljava/lang/Object;
.source "MultiPhraseQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/UnionDocsAndPositionsEnum;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "IntQueue"
.end annotation


# instance fields
.field private _array:[I

.field private _arraySize:I

.field private _index:I

.field private _lastIndex:I


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 433
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 434
    const/16 v0, 0x10

    iput v0, p0, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$IntQueue;->_arraySize:I

    .line 435
    iput v1, p0, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$IntQueue;->_index:I

    .line 436
    iput v1, p0, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$IntQueue;->_lastIndex:I

    .line 437
    iget v0, p0, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$IntQueue;->_arraySize:I

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$IntQueue;->_array:[I

    .line 433
    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$IntQueue;)V
    .locals 0

    .prologue
    .line 433
    invoke-direct {p0}, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$IntQueue;-><init>()V

    return-void
.end method

.method private growArray()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 464
    iget v1, p0, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$IntQueue;->_arraySize:I

    mul-int/lit8 v1, v1, 0x2

    new-array v0, v1, [I

    .line 465
    .local v0, "newArray":[I
    iget-object v1, p0, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$IntQueue;->_array:[I

    iget v2, p0, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$IntQueue;->_arraySize:I

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 466
    iput-object v0, p0, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$IntQueue;->_array:[I

    .line 467
    iget v1, p0, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$IntQueue;->_arraySize:I

    mul-int/lit8 v1, v1, 0x2

    iput v1, p0, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$IntQueue;->_arraySize:I

    .line 468
    return-void
.end method


# virtual methods
.method final add(I)V
    .locals 3
    .param p1, "i"    # I

    .prologue
    .line 440
    iget v0, p0, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$IntQueue;->_lastIndex:I

    iget v1, p0, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$IntQueue;->_arraySize:I

    if-ne v0, v1, :cond_0

    .line 441
    invoke-direct {p0}, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$IntQueue;->growArray()V

    .line 443
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$IntQueue;->_array:[I

    iget v1, p0, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$IntQueue;->_lastIndex:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$IntQueue;->_lastIndex:I

    aput p1, v0, v1

    .line 444
    return-void
.end method

.method final clear()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 455
    iput v0, p0, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$IntQueue;->_index:I

    .line 456
    iput v0, p0, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$IntQueue;->_lastIndex:I

    .line 457
    return-void
.end method

.method final next()I
    .locals 3

    .prologue
    .line 447
    iget-object v0, p0, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$IntQueue;->_array:[I

    iget v1, p0, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$IntQueue;->_index:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$IntQueue;->_index:I

    aget v0, v0, v1

    return v0
.end method

.method final size()I
    .locals 2

    .prologue
    .line 460
    iget v0, p0, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$IntQueue;->_lastIndex:I

    iget v1, p0, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$IntQueue;->_index:I

    sub-int/2addr v0, v1

    return v0
.end method

.method final sort()V
    .locals 3

    .prologue
    .line 451
    iget-object v0, p0, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$IntQueue;->_array:[I

    iget v1, p0, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$IntQueue;->_index:I

    iget v2, p0, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$IntQueue;->_lastIndex:I

    invoke-static {v0, v1, v2}, Ljava/util/Arrays;->sort([III)V

    .line 452
    return-void
.end method
