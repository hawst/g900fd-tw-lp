.class Lorg/apache/lucene/search/FieldCacheImpl$IntsFromArray;
.super Lorg/apache/lucene/search/FieldCache$Ints;
.source "FieldCacheImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FieldCacheImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "IntsFromArray"
.end annotation


# instance fields
.field private final values:[I


# direct methods
.method public constructor <init>([I)V
    .locals 0
    .param p1, "values"    # [I

    .prologue
    .line 578
    invoke-direct {p0}, Lorg/apache/lucene/search/FieldCache$Ints;-><init>()V

    .line 579
    iput-object p1, p0, Lorg/apache/lucene/search/FieldCacheImpl$IntsFromArray;->values:[I

    .line 580
    return-void
.end method


# virtual methods
.method public get(I)I
    .locals 1
    .param p1, "docID"    # I

    .prologue
    .line 584
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl$IntsFromArray;->values:[I

    aget v0, v0, p1

    return v0
.end method
