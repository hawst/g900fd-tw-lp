.class public Lorg/apache/lucene/search/similarities/NormalizationZ;
.super Lorg/apache/lucene/search/similarities/Normalization;
.source "NormalizationZ.java"


# instance fields
.field final z:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    const v0, 0x3e99999a    # 0.3f

    invoke-direct {p0, v0}, Lorg/apache/lucene/search/similarities/NormalizationZ;-><init>(F)V

    .line 32
    return-void
.end method

.method public constructor <init>(F)V
    .locals 0
    .param p1, "z"    # F

    .prologue
    .line 39
    invoke-direct {p0}, Lorg/apache/lucene/search/similarities/Normalization;-><init>()V

    .line 40
    iput p1, p0, Lorg/apache/lucene/search/similarities/NormalizationZ;->z:F

    .line 41
    return-void
.end method


# virtual methods
.method public getZ()F
    .locals 1

    .prologue
    .line 58
    iget v0, p0, Lorg/apache/lucene/search/similarities/NormalizationZ;->z:F

    return v0
.end method

.method public tfn(Lorg/apache/lucene/search/similarities/BasicStats;FF)F
    .locals 6
    .param p1, "stats"    # Lorg/apache/lucene/search/similarities/BasicStats;
    .param p2, "tf"    # F
    .param p3, "len"    # F

    .prologue
    .line 45
    float-to-double v0, p2

    iget v2, p1, Lorg/apache/lucene/search/similarities/BasicStats;->avgFieldLength:F

    div-float/2addr v2, p3

    float-to-double v2, v2

    iget v4, p0, Lorg/apache/lucene/search/similarities/NormalizationZ;->z:F

    float-to-double v4, v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    mul-double/2addr v0, v2

    double-to-float v0, v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 50
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Z("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lorg/apache/lucene/search/similarities/NormalizationZ;->z:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
