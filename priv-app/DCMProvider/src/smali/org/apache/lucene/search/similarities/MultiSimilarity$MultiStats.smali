.class Lorg/apache/lucene/search/similarities/MultiSimilarity$MultiStats;
.super Lorg/apache/lucene/search/similarities/Similarity$SimWeight;
.source "MultiSimilarity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/similarities/MultiSimilarity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "MultiStats"
.end annotation


# instance fields
.field final subStats:[Lorg/apache/lucene/search/similarities/Similarity$SimWeight;


# direct methods
.method constructor <init>([Lorg/apache/lucene/search/similarities/Similarity$SimWeight;)V
    .locals 0
    .param p1, "subStats"    # [Lorg/apache/lucene/search/similarities/Similarity$SimWeight;

    .prologue
    .line 142
    invoke-direct {p0}, Lorg/apache/lucene/search/similarities/Similarity$SimWeight;-><init>()V

    .line 143
    iput-object p1, p0, Lorg/apache/lucene/search/similarities/MultiSimilarity$MultiStats;->subStats:[Lorg/apache/lucene/search/similarities/Similarity$SimWeight;

    .line 144
    return-void
.end method


# virtual methods
.method public getValueForNormalization()F
    .locals 6

    .prologue
    .line 148
    const/4 v1, 0x0

    .line 149
    .local v1, "sum":F
    iget-object v3, p0, Lorg/apache/lucene/search/similarities/MultiSimilarity$MultiStats;->subStats:[Lorg/apache/lucene/search/similarities/Similarity$SimWeight;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v4, :cond_0

    .line 152
    iget-object v2, p0, Lorg/apache/lucene/search/similarities/MultiSimilarity$MultiStats;->subStats:[Lorg/apache/lucene/search/similarities/Similarity$SimWeight;

    array-length v2, v2

    int-to-float v2, v2

    div-float v2, v1, v2

    return v2

    .line 149
    :cond_0
    aget-object v0, v3, v2

    .line 150
    .local v0, "stat":Lorg/apache/lucene/search/similarities/Similarity$SimWeight;
    invoke-virtual {v0}, Lorg/apache/lucene/search/similarities/Similarity$SimWeight;->getValueForNormalization()F

    move-result v5

    add-float/2addr v1, v5

    .line 149
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public normalize(FF)V
    .locals 4
    .param p1, "queryNorm"    # F
    .param p2, "topLevelBoost"    # F

    .prologue
    .line 157
    iget-object v2, p0, Lorg/apache/lucene/search/similarities/MultiSimilarity$MultiStats;->subStats:[Lorg/apache/lucene/search/similarities/Similarity$SimWeight;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v3, :cond_0

    .line 160
    return-void

    .line 157
    :cond_0
    aget-object v0, v2, v1

    .line 158
    .local v0, "stat":Lorg/apache/lucene/search/similarities/Similarity$SimWeight;
    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/search/similarities/Similarity$SimWeight;->normalize(FF)V

    .line 157
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
