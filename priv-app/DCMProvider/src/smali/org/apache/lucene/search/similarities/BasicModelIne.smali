.class public Lorg/apache/lucene/search/similarities/BasicModelIne;
.super Lorg/apache/lucene/search/similarities/BasicModel;
.source "BasicModelIne.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lorg/apache/lucene/search/similarities/BasicModel;-><init>()V

    return-void
.end method


# virtual methods
.method public final score(Lorg/apache/lucene/search/similarities/BasicStats;F)F
    .locals 14
    .param p1, "stats"    # Lorg/apache/lucene/search/similarities/BasicStats;
    .param p2, "tfn"    # F

    .prologue
    .line 34
    invoke-virtual {p1}, Lorg/apache/lucene/search/similarities/BasicStats;->getNumberOfDocuments()J

    move-result-wide v2

    .line 35
    .local v2, "N":J
    invoke-virtual {p1}, Lorg/apache/lucene/search/similarities/BasicStats;->getTotalTermFreq()J

    move-result-wide v0

    .line 36
    .local v0, "F":J
    long-to-double v6, v2

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    const-wide/16 v10, 0x1

    sub-long v10, v2, v10

    long-to-double v10, v10

    long-to-double v12, v2

    div-double/2addr v10, v12

    long-to-double v12, v0

    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v10

    sub-double/2addr v8, v10

    mul-double v4, v6, v8

    .line 37
    .local v4, "ne":D
    const-wide/16 v6, 0x1

    add-long/2addr v6, v2

    long-to-double v6, v6

    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    add-double/2addr v8, v4

    div-double/2addr v6, v8

    invoke-static {v6, v7}, Lorg/apache/lucene/search/similarities/SimilarityBase;->log2(D)D

    move-result-wide v6

    double-to-float v6, v6

    mul-float v6, v6, p2

    return v6
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    const-string v0, "I(ne)"

    return-object v0
.end method
