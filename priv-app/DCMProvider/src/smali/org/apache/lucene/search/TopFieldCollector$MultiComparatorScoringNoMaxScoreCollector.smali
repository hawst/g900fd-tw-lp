.class Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringNoMaxScoreCollector;
.super Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorNonScoringCollector;
.source "TopFieldCollector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/TopFieldCollector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MultiComparatorScoringNoMaxScoreCollector"
.end annotation


# instance fields
.field scorer:Lorg/apache/lucene/search/Scorer;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/FieldValueHitQueue;IZ)V
    .locals 0
    .param p2, "numHits"    # I
    .param p3, "fillFields"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/search/FieldValueHitQueue",
            "<",
            "Lorg/apache/lucene/search/FieldValueHitQueue$Entry;",
            ">;IZ)V"
        }
    .end annotation

    .prologue
    .line 696
    .local p1, "queue":Lorg/apache/lucene/search/FieldValueHitQueue;, "Lorg/apache/lucene/search/FieldValueHitQueue<Lorg/apache/lucene/search/FieldValueHitQueue$Entry;>;"
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorNonScoringCollector;-><init>(Lorg/apache/lucene/search/FieldValueHitQueue;IZ)V

    .line 697
    return-void
.end method


# virtual methods
.method public collect(I)V
    .locals 6
    .param p1, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 707
    iget v4, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringNoMaxScoreCollector;->totalHits:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringNoMaxScoreCollector;->totalHits:I

    .line 708
    iget-boolean v4, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringNoMaxScoreCollector;->queueFull:Z

    if-eqz v4, :cond_4

    .line 710
    const/4 v1, 0x0

    .line 711
    .local v1, "i":I
    :goto_0
    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringNoMaxScoreCollector;->reverseMul:[I

    aget v4, v4, v1

    iget-object v5, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringNoMaxScoreCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    aget-object v5, v5, v1

    invoke-virtual {v5, p1}, Lorg/apache/lucene/search/FieldComparator;->compareBottom(I)I

    move-result v5

    mul-int v0, v4, v5

    .line 712
    .local v0, "c":I
    if-gez v0, :cond_1

    .line 755
    .end local v0    # "c":I
    :cond_0
    return-void

    .line 715
    .restart local v0    # "c":I
    :cond_1
    if-lez v0, :cond_2

    .line 727
    const/4 v1, 0x0

    :goto_1
    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringNoMaxScoreCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    array-length v4, v4

    if-lt v1, v4, :cond_3

    .line 732
    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringNoMaxScoreCollector;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v4}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v2

    .line 733
    .local v2, "score":F
    invoke-virtual {p0, p1, v2}, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringNoMaxScoreCollector;->updateBottom(IF)V

    .line 735
    const/4 v1, 0x0

    :goto_2
    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringNoMaxScoreCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    array-length v4, v4

    if-ge v1, v4, :cond_0

    .line 736
    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringNoMaxScoreCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    aget-object v4, v4, v1

    iget-object v5, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringNoMaxScoreCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iget v5, v5, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->slot:I

    invoke-virtual {v4, v5}, Lorg/apache/lucene/search/FieldComparator;->setBottom(I)V

    .line 735
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 718
    .end local v2    # "score":F
    :cond_2
    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringNoMaxScoreCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    if-eq v1, v4, :cond_0

    .line 710
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 728
    :cond_3
    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringNoMaxScoreCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    aget-object v4, v4, v1

    iget-object v5, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringNoMaxScoreCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iget v5, v5, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->slot:I

    invoke-virtual {v4, v5, p1}, Lorg/apache/lucene/search/FieldComparator;->copy(II)V

    .line 727
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 740
    .end local v0    # "c":I
    .end local v1    # "i":I
    :cond_4
    iget v4, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringNoMaxScoreCollector;->totalHits:I

    add-int/lit8 v3, v4, -0x1

    .line 742
    .local v3, "slot":I
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_3
    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringNoMaxScoreCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    array-length v4, v4

    if-lt v1, v4, :cond_5

    .line 747
    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringNoMaxScoreCollector;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v4}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v2

    .line 748
    .restart local v2    # "score":F
    invoke-virtual {p0, v3, p1, v2}, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringNoMaxScoreCollector;->add(IIF)V

    .line 749
    iget-boolean v4, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringNoMaxScoreCollector;->queueFull:Z

    if-eqz v4, :cond_0

    .line 750
    const/4 v1, 0x0

    :goto_4
    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringNoMaxScoreCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    array-length v4, v4

    if-ge v1, v4, :cond_0

    .line 751
    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringNoMaxScoreCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    aget-object v4, v4, v1

    iget-object v5, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringNoMaxScoreCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iget v5, v5, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->slot:I

    invoke-virtual {v4, v5}, Lorg/apache/lucene/search/FieldComparator;->setBottom(I)V

    .line 750
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 743
    .end local v2    # "score":F
    :cond_5
    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringNoMaxScoreCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    aget-object v4, v4, v1

    invoke-virtual {v4, v3, p1}, Lorg/apache/lucene/search/FieldComparator;->copy(II)V

    .line 742
    add-int/lit8 v1, v1, 0x1

    goto :goto_3
.end method

.method public setScorer(Lorg/apache/lucene/search/Scorer;)V
    .locals 0
    .param p1, "scorer"    # Lorg/apache/lucene/search/Scorer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 759
    iput-object p1, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringNoMaxScoreCollector;->scorer:Lorg/apache/lucene/search/Scorer;

    .line 760
    invoke-super {p0, p1}, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorNonScoringCollector;->setScorer(Lorg/apache/lucene/search/Scorer;)V

    .line 761
    return-void
.end method

.method final updateBottom(IF)V
    .locals 2
    .param p1, "doc"    # I
    .param p2, "score"    # F

    .prologue
    .line 700
    iget-object v0, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringNoMaxScoreCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iget v1, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringNoMaxScoreCollector;->docBase:I

    add-int/2addr v1, p1

    iput v1, v0, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->doc:I

    .line 701
    iget-object v0, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringNoMaxScoreCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iput p2, v0, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->score:F

    .line 702
    iget-object v0, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringNoMaxScoreCollector;->pq:Lorg/apache/lucene/util/PriorityQueue;

    invoke-virtual {v0}, Lorg/apache/lucene/util/PriorityQueue;->updateTop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iput-object v0, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringNoMaxScoreCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    .line 703
    return-void
.end method
