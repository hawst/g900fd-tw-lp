.class public Lorg/apache/lucene/search/similarities/BM25Similarity;
.super Lorg/apache/lucene/search/similarities/Similarity;
.source "BM25Similarity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;,
        Lorg/apache/lucene/search/similarities/BM25Similarity$ExactBM25DocScorer;,
        Lorg/apache/lucene/search/similarities/BM25Similarity$ExactBM25DocScorerNoNorms;,
        Lorg/apache/lucene/search/similarities/BM25Similarity$SloppyBM25DocScorer;
    }
.end annotation


# static fields
.field private static final NORM_TABLE:[F


# instance fields
.field private final b:F

.field protected discountOverlaps:Z

.field private final k1:F


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/16 v5, 0x100

    .line 127
    new-array v2, v5, [F

    sput-object v2, Lorg/apache/lucene/search/similarities/BM25Similarity;->NORM_TABLE:[F

    .line 130
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v5, :cond_0

    .line 134
    return-void

    .line 131
    :cond_0
    int-to-byte v2, v1

    invoke-static {v2}, Lorg/apache/lucene/util/SmallFloat;->byte315ToFloat(B)F

    move-result v0

    .line 132
    .local v0, "f":F
    sget-object v2, Lorg/apache/lucene/search/similarities/BM25Similarity;->NORM_TABLE:[F

    const/high16 v3, 0x3f800000    # 1.0f

    mul-float v4, v0, v0

    div-float/2addr v3, v4

    aput v3, v2, v1

    .line 130
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Lorg/apache/lucene/search/similarities/Similarity;-><init>()V

    .line 109
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/search/similarities/BM25Similarity;->discountOverlaps:Z

    .line 60
    const v0, 0x3f99999a    # 1.2f

    iput v0, p0, Lorg/apache/lucene/search/similarities/BM25Similarity;->k1:F

    .line 61
    const/high16 v0, 0x3f400000    # 0.75f

    iput v0, p0, Lorg/apache/lucene/search/similarities/BM25Similarity;->b:F

    .line 62
    return-void
.end method

.method public constructor <init>(FF)V
    .locals 1
    .param p1, "k1"    # F
    .param p2, "b"    # F

    .prologue
    .line 48
    invoke-direct {p0}, Lorg/apache/lucene/search/similarities/Similarity;-><init>()V

    .line 109
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/search/similarities/BM25Similarity;->discountOverlaps:Z

    .line 49
    iput p1, p0, Lorg/apache/lucene/search/similarities/BM25Similarity;->k1:F

    .line 50
    iput p2, p0, Lorg/apache/lucene/search/similarities/BM25Similarity;->b:F

    .line 51
    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/search/similarities/BM25Similarity;)F
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lorg/apache/lucene/search/similarities/BM25Similarity;->k1:F

    return v0
.end method

.method static synthetic access$1(Lorg/apache/lucene/search/similarities/BM25Similarity;ILorg/apache/lucene/search/Explanation;Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;Lorg/apache/lucene/index/NumericDocValues;)Lorg/apache/lucene/search/Explanation;
    .locals 1

    .prologue
    .line 358
    invoke-direct {p0, p1, p2, p3, p4}, Lorg/apache/lucene/search/similarities/BM25Similarity;->explainScore(ILorg/apache/lucene/search/Explanation;Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;Lorg/apache/lucene/index/NumericDocValues;)Lorg/apache/lucene/search/Explanation;

    move-result-object v0

    return-object v0
.end method

.method private explainScore(ILorg/apache/lucene/search/Explanation;Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;Lorg/apache/lucene/index/NumericDocValues;)Lorg/apache/lucene/search/Explanation;
    .locals 10
    .param p1, "doc"    # I
    .param p2, "freq"    # Lorg/apache/lucene/search/Explanation;
    .param p3, "stats"    # Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;
    .param p4, "norms"    # Lorg/apache/lucene/index/NumericDocValues;

    .prologue
    const/high16 v8, 0x3f800000    # 1.0f

    .line 359
    new-instance v2, Lorg/apache/lucene/search/Explanation;

    invoke-direct {v2}, Lorg/apache/lucene/search/Explanation;-><init>()V

    .line 360
    .local v2, "result":Lorg/apache/lucene/search/Explanation;
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "score(doc="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ",freq="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "), product of:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lorg/apache/lucene/search/Explanation;->setDescription(Ljava/lang/String;)V

    .line 362
    new-instance v0, Lorg/apache/lucene/search/Explanation;

    # getter for: Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;->queryBoost:F
    invoke-static {p3}, Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;->access$3(Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;)F

    move-result v4

    # getter for: Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;->topLevelBoost:F
    invoke-static {p3}, Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;->access$4(Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;)F

    move-result v5

    mul-float/2addr v4, v5

    const-string v5, "boost"

    invoke-direct {v0, v4, v5}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    .line 363
    .local v0, "boostExpl":Lorg/apache/lucene/search/Explanation;
    invoke-virtual {v0}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v4

    cmpl-float v4, v4, v8

    if-eqz v4, :cond_0

    .line 364
    invoke-virtual {v2, v0}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 366
    :cond_0
    # getter for: Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;->idf:Lorg/apache/lucene/search/Explanation;
    invoke-static {p3}, Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;->access$5(Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;)Lorg/apache/lucene/search/Explanation;

    move-result-object v4

    invoke-virtual {v2, v4}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 368
    new-instance v3, Lorg/apache/lucene/search/Explanation;

    invoke-direct {v3}, Lorg/apache/lucene/search/Explanation;-><init>()V

    .line 369
    .local v3, "tfNormExpl":Lorg/apache/lucene/search/Explanation;
    const-string v4, "tfNorm, computed from:"

    invoke-virtual {v3, v4}, Lorg/apache/lucene/search/Explanation;->setDescription(Ljava/lang/String;)V

    .line 370
    invoke-virtual {v3, p2}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 371
    new-instance v4, Lorg/apache/lucene/search/Explanation;

    iget v5, p0, Lorg/apache/lucene/search/similarities/BM25Similarity;->k1:F

    const-string v6, "parameter k1"

    invoke-direct {v4, v5, v6}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    invoke-virtual {v3, v4}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 372
    if-nez p4, :cond_1

    .line 373
    new-instance v4, Lorg/apache/lucene/search/Explanation;

    const/4 v5, 0x0

    const-string v6, "parameter b (norms omitted for field)"

    invoke-direct {v4, v5, v6}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    invoke-virtual {v3, v4}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 374
    invoke-virtual {p2}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v4

    iget v5, p0, Lorg/apache/lucene/search/similarities/BM25Similarity;->k1:F

    add-float/2addr v5, v8

    mul-float/2addr v4, v5

    invoke-virtual {p2}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v5

    iget v6, p0, Lorg/apache/lucene/search/similarities/BM25Similarity;->k1:F

    add-float/2addr v5, v6

    div-float/2addr v4, v5

    invoke-virtual {v3, v4}, Lorg/apache/lucene/search/Explanation;->setValue(F)V

    .line 382
    :goto_0
    invoke-virtual {v2, v3}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 383
    invoke-virtual {v0}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v4

    # getter for: Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;->idf:Lorg/apache/lucene/search/Explanation;
    invoke-static {p3}, Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;->access$5(Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;)Lorg/apache/lucene/search/Explanation;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v5

    mul-float/2addr v4, v5

    invoke-virtual {v3}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v5

    mul-float/2addr v4, v5

    invoke-virtual {v2, v4}, Lorg/apache/lucene/search/Explanation;->setValue(F)V

    .line 384
    return-object v2

    .line 376
    :cond_1
    invoke-virtual {p4, p1}, Lorg/apache/lucene/index/NumericDocValues;->get(I)J

    move-result-wide v4

    long-to-int v4, v4

    int-to-byte v4, v4

    invoke-virtual {p0, v4}, Lorg/apache/lucene/search/similarities/BM25Similarity;->decodeNormValue(B)F

    move-result v1

    .line 377
    .local v1, "doclen":F
    new-instance v4, Lorg/apache/lucene/search/Explanation;

    iget v5, p0, Lorg/apache/lucene/search/similarities/BM25Similarity;->b:F

    const-string v6, "parameter b"

    invoke-direct {v4, v5, v6}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    invoke-virtual {v3, v4}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 378
    new-instance v4, Lorg/apache/lucene/search/Explanation;

    # getter for: Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;->avgdl:F
    invoke-static {p3}, Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;->access$6(Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;)F

    move-result v5

    const-string v6, "avgFieldLength"

    invoke-direct {v4, v5, v6}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    invoke-virtual {v3, v4}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 379
    new-instance v4, Lorg/apache/lucene/search/Explanation;

    const-string v5, "fieldLength"

    invoke-direct {v4, v1, v5}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    invoke-virtual {v3, v4}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 380
    invoke-virtual {p2}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v4

    iget v5, p0, Lorg/apache/lucene/search/similarities/BM25Similarity;->k1:F

    add-float/2addr v5, v8

    mul-float/2addr v4, v5

    invoke-virtual {p2}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v5

    iget v6, p0, Lorg/apache/lucene/search/similarities/BM25Similarity;->k1:F

    iget v7, p0, Lorg/apache/lucene/search/similarities/BM25Similarity;->b:F

    sub-float v7, v8, v7

    iget v8, p0, Lorg/apache/lucene/search/similarities/BM25Similarity;->b:F

    mul-float/2addr v8, v1

    # getter for: Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;->avgdl:F
    invoke-static {p3}, Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;->access$6(Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;)F

    move-result v9

    div-float/2addr v8, v9

    add-float/2addr v7, v8

    mul-float/2addr v6, v7

    add-float/2addr v5, v6

    div-float/2addr v4, v5

    invoke-virtual {v3, v4}, Lorg/apache/lucene/search/Explanation;->setValue(F)V

    goto :goto_0
.end method


# virtual methods
.method protected avgFieldLength(Lorg/apache/lucene/search/CollectionStatistics;)F
    .locals 6
    .param p1, "collectionStats"    # Lorg/apache/lucene/search/CollectionStatistics;

    .prologue
    .line 83
    invoke-virtual {p1}, Lorg/apache/lucene/search/CollectionStatistics;->sumTotalTermFreq()J

    move-result-wide v0

    .line 84
    .local v0, "sumTotalTermFreq":J
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gtz v2, :cond_0

    .line 85
    const/high16 v2, 0x3f800000    # 1.0f

    .line 87
    :goto_0
    return v2

    :cond_0
    long-to-double v2, v0

    invoke-virtual {p1}, Lorg/apache/lucene/search/CollectionStatistics;->maxDoc()J

    move-result-wide v4

    long-to-double v4, v4

    div-double/2addr v2, v4

    double-to-float v2, v2

    goto :goto_0
.end method

.method public final computeNorm(Lorg/apache/lucene/index/FieldInvertState;)J
    .locals 4
    .param p1, "state"    # Lorg/apache/lucene/index/FieldInvertState;

    .prologue
    .line 139
    iget-boolean v1, p0, Lorg/apache/lucene/search/similarities/BM25Similarity;->discountOverlaps:Z

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lorg/apache/lucene/index/FieldInvertState;->getLength()I

    move-result v1

    invoke-virtual {p1}, Lorg/apache/lucene/index/FieldInvertState;->getNumOverlap()I

    move-result v2

    sub-int v0, v1, v2

    .line 140
    .local v0, "numTerms":I
    :goto_0
    invoke-virtual {p1}, Lorg/apache/lucene/index/FieldInvertState;->getBoost()F

    move-result v1

    invoke-virtual {p0, v1, v0}, Lorg/apache/lucene/search/similarities/BM25Similarity;->encodeNormValue(FI)B

    move-result v1

    int-to-long v2, v1

    return-wide v2

    .line 139
    .end local v0    # "numTerms":I
    :cond_0
    invoke-virtual {p1}, Lorg/apache/lucene/index/FieldInvertState;->getLength()I

    move-result v0

    goto :goto_0
.end method

.method public final varargs computeWeight(FLorg/apache/lucene/search/CollectionStatistics;[Lorg/apache/lucene/search/TermStatistics;)Lorg/apache/lucene/search/similarities/Similarity$SimWeight;
    .locals 8
    .param p1, "queryBoost"    # F
    .param p2, "collectionStats"    # Lorg/apache/lucene/search/CollectionStatistics;
    .param p3, "termStats"    # [Lorg/apache/lucene/search/TermStatistics;

    .prologue
    .line 202
    array-length v0, p3

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    aget-object v0, p3, v0

    invoke-virtual {p0, p2, v0}, Lorg/apache/lucene/search/similarities/BM25Similarity;->idfExplain(Lorg/apache/lucene/search/CollectionStatistics;Lorg/apache/lucene/search/TermStatistics;)Lorg/apache/lucene/search/Explanation;

    move-result-object v2

    .line 204
    .local v2, "idf":Lorg/apache/lucene/search/Explanation;
    :goto_0
    invoke-virtual {p0, p2}, Lorg/apache/lucene/search/similarities/BM25Similarity;->avgFieldLength(Lorg/apache/lucene/search/CollectionStatistics;)F

    move-result v4

    .line 207
    .local v4, "avgdl":F
    const/16 v0, 0x100

    new-array v5, v0, [F

    .line 208
    .local v5, "cache":[F
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    array-length v0, v5

    if-lt v6, v0, :cond_1

    .line 211
    new-instance v0, Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;

    invoke-virtual {p2}, Lorg/apache/lucene/search/CollectionStatistics;->field()Ljava/lang/String;

    move-result-object v1

    move v3, p1

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;-><init>(Ljava/lang/String;Lorg/apache/lucene/search/Explanation;FF[F)V

    return-object v0

    .line 202
    .end local v2    # "idf":Lorg/apache/lucene/search/Explanation;
    .end local v4    # "avgdl":F
    .end local v5    # "cache":[F
    .end local v6    # "i":I
    :cond_0
    invoke-virtual {p0, p2, p3}, Lorg/apache/lucene/search/similarities/BM25Similarity;->idfExplain(Lorg/apache/lucene/search/CollectionStatistics;[Lorg/apache/lucene/search/TermStatistics;)Lorg/apache/lucene/search/Explanation;

    move-result-object v2

    goto :goto_0

    .line 209
    .restart local v2    # "idf":Lorg/apache/lucene/search/Explanation;
    .restart local v4    # "avgdl":F
    .restart local v5    # "cache":[F
    .restart local v6    # "i":I
    :cond_1
    iget v0, p0, Lorg/apache/lucene/search/similarities/BM25Similarity;->k1:F

    const/high16 v1, 0x3f800000    # 1.0f

    iget v3, p0, Lorg/apache/lucene/search/similarities/BM25Similarity;->b:F

    sub-float/2addr v1, v3

    iget v3, p0, Lorg/apache/lucene/search/similarities/BM25Similarity;->b:F

    int-to-byte v7, v6

    invoke-virtual {p0, v7}, Lorg/apache/lucene/search/similarities/BM25Similarity;->decodeNormValue(B)F

    move-result v7

    mul-float/2addr v3, v7

    div-float/2addr v3, v4

    add-float/2addr v1, v3

    mul-float/2addr v0, v1

    aput v0, v5, v6

    .line 208
    add-int/lit8 v6, v6, 0x1

    goto :goto_1
.end method

.method protected decodeNormValue(B)F
    .locals 2
    .param p1, "b"    # B

    .prologue
    .line 102
    sget-object v0, Lorg/apache/lucene/search/similarities/BM25Similarity;->NORM_TABLE:[F

    and-int/lit16 v1, p1, 0xff

    aget v0, v0, v1

    return v0
.end method

.method protected encodeNormValue(FI)B
    .locals 2
    .param p1, "boost"    # F
    .param p2, "fieldLength"    # I

    .prologue
    .line 96
    int-to-double v0, p2

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    div-float v0, p1, v0

    invoke-static {v0}, Lorg/apache/lucene/util/SmallFloat;->floatToByte315(F)B

    move-result v0

    return v0
.end method

.method public final exactSimScorer(Lorg/apache/lucene/search/similarities/Similarity$SimWeight;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;
    .locals 4
    .param p1, "stats"    # Lorg/apache/lucene/search/similarities/Similarity$SimWeight;
    .param p2, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 216
    move-object v0, p1

    check-cast v0, Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;

    .line 217
    .local v0, "bm25stats":Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;
    invoke-virtual {p2}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v2

    # getter for: Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;->field:Ljava/lang/String;
    invoke-static {v0}, Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;->access$2(Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/apache/lucene/index/AtomicReader;->getNormValues(Ljava/lang/String;)Lorg/apache/lucene/index/NumericDocValues;

    move-result-object v1

    .line 218
    .local v1, "norms":Lorg/apache/lucene/index/NumericDocValues;
    if-nez v1, :cond_0

    .line 219
    new-instance v2, Lorg/apache/lucene/search/similarities/BM25Similarity$ExactBM25DocScorerNoNorms;

    invoke-direct {v2, p0, v0}, Lorg/apache/lucene/search/similarities/BM25Similarity$ExactBM25DocScorerNoNorms;-><init>(Lorg/apache/lucene/search/similarities/BM25Similarity;Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;)V

    .line 218
    :goto_0
    return-object v2

    .line 220
    :cond_0
    new-instance v2, Lorg/apache/lucene/search/similarities/BM25Similarity$ExactBM25DocScorer;

    invoke-direct {v2, p0, v0, v1}, Lorg/apache/lucene/search/similarities/BM25Similarity$ExactBM25DocScorer;-><init>(Lorg/apache/lucene/search/similarities/BM25Similarity;Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;Lorg/apache/lucene/index/NumericDocValues;)V

    goto :goto_0
.end method

.method public getB()F
    .locals 1

    .prologue
    .line 405
    iget v0, p0, Lorg/apache/lucene/search/similarities/BM25Similarity;->b:F

    return v0
.end method

.method public getDiscountOverlaps()Z
    .locals 1

    .prologue
    .line 123
    iget-boolean v0, p0, Lorg/apache/lucene/search/similarities/BM25Similarity;->discountOverlaps:Z

    return v0
.end method

.method public getK1()F
    .locals 1

    .prologue
    .line 397
    iget v0, p0, Lorg/apache/lucene/search/similarities/BM25Similarity;->k1:F

    return v0
.end method

.method protected idf(JJ)F
    .locals 9
    .param p1, "docFreq"    # J
    .param p3, "numDocs"    # J

    .prologue
    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    .line 66
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    sub-long v2, p3, p1

    long-to-double v2, v2

    add-double/2addr v2, v6

    long-to-double v4, p1

    add-double/2addr v4, v6

    div-double/2addr v2, v4

    add-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method public idfExplain(Lorg/apache/lucene/search/CollectionStatistics;Lorg/apache/lucene/search/TermStatistics;)Lorg/apache/lucene/search/Explanation;
    .locals 8
    .param p1, "collectionStats"    # Lorg/apache/lucene/search/CollectionStatistics;
    .param p2, "termStats"    # Lorg/apache/lucene/search/TermStatistics;

    .prologue
    .line 166
    invoke-virtual {p2}, Lorg/apache/lucene/search/TermStatistics;->docFreq()J

    move-result-wide v0

    .line 167
    .local v0, "df":J
    invoke-virtual {p1}, Lorg/apache/lucene/search/CollectionStatistics;->maxDoc()J

    move-result-wide v4

    .line 168
    .local v4, "max":J
    invoke-virtual {p0, v0, v1, v4, v5}, Lorg/apache/lucene/search/similarities/BM25Similarity;->idf(JJ)F

    move-result v2

    .line 169
    .local v2, "idf":F
    new-instance v3, Lorg/apache/lucene/search/Explanation;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "idf(docFreq="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", maxDocs="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v2, v6}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    return-object v3
.end method

.method public idfExplain(Lorg/apache/lucene/search/CollectionStatistics;[Lorg/apache/lucene/search/TermStatistics;)Lorg/apache/lucene/search/Explanation;
    .locals 13
    .param p1, "collectionStats"    # Lorg/apache/lucene/search/CollectionStatistics;
    .param p2, "termStats"    # [Lorg/apache/lucene/search/TermStatistics;

    .prologue
    .line 186
    invoke-virtual {p1}, Lorg/apache/lucene/search/CollectionStatistics;->maxDoc()J

    move-result-wide v4

    .line 187
    .local v4, "max":J
    const/4 v3, 0x0

    .line 188
    .local v3, "idf":F
    new-instance v2, Lorg/apache/lucene/search/Explanation;

    invoke-direct {v2}, Lorg/apache/lucene/search/Explanation;-><init>()V

    .line 189
    .local v2, "exp":Lorg/apache/lucene/search/Explanation;
    const-string v8, "idf(), sum of:"

    invoke-virtual {v2, v8}, Lorg/apache/lucene/search/Explanation;->setDescription(Ljava/lang/String;)V

    .line 190
    array-length v9, p2

    const/4 v8, 0x0

    :goto_0
    if-lt v8, v9, :cond_0

    .line 196
    invoke-virtual {v2, v3}, Lorg/apache/lucene/search/Explanation;->setValue(F)V

    .line 197
    return-object v2

    .line 190
    :cond_0
    aget-object v6, p2, v8

    .line 191
    .local v6, "stat":Lorg/apache/lucene/search/TermStatistics;
    invoke-virtual {v6}, Lorg/apache/lucene/search/TermStatistics;->docFreq()J

    move-result-wide v0

    .line 192
    .local v0, "df":J
    invoke-virtual {p0, v0, v1, v4, v5}, Lorg/apache/lucene/search/similarities/BM25Similarity;->idf(JJ)F

    move-result v7

    .line 193
    .local v7, "termIdf":F
    new-instance v10, Lorg/apache/lucene/search/Explanation;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "idf(docFreq="

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", maxDocs="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ")"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v7, v11}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    invoke-virtual {v2, v10}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 194
    add-float/2addr v3, v7

    .line 190
    add-int/lit8 v8, v8, 0x1

    goto :goto_0
.end method

.method protected scorePayload(IIILorg/apache/lucene/util/BytesRef;)F
    .locals 1
    .param p1, "doc"    # I
    .param p2, "start"    # I
    .param p3, "end"    # I
    .param p4, "payload"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 76
    const/high16 v0, 0x3f800000    # 1.0f

    return v0
.end method

.method public setDiscountOverlaps(Z)V
    .locals 0
    .param p1, "v"    # Z

    .prologue
    .line 115
    iput-boolean p1, p0, Lorg/apache/lucene/search/similarities/BM25Similarity;->discountOverlaps:Z

    .line 116
    return-void
.end method

.method protected sloppyFreq(I)F
    .locals 2
    .param p1, "distance"    # I

    .prologue
    .line 71
    const/high16 v0, 0x3f800000    # 1.0f

    add-int/lit8 v1, p1, 0x1

    int-to-float v1, v1

    div-float/2addr v0, v1

    return v0
.end method

.method public final sloppySimScorer(Lorg/apache/lucene/search/similarities/Similarity$SimWeight;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;
    .locals 4
    .param p1, "stats"    # Lorg/apache/lucene/search/similarities/Similarity$SimWeight;
    .param p2, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 225
    move-object v0, p1

    check-cast v0, Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;

    .line 226
    .local v0, "bm25stats":Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;
    new-instance v1, Lorg/apache/lucene/search/similarities/BM25Similarity$SloppyBM25DocScorer;

    invoke-virtual {p2}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v2

    # getter for: Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;->field:Ljava/lang/String;
    invoke-static {v0}, Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;->access$2(Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/apache/lucene/index/AtomicReader;->getNormValues(Ljava/lang/String;)Lorg/apache/lucene/index/NumericDocValues;

    move-result-object v2

    invoke-direct {v1, p0, v0, v2}, Lorg/apache/lucene/search/similarities/BM25Similarity$SloppyBM25DocScorer;-><init>(Lorg/apache/lucene/search/similarities/BM25Similarity;Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;Lorg/apache/lucene/index/NumericDocValues;)V

    return-object v1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 389
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "BM25(k1="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lorg/apache/lucene/search/similarities/BM25Similarity;->k1:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",b="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/search/similarities/BM25Similarity;->b:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
