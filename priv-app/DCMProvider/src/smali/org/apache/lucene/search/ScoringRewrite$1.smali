.class Lorg/apache/lucene/search/ScoringRewrite$1;
.super Lorg/apache/lucene/search/ScoringRewrite;
.source "ScoringRewrite.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/ScoringRewrite;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/ScoringRewrite",
        "<",
        "Lorg/apache/lucene/search/BooleanQuery;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Lorg/apache/lucene/search/ScoringRewrite;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method protected addClause(Lorg/apache/lucene/search/BooleanQuery;Lorg/apache/lucene/index/Term;IFLorg/apache/lucene/index/TermContext;)V
    .locals 2
    .param p1, "topLevel"    # Lorg/apache/lucene/search/BooleanQuery;
    .param p2, "term"    # Lorg/apache/lucene/index/Term;
    .param p3, "docCount"    # I
    .param p4, "boost"    # F
    .param p5, "states"    # Lorg/apache/lucene/index/TermContext;

    .prologue
    .line 64
    new-instance v0, Lorg/apache/lucene/search/TermQuery;

    invoke-direct {v0, p2, p5}, Lorg/apache/lucene/search/TermQuery;-><init>(Lorg/apache/lucene/index/Term;Lorg/apache/lucene/index/TermContext;)V

    .line 65
    .local v0, "tq":Lorg/apache/lucene/search/TermQuery;
    invoke-virtual {v0, p4}, Lorg/apache/lucene/search/TermQuery;->setBoost(F)V

    .line 66
    sget-object v1, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {p1, v0, v1}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 67
    return-void
.end method

.method protected bridge synthetic addClause(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/index/Term;IFLorg/apache/lucene/index/TermContext;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    move-object v1, p1

    check-cast v1, Lorg/apache/lucene/search/BooleanQuery;

    move-object v0, p0

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lorg/apache/lucene/search/ScoringRewrite$1;->addClause(Lorg/apache/lucene/search/BooleanQuery;Lorg/apache/lucene/index/Term;IFLorg/apache/lucene/index/TermContext;)V

    return-void
.end method

.method protected checkMaxClauseCount(I)V
    .locals 1
    .param p1, "count"    # I

    .prologue
    .line 71
    invoke-static {}, Lorg/apache/lucene/search/BooleanQuery;->getMaxClauseCount()I

    move-result v0

    if-le p1, v0, :cond_0

    .line 72
    new-instance v0, Lorg/apache/lucene/search/BooleanQuery$TooManyClauses;

    invoke-direct {v0}, Lorg/apache/lucene/search/BooleanQuery$TooManyClauses;-><init>()V

    throw v0

    .line 73
    :cond_0
    return-void
.end method

.method protected getTopLevelQuery()Lorg/apache/lucene/search/BooleanQuery;
    .locals 2

    .prologue
    .line 58
    new-instance v0, Lorg/apache/lucene/search/BooleanQuery;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lorg/apache/lucene/search/BooleanQuery;-><init>(Z)V

    return-object v0
.end method

.method protected bridge synthetic getTopLevelQuery()Lorg/apache/lucene/search/Query;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/search/ScoringRewrite$1;->getTopLevelQuery()Lorg/apache/lucene/search/BooleanQuery;

    move-result-object v0

    return-object v0
.end method
