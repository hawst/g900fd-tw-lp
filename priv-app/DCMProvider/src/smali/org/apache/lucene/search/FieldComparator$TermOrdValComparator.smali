.class public final Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;
.super Lorg/apache/lucene/search/FieldComparator;
.source "FieldComparator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FieldComparator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TermOrdValComparator"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator$AnyOrdComparator;,
        Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator$PerSegmentComparator;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/FieldComparator",
        "<",
        "Lorg/apache/lucene/util/BytesRef;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field bottomOrd:I

.field bottomSameReader:Z

.field bottomSlot:I

.field bottomValue:Lorg/apache/lucene/util/BytesRef;

.field currentReaderGen:I

.field private final field:Ljava/lang/String;

.field final ords:[I

.field final readerGen:[I

.field final tempBR:Lorg/apache/lucene/util/BytesRef;

.field termsIndex:Lorg/apache/lucene/index/SortedDocValues;

.field final values:[Lorg/apache/lucene/util/BytesRef;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 862
    const-class v0, Lorg/apache/lucene/search/FieldComparator;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(ILjava/lang/String;)V
    .locals 1
    .param p1, "numHits"    # I
    .param p2, "field"    # Ljava/lang/String;

    .prologue
    const/4 v0, -0x1

    .line 909
    invoke-direct {p0}, Lorg/apache/lucene/search/FieldComparator;-><init>()V

    .line 880
    iput v0, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->currentReaderGen:I

    .line 890
    iput v0, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->bottomSlot:I

    .line 907
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->tempBR:Lorg/apache/lucene/util/BytesRef;

    .line 910
    new-array v0, p1, [I

    iput-object v0, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->ords:[I

    .line 911
    new-array v0, p1, [Lorg/apache/lucene/util/BytesRef;

    iput-object v0, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->values:[Lorg/apache/lucene/util/BytesRef;

    .line 912
    new-array v0, p1, [I

    iput-object v0, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->readerGen:[I

    .line 913
    iput-object p2, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->field:Ljava/lang/String;

    .line 914
    return-void
.end method


# virtual methods
.method public compare(II)I
    .locals 4
    .param p1, "slot1"    # I
    .param p2, "slot2"    # I

    .prologue
    .line 918
    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->readerGen:[I

    aget v2, v2, p1

    iget-object v3, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->readerGen:[I

    aget v3, v3, p2

    if-ne v2, v3, :cond_0

    .line 919
    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->ords:[I

    aget v2, v2, p1

    iget-object v3, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->ords:[I

    aget v3, v3, p2

    sub-int/2addr v2, v3

    .line 932
    :goto_0
    return v2

    .line 922
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->values:[Lorg/apache/lucene/util/BytesRef;

    aget-object v0, v2, p1

    .line 923
    .local v0, "val1":Lorg/apache/lucene/util/BytesRef;
    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->values:[Lorg/apache/lucene/util/BytesRef;

    aget-object v1, v2, p2

    .line 924
    .local v1, "val2":Lorg/apache/lucene/util/BytesRef;
    if-nez v0, :cond_2

    .line 925
    if-nez v1, :cond_1

    .line 926
    const/4 v2, 0x0

    goto :goto_0

    .line 928
    :cond_1
    const/4 v2, -0x1

    goto :goto_0

    .line 929
    :cond_2
    if-nez v1, :cond_3

    .line 930
    const/4 v2, 0x1

    goto :goto_0

    .line 932
    :cond_3
    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/BytesRef;->compareTo(Lorg/apache/lucene/util/BytesRef;)I

    move-result v2

    goto :goto_0
.end method

.method public compareBottom(I)I
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 937
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public bridge synthetic compareDocToValue(ILjava/lang/Object;)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    check-cast p2, Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->compareDocToValue(ILorg/apache/lucene/util/BytesRef;)I

    move-result v0

    return v0
.end method

.method public compareDocToValue(ILorg/apache/lucene/util/BytesRef;)I
    .locals 3
    .param p1, "doc"    # I
    .param p2, "value"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    const/4 v1, -0x1

    .line 947
    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->termsIndex:Lorg/apache/lucene/index/SortedDocValues;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/index/SortedDocValues;->getOrd(I)I

    move-result v0

    .line 948
    .local v0, "ord":I
    if-ne v0, v1, :cond_1

    .line 949
    if-nez p2, :cond_0

    .line 950
    const/4 v1, 0x0

    .line 957
    :cond_0
    :goto_0
    return v1

    .line 953
    :cond_1
    if-nez p2, :cond_2

    .line 954
    const/4 v1, 0x1

    goto :goto_0

    .line 956
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->termsIndex:Lorg/apache/lucene/index/SortedDocValues;

    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->tempBR:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v1, v0, v2}, Lorg/apache/lucene/index/SortedDocValues;->lookupOrd(ILorg/apache/lucene/util/BytesRef;)V

    .line 957
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->tempBR:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v1, p2}, Lorg/apache/lucene/util/BytesRef;->compareTo(Lorg/apache/lucene/util/BytesRef;)I

    move-result v1

    goto :goto_0
.end method

.method public copy(II)V
    .locals 1
    .param p1, "slot"    # I
    .param p2, "doc"    # I

    .prologue
    .line 942
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setBottom(I)V
    .locals 6
    .param p1, "bottom"    # I

    .prologue
    const/4 v5, -0x1

    const/4 v4, 0x1

    .line 1066
    iput p1, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->bottomSlot:I

    .line 1068
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->values:[Lorg/apache/lucene/util/BytesRef;

    iget v2, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->bottomSlot:I

    aget-object v1, v1, v2

    iput-object v1, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->bottomValue:Lorg/apache/lucene/util/BytesRef;

    .line 1069
    iget v1, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->currentReaderGen:I

    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->readerGen:[I

    iget v3, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->bottomSlot:I

    aget v2, v2, v3

    if-ne v1, v2, :cond_0

    .line 1070
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->ords:[I

    iget v2, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->bottomSlot:I

    aget v1, v1, v2

    iput v1, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->bottomOrd:I

    .line 1071
    iput-boolean v4, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->bottomSameReader:Z

    .line 1093
    :goto_0
    return-void

    .line 1073
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->bottomValue:Lorg/apache/lucene/util/BytesRef;

    if-nez v1, :cond_2

    .line 1075
    sget-boolean v1, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->ords:[I

    iget v2, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->bottomSlot:I

    aget v1, v1, v2

    if-eq v1, v5, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 1076
    :cond_1
    iput v5, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->bottomOrd:I

    .line 1077
    iput-boolean v4, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->bottomSameReader:Z

    .line 1078
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->readerGen:[I

    iget v2, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->bottomSlot:I

    iget v3, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->currentReaderGen:I

    aput v3, v1, v2

    goto :goto_0

    .line 1080
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->termsIndex:Lorg/apache/lucene/index/SortedDocValues;

    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->bottomValue:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/index/SortedDocValues;->lookupTerm(Lorg/apache/lucene/util/BytesRef;)I

    move-result v0

    .line 1081
    .local v0, "index":I
    if-gez v0, :cond_3

    .line 1082
    neg-int v1, v0

    add-int/lit8 v1, v1, -0x2

    iput v1, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->bottomOrd:I

    .line 1083
    const/4 v1, 0x0

    iput-boolean v1, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->bottomSameReader:Z

    goto :goto_0

    .line 1085
    :cond_3
    iput v0, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->bottomOrd:I

    .line 1087
    iput-boolean v4, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->bottomSameReader:Z

    .line 1088
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->readerGen:[I

    iget v2, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->bottomSlot:I

    iget v3, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->currentReaderGen:I

    aput v3, v1, v2

    .line 1089
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->ords:[I

    iget v2, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->bottomSlot:I

    iget v3, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->bottomOrd:I

    aput v3, v1, v2

    goto :goto_0
.end method

.method public setNextReader(Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/search/FieldComparator;
    .locals 5
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/AtomicReaderContext;",
            ")",
            "Lorg/apache/lucene/search/FieldComparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1053
    iget v0, p1, Lorg/apache/lucene/index/AtomicReaderContext;->docBase:I

    .line 1054
    .local v0, "docBase":I
    sget-object v2, Lorg/apache/lucene/search/FieldCache;->DEFAULT:Lorg/apache/lucene/search/FieldCache;

    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->field:Ljava/lang/String;

    invoke-interface {v2, v3, v4}, Lorg/apache/lucene/search/FieldCache;->getTermsIndex(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;)Lorg/apache/lucene/index/SortedDocValues;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->termsIndex:Lorg/apache/lucene/index/SortedDocValues;

    .line 1055
    new-instance v1, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator$AnyOrdComparator;

    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->termsIndex:Lorg/apache/lucene/index/SortedDocValues;

    invoke-direct {v1, p0, v2, v0}, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator$AnyOrdComparator;-><init>(Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;Lorg/apache/lucene/index/SortedDocValues;I)V

    .line 1056
    .local v1, "perSegComp":Lorg/apache/lucene/search/FieldComparator;, "Lorg/apache/lucene/search/FieldComparator<Lorg/apache/lucene/util/BytesRef;>;"
    iget v2, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->currentReaderGen:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->currentReaderGen:I

    .line 1057
    iget v2, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->bottomSlot:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 1058
    iget v2, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->bottomSlot:I

    invoke-virtual {v1, v2}, Lorg/apache/lucene/search/FieldComparator;->setBottom(I)V

    .line 1061
    :cond_0
    return-object v1
.end method

.method public bridge synthetic value(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->value(I)Lorg/apache/lucene/util/BytesRef;

    move-result-object v0

    return-object v0
.end method

.method public value(I)Lorg/apache/lucene/util/BytesRef;
    .locals 1
    .param p1, "slot"    # I

    .prologue
    .line 1097
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->values:[Lorg/apache/lucene/util/BytesRef;

    aget-object v0, v0, p1

    return-object v0
.end method
