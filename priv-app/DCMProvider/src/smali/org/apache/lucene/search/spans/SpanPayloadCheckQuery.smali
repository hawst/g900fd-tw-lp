.class public Lorg/apache/lucene/search/spans/SpanPayloadCheckQuery;
.super Lorg/apache/lucene/search/spans/SpanPositionCheckQuery;
.source "SpanPayloadCheckQuery.java"


# instance fields
.field protected final payloadToMatch:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<[B>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/spans/SpanQuery;Ljava/util/Collection;)V
    .locals 2
    .param p1, "match"    # Lorg/apache/lucene/search/spans/SpanQuery;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/search/spans/SpanQuery;",
            "Ljava/util/Collection",
            "<[B>;)V"
        }
    .end annotation

    .prologue
    .line 45
    .local p2, "payloadToMatch":Ljava/util/Collection;, "Ljava/util/Collection<[B>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery;-><init>(Lorg/apache/lucene/search/spans/SpanQuery;)V

    .line 46
    instance-of v0, p1, Lorg/apache/lucene/search/spans/SpanNearQuery;

    if-eqz v0, :cond_0

    .line 47
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "SpanNearQuery not allowed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 49
    :cond_0
    iput-object p2, p0, Lorg/apache/lucene/search/spans/SpanPayloadCheckQuery;->payloadToMatch:Ljava/util/Collection;

    .line 50
    return-void
.end method


# virtual methods
.method protected acceptPosition(Lorg/apache/lucene/search/spans/Spans;)Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$AcceptStatus;
    .locals 6
    .param p1, "spans"    # Lorg/apache/lucene/search/spans/Spans;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 54
    invoke-virtual {p1}, Lorg/apache/lucene/search/spans/Spans;->isPayloadAvailable()Z

    move-result v2

    .line 55
    .local v2, "result":Z
    if-eqz v2, :cond_3

    .line 56
    invoke-virtual {p1}, Lorg/apache/lucene/search/spans/Spans;->getPayload()Ljava/util/Collection;

    move-result-object v1

    .line 57
    .local v1, "candidate":Ljava/util/Collection;, "Ljava/util/Collection<[B>;"
    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v4

    iget-object v5, p0, Lorg/apache/lucene/search/spans/SpanPayloadCheckQuery;->payloadToMatch:Ljava/util/Collection;

    invoke-interface {v5}, Ljava/util/Collection;->size()I

    move-result v5

    if-ne v4, v5, :cond_2

    .line 59
    iget-object v4, p0, Lorg/apache/lucene/search/spans/SpanPayloadCheckQuery;->payloadToMatch:Ljava/util/Collection;

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 62
    .local v3, "toMatchIter":Ljava/util/Iterator;, "Ljava/util/Iterator<[B>;"
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 69
    sget-object v4, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$AcceptStatus;->YES:Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$AcceptStatus;

    .line 74
    .end local v1    # "candidate":Ljava/util/Collection;, "Ljava/util/Collection<[B>;"
    .end local v3    # "toMatchIter":Ljava/util/Iterator;, "Ljava/util/Iterator<[B>;"
    :goto_0
    return-object v4

    .line 62
    .restart local v1    # "candidate":Ljava/util/Collection;, "Ljava/util/Collection<[B>;"
    .restart local v3    # "toMatchIter":Ljava/util/Iterator;, "Ljava/util/Iterator<[B>;"
    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 64
    .local v0, "candBytes":[B
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [B

    invoke-static {v0, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v4

    if-nez v4, :cond_0

    .line 65
    sget-object v4, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$AcceptStatus;->NO:Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$AcceptStatus;

    goto :goto_0

    .line 71
    .end local v0    # "candBytes":[B
    .end local v3    # "toMatchIter":Ljava/util/Iterator;, "Ljava/util/Iterator<[B>;"
    :cond_2
    sget-object v4, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$AcceptStatus;->NO:Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$AcceptStatus;

    goto :goto_0

    .line 74
    .end local v1    # "candidate":Ljava/util/Collection;, "Ljava/util/Collection<[B>;"
    :cond_3
    sget-object v4, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$AcceptStatus;->YES:Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$AcceptStatus;

    goto :goto_0
.end method

.method public bridge synthetic clone()Lorg/apache/lucene/search/Query;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanPayloadCheckQuery;->clone()Lorg/apache/lucene/search/spans/SpanPayloadCheckQuery;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lorg/apache/lucene/search/spans/SpanPayloadCheckQuery;
    .locals 3

    .prologue
    .line 94
    new-instance v0, Lorg/apache/lucene/search/spans/SpanPayloadCheckQuery;

    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanPayloadCheckQuery;->match:Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/SpanQuery;->clone()Lorg/apache/lucene/search/Query;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/search/spans/SpanQuery;

    iget-object v2, p0, Lorg/apache/lucene/search/spans/SpanPayloadCheckQuery;->payloadToMatch:Ljava/util/Collection;

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/search/spans/SpanPayloadCheckQuery;-><init>(Lorg/apache/lucene/search/spans/SpanQuery;Ljava/util/Collection;)V

    .line 95
    .local v0, "result":Lorg/apache/lucene/search/spans/SpanPayloadCheckQuery;
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanPayloadCheckQuery;->getBoost()F

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/spans/SpanPayloadCheckQuery;->setBoost(F)V

    .line 96
    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 101
    if-ne p0, p1, :cond_1

    .line 105
    :cond_0
    :goto_0
    return v1

    .line 102
    :cond_1
    instance-of v3, p1, Lorg/apache/lucene/search/spans/SpanPayloadCheckQuery;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 104
    check-cast v0, Lorg/apache/lucene/search/spans/SpanPayloadCheckQuery;

    .line 105
    .local v0, "other":Lorg/apache/lucene/search/spans/SpanPayloadCheckQuery;
    iget-object v3, p0, Lorg/apache/lucene/search/spans/SpanPayloadCheckQuery;->payloadToMatch:Ljava/util/Collection;

    iget-object v4, v0, Lorg/apache/lucene/search/spans/SpanPayloadCheckQuery;->payloadToMatch:Ljava/util/Collection;

    invoke-interface {v3, v4}, Ljava/util/Collection;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 106
    iget-object v3, p0, Lorg/apache/lucene/search/spans/SpanPayloadCheckQuery;->match:Lorg/apache/lucene/search/spans/SpanQuery;

    iget-object v4, v0, Lorg/apache/lucene/search/spans/SpanPayloadCheckQuery;->match:Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/search/spans/SpanQuery;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 107
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanPayloadCheckQuery;->getBoost()F

    move-result v3

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/SpanPayloadCheckQuery;->getBoost()F

    move-result v4

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    .line 105
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 112
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanPayloadCheckQuery;->match:Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/SpanQuery;->hashCode()I

    move-result v0

    .line 113
    .local v0, "h":I
    shl-int/lit8 v1, v0, 0x8

    ushr-int/lit8 v2, v0, 0x19

    or-int/2addr v1, v2

    xor-int/2addr v0, v1

    .line 115
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanPayloadCheckQuery;->payloadToMatch:Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 116
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanPayloadCheckQuery;->getBoost()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v1

    xor-int/2addr v0, v1

    .line 117
    return v0
.end method

.method public toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 79
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 80
    .local v0, "buffer":Ljava/lang/StringBuilder;
    const-string v2, "spanPayCheck("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    iget-object v2, p0, Lorg/apache/lucene/search/spans/SpanPayloadCheckQuery;->match:Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/search/spans/SpanQuery;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    const-string v2, ", payloadRef: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83
    iget-object v2, p0, Lorg/apache/lucene/search/spans/SpanPayloadCheckQuery;->payloadToMatch:Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 87
    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 88
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanPayloadCheckQuery;->getBoost()F

    move-result v2

    invoke-static {v2}, Lorg/apache/lucene/util/ToStringUtils;->boost(F)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 83
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    .line 84
    .local v1, "bytes":[B
    invoke-static {v0, v1}, Lorg/apache/lucene/util/ToStringUtils;->byteArray(Ljava/lang/StringBuilder;[B)V

    .line 85
    const/16 v3, 0x3b

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0
.end method
