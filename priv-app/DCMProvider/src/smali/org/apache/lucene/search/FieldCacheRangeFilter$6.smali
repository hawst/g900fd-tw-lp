.class Lorg/apache/lucene/search/FieldCacheRangeFilter$6;
.super Lorg/apache/lucene/search/FieldCacheRangeFilter;
.source "FieldCacheRangeFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/FieldCacheRangeFilter;->newLongRange(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$LongParser;Ljava/lang/Long;Ljava/lang/Long;ZZ)Lorg/apache/lucene/search/FieldCacheRangeFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/FieldCacheRangeFilter",
        "<",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Ljava/lang/Long;Ljava/lang/Long;ZZ)V
    .locals 8
    .param p1, "$anonymous0"    # Ljava/lang/String;
    .param p2, "$anonymous1"    # Lorg/apache/lucene/search/FieldCache$Parser;
    .param p3, "$anonymous2"    # Ljava/lang/Long;
    .param p4, "$anonymous3"    # Ljava/lang/Long;
    .param p5, "$anonymous4"    # Z
    .param p6, "$anonymous5"    # Z

    .prologue
    .line 365
    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v7}, Lorg/apache/lucene/search/FieldCacheRangeFilter;-><init>(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Ljava/lang/Object;Ljava/lang/Object;ZZLorg/apache/lucene/search/FieldCacheRangeFilter;)V

    .line 1
    return-void
.end method


# virtual methods
.method public getDocIdSet(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/DocIdSet;
    .locals 12
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "acceptDocs"    # Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v2, 0x1

    .line 369
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$6;->lowerVal:Ljava/lang/Object;

    if-eqz v0, :cond_2

    .line 370
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$6;->lowerVal:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    .line 371
    .local v10, "i":J
    iget-boolean v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$6;->includeLower:Z

    if-nez v0, :cond_0

    const-wide v0, 0x7fffffffffffffffL

    cmp-long v0, v10, v0

    if-nez v0, :cond_0

    .line 372
    sget-object v1, Lorg/apache/lucene/search/DocIdSet;->EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;

    .line 390
    .end local v10    # "i":J
    :goto_0
    return-object v1

    .line 373
    .restart local v10    # "i":J
    :cond_0
    iget-boolean v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$6;->includeLower:Z

    if-eqz v0, :cond_1

    move-wide v6, v10

    .line 377
    .end local v10    # "i":J
    .local v6, "inclusiveLowerPoint":J
    :goto_1
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$6;->upperVal:Ljava/lang/Object;

    if-eqz v0, :cond_5

    .line 378
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$6;->upperVal:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    .line 379
    .restart local v10    # "i":J
    iget-boolean v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$6;->includeUpper:Z

    if-nez v0, :cond_3

    const-wide/high16 v0, -0x8000000000000000L

    cmp-long v0, v10, v0

    if-nez v0, :cond_3

    .line 380
    sget-object v1, Lorg/apache/lucene/search/DocIdSet;->EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;

    goto :goto_0

    .line 373
    .end local v6    # "inclusiveLowerPoint":J
    :cond_1
    add-long v6, v10, v2

    goto :goto_1

    .line 375
    .end local v10    # "i":J
    :cond_2
    const-wide/high16 v6, -0x8000000000000000L

    .restart local v6    # "inclusiveLowerPoint":J
    goto :goto_1

    .line 381
    .restart local v10    # "i":J
    :cond_3
    iget-boolean v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$6;->includeUpper:Z

    if-eqz v0, :cond_4

    move-wide v8, v10

    .line 386
    .end local v10    # "i":J
    .local v8, "inclusiveUpperPoint":J
    :goto_2
    cmp-long v0, v6, v8

    if-lez v0, :cond_6

    .line 387
    sget-object v1, Lorg/apache/lucene/search/DocIdSet;->EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;

    goto :goto_0

    .line 381
    .end local v8    # "inclusiveUpperPoint":J
    .restart local v10    # "i":J
    :cond_4
    sub-long v8, v10, v2

    goto :goto_2

    .line 383
    .end local v10    # "i":J
    :cond_5
    const-wide v8, 0x7fffffffffffffffL

    .restart local v8    # "inclusiveUpperPoint":J
    goto :goto_2

    .line 389
    :cond_6
    sget-object v1, Lorg/apache/lucene/search/FieldCache;->DEFAULT:Lorg/apache/lucene/search/FieldCache;

    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$6;->field:Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$6;->parser:Lorg/apache/lucene/search/FieldCache$Parser;

    check-cast v0, Lorg/apache/lucene/search/FieldCache$LongParser;

    const/4 v4, 0x0

    invoke-interface {v1, v2, v3, v0, v4}, Lorg/apache/lucene/search/FieldCache;->getLongs(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$LongParser;Z)Lorg/apache/lucene/search/FieldCache$Longs;

    move-result-object v5

    .line 390
    .local v5, "values":Lorg/apache/lucene/search/FieldCache$Longs;
    new-instance v1, Lorg/apache/lucene/search/FieldCacheRangeFilter$6$1;

    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v3

    move-object v2, p0

    move-object v4, p2

    invoke-direct/range {v1 .. v9}, Lorg/apache/lucene/search/FieldCacheRangeFilter$6$1;-><init>(Lorg/apache/lucene/search/FieldCacheRangeFilter$6;ILorg/apache/lucene/util/Bits;Lorg/apache/lucene/search/FieldCache$Longs;JJ)V

    goto :goto_0
.end method
