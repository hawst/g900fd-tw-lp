.class public abstract Lorg/apache/lucene/search/ScoringRewrite;
.super Lorg/apache/lucene/search/TermCollectingRewrite;
.source "ScoringRewrite.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/ScoringRewrite$ParallelArraysTermCollector;,
        Lorg/apache/lucene/search/ScoringRewrite$TermFreqBoostByteStart;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Q:",
        "Lorg/apache/lucene/search/Query;",
        ">",
        "Lorg/apache/lucene/search/TermCollectingRewrite",
        "<TQ;>;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final CONSTANT_SCORE_BOOLEAN_QUERY_REWRITE:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

.field public static final SCORING_BOOLEAN_QUERY_REWRITE:Lorg/apache/lucene/search/ScoringRewrite;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/search/ScoringRewrite",
            "<",
            "Lorg/apache/lucene/search/BooleanQuery;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lorg/apache/lucene/search/ScoringRewrite;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/ScoringRewrite;->$assertionsDisabled:Z

    .line 55
    new-instance v0, Lorg/apache/lucene/search/ScoringRewrite$1;

    invoke-direct {v0}, Lorg/apache/lucene/search/ScoringRewrite$1;-><init>()V

    sput-object v0, Lorg/apache/lucene/search/ScoringRewrite;->SCORING_BOOLEAN_QUERY_REWRITE:Lorg/apache/lucene/search/ScoringRewrite;

    .line 86
    new-instance v0, Lorg/apache/lucene/search/ScoringRewrite$2;

    invoke-direct {v0}, Lorg/apache/lucene/search/ScoringRewrite$2;-><init>()V

    sput-object v0, Lorg/apache/lucene/search/ScoringRewrite;->CONSTANT_SCORE_BOOLEAN_QUERY_REWRITE:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    .line 98
    return-void

    .line 40
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    .local p0, "this":Lorg/apache/lucene/search/ScoringRewrite;, "Lorg/apache/lucene/search/ScoringRewrite<TQ;>;"
    invoke-direct {p0}, Lorg/apache/lucene/search/TermCollectingRewrite;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract checkMaxClauseCount(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public final rewrite(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/MultiTermQuery;)Lorg/apache/lucene/search/Query;
    .locals 13
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "query"    # Lorg/apache/lucene/search/MultiTermQuery;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/IndexReader;",
            "Lorg/apache/lucene/search/MultiTermQuery;",
            ")TQ;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 106
    .local p0, "this":Lorg/apache/lucene/search/ScoringRewrite;, "Lorg/apache/lucene/search/ScoringRewrite<TQ;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/search/ScoringRewrite;->getTopLevelQuery()Lorg/apache/lucene/search/Query;

    move-result-object v1

    .line 107
    .local v1, "result":Lorg/apache/lucene/search/Query;, "TQ;"
    new-instance v7, Lorg/apache/lucene/search/ScoringRewrite$ParallelArraysTermCollector;

    invoke-direct {v7, p0}, Lorg/apache/lucene/search/ScoringRewrite$ParallelArraysTermCollector;-><init>(Lorg/apache/lucene/search/ScoringRewrite;)V

    .line 108
    .local v7, "col":Lorg/apache/lucene/search/ScoringRewrite$ParallelArraysTermCollector;, "Lorg/apache/lucene/search/ScoringRewrite<TQ;>.ParallelArraysTermCollector;"
    invoke-virtual {p0, p1, p2, v7}, Lorg/apache/lucene/search/ScoringRewrite;->collectTerms(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/MultiTermQuery;Lorg/apache/lucene/search/TermCollectingRewrite$TermCollector;)V

    .line 110
    iget-object v0, v7, Lorg/apache/lucene/search/ScoringRewrite$ParallelArraysTermCollector;->terms:Lorg/apache/lucene/util/BytesRefHash;

    invoke-virtual {v0}, Lorg/apache/lucene/util/BytesRefHash;->size()I

    move-result v10

    .line 111
    .local v10, "size":I
    if-lez v10, :cond_0

    .line 112
    iget-object v0, v7, Lorg/apache/lucene/search/ScoringRewrite$ParallelArraysTermCollector;->terms:Lorg/apache/lucene/util/BytesRefHash;

    iget-object v3, v7, Lorg/apache/lucene/search/ScoringRewrite$ParallelArraysTermCollector;->termsEnum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v3}, Lorg/apache/lucene/index/TermsEnum;->getComparator()Ljava/util/Comparator;

    move-result-object v3

    invoke-virtual {v0, v3}, Lorg/apache/lucene/util/BytesRefHash;->sort(Ljava/util/Comparator;)[I

    move-result-object v11

    .line 113
    .local v11, "sort":[I
    iget-object v0, v7, Lorg/apache/lucene/search/ScoringRewrite$ParallelArraysTermCollector;->array:Lorg/apache/lucene/search/ScoringRewrite$TermFreqBoostByteStart;

    iget-object v6, v0, Lorg/apache/lucene/search/ScoringRewrite$TermFreqBoostByteStart;->boost:[F

    .line 114
    .local v6, "boost":[F
    iget-object v0, v7, Lorg/apache/lucene/search/ScoringRewrite$ParallelArraysTermCollector;->array:Lorg/apache/lucene/search/ScoringRewrite$TermFreqBoostByteStart;

    iget-object v12, v0, Lorg/apache/lucene/search/ScoringRewrite$TermFreqBoostByteStart;->termState:[Lorg/apache/lucene/index/TermContext;

    .line 115
    .local v12, "termStates":[Lorg/apache/lucene/index/TermContext;
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    if-lt v8, v10, :cond_1

    .line 122
    .end local v6    # "boost":[F
    .end local v8    # "i":I
    .end local v11    # "sort":[I
    .end local v12    # "termStates":[Lorg/apache/lucene/index/TermContext;
    :cond_0
    return-object v1

    .line 116
    .restart local v6    # "boost":[F
    .restart local v8    # "i":I
    .restart local v11    # "sort":[I
    .restart local v12    # "termStates":[Lorg/apache/lucene/index/TermContext;
    :cond_1
    aget v9, v11, v8

    .line 117
    .local v9, "pos":I
    new-instance v2, Lorg/apache/lucene/index/Term;

    invoke-virtual {p2}, Lorg/apache/lucene/search/MultiTermQuery;->getField()Ljava/lang/String;

    move-result-object v0

    iget-object v3, v7, Lorg/apache/lucene/search/ScoringRewrite$ParallelArraysTermCollector;->terms:Lorg/apache/lucene/util/BytesRefHash;

    new-instance v4, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v4}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    invoke-virtual {v3, v9, v4}, Lorg/apache/lucene/util/BytesRefHash;->get(ILorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/BytesRef;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 118
    .local v2, "term":Lorg/apache/lucene/index/Term;
    sget-boolean v0, Lorg/apache/lucene/search/ScoringRewrite;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    invoke-virtual {p1, v2}, Lorg/apache/lucene/index/IndexReader;->docFreq(Lorg/apache/lucene/index/Term;)I

    move-result v0

    aget-object v3, v12, v9

    invoke-virtual {v3}, Lorg/apache/lucene/index/TermContext;->docFreq()I

    move-result v3

    if-eq v0, v3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 119
    :cond_2
    aget-object v0, v12, v9

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermContext;->docFreq()I

    move-result v3

    invoke-virtual {p2}, Lorg/apache/lucene/search/MultiTermQuery;->getBoost()F

    move-result v0

    aget v4, v6, v9

    mul-float/2addr v4, v0

    aget-object v5, v12, v9

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lorg/apache/lucene/search/ScoringRewrite;->addClause(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/index/Term;IFLorg/apache/lucene/index/TermContext;)V

    .line 115
    add-int/lit8 v8, v8, 0x1

    goto :goto_0
.end method
