.class public Lorg/apache/lucene/search/FuzzyQuery;
.super Lorg/apache/lucene/search/MultiTermQuery;
.source "FuzzyQuery.java"


# static fields
.field public static final defaultMaxEdits:I = 0x2

.field public static final defaultMaxExpansions:I = 0x32

.field public static final defaultMinSimilarity:F = 2.0f
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final defaultPrefixLength:I = 0x0

.field public static final defaultTranspositions:Z = true


# instance fields
.field private final maxEdits:I

.field private final maxExpansions:I

.field private final prefixLength:I

.field private final term:Lorg/apache/lucene/index/Term;

.field private final transpositions:Z


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/Term;)V
    .locals 1
    .param p1, "term"    # Lorg/apache/lucene/index/Term;

    .prologue
    .line 116
    const/4 v0, 0x2

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/search/FuzzyQuery;-><init>(Lorg/apache/lucene/index/Term;I)V

    .line 117
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/index/Term;I)V
    .locals 1
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .param p2, "maxEdits"    # I

    .prologue
    .line 109
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/search/FuzzyQuery;-><init>(Lorg/apache/lucene/index/Term;II)V

    .line 110
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/index/Term;II)V
    .locals 6
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .param p2, "maxEdits"    # I
    .param p3, "prefixLength"    # I

    .prologue
    .line 102
    const/16 v4, 0x32

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/search/FuzzyQuery;-><init>(Lorg/apache/lucene/index/Term;IIIZ)V

    .line 103
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/index/Term;IIIZ)V
    .locals 2
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .param p2, "maxEdits"    # I
    .param p3, "prefixLength"    # I
    .param p4, "maxExpansions"    # I
    .param p5, "transpositions"    # Z

    .prologue
    .line 77
    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/lucene/search/MultiTermQuery;-><init>(Ljava/lang/String;)V

    .line 79
    if-ltz p2, :cond_0

    const/4 v0, 0x2

    if-le p2, v0, :cond_1

    .line 80
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "maxEdits must be between 0 and 2"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 82
    :cond_1
    if-gez p3, :cond_2

    .line 83
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "prefixLength cannot be negative."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 85
    :cond_2
    if-gez p4, :cond_3

    .line 86
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "maxExpansions cannot be negative."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 89
    :cond_3
    iput-object p1, p0, Lorg/apache/lucene/search/FuzzyQuery;->term:Lorg/apache/lucene/index/Term;

    .line 90
    iput p2, p0, Lorg/apache/lucene/search/FuzzyQuery;->maxEdits:I

    .line 91
    iput p3, p0, Lorg/apache/lucene/search/FuzzyQuery;->prefixLength:I

    .line 92
    iput-boolean p5, p0, Lorg/apache/lucene/search/FuzzyQuery;->transpositions:Z

    .line 93
    iput p4, p0, Lorg/apache/lucene/search/FuzzyQuery;->maxExpansions:I

    .line 94
    new-instance v0, Lorg/apache/lucene/search/MultiTermQuery$TopTermsScoringBooleanQueryRewrite;

    invoke-direct {v0, p4}, Lorg/apache/lucene/search/MultiTermQuery$TopTermsScoringBooleanQueryRewrite;-><init>(I)V

    invoke-virtual {p0, v0}, Lorg/apache/lucene/search/FuzzyQuery;->setRewriteMethod(Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;)V

    .line 95
    return-void
.end method

.method public static floatToEdits(FI)I
    .locals 4
    .param p0, "minimumSimilarity"    # F
    .param p1, "termLen"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 218
    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, p0, v0

    if-ltz v0, :cond_0

    .line 219
    const/high16 v0, 0x40000000    # 2.0f

    invoke-static {p0, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    float-to-int v0, v0

    .line 223
    :goto_0
    return v0

    .line 220
    :cond_0
    const/4 v0, 0x0

    cmpl-float v0, p0, v0

    if-nez v0, :cond_1

    .line 221
    const/4 v0, 0x0

    goto :goto_0

    .line 223
    :cond_1
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    float-to-double v2, p0

    sub-double/2addr v0, v2

    int-to-double v2, p1

    mul-double/2addr v0, v2

    double-to-int v0, v0

    .line 224
    const/4 v1, 0x2

    .line 223
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 178
    if-ne p0, p1, :cond_1

    .line 198
    :cond_0
    :goto_0
    return v1

    .line 180
    :cond_1
    invoke-super {p0, p1}, Lorg/apache/lucene/search/MultiTermQuery;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    move v1, v2

    .line 181
    goto :goto_0

    .line 182
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 183
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 184
    check-cast v0, Lorg/apache/lucene/search/FuzzyQuery;

    .line 185
    .local v0, "other":Lorg/apache/lucene/search/FuzzyQuery;
    iget v3, p0, Lorg/apache/lucene/search/FuzzyQuery;->maxEdits:I

    iget v4, v0, Lorg/apache/lucene/search/FuzzyQuery;->maxEdits:I

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 186
    goto :goto_0

    .line 187
    :cond_4
    iget v3, p0, Lorg/apache/lucene/search/FuzzyQuery;->prefixLength:I

    iget v4, v0, Lorg/apache/lucene/search/FuzzyQuery;->prefixLength:I

    if-eq v3, v4, :cond_5

    move v1, v2

    .line 188
    goto :goto_0

    .line 189
    :cond_5
    iget v3, p0, Lorg/apache/lucene/search/FuzzyQuery;->maxExpansions:I

    iget v4, v0, Lorg/apache/lucene/search/FuzzyQuery;->maxExpansions:I

    if-eq v3, v4, :cond_6

    move v1, v2

    .line 190
    goto :goto_0

    .line 191
    :cond_6
    iget-boolean v3, p0, Lorg/apache/lucene/search/FuzzyQuery;->transpositions:Z

    iget-boolean v4, v0, Lorg/apache/lucene/search/FuzzyQuery;->transpositions:Z

    if-eq v3, v4, :cond_7

    move v1, v2

    .line 192
    goto :goto_0

    .line 193
    :cond_7
    iget-object v3, p0, Lorg/apache/lucene/search/FuzzyQuery;->term:Lorg/apache/lucene/index/Term;

    if-nez v3, :cond_8

    .line 194
    iget-object v3, v0, Lorg/apache/lucene/search/FuzzyQuery;->term:Lorg/apache/lucene/index/Term;

    if-eqz v3, :cond_0

    move v1, v2

    .line 195
    goto :goto_0

    .line 196
    :cond_8
    iget-object v3, p0, Lorg/apache/lucene/search/FuzzyQuery;->term:Lorg/apache/lucene/index/Term;

    iget-object v4, v0, Lorg/apache/lucene/search/FuzzyQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/index/Term;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 197
    goto :goto_0
.end method

.method public getMaxEdits()I
    .locals 1

    .prologue
    .line 123
    iget v0, p0, Lorg/apache/lucene/search/FuzzyQuery;->maxEdits:I

    return v0
.end method

.method public getPrefixLength()I
    .locals 1

    .prologue
    .line 132
    iget v0, p0, Lorg/apache/lucene/search/FuzzyQuery;->prefixLength:I

    return v0
.end method

.method public getTerm()Lorg/apache/lucene/index/Term;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lorg/apache/lucene/search/FuzzyQuery;->term:Lorg/apache/lucene/index/Term;

    return-object v0
.end method

.method protected getTermsEnum(Lorg/apache/lucene/index/Terms;Lorg/apache/lucene/util/AttributeSource;)Lorg/apache/lucene/index/TermsEnum;
    .locals 7
    .param p1, "terms"    # Lorg/apache/lucene/index/Terms;
    .param p2, "atts"    # Lorg/apache/lucene/util/AttributeSource;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 137
    iget v0, p0, Lorg/apache/lucene/search/FuzzyQuery;->maxEdits:I

    if-eqz v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/search/FuzzyQuery;->prefixLength:I

    iget-object v1, p0, Lorg/apache/lucene/search/FuzzyQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v1}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lt v0, v1, :cond_1

    .line 138
    :cond_0
    new-instance v0, Lorg/apache/lucene/index/SingleTermsEnum;

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/search/FuzzyQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v2}, Lorg/apache/lucene/index/Term;->bytes()Lorg/apache/lucene/util/BytesRef;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/index/SingleTermsEnum;-><init>(Lorg/apache/lucene/index/TermsEnum;Lorg/apache/lucene/util/BytesRef;)V

    .line 140
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lorg/apache/lucene/search/FuzzyTermsEnum;

    invoke-virtual {p0}, Lorg/apache/lucene/search/FuzzyQuery;->getTerm()Lorg/apache/lucene/index/Term;

    move-result-object v3

    iget v1, p0, Lorg/apache/lucene/search/FuzzyQuery;->maxEdits:I

    int-to-float v4, v1

    iget v5, p0, Lorg/apache/lucene/search/FuzzyQuery;->prefixLength:I

    iget-boolean v6, p0, Lorg/apache/lucene/search/FuzzyQuery;->transpositions:Z

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/search/FuzzyTermsEnum;-><init>(Lorg/apache/lucene/index/Terms;Lorg/apache/lucene/util/AttributeSource;Lorg/apache/lucene/index/Term;FIZ)V

    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 166
    const/16 v0, 0x1f

    .line 167
    .local v0, "prime":I
    invoke-super {p0}, Lorg/apache/lucene/search/MultiTermQuery;->hashCode()I

    move-result v1

    .line 168
    .local v1, "result":I
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lorg/apache/lucene/search/FuzzyQuery;->maxEdits:I

    add-int v1, v2, v4

    .line 169
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lorg/apache/lucene/search/FuzzyQuery;->prefixLength:I

    add-int v1, v2, v4

    .line 170
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lorg/apache/lucene/search/FuzzyQuery;->maxExpansions:I

    add-int v1, v2, v4

    .line 171
    mul-int/lit8 v4, v1, 0x1f

    iget-boolean v2, p0, Lorg/apache/lucene/search/FuzzyQuery;->transpositions:Z

    if-eqz v2, :cond_0

    move v2, v3

    :goto_0
    add-int v1, v4, v2

    .line 172
    mul-int/lit8 v2, v1, 0x1f

    iget-object v4, p0, Lorg/apache/lucene/search/FuzzyQuery;->term:Lorg/apache/lucene/index/Term;

    if-nez v4, :cond_1

    :goto_1
    add-int v1, v2, v3

    .line 173
    return v1

    .line 171
    :cond_0
    const/4 v2, 0x1

    goto :goto_0

    .line 172
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/search/FuzzyQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v3}, Lorg/apache/lucene/index/Term;->hashCode()I

    move-result v3

    goto :goto_1
.end method

.method public toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 152
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 153
    .local v0, "buffer":Ljava/lang/StringBuilder;
    iget-object v1, p0, Lorg/apache/lucene/search/FuzzyQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 154
    iget-object v1, p0, Lorg/apache/lucene/search/FuzzyQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 155
    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 157
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/FuzzyQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v1}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 158
    const/16 v1, 0x7e

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 159
    iget v1, p0, Lorg/apache/lucene/search/FuzzyQuery;->maxEdits:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 160
    invoke-virtual {p0}, Lorg/apache/lucene/search/FuzzyQuery;->getBoost()F

    move-result v1

    invoke-static {v1}, Lorg/apache/lucene/util/ToStringUtils;->boost(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 161
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
