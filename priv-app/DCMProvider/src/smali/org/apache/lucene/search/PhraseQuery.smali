.class public Lorg/apache/lucene/search/PhraseQuery;
.super Lorg/apache/lucene/search/Query;
.source "PhraseQuery.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;,
        Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;
    }
.end annotation


# instance fields
.field private field:Ljava/lang/String;

.field private maxPosition:I

.field private positions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private slop:I

.field private terms:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/lucene/index/Term;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 56
    invoke-direct {p0}, Lorg/apache/lucene/search/Query;-><init>()V

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/search/PhraseQuery;->terms:Ljava/util/ArrayList;

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/search/PhraseQuery;->positions:Ljava/util/ArrayList;

    .line 52
    iput v1, p0, Lorg/apache/lucene/search/PhraseQuery;->maxPosition:I

    .line 53
    iput v1, p0, Lorg/apache/lucene/search/PhraseQuery;->slop:I

    .line 56
    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/search/PhraseQuery;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lorg/apache/lucene/search/PhraseQuery;->terms:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1(Lorg/apache/lucene/search/PhraseQuery;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lorg/apache/lucene/search/PhraseQuery;->field:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2(Lorg/apache/lucene/search/PhraseQuery;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lorg/apache/lucene/search/PhraseQuery;->positions:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$3(Lorg/apache/lucene/search/PhraseQuery;)I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lorg/apache/lucene/search/PhraseQuery;->slop:I

    return v0
.end method


# virtual methods
.method public add(Lorg/apache/lucene/index/Term;)V
    .locals 3
    .param p1, "term"    # Lorg/apache/lucene/index/Term;

    .prologue
    .line 81
    const/4 v0, 0x0

    .line 82
    .local v0, "position":I
    iget-object v1, p0, Lorg/apache/lucene/search/PhraseQuery;->positions:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 83
    iget-object v1, p0, Lorg/apache/lucene/search/PhraseQuery;->positions:Ljava/util/ArrayList;

    iget-object v2, p0, Lorg/apache/lucene/search/PhraseQuery;->positions:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v0, v1, 0x1

    .line 85
    :cond_0
    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/search/PhraseQuery;->add(Lorg/apache/lucene/index/Term;I)V

    .line 86
    return-void
.end method

.method public add(Lorg/apache/lucene/index/Term;I)V
    .locals 3
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .param p2, "position"    # I

    .prologue
    .line 96
    iget-object v0, p0, Lorg/apache/lucene/search/PhraseQuery;->terms:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 97
    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/PhraseQuery;->field:Ljava/lang/String;

    .line 102
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/PhraseQuery;->terms:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 103
    iget-object v0, p0, Lorg/apache/lucene/search/PhraseQuery;->positions:Ljava/util/ArrayList;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 104
    iget v0, p0, Lorg/apache/lucene/search/PhraseQuery;->maxPosition:I

    if-le p2, v0, :cond_1

    iput p2, p0, Lorg/apache/lucene/search/PhraseQuery;->maxPosition:I

    .line 105
    :cond_1
    return-void

    .line 98
    :cond_2
    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/search/PhraseQuery;->field:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 99
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "All phrase terms must be in the same field: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public createWeight(Lorg/apache/lucene/search/IndexSearcher;)Lorg/apache/lucene/search/Weight;
    .locals 1
    .param p1, "searcher"    # Lorg/apache/lucene/search/IndexSearcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 326
    new-instance v0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;-><init>(Lorg/apache/lucene/search/PhraseQuery;Lorg/apache/lucene/search/IndexSearcher;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 384
    instance-of v2, p1, Lorg/apache/lucene/search/PhraseQuery;

    if-nez v2, :cond_1

    .line 387
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 386
    check-cast v0, Lorg/apache/lucene/search/PhraseQuery;

    .line 387
    .local v0, "other":Lorg/apache/lucene/search/PhraseQuery;
    invoke-virtual {p0}, Lorg/apache/lucene/search/PhraseQuery;->getBoost()F

    move-result v2

    invoke-virtual {v0}, Lorg/apache/lucene/search/PhraseQuery;->getBoost()F

    move-result v3

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    .line 388
    iget v2, p0, Lorg/apache/lucene/search/PhraseQuery;->slop:I

    iget v3, v0, Lorg/apache/lucene/search/PhraseQuery;->slop:I

    if-ne v2, v3, :cond_0

    .line 389
    iget-object v2, p0, Lorg/apache/lucene/search/PhraseQuery;->terms:Ljava/util/ArrayList;

    iget-object v3, v0, Lorg/apache/lucene/search/PhraseQuery;->terms:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 390
    iget-object v2, p0, Lorg/apache/lucene/search/PhraseQuery;->positions:Ljava/util/ArrayList;

    iget-object v3, v0, Lorg/apache/lucene/search/PhraseQuery;->positions:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 387
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public extractTerms(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lorg/apache/lucene/index/Term;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 334
    .local p1, "queryTerms":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/index/Term;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/PhraseQuery;->terms:Ljava/util/ArrayList;

    invoke-interface {p1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 335
    return-void
.end method

.method public getPositions()[I
    .locals 3

    .prologue
    .line 116
    iget-object v2, p0, Lorg/apache/lucene/search/PhraseQuery;->positions:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v1, v2, [I

    .line 117
    .local v1, "result":[I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/search/PhraseQuery;->positions:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v0, v2, :cond_0

    .line 119
    return-object v1

    .line 118
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/search/PhraseQuery;->positions:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    aput v2, v1, v0

    .line 117
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getSlop()I
    .locals 1

    .prologue
    .line 74
    iget v0, p0, Lorg/apache/lucene/search/PhraseQuery;->slop:I

    return v0
.end method

.method public getTerms()[Lorg/apache/lucene/index/Term;
    .locals 2

    .prologue
    .line 109
    iget-object v0, p0, Lorg/apache/lucene/search/PhraseQuery;->terms:Ljava/util/ArrayList;

    const/4 v1, 0x0

    new-array v1, v1, [Lorg/apache/lucene/index/Term;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/lucene/index/Term;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 396
    invoke-virtual {p0}, Lorg/apache/lucene/search/PhraseQuery;->getBoost()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    .line 397
    iget v1, p0, Lorg/apache/lucene/search/PhraseQuery;->slop:I

    .line 396
    xor-int/2addr v0, v1

    .line 398
    iget-object v1, p0, Lorg/apache/lucene/search/PhraseQuery;->terms:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->hashCode()I

    move-result v1

    .line 396
    xor-int/2addr v0, v1

    .line 399
    iget-object v1, p0, Lorg/apache/lucene/search/PhraseQuery;->positions:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->hashCode()I

    move-result v1

    .line 396
    xor-int/2addr v0, v1

    return v0
.end method

.method public rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;
    .locals 4
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 124
    iget-object v2, p0, Lorg/apache/lucene/search/PhraseQuery;->terms:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 125
    new-instance v0, Lorg/apache/lucene/search/BooleanQuery;

    invoke-direct {v0}, Lorg/apache/lucene/search/BooleanQuery;-><init>()V

    .line 126
    .local v0, "bq":Lorg/apache/lucene/search/BooleanQuery;
    invoke-virtual {p0}, Lorg/apache/lucene/search/PhraseQuery;->getBoost()F

    move-result v2

    invoke-virtual {v0, v2}, Lorg/apache/lucene/search/BooleanQuery;->setBoost(F)V

    .line 133
    .end local v0    # "bq":Lorg/apache/lucene/search/BooleanQuery;
    :goto_0
    return-object v0

    .line 128
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/search/PhraseQuery;->terms:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 129
    new-instance v1, Lorg/apache/lucene/search/TermQuery;

    iget-object v2, p0, Lorg/apache/lucene/search/PhraseQuery;->terms:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/index/Term;

    invoke-direct {v1, v2}, Lorg/apache/lucene/search/TermQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    .line 130
    .local v1, "tq":Lorg/apache/lucene/search/TermQuery;
    invoke-virtual {p0}, Lorg/apache/lucene/search/PhraseQuery;->getBoost()F

    move-result v2

    invoke-virtual {v1, v2}, Lorg/apache/lucene/search/TermQuery;->setBoost(F)V

    move-object v0, v1

    .line 131
    goto :goto_0

    .line 133
    .end local v1    # "tq":Lorg/apache/lucene/search/TermQuery;
    :cond_1
    invoke-super {p0, p1}, Lorg/apache/lucene/search/Query;->rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;

    move-result-object v0

    goto :goto_0
.end method

.method public setSlop(I)V
    .locals 0
    .param p1, "s"    # I

    .prologue
    .line 72
    iput p1, p0, Lorg/apache/lucene/search/PhraseQuery;->slop:I

    return-void
.end method

.method public toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "f"    # Ljava/lang/String;

    .prologue
    .line 340
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 341
    .local v0, "buffer":Ljava/lang/StringBuilder;
    iget-object v5, p0, Lorg/apache/lucene/search/PhraseQuery;->field:Ljava/lang/String;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lorg/apache/lucene/search/PhraseQuery;->field:Ljava/lang/String;

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 342
    iget-object v5, p0, Lorg/apache/lucene/search/PhraseQuery;->field:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 343
    const-string v5, ":"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 346
    :cond_0
    const-string v5, "\""

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 347
    iget v5, p0, Lorg/apache/lucene/search/PhraseQuery;->maxPosition:I

    add-int/lit8 v5, v5, 0x1

    new-array v2, v5, [Ljava/lang/String;

    .line 348
    .local v2, "pieces":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v5, p0, Lorg/apache/lucene/search/PhraseQuery;->terms:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lt v1, v5, :cond_2

    .line 358
    const/4 v1, 0x0

    :goto_1
    array-length v5, v2

    if-lt v1, v5, :cond_4

    .line 369
    const-string v5, "\""

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 371
    iget v5, p0, Lorg/apache/lucene/search/PhraseQuery;->slop:I

    if-eqz v5, :cond_1

    .line 372
    const-string/jumbo v5, "~"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 373
    iget v5, p0, Lorg/apache/lucene/search/PhraseQuery;->slop:I

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 376
    :cond_1
    invoke-virtual {p0}, Lorg/apache/lucene/search/PhraseQuery;->getBoost()F

    move-result v5

    invoke-static {v5}, Lorg/apache/lucene/util/ToStringUtils;->boost(F)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 378
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5

    .line 349
    :cond_2
    iget-object v5, p0, Lorg/apache/lucene/search/PhraseQuery;->positions:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 350
    .local v3, "pos":I
    aget-object v4, v2, v3

    .line 351
    .local v4, "s":Ljava/lang/String;
    if-nez v4, :cond_3

    .line 352
    iget-object v5, p0, Lorg/apache/lucene/search/PhraseQuery;->terms:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/index/Term;

    invoke-virtual {v5}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v4

    .line 356
    :goto_2
    aput-object v4, v2, v3

    .line 348
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 354
    :cond_3
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v6, "|"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v5, p0, Lorg/apache/lucene/search/PhraseQuery;->terms:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/index/Term;

    invoke-virtual {v5}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_2

    .line 359
    .end local v3    # "pos":I
    .end local v4    # "s":Ljava/lang/String;
    :cond_4
    if-lez v1, :cond_5

    .line 360
    const/16 v5, 0x20

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 362
    :cond_5
    aget-object v4, v2, v1

    .line 363
    .restart local v4    # "s":Ljava/lang/String;
    if-nez v4, :cond_6

    .line 364
    const/16 v5, 0x3f

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 358
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 366
    :cond_6
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3
.end method
