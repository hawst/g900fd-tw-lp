.class final Lorg/apache/lucene/search/IndexSearcher$SearcherCallableNoSort;
.super Ljava/lang/Object;
.source "IndexSearcher.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/IndexSearcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "SearcherCallableNoSort"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lorg/apache/lucene/search/TopDocs;",
        ">;"
    }
.end annotation


# instance fields
.field private final after:Lorg/apache/lucene/search/ScoreDoc;

.field private final hq:Lorg/apache/lucene/search/HitQueue;

.field private final lock:Ljava/util/concurrent/locks/Lock;

.field private final nDocs:I

.field private final searcher:Lorg/apache/lucene/search/IndexSearcher;

.field private final slice:Lorg/apache/lucene/search/IndexSearcher$LeafSlice;

.field private final weight:Lorg/apache/lucene/search/Weight;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/locks/Lock;Lorg/apache/lucene/search/IndexSearcher;Lorg/apache/lucene/search/IndexSearcher$LeafSlice;Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/ScoreDoc;ILorg/apache/lucene/search/HitQueue;)V
    .locals 0
    .param p1, "lock"    # Ljava/util/concurrent/locks/Lock;
    .param p2, "searcher"    # Lorg/apache/lucene/search/IndexSearcher;
    .param p3, "slice"    # Lorg/apache/lucene/search/IndexSearcher$LeafSlice;
    .param p4, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p5, "after"    # Lorg/apache/lucene/search/ScoreDoc;
    .param p6, "nDocs"    # I
    .param p7, "hq"    # Lorg/apache/lucene/search/HitQueue;

    .prologue
    .line 707
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 709
    iput-object p1, p0, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableNoSort;->lock:Ljava/util/concurrent/locks/Lock;

    .line 710
    iput-object p2, p0, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableNoSort;->searcher:Lorg/apache/lucene/search/IndexSearcher;

    .line 711
    iput-object p4, p0, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableNoSort;->weight:Lorg/apache/lucene/search/Weight;

    .line 712
    iput-object p5, p0, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableNoSort;->after:Lorg/apache/lucene/search/ScoreDoc;

    .line 713
    iput p6, p0, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableNoSort;->nDocs:I

    .line 714
    iput-object p7, p0, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableNoSort;->hq:Lorg/apache/lucene/search/HitQueue;

    .line 715
    iput-object p3, p0, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableNoSort;->slice:Lorg/apache/lucene/search/IndexSearcher$LeafSlice;

    .line 716
    return-void
.end method


# virtual methods
.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableNoSort;->call()Lorg/apache/lucene/search/TopDocs;

    move-result-object v0

    return-object v0
.end method

.method public call()Lorg/apache/lucene/search/TopDocs;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 720
    iget-object v4, p0, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableNoSort;->searcher:Lorg/apache/lucene/search/IndexSearcher;

    iget-object v5, p0, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableNoSort;->slice:Lorg/apache/lucene/search/IndexSearcher$LeafSlice;

    iget-object v5, v5, Lorg/apache/lucene/search/IndexSearcher$LeafSlice;->leaves:[Lorg/apache/lucene/index/AtomicReaderContext;

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    iget-object v6, p0, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableNoSort;->weight:Lorg/apache/lucene/search/Weight;

    iget-object v7, p0, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableNoSort;->after:Lorg/apache/lucene/search/ScoreDoc;

    iget v8, p0, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableNoSort;->nDocs:I

    invoke-virtual {v4, v5, v6, v7, v8}, Lorg/apache/lucene/search/IndexSearcher;->search(Ljava/util/List;Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/ScoreDoc;I)Lorg/apache/lucene/search/TopDocs;

    move-result-object v0

    .line 721
    .local v0, "docs":Lorg/apache/lucene/search/TopDocs;
    iget-object v3, v0, Lorg/apache/lucene/search/TopDocs;->scoreDocs:[Lorg/apache/lucene/search/ScoreDoc;

    .line 723
    .local v3, "scoreDocs":[Lorg/apache/lucene/search/ScoreDoc;
    iget-object v4, p0, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableNoSort;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v4}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 725
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_0
    :try_start_0
    array-length v4, v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-lt v1, v4, :cond_1

    .line 732
    :cond_0
    iget-object v4, p0, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableNoSort;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v4}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 734
    return-object v0

    .line 726
    :cond_1
    :try_start_1
    aget-object v2, v3, v1

    .line 727
    .local v2, "scoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    iget-object v4, p0, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableNoSort;->hq:Lorg/apache/lucene/search/HitQueue;

    invoke-virtual {v4, v2}, Lorg/apache/lucene/search/HitQueue;->insertWithOverflow(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v4

    if-eq v2, v4, :cond_0

    .line 725
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 731
    .end local v2    # "scoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    :catchall_0
    move-exception v4

    .line 732
    iget-object v5, p0, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableNoSort;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v5}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 733
    throw v4
.end method
