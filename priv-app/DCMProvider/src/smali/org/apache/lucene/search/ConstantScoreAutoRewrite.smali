.class Lorg/apache/lucene/search/ConstantScoreAutoRewrite;
.super Lorg/apache/lucene/search/TermCollectingRewrite;
.source "ConstantScoreAutoRewrite.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;,
        Lorg/apache/lucene/search/ConstantScoreAutoRewrite$TermStateByteStart;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/TermCollectingRewrite",
        "<",
        "Lorg/apache/lucene/search/BooleanQuery;",
        ">;"
    }
.end annotation


# static fields
.field public static DEFAULT_DOC_COUNT_PERCENT:D

.field public static DEFAULT_TERM_COUNT_CUTOFF:I


# instance fields
.field private docCountPercent:D

.field private termCountCutoff:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 39
    const/16 v0, 0x15e

    sput v0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;->DEFAULT_TERM_COUNT_CUTOFF:I

    .line 43
    const-wide v0, 0x3fb999999999999aL    # 0.1

    sput-wide v0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;->DEFAULT_DOC_COUNT_PERCENT:D

    return-void
.end method

.method constructor <init>()V
    .locals 2

    .prologue
    .line 34
    invoke-direct {p0}, Lorg/apache/lucene/search/TermCollectingRewrite;-><init>()V

    .line 45
    sget v0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;->DEFAULT_TERM_COUNT_CUTOFF:I

    iput v0, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;->termCountCutoff:I

    .line 46
    sget-wide v0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;->DEFAULT_DOC_COUNT_PERCENT:D

    iput-wide v0, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;->docCountPercent:D

    .line 34
    return-void
.end method


# virtual methods
.method protected addClause(Lorg/apache/lucene/search/BooleanQuery;Lorg/apache/lucene/index/Term;IFLorg/apache/lucene/index/TermContext;)V
    .locals 2
    .param p1, "topLevel"    # Lorg/apache/lucene/search/BooleanQuery;
    .param p2, "term"    # Lorg/apache/lucene/index/Term;
    .param p3, "docFreq"    # I
    .param p4, "boost"    # F
    .param p5, "states"    # Lorg/apache/lucene/index/TermContext;

    .prologue
    .line 81
    new-instance v0, Lorg/apache/lucene/search/TermQuery;

    invoke-direct {v0, p2, p5}, Lorg/apache/lucene/search/TermQuery;-><init>(Lorg/apache/lucene/index/Term;Lorg/apache/lucene/index/TermContext;)V

    sget-object v1, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {p1, v0, v1}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 82
    return-void
.end method

.method protected bridge synthetic addClause(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/index/Term;IFLorg/apache/lucene/index/TermContext;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    move-object v1, p1

    check-cast v1, Lorg/apache/lucene/search/BooleanQuery;

    move-object v0, p0

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;->addClause(Lorg/apache/lucene/search/BooleanQuery;Lorg/apache/lucene/index/Term;IFLorg/apache/lucene/index/TermContext;)V

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 166
    if-ne p0, p1, :cond_1

    .line 182
    :cond_0
    :goto_0
    return v1

    .line 168
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 169
    goto :goto_0

    .line 170
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 171
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 173
    check-cast v0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;

    .line 174
    .local v0, "other":Lorg/apache/lucene/search/ConstantScoreAutoRewrite;
    iget v3, v0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;->termCountCutoff:I

    iget v4, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;->termCountCutoff:I

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 175
    goto :goto_0

    .line 178
    :cond_4
    iget-wide v4, v0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;->docCountPercent:D

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    iget-wide v6, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;->docCountPercent:D

    invoke-static {v6, v7}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    move v1, v2

    .line 179
    goto :goto_0
.end method

.method public getDocCountPercent()D
    .locals 2

    .prologue
    .line 71
    iget-wide v0, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;->docCountPercent:D

    return-wide v0
.end method

.method public getTermCountCutoff()I
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;->termCountCutoff:I

    return v0
.end method

.method protected getTopLevelQuery()Lorg/apache/lucene/search/BooleanQuery;
    .locals 2

    .prologue
    .line 76
    new-instance v0, Lorg/apache/lucene/search/BooleanQuery;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lorg/apache/lucene/search/BooleanQuery;-><init>(Z)V

    return-object v0
.end method

.method protected bridge synthetic getTopLevelQuery()Lorg/apache/lucene/search/Query;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;->getTopLevelQuery()Lorg/apache/lucene/search/BooleanQuery;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    .line 160
    const/16 v0, 0x4ff

    .line 161
    .local v0, "prime":I
    iget v1, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;->termCountCutoff:I

    mul-int/lit16 v1, v1, 0x4ff

    int-to-long v2, v1

    iget-wide v4, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;->docCountPercent:D

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    add-long/2addr v2, v4

    long-to-int v1, v2

    return v1
.end method

.method public rewrite(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/MultiTermQuery;)Lorg/apache/lucene/search/Query;
    .locals 19
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "query"    # Lorg/apache/lucene/search/MultiTermQuery;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 91
    move-object/from16 v0, p0

    iget-wide v6, v0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;->docCountPercent:D

    const-wide/high16 v8, 0x4059000000000000L    # 100.0

    div-double/2addr v6, v8

    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v4

    int-to-double v8, v4

    mul-double/2addr v6, v8

    double-to-int v11, v6

    .line 92
    .local v11, "docCountCutoff":I
    invoke-static {}, Lorg/apache/lucene/search/BooleanQuery;->getMaxClauseCount()I

    move-result v4

    move-object/from16 v0, p0

    iget v6, v0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;->termCountCutoff:I

    invoke-static {v4, v6}, Ljava/lang/Math;->min(II)I

    move-result v18

    .line 94
    .local v18, "termCountLimit":I
    new-instance v10, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;

    move/from16 v0, v18

    invoke-direct {v10, v11, v0}, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;-><init>(II)V

    .line 95
    .local v10, "col":Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-virtual {v0, v1, v2, v10}, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;->collectTerms(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/MultiTermQuery;Lorg/apache/lucene/search/TermCollectingRewrite$TermCollector;)V

    .line 96
    iget-object v4, v10, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;->pendingTerms:Lorg/apache/lucene/util/BytesRefHash;

    invoke-virtual {v4}, Lorg/apache/lucene/util/BytesRefHash;->size()I

    move-result v16

    .line 97
    .local v16, "size":I
    iget-boolean v4, v10, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;->hasCutOff:Z

    if-eqz v4, :cond_0

    .line 98
    sget-object v4, Lorg/apache/lucene/search/MultiTermQuery;->CONSTANT_SCORE_FILTER_REWRITE:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v4, v0, v1}, Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;->rewrite(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/MultiTermQuery;)Lorg/apache/lucene/search/Query;

    move-result-object v15

    .line 114
    :goto_0
    return-object v15

    .line 99
    :cond_0
    if-nez v16, :cond_1

    .line 100
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;->getTopLevelQuery()Lorg/apache/lucene/search/BooleanQuery;

    move-result-object v15

    goto :goto_0

    .line 102
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;->getTopLevelQuery()Lorg/apache/lucene/search/BooleanQuery;

    move-result-object v5

    .line 103
    .local v5, "bq":Lorg/apache/lucene/search/BooleanQuery;
    iget-object v13, v10, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;->pendingTerms:Lorg/apache/lucene/util/BytesRefHash;

    .line 104
    .local v13, "pendingTerms":Lorg/apache/lucene/util/BytesRefHash;
    iget-object v4, v10, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;->termsEnum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v4}, Lorg/apache/lucene/index/TermsEnum;->getComparator()Ljava/util/Comparator;

    move-result-object v4

    invoke-virtual {v13, v4}, Lorg/apache/lucene/util/BytesRefHash;->sort(Ljava/util/Comparator;)[I

    move-result-object v17

    .line 105
    .local v17, "sort":[I
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_1
    move/from16 v0, v16

    if-lt v12, v0, :cond_2

    .line 112
    new-instance v15, Lorg/apache/lucene/search/ConstantScoreQuery;

    invoke-direct {v15, v5}, Lorg/apache/lucene/search/ConstantScoreQuery;-><init>(Lorg/apache/lucene/search/Query;)V

    .line 113
    .local v15, "result":Lorg/apache/lucene/search/Query;
    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/search/MultiTermQuery;->getBoost()F

    move-result v4

    invoke-virtual {v15, v4}, Lorg/apache/lucene/search/Query;->setBoost(F)V

    goto :goto_0

    .line 106
    .end local v15    # "result":Lorg/apache/lucene/search/Query;
    :cond_2
    aget v14, v17, v12

    .line 109
    .local v14, "pos":I
    new-instance v6, Lorg/apache/lucene/index/Term;

    move-object/from16 v0, p2

    iget-object v4, v0, Lorg/apache/lucene/search/MultiTermQuery;->field:Ljava/lang/String;

    new-instance v7, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v7}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    invoke-virtual {v13, v14, v7}, Lorg/apache/lucene/util/BytesRefHash;->get(ILorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/BytesRef;

    move-result-object v7

    invoke-direct {v6, v4, v7}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    const/4 v7, 0x1

    const/high16 v8, 0x3f800000    # 1.0f

    iget-object v4, v10, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;->array:Lorg/apache/lucene/search/ConstantScoreAutoRewrite$TermStateByteStart;

    iget-object v4, v4, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$TermStateByteStart;->termState:[Lorg/apache/lucene/index/TermContext;

    aget-object v9, v4, v14

    move-object/from16 v4, p0

    invoke-virtual/range {v4 .. v9}, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;->addClause(Lorg/apache/lucene/search/BooleanQuery;Lorg/apache/lucene/index/Term;IFLorg/apache/lucene/index/TermContext;)V

    .line 105
    add-int/lit8 v12, v12, 0x1

    goto :goto_1
.end method

.method public setDocCountPercent(D)V
    .locals 1
    .param p1, "percent"    # D

    .prologue
    .line 66
    iput-wide p1, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;->docCountPercent:D

    .line 67
    return-void
.end method

.method public setTermCountCutoff(I)V
    .locals 0
    .param p1, "count"    # I

    .prologue
    .line 52
    iput p1, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;->termCountCutoff:I

    .line 53
    return-void
.end method
