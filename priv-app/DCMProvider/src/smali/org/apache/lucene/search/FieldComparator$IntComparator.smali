.class public final Lorg/apache/lucene/search/FieldComparator$IntComparator;
.super Lorg/apache/lucene/search/FieldComparator$NumericComparator;
.source "FieldComparator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FieldComparator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "IntComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/FieldComparator$NumericComparator",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field private bottom:I

.field private currentReaderValues:Lorg/apache/lucene/search/FieldCache$Ints;

.field private final parser:Lorg/apache/lucene/search/FieldCache$IntParser;

.field private final values:[I


# direct methods
.method constructor <init>(ILjava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Ljava/lang/Integer;)V
    .locals 1
    .param p1, "numHits"    # I
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "parser"    # Lorg/apache/lucene/search/FieldCache$Parser;
    .param p4, "missingValue"    # Ljava/lang/Integer;

    .prologue
    .line 525
    invoke-direct {p0, p2, p4}, Lorg/apache/lucene/search/FieldComparator$NumericComparator;-><init>(Ljava/lang/String;Ljava/lang/Number;)V

    .line 526
    new-array v0, p1, [I

    iput-object v0, p0, Lorg/apache/lucene/search/FieldComparator$IntComparator;->values:[I

    .line 527
    check-cast p3, Lorg/apache/lucene/search/FieldCache$IntParser;

    .end local p3    # "parser":Lorg/apache/lucene/search/FieldCache$Parser;
    iput-object p3, p0, Lorg/apache/lucene/search/FieldComparator$IntComparator;->parser:Lorg/apache/lucene/search/FieldCache$IntParser;

    .line 528
    return-void
.end method


# virtual methods
.method public compare(II)I
    .locals 3
    .param p1, "slot1"    # I
    .param p2, "slot2"    # I

    .prologue
    .line 536
    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$IntComparator;->values:[I

    aget v0, v2, p1

    .line 537
    .local v0, "v1":I
    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$IntComparator;->values:[I

    aget v1, v2, p2

    .line 538
    .local v1, "v2":I
    if-le v0, v1, :cond_0

    .line 539
    const/4 v2, 0x1

    .line 543
    :goto_0
    return v2

    .line 540
    :cond_0
    if-ge v0, v1, :cond_1

    .line 541
    const/4 v2, -0x1

    goto :goto_0

    .line 543
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public compareBottom(I)I
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 553
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$IntComparator;->currentReaderValues:Lorg/apache/lucene/search/FieldCache$Ints;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/FieldCache$Ints;->get(I)I

    move-result v0

    .line 556
    .local v0, "v2":I
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$IntComparator;->docsWithField:Lorg/apache/lucene/util/Bits;

    if-eqz v1, :cond_0

    if-nez v0, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$IntComparator;->docsWithField:Lorg/apache/lucene/util/Bits;

    invoke-interface {v1, p1}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 557
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$IntComparator;->missingValue:Ljava/lang/Number;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 560
    :cond_0
    iget v1, p0, Lorg/apache/lucene/search/FieldComparator$IntComparator;->bottom:I

    if-le v1, v0, :cond_1

    .line 561
    const/4 v1, 0x1

    .line 565
    :goto_0
    return v1

    .line 562
    :cond_1
    iget v1, p0, Lorg/apache/lucene/search/FieldComparator$IntComparator;->bottom:I

    if-ge v1, v0, :cond_2

    .line 563
    const/4 v1, -0x1

    goto :goto_0

    .line 565
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public compareDocToValue(ILjava/lang/Integer;)I
    .locals 3
    .param p1, "doc"    # I
    .param p2, "valueObj"    # Ljava/lang/Integer;

    .prologue
    .line 601
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 602
    .local v1, "value":I
    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$IntComparator;->currentReaderValues:Lorg/apache/lucene/search/FieldCache$Ints;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/search/FieldCache$Ints;->get(I)I

    move-result v0

    .line 605
    .local v0, "docValue":I
    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$IntComparator;->docsWithField:Lorg/apache/lucene/util/Bits;

    if-eqz v2, :cond_0

    if-nez v0, :cond_0

    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$IntComparator;->docsWithField:Lorg/apache/lucene/util/Bits;

    invoke-interface {v2, p1}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 606
    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$IntComparator;->missingValue:Ljava/lang/Number;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 608
    :cond_0
    if-ge v0, v1, :cond_1

    .line 609
    const/4 v2, -0x1

    .line 613
    :goto_0
    return v2

    .line 610
    :cond_1
    if-le v0, v1, :cond_2

    .line 611
    const/4 v2, 0x1

    goto :goto_0

    .line 613
    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public bridge synthetic compareDocToValue(ILjava/lang/Object;)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/search/FieldComparator$IntComparator;->compareDocToValue(ILjava/lang/Integer;)I

    move-result v0

    return v0
.end method

.method public copy(II)V
    .locals 2
    .param p1, "slot"    # I
    .param p2, "doc"    # I

    .prologue
    .line 571
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$IntComparator;->currentReaderValues:Lorg/apache/lucene/search/FieldCache$Ints;

    invoke-virtual {v1, p2}, Lorg/apache/lucene/search/FieldCache$Ints;->get(I)I

    move-result v0

    .line 574
    .local v0, "v2":I
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$IntComparator;->docsWithField:Lorg/apache/lucene/util/Bits;

    if-eqz v1, :cond_0

    if-nez v0, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$IntComparator;->docsWithField:Lorg/apache/lucene/util/Bits;

    invoke-interface {v1, p2}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 575
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$IntComparator;->missingValue:Ljava/lang/Number;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 578
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$IntComparator;->values:[I

    aput v0, v1, p1

    .line 579
    return-void
.end method

.method public setBottom(I)V
    .locals 1
    .param p1, "bottom"    # I

    .prologue
    .line 591
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$IntComparator;->values:[I

    aget v0, v0, p1

    iput v0, p0, Lorg/apache/lucene/search/FieldComparator$IntComparator;->bottom:I

    .line 592
    return-void
.end method

.method public setNextReader(Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/search/FieldComparator;
    .locals 5
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/AtomicReaderContext;",
            ")",
            "Lorg/apache/lucene/search/FieldComparator",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 585
    sget-object v1, Lorg/apache/lucene/search/FieldCache;->DEFAULT:Lorg/apache/lucene/search/FieldCache;

    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/search/FieldComparator$IntComparator;->field:Ljava/lang/String;

    iget-object v4, p0, Lorg/apache/lucene/search/FieldComparator$IntComparator;->parser:Lorg/apache/lucene/search/FieldCache$IntParser;

    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$IntComparator;->missingValue:Ljava/lang/Number;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v2, v3, v4, v0}, Lorg/apache/lucene/search/FieldCache;->getInts(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$IntParser;Z)Lorg/apache/lucene/search/FieldCache$Ints;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/FieldComparator$IntComparator;->currentReaderValues:Lorg/apache/lucene/search/FieldCache$Ints;

    .line 586
    invoke-super {p0, p1}, Lorg/apache/lucene/search/FieldComparator$NumericComparator;->setNextReader(Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/search/FieldComparator;

    move-result-object v0

    return-object v0

    .line 585
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public value(I)Ljava/lang/Integer;
    .locals 1
    .param p1, "slot"    # I

    .prologue
    .line 596
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$IntComparator;->values:[I

    aget v0, v0, p1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic value(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/FieldComparator$IntComparator;->value(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method
