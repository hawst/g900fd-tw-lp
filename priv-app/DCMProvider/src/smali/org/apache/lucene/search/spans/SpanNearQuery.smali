.class public Lorg/apache/lucene/search/spans/SpanNearQuery;
.super Lorg/apache/lucene/search/spans/SpanQuery;
.source "SpanNearQuery.java"

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field protected clauses:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/search/spans/SpanQuery;",
            ">;"
        }
    .end annotation
.end field

.field private collectPayloads:Z

.field protected field:Ljava/lang/String;

.field protected inOrder:Z

.field protected slop:I


# direct methods
.method public constructor <init>([Lorg/apache/lucene/search/spans/SpanQuery;IZ)V
    .locals 1
    .param p1, "clauses"    # [Lorg/apache/lucene/search/spans/SpanQuery;
    .param p2, "slop"    # I
    .param p3, "inOrder"    # Z

    .prologue
    .line 58
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lorg/apache/lucene/search/spans/SpanNearQuery;-><init>([Lorg/apache/lucene/search/spans/SpanQuery;IZZ)V

    .line 59
    return-void
.end method

.method public constructor <init>([Lorg/apache/lucene/search/spans/SpanQuery;IZZ)V
    .locals 4
    .param p1, "clauses"    # [Lorg/apache/lucene/search/spans/SpanQuery;
    .param p2, "slop"    # I
    .param p3, "inOrder"    # Z
    .param p4, "collectPayloads"    # Z

    .prologue
    .line 61
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/SpanQuery;-><init>()V

    .line 64
    new-instance v2, Ljava/util/ArrayList;

    array-length v3, p1

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v2, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->clauses:Ljava/util/List;

    .line 65
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, p1

    if-lt v1, v2, :cond_0

    .line 74
    iput-boolean p4, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->collectPayloads:Z

    .line 75
    iput p2, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->slop:I

    .line 76
    iput-boolean p3, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->inOrder:Z

    .line 77
    return-void

    .line 66
    :cond_0
    aget-object v0, p1, v1

    .line 67
    .local v0, "clause":Lorg/apache/lucene/search/spans/SpanQuery;
    if-nez v1, :cond_2

    .line 68
    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/SpanQuery;->getField()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->field:Ljava/lang/String;

    .line 72
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->clauses:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 65
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 69
    :cond_2
    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/SpanQuery;->getField()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->field:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 70
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Clauses must have same field."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
.end method


# virtual methods
.method public bridge synthetic clone()Lorg/apache/lucene/search/Query;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanNearQuery;->clone()Lorg/apache/lucene/search/spans/SpanNearQuery;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lorg/apache/lucene/search/spans/SpanNearQuery;
    .locals 6

    .prologue
    .line 156
    iget-object v4, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->clauses:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v3

    .line 157
    .local v3, "sz":I
    new-array v1, v3, [Lorg/apache/lucene/search/spans/SpanQuery;

    .line 159
    .local v1, "newClauses":[Lorg/apache/lucene/search/spans/SpanQuery;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v3, :cond_0

    .line 162
    new-instance v2, Lorg/apache/lucene/search/spans/SpanNearQuery;

    iget v4, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->slop:I

    iget-boolean v5, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->inOrder:Z

    invoke-direct {v2, v1, v4, v5}, Lorg/apache/lucene/search/spans/SpanNearQuery;-><init>([Lorg/apache/lucene/search/spans/SpanQuery;IZ)V

    .line 163
    .local v2, "spanNearQuery":Lorg/apache/lucene/search/spans/SpanNearQuery;
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanNearQuery;->getBoost()F

    move-result v4

    invoke-virtual {v2, v4}, Lorg/apache/lucene/search/spans/SpanNearQuery;->setBoost(F)V

    .line 164
    return-object v2

    .line 160
    .end local v2    # "spanNearQuery":Lorg/apache/lucene/search/spans/SpanNearQuery;
    :cond_0
    iget-object v4, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->clauses:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v4}, Lorg/apache/lucene/search/spans/SpanQuery;->clone()Lorg/apache/lucene/search/Query;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/search/spans/SpanQuery;

    aput-object v4, v1, v0

    .line 159
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 170
    if-ne p0, p1, :cond_1

    .line 179
    :cond_0
    :goto_0
    return v1

    .line 171
    :cond_1
    instance-of v3, p1, Lorg/apache/lucene/search/spans/SpanNearQuery;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 173
    check-cast v0, Lorg/apache/lucene/search/spans/SpanNearQuery;

    .line 175
    .local v0, "spanNearQuery":Lorg/apache/lucene/search/spans/SpanNearQuery;
    iget-boolean v3, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->inOrder:Z

    iget-boolean v4, v0, Lorg/apache/lucene/search/spans/SpanNearQuery;->inOrder:Z

    if-eq v3, v4, :cond_3

    move v1, v2

    goto :goto_0

    .line 176
    :cond_3
    iget v3, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->slop:I

    iget v4, v0, Lorg/apache/lucene/search/spans/SpanNearQuery;->slop:I

    if-eq v3, v4, :cond_4

    move v1, v2

    goto :goto_0

    .line 177
    :cond_4
    iget-object v3, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->clauses:Ljava/util/List;

    iget-object v4, v0, Lorg/apache/lucene/search/spans/SpanNearQuery;->clauses:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    move v1, v2

    goto :goto_0

    .line 179
    :cond_5
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanNearQuery;->getBoost()F

    move-result v3

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/SpanNearQuery;->getBoost()F

    move-result v4

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public extractTerms(Ljava/util/Set;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lorg/apache/lucene/index/Term;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 95
    .local p1, "terms":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/index/Term;>;"
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->clauses:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 98
    return-void

    .line 95
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/spans/SpanQuery;

    .line 96
    .local v0, "clause":Lorg/apache/lucene/search/spans/SpanQuery;
    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/spans/SpanQuery;->extractTerms(Ljava/util/Set;)V

    goto :goto_0
.end method

.method public getClauses()[Lorg/apache/lucene/search/spans/SpanQuery;
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->clauses:Ljava/util/List;

    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->clauses:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/lucene/search/spans/SpanQuery;

    return-object v0
.end method

.method public getField()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->field:Ljava/lang/String;

    return-object v0
.end method

.method public getSlop()I
    .locals 1

    .prologue
    .line 85
    iget v0, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->slop:I

    return v0
.end method

.method public getSpans(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;Ljava/util/Map;)Lorg/apache/lucene/search/spans/Spans;
    .locals 6
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "acceptDocs"    # Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/AtomicReaderContext;",
            "Lorg/apache/lucene/util/Bits;",
            "Ljava/util/Map",
            "<",
            "Lorg/apache/lucene/index/Term;",
            "Lorg/apache/lucene/index/TermContext;",
            ">;)",
            "Lorg/apache/lucene/search/spans/Spans;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 124
    .local p3, "termContexts":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/index/Term;Lorg/apache/lucene/index/TermContext;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->clauses:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 125
    new-instance v0, Lorg/apache/lucene/search/spans/SpanOrQuery;

    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanNearQuery;->getClauses()[Lorg/apache/lucene/search/spans/SpanQuery;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/lucene/search/spans/SpanOrQuery;-><init>([Lorg/apache/lucene/search/spans/SpanQuery;)V

    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/lucene/search/spans/SpanOrQuery;->getSpans(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;Ljava/util/Map;)Lorg/apache/lucene/search/spans/Spans;

    move-result-object v0

    .line 130
    :goto_0
    return-object v0

    .line 127
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->clauses:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 128
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->clauses:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/lucene/search/spans/SpanQuery;->getSpans(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;Ljava/util/Map;)Lorg/apache/lucene/search/spans/Spans;

    move-result-object v0

    goto :goto_0

    .line 130
    :cond_1
    iget-boolean v0, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->inOrder:Z

    if-eqz v0, :cond_2

    .line 131
    new-instance v0, Lorg/apache/lucene/search/spans/NearSpansOrdered;

    iget-boolean v5, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->collectPayloads:Z

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/search/spans/NearSpansOrdered;-><init>(Lorg/apache/lucene/search/spans/SpanNearQuery;Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;Ljava/util/Map;Z)V

    goto :goto_0

    .line 132
    :cond_2
    new-instance v0, Lorg/apache/lucene/search/spans/NearSpansUnordered;

    invoke-direct {v0, p0, p1, p2, p3}, Lorg/apache/lucene/search/spans/NearSpansUnordered;-><init>(Lorg/apache/lucene/search/spans/SpanNearQuery;Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;Ljava/util/Map;)V

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 185
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->clauses:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v0

    .line 189
    .local v0, "result":I
    shl-int/lit8 v1, v0, 0xe

    ushr-int/lit8 v2, v0, 0x13

    or-int/2addr v1, v2

    xor-int/2addr v0, v1

    .line 190
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanNearQuery;->getBoost()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 191
    iget v1, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->slop:I

    add-int/2addr v0, v1

    .line 192
    iget-boolean v1, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->inOrder:Z

    if-eqz v1, :cond_0

    const v1, -0x66502c43

    :goto_0
    xor-int/2addr v0, v1

    .line 193
    return v0

    .line 192
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isInOrder()Z
    .locals 1

    .prologue
    .line 88
    iget-boolean v0, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->inOrder:Z

    return v0
.end method

.method public rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;
    .locals 5
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 137
    const/4 v1, 0x0

    .line 138
    .local v1, "clone":Lorg/apache/lucene/search/spans/SpanNearQuery;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v4, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->clauses:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lt v2, v4, :cond_0

    .line 147
    if-eqz v1, :cond_3

    .line 150
    .end local v1    # "clone":Lorg/apache/lucene/search/spans/SpanNearQuery;
    :goto_1
    return-object v1

    .line 139
    .restart local v1    # "clone":Lorg/apache/lucene/search/spans/SpanNearQuery;
    :cond_0
    iget-object v4, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->clauses:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/spans/SpanQuery;

    .line 140
    .local v0, "c":Lorg/apache/lucene/search/spans/SpanQuery;
    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/spans/SpanQuery;->rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/search/spans/SpanQuery;

    .line 141
    .local v3, "query":Lorg/apache/lucene/search/spans/SpanQuery;
    if-eq v3, v0, :cond_2

    .line 142
    if-nez v1, :cond_1

    .line 143
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanNearQuery;->clone()Lorg/apache/lucene/search/spans/SpanNearQuery;

    move-result-object v1

    .line 144
    :cond_1
    iget-object v4, v1, Lorg/apache/lucene/search/spans/SpanNearQuery;->clauses:Ljava/util/List;

    invoke-interface {v4, v2, v3}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 138
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .end local v0    # "c":Lorg/apache/lucene/search/spans/SpanQuery;
    .end local v3    # "query":Lorg/apache/lucene/search/spans/SpanQuery;
    :cond_3
    move-object v1, p0

    .line 150
    goto :goto_1
.end method

.method public toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 103
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 104
    .local v0, "buffer":Ljava/lang/StringBuilder;
    const-string v3, "spanNear(["

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105
    iget-object v3, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->clauses:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 106
    .local v2, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/search/spans/SpanQuery;>;"
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 113
    const-string v3, "], "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    iget v3, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->slop:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 115
    const-string v3, ", "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 116
    iget-boolean v3, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->inOrder:Z

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 117
    const-string v3, ")"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 118
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanNearQuery;->getBoost()F

    move-result v3

    invoke-static {v3}, Lorg/apache/lucene/util/ToStringUtils;->boost(F)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 119
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 107
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/search/spans/SpanQuery;

    .line 108
    .local v1, "clause":Lorg/apache/lucene/search/spans/SpanQuery;
    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/spans/SpanQuery;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 109
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 110
    const-string v3, ", "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method
