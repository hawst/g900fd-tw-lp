.class Lorg/apache/lucene/search/FieldCacheRangeFilter$8$1;
.super Lorg/apache/lucene/search/FieldCacheDocIdSet;
.source "FieldCacheRangeFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/FieldCacheRangeFilter$8;->getDocIdSet(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/DocIdSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lorg/apache/lucene/search/FieldCacheRangeFilter$8;

.field private final synthetic val$inclusiveLowerPoint:D

.field private final synthetic val$inclusiveUpperPoint:D

.field private final synthetic val$values:Lorg/apache/lucene/search/FieldCache$Doubles;


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/FieldCacheRangeFilter$8;ILorg/apache/lucene/util/Bits;Lorg/apache/lucene/search/FieldCache$Doubles;DD)V
    .locals 1
    .param p2, "$anonymous0"    # I
    .param p3, "$anonymous1"    # Lorg/apache/lucene/util/Bits;

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$8$1;->this$1:Lorg/apache/lucene/search/FieldCacheRangeFilter$8;

    iput-object p4, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$8$1;->val$values:Lorg/apache/lucene/search/FieldCache$Doubles;

    iput-wide p5, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$8$1;->val$inclusiveLowerPoint:D

    iput-wide p7, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$8$1;->val$inclusiveUpperPoint:D

    .line 501
    invoke-direct {p0, p2, p3}, Lorg/apache/lucene/search/FieldCacheDocIdSet;-><init>(ILorg/apache/lucene/util/Bits;)V

    return-void
.end method


# virtual methods
.method protected matchDoc(I)Z
    .locals 4
    .param p1, "doc"    # I

    .prologue
    .line 504
    iget-object v2, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$8$1;->val$values:Lorg/apache/lucene/search/FieldCache$Doubles;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/search/FieldCache$Doubles;->get(I)D

    move-result-wide v0

    .line 505
    .local v0, "value":D
    iget-wide v2, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$8$1;->val$inclusiveLowerPoint:D

    cmpl-double v2, v0, v2

    if-ltz v2, :cond_0

    iget-wide v2, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$8$1;->val$inclusiveUpperPoint:D

    cmpg-double v2, v0, v2

    if-gtz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method
