.class public Lorg/apache/lucene/search/spans/SpanOrQuery;
.super Lorg/apache/lucene/search/spans/SpanQuery;
.source "SpanOrQuery.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;
    }
.end annotation


# instance fields
.field private clauses:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/search/spans/SpanQuery;",
            ">;"
        }
    .end annotation
.end field

.field private field:Ljava/lang/String;


# direct methods
.method public varargs constructor <init>([Lorg/apache/lucene/search/spans/SpanQuery;)V
    .locals 3
    .param p1, "clauses"    # [Lorg/apache/lucene/search/spans/SpanQuery;

    .prologue
    .line 44
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/SpanQuery;-><init>()V

    .line 47
    new-instance v1, Ljava/util/ArrayList;

    array-length v2, p1

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lorg/apache/lucene/search/spans/SpanOrQuery;->clauses:Ljava/util/List;

    .line 48
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-lt v0, v1, :cond_0

    .line 51
    return-void

    .line 49
    :cond_0
    aget-object v1, p1, v0

    invoke-virtual {p0, v1}, Lorg/apache/lucene/search/spans/SpanOrQuery;->addClause(Lorg/apache/lucene/search/spans/SpanQuery;)V

    .line 48
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method static synthetic access$1(Lorg/apache/lucene/search/spans/SpanOrQuery;)Ljava/util/List;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanOrQuery;->clauses:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public final addClause(Lorg/apache/lucene/search/spans/SpanQuery;)V
    .locals 2
    .param p1, "clause"    # Lorg/apache/lucene/search/spans/SpanQuery;

    .prologue
    .line 55
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanOrQuery;->field:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 56
    invoke-virtual {p1}, Lorg/apache/lucene/search/spans/SpanQuery;->getField()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/spans/SpanOrQuery;->field:Ljava/lang/String;

    .line 60
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanOrQuery;->clauses:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 61
    return-void

    .line 57
    :cond_1
    invoke-virtual {p1}, Lorg/apache/lucene/search/spans/SpanQuery;->getField()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanOrQuery;->field:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 58
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Clauses must have same field."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public bridge synthetic clone()Lorg/apache/lucene/search/Query;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanOrQuery;->clone()Lorg/apache/lucene/search/spans/SpanOrQuery;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lorg/apache/lucene/search/spans/SpanOrQuery;
    .locals 5

    .prologue
    .line 80
    iget-object v4, p0, Lorg/apache/lucene/search/spans/SpanOrQuery;->clauses:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v3

    .line 81
    .local v3, "sz":I
    new-array v1, v3, [Lorg/apache/lucene/search/spans/SpanQuery;

    .line 83
    .local v1, "newClauses":[Lorg/apache/lucene/search/spans/SpanQuery;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v3, :cond_0

    .line 86
    new-instance v2, Lorg/apache/lucene/search/spans/SpanOrQuery;

    invoke-direct {v2, v1}, Lorg/apache/lucene/search/spans/SpanOrQuery;-><init>([Lorg/apache/lucene/search/spans/SpanQuery;)V

    .line 87
    .local v2, "soq":Lorg/apache/lucene/search/spans/SpanOrQuery;
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanOrQuery;->getBoost()F

    move-result v4

    invoke-virtual {v2, v4}, Lorg/apache/lucene/search/spans/SpanOrQuery;->setBoost(F)V

    .line 88
    return-object v2

    .line 84
    .end local v2    # "soq":Lorg/apache/lucene/search/spans/SpanOrQuery;
    :cond_0
    iget-object v4, p0, Lorg/apache/lucene/search/spans/SpanOrQuery;->clauses:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v4}, Lorg/apache/lucene/search/spans/SpanQuery;->clone()Lorg/apache/lucene/search/Query;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/search/spans/SpanQuery;

    aput-object v4, v1, v0

    .line 83
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 129
    if-ne p0, p1, :cond_1

    .line 137
    :cond_0
    :goto_0
    return v1

    .line 130
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 132
    check-cast v0, Lorg/apache/lucene/search/spans/SpanOrQuery;

    .line 134
    .local v0, "that":Lorg/apache/lucene/search/spans/SpanOrQuery;
    iget-object v3, p0, Lorg/apache/lucene/search/spans/SpanOrQuery;->clauses:Ljava/util/List;

    iget-object v4, v0, Lorg/apache/lucene/search/spans/SpanOrQuery;->clauses:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    goto :goto_0

    .line 135
    :cond_4
    iget-object v3, p0, Lorg/apache/lucene/search/spans/SpanOrQuery;->clauses:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_5

    iget-object v3, p0, Lorg/apache/lucene/search/spans/SpanOrQuery;->field:Ljava/lang/String;

    iget-object v4, v0, Lorg/apache/lucene/search/spans/SpanOrQuery;->field:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    move v1, v2

    goto :goto_0

    .line 137
    :cond_5
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanOrQuery;->getBoost()F

    move-result v3

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/SpanOrQuery;->getBoost()F

    move-result v4

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public extractTerms(Ljava/util/Set;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lorg/apache/lucene/index/Term;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 73
    .local p1, "terms":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/index/Term;>;"
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanOrQuery;->clauses:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 76
    return-void

    .line 73
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/spans/SpanQuery;

    .line 74
    .local v0, "clause":Lorg/apache/lucene/search/spans/SpanQuery;
    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/spans/SpanQuery;->extractTerms(Ljava/util/Set;)V

    goto :goto_0
.end method

.method public getClauses()[Lorg/apache/lucene/search/spans/SpanQuery;
    .locals 2

    .prologue
    .line 65
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanOrQuery;->clauses:Ljava/util/List;

    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanOrQuery;->clauses:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/lucene/search/spans/SpanQuery;

    return-object v0
.end method

.method public getField()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanOrQuery;->field:Ljava/lang/String;

    return-object v0
.end method

.method public getSpans(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;Ljava/util/Map;)Lorg/apache/lucene/search/spans/Spans;
    .locals 2
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "acceptDocs"    # Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/AtomicReaderContext;",
            "Lorg/apache/lucene/util/Bits;",
            "Ljava/util/Map",
            "<",
            "Lorg/apache/lucene/index/Term;",
            "Lorg/apache/lucene/index/TermContext;",
            ">;)",
            "Lorg/apache/lucene/search/spans/Spans;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 170
    .local p3, "termContexts":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/index/Term;Lorg/apache/lucene/index/TermContext;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanOrQuery;->clauses:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 171
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanOrQuery;->clauses:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/lucene/search/spans/SpanQuery;->getSpans(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;Ljava/util/Map;)Lorg/apache/lucene/search/spans/Spans;

    move-result-object v0

    .line 173
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/apache/lucene/search/spans/SpanOrQuery$1;

    invoke-direct {v0, p0, p1, p2, p3}, Lorg/apache/lucene/search/spans/SpanOrQuery$1;-><init>(Lorg/apache/lucene/search/spans/SpanOrQuery;Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;Ljava/util/Map;)V

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 142
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanOrQuery;->clauses:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v0

    .line 143
    .local v0, "h":I
    shl-int/lit8 v1, v0, 0xa

    ushr-int/lit8 v2, v0, 0x17

    or-int/2addr v1, v2

    xor-int/2addr v0, v1

    .line 144
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanOrQuery;->getBoost()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v1

    xor-int/2addr v0, v1

    .line 145
    return v0
.end method

.method public rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;
    .locals 5
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 93
    const/4 v1, 0x0

    .line 94
    .local v1, "clone":Lorg/apache/lucene/search/spans/SpanOrQuery;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v4, p0, Lorg/apache/lucene/search/spans/SpanOrQuery;->clauses:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lt v2, v4, :cond_0

    .line 103
    if-eqz v1, :cond_3

    .line 106
    .end local v1    # "clone":Lorg/apache/lucene/search/spans/SpanOrQuery;
    :goto_1
    return-object v1

    .line 95
    .restart local v1    # "clone":Lorg/apache/lucene/search/spans/SpanOrQuery;
    :cond_0
    iget-object v4, p0, Lorg/apache/lucene/search/spans/SpanOrQuery;->clauses:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/spans/SpanQuery;

    .line 96
    .local v0, "c":Lorg/apache/lucene/search/spans/SpanQuery;
    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/spans/SpanQuery;->rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/search/spans/SpanQuery;

    .line 97
    .local v3, "query":Lorg/apache/lucene/search/spans/SpanQuery;
    if-eq v3, v0, :cond_2

    .line 98
    if-nez v1, :cond_1

    .line 99
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanOrQuery;->clone()Lorg/apache/lucene/search/spans/SpanOrQuery;

    move-result-object v1

    .line 100
    :cond_1
    iget-object v4, v1, Lorg/apache/lucene/search/spans/SpanOrQuery;->clauses:Ljava/util/List;

    invoke-interface {v4, v2, v3}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 94
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .end local v0    # "c":Lorg/apache/lucene/search/spans/SpanQuery;
    .end local v3    # "query":Lorg/apache/lucene/search/spans/SpanQuery;
    :cond_3
    move-object v1, p0

    .line 106
    goto :goto_1
.end method

.method public toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 112
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 113
    .local v0, "buffer":Ljava/lang/StringBuilder;
    const-string v3, "spanOr(["

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    iget-object v3, p0, Lorg/apache/lucene/search/spans/SpanOrQuery;->clauses:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 115
    .local v2, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/search/spans/SpanQuery;>;"
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 122
    const-string v3, "])"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 123
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanOrQuery;->getBoost()F

    move-result v3

    invoke-static {v3}, Lorg/apache/lucene/util/ToStringUtils;->boost(F)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 124
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 116
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/search/spans/SpanQuery;

    .line 117
    .local v1, "clause":Lorg/apache/lucene/search/spans/SpanQuery;
    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/spans/SpanQuery;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 118
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 119
    const-string v3, ", "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method
