.class final Lorg/apache/lucene/search/TermScorer;
.super Lorg/apache/lucene/search/Scorer;
.source "TermScorer.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final docScorer:Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;

.field private final docsEnum:Lorg/apache/lucene/index/DocsEnum;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lorg/apache/lucene/search/TermScorer;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/TermScorer;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/index/DocsEnum;Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;)V
    .locals 0
    .param p1, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p2, "td"    # Lorg/apache/lucene/index/DocsEnum;
    .param p3, "docScorer"    # Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/Scorer;-><init>(Lorg/apache/lucene/search/Weight;)V

    .line 44
    iput-object p3, p0, Lorg/apache/lucene/search/TermScorer;->docScorer:Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;

    .line 45
    iput-object p2, p0, Lorg/apache/lucene/search/TermScorer;->docsEnum:Lorg/apache/lucene/index/DocsEnum;

    .line 46
    return-void
.end method


# virtual methods
.method public advance(I)I
    .locals 1
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 85
    iget-object v0, p0, Lorg/apache/lucene/search/TermScorer;->docsEnum:Lorg/apache/lucene/index/DocsEnum;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/DocsEnum;->advance(I)I

    move-result v0

    return v0
.end method

.method public cost()J
    .locals 2

    .prologue
    .line 90
    iget-object v0, p0, Lorg/apache/lucene/search/TermScorer;->docsEnum:Lorg/apache/lucene/index/DocsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocsEnum;->cost()J

    move-result-wide v0

    return-wide v0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lorg/apache/lucene/search/TermScorer;->docsEnum:Lorg/apache/lucene/index/DocsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocsEnum;->docID()I

    move-result v0

    return v0
.end method

.method public freq()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55
    iget-object v0, p0, Lorg/apache/lucene/search/TermScorer;->docsEnum:Lorg/apache/lucene/index/DocsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocsEnum;->freq()I

    move-result v0

    return v0
.end method

.method public nextDoc()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 65
    iget-object v0, p0, Lorg/apache/lucene/search/TermScorer;->docsEnum:Lorg/apache/lucene/index/DocsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocsEnum;->nextDoc()I

    move-result v0

    return v0
.end method

.method public score()F
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 70
    sget-boolean v0, Lorg/apache/lucene/search/TermScorer;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/search/TermScorer;->docID()I

    move-result v0

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 71
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/TermScorer;->docScorer:Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;

    iget-object v1, p0, Lorg/apache/lucene/search/TermScorer;->docsEnum:Lorg/apache/lucene/index/DocsEnum;

    invoke-virtual {v1}, Lorg/apache/lucene/index/DocsEnum;->docID()I

    move-result v1

    iget-object v2, p0, Lorg/apache/lucene/search/TermScorer;->docsEnum:Lorg/apache/lucene/index/DocsEnum;

    invoke-virtual {v2}, Lorg/apache/lucene/index/DocsEnum;->freq()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;->score(II)F

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 95
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "scorer("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/search/TermScorer;->weight:Lorg/apache/lucene/search/Weight;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
