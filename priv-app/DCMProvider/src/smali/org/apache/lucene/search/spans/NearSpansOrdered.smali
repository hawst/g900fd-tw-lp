.class public Lorg/apache/lucene/search/spans/NearSpansOrdered;
.super Lorg/apache/lucene/search/spans/Spans;
.source "NearSpansOrdered.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final allowedSlop:I

.field private collectPayloads:Z

.field private firstTime:Z

.field private inSameDoc:Z

.field private matchDoc:I

.field private matchEnd:I

.field private matchPayload:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<[B>;"
        }
    .end annotation
.end field

.field private matchStart:I

.field private more:Z

.field private query:Lorg/apache/lucene/search/spans/SpanNearQuery;

.field private final spanDocComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/search/spans/Spans;",
            ">;"
        }
    .end annotation
.end field

.field private final subSpans:[Lorg/apache/lucene/search/spans/Spans;

.field private final subSpansByDoc:[Lorg/apache/lucene/search/spans/Spans;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    const-class v0, Lorg/apache/lucene/search/spans/NearSpansOrdered;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/search/spans/SpanNearQuery;Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;Ljava/util/Map;)V
    .locals 6
    .param p1, "spanNearQuery"    # Lorg/apache/lucene/search/spans/SpanNearQuery;
    .param p2, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p3, "acceptDocs"    # Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/search/spans/SpanNearQuery;",
            "Lorg/apache/lucene/index/AtomicReaderContext;",
            "Lorg/apache/lucene/util/Bits;",
            "Ljava/util/Map",
            "<",
            "Lorg/apache/lucene/index/Term;",
            "Lorg/apache/lucene/index/TermContext;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 86
    .local p4, "termContexts":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/index/Term;Lorg/apache/lucene/index/TermContext;>;"
    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/search/spans/NearSpansOrdered;-><init>(Lorg/apache/lucene/search/spans/SpanNearQuery;Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;Ljava/util/Map;Z)V

    .line 87
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/search/spans/SpanNearQuery;Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;Ljava/util/Map;Z)V
    .locals 5
    .param p1, "spanNearQuery"    # Lorg/apache/lucene/search/spans/SpanNearQuery;
    .param p2, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p3, "acceptDocs"    # Lorg/apache/lucene/util/Bits;
    .param p5, "collectPayloads"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/search/spans/SpanNearQuery;",
            "Lorg/apache/lucene/index/AtomicReaderContext;",
            "Lorg/apache/lucene/util/Bits;",
            "Ljava/util/Map",
            "<",
            "Lorg/apache/lucene/index/Term;",
            "Lorg/apache/lucene/index/TermContext;",
            ">;Z)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p4, "termContexts":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/index/Term;Lorg/apache/lucene/index/TermContext;>;"
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 89
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/Spans;-><init>()V

    .line 60
    iput-boolean v4, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->firstTime:Z

    .line 61
    iput-boolean v3, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->more:Z

    .line 67
    iput-boolean v3, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->inSameDoc:Z

    .line 69
    iput v2, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->matchDoc:I

    .line 70
    iput v2, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->matchStart:I

    .line 71
    iput v2, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->matchEnd:I

    .line 75
    new-instance v2, Lorg/apache/lucene/search/spans/NearSpansOrdered$1;

    invoke-direct {v2, p0}, Lorg/apache/lucene/search/spans/NearSpansOrdered$1;-><init>(Lorg/apache/lucene/search/spans/NearSpansOrdered;)V

    iput-object v2, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->spanDocComparator:Ljava/util/Comparator;

    .line 83
    iput-boolean v4, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->collectPayloads:Z

    .line 91
    invoke-virtual {p1}, Lorg/apache/lucene/search/spans/SpanNearQuery;->getClauses()[Lorg/apache/lucene/search/spans/SpanQuery;

    move-result-object v2

    array-length v2, v2

    const/4 v3, 0x2

    if-ge v2, v3, :cond_0

    .line 92
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Less than 2 clauses: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 93
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 92
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 95
    :cond_0
    iput-boolean p5, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->collectPayloads:Z

    .line 96
    invoke-virtual {p1}, Lorg/apache/lucene/search/spans/SpanNearQuery;->getSlop()I

    move-result v2

    iput v2, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->allowedSlop:I

    .line 97
    invoke-virtual {p1}, Lorg/apache/lucene/search/spans/SpanNearQuery;->getClauses()[Lorg/apache/lucene/search/spans/SpanQuery;

    move-result-object v0

    .line 98
    .local v0, "clauses":[Lorg/apache/lucene/search/spans/SpanQuery;
    array-length v2, v0

    new-array v2, v2, [Lorg/apache/lucene/search/spans/Spans;

    iput-object v2, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    .line 99
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    iput-object v2, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->matchPayload:Ljava/util/List;

    .line 100
    array-length v2, v0

    new-array v2, v2, [Lorg/apache/lucene/search/spans/Spans;

    iput-object v2, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpansByDoc:[Lorg/apache/lucene/search/spans/Spans;

    .line 101
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, v0

    if-lt v1, v2, :cond_1

    .line 105
    iput-object p1, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->query:Lorg/apache/lucene/search/spans/SpanNearQuery;

    .line 106
    return-void

    .line 102
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    aget-object v3, v0, v1

    invoke-virtual {v3, p2, p3, p4}, Lorg/apache/lucene/search/spans/SpanQuery;->getSpans(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;Ljava/util/Map;)Lorg/apache/lucene/search/spans/Spans;

    move-result-object v3

    aput-object v3, v2, v1

    .line 103
    iget-object v2, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpansByDoc:[Lorg/apache/lucene/search/spans/Spans;

    iget-object v3, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    aget-object v3, v3, v1

    aput-object v3, v2, v1

    .line 101
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private advanceAfterOrdered()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 196
    :cond_0
    iget-boolean v0, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->more:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->inSameDoc:Z

    if-nez v0, :cond_2

    invoke-direct {p0}, Lorg/apache/lucene/search/spans/NearSpansOrdered;->toSameDoc()Z

    move-result v0

    if-nez v0, :cond_2

    .line 201
    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 197
    :cond_2
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/NearSpansOrdered;->stretchToOrder()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/search/spans/NearSpansOrdered;->shrinkToAfterShortestMatch()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 198
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static final docSpansOrdered(IIII)Z
    .locals 2
    .param p0, "start1"    # I
    .param p1, "end1"    # I
    .param p2, "start2"    # I
    .param p3, "end2"    # I

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 248
    if-ne p0, p2, :cond_2

    if-ge p1, p3, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    if-lt p0, p2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method static final docSpansOrdered(Lorg/apache/lucene/search/spans/Spans;Lorg/apache/lucene/search/spans/Spans;)Z
    .locals 6
    .param p0, "spans1"    # Lorg/apache/lucene/search/spans/Spans;
    .param p1, "spans2"    # Lorg/apache/lucene/search/spans/Spans;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 237
    sget-boolean v4, Lorg/apache/lucene/search/spans/NearSpansOrdered;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v4

    invoke-virtual {p1}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v5

    if-eq v4, v5, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "doc1 "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " != doc2 "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 238
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/Spans;->start()I

    move-result v0

    .line 239
    .local v0, "start1":I
    invoke-virtual {p1}, Lorg/apache/lucene/search/spans/Spans;->start()I

    move-result v1

    .line 241
    .local v1, "start2":I
    if-ne v0, v1, :cond_3

    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/Spans;->end()I

    move-result v4

    invoke-virtual {p1}, Lorg/apache/lucene/search/spans/Spans;->end()I

    move-result v5

    if-ge v4, v5, :cond_2

    :cond_1
    :goto_0
    return v2

    :cond_2
    move v2, v3

    goto :goto_0

    :cond_3
    if-lt v0, v1, :cond_1

    move v2, v3

    goto :goto_0
.end method

.method private shrinkToAfterShortestMatch()Z
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 276
    iget-object v13, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    iget-object v14, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    array-length v14, v14

    add-int/lit8 v14, v14, -0x1

    aget-object v13, v13, v14

    invoke-virtual {v13}, Lorg/apache/lucene/search/spans/Spans;->start()I

    move-result v13

    iput v13, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->matchStart:I

    .line 277
    iget-object v13, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    iget-object v14, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    array-length v14, v14

    add-int/lit8 v14, v14, -0x1

    aget-object v13, v13, v14

    invoke-virtual {v13}, Lorg/apache/lucene/search/spans/Spans;->end()I

    move-result v13

    iput v13, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->matchEnd:I

    .line 278
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 279
    .local v6, "possibleMatchPayloads":Ljava/util/Set;, "Ljava/util/Set<[B>;"
    iget-object v13, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    iget-object v14, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    array-length v14, v14

    add-int/lit8 v14, v14, -0x1

    aget-object v13, v13, v14

    invoke-virtual {v13}, Lorg/apache/lucene/search/spans/Spans;->isPayloadAvailable()Z

    move-result v13

    if-eqz v13, :cond_0

    .line 280
    iget-object v13, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    iget-object v14, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    array-length v14, v14

    add-int/lit8 v14, v14, -0x1

    aget-object v13, v13, v14

    invoke-virtual {v13}, Lorg/apache/lucene/search/spans/Spans;->getPayload()Ljava/util/Collection;

    move-result-object v13

    invoke-interface {v6, v13}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 283
    :cond_0
    const/4 v7, 0x0

    .line 285
    .local v7, "possiblePayload":Ljava/util/Collection;, "Ljava/util/Collection<[B>;"
    const/4 v4, 0x0

    .line 286
    .local v4, "matchSlop":I
    iget v2, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->matchStart:I

    .line 287
    .local v2, "lastStart":I
    iget v1, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->matchEnd:I

    .line 288
    .local v1, "lastEnd":I
    iget-object v13, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    array-length v13, v13

    add-int/lit8 v0, v13, -0x2

    .local v0, "i":I
    :goto_0
    if-gez v0, :cond_3

    .line 340
    iget v13, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->allowedSlop:I

    if-gt v4, v13, :cond_1

    const/4 v3, 0x1

    .line 342
    .local v3, "match":Z
    :cond_1
    iget-boolean v13, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->collectPayloads:Z

    if-eqz v13, :cond_2

    if-eqz v3, :cond_2

    invoke-interface {v6}, Ljava/util/Set;->size()I

    move-result v13

    if-lez v13, :cond_2

    .line 343
    iget-object v13, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->matchPayload:Ljava/util/List;

    invoke-interface {v13, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 346
    :cond_2
    return v3

    .line 289
    .end local v3    # "match":Z
    :cond_3
    iget-object v13, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    aget-object v11, v13, v0

    .line 290
    .local v11, "prevSpans":Lorg/apache/lucene/search/spans/Spans;
    iget-boolean v13, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->collectPayloads:Z

    if-eqz v13, :cond_4

    invoke-virtual {v11}, Lorg/apache/lucene/search/spans/Spans;->isPayloadAvailable()Z

    move-result v13

    if-eqz v13, :cond_4

    .line 291
    invoke-virtual {v11}, Lorg/apache/lucene/search/spans/Spans;->getPayload()Ljava/util/Collection;

    move-result-object v5

    .line 292
    .local v5, "payload":Ljava/util/Collection;, "Ljava/util/Collection<[B>;"
    new-instance v7, Ljava/util/ArrayList;

    .end local v7    # "possiblePayload":Ljava/util/Collection;, "Ljava/util/Collection<[B>;"
    invoke-interface {v5}, Ljava/util/Collection;->size()I

    move-result v13

    invoke-direct {v7, v13}, Ljava/util/ArrayList;-><init>(I)V

    .line 293
    .restart local v7    # "possiblePayload":Ljava/util/Collection;, "Ljava/util/Collection<[B>;"
    invoke-interface {v7, v5}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 296
    .end local v5    # "payload":Ljava/util/Collection;, "Ljava/util/Collection<[B>;"
    :cond_4
    invoke-virtual {v11}, Lorg/apache/lucene/search/spans/Spans;->start()I

    move-result v12

    .line 297
    .local v12, "prevStart":I
    invoke-virtual {v11}, Lorg/apache/lucene/search/spans/Spans;->end()I

    move-result v10

    .line 299
    .local v10, "prevEnd":I
    :cond_5
    :goto_1
    invoke-virtual {v11}, Lorg/apache/lucene/search/spans/Spans;->next()Z

    move-result v13

    if-nez v13, :cond_8

    .line 300
    iput-boolean v3, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->inSameDoc:Z

    .line 301
    iput-boolean v3, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->more:Z

    .line 323
    :cond_6
    :goto_2
    iget-boolean v13, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->collectPayloads:Z

    if-eqz v13, :cond_7

    if-eqz v7, :cond_7

    .line 324
    invoke-interface {v6, v7}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 327
    :cond_7
    sget-boolean v13, Lorg/apache/lucene/search/spans/NearSpansOrdered;->$assertionsDisabled:Z

    if-nez v13, :cond_a

    iget v13, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->matchStart:I

    if-le v12, v13, :cond_a

    new-instance v13, Ljava/lang/AssertionError;

    invoke-direct {v13}, Ljava/lang/AssertionError;-><init>()V

    throw v13

    .line 303
    :cond_8
    iget v13, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->matchDoc:I

    invoke-virtual {v11}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v14

    if-eq v13, v14, :cond_9

    .line 304
    iput-boolean v3, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->inSameDoc:Z

    goto :goto_2

    .line 307
    :cond_9
    invoke-virtual {v11}, Lorg/apache/lucene/search/spans/Spans;->start()I

    move-result v9

    .line 308
    .local v9, "ppStart":I
    invoke-virtual {v11}, Lorg/apache/lucene/search/spans/Spans;->end()I

    move-result v8

    .line 309
    .local v8, "ppEnd":I
    invoke-static {v9, v8, v2, v1}, Lorg/apache/lucene/search/spans/NearSpansOrdered;->docSpansOrdered(IIII)Z

    move-result v13

    if-eqz v13, :cond_6

    .line 312
    move v12, v9

    .line 313
    move v10, v8

    .line 314
    iget-boolean v13, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->collectPayloads:Z

    if-eqz v13, :cond_5

    invoke-virtual {v11}, Lorg/apache/lucene/search/spans/Spans;->isPayloadAvailable()Z

    move-result v13

    if-eqz v13, :cond_5

    .line 315
    invoke-virtual {v11}, Lorg/apache/lucene/search/spans/Spans;->getPayload()Ljava/util/Collection;

    move-result-object v5

    .line 316
    .restart local v5    # "payload":Ljava/util/Collection;, "Ljava/util/Collection<[B>;"
    new-instance v7, Ljava/util/ArrayList;

    .end local v7    # "possiblePayload":Ljava/util/Collection;, "Ljava/util/Collection<[B>;"
    invoke-interface {v5}, Ljava/util/Collection;->size()I

    move-result v13

    invoke-direct {v7, v13}, Ljava/util/ArrayList;-><init>(I)V

    .line 317
    .restart local v7    # "possiblePayload":Ljava/util/Collection;, "Ljava/util/Collection<[B>;"
    invoke-interface {v7, v5}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 328
    .end local v5    # "payload":Ljava/util/Collection;, "Ljava/util/Collection<[B>;"
    .end local v8    # "ppEnd":I
    .end local v9    # "ppStart":I
    :cond_a
    iget v13, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->matchStart:I

    if-le v13, v10, :cond_b

    .line 329
    iget v13, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->matchStart:I

    sub-int/2addr v13, v10

    add-int/2addr v4, v13

    .line 335
    :cond_b
    iput v12, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->matchStart:I

    .line 336
    move v2, v12

    .line 337
    move v1, v10

    .line 288
    add-int/lit8 v0, v0, -0x1

    goto/16 :goto_0
.end method

.method private stretchToOrder()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 255
    iget-object v1, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    aget-object v1, v1, v3

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->matchDoc:I

    .line 256
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    iget-boolean v1, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->inSameDoc:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    array-length v1, v1

    if-lt v0, v1, :cond_3

    .line 268
    :cond_0
    iget-boolean v1, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->inSameDoc:Z

    return v1

    .line 258
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/Spans;->next()Z

    move-result v1

    if-nez v1, :cond_2

    .line 259
    iput-boolean v3, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->inSameDoc:Z

    .line 260
    iput-boolean v3, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->more:Z

    .line 256
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 262
    :cond_2
    iget v1, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->matchDoc:I

    iget-object v2, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v2

    if-eq v1, v2, :cond_3

    .line 263
    iput-boolean v3, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->inSameDoc:Z

    goto :goto_1

    .line 257
    :cond_3
    iget-object v1, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    add-int/lit8 v2, v0, -0x1

    aget-object v1, v1, v2

    iget-object v2, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    aget-object v2, v2, v0

    invoke-static {v1, v2}, Lorg/apache/lucene/search/spans/NearSpansOrdered;->docSpansOrdered(Lorg/apache/lucene/search/spans/Spans;Lorg/apache/lucene/search/spans/Spans;)Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_1
.end method

.method private toSameDoc()Z
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 207
    iget-object v5, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpansByDoc:[Lorg/apache/lucene/search/spans/Spans;

    iget-object v6, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->spanDocComparator:Ljava/util/Comparator;

    invoke-static {v5, v6}, Lorg/apache/lucene/util/ArrayUtil;->mergeSort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 208
    const/4 v0, 0x0

    .line 209
    .local v0, "firstIndex":I
    iget-object v5, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpansByDoc:[Lorg/apache/lucene/search/spans/Spans;

    iget-object v6, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpansByDoc:[Lorg/apache/lucene/search/spans/Spans;

    array-length v6, v6

    add-int/lit8 v6, v6, -0x1

    aget-object v5, v5, v6

    invoke-virtual {v5}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v2

    .line 210
    .local v2, "maxDoc":I
    :cond_0
    :goto_0
    iget-object v5, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpansByDoc:[Lorg/apache/lucene/search/spans/Spans;

    aget-object v5, v5, v0

    invoke-virtual {v5}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v5

    if-ne v5, v2, :cond_1

    .line 221
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v5, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpansByDoc:[Lorg/apache/lucene/search/spans/Spans;

    array-length v5, v5

    if-lt v1, v5, :cond_3

    .line 227
    iput-boolean v4, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->inSameDoc:Z

    move v3, v4

    .line 228
    .end local v1    # "i":I
    :goto_2
    return v3

    .line 211
    :cond_1
    iget-object v5, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpansByDoc:[Lorg/apache/lucene/search/spans/Spans;

    aget-object v5, v5, v0

    invoke-virtual {v5, v2}, Lorg/apache/lucene/search/spans/Spans;->skipTo(I)Z

    move-result v5

    if-nez v5, :cond_2

    .line 212
    iput-boolean v3, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->more:Z

    .line 213
    iput-boolean v3, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->inSameDoc:Z

    goto :goto_2

    .line 216
    :cond_2
    iget-object v5, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpansByDoc:[Lorg/apache/lucene/search/spans/Spans;

    aget-object v5, v5, v0

    invoke-virtual {v5}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v2

    .line 217
    add-int/lit8 v0, v0, 0x1

    iget-object v5, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpansByDoc:[Lorg/apache/lucene/search/spans/Spans;

    array-length v5, v5

    if-ne v0, v5, :cond_0

    .line 218
    const/4 v0, 0x0

    goto :goto_0

    .line 222
    .restart local v1    # "i":I
    :cond_3
    sget-boolean v5, Lorg/apache/lucene/search/spans/NearSpansOrdered;->$assertionsDisabled:Z

    if-nez v5, :cond_4

    iget-object v5, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpansByDoc:[Lorg/apache/lucene/search/spans/Spans;

    aget-object v5, v5, v1

    invoke-virtual {v5}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v5

    if-eq v5, v2, :cond_4

    new-instance v4, Ljava/lang/AssertionError;

    .line 223
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, " NearSpansOrdered.toSameDoc() spans "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpansByDoc:[Lorg/apache/lucene/search/spans/Spans;

    aget-object v3, v6, v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 224
    const-string v5, "\n at doc "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpansByDoc:[Lorg/apache/lucene/search/spans/Spans;

    aget-object v5, v5, v1

    invoke-virtual {v5}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 225
    const-string v5, ", but should be at "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 223
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    .line 221
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method


# virtual methods
.method public cost()J
    .locals 6

    .prologue
    .line 139
    const-wide v2, 0x7fffffffffffffffL

    .line 140
    .local v2, "minCost":J
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 143
    return-wide v2

    .line 141
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/Spans;->cost()J

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    .line 140
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public doc()I
    .locals 1

    .prologue
    .line 110
    iget v0, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->matchDoc:I

    return v0
.end method

.method public end()I
    .locals 1

    .prologue
    .line 118
    iget v0, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->matchEnd:I

    return v0
.end method

.method public getPayload()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<[B>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 128
    iget-object v0, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->matchPayload:Ljava/util/List;

    return-object v0
.end method

.method public getSubSpans()[Lorg/apache/lucene/search/spans/Spans;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    return-object v0
.end method

.method public isPayloadAvailable()Z
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->matchPayload:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public next()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 149
    iget-boolean v2, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->firstTime:Z

    if-eqz v2, :cond_0

    .line 150
    iput-boolean v1, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->firstTime:Z

    .line 151
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    array-length v2, v2

    if-lt v0, v2, :cond_2

    .line 157
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->more:Z

    .line 159
    .end local v0    # "i":I
    :cond_0
    iget-boolean v1, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->collectPayloads:Z

    if-eqz v1, :cond_1

    .line 160
    iget-object v1, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->matchPayload:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 162
    :cond_1
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/NearSpansOrdered;->advanceAfterOrdered()Z

    move-result v1

    :goto_1
    return v1

    .line 152
    .restart local v0    # "i":I
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lorg/apache/lucene/search/spans/Spans;->next()Z

    move-result v2

    if-nez v2, :cond_3

    .line 153
    iput-boolean v1, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->more:Z

    goto :goto_1

    .line 151
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public skipTo(I)Z
    .locals 3
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 168
    iget-boolean v2, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->firstTime:Z

    if-eqz v2, :cond_4

    .line 169
    iput-boolean v1, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->firstTime:Z

    .line 170
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    array-length v2, v2

    if-lt v0, v2, :cond_2

    .line 176
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->more:Z

    .line 185
    .end local v0    # "i":I
    :cond_0
    :goto_1
    iget-boolean v1, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->collectPayloads:Z

    if-eqz v1, :cond_1

    .line 186
    iget-object v1, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->matchPayload:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 188
    :cond_1
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/NearSpansOrdered;->advanceAfterOrdered()Z

    move-result v1

    :goto_2
    return v1

    .line 171
    .restart local v0    # "i":I
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    aget-object v2, v2, v0

    invoke-virtual {v2, p1}, Lorg/apache/lucene/search/spans/Spans;->skipTo(I)Z

    move-result v2

    if-nez v2, :cond_3

    .line 172
    iput-boolean v1, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->more:Z

    goto :goto_2

    .line 170
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 177
    .end local v0    # "i":I
    :cond_4
    iget-boolean v2, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->more:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v2

    if-ge v2, p1, :cond_0

    .line 178
    iget-object v2, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    aget-object v2, v2, v1

    invoke-virtual {v2, p1}, Lorg/apache/lucene/search/spans/Spans;->skipTo(I)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 179
    iput-boolean v1, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->inSameDoc:Z

    goto :goto_1

    .line 181
    :cond_5
    iput-boolean v1, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->more:Z

    goto :goto_2
.end method

.method public start()I
    .locals 1

    .prologue
    .line 114
    iget v0, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->matchStart:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 351
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->query:Lorg/apache/lucene/search/spans/SpanNearQuery;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/SpanNearQuery;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 352
    iget-boolean v0, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->firstTime:Z

    if-eqz v0, :cond_0

    const-string v0, "START"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 351
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 352
    :cond_0
    iget-boolean v0, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->more:Z

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/NearSpansOrdered;->doc()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/NearSpansOrdered;->start()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "-"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/NearSpansOrdered;->end()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const-string v0, "END"

    goto :goto_0
.end method
