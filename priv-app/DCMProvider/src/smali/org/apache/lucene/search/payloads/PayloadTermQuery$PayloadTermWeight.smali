.class public Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;
.super Lorg/apache/lucene/search/spans/SpanWeight;
.source "PayloadTermQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/payloads/PayloadTermQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "PayloadTermWeight"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;
    }
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/search/payloads/PayloadTermQuery;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/payloads/PayloadTermQuery;Lorg/apache/lucene/search/payloads/PayloadTermQuery;Lorg/apache/lucene/search/IndexSearcher;)V
    .locals 0
    .param p2, "query"    # Lorg/apache/lucene/search/payloads/PayloadTermQuery;
    .param p3, "searcher"    # Lorg/apache/lucene/search/IndexSearcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 77
    iput-object p1, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;->this$0:Lorg/apache/lucene/search/payloads/PayloadTermQuery;

    .line 78
    invoke-direct {p0, p2, p3}, Lorg/apache/lucene/search/spans/SpanWeight;-><init>(Lorg/apache/lucene/search/spans/SpanQuery;Lorg/apache/lucene/search/IndexSearcher;)V

    .line 79
    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;)Lorg/apache/lucene/search/similarities/Similarity;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;->similarity:Lorg/apache/lucene/search/similarities/Similarity;

    return-object v0
.end method

.method static synthetic access$1(Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;)Lorg/apache/lucene/search/payloads/PayloadTermQuery;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;->this$0:Lorg/apache/lucene/search/payloads/PayloadTermQuery;

    return-object v0
.end method


# virtual methods
.method public explain(Lorg/apache/lucene/index/AtomicReaderContext;I)Lorg/apache/lucene/search/Explanation;
    .locals 13
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 180
    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v9

    invoke-virtual {v9}, Lorg/apache/lucene/index/AtomicReader;->getLiveDocs()Lorg/apache/lucene/util/Bits;

    move-result-object v9

    invoke-virtual {p0, p1, v12, v11, v9}, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;->scorer(Lorg/apache/lucene/index/AtomicReaderContext;ZZLorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/Scorer;

    move-result-object v8

    check-cast v8, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;

    .line 181
    .local v8, "scorer":Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;
    if-eqz v8, :cond_1

    .line 182
    invoke-virtual {v8, p2}, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->advance(I)I

    move-result v4

    .line 183
    .local v4, "newDoc":I
    if-ne v4, p2, :cond_1

    .line 184
    invoke-virtual {v8}, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->sloppyFreq()F

    move-result v3

    .line 185
    .local v3, "freq":F
    iget-object v9, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;->similarity:Lorg/apache/lucene/search/similarities/Similarity;

    iget-object v10, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;->stats:Lorg/apache/lucene/search/similarities/Similarity$SimWeight;

    invoke-virtual {v9, v10, p1}, Lorg/apache/lucene/search/similarities/Similarity;->sloppySimScorer(Lorg/apache/lucene/search/similarities/Similarity$SimWeight;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;

    move-result-object v0

    .line 186
    .local v0, "docScorer":Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;
    new-instance v1, Lorg/apache/lucene/search/Explanation;

    invoke-direct {v1}, Lorg/apache/lucene/search/Explanation;-><init>()V

    .line 187
    .local v1, "expl":Lorg/apache/lucene/search/Explanation;
    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "weight("

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " in "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ") ["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;->similarity:Lorg/apache/lucene/search/similarities/Similarity;

    invoke-virtual {v10}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "], result of:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v9}, Lorg/apache/lucene/search/Explanation;->setDescription(Ljava/lang/String;)V

    .line 188
    new-instance v9, Lorg/apache/lucene/search/Explanation;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "phraseFreq="

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v3, v10}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    invoke-virtual {v0, p2, v9}, Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;->explain(ILorg/apache/lucene/search/Explanation;)Lorg/apache/lucene/search/Explanation;

    move-result-object v7

    .line 189
    .local v7, "scoreExplanation":Lorg/apache/lucene/search/Explanation;
    invoke-virtual {v1, v7}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 190
    invoke-virtual {v7}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v9

    invoke-virtual {v1, v9}, Lorg/apache/lucene/search/Explanation;->setValue(F)V

    .line 196
    invoke-virtual {p0}, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v9

    check-cast v9, Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v9}, Lorg/apache/lucene/search/spans/SpanQuery;->getField()Ljava/lang/String;

    move-result-object v2

    .line 197
    .local v2, "field":Ljava/lang/String;
    iget-object v9, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;->this$0:Lorg/apache/lucene/search/payloads/PayloadTermQuery;

    iget-object v9, v9, Lorg/apache/lucene/search/payloads/PayloadTermQuery;->function:Lorg/apache/lucene/search/payloads/PayloadFunction;

    iget v10, v8, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->payloadsSeen:I

    iget v11, v8, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->payloadScore:F

    invoke-virtual {v9, p2, v2, v10, v11}, Lorg/apache/lucene/search/payloads/PayloadFunction;->explain(ILjava/lang/String;IF)Lorg/apache/lucene/search/Explanation;

    move-result-object v5

    .line 198
    .local v5, "payloadExpl":Lorg/apache/lucene/search/Explanation;
    invoke-virtual {v8}, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->getPayloadScore()F

    move-result v9

    invoke-virtual {v5, v9}, Lorg/apache/lucene/search/Explanation;->setValue(F)V

    .line 200
    new-instance v6, Lorg/apache/lucene/search/ComplexExplanation;

    invoke-direct {v6}, Lorg/apache/lucene/search/ComplexExplanation;-><init>()V

    .line 201
    .local v6, "result":Lorg/apache/lucene/search/ComplexExplanation;
    iget-object v9, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;->this$0:Lorg/apache/lucene/search/payloads/PayloadTermQuery;

    # getter for: Lorg/apache/lucene/search/payloads/PayloadTermQuery;->includeSpanScore:Z
    invoke-static {v9}, Lorg/apache/lucene/search/payloads/PayloadTermQuery;->access$1(Lorg/apache/lucene/search/payloads/PayloadTermQuery;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 202
    invoke-virtual {v6, v1}, Lorg/apache/lucene/search/ComplexExplanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 203
    invoke-virtual {v6, v5}, Lorg/apache/lucene/search/ComplexExplanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 204
    invoke-virtual {v1}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v9

    invoke-virtual {v5}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v10

    mul-float/2addr v9, v10

    invoke-virtual {v6, v9}, Lorg/apache/lucene/search/ComplexExplanation;->setValue(F)V

    .line 205
    const-string v9, "btq, product of:"

    invoke-virtual {v6, v9}, Lorg/apache/lucene/search/ComplexExplanation;->setDescription(Ljava/lang/String;)V

    .line 211
    :goto_0
    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-virtual {v6, v9}, Lorg/apache/lucene/search/ComplexExplanation;->setMatch(Ljava/lang/Boolean;)V

    .line 216
    .end local v0    # "docScorer":Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;
    .end local v1    # "expl":Lorg/apache/lucene/search/Explanation;
    .end local v2    # "field":Ljava/lang/String;
    .end local v3    # "freq":F
    .end local v4    # "newDoc":I
    .end local v5    # "payloadExpl":Lorg/apache/lucene/search/Explanation;
    .end local v6    # "result":Lorg/apache/lucene/search/ComplexExplanation;
    .end local v7    # "scoreExplanation":Lorg/apache/lucene/search/Explanation;
    :goto_1
    return-object v6

    .line 207
    .restart local v0    # "docScorer":Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;
    .restart local v1    # "expl":Lorg/apache/lucene/search/Explanation;
    .restart local v2    # "field":Ljava/lang/String;
    .restart local v3    # "freq":F
    .restart local v4    # "newDoc":I
    .restart local v5    # "payloadExpl":Lorg/apache/lucene/search/Explanation;
    .restart local v6    # "result":Lorg/apache/lucene/search/ComplexExplanation;
    .restart local v7    # "scoreExplanation":Lorg/apache/lucene/search/Explanation;
    :cond_0
    invoke-virtual {v6, v5}, Lorg/apache/lucene/search/ComplexExplanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 208
    invoke-virtual {v5}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v9

    invoke-virtual {v6, v9}, Lorg/apache/lucene/search/ComplexExplanation;->setValue(F)V

    .line 209
    const-string v9, "btq(includeSpanScore=false), result of:"

    invoke-virtual {v6, v9}, Lorg/apache/lucene/search/ComplexExplanation;->setDescription(Ljava/lang/String;)V

    goto :goto_0

    .line 216
    .end local v0    # "docScorer":Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;
    .end local v1    # "expl":Lorg/apache/lucene/search/Explanation;
    .end local v2    # "field":Ljava/lang/String;
    .end local v3    # "freq":F
    .end local v4    # "newDoc":I
    .end local v5    # "payloadExpl":Lorg/apache/lucene/search/Explanation;
    .end local v6    # "result":Lorg/apache/lucene/search/ComplexExplanation;
    .end local v7    # "scoreExplanation":Lorg/apache/lucene/search/Explanation;
    :cond_1
    new-instance v6, Lorg/apache/lucene/search/ComplexExplanation;

    const/4 v9, 0x0

    const-string v10, "no matching term"

    invoke-direct {v6, v11, v9, v10}, Lorg/apache/lucene/search/ComplexExplanation;-><init>(ZFLjava/lang/String;)V

    goto :goto_1
.end method

.method public scorer(Lorg/apache/lucene/index/AtomicReaderContext;ZZLorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/Scorer;
    .locals 4
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "scoreDocsInOrder"    # Z
    .param p3, "topScorer"    # Z
    .param p4, "acceptDocs"    # Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 84
    new-instance v1, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;

    iget-object v0, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;->query:Lorg/apache/lucene/search/spans/SpanQuery;

    iget-object v2, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;->termContexts:Ljava/util/Map;

    invoke-virtual {v0, p1, p4, v2}, Lorg/apache/lucene/search/spans/SpanQuery;->getSpans(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;Ljava/util/Map;)Lorg/apache/lucene/search/spans/Spans;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/spans/TermSpans;

    .line 85
    iget-object v2, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;->similarity:Lorg/apache/lucene/search/similarities/Similarity;

    iget-object v3, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;->stats:Lorg/apache/lucene/search/similarities/Similarity$SimWeight;

    invoke-virtual {v2, v3, p1}, Lorg/apache/lucene/search/similarities/Similarity;->sloppySimScorer(Lorg/apache/lucene/search/similarities/Similarity$SimWeight;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;

    move-result-object v2

    .line 84
    invoke-direct {v1, p0, v0, p0, v2}, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;-><init>(Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;Lorg/apache/lucene/search/spans/TermSpans;Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;)V

    return-object v1
.end method
