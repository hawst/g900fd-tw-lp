.class public final Lorg/apache/lucene/search/BitsFilteredDocIdSet;
.super Lorg/apache/lucene/search/FilteredDocIdSet;
.source "BitsFilteredDocIdSet.java"


# instance fields
.field private final acceptDocs:Lorg/apache/lucene/util/Bits;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/DocIdSet;Lorg/apache/lucene/util/Bits;)V
    .locals 2
    .param p1, "innerSet"    # Lorg/apache/lucene/search/DocIdSet;
    .param p2, "acceptDocs"    # Lorg/apache/lucene/util/Bits;

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/FilteredDocIdSet;-><init>(Lorg/apache/lucene/search/DocIdSet;)V

    .line 53
    if-nez p2, :cond_0

    .line 54
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "acceptDocs is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 55
    :cond_0
    iput-object p2, p0, Lorg/apache/lucene/search/BitsFilteredDocIdSet;->acceptDocs:Lorg/apache/lucene/util/Bits;

    .line 56
    return-void
.end method

.method public static wrap(Lorg/apache/lucene/search/DocIdSet;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/DocIdSet;
    .locals 1
    .param p0, "set"    # Lorg/apache/lucene/search/DocIdSet;
    .param p1, "acceptDocs"    # Lorg/apache/lucene/util/Bits;

    .prologue
    .line 43
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .end local p0    # "set":Lorg/apache/lucene/search/DocIdSet;
    :cond_0
    :goto_0
    return-object p0

    .restart local p0    # "set":Lorg/apache/lucene/search/DocIdSet;
    :cond_1
    new-instance v0, Lorg/apache/lucene/search/BitsFilteredDocIdSet;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/search/BitsFilteredDocIdSet;-><init>(Lorg/apache/lucene/search/DocIdSet;Lorg/apache/lucene/util/Bits;)V

    move-object p0, v0

    goto :goto_0
.end method


# virtual methods
.method protected match(I)Z
    .locals 1
    .param p1, "docid"    # I

    .prologue
    .line 60
    iget-object v0, p0, Lorg/apache/lucene/search/BitsFilteredDocIdSet;->acceptDocs:Lorg/apache/lucene/util/Bits;

    invoke-interface {v0, p1}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v0

    return v0
.end method
