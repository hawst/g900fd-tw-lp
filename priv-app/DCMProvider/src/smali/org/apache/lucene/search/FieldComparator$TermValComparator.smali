.class public final Lorg/apache/lucene/search/FieldComparator$TermValComparator;
.super Lorg/apache/lucene/search/FieldComparator;
.source "FieldComparator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FieldComparator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TermValComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/FieldComparator",
        "<",
        "Lorg/apache/lucene/util/BytesRef;",
        ">;"
    }
.end annotation


# instance fields
.field private bottom:Lorg/apache/lucene/util/BytesRef;

.field private docTerms:Lorg/apache/lucene/index/BinaryDocValues;

.field private final field:Ljava/lang/String;

.field private final tempBR:Lorg/apache/lucene/util/BytesRef;

.field private values:[Lorg/apache/lucene/util/BytesRef;


# direct methods
.method constructor <init>(ILjava/lang/String;)V
    .locals 1
    .param p1, "numHits"    # I
    .param p2, "field"    # Ljava/lang/String;

    .prologue
    .line 1113
    invoke-direct {p0}, Lorg/apache/lucene/search/FieldComparator;-><init>()V

    .line 1111
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/search/FieldComparator$TermValComparator;->tempBR:Lorg/apache/lucene/util/BytesRef;

    .line 1114
    new-array v0, p1, [Lorg/apache/lucene/util/BytesRef;

    iput-object v0, p0, Lorg/apache/lucene/search/FieldComparator$TermValComparator;->values:[Lorg/apache/lucene/util/BytesRef;

    .line 1115
    iput-object p2, p0, Lorg/apache/lucene/search/FieldComparator$TermValComparator;->field:Ljava/lang/String;

    .line 1116
    return-void
.end method


# virtual methods
.method public compare(II)I
    .locals 3
    .param p1, "slot1"    # I
    .param p2, "slot2"    # I

    .prologue
    .line 1120
    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$TermValComparator;->values:[Lorg/apache/lucene/util/BytesRef;

    aget-object v0, v2, p1

    .line 1121
    .local v0, "val1":Lorg/apache/lucene/util/BytesRef;
    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$TermValComparator;->values:[Lorg/apache/lucene/util/BytesRef;

    aget-object v1, v2, p2

    .line 1122
    .local v1, "val2":Lorg/apache/lucene/util/BytesRef;
    if-nez v0, :cond_1

    .line 1123
    if-nez v1, :cond_0

    .line 1124
    const/4 v2, 0x0

    .line 1131
    :goto_0
    return v2

    .line 1126
    :cond_0
    const/4 v2, -0x1

    goto :goto_0

    .line 1127
    :cond_1
    if-nez v1, :cond_2

    .line 1128
    const/4 v2, 0x1

    goto :goto_0

    .line 1131
    :cond_2
    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/BytesRef;->compareTo(Lorg/apache/lucene/util/BytesRef;)I

    move-result v2

    goto :goto_0
.end method

.method public compareBottom(I)I
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 1136
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$TermValComparator;->docTerms:Lorg/apache/lucene/index/BinaryDocValues;

    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$TermValComparator;->tempBR:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v0, p1, v1}, Lorg/apache/lucene/index/BinaryDocValues;->get(ILorg/apache/lucene/util/BytesRef;)V

    .line 1137
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$TermValComparator;->bottom:Lorg/apache/lucene/util/BytesRef;

    iget-object v0, v0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    sget-object v1, Lorg/apache/lucene/index/BinaryDocValues;->MISSING:[B

    if-ne v0, v1, :cond_1

    .line 1138
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$TermValComparator;->tempBR:Lorg/apache/lucene/util/BytesRef;

    iget-object v0, v0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    sget-object v1, Lorg/apache/lucene/index/BinaryDocValues;->MISSING:[B

    if-ne v0, v1, :cond_0

    .line 1139
    const/4 v0, 0x0

    .line 1145
    :goto_0
    return v0

    .line 1141
    :cond_0
    const/4 v0, -0x1

    goto :goto_0

    .line 1142
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$TermValComparator;->tempBR:Lorg/apache/lucene/util/BytesRef;

    iget-object v0, v0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    sget-object v1, Lorg/apache/lucene/index/BinaryDocValues;->MISSING:[B

    if-ne v0, v1, :cond_2

    .line 1143
    const/4 v0, 0x1

    goto :goto_0

    .line 1145
    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$TermValComparator;->bottom:Lorg/apache/lucene/util/BytesRef;

    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$TermValComparator;->tempBR:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/BytesRef;->compareTo(Lorg/apache/lucene/util/BytesRef;)I

    move-result v0

    goto :goto_0
.end method

.method public bridge synthetic compareDocToValue(ILjava/lang/Object;)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    check-cast p2, Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/search/FieldComparator$TermValComparator;->compareDocToValue(ILorg/apache/lucene/util/BytesRef;)I

    move-result v0

    return v0
.end method

.method public compareDocToValue(ILorg/apache/lucene/util/BytesRef;)I
    .locals 2
    .param p1, "doc"    # I
    .param p2, "value"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 1187
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$TermValComparator;->docTerms:Lorg/apache/lucene/index/BinaryDocValues;

    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$TermValComparator;->tempBR:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v0, p1, v1}, Lorg/apache/lucene/index/BinaryDocValues;->get(ILorg/apache/lucene/util/BytesRef;)V

    .line 1188
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$TermValComparator;->tempBR:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v0, p2}, Lorg/apache/lucene/util/BytesRef;->compareTo(Lorg/apache/lucene/util/BytesRef;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compareValues(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/util/BytesRef;

    check-cast p2, Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/search/FieldComparator$TermValComparator;->compareValues(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)I

    move-result v0

    return v0
.end method

.method public compareValues(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)I
    .locals 1
    .param p1, "val1"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "val2"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 1174
    if-nez p1, :cond_1

    .line 1175
    if-nez p2, :cond_0

    .line 1176
    const/4 v0, 0x0

    .line 1182
    :goto_0
    return v0

    .line 1178
    :cond_0
    const/4 v0, -0x1

    goto :goto_0

    .line 1179
    :cond_1
    if-nez p2, :cond_2

    .line 1180
    const/4 v0, 0x1

    goto :goto_0

    .line 1182
    :cond_2
    invoke-virtual {p1, p2}, Lorg/apache/lucene/util/BytesRef;->compareTo(Lorg/apache/lucene/util/BytesRef;)I

    move-result v0

    goto :goto_0
.end method

.method public copy(II)V
    .locals 2
    .param p1, "slot"    # I
    .param p2, "doc"    # I

    .prologue
    .line 1150
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$TermValComparator;->values:[Lorg/apache/lucene/util/BytesRef;

    aget-object v0, v0, p1

    if-nez v0, :cond_0

    .line 1151
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$TermValComparator;->values:[Lorg/apache/lucene/util/BytesRef;

    new-instance v1, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v1}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    aput-object v1, v0, p1

    .line 1153
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$TermValComparator;->docTerms:Lorg/apache/lucene/index/BinaryDocValues;

    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$TermValComparator;->values:[Lorg/apache/lucene/util/BytesRef;

    aget-object v1, v1, p1

    invoke-virtual {v0, p2, v1}, Lorg/apache/lucene/index/BinaryDocValues;->get(ILorg/apache/lucene/util/BytesRef;)V

    .line 1154
    return-void
.end method

.method public setBottom(I)V
    .locals 1
    .param p1, "bottom"    # I

    .prologue
    .line 1164
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$TermValComparator;->values:[Lorg/apache/lucene/util/BytesRef;

    aget-object v0, v0, p1

    iput-object v0, p0, Lorg/apache/lucene/search/FieldComparator$TermValComparator;->bottom:Lorg/apache/lucene/util/BytesRef;

    .line 1165
    return-void
.end method

.method public setNextReader(Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/search/FieldComparator;
    .locals 3
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/AtomicReaderContext;",
            ")",
            "Lorg/apache/lucene/search/FieldComparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1158
    sget-object v0, Lorg/apache/lucene/search/FieldCache;->DEFAULT:Lorg/apache/lucene/search/FieldCache;

    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$TermValComparator;->field:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lorg/apache/lucene/search/FieldCache;->getTerms(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;)Lorg/apache/lucene/index/BinaryDocValues;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/FieldComparator$TermValComparator;->docTerms:Lorg/apache/lucene/index/BinaryDocValues;

    .line 1159
    return-object p0
.end method

.method public bridge synthetic value(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/FieldComparator$TermValComparator;->value(I)Lorg/apache/lucene/util/BytesRef;

    move-result-object v0

    return-object v0
.end method

.method public value(I)Lorg/apache/lucene/util/BytesRef;
    .locals 1
    .param p1, "slot"    # I

    .prologue
    .line 1169
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$TermValComparator;->values:[Lorg/apache/lucene/util/BytesRef;

    aget-object v0, v0, p1

    return-object v0
.end method
