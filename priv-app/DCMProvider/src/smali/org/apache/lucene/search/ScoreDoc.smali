.class public Lorg/apache/lucene/search/ScoreDoc;
.super Ljava/lang/Object;
.source "ScoreDoc.java"


# instance fields
.field public doc:I

.field public score:F

.field public shardIndex:I


# direct methods
.method public constructor <init>(IF)V
    .locals 1
    .param p1, "doc"    # I
    .param p2, "score"    # F

    .prologue
    .line 36
    const/4 v0, -0x1

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/search/ScoreDoc;-><init>(IFI)V

    .line 37
    return-void
.end method

.method public constructor <init>(IFI)V
    .locals 0
    .param p1, "doc"    # I
    .param p2, "score"    # F
    .param p3, "shardIndex"    # I

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput p1, p0, Lorg/apache/lucene/search/ScoreDoc;->doc:I

    .line 42
    iput p2, p0, Lorg/apache/lucene/search/ScoreDoc;->score:F

    .line 43
    iput p3, p0, Lorg/apache/lucene/search/ScoreDoc;->shardIndex:I

    .line 44
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 49
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "doc="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lorg/apache/lucene/search/ScoreDoc;->doc:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " score="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/search/ScoreDoc;->score:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " shardIndex="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/search/ScoreDoc;->shardIndex:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
