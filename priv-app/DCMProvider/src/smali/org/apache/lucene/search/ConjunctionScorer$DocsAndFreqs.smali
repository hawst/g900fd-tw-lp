.class final Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;
.super Ljava/lang/Object;
.source "ConjunctionScorer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/ConjunctionScorer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "DocsAndFreqs"
.end annotation


# instance fields
.field final cost:J

.field doc:I

.field final scorer:Lorg/apache/lucene/search/Scorer;


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/Scorer;)V
    .locals 2
    .param p1, "scorer"    # Lorg/apache/lucene/search/Scorer;

    .prologue
    .line 137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 135
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;->doc:I

    .line 138
    iput-object p1, p0, Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;->scorer:Lorg/apache/lucene/search/Scorer;

    .line 139
    invoke-virtual {p1}, Lorg/apache/lucene/search/Scorer;->cost()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;->cost:J

    .line 140
    return-void
.end method
