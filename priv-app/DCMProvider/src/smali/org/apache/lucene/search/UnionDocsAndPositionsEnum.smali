.class Lorg/apache/lucene/search/UnionDocsAndPositionsEnum;
.super Lorg/apache/lucene/index/DocsAndPositionsEnum;
.source "MultiPhraseQuery.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$DocsQueue;,
        Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$IntQueue;
    }
.end annotation


# instance fields
.field private _doc:I

.field private _freq:I

.field private _posList:Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$IntQueue;

.field private _queue:Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$DocsQueue;

.field private cost:J


# direct methods
.method public constructor <init>(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/AtomicReaderContext;[Lorg/apache/lucene/index/Term;Ljava/util/Map;Lorg/apache/lucene/index/TermsEnum;)V
    .locals 10
    .param p1, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p2, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p3, "terms"    # [Lorg/apache/lucene/index/Term;
    .param p5, "termsEnum"    # Lorg/apache/lucene/index/TermsEnum;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/Bits;",
            "Lorg/apache/lucene/index/AtomicReaderContext;",
            "[",
            "Lorg/apache/lucene/index/Term;",
            "Ljava/util/Map",
            "<",
            "Lorg/apache/lucene/index/Term;",
            "Lorg/apache/lucene/index/TermContext;",
            ">;",
            "Lorg/apache/lucene/index/TermsEnum;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 477
    .local p4, "termContexts":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/index/Term;Lorg/apache/lucene/index/TermContext;>;"
    invoke-direct {p0}, Lorg/apache/lucene/index/DocsAndPositionsEnum;-><init>()V

    .line 478
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 479
    .local v0, "docsEnums":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/DocsAndPositionsEnum;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v5, p3

    if-lt v1, v5, :cond_0

    .line 496
    new-instance v5, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$DocsQueue;

    invoke-direct {v5, v0}, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$DocsQueue;-><init>(Ljava/util/List;)V

    iput-object v5, p0, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum;->_queue:Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$DocsQueue;

    .line 497
    new-instance v5, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$IntQueue;

    const/4 v6, 0x0

    invoke-direct {v5, v6}, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$IntQueue;-><init>(Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$IntQueue;)V

    iput-object v5, p0, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum;->_posList:Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$IntQueue;

    .line 498
    return-void

    .line 480
    :cond_0
    aget-object v3, p3, v1

    .line 481
    .local v3, "term":Lorg/apache/lucene/index/Term;
    invoke-interface {p4, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/index/TermContext;

    iget v6, p2, Lorg/apache/lucene/index/AtomicReaderContext;->ord:I

    invoke-virtual {v5, v6}, Lorg/apache/lucene/index/TermContext;->get(I)Lorg/apache/lucene/index/TermState;

    move-result-object v4

    .line 482
    .local v4, "termState":Lorg/apache/lucene/index/TermState;
    if-nez v4, :cond_1

    .line 479
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 486
    :cond_1
    invoke-virtual {v3}, Lorg/apache/lucene/index/Term;->bytes()Lorg/apache/lucene/util/BytesRef;

    move-result-object v5

    invoke-virtual {p5, v5, v4}, Lorg/apache/lucene/index/TermsEnum;->seekExact(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/index/TermState;)V

    .line 487
    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {p5, p1, v5, v6}, Lorg/apache/lucene/index/TermsEnum;->docsAndPositions(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsAndPositionsEnum;I)Lorg/apache/lucene/index/DocsAndPositionsEnum;

    move-result-object v2

    .line 488
    .local v2, "postings":Lorg/apache/lucene/index/DocsAndPositionsEnum;
    if-nez v2, :cond_2

    .line 490
    new-instance v5, Ljava/lang/IllegalStateException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "field \""

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\" was indexed without position data; cannot run PhraseQuery (term="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 492
    :cond_2
    iget-wide v6, p0, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum;->cost:J

    invoke-virtual {v2}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->cost()J

    move-result-wide v8

    add-long/2addr v6, v8

    iput-wide v6, p0, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum;->cost:J

    .line 493
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method


# virtual methods
.method public final advance(I)I
    .locals 3
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 557
    :cond_0
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum;->_queue:Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$DocsQueue;

    invoke-virtual {v1}, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$DocsQueue;->top()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum;->_queue:Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$DocsQueue;

    invoke-virtual {v1}, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$DocsQueue;->top()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v1}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->docID()I

    move-result v1

    if-gt p1, v1, :cond_2

    .line 563
    :cond_1
    invoke-virtual {p0}, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum;->nextDoc()I

    move-result v1

    return v1

    .line 558
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum;->_queue:Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$DocsQueue;

    invoke-virtual {v1}, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$DocsQueue;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/DocsAndPositionsEnum;

    .line 559
    .local v0, "postings":Lorg/apache/lucene/index/DocsAndPositionsEnum;
    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->advance(I)I

    move-result v1

    const v2, 0x7fffffff

    if-eq v1, v2, :cond_0

    .line 560
    iget-object v1, p0, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum;->_queue:Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$DocsQueue;

    invoke-virtual {v1, v0}, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$DocsQueue;->add(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public cost()J
    .locals 2

    .prologue
    .line 578
    iget-wide v0, p0, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum;->cost:J

    return-wide v0
.end method

.method public final docID()I
    .locals 1

    .prologue
    .line 573
    iget v0, p0, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum;->_doc:I

    return v0
.end method

.method public endOffset()I
    .locals 1

    .prologue
    .line 547
    const/4 v0, -0x1

    return v0
.end method

.method public final freq()I
    .locals 1

    .prologue
    .line 568
    iget v0, p0, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum;->_freq:I

    return v0
.end method

.method public getPayload()Lorg/apache/lucene/util/BytesRef;
    .locals 1

    .prologue
    .line 552
    const/4 v0, 0x0

    return-object v0
.end method

.method public final nextDoc()I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const v4, 0x7fffffff

    .line 502
    iget-object v3, p0, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum;->_queue:Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$DocsQueue;

    invoke-virtual {v3}, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$DocsQueue;->size()I

    move-result v3

    if-nez v3, :cond_0

    move v3, v4

    .line 532
    :goto_0
    return v3

    .line 509
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum;->_posList:Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$IntQueue;

    invoke-virtual {v3}, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$IntQueue;->clear()V

    .line 510
    iget-object v3, p0, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum;->_queue:Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$DocsQueue;

    invoke-virtual {v3}, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$DocsQueue;->top()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v3}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->docID()I

    move-result v3

    iput v3, p0, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum;->_doc:I

    .line 515
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum;->_queue:Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$DocsQueue;

    invoke-virtual {v3}, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$DocsQueue;->top()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/index/DocsAndPositionsEnum;

    .line 517
    .local v2, "postings":Lorg/apache/lucene/index/DocsAndPositionsEnum;
    invoke-virtual {v2}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->freq()I

    move-result v0

    .line 518
    .local v0, "freq":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-lt v1, v0, :cond_3

    .line 522
    invoke-virtual {v2}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->nextDoc()I

    move-result v3

    if-eq v3, v4, :cond_4

    .line 523
    iget-object v3, p0, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum;->_queue:Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$DocsQueue;

    invoke-virtual {v3}, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$DocsQueue;->updateTop()Ljava/lang/Object;

    .line 527
    :goto_2
    iget-object v3, p0, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum;->_queue:Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$DocsQueue;

    invoke-virtual {v3}, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$DocsQueue;->size()I

    move-result v3

    if-lez v3, :cond_2

    iget-object v3, p0, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum;->_queue:Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$DocsQueue;

    invoke-virtual {v3}, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$DocsQueue;->top()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v3}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->docID()I

    move-result v3

    iget v5, p0, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum;->_doc:I

    if-eq v3, v5, :cond_1

    .line 529
    :cond_2
    iget-object v3, p0, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum;->_posList:Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$IntQueue;

    invoke-virtual {v3}, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$IntQueue;->sort()V

    .line 530
    iget-object v3, p0, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum;->_posList:Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$IntQueue;

    invoke-virtual {v3}, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$IntQueue;->size()I

    move-result v3

    iput v3, p0, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum;->_freq:I

    .line 532
    iget v3, p0, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum;->_doc:I

    goto :goto_0

    .line 519
    :cond_3
    iget-object v3, p0, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum;->_posList:Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$IntQueue;

    invoke-virtual {v2}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->nextPosition()I

    move-result v5

    invoke-virtual {v3, v5}, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$IntQueue;->add(I)V

    .line 518
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 525
    :cond_4
    iget-object v3, p0, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum;->_queue:Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$DocsQueue;

    invoke-virtual {v3}, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$DocsQueue;->pop()Ljava/lang/Object;

    goto :goto_2
.end method

.method public nextPosition()I
    .locals 1

    .prologue
    .line 537
    iget-object v0, p0, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum;->_posList:Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$IntQueue;

    invoke-virtual {v0}, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$IntQueue;->next()I

    move-result v0

    return v0
.end method

.method public startOffset()I
    .locals 1

    .prologue
    .line 542
    const/4 v0, -0x1

    return v0
.end method
