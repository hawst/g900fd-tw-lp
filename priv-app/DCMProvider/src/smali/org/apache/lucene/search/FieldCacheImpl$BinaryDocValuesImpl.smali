.class Lorg/apache/lucene/search/FieldCacheImpl$BinaryDocValuesImpl;
.super Lorg/apache/lucene/index/BinaryDocValues;
.source "FieldCacheImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FieldCacheImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "BinaryDocValuesImpl"
.end annotation


# instance fields
.field private final bytes:Lorg/apache/lucene/util/PagedBytes$Reader;

.field private final docToOffset:Lorg/apache/lucene/util/packed/PackedInts$Reader;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/util/PagedBytes$Reader;Lorg/apache/lucene/util/packed/PackedInts$Reader;)V
    .locals 0
    .param p1, "bytes"    # Lorg/apache/lucene/util/PagedBytes$Reader;
    .param p2, "docToOffset"    # Lorg/apache/lucene/util/packed/PackedInts$Reader;

    .prologue
    .line 1240
    invoke-direct {p0}, Lorg/apache/lucene/index/BinaryDocValues;-><init>()V

    .line 1241
    iput-object p1, p0, Lorg/apache/lucene/search/FieldCacheImpl$BinaryDocValuesImpl;->bytes:Lorg/apache/lucene/util/PagedBytes$Reader;

    .line 1242
    iput-object p2, p0, Lorg/apache/lucene/search/FieldCacheImpl$BinaryDocValuesImpl;->docToOffset:Lorg/apache/lucene/util/packed/PackedInts$Reader;

    .line 1243
    return-void
.end method


# virtual methods
.method public get(ILorg/apache/lucene/util/BytesRef;)V
    .locals 5
    .param p1, "docID"    # I
    .param p2, "ret"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    const/4 v4, 0x0

    .line 1247
    iget-object v1, p0, Lorg/apache/lucene/search/FieldCacheImpl$BinaryDocValuesImpl;->docToOffset:Lorg/apache/lucene/util/packed/PackedInts$Reader;

    invoke-interface {v1, p1}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I)J

    move-result-wide v2

    long-to-int v0, v2

    .line 1248
    .local v0, "pointer":I
    if-nez v0, :cond_0

    .line 1249
    sget-object v1, Lorg/apache/lucene/search/FieldCacheImpl$BinaryDocValuesImpl;->MISSING:[B

    iput-object v1, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 1250
    iput v4, p2, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 1251
    iput v4, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 1255
    :goto_0
    return-void

    .line 1253
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/FieldCacheImpl$BinaryDocValuesImpl;->bytes:Lorg/apache/lucene/util/PagedBytes$Reader;

    int-to-long v2, v0

    invoke-virtual {v1, p2, v2, v3}, Lorg/apache/lucene/util/PagedBytes$Reader;->fill(Lorg/apache/lucene/util/BytesRef;J)V

    goto :goto_0
.end method
