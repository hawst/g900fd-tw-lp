.class public Lorg/apache/lucene/search/similarities/BasicModelG;
.super Lorg/apache/lucene/search/similarities/BasicModel;
.source "BasicModelG.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lorg/apache/lucene/search/similarities/BasicModel;-><init>()V

    return-void
.end method


# virtual methods
.method public final score(Lorg/apache/lucene/search/similarities/BasicStats;F)F
    .locals 12
    .param p1, "stats"    # Lorg/apache/lucene/search/similarities/BasicStats;
    .param p2, "tfn"    # F

    .prologue
    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    .line 36
    invoke-virtual {p1}, Lorg/apache/lucene/search/similarities/BasicStats;->getTotalTermFreq()J

    move-result-wide v6

    const-wide/16 v8, 0x1

    add-long/2addr v6, v8

    long-to-double v0, v6

    .line 37
    .local v0, "F":D
    invoke-virtual {p1}, Lorg/apache/lucene/search/similarities/BasicStats;->getNumberOfDocuments()J

    move-result-wide v6

    long-to-double v2, v6

    .line 38
    .local v2, "N":D
    add-double v6, v2, v0

    div-double v4, v0, v6

    .line 40
    .local v4, "lambda":D
    add-double v6, v4, v10

    invoke-static {v6, v7}, Lorg/apache/lucene/search/similarities/SimilarityBase;->log2(D)D

    move-result-wide v6

    float-to-double v8, p2

    add-double/2addr v10, v4

    div-double/2addr v10, v4

    invoke-static {v10, v11}, Lorg/apache/lucene/search/similarities/SimilarityBase;->log2(D)D

    move-result-wide v10

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    double-to-float v6, v6

    return v6
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    const-string v0, "G"

    return-object v0
.end method
