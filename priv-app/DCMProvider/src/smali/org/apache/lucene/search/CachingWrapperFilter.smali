.class public Lorg/apache/lucene/search/CachingWrapperFilter;
.super Lorg/apache/lucene/search/Filter;
.source "CachingWrapperFilter.java"


# instance fields
.field private final cache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "Lorg/apache/lucene/search/DocIdSet;",
            ">;"
        }
    .end annotation
.end field

.field private final filter:Lorg/apache/lucene/search/Filter;

.field hitCount:I

.field missCount:I


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/Filter;)V
    .locals 1
    .param p1, "filter"    # Lorg/apache/lucene/search/Filter;

    .prologue
    .line 46
    invoke-direct {p0}, Lorg/apache/lucene/search/Filter;-><init>()V

    .line 41
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/CachingWrapperFilter;->cache:Ljava/util/Map;

    .line 47
    iput-object p1, p0, Lorg/apache/lucene/search/CachingWrapperFilter;->filter:Lorg/apache/lucene/search/Filter;

    .line 48
    return-void
.end method


# virtual methods
.method protected docIdSetToCache(Lorg/apache/lucene/search/DocIdSet;Lorg/apache/lucene/index/AtomicReader;)Lorg/apache/lucene/search/DocIdSet;
    .locals 3
    .param p1, "docIdSet"    # Lorg/apache/lucene/search/DocIdSet;
    .param p2, "reader"    # Lorg/apache/lucene/index/AtomicReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 57
    if-nez p1, :cond_1

    .line 59
    sget-object p1, Lorg/apache/lucene/search/DocIdSet;->EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;

    .line 72
    .end local p1    # "docIdSet":Lorg/apache/lucene/search/DocIdSet;
    :cond_0
    :goto_0
    return-object p1

    .line 60
    .restart local p1    # "docIdSet":Lorg/apache/lucene/search/DocIdSet;
    :cond_1
    invoke-virtual {p1}, Lorg/apache/lucene/search/DocIdSet;->isCacheable()Z

    move-result v2

    if-nez v2, :cond_0

    .line 63
    invoke-virtual {p1}, Lorg/apache/lucene/search/DocIdSet;->iterator()Lorg/apache/lucene/search/DocIdSetIterator;

    move-result-object v1

    .line 67
    .local v1, "it":Lorg/apache/lucene/search/DocIdSetIterator;
    if-nez v1, :cond_2

    .line 68
    sget-object p1, Lorg/apache/lucene/search/DocIdSet;->EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;

    goto :goto_0

    .line 70
    :cond_2
    new-instance v0, Lorg/apache/lucene/util/FixedBitSet;

    invoke-virtual {p2}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v2

    invoke-direct {v0, v2}, Lorg/apache/lucene/util/FixedBitSet;-><init>(I)V

    .line 71
    .local v0, "bits":Lorg/apache/lucene/util/FixedBitSet;
    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/FixedBitSet;->or(Lorg/apache/lucene/search/DocIdSetIterator;)V

    move-object p1, v0

    .line 72
    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 104
    instance-of v1, p1, Lorg/apache/lucene/search/CachingWrapperFilter;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    .line 106
    :goto_0
    return v1

    :cond_0
    move-object v0, p1

    .line 105
    check-cast v0, Lorg/apache/lucene/search/CachingWrapperFilter;

    .line 106
    .local v0, "other":Lorg/apache/lucene/search/CachingWrapperFilter;
    iget-object v1, p0, Lorg/apache/lucene/search/CachingWrapperFilter;->filter:Lorg/apache/lucene/search/Filter;

    iget-object v2, v0, Lorg/apache/lucene/search/CachingWrapperFilter;->filter:Lorg/apache/lucene/search/Filter;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getDocIdSet(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/DocIdSet;
    .locals 5
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "acceptDocs"    # Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 82
    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v2

    .line 83
    .local v2, "reader":Lorg/apache/lucene/index/AtomicReader;
    invoke-virtual {v2}, Lorg/apache/lucene/index/AtomicReader;->getCoreCacheKey()Ljava/lang/Object;

    move-result-object v1

    .line 85
    .local v1, "key":Ljava/lang/Object;
    iget-object v3, p0, Lorg/apache/lucene/search/CachingWrapperFilter;->cache:Ljava/util/Map;

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/DocIdSet;

    .line 86
    .local v0, "docIdSet":Lorg/apache/lucene/search/DocIdSet;
    if-eqz v0, :cond_0

    .line 87
    iget v3, p0, Lorg/apache/lucene/search/CachingWrapperFilter;->hitCount:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/lucene/search/CachingWrapperFilter;->hitCount:I

    .line 94
    :goto_0
    invoke-static {v0, p2}, Lorg/apache/lucene/search/BitsFilteredDocIdSet;->wrap(Lorg/apache/lucene/search/DocIdSet;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/DocIdSet;

    move-result-object v3

    return-object v3

    .line 89
    :cond_0
    iget v3, p0, Lorg/apache/lucene/search/CachingWrapperFilter;->missCount:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/lucene/search/CachingWrapperFilter;->missCount:I

    .line 90
    iget-object v3, p0, Lorg/apache/lucene/search/CachingWrapperFilter;->filter:Lorg/apache/lucene/search/Filter;

    const/4 v4, 0x0

    invoke-virtual {v3, p1, v4}, Lorg/apache/lucene/search/Filter;->getDocIdSet(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/DocIdSet;

    move-result-object v3

    invoke-virtual {p0, v3, v2}, Lorg/apache/lucene/search/CachingWrapperFilter;->docIdSetToCache(Lorg/apache/lucene/search/DocIdSet;Lorg/apache/lucene/index/AtomicReader;)Lorg/apache/lucene/search/DocIdSet;

    move-result-object v0

    .line 91
    iget-object v3, p0, Lorg/apache/lucene/search/CachingWrapperFilter;->cache:Ljava/util/Map;

    invoke-interface {v3, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Lorg/apache/lucene/search/CachingWrapperFilter;->filter:Lorg/apache/lucene/search/Filter;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    const v1, 0x1117bf25

    xor-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 99
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CachingWrapperFilter("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/search/CachingWrapperFilter;->filter:Lorg/apache/lucene/search/Filter;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
