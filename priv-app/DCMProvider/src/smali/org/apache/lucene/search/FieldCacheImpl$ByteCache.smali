.class final Lorg/apache/lucene/search/FieldCacheImpl$ByteCache;
.super Lorg/apache/lucene/search/FieldCacheImpl$Cache;
.source "FieldCacheImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FieldCacheImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "ByteCache"
.end annotation


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/FieldCacheImpl;)V
    .locals 0
    .param p1, "wrapper"    # Lorg/apache/lucene/search/FieldCacheImpl;

    .prologue
    .line 404
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/FieldCacheImpl$Cache;-><init>(Lorg/apache/lucene/search/FieldCacheImpl;)V

    .line 405
    return-void
.end method


# virtual methods
.method protected createValue(Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;Z)Ljava/lang/Object;
    .locals 7
    .param p1, "reader"    # Lorg/apache/lucene/index/AtomicReader;
    .param p2, "key"    # Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;
    .param p3, "setDocsWithField"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 411
    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v0

    .line 413
    .local v0, "maxDoc":I
    iget-object v1, p2, Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;->custom:Ljava/lang/Object;

    check-cast v1, Lorg/apache/lucene/search/FieldCache$ByteParser;

    .line 414
    .local v1, "parser":Lorg/apache/lucene/search/FieldCache$ByteParser;
    if-nez v1, :cond_0

    .line 418
    iget-object v4, p0, Lorg/apache/lucene/search/FieldCacheImpl$ByteCache;->wrapper:Lorg/apache/lucene/search/FieldCacheImpl;

    iget-object v5, p2, Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;->field:Ljava/lang/String;

    sget-object v6, Lorg/apache/lucene/search/FieldCacheImpl;->DEFAULT_BYTE_PARSER:Lorg/apache/lucene/search/FieldCache$ByteParser;

    invoke-virtual {v4, p1, v5, v6, p3}, Lorg/apache/lucene/search/FieldCacheImpl;->getBytes(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$ByteParser;Z)Lorg/apache/lucene/search/FieldCache$Bytes;

    move-result-object v4

    .line 448
    :goto_0
    return-object v4

    .line 421
    :cond_0
    new-array v3, v0, [B

    .line 423
    .local v3, "values":[B
    new-instance v2, Lorg/apache/lucene/search/FieldCacheImpl$ByteCache$1;

    invoke-direct {v2, p0, v1, v3}, Lorg/apache/lucene/search/FieldCacheImpl$ByteCache$1;-><init>(Lorg/apache/lucene/search/FieldCacheImpl$ByteCache;Lorg/apache/lucene/search/FieldCache$ByteParser;[B)V

    .line 442
    .local v2, "u":Lorg/apache/lucene/search/FieldCacheImpl$Uninvert;
    iget-object v4, p2, Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;->field:Ljava/lang/String;

    invoke-virtual {v2, p1, v4, p3}, Lorg/apache/lucene/search/FieldCacheImpl$Uninvert;->uninvert(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;Z)V

    .line 444
    if-eqz p3, :cond_1

    .line 445
    iget-object v4, p0, Lorg/apache/lucene/search/FieldCacheImpl$ByteCache;->wrapper:Lorg/apache/lucene/search/FieldCacheImpl;

    iget-object v5, p2, Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;->field:Ljava/lang/String;

    iget-object v6, v2, Lorg/apache/lucene/search/FieldCacheImpl$Uninvert;->docsWithField:Lorg/apache/lucene/util/Bits;

    invoke-virtual {v4, p1, v5, v6}, Lorg/apache/lucene/search/FieldCacheImpl;->setDocsWithField(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;Lorg/apache/lucene/util/Bits;)V

    .line 448
    :cond_1
    new-instance v4, Lorg/apache/lucene/search/FieldCacheImpl$BytesFromArray;

    invoke-direct {v4, v3}, Lorg/apache/lucene/search/FieldCacheImpl$BytesFromArray;-><init>([B)V

    goto :goto_0
.end method
