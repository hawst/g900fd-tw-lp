.class public Lorg/apache/lucene/search/spans/FieldMaskingSpanQuery;
.super Lorg/apache/lucene/search/spans/SpanQuery;
.source "FieldMaskingSpanQuery.java"


# instance fields
.field private field:Ljava/lang/String;

.field private maskedQuery:Lorg/apache/lucene/search/spans/SpanQuery;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/spans/SpanQuery;Ljava/lang/String;)V
    .locals 0
    .param p1, "maskedQuery"    # Lorg/apache/lucene/search/spans/SpanQuery;
    .param p2, "maskedField"    # Ljava/lang/String;

    .prologue
    .line 81
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/SpanQuery;-><init>()V

    .line 82
    iput-object p1, p0, Lorg/apache/lucene/search/spans/FieldMaskingSpanQuery;->maskedQuery:Lorg/apache/lucene/search/spans/SpanQuery;

    .line 83
    iput-object p2, p0, Lorg/apache/lucene/search/spans/FieldMaskingSpanQuery;->field:Ljava/lang/String;

    .line 84
    return-void
.end method


# virtual methods
.method public createWeight(Lorg/apache/lucene/search/IndexSearcher;)Lorg/apache/lucene/search/Weight;
    .locals 1
    .param p1, "searcher"    # Lorg/apache/lucene/search/IndexSearcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 110
    iget-object v0, p0, Lorg/apache/lucene/search/spans/FieldMaskingSpanQuery;->maskedQuery:Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/spans/SpanQuery;->createWeight(Lorg/apache/lucene/search/IndexSearcher;)Lorg/apache/lucene/search/Weight;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 144
    instance-of v2, p1, Lorg/apache/lucene/search/spans/FieldMaskingSpanQuery;

    if-nez v2, :cond_1

    .line 147
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 146
    check-cast v0, Lorg/apache/lucene/search/spans/FieldMaskingSpanQuery;

    .line 147
    .local v0, "other":Lorg/apache/lucene/search/spans/FieldMaskingSpanQuery;
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/FieldMaskingSpanQuery;->getField()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/FieldMaskingSpanQuery;->getField()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 148
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/FieldMaskingSpanQuery;->getBoost()F

    move-result v2

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/FieldMaskingSpanQuery;->getBoost()F

    move-result v3

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    .line 149
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/FieldMaskingSpanQuery;->getMaskedQuery()Lorg/apache/lucene/search/spans/SpanQuery;

    move-result-object v2

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/FieldMaskingSpanQuery;->getMaskedQuery()Lorg/apache/lucene/search/spans/SpanQuery;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/apache/lucene/search/spans/SpanQuery;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 147
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public extractTerms(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lorg/apache/lucene/index/Term;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 105
    .local p1, "terms":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/index/Term;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/spans/FieldMaskingSpanQuery;->maskedQuery:Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/spans/SpanQuery;->extractTerms(Ljava/util/Set;)V

    .line 106
    return-void
.end method

.method public getField()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lorg/apache/lucene/search/spans/FieldMaskingSpanQuery;->field:Ljava/lang/String;

    return-object v0
.end method

.method public getMaskedQuery()Lorg/apache/lucene/search/spans/SpanQuery;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lorg/apache/lucene/search/spans/FieldMaskingSpanQuery;->maskedQuery:Lorg/apache/lucene/search/spans/SpanQuery;

    return-object v0
.end method

.method public getSpans(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;Ljava/util/Map;)Lorg/apache/lucene/search/spans/Spans;
    .locals 1
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "acceptDocs"    # Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/AtomicReaderContext;",
            "Lorg/apache/lucene/util/Bits;",
            "Ljava/util/Map",
            "<",
            "Lorg/apache/lucene/index/Term;",
            "Lorg/apache/lucene/index/TermContext;",
            ">;)",
            "Lorg/apache/lucene/search/spans/Spans;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 100
    .local p3, "termContexts":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/index/Term;Lorg/apache/lucene/index/TermContext;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/spans/FieldMaskingSpanQuery;->maskedQuery:Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/lucene/search/spans/SpanQuery;->getSpans(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;Ljava/util/Map;)Lorg/apache/lucene/search/spans/Spans;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 155
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/FieldMaskingSpanQuery;->getMaskedQuery()Lorg/apache/lucene/search/spans/SpanQuery;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/SpanQuery;->hashCode()I

    move-result v0

    .line 156
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/FieldMaskingSpanQuery;->getField()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    .line 155
    xor-int/2addr v0, v1

    .line 157
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/FieldMaskingSpanQuery;->getBoost()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v1

    .line 155
    xor-int/2addr v0, v1

    return v0
.end method

.method public rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;
    .locals 3
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 115
    const/4 v0, 0x0

    .line 117
    .local v0, "clone":Lorg/apache/lucene/search/spans/FieldMaskingSpanQuery;
    iget-object v2, p0, Lorg/apache/lucene/search/spans/FieldMaskingSpanQuery;->maskedQuery:Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/search/spans/SpanQuery;->rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/search/spans/SpanQuery;

    .line 118
    .local v1, "rewritten":Lorg/apache/lucene/search/spans/SpanQuery;
    iget-object v2, p0, Lorg/apache/lucene/search/spans/FieldMaskingSpanQuery;->maskedQuery:Lorg/apache/lucene/search/spans/SpanQuery;

    if-eq v1, v2, :cond_0

    .line 119
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/FieldMaskingSpanQuery;->clone()Lorg/apache/lucene/search/Query;

    move-result-object v0

    .end local v0    # "clone":Lorg/apache/lucene/search/spans/FieldMaskingSpanQuery;
    check-cast v0, Lorg/apache/lucene/search/spans/FieldMaskingSpanQuery;

    .line 120
    .restart local v0    # "clone":Lorg/apache/lucene/search/spans/FieldMaskingSpanQuery;
    iput-object v1, v0, Lorg/apache/lucene/search/spans/FieldMaskingSpanQuery;->maskedQuery:Lorg/apache/lucene/search/spans/SpanQuery;

    .line 123
    :cond_0
    if-eqz v0, :cond_1

    .line 126
    .end local v0    # "clone":Lorg/apache/lucene/search/spans/FieldMaskingSpanQuery;
    :goto_0
    return-object v0

    .restart local v0    # "clone":Lorg/apache/lucene/search/spans/FieldMaskingSpanQuery;
    :cond_1
    move-object v0, p0

    goto :goto_0
.end method

.method public toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 132
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 133
    .local v0, "buffer":Ljava/lang/StringBuilder;
    const-string v1, "mask("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 134
    iget-object v1, p0, Lorg/apache/lucene/search/spans/FieldMaskingSpanQuery;->maskedQuery:Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/spans/SpanQuery;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 135
    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 136
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/FieldMaskingSpanQuery;->getBoost()F

    move-result v1

    invoke-static {v1}, Lorg/apache/lucene/util/ToStringUtils;->boost(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 137
    const-string v1, " as "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138
    iget-object v1, p0, Lorg/apache/lucene/search/spans/FieldMaskingSpanQuery;->field:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 139
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
