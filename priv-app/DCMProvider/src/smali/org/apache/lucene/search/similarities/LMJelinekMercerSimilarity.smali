.class public Lorg/apache/lucene/search/similarities/LMJelinekMercerSimilarity;
.super Lorg/apache/lucene/search/similarities/LMSimilarity;
.source "LMJelinekMercerSimilarity.java"


# instance fields
.field private final lambda:F


# direct methods
.method public constructor <init>(F)V
    .locals 0
    .param p1, "lambda"    # F

    .prologue
    .line 48
    invoke-direct {p0}, Lorg/apache/lucene/search/similarities/LMSimilarity;-><init>()V

    .line 49
    iput p1, p0, Lorg/apache/lucene/search/similarities/LMJelinekMercerSimilarity;->lambda:F

    .line 50
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/search/similarities/LMSimilarity$CollectionModel;F)V
    .locals 0
    .param p1, "collectionModel"    # Lorg/apache/lucene/search/similarities/LMSimilarity$CollectionModel;
    .param p2, "lambda"    # F

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/similarities/LMSimilarity;-><init>(Lorg/apache/lucene/search/similarities/LMSimilarity$CollectionModel;)V

    .line 44
    iput p2, p0, Lorg/apache/lucene/search/similarities/LMJelinekMercerSimilarity;->lambda:F

    .line 45
    return-void
.end method


# virtual methods
.method protected explain(Lorg/apache/lucene/search/Explanation;Lorg/apache/lucene/search/similarities/BasicStats;IFF)V
    .locals 3
    .param p1, "expl"    # Lorg/apache/lucene/search/Explanation;
    .param p2, "stats"    # Lorg/apache/lucene/search/similarities/BasicStats;
    .param p3, "doc"    # I
    .param p4, "freq"    # F
    .param p5, "docLen"    # F

    .prologue
    .line 63
    invoke-virtual {p2}, Lorg/apache/lucene/search/similarities/BasicStats;->getTotalBoost()F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    .line 64
    new-instance v0, Lorg/apache/lucene/search/Explanation;

    invoke-virtual {p2}, Lorg/apache/lucene/search/similarities/BasicStats;->getTotalBoost()F

    move-result v1

    const-string v2, "boost"

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    invoke-virtual {p1, v0}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 66
    :cond_0
    new-instance v0, Lorg/apache/lucene/search/Explanation;

    iget v1, p0, Lorg/apache/lucene/search/similarities/LMJelinekMercerSimilarity;->lambda:F

    const-string v2, "lambda"

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    invoke-virtual {p1, v0}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 67
    invoke-super/range {p0 .. p5}, Lorg/apache/lucene/search/similarities/LMSimilarity;->explain(Lorg/apache/lucene/search/Explanation;Lorg/apache/lucene/search/similarities/BasicStats;IFF)V

    .line 68
    return-void
.end method

.method public getLambda()F
    .locals 1

    .prologue
    .line 72
    iget v0, p0, Lorg/apache/lucene/search/similarities/LMJelinekMercerSimilarity;->lambda:F

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 5

    .prologue
    .line 77
    sget-object v0, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    const-string v1, "Jelinek-Mercer(%f)"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lorg/apache/lucene/search/similarities/LMJelinekMercerSimilarity;->getLambda()F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected score(Lorg/apache/lucene/search/similarities/BasicStats;FF)F
    .locals 5
    .param p1, "stats"    # Lorg/apache/lucene/search/similarities/BasicStats;
    .param p2, "freq"    # F
    .param p3, "docLen"    # F

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 54
    invoke-virtual {p1}, Lorg/apache/lucene/search/similarities/BasicStats;->getTotalBoost()F

    move-result v0

    .line 56
    iget v1, p0, Lorg/apache/lucene/search/similarities/LMJelinekMercerSimilarity;->lambda:F

    sub-float v1, v4, v1

    mul-float/2addr v1, p2

    div-float/2addr v1, p3

    .line 57
    iget v2, p0, Lorg/apache/lucene/search/similarities/LMJelinekMercerSimilarity;->lambda:F

    check-cast p1, Lorg/apache/lucene/search/similarities/LMSimilarity$LMStats;

    .end local p1    # "stats":Lorg/apache/lucene/search/similarities/BasicStats;
    invoke-virtual {p1}, Lorg/apache/lucene/search/similarities/LMSimilarity$LMStats;->getCollectionProbability()F

    move-result v3

    mul-float/2addr v2, v3

    .line 56
    div-float/2addr v1, v2

    .line 55
    add-float/2addr v1, v4

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    double-to-float v1, v2

    .line 54
    mul-float/2addr v0, v1

    return v0
.end method
