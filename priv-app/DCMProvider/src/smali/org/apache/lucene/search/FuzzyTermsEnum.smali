.class public Lorg/apache/lucene/search/FuzzyTermsEnum;
.super Lorg/apache/lucene/index/TermsEnum;
.source "FuzzyTermsEnum.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/FuzzyTermsEnum$AutomatonFuzzyTermsEnum;,
        Lorg/apache/lucene/search/FuzzyTermsEnum$LevenshteinAutomataAttribute;,
        Lorg/apache/lucene/search/FuzzyTermsEnum$LevenshteinAutomataAttributeImpl;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private actualBoostAtt:Lorg/apache/lucene/search/BoostAttribute;

.field private actualEnum:Lorg/apache/lucene/index/TermsEnum;

.field private final boostAtt:Lorg/apache/lucene/search/BoostAttribute;

.field private bottom:F

.field private bottomTerm:Lorg/apache/lucene/util/BytesRef;

.field private final dfaAtt:Lorg/apache/lucene/search/FuzzyTermsEnum$LevenshteinAutomataAttribute;

.field private final maxBoostAtt:Lorg/apache/lucene/search/MaxNonCompetitiveBoostAttribute;

.field protected maxEdits:I

.field protected final minSimilarity:F

.field private queuedBottom:Lorg/apache/lucene/util/BytesRef;

.field protected final raw:Z

.field protected final realPrefixLength:I

.field protected final scale_factor:F

.field private final term:Lorg/apache/lucene/index/Term;

.field private final termComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation
.end field

.field protected final termLength:I

.field protected final termText:[I

.field protected final terms:Lorg/apache/lucene/index/Terms;

.field private final transpositions:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const-class v0, Lorg/apache/lucene/search/FuzzyTermsEnum;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/FuzzyTermsEnum;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/index/Terms;Lorg/apache/lucene/util/AttributeSource;Lorg/apache/lucene/index/Term;FIZ)V
    .locals 8
    .param p1, "terms"    # Lorg/apache/lucene/index/Terms;
    .param p2, "atts"    # Lorg/apache/lucene/util/AttributeSource;
    .param p3, "term"    # Lorg/apache/lucene/index/Term;
    .param p4, "minSimilarity"    # F
    .param p5, "prefixLength"    # I
    .param p6, "transpositions"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 101
    invoke-direct {p0}, Lorg/apache/lucene/index/TermsEnum;-><init>()V

    .line 57
    invoke-virtual {p0}, Lorg/apache/lucene/search/FuzzyTermsEnum;->attributes()Lorg/apache/lucene/util/AttributeSource;

    move-result-object v5

    const-class v6, Lorg/apache/lucene/search/BoostAttribute;

    invoke-virtual {v5, v6}, Lorg/apache/lucene/util/AttributeSource;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/search/BoostAttribute;

    iput-object v5, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->boostAtt:Lorg/apache/lucene/search/BoostAttribute;

    .line 66
    invoke-static {}, Lorg/apache/lucene/util/BytesRef;->getUTF8SortedAsUnicodeComparator()Ljava/util/Comparator;

    move-result-object v5

    iput-object v5, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->termComparator:Ljava/util/Comparator;

    .line 238
    const/4 v5, 0x0

    iput-object v5, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->queuedBottom:Lorg/apache/lucene/util/BytesRef;

    .line 103
    const/high16 v5, 0x3f800000    # 1.0f

    cmpl-float v5, p4, v5

    if-ltz v5, :cond_0

    float-to-int v5, p4

    int-to-float v5, v5

    cmpl-float v5, p4, v5

    if-eqz v5, :cond_0

    .line 104
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "fractional edit distances are not allowed"

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 105
    :cond_0
    const/4 v5, 0x0

    cmpg-float v5, p4, v5

    if-gez v5, :cond_1

    .line 106
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "minimumSimilarity cannot be less than 0"

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 107
    :cond_1
    if-gez p5, :cond_2

    .line 108
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "prefixLength cannot be less than 0"

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 109
    :cond_2
    iput-object p1, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->terms:Lorg/apache/lucene/index/Terms;

    .line 110
    iput-object p3, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->term:Lorg/apache/lucene/index/Term;

    .line 113
    invoke-virtual {p3}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v4

    .line 114
    .local v4, "utf16":Ljava/lang/String;
    const/4 v5, 0x0

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->codePointCount(II)I

    move-result v5

    new-array v5, v5, [I

    iput-object v5, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->termText:[I

    .line 115
    const/4 v1, 0x0

    .local v1, "i":I
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_0
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-lt v1, v5, :cond_4

    .line 117
    iget-object v5, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->termText:[I

    array-length v5, v5

    iput v5, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->termLength:I

    .line 118
    const-class v5, Lorg/apache/lucene/search/FuzzyTermsEnum$LevenshteinAutomataAttribute;

    invoke-virtual {p2, v5}, Lorg/apache/lucene/util/AttributeSource;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/search/FuzzyTermsEnum$LevenshteinAutomataAttribute;

    iput-object v5, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->dfaAtt:Lorg/apache/lucene/search/FuzzyTermsEnum$LevenshteinAutomataAttribute;

    .line 122
    iget v5, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->termLength:I

    if-le p5, v5, :cond_3

    iget p5, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->termLength:I

    .end local p5    # "prefixLength":I
    :cond_3
    iput p5, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->realPrefixLength:I

    .line 124
    const/high16 v5, 0x3f800000    # 1.0f

    cmpl-float v5, p4, v5

    if-ltz v5, :cond_5

    .line 125
    const/4 v5, 0x0

    iput v5, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->minSimilarity:F

    .line 126
    float-to-int v5, p4

    iput v5, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->maxEdits:I

    .line 127
    const/4 v5, 0x1

    iput-boolean v5, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->raw:Z

    .line 134
    :goto_1
    if-eqz p6, :cond_6

    iget v5, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->maxEdits:I

    const/4 v6, 0x2

    if-le v5, v6, :cond_6

    .line 135
    new-instance v5, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v6, "with transpositions enabled, distances > 2 are not supported "

    invoke-direct {v5, v6}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 116
    .restart local p5    # "prefixLength":I
    :cond_4
    iget-object v5, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->termText:[I

    add-int/lit8 v3, v2, 0x1

    .end local v2    # "j":I
    .local v3, "j":I
    invoke-virtual {v4, v1}, Ljava/lang/String;->codePointAt(I)I

    move-result v0

    .local v0, "cp":I
    aput v0, v5, v2

    .line 115
    invoke-static {v0}, Ljava/lang/Character;->charCount(I)I

    move-result v5

    add-int/2addr v1, v5

    move v2, v3

    .end local v3    # "j":I
    .restart local v2    # "j":I
    goto :goto_0

    .line 129
    .end local v0    # "cp":I
    .end local p5    # "prefixLength":I
    :cond_5
    iput p4, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->minSimilarity:F

    .line 131
    iget v5, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->minSimilarity:F

    iget v6, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->termLength:I

    invoke-direct {p0, v5, v6}, Lorg/apache/lucene/search/FuzzyTermsEnum;->initialMaxDistance(FI)I

    move-result v5

    iput v5, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->maxEdits:I

    .line 132
    const/4 v5, 0x0

    iput-boolean v5, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->raw:Z

    goto :goto_1

    .line 138
    :cond_6
    iput-boolean p6, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->transpositions:Z

    .line 139
    const/high16 v5, 0x3f800000    # 1.0f

    const/high16 v6, 0x3f800000    # 1.0f

    iget v7, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->minSimilarity:F

    sub-float/2addr v6, v7

    div-float/2addr v5, v6

    iput v5, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->scale_factor:F

    .line 141
    const-class v5, Lorg/apache/lucene/search/MaxNonCompetitiveBoostAttribute;

    invoke-virtual {p2, v5}, Lorg/apache/lucene/util/AttributeSource;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/search/MaxNonCompetitiveBoostAttribute;

    iput-object v5, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->maxBoostAtt:Lorg/apache/lucene/search/MaxNonCompetitiveBoostAttribute;

    .line 142
    iget-object v5, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->maxBoostAtt:Lorg/apache/lucene/search/MaxNonCompetitiveBoostAttribute;

    invoke-interface {v5}, Lorg/apache/lucene/search/MaxNonCompetitiveBoostAttribute;->getMaxNonCompetitiveBoost()F

    move-result v5

    iput v5, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->bottom:F

    .line 143
    iget-object v5, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->maxBoostAtt:Lorg/apache/lucene/search/MaxNonCompetitiveBoostAttribute;

    invoke-interface {v5}, Lorg/apache/lucene/search/MaxNonCompetitiveBoostAttribute;->getCompetitiveTerm()Lorg/apache/lucene/util/BytesRef;

    move-result-object v5

    iput-object v5, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->bottomTerm:Lorg/apache/lucene/util/BytesRef;

    .line 144
    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-direct {p0, v5, v6}, Lorg/apache/lucene/search/FuzzyTermsEnum;->bottomChanged(Lorg/apache/lucene/util/BytesRef;Z)V

    .line 145
    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/search/FuzzyTermsEnum;)Lorg/apache/lucene/index/Term;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->term:Lorg/apache/lucene/index/Term;

    return-object v0
.end method

.method private bottomChanged(Lorg/apache/lucene/util/BytesRef;Z)V
    .locals 4
    .param p1, "lastTerm"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "init"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 200
    iget v0, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->maxEdits:I

    .line 203
    .local v0, "oldMaxEdits":I
    iget-object v2, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->bottomTerm:Lorg/apache/lucene/util/BytesRef;

    if-eqz v2, :cond_4

    if-eqz p1, :cond_0

    iget-object v2, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->termComparator:Ljava/util/Comparator;

    iget-object v3, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->bottomTerm:Lorg/apache/lucene/util/BytesRef;

    invoke-interface {v2, p1, v3}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v2

    if-gez v2, :cond_4

    :cond_0
    const/4 v1, 0x0

    .line 207
    .local v1, "termAfter":Z
    :goto_0
    iget v2, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->maxEdits:I

    if-lez v2, :cond_1

    if-eqz v1, :cond_6

    iget v2, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->bottom:F

    iget v3, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->maxEdits:I

    invoke-direct {p0, v3}, Lorg/apache/lucene/search/FuzzyTermsEnum;->calculateMaxBoost(I)F

    move-result v3

    cmpl-float v2, v2, v3

    if-gez v2, :cond_5

    .line 210
    :cond_1
    :goto_1
    iget v2, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->maxEdits:I

    if-ne v0, v2, :cond_2

    if-eqz p2, :cond_3

    .line 211
    :cond_2
    iget v2, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->maxEdits:I

    invoke-virtual {p0, p1, v2, p2}, Lorg/apache/lucene/search/FuzzyTermsEnum;->maxEditDistanceChanged(Lorg/apache/lucene/util/BytesRef;IZ)V

    .line 213
    :cond_3
    return-void

    .line 203
    .end local v1    # "termAfter":Z
    :cond_4
    const/4 v1, 0x1

    goto :goto_0

    .line 208
    .restart local v1    # "termAfter":Z
    :cond_5
    iget v2, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->maxEdits:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->maxEdits:I

    goto :goto_0

    .line 207
    :cond_6
    iget v2, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->bottom:F

    iget v3, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->maxEdits:I

    invoke-direct {p0, v3}, Lorg/apache/lucene/search/FuzzyTermsEnum;->calculateMaxBoost(I)F

    move-result v3

    cmpl-float v2, v2, v3

    if-gtz v2, :cond_5

    goto :goto_1
.end method

.method private calculateMaxBoost(I)F
    .locals 4
    .param p1, "nEdits"    # I

    .prologue
    .line 234
    const/high16 v1, 0x3f800000    # 1.0f

    int-to-float v2, p1

    iget v3, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->termLength:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    sub-float v0, v1, v2

    .line 235
    .local v0, "similarity":F
    iget v1, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->minSimilarity:F

    sub-float v1, v0, v1

    iget v2, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->scale_factor:F

    mul-float/2addr v1, v2

    return v1
.end method

.method private initAutomata(I)Ljava/util/List;
    .locals 10
    .param p1, "maxDistance"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/util/automaton/CompiledAutomaton;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 166
    iget-object v5, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->dfaAtt:Lorg/apache/lucene/search/FuzzyTermsEnum$LevenshteinAutomataAttribute;

    invoke-interface {v5}, Lorg/apache/lucene/search/FuzzyTermsEnum$LevenshteinAutomataAttribute;->automata()Ljava/util/List;

    move-result-object v4

    .line 168
    .local v4, "runAutomata":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/util/automaton/CompiledAutomaton;>;"
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    if-gt v5, p1, :cond_0

    .line 169
    const/4 v5, 0x2

    if-gt p1, v5, :cond_0

    .line 171
    new-instance v1, Lorg/apache/lucene/util/automaton/LevenshteinAutomata;

    iget-object v5, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->termText:[I

    iget v6, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->realPrefixLength:I

    iget-object v7, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->termText:[I

    array-length v7, v7

    iget v8, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->realPrefixLength:I

    sub-int/2addr v7, v8

    invoke-static {v5, v6, v7}, Lorg/apache/lucene/util/UnicodeUtil;->newString([III)Ljava/lang/String;

    move-result-object v5

    iget-boolean v6, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->transpositions:Z

    invoke-direct {v1, v5, v6}, Lorg/apache/lucene/util/automaton/LevenshteinAutomata;-><init>(Ljava/lang/String;Z)V

    .line 173
    .local v1, "builder":Lorg/apache/lucene/util/automaton/LevenshteinAutomata;
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v2

    .local v2, "i":I
    :goto_0
    if-le v2, p1, :cond_1

    .line 185
    .end local v1    # "builder":Lorg/apache/lucene/util/automaton/LevenshteinAutomata;
    .end local v2    # "i":I
    :cond_0
    return-object v4

    .line 174
    .restart local v1    # "builder":Lorg/apache/lucene/util/automaton/LevenshteinAutomata;
    .restart local v2    # "i":I
    :cond_1
    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/automaton/LevenshteinAutomata;->toAutomaton(I)Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v0

    .line 177
    .local v0, "a":Lorg/apache/lucene/util/automaton/Automaton;
    iget v5, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->realPrefixLength:I

    if-lez v5, :cond_2

    .line 179
    iget-object v5, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->termText:[I

    iget v6, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->realPrefixLength:I

    invoke-static {v5, v9, v6}, Lorg/apache/lucene/util/UnicodeUtil;->newString([III)Ljava/lang/String;

    move-result-object v5

    .line 178
    invoke-static {v5}, Lorg/apache/lucene/util/automaton/BasicAutomata;->makeString(Ljava/lang/String;)Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v3

    .line 180
    .local v3, "prefix":Lorg/apache/lucene/util/automaton/Automaton;
    invoke-static {v3, v0}, Lorg/apache/lucene/util/automaton/BasicOperations;->concatenate(Lorg/apache/lucene/util/automaton/Automaton;Lorg/apache/lucene/util/automaton/Automaton;)Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v0

    .line 182
    .end local v3    # "prefix":Lorg/apache/lucene/util/automaton/Automaton;
    :cond_2
    new-instance v5, Lorg/apache/lucene/util/automaton/CompiledAutomaton;

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-direct {v5, v0, v6, v9}, Lorg/apache/lucene/util/automaton/CompiledAutomaton;-><init>(Lorg/apache/lucene/util/automaton/Automaton;Ljava/lang/Boolean;Z)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 173
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private initialMaxDistance(FI)I
    .locals 4
    .param p1, "minimumSimilarity"    # F
    .param p2, "termLen"    # I

    .prologue
    .line 229
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    float-to-double v2, p1

    sub-double/2addr v0, v2

    int-to-double v2, p2

    mul-double/2addr v0, v2

    double-to-int v0, v0

    return v0
.end method


# virtual methods
.method public docFreq()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 266
    iget-object v0, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->actualEnum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermsEnum;->docFreq()I

    move-result v0

    return v0
.end method

.method public docs(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;I)Lorg/apache/lucene/index/DocsEnum;
    .locals 1
    .param p1, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p2, "reuse"    # Lorg/apache/lucene/index/DocsEnum;
    .param p3, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 276
    iget-object v0, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->actualEnum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/lucene/index/TermsEnum;->docs(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;I)Lorg/apache/lucene/index/DocsEnum;

    move-result-object v0

    return-object v0
.end method

.method public docsAndPositions(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsAndPositionsEnum;I)Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .locals 1
    .param p1, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p2, "reuse"    # Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .param p3, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 282
    iget-object v0, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->actualEnum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/lucene/index/TermsEnum;->docsAndPositions(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsAndPositionsEnum;I)Lorg/apache/lucene/index/DocsAndPositionsEnum;

    move-result-object v0

    return-object v0
.end method

.method protected getAutomatonEnum(ILorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/index/TermsEnum;
    .locals 6
    .param p1, "editDistance"    # I
    .param p2, "lastTerm"    # Lorg/apache/lucene/util/BytesRef;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 153
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/FuzzyTermsEnum;->initAutomata(I)Ljava/util/List;

    move-result-object v1

    .line 154
    .local v1, "runAutomata":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/util/automaton/CompiledAutomaton;>;"
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-ge p1, v3, :cond_0

    .line 156
    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;

    .line 157
    .local v0, "compiled":Lorg/apache/lucene/util/automaton/CompiledAutomaton;
    new-instance v3, Lorg/apache/lucene/search/FuzzyTermsEnum$AutomatonFuzzyTermsEnum;

    iget-object v4, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->terms:Lorg/apache/lucene/index/Terms;

    if-nez p2, :cond_1

    :goto_0
    invoke-virtual {v4, v0, v2}, Lorg/apache/lucene/index/Terms;->intersect(Lorg/apache/lucene/util/automaton/CompiledAutomaton;Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v4

    .line 158
    const/4 v2, 0x0

    add-int/lit8 v5, p1, 0x1

    invoke-interface {v1, v2, v5}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v2

    add-int/lit8 v5, p1, 0x1

    new-array v5, v5, [Lorg/apache/lucene/util/automaton/CompiledAutomaton;

    invoke-interface {v2, v5}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lorg/apache/lucene/util/automaton/CompiledAutomaton;

    .line 157
    invoke-direct {v3, p0, v4, v2}, Lorg/apache/lucene/search/FuzzyTermsEnum$AutomatonFuzzyTermsEnum;-><init>(Lorg/apache/lucene/search/FuzzyTermsEnum;Lorg/apache/lucene/index/TermsEnum;[Lorg/apache/lucene/util/automaton/CompiledAutomaton;)V

    move-object v2, v3

    .line 160
    .end local v0    # "compiled":Lorg/apache/lucene/util/automaton/CompiledAutomaton;
    :cond_0
    return-object v2

    .line 157
    .restart local v0    # "compiled":Lorg/apache/lucene/util/automaton/CompiledAutomaton;
    :cond_1
    new-instance v2, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v2}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    invoke-virtual {v0, p2, v2}, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->floor(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/BytesRef;

    move-result-object v2

    goto :goto_0
.end method

.method public getComparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation

    .prologue
    .line 297
    iget-object v0, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->actualEnum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermsEnum;->getComparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public getMinSimilarity()F
    .locals 1

    .prologue
    .line 393
    iget v0, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->minSimilarity:F

    return v0
.end method

.method public getScaleFactor()F
    .locals 1

    .prologue
    .line 398
    iget v0, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->scale_factor:F

    return v0
.end method

.method protected maxEditDistanceChanged(Lorg/apache/lucene/util/BytesRef;IZ)V
    .locals 3
    .param p1, "lastTerm"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "maxEdits"    # I
    .param p3, "init"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 217
    invoke-virtual {p0, p2, p1}, Lorg/apache/lucene/search/FuzzyTermsEnum;->getAutomatonEnum(ILorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v0

    .line 220
    .local v0, "newEnum":Lorg/apache/lucene/index/TermsEnum;
    if-nez v0, :cond_1

    .line 221
    sget-boolean v1, Lorg/apache/lucene/search/FuzzyTermsEnum;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    const/4 v1, 0x2

    if-gt p2, v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 222
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "maxEdits cannot be > LevenshteinAutomata.MAXIMUM_SUPPORTED_DISTANCE"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 224
    :cond_1
    invoke-virtual {p0, v0}, Lorg/apache/lucene/search/FuzzyTermsEnum;->setEnum(Lorg/apache/lucene/index/TermsEnum;)V

    .line 225
    return-void
.end method

.method public next()Lorg/apache/lucene/util/BytesRef;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 242
    iget-object v3, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->queuedBottom:Lorg/apache/lucene/util/BytesRef;

    if-eqz v3, :cond_0

    .line 243
    iget-object v3, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->queuedBottom:Lorg/apache/lucene/util/BytesRef;

    const/4 v4, 0x0

    invoke-direct {p0, v3, v4}, Lorg/apache/lucene/search/FuzzyTermsEnum;->bottomChanged(Lorg/apache/lucene/util/BytesRef;Z)V

    .line 244
    const/4 v3, 0x0

    iput-object v3, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->queuedBottom:Lorg/apache/lucene/util/BytesRef;

    .line 247
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->actualEnum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v3}, Lorg/apache/lucene/index/TermsEnum;->next()Lorg/apache/lucene/util/BytesRef;

    move-result-object v2

    .line 248
    .local v2, "term":Lorg/apache/lucene/util/BytesRef;
    iget-object v3, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->boostAtt:Lorg/apache/lucene/search/BoostAttribute;

    iget-object v4, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->actualBoostAtt:Lorg/apache/lucene/search/BoostAttribute;

    invoke-interface {v4}, Lorg/apache/lucene/search/BoostAttribute;->getBoost()F

    move-result v4

    invoke-interface {v3, v4}, Lorg/apache/lucene/search/BoostAttribute;->setBoost(F)V

    .line 250
    iget-object v3, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->maxBoostAtt:Lorg/apache/lucene/search/MaxNonCompetitiveBoostAttribute;

    invoke-interface {v3}, Lorg/apache/lucene/search/MaxNonCompetitiveBoostAttribute;->getMaxNonCompetitiveBoost()F

    move-result v0

    .line 251
    .local v0, "bottom":F
    iget-object v3, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->maxBoostAtt:Lorg/apache/lucene/search/MaxNonCompetitiveBoostAttribute;

    invoke-interface {v3}, Lorg/apache/lucene/search/MaxNonCompetitiveBoostAttribute;->getCompetitiveTerm()Lorg/apache/lucene/util/BytesRef;

    move-result-object v1

    .line 252
    .local v1, "bottomTerm":Lorg/apache/lucene/util/BytesRef;
    if-eqz v2, :cond_2

    iget v3, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->bottom:F

    cmpl-float v3, v0, v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->bottomTerm:Lorg/apache/lucene/util/BytesRef;

    if-eq v1, v3, :cond_2

    .line 253
    :cond_1
    iput v0, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->bottom:F

    .line 254
    iput-object v1, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->bottomTerm:Lorg/apache/lucene/util/BytesRef;

    .line 257
    invoke-static {v2}, Lorg/apache/lucene/util/BytesRef;->deepCopyOf(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/BytesRef;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->queuedBottom:Lorg/apache/lucene/util/BytesRef;

    .line 260
    :cond_2
    return-object v2
.end method

.method public ord()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 302
    iget-object v0, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->actualEnum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermsEnum;->ord()J

    move-result-wide v0

    return-wide v0
.end method

.method public seekCeil(Lorg/apache/lucene/util/BytesRef;Z)Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    .locals 1
    .param p1, "text"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "useCache"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 312
    iget-object v0, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->actualEnum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/index/TermsEnum;->seekCeil(Lorg/apache/lucene/util/BytesRef;Z)Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    move-result-object v0

    return-object v0
.end method

.method public seekExact(J)V
    .locals 1
    .param p1, "ord"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 317
    iget-object v0, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->actualEnum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/index/TermsEnum;->seekExact(J)V

    .line 318
    return-void
.end method

.method public seekExact(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/index/TermState;)V
    .locals 1
    .param p1, "term"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "state"    # Lorg/apache/lucene/index/TermState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 287
    iget-object v0, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->actualEnum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/index/TermsEnum;->seekExact(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/index/TermState;)V

    .line 288
    return-void
.end method

.method public seekExact(Lorg/apache/lucene/util/BytesRef;Z)Z
    .locals 1
    .param p1, "text"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "useCache"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 307
    iget-object v0, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->actualEnum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/index/TermsEnum;->seekExact(Lorg/apache/lucene/util/BytesRef;Z)Z

    move-result v0

    return v0
.end method

.method protected setEnum(Lorg/apache/lucene/index/TermsEnum;)V
    .locals 2
    .param p1, "actualEnum"    # Lorg/apache/lucene/index/TermsEnum;

    .prologue
    .line 190
    iput-object p1, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->actualEnum:Lorg/apache/lucene/index/TermsEnum;

    .line 191
    invoke-virtual {p1}, Lorg/apache/lucene/index/TermsEnum;->attributes()Lorg/apache/lucene/util/AttributeSource;

    move-result-object v0

    const-class v1, Lorg/apache/lucene/search/BoostAttribute;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/AttributeSource;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/BoostAttribute;

    iput-object v0, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->actualBoostAtt:Lorg/apache/lucene/search/BoostAttribute;

    .line 192
    return-void
.end method

.method public term()Lorg/apache/lucene/util/BytesRef;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 322
    iget-object v0, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->actualEnum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermsEnum;->term()Lorg/apache/lucene/util/BytesRef;

    move-result-object v0

    return-object v0
.end method

.method public termState()Lorg/apache/lucene/index/TermState;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 292
    iget-object v0, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->actualEnum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermsEnum;->termState()Lorg/apache/lucene/index/TermState;

    move-result-object v0

    return-object v0
.end method

.method public totalTermFreq()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 271
    iget-object v0, p0, Lorg/apache/lucene/search/FuzzyTermsEnum;->actualEnum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermsEnum;->totalTermFreq()J

    move-result-wide v0

    return-wide v0
.end method
