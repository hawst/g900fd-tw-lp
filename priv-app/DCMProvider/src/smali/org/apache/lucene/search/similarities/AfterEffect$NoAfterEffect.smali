.class public final Lorg/apache/lucene/search/similarities/AfterEffect$NoAfterEffect;
.super Lorg/apache/lucene/search/similarities/AfterEffect;
.source "AfterEffect.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/similarities/AfterEffect;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NoAfterEffect"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Lorg/apache/lucene/search/similarities/AfterEffect;-><init>()V

    return-void
.end method


# virtual methods
.method public final explain(Lorg/apache/lucene/search/similarities/BasicStats;F)Lorg/apache/lucene/search/Explanation;
    .locals 3
    .param p1, "stats"    # Lorg/apache/lucene/search/similarities/BasicStats;
    .param p2, "tfn"    # F

    .prologue
    .line 59
    new-instance v0, Lorg/apache/lucene/search/Explanation;

    const/high16 v1, 0x3f800000    # 1.0f

    const-string v2, "no aftereffect"

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    return-object v0
.end method

.method public final score(Lorg/apache/lucene/search/similarities/BasicStats;F)F
    .locals 1
    .param p1, "stats"    # Lorg/apache/lucene/search/similarities/BasicStats;
    .param p2, "tfn"    # F

    .prologue
    .line 54
    const/high16 v0, 0x3f800000    # 1.0f

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    const-string v0, ""

    return-object v0
.end method
