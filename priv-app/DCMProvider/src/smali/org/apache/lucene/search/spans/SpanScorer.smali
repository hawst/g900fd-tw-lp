.class public Lorg/apache/lucene/search/spans/SpanScorer;
.super Lorg/apache/lucene/search/Scorer;
.source "SpanScorer.java"


# instance fields
.field protected doc:I

.field protected final docScorer:Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;

.field protected freq:F

.field protected more:Z

.field protected numMatches:I

.field protected spans:Lorg/apache/lucene/search/spans/Spans;


# direct methods
.method protected constructor <init>(Lorg/apache/lucene/search/spans/Spans;Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;)V
    .locals 1
    .param p1, "spans"    # Lorg/apache/lucene/search/spans/Spans;
    .param p2, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p3, "docScorer"    # Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0, p2}, Lorg/apache/lucene/search/Scorer;-><init>(Lorg/apache/lucene/search/Weight;)V

    .line 32
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/search/spans/SpanScorer;->more:Z

    .line 42
    iput-object p3, p0, Lorg/apache/lucene/search/spans/SpanScorer;->docScorer:Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;

    .line 43
    iput-object p1, p0, Lorg/apache/lucene/search/spans/SpanScorer;->spans:Lorg/apache/lucene/search/spans/Spans;

    .line 45
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/search/spans/SpanScorer;->doc:I

    .line 46
    invoke-virtual {p1}, Lorg/apache/lucene/search/spans/Spans;->next()Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/search/spans/SpanScorer;->more:Z

    .line 47
    return-void
.end method


# virtual methods
.method public advance(I)I
    .locals 2
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const v0, 0x7fffffff

    .line 59
    iget-boolean v1, p0, Lorg/apache/lucene/search/spans/SpanScorer;->more:Z

    if-nez v1, :cond_0

    .line 60
    iput v0, p0, Lorg/apache/lucene/search/spans/SpanScorer;->doc:I

    .line 68
    :goto_0
    return v0

    .line 62
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanScorer;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v1

    if-ge v1, p1, :cond_1

    .line 63
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanScorer;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/spans/Spans;->skipTo(I)Z

    move-result v1

    iput-boolean v1, p0, Lorg/apache/lucene/search/spans/SpanScorer;->more:Z

    .line 65
    :cond_1
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanScorer;->setFreqCurrentDoc()Z

    move-result v1

    if-nez v1, :cond_2

    .line 66
    iput v0, p0, Lorg/apache/lucene/search/spans/SpanScorer;->doc:I

    .line 68
    :cond_2
    iget v0, p0, Lorg/apache/lucene/search/spans/SpanScorer;->doc:I

    goto :goto_0
.end method

.method public cost()J
    .locals 2

    .prologue
    .line 109
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanScorer;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->cost()J

    move-result-wide v0

    return-wide v0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 88
    iget v0, p0, Lorg/apache/lucene/search/spans/SpanScorer;->doc:I

    return v0
.end method

.method public freq()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 97
    iget v0, p0, Lorg/apache/lucene/search/spans/SpanScorer;->numMatches:I

    return v0
.end method

.method public nextDoc()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 51
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanScorer;->setFreqCurrentDoc()Z

    move-result v0

    if-nez v0, :cond_0

    .line 52
    const v0, 0x7fffffff

    iput v0, p0, Lorg/apache/lucene/search/spans/SpanScorer;->doc:I

    .line 54
    :cond_0
    iget v0, p0, Lorg/apache/lucene/search/spans/SpanScorer;->doc:I

    return v0
.end method

.method public score()F
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 92
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanScorer;->docScorer:Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;

    iget v1, p0, Lorg/apache/lucene/search/spans/SpanScorer;->doc:I

    iget v2, p0, Lorg/apache/lucene/search/spans/SpanScorer;->freq:F

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;->score(IF)F

    move-result v0

    return v0
.end method

.method protected setFreqCurrentDoc()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 72
    iget-boolean v2, p0, Lorg/apache/lucene/search/spans/SpanScorer;->more:Z

    if-nez v2, :cond_0

    .line 84
    :goto_0
    return v1

    .line 75
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/search/spans/SpanScorer;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v2}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v2

    iput v2, p0, Lorg/apache/lucene/search/spans/SpanScorer;->doc:I

    .line 76
    const/4 v2, 0x0

    iput v2, p0, Lorg/apache/lucene/search/spans/SpanScorer;->freq:F

    .line 77
    iput v1, p0, Lorg/apache/lucene/search/spans/SpanScorer;->numMatches:I

    .line 79
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanScorer;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/Spans;->end()I

    move-result v1

    iget-object v2, p0, Lorg/apache/lucene/search/spans/SpanScorer;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v2}, Lorg/apache/lucene/search/spans/Spans;->start()I

    move-result v2

    sub-int v0, v1, v2

    .line 80
    .local v0, "matchLength":I
    iget v1, p0, Lorg/apache/lucene/search/spans/SpanScorer;->freq:F

    iget-object v2, p0, Lorg/apache/lucene/search/spans/SpanScorer;->docScorer:Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;

    invoke-virtual {v2, v0}, Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;->computeSlopFactor(I)F

    move-result v2

    add-float/2addr v1, v2

    iput v1, p0, Lorg/apache/lucene/search/spans/SpanScorer;->freq:F

    .line 81
    iget v1, p0, Lorg/apache/lucene/search/spans/SpanScorer;->numMatches:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/search/spans/SpanScorer;->numMatches:I

    .line 82
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanScorer;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/Spans;->next()Z

    move-result v1

    iput-boolean v1, p0, Lorg/apache/lucene/search/spans/SpanScorer;->more:Z

    .line 83
    iget-boolean v1, p0, Lorg/apache/lucene/search/spans/SpanScorer;->more:Z

    if-eqz v1, :cond_2

    iget v1, p0, Lorg/apache/lucene/search/spans/SpanScorer;->doc:I

    iget-object v2, p0, Lorg/apache/lucene/search/spans/SpanScorer;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v2}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v2

    if-eq v1, v2, :cond_1

    .line 84
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public sloppyFreq()F
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 104
    iget v0, p0, Lorg/apache/lucene/search/spans/SpanScorer;->freq:F

    return v0
.end method
