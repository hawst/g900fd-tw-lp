.class Lorg/apache/lucene/search/similarities/MultiSimilarity$MultiSloppyDocScorer;
.super Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;
.source "MultiSimilarity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/similarities/MultiSimilarity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "MultiSloppyDocScorer"
.end annotation


# instance fields
.field private final subScorers:[Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;


# direct methods
.method constructor <init>([Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;)V
    .locals 0
    .param p1, "subScorers"    # [Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;

    .prologue
    .line 106
    invoke-direct {p0}, Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;-><init>()V

    .line 107
    iput-object p1, p0, Lorg/apache/lucene/search/similarities/MultiSimilarity$MultiSloppyDocScorer;->subScorers:[Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;

    .line 108
    return-void
.end method


# virtual methods
.method public computePayloadFactor(IIILorg/apache/lucene/util/BytesRef;)F
    .locals 2
    .param p1, "doc"    # I
    .param p2, "start"    # I
    .param p3, "end"    # I
    .param p4, "payload"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 135
    iget-object v0, p0, Lorg/apache/lucene/search/similarities/MultiSimilarity$MultiSloppyDocScorer;->subScorers:[Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0, p1, p2, p3, p4}, Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;->computePayloadFactor(IIILorg/apache/lucene/util/BytesRef;)F

    move-result v0

    return v0
.end method

.method public computeSlopFactor(I)F
    .locals 2
    .param p1, "distance"    # I

    .prologue
    .line 130
    iget-object v0, p0, Lorg/apache/lucene/search/similarities/MultiSimilarity$MultiSloppyDocScorer;->subScorers:[Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;->computeSlopFactor(I)F

    move-result v0

    return v0
.end method

.method public explain(ILorg/apache/lucene/search/Explanation;)Lorg/apache/lucene/search/Explanation;
    .locals 6
    .param p1, "doc"    # I
    .param p2, "freq"    # Lorg/apache/lucene/search/Explanation;

    .prologue
    .line 121
    new-instance v0, Lorg/apache/lucene/search/Explanation;

    invoke-virtual {p2}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v2

    invoke-virtual {p0, p1, v2}, Lorg/apache/lucene/search/similarities/MultiSimilarity$MultiSloppyDocScorer;->score(IF)F

    move-result v2

    const-string v3, "sum of:"

    invoke-direct {v0, v2, v3}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    .line 122
    .local v0, "expl":Lorg/apache/lucene/search/Explanation;
    iget-object v3, p0, Lorg/apache/lucene/search/similarities/MultiSimilarity$MultiSloppyDocScorer;->subScorers:[Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v4, :cond_0

    .line 125
    return-object v0

    .line 122
    :cond_0
    aget-object v1, v3, v2

    .line 123
    .local v1, "subScorer":Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;
    invoke-virtual {v1, p1, p2}, Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;->explain(ILorg/apache/lucene/search/Explanation;)Lorg/apache/lucene/search/Explanation;

    move-result-object v5

    invoke-virtual {v0, v5}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 122
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public score(IF)F
    .locals 6
    .param p1, "doc"    # I
    .param p2, "freq"    # F

    .prologue
    .line 112
    const/4 v1, 0x0

    .line 113
    .local v1, "sum":F
    iget-object v3, p0, Lorg/apache/lucene/search/similarities/MultiSimilarity$MultiSloppyDocScorer;->subScorers:[Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v4, :cond_0

    .line 116
    return v1

    .line 113
    :cond_0
    aget-object v0, v3, v2

    .line 114
    .local v0, "subScorer":Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;
    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;->score(IF)F

    move-result v5

    add-float/2addr v1, v5

    .line 113
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method
