.class final Lorg/apache/lucene/search/similarities/TFIDFSimilarity$SloppyTFIDFDocScorer;
.super Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;
.source "TFIDFSimilarity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/similarities/TFIDFSimilarity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SloppyTFIDFDocScorer"
.end annotation


# instance fields
.field private final norms:Lorg/apache/lucene/index/NumericDocValues;

.field private final stats:Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;

.field final synthetic this$0:Lorg/apache/lucene/search/similarities/TFIDFSimilarity;

.field private final weightValue:F


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/similarities/TFIDFSimilarity;Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;Lorg/apache/lucene/index/NumericDocValues;)V
    .locals 1
    .param p2, "stats"    # Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;
    .param p3, "norms"    # Lorg/apache/lucene/index/NumericDocValues;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 800
    iput-object p1, p0, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$SloppyTFIDFDocScorer;->this$0:Lorg/apache/lucene/search/similarities/TFIDFSimilarity;

    invoke-direct {p0}, Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;-><init>()V

    .line 801
    iput-object p2, p0, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$SloppyTFIDFDocScorer;->stats:Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;

    .line 802
    # getter for: Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;->value:F
    invoke-static {p2}, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;->access$0(Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;)F

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$SloppyTFIDFDocScorer;->weightValue:F

    .line 803
    iput-object p3, p0, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$SloppyTFIDFDocScorer;->norms:Lorg/apache/lucene/index/NumericDocValues;

    .line 804
    return-void
.end method


# virtual methods
.method public computePayloadFactor(IIILorg/apache/lucene/util/BytesRef;)F
    .locals 1
    .param p1, "doc"    # I
    .param p2, "start"    # I
    .param p3, "end"    # I
    .param p4, "payload"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 820
    iget-object v0, p0, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$SloppyTFIDFDocScorer;->this$0:Lorg/apache/lucene/search/similarities/TFIDFSimilarity;

    invoke-virtual {v0, p1, p2, p3, p4}, Lorg/apache/lucene/search/similarities/TFIDFSimilarity;->scorePayload(IIILorg/apache/lucene/util/BytesRef;)F

    move-result v0

    return v0
.end method

.method public computeSlopFactor(I)F
    .locals 1
    .param p1, "distance"    # I

    .prologue
    .line 815
    iget-object v0, p0, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$SloppyTFIDFDocScorer;->this$0:Lorg/apache/lucene/search/similarities/TFIDFSimilarity;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/similarities/TFIDFSimilarity;->sloppyFreq(I)F

    move-result v0

    return v0
.end method

.method public explain(ILorg/apache/lucene/search/Explanation;)Lorg/apache/lucene/search/Explanation;
    .locals 3
    .param p1, "doc"    # I
    .param p2, "freq"    # Lorg/apache/lucene/search/Explanation;

    .prologue
    .line 825
    iget-object v0, p0, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$SloppyTFIDFDocScorer;->this$0:Lorg/apache/lucene/search/similarities/TFIDFSimilarity;

    iget-object v1, p0, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$SloppyTFIDFDocScorer;->stats:Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;

    iget-object v2, p0, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$SloppyTFIDFDocScorer;->norms:Lorg/apache/lucene/index/NumericDocValues;

    # invokes: Lorg/apache/lucene/search/similarities/TFIDFSimilarity;->explainScore(ILorg/apache/lucene/search/Explanation;Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;Lorg/apache/lucene/index/NumericDocValues;)Lorg/apache/lucene/search/Explanation;
    invoke-static {v0, p1, p2, v1, v2}, Lorg/apache/lucene/search/similarities/TFIDFSimilarity;->access$0(Lorg/apache/lucene/search/similarities/TFIDFSimilarity;ILorg/apache/lucene/search/Explanation;Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;Lorg/apache/lucene/index/NumericDocValues;)Lorg/apache/lucene/search/Explanation;

    move-result-object v0

    return-object v0
.end method

.method public score(IF)F
    .locals 4
    .param p1, "doc"    # I
    .param p2, "freq"    # F

    .prologue
    .line 808
    iget-object v1, p0, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$SloppyTFIDFDocScorer;->this$0:Lorg/apache/lucene/search/similarities/TFIDFSimilarity;

    invoke-virtual {v1, p2}, Lorg/apache/lucene/search/similarities/TFIDFSimilarity;->tf(F)F

    move-result v1

    iget v2, p0, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$SloppyTFIDFDocScorer;->weightValue:F

    mul-float v0, v1, v2

    .line 810
    .local v0, "raw":F
    iget-object v1, p0, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$SloppyTFIDFDocScorer;->norms:Lorg/apache/lucene/index/NumericDocValues;

    if-nez v1, :cond_0

    .end local v0    # "raw":F
    :goto_0
    return v0

    .restart local v0    # "raw":F
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$SloppyTFIDFDocScorer;->this$0:Lorg/apache/lucene/search/similarities/TFIDFSimilarity;

    iget-object v2, p0, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$SloppyTFIDFDocScorer;->norms:Lorg/apache/lucene/index/NumericDocValues;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/index/NumericDocValues;->get(I)J

    move-result-wide v2

    long-to-int v2, v2

    int-to-byte v2, v2

    invoke-virtual {v1, v2}, Lorg/apache/lucene/search/similarities/TFIDFSimilarity;->decodeNormValue(B)F

    move-result v1

    mul-float/2addr v0, v1

    goto :goto_0
.end method
