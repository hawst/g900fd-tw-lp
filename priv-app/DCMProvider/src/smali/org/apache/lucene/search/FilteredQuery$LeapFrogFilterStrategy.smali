.class final Lorg/apache/lucene/search/FilteredQuery$LeapFrogFilterStrategy;
.super Lorg/apache/lucene/search/FilteredQuery$FilterStrategy;
.source "FilteredQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FilteredQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "LeapFrogFilterStrategy"
.end annotation


# instance fields
.field private final scorerFirst:Z


# direct methods
.method private constructor <init>(Z)V
    .locals 0
    .param p1, "scorerFirst"    # Z

    .prologue
    .line 566
    invoke-direct {p0}, Lorg/apache/lucene/search/FilteredQuery$FilterStrategy;-><init>()V

    .line 567
    iput-boolean p1, p0, Lorg/apache/lucene/search/FilteredQuery$LeapFrogFilterStrategy;->scorerFirst:Z

    .line 568
    return-void
.end method

.method synthetic constructor <init>(ZLorg/apache/lucene/search/FilteredQuery$LeapFrogFilterStrategy;)V
    .locals 0

    .prologue
    .line 566
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/FilteredQuery$LeapFrogFilterStrategy;-><init>(Z)V

    return-void
.end method


# virtual methods
.method public filteredScorer(Lorg/apache/lucene/index/AtomicReaderContext;ZZLorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/DocIdSet;)Lorg/apache/lucene/search/Scorer;
    .locals 5
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "scoreDocsInOrder"    # Z
    .param p3, "topScorer"    # Z
    .param p4, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p5, "docIdSet"    # Lorg/apache/lucene/search/DocIdSet;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 573
    invoke-virtual {p5}, Lorg/apache/lucene/search/DocIdSet;->iterator()Lorg/apache/lucene/search/DocIdSetIterator;

    move-result-object v0

    .line 574
    .local v0, "filterIter":Lorg/apache/lucene/search/DocIdSetIterator;
    if-nez v0, :cond_1

    .line 584
    :cond_0
    :goto_0
    return-object v2

    .line 580
    :cond_1
    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {p4, p1, v3, v4, v2}, Lorg/apache/lucene/search/Weight;->scorer(Lorg/apache/lucene/index/AtomicReaderContext;ZZLorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/Scorer;

    move-result-object v1

    .line 581
    .local v1, "scorer":Lorg/apache/lucene/search/Scorer;
    iget-boolean v3, p0, Lorg/apache/lucene/search/FilteredQuery$LeapFrogFilterStrategy;->scorerFirst:Z

    if-eqz v3, :cond_2

    .line 582
    if-eqz v1, :cond_0

    new-instance v2, Lorg/apache/lucene/search/FilteredQuery$LeapFrogScorer;

    invoke-direct {v2, p4, v1, v0, v1}, Lorg/apache/lucene/search/FilteredQuery$LeapFrogScorer;-><init>(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/DocIdSetIterator;Lorg/apache/lucene/search/DocIdSetIterator;Lorg/apache/lucene/search/Scorer;)V

    goto :goto_0

    .line 584
    :cond_2
    if-eqz v1, :cond_0

    new-instance v2, Lorg/apache/lucene/search/FilteredQuery$LeapFrogScorer;

    invoke-direct {v2, p4, v0, v1, v1}, Lorg/apache/lucene/search/FilteredQuery$LeapFrogScorer;-><init>(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/DocIdSetIterator;Lorg/apache/lucene/search/DocIdSetIterator;Lorg/apache/lucene/search/Scorer;)V

    goto :goto_0
.end method
