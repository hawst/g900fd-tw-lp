.class Lorg/apache/lucene/search/DocTermOrdsRangeFilter$1$1;
.super Lorg/apache/lucene/search/FieldCacheDocIdSet;
.source "DocTermOrdsRangeFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/DocTermOrdsRangeFilter$1;->getDocIdSet(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/DocIdSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lorg/apache/lucene/search/DocTermOrdsRangeFilter$1;

.field private final synthetic val$docTermOrds:Lorg/apache/lucene/index/SortedSetDocValues;

.field private final synthetic val$inclusiveLowerPoint:J

.field private final synthetic val$inclusiveUpperPoint:J


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/DocTermOrdsRangeFilter$1;ILorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/SortedSetDocValues;JJ)V
    .locals 1
    .param p2, "$anonymous0"    # I
    .param p3, "$anonymous1"    # Lorg/apache/lucene/util/Bits;

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/search/DocTermOrdsRangeFilter$1$1;->this$1:Lorg/apache/lucene/search/DocTermOrdsRangeFilter$1;

    iput-object p4, p0, Lorg/apache/lucene/search/DocTermOrdsRangeFilter$1$1;->val$docTermOrds:Lorg/apache/lucene/index/SortedSetDocValues;

    iput-wide p5, p0, Lorg/apache/lucene/search/DocTermOrdsRangeFilter$1$1;->val$inclusiveUpperPoint:J

    iput-wide p7, p0, Lorg/apache/lucene/search/DocTermOrdsRangeFilter$1$1;->val$inclusiveLowerPoint:J

    .line 98
    invoke-direct {p0, p2, p3}, Lorg/apache/lucene/search/FieldCacheDocIdSet;-><init>(ILorg/apache/lucene/util/Bits;)V

    return-void
.end method


# virtual methods
.method protected final matchDoc(I)Z
    .locals 6
    .param p1, "doc"    # I

    .prologue
    const/4 v2, 0x0

    .line 101
    iget-object v3, p0, Lorg/apache/lucene/search/DocTermOrdsRangeFilter$1$1;->val$docTermOrds:Lorg/apache/lucene/index/SortedSetDocValues;

    invoke-virtual {v3, p1}, Lorg/apache/lucene/index/SortedSetDocValues;->setDocument(I)V

    .line 103
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/search/DocTermOrdsRangeFilter$1$1;->val$docTermOrds:Lorg/apache/lucene/index/SortedSetDocValues;

    invoke-virtual {v3}, Lorg/apache/lucene/index/SortedSetDocValues;->nextOrd()J

    move-result-wide v0

    .local v0, "ord":J
    const-wide/16 v4, -0x1

    cmp-long v3, v0, v4

    if-nez v3, :cond_2

    .line 110
    :cond_1
    :goto_0
    return v2

    .line 104
    :cond_2
    iget-wide v4, p0, Lorg/apache/lucene/search/DocTermOrdsRangeFilter$1$1;->val$inclusiveUpperPoint:J

    cmp-long v3, v0, v4

    if-gtz v3, :cond_1

    .line 106
    iget-wide v4, p0, Lorg/apache/lucene/search/DocTermOrdsRangeFilter$1$1;->val$inclusiveLowerPoint:J

    cmp-long v3, v0, v4

    if-ltz v3, :cond_0

    .line 107
    const/4 v2, 0x1

    goto :goto_0
.end method
