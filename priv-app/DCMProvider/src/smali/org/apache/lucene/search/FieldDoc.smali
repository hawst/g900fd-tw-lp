.class public Lorg/apache/lucene/search/FieldDoc;
.super Lorg/apache/lucene/search/ScoreDoc;
.source "FieldDoc.java"


# instance fields
.field public fields:[Ljava/lang/Object;


# direct methods
.method public constructor <init>(IF)V
    .locals 0
    .param p1, "doc"    # I
    .param p2, "score"    # F

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/search/ScoreDoc;-><init>(IF)V

    .line 54
    return-void
.end method

.method public constructor <init>(IF[Ljava/lang/Object;)V
    .locals 0
    .param p1, "doc"    # I
    .param p2, "score"    # F
    .param p3, "fields"    # [Ljava/lang/Object;

    .prologue
    .line 58
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/search/ScoreDoc;-><init>(IF)V

    .line 59
    iput-object p3, p0, Lorg/apache/lucene/search/FieldDoc;->fields:[Ljava/lang/Object;

    .line 60
    return-void
.end method

.method public constructor <init>(IF[Ljava/lang/Object;I)V
    .locals 0
    .param p1, "doc"    # I
    .param p2, "score"    # F
    .param p3, "fields"    # [Ljava/lang/Object;
    .param p4, "shardIndex"    # I

    .prologue
    .line 64
    invoke-direct {p0, p1, p2, p4}, Lorg/apache/lucene/search/ScoreDoc;-><init>(IFI)V

    .line 65
    iput-object p3, p0, Lorg/apache/lucene/search/FieldDoc;->fields:[Ljava/lang/Object;

    .line 66
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 73
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-super {p0}, Lorg/apache/lucene/search/ScoreDoc;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 74
    .local v1, "sb":Ljava/lang/StringBuilder;
    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 75
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/search/FieldDoc;->fields:[Ljava/lang/Object;

    array-length v2, v2

    if-lt v0, v2, :cond_0

    .line 78
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 79
    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 76
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/search/FieldDoc;->fields:[Ljava/lang/Object;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 75
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
