.class abstract Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator$PerSegmentComparator;
.super Lorg/apache/lucene/search/FieldComparator;
.source "FieldComparator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x400
    name = "PerSegmentComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/FieldComparator",
        "<",
        "Lorg/apache/lucene/util/BytesRef;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;)V
    .locals 0

    .prologue
    .line 966
    iput-object p1, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator$PerSegmentComparator;->this$1:Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;

    invoke-direct {p0}, Lorg/apache/lucene/search/FieldComparator;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(II)I
    .locals 1
    .param p1, "slot1"    # I
    .param p2, "slot2"    # I

    .prologue
    .line 975
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator$PerSegmentComparator;->this$1:Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->compare(II)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compareDocToValue(ILjava/lang/Object;)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    check-cast p2, Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator$PerSegmentComparator;->compareDocToValue(ILorg/apache/lucene/util/BytesRef;)I

    move-result v0

    return v0
.end method

.method public compareDocToValue(ILorg/apache/lucene/util/BytesRef;)I
    .locals 1
    .param p1, "doc"    # I
    .param p2, "value"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 1003
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator$PerSegmentComparator;->this$1:Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->compareDocToValue(ILorg/apache/lucene/util/BytesRef;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compareValues(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/util/BytesRef;

    check-cast p2, Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator$PerSegmentComparator;->compareValues(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)I

    move-result v0

    return v0
.end method

.method public compareValues(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)I
    .locals 1
    .param p1, "val1"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "val2"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 990
    if-nez p1, :cond_1

    .line 991
    if-nez p2, :cond_0

    .line 992
    const/4 v0, 0x0

    .line 998
    :goto_0
    return v0

    .line 994
    :cond_0
    const/4 v0, -0x1

    goto :goto_0

    .line 995
    :cond_1
    if-nez p2, :cond_2

    .line 996
    const/4 v0, 0x1

    goto :goto_0

    .line 998
    :cond_2
    invoke-virtual {p1, p2}, Lorg/apache/lucene/util/BytesRef;->compareTo(Lorg/apache/lucene/util/BytesRef;)I

    move-result v0

    goto :goto_0
.end method

.method public setBottom(I)V
    .locals 1
    .param p1, "bottom"    # I

    .prologue
    .line 980
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator$PerSegmentComparator;->this$1:Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->setBottom(I)V

    .line 981
    return-void
.end method

.method public setNextReader(Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/search/FieldComparator;
    .locals 1
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/AtomicReaderContext;",
            ")",
            "Lorg/apache/lucene/search/FieldComparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 970
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator$PerSegmentComparator;->this$1:Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->setNextReader(Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/search/FieldComparator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic value(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator$PerSegmentComparator;->value(I)Lorg/apache/lucene/util/BytesRef;

    move-result-object v0

    return-object v0
.end method

.method public value(I)Lorg/apache/lucene/util/BytesRef;
    .locals 1
    .param p1, "slot"    # I

    .prologue
    .line 985
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator$PerSegmentComparator;->this$1:Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;->value(I)Lorg/apache/lucene/util/BytesRef;

    move-result-object v0

    return-object v0
.end method
