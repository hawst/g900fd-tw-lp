.class public final Lorg/apache/lucene/search/FieldCache$CacheEntry;
.super Ljava/lang/Object;
.source "FieldCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FieldCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CacheEntry"
.end annotation


# instance fields
.field private final cacheType:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private final custom:Ljava/lang/Object;

.field private final fieldName:Ljava/lang/String;

.field private final readerKey:Ljava/lang/Object;

.field private size:Ljava/lang/String;

.field private final value:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "readerKey"    # Ljava/lang/Object;
    .param p2, "fieldName"    # Ljava/lang/String;
    .param p4, "custom"    # Ljava/lang/Object;
    .param p5, "value"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 690
    .local p3, "cacheType":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 694
    iput-object p1, p0, Lorg/apache/lucene/search/FieldCache$CacheEntry;->readerKey:Ljava/lang/Object;

    .line 695
    iput-object p2, p0, Lorg/apache/lucene/search/FieldCache$CacheEntry;->fieldName:Ljava/lang/String;

    .line 696
    iput-object p3, p0, Lorg/apache/lucene/search/FieldCache$CacheEntry;->cacheType:Ljava/lang/Class;

    .line 697
    iput-object p4, p0, Lorg/apache/lucene/search/FieldCache$CacheEntry;->custom:Ljava/lang/Object;

    .line 698
    iput-object p5, p0, Lorg/apache/lucene/search/FieldCache$CacheEntry;->value:Ljava/lang/Object;

    .line 699
    return-void
.end method


# virtual methods
.method public estimateSize()V
    .locals 3

    .prologue
    .line 726
    invoke-virtual {p0}, Lorg/apache/lucene/search/FieldCache$CacheEntry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Lorg/apache/lucene/util/RamUsageEstimator;->sizeOf(Ljava/lang/Object;)J

    move-result-wide v0

    .line 727
    .local v0, "bytesUsed":J
    invoke-static {v0, v1}, Lorg/apache/lucene/util/RamUsageEstimator;->humanReadableUnits(J)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/lucene/search/FieldCache$CacheEntry;->size:Ljava/lang/String;

    .line 728
    return-void
.end method

.method public getCacheType()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 710
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCache$CacheEntry;->cacheType:Ljava/lang/Class;

    return-object v0
.end method

.method public getCustom()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 714
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCache$CacheEntry;->custom:Ljava/lang/Object;

    return-object v0
.end method

.method public getEstimatedSize()Ljava/lang/String;
    .locals 1

    .prologue
    .line 735
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCache$CacheEntry;->size:Ljava/lang/String;

    return-object v0
.end method

.method public getFieldName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 706
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCache$CacheEntry;->fieldName:Ljava/lang/String;

    return-object v0
.end method

.method public getReaderKey()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 702
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCache$CacheEntry;->readerKey:Ljava/lang/Object;

    return-object v0
.end method

.method public getValue()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 718
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCache$CacheEntry;->value:Ljava/lang/Object;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 740
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 741
    .local v0, "b":Ljava/lang/StringBuilder;
    const-string v2, "\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lorg/apache/lucene/search/FieldCache$CacheEntry;->getReaderKey()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'=>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 742
    const-string v2, "\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lorg/apache/lucene/search/FieldCache$CacheEntry;->getFieldName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\',"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 743
    invoke-virtual {p0}, Lorg/apache/lucene/search/FieldCache$CacheEntry;->getCacheType()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lorg/apache/lucene/search/FieldCache$CacheEntry;->getCustom()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 744
    const-string v2, "=>"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lorg/apache/lucene/search/FieldCache$CacheEntry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 745
    invoke-virtual {p0}, Lorg/apache/lucene/search/FieldCache$CacheEntry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 747
    invoke-virtual {p0}, Lorg/apache/lucene/search/FieldCache$CacheEntry;->getEstimatedSize()Ljava/lang/String;

    move-result-object v1

    .line 748
    .local v1, "s":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 749
    const-string v2, " (size =~ "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x29

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 752
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
