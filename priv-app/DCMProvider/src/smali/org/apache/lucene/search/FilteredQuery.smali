.class public Lorg/apache/lucene/search/FilteredQuery;
.super Lorg/apache/lucene/search/Query;
.source "FilteredQuery.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/FilteredQuery$FilterStrategy;,
        Lorg/apache/lucene/search/FilteredQuery$LeapFrogFilterStrategy;,
        Lorg/apache/lucene/search/FilteredQuery$LeapFrogScorer;,
        Lorg/apache/lucene/search/FilteredQuery$PrimaryAdvancedLeapFrogScorer;,
        Lorg/apache/lucene/search/FilteredQuery$QueryFirstFilterStrategy;,
        Lorg/apache/lucene/search/FilteredQuery$QueryFirstScorer;,
        Lorg/apache/lucene/search/FilteredQuery$RandomAccessFilterStrategy;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final LEAP_FROG_FILTER_FIRST_STRATEGY:Lorg/apache/lucene/search/FilteredQuery$FilterStrategy;

.field public static final LEAP_FROG_QUERY_FIRST_STRATEGY:Lorg/apache/lucene/search/FilteredQuery$FilterStrategy;

.field public static final QUERY_FIRST_FILTER_STRATEGY:Lorg/apache/lucene/search/FilteredQuery$FilterStrategy;

.field public static final RANDOM_ACCESS_FILTER_STRATEGY:Lorg/apache/lucene/search/FilteredQuery$FilterStrategy;


# instance fields
.field private final filter:Lorg/apache/lucene/search/Filter;

.field private final query:Lorg/apache/lucene/search/Query;

.field private final strategy:Lorg/apache/lucene/search/FilteredQuery$FilterStrategy;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 41
    const-class v0, Lorg/apache/lucene/search/FilteredQuery;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/FilteredQuery;->$assertionsDisabled:Z

    .line 433
    new-instance v0, Lorg/apache/lucene/search/FilteredQuery$RandomAccessFilterStrategy;

    invoke-direct {v0}, Lorg/apache/lucene/search/FilteredQuery$RandomAccessFilterStrategy;-><init>()V

    sput-object v0, Lorg/apache/lucene/search/FilteredQuery;->RANDOM_ACCESS_FILTER_STRATEGY:Lorg/apache/lucene/search/FilteredQuery$FilterStrategy;

    .line 445
    new-instance v0, Lorg/apache/lucene/search/FilteredQuery$LeapFrogFilterStrategy;

    invoke-direct {v0, v2, v3}, Lorg/apache/lucene/search/FilteredQuery$LeapFrogFilterStrategy;-><init>(ZLorg/apache/lucene/search/FilteredQuery$LeapFrogFilterStrategy;)V

    sput-object v0, Lorg/apache/lucene/search/FilteredQuery;->LEAP_FROG_FILTER_FIRST_STRATEGY:Lorg/apache/lucene/search/FilteredQuery$FilterStrategy;

    .line 457
    new-instance v0, Lorg/apache/lucene/search/FilteredQuery$LeapFrogFilterStrategy;

    invoke-direct {v0, v1, v3}, Lorg/apache/lucene/search/FilteredQuery$LeapFrogFilterStrategy;-><init>(ZLorg/apache/lucene/search/FilteredQuery$LeapFrogFilterStrategy;)V

    sput-object v0, Lorg/apache/lucene/search/FilteredQuery;->LEAP_FROG_QUERY_FIRST_STRATEGY:Lorg/apache/lucene/search/FilteredQuery$FilterStrategy;

    .line 472
    new-instance v0, Lorg/apache/lucene/search/FilteredQuery$QueryFirstFilterStrategy;

    invoke-direct {v0, v3}, Lorg/apache/lucene/search/FilteredQuery$QueryFirstFilterStrategy;-><init>(Lorg/apache/lucene/search/FilteredQuery$QueryFirstFilterStrategy;)V

    sput-object v0, Lorg/apache/lucene/search/FilteredQuery;->QUERY_FIRST_FILTER_STRATEGY:Lorg/apache/lucene/search/FilteredQuery$FilterStrategy;

    return-void

    :cond_0
    move v0, v2

    .line 41
    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;)V
    .locals 1
    .param p1, "query"    # Lorg/apache/lucene/search/Query;
    .param p2, "filter"    # Lorg/apache/lucene/search/Filter;

    .prologue
    .line 54
    sget-object v0, Lorg/apache/lucene/search/FilteredQuery;->RANDOM_ACCESS_FILTER_STRATEGY:Lorg/apache/lucene/search/FilteredQuery$FilterStrategy;

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/search/FilteredQuery;-><init>(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;Lorg/apache/lucene/search/FilteredQuery$FilterStrategy;)V

    .line 55
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;Lorg/apache/lucene/search/FilteredQuery$FilterStrategy;)V
    .locals 2
    .param p1, "query"    # Lorg/apache/lucene/search/Query;
    .param p2, "filter"    # Lorg/apache/lucene/search/Filter;
    .param p3, "strategy"    # Lorg/apache/lucene/search/FilteredQuery$FilterStrategy;

    .prologue
    .line 66
    invoke-direct {p0}, Lorg/apache/lucene/search/Query;-><init>()V

    .line 67
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 68
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Query and filter cannot be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 69
    :cond_1
    if-nez p3, :cond_2

    .line 70
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "FilterStrategy can not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 71
    :cond_2
    iput-object p3, p0, Lorg/apache/lucene/search/FilteredQuery;->strategy:Lorg/apache/lucene/search/FilteredQuery$FilterStrategy;

    .line 72
    iput-object p1, p0, Lorg/apache/lucene/search/FilteredQuery;->query:Lorg/apache/lucene/search/Query;

    .line 73
    iput-object p2, p0, Lorg/apache/lucene/search/FilteredQuery;->filter:Lorg/apache/lucene/search/Filter;

    .line 74
    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/search/FilteredQuery;)Lorg/apache/lucene/search/Filter;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lorg/apache/lucene/search/FilteredQuery;->filter:Lorg/apache/lucene/search/Filter;

    return-object v0
.end method

.method static synthetic access$1(Lorg/apache/lucene/search/FilteredQuery;)Lorg/apache/lucene/search/FilteredQuery$FilterStrategy;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lorg/apache/lucene/search/FilteredQuery;->strategy:Lorg/apache/lucene/search/FilteredQuery$FilterStrategy;

    return-object v0
.end method


# virtual methods
.method public createWeight(Lorg/apache/lucene/search/IndexSearcher;)Lorg/apache/lucene/search/Weight;
    .locals 2
    .param p1, "searcher"    # Lorg/apache/lucene/search/IndexSearcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 82
    iget-object v1, p0, Lorg/apache/lucene/search/FilteredQuery;->query:Lorg/apache/lucene/search/Query;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/Query;->createWeight(Lorg/apache/lucene/search/IndexSearcher;)Lorg/apache/lucene/search/Weight;

    move-result-object v0

    .line 83
    .local v0, "weight":Lorg/apache/lucene/search/Weight;
    new-instance v1, Lorg/apache/lucene/search/FilteredQuery$1;

    invoke-direct {v1, p0, v0}, Lorg/apache/lucene/search/FilteredQuery$1;-><init>(Lorg/apache/lucene/search/FilteredQuery;Lorg/apache/lucene/search/Weight;)V

    return-object v1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 402
    if-ne p1, p0, :cond_1

    .line 408
    :cond_0
    :goto_0
    return v1

    .line 404
    :cond_1
    invoke-super {p0, p1}, Lorg/apache/lucene/search/Query;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    move v1, v2

    .line 405
    goto :goto_0

    .line 406
    :cond_2
    sget-boolean v3, Lorg/apache/lucene/search/FilteredQuery;->$assertionsDisabled:Z

    if-nez v3, :cond_3

    instance-of v3, p1, Lorg/apache/lucene/search/FilteredQuery;

    if-nez v3, :cond_3

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    :cond_3
    move-object v0, p1

    .line 407
    check-cast v0, Lorg/apache/lucene/search/FilteredQuery;

    .line 408
    .local v0, "fq":Lorg/apache/lucene/search/FilteredQuery;
    iget-object v3, v0, Lorg/apache/lucene/search/FilteredQuery;->query:Lorg/apache/lucene/search/Query;

    iget-object v4, p0, Lorg/apache/lucene/search/FilteredQuery;->query:Lorg/apache/lucene/search/Query;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/search/Query;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, v0, Lorg/apache/lucene/search/FilteredQuery;->filter:Lorg/apache/lucene/search/Filter;

    iget-object v4, p0, Lorg/apache/lucene/search/FilteredQuery;->filter:Lorg/apache/lucene/search/Filter;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, v0, Lorg/apache/lucene/search/FilteredQuery;->strategy:Lorg/apache/lucene/search/FilteredQuery$FilterStrategy;

    iget-object v4, p0, Lorg/apache/lucene/search/FilteredQuery;->strategy:Lorg/apache/lucene/search/FilteredQuery$FilterStrategy;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_4
    move v1, v2

    goto :goto_0
.end method

.method public extractTerms(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lorg/apache/lucene/index/Term;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 384
    .local p1, "terms":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/index/Term;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/search/FilteredQuery;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/Query;->extractTerms(Ljava/util/Set;)V

    .line 385
    return-void
.end method

.method public final getFilter()Lorg/apache/lucene/search/Filter;
    .locals 1

    .prologue
    .line 373
    iget-object v0, p0, Lorg/apache/lucene/search/FilteredQuery;->filter:Lorg/apache/lucene/search/Filter;

    return-object v0
.end method

.method public getFilterStrategy()Lorg/apache/lucene/search/FilteredQuery$FilterStrategy;
    .locals 1

    .prologue
    .line 378
    iget-object v0, p0, Lorg/apache/lucene/search/FilteredQuery;->strategy:Lorg/apache/lucene/search/FilteredQuery$FilterStrategy;

    return-object v0
.end method

.method public final getQuery()Lorg/apache/lucene/search/Query;
    .locals 1

    .prologue
    .line 368
    iget-object v0, p0, Lorg/apache/lucene/search/FilteredQuery;->query:Lorg/apache/lucene/search/Query;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 414
    invoke-super {p0}, Lorg/apache/lucene/search/Query;->hashCode()I

    move-result v0

    .line 415
    .local v0, "hash":I
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lorg/apache/lucene/search/FilteredQuery;->strategy:Lorg/apache/lucene/search/FilteredQuery$FilterStrategy;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    add-int v0, v1, v2

    .line 416
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lorg/apache/lucene/search/FilteredQuery;->query:Lorg/apache/lucene/search/Query;

    invoke-virtual {v2}, Lorg/apache/lucene/search/Query;->hashCode()I

    move-result v2

    add-int v0, v1, v2

    .line 417
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lorg/apache/lucene/search/FilteredQuery;->filter:Lorg/apache/lucene/search/Filter;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    add-int v0, v1, v2

    .line 418
    return v0
.end method

.method public rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;
    .locals 4
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 344
    iget-object v2, p0, Lorg/apache/lucene/search/FilteredQuery;->query:Lorg/apache/lucene/search/Query;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/search/Query;->rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;

    move-result-object v0

    .line 346
    .local v0, "queryRewritten":Lorg/apache/lucene/search/Query;
    instance-of v2, v0, Lorg/apache/lucene/search/MatchAllDocsQuery;

    if-eqz v2, :cond_0

    .line 349
    new-instance v1, Lorg/apache/lucene/search/ConstantScoreQuery;

    iget-object v2, p0, Lorg/apache/lucene/search/FilteredQuery;->filter:Lorg/apache/lucene/search/Filter;

    invoke-direct {v1, v2}, Lorg/apache/lucene/search/ConstantScoreQuery;-><init>(Lorg/apache/lucene/search/Filter;)V

    .line 351
    .local v1, "rewritten":Lorg/apache/lucene/search/Query;
    invoke-virtual {p0}, Lorg/apache/lucene/search/FilteredQuery;->getBoost()F

    move-result v2

    invoke-virtual {v0}, Lorg/apache/lucene/search/Query;->getBoost()F

    move-result v3

    mul-float/2addr v2, v3

    invoke-virtual {v1, v2}, Lorg/apache/lucene/search/Query;->setBoost(F)V

    .line 362
    .end local v1    # "rewritten":Lorg/apache/lucene/search/Query;
    :goto_0
    return-object v1

    .line 355
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/search/FilteredQuery;->query:Lorg/apache/lucene/search/Query;

    if-eq v0, v2, :cond_1

    .line 357
    new-instance v1, Lorg/apache/lucene/search/FilteredQuery;

    iget-object v2, p0, Lorg/apache/lucene/search/FilteredQuery;->filter:Lorg/apache/lucene/search/Filter;

    iget-object v3, p0, Lorg/apache/lucene/search/FilteredQuery;->strategy:Lorg/apache/lucene/search/FilteredQuery$FilterStrategy;

    invoke-direct {v1, v0, v2, v3}, Lorg/apache/lucene/search/FilteredQuery;-><init>(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;Lorg/apache/lucene/search/FilteredQuery$FilterStrategy;)V

    .line 358
    .restart local v1    # "rewritten":Lorg/apache/lucene/search/Query;
    invoke-virtual {p0}, Lorg/apache/lucene/search/FilteredQuery;->getBoost()F

    move-result v2

    invoke-virtual {v1, v2}, Lorg/apache/lucene/search/Query;->setBoost(F)V

    goto :goto_0

    .end local v1    # "rewritten":Lorg/apache/lucene/search/Query;
    :cond_1
    move-object v1, p0

    .line 362
    goto :goto_0
.end method

.method public toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 390
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 391
    .local v0, "buffer":Ljava/lang/StringBuilder;
    const-string v1, "filtered("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 392
    iget-object v1, p0, Lorg/apache/lucene/search/FilteredQuery;->query:Lorg/apache/lucene/search/Query;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/Query;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 393
    const-string v1, ")->"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 394
    iget-object v1, p0, Lorg/apache/lucene/search/FilteredQuery;->filter:Lorg/apache/lucene/search/Filter;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 395
    invoke-virtual {p0}, Lorg/apache/lucene/search/FilteredQuery;->getBoost()F

    move-result v1

    invoke-static {v1}, Lorg/apache/lucene/util/ToStringUtils;->boost(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 396
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
