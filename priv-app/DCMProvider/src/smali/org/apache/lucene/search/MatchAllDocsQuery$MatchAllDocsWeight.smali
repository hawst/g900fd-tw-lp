.class Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllDocsWeight;
.super Lorg/apache/lucene/search/Weight;
.source "MatchAllDocsQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/MatchAllDocsQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MatchAllDocsWeight"
.end annotation


# instance fields
.field private queryNorm:F

.field private queryWeight:F

.field final synthetic this$0:Lorg/apache/lucene/search/MatchAllDocsQuery;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/MatchAllDocsQuery;Lorg/apache/lucene/search/IndexSearcher;)V
    .locals 0
    .param p2, "searcher"    # Lorg/apache/lucene/search/IndexSearcher;

    .prologue
    .line 91
    iput-object p1, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllDocsWeight;->this$0:Lorg/apache/lucene/search/MatchAllDocsQuery;

    invoke-direct {p0}, Lorg/apache/lucene/search/Weight;-><init>()V

    .line 92
    return-void
.end method


# virtual methods
.method public explain(Lorg/apache/lucene/index/AtomicReaderContext;I)Lorg/apache/lucene/search/Explanation;
    .locals 4
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "doc"    # I

    .prologue
    .line 125
    new-instance v0, Lorg/apache/lucene/search/ComplexExplanation;

    .line 126
    const/4 v1, 0x1

    iget v2, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllDocsWeight;->queryWeight:F

    const-string v3, "MatchAllDocsQuery, product of:"

    .line 125
    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/search/ComplexExplanation;-><init>(ZFLjava/lang/String;)V

    .line 127
    .local v0, "queryExpl":Lorg/apache/lucene/search/Explanation;
    iget-object v1, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllDocsWeight;->this$0:Lorg/apache/lucene/search/MatchAllDocsQuery;

    invoke-virtual {v1}, Lorg/apache/lucene/search/MatchAllDocsQuery;->getBoost()F

    move-result v1

    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_0

    .line 128
    new-instance v1, Lorg/apache/lucene/search/Explanation;

    iget-object v2, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllDocsWeight;->this$0:Lorg/apache/lucene/search/MatchAllDocsQuery;

    invoke-virtual {v2}, Lorg/apache/lucene/search/MatchAllDocsQuery;->getBoost()F

    move-result v2

    const-string v3, "boost"

    invoke-direct {v1, v2, v3}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 130
    :cond_0
    new-instance v1, Lorg/apache/lucene/search/Explanation;

    iget v2, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllDocsWeight;->queryNorm:F

    const-string v3, "queryNorm"

    invoke-direct {v1, v2, v3}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 132
    return-object v0
.end method

.method public getQuery()Lorg/apache/lucene/search/Query;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllDocsWeight;->this$0:Lorg/apache/lucene/search/MatchAllDocsQuery;

    return-object v0
.end method

.method public getValueForNormalization()F
    .locals 2

    .prologue
    .line 106
    iget-object v0, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllDocsWeight;->this$0:Lorg/apache/lucene/search/MatchAllDocsQuery;

    invoke-virtual {v0}, Lorg/apache/lucene/search/MatchAllDocsQuery;->getBoost()F

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllDocsWeight;->queryWeight:F

    .line 107
    iget v0, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllDocsWeight;->queryWeight:F

    iget v1, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllDocsWeight;->queryWeight:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public normalize(FF)V
    .locals 2
    .param p1, "queryNorm"    # F
    .param p2, "topLevelBoost"    # F

    .prologue
    .line 112
    mul-float v0, p1, p2

    iput v0, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllDocsWeight;->queryNorm:F

    .line 113
    iget v0, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllDocsWeight;->queryWeight:F

    iget v1, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllDocsWeight;->queryNorm:F

    mul-float/2addr v0, v1

    iput v0, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllDocsWeight;->queryWeight:F

    .line 114
    return-void
.end method

.method public scorer(Lorg/apache/lucene/index/AtomicReaderContext;ZZLorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/Scorer;
    .locals 6
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "scoreDocsInOrder"    # Z
    .param p3, "topScorer"    # Z
    .param p4, "acceptDocs"    # Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 119
    new-instance v0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllScorer;

    iget-object v1, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllDocsWeight;->this$0:Lorg/apache/lucene/search/MatchAllDocsQuery;

    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v2

    iget v5, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllDocsWeight;->queryWeight:F

    move-object v3, p4

    move-object v4, p0

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllScorer;-><init>(Lorg/apache/lucene/search/MatchAllDocsQuery;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/search/Weight;F)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 96
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "weight("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllDocsWeight;->this$0:Lorg/apache/lucene/search/MatchAllDocsQuery;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
