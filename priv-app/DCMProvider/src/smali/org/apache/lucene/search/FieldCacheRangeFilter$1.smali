.class Lorg/apache/lucene/search/FieldCacheRangeFilter$1;
.super Lorg/apache/lucene/search/FieldCacheRangeFilter;
.source "FieldCacheRangeFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/FieldCacheRangeFilter;->newStringRange(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Lorg/apache/lucene/search/FieldCacheRangeFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/FieldCacheRangeFilter",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 8
    .param p1, "$anonymous0"    # Ljava/lang/String;
    .param p2, "$anonymous1"    # Lorg/apache/lucene/search/FieldCache$Parser;
    .param p3, "$anonymous2"    # Ljava/lang/String;
    .param p4, "$anonymous3"    # Ljava/lang/String;
    .param p5, "$anonymous4"    # Z
    .param p6, "$anonymous5"    # Z

    .prologue
    .line 90
    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v7}, Lorg/apache/lucene/search/FieldCacheRangeFilter;-><init>(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Ljava/lang/Object;Ljava/lang/Object;ZZLorg/apache/lucene/search/FieldCacheRangeFilter;)V

    .line 1
    return-void
.end method


# virtual methods
.method public getDocIdSet(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/DocIdSet;
    .locals 9
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "acceptDocs"    # Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 93
    sget-object v0, Lorg/apache/lucene/search/FieldCache;->DEFAULT:Lorg/apache/lucene/search/FieldCache;

    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$1;->field:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Lorg/apache/lucene/search/FieldCache;->getTermsIndex(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;)Lorg/apache/lucene/index/SortedDocValues;

    move-result-object v4

    .line 94
    .local v4, "fcsi":Lorg/apache/lucene/index/SortedDocValues;
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$1;->lowerVal:Ljava/lang/Object;

    if-nez v0, :cond_1

    move v7, v1

    .line 95
    .local v7, "lowerPoint":I
    :goto_0
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$1;->upperVal:Ljava/lang/Object;

    if-nez v0, :cond_2

    move v8, v1

    .line 103
    .local v8, "upperPoint":I
    :goto_1
    if-ne v7, v1, :cond_3

    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$1;->lowerVal:Ljava/lang/Object;

    if-nez v0, :cond_3

    .line 104
    const/4 v5, 0x0

    .line 113
    .local v5, "inclusiveLowerPoint":I
    :goto_2
    if-ne v8, v1, :cond_6

    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$1;->upperVal:Ljava/lang/Object;

    if-nez v0, :cond_6

    .line 114
    const v6, 0x7fffffff

    .line 123
    .local v6, "inclusiveUpperPoint":I
    :goto_3
    if-ltz v6, :cond_0

    if-le v5, v6, :cond_9

    .line 124
    :cond_0
    sget-object v0, Lorg/apache/lucene/search/DocIdSet;->EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;

    .line 129
    :goto_4
    return-object v0

    .line 94
    .end local v5    # "inclusiveLowerPoint":I
    .end local v6    # "inclusiveUpperPoint":I
    .end local v7    # "lowerPoint":I
    .end local v8    # "upperPoint":I
    :cond_1
    new-instance v2, Lorg/apache/lucene/util/BytesRef;

    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$1;->lowerVal:Ljava/lang/Object;

    check-cast v0, Ljava/lang/CharSequence;

    invoke-direct {v2, v0}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v4, v2}, Lorg/apache/lucene/index/SortedDocValues;->lookupTerm(Lorg/apache/lucene/util/BytesRef;)I

    move-result v7

    goto :goto_0

    .line 95
    .restart local v7    # "lowerPoint":I
    :cond_2
    new-instance v2, Lorg/apache/lucene/util/BytesRef;

    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$1;->upperVal:Ljava/lang/Object;

    check-cast v0, Ljava/lang/CharSequence;

    invoke-direct {v2, v0}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v4, v2}, Lorg/apache/lucene/index/SortedDocValues;->lookupTerm(Lorg/apache/lucene/util/BytesRef;)I

    move-result v8

    goto :goto_1

    .line 105
    .restart local v8    # "upperPoint":I
    :cond_3
    iget-boolean v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$1;->includeLower:Z

    if-eqz v0, :cond_4

    if-ltz v7, :cond_4

    .line 106
    move v5, v7

    .line 107
    .restart local v5    # "inclusiveLowerPoint":I
    goto :goto_2

    .end local v5    # "inclusiveLowerPoint":I
    :cond_4
    if-ltz v7, :cond_5

    .line 108
    add-int/lit8 v5, v7, 0x1

    .line 109
    .restart local v5    # "inclusiveLowerPoint":I
    goto :goto_2

    .line 110
    .end local v5    # "inclusiveLowerPoint":I
    :cond_5
    const/4 v0, 0x0

    neg-int v2, v7

    add-int/lit8 v2, v2, -0x1

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v5

    .restart local v5    # "inclusiveLowerPoint":I
    goto :goto_2

    .line 115
    :cond_6
    iget-boolean v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$1;->includeUpper:Z

    if-eqz v0, :cond_7

    if-ltz v8, :cond_7

    .line 116
    move v6, v8

    .line 117
    .restart local v6    # "inclusiveUpperPoint":I
    goto :goto_3

    .end local v6    # "inclusiveUpperPoint":I
    :cond_7
    if-ltz v8, :cond_8

    .line 118
    add-int/lit8 v6, v8, -0x1

    .line 119
    .restart local v6    # "inclusiveUpperPoint":I
    goto :goto_3

    .line 120
    .end local v6    # "inclusiveUpperPoint":I
    :cond_8
    neg-int v0, v8

    add-int/lit8 v6, v0, -0x2

    .restart local v6    # "inclusiveUpperPoint":I
    goto :goto_3

    .line 127
    :cond_9
    sget-boolean v0, Lorg/apache/lucene/search/FieldCacheRangeFilter;->$assertionsDisabled:Z

    if-nez v0, :cond_b

    if-ltz v5, :cond_a

    if-gez v6, :cond_b

    :cond_a
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 129
    :cond_b
    new-instance v0, Lorg/apache/lucene/search/FieldCacheRangeFilter$1$1;

    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v2

    move-object v1, p0

    move-object v3, p2

    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/search/FieldCacheRangeFilter$1$1;-><init>(Lorg/apache/lucene/search/FieldCacheRangeFilter$1;ILorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/SortedDocValues;II)V

    goto :goto_4
.end method
