.class public Lorg/apache/lucene/search/TermQuery;
.super Lorg/apache/lucene/search/Query;
.source "TermQuery.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/TermQuery$TermWeight;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final docFreq:I

.field private final perReaderTermState:Lorg/apache/lucene/index/TermContext;

.field private final term:Lorg/apache/lucene/index/Term;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lorg/apache/lucene/search/TermQuery;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/TermQuery;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/index/Term;)V
    .locals 1
    .param p1, "t"    # Lorg/apache/lucene/index/Term;

    .prologue
    .line 135
    const/4 v0, -0x1

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/search/TermQuery;-><init>(Lorg/apache/lucene/index/Term;I)V

    .line 136
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/index/Term;I)V
    .locals 1
    .param p1, "t"    # Lorg/apache/lucene/index/Term;
    .param p2, "docFreq"    # I

    .prologue
    .line 141
    invoke-direct {p0}, Lorg/apache/lucene/search/Query;-><init>()V

    .line 142
    iput-object p1, p0, Lorg/apache/lucene/search/TermQuery;->term:Lorg/apache/lucene/index/Term;

    .line 143
    iput p2, p0, Lorg/apache/lucene/search/TermQuery;->docFreq:I

    .line 144
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/search/TermQuery;->perReaderTermState:Lorg/apache/lucene/index/TermContext;

    .line 145
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/index/Term;Lorg/apache/lucene/index/TermContext;)V
    .locals 1
    .param p1, "t"    # Lorg/apache/lucene/index/Term;
    .param p2, "states"    # Lorg/apache/lucene/index/TermContext;

    .prologue
    .line 150
    invoke-direct {p0}, Lorg/apache/lucene/search/Query;-><init>()V

    .line 151
    sget-boolean v0, Lorg/apache/lucene/search/TermQuery;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 152
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/search/TermQuery;->term:Lorg/apache/lucene/index/Term;

    .line 153
    invoke-virtual {p2}, Lorg/apache/lucene/index/TermContext;->docFreq()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/TermQuery;->docFreq:I

    .line 154
    iput-object p2, p0, Lorg/apache/lucene/search/TermQuery;->perReaderTermState:Lorg/apache/lucene/index/TermContext;

    .line 155
    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/search/TermQuery;)Lorg/apache/lucene/index/Term;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lorg/apache/lucene/search/TermQuery;->term:Lorg/apache/lucene/index/Term;

    return-object v0
.end method


# virtual methods
.method public createWeight(Lorg/apache/lucene/search/IndexSearcher;)Lorg/apache/lucene/search/Weight;
    .locals 4
    .param p1, "searcher"    # Lorg/apache/lucene/search/IndexSearcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 162
    invoke-virtual {p1}, Lorg/apache/lucene/search/IndexSearcher;->getTopReaderContext()Lorg/apache/lucene/index/IndexReaderContext;

    move-result-object v0

    .line 164
    .local v0, "context":Lorg/apache/lucene/index/IndexReaderContext;
    iget-object v2, p0, Lorg/apache/lucene/search/TermQuery;->perReaderTermState:Lorg/apache/lucene/index/TermContext;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/apache/lucene/search/TermQuery;->perReaderTermState:Lorg/apache/lucene/index/TermContext;

    iget-object v2, v2, Lorg/apache/lucene/index/TermContext;->topReaderContext:Lorg/apache/lucene/index/IndexReaderContext;

    if-eq v2, v0, :cond_2

    .line 166
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/search/TermQuery;->term:Lorg/apache/lucene/index/Term;

    const/4 v3, 0x1

    invoke-static {v0, v2, v3}, Lorg/apache/lucene/index/TermContext;->build(Lorg/apache/lucene/index/IndexReaderContext;Lorg/apache/lucene/index/Term;Z)Lorg/apache/lucene/index/TermContext;

    move-result-object v1

    .line 173
    .local v1, "termState":Lorg/apache/lucene/index/TermContext;
    :goto_0
    iget v2, p0, Lorg/apache/lucene/search/TermQuery;->docFreq:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    .line 174
    iget v2, p0, Lorg/apache/lucene/search/TermQuery;->docFreq:I

    invoke-virtual {v1, v2}, Lorg/apache/lucene/index/TermContext;->setDocFreq(I)V

    .line 176
    :cond_1
    new-instance v2, Lorg/apache/lucene/search/TermQuery$TermWeight;

    invoke-direct {v2, p0, p1, v1}, Lorg/apache/lucene/search/TermQuery$TermWeight;-><init>(Lorg/apache/lucene/search/TermQuery;Lorg/apache/lucene/search/IndexSearcher;Lorg/apache/lucene/index/TermContext;)V

    return-object v2

    .line 169
    .end local v1    # "termState":Lorg/apache/lucene/index/TermContext;
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/search/TermQuery;->perReaderTermState:Lorg/apache/lucene/index/TermContext;

    .restart local v1    # "termState":Lorg/apache/lucene/index/TermContext;
    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 200
    instance-of v2, p1, Lorg/apache/lucene/search/TermQuery;

    if-nez v2, :cond_1

    .line 203
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 202
    check-cast v0, Lorg/apache/lucene/search/TermQuery;

    .line 203
    .local v0, "other":Lorg/apache/lucene/search/TermQuery;
    invoke-virtual {p0}, Lorg/apache/lucene/search/TermQuery;->getBoost()F

    move-result v2

    invoke-virtual {v0}, Lorg/apache/lucene/search/TermQuery;->getBoost()F

    move-result v3

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    .line 204
    iget-object v2, p0, Lorg/apache/lucene/search/TermQuery;->term:Lorg/apache/lucene/index/Term;

    iget-object v3, v0, Lorg/apache/lucene/search/TermQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/index/Term;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 203
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public extractTerms(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lorg/apache/lucene/index/Term;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 181
    .local p1, "terms":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/index/Term;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/search/TermQuery;->getTerm()Lorg/apache/lucene/index/Term;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 182
    return-void
.end method

.method public getTerm()Lorg/apache/lucene/index/Term;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lorg/apache/lucene/search/TermQuery;->term:Lorg/apache/lucene/index/Term;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 210
    invoke-virtual {p0}, Lorg/apache/lucene/search/TermQuery;->getBoost()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/search/TermQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v1}, Lorg/apache/lucene/index/Term;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 187
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 188
    .local v0, "buffer":Ljava/lang/StringBuilder;
    iget-object v1, p0, Lorg/apache/lucene/search/TermQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 189
    iget-object v1, p0, Lorg/apache/lucene/search/TermQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 190
    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 192
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/TermQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v1}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 193
    invoke-virtual {p0}, Lorg/apache/lucene/search/TermQuery;->getBoost()F

    move-result v1

    invoke-static {v1}, Lorg/apache/lucene/util/ToStringUtils;->boost(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 194
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
