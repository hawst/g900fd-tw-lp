.class final Lorg/apache/lucene/search/similarities/TFIDFSimilarity$ExactTFIDFDocScorer;
.super Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;
.source "TFIDFSimilarity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/similarities/TFIDFSimilarity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ExactTFIDFDocScorer"
.end annotation


# instance fields
.field private final norms:Lorg/apache/lucene/index/NumericDocValues;

.field private final stats:Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;

.field final synthetic this$0:Lorg/apache/lucene/search/similarities/TFIDFSimilarity;

.field private final weightValue:F


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/similarities/TFIDFSimilarity;Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;Lorg/apache/lucene/index/NumericDocValues;)V
    .locals 1
    .param p2, "stats"    # Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;
    .param p3, "norms"    # Lorg/apache/lucene/index/NumericDocValues;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 776
    iput-object p1, p0, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$ExactTFIDFDocScorer;->this$0:Lorg/apache/lucene/search/similarities/TFIDFSimilarity;

    invoke-direct {p0}, Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;-><init>()V

    .line 777
    iput-object p2, p0, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$ExactTFIDFDocScorer;->stats:Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;

    .line 778
    # getter for: Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;->value:F
    invoke-static {p2}, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;->access$0(Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;)F

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$ExactTFIDFDocScorer;->weightValue:F

    .line 779
    iput-object p3, p0, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$ExactTFIDFDocScorer;->norms:Lorg/apache/lucene/index/NumericDocValues;

    .line 780
    return-void
.end method


# virtual methods
.method public explain(ILorg/apache/lucene/search/Explanation;)Lorg/apache/lucene/search/Explanation;
    .locals 3
    .param p1, "doc"    # I
    .param p2, "freq"    # Lorg/apache/lucene/search/Explanation;

    .prologue
    .line 791
    iget-object v0, p0, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$ExactTFIDFDocScorer;->this$0:Lorg/apache/lucene/search/similarities/TFIDFSimilarity;

    iget-object v1, p0, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$ExactTFIDFDocScorer;->stats:Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;

    iget-object v2, p0, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$ExactTFIDFDocScorer;->norms:Lorg/apache/lucene/index/NumericDocValues;

    # invokes: Lorg/apache/lucene/search/similarities/TFIDFSimilarity;->explainScore(ILorg/apache/lucene/search/Explanation;Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;Lorg/apache/lucene/index/NumericDocValues;)Lorg/apache/lucene/search/Explanation;
    invoke-static {v0, p1, p2, v1, v2}, Lorg/apache/lucene/search/similarities/TFIDFSimilarity;->access$0(Lorg/apache/lucene/search/similarities/TFIDFSimilarity;ILorg/apache/lucene/search/Explanation;Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;Lorg/apache/lucene/index/NumericDocValues;)Lorg/apache/lucene/search/Explanation;

    move-result-object v0

    return-object v0
.end method

.method public score(II)F
    .locals 4
    .param p1, "doc"    # I
    .param p2, "freq"    # I

    .prologue
    .line 784
    iget-object v1, p0, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$ExactTFIDFDocScorer;->this$0:Lorg/apache/lucene/search/similarities/TFIDFSimilarity;

    invoke-virtual {v1, p2}, Lorg/apache/lucene/search/similarities/TFIDFSimilarity;->tf(I)F

    move-result v1

    iget v2, p0, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$ExactTFIDFDocScorer;->weightValue:F

    mul-float v0, v1, v2

    .line 786
    .local v0, "raw":F
    iget-object v1, p0, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$ExactTFIDFDocScorer;->norms:Lorg/apache/lucene/index/NumericDocValues;

    if-nez v1, :cond_0

    .end local v0    # "raw":F
    :goto_0
    return v0

    .restart local v0    # "raw":F
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$ExactTFIDFDocScorer;->this$0:Lorg/apache/lucene/search/similarities/TFIDFSimilarity;

    iget-object v2, p0, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$ExactTFIDFDocScorer;->norms:Lorg/apache/lucene/index/NumericDocValues;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/index/NumericDocValues;->get(I)J

    move-result-wide v2

    long-to-int v2, v2

    int-to-byte v2, v2

    invoke-virtual {v1, v2}, Lorg/apache/lucene/search/similarities/TFIDFSimilarity;->decodeNormValue(B)F

    move-result v1

    mul-float/2addr v0, v1

    goto :goto_0
.end method
