.class Lorg/apache/lucene/search/DisjunctionSumScorer;
.super Lorg/apache/lucene/search/DisjunctionScorer;
.source "DisjunctionSumScorer.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final coord:[F

.field private doc:I

.field protected nrMatchers:I

.field protected score:D


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lorg/apache/lucene/search/DisjunctionSumScorer;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/DisjunctionSumScorer;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/search/Weight;[Lorg/apache/lucene/search/Scorer;[F)V
    .locals 2
    .param p1, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p2, "subScorers"    # [Lorg/apache/lucene/search/Scorer;
    .param p3, "coord"    # [F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 41
    array-length v0, p2

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/search/DisjunctionScorer;-><init>(Lorg/apache/lucene/search/Weight;[Lorg/apache/lucene/search/Scorer;I)V

    .line 27
    iput v1, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->doc:I

    .line 30
    iput v1, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->nrMatchers:I

    .line 32
    const-wide/high16 v0, 0x7ff8000000000000L    # NaN

    iput-wide v0, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->score:D

    .line 43
    iget v0, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->numScorers:I

    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    .line 44
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "There must be at least 2 subScorers"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 46
    :cond_0
    iput-object p3, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->coord:[F

    .line 47
    return-void
.end method

.method private afterNext()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 69
    iget-object v1, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    const/4 v2, 0x0

    aget-object v0, v1, v2

    .line 70
    .local v0, "sub":Lorg/apache/lucene/search/Scorer;
    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->doc:I

    .line 71
    iget v1, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->doc:I

    const v2, 0x7fffffff

    if-eq v1, v2, :cond_0

    .line 72
    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v1

    float-to-double v2, v1

    iput-wide v2, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->score:D

    .line 73
    iput v4, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->nrMatchers:I

    .line 74
    invoke-direct {p0, v4}, Lorg/apache/lucene/search/DisjunctionSumScorer;->countMatches(I)V

    .line 75
    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lorg/apache/lucene/search/DisjunctionSumScorer;->countMatches(I)V

    .line 77
    :cond_0
    return-void
.end method

.method private countMatches(I)V
    .locals 4
    .param p1, "root"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 84
    iget v0, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->numScorers:I

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v0

    iget v1, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->doc:I

    if-ne v0, v1, :cond_0

    .line 85
    iget v0, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->nrMatchers:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->nrMatchers:I

    .line 86
    iget-wide v0, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->score:D

    iget-object v2, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v2, v2, p1

    invoke-virtual {v2}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v2

    float-to-double v2, v2

    add-double/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->score:D

    .line 87
    shl-int/lit8 v0, p1, 0x1

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Lorg/apache/lucene/search/DisjunctionSumScorer;->countMatches(I)V

    .line 88
    shl-int/lit8 v0, p1, 0x1

    add-int/lit8 v0, v0, 0x2

    invoke-direct {p0, v0}, Lorg/apache/lucene/search/DisjunctionSumScorer;->countMatches(I)V

    .line 90
    :cond_0
    return-void
.end method


# virtual methods
.method public advance(I)I
    .locals 3
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const v0, 0x7fffffff

    const/4 v2, 0x0

    .line 122
    sget-boolean v1, Lorg/apache/lucene/search/DisjunctionSumScorer;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget v1, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->doc:I

    if-ne v1, v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 124
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v1, v1, v2

    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/Scorer;->advance(I)I

    move-result v1

    if-eq v1, v0, :cond_2

    .line 125
    invoke-virtual {p0, v2}, Lorg/apache/lucene/search/DisjunctionSumScorer;->heapAdjust(I)V

    .line 132
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v1, v1, v2

    invoke-virtual {v1}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v1

    if-lt v1, p1, :cond_0

    .line 133
    invoke-direct {p0}, Lorg/apache/lucene/search/DisjunctionSumScorer;->afterNext()V

    .line 134
    iget v0, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->doc:I

    :goto_0
    return v0

    .line 127
    :cond_2
    invoke-virtual {p0}, Lorg/apache/lucene/search/DisjunctionSumScorer;->heapRemoveRoot()V

    .line 128
    iget v1, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->numScorers:I

    if-nez v1, :cond_1

    .line 129
    iput v0, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->doc:I

    goto :goto_0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 102
    iget v0, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->doc:I

    return v0
.end method

.method public freq()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 107
    iget v0, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->nrMatchers:I

    return v0
.end method

.method public nextDoc()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const v0, 0x7fffffff

    const/4 v3, 0x0

    .line 51
    sget-boolean v1, Lorg/apache/lucene/search/DisjunctionSumScorer;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget v1, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->doc:I

    if-ne v1, v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 53
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v1, v1, v3

    invoke-virtual {v1}, Lorg/apache/lucene/search/Scorer;->nextDoc()I

    move-result v1

    if-eq v1, v0, :cond_2

    .line 54
    invoke-virtual {p0, v3}, Lorg/apache/lucene/search/DisjunctionSumScorer;->heapAdjust(I)V

    .line 61
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v1, v1, v3

    invoke-virtual {v1}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v1

    iget v2, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->doc:I

    if-eq v1, v2, :cond_0

    .line 62
    invoke-direct {p0}, Lorg/apache/lucene/search/DisjunctionSumScorer;->afterNext()V

    .line 63
    iget v0, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->doc:I

    :goto_0
    return v0

    .line 56
    :cond_2
    invoke-virtual {p0}, Lorg/apache/lucene/search/DisjunctionSumScorer;->heapRemoveRoot()V

    .line 57
    iget v1, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->numScorers:I

    if-nez v1, :cond_1

    .line 58
    iput v0, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->doc:I

    goto :goto_0
.end method

.method public score()F
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 97
    iget-wide v0, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->score:D

    double-to-float v0, v0

    iget-object v1, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->coord:[F

    iget v2, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->nrMatchers:I

    aget v1, v1, v2

    mul-float/2addr v0, v1

    return v0
.end method
