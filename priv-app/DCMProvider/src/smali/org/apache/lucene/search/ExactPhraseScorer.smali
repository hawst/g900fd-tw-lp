.class final Lorg/apache/lucene/search/ExactPhraseScorer;
.super Lorg/apache/lucene/search/Scorer;
.source "ExactPhraseScorer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final CHUNK:I = 0x1000


# instance fields
.field private final chunkStates:[Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;

.field private final cost:J

.field private final counts:[I

.field private docID:I

.field private final docScorer:Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;

.field private final endMinus1:I

.field private freq:I

.field private gen:I

.field private final gens:[I

.field noDocs:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lorg/apache/lucene/search/ExactPhraseScorer;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/ExactPhraseScorer;->$assertionsDisabled:Z

    .line 29
    return-void

    .line 26
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/search/Weight;[Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;)V
    .locals 8
    .param p1, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p2, "postings"    # [Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;
    .param p3, "docScorer"    # Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v5, 0x1000

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 63
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/Scorer;-><init>(Lorg/apache/lucene/search/Weight;)V

    .line 32
    new-array v4, v5, [I

    iput-object v4, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->counts:[I

    .line 33
    new-array v4, v5, [I

    iput-object v4, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->gens:[I

    .line 56
    const/4 v4, -0x1

    iput v4, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->docID:I

    .line 64
    iput-object p3, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->docScorer:Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;

    .line 66
    array-length v4, p2

    new-array v4, v4, [Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;

    iput-object v4, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->chunkStates:[Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;

    .line 68
    array-length v4, p2

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->endMinus1:I

    .line 71
    aget-object v4, p2, v3

    iget-object v4, v4, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->postings:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v4}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->cost()J

    move-result-wide v4

    iput-wide v4, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->cost:J

    .line 73
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v4, p2

    if-lt v0, v4, :cond_0

    .line 88
    :goto_1
    return-void

    .line 81
    :cond_0
    aget-object v4, p2, v0

    iget v4, v4, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->docFreq:I

    aget-object v5, p2, v3

    iget v5, v5, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->docFreq:I

    mul-int/lit8 v5, v5, 0x5

    if-le v4, v5, :cond_1

    move v1, v2

    .line 82
    .local v1, "useAdvance":Z
    :goto_2
    iget-object v4, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->chunkStates:[Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;

    new-instance v5, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;

    aget-object v6, p2, v0

    iget-object v6, v6, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->postings:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    aget-object v7, p2, v0

    iget v7, v7, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->position:I

    neg-int v7, v7

    invoke-direct {v5, v6, v7, v1}, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;-><init>(Lorg/apache/lucene/index/DocsAndPositionsEnum;IZ)V

    aput-object v5, v4, v0

    .line 83
    if-lez v0, :cond_2

    aget-object v4, p2, v0

    iget-object v4, v4, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->postings:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v4}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->nextDoc()I

    move-result v4

    const v5, 0x7fffffff

    if-ne v4, v5, :cond_2

    .line 84
    iput-boolean v2, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->noDocs:Z

    goto :goto_1

    .end local v1    # "useAdvance":Z
    :cond_1
    move v1, v3

    .line 81
    goto :goto_2

    .line 73
    .restart local v1    # "useAdvance":Z
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private phraseFreq()I
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 207
    iput v10, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->freq:I

    .line 210
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    iget-object v8, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->chunkStates:[Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;

    array-length v8, v8

    if-lt v5, v8, :cond_0

    .line 218
    const/4 v2, 0x0

    .line 219
    .local v2, "chunkStart":I
    const/16 v1, 0x1000

    .line 222
    .local v1, "chunkEnd":I
    const/4 v4, 0x0

    .line 227
    .local v4, "end":Z
    :goto_1
    if-eqz v4, :cond_1

    .line 320
    iget v8, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->freq:I

    return v8

    .line 211
    .end local v1    # "chunkEnd":I
    .end local v2    # "chunkStart":I
    .end local v4    # "end":Z
    :cond_0
    iget-object v8, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->chunkStates:[Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;

    aget-object v3, v8, v5

    .line 212
    .local v3, "cs":Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;
    iget-object v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posEnum:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v8}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->freq()I

    move-result v8

    iput v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posLimit:I

    .line 213
    iget v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->offset:I

    iget-object v9, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posEnum:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v9}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->nextPosition()I

    move-result v9

    add-int/2addr v8, v9

    iput v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->pos:I

    .line 214
    iput v11, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posUpto:I

    .line 215
    const/4 v8, -0x1

    iput v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->lastPos:I

    .line 210
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 229
    .end local v3    # "cs":Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;
    .restart local v1    # "chunkEnd":I
    .restart local v2    # "chunkStart":I
    .restart local v4    # "end":Z
    :cond_1
    iget v8, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->gen:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->gen:I

    .line 231
    iget v8, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->gen:I

    if-nez v8, :cond_2

    .line 233
    iget-object v8, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->gens:[I

    invoke-static {v8, v10}, Ljava/util/Arrays;->fill([II)V

    .line 234
    iget v8, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->gen:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->gen:I

    .line 239
    :cond_2
    iget-object v8, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->chunkStates:[Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;

    aget-object v3, v8, v10

    .line 240
    .restart local v3    # "cs":Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;
    :goto_2
    iget v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->pos:I

    if-lt v8, v1, :cond_4

    .line 259
    :goto_3
    const/4 v0, 0x1

    .line 260
    .local v0, "any":Z
    const/4 v7, 0x1

    .local v7, "t":I
    :goto_4
    iget v8, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->endMinus1:I

    if-lt v7, v8, :cond_8

    .line 287
    :cond_3
    if-nez v0, :cond_c

    .line 289
    add-int/lit16 v2, v2, 0x1000

    .line 290
    add-int/lit16 v1, v1, 0x1000

    .line 291
    goto :goto_1

    .line 241
    .end local v0    # "any":Z
    .end local v7    # "t":I
    :cond_4
    iget v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->pos:I

    iget v9, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->lastPos:I

    if-le v8, v9, :cond_6

    .line 242
    iget v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->pos:I

    iput v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->lastPos:I

    .line 243
    iget v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->pos:I

    sub-int v6, v8, v2

    .line 244
    .local v6, "posIndex":I
    iget-object v8, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->counts:[I

    aput v11, v8, v6

    .line 245
    sget-boolean v8, Lorg/apache/lucene/search/ExactPhraseScorer;->$assertionsDisabled:Z

    if-nez v8, :cond_5

    iget-object v8, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->gens:[I

    aget v8, v8, v6

    iget v9, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->gen:I

    if-ne v8, v9, :cond_5

    new-instance v8, Ljava/lang/AssertionError;

    invoke-direct {v8}, Ljava/lang/AssertionError;-><init>()V

    throw v8

    .line 246
    :cond_5
    iget-object v8, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->gens:[I

    iget v9, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->gen:I

    aput v9, v8, v6

    .line 249
    .end local v6    # "posIndex":I
    :cond_6
    iget v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posUpto:I

    iget v9, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posLimit:I

    if-ne v8, v9, :cond_7

    .line 250
    const/4 v4, 0x1

    .line 251
    goto :goto_3

    .line 253
    :cond_7
    iget v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posUpto:I

    add-int/lit8 v8, v8, 0x1

    iput v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posUpto:I

    .line 254
    iget v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->offset:I

    iget-object v9, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posEnum:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v9}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->nextPosition()I

    move-result v9

    add-int/2addr v8, v9

    iput v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->pos:I

    goto :goto_2

    .line 261
    .restart local v0    # "any":Z
    .restart local v7    # "t":I
    :cond_8
    iget-object v8, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->chunkStates:[Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;

    aget-object v3, v8, v7

    .line 262
    const/4 v0, 0x0

    .line 263
    :goto_5
    iget v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->pos:I

    if-lt v8, v1, :cond_9

    .line 282
    :goto_6
    if-eqz v0, :cond_3

    .line 260
    add-int/lit8 v7, v7, 0x1

    goto :goto_4

    .line 264
    :cond_9
    iget v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->pos:I

    iget v9, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->lastPos:I

    if-le v8, v9, :cond_a

    .line 265
    iget v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->pos:I

    iput v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->lastPos:I

    .line 266
    iget v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->pos:I

    sub-int v6, v8, v2

    .line 267
    .restart local v6    # "posIndex":I
    if-ltz v6, :cond_a

    iget-object v8, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->gens:[I

    aget v8, v8, v6

    iget v9, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->gen:I

    if-ne v8, v9, :cond_a

    iget-object v8, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->counts:[I

    aget v8, v8, v6

    if-ne v8, v7, :cond_a

    .line 269
    iget-object v8, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->counts:[I

    aget v9, v8, v6

    add-int/lit8 v9, v9, 0x1

    aput v9, v8, v6

    .line 270
    const/4 v0, 0x1

    .line 274
    .end local v6    # "posIndex":I
    :cond_a
    iget v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posUpto:I

    iget v9, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posLimit:I

    if-ne v8, v9, :cond_b

    .line 275
    const/4 v4, 0x1

    .line 276
    goto :goto_6

    .line 278
    :cond_b
    iget v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posUpto:I

    add-int/lit8 v8, v8, 0x1

    iput v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posUpto:I

    .line 279
    iget v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->offset:I

    iget-object v9, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posEnum:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v9}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->nextPosition()I

    move-result v9

    add-int/2addr v8, v9

    iput v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->pos:I

    goto :goto_5

    .line 297
    :cond_c
    iget-object v8, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->chunkStates:[Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;

    iget v9, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->endMinus1:I

    aget-object v3, v8, v9

    .line 298
    :goto_7
    iget v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->pos:I

    if-lt v8, v1, :cond_d

    .line 316
    :goto_8
    add-int/lit16 v2, v2, 0x1000

    .line 317
    add-int/lit16 v1, v1, 0x1000

    goto/16 :goto_1

    .line 299
    :cond_d
    iget v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->pos:I

    iget v9, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->lastPos:I

    if-le v8, v9, :cond_e

    .line 300
    iget v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->pos:I

    iput v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->lastPos:I

    .line 301
    iget v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->pos:I

    sub-int v6, v8, v2

    .line 302
    .restart local v6    # "posIndex":I
    if-ltz v6, :cond_e

    iget-object v8, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->gens:[I

    aget v8, v8, v6

    iget v9, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->gen:I

    if-ne v8, v9, :cond_e

    iget-object v8, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->counts:[I

    aget v8, v8, v6

    iget v9, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->endMinus1:I

    if-ne v8, v9, :cond_e

    .line 303
    iget v8, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->freq:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->freq:I

    .line 307
    .end local v6    # "posIndex":I
    :cond_e
    iget v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posUpto:I

    iget v9, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posLimit:I

    if-ne v8, v9, :cond_f

    .line 308
    const/4 v4, 0x1

    .line 309
    goto :goto_8

    .line 311
    :cond_f
    iget v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posUpto:I

    add-int/lit8 v8, v8, 0x1

    iput v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posUpto:I

    .line 312
    iget v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->offset:I

    iget-object v9, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posEnum:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v9}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->nextPosition()I

    move-result v9

    add-int/2addr v8, v9

    iput v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->pos:I

    goto :goto_7
.end method


# virtual methods
.method public advance(I)I
    .locals 6
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const v4, 0x7fffffff

    .line 146
    iget-object v3, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->chunkStates:[Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;

    aget-object v3, v3, v5

    iget-object v3, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posEnum:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v3, p1}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->advance(I)I

    move-result v0

    .line 147
    .local v0, "doc":I
    if-ne v0, v4, :cond_0

    .line 148
    iput v4, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->docID:I

    move v3, v0

    .line 180
    :goto_0
    return v3

    .line 155
    :cond_0
    const/4 v2, 0x1

    .line 156
    .local v2, "i":I
    :goto_1
    iget-object v3, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->chunkStates:[Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;

    array-length v3, v3

    if-lt v2, v3, :cond_2

    .line 167
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->chunkStates:[Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;

    array-length v3, v3

    if-ne v2, v3, :cond_4

    .line 170
    iput v0, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->docID:I

    .line 171
    invoke-direct {p0}, Lorg/apache/lucene/search/ExactPhraseScorer;->phraseFreq()I

    move-result v3

    iput v3, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->freq:I

    .line 172
    iget v3, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->freq:I

    if-eqz v3, :cond_4

    .line 173
    iget v3, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->docID:I

    goto :goto_0

    .line 157
    :cond_2
    iget-object v3, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->chunkStates:[Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;

    aget-object v3, v3, v2

    iget-object v3, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posEnum:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v3}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->docID()I

    move-result v1

    .line 158
    .local v1, "doc2":I
    if-ge v1, v0, :cond_3

    .line 159
    iget-object v3, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->chunkStates:[Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;

    aget-object v3, v3, v2

    iget-object v3, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posEnum:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v3, v0}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->advance(I)I

    move-result v1

    .line 161
    :cond_3
    if-gt v1, v0, :cond_1

    .line 164
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 177
    .end local v1    # "doc2":I
    :cond_4
    iget-object v3, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->chunkStates:[Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;

    aget-object v3, v3, v5

    iget-object v3, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posEnum:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v3}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->nextDoc()I

    move-result v0

    .line 178
    if-ne v0, v4, :cond_0

    .line 179
    iput v0, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->docID:I

    move v3, v0

    .line 180
    goto :goto_0
.end method

.method public cost()J
    .locals 2

    .prologue
    .line 325
    iget-wide v0, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->cost:J

    return-wide v0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 197
    iget v0, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->docID:I

    return v0
.end method

.method public freq()I
    .locals 1

    .prologue
    .line 192
    iget v0, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->freq:I

    return v0
.end method

.method public nextDoc()I
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 95
    :cond_0
    iget-object v5, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->chunkStates:[Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;

    const/4 v6, 0x0

    aget-object v5, v5, v6

    iget-object v5, v5, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posEnum:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v5}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->nextDoc()I

    move-result v1

    .line 96
    .local v1, "doc":I
    const v5, 0x7fffffff

    if-ne v1, v5, :cond_1

    .line 97
    iput v1, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->docID:I

    .line 136
    .end local v1    # "doc":I
    :goto_0
    return v1

    .line 102
    .restart local v1    # "doc":I
    :cond_1
    const/4 v3, 0x1

    .line 103
    .local v3, "i":I
    :goto_1
    iget-object v5, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->chunkStates:[Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;

    array-length v5, v5

    if-lt v3, v5, :cond_3

    .line 129
    :cond_2
    iget-object v5, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->chunkStates:[Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;

    array-length v5, v5

    if-ne v3, v5, :cond_0

    .line 132
    iput v1, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->docID:I

    .line 134
    invoke-direct {p0}, Lorg/apache/lucene/search/ExactPhraseScorer;->phraseFreq()I

    move-result v5

    iput v5, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->freq:I

    .line 135
    iget v5, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->freq:I

    if-eqz v5, :cond_0

    .line 136
    iget v1, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->docID:I

    goto :goto_0

    .line 104
    :cond_3
    iget-object v5, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->chunkStates:[Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;

    aget-object v0, v5, v3

    .line 105
    .local v0, "cs":Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;
    iget-object v5, v0, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posEnum:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v5}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->docID()I

    move-result v2

    .line 106
    .local v2, "doc2":I
    iget-boolean v5, v0, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->useAdvance:Z

    if-eqz v5, :cond_5

    .line 107
    if-ge v2, v1, :cond_4

    .line 108
    iget-object v5, v0, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posEnum:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v5, v1}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->advance(I)I

    move-result v2

    .line 123
    :cond_4
    :goto_2
    if-gt v2, v1, :cond_2

    .line 126
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 111
    :cond_5
    const/4 v4, 0x0

    .line 112
    .local v4, "iter":I
    :goto_3
    if-ge v2, v1, :cond_4

    .line 115
    add-int/lit8 v4, v4, 0x1

    const/16 v5, 0x32

    if-ne v4, v5, :cond_6

    .line 116
    iget-object v5, v0, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posEnum:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v5, v1}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->advance(I)I

    move-result v2

    .line 117
    goto :goto_2

    .line 119
    :cond_6
    iget-object v5, v0, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posEnum:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v5}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->nextDoc()I

    move-result v2

    goto :goto_3
.end method

.method public score()F
    .locals 3

    .prologue
    .line 202
    iget-object v0, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->docScorer:Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;

    iget v1, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->docID:I

    iget v2, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->freq:I

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;->score(II)F

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 187
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ExactPhraseScorer("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->weight:Lorg/apache/lucene/search/Weight;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
