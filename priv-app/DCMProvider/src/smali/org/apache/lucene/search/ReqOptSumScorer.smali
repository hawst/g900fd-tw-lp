.class Lorg/apache/lucene/search/ReqOptSumScorer;
.super Lorg/apache/lucene/search/Scorer;
.source "ReqOptSumScorer.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private optScorer:Lorg/apache/lucene/search/Scorer;

.field private reqScorer:Lorg/apache/lucene/search/Scorer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lorg/apache/lucene/search/ReqOptSumScorer;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/ReqOptSumScorer;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/search/Scorer;Lorg/apache/lucene/search/Scorer;)V
    .locals 1
    .param p1, "reqScorer"    # Lorg/apache/lucene/search/Scorer;
    .param p2, "optScorer"    # Lorg/apache/lucene/search/Scorer;

    .prologue
    .line 43
    iget-object v0, p1, Lorg/apache/lucene/search/Scorer;->weight:Lorg/apache/lucene/search/Weight;

    invoke-direct {p0, v0}, Lorg/apache/lucene/search/Scorer;-><init>(Lorg/apache/lucene/search/Weight;)V

    .line 44
    sget-boolean v0, Lorg/apache/lucene/search/ReqOptSumScorer;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 45
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/search/ReqOptSumScorer;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 46
    :cond_1
    iput-object p1, p0, Lorg/apache/lucene/search/ReqOptSumScorer;->reqScorer:Lorg/apache/lucene/search/Scorer;

    .line 47
    iput-object p2, p0, Lorg/apache/lucene/search/ReqOptSumScorer;->optScorer:Lorg/apache/lucene/search/Scorer;

    .line 48
    return-void
.end method


# virtual methods
.method public advance(I)I
    .locals 1
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 57
    iget-object v0, p0, Lorg/apache/lucene/search/ReqOptSumScorer;->reqScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/Scorer;->advance(I)I

    move-result v0

    return v0
.end method

.method public cost()J
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lorg/apache/lucene/search/ReqOptSumScorer;->reqScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->cost()J

    move-result-wide v0

    return-wide v0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lorg/apache/lucene/search/ReqOptSumScorer;->reqScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v0

    return v0
.end method

.method public freq()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 91
    invoke-virtual {p0}, Lorg/apache/lucene/search/ReqOptSumScorer;->score()F

    .line 92
    iget-object v0, p0, Lorg/apache/lucene/search/ReqOptSumScorer;->optScorer:Lorg/apache/lucene/search/Scorer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/search/ReqOptSumScorer;->optScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/search/ReqOptSumScorer;->reqScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v1}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getChildren()Ljava/util/Collection;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/search/Scorer$ChildScorer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 97
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 98
    .local v0, "children":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/search/Scorer$ChildScorer;>;"
    new-instance v1, Lorg/apache/lucene/search/Scorer$ChildScorer;

    iget-object v2, p0, Lorg/apache/lucene/search/ReqOptSumScorer;->reqScorer:Lorg/apache/lucene/search/Scorer;

    const-string v3, "MUST"

    invoke-direct {v1, v2, v3}, Lorg/apache/lucene/search/Scorer$ChildScorer;-><init>(Lorg/apache/lucene/search/Scorer;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 99
    new-instance v1, Lorg/apache/lucene/search/Scorer$ChildScorer;

    iget-object v2, p0, Lorg/apache/lucene/search/ReqOptSumScorer;->optScorer:Lorg/apache/lucene/search/Scorer;

    const-string v3, "SHOULD"

    invoke-direct {v1, v2, v3}, Lorg/apache/lucene/search/Scorer$ChildScorer;-><init>(Lorg/apache/lucene/search/Scorer;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 100
    return-object v0
.end method

.method public nextDoc()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52
    iget-object v0, p0, Lorg/apache/lucene/search/ReqOptSumScorer;->reqScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->nextDoc()I

    move-result v0

    return v0
.end method

.method public score()F
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 73
    iget-object v3, p0, Lorg/apache/lucene/search/ReqOptSumScorer;->reqScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v3}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v0

    .line 74
    .local v0, "curDoc":I
    iget-object v3, p0, Lorg/apache/lucene/search/ReqOptSumScorer;->reqScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v3}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v2

    .line 75
    .local v2, "reqScore":F
    iget-object v3, p0, Lorg/apache/lucene/search/ReqOptSumScorer;->optScorer:Lorg/apache/lucene/search/Scorer;

    if-nez v3, :cond_1

    .line 85
    .end local v2    # "reqScore":F
    :cond_0
    :goto_0
    return v2

    .line 79
    .restart local v2    # "reqScore":F
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/search/ReqOptSumScorer;->optScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v3}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v1

    .line 80
    .local v1, "optScorerDoc":I
    if-ge v1, v0, :cond_2

    iget-object v3, p0, Lorg/apache/lucene/search/ReqOptSumScorer;->optScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v3, v0}, Lorg/apache/lucene/search/Scorer;->advance(I)I

    move-result v1

    const v3, 0x7fffffff

    if-ne v1, v3, :cond_2

    .line 81
    const/4 v3, 0x0

    iput-object v3, p0, Lorg/apache/lucene/search/ReqOptSumScorer;->optScorer:Lorg/apache/lucene/search/Scorer;

    goto :goto_0

    .line 85
    :cond_2
    if-ne v1, v0, :cond_0

    iget-object v3, p0, Lorg/apache/lucene/search/ReqOptSumScorer;->optScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v3}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v3

    add-float/2addr v2, v3

    goto :goto_0
.end method
