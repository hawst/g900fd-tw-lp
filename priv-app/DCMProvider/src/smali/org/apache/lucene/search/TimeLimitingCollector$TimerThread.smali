.class public final Lorg/apache/lucene/search/TimeLimitingCollector$TimerThread;
.super Ljava/lang/Thread;
.source "TimeLimitingCollector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/TimeLimitingCollector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TimerThread"
.end annotation


# static fields
.field public static final DEFAULT_RESOLUTION:I = 0x14

.field public static final THREAD_NAME:Ljava/lang/String; = "TimeLimitedCollector timer thread"


# instance fields
.field final counter:Lorg/apache/lucene/util/Counter;

.field private volatile resolution:J

.field private volatile stop:Z

.field private volatile time:J


# direct methods
.method public constructor <init>(JLorg/apache/lucene/util/Counter;)V
    .locals 3
    .param p1, "resolution"    # J
    .param p3, "counter"    # Lorg/apache/lucene/util/Counter;

    .prologue
    .line 251
    const-string v0, "TimeLimitedCollector timer thread"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 245
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/apache/lucene/search/TimeLimitingCollector$TimerThread;->time:J

    .line 246
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/search/TimeLimitingCollector$TimerThread;->stop:Z

    .line 252
    iput-wide p1, p0, Lorg/apache/lucene/search/TimeLimitingCollector$TimerThread;->resolution:J

    .line 253
    iput-object p3, p0, Lorg/apache/lucene/search/TimeLimitingCollector$TimerThread;->counter:Lorg/apache/lucene/util/Counter;

    .line 254
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lorg/apache/lucene/search/TimeLimitingCollector$TimerThread;->setDaemon(Z)V

    .line 255
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Counter;)V
    .locals 2
    .param p1, "counter"    # Lorg/apache/lucene/util/Counter;

    .prologue
    .line 258
    const-wide/16 v0, 0x14

    invoke-direct {p0, v0, v1, p1}, Lorg/apache/lucene/search/TimeLimitingCollector$TimerThread;-><init>(JLorg/apache/lucene/util/Counter;)V

    .line 259
    return-void
.end method


# virtual methods
.method public getMilliseconds()J
    .locals 2

    .prologue
    .line 278
    iget-wide v0, p0, Lorg/apache/lucene/search/TimeLimitingCollector$TimerThread;->time:J

    return-wide v0
.end method

.method public getResolution()J
    .locals 2

    .prologue
    .line 293
    iget-wide v0, p0, Lorg/apache/lucene/search/TimeLimitingCollector$TimerThread;->resolution:J

    return-wide v0
.end method

.method public run()V
    .locals 4

    .prologue
    .line 263
    :goto_0
    iget-boolean v1, p0, Lorg/apache/lucene/search/TimeLimitingCollector$TimerThread;->stop:Z

    if-eqz v1, :cond_0

    .line 272
    return-void

    .line 265
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/TimeLimitingCollector$TimerThread;->counter:Lorg/apache/lucene/util/Counter;

    iget-wide v2, p0, Lorg/apache/lucene/search/TimeLimitingCollector$TimerThread;->resolution:J

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/util/Counter;->addAndGet(J)J

    .line 267
    :try_start_0
    iget-wide v2, p0, Lorg/apache/lucene/search/TimeLimitingCollector$TimerThread;->resolution:J

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 268
    :catch_0
    move-exception v0

    .line 269
    .local v0, "ie":Ljava/lang/InterruptedException;
    new-instance v1, Lorg/apache/lucene/util/ThreadInterruptedException;

    invoke-direct {v1, v0}, Lorg/apache/lucene/util/ThreadInterruptedException;-><init>(Ljava/lang/InterruptedException;)V

    throw v1
.end method

.method public setResolution(J)V
    .locals 3
    .param p1, "resolution"    # J

    .prologue
    .line 311
    const-wide/16 v0, 0x5

    invoke-static {p1, p2, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/lucene/search/TimeLimitingCollector$TimerThread;->resolution:J

    .line 312
    return-void
.end method

.method public stopTimer()V
    .locals 1

    .prologue
    .line 285
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/search/TimeLimitingCollector$TimerThread;->stop:Z

    .line 286
    return-void
.end method
