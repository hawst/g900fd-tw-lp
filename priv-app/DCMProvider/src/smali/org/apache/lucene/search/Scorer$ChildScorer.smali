.class public Lorg/apache/lucene/search/Scorer$ChildScorer;
.super Ljava/lang/Object;
.source "Scorer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/Scorer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ChildScorer"
.end annotation


# instance fields
.field public final child:Lorg/apache/lucene/search/Scorer;

.field public final relationship:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/Scorer;Ljava/lang/String;)V
    .locals 0
    .param p1, "child"    # Lorg/apache/lucene/search/Scorer;
    .param p2, "relationship"    # Ljava/lang/String;

    .prologue
    .line 133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 134
    iput-object p1, p0, Lorg/apache/lucene/search/Scorer$ChildScorer;->child:Lorg/apache/lucene/search/Scorer;

    .line 135
    iput-object p2, p0, Lorg/apache/lucene/search/Scorer$ChildScorer;->relationship:Ljava/lang/String;

    .line 136
    return-void
.end method
