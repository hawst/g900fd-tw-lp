.class Lorg/apache/lucene/search/DocTermOrdsRewriteMethod$MultiTermQueryDocTermOrdsWrapperFilter$1;
.super Lorg/apache/lucene/index/Terms;
.source "DocTermOrdsRewriteMethod.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/DocTermOrdsRewriteMethod$MultiTermQueryDocTermOrdsWrapperFilter;->getDocIdSet(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/DocIdSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lorg/apache/lucene/search/DocTermOrdsRewriteMethod$MultiTermQueryDocTermOrdsWrapperFilter;

.field private final synthetic val$docTermOrds:Lorg/apache/lucene/index/SortedSetDocValues;


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/DocTermOrdsRewriteMethod$MultiTermQueryDocTermOrdsWrapperFilter;Lorg/apache/lucene/index/SortedSetDocValues;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/search/DocTermOrdsRewriteMethod$MultiTermQueryDocTermOrdsWrapperFilter$1;->this$1:Lorg/apache/lucene/search/DocTermOrdsRewriteMethod$MultiTermQueryDocTermOrdsWrapperFilter;

    iput-object p2, p0, Lorg/apache/lucene/search/DocTermOrdsRewriteMethod$MultiTermQueryDocTermOrdsWrapperFilter$1;->val$docTermOrds:Lorg/apache/lucene/index/SortedSetDocValues;

    .line 91
    invoke-direct {p0}, Lorg/apache/lucene/index/Terms;-><init>()V

    return-void
.end method


# virtual methods
.method public getComparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation

    .prologue
    .line 95
    invoke-static {}, Lorg/apache/lucene/util/BytesRef;->getUTF8SortedAsUnicodeComparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public getDocCount()I
    .locals 1

    .prologue
    .line 115
    const/4 v0, -0x1

    return v0
.end method

.method public getSumDocFreq()J
    .locals 2

    .prologue
    .line 110
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public getSumTotalTermFreq()J
    .locals 2

    .prologue
    .line 105
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public hasOffsets()Z
    .locals 1

    .prologue
    .line 125
    const/4 v0, 0x0

    return v0
.end method

.method public hasPayloads()Z
    .locals 1

    .prologue
    .line 135
    const/4 v0, 0x0

    return v0
.end method

.method public hasPositions()Z
    .locals 1

    .prologue
    .line 130
    const/4 v0, 0x0

    return v0
.end method

.method public iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;
    .locals 1
    .param p1, "reuse"    # Lorg/apache/lucene/index/TermsEnum;

    .prologue
    .line 100
    iget-object v0, p0, Lorg/apache/lucene/search/DocTermOrdsRewriteMethod$MultiTermQueryDocTermOrdsWrapperFilter$1;->val$docTermOrds:Lorg/apache/lucene/index/SortedSetDocValues;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SortedSetDocValues;->termsEnum()Lorg/apache/lucene/index/TermsEnum;

    move-result-object v0

    return-object v0
.end method

.method public size()J
    .locals 2

    .prologue
    .line 120
    const-wide/16 v0, -0x1

    return-wide v0
.end method
