.class public Lorg/apache/lucene/search/TermRangeTermsEnum;
.super Lorg/apache/lucene/index/FilteredTermsEnum;
.source "TermRangeTermsEnum.java"


# instance fields
.field private final includeLower:Z

.field private final includeUpper:Z

.field private final lowerBytesRef:Lorg/apache/lucene/util/BytesRef;

.field private final termComp:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation
.end field

.field private final upperBytesRef:Lorg/apache/lucene/util/BytesRef;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/TermsEnum;Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;ZZ)V
    .locals 2
    .param p1, "tenum"    # Lorg/apache/lucene/index/TermsEnum;
    .param p2, "lowerTerm"    # Lorg/apache/lucene/util/BytesRef;
    .param p3, "upperTerm"    # Lorg/apache/lucene/util/BytesRef;
    .param p4, "includeLower"    # Z
    .param p5, "includeUpper"    # Z

    .prologue
    const/4 v1, 0x1

    .line 64
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/FilteredTermsEnum;-><init>(Lorg/apache/lucene/index/TermsEnum;)V

    .line 68
    if-nez p2, :cond_0

    .line 69
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/search/TermRangeTermsEnum;->lowerBytesRef:Lorg/apache/lucene/util/BytesRef;

    .line 70
    iput-boolean v1, p0, Lorg/apache/lucene/search/TermRangeTermsEnum;->includeLower:Z

    .line 76
    :goto_0
    if-nez p3, :cond_1

    .line 77
    iput-boolean v1, p0, Lorg/apache/lucene/search/TermRangeTermsEnum;->includeUpper:Z

    .line 78
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/search/TermRangeTermsEnum;->upperBytesRef:Lorg/apache/lucene/util/BytesRef;

    .line 84
    :goto_1
    iget-object v0, p0, Lorg/apache/lucene/search/TermRangeTermsEnum;->lowerBytesRef:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/search/TermRangeTermsEnum;->setInitialSeekTerm(Lorg/apache/lucene/util/BytesRef;)V

    .line 85
    invoke-virtual {p0}, Lorg/apache/lucene/search/TermRangeTermsEnum;->getComparator()Ljava/util/Comparator;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/TermRangeTermsEnum;->termComp:Ljava/util/Comparator;

    .line 86
    return-void

    .line 72
    :cond_0
    iput-object p2, p0, Lorg/apache/lucene/search/TermRangeTermsEnum;->lowerBytesRef:Lorg/apache/lucene/util/BytesRef;

    .line 73
    iput-boolean p4, p0, Lorg/apache/lucene/search/TermRangeTermsEnum;->includeLower:Z

    goto :goto_0

    .line 80
    :cond_1
    iput-boolean p5, p0, Lorg/apache/lucene/search/TermRangeTermsEnum;->includeUpper:Z

    .line 81
    iput-object p3, p0, Lorg/apache/lucene/search/TermRangeTermsEnum;->upperBytesRef:Lorg/apache/lucene/util/BytesRef;

    goto :goto_1
.end method


# virtual methods
.method protected accept(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;
    .locals 3
    .param p1, "term"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 90
    iget-boolean v1, p0, Lorg/apache/lucene/search/TermRangeTermsEnum;->includeLower:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/search/TermRangeTermsEnum;->lowerBytesRef:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {p1, v1}, Lorg/apache/lucene/util/BytesRef;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 91
    sget-object v1, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;->NO:Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

    .line 106
    :goto_0
    return-object v1

    .line 94
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/TermRangeTermsEnum;->upperBytesRef:Lorg/apache/lucene/util/BytesRef;

    if-eqz v1, :cond_2

    .line 95
    iget-object v1, p0, Lorg/apache/lucene/search/TermRangeTermsEnum;->termComp:Ljava/util/Comparator;

    iget-object v2, p0, Lorg/apache/lucene/search/TermRangeTermsEnum;->upperBytesRef:Lorg/apache/lucene/util/BytesRef;

    invoke-interface {v1, v2, p1}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    .line 100
    .local v0, "cmp":I
    if-ltz v0, :cond_1

    .line 101
    iget-boolean v1, p0, Lorg/apache/lucene/search/TermRangeTermsEnum;->includeUpper:Z

    if-nez v1, :cond_2

    if-nez v0, :cond_2

    .line 102
    :cond_1
    sget-object v1, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;->END:Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

    goto :goto_0

    .line 106
    .end local v0    # "cmp":I
    :cond_2
    sget-object v1, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;->YES:Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

    goto :goto_0
.end method
