.class Lorg/apache/lucene/search/MinShouldMatchSumScorer$1;
.super Ljava/lang/Object;
.source "MinShouldMatchSumScorer.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/MinShouldMatchSumScorer;-><init>(Lorg/apache/lucene/search/Weight;Ljava/util/List;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lorg/apache/lucene/search/Scorer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/search/MinShouldMatchSumScorer;


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/MinShouldMatchSumScorer;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer$1;->this$0:Lorg/apache/lucene/search/MinShouldMatchSumScorer;

    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/search/Scorer;

    check-cast p2, Lorg/apache/lucene/search/Scorer;

    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/search/MinShouldMatchSumScorer$1;->compare(Lorg/apache/lucene/search/Scorer;Lorg/apache/lucene/search/Scorer;)I

    move-result v0

    return v0
.end method

.method public compare(Lorg/apache/lucene/search/Scorer;Lorg/apache/lucene/search/Scorer;)I
    .locals 4
    .param p1, "o1"    # Lorg/apache/lucene/search/Scorer;
    .param p2, "o2"    # Lorg/apache/lucene/search/Scorer;

    .prologue
    .line 93
    invoke-virtual {p2}, Lorg/apache/lucene/search/Scorer;->cost()J

    move-result-wide v0

    invoke-virtual {p1}, Lorg/apache/lucene/search/Scorer;->cost()J

    move-result-wide v2

    sub-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->signum(J)I

    move-result v0

    return v0
.end method
