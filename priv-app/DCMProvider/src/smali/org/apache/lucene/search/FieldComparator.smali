.class public abstract Lorg/apache/lucene/search/FieldComparator;
.super Ljava/lang/Object;
.source "FieldComparator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/FieldComparator$ByteComparator;,
        Lorg/apache/lucene/search/FieldComparator$DocComparator;,
        Lorg/apache/lucene/search/FieldComparator$DoubleComparator;,
        Lorg/apache/lucene/search/FieldComparator$FloatComparator;,
        Lorg/apache/lucene/search/FieldComparator$IntComparator;,
        Lorg/apache/lucene/search/FieldComparator$LongComparator;,
        Lorg/apache/lucene/search/FieldComparator$NumericComparator;,
        Lorg/apache/lucene/search/FieldComparator$RelevanceComparator;,
        Lorg/apache/lucene/search/FieldComparator$ShortComparator;,
        Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;,
        Lorg/apache/lucene/search/FieldComparator$TermValComparator;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 85
    .local p0, "this":Lorg/apache/lucene/search/FieldComparator;, "Lorg/apache/lucene/search/FieldComparator<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract compare(II)I
.end method

.method public abstract compareBottom(I)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract compareDocToValue(ILjava/lang/Object;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITT;)I"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public compareValues(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TT;)I"
        }
    .end annotation

    .prologue
    .line 176
    .local p0, "this":Lorg/apache/lucene/search/FieldComparator;, "Lorg/apache/lucene/search/FieldComparator<TT;>;"
    .local p1, "first":Ljava/lang/Object;, "TT;"
    .local p2, "second":Ljava/lang/Object;, "TT;"
    if-nez p1, :cond_1

    .line 177
    if-nez p2, :cond_0

    .line 178
    const/4 v0, 0x0

    .line 185
    .end local p1    # "first":Ljava/lang/Object;, "TT;"
    :goto_0
    return v0

    .line 180
    .restart local p1    # "first":Ljava/lang/Object;, "TT;"
    :cond_0
    const/4 v0, -0x1

    goto :goto_0

    .line 182
    :cond_1
    if-nez p2, :cond_2

    .line 183
    const/4 v0, 0x1

    goto :goto_0

    .line 185
    :cond_2
    check-cast p1, Ljava/lang/Comparable;

    .end local p1    # "first":Ljava/lang/Object;, "TT;"
    invoke-interface {p1, p2}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v0

    goto :goto_0
.end method

.method public abstract copy(II)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract setBottom(I)V
.end method

.method public abstract setNextReader(Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/search/FieldComparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/AtomicReaderContext;",
            ")",
            "Lorg/apache/lucene/search/FieldComparator",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public setScorer(Lorg/apache/lucene/search/Scorer;)V
    .locals 0
    .param p1, "scorer"    # Lorg/apache/lucene/search/Scorer;

    .prologue
    .line 159
    .local p0, "this":Lorg/apache/lucene/search/FieldComparator;, "Lorg/apache/lucene/search/FieldComparator<TT;>;"
    return-void
.end method

.method public abstract value(I)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation
.end method
