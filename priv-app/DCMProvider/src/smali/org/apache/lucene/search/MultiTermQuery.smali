.class public abstract Lorg/apache/lucene/search/MultiTermQuery;
.super Lorg/apache/lucene/search/Query;
.source "MultiTermQuery.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/MultiTermQuery$ConstantScoreAutoRewrite;,
        Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;,
        Lorg/apache/lucene/search/MultiTermQuery$TopTermsBoostOnlyBooleanQueryRewrite;,
        Lorg/apache/lucene/search/MultiTermQuery$TopTermsScoringBooleanQueryRewrite;
    }
.end annotation


# static fields
.field public static final CONSTANT_SCORE_AUTO_REWRITE_DEFAULT:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

.field public static final CONSTANT_SCORE_BOOLEAN_QUERY_REWRITE:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

.field public static final CONSTANT_SCORE_FILTER_REWRITE:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

.field public static final SCORING_BOOLEAN_QUERY_REWRITE:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;


# instance fields
.field protected final field:Ljava/lang/String;

.field protected rewriteMethod:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 92
    new-instance v0, Lorg/apache/lucene/search/MultiTermQuery$1;

    invoke-direct {v0}, Lorg/apache/lucene/search/MultiTermQuery$1;-><init>()V

    sput-object v0, Lorg/apache/lucene/search/MultiTermQuery;->CONSTANT_SCORE_FILTER_REWRITE:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    .line 114
    sget-object v0, Lorg/apache/lucene/search/ScoringRewrite;->SCORING_BOOLEAN_QUERY_REWRITE:Lorg/apache/lucene/search/ScoringRewrite;

    sput-object v0, Lorg/apache/lucene/search/MultiTermQuery;->SCORING_BOOLEAN_QUERY_REWRITE:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    .line 126
    sget-object v0, Lorg/apache/lucene/search/ScoringRewrite;->CONSTANT_SCORE_BOOLEAN_QUERY_REWRITE:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    sput-object v0, Lorg/apache/lucene/search/MultiTermQuery;->CONSTANT_SCORE_BOOLEAN_QUERY_REWRITE:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    .line 234
    new-instance v0, Lorg/apache/lucene/search/MultiTermQuery$2;

    invoke-direct {v0}, Lorg/apache/lucene/search/MultiTermQuery$2;-><init>()V

    sput-object v0, Lorg/apache/lucene/search/MultiTermQuery;->CONSTANT_SCORE_AUTO_REWRITE_DEFAULT:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    .line 244
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 250
    invoke-direct {p0}, Lorg/apache/lucene/search/Query;-><init>()V

    .line 66
    sget-object v0, Lorg/apache/lucene/search/MultiTermQuery;->CONSTANT_SCORE_AUTO_REWRITE_DEFAULT:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    iput-object v0, p0, Lorg/apache/lucene/search/MultiTermQuery;->rewriteMethod:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    .line 251
    if-nez p1, :cond_0

    .line 252
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "field must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 254
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/search/MultiTermQuery;->field:Ljava/lang/String;

    .line 255
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 318
    if-ne p0, p1, :cond_1

    move v2, v1

    .line 330
    :cond_0
    :goto_0
    return v2

    .line 320
    :cond_1
    if-eqz p1, :cond_0

    .line 322
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-ne v3, v4, :cond_0

    move-object v0, p1

    .line 324
    check-cast v0, Lorg/apache/lucene/search/MultiTermQuery;

    .line 325
    .local v0, "other":Lorg/apache/lucene/search/MultiTermQuery;
    invoke-virtual {p0}, Lorg/apache/lucene/search/MultiTermQuery;->getBoost()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    invoke-virtual {v0}, Lorg/apache/lucene/search/MultiTermQuery;->getBoost()F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    if-ne v3, v4, :cond_0

    .line 327
    iget-object v3, p0, Lorg/apache/lucene/search/MultiTermQuery;->rewriteMethod:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    iget-object v4, v0, Lorg/apache/lucene/search/MultiTermQuery;->rewriteMethod:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 330
    iget-object v3, v0, Lorg/apache/lucene/search/MultiTermQuery;->field:Ljava/lang/String;

    if-nez v3, :cond_3

    iget-object v3, p0, Lorg/apache/lucene/search/MultiTermQuery;->field:Ljava/lang/String;

    if-nez v3, :cond_2

    :goto_1
    move v2, v1

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1

    :cond_3
    iget-object v1, v0, Lorg/apache/lucene/search/MultiTermQuery;->field:Ljava/lang/String;

    iget-object v2, p0, Lorg/apache/lucene/search/MultiTermQuery;->field:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_1
.end method

.method public final getField()Ljava/lang/String;
    .locals 1

    .prologue
    .line 258
    iget-object v0, p0, Lorg/apache/lucene/search/MultiTermQuery;->field:Ljava/lang/String;

    return-object v0
.end method

.method public getRewriteMethod()Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;
    .locals 1

    .prologue
    .line 295
    iget-object v0, p0, Lorg/apache/lucene/search/MultiTermQuery;->rewriteMethod:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    return-object v0
.end method

.method protected final getTermsEnum(Lorg/apache/lucene/index/Terms;)Lorg/apache/lucene/index/TermsEnum;
    .locals 1
    .param p1, "terms"    # Lorg/apache/lucene/index/Terms;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 278
    new-instance v0, Lorg/apache/lucene/util/AttributeSource;

    invoke-direct {v0}, Lorg/apache/lucene/util/AttributeSource;-><init>()V

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/search/MultiTermQuery;->getTermsEnum(Lorg/apache/lucene/index/Terms;Lorg/apache/lucene/util/AttributeSource;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v0

    return-object v0
.end method

.method protected abstract getTermsEnum(Lorg/apache/lucene/index/Terms;Lorg/apache/lucene/util/AttributeSource;)Lorg/apache/lucene/index/TermsEnum;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 308
    const/16 v0, 0x1f

    .line 309
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 310
    .local v1, "result":I
    invoke-virtual {p0}, Lorg/apache/lucene/search/MultiTermQuery;->getBoost()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int/lit8 v1, v2, 0x1f

    .line 311
    mul-int/lit8 v2, v1, 0x1f

    iget-object v3, p0, Lorg/apache/lucene/search/MultiTermQuery;->rewriteMethod:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    add-int v1, v2, v3

    .line 312
    iget-object v2, p0, Lorg/apache/lucene/search/MultiTermQuery;->field:Ljava/lang/String;

    if-eqz v2, :cond_0

    mul-int/lit8 v2, v1, 0x1f

    iget-object v3, p0, Lorg/apache/lucene/search/MultiTermQuery;->field:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    add-int v1, v2, v3

    .line 313
    :cond_0
    return v1
.end method

.method public final rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;
    .locals 1
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 288
    iget-object v0, p0, Lorg/apache/lucene/search/MultiTermQuery;->rewriteMethod:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    invoke-virtual {v0, p1, p0}, Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;->rewrite(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/MultiTermQuery;)Lorg/apache/lucene/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public setRewriteMethod(Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;)V
    .locals 0
    .param p1, "method"    # Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    .prologue
    .line 303
    iput-object p1, p0, Lorg/apache/lucene/search/MultiTermQuery;->rewriteMethod:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    .line 304
    return-void
.end method
