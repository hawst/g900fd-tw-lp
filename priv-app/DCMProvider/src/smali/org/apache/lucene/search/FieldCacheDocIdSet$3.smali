.class Lorg/apache/lucene/search/FieldCacheDocIdSet$3;
.super Lorg/apache/lucene/search/DocIdSetIterator;
.source "FieldCacheDocIdSet.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/FieldCacheDocIdSet;->iterator()Lorg/apache/lucene/search/DocIdSetIterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private doc:I

.field final synthetic this$0:Lorg/apache/lucene/search/FieldCacheDocIdSet;


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/FieldCacheDocIdSet;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$3;->this$0:Lorg/apache/lucene/search/FieldCacheDocIdSet;

    .line 86
    invoke-direct {p0}, Lorg/apache/lucene/search/DocIdSetIterator;-><init>()V

    .line 87
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$3;->doc:I

    return-void
.end method


# virtual methods
.method public advance(I)I
    .locals 2
    .param p1, "target"    # I

    .prologue
    .line 107
    iput p1, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$3;->doc:I

    :goto_0
    iget v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$3;->doc:I

    iget-object v1, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$3;->this$0:Lorg/apache/lucene/search/FieldCacheDocIdSet;

    iget v1, v1, Lorg/apache/lucene/search/FieldCacheDocIdSet;->maxDoc:I

    if-lt v0, v1, :cond_0

    .line 112
    const v0, 0x7fffffff

    iput v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$3;->doc:I

    :goto_1
    return v0

    .line 108
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$3;->this$0:Lorg/apache/lucene/search/FieldCacheDocIdSet;

    iget v1, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$3;->doc:I

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/FieldCacheDocIdSet;->matchDoc(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 109
    iget v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$3;->doc:I

    goto :goto_1

    .line 107
    :cond_1
    iget v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$3;->doc:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$3;->doc:I

    goto :goto_0
.end method

.method public cost()J
    .locals 2

    .prologue
    .line 117
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$3;->this$0:Lorg/apache/lucene/search/FieldCacheDocIdSet;

    iget v0, v0, Lorg/apache/lucene/search/FieldCacheDocIdSet;->maxDoc:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 91
    iget v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$3;->doc:I

    return v0
.end method

.method public nextDoc()I
    .locals 2

    .prologue
    .line 97
    :cond_0
    iget v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$3;->doc:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$3;->doc:I

    .line 98
    iget v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$3;->doc:I

    iget-object v1, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$3;->this$0:Lorg/apache/lucene/search/FieldCacheDocIdSet;

    iget v1, v1, Lorg/apache/lucene/search/FieldCacheDocIdSet;->maxDoc:I

    if-lt v0, v1, :cond_1

    .line 99
    const v0, 0x7fffffff

    iput v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$3;->doc:I

    .line 102
    :goto_0
    return v0

    .line 101
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$3;->this$0:Lorg/apache/lucene/search/FieldCacheDocIdSet;

    iget v1, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$3;->doc:I

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/FieldCacheDocIdSet;->matchDoc(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    iget v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$3;->doc:I

    goto :goto_0
.end method
