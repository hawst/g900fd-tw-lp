.class Lorg/apache/lucene/search/FieldCacheImpl$ShortCache$1;
.super Lorg/apache/lucene/search/FieldCacheImpl$Uninvert;
.source "FieldCacheImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/FieldCacheImpl$ShortCache;->createValue(Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;Z)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private currentValue:S

.field final synthetic this$1:Lorg/apache/lucene/search/FieldCacheImpl$ShortCache;

.field private final synthetic val$parser:Lorg/apache/lucene/search/FieldCache$ShortParser;

.field private final synthetic val$values:[S


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/FieldCacheImpl$ShortCache;Lorg/apache/lucene/search/FieldCache$ShortParser;[S)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/search/FieldCacheImpl$ShortCache$1;->this$1:Lorg/apache/lucene/search/FieldCacheImpl$ShortCache;

    iput-object p2, p0, Lorg/apache/lucene/search/FieldCacheImpl$ShortCache$1;->val$parser:Lorg/apache/lucene/search/FieldCache$ShortParser;

    iput-object p3, p0, Lorg/apache/lucene/search/FieldCacheImpl$ShortCache$1;->val$values:[S

    .line 516
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/lucene/search/FieldCacheImpl$Uninvert;-><init>(Lorg/apache/lucene/search/FieldCacheImpl$Uninvert;)V

    return-void
.end method


# virtual methods
.method protected termsEnum(Lorg/apache/lucene/index/Terms;)Lorg/apache/lucene/index/TermsEnum;
    .locals 1
    .param p1, "terms"    # Lorg/apache/lucene/index/Terms;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 531
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl$ShortCache$1;->val$parser:Lorg/apache/lucene/search/FieldCache$ShortParser;

    invoke-interface {v0, p1}, Lorg/apache/lucene/search/FieldCache$ShortParser;->termsEnum(Lorg/apache/lucene/index/Terms;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v0

    return-object v0
.end method

.method public visitDoc(I)V
    .locals 2
    .param p1, "docID"    # I

    .prologue
    .line 526
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl$ShortCache$1;->val$values:[S

    iget-short v1, p0, Lorg/apache/lucene/search/FieldCacheImpl$ShortCache$1;->currentValue:S

    aput-short v1, v0, p1

    .line 527
    return-void
.end method

.method public visitTerm(Lorg/apache/lucene/util/BytesRef;)V
    .locals 1
    .param p1, "term"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 521
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl$ShortCache$1;->val$parser:Lorg/apache/lucene/search/FieldCache$ShortParser;

    invoke-interface {v0, p1}, Lorg/apache/lucene/search/FieldCache$ShortParser;->parseShort(Lorg/apache/lucene/util/BytesRef;)S

    move-result v0

    iput-short v0, p0, Lorg/apache/lucene/search/FieldCacheImpl$ShortCache$1;->currentValue:S

    .line 522
    return-void
.end method
