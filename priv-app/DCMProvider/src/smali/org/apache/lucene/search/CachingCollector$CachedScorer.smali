.class final Lorg/apache/lucene/search/CachingCollector$CachedScorer;
.super Lorg/apache/lucene/search/Scorer;
.source "CachingCollector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/CachingCollector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "CachedScorer"
.end annotation


# instance fields
.field doc:I

.field score:F


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/lucene/search/Scorer;-><init>(Lorg/apache/lucene/search/Weight;)V

    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/search/CachingCollector$CachedScorer;)V
    .locals 0

    .prologue
    .line 76
    invoke-direct {p0}, Lorg/apache/lucene/search/CachingCollector$CachedScorer;-><init>()V

    return-void
.end method


# virtual methods
.method public final advance(I)I
    .locals 1
    .param p1, "target"    # I

    .prologue
    .line 82
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public cost()J
    .locals 2

    .prologue
    .line 94
    const-wide/16 v0, 0x1

    return-wide v0
.end method

.method public final docID()I
    .locals 1

    .prologue
    .line 85
    iget v0, p0, Lorg/apache/lucene/search/CachingCollector$CachedScorer;->doc:I

    return v0
.end method

.method public final freq()I
    .locals 1

    .prologue
    .line 88
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final nextDoc()I
    .locals 1

    .prologue
    .line 91
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final score()F
    .locals 1

    .prologue
    .line 79
    iget v0, p0, Lorg/apache/lucene/search/CachingCollector$CachedScorer;->score:F

    return v0
.end method
