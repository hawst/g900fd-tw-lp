.class public abstract Lorg/apache/lucene/search/TopTermsRewrite;
.super Lorg/apache/lucene/search/TermCollectingRewrite;
.source "TopTermsRewrite.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Q:",
        "Lorg/apache/lucene/search/Query;",
        ">",
        "Lorg/apache/lucene/search/TermCollectingRewrite",
        "<TQ;>;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final scoreTermSortByTermComp:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final size:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lorg/apache/lucene/search/TopTermsRewrite;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/TopTermsRewrite;->$assertionsDisabled:Z

    .line 185
    new-instance v0, Lorg/apache/lucene/search/TopTermsRewrite$1;

    invoke-direct {v0}, Lorg/apache/lucene/search/TopTermsRewrite$1;-><init>()V

    .line 184
    sput-object v0, Lorg/apache/lucene/search/TopTermsRewrite;->scoreTermSortByTermComp:Ljava/util/Comparator;

    .line 192
    return-void

    .line 39
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(I)V
    .locals 0
    .param p1, "size"    # I

    .prologue
    .line 50
    .local p0, "this":Lorg/apache/lucene/search/TopTermsRewrite;, "Lorg/apache/lucene/search/TopTermsRewrite<TQ;>;"
    invoke-direct {p0}, Lorg/apache/lucene/search/TermCollectingRewrite;-><init>()V

    .line 51
    iput p1, p0, Lorg/apache/lucene/search/TopTermsRewrite;->size:I

    .line 52
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .local p0, "this":Lorg/apache/lucene/search/TopTermsRewrite;, "Lorg/apache/lucene/search/TopTermsRewrite<TQ;>;"
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 176
    if-ne p0, p1, :cond_1

    .line 181
    :cond_0
    :goto_0
    return v1

    .line 177
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    goto :goto_0

    .line 178
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 179
    check-cast v0, Lorg/apache/lucene/search/TopTermsRewrite;

    .line 180
    .local v0, "other":Lorg/apache/lucene/search/TopTermsRewrite;, "Lorg/apache/lucene/search/TopTermsRewrite<*>;"
    iget v3, p0, Lorg/apache/lucene/search/TopTermsRewrite;->size:I

    iget v4, v0, Lorg/apache/lucene/search/TopTermsRewrite;->size:I

    if-eq v3, v4, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method protected abstract getMaxSize()I
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 56
    .local p0, "this":Lorg/apache/lucene/search/TopTermsRewrite;, "Lorg/apache/lucene/search/TopTermsRewrite<TQ;>;"
    iget v0, p0, Lorg/apache/lucene/search/TopTermsRewrite;->size:I

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 171
    .local p0, "this":Lorg/apache/lucene/search/TopTermsRewrite;, "Lorg/apache/lucene/search/TopTermsRewrite<TQ;>;"
    iget v0, p0, Lorg/apache/lucene/search/TopTermsRewrite;->size:I

    mul-int/lit8 v0, v0, 0x1f

    return v0
.end method

.method public final rewrite(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/MultiTermQuery;)Lorg/apache/lucene/search/Query;
    .locals 12
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "query"    # Lorg/apache/lucene/search/MultiTermQuery;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/IndexReader;",
            "Lorg/apache/lucene/search/MultiTermQuery;",
            ")TQ;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 64
    .local p0, "this":Lorg/apache/lucene/search/TopTermsRewrite;, "Lorg/apache/lucene/search/TopTermsRewrite<TQ;>;"
    iget v0, p0, Lorg/apache/lucene/search/TopTermsRewrite;->size:I

    invoke-virtual {p0}, Lorg/apache/lucene/search/TopTermsRewrite;->getMaxSize()I

    move-result v3

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v6

    .line 65
    .local v6, "maxSize":I
    new-instance v9, Ljava/util/PriorityQueue;

    invoke-direct {v9}, Ljava/util/PriorityQueue;-><init>()V

    .line 66
    .local v9, "stQueue":Ljava/util/PriorityQueue;, "Ljava/util/PriorityQueue<Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;>;"
    new-instance v0, Lorg/apache/lucene/search/TopTermsRewrite$2;

    invoke-direct {v0, p0, v9, v6}, Lorg/apache/lucene/search/TopTermsRewrite$2;-><init>(Lorg/apache/lucene/search/TopTermsRewrite;Ljava/util/PriorityQueue;I)V

    invoke-virtual {p0, p1, p2, v0}, Lorg/apache/lucene/search/TopTermsRewrite;->collectTerms(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/MultiTermQuery;Lorg/apache/lucene/search/TermCollectingRewrite$TermCollector;)V

    .line 157
    invoke-virtual {p0}, Lorg/apache/lucene/search/TopTermsRewrite;->getTopLevelQuery()Lorg/apache/lucene/search/Query;

    move-result-object v1

    .line 158
    .local v1, "q":Lorg/apache/lucene/search/Query;, "TQ;"
    invoke-virtual {v9}, Ljava/util/PriorityQueue;->size()I

    move-result v0

    new-array v0, v0, [Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;

    invoke-virtual {v9, v0}, Ljava/util/PriorityQueue;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;

    .line 159
    .local v7, "scoreTerms":[Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;
    sget-object v0, Lorg/apache/lucene/search/TopTermsRewrite;->scoreTermSortByTermComp:Ljava/util/Comparator;

    invoke-static {v7, v0}, Lorg/apache/lucene/util/ArrayUtil;->mergeSort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 161
    array-length v11, v7

    const/4 v0, 0x0

    move v10, v0

    :goto_0
    if-lt v10, v11, :cond_0

    .line 166
    return-object v1

    .line 161
    :cond_0
    aget-object v8, v7, v10

    .line 162
    .local v8, "st":Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;, "Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;"
    new-instance v2, Lorg/apache/lucene/index/Term;

    iget-object v0, p2, Lorg/apache/lucene/search/MultiTermQuery;->field:Ljava/lang/String;

    iget-object v3, v8, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;->bytes:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v2, v0, v3}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 163
    .local v2, "term":Lorg/apache/lucene/index/Term;
    sget-boolean v0, Lorg/apache/lucene/search/TopTermsRewrite;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    invoke-virtual {p1, v2}, Lorg/apache/lucene/index/IndexReader;->docFreq(Lorg/apache/lucene/index/Term;)I

    move-result v0

    iget-object v3, v8, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;->termState:Lorg/apache/lucene/index/TermContext;

    invoke-virtual {v3}, Lorg/apache/lucene/index/TermContext;->docFreq()I

    move-result v3

    if-eq v0, v3, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "reader DF is "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v2}, Lorg/apache/lucene/index/IndexReader;->docFreq(Lorg/apache/lucene/index/Term;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " vs "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v8, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;->termState:Lorg/apache/lucene/index/TermContext;

    invoke-virtual {v4}, Lorg/apache/lucene/index/TermContext;->docFreq()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " term="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 164
    :cond_1
    iget-object v0, v8, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;->termState:Lorg/apache/lucene/index/TermContext;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermContext;->docFreq()I

    move-result v3

    invoke-virtual {p2}, Lorg/apache/lucene/search/MultiTermQuery;->getBoost()F

    move-result v0

    iget v4, v8, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;->boost:F

    mul-float/2addr v4, v0

    iget-object v5, v8, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;->termState:Lorg/apache/lucene/index/TermContext;

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lorg/apache/lucene/search/TopTermsRewrite;->addClause(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/index/Term;IFLorg/apache/lucene/index/TermContext;)V

    .line 161
    add-int/lit8 v0, v10, 0x1

    move v10, v0

    goto :goto_0
.end method
