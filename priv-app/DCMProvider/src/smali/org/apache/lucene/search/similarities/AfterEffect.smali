.class public abstract Lorg/apache/lucene/search/similarities/AfterEffect;
.super Ljava/lang/Object;
.source "AfterEffect.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/similarities/AfterEffect$NoAfterEffect;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract explain(Lorg/apache/lucene/search/similarities/BasicStats;F)Lorg/apache/lucene/search/Explanation;
.end method

.method public abstract score(Lorg/apache/lucene/search/similarities/BasicStats;F)F
.end method

.method public abstract toString()Ljava/lang/String;
.end method
