.class Lorg/apache/lucene/search/MultiTermQuery$1;
.super Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;
.source "MultiTermQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/MultiTermQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0}, Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public rewrite(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/MultiTermQuery;)Lorg/apache/lucene/search/Query;
    .locals 2
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "query"    # Lorg/apache/lucene/search/MultiTermQuery;

    .prologue
    .line 95
    new-instance v0, Lorg/apache/lucene/search/ConstantScoreQuery;

    new-instance v1, Lorg/apache/lucene/search/MultiTermQueryWrapperFilter;

    invoke-direct {v1, p2}, Lorg/apache/lucene/search/MultiTermQueryWrapperFilter;-><init>(Lorg/apache/lucene/search/MultiTermQuery;)V

    invoke-direct {v0, v1}, Lorg/apache/lucene/search/ConstantScoreQuery;-><init>(Lorg/apache/lucene/search/Filter;)V

    .line 96
    .local v0, "result":Lorg/apache/lucene/search/Query;
    invoke-virtual {p2}, Lorg/apache/lucene/search/MultiTermQuery;->getBoost()F

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/Query;->setBoost(F)V

    .line 97
    return-object v0
.end method
