.class public abstract Lorg/apache/lucene/search/FieldValueHitQueue;
.super Lorg/apache/lucene/util/PriorityQueue;
.source "FieldValueHitQueue.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/FieldValueHitQueue$Entry;,
        Lorg/apache/lucene/search/FieldValueHitQueue$MultiComparatorsFieldValueHitQueue;,
        Lorg/apache/lucene/search/FieldValueHitQueue$OneComparatorFieldValueHitQueue;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lorg/apache/lucene/search/FieldValueHitQueue$Entry;",
        ">",
        "Lorg/apache/lucene/util/PriorityQueue",
        "<TT;>;"
    }
.end annotation


# instance fields
.field protected final comparators:[Lorg/apache/lucene/search/FieldComparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lorg/apache/lucene/search/FieldComparator",
            "<*>;"
        }
    .end annotation
.end field

.field protected final fields:[Lorg/apache/lucene/search/SortField;

.field protected firstComparator:Lorg/apache/lucene/search/FieldComparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/search/FieldComparator",
            "<*>;"
        }
    .end annotation
.end field

.field protected final reverseMul:[I


# direct methods
.method private constructor <init>([Lorg/apache/lucene/search/SortField;I)V
    .locals 2
    .param p1, "fields"    # [Lorg/apache/lucene/search/SortField;
    .param p2, "size"    # I

    .prologue
    .line 138
    .local p0, "this":Lorg/apache/lucene/search/FieldValueHitQueue;, "Lorg/apache/lucene/search/FieldValueHitQueue<TT;>;"
    invoke-direct {p0, p2}, Lorg/apache/lucene/util/PriorityQueue;-><init>(I)V

    .line 145
    iput-object p1, p0, Lorg/apache/lucene/search/FieldValueHitQueue;->fields:[Lorg/apache/lucene/search/SortField;

    .line 146
    array-length v0, p1

    .line 147
    .local v0, "numComparators":I
    new-array v1, v0, [Lorg/apache/lucene/search/FieldComparator;

    iput-object v1, p0, Lorg/apache/lucene/search/FieldValueHitQueue;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    .line 148
    new-array v1, v0, [I

    iput-object v1, p0, Lorg/apache/lucene/search/FieldValueHitQueue;->reverseMul:[I

    .line 149
    return-void
.end method

.method synthetic constructor <init>([Lorg/apache/lucene/search/SortField;ILorg/apache/lucene/search/FieldValueHitQueue;)V
    .locals 0

    .prologue
    .line 137
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/search/FieldValueHitQueue;-><init>([Lorg/apache/lucene/search/SortField;I)V

    return-void
.end method

.method public static create([Lorg/apache/lucene/search/SortField;I)Lorg/apache/lucene/search/FieldValueHitQueue;
    .locals 2
    .param p0, "fields"    # [Lorg/apache/lucene/search/SortField;
    .param p1, "size"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lorg/apache/lucene/search/FieldValueHitQueue$Entry;",
            ">([",
            "Lorg/apache/lucene/search/SortField;",
            "I)",
            "Lorg/apache/lucene/search/FieldValueHitQueue",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 166
    array-length v0, p0

    if-nez v0, :cond_0

    .line 167
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Sort must contain at least one field"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 170
    :cond_0
    array-length v0, p0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 171
    new-instance v0, Lorg/apache/lucene/search/FieldValueHitQueue$OneComparatorFieldValueHitQueue;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/search/FieldValueHitQueue$OneComparatorFieldValueHitQueue;-><init>([Lorg/apache/lucene/search/SortField;I)V

    .line 173
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lorg/apache/lucene/search/FieldValueHitQueue$MultiComparatorsFieldValueHitQueue;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/search/FieldValueHitQueue$MultiComparatorsFieldValueHitQueue;-><init>([Lorg/apache/lucene/search/SortField;I)V

    goto :goto_0
.end method


# virtual methods
.method fillFields(Lorg/apache/lucene/search/FieldValueHitQueue$Entry;)Lorg/apache/lucene/search/FieldDoc;
    .locals 6

    .prologue
    .line 211
    .local p0, "this":Lorg/apache/lucene/search/FieldValueHitQueue;, "Lorg/apache/lucene/search/FieldValueHitQueue<TT;>;"
    .local p1, "entry":Lorg/apache/lucene/search/FieldValueHitQueue$Entry;, "Lorg/apache/lucene/search/FieldValueHitQueue$Entry;"
    iget-object v3, p0, Lorg/apache/lucene/search/FieldValueHitQueue;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    array-length v2, v3

    .line 212
    .local v2, "n":I
    new-array v0, v2, [Ljava/lang/Object;

    .line 213
    .local v0, "fields":[Ljava/lang/Object;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v2, :cond_0

    .line 217
    new-instance v3, Lorg/apache/lucene/search/FieldDoc;

    iget v4, p1, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->doc:I

    iget v5, p1, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->score:F

    invoke-direct {v3, v4, v5, v0}, Lorg/apache/lucene/search/FieldDoc;-><init>(IF[Ljava/lang/Object;)V

    return-object v3

    .line 214
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/search/FieldValueHitQueue;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    aget-object v3, v3, v1

    iget v4, p1, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->slot:I

    invoke-virtual {v3, v4}, Lorg/apache/lucene/search/FieldComparator;->value(I)Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v0, v1

    .line 213
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getComparators()[Lorg/apache/lucene/search/FieldComparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()[",
            "Lorg/apache/lucene/search/FieldComparator",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 178
    .local p0, "this":Lorg/apache/lucene/search/FieldValueHitQueue;, "Lorg/apache/lucene/search/FieldValueHitQueue<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/FieldValueHitQueue;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    return-object v0
.end method

.method getFields()[Lorg/apache/lucene/search/SortField;
    .locals 1

    .prologue
    .line 222
    .local p0, "this":Lorg/apache/lucene/search/FieldValueHitQueue;, "Lorg/apache/lucene/search/FieldValueHitQueue<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/FieldValueHitQueue;->fields:[Lorg/apache/lucene/search/SortField;

    return-object v0
.end method

.method public getReverseMul()[I
    .locals 1

    .prologue
    .line 182
    .local p0, "this":Lorg/apache/lucene/search/FieldValueHitQueue;, "Lorg/apache/lucene/search/FieldValueHitQueue<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/FieldValueHitQueue;->reverseMul:[I

    return-object v0
.end method

.method protected bridge synthetic lessThan(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    check-cast p2, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/search/FieldValueHitQueue;->lessThan(Lorg/apache/lucene/search/FieldValueHitQueue$Entry;Lorg/apache/lucene/search/FieldValueHitQueue$Entry;)Z

    move-result v0

    return v0
.end method

.method protected abstract lessThan(Lorg/apache/lucene/search/FieldValueHitQueue$Entry;Lorg/apache/lucene/search/FieldValueHitQueue$Entry;)Z
.end method

.method public setComparator(ILorg/apache/lucene/search/FieldComparator;)V
    .locals 1
    .param p1, "pos"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lorg/apache/lucene/search/FieldComparator",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 186
    .local p0, "this":Lorg/apache/lucene/search/FieldValueHitQueue;, "Lorg/apache/lucene/search/FieldValueHitQueue<TT;>;"
    .local p2, "comparator":Lorg/apache/lucene/search/FieldComparator;, "Lorg/apache/lucene/search/FieldComparator<*>;"
    if-nez p1, :cond_0

    iput-object p2, p0, Lorg/apache/lucene/search/FieldValueHitQueue;->firstComparator:Lorg/apache/lucene/search/FieldComparator;

    .line 187
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/FieldValueHitQueue;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    aput-object p2, v0, p1

    .line 188
    return-void
.end method
