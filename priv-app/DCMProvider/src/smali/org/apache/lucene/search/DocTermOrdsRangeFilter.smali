.class public abstract Lorg/apache/lucene/search/DocTermOrdsRangeFilter;
.super Lorg/apache/lucene/search/Filter;
.source "DocTermOrdsRangeFilter.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final field:Ljava/lang/String;

.field final includeLower:Z

.field final includeUpper:Z

.field final lowerVal:Lorg/apache/lucene/util/BytesRef;

.field final upperVal:Lorg/apache/lucene/util/BytesRef;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lorg/apache/lucene/search/DocTermOrdsRangeFilter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/DocTermOrdsRangeFilter;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;ZZ)V
    .locals 0
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "lowerVal"    # Lorg/apache/lucene/util/BytesRef;
    .param p3, "upperVal"    # Lorg/apache/lucene/util/BytesRef;
    .param p4, "includeLower"    # Z
    .param p5, "includeUpper"    # Z

    .prologue
    .line 41
    invoke-direct {p0}, Lorg/apache/lucene/search/Filter;-><init>()V

    .line 42
    iput-object p1, p0, Lorg/apache/lucene/search/DocTermOrdsRangeFilter;->field:Ljava/lang/String;

    .line 43
    iput-object p2, p0, Lorg/apache/lucene/search/DocTermOrdsRangeFilter;->lowerVal:Lorg/apache/lucene/util/BytesRef;

    .line 44
    iput-object p3, p0, Lorg/apache/lucene/search/DocTermOrdsRangeFilter;->upperVal:Lorg/apache/lucene/util/BytesRef;

    .line 45
    iput-boolean p4, p0, Lorg/apache/lucene/search/DocTermOrdsRangeFilter;->includeLower:Z

    .line 46
    iput-boolean p5, p0, Lorg/apache/lucene/search/DocTermOrdsRangeFilter;->includeUpper:Z

    .line 47
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;ZZLorg/apache/lucene/search/DocTermOrdsRangeFilter;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct/range {p0 .. p5}, Lorg/apache/lucene/search/DocTermOrdsRangeFilter;-><init>(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;ZZ)V

    return-void
.end method

.method public static newBytesRefRange(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;ZZ)Lorg/apache/lucene/search/DocTermOrdsRangeFilter;
    .locals 6
    .param p0, "field"    # Ljava/lang/String;
    .param p1, "lowerVal"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "upperVal"    # Lorg/apache/lucene/util/BytesRef;
    .param p3, "includeLower"    # Z
    .param p4, "includeUpper"    # Z

    .prologue
    .line 59
    new-instance v0, Lorg/apache/lucene/search/DocTermOrdsRangeFilter$1;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/search/DocTermOrdsRangeFilter$1;-><init>(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;ZZ)V

    return-object v0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 130
    if-ne p0, p1, :cond_1

    .line 140
    :cond_0
    :goto_0
    return v1

    .line 131
    :cond_1
    instance-of v3, p1, Lorg/apache/lucene/search/DocTermOrdsRangeFilter;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 132
    check-cast v0, Lorg/apache/lucene/search/DocTermOrdsRangeFilter;

    .line 134
    .local v0, "other":Lorg/apache/lucene/search/DocTermOrdsRangeFilter;
    iget-object v3, p0, Lorg/apache/lucene/search/DocTermOrdsRangeFilter;->field:Ljava/lang/String;

    iget-object v4, v0, Lorg/apache/lucene/search/DocTermOrdsRangeFilter;->field:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 135
    iget-boolean v3, p0, Lorg/apache/lucene/search/DocTermOrdsRangeFilter;->includeLower:Z

    iget-boolean v4, v0, Lorg/apache/lucene/search/DocTermOrdsRangeFilter;->includeLower:Z

    if-ne v3, v4, :cond_3

    .line 136
    iget-boolean v3, p0, Lorg/apache/lucene/search/DocTermOrdsRangeFilter;->includeUpper:Z

    iget-boolean v4, v0, Lorg/apache/lucene/search/DocTermOrdsRangeFilter;->includeUpper:Z

    if-eq v3, v4, :cond_4

    :cond_3
    move v1, v2

    .line 137
    goto :goto_0

    .line 138
    :cond_4
    iget-object v3, p0, Lorg/apache/lucene/search/DocTermOrdsRangeFilter;->lowerVal:Lorg/apache/lucene/util/BytesRef;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lorg/apache/lucene/search/DocTermOrdsRangeFilter;->lowerVal:Lorg/apache/lucene/util/BytesRef;

    iget-object v4, v0, Lorg/apache/lucene/search/DocTermOrdsRangeFilter;->lowerVal:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/util/BytesRef;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    :cond_5
    move v1, v2

    goto :goto_0

    :cond_6
    iget-object v3, v0, Lorg/apache/lucene/search/DocTermOrdsRangeFilter;->lowerVal:Lorg/apache/lucene/util/BytesRef;

    if-nez v3, :cond_5

    .line 139
    :cond_7
    iget-object v3, p0, Lorg/apache/lucene/search/DocTermOrdsRangeFilter;->upperVal:Lorg/apache/lucene/util/BytesRef;

    if-eqz v3, :cond_8

    iget-object v3, p0, Lorg/apache/lucene/search/DocTermOrdsRangeFilter;->upperVal:Lorg/apache/lucene/util/BytesRef;

    iget-object v4, v0, Lorg/apache/lucene/search/DocTermOrdsRangeFilter;->upperVal:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/util/BytesRef;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :goto_1
    move v1, v2

    goto :goto_0

    :cond_8
    iget-object v3, v0, Lorg/apache/lucene/search/DocTermOrdsRangeFilter;->upperVal:Lorg/apache/lucene/util/BytesRef;

    if-eqz v3, :cond_0

    goto :goto_1
.end method

.method public abstract getDocIdSet(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/DocIdSet;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public getField()Ljava/lang/String;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lorg/apache/lucene/search/DocTermOrdsRangeFilter;->field:Ljava/lang/String;

    return-object v0
.end method

.method public getLowerVal()Lorg/apache/lucene/util/BytesRef;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lorg/apache/lucene/search/DocTermOrdsRangeFilter;->lowerVal:Lorg/apache/lucene/util/BytesRef;

    return-object v0
.end method

.method public getUpperVal()Lorg/apache/lucene/util/BytesRef;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lorg/apache/lucene/search/DocTermOrdsRangeFilter;->upperVal:Lorg/apache/lucene/util/BytesRef;

    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 145
    iget-object v1, p0, Lorg/apache/lucene/search/DocTermOrdsRangeFilter;->field:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 146
    .local v0, "h":I
    iget-object v1, p0, Lorg/apache/lucene/search/DocTermOrdsRangeFilter;->lowerVal:Lorg/apache/lucene/util/BytesRef;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/search/DocTermOrdsRangeFilter;->lowerVal:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v1}, Lorg/apache/lucene/util/BytesRef;->hashCode()I

    move-result v1

    :goto_0
    xor-int/2addr v0, v1

    .line 147
    shl-int/lit8 v1, v0, 0x1

    ushr-int/lit8 v2, v0, 0x1f

    or-int v0, v1, v2

    .line 148
    iget-object v1, p0, Lorg/apache/lucene/search/DocTermOrdsRangeFilter;->upperVal:Lorg/apache/lucene/util/BytesRef;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/apache/lucene/search/DocTermOrdsRangeFilter;->upperVal:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v1}, Lorg/apache/lucene/util/BytesRef;->hashCode()I

    move-result v1

    :goto_1
    xor-int/2addr v0, v1

    .line 149
    iget-boolean v1, p0, Lorg/apache/lucene/search/DocTermOrdsRangeFilter;->includeLower:Z

    if-eqz v1, :cond_2

    const v1, 0x5c586ea0

    :goto_2
    iget-boolean v2, p0, Lorg/apache/lucene/search/DocTermOrdsRangeFilter;->includeUpper:Z

    if-eqz v2, :cond_3

    const v2, 0x6695b902

    :goto_3
    xor-int/2addr v1, v2

    xor-int/2addr v0, v1

    .line 150
    return v0

    .line 146
    :cond_0
    const v1, 0x20cdc4ec

    goto :goto_0

    .line 148
    :cond_1
    const v1, -0x63cd9023

    goto :goto_1

    .line 149
    :cond_2
    const v1, -0x15c209ca

    goto :goto_2

    :cond_3
    const v2, 0x742608b5

    goto :goto_3
.end method

.method public includesLower()Z
    .locals 1

    .prologue
    .line 157
    iget-boolean v0, p0, Lorg/apache/lucene/search/DocTermOrdsRangeFilter;->includeLower:Z

    return v0
.end method

.method public includesUpper()Z
    .locals 1

    .prologue
    .line 160
    iget-boolean v0, p0, Lorg/apache/lucene/search/DocTermOrdsRangeFilter;->includeUpper:Z

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 119
    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lorg/apache/lucene/search/DocTermOrdsRangeFilter;->field:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 120
    .local v0, "sb":Ljava/lang/StringBuilder;
    iget-boolean v1, p0, Lorg/apache/lucene/search/DocTermOrdsRangeFilter;->includeLower:Z

    if-eqz v1, :cond_0

    const/16 v1, 0x5b

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 121
    iget-object v1, p0, Lorg/apache/lucene/search/DocTermOrdsRangeFilter;->lowerVal:Lorg/apache/lucene/util/BytesRef;

    if-nez v1, :cond_1

    const-string v1, "*"

    :goto_1
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 122
    const-string v2, " TO "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 123
    iget-object v1, p0, Lorg/apache/lucene/search/DocTermOrdsRangeFilter;->upperVal:Lorg/apache/lucene/util/BytesRef;

    if-nez v1, :cond_2

    const-string v1, "*"

    :goto_2
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 124
    iget-boolean v1, p0, Lorg/apache/lucene/search/DocTermOrdsRangeFilter;->includeUpper:Z

    if-eqz v1, :cond_3

    const/16 v1, 0x5d

    :goto_3
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 125
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 120
    return-object v1

    :cond_0
    const/16 v1, 0x7b

    goto :goto_0

    .line 121
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/search/DocTermOrdsRangeFilter;->lowerVal:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v1}, Lorg/apache/lucene/util/BytesRef;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 123
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/search/DocTermOrdsRangeFilter;->upperVal:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v1}, Lorg/apache/lucene/util/BytesRef;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 124
    :cond_3
    const/16 v1, 0x7d

    goto :goto_3
.end method
