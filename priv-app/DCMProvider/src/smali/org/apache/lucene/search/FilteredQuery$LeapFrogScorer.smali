.class Lorg/apache/lucene/search/FilteredQuery$LeapFrogScorer;
.super Lorg/apache/lucene/search/Scorer;
.source "FilteredQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FilteredQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "LeapFrogScorer"
.end annotation


# instance fields
.field private final primary:Lorg/apache/lucene/search/DocIdSetIterator;

.field protected primaryDoc:I

.field private final scorer:Lorg/apache/lucene/search/Scorer;

.field private final secondary:Lorg/apache/lucene/search/DocIdSetIterator;

.field protected secondaryDoc:I


# direct methods
.method protected constructor <init>(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/DocIdSetIterator;Lorg/apache/lucene/search/DocIdSetIterator;Lorg/apache/lucene/search/Scorer;)V
    .locals 1
    .param p1, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p2, "primary"    # Lorg/apache/lucene/search/DocIdSetIterator;
    .param p3, "secondary"    # Lorg/apache/lucene/search/DocIdSetIterator;
    .param p4, "scorer"    # Lorg/apache/lucene/search/Scorer;

    .prologue
    const/4 v0, -0x1

    .line 234
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/Scorer;-><init>(Lorg/apache/lucene/search/Weight;)V

    .line 230
    iput v0, p0, Lorg/apache/lucene/search/FilteredQuery$LeapFrogScorer;->primaryDoc:I

    .line 231
    iput v0, p0, Lorg/apache/lucene/search/FilteredQuery$LeapFrogScorer;->secondaryDoc:I

    .line 235
    iput-object p2, p0, Lorg/apache/lucene/search/FilteredQuery$LeapFrogScorer;->primary:Lorg/apache/lucene/search/DocIdSetIterator;

    .line 236
    iput-object p3, p0, Lorg/apache/lucene/search/FilteredQuery$LeapFrogScorer;->secondary:Lorg/apache/lucene/search/DocIdSetIterator;

    .line 237
    iput-object p4, p0, Lorg/apache/lucene/search/FilteredQuery$LeapFrogScorer;->scorer:Lorg/apache/lucene/search/Scorer;

    .line 238
    return-void
.end method

.method private final advanceToNextCommonDoc()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 267
    :goto_0
    iget v0, p0, Lorg/apache/lucene/search/FilteredQuery$LeapFrogScorer;->secondaryDoc:I

    iget v1, p0, Lorg/apache/lucene/search/FilteredQuery$LeapFrogScorer;->primaryDoc:I

    if-ge v0, v1, :cond_0

    .line 268
    iget-object v0, p0, Lorg/apache/lucene/search/FilteredQuery$LeapFrogScorer;->secondary:Lorg/apache/lucene/search/DocIdSetIterator;

    iget v1, p0, Lorg/apache/lucene/search/FilteredQuery$LeapFrogScorer;->primaryDoc:I

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/DocIdSetIterator;->advance(I)I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/FilteredQuery$LeapFrogScorer;->secondaryDoc:I

    goto :goto_0

    .line 269
    :cond_0
    iget v0, p0, Lorg/apache/lucene/search/FilteredQuery$LeapFrogScorer;->secondaryDoc:I

    iget v1, p0, Lorg/apache/lucene/search/FilteredQuery$LeapFrogScorer;->primaryDoc:I

    if-ne v0, v1, :cond_1

    .line 270
    iget v0, p0, Lorg/apache/lucene/search/FilteredQuery$LeapFrogScorer;->primaryDoc:I

    return v0

    .line 272
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/search/FilteredQuery$LeapFrogScorer;->primary:Lorg/apache/lucene/search/DocIdSetIterator;

    iget v1, p0, Lorg/apache/lucene/search/FilteredQuery$LeapFrogScorer;->secondaryDoc:I

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/DocIdSetIterator;->advance(I)I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/FilteredQuery$LeapFrogScorer;->primaryDoc:I

    goto :goto_0
.end method


# virtual methods
.method public final advance(I)I
    .locals 1
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 289
    iget v0, p0, Lorg/apache/lucene/search/FilteredQuery$LeapFrogScorer;->primaryDoc:I

    if-le p1, v0, :cond_0

    .line 290
    iget-object v0, p0, Lorg/apache/lucene/search/FilteredQuery$LeapFrogScorer;->primary:Lorg/apache/lucene/search/DocIdSetIterator;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/DocIdSetIterator;->advance(I)I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/FilteredQuery$LeapFrogScorer;->primaryDoc:I

    .line 292
    :cond_0
    invoke-direct {p0}, Lorg/apache/lucene/search/FilteredQuery$LeapFrogScorer;->advanceToNextCommonDoc()I

    move-result v0

    return v0
.end method

.method public cost()J
    .locals 4

    .prologue
    .line 315
    iget-object v0, p0, Lorg/apache/lucene/search/FilteredQuery$LeapFrogScorer;->primary:Lorg/apache/lucene/search/DocIdSetIterator;

    invoke-virtual {v0}, Lorg/apache/lucene/search/DocIdSetIterator;->cost()J

    move-result-wide v0

    iget-object v2, p0, Lorg/apache/lucene/search/FilteredQuery$LeapFrogScorer;->secondary:Lorg/apache/lucene/search/DocIdSetIterator;

    invoke-virtual {v2}, Lorg/apache/lucene/search/DocIdSetIterator;->cost()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public final docID()I
    .locals 1

    .prologue
    .line 297
    iget v0, p0, Lorg/apache/lucene/search/FilteredQuery$LeapFrogScorer;->secondaryDoc:I

    return v0
.end method

.method public final freq()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 306
    iget-object v0, p0, Lorg/apache/lucene/search/FilteredQuery$LeapFrogScorer;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->freq()I

    move-result v0

    return v0
.end method

.method public final getChildren()Ljava/util/Collection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/search/Scorer$ChildScorer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 310
    new-instance v0, Lorg/apache/lucene/search/Scorer$ChildScorer;

    iget-object v1, p0, Lorg/apache/lucene/search/FilteredQuery$LeapFrogScorer;->scorer:Lorg/apache/lucene/search/Scorer;

    const-string v2, "FILTERED"

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/search/Scorer$ChildScorer;-><init>(Lorg/apache/lucene/search/Scorer;Ljava/lang/String;)V

    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final nextDoc()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 279
    invoke-virtual {p0}, Lorg/apache/lucene/search/FilteredQuery$LeapFrogScorer;->primaryNext()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/FilteredQuery$LeapFrogScorer;->primaryDoc:I

    .line 280
    invoke-direct {p0}, Lorg/apache/lucene/search/FilteredQuery$LeapFrogScorer;->advanceToNextCommonDoc()I

    move-result v0

    return v0
.end method

.method protected primaryNext()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 284
    iget-object v0, p0, Lorg/apache/lucene/search/FilteredQuery$LeapFrogScorer;->primary:Lorg/apache/lucene/search/DocIdSetIterator;

    invoke-virtual {v0}, Lorg/apache/lucene/search/DocIdSetIterator;->nextDoc()I

    move-result v0

    return v0
.end method

.method public final score()F
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 302
    iget-object v0, p0, Lorg/apache/lucene/search/FilteredQuery$LeapFrogScorer;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v0

    return v0
.end method

.method public final score(Lorg/apache/lucene/search/Collector;)V
    .locals 3
    .param p1, "collector"    # Lorg/apache/lucene/search/Collector;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 245
    iget-object v2, p0, Lorg/apache/lucene/search/FilteredQuery$LeapFrogScorer;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {p1, v2}, Lorg/apache/lucene/search/Collector;->setScorer(Lorg/apache/lucene/search/Scorer;)V

    .line 246
    invoke-virtual {p0}, Lorg/apache/lucene/search/FilteredQuery$LeapFrogScorer;->primaryNext()I

    move-result v0

    .line 247
    .local v0, "primDoc":I
    iget-object v2, p0, Lorg/apache/lucene/search/FilteredQuery$LeapFrogScorer;->secondary:Lorg/apache/lucene/search/DocIdSetIterator;

    invoke-virtual {v2, v0}, Lorg/apache/lucene/search/DocIdSetIterator;->advance(I)I

    move-result v1

    .line 249
    .local v1, "secDoc":I
    :goto_0
    if-ne v0, v1, :cond_1

    .line 251
    const v2, 0x7fffffff

    if-ne v0, v2, :cond_0

    .line 263
    return-void

    .line 254
    :cond_0
    invoke-virtual {p1, v0}, Lorg/apache/lucene/search/Collector;->collect(I)V

    .line 255
    iget-object v2, p0, Lorg/apache/lucene/search/FilteredQuery$LeapFrogScorer;->primary:Lorg/apache/lucene/search/DocIdSetIterator;

    invoke-virtual {v2}, Lorg/apache/lucene/search/DocIdSetIterator;->nextDoc()I

    move-result v0

    .line 256
    iget-object v2, p0, Lorg/apache/lucene/search/FilteredQuery$LeapFrogScorer;->secondary:Lorg/apache/lucene/search/DocIdSetIterator;

    invoke-virtual {v2, v0}, Lorg/apache/lucene/search/DocIdSetIterator;->advance(I)I

    move-result v1

    .line 257
    goto :goto_0

    :cond_1
    if-le v1, v0, :cond_2

    .line 258
    iget-object v2, p0, Lorg/apache/lucene/search/FilteredQuery$LeapFrogScorer;->primary:Lorg/apache/lucene/search/DocIdSetIterator;

    invoke-virtual {v2, v1}, Lorg/apache/lucene/search/DocIdSetIterator;->advance(I)I

    move-result v0

    .line 259
    goto :goto_0

    .line 260
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/search/FilteredQuery$LeapFrogScorer;->secondary:Lorg/apache/lucene/search/DocIdSetIterator;

    invoke-virtual {v2, v0}, Lorg/apache/lucene/search/DocIdSetIterator;->advance(I)I

    move-result v1

    .line 248
    goto :goto_0
.end method
