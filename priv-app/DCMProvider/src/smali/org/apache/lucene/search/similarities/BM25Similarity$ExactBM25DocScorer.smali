.class Lorg/apache/lucene/search/similarities/BM25Similarity$ExactBM25DocScorer;
.super Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;
.source "BM25Similarity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/similarities/BM25Similarity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ExactBM25DocScorer"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final cache:[F

.field private final norms:Lorg/apache/lucene/index/NumericDocValues;

.field private final stats:Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;

.field final synthetic this$0:Lorg/apache/lucene/search/similarities/BM25Similarity;

.field private final weightValue:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 229
    const-class v0, Lorg/apache/lucene/search/similarities/BM25Similarity;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/similarities/BM25Similarity$ExactBM25DocScorer;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/search/similarities/BM25Similarity;Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;Lorg/apache/lucene/index/NumericDocValues;)V
    .locals 3
    .param p2, "stats"    # Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;
    .param p3, "norms"    # Lorg/apache/lucene/index/NumericDocValues;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 235
    iput-object p1, p0, Lorg/apache/lucene/search/similarities/BM25Similarity$ExactBM25DocScorer;->this$0:Lorg/apache/lucene/search/similarities/BM25Similarity;

    invoke-direct {p0}, Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;-><init>()V

    .line 236
    sget-boolean v0, Lorg/apache/lucene/search/similarities/BM25Similarity$ExactBM25DocScorer;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p3, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 237
    :cond_0
    iput-object p2, p0, Lorg/apache/lucene/search/similarities/BM25Similarity$ExactBM25DocScorer;->stats:Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;

    .line 238
    # getter for: Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;->weight:F
    invoke-static {p2}, Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;->access$0(Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;)F

    move-result v0

    # getter for: Lorg/apache/lucene/search/similarities/BM25Similarity;->k1:F
    invoke-static {p1}, Lorg/apache/lucene/search/similarities/BM25Similarity;->access$0(Lorg/apache/lucene/search/similarities/BM25Similarity;)F

    move-result v1

    const/high16 v2, 0x3f800000    # 1.0f

    add-float/2addr v1, v2

    mul-float/2addr v0, v1

    iput v0, p0, Lorg/apache/lucene/search/similarities/BM25Similarity$ExactBM25DocScorer;->weightValue:F

    .line 239
    # getter for: Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;->cache:[F
    invoke-static {p2}, Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;->access$1(Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;)[F

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/similarities/BM25Similarity$ExactBM25DocScorer;->cache:[F

    .line 240
    iput-object p3, p0, Lorg/apache/lucene/search/similarities/BM25Similarity$ExactBM25DocScorer;->norms:Lorg/apache/lucene/index/NumericDocValues;

    .line 241
    return-void
.end method


# virtual methods
.method public explain(ILorg/apache/lucene/search/Explanation;)Lorg/apache/lucene/search/Explanation;
    .locals 3
    .param p1, "doc"    # I
    .param p2, "freq"    # Lorg/apache/lucene/search/Explanation;

    .prologue
    .line 250
    iget-object v0, p0, Lorg/apache/lucene/search/similarities/BM25Similarity$ExactBM25DocScorer;->this$0:Lorg/apache/lucene/search/similarities/BM25Similarity;

    iget-object v1, p0, Lorg/apache/lucene/search/similarities/BM25Similarity$ExactBM25DocScorer;->stats:Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;

    iget-object v2, p0, Lorg/apache/lucene/search/similarities/BM25Similarity$ExactBM25DocScorer;->norms:Lorg/apache/lucene/index/NumericDocValues;

    # invokes: Lorg/apache/lucene/search/similarities/BM25Similarity;->explainScore(ILorg/apache/lucene/search/Explanation;Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;Lorg/apache/lucene/index/NumericDocValues;)Lorg/apache/lucene/search/Explanation;
    invoke-static {v0, p1, p2, v1, v2}, Lorg/apache/lucene/search/similarities/BM25Similarity;->access$1(Lorg/apache/lucene/search/similarities/BM25Similarity;ILorg/apache/lucene/search/Explanation;Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;Lorg/apache/lucene/index/NumericDocValues;)Lorg/apache/lucene/search/Explanation;

    move-result-object v0

    return-object v0
.end method

.method public score(II)F
    .locals 6
    .param p1, "doc"    # I
    .param p2, "freq"    # I

    .prologue
    .line 245
    iget v0, p0, Lorg/apache/lucene/search/similarities/BM25Similarity$ExactBM25DocScorer;->weightValue:F

    int-to-float v1, p2

    mul-float/2addr v0, v1

    int-to-float v1, p2

    iget-object v2, p0, Lorg/apache/lucene/search/similarities/BM25Similarity$ExactBM25DocScorer;->cache:[F

    iget-object v3, p0, Lorg/apache/lucene/search/similarities/BM25Similarity$ExactBM25DocScorer;->norms:Lorg/apache/lucene/index/NumericDocValues;

    invoke-virtual {v3, p1}, Lorg/apache/lucene/index/NumericDocValues;->get(I)J

    move-result-wide v4

    long-to-int v3, v4

    int-to-byte v3, v3

    and-int/lit16 v3, v3, 0xff

    aget v2, v2, v3

    add-float/2addr v1, v2

    div-float/2addr v0, v1

    return v0
.end method
