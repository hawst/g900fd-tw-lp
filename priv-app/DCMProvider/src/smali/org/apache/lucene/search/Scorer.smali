.class public abstract Lorg/apache/lucene/search/Scorer;
.super Lorg/apache/lucene/index/DocsEnum;
.source "Scorer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/Scorer$ChildScorer;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field protected final weight:Lorg/apache/lucene/search/Weight;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-class v0, Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/Scorer;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected constructor <init>(Lorg/apache/lucene/search/Weight;)V
    .locals 0
    .param p1, "weight"    # Lorg/apache/lucene/search/Weight;

    .prologue
    .line 53
    invoke-direct {p0}, Lorg/apache/lucene/index/DocsEnum;-><init>()V

    .line 54
    iput-object p1, p0, Lorg/apache/lucene/search/Scorer;->weight:Lorg/apache/lucene/search/Weight;

    .line 55
    return-void
.end method


# virtual methods
.method public getChildren()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/search/Scorer$ChildScorer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 110
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getWeight()Lorg/apache/lucene/search/Weight;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lorg/apache/lucene/search/Scorer;->weight:Lorg/apache/lucene/search/Weight;

    return-object v0
.end method

.method public abstract score()F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public score(Lorg/apache/lucene/search/Collector;)V
    .locals 3
    .param p1, "collector"    # Lorg/apache/lucene/search/Collector;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 61
    sget-boolean v1, Lorg/apache/lucene/search/Scorer;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 62
    :cond_0
    invoke-virtual {p1, p0}, Lorg/apache/lucene/search/Collector;->setScorer(Lorg/apache/lucene/search/Scorer;)V

    .line 64
    :goto_0
    invoke-virtual {p0}, Lorg/apache/lucene/search/Scorer;->nextDoc()I

    move-result v0

    .local v0, "doc":I
    const v1, 0x7fffffff

    if-ne v0, v1, :cond_1

    .line 67
    return-void

    .line 65
    :cond_1
    invoke-virtual {p1, v0}, Lorg/apache/lucene/search/Collector;->collect(I)V

    goto :goto_0
.end method

.method public score(Lorg/apache/lucene/search/Collector;II)Z
    .locals 2
    .param p1, "collector"    # Lorg/apache/lucene/search/Collector;
    .param p2, "max"    # I
    .param p3, "firstDocID"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 84
    sget-boolean v1, Lorg/apache/lucene/search/Scorer;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v1

    if-eq v1, p3, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 85
    :cond_0
    invoke-virtual {p1, p0}, Lorg/apache/lucene/search/Collector;->setScorer(Lorg/apache/lucene/search/Scorer;)V

    .line 87
    move v0, p3

    .local v0, "doc":I
    :goto_0
    if-lt v0, p2, :cond_1

    .line 90
    const v1, 0x7fffffff

    if-eq v0, v1, :cond_2

    const/4 v1, 0x1

    :goto_1
    return v1

    .line 88
    :cond_1
    invoke-virtual {p1, v0}, Lorg/apache/lucene/search/Collector;->collect(I)V

    .line 87
    invoke-virtual {p0}, Lorg/apache/lucene/search/Scorer;->nextDoc()I

    move-result v0

    goto :goto_0

    .line 90
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method
