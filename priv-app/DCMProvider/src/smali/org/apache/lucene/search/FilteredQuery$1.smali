.class Lorg/apache/lucene/search/FilteredQuery$1;
.super Lorg/apache/lucene/search/Weight;
.source "FilteredQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/FilteredQuery;->createWeight(Lorg/apache/lucene/search/IndexSearcher;)Lorg/apache/lucene/search/Weight;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/search/FilteredQuery;

.field private final synthetic val$weight:Lorg/apache/lucene/search/Weight;


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/FilteredQuery;Lorg/apache/lucene/search/Weight;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/search/FilteredQuery$1;->this$0:Lorg/apache/lucene/search/FilteredQuery;

    iput-object p2, p0, Lorg/apache/lucene/search/FilteredQuery$1;->val$weight:Lorg/apache/lucene/search/Weight;

    .line 83
    invoke-direct {p0}, Lorg/apache/lucene/search/Weight;-><init>()V

    return-void
.end method


# virtual methods
.method public explain(Lorg/apache/lucene/index/AtomicReaderContext;I)Lorg/apache/lucene/search/Explanation;
    .locals 8
    .param p1, "ir"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "i"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 102
    iget-object v5, p0, Lorg/apache/lucene/search/FilteredQuery$1;->val$weight:Lorg/apache/lucene/search/Weight;

    invoke-virtual {v5, p1, p2}, Lorg/apache/lucene/search/Weight;->explain(Lorg/apache/lucene/index/AtomicReaderContext;I)Lorg/apache/lucene/search/Explanation;

    move-result-object v3

    .line 103
    .local v3, "inner":Lorg/apache/lucene/search/Explanation;
    iget-object v5, p0, Lorg/apache/lucene/search/FilteredQuery$1;->this$0:Lorg/apache/lucene/search/FilteredQuery;

    # getter for: Lorg/apache/lucene/search/FilteredQuery;->filter:Lorg/apache/lucene/search/Filter;
    invoke-static {v5}, Lorg/apache/lucene/search/FilteredQuery;->access$0(Lorg/apache/lucene/search/FilteredQuery;)Lorg/apache/lucene/search/Filter;

    move-result-object v2

    .line 104
    .local v2, "f":Lorg/apache/lucene/search/Filter;
    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/lucene/index/AtomicReader;->getLiveDocs()Lorg/apache/lucene/util/Bits;

    move-result-object v5

    invoke-virtual {v2, p1, v5}, Lorg/apache/lucene/search/Filter;->getDocIdSet(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/DocIdSet;

    move-result-object v0

    .line 105
    .local v0, "docIdSet":Lorg/apache/lucene/search/DocIdSet;
    if-nez v0, :cond_1

    sget-object v5, Lorg/apache/lucene/search/DocIdSet;->EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;

    invoke-virtual {v5}, Lorg/apache/lucene/search/DocIdSet;->iterator()Lorg/apache/lucene/search/DocIdSetIterator;

    move-result-object v1

    .line 106
    .local v1, "docIdSetIterator":Lorg/apache/lucene/search/DocIdSetIterator;
    :goto_0
    if-nez v1, :cond_0

    .line 107
    sget-object v5, Lorg/apache/lucene/search/DocIdSet;->EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;

    invoke-virtual {v5}, Lorg/apache/lucene/search/DocIdSet;->iterator()Lorg/apache/lucene/search/DocIdSetIterator;

    move-result-object v1

    .line 109
    :cond_0
    invoke-virtual {v1, p2}, Lorg/apache/lucene/search/DocIdSetIterator;->advance(I)I

    move-result v5

    if-ne v5, p2, :cond_2

    .line 115
    .end local v3    # "inner":Lorg/apache/lucene/search/Explanation;
    :goto_1
    return-object v3

    .line 105
    .end local v1    # "docIdSetIterator":Lorg/apache/lucene/search/DocIdSetIterator;
    .restart local v3    # "inner":Lorg/apache/lucene/search/Explanation;
    :cond_1
    invoke-virtual {v0}, Lorg/apache/lucene/search/DocIdSet;->iterator()Lorg/apache/lucene/search/DocIdSetIterator;

    move-result-object v1

    goto :goto_0

    .line 112
    .restart local v1    # "docIdSetIterator":Lorg/apache/lucene/search/DocIdSetIterator;
    :cond_2
    new-instance v4, Lorg/apache/lucene/search/Explanation;

    .line 113
    const/4 v5, 0x0

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "failure to match filter: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 112
    invoke-direct {v4, v5, v6}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    .line 114
    .local v4, "result":Lorg/apache/lucene/search/Explanation;
    invoke-virtual {v4, v3}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    move-object v3, v4

    .line 115
    goto :goto_1
.end method

.method public getQuery()Lorg/apache/lucene/search/Query;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lorg/apache/lucene/search/FilteredQuery$1;->this$0:Lorg/apache/lucene/search/FilteredQuery;

    return-object v0
.end method

.method public getValueForNormalization()F
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 92
    iget-object v0, p0, Lorg/apache/lucene/search/FilteredQuery$1;->val$weight:Lorg/apache/lucene/search/Weight;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Weight;->getValueForNormalization()F

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/search/FilteredQuery$1;->this$0:Lorg/apache/lucene/search/FilteredQuery;

    invoke-virtual {v1}, Lorg/apache/lucene/search/FilteredQuery;->getBoost()F

    move-result v1

    mul-float/2addr v0, v1

    iget-object v1, p0, Lorg/apache/lucene/search/FilteredQuery$1;->this$0:Lorg/apache/lucene/search/FilteredQuery;

    invoke-virtual {v1}, Lorg/apache/lucene/search/FilteredQuery;->getBoost()F

    move-result v1

    mul-float/2addr v0, v1

    return v0
.end method

.method public normalize(FF)V
    .locals 2
    .param p1, "norm"    # F
    .param p2, "topLevelBoost"    # F

    .prologue
    .line 97
    iget-object v0, p0, Lorg/apache/lucene/search/FilteredQuery$1;->val$weight:Lorg/apache/lucene/search/Weight;

    iget-object v1, p0, Lorg/apache/lucene/search/FilteredQuery$1;->this$0:Lorg/apache/lucene/search/FilteredQuery;

    invoke-virtual {v1}, Lorg/apache/lucene/search/FilteredQuery;->getBoost()F

    move-result v1

    mul-float/2addr v1, p2

    invoke-virtual {v0, p1, v1}, Lorg/apache/lucene/search/Weight;->normalize(FF)V

    .line 98
    return-void
.end method

.method public scorer(Lorg/apache/lucene/index/AtomicReaderContext;ZZLorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/Scorer;
    .locals 6
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "scoreDocsInOrder"    # Z
    .param p3, "topScorer"    # Z
    .param p4, "acceptDocs"    # Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 126
    sget-boolean v0, Lorg/apache/lucene/search/FilteredQuery;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/search/FilteredQuery$1;->this$0:Lorg/apache/lucene/search/FilteredQuery;

    # getter for: Lorg/apache/lucene/search/FilteredQuery;->filter:Lorg/apache/lucene/search/Filter;
    invoke-static {v0}, Lorg/apache/lucene/search/FilteredQuery;->access$0(Lorg/apache/lucene/search/FilteredQuery;)Lorg/apache/lucene/search/Filter;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 128
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/FilteredQuery$1;->this$0:Lorg/apache/lucene/search/FilteredQuery;

    # getter for: Lorg/apache/lucene/search/FilteredQuery;->filter:Lorg/apache/lucene/search/Filter;
    invoke-static {v0}, Lorg/apache/lucene/search/FilteredQuery;->access$0(Lorg/apache/lucene/search/FilteredQuery;)Lorg/apache/lucene/search/Filter;

    move-result-object v0

    invoke-virtual {v0, p1, p4}, Lorg/apache/lucene/search/Filter;->getDocIdSet(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/DocIdSet;

    move-result-object v5

    .line 129
    .local v5, "filterDocIdSet":Lorg/apache/lucene/search/DocIdSet;
    if-nez v5, :cond_1

    .line 131
    const/4 v0, 0x0

    .line 133
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/search/FilteredQuery$1;->this$0:Lorg/apache/lucene/search/FilteredQuery;

    # getter for: Lorg/apache/lucene/search/FilteredQuery;->strategy:Lorg/apache/lucene/search/FilteredQuery$FilterStrategy;
    invoke-static {v0}, Lorg/apache/lucene/search/FilteredQuery;->access$1(Lorg/apache/lucene/search/FilteredQuery;)Lorg/apache/lucene/search/FilteredQuery$FilterStrategy;

    move-result-object v0

    iget-object v4, p0, Lorg/apache/lucene/search/FilteredQuery$1;->val$weight:Lorg/apache/lucene/search/Weight;

    move-object v1, p1

    move v2, p2

    move v3, p3

    invoke-virtual/range {v0 .. v5}, Lorg/apache/lucene/search/FilteredQuery$FilterStrategy;->filteredScorer(Lorg/apache/lucene/index/AtomicReaderContext;ZZLorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/DocIdSet;)Lorg/apache/lucene/search/Scorer;

    move-result-object v0

    goto :goto_0
.end method

.method public scoresDocsOutOfOrder()Z
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x1

    return v0
.end method
