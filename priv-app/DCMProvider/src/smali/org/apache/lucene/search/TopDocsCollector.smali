.class public abstract Lorg/apache/lucene/search/TopDocsCollector;
.super Lorg/apache/lucene/search/Collector;
.source "TopDocsCollector.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lorg/apache/lucene/search/ScoreDoc;",
        ">",
        "Lorg/apache/lucene/search/Collector;"
    }
.end annotation


# static fields
.field protected static final EMPTY_TOPDOCS:Lorg/apache/lucene/search/TopDocs;


# instance fields
.field protected pq:Lorg/apache/lucene/util/PriorityQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/PriorityQueue",
            "<TT;>;"
        }
    .end annotation
.end field

.field protected totalHits:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 38
    new-instance v0, Lorg/apache/lucene/search/TopDocs;

    new-array v1, v3, [Lorg/apache/lucene/search/ScoreDoc;

    const/high16 v2, 0x7fc00000    # NaNf

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/lucene/search/TopDocs;-><init>(I[Lorg/apache/lucene/search/ScoreDoc;F)V

    sput-object v0, Lorg/apache/lucene/search/TopDocsCollector;->EMPTY_TOPDOCS:Lorg/apache/lucene/search/TopDocs;

    return-void
.end method

.method protected constructor <init>(Lorg/apache/lucene/util/PriorityQueue;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/PriorityQueue",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 51
    .local p0, "this":Lorg/apache/lucene/search/TopDocsCollector;, "Lorg/apache/lucene/search/TopDocsCollector<TT;>;"
    .local p1, "pq":Lorg/apache/lucene/util/PriorityQueue;, "Lorg/apache/lucene/util/PriorityQueue<TT;>;"
    invoke-direct {p0}, Lorg/apache/lucene/search/Collector;-><init>()V

    .line 52
    iput-object p1, p0, Lorg/apache/lucene/search/TopDocsCollector;->pq:Lorg/apache/lucene/util/PriorityQueue;

    .line 53
    return-void
.end method


# virtual methods
.method public getTotalHits()I
    .locals 1

    .prologue
    .line 77
    .local p0, "this":Lorg/apache/lucene/search/TopDocsCollector;, "Lorg/apache/lucene/search/TopDocsCollector<TT;>;"
    iget v0, p0, Lorg/apache/lucene/search/TopDocsCollector;->totalHits:I

    return v0
.end method

.method protected newTopDocs([Lorg/apache/lucene/search/ScoreDoc;I)Lorg/apache/lucene/search/TopDocs;
    .locals 2
    .param p1, "results"    # [Lorg/apache/lucene/search/ScoreDoc;
    .param p2, "start"    # I

    .prologue
    .line 72
    .local p0, "this":Lorg/apache/lucene/search/TopDocsCollector;, "Lorg/apache/lucene/search/TopDocsCollector<TT;>;"
    if-nez p1, :cond_0

    sget-object v0, Lorg/apache/lucene/search/TopDocsCollector;->EMPTY_TOPDOCS:Lorg/apache/lucene/search/TopDocs;

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/apache/lucene/search/TopDocs;

    iget v1, p0, Lorg/apache/lucene/search/TopDocsCollector;->totalHits:I

    invoke-direct {v0, v1, p1}, Lorg/apache/lucene/search/TopDocs;-><init>(I[Lorg/apache/lucene/search/ScoreDoc;)V

    goto :goto_0
.end method

.method protected populateResults([Lorg/apache/lucene/search/ScoreDoc;I)V
    .locals 2
    .param p1, "results"    # [Lorg/apache/lucene/search/ScoreDoc;
    .param p2, "howMany"    # I

    .prologue
    .line 60
    .local p0, "this":Lorg/apache/lucene/search/TopDocsCollector;, "Lorg/apache/lucene/search/TopDocsCollector<TT;>;"
    add-int/lit8 v0, p2, -0x1

    .local v0, "i":I
    :goto_0
    if-gez v0, :cond_0

    .line 63
    return-void

    .line 61
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/TopDocsCollector;->pq:Lorg/apache/lucene/util/PriorityQueue;

    invoke-virtual {v1}, Lorg/apache/lucene/util/PriorityQueue;->pop()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/search/ScoreDoc;

    aput-object v1, p1, v0

    .line 60
    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method public topDocs()Lorg/apache/lucene/search/TopDocs;
    .locals 2

    .prologue
    .line 93
    .local p0, "this":Lorg/apache/lucene/search/TopDocsCollector;, "Lorg/apache/lucene/search/TopDocsCollector<TT;>;"
    const/4 v0, 0x0

    invoke-virtual {p0}, Lorg/apache/lucene/search/TopDocsCollector;->topDocsSize()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/search/TopDocsCollector;->topDocs(II)Lorg/apache/lucene/search/TopDocs;

    move-result-object v0

    return-object v0
.end method

.method public topDocs(I)Lorg/apache/lucene/search/TopDocs;
    .locals 1
    .param p1, "start"    # I

    .prologue
    .line 112
    .local p0, "this":Lorg/apache/lucene/search/TopDocsCollector;, "Lorg/apache/lucene/search/TopDocsCollector<TT;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/search/TopDocsCollector;->topDocsSize()I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/search/TopDocsCollector;->topDocs(II)Lorg/apache/lucene/search/TopDocs;

    move-result-object v0

    return-object v0
.end method

.method public topDocs(II)Lorg/apache/lucene/search/TopDocs;
    .locals 4
    .param p1, "start"    # I
    .param p2, "howMany"    # I

    .prologue
    .line 134
    .local p0, "this":Lorg/apache/lucene/search/TopDocsCollector;, "Lorg/apache/lucene/search/TopDocsCollector<TT;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/search/TopDocsCollector;->topDocsSize()I

    move-result v2

    .line 140
    .local v2, "size":I
    if-ltz p1, :cond_0

    if-ge p1, v2, :cond_0

    if-gtz p2, :cond_1

    .line 141
    :cond_0
    const/4 v3, 0x0

    invoke-virtual {p0, v3, p1}, Lorg/apache/lucene/search/TopDocsCollector;->newTopDocs([Lorg/apache/lucene/search/ScoreDoc;I)Lorg/apache/lucene/search/TopDocs;

    move-result-object v3

    .line 158
    :goto_0
    return-object v3

    .line 145
    :cond_1
    sub-int v3, v2, p1

    invoke-static {v3, p2}, Ljava/lang/Math;->min(II)I

    move-result p2

    .line 146
    new-array v1, p2, [Lorg/apache/lucene/search/ScoreDoc;

    .line 153
    .local v1, "results":[Lorg/apache/lucene/search/ScoreDoc;
    iget-object v3, p0, Lorg/apache/lucene/search/TopDocsCollector;->pq:Lorg/apache/lucene/util/PriorityQueue;

    invoke-virtual {v3}, Lorg/apache/lucene/util/PriorityQueue;->size()I

    move-result v3

    sub-int/2addr v3, p1

    sub-int v0, v3, p2

    .local v0, "i":I
    :goto_1
    if-gtz v0, :cond_2

    .line 156
    invoke-virtual {p0, v1, p2}, Lorg/apache/lucene/search/TopDocsCollector;->populateResults([Lorg/apache/lucene/search/ScoreDoc;I)V

    .line 158
    invoke-virtual {p0, v1, p1}, Lorg/apache/lucene/search/TopDocsCollector;->newTopDocs([Lorg/apache/lucene/search/ScoreDoc;I)Lorg/apache/lucene/search/TopDocs;

    move-result-object v3

    goto :goto_0

    .line 153
    :cond_2
    iget-object v3, p0, Lorg/apache/lucene/search/TopDocsCollector;->pq:Lorg/apache/lucene/util/PriorityQueue;

    invoke-virtual {v3}, Lorg/apache/lucene/util/PriorityQueue;->pop()Ljava/lang/Object;

    add-int/lit8 v0, v0, -0x1

    goto :goto_1
.end method

.method protected topDocsSize()I
    .locals 2

    .prologue
    .line 85
    .local p0, "this":Lorg/apache/lucene/search/TopDocsCollector;, "Lorg/apache/lucene/search/TopDocsCollector<TT;>;"
    iget v0, p0, Lorg/apache/lucene/search/TopDocsCollector;->totalHits:I

    iget-object v1, p0, Lorg/apache/lucene/search/TopDocsCollector;->pq:Lorg/apache/lucene/util/PriorityQueue;

    invoke-virtual {v1}, Lorg/apache/lucene/util/PriorityQueue;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget v0, p0, Lorg/apache/lucene/search/TopDocsCollector;->totalHits:I

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/TopDocsCollector;->pq:Lorg/apache/lucene/util/PriorityQueue;

    invoke-virtual {v0}, Lorg/apache/lucene/util/PriorityQueue;->size()I

    move-result v0

    goto :goto_0
.end method
