.class public Lorg/apache/lucene/search/ComplexExplanation;
.super Lorg/apache/lucene/search/Explanation;
.source "ComplexExplanation.java"


# instance fields
.field private match:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lorg/apache/lucene/search/Explanation;-><init>()V

    .line 27
    return-void
.end method

.method public constructor <init>(ZFLjava/lang/String;)V
    .locals 1
    .param p1, "match"    # Z
    .param p2, "value"    # F
    .param p3, "description"    # Ljava/lang/String;

    .prologue
    .line 32
    invoke-direct {p0, p2, p3}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    .line 33
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/ComplexExplanation;->match:Ljava/lang/Boolean;

    .line 34
    return-void
.end method


# virtual methods
.method public getMatch()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lorg/apache/lucene/search/ComplexExplanation;->match:Ljava/lang/Boolean;

    return-object v0
.end method

.method protected getSummary()Ljava/lang/String;
    .locals 2

    .prologue
    .line 63
    invoke-virtual {p0}, Lorg/apache/lucene/search/ComplexExplanation;->getMatch()Ljava/lang/Boolean;

    move-result-object v0

    if-nez v0, :cond_0

    .line 64
    invoke-super {p0}, Lorg/apache/lucene/search/Explanation;->getSummary()Ljava/lang/String;

    move-result-object v0

    .line 66
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/lucene/search/ComplexExplanation;->getValue()F

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, " = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 67
    invoke-virtual {p0}, Lorg/apache/lucene/search/ComplexExplanation;->isMatch()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "(MATCH) "

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 68
    invoke-virtual {p0}, Lorg/apache/lucene/search/ComplexExplanation;->getDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 66
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 67
    :cond_1
    const-string v0, "(NON-MATCH) "

    goto :goto_1
.end method

.method public isMatch()Z
    .locals 2

    .prologue
    .line 57
    invoke-virtual {p0}, Lorg/apache/lucene/search/ComplexExplanation;->getMatch()Ljava/lang/Boolean;

    move-result-object v0

    .line 58
    .local v0, "m":Ljava/lang/Boolean;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    :goto_0
    return v1

    :cond_0
    invoke-super {p0}, Lorg/apache/lucene/search/Explanation;->isMatch()Z

    move-result v1

    goto :goto_0
.end method

.method public setMatch(Ljava/lang/Boolean;)V
    .locals 0
    .param p1, "match"    # Ljava/lang/Boolean;

    .prologue
    .line 45
    iput-object p1, p0, Lorg/apache/lucene/search/ComplexExplanation;->match:Ljava/lang/Boolean;

    return-void
.end method
