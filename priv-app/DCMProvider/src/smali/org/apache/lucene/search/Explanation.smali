.class public Lorg/apache/lucene/search/Explanation;
.super Ljava/lang/Object;
.source "Explanation.java"


# instance fields
.field private description:Ljava/lang/String;

.field private details:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/lucene/search/Explanation;",
            ">;"
        }
    .end annotation
.end field

.field private value:F


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(FLjava/lang/String;)V
    .locals 0
    .param p1, "value"    # F
    .param p2, "description"    # Ljava/lang/String;

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput p1, p0, Lorg/apache/lucene/search/Explanation;->value:F

    .line 32
    iput-object p2, p0, Lorg/apache/lucene/search/Explanation;->description:Ljava/lang/String;

    .line 33
    return-void
.end method


# virtual methods
.method public addDetail(Lorg/apache/lucene/search/Explanation;)V
    .locals 1
    .param p1, "detail"    # Lorg/apache/lucene/search/Explanation;

    .prologue
    .line 78
    iget-object v0, p0, Lorg/apache/lucene/search/Explanation;->details:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 79
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/search/Explanation;->details:Ljava/util/ArrayList;

    .line 80
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/Explanation;->details:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 81
    return-void
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lorg/apache/lucene/search/Explanation;->description:Ljava/lang/String;

    return-object v0
.end method

.method public getDetails()[Lorg/apache/lucene/search/Explanation;
    .locals 2

    .prologue
    .line 71
    iget-object v0, p0, Lorg/apache/lucene/search/Explanation;->details:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 72
    const/4 v0, 0x0

    .line 73
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/Explanation;->details:Ljava/util/ArrayList;

    const/4 v1, 0x0

    new-array v1, v1, [Lorg/apache/lucene/search/Explanation;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/lucene/search/Explanation;

    goto :goto_0
.end method

.method protected getSummary()Ljava/lang/String;
    .locals 2

    .prologue
    .line 66
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, " = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/lucene/search/Explanation;->getDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getValue()F
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lorg/apache/lucene/search/Explanation;->value:F

    return v0
.end method

.method public isMatch()Z
    .locals 2

    .prologue
    .line 44
    const/4 v0, 0x0

    invoke-virtual {p0}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setDescription(Ljava/lang/String;)V
    .locals 0
    .param p1, "description"    # Ljava/lang/String;

    .prologue
    .line 58
    iput-object p1, p0, Lorg/apache/lucene/search/Explanation;->description:Ljava/lang/String;

    .line 59
    return-void
.end method

.method public setValue(F)V
    .locals 0
    .param p1, "value"    # F

    .prologue
    .line 52
    iput p1, p0, Lorg/apache/lucene/search/Explanation;->value:F

    return-void
.end method

.method public toHtml()Ljava/lang/String;
    .locals 4

    .prologue
    .line 109
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 110
    .local v0, "buffer":Ljava/lang/StringBuilder;
    const-string v3, "<ul>\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    const-string v3, "<li>"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 113
    invoke-virtual {p0}, Lorg/apache/lucene/search/Explanation;->getSummary()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    const-string v3, "<br />\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 116
    invoke-virtual {p0}, Lorg/apache/lucene/search/Explanation;->getDetails()[Lorg/apache/lucene/search/Explanation;

    move-result-object v1

    .line 117
    .local v1, "details":[Lorg/apache/lucene/search/Explanation;
    if-eqz v1, :cond_0

    .line 118
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v3, v1

    if-lt v2, v3, :cond_1

    .line 123
    .end local v2    # "i":I
    :cond_0
    const-string v3, "</li>\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 124
    const-string v3, "</ul>\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 119
    .restart local v2    # "i":I
    :cond_1
    aget-object v3, v1, v2

    invoke-virtual {v3}, Lorg/apache/lucene/search/Explanation;->toHtml()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 118
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/search/Explanation;->toString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected toString(I)Ljava/lang/String;
    .locals 5
    .param p1, "depth"    # I

    .prologue
    .line 89
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 90
    .local v0, "buffer":Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-lt v2, p1, :cond_1

    .line 93
    invoke-virtual {p0}, Lorg/apache/lucene/search/Explanation;->getSummary()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 94
    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 96
    invoke-virtual {p0}, Lorg/apache/lucene/search/Explanation;->getDetails()[Lorg/apache/lucene/search/Explanation;

    move-result-object v1

    .line 97
    .local v1, "details":[Lorg/apache/lucene/search/Explanation;
    if-eqz v1, :cond_0

    .line 98
    const/4 v2, 0x0

    :goto_1
    array-length v3, v1

    if-lt v2, v3, :cond_2

    .line 103
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 91
    .end local v1    # "details":[Lorg/apache/lucene/search/Explanation;
    :cond_1
    const-string v3, "  "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 90
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 99
    .restart local v1    # "details":[Lorg/apache/lucene/search/Explanation;
    :cond_2
    aget-object v3, v1, v2

    add-int/lit8 v4, p1, 0x1

    invoke-virtual {v3, v4}, Lorg/apache/lucene/search/Explanation;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 98
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method
