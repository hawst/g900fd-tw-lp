.class public abstract Lorg/apache/lucene/search/FieldCacheDocIdSet;
.super Lorg/apache/lucene/search/DocIdSet;
.source "FieldCacheDocIdSet.java"


# instance fields
.field protected final acceptDocs:Lorg/apache/lucene/util/Bits;

.field protected final maxDoc:I


# direct methods
.method public constructor <init>(ILorg/apache/lucene/util/Bits;)V
    .locals 0
    .param p1, "maxDoc"    # I
    .param p2, "acceptDocs"    # Lorg/apache/lucene/util/Bits;

    .prologue
    .line 38
    invoke-direct {p0}, Lorg/apache/lucene/search/DocIdSet;-><init>()V

    .line 39
    iput p1, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet;->maxDoc:I

    .line 40
    iput-object p2, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet;->acceptDocs:Lorg/apache/lucene/util/Bits;

    .line 41
    return-void
.end method


# virtual methods
.method public final bits()Lorg/apache/lucene/util/Bits;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet;->acceptDocs:Lorg/apache/lucene/util/Bits;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/lucene/search/FieldCacheDocIdSet$1;

    invoke-direct {v0, p0}, Lorg/apache/lucene/search/FieldCacheDocIdSet$1;-><init>(Lorg/apache/lucene/search/FieldCacheDocIdSet;)V

    :goto_0
    return-object v0

    .line 69
    :cond_0
    new-instance v0, Lorg/apache/lucene/search/FieldCacheDocIdSet$2;

    invoke-direct {v0, p0}, Lorg/apache/lucene/search/FieldCacheDocIdSet$2;-><init>(Lorg/apache/lucene/search/FieldCacheDocIdSet;)V

    goto :goto_0
.end method

.method public final isCacheable()Z
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x1

    return v0
.end method

.method public final iterator()Lorg/apache/lucene/search/DocIdSetIterator;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 84
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet;->acceptDocs:Lorg/apache/lucene/util/Bits;

    if-nez v0, :cond_0

    .line 86
    new-instance v0, Lorg/apache/lucene/search/FieldCacheDocIdSet$3;

    invoke-direct {v0, p0}, Lorg/apache/lucene/search/FieldCacheDocIdSet$3;-><init>(Lorg/apache/lucene/search/FieldCacheDocIdSet;)V

    .line 131
    :goto_0
    return-object v0

    .line 120
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet;->acceptDocs:Lorg/apache/lucene/util/Bits;

    instance-of v0, v0, Lorg/apache/lucene/util/FixedBitSet;

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet;->acceptDocs:Lorg/apache/lucene/util/Bits;

    instance-of v0, v0, Lorg/apache/lucene/util/OpenBitSet;

    if-eqz v0, :cond_2

    .line 123
    :cond_1
    new-instance v1, Lorg/apache/lucene/search/FieldCacheDocIdSet$4;

    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet;->acceptDocs:Lorg/apache/lucene/util/Bits;

    check-cast v0, Lorg/apache/lucene/search/DocIdSet;

    invoke-virtual {v0}, Lorg/apache/lucene/search/DocIdSet;->iterator()Lorg/apache/lucene/search/DocIdSetIterator;

    move-result-object v0

    invoke-direct {v1, p0, v0}, Lorg/apache/lucene/search/FieldCacheDocIdSet$4;-><init>(Lorg/apache/lucene/search/FieldCacheDocIdSet;Lorg/apache/lucene/search/DocIdSetIterator;)V

    move-object v0, v1

    goto :goto_0

    .line 131
    :cond_2
    new-instance v0, Lorg/apache/lucene/search/FieldCacheDocIdSet$5;

    invoke-direct {v0, p0}, Lorg/apache/lucene/search/FieldCacheDocIdSet$5;-><init>(Lorg/apache/lucene/search/FieldCacheDocIdSet;)V

    goto :goto_0
.end method

.method protected abstract matchDoc(I)Z
.end method
