.class public Lorg/apache/lucene/search/AutomatonQuery;
.super Lorg/apache/lucene/search/MultiTermQuery;
.source "AutomatonQuery.java"


# instance fields
.field protected final automaton:Lorg/apache/lucene/util/automaton/Automaton;

.field protected final compiled:Lorg/apache/lucene/util/automaton/CompiledAutomaton;

.field protected final term:Lorg/apache/lucene/index/Term;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/Term;Lorg/apache/lucene/util/automaton/Automaton;)V
    .locals 1
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .param p2, "automaton"    # Lorg/apache/lucene/util/automaton/Automaton;

    .prologue
    .line 65
    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/lucene/search/MultiTermQuery;-><init>(Ljava/lang/String;)V

    .line 66
    iput-object p1, p0, Lorg/apache/lucene/search/AutomatonQuery;->term:Lorg/apache/lucene/index/Term;

    .line 67
    iput-object p2, p0, Lorg/apache/lucene/search/AutomatonQuery;->automaton:Lorg/apache/lucene/util/automaton/Automaton;

    .line 68
    new-instance v0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;

    invoke-direct {v0, p2}, Lorg/apache/lucene/util/automaton/CompiledAutomaton;-><init>(Lorg/apache/lucene/util/automaton/Automaton;)V

    iput-object v0, p0, Lorg/apache/lucene/search/AutomatonQuery;->compiled:Lorg/apache/lucene/util/automaton/CompiledAutomaton;

    .line 69
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 96
    if-ne p0, p1, :cond_1

    .line 113
    :cond_0
    :goto_0
    return v1

    .line 98
    :cond_1
    invoke-super {p0, p1}, Lorg/apache/lucene/search/MultiTermQuery;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    move v1, v2

    .line 99
    goto :goto_0

    .line 100
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 101
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 102
    check-cast v0, Lorg/apache/lucene/search/AutomatonQuery;

    .line 103
    .local v0, "other":Lorg/apache/lucene/search/AutomatonQuery;
    iget-object v3, p0, Lorg/apache/lucene/search/AutomatonQuery;->automaton:Lorg/apache/lucene/util/automaton/Automaton;

    if-nez v3, :cond_4

    .line 104
    iget-object v3, v0, Lorg/apache/lucene/search/AutomatonQuery;->automaton:Lorg/apache/lucene/util/automaton/Automaton;

    if-eqz v3, :cond_5

    move v1, v2

    .line 105
    goto :goto_0

    .line 106
    :cond_4
    iget-object v3, p0, Lorg/apache/lucene/search/AutomatonQuery;->automaton:Lorg/apache/lucene/util/automaton/Automaton;

    iget-object v4, v0, Lorg/apache/lucene/search/AutomatonQuery;->automaton:Lorg/apache/lucene/util/automaton/Automaton;

    invoke-static {v3, v4}, Lorg/apache/lucene/util/automaton/BasicOperations;->sameLanguage(Lorg/apache/lucene/util/automaton/Automaton;Lorg/apache/lucene/util/automaton/Automaton;)Z

    move-result v3

    if-nez v3, :cond_5

    move v1, v2

    .line 107
    goto :goto_0

    .line 108
    :cond_5
    iget-object v3, p0, Lorg/apache/lucene/search/AutomatonQuery;->term:Lorg/apache/lucene/index/Term;

    if-nez v3, :cond_6

    .line 109
    iget-object v3, v0, Lorg/apache/lucene/search/AutomatonQuery;->term:Lorg/apache/lucene/index/Term;

    if-eqz v3, :cond_0

    move v1, v2

    .line 110
    goto :goto_0

    .line 111
    :cond_6
    iget-object v3, p0, Lorg/apache/lucene/search/AutomatonQuery;->term:Lorg/apache/lucene/index/Term;

    iget-object v4, v0, Lorg/apache/lucene/search/AutomatonQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/index/Term;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 112
    goto :goto_0
.end method

.method protected getTermsEnum(Lorg/apache/lucene/index/Terms;Lorg/apache/lucene/util/AttributeSource;)Lorg/apache/lucene/index/TermsEnum;
    .locals 1
    .param p1, "terms"    # Lorg/apache/lucene/index/Terms;
    .param p2, "atts"    # Lorg/apache/lucene/util/AttributeSource;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 73
    iget-object v0, p0, Lorg/apache/lucene/search/AutomatonQuery;->compiled:Lorg/apache/lucene/util/automaton/CompiledAutomaton;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->getTermsEnum(Lorg/apache/lucene/index/Terms;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    .line 78
    const/16 v1, 0x1f

    .line 79
    .local v1, "prime":I
    invoke-super {p0}, Lorg/apache/lucene/search/MultiTermQuery;->hashCode()I

    move-result v2

    .line 80
    .local v2, "result":I
    iget-object v3, p0, Lorg/apache/lucene/search/AutomatonQuery;->automaton:Lorg/apache/lucene/util/automaton/Automaton;

    if-eqz v3, :cond_1

    .line 84
    iget-object v3, p0, Lorg/apache/lucene/search/AutomatonQuery;->automaton:Lorg/apache/lucene/util/automaton/Automaton;

    invoke-virtual {v3}, Lorg/apache/lucene/util/automaton/Automaton;->getNumberOfStates()I

    move-result v3

    mul-int/lit8 v3, v3, 0x3

    iget-object v4, p0, Lorg/apache/lucene/search/AutomatonQuery;->automaton:Lorg/apache/lucene/util/automaton/Automaton;

    invoke-virtual {v4}, Lorg/apache/lucene/util/automaton/Automaton;->getNumberOfTransitions()I

    move-result v4

    mul-int/lit8 v4, v4, 0x2

    add-int v0, v3, v4

    .line 85
    .local v0, "automatonHashCode":I
    if-nez v0, :cond_0

    .line 86
    const/4 v0, 0x1

    .line 88
    :cond_0
    mul-int/lit8 v3, v2, 0x1f

    add-int v2, v3, v0

    .line 90
    .end local v0    # "automatonHashCode":I
    :cond_1
    mul-int/lit8 v4, v2, 0x1f

    iget-object v3, p0, Lorg/apache/lucene/search/AutomatonQuery;->term:Lorg/apache/lucene/index/Term;

    if-nez v3, :cond_2

    const/4 v3, 0x0

    :goto_0
    add-int v2, v4, v3

    .line 91
    return v2

    .line 90
    :cond_2
    iget-object v3, p0, Lorg/apache/lucene/search/AutomatonQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v3}, Lorg/apache/lucene/index/Term;->hashCode()I

    move-result v3

    goto :goto_0
.end method

.method public toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 118
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 119
    .local v0, "buffer":Ljava/lang/StringBuilder;
    iget-object v1, p0, Lorg/apache/lucene/search/AutomatonQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 120
    iget-object v1, p0, Lorg/apache/lucene/search/AutomatonQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 121
    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 123
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 124
    const-string v1, " {"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 125
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 126
    iget-object v1, p0, Lorg/apache/lucene/search/AutomatonQuery;->automaton:Lorg/apache/lucene/util/automaton/Automaton;

    invoke-virtual {v1}, Lorg/apache/lucene/util/automaton/Automaton;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 127
    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 128
    invoke-virtual {p0}, Lorg/apache/lucene/search/AutomatonQuery;->getBoost()F

    move-result v1

    invoke-static {v1}, Lorg/apache/lucene/util/ToStringUtils;->boost(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 129
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
