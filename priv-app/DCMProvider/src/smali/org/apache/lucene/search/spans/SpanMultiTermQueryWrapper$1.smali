.class Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$1;
.super Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$SpanRewriteMethod;
.source "SpanMultiTermQueryWrapper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$SpanRewriteMethod;"
    }
.end annotation


# instance fields
.field private final delegate:Lorg/apache/lucene/search/ScoringRewrite;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/search/ScoringRewrite",
            "<",
            "Lorg/apache/lucene/search/spans/SpanOrQuery;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 149
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$SpanRewriteMethod;-><init>()V

    .line 150
    new-instance v0, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$1$1;

    invoke-direct {v0, p0}, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$1$1;-><init>(Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$1;)V

    iput-object v0, p0, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$1;->delegate:Lorg/apache/lucene/search/ScoringRewrite;

    .line 1
    return-void
.end method


# virtual methods
.method public rewrite(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/MultiTermQuery;)Lorg/apache/lucene/search/spans/SpanQuery;
    .locals 1
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "query"    # Lorg/apache/lucene/search/MultiTermQuery;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 174
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$1;->delegate:Lorg/apache/lucene/search/ScoringRewrite;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/search/ScoringRewrite;->rewrite(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/MultiTermQuery;)Lorg/apache/lucene/search/Query;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/spans/SpanQuery;

    return-object v0
.end method
