.class public Lorg/apache/lucene/search/similarities/NormalizationH1;
.super Lorg/apache/lucene/search/similarities/Normalization;
.source "NormalizationH1.java"


# instance fields
.field private final c:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-direct {p0, v0}, Lorg/apache/lucene/search/similarities/NormalizationH1;-><init>(F)V

    .line 47
    return-void
.end method

.method public constructor <init>(F)V
    .locals 0
    .param p1, "c"    # F

    .prologue
    .line 38
    invoke-direct {p0}, Lorg/apache/lucene/search/similarities/Normalization;-><init>()V

    .line 39
    iput p1, p0, Lorg/apache/lucene/search/similarities/NormalizationH1;->c:F

    .line 40
    return-void
.end method


# virtual methods
.method public getC()F
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lorg/apache/lucene/search/similarities/NormalizationH1;->c:F

    return v0
.end method

.method public final tfn(Lorg/apache/lucene/search/similarities/BasicStats;FF)F
    .locals 1
    .param p1, "stats"    # Lorg/apache/lucene/search/similarities/BasicStats;
    .param p2, "tf"    # F
    .param p3, "len"    # F

    .prologue
    .line 51
    invoke-virtual {p1}, Lorg/apache/lucene/search/similarities/BasicStats;->getAvgFieldLength()F

    move-result v0

    mul-float/2addr v0, p2

    div-float/2addr v0, p3

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    const-string v0, "1"

    return-object v0
.end method
