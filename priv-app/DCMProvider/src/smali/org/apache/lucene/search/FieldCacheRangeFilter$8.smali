.class Lorg/apache/lucene/search/FieldCacheRangeFilter$8;
.super Lorg/apache/lucene/search/FieldCacheRangeFilter;
.source "FieldCacheRangeFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/FieldCacheRangeFilter;->newDoubleRange(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$DoubleParser;Ljava/lang/Double;Ljava/lang/Double;ZZ)Lorg/apache/lucene/search/FieldCacheRangeFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/FieldCacheRangeFilter",
        "<",
        "Ljava/lang/Double;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Ljava/lang/Double;Ljava/lang/Double;ZZ)V
    .locals 8
    .param p1, "$anonymous0"    # Ljava/lang/String;
    .param p2, "$anonymous1"    # Lorg/apache/lucene/search/FieldCache$Parser;
    .param p3, "$anonymous2"    # Ljava/lang/Double;
    .param p4, "$anonymous3"    # Ljava/lang/Double;
    .param p5, "$anonymous4"    # Z
    .param p6, "$anonymous5"    # Z

    .prologue
    .line 471
    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v7}, Lorg/apache/lucene/search/FieldCacheRangeFilter;-><init>(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Ljava/lang/Object;Ljava/lang/Object;ZZLorg/apache/lucene/search/FieldCacheRangeFilter;)V

    .line 1
    return-void
.end method


# virtual methods
.method public getDocIdSet(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/DocIdSet;
    .locals 14
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "acceptDocs"    # Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 477
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$8;->lowerVal:Ljava/lang/Object;

    if-eqz v0, :cond_2

    .line 478
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$8;->lowerVal:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v10

    .line 479
    .local v10, "f":D
    iget-boolean v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$8;->includeUpper:Z

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    cmpl-double v0, v10, v0

    if-lez v0, :cond_0

    invoke-static {v10, v11}, Ljava/lang/Double;->isInfinite(D)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 480
    sget-object v1, Lorg/apache/lucene/search/DocIdSet;->EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;

    .line 501
    .end local v10    # "f":D
    :goto_0
    return-object v1

    .line 481
    .restart local v10    # "f":D
    :cond_0
    invoke-static {v10, v11}, Lorg/apache/lucene/util/NumericUtils;->doubleToSortableLong(D)J

    move-result-wide v12

    .line 482
    .local v12, "i":J
    iget-boolean v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$8;->includeLower:Z

    if-eqz v0, :cond_1

    .end local v12    # "i":J
    :goto_1
    invoke-static {v12, v13}, Lorg/apache/lucene/util/NumericUtils;->sortableLongToDouble(J)D

    move-result-wide v6

    .line 486
    .end local v10    # "f":D
    .local v6, "inclusiveLowerPoint":D
    :goto_2
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$8;->upperVal:Ljava/lang/Object;

    if-eqz v0, :cond_5

    .line 487
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$8;->upperVal:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v10

    .line 488
    .restart local v10    # "f":D
    iget-boolean v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$8;->includeUpper:Z

    if-nez v0, :cond_3

    const-wide/16 v0, 0x0

    cmpg-double v0, v10, v0

    if-gez v0, :cond_3

    invoke-static {v10, v11}, Ljava/lang/Double;->isInfinite(D)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 489
    sget-object v1, Lorg/apache/lucene/search/DocIdSet;->EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;

    goto :goto_0

    .line 482
    .end local v6    # "inclusiveLowerPoint":D
    .restart local v12    # "i":J
    :cond_1
    const-wide/16 v0, 0x1

    add-long/2addr v12, v0

    goto :goto_1

    .line 484
    .end local v10    # "f":D
    .end local v12    # "i":J
    :cond_2
    const-wide/high16 v6, -0x10000000000000L    # Double.NEGATIVE_INFINITY

    .restart local v6    # "inclusiveLowerPoint":D
    goto :goto_2

    .line 490
    .restart local v10    # "f":D
    :cond_3
    invoke-static {v10, v11}, Lorg/apache/lucene/util/NumericUtils;->doubleToSortableLong(D)J

    move-result-wide v12

    .line 491
    .restart local v12    # "i":J
    iget-boolean v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$8;->includeUpper:Z

    if-eqz v0, :cond_4

    .end local v12    # "i":J
    :goto_3
    invoke-static {v12, v13}, Lorg/apache/lucene/util/NumericUtils;->sortableLongToDouble(J)D

    move-result-wide v8

    .line 496
    .end local v10    # "f":D
    .local v8, "inclusiveUpperPoint":D
    :goto_4
    cmpl-double v0, v6, v8

    if-lez v0, :cond_6

    .line 497
    sget-object v1, Lorg/apache/lucene/search/DocIdSet;->EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;

    goto :goto_0

    .line 491
    .end local v8    # "inclusiveUpperPoint":D
    .restart local v10    # "f":D
    .restart local v12    # "i":J
    :cond_4
    const-wide/16 v0, 0x1

    sub-long/2addr v12, v0

    goto :goto_3

    .line 493
    .end local v10    # "f":D
    .end local v12    # "i":J
    :cond_5
    const-wide/high16 v8, 0x7ff0000000000000L    # Double.POSITIVE_INFINITY

    .restart local v8    # "inclusiveUpperPoint":D
    goto :goto_4

    .line 499
    :cond_6
    sget-object v1, Lorg/apache/lucene/search/FieldCache;->DEFAULT:Lorg/apache/lucene/search/FieldCache;

    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$8;->field:Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$8;->parser:Lorg/apache/lucene/search/FieldCache$Parser;

    check-cast v0, Lorg/apache/lucene/search/FieldCache$DoubleParser;

    const/4 v4, 0x0

    invoke-interface {v1, v2, v3, v0, v4}, Lorg/apache/lucene/search/FieldCache;->getDoubles(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$DoubleParser;Z)Lorg/apache/lucene/search/FieldCache$Doubles;

    move-result-object v5

    .line 501
    .local v5, "values":Lorg/apache/lucene/search/FieldCache$Doubles;
    new-instance v1, Lorg/apache/lucene/search/FieldCacheRangeFilter$8$1;

    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v3

    move-object v2, p0

    move-object/from16 v4, p2

    invoke-direct/range {v1 .. v9}, Lorg/apache/lucene/search/FieldCacheRangeFilter$8$1;-><init>(Lorg/apache/lucene/search/FieldCacheRangeFilter$8;ILorg/apache/lucene/util/Bits;Lorg/apache/lucene/search/FieldCache$Doubles;DD)V

    goto :goto_0
.end method
