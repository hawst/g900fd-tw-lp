.class Lorg/apache/lucene/search/FieldCacheRangeFilter$7;
.super Lorg/apache/lucene/search/FieldCacheRangeFilter;
.source "FieldCacheRangeFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/FieldCacheRangeFilter;->newFloatRange(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$FloatParser;Ljava/lang/Float;Ljava/lang/Float;ZZ)Lorg/apache/lucene/search/FieldCacheRangeFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/FieldCacheRangeFilter",
        "<",
        "Ljava/lang/Float;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Ljava/lang/Float;Ljava/lang/Float;ZZ)V
    .locals 8
    .param p1, "$anonymous0"    # Ljava/lang/String;
    .param p2, "$anonymous1"    # Lorg/apache/lucene/search/FieldCache$Parser;
    .param p3, "$anonymous2"    # Ljava/lang/Float;
    .param p4, "$anonymous3"    # Ljava/lang/Float;
    .param p5, "$anonymous4"    # Z
    .param p6, "$anonymous5"    # Z

    .prologue
    .line 416
    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v7}, Lorg/apache/lucene/search/FieldCacheRangeFilter;-><init>(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Ljava/lang/Object;Ljava/lang/Object;ZZLorg/apache/lucene/search/FieldCacheRangeFilter;)V

    .line 1
    return-void
.end method


# virtual methods
.method public getDocIdSet(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/DocIdSet;
    .locals 10
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "acceptDocs"    # Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 422
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$7;->lowerVal:Ljava/lang/Object;

    if-eqz v0, :cond_2

    .line 423
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$7;->lowerVal:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v7

    .line 424
    .local v7, "f":F
    iget-boolean v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$7;->includeUpper:Z

    if-nez v0, :cond_0

    cmpl-float v0, v7, v1

    if-lez v0, :cond_0

    invoke-static {v7}, Ljava/lang/Float;->isInfinite(F)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 425
    sget-object v0, Lorg/apache/lucene/search/DocIdSet;->EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;

    .line 445
    .end local v7    # "f":F
    :goto_0
    return-object v0

    .line 426
    .restart local v7    # "f":F
    :cond_0
    invoke-static {v7}, Lorg/apache/lucene/util/NumericUtils;->floatToSortableInt(F)I

    move-result v8

    .line 427
    .local v8, "i":I
    iget-boolean v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$7;->includeLower:Z

    if-eqz v0, :cond_1

    .end local v8    # "i":I
    :goto_1
    invoke-static {v8}, Lorg/apache/lucene/util/NumericUtils;->sortableIntToFloat(I)F

    move-result v5

    .line 431
    .end local v7    # "f":F
    .local v5, "inclusiveLowerPoint":F
    :goto_2
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$7;->upperVal:Ljava/lang/Object;

    if-eqz v0, :cond_5

    .line 432
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$7;->upperVal:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v7

    .line 433
    .restart local v7    # "f":F
    iget-boolean v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$7;->includeUpper:Z

    if-nez v0, :cond_3

    cmpg-float v0, v7, v1

    if-gez v0, :cond_3

    invoke-static {v7}, Ljava/lang/Float;->isInfinite(F)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 434
    sget-object v0, Lorg/apache/lucene/search/DocIdSet;->EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;

    goto :goto_0

    .line 427
    .end local v5    # "inclusiveLowerPoint":F
    .restart local v8    # "i":I
    :cond_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 429
    .end local v7    # "f":F
    .end local v8    # "i":I
    :cond_2
    const/high16 v5, -0x800000    # Float.NEGATIVE_INFINITY

    .restart local v5    # "inclusiveLowerPoint":F
    goto :goto_2

    .line 435
    .restart local v7    # "f":F
    :cond_3
    invoke-static {v7}, Lorg/apache/lucene/util/NumericUtils;->floatToSortableInt(F)I

    move-result v8

    .line 436
    .restart local v8    # "i":I
    iget-boolean v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$7;->includeUpper:Z

    if-eqz v0, :cond_4

    .end local v8    # "i":I
    :goto_3
    invoke-static {v8}, Lorg/apache/lucene/util/NumericUtils;->sortableIntToFloat(I)F

    move-result v6

    .line 441
    .end local v7    # "f":F
    .local v6, "inclusiveUpperPoint":F
    :goto_4
    cmpl-float v0, v5, v6

    if-lez v0, :cond_6

    .line 442
    sget-object v0, Lorg/apache/lucene/search/DocIdSet;->EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;

    goto :goto_0

    .line 436
    .end local v6    # "inclusiveUpperPoint":F
    .restart local v7    # "f":F
    .restart local v8    # "i":I
    :cond_4
    add-int/lit8 v8, v8, -0x1

    goto :goto_3

    .line 438
    .end local v7    # "f":F
    .end local v8    # "i":I
    :cond_5
    const/high16 v6, 0x7f800000    # Float.POSITIVE_INFINITY

    .restart local v6    # "inclusiveUpperPoint":F
    goto :goto_4

    .line 444
    :cond_6
    sget-object v1, Lorg/apache/lucene/search/FieldCache;->DEFAULT:Lorg/apache/lucene/search/FieldCache;

    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$7;->field:Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$7;->parser:Lorg/apache/lucene/search/FieldCache$Parser;

    check-cast v0, Lorg/apache/lucene/search/FieldCache$FloatParser;

    const/4 v9, 0x0

    invoke-interface {v1, v2, v3, v0, v9}, Lorg/apache/lucene/search/FieldCache;->getFloats(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$FloatParser;Z)Lorg/apache/lucene/search/FieldCache$Floats;

    move-result-object v4

    .line 445
    .local v4, "values":Lorg/apache/lucene/search/FieldCache$Floats;
    new-instance v0, Lorg/apache/lucene/search/FieldCacheRangeFilter$7$1;

    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v2

    move-object v1, p0

    move-object v3, p2

    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/search/FieldCacheRangeFilter$7$1;-><init>(Lorg/apache/lucene/search/FieldCacheRangeFilter$7;ILorg/apache/lucene/util/Bits;Lorg/apache/lucene/search/FieldCache$Floats;FF)V

    goto :goto_0
.end method
