.class Lorg/apache/lucene/search/FieldCacheImpl$BytesFromArray;
.super Lorg/apache/lucene/search/FieldCache$Bytes;
.source "FieldCacheImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FieldCacheImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "BytesFromArray"
.end annotation


# instance fields
.field private final values:[B


# direct methods
.method public constructor <init>([B)V
    .locals 0
    .param p1, "values"    # [B

    .prologue
    .line 392
    invoke-direct {p0}, Lorg/apache/lucene/search/FieldCache$Bytes;-><init>()V

    .line 393
    iput-object p1, p0, Lorg/apache/lucene/search/FieldCacheImpl$BytesFromArray;->values:[B

    .line 394
    return-void
.end method


# virtual methods
.method public get(I)B
    .locals 1
    .param p1, "docID"    # I

    .prologue
    .line 398
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl$BytesFromArray;->values:[B

    aget-byte v0, v0, p1

    return v0
.end method
