.class public Lorg/apache/lucene/search/DisjunctionMaxQuery$DisjunctionMaxWeight;
.super Lorg/apache/lucene/search/Weight;
.source "DisjunctionMaxQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/DisjunctionMaxQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "DisjunctionMaxWeight"
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/search/DisjunctionMaxQuery;

.field protected weights:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/lucene/search/Weight;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/DisjunctionMaxQuery;Lorg/apache/lucene/search/IndexSearcher;)V
    .locals 4
    .param p2, "searcher"    # Lorg/apache/lucene/search/IndexSearcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 120
    iput-object p1, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery$DisjunctionMaxWeight;->this$0:Lorg/apache/lucene/search/DisjunctionMaxQuery;

    invoke-direct {p0}, Lorg/apache/lucene/search/Weight;-><init>()V

    .line 117
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery$DisjunctionMaxWeight;->weights:Ljava/util/ArrayList;

    .line 121
    # getter for: Lorg/apache/lucene/search/DisjunctionMaxQuery;->disjuncts:Ljava/util/ArrayList;
    invoke-static {p1}, Lorg/apache/lucene/search/DisjunctionMaxQuery;->access$1(Lorg/apache/lucene/search/DisjunctionMaxQuery;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 124
    return-void

    .line 121
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/Query;

    .line 122
    .local v0, "disjunctQuery":Lorg/apache/lucene/search/Query;
    iget-object v2, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery$DisjunctionMaxWeight;->weights:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Lorg/apache/lucene/search/Query;->createWeight(Lorg/apache/lucene/search/IndexSearcher;)Lorg/apache/lucene/search/Weight;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method


# virtual methods
.method public explain(Lorg/apache/lucene/index/AtomicReaderContext;I)Lorg/apache/lucene/search/Explanation;
    .locals 7
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 174
    iget-object v5, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery$DisjunctionMaxWeight;->this$0:Lorg/apache/lucene/search/DisjunctionMaxQuery;

    # getter for: Lorg/apache/lucene/search/DisjunctionMaxQuery;->disjuncts:Ljava/util/ArrayList;
    invoke-static {v5}, Lorg/apache/lucene/search/DisjunctionMaxQuery;->access$1(Lorg/apache/lucene/search/DisjunctionMaxQuery;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_0

    iget-object v5, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery$DisjunctionMaxWeight;->weights:Ljava/util/ArrayList;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/search/Weight;

    invoke-virtual {v5, p1, p2}, Lorg/apache/lucene/search/Weight;->explain(Lorg/apache/lucene/index/AtomicReaderContext;I)Lorg/apache/lucene/search/Explanation;

    move-result-object v2

    .line 188
    :goto_0
    return-object v2

    .line 175
    :cond_0
    new-instance v2, Lorg/apache/lucene/search/ComplexExplanation;

    invoke-direct {v2}, Lorg/apache/lucene/search/ComplexExplanation;-><init>()V

    .line 176
    .local v2, "result":Lorg/apache/lucene/search/ComplexExplanation;
    const/4 v1, 0x0

    .local v1, "max":F
    const/4 v3, 0x0

    .line 177
    .local v3, "sum":F
    iget-object v5, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery$DisjunctionMaxWeight;->this$0:Lorg/apache/lucene/search/DisjunctionMaxQuery;

    # getter for: Lorg/apache/lucene/search/DisjunctionMaxQuery;->tieBreakerMultiplier:F
    invoke-static {v5}, Lorg/apache/lucene/search/DisjunctionMaxQuery;->access$2(Lorg/apache/lucene/search/DisjunctionMaxQuery;)F

    move-result v5

    const/4 v6, 0x0

    cmpl-float v5, v5, v6

    if-nez v5, :cond_2

    const-string v5, "max of:"

    :goto_1
    invoke-virtual {v2, v5}, Lorg/apache/lucene/search/ComplexExplanation;->setDescription(Ljava/lang/String;)V

    .line 178
    iget-object v5, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery$DisjunctionMaxWeight;->weights:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_3

    .line 187
    sub-float v5, v3, v1

    iget-object v6, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery$DisjunctionMaxWeight;->this$0:Lorg/apache/lucene/search/DisjunctionMaxQuery;

    # getter for: Lorg/apache/lucene/search/DisjunctionMaxQuery;->tieBreakerMultiplier:F
    invoke-static {v6}, Lorg/apache/lucene/search/DisjunctionMaxQuery;->access$2(Lorg/apache/lucene/search/DisjunctionMaxQuery;)F

    move-result v6

    mul-float/2addr v5, v6

    add-float/2addr v5, v1

    invoke-virtual {v2, v5}, Lorg/apache/lucene/search/ComplexExplanation;->setValue(F)V

    goto :goto_0

    .line 177
    :cond_2
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "max plus "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery$DisjunctionMaxWeight;->this$0:Lorg/apache/lucene/search/DisjunctionMaxQuery;

    # getter for: Lorg/apache/lucene/search/DisjunctionMaxQuery;->tieBreakerMultiplier:F
    invoke-static {v6}, Lorg/apache/lucene/search/DisjunctionMaxQuery;->access$2(Lorg/apache/lucene/search/DisjunctionMaxQuery;)F

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " times others of:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 178
    :cond_3
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/search/Weight;

    .line 179
    .local v4, "wt":Lorg/apache/lucene/search/Weight;
    invoke-virtual {v4, p1, p2}, Lorg/apache/lucene/search/Weight;->explain(Lorg/apache/lucene/index/AtomicReaderContext;I)Lorg/apache/lucene/search/Explanation;

    move-result-object v0

    .line 180
    .local v0, "e":Lorg/apache/lucene/search/Explanation;
    invoke-virtual {v0}, Lorg/apache/lucene/search/Explanation;->isMatch()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 181
    sget-object v6, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v2, v6}, Lorg/apache/lucene/search/ComplexExplanation;->setMatch(Ljava/lang/Boolean;)V

    .line 182
    invoke-virtual {v2, v0}, Lorg/apache/lucene/search/ComplexExplanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 183
    invoke-virtual {v0}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v6

    add-float/2addr v3, v6

    .line 184
    invoke-virtual {v0}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v6

    invoke-static {v1, v6}, Ljava/lang/Math;->max(FF)F

    move-result v1

    goto :goto_2
.end method

.method public getQuery()Lorg/apache/lucene/search/Query;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery$DisjunctionMaxWeight;->this$0:Lorg/apache/lucene/search/DisjunctionMaxQuery;

    return-object v0
.end method

.method public getValueForNormalization()F
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 133
    const/4 v2, 0x0

    .local v2, "max":F
    const/4 v4, 0x0

    .line 134
    .local v4, "sum":F
    iget-object v5, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery$DisjunctionMaxWeight;->weights:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_0

    .line 140
    iget-object v5, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery$DisjunctionMaxWeight;->this$0:Lorg/apache/lucene/search/DisjunctionMaxQuery;

    invoke-virtual {v5}, Lorg/apache/lucene/search/DisjunctionMaxQuery;->getBoost()F

    move-result v0

    .line 141
    .local v0, "boost":F
    sub-float v5, v4, v2

    iget-object v6, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery$DisjunctionMaxWeight;->this$0:Lorg/apache/lucene/search/DisjunctionMaxQuery;

    # getter for: Lorg/apache/lucene/search/DisjunctionMaxQuery;->tieBreakerMultiplier:F
    invoke-static {v6}, Lorg/apache/lucene/search/DisjunctionMaxQuery;->access$2(Lorg/apache/lucene/search/DisjunctionMaxQuery;)F

    move-result v6

    mul-float/2addr v5, v6

    iget-object v6, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery$DisjunctionMaxWeight;->this$0:Lorg/apache/lucene/search/DisjunctionMaxQuery;

    # getter for: Lorg/apache/lucene/search/DisjunctionMaxQuery;->tieBreakerMultiplier:F
    invoke-static {v6}, Lorg/apache/lucene/search/DisjunctionMaxQuery;->access$2(Lorg/apache/lucene/search/DisjunctionMaxQuery;)F

    move-result v6

    mul-float/2addr v5, v6

    add-float/2addr v5, v2

    mul-float/2addr v5, v0

    mul-float/2addr v5, v0

    return v5

    .line 134
    .end local v0    # "boost":F
    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/search/Weight;

    .line 135
    .local v1, "currentWeight":Lorg/apache/lucene/search/Weight;
    invoke-virtual {v1}, Lorg/apache/lucene/search/Weight;->getValueForNormalization()F

    move-result v3

    .line 136
    .local v3, "sub":F
    add-float/2addr v4, v3

    .line 137
    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    goto :goto_0
.end method

.method public normalize(FF)V
    .locals 3
    .param p1, "norm"    # F
    .param p2, "topLevelBoost"    # F

    .prologue
    .line 147
    iget-object v1, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery$DisjunctionMaxWeight;->this$0:Lorg/apache/lucene/search/DisjunctionMaxQuery;

    invoke-virtual {v1}, Lorg/apache/lucene/search/DisjunctionMaxQuery;->getBoost()F

    move-result v1

    mul-float/2addr p2, v1

    .line 148
    iget-object v1, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery$DisjunctionMaxWeight;->weights:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 151
    return-void

    .line 148
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/Weight;

    .line 149
    .local v0, "wt":Lorg/apache/lucene/search/Weight;
    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/search/Weight;->normalize(FF)V

    goto :goto_0
.end method

.method public scorer(Lorg/apache/lucene/index/AtomicReaderContext;ZZLorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/Scorer;
    .locals 9
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "scoreDocsInOrder"    # Z
    .param p3, "topScorer"    # Z
    .param p4, "acceptDocs"    # Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 157
    iget-object v6, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery$DisjunctionMaxWeight;->weights:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    new-array v3, v6, [Lorg/apache/lucene/search/Scorer;

    .line 158
    .local v3, "scorers":[Lorg/apache/lucene/search/Scorer;
    const/4 v0, 0x0

    .line 159
    .local v0, "idx":I
    iget-object v6, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery$DisjunctionMaxWeight;->weights:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_1

    .line 166
    if-nez v0, :cond_2

    const/4 v2, 0x0

    .line 168
    :goto_1
    return-object v2

    .line 159
    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/search/Weight;

    .line 161
    .local v5, "w":Lorg/apache/lucene/search/Weight;
    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-virtual {v5, p1, v7, v8, p4}, Lorg/apache/lucene/search/Weight;->scorer(Lorg/apache/lucene/index/AtomicReaderContext;ZZLorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/Scorer;

    move-result-object v4

    .line 162
    .local v4, "subScorer":Lorg/apache/lucene/search/Scorer;
    if-eqz v4, :cond_0

    .line 163
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "idx":I
    .local v1, "idx":I
    aput-object v4, v3, v0

    move v0, v1

    .end local v1    # "idx":I
    .restart local v0    # "idx":I
    goto :goto_0

    .line 167
    .end local v4    # "subScorer":Lorg/apache/lucene/search/Scorer;
    .end local v5    # "w":Lorg/apache/lucene/search/Weight;
    :cond_2
    new-instance v2, Lorg/apache/lucene/search/DisjunctionMaxScorer;

    iget-object v6, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery$DisjunctionMaxWeight;->this$0:Lorg/apache/lucene/search/DisjunctionMaxQuery;

    # getter for: Lorg/apache/lucene/search/DisjunctionMaxQuery;->tieBreakerMultiplier:F
    invoke-static {v6}, Lorg/apache/lucene/search/DisjunctionMaxQuery;->access$2(Lorg/apache/lucene/search/DisjunctionMaxQuery;)F

    move-result v6

    invoke-direct {v2, p0, v6, v3, v0}, Lorg/apache/lucene/search/DisjunctionMaxScorer;-><init>(Lorg/apache/lucene/search/Weight;F[Lorg/apache/lucene/search/Scorer;I)V

    .line 168
    .local v2, "result":Lorg/apache/lucene/search/DisjunctionMaxScorer;
    goto :goto_1
.end method
