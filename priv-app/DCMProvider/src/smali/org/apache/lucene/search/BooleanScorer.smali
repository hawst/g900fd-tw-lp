.class final Lorg/apache/lucene/search/BooleanScorer;
.super Lorg/apache/lucene/search/Scorer;
.source "BooleanScorer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/BooleanScorer$BooleanScorerCollector;,
        Lorg/apache/lucene/search/BooleanScorer$Bucket;,
        Lorg/apache/lucene/search/BooleanScorer$BucketScorer;,
        Lorg/apache/lucene/search/BooleanScorer$BucketTable;,
        Lorg/apache/lucene/search/BooleanScorer$SubScorer;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final PROHIBITED_MASK:I = 0x1


# instance fields
.field private bucketTable:Lorg/apache/lucene/search/BooleanScorer$BucketTable;

.field private final coordFactors:[F

.field private current:Lorg/apache/lucene/search/BooleanScorer$Bucket;

.field private end:I

.field private final minNrShouldMatch:I

.field private scorers:Lorg/apache/lucene/search/BooleanScorer$SubScorer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    const-class v0, Lorg/apache/lucene/search/BooleanScorer;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/BooleanScorer;->$assertionsDisabled:Z

    .line 208
    return-void

    .line 61
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;ZILjava/util/List;Ljava/util/List;I)V
    .locals 8
    .param p1, "weight"    # Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;
    .param p2, "disableCoord"    # Z
    .param p3, "minNrShouldMatch"    # I
    .param p6, "maxCoord"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;",
            "ZI",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/search/Scorer;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/search/Scorer;",
            ">;I)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 212
    .local p4, "optionalScorers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/Scorer;>;"
    .local p5, "prohibitedScorers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/Scorer;>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/Scorer;-><init>(Lorg/apache/lucene/search/Weight;)V

    .line 199
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/search/BooleanScorer;->scorers:Lorg/apache/lucene/search/BooleanScorer$SubScorer;

    .line 200
    new-instance v0, Lorg/apache/lucene/search/BooleanScorer$BucketTable;

    invoke-direct {v0}, Lorg/apache/lucene/search/BooleanScorer$BucketTable;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/search/BooleanScorer;->bucketTable:Lorg/apache/lucene/search/BooleanScorer$BucketTable;

    .line 213
    iput p3, p0, Lorg/apache/lucene/search/BooleanScorer;->minNrShouldMatch:I

    .line 215
    if-eqz p4, :cond_1

    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 216
    invoke-interface {p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_4

    .line 223
    :cond_1
    if-eqz p5, :cond_3

    invoke-interface {p5}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 224
    invoke-interface {p5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_2
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_5

    .line 231
    :cond_3
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [F

    iput-object v0, p0, Lorg/apache/lucene/search/BooleanScorer;->coordFactors:[F

    .line 232
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_2
    iget-object v0, p0, Lorg/apache/lucene/search/BooleanScorer;->coordFactors:[F

    array-length v0, v0

    if-lt v6, v0, :cond_6

    .line 235
    return-void

    .line 216
    .end local v6    # "i":I
    :cond_4
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/search/Scorer;

    .line 217
    .local v1, "scorer":Lorg/apache/lucene/search/Scorer;
    invoke-virtual {v1}, Lorg/apache/lucene/search/Scorer;->nextDoc()I

    move-result v0

    const v2, 0x7fffffff

    if-eq v0, v2, :cond_0

    .line 218
    new-instance v0, Lorg/apache/lucene/search/BooleanScorer$SubScorer;

    const/4 v2, 0x0

    const/4 v3, 0x0

    iget-object v4, p0, Lorg/apache/lucene/search/BooleanScorer;->bucketTable:Lorg/apache/lucene/search/BooleanScorer$BucketTable;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lorg/apache/lucene/search/BooleanScorer$BucketTable;->newCollector(I)Lorg/apache/lucene/search/Collector;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/lucene/search/BooleanScorer;->scorers:Lorg/apache/lucene/search/BooleanScorer$SubScorer;

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/search/BooleanScorer$SubScorer;-><init>(Lorg/apache/lucene/search/Scorer;ZZLorg/apache/lucene/search/Collector;Lorg/apache/lucene/search/BooleanScorer$SubScorer;)V

    iput-object v0, p0, Lorg/apache/lucene/search/BooleanScorer;->scorers:Lorg/apache/lucene/search/BooleanScorer$SubScorer;

    goto :goto_0

    .line 224
    .end local v1    # "scorer":Lorg/apache/lucene/search/Scorer;
    :cond_5
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/search/Scorer;

    .line 225
    .restart local v1    # "scorer":Lorg/apache/lucene/search/Scorer;
    invoke-virtual {v1}, Lorg/apache/lucene/search/Scorer;->nextDoc()I

    move-result v0

    const v2, 0x7fffffff

    if-eq v0, v2, :cond_2

    .line 226
    new-instance v0, Lorg/apache/lucene/search/BooleanScorer$SubScorer;

    const/4 v2, 0x0

    const/4 v3, 0x1

    iget-object v4, p0, Lorg/apache/lucene/search/BooleanScorer;->bucketTable:Lorg/apache/lucene/search/BooleanScorer$BucketTable;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lorg/apache/lucene/search/BooleanScorer$BucketTable;->newCollector(I)Lorg/apache/lucene/search/Collector;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/lucene/search/BooleanScorer;->scorers:Lorg/apache/lucene/search/BooleanScorer$SubScorer;

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/search/BooleanScorer$SubScorer;-><init>(Lorg/apache/lucene/search/Scorer;ZZLorg/apache/lucene/search/Collector;Lorg/apache/lucene/search/BooleanScorer$SubScorer;)V

    iput-object v0, p0, Lorg/apache/lucene/search/BooleanScorer;->scorers:Lorg/apache/lucene/search/BooleanScorer$SubScorer;

    goto :goto_1

    .line 233
    .end local v1    # "scorer":Lorg/apache/lucene/search/Scorer;
    .restart local v6    # "i":I
    :cond_6
    iget-object v2, p0, Lorg/apache/lucene/search/BooleanScorer;->coordFactors:[F

    if-eqz p2, :cond_7

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_3
    aput v0, v2, v6

    .line 232
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 233
    :cond_7
    invoke-virtual {p1, v6, p6}, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->coord(II)F

    move-result v0

    goto :goto_3
.end method


# virtual methods
.method public advance(I)I
    .locals 1
    .param p1, "target"    # I

    .prologue
    .line 309
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public cost()J
    .locals 2

    .prologue
    .line 334
    const-wide/32 v0, 0x7fffffff

    return-wide v0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 314
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public freq()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 329
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getChildren()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/search/Scorer$ChildScorer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 356
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public nextDoc()I
    .locals 1

    .prologue
    .line 319
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public score()F
    .locals 1

    .prologue
    .line 324
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public score(Lorg/apache/lucene/search/Collector;)V
    .locals 2
    .param p1, "collector"    # Lorg/apache/lucene/search/Collector;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 339
    const v0, 0x7fffffff

    const/4 v1, -0x1

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/search/BooleanScorer;->score(Lorg/apache/lucene/search/Collector;II)Z

    .line 340
    return-void
.end method

.method public score(Lorg/apache/lucene/search/Collector;II)Z
    .locals 10
    .param p1, "collector"    # Lorg/apache/lucene/search/Collector;
    .param p2, "max"    # I
    .param p3, "firstDocID"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 241
    sget-boolean v5, Lorg/apache/lucene/search/BooleanScorer;->$assertionsDisabled:Z

    if-nez v5, :cond_0

    const/4 v5, -0x1

    if-eq p3, v5, :cond_0

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 244
    :cond_0
    new-instance v0, Lorg/apache/lucene/search/BooleanScorer$BucketScorer;

    iget-object v5, p0, Lorg/apache/lucene/search/BooleanScorer;->weight:Lorg/apache/lucene/search/Weight;

    invoke-direct {v0, v5}, Lorg/apache/lucene/search/BooleanScorer$BucketScorer;-><init>(Lorg/apache/lucene/search/Weight;)V

    .line 247
    .local v0, "bs":Lorg/apache/lucene/search/BooleanScorer$BucketScorer;
    invoke-virtual {p1, v0}, Lorg/apache/lucene/search/Collector;->setScorer(Lorg/apache/lucene/search/Scorer;)V

    .line 249
    :cond_1
    iget-object v5, p0, Lorg/apache/lucene/search/BooleanScorer;->bucketTable:Lorg/apache/lucene/search/BooleanScorer$BucketTable;

    const/4 v6, 0x0

    iput-object v6, v5, Lorg/apache/lucene/search/BooleanScorer$BucketTable;->first:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    .line 251
    :goto_0
    iget-object v5, p0, Lorg/apache/lucene/search/BooleanScorer;->current:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    if-nez v5, :cond_2

    .line 285
    iget-object v5, p0, Lorg/apache/lucene/search/BooleanScorer;->bucketTable:Lorg/apache/lucene/search/BooleanScorer$BucketTable;

    iget-object v5, v5, Lorg/apache/lucene/search/BooleanScorer$BucketTable;->first:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    if-eqz v5, :cond_5

    .line 286
    iget-object v5, p0, Lorg/apache/lucene/search/BooleanScorer;->bucketTable:Lorg/apache/lucene/search/BooleanScorer$BucketTable;

    iget-object v5, v5, Lorg/apache/lucene/search/BooleanScorer$BucketTable;->first:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    iput-object v5, p0, Lorg/apache/lucene/search/BooleanScorer;->current:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    .line 287
    iget-object v5, p0, Lorg/apache/lucene/search/BooleanScorer;->bucketTable:Lorg/apache/lucene/search/BooleanScorer$BucketTable;

    iget-object v6, p0, Lorg/apache/lucene/search/BooleanScorer;->current:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    iget-object v6, v6, Lorg/apache/lucene/search/BooleanScorer$Bucket;->next:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    iput-object v6, v5, Lorg/apache/lucene/search/BooleanScorer$BucketTable;->first:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    .line 288
    const/4 v5, 0x1

    .line 304
    :goto_1
    return v5

    .line 254
    :cond_2
    iget-object v5, p0, Lorg/apache/lucene/search/BooleanScorer;->current:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    iget v5, v5, Lorg/apache/lucene/search/BooleanScorer$Bucket;->bits:I

    and-int/lit8 v5, v5, 0x1

    if-nez v5, :cond_4

    .line 266
    iget-object v5, p0, Lorg/apache/lucene/search/BooleanScorer;->current:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    iget v5, v5, Lorg/apache/lucene/search/BooleanScorer$Bucket;->doc:I

    if-lt v5, p2, :cond_3

    .line 267
    iget-object v4, p0, Lorg/apache/lucene/search/BooleanScorer;->current:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    .line 268
    .local v4, "tmp":Lorg/apache/lucene/search/BooleanScorer$Bucket;
    iget-object v5, p0, Lorg/apache/lucene/search/BooleanScorer;->current:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    iget-object v5, v5, Lorg/apache/lucene/search/BooleanScorer$Bucket;->next:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    iput-object v5, p0, Lorg/apache/lucene/search/BooleanScorer;->current:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    .line 269
    iget-object v5, p0, Lorg/apache/lucene/search/BooleanScorer;->bucketTable:Lorg/apache/lucene/search/BooleanScorer$BucketTable;

    iget-object v5, v5, Lorg/apache/lucene/search/BooleanScorer$BucketTable;->first:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    iput-object v5, v4, Lorg/apache/lucene/search/BooleanScorer$Bucket;->next:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    .line 270
    iget-object v5, p0, Lorg/apache/lucene/search/BooleanScorer;->bucketTable:Lorg/apache/lucene/search/BooleanScorer$BucketTable;

    iput-object v4, v5, Lorg/apache/lucene/search/BooleanScorer$BucketTable;->first:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    goto :goto_0

    .line 274
    .end local v4    # "tmp":Lorg/apache/lucene/search/BooleanScorer$Bucket;
    :cond_3
    iget-object v5, p0, Lorg/apache/lucene/search/BooleanScorer;->current:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    iget v5, v5, Lorg/apache/lucene/search/BooleanScorer$Bucket;->coord:I

    iget v6, p0, Lorg/apache/lucene/search/BooleanScorer;->minNrShouldMatch:I

    if-lt v5, v6, :cond_4

    .line 275
    iget-object v5, p0, Lorg/apache/lucene/search/BooleanScorer;->current:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    iget-wide v6, v5, Lorg/apache/lucene/search/BooleanScorer$Bucket;->score:D

    iget-object v5, p0, Lorg/apache/lucene/search/BooleanScorer;->coordFactors:[F

    iget-object v8, p0, Lorg/apache/lucene/search/BooleanScorer;->current:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    iget v8, v8, Lorg/apache/lucene/search/BooleanScorer$Bucket;->coord:I

    aget v5, v5, v8

    float-to-double v8, v5

    mul-double/2addr v6, v8

    iput-wide v6, v0, Lorg/apache/lucene/search/BooleanScorer$BucketScorer;->score:D

    .line 276
    iget-object v5, p0, Lorg/apache/lucene/search/BooleanScorer;->current:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    iget v5, v5, Lorg/apache/lucene/search/BooleanScorer$Bucket;->doc:I

    iput v5, v0, Lorg/apache/lucene/search/BooleanScorer$BucketScorer;->doc:I

    .line 277
    iget-object v5, p0, Lorg/apache/lucene/search/BooleanScorer;->current:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    iget v5, v5, Lorg/apache/lucene/search/BooleanScorer$Bucket;->coord:I

    iput v5, v0, Lorg/apache/lucene/search/BooleanScorer$BucketScorer;->freq:I

    .line 278
    iget-object v5, p0, Lorg/apache/lucene/search/BooleanScorer;->current:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    iget v5, v5, Lorg/apache/lucene/search/BooleanScorer$Bucket;->doc:I

    invoke-virtual {p1, v5}, Lorg/apache/lucene/search/Collector;->collect(I)V

    .line 282
    :cond_4
    iget-object v5, p0, Lorg/apache/lucene/search/BooleanScorer;->current:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    iget-object v5, v5, Lorg/apache/lucene/search/BooleanScorer$Bucket;->next:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    iput-object v5, p0, Lorg/apache/lucene/search/BooleanScorer;->current:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    goto :goto_0

    .line 292
    :cond_5
    const/4 v1, 0x0

    .line 293
    .local v1, "more":Z
    iget v5, p0, Lorg/apache/lucene/search/BooleanScorer;->end:I

    add-int/lit16 v5, v5, 0x800

    iput v5, p0, Lorg/apache/lucene/search/BooleanScorer;->end:I

    .line 294
    iget-object v2, p0, Lorg/apache/lucene/search/BooleanScorer;->scorers:Lorg/apache/lucene/search/BooleanScorer$SubScorer;

    .local v2, "sub":Lorg/apache/lucene/search/BooleanScorer$SubScorer;
    :goto_2
    if-nez v2, :cond_6

    .line 300
    iget-object v5, p0, Lorg/apache/lucene/search/BooleanScorer;->bucketTable:Lorg/apache/lucene/search/BooleanScorer$BucketTable;

    iget-object v5, v5, Lorg/apache/lucene/search/BooleanScorer$BucketTable;->first:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    iput-object v5, p0, Lorg/apache/lucene/search/BooleanScorer;->current:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    .line 302
    iget-object v5, p0, Lorg/apache/lucene/search/BooleanScorer;->current:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    if-nez v5, :cond_1

    if-nez v1, :cond_1

    .line 304
    const/4 v5, 0x0

    goto :goto_1

    .line 295
    :cond_6
    iget-object v5, v2, Lorg/apache/lucene/search/BooleanScorer$SubScorer;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v5}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v3

    .line 296
    .local v3, "subScorerDocID":I
    const v5, 0x7fffffff

    if-eq v3, v5, :cond_7

    .line 297
    iget-object v5, v2, Lorg/apache/lucene/search/BooleanScorer$SubScorer;->scorer:Lorg/apache/lucene/search/Scorer;

    iget-object v6, v2, Lorg/apache/lucene/search/BooleanScorer$SubScorer;->collector:Lorg/apache/lucene/search/Collector;

    iget v7, p0, Lorg/apache/lucene/search/BooleanScorer;->end:I

    invoke-virtual {v5, v6, v7, v3}, Lorg/apache/lucene/search/Scorer;->score(Lorg/apache/lucene/search/Collector;II)Z

    move-result v5

    or-int/2addr v1, v5

    .line 294
    :cond_7
    iget-object v2, v2, Lorg/apache/lucene/search/BooleanScorer$SubScorer;->next:Lorg/apache/lucene/search/BooleanScorer$SubScorer;

    goto :goto_2
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 344
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 345
    .local v0, "buffer":Ljava/lang/StringBuilder;
    const-string v2, "boolean("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 346
    iget-object v1, p0, Lorg/apache/lucene/search/BooleanScorer;->scorers:Lorg/apache/lucene/search/BooleanScorer$SubScorer;

    .local v1, "sub":Lorg/apache/lucene/search/BooleanScorer$SubScorer;
    :goto_0
    if-nez v1, :cond_0

    .line 350
    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 351
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 347
    :cond_0
    iget-object v2, v1, Lorg/apache/lucene/search/BooleanScorer$SubScorer;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 348
    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 346
    iget-object v1, v1, Lorg/apache/lucene/search/BooleanScorer$SubScorer;->next:Lorg/apache/lucene/search/BooleanScorer$SubScorer;

    goto :goto_0
.end method
