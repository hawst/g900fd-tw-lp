.class public Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;
.super Lorg/apache/lucene/search/Weight;
.source "ConstantScoreQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/ConstantScoreQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "ConstantWeight"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final innerWeight:Lorg/apache/lucene/search/Weight;

.field private queryNorm:F

.field private queryWeight:F

.field final synthetic this$0:Lorg/apache/lucene/search/ConstantScoreQuery;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 95
    const-class v0, Lorg/apache/lucene/search/ConstantScoreQuery;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/search/ConstantScoreQuery;Lorg/apache/lucene/search/IndexSearcher;)V
    .locals 1
    .param p2, "searcher"    # Lorg/apache/lucene/search/IndexSearcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 100
    iput-object p1, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->this$0:Lorg/apache/lucene/search/ConstantScoreQuery;

    invoke-direct {p0}, Lorg/apache/lucene/search/Weight;-><init>()V

    .line 101
    iget-object v0, p1, Lorg/apache/lucene/search/ConstantScoreQuery;->query:Lorg/apache/lucene/search/Query;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->innerWeight:Lorg/apache/lucene/search/Weight;

    .line 102
    return-void

    .line 101
    :cond_0
    iget-object v0, p1, Lorg/apache/lucene/search/ConstantScoreQuery;->query:Lorg/apache/lucene/search/Query;

    invoke-virtual {v0, p2}, Lorg/apache/lucene/search/Query;->createWeight(Lorg/apache/lucene/search/IndexSearcher;)Lorg/apache/lucene/search/Weight;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public explain(Lorg/apache/lucene/index/AtomicReaderContext;I)Lorg/apache/lucene/search/Explanation;
    .locals 6
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 154
    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/lucene/index/AtomicReader;->getLiveDocs()Lorg/apache/lucene/util/Bits;

    move-result-object v4

    invoke-virtual {p0, p1, v1, v3, v4}, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->scorer(Lorg/apache/lucene/index/AtomicReaderContext;ZZLorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/Scorer;

    move-result-object v0

    .line 155
    .local v0, "cs":Lorg/apache/lucene/search/Scorer;
    if-eqz v0, :cond_0

    invoke-virtual {v0, p2}, Lorg/apache/lucene/search/Scorer;->advance(I)I

    move-result v4

    if-ne v4, p2, :cond_0

    .line 157
    .local v1, "exists":Z
    :goto_0
    new-instance v2, Lorg/apache/lucene/search/ComplexExplanation;

    invoke-direct {v2}, Lorg/apache/lucene/search/ComplexExplanation;-><init>()V

    .line 158
    .local v2, "result":Lorg/apache/lucene/search/ComplexExplanation;
    if-eqz v1, :cond_1

    .line 159
    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->this$0:Lorg/apache/lucene/search/ConstantScoreQuery;

    invoke-virtual {v4}, Lorg/apache/lucene/search/ConstantScoreQuery;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, ", product of:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/apache/lucene/search/ComplexExplanation;->setDescription(Ljava/lang/String;)V

    .line 160
    iget v3, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->queryWeight:F

    invoke-virtual {v2, v3}, Lorg/apache/lucene/search/ComplexExplanation;->setValue(F)V

    .line 161
    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/search/ComplexExplanation;->setMatch(Ljava/lang/Boolean;)V

    .line 162
    new-instance v3, Lorg/apache/lucene/search/Explanation;

    iget-object v4, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->this$0:Lorg/apache/lucene/search/ConstantScoreQuery;

    invoke-virtual {v4}, Lorg/apache/lucene/search/ConstantScoreQuery;->getBoost()F

    move-result v4

    const-string v5, "boost"

    invoke-direct {v3, v4, v5}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    invoke-virtual {v2, v3}, Lorg/apache/lucene/search/ComplexExplanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 163
    new-instance v3, Lorg/apache/lucene/search/Explanation;

    iget v4, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->queryNorm:F

    const-string v5, "queryNorm"

    invoke-direct {v3, v4, v5}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    invoke-virtual {v2, v3}, Lorg/apache/lucene/search/ComplexExplanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 169
    :goto_1
    return-object v2

    .end local v1    # "exists":Z
    .end local v2    # "result":Lorg/apache/lucene/search/ComplexExplanation;
    :cond_0
    move v1, v3

    .line 155
    goto :goto_0

    .line 165
    .restart local v1    # "exists":Z
    .restart local v2    # "result":Lorg/apache/lucene/search/ComplexExplanation;
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->this$0:Lorg/apache/lucene/search/ConstantScoreQuery;

    invoke-virtual {v4}, Lorg/apache/lucene/search/ConstantScoreQuery;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " doesn\'t match id "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/apache/lucene/search/ComplexExplanation;->setDescription(Ljava/lang/String;)V

    .line 166
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lorg/apache/lucene/search/ComplexExplanation;->setValue(F)V

    .line 167
    sget-object v3, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/search/ComplexExplanation;->setMatch(Ljava/lang/Boolean;)V

    goto :goto_1
.end method

.method public getQuery()Lorg/apache/lucene/search/Query;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->this$0:Lorg/apache/lucene/search/ConstantScoreQuery;

    return-object v0
.end method

.method public getValueForNormalization()F
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 112
    iget-object v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->innerWeight:Lorg/apache/lucene/search/Weight;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->innerWeight:Lorg/apache/lucene/search/Weight;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Weight;->getValueForNormalization()F

    .line 113
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->this$0:Lorg/apache/lucene/search/ConstantScoreQuery;

    invoke-virtual {v0}, Lorg/apache/lucene/search/ConstantScoreQuery;->getBoost()F

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->queryWeight:F

    .line 114
    iget v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->queryWeight:F

    iget v1, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->queryWeight:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public normalize(FF)V
    .locals 2
    .param p1, "norm"    # F
    .param p2, "topLevelBoost"    # F

    .prologue
    .line 119
    mul-float v0, p1, p2

    iput v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->queryNorm:F

    .line 120
    iget v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->queryWeight:F

    iget v1, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->queryNorm:F

    mul-float/2addr v0, v1

    iput v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->queryWeight:F

    .line 122
    iget-object v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->innerWeight:Lorg/apache/lucene/search/Weight;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->innerWeight:Lorg/apache/lucene/search/Weight;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/search/Weight;->normalize(FF)V

    .line 123
    :cond_0
    return-void
.end method

.method public scorer(Lorg/apache/lucene/index/AtomicReaderContext;ZZLorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/Scorer;
    .locals 5
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "scoreDocsInOrder"    # Z
    .param p3, "topScorer"    # Z
    .param p4, "acceptDocs"    # Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 129
    iget-object v3, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->this$0:Lorg/apache/lucene/search/ConstantScoreQuery;

    iget-object v3, v3, Lorg/apache/lucene/search/ConstantScoreQuery;->filter:Lorg/apache/lucene/search/Filter;

    if-eqz v3, :cond_3

    .line 130
    sget-boolean v3, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->this$0:Lorg/apache/lucene/search/ConstantScoreQuery;

    iget-object v3, v3, Lorg/apache/lucene/search/ConstantScoreQuery;->query:Lorg/apache/lucene/search/Query;

    if-eqz v3, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 131
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->this$0:Lorg/apache/lucene/search/ConstantScoreQuery;

    iget-object v3, v3, Lorg/apache/lucene/search/ConstantScoreQuery;->filter:Lorg/apache/lucene/search/Filter;

    invoke-virtual {v3, p1, p4}, Lorg/apache/lucene/search/Filter;->getDocIdSet(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/DocIdSet;

    move-result-object v0

    .line 132
    .local v0, "dis":Lorg/apache/lucene/search/DocIdSet;
    if-nez v0, :cond_2

    .line 144
    .end local v0    # "dis":Lorg/apache/lucene/search/DocIdSet;
    :cond_1
    :goto_0
    return-object v2

    .line 135
    .restart local v0    # "dis":Lorg/apache/lucene/search/DocIdSet;
    :cond_2
    invoke-virtual {v0}, Lorg/apache/lucene/search/DocIdSet;->iterator()Lorg/apache/lucene/search/DocIdSetIterator;

    move-result-object v1

    .line 141
    .end local v0    # "dis":Lorg/apache/lucene/search/DocIdSet;
    .local v1, "disi":Lorg/apache/lucene/search/DocIdSetIterator;
    :goto_1
    if-eqz v1, :cond_1

    .line 144
    new-instance v2, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;

    iget-object v3, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->this$0:Lorg/apache/lucene/search/ConstantScoreQuery;

    iget v4, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->queryWeight:F

    invoke-direct {v2, v3, v1, p0, v4}, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;-><init>(Lorg/apache/lucene/search/ConstantScoreQuery;Lorg/apache/lucene/search/DocIdSetIterator;Lorg/apache/lucene/search/Weight;F)V

    goto :goto_0

    .line 137
    .end local v1    # "disi":Lorg/apache/lucene/search/DocIdSetIterator;
    :cond_3
    sget-boolean v3, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->$assertionsDisabled:Z

    if-nez v3, :cond_5

    iget-object v3, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->this$0:Lorg/apache/lucene/search/ConstantScoreQuery;

    iget-object v3, v3, Lorg/apache/lucene/search/ConstantScoreQuery;->query:Lorg/apache/lucene/search/Query;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->innerWeight:Lorg/apache/lucene/search/Weight;

    if-nez v3, :cond_5

    :cond_4
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 138
    :cond_5
    iget-object v3, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->innerWeight:Lorg/apache/lucene/search/Weight;

    invoke-virtual {v3, p1, p2, p3, p4}, Lorg/apache/lucene/search/Weight;->scorer(Lorg/apache/lucene/index/AtomicReaderContext;ZZLorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/Scorer;

    move-result-object v1

    .restart local v1    # "disi":Lorg/apache/lucene/search/DocIdSetIterator;
    goto :goto_1
.end method

.method public scoresDocsOutOfOrder()Z
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->innerWeight:Lorg/apache/lucene/search/Weight;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->innerWeight:Lorg/apache/lucene/search/Weight;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Weight;->scoresDocsOutOfOrder()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
