.class public Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;
.super Lorg/apache/lucene/search/spans/SpanScorer;
.source "PayloadTermQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "PayloadTermSpanScorer"
.end annotation


# instance fields
.field protected payload:Lorg/apache/lucene/util/BytesRef;

.field protected payloadScore:F

.field protected payloadsSeen:I

.field private final termSpans:Lorg/apache/lucene/search/spans/TermSpans;

.field final synthetic this$1:Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;Lorg/apache/lucene/search/spans/TermSpans;Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;)V
    .locals 0
    .param p2, "spans"    # Lorg/apache/lucene/search/spans/TermSpans;
    .param p3, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p4, "docScorer"    # Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 94
    iput-object p1, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->this$1:Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;

    .line 95
    invoke-direct {p0, p2, p3, p4}, Lorg/apache/lucene/search/spans/SpanScorer;-><init>(Lorg/apache/lucene/search/spans/Spans;Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;)V

    .line 96
    iput-object p2, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->termSpans:Lorg/apache/lucene/search/spans/TermSpans;

    .line 97
    return-void
.end method


# virtual methods
.method protected getPayloadScore()F
    .locals 5

    .prologue
    .line 174
    iget-object v0, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->this$1:Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;

    # getter for: Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;->this$0:Lorg/apache/lucene/search/payloads/PayloadTermQuery;
    invoke-static {v0}, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;->access$1(Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;)Lorg/apache/lucene/search/payloads/PayloadTermQuery;

    move-result-object v0

    iget-object v0, v0, Lorg/apache/lucene/search/payloads/PayloadTermQuery;->function:Lorg/apache/lucene/search/payloads/PayloadFunction;

    iget v1, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->doc:I

    iget-object v2, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->this$1:Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;

    # getter for: Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;->this$0:Lorg/apache/lucene/search/payloads/PayloadTermQuery;
    invoke-static {v2}, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;->access$1(Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;)Lorg/apache/lucene/search/payloads/PayloadTermQuery;

    move-result-object v2

    # getter for: Lorg/apache/lucene/search/payloads/PayloadTermQuery;->term:Lorg/apache/lucene/index/Term;
    invoke-static {v2}, Lorg/apache/lucene/search/payloads/PayloadTermQuery;->access$0(Lorg/apache/lucene/search/payloads/PayloadTermQuery;)Lorg/apache/lucene/index/Term;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->payloadsSeen:I

    iget v4, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->payloadScore:F

    invoke-virtual {v0, v1, v2, v3, v4}, Lorg/apache/lucene/search/payloads/PayloadFunction;->docScore(ILjava/lang/String;IF)F

    move-result v0

    return v0
.end method

.method protected getSpanScore()F
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 164
    invoke-super {p0}, Lorg/apache/lucene/search/spans/SpanScorer;->score()F

    move-result v0

    return v0
.end method

.method protected processPayload(Lorg/apache/lucene/search/similarities/Similarity;)V
    .locals 13
    .param p1, "similarity"    # Lorg/apache/lucene/search/similarities/Similarity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 123
    iget-object v0, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->termSpans:Lorg/apache/lucene/search/spans/TermSpans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/TermSpans;->isPayloadAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->termSpans:Lorg/apache/lucene/search/spans/TermSpans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/TermSpans;->getPostings()Lorg/apache/lucene/index/DocsAndPositionsEnum;

    move-result-object v8

    .line 125
    .local v8, "postings":Lorg/apache/lucene/index/DocsAndPositionsEnum;
    invoke-virtual {v8}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->getPayload()Lorg/apache/lucene/util/BytesRef;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->payload:Lorg/apache/lucene/util/BytesRef;

    .line 126
    iget-object v0, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->payload:Lorg/apache/lucene/util/BytesRef;

    if-eqz v0, :cond_1

    .line 127
    iget-object v0, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->this$1:Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;

    # getter for: Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;->this$0:Lorg/apache/lucene/search/payloads/PayloadTermQuery;
    invoke-static {v0}, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;->access$1(Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;)Lorg/apache/lucene/search/payloads/PayloadTermQuery;

    move-result-object v0

    iget-object v0, v0, Lorg/apache/lucene/search/payloads/PayloadTermQuery;->function:Lorg/apache/lucene/search/payloads/PayloadFunction;

    iget v1, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->doc:I

    iget-object v2, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->this$1:Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;

    # getter for: Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;->this$0:Lorg/apache/lucene/search/payloads/PayloadTermQuery;
    invoke-static {v2}, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;->access$1(Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;)Lorg/apache/lucene/search/payloads/PayloadTermQuery;

    move-result-object v2

    # getter for: Lorg/apache/lucene/search/payloads/PayloadTermQuery;->term:Lorg/apache/lucene/index/Term;
    invoke-static {v2}, Lorg/apache/lucene/search/payloads/PayloadTermQuery;->access$0(Lorg/apache/lucene/search/payloads/PayloadTermQuery;)Lorg/apache/lucene/index/Term;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v2

    .line 128
    iget-object v3, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v3}, Lorg/apache/lucene/search/spans/Spans;->start()I

    move-result v3

    iget-object v4, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v4}, Lorg/apache/lucene/search/spans/Spans;->end()I

    move-result v4

    iget v5, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->payloadsSeen:I

    iget v6, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->payloadScore:F

    .line 129
    iget-object v7, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->docScorer:Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;

    iget v9, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->doc:I

    iget-object v10, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v10}, Lorg/apache/lucene/search/spans/Spans;->start()I

    move-result v10

    iget-object v11, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v11}, Lorg/apache/lucene/search/spans/Spans;->end()I

    move-result v11

    iget-object v12, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->payload:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v7, v9, v10, v11, v12}, Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;->computePayloadFactor(IIILorg/apache/lucene/util/BytesRef;)F

    move-result v7

    .line 127
    invoke-virtual/range {v0 .. v7}, Lorg/apache/lucene/search/payloads/PayloadFunction;->currentScore(ILjava/lang/String;IIIFF)F

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->payloadScore:F

    .line 134
    :goto_0
    iget v0, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->payloadsSeen:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->payloadsSeen:I

    .line 139
    .end local v8    # "postings":Lorg/apache/lucene/index/DocsAndPositionsEnum;
    :cond_0
    return-void

    .line 131
    .restart local v8    # "postings":Lorg/apache/lucene/index/DocsAndPositionsEnum;
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->this$1:Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;

    # getter for: Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;->this$0:Lorg/apache/lucene/search/payloads/PayloadTermQuery;
    invoke-static {v0}, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;->access$1(Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;)Lorg/apache/lucene/search/payloads/PayloadTermQuery;

    move-result-object v0

    iget-object v0, v0, Lorg/apache/lucene/search/payloads/PayloadTermQuery;->function:Lorg/apache/lucene/search/payloads/PayloadFunction;

    iget v1, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->doc:I

    iget-object v2, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->this$1:Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;

    # getter for: Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;->this$0:Lorg/apache/lucene/search/payloads/PayloadTermQuery;
    invoke-static {v2}, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;->access$1(Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;)Lorg/apache/lucene/search/payloads/PayloadTermQuery;

    move-result-object v2

    # getter for: Lorg/apache/lucene/search/payloads/PayloadTermQuery;->term:Lorg/apache/lucene/index/Term;
    invoke-static {v2}, Lorg/apache/lucene/search/payloads/PayloadTermQuery;->access$0(Lorg/apache/lucene/search/payloads/PayloadTermQuery;)Lorg/apache/lucene/index/Term;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v2

    .line 132
    iget-object v3, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v3}, Lorg/apache/lucene/search/spans/Spans;->start()I

    move-result v3

    iget-object v4, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v4}, Lorg/apache/lucene/search/spans/Spans;->end()I

    move-result v4

    iget v5, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->payloadsSeen:I

    iget v6, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->payloadScore:F

    const/high16 v7, 0x3f800000    # 1.0f

    .line 131
    invoke-virtual/range {v0 .. v7}, Lorg/apache/lucene/search/payloads/PayloadFunction;->currentScore(ILjava/lang/String;IIIFF)F

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->payloadScore:F

    goto :goto_0
.end method

.method public score()F
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 149
    iget-object v0, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->this$1:Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;

    # getter for: Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;->this$0:Lorg/apache/lucene/search/payloads/PayloadTermQuery;
    invoke-static {v0}, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;->access$1(Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;)Lorg/apache/lucene/search/payloads/PayloadTermQuery;

    move-result-object v0

    # getter for: Lorg/apache/lucene/search/payloads/PayloadTermQuery;->includeSpanScore:Z
    invoke-static {v0}, Lorg/apache/lucene/search/payloads/PayloadTermQuery;->access$1(Lorg/apache/lucene/search/payloads/PayloadTermQuery;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->getSpanScore()F

    move-result v0

    invoke-virtual {p0}, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->getPayloadScore()F

    move-result v1

    mul-float/2addr v0, v1

    :goto_0
    return v0

    .line 150
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->getPayloadScore()F

    move-result v0

    goto :goto_0
.end method

.method protected setFreqCurrentDoc()Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 101
    iget-boolean v2, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->more:Z

    if-nez v2, :cond_1

    .line 119
    :cond_0
    :goto_0
    return v1

    .line 104
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v2}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v2

    iput v2, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->doc:I

    .line 105
    iput v4, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->freq:F

    .line 106
    iput v1, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->numMatches:I

    .line 107
    iput v4, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->payloadScore:F

    .line 108
    iput v1, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->payloadsSeen:I

    .line 109
    :goto_1
    iget-boolean v2, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->more:Z

    if-eqz v2, :cond_2

    iget v2, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->doc:I

    iget-object v3, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v3}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v3

    if-eq v2, v3, :cond_4

    .line 119
    :cond_2
    iget-boolean v2, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->more:Z

    if-nez v2, :cond_3

    iget v2, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->freq:F

    cmpl-float v2, v2, v4

    if-eqz v2, :cond_0

    :cond_3
    const/4 v1, 0x1

    goto :goto_0

    .line 110
    :cond_4
    iget-object v2, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v2}, Lorg/apache/lucene/search/spans/Spans;->end()I

    move-result v2

    iget-object v3, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v3}, Lorg/apache/lucene/search/spans/Spans;->start()I

    move-result v3

    sub-int v0, v2, v3

    .line 112
    .local v0, "matchLength":I
    iget v2, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->freq:F

    iget-object v3, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->docScorer:Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;

    invoke-virtual {v3, v0}, Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;->computeSlopFactor(I)F

    move-result v3

    add-float/2addr v2, v3

    iput v2, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->freq:F

    .line 113
    iget v2, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->numMatches:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->numMatches:I

    .line 114
    iget-object v2, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->this$1:Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;

    # getter for: Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;->similarity:Lorg/apache/lucene/search/similarities/Similarity;
    invoke-static {v2}, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;->access$0(Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;)Lorg/apache/lucene/search/similarities/Similarity;

    move-result-object v2

    invoke-virtual {p0, v2}, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->processPayload(Lorg/apache/lucene/search/similarities/Similarity;)V

    .line 116
    iget-object v2, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v2}, Lorg/apache/lucene/search/spans/Spans;->next()Z

    move-result v2

    iput-boolean v2, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->more:Z

    goto :goto_1
.end method
