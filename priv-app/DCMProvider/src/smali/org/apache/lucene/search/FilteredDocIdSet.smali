.class public abstract Lorg/apache/lucene/search/FilteredDocIdSet;
.super Lorg/apache/lucene/search/DocIdSet;
.source "FilteredDocIdSet.java"


# instance fields
.field private final _innerSet:Lorg/apache/lucene/search/DocIdSet;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/DocIdSet;)V
    .locals 0
    .param p1, "innerSet"    # Lorg/apache/lucene/search/DocIdSet;

    .prologue
    .line 49
    invoke-direct {p0}, Lorg/apache/lucene/search/DocIdSet;-><init>()V

    .line 50
    iput-object p1, p0, Lorg/apache/lucene/search/FilteredDocIdSet;->_innerSet:Lorg/apache/lucene/search/DocIdSet;

    .line 51
    return-void
.end method


# virtual methods
.method public bits()Lorg/apache/lucene/util/Bits;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 61
    iget-object v1, p0, Lorg/apache/lucene/search/FilteredDocIdSet;->_innerSet:Lorg/apache/lucene/search/DocIdSet;

    invoke-virtual {v1}, Lorg/apache/lucene/search/DocIdSet;->bits()Lorg/apache/lucene/util/Bits;

    move-result-object v0

    .line 62
    .local v0, "bits":Lorg/apache/lucene/util/Bits;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lorg/apache/lucene/search/FilteredDocIdSet$1;

    invoke-direct {v1, p0, v0}, Lorg/apache/lucene/search/FilteredDocIdSet$1;-><init>(Lorg/apache/lucene/search/FilteredDocIdSet;Lorg/apache/lucene/util/Bits;)V

    goto :goto_0
.end method

.method public isCacheable()Z
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lorg/apache/lucene/search/FilteredDocIdSet;->_innerSet:Lorg/apache/lucene/search/DocIdSet;

    invoke-virtual {v0}, Lorg/apache/lucene/search/DocIdSet;->isCacheable()Z

    move-result v0

    return v0
.end method

.method public iterator()Lorg/apache/lucene/search/DocIdSetIterator;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 89
    iget-object v1, p0, Lorg/apache/lucene/search/FilteredDocIdSet;->_innerSet:Lorg/apache/lucene/search/DocIdSet;

    invoke-virtual {v1}, Lorg/apache/lucene/search/DocIdSet;->iterator()Lorg/apache/lucene/search/DocIdSetIterator;

    move-result-object v0

    .line 90
    .local v0, "iterator":Lorg/apache/lucene/search/DocIdSetIterator;
    if-nez v0, :cond_0

    .line 91
    const/4 v1, 0x0

    .line 93
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lorg/apache/lucene/search/FilteredDocIdSet$2;

    invoke-direct {v1, p0, v0}, Lorg/apache/lucene/search/FilteredDocIdSet$2;-><init>(Lorg/apache/lucene/search/FilteredDocIdSet;Lorg/apache/lucene/search/DocIdSetIterator;)V

    goto :goto_0
.end method

.method protected abstract match(I)Z
.end method
