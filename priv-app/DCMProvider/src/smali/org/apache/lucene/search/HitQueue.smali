.class final Lorg/apache/lucene/search/HitQueue;
.super Lorg/apache/lucene/util/PriorityQueue;
.source "HitQueue.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/util/PriorityQueue",
        "<",
        "Lorg/apache/lucene/search/ScoreDoc;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(IZ)V
    .locals 0
    .param p1, "size"    # I
    .param p2, "prePopulate"    # Z

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/util/PriorityQueue;-><init>(IZ)V

    .line 65
    return-void
.end method


# virtual methods
.method protected bridge synthetic getSentinelObject()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/search/HitQueue;->getSentinelObject()Lorg/apache/lucene/search/ScoreDoc;

    move-result-object v0

    return-object v0
.end method

.method protected getSentinelObject()Lorg/apache/lucene/search/ScoreDoc;
    .locals 3

    .prologue
    .line 72
    new-instance v0, Lorg/apache/lucene/search/ScoreDoc;

    const v1, 0x7fffffff

    const/high16 v2, -0x800000    # Float.NEGATIVE_INFINITY

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/search/ScoreDoc;-><init>(IF)V

    return-object v0
.end method

.method protected bridge synthetic lessThan(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/search/ScoreDoc;

    check-cast p2, Lorg/apache/lucene/search/ScoreDoc;

    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/search/HitQueue;->lessThan(Lorg/apache/lucene/search/ScoreDoc;Lorg/apache/lucene/search/ScoreDoc;)Z

    move-result v0

    return v0
.end method

.method protected final lessThan(Lorg/apache/lucene/search/ScoreDoc;Lorg/apache/lucene/search/ScoreDoc;)Z
    .locals 4
    .param p1, "hitA"    # Lorg/apache/lucene/search/ScoreDoc;
    .param p2, "hitB"    # Lorg/apache/lucene/search/ScoreDoc;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 77
    iget v2, p1, Lorg/apache/lucene/search/ScoreDoc;->score:F

    iget v3, p2, Lorg/apache/lucene/search/ScoreDoc;->score:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_2

    .line 78
    iget v2, p1, Lorg/apache/lucene/search/ScoreDoc;->doc:I

    iget v3, p2, Lorg/apache/lucene/search/ScoreDoc;->doc:I

    if-le v2, v3, :cond_1

    .line 80
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 78
    goto :goto_0

    .line 80
    :cond_2
    iget v2, p1, Lorg/apache/lucene/search/ScoreDoc;->score:F

    iget v3, p2, Lorg/apache/lucene/search/ScoreDoc;->score:F

    cmpg-float v2, v2, v3

    if-ltz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method
